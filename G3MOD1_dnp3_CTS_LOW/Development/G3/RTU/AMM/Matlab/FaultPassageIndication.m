% Fault Passage Indication
%
%

function FpiCfgAndParams = FaultPassageIndication(FpiCfgAndParams,...
                                                  measuredValue,...
                                                  currentTimeMs)
%#codegen
%
%

coder.inline('always');

if FpiCfgAndParams.cfg.enabled >= 1
    % Reset the fault function if the phase measuredValue has fallen below the
	% fault threshold once the counter has been triggered at least once
    if (FpiCfgAndParams.faultParams.faultEventDetected >= 1) &&...
            (measuredValue < FpiCfgAndParams.cfg.timedFaultCurrent)
        % Reset overcurrent timers
        FpiCfgAndParams.faultParams.faultEventDetected = uint32(0);
        FpiCfgAndParams.faultParams.initialTimeofFaultMs = uint32(0);
        FpiCfgAndParams.faultParams.faultDurationMs = uint32(0);
    end
    
    % Reset inrush timer if the phase current value
	% has fallen below the inrush threshold level
    if (FpiCfgAndParams.faultParams.inrushDetected >= 1) &&...
            (measuredValue < FpiCfgAndParams.cfg.inrushCurrent)
        % Reset inrush timers
        FpiCfgAndParams.faultParams.inrushDurationMs = uint32(0);
        FpiCfgAndParams.faultParams.initTimeofInrushMs = uint32(0);
        FpiCfgAndParams.faultParams.inrushDetected = uint32(0);
    end
    
    % If an instant overcurrent has not been detected previously
    % check the current phase value
    if (FpiCfgAndParams.faultParams.instantAlarm == 0) &&...
            (FpiCfgAndParams.cfg.instantFaultCurrent > 0)
        if (measuredValue >= FpiCfgAndParams.cfg.instantFaultCurrent)
            % Increment
            FpiCfgAndParams.faultParams.instantFaultEventDetected = ...
                FpiCfgAndParams.faultParams.instantFaultEventDetected + 1;
        end
    end
    
    % Disable the fault function if it has found a fault
    % The function is reset remotely or through a timer
    if (FpiCfgAndParams.faultParams.alarm == 0) &&...
            (FpiCfgAndParams.cfg.timedFaultCurrent > 0)
        % Check for motor and transformer inrush(/overload) current
        if (measuredValue >= FpiCfgAndParams.cfg.inrushCurrent)
            
            FpiCfgAndParams.faultParams.inrushDetected =...
                FpiCfgAndParams.faultParams.inrushDetected + 1;

            % first inrush event detected, get initial time
            if(FpiCfgAndParams.faultParams.inrushDetected == 1)
         
                FpiCfgAndParams.faultParams.initTimeofInrushMs = currentTimeMs;
            end

            FpiCfgAndParams.faultParams.inrushDurationMs = currentTimeMs;
            FpiCfgAndParams.faultParams.inrushDurationMs = ElapsedTime(...
                            FpiCfgAndParams.faultParams.initTimeofInrushMs,...
                            FpiCfgAndParams.faultParams.inrushDurationMs);
        end

        % Is the phase measuredValue greater than the fault
        % threshold level and below the inrush current threshold
        if ((measuredValue >= FpiCfgAndParams.cfg.timedFaultCurrent) &&...
             (measuredValue < FpiCfgAndParams.cfg.inrushCurrent))

            FpiCfgAndParams.faultParams.faultEventDetected =...
                FpiCfgAndParams.faultParams.faultEventDetected + 1;
            
            if (FpiCfgAndParams.faultParams.faultEventDetected == 1)
            
                FpiCfgAndParams.faultParams.initialTimeofFaultMs = currentTimeMs;
            end

            FpiCfgAndParams.faultParams.faultDurationMs = currentTimeMs;
            FpiCfgAndParams.faultParams.faultDurationMs = ElapsedTime(...
                        FpiCfgAndParams.faultParams.initialTimeofFaultMs,...
                        FpiCfgAndParams.faultParams.faultDurationMs);

         end
     end % disable overcurrent detect
    
    % Have the inrush inhibition condition been exceeded
    if (FpiCfgAndParams.faultParams.inrushDurationMs >=...
            FpiCfgAndParams.cfg.minInrushDurationMs)
    
        % Alarm for overcurrent event
        FpiCfgAndParams.faultParams.initTimeofAlarmMs = currentTimeMs;

        % Reset timer variable
        FpiCfgAndParams.faultParams.inrushDurationMs  = uint32(0);
        FpiCfgAndParams.faultParams.inrushDetected = uint32(0);
        FpiCfgAndParams.faultParams.initTimeofInrushMs = uint32(0);
        FpiCfgAndParams.faultParams.faultDurationMs  = uint32(0);
        FpiCfgAndParams.faultParams.initialTimeofFaultMs = uint32(0);
        FpiCfgAndParams.faultParams.faultEventDetected = uint32(0);
        % Set alarm
        FpiCfgAndParams.faultParams.alarm = uint32(1);
    end %inrushDurationMs
    
    % Are the Overcurrent fault alarm condition met
    if (FpiCfgAndParams.faultParams.faultDurationMs >=...
            FpiCfgAndParams.cfg.minFaultDurationMs)
    
        % Alarm for overcurrent event
        % Load global fault timer
        FpiCfgAndParams.faultParams.initTimeofAlarmMs = currentTimeMs;
        % Reset timer variable
        FpiCfgAndParams.faultParams.faultDurationMs  = uint32(0);
        FpiCfgAndParams.faultParams.initialTimeofFaultMs = uint32(0);
        FpiCfgAndParams.faultParams.faultEventDetected = uint32(0);
        % Set alarm
        FpiCfgAndParams.faultParams.alarm = uint32(1);
    end % FaultDurationMs
    
    % instant overcurrent warning
    if (FpiCfgAndParams.faultParams.instantFaultEventDetected == 1)

        FpiCfgAndParams.faultParams.initTimeofInstantAlarmMs = currentTimeMs;
        FpiCfgAndParams.faultParams.instantFaultEventDetected = uint32(0);
        % Set instantaneous alarm
        FpiCfgAndParams.faultParams.instantAlarm = uint32(1);
    end % InstantFaultEventDetected > 0

    % Reset overcurrent alarm
    if (FpiCfgAndParams.faultParams.alarm == 1)

        FpiCfgAndParams.faultParams.timeSinceAlarmMs = currentTimeMs;
        FpiCfgAndParams.faultParams.timeSinceAlarmMs = ElapsedTime(...
                        FpiCfgAndParams.faultParams.initTimeofAlarmMs,...
                        FpiCfgAndParams.faultParams.timeSinceAlarmMs);
    end
    
    % Reset instant overcurrent alarm
    if (FpiCfgAndParams.faultParams.instantAlarm == 1)
    
        FpiCfgAndParams.faultParams.timeSinceInstantAlarmMs = currentTimeMs;
        FpiCfgAndParams.faultParams.timeSinceInstantAlarmMs = ElapsedTime(...
                        FpiCfgAndParams.faultParams.initTimeofInstantAlarmMs,...
                        FpiCfgAndParams.faultParams.timeSinceInstantAlarmMs);
    end
     
end % FPI Enabled


end