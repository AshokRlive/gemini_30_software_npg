% Orthogonal Component Frequency Measurement
% Rate of Change of Phase Angle between two consecutive points.
% More info: "Understaning microprocessor-based technology applied to
% relaying", Power System Relaying Committee, WG I-01, Jan 2009.
% Section 4.2 Phasor-Based Methods.

function estimatedFrequency = RecursiveOCFrequency2(ys, yc, ysOld, ycOld, samplingTimePeriod)
% Orthogonal Component Frequency Measurement
% param 
% 
% return

estimatedFrequency = 0;

estimatedFrequency = (atan2(ys,yc)-atan2(ysOld,ycOld))/(2*pi*samplingTimePeriod);                                     
end