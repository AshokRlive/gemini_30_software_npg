%

classdef ApparentPower < uint32
   enumeration
      Sa       (1)           % Apparent Power Phase 1
      Sb       (2)           % Apparent Power Phase 2
      Sc       (3)           % Apparent Power Phase 3
      LAST     (4)           % 
   end
end