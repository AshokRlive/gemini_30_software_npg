% Orthogonal Component based Alternative Real Power 
% Function located here are for the evalution of Real Power
% algorithms per "8.1.3.2 Measurement of Power" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function realPower = OCAltRealPower( us, uc, is, ic, filterGain)

realPower = 0;

realPower = ((uc * ic) + (us * is))/(2 * (filterGain^2));

end