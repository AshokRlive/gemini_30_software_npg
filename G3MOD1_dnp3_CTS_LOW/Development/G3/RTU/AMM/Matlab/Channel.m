%

classdef Channel < int32
   enumeration
      ChA(1),           % Channel (/chip) 1
      ChB(2),           % Channel (/chip) 2
      LAST(3)
   end
   % Include Class Name Prefix in Generated Enumerated Type Value Names
   methods(Static)
      function y = addClassNameToEnumNames()
        y=true;
      end
    end
end