% Calculate Orthogonal Sine and Cosine Components
% based on the recursive algorithm
%

function OCStr = CalculateRecursiveOC( dataIdx, pointsPerCycle, windowLength, UnitsStr, OCStr)
% Calculate the Recursive Orthogonal Sine and Cosine Components
% param 
% 
% return

for windowLenghtIdx = 1:1:windowLength

    % Voltage Components
    OCStr.Cosine.LE.L1(windowLenghtIdx+1) = ...
        RecursiveOCCosine(UnitsStr.LE.L1((dataIdx-windowLength):dataIdx+windowLength),...
                            OCStr.Cosine.LE.L1(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

    OCStr.Sine.LE.L1(windowLenghtIdx+1) = ...
        RecursiveOCSine(UnitsStr.LE.L1((dataIdx-windowLength):dataIdx+windowLength),...
                        OCStr.Sine.LE.L1(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

    OCStr.Cosine.LE.L2(windowLenghtIdx+1) = ...
        RecursiveOCCosine(UnitsStr.LE.L2((dataIdx-windowLength):dataIdx+windowLength),...
                            OCStr.Cosine.LE.L2(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

    OCStr.Sine.LE.L2(windowLenghtIdx+1) = ...
        RecursiveOCSine(UnitsStr.LE.L2((dataIdx-windowLength):dataIdx+windowLength),...
                        OCStr.Sine.LE.L2(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

     OCStr.Cosine.LE.L3(windowLenghtIdx+1) = ...
        RecursiveOCCosine(UnitsStr.LE.L3((dataIdx-windowLength):dataIdx+windowLength),...
                            OCStr.Cosine.LE.L3(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

    OCStr.Sine.LE.L3(windowLenghtIdx+1) = ...
        RecursiveOCSine(UnitsStr.LE.L3((dataIdx-windowLength):dataIdx+windowLength),...
                        OCStr.Sine.LE.L3(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

    OCStr.Cosine.LE.Ne(windowLenghtIdx+1) = ...
        RecursiveOCCosine(UnitsStr.LE.Ne((dataIdx-windowLength):dataIdx+windowLength),...
                            OCStr.Cosine.LE.Ne(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

    OCStr.Sine.LE.Ne(windowLenghtIdx+1) = ...
        RecursiveOCSine(UnitsStr.LE.Ne((dataIdx-windowLength):dataIdx+windowLength),...
                        OCStr.Sine.LE.Ne(windowLenghtIdx), windowLenghtIdx, windowLength, pointsPerCycle/2);

end

OCStr.Cosine.LE.L1(1) = OCStr.Cosine.LE.L1(windowLength+1);
OCStr.Sine.LE.L1(1) = OCStr.Sine.LE.L1(windowLength+1);
OCStr.Cosine.LE.L2(1) = OCStr.Cosine.LE.L2(windowLength+1);
OCStr.Sine.LE.L2(1) = OCStr.Sine.LE.L2(windowLength+1);
OCStr.Cosine.LE.L3(1) = OCStr.Cosine.LE.L3(windowLength+1);
OCStr.Sine.LE.L3(1) = OCStr.Sine.LE.L3(windowLength+1);
OCStr.Cosine.LE.Ne(1) = OCStr.Cosine.LE.Ne(windowLength+1);
OCStr.Sine.LE.Ne(1) = OCStr.Sine.LE.Ne(windowLength+1);


end