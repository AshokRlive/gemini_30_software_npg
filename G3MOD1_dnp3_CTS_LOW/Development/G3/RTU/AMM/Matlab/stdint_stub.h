#ifndef STDINT_STUB_H
#define STDINT_STUB_H

#define ARM_MATH_CM7 1

typedef signed char         int8_t;
typedef signed short int    int16_t;
typedef signed int          int32_t;
typedef signed long long    int64_t;

typedef unsigned char         uint8_t;
typedef unsigned short int    uint16_t;
typedef unsigned int          uint32_t;
typedef unsigned long long    uint64_t;

#ifndef __INLINE
# define __INLINE inline
#endif /* inline */

#endif // defined STDINT_STUB_H