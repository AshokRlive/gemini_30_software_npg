function FpiCfgAndParams = ConfigFPI(FpiCfgAndParams)
%#codegen
% Configure FPI related values
%

coder.inline('always');

% Configure FPI Config values                       
for idxPhase = 1:1:(uint32(WithReferenceTo.Neutral))  
    FpiCfgAndParams(idxPhase).cfg.enabled = uint32(1);
    FpiCfgAndParams(idxPhase).cfg.minFaultDurationMs = uint32(50);
    FpiCfgAndParams(idxPhase).cfg.minInrushDurationMs = uint32(100);
    FpiCfgAndParams(idxPhase).cfg.instantFaultCurrent = uint32(2);
    FpiCfgAndParams(idxPhase).cfg.inrushCurrent = uint32(10);
    FpiCfgAndParams(idxPhase).cfg.timedFaultCurrent = uint32(2);
end

end