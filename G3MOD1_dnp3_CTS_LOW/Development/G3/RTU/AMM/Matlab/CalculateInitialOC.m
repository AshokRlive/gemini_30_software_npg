% Calculate Initial Orthogonal Sine and Cosine Components
% 
%

function PhasorStr = CalculateInitialOC( dataIdx, windowLength, RawWaveformDataStr, PhasorStr)
% Calculate Initial Orthogonal Sine and Cosine Components
% param 
% 
% return

% Voltage Components
PhasorStr.VL1_E.CosineCmpnt(1) = OrthogonalComponentCosine(RawWaveformDataStr.VL1_E((dataIdx-windowLength):dataIdx), windowLength);
PhasorStr.VL1_E.SineCmpnt(1) = OrthogonalComponentSine(RawWaveformDataStr.VL1_E((dataIdx-windowLength):dataIdx), windowLength);

PhasorStr.VL2_E.CosineCmpnt(1) = OrthogonalComponentCosine(RawWaveformDataStr.VL2_E((dataIdx-windowLength):dataIdx), windowLength);
PhasorStr.VL2_E.SineCmpnt(1) = OrthogonalComponentSine(RawWaveformDataStr.VL2_E((dataIdx-windowLength):dataIdx), windowLength);

PhasorStr.VL3_E.CosineCmpnt(1) = OrthogonalComponentCosine(RawWaveformDataStr.VL3_E((dataIdx-windowLength):dataIdx), windowLength);
PhasorStr.VL3_E.SineCmpnt(1) = OrthogonalComponentSine(RawWaveformDataStr.VL3_E((dataIdx-windowLength):dataIdx), windowLength);

%TODO Add Neutral voltage displacement


% Current Components
PhasorStr.IL1.CosineCmpnt(1) = OrthogonalComponentCosine(RawWaveformDataStr.IL1((dataIdx-windowLength):dataIdx), windowLength);
PhasorStr.IL1.SineCmpnt(1) = OrthogonalComponentSine(RawWaveformDataStr.IL1((dataIdx-windowLength):dataIdx), windowLength);

PhasorStr.IL2.CosineCmpnt(1) = OrthogonalComponentCosine(RawWaveformDataStr.IL2((dataIdx-windowLength):dataIdx), windowLength);
PhasorStr.IL2.SineCmpnt(1) = OrthogonalComponentSine(RawWaveformDataStr.IL2((dataIdx-windowLength):dataIdx), windowLength);

PhasorStr.IL3.CosineCmpnt(1) = OrthogonalComponentCosine(RawWaveformDataStr.IL3((dataIdx-windowLength):dataIdx), windowLength);
PhasorStr.IL3.SineCmpnt(1) = OrthogonalComponentSine(RawWaveformDataStr.IL3((dataIdx-windowLength):dataIdx), windowLength);

%TODO Add Neutral current displacement

end