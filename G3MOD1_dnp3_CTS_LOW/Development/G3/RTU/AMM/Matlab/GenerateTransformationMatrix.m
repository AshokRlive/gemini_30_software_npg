% Transformation (/Left Psuedoinverse) Matrix generation
% Calculating the coefficients matrix for the fundamental frequency only

function transformationMatrix = GenerateTransformationMatrix(windowLength, PointsPerCycle, nominalFrequency)

columns = 2;                               % fundamental frequency only
rows = windowLength;

matrixA = zeros(rows, columns);
omega = 2 * pi * nominalFrequency;
deltaT = 1/(PointsPerCycle * nominalFrequency);

% Calculating the coefficients matrix for the fundamental frequency only    
for i = 1:columns
    for j = 1:rows
        if  i == 1
            value = cos(omega * j * deltaT);
        
        elseif i == 2
            value = sin(omega * j * deltaT);
            
        end
        matrixA(j,i) = value;        
    end
end

% The product of (pinv(matrixA' * matrixA) * matrixA')
% is known as the Least Square Error transformation matrix
% also known as a left psuedoinverse matrix
transformationMatrix = (pinv(matrixA' * matrixA) * matrixA');
end