% Initialize AMM configuration parameters
%
%

function AMMConfigStr = TestBench_InitAMM2(AMMConfigStr)
%#codegen
% 
% 
% 
%
coder.inline('always');
% TODO Code Generation

AMMConfigStr.HardwareModel = uint32(0);
AMMConfigStr.NominalFrequency = single (0);
AMMConfigStr.NominalSamplingRate = single (0);
AMMConfigStr.SamplingTime = single (0);
AMMConfigStr.PointsPerCycle = single (0);
AMMConfigStr.MinReliableCurrentAmplitudeLevel = single (0);
AMMConfigStr.MinReliableVoltageAmplitudeLevel = single (0);

% AMMConfigStr
% if coder.target('MATLAB')                  
% AMMConfigStr = struct(  'HardwareModel', uint32(zeros(1,1)),...
%                         'NominalFrequency', single(zeros(1,1)),...
%                         'nominalSamplingRate', single(zeros(1,1)),...
%                         'SamplingTime', single(zeros(1,1)),...
%                         'PointsPerCycle', single(zeros(1,1)),...
%                         'minReliableCurrentAmplitudeLevel', single(zeros(1,1)),...
%                         'minReliableVoltageAmplitudeLevel', single(zeros(1,1)));                   
% else
% AMMConfigStr = struct(  'HardwareModel', coder.opaque('lu_uint32_t', '0', 'HeaderFile', '"lu_types.h"'),...
%                         'NominalFrequency', coder.opaque('lu_float32_t', '0', 'HeaderFile', '"lu_types.h"'),...
%                         'NominalSamplingRate', coder.opaque('lu_float32_t', '0', 'HeaderFile', '"lu_types.h"'),...
%                         'SamplingTime', coder.opaque('lu_float32_t', '0', 'HeaderFile', '"lu_types.h"'),...
%                         'PointsPerCycle', coder.opaque('lu_uint32_t', '0', 'HeaderFile', '"lu_types.h"'),...
%                         'MinReliableCurrentAmplitudeLevel',coder.opaque('lu_float32_t', '0', 'HeaderFile', '"lu_types.h"'),...
%                         'MinReliableVoltageAmplitudeLevel', coder.opaque('lu_float32_t', '0', 'HeaderFile', '"lu_types.h"'));
% end


% TODO Code Generation
% coder.cstructname(AMMConfigStr, 'AMMConfigStr');

%AMMConfigStr = InitAMM2(AMMConfigStr);
            
end