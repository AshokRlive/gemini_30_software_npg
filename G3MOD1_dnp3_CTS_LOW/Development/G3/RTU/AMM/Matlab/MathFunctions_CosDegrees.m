function argumentInDegrees = MathFunctions_CosDegrees(argumentInDegrees)
%codegen
% 
%
%

coder.inline('always');
if coder.target('MATLAB')
    % Executing in MATLAB, call function below
    argumentInDegrees = cosd(argumentInDegrees);
    
else
    % For code generation use cos function, which has been code generated
    % from MathFuctions reference model
    coder.ceval('MathFunctions_CosDegrees', coder.ref(argumentInDegrees));
end