% Orthogonal Component based Real Power 
% Function located here are for the evalution of Real Power
% algorithms per "8.1.3.2 Measurement of Power" from the book (Eq. 8.64)
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function realPower = OCRealPower3( us, ic, icMinusOne, fc, fs)

realPower = 0;

realPower = ((us * icMinusOne) - (us * ic)) / (2 * fc * fs * sin(((delayK * 2 * pi) / windowLength)));
end