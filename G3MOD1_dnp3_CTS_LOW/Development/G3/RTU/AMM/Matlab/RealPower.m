%

classdef RealPower < uint32
   enumeration
      Pa       (1)           % Real Power Phase 1
      Pb       (2)           % Real Power Phase 2
      Pc       (3)           % Real Power Phase 3
      LAST     (4)           % 
   end
end