%

classdef FaultedPhase < uint32
   enumeration
      La       (1)           % Fault on phase A
      Lb       (2)           % Fault on phase B
      Lc       (3)           % 
      Ne       (4)           % Fault on neutral
      Lab      (5)           % Fault on phase A and B
      Lbc      (6)           %
      Lca      (7)           %
      Labc     (8)           % Three phase fault
      LAST     (9)           % 
   end
end