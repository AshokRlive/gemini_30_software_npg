clear all;

cref = load('SimulinkBaseConfig.mat');
SimulinkBaseConfig = cref.SimulinkBaseConfig;
clear cref;
cref = load('DFPISimulinkConfig.mat');
DFPISimulinkConfig = cref.DFPISimulinkConfig;
clear cref;
cref = load('MathFunctionsConfig.mat');
MathFunctionsConfig = cref.MathFunctionsConfig;
clear cref;

% Used for the replacement of Matlab data types with lu_types
% using Simulink AliasType
load('lu_uint8_t.mat');
%load('lu_uint64_t.mat');
load('lu_uint32_t.mat');
load('lu_uint16_t.mat');
load('lu_int8_t.mat');
%load('lu_int64_t.mat');
load('lu_int32_t.mat');
load('lu_int16_t.mat');
load('lu_float64_t.mat');
load('lu_float32_t.mat');
%load('lu_char_t.mat');
load('lu_bool_t.mat');

% Generate Simulink Bus Objects
% InitAMMGxSwitch related structures
[OC, CurrentParam, VoltageParam, Power, HardwareModel, AMMConfig, Phasor] = InitAMMGxSwitch();

Simulink.Bus.createObject(OC);
OCStr = slBus1;
OCStr.DataScope = 'Exported';
OCStr.HeaderFile = 'DFPI_types.h';
clear slBus1;
OC = Simulink.Parameter(OC);
OC.DataType = 'Bus: OCStr';

% EstimatedParamStr
Simulink.Bus.createObject(CurrentParam);
CurrentParamStr = slBus2;
% When casting Matlab nested structures into Simulink ones, even if a 
% particular nested structure has previously been cast into a Bus Object,
% Matlab/Simulink has no knowledge of this. I manually overwrite the
% Bus Object (/struct) DataType with the one created previously.
% TODO Consider improving.
CurrentParamStr.Elements(3).DataType = 'Bus: OCStr';
CurrentParamStr.DataScope = 'Exported';
CurrentParamStr.HeaderFile = 'DFPI_types.h';
clear slBus2;
clear slBus1_OC;
CurrentParam = Simulink.Parameter(CurrentParam);
CurrentParam.DataType = 'Bus: CurrentParamStr';

% V_EstimatedParamStr
Simulink.Bus.createObject(VoltageParam);
VoltageParamStr = slBus2;
VoltageParamStr.Elements(3).DataType = 'Bus: OCStr';
VoltageParamStr.DataScope = 'Exported';
VoltageParamStr.HeaderFile = 'DFPI_types.h';
clear slBus2;
clear slBus1_OC;
VoltageParam = Simulink.Parameter(VoltageParam);
VoltageParam.DataType = 'Bus: VoltageParamStr';

% PowerStr
Simulink.Bus.createObject(Power);
PowerStr = slBus1;
PowerStr.DataScope = 'Exported';
PowerStr.HeaderFile = 'DFPI_types.h';
clear slBus1;
Power = Simulink.Parameter(Power);
Power.DataType = 'Bus: PowerStr';

% HardwareModel
Simulink.Bus.createObject(HardwareModel);
HardwareModelStr = slBus1;
HardwareModelStr.DataScope = 'Exported';
HardwareModelStr.HeaderFile = 'DFPI_types.h';
clear slBus1;
HardwareModel = Simulink.Parameter(HardwareModel);
HardwareModel.DataType = 'Bus: HardwareModelStr';

% AMMConfigStr
Simulink.Bus.createObject(AMMConfig);
AMMConfigStr = slBus2;
AMMConfigStr.Elements(1).DataType = 'Bus: HardwareModelStr';
AMMConfigStr.DataScope = 'Exported';
AMMConfigStr.HeaderFile = 'DFPI_types.h';
clear slBus1_HardwareModel;
clear slBus2;
AMMConfig = Simulink.Parameter(AMMConfig);
AMMConfig.DataType = 'Bus: AMMConfigStr';

Simulink.Bus.createObject(Phasor);
PhasorStr = slBus3;
% V struct data type
PhasorStr.Elements(1).DataType = 'Bus: VoltageParamStr';
% I struct data type
PhasorStr.Elements(2).DataType = 'Bus: CurrentParamStr';
% Power struct data type
PhasorStr.Elements(3).DataType = 'Bus: PowerStr';
PhasorStr.DataScope = 'Exported';
PhasorStr.HeaderFile = 'DFPI_types.h';
clear slBus3;
clear slBus1_OC;
clear slBus2_OC;
clear V;
clear I;
clear Pwr;
Phasor1 = Phasor;
Phasor = Simulink.Parameter(Phasor);
Phasor.DataType = 'Bus: PhasorStr';
Phasor.CoderInfo.StorageClass = 'Custom';
Phasor.CoderInfo.CustomStorageClass = 'Volatile';
% Phasor.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% Phasor.CoderInfo.CustomAttributes.Owner = 'DFPI_types';
% Phasor.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

Phasor1 = Simulink.Signal;
Phasor1.DataType = 'Bus: PhasorStr';
% Phasor1.CoderInfo.StorageClass = 'Custom';
% Phasor1.CoderInfo.CustomStorageClass = 'ExportToFile';
% Phasor1.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% Phasor1.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% temp = Simulink.Signal;
% Phasor1 = sfclipboard;
% Phasor1.copy(temp);
% Phasor1.pasteTo(Phasor2);
%Phasor2 = Phasor1;
% Phasor2 = Simulink.Signal;
% Phasor2.DataType = 'Bus: PhasorStr';
% Phasor2.CoderInfo.StorageClass = 'Custom';
% Phasor2.CoderInfo.CustomStorageClass = 'Reusable';

% InitAMMStructures related structures
[CurrentDirection, TypeOfFault,...
 VoltageMagnitude, CurrentMagnitude,...
 VoltagePhaseAngle, CurrentPhaseAngle ] = InitAMMStructures();

Simulink.Bus.createObject(TypeOfFault);
TypeOfFaultStr = slBus1;
TypeOfFaultStr.DataScope = 'Exported';
TypeOfFaultStr.HeaderFile = 'DFPI_types.h';
clear slBus1;
%clear TypeOfFault;
% TypeOfFault = Simulink.Parameter(TypeOfFault);
% TypeOfFault.DataType = 'Bus: TypeOfFaultStr';
% TypeOfFault.CoderInfo.StorageClass = 'Custom';
% TypeOfFault.CoderInfo.CustomStorageClass = 'ExportToFile';
% TypeOfFault.CoderInfo.CustomAttributes.HeaderFile = 'DFPI_types.h';
% TypeOfFault.CoderInfo.CustomAttributes.Owner = 'DFPI_types';
% TypeOfFault.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';
% TypeOfFault.Headerfile = 'Directionality_grt.h';

%mTypeOfFault = TypeOfFault;
% TypeOfFault = mpt.Parameter(TypeOfFault);
% TypeOfFault.DataType = 'Bus: TypeOfFaultStr';
% TypeOfFault.CoderInfo.StorageClass = 'Custom';
% TypeOfFault.CoderInfo.CustomStorageClass = 'Define';
% TypeOfFault.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% TypeOfFault.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% TypeOfFault.CoderInfo.CustomAttributes.Owner = 'DFPI';
% TypeOfFault.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% Create bus elements (/Matlab arrays)
% CurrentDirectionStr
clear elementTemp;
elementTemp = Simulink.BusElement;
elementTemp.Name = 'CurrentDirection';
elementTemp.Dimensions = 1;
elementTemp.DimensionsMode = 'Fixed';
elementTemp.DataType = 'int32';
elementTemp.SampleTime = -1;
elementTemp.Complexity = 'real';
CurrentDirectionType = Simulink.Bus;
CurrentDirectionType.DataScope = 'Exported';
%CurrentDirectionType.HeaderFile = 'DirectionalFPI2_types.h';
CurrentDirectionType.Elements = elementTemp;
% elementTemp.Name = 'CurrentDirection2';
% CurrentDirectionType.Elements(2) = elementTemp;
% elementTemp.Name = 'CurrentDirection3';
% CurrentDirectionType.Elements(3) = elementTemp;
% elementTemp.Name = 'CurrentDirection4';
% CurrentDirectionType.Elements(4) = elementTemp;
%CurrentDirectionType.Elements.DataType = 'Bus: CurrentDirectionType';

% CurrentDirection = mpt.Parameter(CurrentDirection);
% CurrentDirection.DataType = 'Bus: CurrentDirectionType';
% CurrentDirection.CoderInfo.StorageClass = 'Custom';
% CurrentDirection.CoderInfo.CustomStorageClass = 'Define';
% CurrentDirection.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% CurrentDirection.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% CurrentDirection.CoderInfo.CustomAttributes.Owner = 'DFPI';
% CurrentDirection.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% VoltageMagnitude
clear elementTemp;
elementTemp = Simulink.BusElement;
elementTemp.Name = 'VoltageMagnitude';
elementTemp.Dimensions = 1;
elementTemp.DimensionsMode = 'Fixed';
elementTemp.DataType = 'single';
elementTemp.SampleTime = -1;
elementTemp.Complexity = 'real';
VoltageMagnitudeType = Simulink.Bus;
VoltageMagnitudeType.DataScope = 'Exported';
%VoltageMagnitudeType.HeaderFile = 'DirectionalFPI2_types.h';
VoltageMagnitudeType.Elements = elementTemp;

% VoltageMagnitude = mpt.Parameter(VoltageMagnitude);
% VoltageMagnitude.DataType = 'Bus: VoltageMagnitudeType';
% VoltageMagnitude.CoderInfo.StorageClass = 'Custom';
% VoltageMagnitude.CoderInfo.CustomStorageClass = 'Define';
% VoltageMagnitude.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% VoltageMagnitude.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% VoltageMagnitude.CoderInfo.CustomAttributes.Owner = 'DFPI';
% VoltageMagnitude.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% CurrentMagnitude
clear elementTemp;
elementTemp = Simulink.BusElement;
elementTemp.Name = 'CurrentMagnitude';
elementTemp.Dimensions = 1;
elementTemp.DimensionsMode = 'Fixed';
elementTemp.DataType = 'single';
elementTemp.SampleTime = -1;
elementTemp.Complexity = 'real';
CurrentMagnitudeType = Simulink.Bus;
CurrentMagnitudeType.DataScope = 'Exported';
%CurrentMagnitudeType.HeaderFile = 'DirectionalFPI2_types.h';
CurrentMagnitudeType.Elements = elementTemp;

% CurrentMagnitude = mpt.Parameter(CurrentMagnitude);
% CurrentMagnitude.DataType = 'Bus: CurrentMagnitudeType';
% CurrentMagnitude.CoderInfo.StorageClass = 'Custom';
% CurrentMagnitude.CoderInfo.CustomStorageClass = 'Define';
% CurrentMagnitude.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% CurrentMagnitude.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% CurrentMagnitude.CoderInfo.CustomAttributes.Owner = 'DFPI';
% CurrentMagnitude.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% VoltagePhaseAngle
clear elementTemp;
elementTemp = Simulink.BusElement;
elementTemp.Name = 'VoltagePhaseAngle';
elementTemp.Dimensions = 1;
elementTemp.DimensionsMode = 'Fixed';
elementTemp.DataType = 'single';
elementTemp.SampleTime = -1;
elementTemp.Complexity = 'real';
VoltagePhaseAngleType = Simulink.Bus;
VoltagePhaseAngleType.DataScope = 'Exported';
%VoltagePhaseAngleType.HeaderFile = 'DirectionalFPI2_types.h';
VoltagePhaseAngleType.Elements = elementTemp;

% VoltagePhaseAngle = mpt.Parameter(VoltagePhaseAngle);
% VoltagePhaseAngle.DataType = 'Bus: VoltagePhaseAngleType';
% VoltagePhaseAngle.CoderInfo.StorageClass = 'Custom';
% VoltagePhaseAngle.CoderInfo.CustomStorageClass = 'Define';
% VoltagePhaseAngle.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% VoltagePhaseAngle.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% VoltagePhaseAngle.CoderInfo.CustomAttributes.Owner = 'DFPI';
% VoltagePhaseAngle.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% CurrentPhaseAngle
clear elementTemp;
elementTemp = Simulink.BusElement;
elementTemp.Name = 'CurrentPhaseAngle';
elementTemp.Dimensions = 1;
elementTemp.DimensionsMode = 'Fixed';
elementTemp.DataType = 'single';
elementTemp.SampleTime = -1;
elementTemp.Complexity = 'real';
CurrentPhaseAngleType = Simulink.Bus;
CurrentPhaseAngleType.DataScope = 'Exported';
%CurrentPhaseAngleType.HeaderFile = 'DirectionalFPI2_types.h';
CurrentPhaseAngleType.Elements = elementTemp;
clear elementTemp;

% CurrentPhaseAngle = mpt.Parameter(CurrentPhaseAngle);
% CurrentPhaseAngle.DataType = 'Bus: CurrentPhaseAngleType';
% CurrentPhaseAngle.CoderInfo.StorageClass = 'Custom';
% CurrentPhaseAngle.CoderInfo.CustomStorageClass = 'Define';
% CurrentPhaseAngle.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% CurrentPhaseAngle.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% CurrentPhaseAngle.CoderInfo.CustomAttributes.Owner = 'DFPI';
% CurrentPhaseAngle.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% FPI related structures
[FpiCfgAndParams, FpiFaultParams, FpiConfig] = InitFPI();

% FpiFaultParamsStr
Simulink.Bus.createObject(FpiFaultParams);
FpiFaultParamsStr = slBus1;
FpiFaultParamsStr.DataScope = 'Exported';
FpiFaultParamsStr.HeaderFile = 'DFPI_types.h';
clear slBus1;
FpiFaultParams = Simulink.Parameter(FpiFaultParams);
FpiFaultParams.DataType = 'Bus: FpiFaultParamsStr';

% FpiConfigStr
Simulink.Bus.createObject(FpiConfig);
FpiConfigStr = slBus1;
FpiConfigStr.DataScope = 'Exported';
FpiConfigStr.HeaderFile = 'DFPI_types.h';
clear slBus1;
FpiConfig = Simulink.Parameter(FpiConfig);
FpiConfig.DataType = 'Bus: FpiConfigStr';

% FpiCfgAndParamsStr
Simulink.Bus.createObject(FpiCfgAndParams);
FpiCfgAndParamsStr = slBus1;
% FpiConfigStr struct data type
FpiCfgAndParamsStr.Elements(1).DataType = 'Bus: FpiConfigStr';
% FpiFaultParamsStr struct data type
FpiCfgAndParamsStr.Elements(2).DataType = 'Bus: FpiFaultParamsStr';
FpiCfgAndParamsStr.DataScope = 'Exported';
FpiCfgAndParamsStr.HeaderFile = 'DFPI_types.h';
% FpiCfgAndParamsStr.Dimensions = [1 4];
clear slBus1;
clear cfg;
clear faultParams;
FpiCfgAndParams = Simulink.Parameter(FpiCfgAndParams);
FpiCfgAndParams.DataType = 'Bus: FpiCfgAndParamsStr';

% Directionality related strctures
DirectionalityConfig = InitDirectionality();
% DirectionalityConfigStr
Simulink.Bus.createObject(DirectionalityConfig);
DirectionalityConfigStr = slBus1;
DirectionalityConfigStr.DataScope = 'Exported';
DirectionalityConfigStr.HeaderFile = 'DFPI_types.h';
clear slBus1;

% waveformArrayType = single(0);
% waveformArrayType = cast(waveformArrayType, type(CurrentDirection));
% coder.cstructname(mDirectionalityConfig, 'DirectionalityConfigStr', 'extern', 'HeaderFile', 'DirectionalFPI_types.h');

%clear DirectionalityConfig;
% DirectionalityConfig = Simulink.Parameter(DirectionalityConfig);
% DirectionalityConfig.DataType = 'Bus: DirectionalityConfigStr';
% DirectionalityConfig.CoderInfo.StorageClass = 'Custom';
% DirectionalityConfig.CoderInfo.CustomStorageClass = 'ExportToFile';
% DirectionalityConfig.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% DirectionalityConfig.CoderInfo.CustomAttributes.Owner = 'DFPI';
% DirectionalityConfig.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% mDirectionalityConfig = DirectionalityConfig;
% DirectionalityConfig = mpt.Parameter(DirectionalityConfig);
% DirectionalityConfig.DataType = 'Bus: DirectionalityConfigStr';
% DirectionalityConfig.CoderInfo.StorageClass = 'Custom';
% DirectionalityConfig.CoderInfo.CustomStorageClass = 'Define';
% DirectionalityConfig.CoderInfo.CustomAttributes.MemorySection = 'MemVolatile';
% DirectionalityConfig.CoderInfo.CustomAttributes.HeaderFile = 'DFPI.h';
% DirectionalityConfig.CoderInfo.CustomAttributes.Owner = 'DFPI';
% DirectionalityConfig.CoderInfo.CustomAttributes.DefinitionFile = 'DFPI.c';

% cref = load('DFPISimulinkConfig.mat');
% DFPISimulinkConfig = cref.DFPISimulinkConfig;
% clear cref;
% open_system('DirectionalFPI2');
% rtwbuild('DirectionalFPI2');

% cref = load('SimulinkBaseConfig.mat');
% SimulinkBaseConfig = cref.SimulinkBaseConfig;
% clear cref;
%simulink_test_model = 'TestBench_InitAMM3';

% Open model first
open_system('MathFunctions');
open_system('DFPI');
open_system('MainAMM');

% portHandles = get_param('AMM/Chart','portHandles');
% set_param(portHandles.Outport(1),'MustResolveToSignalObject','off');
% 
% portHandles = get_param('AMM/Chart/OCSine','portHandles');
% set_param(portHandles.Outport(1),'MustResolveToSignalObject','off');
% 
% portHandles = get_param('AMM/Chart/OCCosine','portHandles');
% set_param(portHandles.Outport(1),'MustResolveToSignalObject','off');
% 
% portHandles = get_param('AMM/Chart/OCMagnitude','portHandles');
% set_param(portHandles.Outport(1),'MustResolveToSignalObject','off');
% 
% portHandles = get_param('AMM/Chart/OCPhaseAngle','portHandles');
% set_param(portHandles.Outport(1),'MustResolveToSignalObject','off');

disp('Generating C code. This will take one or two minutes. Please wait.');

% Generate code for Simulink model
rtwbuild('MathFunctions');
rtwbuild('DFPI');
rtwbuild('MainAMM');