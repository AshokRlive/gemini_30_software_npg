function remainder = MathFunctions_Modulo(dividend, divisor)
%#codegen
% 
%
%
remainder = single(0);

coder.inline('always');
if coder.target('MATLAB')
    % Executing in MATLAB, call function below
    remainder = mod(dividend, divisor);
else
    % For code generation use modulo function, which has been code generated
    % from MathFuctions reference model
%     T = coder.newtype(struct('rt_unused',char(0)));
%     
%     T = coder.cstructname(T, 'tag_RTM_MathFunctions_T', 'extern',...
%                         'HeaderFile', 'MathFunctions.h');
                    
    remainder = coder.ceval('MathFunctions_Modulo', dividend, divisor);
end