function CurrentDir = DetermineDirection(  angleDifference,...
                                           CurrentDir,...
                                           DirectionalityConfig)
%#codegen                                       
% Determine direction for both Phase and Earth Faults based on the angle 
% difference between faulted current(s) angle(s) and respective polarization 
% element. The angle difference is compared against the user defined Forward,
% Reverse and Non-Operating areas.
                                  
                                       
% The forward direction refers to the power-flow towards the load 
% (downstream switchgear (load)).
% The reverse direction refers to the power-flow towards the source 
% (upstream switchgear (source)).

CurrentDir(Direction.UnknownDir) = int32(0);
CurrentDir(Direction.ReverseDir) = int32(0);
CurrentDir(Direction.ForwardDir) = int32(0);
CurrentDir(Direction.BothDir) = int32(0);

% TODO When doing comparisons add a tolerance limit?
% Check for angles that are greater than Min Forward Angle threshold
if angleDifference > DirectionalityConfig.MinFwdAngle
    % Check if its in the Reverse area
    if  (angleDifference > DirectionalityConfig.MaxRvrsAngle) &&...
                (angleDifference < DirectionalityConfig.MinRvrsAngle)
        CurrentDir(Direction.ReverseDir)= int32(1);

    % Check if its in the Non-operating area           
    elseif (angleDifference < DirectionalityConfig.MaxRvrsAngle)
        CurrentDir(Direction.UnknownDir) = int32(1);
        
    % Check if its in the Non-operating area           
    elseif (angleDifference > DirectionalityConfig.MinRvrsAngle) &&...
                (angleDifference < DirectionalityConfig.MaxFwdAngle)
        CurrentDir(Direction.UnknownDir) = int32(1);
        
    % Check if its in the Forward area  
    elseif (angleDifference > DirectionalityConfig.MaxRvrsAngle)
        CurrentDir(Direction.ForwardDir) = int32(1);

    % TODO Decide what should happen in this case    
    else

    end
      
elseif (angleDifference < DirectionalityConfig.MinFwdAngle)
    CurrentDir(Direction.ForwardDir) = int32(1);

% Check angle is negative but within Minimum Forward Angle threshold     
% elseif (angleDifference > DirectionalityConfigStr.MinFwdAngle) &&...
%         (angleDifference < 0)
%     CurrentDirectionStr.Forward = 1;
% 
% % Check for negative angles that are smaller than Minimum Forward Angle threshold   
% elseif angleDifference < DirectionalityConfigStr.MinFwdAngle
%     % Check if its in the Reverse area
%     if  angleDifference < (DirectionalityConfigStr.MinFwdAngle -...
%                                     DirectionalityConfigStr.MinNonOpAngle)
%         CurrentDirectionStr.Reverse = 1;
% 
%     % Check if its in the Non-operating area           
%     elseif (angleDifference > (DirectionalityConfigStr.MinFwdAngle -...
%                                 DirectionalityConfigStr.MinNonOpAngle))
%         CurrentDirectionStr.UknownDir = 1;
% 
%     % TODO Decide what should happen in this case    
%     else
% 
%     end

end

% Are the sectors overlapping?
if(CurrentDir(Direction.ForwardDir) == 1) && (CurrentDir(Direction.ReverseDir) == 1)
    CurrentDir(Direction.BothDir) = int32(1); 
    % TODO Decide what happens when sector are overlapping, 
    % as we should never get here!
    
end

end