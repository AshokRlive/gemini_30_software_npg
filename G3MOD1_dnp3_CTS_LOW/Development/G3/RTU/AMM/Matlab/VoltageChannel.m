%

classdef VoltageChannel < uint32
   enumeration
      SetA       (1)           % Voltage Channel  1
      SetB       (2)           % Voltage Channel  2
      LAST       (3)
   end
end