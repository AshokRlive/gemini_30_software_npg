function AMMConfigStr = ConfigAMMGxSwitch(AMMConfigStr)
%#codegen
% Config AMM parameters
%
%

% AMMConfigStr
% Number of Voltage and Current
AMMConfigStr.HardwareModel = single(1); % GX switch - Six Voltage and Four Currents                   
                                    
% Nominal frequency (mains frequency 50 or 60Hz)
AMMConfigStr.NominalFrequency = single(50);

% Selected ADCs' Sampling Rate (Hz)
AMMConfigStr.NominalSamplingRate = single(1000);

% Selected ADCs' Sampling Time 
AMMConfigStr.SamplingTime = single(1 / AMMConfigStr.NominalSamplingRate);

% Number of samples per cycle
AMMConfigStr.PointsPerCycle = double(AMMConfigStr.NominalSamplingRate / AMMConfigStr.NominalFrequency);                                    

% Minimum operating current to allow directional criteria
AMMConfigStr.MinReliableCurrentAmplitudeLevel = single(0.1);

%Minimum operating voltage to allow directional criteria
AMMConfigStr.MinReliableVoltageAmplitudeLevel = single(0.1);

end