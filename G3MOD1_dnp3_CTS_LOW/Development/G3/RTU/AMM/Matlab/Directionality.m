% Directionality
%
%
%
%

function DirectionalityParamsStr = Directionality(  DirectionalityParamsStr,...
                                                    DirectionalityConfigStr,...
                                                    PhasorStr,...
                                                    AMMConfigStr)


% TODO This section needs more work
% Checking of which phases have faulted
% Adding phase-to-phase angle value to a ring buffer
% Checking when to use phase angle from memory
% Remove thershold comparisons
                                                
% Is directionality enabled
if (DirectionalityConfigStr.Enabled == 1)
    
% Are all the voltages and currents above minimum current or voltage thresholds
% so we can reliably estimate phase angle?
    if ((PhasorStr.MagIL1 >= AMMConfigStr.MinCurrentThrshld) &&...
            (PhasorStr.MagIL2 >= AMMConfigStr.MinCurrentThrshld) &&...
            (PhasorStr.MagIL3 >= AMMConfigStr.MinCurrentThrshld) &&...
            (PhasorStr.MagVL1_E >= AMMConfigStr.MinVoltageThrshld) &&...
            (PhasorStr.MagVL2_E >= AMMConfigStr.MinVoltageThrshld) &&...
            (PhasorStr.MagVL3_E >= AMMConfigStr.MinVoltageThrshld))
            
    % Rotate ALL voltage angles by the Angle of Connection
    DirectionalityParamsStr = RotateByAngleOfConnection(    DirectionalityParamsStr,...
                                                            DirectionalityConfigStr,...
                                                            PhasorStr,...
                                                            AMMConfigStr);

     DirectionalityParamsStr.FaultedPhases.PhiIL1 = 1;                                                   
                                                        
    % Determine appropriate reference (cross-polarizing) voltage quantity
    % Rotate angle of reference voltage to +45� (typically)
    % Determine direction (Forward, Reverse, or Non-Operating Zone)
    DirectionalityParamsStr = CrossPolarizationDirectionality( DirectionalityParamsStr,...
                                                               DirectionalityConfigStr,...
                                                               AMMConfigStr);




    end
end
end