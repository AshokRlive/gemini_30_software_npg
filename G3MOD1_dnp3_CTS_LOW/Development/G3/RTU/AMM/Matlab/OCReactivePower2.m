% Orthogonal Component based Reactive Power 
% Function located here are for the evalution of Real Power
% algorithms per "8.1.3.2 Measurement of Power" from the book (Eq. 8.65)
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function reactivePower = OCReactivePower2( uc, us, ic, is, fc, fs)
reactivePower = 0;

reactivePower = ((us * ic) - (uc * is)) / (2 * fc * fs);
end