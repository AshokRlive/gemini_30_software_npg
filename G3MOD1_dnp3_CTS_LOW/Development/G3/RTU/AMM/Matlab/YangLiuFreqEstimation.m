% Orthogonal Component Frequency Measurement
% A Precise Calculation of Power System Frequency and Phasor
% Jun-Zhe Yang and Chih-Wen Liu
% IEEE TRANSACTIONS ON POWER DELIVERY, VOL. 15, NO. 2, APRIL 2000

function estimatedFrequency = YangLiuFreqEstimation(yc, samplingRate, pointsPerCycle)
% Orthogonal Component Frequency Measurement
% param 
% 
% return
estimatedFrequency = 0;
tmpEstimatedFrequency = 0;
frequencyBin = 0;
alpha = 0

for index = 2:1:((pointsPerCycle)-5)
    alpha = (yc(index)+yc(index+2) + sqrt(((yc(index)+yc(index+2))^2)-4*(yc(index+1)^2)))/(2*yc(index+1));
    
    tmpEstimatedFrequency(index) = (samplingRate * acos(alpha)) / (2*pi);


    frequencyBin(index) = tmpEstimatedFrequency(index);
    tmpEstimatedFrequency(index) = tmpEstimatedFrequency(index)+tmpEstimatedFrequency(index-1);
end

estimatedFrequency = tmpEstimatedFrequency(index) / (((pointsPerCycle)-6));
end