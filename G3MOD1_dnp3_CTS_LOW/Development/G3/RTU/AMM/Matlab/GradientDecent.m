% Gradient Decent
% Find the data point at which the Least Square Error is minimal.
% ie Find the fault inception point

function faultDataPointIdx = GradientDecent( waveformDataPoints,...
                                            PointsPerCycle,...
                                            projectionMatrix,...
                                            transformationMatrix,...
                                            tolerance)

convergance = 0;
errors = 0;
gradientMagnitude = 0;
derivative = 0;
predictions = 0;
buildArray =0;

% Repeat until convergance becomes True
while convergance == 0
    
    errors = FaultInceptionPoint( waveformDataPoints, PointsPerCycle, projectionMatrix);
%    predictions = dot(waveformDataPoints,transformationMatrix);
    
    gradientSumSquares = 0;
    % while we haven't reached the tolerance yet, update each feature's weight
    for faultDataPointIdx = 1:PointsPerCycle
      % Compute the derivative for weight[i]  
      derivative(faultDataPointIdx,1) = 2 * dot(waveformDataPoints(faultDataPointIdx), errors);
      % Add the squared value of the derivative to the gradient sum of squares (for assessing convergence)
      gradientSumSquares = dot(derivative,derivative) + gradientSumSquares;
      buildArray(faultDataPointIdx,1) = gradientSumSquares;
      % Subtract the step size times the derivative from the current weight
      
    end
    
    % Compute the square-root of the gradient sum of squares to get the gradient matnigude:
    gradientMagnitude = sqrt(gradientSumSquares);
    
    if gradientMagnitude < tolerance
        convergance = 1;
    end
end

end