%

classdef CurrentChannel < uint32
   enumeration
      SetA       (1)           % Current Channel  1
      SetB       (2)           % Current Channel  2
      SetC       (3)           % Current Channel  3
      LAST       (4)
   end
end