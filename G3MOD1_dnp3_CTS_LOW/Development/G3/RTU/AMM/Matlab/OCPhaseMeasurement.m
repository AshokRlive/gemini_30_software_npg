% Orthogonal component Phase Measurement 
% 
% 

function phaseAngle = OCPhaseMeasurement(phaseAngle, ys, yc)
% Orthogonal component Phase Measurement
% param 
% 
% return

phaseAngle = 0;
phaseAngle = atan2d(ys, yc);
% if ((yc>=0) && (ys>=0))
%     phaseAngle = atan(abs(ys / yc)) - pi;
% elseif ((yc>=0) && (ys<0))
%     phaseAngle = -atan(abs(ys / yc)) + pi;
% elseif ((yc<0) && (ys<0))
%     phaseAngle = atan(abs(ys / yc));
% elseif ((yc<0) && (ys>=0))
%     phaseAngle = -atan(abs(ys / yc));
% end
end