%

classdef LineToLine < uint32
   enumeration
      Lab   (1)           % Line to Line Phase 1
      Lbc   (2)           % Line to Line Phase 2
      Lca   (3)           % Line to Line Phase 3
      LAST  (4)
   end
end