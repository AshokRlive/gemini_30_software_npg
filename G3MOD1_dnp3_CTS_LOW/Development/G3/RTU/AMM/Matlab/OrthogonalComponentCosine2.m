% Orthogonal Component Cosine
% Algorithm per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function yc = OrthogonalComponentCosine2(yc, waveformDataPoints, windowLength, pointsPerCycle)
% Orthogonal component Cosine
% param 
% 
% return
coder.inline('always');
yc = single(0);

for index = 1:windowLength
   yc = yc + (waveformDataPoints(windowLength+1-index) * cos((1/(pointsPerCycle/2)) * pi * (((windowLength-1)/2) - index)));
end

yc = yc / (windowLength/sqrt(2));
end