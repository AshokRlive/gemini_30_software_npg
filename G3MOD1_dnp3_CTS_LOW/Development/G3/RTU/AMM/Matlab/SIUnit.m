%

classdef SIUnit < uint32
   enumeration
      V    (1)           % Voltage
      I    (2)           % Current
   end
end