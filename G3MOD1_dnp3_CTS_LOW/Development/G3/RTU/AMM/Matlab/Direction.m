%

classdef Direction < int32
   enumeration
      UnknownDir(1),
      ForwardDir(2),
      ReverseDir(3),
      BothDir(4)
   end
   % Include Class Name Prefix in Generated Enumerated Type Value Names
   methods(Static)
      function y = addClassNameToEnumNames()
        y = true;
      end
    end
end