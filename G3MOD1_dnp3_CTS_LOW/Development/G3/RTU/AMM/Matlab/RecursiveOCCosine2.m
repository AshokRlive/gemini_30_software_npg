% Recursive Orthogonal Component Cosine 
% 
% 
% 

function yc = RecursiveOCCosine2(waveformDataPoints, ycOld, index, windowLength, numOfHarmonics)
% Recursive Orthogonal Component Cosine
% param 
% 
% return


yc = ycOld + (waveformDataPoints(windowLength + index) - waveformDataPoints(index)) *...
                    (cos(2 * pi * index / windowLength))/numOfHarmonics; 
%yc = yc/windowLength;                       
end