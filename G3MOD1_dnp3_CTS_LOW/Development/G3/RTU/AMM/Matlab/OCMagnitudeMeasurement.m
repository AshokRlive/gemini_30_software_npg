% Orthogonal Component based Magnitude Measurement 
% Function located here is for the evalution of Magnitude Measurement
% algorithms per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function magnitude = OCMagnitudeMeasurement(magnitude, ys, yc)
% Orthogonal Component based Magnitude Measurement
% param 
% 
% return

magnitude = 0;

% Applying Pythagore's Theorem
% Peak value of waveform
magnitude = sqrt((yc^2) + (ys^2));
% For RMS value
magnitude = magnitude/1.4142;
end