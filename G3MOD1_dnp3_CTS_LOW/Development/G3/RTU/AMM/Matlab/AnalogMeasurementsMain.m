%--------------------------------------------------------------------------
% Analog Measurements Module
% FIR Orthogoal Component based measurement algorithms
% 
% Raw 3-phase waveform data import via COMTRADE formated files
% Import 3 Voltage and 3 Current waveforms
%
% Calculations of V, I, P, Q, S, cos(phi), phase angle, frequency and True RMS parameters
% 
% Use of above parameters for:
% Overcurrent (/FPI)
% Directionality
% Inrush Restraint
% Energy related (kWhr, KVAhr, etc...)
% Sag, Swell, Interruption
% THD (Notch filter based)
% Harmonics (Short-Time Fourier Transform based)
% High Impedance Fault Detection AND Location
% Distrubance Recording (COMTRADE based)
%

clear workspace
clear variables
clear functions
clf
close all              

% Initialize Analog Measurements Module structure variables
[AMMConfigStr, PhasorStr, FaultedPhasesStr] = InitAMM();
% Initialize Fault Passage Indication structure variables
FpiCfgAndParamsStr = InitFPI();
% Initialize Directionality structure variables
DirectionalityConfigStr = InitDirectionality();

% Initialization of tmpFaultedPhasesStr
tmpFaultedPhasesStr = FaultedPhasesStr;
                                   
nominalFrequency = AMMConfigStr.NominalFrequency; 
nominalSamplingRate = AMMConfigStr.nominalSamplingRate; 
samplingTime = AMMConfigStr.SamplingTime;
pointsPerCycle = AMMConfigStr.PointsPerCycle;                 

% If set to 1 the windowLength moves one full cycle at time
% Set to 2 and it moves half a cycle at time
windowLengthDivider = 1;
% Adjust the window length
windowLength = pointsPerCycle / windowLengthDivider;
% If set to 1 the windowLength moves one full cycle (/one windowLength) at time (ie no overlap)
% Set to 2 and it moves half a cycle (/half a windowLength) at time (ie Half a window length of overlap)
slidingWindowSize = 1;


% Initialize local variables
mainsCycleNumber = 0;
delayK = 1;
frequencyMsrmnt = zeros(1,1);
idxPhase = 0;
a=0;
b=0;
c=0;
neutralCurrentFound = 0;
faultedPointIndex = 0;
currentTimeMs = 0;
averagedFrequency = 0;
voltageTransientDetected = 0;
currentTransientDetected = 0;

single singleDataVariable;

%
d = designfilt('lowpassiir', ...                % Response type
       'PassbandFrequency',50, ...              % Frequency constraints
       'StopbandFrequency',100, ...
       'PassbandRipple',0.1, ...                % Magnitude constraints
       'StopbandAttenuation',106, ...
       'DesignMethod','ellip', ...             % Design method
       'MatchExactly','passband', ...           % Design method options
       'SampleRate',nominalSamplingRate);       % Sample rate

%fvtool(d);

% Read analogue waveform data
% Variable naming must be:
% Phase-to-earth Voltage 1 should be named "VL1_E", Voltage 2 "VL2_E", etc
% Phase Current 1 should be named "IL1", Current 2 "IL2", etc
read_comtrade;

% List variable names in the workspace
listVariableNames = who;

% Raw Comtrade Waveform data time lenght in seconds
% TODO Read Comtrade config file parameters like nominal frequency, sampling rate
% number of samples and use accordingly
rawComTradeDataTimeLength = length(VL1_E)/Sample_rate;
% 0.16; 1.5;  0.52                 

% Check if loaded COMTRADE signal variables contain an integer number of mains cycles
% If not then remove the data points of the last non-integer mains cycle 
 remainder = rem(length(VL1_E),(Sample_rate/Frequency));
 tmpVariable = (abs(remainder-0) < 1e8*eps(min(abs(remainder),abs(0))));
 if remainder ~= 0
%     abs(A-B) < 1e4*eps(min(abs(A),abs(B)))
     VL1_E = VL1_E(1:length(VL1_E)-remainder);
     VL2_E = VL2_E(1:length(VL2_E)-remainder);
     VL3_E = VL3_E(1:length(VL3_E)-remainder);
     IL1 = IL1(1:length(IL1)-remainder);
     IL2 = IL2(1:length(IL2)-remainder);
     IL3 = IL3(1:length(IL3)-remainder);
     % If neutral current variable exists concatenate it as well
      for indexWorkspaceVariables = 1:1:numel(listVariableNames)
            if strcmp(listVariableNames(indexWorkspaceVariables), 'In')
               In = In(1:length(In)-remainder); 
            end
      end
     
     % Re-estimate the time length variable
     rawComTradeDataTimeLength = length(VL1_E)/Sample_rate;
 end

% If the declared Comtrade Sample_rate is not the same as the initAMM 
% sampleRate we need to resample the signals
% if Sample_rate ~= nominalSamplingRate
% Resample as Comtrade waveform data might be sampled at different sampling rates
resampledDataLength = round(pointsPerCycle*Frequency*rawComTradeDataTimeLength);
integerTotalMainsCycleNumber = round(Frequency*rawComTradeDataTimeLength, 1);
% Voltage Signals
PhasorStr.V.SetA.RawData(1:resampledDataLength,WithReferenceTo.La) = resample(VL1_E,...
                                                        resampledDataLength,...
                                                        length(VL1_E));

PhasorStr.V.SetA.RawData(1:resampledDataLength,WithReferenceTo.Lb)= resample(VL2_E,...
                                                        resampledDataLength,...
                                                        length(VL2_E));

PhasorStr.V.SetA.RawData(1:resampledDataLength,WithReferenceTo.Lc) = resample(VL3_E,...
                                                        resampledDataLength,...
                                                        length(VL3_E));
% Neutral Voltage Displacement
for dataIdx = 1:1:resampledDataLength
    PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Ne) =...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.La)+...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lb)+...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lc);
end
% else
%     
% end

% Phase-to-Phase Voltage Generation 
for dataIdx = 1:1:resampledDataLength
    % Phase-to-Phase Voltage Vab
    PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lab) =...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.La)-...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lb);
    % Phase-to-Phase Voltage Vbc
    PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lbc) =...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lb)-...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lc);
    % Phase-to-Phase Voltage Vca
    PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lca) =...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.Lc)-...
                        PhasorStr.V.SetA.RawData(dataIdx,WithReferenceTo.La);                    
end
% Current Signals
PhasorStr.I.SetA.RawData(1:resampledDataLength,WithReferenceTo.La) = resample(IL1,...
                                                        resampledDataLength,...
                                                        length(IL1));
                                                    
PhasorStr.I.SetA.RawData(1:resampledDataLength,WithReferenceTo.Lb) = resample(IL2,...
                                                        resampledDataLength,...
                                                        length(IL2));
                                                    
PhasorStr.I.SetA.RawData(1:resampledDataLength,WithReferenceTo.Lc) = resample(IL3,...
                                                        resampledDataLength,...
                                                        length(IL3));

 % Search for the Neutral Current variable
 % The read_comtrade function above should load all the analog measurement
 % signals into the workspace
 for indexWorkspaceVariables = 1:1:numel(listVariableNames)
     if strcmp(listVariableNames(indexWorkspaceVariables), 'In')
        neutralCurrentFound = 1; 
        PhasorStr.I.SetA.RawData(1:resampledDataLength,WithReferenceTo.Ne)= resample(In,...
                                                        resampledDataLength,...
                                                        length(In));
     end
 end
 
% If the Neutral Current variable can not be found, generate it by vectorial sum
if neutralCurrentFound == 0
    for dataIdx = 1:1:resampledDataLength
        PhasorStr.I.SetA.RawData(dataIdx,WithReferenceTo.Ne) =...
                    PhasorStr.I.SetA.RawData(dataIdx,WithReferenceTo.La)+...
                    PhasorStr.I.SetA.RawData(dataIdx,WithReferenceTo.Lb)+...
                    PhasorStr.I.SetA.RawData(dataIdx,WithReferenceTo.Lc);
    end    
end


PhasorStr.V.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.La) = filter(d, PhasorStr.V.SetA.RawData(1:end,WithReferenceTo.La));
PhasorStr.V.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.Lb) = filter(d, PhasorStr.V.SetA.RawData(1:end,WithReferenceTo.Lb));
PhasorStr.V.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.Lc) = filter(d, PhasorStr.V.SetA.RawData(1:end,WithReferenceTo.Lc));
PhasorStr.V.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.Ne) = filter(d, PhasorStr.V.SetA.RawData(1:end,WithReferenceTo.Ne));

PhasorStr.I.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.La) = filter(d, PhasorStr.I.SetA.RawData(1:end,WithReferenceTo.La));
PhasorStr.I.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.Lb) = filter(d, PhasorStr.I.SetA.RawData(1:end,WithReferenceTo.Lb));
PhasorStr.I.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.Lc) = filter(d, PhasorStr.I.SetA.RawData(1:end,WithReferenceTo.Lc));
PhasorStr.I.SetA.FilteredData(1:resampledDataLength,WithReferenceTo.Ne) = filter(d, PhasorStr.I.SetA.RawData(1:end,WithReferenceTo.Ne));

% Mock a fault on a particular phase      
%FaultedPhasesStr = MockAFault(FaultedPhasesStr, integerTotalMainsCycleNumber);

% Generate projection matrix for Fault Inception Point function
projectionMatrix = GenerateProjectionMatrix(windowLength, pointsPerCycle, nominalFrequency);

% Generate transfomration matrix
transformationMatrix = GenerateTransformationMatrix(windowLength, pointsPerCycle, nominalFrequency);

cosineLUT = GenerateCosineLUT( windowLength, pointsPerCycle);

sineLUT = GenerateSineLUT( windowLength, pointsPerCycle);

% Tolerance value for gradient decent
tolerance = 25000000;

% Inialize where the dataIdx should start
% A full cycle of data is intially required for the FIR to operate
if windowLengthDivider == 1
    firstDataIdxElement = 1;
elseif windowLengthDivider > 1
    firstDataIdxElement = pointsPerCycle*1;
end

% Orthogonal Component Sine and Cosine Calculation using the
% 'standard' (and more CPU intesive) algorithm. This should run
% only once and provide the initial OC values for the recursive
% OC calculation algorithm.
for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
    for windowLenghtIdx = 1:1:windowLength
    % Voltage Components
    PhasorStr.V.SetA.OC.Cosine(windowLenghtIdx, idxPhase) = OrthogonalComponentCosine(...
                                                    PhasorStr.V.SetA.RawData(1:(windowLength), idxPhase),...
                                                    windowLength,...
                                                    windowLenghtIdx,...
                                                    idxPhase,...
                                                    uint32(SIUnit.V));
    PhasorStr.V.SetA.OC.Sine(windowLenghtIdx, idxPhase) = OrthogonalComponentSine(...
                                                    PhasorStr.V.SetA.RawData(1:(windowLength), idxPhase),...
                                                    windowLength,...
                                                    windowLenghtIdx,...
                                                    idxPhase,...
                                                    uint32(SIUnit.V));

    % Current Components
    PhasorStr.I.SetA.OC.Cosine(windowLenghtIdx, idxPhase) = OrthogonalComponentCosine(...
                                                    PhasorStr.I.SetA.RawData(1:(windowLength), idxPhase),...
                                                    windowLength,...
                                                    windowLenghtIdx,...
                                                    idxPhase,...
                                                    uint32(SIUnit.I));
    PhasorStr.I.SetA.OC.Sine(windowLenghtIdx, idxPhase) = OrthogonalComponentSine(...
                                                    PhasorStr.I.SetA.RawData(1:(windowLength), idxPhase),...
                                                    windowLength,...
                                                    windowLenghtIdx,...
                                                    idxPhase,...
                                                    uint32(SIUnit.I));                 
    end
end

% One of the weakness of the recursive DFT is that it requires a full cycle
% of waveform data BEFORE it can start computing its output
% Fill up the rest of cycle with the last value from the Standard DFT orthogonal component
for indexPhase = 1:1:(uint32(WithReferenceTo.Ne))
    for windowLenghtIdx = 1:1:windowLength
       PhasorStr.V.SetA.OC.rCosine(windowLenghtIdx, indexPhase) = PhasorStr.V.SetA.OC.Cosine(windowLength, indexPhase);
       PhasorStr.V.SetA.OC.rSine(windowLenghtIdx, indexPhase) = PhasorStr.V.SetA.OC.Sine(windowLength, indexPhase);
       PhasorStr.I.SetA.OC.rCosine(windowLenghtIdx, indexPhase) = PhasorStr.I.SetA.OC.Cosine(windowLength, indexPhase);
       PhasorStr.I.SetA.OC.rSine(windowLenghtIdx, indexPhase) = PhasorStr.I.SetA.OC.Sine(windowLength, indexPhase);
    end
end

% Intiliaze Magnitude values to Overvoltage and Overcurrent threshold
% For ACUMSUM purposes
for indexPhase = 1:1:(uint32(WithReferenceTo.Ne))
    for windowLenghtIdx = 1:1:windowLength+1
        PhasorStr.V.SetA.Mag(windowLenghtIdx, indexPhase) = 10.0;
        PhasorStr.I.SetA.Mag(windowLenghtIdx, indexPhase) = 2.0;
    end
end
% Initialize main while loop waveform data index
dataIdx = firstDataIdxElement;

% Main loop
while (dataIdx <= (rawComTradeDataTimeLength*nominalSamplingRate)+pointsPerCycle)
%for dataIdx = firstDataIdxElement:(pointsPerCycle/slidingWindowSize):(rawComTradeDataTimeLength*nominalSamplingRate)+pointsPerCycle

    % Stop when index is greater than the number of points in the
    % comtrade file
%    if (dataIdx) <= ((pointsPerCycle*nominalFrequency*rawComTradeDataTimeLength)+pointsPerCycle)

        % Increment the window cycle number
        mainsCycleNumber = mainsCycleNumber + 1;
        currentTimeMs = currentTimeMs + 20;
        
        if mainsCycleNumber == 1
        % Do nothing         
     
        else
            % This IF statememnt is required so the very last mains cycle
            % worth of data is operated on
            if (mainsCycleNumber < integerTotalMainsCycleNumber+1) &&...
                   (dataIdx) <= ((pointsPerCycle*Frequency*rawComTradeDataTimeLength)-pointsPerCycle)
                
                % Fault Inception Point (/Transient Event Monitor)
                % based on a least error squares approach.
                % The objective is to detect the exact data ponit at which  
                % the fault occured and then re-adjust the FIR filter data window to only
                % contain post fault data.
                % TODO Needs more work
                fFaultMonitor =@LeastSquareError;
                
                % Allow for filter start-up and stop one cycle before end
                % of data
                if (mainsCycleNumber > 7) && (dataIdx+pointsPerCycle < resampledDataLength-pointsPerCycle*2)
                    % Get the Transient Monitor value for 3xV, 3xI Signals  
                    for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                        for windowLenghtIdx = 1:1:windowLength
                        % Voltage Signals
                        PhasorStr.V.SetA.TransientMonitor(((mainsCycleNumber-2)*pointsPerCycle+1):((mainsCycleNumber-1)*pointsPerCycle)-2, idxPhase) =...
                            GilcrestFaultInceptionMonitor( PhasorStr.V.SetA.FilteredData(dataIdx+pointsPerCycle*2:dataIdx+pointsPerCycle*2+windowLength-1,idxPhase),...
                                                            pointsPerCycle);
                         
                         % Current Signals
                         PhasorStr.I.SetA.TransientMonitor(((mainsCycleNumber-2)*pointsPerCycle+1):((mainsCycleNumber-1)*pointsPerCycle)-2, idxPhase) =...
                             GilcrestFaultInceptionMonitor( PhasorStr.I.SetA.FilteredData(dataIdx+pointsPerCycle*2:dataIdx+pointsPerCycle*2+windowLength-1,idxPhase),...
                                                            pointsPerCycle);
                        end
                    end
                

                    % Find the first phase that has exceeded the Transient Monitor threshold.
                    % Then find the fault inception point.
                    % Exit while loop without scanning all phases once a fault
                    % inception point has been found.
                    indexPhase = (uint32(WithReferenceTo.La));
                    while (indexPhase <= (uint32(WithReferenceTo.Ne))) && (faultedPointIndex == 0)
                        windowLenghtIdx = 1;
                        % 
                        while (windowLenghtIdx <= windowLength-2) && (faultedPointIndex == 0)
                            
                            if indexPhase == (uint32(WithReferenceTo.Ne))
                                % Neutral Voltage only
                                if (PhasorStr.V.SetA.TransientMonitor((mainsCycleNumber-2)*pointsPerCycle+windowLenghtIdx, indexPhase) >= 1.0)
                                    voltageTransientDetected = voltageTransientDetected + 1;
                                    voltageTransientCounter = windowLenghtIdx;
                                % Neutral Current only   
                                elseif (PhasorStr.I.SetA.TransientMonitor((mainsCycleNumber-2)*pointsPerCycle+windowLenghtIdx, indexPhase) >= 1.0)
                                    currentTransientDetected = currentTransientDetected + 1;
                                    currentTransientCounter = windowLenghtIdx;
                                end
                                
                            % Voltage Signals La, Lb, Lc
                            elseif (PhasorStr.V.SetA.TransientMonitor((mainsCycleNumber-2)*pointsPerCycle+windowLenghtIdx, indexPhase) >= 8.6)
                                voltageTransientDetected = voltageTransientDetected + 1;
                                voltageTransientCounter = windowLenghtIdx;

                            % Current Signals La, Lb, Lc  
                            elseif (PhasorStr.I.SetA.TransientMonitor((mainsCycleNumber-2)*pointsPerCycle+windowLenghtIdx, indexPhase) >= 1.2)
                                currentTransientDetected = currentTransientDetected + 1;
                                currentTransientCounter = windowLenghtIdx;
                            end
                            %  Reset if voltage threshold crossing was not consecutive
                            % ie If a transient event has been detected previously
                            % and the voltageTransientCounter is not
                            % incremented it can only mean the trasneint event
                            % happened sometime before the last point
                            if (voltageTransientDetected >= 1)  &&  (voltageTransientCounter ~= windowLenghtIdx) 
                                voltageTransientDetected = 0;
                                voltageTransientCounter = 0;
                            end

                            %  Reset if current threshold crossing was not consecutive
                            if  (currentTransientDetected >= 1) &&  (currentTransientCounter ~= windowLenghtIdx) 
                                currentTransientDetected = 0;
                                currentTransientCounter = 0;
                            end

                            % Look for three (3) consecutive threshold crossings
                            if (voltageTransientDetected >= 2) || (currentTransientDetected >= 2)
                                faultedPointIndex = windowLenghtIdx;
                            end

                            % Increment windowLenghtIdx value
                            windowLenghtIdx = windowLenghtIdx +1;          
                        end
                        % Increment phase index
                        indexPhase = indexPhase +1;

                        if faultedPointIndex > 0
                            idxPhase = 0;
                            windowLenghtIdx = 0;
                            % Now that we have found the Fault Inception point we need to re-run the
                            % the 'standard' DFT algorithm in order to get the
                            % initial values for the Recursive DFT algorithm
                            for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                                for windowLenghtIdx = 1:1:windowLength
                                    % Voltage Components
                                    PhasorStr.V.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentCosine(...
                                                            PhasorStr.V.SetA.RawData(dataIdx+faultedPointIndex:dataIdx+windowLength+faultedPointIndex-1, idxPhase),...
                                                            windowLength,...
                                                            windowLenghtIdx,...
                                                            idxPhase,...
                                                            uint32(SIUnit.V));
                                    PhasorStr.V.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentSine(...
                                                            PhasorStr.V.SetA.RawData(dataIdx+faultedPointIndex:dataIdx+windowLength+faultedPointIndex-1, idxPhase),...
                                                            windowLength,...
                                                            windowLenghtIdx,...
                                                            idxPhase,...
                                                            uint32(SIUnit.V));

                                    % Current Components
                                    PhasorStr.I.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentCosine(...
                                                        PhasorStr.I.SetA.RawData(dataIdx+faultedPointIndex:dataIdx+windowLength+faultedPointIndex-1, idxPhase),...
                                                        windowLength,...
                                                        windowLenghtIdx,...
                                                        idxPhase,...
                                                        uint32(SIUnit.I));
                                    PhasorStr.I.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentSine(...
                                                        PhasorStr.I.SetA.RawData(dataIdx+faultedPointIndex:dataIdx+windowLength+faultedPointIndex-1, idxPhase),...
                                                        windowLength,...
                                                        windowLenghtIdx,...
                                                        idxPhase,...
                                                        uint32(SIUnit.I));
                                end
                            end

                            for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                                    PhasorStr.V.SetA.OC.rCosine(windowLength, idxPhase) = PhasorStr.V.SetA.OC.Cosine(windowLength*2, idxPhase);
                                    PhasorStr.V.SetA.OC.rSine(windowLength, idxPhase) = PhasorStr.V.SetA.OC.Sine(windowLength*2, idxPhase);
                                    PhasorStr.I.SetA.OC.rCosine(windowLength, idxPhase) = PhasorStr.I.SetA.OC.Cosine(windowLength*2, idxPhase);
                                    PhasorStr.I.SetA.OC.rSine(windowLength, idxPhase) = PhasorStr.I.SetA.OC.Sine(windowLength*2, idxPhase);
                            end

                            for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                                for windowLenghtIdx = 1:1:windowLength
                                    % Recursive DFT function.
                                    % Voltage Components
                                    PhasorStr.V.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase) = ...
                                                RecursiveOCCosine(PhasorStr.V.SetA.OC.rCosine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                                  PhasorStr.V.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase),...
                                                                  PhasorStr.V.SetA.OC.Cosine(windowLenghtIdx, idxPhase));

                                    PhasorStr.V.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase) = ...
                                                RecursiveOCCosine(PhasorStr.V.SetA.OC.rSine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                                  PhasorStr.V.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase),...
                                                                  PhasorStr.V.SetA.OC.Sine(windowLenghtIdx, idxPhase));
                                    % Current Components
                                    PhasorStr.I.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase) = ...
                                                RecursiveOCCosine(PhasorStr.I.SetA.OC.rCosine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                                  PhasorStr.I.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase),...
                                                                  PhasorStr.I.SetA.OC.Cosine(windowLenghtIdx, idxPhase));

                                    PhasorStr.I.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase) = ...
                                                RecursiveOCCosine(PhasorStr.I.SetA.OC.rSine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                                  PhasorStr.I.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase),...
                                                                  PhasorStr.I.SetA.OC.Sine(windowLenghtIdx, idxPhase));
                                end
                            end
                           % Set the new raw waveform data index value
                            dataIdx = dataIdx+windowLength+faultedPointIndex;
                            % Reset transient counters
                            voltageTransientDetected = 0;
                            voltageTransientCounter = 0;
                            currentTransientDetected = 0;
                            currentTransientCounter = 0;
                        end
                    end  
                end
                
                 if mainsCycleNumber>2
%                      for index = 1:1:(pointsPerCycle-3)
%                         estimatedFrequency(index) = RecursiveOCFrequency(PhasorStr.V.SetA.Mag(((mainsCycleNumber-3)*pointsPerCycle)+index+3,uint32(WithReferenceTo.Lb)),...
%                                                               PhasorStr.V.SetA.Mag(((mainsCycleNumber-3)*pointsPerCycle)+index+2,uint32(WithReferenceTo.Lb)),...
%                                                               PhasorStr.V.SetA.Mag(((mainsCycleNumber-3)*pointsPerCycle)+index+1,uint32(WithReferenceTo.Lb)),...
%                                                               PhasorStr.V.SetA.RawData(((mainsCycleNumber-3)*pointsPerCycle)+index+3,uint32(WithReferenceTo.Lb)),...
%                                                               PhasorStr.V.SetA.RawData(((mainsCycleNumber-3)*pointsPerCycle)+index+2,uint32(WithReferenceTo.Lb)),...
%                                                               PhasorStr.V.SetA.RawData(((mainsCycleNumber-3)*pointsPerCycle)+index+1,uint32(WithReferenceTo.Lb)),...
%                                                               samplingTime);
%                          averagedFrequency = averagedFrequency + estimatedFrequency(index);
%                      end
%                      averagedFrequency = averagedFrequency/(pointsPerCycle-3);
%                      averagedFrequency = 0;
% %                     estimatedFrequency = FrequencyMeasurementProny(PhasorStr.V.SetA.OC.Sine(1:pointsPerCycle,uint32(WithReferenceTo.Lb)),...
% %                                                                     pointsPerCycle,...
% %                                                                     nominalFrequency);
                    estimatedFrequency = RecursiveOCFrequency3(PhasorStr.V.SetA.OC.Sine(pointsPerCycle,uint32(WithReferenceTo.Lb)),...
                                        PhasorStr.V.SetA.OC.Cosine(pointsPerCycle,uint32(WithReferenceTo.Lb)),...
                                        PhasorStr.V.SetA.OC.Sine(pointsPerCycle-1,uint32(WithReferenceTo.Lb)),...
                                        PhasorStr.V.SetA.OC.Cosine(pointsPerCycle-1,uint32(WithReferenceTo.Lb)),...
                                        1, pointsPerCycle, nominalFrequency, samplingTime);
                 end
                
%                 for index = 1:1:(pointsPerCycle-3)
%                     estimatedFrequency(index) = OCFrequencyMeasurement(PhasorStr.V.SetA.OC.Cosine(index+3,uint32(WithReferenceTo.Lb)),...
%                                                                     PhasorStr.V.SetA.OC.Sine(index+3,uint32(WithReferenceTo.Lb)),...
%                                                                     PhasorStr.V.SetA.OC.Cosine(index+2,uint32(WithReferenceTo.Lb)),...
%                                                                     PhasorStr.V.SetA.OC.Sine(index+2,uint32(WithReferenceTo.Lb)),...
%                                                                     PhasorStr.V.SetA.OC.Cosine(index,uint32(WithReferenceTo.Lb)),...
%                                                                     PhasorStr.V.SetA.OC.Sine(index,uint32(WithReferenceTo.Lb)),...
%                                                                     delayK, pointsPerCycle, nominalFrequency);
% 
%                     averagedFrequency = averagedFrequency + estimatedFrequency(index);
%                 end
%                 averagedFrequency = averagedFrequency/(pointsPerCycle-3);
%                 averagedFrequency = 0;
                
                % Calculate Sine and Cosine Orthogonal Components using
                % Recursive (point by point) method
                % for the Voltage and Current signals 
                for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                    for windowLenghtIdx = 1:1:windowLength
                        % Standard DFT function. Values from this function
                        % will feed into the Recursive DFT function.
                        % Voltage Components
                        PhasorStr.V.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentCosine(....
                                                                        PhasorStr.V.SetA.RawData((dataIdx-windowLength):dataIdx, idxPhase),...
                                                                        windowLength,...
                                                                        windowLenghtIdx,...
                                                                        idxPhase,...
                                                                        uint32(SIUnit.V));
                        PhasorStr.V.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentSine(...
                                                                        PhasorStr.V.SetA.RawData((dataIdx-windowLength):dataIdx, idxPhase),...
                                                                        windowLength,...
                                                                        windowLenghtIdx,...
                                                                        idxPhase,...
                                                                        uint32(SIUnit.V));
                        % Current Components
                        PhasorStr.I.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentCosine(...
                                                                        PhasorStr.I.SetA.RawData((dataIdx-windowLength):dataIdx, idxPhase),...
                                                                        windowLength,...
                                                                        windowLenghtIdx,...
                                                                        idxPhase,...
                                                                        uint32(SIUnit.I));
                        PhasorStr.I.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase) = OrthogonalComponentSine(...
                                                                        PhasorStr.I.SetA.RawData((dataIdx-windowLength):dataIdx, idxPhase),...
                                                                        windowLength,...
                                                                        windowLenghtIdx,...
                                                                        idxPhase,...
                                                                        uint32(SIUnit.I));
                   end
                end
              
                for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                    for windowLenghtIdx = 1:1:windowLength
                        % Load the Standard DFT value (the last one) into
                        % the Recursive DFT array
                        PhasorStr.V.SetA.OC.Cosine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase);
                        PhasorStr.V.SetA.OC.Sine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase);
                        PhasorStr.I.SetA.OC.Cosine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase);
                        PhasorStr.I.SetA.OC.Sine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase);

%                         PhasorStr.V.SetA.OC.rCosine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase);
%                         PhasorStr.V.SetA.OC.rSine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase);
%                         PhasorStr.I.SetA.OC.rCosine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase);
%                         PhasorStr.I.SetA.OC.rSine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase);
                    end
                end
                for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                    PhasorStr.V.SetA.OC.rCosine(windowLength, idxPhase) = PhasorStr.V.SetA.OC.Cosine(windowLength*2, idxPhase);
                    PhasorStr.V.SetA.OC.rSine(windowLength, idxPhase) = PhasorStr.V.SetA.OC.Sine(windowLength*2, idxPhase);
                    PhasorStr.I.SetA.OC.rCosine(windowLength, idxPhase) = PhasorStr.I.SetA.OC.Cosine(windowLength*2, idxPhase);
                    PhasorStr.I.SetA.OC.rSine(windowLength, idxPhase) = PhasorStr.I.SetA.OC.Sine(windowLength*2, idxPhase);
                end

                
                for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
                    for windowLenghtIdx = 1:1:windowLength
                        % Recursive DFT function.
                        % Voltage Components
                        PhasorStr.V.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase) = ...
                                    RecursiveOCCosine(PhasorStr.V.SetA.OC.rCosine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                      PhasorStr.V.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase),...
                                                      PhasorStr.V.SetA.OC.Cosine(windowLenghtIdx, idxPhase));

                        PhasorStr.V.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase) = ...
                                    RecursiveOCCosine(PhasorStr.V.SetA.OC.rSine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                      PhasorStr.V.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase),...
                                                      PhasorStr.V.SetA.OC.Sine(windowLenghtIdx, idxPhase));
                        % Current Components
                        PhasorStr.I.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase) = ...
                                    RecursiveOCCosine(PhasorStr.I.SetA.OC.rCosine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                      PhasorStr.I.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase),...
                                                      PhasorStr.I.SetA.OC.Cosine(windowLenghtIdx, idxPhase));

                        PhasorStr.I.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase) = ...
                                    RecursiveOCCosine(PhasorStr.I.SetA.OC.rSine(windowLength+windowLenghtIdx-1, idxPhase),...
                                                      PhasorStr.I.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase),...
                                                      PhasorStr.I.SetA.OC.Sine(windowLenghtIdx, idxPhase));
                    end
                end

            end % mainsCycleNumber < integerTotalMainsCycleNumber+1                                                        

        % Magnitude calculation
        % TODO Encapsulate, test magnitude, phase, frequency response, etc...
        % Voltage Magnitude
        for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
            for windowLenghtIdx = 1:1:windowLength
                
                PhasorStr.V.SetA.Mag(((mainsCycleNumber-2)*pointsPerCycle)+windowLenghtIdx, idxPhase) = OCMagnitudeMeasurement(...
                                    PhasorStr.V.SetA.OC.rSine(windowLenghtIdx,idxPhase),...
                                    PhasorStr.V.SetA.OC.rCosine(windowLenghtIdx,idxPhase));     
            end
        end
        
        % Current Magnitude 
        % TODO Encapsulate, test magnitude, phase, frequency response, etc...
        % Magnitude calculation
        for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
            for windowLenghtIdx = 1:1:windowLength
                                       
                PhasorStr.I.SetA.Mag(((mainsCycleNumber-2)*pointsPerCycle)+windowLenghtIdx, idxPhase) = OCMagnitudeMeasurement(...
                                    PhasorStr.I.SetA.OC.rSine(windowLenghtIdx,idxPhase),...
                                    PhasorStr.I.SetA.OC.rCosine(windowLenghtIdx,idxPhase));  
            end
        end
        % Frequency Calculation
        % Only one voltage phase is used for the frequency calculations
        % TODO Frequency calculation should only happen if the Voltage
        % magnitude is above a certain threhold. Otherwise use one of the other
        % voltage phases
        % FIXME Does NOT work!
%           for index = 1:1:(pointsPerCycle-1)
%         estimatedFrequency(index) = RecursiveOCFrequency(  PhasorStr.V.SetA.Mag(((mainsCycleNumber-2)*pointsPerCycle)+index+3, uint32(WithReferenceTo.La)),...
%                                                     PhasorStr.V.SetA.Mag(((mainsCycleNumber-2)*pointsPerCycle)+index+2, uint32(WithReferenceTo.La)),...
%                                                     PhasorStr.V.SetA.Mag(((mainsCycleNumber-2)*pointsPerCycle)+index+1, uint32(WithReferenceTo.La)),...
%                                                     PhasorStr.V.SetA.RawData(((mainsCycleNumber-2)*pointsPerCycle)+index+3, uint32(WithReferenceTo.La)),...
%                                                     PhasorStr.V.SetA.RawData(((mainsCycleNumber-2)*pointsPerCycle)+index+2, uint32(WithReferenceTo.La)),...
%                                                     PhasorStr.V.SetA.RawData(((mainsCycleNumber-2)*pointsPerCycle)+index+1, uint32(WithReferenceTo.La)),...
%                                                     samplingTime);

%             estimatedFrequency = RecursiveOCFrequency3(PhasorStr.V.SetA.OC.Sine(pointsPerCycle*2,uint32(WithReferenceTo.La)),...
%                                                                 PhasorStr.V.SetA.OC.Cosine(pointsPerCycle*2,uint32(WithReferenceTo.La)),...
%                                                                 PhasorStr.V.SetA.OC.Sine(pointsPerCycle,uint32(WithReferenceTo.La)),...
%                                                                 PhasorStr.V.SetA.OC.Cosine(pointsPerCycle,uint32(WithReferenceTo.La)),...
%                                                                 1, pointsPerCycle, nominalFrequency, samplingTime);
        
%               averagedFrequency = averagedFrequency + estimatedFrequency(index);
%           end
%           averagedFrequency = averagedFrequency/(pointsPerCycle-1);
%           averagedFrequency = 0;
%         frequencyMsrmnt(mainsCycleNumber) = OCFrequencyMeasurement(PhasorStr(Channel.ChA).OC.Cosine.VLE(3,LineToEarth.L1),...
%                                     PhasorStr(Channel.ChA).OC.Sine.VLE(3,LineToEarth.L1),...
%                                     PhasorStr(Channel.ChA).OC.Cosine.VLE(2,LineToEarth.L1),...
%                                     PhasorStr(Channel.ChA).OC.Sine.VLE(2,LineToEarth.L1),...
%                                     PhasorStr(Channel.ChA).OC.Cosine.VLE(1,LineToEarth.L1),...
%                                     PhasorStr(Channel.ChA).OC.Sine.VLE(1,LineToEarth.L1),...
%                                     delayK, pointsPerCycle, nominalFrequency);

        % Phase angle estimation for Voltage
         for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
             PhasorStr.V.SetA.Phi(mainsCycleNumber-1,idxPhase) = OCPhaseMeasurement(...
                  PhasorStr.V.SetA.OC.rCosine(16,idxPhase),...
                  PhasorStr.V.SetA.OC.rSine(16,idxPhase));

%             PhasorStr.V.SetA.Phi(mainsCycleNumber-1,idxPhase) = rad2deg(PhasorStr.V.SetA.Phi(mainsCycleNumber-1,idxPhase));
          end

        % Current Magnitude
         for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
              PhasorStr.I.SetA.Phi(mainsCycleNumber-1,idxPhase) = OCPhaseMeasurement(...
                  PhasorStr.I.SetA.OC.rCosine(16,idxPhase),...
                  PhasorStr.I.SetA.OC.rSine(16,idxPhase));

%              PhasorStr.I.SetA.Phi(mainsCycleNumber-1,idxPhase) = rad2deg(PhasorStr.I.SetA.Phi(mainsCycleNumber-1,idxPhase));
         end
         
        % Frequency Calculation
%         if mainsCycleNumber >= 3
%         deltaFrequency = RecursiveOCFrequency(PhasorStr.V.SetA.Phi(mainsCycleNumber-1, uint32(WithReferenceTo.La)),...
%                                                     PhasorStr.V.SetA.Phi(mainsCycleNumber-2, uint32(WithReferenceTo.La)),...
%                                                     pointsPerCycle,...
%                                                     pointsPerCycle,...
%                                                     nominalFrequency);
%         end

         % Calculate the Phase-to-Phase Voltage and Phase Angles from
         % the Phase-to-Earth measurements
        [PhasorStr.V.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,1:end),...
         PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,1:end),...
         PhasorStr.V.SetA.Phi((mainsCycleNumber-2)+1,1:end),...
         PhasorStr.I.SetA.Phi((mainsCycleNumber-2)+1,1:end)] = PhaseAngleCalculations( PhasorStr.V.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,1:end),...
                                             PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,1:end),...
                                             PhasorStr.V.SetA.Phi((mainsCycleNumber-2)+1,1:end),...
                                             PhasorStr.I.SetA.Phi((mainsCycleNumber-2)+1,1:end),...
                                             AMMConfigStr.minReliableVoltageAmplitudeLevel,...
                                             AMMConfigStr.minReliableCurrentAmplitudeLevel);
        
        % FPI                              
        for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))

            FpiCfgAndParamsStr(idxPhase) = FaultPassageIndication(FpiCfgAndParamsStr(idxPhase),...
                                                        PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,idxPhase),...
                                                        currentTimeMs);
        end
        
        % Clear all previous values in tmpFaultedPhasesStr
        for idxDirection = 1:1:(uint32(Direction.Both))
            tmpFaultedPhasesStr.La(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Lb(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Lc(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Ne(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Lab(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Lbc(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Lca(1,idxDirection) = 0;
            tmpFaultedPhasesStr.Labc(1,idxDirection) = 0;
        end
        
        % Fault Type Classification
        [FpiCfgAndParamsStr, tmpFaultedPhasesStr] = FaultTypeClassification(FpiCfgAndParamsStr,...
                                                                            tmpFaultedPhasesStr,...
                                                                            PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,WithReferenceTo.La),...
                                                                            PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,WithReferenceTo.Lb),...
                                                                            PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,WithReferenceTo.Lc),...
                                                                            PhasorStr.I.SetA.Mag((mainsCycleNumber-2)*pointsPerCycle+1,WithReferenceTo.Ne),...
                                                                            AMMConfigStr.minReliableCurrentAmplitudeLevel);
        % CrossPolarization Directionality                                                                 
        tmpFaultedPhasesStr = CrossPolarizationDirectionality( tmpFaultedPhasesStr,...
                                                         PhasorStr.V.SetA.Phi(mainsCycleNumber-1,:),...
                                                         PhasorStr.I.SetA.Phi(mainsCycleNumber-1,:),...
                                                         DirectionalityConfigStr);
                                                     
        % Ugly Matlab way of accessing structure fields
        % TODO IMPROVE! How?
%         names = fieldnames(FaultedPhasesStr);
%         lengthNames = length(names);
%         for idxNames = 1:1:length(names)
%             tmpFaultedPhasesStr.(names{idxNames})(1, :) =...
%                     FaultedPhasesStr.(names{idxNames})(mainsCycleNumber-1, :);        
%         end  
        
        % TODO Create array of FaultedPhasesStr
        FaultedPhasesStr.La(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.La(1,:);
        FaultedPhasesStr.Lb(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Lb(1,:);
        FaultedPhasesStr.Lc(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Lc(1,:);
        FaultedPhasesStr.Ne(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Ne(1,:);
        FaultedPhasesStr.Lab(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Lab(1,:);
        FaultedPhasesStr.Lbc(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Lbc(1,:);
        FaultedPhasesStr.Lca(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Lca(1,:);
        FaultedPhasesStr.Labc(mainsCycleNumber-1, :) = tmpFaultedPhasesStr.Labc(1,:);


        % Move the orthogonal component data from current window
        % to previous window
        for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
            for windowLenghtIdx = 1:1:windowLength  
                % Standard Orthogonal Components
%                 PhasorStr.V.SetA.OC.Cosine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase);
%                 PhasorStr.V.SetA.OC.Sine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase);
%                 PhasorStr.I.SetA.OC.Cosine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.Cosine(windowLength+windowLenghtIdx, idxPhase);
%                 PhasorStr.I.SetA.OC.Sine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.Sine(windowLength+windowLenghtIdx, idxPhase);
                % Recursive Orthogonal Components
                PhasorStr.V.SetA.OC.rCosine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase);
                PhasorStr.V.SetA.OC.rSine(windowLenghtIdx, idxPhase) = PhasorStr.V.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase);
                PhasorStr.I.SetA.OC.rCosine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.rCosine(windowLength+windowLenghtIdx, idxPhase);
                PhasorStr.I.SetA.OC.rSine(windowLenghtIdx, idxPhase) = PhasorStr.I.SetA.OC.rSine(windowLength+windowLenghtIdx, idxPhase);
            end
%        end
        end
%         for idxPhase = 1:1:(uint32(WithReferenceTo.Ne))
%             PhasorStr.V.SetA.OC.rCosine(windowLength, idxPhase) = PhasorStr.V.SetA.OC.Cosine(windowLength*2, idxPhase);
%             PhasorStr.V.SetA.OC.rSine(windowLength, idxPhase) = PhasorStr.V.SetA.OC.Sine(windowLength*2, idxPhase);
%             PhasorStr.I.SetA.OC.rCosine(windowLength, idxPhase) = PhasorStr.I.SetA.OC.Cosine(windowLength*2, idxPhase);
%             PhasorStr.I.SetA.OC.rSine(windowLength, idxPhase) = PhasorStr.I.SetA.OC.Sine(windowLength*2, idxPhase);
%         end
        end

    % Increment while loop waveform data index
%    if mainsCycleNumber~=1
        dataIdx = dataIdx+(pointsPerCycle/slidingWindowSize);
%    end
end  
