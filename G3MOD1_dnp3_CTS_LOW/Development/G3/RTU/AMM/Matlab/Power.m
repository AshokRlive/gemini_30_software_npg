%

classdef Power < uint32
   enumeration
      P1       (1)           % Real Power Phase 1
      P2       (2)           % Real Power Phase 2
      P3       (3)           % Real Power Phase 3
      Q1       (4)           % Reactive Power Phase 1
      Q2       (5)           % Reactive Power Phase 2
      Q3       (6)           % Reactive Power Phase 3
      S1       (7)           % Apparent Power Phase 1
      S2       (8)           % Apparent Power Phase 2
      S3       (9)           % Apparent Power Phase 3
      PF1      (10)          % Power Factor Phase 1 
      PF2      (11)          % Power Factor Phase 2 
      PF3      (12)          % Power Factor Phase 3 
      LAST     (13)          % 
   end
end