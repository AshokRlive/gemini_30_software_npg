% Recursive Orthogonal Component Frequency Measurement 
% Normalized instantaneous fundamental voltage magnitude of three consecutive samples.
% More info: "A Recursive DFT Based Technique for Accurate Estimation of Grid Voltage Frequency",
% Reza M., Ciobotaru M., Agelidis V., Industrial Electronics Society, 
% IECON 2013 - 39th Annual Conference of the IEEE

function estimatedFrequency = RecursiveOCFrequency(MagCurrent, MagLast, MagBeforeLast, RawValCurrent, RawValLast, RawValBeforeLast, samplingTimePeriod)
% Recursive Orthogonal Component Frequency Measurement
% param 
% 
% return
% MagCurrent = MagCurrent*1.4142;
% MagLast = MagLast*1.4142;
% MagBeforeLast = MagBeforeLast*1.4142;
estimatedFrequency = 0;

estimatedFrequency = asin(sqrt(((RawValLast/MagLast)^2) - ((RawValCurrent/MagCurrent)...
                            *(RawValBeforeLast/MagBeforeLast))) )/(2*pi*samplingTimePeriod);                                      
end