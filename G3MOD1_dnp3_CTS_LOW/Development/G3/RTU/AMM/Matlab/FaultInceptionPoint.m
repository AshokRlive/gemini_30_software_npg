% Fault inception point
% Finds the fault inception point
% 
% 
% 

function faultedPointIndex = FaultInceptionPoint( fFaultMonitor,...
                                                    waveformDataPoints,...
                                                    PointsPerCycle,...
                                                    projectionMatrix)

faultedPointIndex = 0;
halfCycle = PointsPerCycle/2;
twoCycleLength = PointsPerCycle*2;
% As we have passed two windows worth of data we need to concatenate the
% waveform data
tmpWaveformDataPoints = waveformDataPoints(PointsPerCycle*2+faultedPointIndex:PointsPerCycle*3+faultedPointIndex);

% Evaluate Fault Monitor function output against threshold                                               
% If the threshold has been crossed, 
% decrement (/move left) the whole raw waveform data by one point at a time,
% until we are below the minimum theshold                                            
while (fFaultMonitor( tmpWaveformDataPoints, PointsPerCycle, projectionMatrix) > 0.05)...
        && (faultedPointIndex <= PointsPerCycle-1)
    
    faultedPointIndex = faultedPointIndex + 1;
    tmpWaveformDataPoints = waveformDataPoints(PointsPerCycle*2+faultedPointIndex:PointsPerCycle*3+faultedPointIndex);
end
end