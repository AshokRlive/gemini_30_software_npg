% Three phase angle calculations
% A phase sequence defines the sequential timing in which each phase-to-ground phasor (voltage or current)
% lags each other phase-to-ground phasor in the counter-clockwise direction.
%

function [voltageMagnitude,...
          currentMagnitude,...
          voltagePhaseAngle,...
          currentPhaseAngle] = PhaseAngleCalculations( voltageMagnitude,...
                                                        currentMagnitude,...
                                                        voltagePhaseAngle,...
                                                        currentPhaseAngle,...
                                                        minReliableVoltageAmplitudeLevel,...
                                                        minReliableCurrentAmplitudeLevel)
%#codegen
% 
% 
%

coder.inline('always');
%coder.extrinsic('MathFunctions_SinDegrees');

% Variables for mapping of voltage vectors a,b,c on x and y axis
phaseAvoltageMagOnYaxis = single(0);
phaseAvoltageMagOnXaxis = single(0);
phaseBvoltageMagOnYaxis = single(0);
phaseBvoltageMagOnXaxis = single(0);
phaseCvoltageMagOnYaxis = single(0);
phaseCvoltageMagOnXaxis = single(0);

voltagePhaseAngle(WithReferenceTo.LineAB) = single(0);
voltagePhaseAngle(WithReferenceTo.LineBC) = single(0);
voltagePhaseAngle(WithReferenceTo.LineCA) = single(0);

% referenceAngle is used to measure the angle difference between itself and
% ALL other vectors (Voltages and Currents). Typically Phase A voltage is 
% used as the reference angle.
referenceAngle = single(0);

% Under balanced system conditions, the phase angle of the voltage of phase A 
% is used as reference for both voltage and current angles
if voltageMagnitude(WithReferenceTo.LineA) >= minReliableVoltageAmplitudeLevel

    referenceAngle = voltagePhaseAngle(WithReferenceTo.LineA);
    voltagePhaseAngle(WithReferenceTo.LineA) = single(0);
    
    % Find the magnitude of the phase voltage A vector purely applied to the
    % x and y axis
    phaseAvoltageMagOnYaxis = voltageMagnitude(WithReferenceTo.LineA) * MathFunctions_SinDegrees(voltagePhaseAngle(WithReferenceTo.LineA));
    phaseAvoltageMagOnXaxis = voltageMagnitude(WithReferenceTo.LineA) * MathFunctions_CosDegrees(voltagePhaseAngle(WithReferenceTo.LineA)); 

    % Check LineB is above Threshold
    if voltageMagnitude(WithReferenceTo.LineB) >= minReliableVoltageAmplitudeLevel
        % Phase to ground voltages and neutral voltage displacement
        voltagePhaseAngle(WithReferenceTo.LineB) = referenceAngle - voltagePhaseAngle(WithReferenceTo.LineB);
        % Normalize to 360 degrees
        voltagePhaseAngle(WithReferenceTo.LineB) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineB), 360);

        % Phase to Phase voltages
        % In a balanced grid, all phase-to-phase phasor supply voltages 
        % are phase-to-neutral voltages multiplied by sqrt(3) and lead the 
        % phase-to-neutral voltage phasors by 30�.
        % FIXME this assumption can NOT be made!!!
        % Convert Phase-to-neutral phase angles to phase-to-phase angles

        % Find the magnitude of the Voltage vector purely applied to the
        % x and y axis
        phaseBvoltageMagOnYaxis = voltageMagnitude(WithReferenceTo.LineB) * MathFunctions_SinDegrees(voltagePhaseAngle(WithReferenceTo.LineB));
        phaseBvoltageMagOnXaxis = voltageMagnitude(WithReferenceTo.LineB) * MathFunctions_CosDegrees(voltagePhaseAngle(WithReferenceTo.LineB));
        % Apply pythagoras theorem
        voltageMagnitude(WithReferenceTo.LineAB) = sqrt((phaseAvoltageMagOnXaxis - phaseBvoltageMagOnXaxis)^2 +...
                                                        (phaseAvoltageMagOnYaxis - phaseBvoltageMagOnYaxis)^2);    

        voltagePhaseAngle(WithReferenceTo.LineAB) = atan2d(phaseAvoltageMagOnYaxis - phaseBvoltageMagOnYaxis,...
                                                            phaseAvoltageMagOnXaxis - phaseBvoltageMagOnXaxis);
                                                        
        voltagePhaseAngle(WithReferenceTo.LineAB) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineAB), 360);
        
    % If LineB magnitude is too low    
    else
        voltageMagnitude(WithReferenceTo.LineAB) = voltageMagnitude(WithReferenceTo.LineA);
        
        voltagePhaseAngle(WithReferenceTo.LineB) = single(0);
        voltagePhaseAngle(WithReferenceTo.LineAB) = voltagePhaseAngle(WithReferenceTo.LineA);
%        voltagePhaseAngle(WithReferenceTo.LineBC) = voltagePhaseAngle(WithReferenceTo.LineC);
    end    

    % Check LineC is above Threshold
    if voltageMagnitude(WithReferenceTo.LineC) >= minReliableVoltageAmplitudeLevel
        voltagePhaseAngle(WithReferenceTo.LineC) = referenceAngle - voltagePhaseAngle(WithReferenceTo.LineC);
        voltagePhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineC), 360);

        % FIXME Phase to Phase voltages       
        % Find the magnitude of the Voltage vector purely applied to the
        % x and y axis
        phaseCvoltageMagOnYaxis = voltageMagnitude(WithReferenceTo.LineC) * MathFunctions_SinDegrees(voltagePhaseAngle(WithReferenceTo.LineC));
        phaseCvoltageMagOnXaxis = voltageMagnitude(WithReferenceTo.LineC) * MathFunctions_CosDegrees(voltagePhaseAngle(WithReferenceTo.LineC)); 
        % FIXME Phase to Phase voltages
        % Apply pythagoras theorem
        voltageMagnitude(WithReferenceTo.LineBC) = sqrt((phaseBvoltageMagOnXaxis - phaseCvoltageMagOnXaxis)^2 +...
                                                        (phaseBvoltageMagOnYaxis - phaseCvoltageMagOnYaxis)^2);
        
        % FIXME Phase to phase Voltage Vca
        % Apply pythagoras theorem
        voltageMagnitude(WithReferenceTo.LineCA) = sqrt((phaseCvoltageMagOnXaxis - phaseAvoltageMagOnXaxis)^2 +...
                                                        (phaseCvoltageMagOnYaxis - phaseAvoltageMagOnYaxis)^2);
        
       
       voltagePhaseAngle(WithReferenceTo.LineBC) = atan2d(phaseBvoltageMagOnYaxis - phaseCvoltageMagOnYaxis,...
                                                            phaseBvoltageMagOnXaxis - phaseCvoltageMagOnXaxis);
       voltagePhaseAngle(WithReferenceTo.LineCA) = atan2d(phaseCvoltageMagOnYaxis - phaseAvoltageMagOnYaxis,...
                                                            phaseCvoltageMagOnXaxis - phaseAvoltageMagOnXaxis);
                                  
       voltagePhaseAngle(WithReferenceTo.LineBC) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineBC), 360);
       voltagePhaseAngle(WithReferenceTo.LineCA) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineCA), 360);
       
    % If LineC magnitude is also too low
    else
        voltageMagnitude(WithReferenceTo.LineBC) = voltageMagnitude(WithReferenceTo.LineB);
        voltageMagnitude(WithReferenceTo.LineCA) = voltageMagnitude(WithReferenceTo.LineA);
        
        voltagePhaseAngle(WithReferenceTo.LineC) = single(0);
        voltagePhaseAngle(WithReferenceTo.LineBC) = voltagePhaseAngle(WithReferenceTo.LineB);
        voltagePhaseAngle(WithReferenceTo.LineCA) = 180+voltagePhaseAngle(WithReferenceTo.LineA);
        voltagePhaseAngle(WithReferenceTo.LineCA) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineCA), 360);
    end
    
    % LineB and LineC are BOTH below Threshold
    if (voltageMagnitude(WithReferenceTo.LineB) <= minReliableVoltageAmplitudeLevel) ...
            && (voltageMagnitude(WithReferenceTo.LineC) <= minReliableVoltageAmplitudeLevel)
        
        voltageMagnitude(WithReferenceTo.LineBC) = single(0);
        voltagePhaseAngle(WithReferenceTo.LineBC) = single(0);
    end
    
    % TODO Decide and make changes to Neutral Voltage Displacement calculation and buffer memory method
    voltagePhaseAngle(WithReferenceTo.Neutral) = referenceAngle - voltagePhaseAngle(WithReferenceTo.Neutral);
    voltagePhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.Neutral), 360);

    % Phase currents and neutral(/residual) current
    if currentMagnitude(WithReferenceTo.LineA) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineA) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineA);
        currentPhaseAngle(WithReferenceTo.LineA) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineA), 360);
    else
        currentPhaseAngle(WithReferenceTo.LineA) = single(0);
    end

    if currentMagnitude(WithReferenceTo.LineB) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineB) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineB);
        currentPhaseAngle(WithReferenceTo.LineB) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineB), 360);

    else
        currentPhaseAngle(WithReferenceTo.LineB) = single(0);
    end

    if currentMagnitude(WithReferenceTo.LineC) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineC) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineC);
        currentPhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineC), 360);
    else
       currentPhaseAngle(WithReferenceTo.LineC) = single(0); 
    end

    currentPhaseAngle(WithReferenceTo.Neutral) = referenceAngle - currentPhaseAngle(WithReferenceTo.Neutral);
    currentPhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.Neutral) , 360);

% When the voltage of LineA is too low, we use LineB as the referenceAngle
elseif voltageMagnitude(WithReferenceTo.LineB) >= minReliableVoltageAmplitudeLevel

    referenceAngle = voltagePhaseAngle(WithReferenceTo.LineB);
    voltagePhaseAngle(WithReferenceTo.LineB) = single(240);
    
    % Since LineA is zero
    voltageMagnitude(WithReferenceTo.LineAB) = voltageMagnitude(WithReferenceTo.LineB);
        
    voltagePhaseAngle(WithReferenceTo.LineA) = single(0);
    % Phase to Phase voltages that are missing
    voltagePhaseAngle(WithReferenceTo.LineAB) = voltagePhaseAngle(WithReferenceTo.LineB);
    
    
    % Check LineC is above Threshold 
    if voltageMagnitude(WithReferenceTo.LineC) >= minReliableVoltageAmplitudeLevel
        voltagePhaseAngle(WithReferenceTo.LineC) = referenceAngle - voltagePhaseAngle(WithReferenceTo.LineC);
        voltagePhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineC), 360);

        % Find the magnitude of the Voltage vector purely applied to the
        % x and y axis
        phaseBvoltageMagOnYaxis = voltageMagnitude(WithReferenceTo.LineB) * MathFunctions_SinDegrees(voltagePhaseAngle(WithReferenceTo.LineB));
        phaseBvoltageMagOnXaxis = voltageMagnitude(WithReferenceTo.LineB) * MathFunctions_CosDegrees(voltagePhaseAngle(WithReferenceTo.LineB));
             
        %FIXME Phase to Phase voltages
        voltageMagnitude(WithReferenceTo.LineBC) = sqrt((phaseBvoltageMagOnXaxis - phaseCvoltageMagOnXaxis)^2 +...
                                                        (phaseBvoltageMagOnYaxis - phaseCvoltageMagOnYaxis)^2);
                                                          
        voltagePhaseAngle(WithReferenceTo.LineBC) = atan2d(phaseBvoltageMagOnYaxis - phaseCvoltageMagOnYaxis,...
                                                            phaseBvoltageMagOnXaxis - phaseCvoltageMagOnXaxis);
        voltagePhaseAngle(WithReferenceTo.LineBC) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineBC), 360);
        
        
        voltageMagnitude(WithReferenceTo.LineCA) = voltageMagnitude(WithReferenceTo.LineC);
        
        voltagePhaseAngle(WithReferenceTo.LineCA) = voltagePhaseAngle(WithReferenceTo.LineC);
    % If LineC magnitude is also too low    
    else
        voltageMagnitude(WithReferenceTo.LineCA) = single(0);
         
        voltagePhaseAngle(WithReferenceTo.LineC) = single(0);
        voltagePhaseAngle(WithReferenceTo.LineBC) = voltagePhaseAngle(WithReferenceTo.LineB);
    end
    
    % TODO Decide and make changes to Neutral Voltage Displacement calculation and buffer memory method
    voltagePhaseAngle(WithReferenceTo.Neutral) = referenceAngle - voltagePhaseAngle(WithReferenceTo.Neutral) ;
    voltagePhaseAngle(WithReferenceTo.Neutral)  = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.Neutral) , 360);

    % Phase currents and neutral(/residual) current
    if currentMagnitude(WithReferenceTo.LineA) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineA) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineA);
        currentPhaseAngle(WithReferenceTo.LineA) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineA), 360);
        
    else
        currentPhaseAngle(WithReferenceTo.LineA) = single(0);
    end

    if currentMagnitude(WithReferenceTo.LineB) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineB) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineB);
        currentPhaseAngle(WithReferenceTo.LineB) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineB), 360);
    else
        currentPhaseAngle(WithReferenceTo.LineB) = single(0);
    end

    if currentMagnitude(WithReferenceTo.LineC) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineC) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineC);
        currentPhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineC), 360);
    else
        currentPhaseAngle(WithReferenceTo.LineC) = single(0);
    end

    currentPhaseAngle(WithReferenceTo.Neutral) = referenceAngle - currentPhaseAngle(WithReferenceTo.Neutral);
    currentPhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.Neutral), 360);

% When the voltages of both LineA and LineB are too low, we use LineC as
% the referenceAngle
elseif voltageMagnitude(WithReferenceTo.LineC) >= minReliableVoltageAmplitudeLevel

    referenceAngle = voltagePhaseAngle(WithReferenceTo.LineC);
    voltagePhaseAngle(WithReferenceTo.LineC) = single(120);
    
    % LineA and LineB has no value (Not-A-Number)
    voltagePhaseAngle(WithReferenceTo.LineA) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineB) = single(0);

    % TODO Decide and make changes to Neutral Voltage Displacement calculation and buffer memory method
    voltagePhaseAngle(WithReferenceTo.Neutral) = referenceAngle - voltagePhaseAngle(WithReferenceTo.Neutral);
    voltagePhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.Neutral), 360);
    
    % Phase to Phase voltages
    voltageMagnitude(WithReferenceTo.LineAB) = single(0);
    voltageMagnitude(WithReferenceTo.LineBC) = voltageMagnitude(WithReferenceTo.LineC);
    voltageMagnitude(WithReferenceTo.LineCA) = voltageMagnitude(WithReferenceTo.LineC);
    
    voltagePhaseAngle(WithReferenceTo.LineAB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineBC) = 180 + voltagePhaseAngle(WithReferenceTo.LineC);
    voltagePhaseAngle(WithReferenceTo.LineBC) = MathFunctions_Modulo(voltagePhaseAngle(WithReferenceTo.LineBC), 360);
    voltagePhaseAngle(WithReferenceTo.LineCA) = voltagePhaseAngle(WithReferenceTo.LineC);
    % Use phase-to-phase angle from memory
    % TODO Set a variable to use the voltage phase angles stored in memory
    
    % Phase currents and neutral(/residual) current
    if currentMagnitude(WithReferenceTo.LineA) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineA) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineA);
        currentPhaseAngle(WithReferenceTo.LineA) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineA), 360);
    else
        currentPhaseAngle(WithReferenceTo.LineA) = single(0);
    end
    
     if currentMagnitude(WithReferenceTo.LineB) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineB) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineB);
        currentPhaseAngle(WithReferenceTo.LineB) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineB), 360);
     else
         currentPhaseAngle(WithReferenceTo.LineB) = single(0);
     end
     
    if currentMagnitude(WithReferenceTo.LineC) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineC) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineC);
        currentPhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineC), 360); 
    else
        currentPhaseAngle(WithReferenceTo.LineC) = single(0);
    end

    currentPhaseAngle(WithReferenceTo.Neutral) = referenceAngle - currentPhaseAngle(WithReferenceTo.Neutral);
    currentPhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.Neutral), 360);
    
% When the voltages of LineA, LineB and LineC are too low, we use the LineA
% current as the referenceAngle
elseif currentMagnitude(WithReferenceTo.LineA) >= minReliableCurrentAmplitudeLevel

    % Phase to ground voltage angles
    voltagePhaseAngle(WithReferenceTo.LineA) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineC) = single(0);
    % Phase to Phase voltages angles
    voltagePhaseAngle(WithReferenceTo.LineAB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineBC) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineCA) = single(0);
    % Use phase-to-phase angle from memory
    % TODO Set a variable to use the voltage phase angles stored in memory
    
    referenceAngle = currentPhaseAngle(WithReferenceTo.LineA);
    currentPhaseAngle(WithReferenceTo.LineA) = single(0);

    if currentMagnitude(WithReferenceTo.LineB) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineB) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineB);
        currentPhaseAngle(WithReferenceTo.LineB) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineB), 360);
    else
         currentPhaseAngle(WithReferenceTo.LineB) = single(0);
    end
     
    if currentMagnitude(WithReferenceTo.LineC) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineC) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineC);
        currentPhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineC), 360);
    else
        currentPhaseAngle(WithReferenceTo.LineC) = single(0);
    end
    
    currentPhaseAngle(WithReferenceTo.Neutral) = referenceAngle - currentPhaseAngle(WithReferenceTo.Neutral);
    currentPhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.Neutral), 360);
    
% When the voltages of LineA, LineB, LineC and LineA current are too low,
% we use LineB current as referenceAngle
elseif currentMagnitude(WithReferenceTo.LineB) >= minReliableCurrentAmplitudeLevel 
    
    % Phase to ground voltage
    voltagePhaseAngle(WithReferenceTo.LineA) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineC) = single(0);
    % Phase to Phase voltages
    voltagePhaseAngle(WithReferenceTo.LineAB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineBC) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineCA) = single(0);
    % Use phase-to-phase angle from memory
    % TODO Set a variable to use the voltage phase angles stored in memory

    referenceAngle = currentPhaseAngle(WithReferenceTo.LineB);
    currentPhaseAngle(WithReferenceTo.LineB) = single(240);    
    
    currentPhaseAngle(WithReferenceTo.LineA) = single(0);
    
    if currentMagnitude(WithReferenceTo.LineC) >= minReliableCurrentAmplitudeLevel
        currentPhaseAngle(WithReferenceTo.LineC) = referenceAngle - currentPhaseAngle(WithReferenceTo.LineC);
        currentPhaseAngle(WithReferenceTo.LineC) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.LineC), 360);
    else
        currentPhaseAngle(WithReferenceTo.LineC) = single(0);
    end
    
    currentPhaseAngle(WithReferenceTo.Neutral) = referenceAngle - currentPhaseAngle(WithReferenceTo.Neutral);
    currentPhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.Neutral), 360);
    
% When the voltages of LineA, LineB, LineC and currents LineA and LineB current are too low,
% we use LineC current as referenceAngle
elseif currentMagnitude(WithReferenceTo.LineC) >= minReliableCurrentAmplitudeLevel 
    
    % Phase to ground voltage
    voltagePhaseAngle(WithReferenceTo.LineA) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineC) = single(0);
    % Phase to Phase voltages
    voltagePhaseAngle(WithReferenceTo.LineAB) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineBC) = single(0);
    voltagePhaseAngle(WithReferenceTo.LineCA) = single(0);
    % Use phase-to-phase angle from memory
    % TODO Set a variable to use the voltage phase angles stored in memory
    
    referenceAngle = currentPhaseAngle(WithReferenceTo.LineC);
    currentPhaseAngle(WithReferenceTo.LineC) = single(120);
  
    currentPhaseAngle(WithReferenceTo.LineA) = single(0);
    currentPhaseAngle(WithReferenceTo.LineB) = single(0);
   
    currentPhaseAngle(WithReferenceTo.Neutral) = referenceAngle - currentPhaseAngle(WithReferenceTo.Neutral);
    currentPhaseAngle(WithReferenceTo.Neutral) = MathFunctions_Modulo(currentPhaseAngle(WithReferenceTo.Neutral), 360);
end
end