% Orthogonal Component Frequency Measurement (linear model/version)
% A Hybrid Method for Power System Frequency Estimation
% Jinfeng Ren, Student Member, IEEE, and Mladen Kezunovic, Fellow, IEEE
% IEEE TRANSACTIONS ON POWER DELIVERY, VOL. 27, NO. 3, JULY 2012

function estimatedFrequency = OCFrequencyMeasurement2(ys, yc, ysOld, ycOld, nominalFrequency)
% Orthogonal Component Frequency Measurement
% param 
% 
% return

estimatedFrequency = 0;

estimatedFrequency = nominalFrequency +((1/(2*pi))*((ycOld*ys - ysOld*yc)/(ycOld^2+ysOld^2)) );   