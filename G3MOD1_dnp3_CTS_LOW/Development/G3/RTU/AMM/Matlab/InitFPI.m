function [FpiCfgAndParams, FpiFaultParams, FpiConfig] = InitFPI()
%#codegen
% Create FPI related structures
%

coder.inline('always');

FpiFaultParams = struct('initialTimeofFaultMs', uint32(zeros(1)),...       % Initial time of fault occuring
                        'faultDurationMs', uint32(zeros(1)),...               % Fault duration
                        'inrushDurationMs', uint32(zeros(1)),...              % Inrush duration
                        'instantFaultEventDetected', uint32(zeros(1)),...     % Flag for Instant fault event
                        'faultEventDetected', uint32(zeros(1)),...            % Flag for fault event
                        'inrushDetected', uint32(zeros(1)),...                % Inrush Flag
                        'initTimeofInstantAlarmMs', uint32(zeros(1)),...      % Initial time at which Instant Alarm triggered
                        'initTimeofAlarmMs', uint32(zeros(1)),...             % Initial time at which Alarm triggered
                        'initTimeofInrushMs', uint32(zeros(1)),...            % Initial time at which Inrush event started
                        'timeSinceAlarmMs', uint32(zeros(1)),...              % Duration since Alarm triggered
                        'timeSinceInstantAlarmMs', uint32(zeros(1)),...       % Duration since Instant Alarm triggered
                        'alarm', uint32(zeros(1)),...                         % Alarm
                        'instantAlarm', uint32(zeros(1)));                    % Instant Alarm

FpiConfig = struct('enabled', uint32(zeros(1)),...
                      'minFaultDurationMs', uint32(zeros(1)),... 
                      'minInrushDurationMs', uint32(zeros(1)),... 
                      'instantFaultCurrent', uint32(zeros(1)),...
                      'inrushCurrent', uint32(zeros(1)),...
                      'timedFaultCurrent', uint32(zeros(1)));

% Matlab way of creating arrays of a structure
temp = struct(   'cfg', FpiConfig,...
                 'faultParams',  FpiFaultParams);

% For code generation purposes the function repmat HAS to be used
% in order to code generate arrays of structures.
% Using a for loop instead of repmat simply does NOT work for code
% generation purposes, while having identical results in Matlab.
FpiCfgAndParams = repmat(temp,1,int32(WithReferenceTo.Neutral));

% for i = 1:1:uint32(WithReferenceTo.Neutral)
%     FpiCfgAndParamsStr(i).cfg = FpiCfgAndParamsStr(1).cfg;
%     FpiCfgAndParamsStr(i).faultParams = FpiCfgAndParamsStr(1).faultParams;
% end
end