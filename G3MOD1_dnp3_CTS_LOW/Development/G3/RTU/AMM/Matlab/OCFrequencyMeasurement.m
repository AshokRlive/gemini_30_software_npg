% Orthogonal Component based Frequency Measurement 
% Function located here is for the evalution of frequency measurement
% algorithms per "8.2.5.3 Measurement of Frequency..." from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function frequencyMsrmnt = OCFrequencyMeasurement(yc, ys, ycMinusOne, ysMinusOne, ycMinusTwo, ysMinusTwo, delayK, PointsPerCycle, nominalFrequency)

frequencyMsrmnt = 0;

frequencyMsrmnt = (1/(2*pi)) * PointsPerCycle * nominalFrequency *(1/delayK) * ...
    acos((0.5*((ys * ycMinusTwo)-(yc * ysMinusTwo)))/((ys * ycMinusOne) - (yc * ysMinusOne)));
end