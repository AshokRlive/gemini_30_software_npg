% Orthogonal Component Frequency Measurement
% 
%  
% 
% 

function estimatedFrequency = RecursiveOCFrequency4(ys, yc, seperationBetweenSamples, samplingRate, pointsPerCycle)
% Orthogonal Component Frequency Measurement
% param 
% 
% return
estimatedFrequency = 0;
tmpEstimatedFrequency = 0;
frequencyBin = 0;

for index = 2:1:((pointsPerCycle)-1)
%    tmpEstimatedFrequency(index) = (samplingRate * atan2(yc(index+seperationBetweenSamples), ys(index))) / (2*pi*seperationBetweenSamples);
%    tmpEstimatedFrequency(index) = (samplingRate * atan2(yc(index), ys(index))) / (2*pi);
    tmpEstimatedFrequency(index) = (samplingRate/(2*pi))*acos(0.5*((ys)-1));
    
    frequencyBin(index) = tmpEstimatedFrequency(index);
    tmpEstimatedFrequency(index) = tmpEstimatedFrequency(index)+tmpEstimatedFrequency(index-1);
end

estimatedFrequency = tmpEstimatedFrequency(index) / (((pointsPerCycle)-2));
end