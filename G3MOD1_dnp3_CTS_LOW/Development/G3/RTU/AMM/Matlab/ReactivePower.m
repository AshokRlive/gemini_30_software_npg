%

classdef ReactivePower < uint32
   enumeration
      Qa       (1)           % Reactive Power Phase 1
      Qb       (2)           % Reactive Power Phase 2
      Qc       (3)           % Reactive Power Phase 3
      LAST     (4)           % 
   end
end