% Orthogonal Component Cosine
% Algorithm per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function yc = OrthogonalComponentCosine(waveformDataPoints, windowLength, dataIndex, phaseIndex, siUnitIndex)
% Orthogonal component Cosine
% param 
% 
% return

persistent ycPrevious;
if isempty(ycPrevious)
    ycPrevious = zeros(uint32(WithReferenceTo.Ne), uint32(SIUnit.I));
end

%yc = ycPrevious(phaseIndex, siUnitIndex) + (waveformDataPoints(dataIndex) * cos(2 * pi * dataIndex / windowLength));
yc = ycPrevious(phaseIndex, siUnitIndex) + (waveformDataPoints(windowLength+1-dataIndex) * cos((1/(windowLength/2)) * pi * (((windowLength-1)/2) - dataIndex)));


ycPrevious(phaseIndex, siUnitIndex) = yc;

if dataIndex == windowLength
    ycPrevious(phaseIndex, siUnitIndex) = 0;
    yc = yc / (windowLength/2);
end
end