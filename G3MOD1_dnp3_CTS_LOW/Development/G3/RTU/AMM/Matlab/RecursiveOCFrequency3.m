% Orthogonal Component Frequency Measurement
% Rate of Change of Phase Angle between two consecutive points.
% More info: "A New Measurement Technique for Tracking Voltage Phasors, 
% Local System Frequency, and Rate of Change of Frequency",
% IEEE Transactions on Power Apparatus and Systems, Vol. PAS-102, No. 5-May 1983

function estimatedFrequency = RecursiveOCFrequency3(ys, yc, ysOld, ycOld, index, pointsPerCycle, nominalFrequency, samplingTimePeriod)
% Orthogonal Component Frequency Measurement
% param 
% 
% return

estimatedFrequency = 0;

%estimatedFrequency = (atan2(ys,yc)*((2*pi*(index+1))/(nominalFrequency*pointsPerCycle)))-(atan2(ysOld,ycOld)*((2*pi*(index))/(nominalFrequency*pointsPerCycle)));
estimatedFrequency = (atan2(ys,yc)-atan2(ysOld,ycOld));
%*50*pointsPerCycle)/(2*pi*index);
                                      
end