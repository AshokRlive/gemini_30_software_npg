%

classdef WithReferenceTo < Simulink.IntEnumType
   enumeration
      LineA(1),           % Line to Earth Phase 1
      LineB(2),           % Line to Earth Phase 2
      LineC(3),           % Line to Earth Phase 3
      Neutral(4),         % Neutral
      LineAB(5),          % Line to Line Phase 12
      LineBC(6),          % Line to Line Phase 23
      LineCA(7)          % Line to Line Phase 31
   end
   % Include Class Name Prefix in Generated Enumerated Type Value Names
%    methods(Static)
%       function y = addClassNameToEnumNames()
%         y=true;
%       end
%     end
end