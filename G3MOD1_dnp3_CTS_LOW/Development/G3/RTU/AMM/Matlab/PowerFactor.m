% Power Factor 
% Function located here are for the evalution of the Power Factor Eq.8131
% algorithms per "8.2.4.2 Measurement of Phase Shift Between Two Signals" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function pf = PowerFactor( realPower, reactivePower)


phaseAngle = 0;
pf = 0;

phaseAngle = atan(reactivePower / realPower);
pf = cos(phaseAngle);
end