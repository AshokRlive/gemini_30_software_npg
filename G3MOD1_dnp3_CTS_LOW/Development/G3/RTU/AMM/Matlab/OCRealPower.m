% Orthogonal Component based Real Power 
% Function located here are for the evalution of Real Power
% algorithms per "8.1.3.2 Measurement of Power" from the book (Eq. 8.64)
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function realPower = OCRealPower( us, ic,...
                                  isMinusOne, ucMinusOne,...
                                  delayK, windowLength)

realPower = 0;

realPower = ((us * isMinusOne) + (ucMinusOne * ic))/...
                ( cos(((delayK * 2 * pi) / windowLength)));
end