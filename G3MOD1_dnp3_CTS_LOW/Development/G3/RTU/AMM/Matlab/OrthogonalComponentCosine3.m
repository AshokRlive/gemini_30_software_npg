% Orthogonal Component Cosine
% Algorithm per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function yc = OrthogonalComponentCosine3(waveformDataPoints, windowLength, cosineLUT)
% Orthogonal component Cosine
% param 
% 
% return

yc = 0;

for index = 1:windowLength
   yc = yc + (waveformDataPoints(windowLength+1-index) * cosineLUT(index));
end

yc = yc / (windowLength/sqrt(2));
end