%--------------------------------------------------------------------------
% Analog Measurements Module
% FIR Orthogoal Component based measurement algorithms
% 
% Raw 3-phase waveform data import via COMTRADE formated files
% Import 3 Voltage and 3 Current waveforms
%
% Calculations of V, I, P, Q, S, cos(phi), phase angle, frequency and True RMS parameters
% 
% Use of above parameters for:
% Overcurrent (/FPI)
% Directionality
% Inrush Restraint
% Energy related (kWhr, KVAhr, etc...)
% Sag, Swell, Interruption
% THD (Notch filter based)
% Harmonics (Short-Time Fourier Transform based)
% High Impedance Fault Detection AND Location
% Distrubance Recording (COMTRADE based)
%

clear workspace
clear variables
clear functions
clf
close all              

% Initialize AMM structures that are common across all hardware models
[CurrentDirection, TypeOfFault,...
 VoltageMagnitude, CurrentMagnitude,...
 VoltagePhaseAngle, CurrentPhaseAngle ] = InitAMMStructures();

% Create Analog Measurements Module structure variables
[OC, I_EstimatedParam, V_EstimatedParam, Power, AMMConfig, Phasor] = InitAMMGxSwitch();
% Configure AMM structures
AMMConfig = ConfigAMMGxSwitch(AMMConfig);

% Initialize Fault Passage Indication structure variables
[FpiCfgAndParams, FpiFaultParams, FpiConfig] = InitFPI();
% Configure FPI related values
FpiCfgAndParams = ConfigFPI(FpiCfgAndParams);

% Initialize Directionality structure variables
DirectionalityConfig = InitDirectionality();
% Configure Directionality parameters
DirectionalityConfig = ConfigDirectionality(DirectionalityConfig);

% Initialization of tmpTypeOfFaultStr
tmpTypeOfFaultStr = TypeOfFault;
                                   
nominalFrequency = AMMConfig.NominalFrequency; 
nominalSamplingRate = AMMConfig.NominalSamplingRate; 
samplingTime = AMMConfig.SamplingTime;
pointsPerCycle = AMMConfig.PointsPerCycle;                 

% If set to 1 the windowLength moves one full cycle at time
% Set to 2 and it moves half a cycle at time
windowLengthDivider = 1;
% Adjust the window length
windowLength = pointsPerCycle / windowLengthDivider;
% If set to 1 the windowLength moves one full cycle (/one windowLength) at time (ie no overlap)
% Set to 2 and it moves half a cycle (/half a windowLength) at time (ie Half a window length of overlap)
slidingWindowSize = 1;


% Initialize local variables
mainsCycleNumber = 0;
delayK = 1;
frequencyMsrmnt = zeros(1,1);
idxPhase = 0;
a=0;
b=0;
c=0;
neutralCurrentFound = 0;
faultedPointIndex = 0;
currentTimeMs = 0;
averagedFrequency = 0;
voltageTransientDetected = 0;
currentTransientDetected = 0;
idxHalfCycle = 0;
% How many ADC chips (/ sets of V and I) are used
channelsUsed = 1;

single singleDataVariable;

%
d = designfilt('lowpassiir', ...                % Response type
       'PassbandFrequency',50, ...              % Frequency constraints
       'StopbandFrequency',100, ...
       'PassbandRipple',0.1, ...                % Magnitude constraints
       'StopbandAttenuation',106, ...           % SINAD(dB) for AD7779
       'DesignMethod','ellip', ...              % Design method
       'MatchExactly','passband', ...           % Design method options
       'SampleRate',nominalSamplingRate);       % Sample rate

%fvtool(d);

% Read analogue waveform data
% Variable naming must be:
% Phase-to-earth Voltage 1 should be named "VL1_E", Voltage 2 "VL2_E", etc
% Phase Current 1 should be named "IL1", Current 2 "IL2", etc
read_comtrade;

% List variable names in the workspace
listVariableNames = who;

% Raw Comtrade Waveform data time lenght in seconds
% TODO Read Comtrade config file parameters like nominal frequency, sampling rate
% number of samples and use accordingly
rawComTradeDataTimeLength = length(VL1_E)/Sample_rate;
% 0.16; 1.5;  0.52                 

% Check if loaded COMTRADE signal variables contain an integer number of mains cycles
% If not then remove the data points of the last non-integer mains cycle 
 remainder = rem(length(VL1_E),(Sample_rate/Frequency));
 tmpVariable = (abs(remainder-0) < 1e8*eps(min(abs(remainder),abs(0))));
 if remainder ~= 0
%     abs(A-B) < 1e4*eps(min(abs(A),abs(B)))
     VL1_E = VL1_E(1:length(VL1_E)-remainder);
     VL2_E = VL2_E(1:length(VL2_E)-remainder);
     VL3_E = VL3_E(1:length(VL3_E)-remainder);
     IL1 = IL1(1:length(IL1)-remainder);
     IL2 = IL2(1:length(IL2)-remainder);
     IL3 = IL3(1:length(IL3)-remainder);
     % If neutral current variable exists concatenate it as well
      for indexWorkspaceVariables = 1:1:numel(listVariableNames)
            if strcmp(listVariableNames(indexWorkspaceVariables), 'In')
               In = In(1:length(In)-remainder); 
            end
      end
     
     % Re-estimate the time length variable
     rawComTradeDataTimeLength = length(VL1_E)/Sample_rate;
 end

% If the declared Comtrade Sample_rate is not the same as the initAMM 
% sampleRate we need to resample the signals
% if Sample_rate ~= nominalSamplingRate
% Resample as Comtrade waveform data might be sampled at different sampling rates
resampledDataLength = round(pointsPerCycle*Frequency*rawComTradeDataTimeLength);
integerTotalMainsCycleNumber = round(Frequency*rawComTradeDataTimeLength, 1);
% Voltage Signals
 Phasor.V(1).RawData(1:resampledDataLength,WithReferenceTo.LineA) = resample(VL1_E,...
                                                        resampledDataLength,...
                                                        length(VL1_E));

 Phasor.V(1).RawData(1:resampledDataLength,WithReferenceTo.LineB)= resample(VL2_E,...
                                                        resampledDataLength,...
                                                        length(VL2_E));

 Phasor.V(1).RawData(1:resampledDataLength,WithReferenceTo.LineC) = resample(VL3_E,...
                                                        resampledDataLength,...
                                                        length(VL3_E));
% Generate Neutral Voltage Displacement by vectorial summation
for dataIdx = 1:1:resampledDataLength
    Phasor.V(1).RawData(dataIdx,WithReferenceTo.Neutral) =...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineA)+...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineB)+...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineC);
end
% else
%     
% end

% Phase-to-Phase Voltage Generation 
for dataIdx = 1:1:resampledDataLength
    % Phase-to-Phase Voltage Vab
    Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineAB) =...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineA)-...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineB);
    % Phase-to-Phase Voltage Vbc
    Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineBC) =...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineB)-...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineC);
    % Phase-to-Phase Voltage Vca
    Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineCA) =...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineC)-...
                        Phasor.V(1).RawData(dataIdx,WithReferenceTo.LineA);                    
end
% Current Signals
 Phasor.I.RawData(1:resampledDataLength,WithReferenceTo.LineA) = resample(IL1,...
                                                        resampledDataLength,...
                                                        length(IL1));
                                                    
 Phasor.I.RawData(1:resampledDataLength,WithReferenceTo.LineB) = resample(IL2,...
                                                        resampledDataLength,...
                                                        length(IL2));
                                                    
 Phasor.I.RawData(1:resampledDataLength,WithReferenceTo.LineC) = resample(IL3,...
                                                        resampledDataLength,...
                                                        length(IL3));

 % Search for the Neutral Current variable
 % The read_comtrade function above should load all the analog measurement
 % signals into the workspace
 for indexWorkspaceVariables = 1:1:numel(listVariableNames)
     if strcmp(listVariableNames(indexWorkspaceVariables), 'In')
        neutralCurrentFound = 1; 
        Phasor.I.RawData(1:resampledDataLength,WithReferenceTo.Neutral)= resample(In,...
                                                        resampledDataLength,...
                                                        length(In));
     end
 end
 
% If the Neutral Current variable can not be found, generate it by vectorial sum
if neutralCurrentFound == 0
    for dataIdx = 1:1:resampledDataLength
        Phasor.I.RawData(dataIdx,WithReferenceTo.Neutral) =...
                    Phasor.I.RawData(dataIdx,WithReferenceTo.LineA)+...
                    Phasor.I.RawData(dataIdx,WithReferenceTo.LineB)+...
                    Phasor.I.RawData(dataIdx,WithReferenceTo.LineC);
    end    
end


 Phasor.V(1).FilteredData(1:resampledDataLength,WithReferenceTo.LineA) = filter(d, Phasor.V(1).RawData(1:end,WithReferenceTo.LineA));
 Phasor.V(1).FilteredData(1:resampledDataLength,WithReferenceTo.LineB) = filter(d, Phasor.V(1).RawData(1:end,WithReferenceTo.LineB));
 Phasor.V(1).FilteredData(1:resampledDataLength,WithReferenceTo.LineC) = filter(d, Phasor.V(1).RawData(1:end,WithReferenceTo.LineC));
 Phasor.V(1).FilteredData(1:resampledDataLength,WithReferenceTo.Neutral) = filter(d, Phasor.V(1).RawData(1:end,WithReferenceTo.Neutral));

 Phasor.I.FilteredData(1:resampledDataLength,WithReferenceTo.LineA) = filter(d, Phasor.I.RawData(1:end,WithReferenceTo.LineA));
 Phasor.I.FilteredData(1:resampledDataLength,WithReferenceTo.LineB) = filter(d, Phasor.I.RawData(1:end,WithReferenceTo.LineB));
 Phasor.I.FilteredData(1:resampledDataLength,WithReferenceTo.LineC) = filter(d, Phasor.I.RawData(1:end,WithReferenceTo.LineC));
 Phasor.I.FilteredData(1:resampledDataLength,WithReferenceTo.Neutral) = filter(d, Phasor.I.RawData(1:end,WithReferenceTo.Neutral));

% Mock a fault on a particular phase      
%TypeOfFaultStr = MockAFault(TypeOfFaultStr, integerTotalMainsCycleNumber);

% Generate projection matrix for Fault Inception Point function
projectionMatrix = GenerateProjectionMatrix(windowLength, pointsPerCycle, nominalFrequency);

% Generate transfomration matrix
transformationMatrix = GenerateTransformationMatrix(windowLength, pointsPerCycle, nominalFrequency);

cosineLUT = GenerateCosineLUT( windowLength, pointsPerCycle);

sineLUT = GenerateSineLUT( windowLength, pointsPerCycle);

% Tolerance value for gradient decent
tolerance = 25000000;

% Initialize where the dataIdx should start
% A full cycle of data is intially required for the FIR to operate
if windowLengthDivider == 1
    firstDataIdxElement = 1;
elseif windowLengthDivider > 1
    firstDataIdxElement = pointsPerCycle*1;
end

% Orthogonal Component Sine and Cosine Calculation using the
% 'standard' (and more CPU intesive) algorithm. This should run
% only once and provide the initial OC values for the recursive
% OC calculation algorithm.
for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
    for idxWindowLenght = 1:1:windowLength
    % Voltage Components
    Phasor.V(1).OC.Cosine(idxWindowLenght, idxPhase) = OrthogonalComponentCosine2(...
                                                    Phasor.V(1).RawData(idxWindowLenght:(windowLength+idxWindowLenght), idxPhase),...
                                                    windowLength,...
                                                    pointsPerCycle);
    Phasor.V(1).OC.Sine(idxWindowLenght, idxPhase) = OrthogonalComponentSine2(...
                                                    Phasor.V(1).RawData(idxWindowLenght:(windowLength+idxWindowLenght), idxPhase),...
                                                    windowLength,...
                                                    pointsPerCycle);
    % Current Components
    Phasor.I.OC.Cosine(idxWindowLenght, idxPhase) = OrthogonalComponentCosine2(...
                                                    Phasor.I.RawData(idxWindowLenght:(windowLength+idxWindowLenght), idxPhase),...
                                                    windowLength,...
                                                    pointsPerCycle);
    Phasor.I.OC.Sine(idxWindowLenght, idxPhase) = OrthogonalComponentSine2(...
                                                    Phasor.I.RawData(idxWindowLenght:(windowLength+idxWindowLenght), idxPhase),...
                                                    windowLength,...
                                                    pointsPerCycle);                 
    end
end

% Initialize main while loop waveform data index
dataIdx = firstDataIdxElement;

% Main loop
while (dataIdx <= (rawComTradeDataTimeLength*nominalSamplingRate)+pointsPerCycle)
%for dataIdx = firstDataIdxElement:(pointsPerCycle/slidingWindowSize):(rawComTradeDataTimeLength*nominalSamplingRate)+pointsPerCycle

    % Stop when index is greater than the number of points in the
    % comtrade file
%    if (dataIdx) <= ((pointsPerCycle*nominalFrequency*rawComTradeDataTimeLength)+pointsPerCycle)

        % Increment the window cycle number
        mainsCycleNumber = mainsCycleNumber + 1;
        currentTimeMs = currentTimeMs + 20;
        
        if mainsCycleNumber == 1
        % Do nothing         
     
        else
            % This IF statememnt is required so the very last mains cycle
            % worth of data is operated on
            if (mainsCycleNumber < integerTotalMainsCycleNumber+1) &&...
                   (dataIdx) <= ((pointsPerCycle*Frequency*rawComTradeDataTimeLength)-pointsPerCycle)
                
                if mainsCycleNumber > 2
                    %
                    
                end
                
                % Calculate Sine and Cosine Orthogonal Components using
                % non-Recursive (point by point) DFT method
                % for the Voltage and Current signals 
                for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
                    for idxWindowLenght = 1:1:windowLength
                        % Standard DFT function.
                        % Voltage Components
                        Phasor.V(1).OC.Cosine(windowLength+idxWindowLenght, idxPhase) = OrthogonalComponentCosine2(...
                                                                        Phasor.V(1).RawData((dataIdx-windowLength+idxWindowLenght):dataIdx+idxWindowLenght, idxPhase),...
                                                                        windowLength,...
                                                                        pointsPerCycle);
                        Phasor.V(1).OC.Sine(windowLength+idxWindowLenght, idxPhase) = OrthogonalComponentSine2(...
                                                                        Phasor.V(1).RawData((dataIdx-windowLength+idxWindowLenght):dataIdx+idxWindowLenght, idxPhase),...
                                                                        windowLength,...
                                                                        pointsPerCycle);
                        % Current Components
                        Phasor.I.OC.Cosine(windowLength+idxWindowLenght, idxPhase) = OrthogonalComponentCosine2(...
                                                                        Phasor.I.RawData((dataIdx-windowLength+idxWindowLenght):dataIdx+idxWindowLenght, idxPhase),...
                                                                        windowLength,...
                                                                        pointsPerCycle);
                        Phasor.I.OC.Sine(windowLength+idxWindowLenght, idxPhase) = OrthogonalComponentSine2(...
                                                                        Phasor.I.RawData((dataIdx-windowLength+idxWindowLenght):dataIdx+idxWindowLenght, idxPhase),...
                                                                        windowLength,...
                                                                        pointsPerCycle);
                   end
                end
            end % mainsCycleNumber < integerTotalMainsCycleNumber+1                                                        

            % Magnitude calculation
            % TODO Encapsulate, test magnitude, phase, frequency response, etc...
            % Voltage Magnitude
            for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
                for idxWindowLenght = 1:1:windowLength

                    Phasor.V(1).Mag(((mainsCycleNumber-2)*pointsPerCycle)+idxWindowLenght, idxPhase) = OCMagnitudeMeasurement(...
                                        Phasor.V(1).OC.Sine(idxWindowLenght,idxPhase),...
                                        Phasor.V(1).OC.Cosine(idxWindowLenght,idxPhase));     
                end
            end

            % Current Magnitude 
            % TODO Encapsulate, test magnitude, phase, frequency response, etc...
            % Magnitude calculation
            for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
                for idxWindowLenght = 1:1:windowLength

                    Phasor.I.Mag(((mainsCycleNumber-2)*pointsPerCycle)+idxWindowLenght, idxPhase) = OCMagnitudeMeasurement(...
                                        Phasor.I.OC.Sine(idxWindowLenght,idxPhase),...
                                        Phasor.I.OC.Cosine(idxWindowLenght,idxPhase));  
                end
            end
            % Frequency Calculation
            % Only one voltage phase is used for the frequency calculations
            % TODO Frequency calculation should only happen if the Voltage
            % magnitude is above a certain threhold. Otherwise use one of the other
            % voltage phases
            % FIXME Does NOT work!


            % Phase angle estimation 
            for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
                for idxWindowLenght = 1:1:windowLength
                    % Voltage Signals
                    Phasor.V(1).Phi(((mainsCycleNumber-2)*pointsPerCycle)+idxWindowLenght,idxPhase) = OCPhaseMeasurement(...
                      Phasor.V(1).OC.Sine(idxWindowLenght,idxPhase),...
                      Phasor.V(1).OC.Cosine(idxWindowLenght,idxPhase));

%                    PhasorStr.V.SetA.Phi(mainsCycleNumber-1,idxPhase) = rad2deg(PhasorStr.V.SetA.Phi(mainsCycleNumber-1,idxPhase));
                    % Current Signals
                    Phasor.I.Phi(((mainsCycleNumber-2)*pointsPerCycle)+idxWindowLenght,idxPhase) = OCPhaseMeasurement(...
                      Phasor.I.OC.Sine(idxWindowLenght,idxPhase),...
                      Phasor.I.OC.Cosine(idxWindowLenght,idxPhase));

%                    PhasorStr.I.SetA.Phi(mainsCycleNumber-1,idxPhase) = rad2deg(PhasorStr.I.SetA.Phi(mainsCycleNumber-1,idxPhase));
                end
             end

            % Calculate the Phase-to-Phase Voltage and Phase Angles from
            % the Phase-to-Earth measurements
            for idxWindowLenght = 1:1:windowLength
                [ Phasor.V(1).Mag((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                 Phasor.I.Mag((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                 Phasor.V(1).Phi((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                 Phasor.I.Phi((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end)] = PhaseAngleCalculations( Phasor.V(1).Mag((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                                                     Phasor.I.Mag((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                                                     Phasor.V(1).Phi((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                                                     Phasor.I.Phi((mainsCycleNumber-2)*pointsPerCycle+idxWindowLenght,1:end),...
                                                     AMMConfig.MinReliableVoltageAmplitudeLevel,...
                                                     AMMConfig.MinReliableCurrentAmplitudeLevel);
            end
        
            % Move the orthogonal component data from current window
            % to previous window
            for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
                for idxWindowLenght = 1:1:windowLength  
                    % Standard Orthogonal Components
                     Phasor.V(1).OC.Cosine(idxWindowLenght, idxPhase) = Phasor.V(1).OC.Cosine(windowLength+idxWindowLenght, idxPhase);
                     Phasor.V(1).OC.Sine(idxWindowLenght, idxPhase) = Phasor.V(1).OC.Sine(windowLength+idxWindowLenght, idxPhase);
                     Phasor.I.OC.Cosine(idxWindowLenght, idxPhase) = Phasor.I.OC.Cosine(windowLength+idxWindowLenght, idxPhase);
                     Phasor.I.OC.Sine(idxWindowLenght, idxPhase) = Phasor.I.OC.Sine(windowLength+idxWindowLenght, idxPhase);
                end
            end
            
            % At this point we have estimated the Point-by-Point Magnitude and Phase Angle values
            % The next steps are to:
            % 1. Run the Fault Passage Indication algorithm
            % 2. Clasify/determine the type of fault (Single Phase to Earth, Three phase fault, etc...)
            % 3. Determine the direction of the fault using the Cross Polarization method
            
            % Clear all previous values in tmpTypeOfFaultStr
            for idxDirection = 1:1:(int32(Direction.BothDir))
                tmpTypeOfFaultStr.SinglePhaseOnA(1,idxDirection) = 0;
                tmpTypeOfFaultStr.SinglePhaseOnB(1,idxDirection) = 0;
                tmpTypeOfFaultStr.SinglePhaseOnC(1,idxDirection) = 0;
                tmpTypeOfFaultStr.Earth(1,idxDirection) = 0;
                tmpTypeOfFaultStr.PhaseToPhaseOnAB(1,idxDirection) = 0;
                tmpTypeOfFaultStr.PhaseToPhaseOnBC(1,idxDirection) = 0;
                tmpTypeOfFaultStr.PhaseToPhaseOnCA(1,idxDirection) = 0;
                tmpTypeOfFaultStr.ThreePhaseOnABC(1,idxDirection) = 0;
            end
            
            % We need to determine every half a cycle whether a fault (FPI) has occured
            % and in which direction
            for idxEveryHalfCycle = (pointsPerCycle/2):-(pointsPerCycle/2):0
                
                idxHalfCycle = idxHalfCycle + 1;
                % FPI
                for idxPhase = 1:1:(int32(WithReferenceTo.Neutral))
                    FpiCfgAndParams(idxPhase) = FaultPassageIndication(FpiCfgAndParams(idxPhase),...
                                                                Phasor.I.Mag(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,idxPhase),...
                                                                currentTimeMs);
                end

                % Fault Type Classification
                [FpiCfgAndParams, tmpTypeOfFaultStr] = FaultTypeClassification(FpiCfgAndParams,...
                                                                                    tmpTypeOfFaultStr,...
                                                                                    Phasor.I.Mag(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,WithReferenceTo.LineA),...
                                                                                    Phasor.I.Mag(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,WithReferenceTo.LineB),...
                                                                                    Phasor.I.Mag(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,WithReferenceTo.LineC),...
                                                                                    Phasor.I.Mag(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,WithReferenceTo.Neutral),...
                                                                                    AMMConfig.MinReliableCurrentAmplitudeLevel);
                % Cross Polarization Directionality                                                                 
                tmpTypeOfFaultStr = CrossPolarizationDirectionality( tmpTypeOfFaultStr,...
                                                                 Phasor.V(1).Phi(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,:),...
                                                                 Phasor.I.Phi(((mainsCycleNumber-1)*pointsPerCycle)-idxEveryHalfCycle,:),...
                                                                 DirectionalityConfig);
                                                             
                % TODO Create array of TypeOfFaultStr
                TypeOfFault.SinglePhaseOnA((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.SinglePhaseOnA(1,:);
                TypeOfFault.SinglePhaseOnB((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.SinglePhaseOnB(1,:);
                TypeOfFault.SinglePhaseOnC((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.SinglePhaseOnC(1,:);
                TypeOfFault.Earth((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.Earth(1,:);
                TypeOfFault.PhaseToPhaseOnAB((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.PhaseToPhaseOnAB(1,:);
                TypeOfFault.PhaseToPhaseOnBC((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.PhaseToPhaseOnBC(1,:);
                TypeOfFault.PhaseToPhaseOnCA((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.PhaseToPhaseOnCA(1,:);
                TypeOfFault.ThreePhaseOnABC((mainsCycleNumber)+idxHalfCycle, :) = tmpTypeOfFaultStr.ThreePhaseOnABC(1,:);
                   
            end
            idxHalfCycle = mainsCycleNumber-1;

        end

    % Increment while loop waveform data index
%    if mainsCycleNumber~=1
        dataIdx = dataIdx+(pointsPerCycle/slidingWindowSize);
%    end
end  

