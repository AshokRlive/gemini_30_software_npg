#ifndef DFPI_POINTER_TYPES_H
#define DFPI_POINTER_TYPES_H

#include "rtwtypes.h"
#include "lu_types.h"
#include "DFPI.h"
#include "DFPI_types.h"

extern VoltageParamStr VoltageParam;
extern VoltageParamStr *voltageParamPtr;
extern CurrentParamStr CurrentParam;
extern CurrentParamStr *currentParamPtr;

extern OCStr *ocParamPtr;

#endif // defined DFPI_POINTER_TYPES_H