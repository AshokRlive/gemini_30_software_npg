% Orthogonal Component based Alternative Reactive Power 
% Function located here are for the evalution of Real Power
% algorithms per "8.1.3.2 Measurement of Power" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function reactivePower = OCAltReactivePower( us, uc, is, ic,...
                                            filterGainC, filterGainS)

reactivePower = 0;

reactivePower = ((us * ic) - (uc * is))/(2 * filterGainC * filterGainS);

end