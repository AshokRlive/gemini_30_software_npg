function [  CurrentDirection, TypeOfFault,...
            VoltageMagnitude, CurrentMagnitude,...
            VoltagePhaseAngle, CurrentPhaseAngle ] = InitAMMStructures()
%#codegen
% 
% 
% 

coder.inline('never');

% Structure indicating the possible current directions of a fault
CurrentDirection = int32(zeros(1,int32(Direction.BothDir)));
%CurrentDirection = single(1:int32(Direction.BothDir));

%zeros(1, int32(Direction.BothDir), 'int32');
                            
% Structure indicating the type of fault that has occured
% i.e. Overcurrent Fault on phase A is "La", phase-to-phase fault on AB is "Lab",
% three phase fault is "Labc", etc...                           
TypeOfFault = struct('SinglePhaseOnA', int32(zeros(1, int32(Direction.BothDir))),...
                        'SinglePhaseOnB', int32(zeros(1, int32(Direction.BothDir))),...
                        'SinglePhaseOnC', int32(zeros(1, int32(Direction.BothDir))),...
                        'Earth',        int32(zeros(1, int32(Direction.BothDir))),...
                        'PhaseToPhaseOnAB', int32(zeros(1, int32(Direction.BothDir))),...
                        'PhaseToPhaseOnBC', int32(zeros(1, int32(Direction.BothDir))),...
                        'PhaseToPhaseOnCA', int32(zeros(1, int32(Direction.BothDir))),...
                        'ThreePhaseOnABC', int32(zeros(1, int32(Direction.BothDir))));

VoltageMagnitude = single(zeros(1,int32(WithReferenceTo.LineCA)));

CurrentMagnitude = single(zeros(1,int32(WithReferenceTo.Neutral)));

VoltagePhaseAngle = single(zeros(1,int32(WithReferenceTo.LineCA)));

CurrentPhaseAngle = single(zeros(1,int32(WithReferenceTo.Neutral)));

end