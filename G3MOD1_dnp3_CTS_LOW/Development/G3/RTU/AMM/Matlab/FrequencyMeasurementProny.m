% Raw Sample Frequency Measurement - Prony method
% Real-Time Determination of Power System Frequency
% Tadeusz Lobos and Jacek Rezmer
% IEEE TRANSACTIONS ON INSTRUMENTATION AND MEASUREMENT, VOL. 46, NO. 4, AUGUST 1997

function estimatedFrequency = FrequencyMeasurementProny(waveformData, pointsPerCycle, nominalFrequency)
% Orthogonal Component Frequency Measurement
% param 
% 
% return

estimatedFrequency = 0;
tmpSum1 = 0;
tmpSum2 = 0;
for index = 2:1:pointsPerCycle-1
    tmpSum1 = tmpSum1 +(waveformData(index-1)+ waveformData(index+1))^2;
    tmpSum2 = tmpSum2 + (waveformData(index)*(waveformData(index-1)+waveformData(index+1)));
end

estimatedFrequency = 2.772*((nominalFrequency*pointsPerCycle)/(2*pi)) * acos(tmpSum1/(2*tmpSum2)); 

end