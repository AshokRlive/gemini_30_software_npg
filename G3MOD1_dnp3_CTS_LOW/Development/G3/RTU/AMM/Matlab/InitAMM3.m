function AMMConfigStr = InitAMM3()
%#codegen
% 
% 
% 
%

AMMConfigStr.HardwareModel = uint32(0); % GX switch - Six Voltage and Four Currents                   
                                    
% Nominal frequency (mains frequency 50 or 60Hz)
AMMConfigStr.NominalFrequency = single(0);

% Selected ADCs' Sampling Rate (Hz)
AMMConfigStr.NominalSamplingRate = single(0);

% Selected ADCs' Sampling Time 
AMMConfigStr.SamplingTime = single(1 / AMMConfigStr.NominalSamplingRate);

% Number of samples per cycle
AMMConfigStr.PointsPerCycle = single(AMMConfigStr.NominalSamplingRate / AMMConfigStr.NominalFrequency);                                    

% Minimum operating current to allow directional criteria
AMMConfigStr.MinReliableCurrentAmplitudeLevel = single(0);

%Minimum operating voltage to allow directional criteria
AMMConfigStr.MinReliableVoltageAmplitudeLevel = single(0);

end