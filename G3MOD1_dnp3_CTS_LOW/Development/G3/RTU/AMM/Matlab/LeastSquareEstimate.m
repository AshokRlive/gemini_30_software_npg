% Least Square Estimate
%
%

function magnitude = LeastSquareEstimate( waveformDataPoints, PointsPerCycle, transformationMatrix)

Sum1 = 0;
Sum2 = 0;

%  
for indexData = 1:PointsPerCycle
    % TODO Consider changing as a dot(,) product
    residuals = transformationMatrix * waveformDataPoints(indexData);
    % Sum all the cosine (row 1) and the sine (row 2) components
    Sum1 = Sum1 + residuals(1, indexData);
    Sum2 = Sum2 + residuals(2, indexData);
end

magnitude = sqrt(Sum1^2 + Sum2^2);
end