% Orthogonal Component Sine 
% Function located here are for the evalution of frequency measurement
% algorithms per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book
% Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011)

function ys = OrthogonalComponentSine(waveformDataPoints, windowLength, dataIndex, phaseIndex, siUnitIndex)
% Orthogonal component Sine
% param 
% 
% return

persistent ysPrevious;
if isempty(ysPrevious)
    ysPrevious = zeros(uint32(WithReferenceTo.Ne),uint32(SIUnit.I));
end

%ys = ysPrevious(phaseIndex, siUnitIndex) + (waveformDataPoints(dataIndex) * sin(2 * pi * dataIndex / windowLength)); 
ys = ysPrevious(phaseIndex, siUnitIndex) + (waveformDataPoints(windowLength+1-dataIndex) * sin((1/(windowLength/2)) * pi * (((windowLength-1)/2) - dataIndex))); 

ysPrevious(phaseIndex, siUnitIndex) = ys;

if dataIndex == windowLength
    ysPrevious(phaseIndex, siUnitIndex) = 0;
    ys = ys /(windowLength/2);
end
end