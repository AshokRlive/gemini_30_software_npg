%

classdef PowerChannel < uint32
   enumeration
      SetA       (1)           % Power Channel  1
      SetB       (2)           % Power Channel  2
      SetC       (3)           % Power Channel  3
      LAST       (4)
   end
end