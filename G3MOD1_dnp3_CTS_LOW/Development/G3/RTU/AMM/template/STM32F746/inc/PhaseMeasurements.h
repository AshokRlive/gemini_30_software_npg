/*
 * PHASEMEASUREMENTS.h
 *
 *  Created on: 4 Aug 2016
 *      Author: bogias_a
 */

#ifndef PHASEMEASUREMENTS_H_
#define PHASEMEASUREMENTS_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

//#include "lu_types.h"

//#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define Hello_world        (0xFFFFFFFF)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	ADE7779_CH_A		= 0,
	ADE7779_CH_B        = 1,

	ADE7779_CH_LAST
}ADE7779_CH;

typedef enum
{
	L1 = 0,
	L2 = 1,
	L3 = 2,
	Ne = 3,

	LineToEarth_LAST
}LineToEarth;

typedef enum
{
	L12 = 0,
	L23 = 1,
	L31 = 2,

	LineToLine_LAST
}LineToLine;

typedef enum
{
	P1 = 0,
	P2 = 1,
	P3 = 2,

	Power_LAST
}RealPower;

typedef enum
{
	Q1 = 0,
	Q2 = 1,
	Q3 = 2,

	Reactive_LAST
}ReactivePower;

typedef enum
{
	S1 = 0,
	S2 = 1,
	S3 = 2,

	Apparent_LAST
}ApparentPower;

typedef struct UnitsDef
{
	int32_t VLE[LineToEarth_LAST];
	int32_t VLL[LineToLine_LAST];
	int32_t I[LineToEarth_LAST];

}UnitsStr;

typedef struct OCDef
{
	UnitsStr Sine;
	UnitsStr Cosine;
}OCStr;

typedef struct PhasorDef
{
	UnitsStr 		Mag;
	UnitsStr  		Phi;
	int32_t			P[Power_LAST];
	int32_t			Q[Reactive_LAST];
	int32_t 		S[Apparent_LAST];
	UnitsStr		RawData;
	OCStr			OC;

}PhasorStr;


#endif /* PHASEMEASUREMENTS_H_ */
