/*
 * OrthogonalComponentCosine.h
 *
 *  Created on: 20 Jul 2016
 *      Author: bogias_a
 */
/*
 * Trial License - for use to evaluate programs for possible purchase as
 * an end-user only.
 * File: OrthogonalComponentCosine.h
 *
 * MATLAB Coder version            : 3.1
 * C/C++ source code generated on  : 20-Jul-2016 10:45:56
 */

#ifndef ORTHOGONALCOMPONENTCOSINE_H
#define ORTHOGONALCOMPONENTCOSINE_H

/* Include Files */
#include <arm_math.h>
//#include <core_cm7.h>

#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
/*#include "rtwtypes.h"
#include "OrthogonalComponentCosine_types.h"*/

#define FPI_TIME_OVERFLOW        (0xFFFFFFFFFF )

/* Function Declarations */
extern float OrthogonalComponentCosine(const float waveformDataPoints[32],
		float PointsPerCycle, float windowLength);

inline static uint32_t ElapsedTime(uint32_t timeStart, uint32_t timeEnd);
inline static uint32_t ElapsedTime(uint32_t timeStart, uint32_t timeEnd)
{
    if (timeEnd > timeStart)
    {
        return (timeEnd - timeStart);
    }
    else
    {
        return ( (FPI_TIME_OVERFLOW - timeStart + timeEnd) + 1);
    }
}
#endif

/*
 * File trailer for OrthogonalComponentCosine.h
 *
 * [EOF]
 */
