/*
 * Measure.c
 *
 *  Created on: 4 Aug 2016
 *      Author: bogias_a
 */
/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

//#include "lu_types.h"
#include "PhaseMeasurements.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void PhaseMeasurements(void)
{
	ADE7779_CH			adcChan;
	PhasorStr 			phasor[ADE7779_CH_LAST];
	LineToEarth			phaseIdx;

	/* Assuming Line to Earth Values are required */

	/* Get Orthogonal Components (/Perform DFT) */
	/* Index for each ADC chip */
	for (adcChan = 0; adcChan < ADE7779_CH_LAST; adcChan++)
	{
		/* Index for each Line-to-Earth phase */
		for (phaseIdx = 0; phaseIdx < LineToEarth_LAST; phaseIdx++)
		{
			/* Voltage */
			phasor[adcChan].OC.Sine.VLE[phaseIdx] = OCSine();
			phasor[adcChan].OC.Cosine.VLE[phaseIdx] = OCCosine();
			/* Current */
			phasor[adcChan].OC.Sine.I[phaseIdx] = OCSine();
			phasor[adcChan].OC.Cosine.I[phaseIdx] = OCCosine();
		}
	}

	/* Get Magnitude from Orthogonal Components */
	/* Index for each ADC chip */
	for (adcChan = 0; adcChan < ADE7779_CH_LAST; adcChan++)
	{
		/* Index for each Line-to-Earth phase */
		for (phaseIdx = 0; phaseIdx < LineToEarth_LAST; phaseIdx++)
		{
			/* Voltage */
			phasor[adcChan].Mag.VLE[phaseIdx] = OCMagnitude(phasor[adcChan].OC.Sine.VLE[phaseIdx],
															phasor[adcChan].OC.Cosine.VLE[phaseIdx]);
			/* Current */
			phasor[adcChan].Mag.I[phaseIdx] = OCMagnitude(phasor[adcChan].OC.Sine.I[phaseIdx],
														phasor[adcChan].OC.Cosine.I[phaseIdx]);
		}
	}

	/* Get Real Power */
	/* Index for each ADC chip */
	for (adcChan = 0; adcChan < ADE7779_CH_LAST; adcChan++)
	{
		/* Index for each Line-to-Earth phase */
		for (phaseIdx = 0; phaseIdx < Power_LAST; phaseIdx++)
		{
			phasor[adcChan].P[phaseIdx] = OCRealPower();
		}
	}

	/* Get Reactive Power */
	/* Index for each ADC chip */
	for (adcChan = 0; adcChan < ADE7779_CH_LAST; adcChan++)
	{
		/* Index for each Line-to-Earth phase */
		for (phaseIdx = 0; phaseIdx < Reactive_LAST; phaseIdx++)
		{
			phasor[adcChan].Q[phaseIdx] = OCReactivePower();
		}
	}

	/* Get Phase Difference */
	/* Index for each ADC chip */
	for (adcChan = 0; adcChan < ADE7779_CH_LAST; adcChan++)
	{
		/* Index for each Line-to-Earth phase */
		for (phaseIdx = 0; phaseIdx < LineToEarth_LAST; phaseIdx++)
		{
			phasor[adcChan].Phi.VLE[phaseIdx] = PhaseDifference(phasor[adcChan].P[phaseIdx],
																phasor[adcChan].Q[phaseIdx]);
		}
	}

}

void OCSine(waveformData[windowLenght], pointsPerCycle, windowLenght)
{

}

void OCCosine(waveformData[windowLenght], pointsPerCycle, windowLenght)
{

}

void OCMagnitude(OCSineValue, OCCosineValue)
{

}

void OCRealPower(void)
{

}

void OCReactivePower(void)
{

}

void PhaseDifference(realPower, reactivePower)
{

}
