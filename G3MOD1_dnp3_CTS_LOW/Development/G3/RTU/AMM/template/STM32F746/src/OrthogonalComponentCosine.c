/*
 * OrthogonalComponentCosine.c
 *
 *  Created on: 20 Jul 2016
 *      Author: bogias_a
 */
/*
 * Trial License - for use to evaluate programs for possible purchase as
 * an end-user only.
 * File: OrthogonalComponentCosine.c
 *
 * MATLAB Coder version            : 3.1
 * C/C++ source code generated on  : 20-Jul-2016 10:45:56
 */

/* Include Files */
//#include "rt_nonfinite.h"
#include "OrthogonalComponentCosine.h"

/* Function Definitions */

/*
 * Orthogonal component Cosine
 *  param
 *
 *  return
 * Arguments    : const double waveformDataPoints[32]
 *                double PointsPerCycle
 *                double windowLength
 * Return Type  : double
 */
float OrthogonalComponentCosine(const float waveformDataPoints[32], float
  PointsPerCycle, float windowLength)
{
  float yc;
  int b_index;

  /*  Orthogonal Component Cosine  */
  /*  Function located here are for the evalution of frequency measurement */
  /*  algorithms per "8.2.1 Measurement of Magnitude of Voltage or Current" from the book */
  /*  Digital Signal Processing in Power System Protection and Control - W. Rebizant et al. (Springer, 2011) */
  yc = 0.0;
  for (b_index = 0; b_index < (int)windowLength; b_index++) {
    /*     yc = yc + (waveformDataPoints(idxNumber-idx) * cos((1/(PointsPerCycle/2)) * pi * (((windowLength-1)/2) - idx))); */
    yc += waveformDataPoints[(int)(windowLength - (1.0 + (float)b_index)) - 1] * 0.1;
//    	arm_cos_f32(((windowLength - 1.0) / 2.0 - (1.0 + (float)b_index)) * 2.0 *
//          3.1415926535897931 / PointsPerCycle);
  }

  return yc;
}

/*
 * File trailer for OrthogonalComponentCosine.c
 *
 * [EOF]
*/
