/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/


#include "stm32f7xx.h"
#include "stm32746g_discovery.h"
#include "stm32f7xx_hal_rcc.h"
#include "stm32f7xx_hal_conf.h"

#include <stdio.h>
#include <stdlib.h>

#include "OrthogonalComponentCosine.h"

//#define TRACE

uint32_t ElapsedTime(uint32_t timeStart, uint32_t timeEnd);
extern void initialise_monitor_handles(void);





//RCC_Config. = RCC_OSCILLATORTYPE_HSE;
//RCC_Config.HSEState = RCC_HSE_ON;

/*
uint32_t OscillatorType;
uint32_t HSEState;
uint32_t LSEState;
uint32_t HSIState;
uint32_t HSICalibrationValue;
uint32_t LSIState;
*/

int main()
{
	//int argc, char* argv[]
	uint32_t hCKL = 0;
	uint32_t sysClk = 0;

	HAL_StatusTypeDef HALStatus = HAL_OK;
	RCC_PLLInitTypeDef PLL_Config =
	{
			RCC_PLL_ON,
			RCC_PLLSOURCE_HSE,
			25,
			432,
			RCC_PLLP_DIV2,
			9
	};

	RCC_OscInitTypeDef RCC_Config =
	{
		RCC_OSCILLATORTYPE_HSE,
		RCC_HSE_BYPASS,
		RCC_LSE_OFF,
		RCC_HSI_OFF,
		0,
		RCC_LSI_OFF,
		PLL_Config
	};

//	uint32_t FLatency = 7;
	RCC_ClkInitTypeDef Clk_Config1 =
	{
			(RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2),
			RCC_SYSCLKSOURCE_PLLCLK,
			RCC_SYSCLK_DIV1,
			RCC_HCLK_DIV4,
			RCC_HCLK_DIV2
	};

	initialise_monitor_handles();
	HAL_Init();

	HAL_RCC_DeInit();
	/* Clock Output should be Enabled before Ext Ocsi and PLLs */
//	HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_HSE, RCC_MCODIV_1);
	HALStatus = HAL_RCC_OscConfig( &RCC_Config);

	HALStatus = HAL_PWREx_EnableOverDrive();

	HALStatus = HAL_RCC_ClockConfig( &Clk_Config1, FLASH_LATENCY_7);


	HAL_RCC_EnableCSS();


  // Show the program parameters (passed via semihosting).
  // Output is via the semihosting output channel.
//  trace_dump_args(argc, argv);

  // Send a greeting to the trace device (skipped on Release).
//  trace_puts("Hello ARM World!");

  // Send a message to the standard output.
//  puts("Standard output message.");

  // Send a message to the standard error.
//  fprintf(stderr, "Standard error message.\n");

  // At this stage the system clock should have already been configured
  // at high speed.
	hCKL = HAL_RCC_GetHCLKFreq();
	sysClk = HAL_RCC_GetSysClockFreq();

	printf("System clock: %u Hz\n", sysClk);

//  timer_start();

//  blink_led_init();

  uint32_t seconds = 0;

  float yc = 0;
  float waveformDataPoints[32] =  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  float PointsPerCycle = 32;
  float windowLength = 32;
  uint32_t timeMS = 0;
  uint32_t durationMS = 0;

#define LOOP_COUNT (5)

  int loops = LOOP_COUNT;
//  if (argc > 1)
//  {
      // If defined, get the number of loops from the command line,
      // configurable via semihosting.
//      loops = atoi (argv[1]);
//  }

  // Short loop.
  for (int i = 0; i < loops; i++)
    {
//      blink_led_on();
//      timer_sleep(i == 0 ? TIMER_FREQUENCY_HZ : BLINK_ON_TICKS);

//     blink_led_off();

//      timer_sleep(BLINK_OFF_TICKS);

      timeMS = HAL_GetTick();

      uint32_t ix=0;
      for (ix=0; ix<1000; ix++)
        {
//         ix=ix+1;
          yc = OrthogonalComponentCosine( waveformDataPoints, PointsPerCycle, windowLength);
        }
      ix=0;

      durationMS = HAL_GetTick();
      durationMS = ElapsedTime(timeMS, durationMS);

      ++seconds;
      // Count seconds on the trace device.
      printf("Duration %u\n", durationMS);
    }
  return 0;
}
