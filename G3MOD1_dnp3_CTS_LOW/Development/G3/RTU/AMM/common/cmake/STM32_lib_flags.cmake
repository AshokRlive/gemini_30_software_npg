# Common C FLAGS
#SET (CMAKE_C_COMMON_FLAGS "-march=armv7e-m -mtune=cortex-m7 -mfloat-abi=hard -mfpu=fpv5-sp-d16  -DSTM32F7 -DSTM32 -DSTM32F746NGHx -DDEBUG -DUSE_HAL_DRIVER -DSTM32F746xx -g3 -Wall -fmessage-length=0 -ffunction-sections")
SET (CMAKE_C_COMMON_FLAGS "-mthumb -march=armv7e-m -mfloat-abi=hard -mfpu=fpv5-sp-d16  -DSTM32F7 -DSTM32 -DSTM32F746NGHx -DDEBUG -DUSE_HAL_DRIVER -DSTM32F746xx -g3 -Wall -fmessage-length=0 -ffunction-sections")

# Common Linker FLAGS
SET (CMAKE_COMMON_EXE_LINKER_FLAGS "-Wl,--gc-sections -Xlinker -Map=${PROJECT_NAME}.map")

# Debug FLAGS
SET(CMAKE_C_FLAGS_DEBUG "-O0 -DBUILD=Debug ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_COMMON_EXE_LINKER_FLAGS}")

# Release FLAGS
SET(CMAKE_C_FLAGS_RELEASE "-O2 -DBUILD=Release ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_COMMON_EXE_LINKER_FLAGS}")
