include (CMakeForceCompiler)

# this one is important
SET(CMAKE_SYSTEM_NAME Generic)

#this one not so much
#SET(CMAKE_SYSTEM_VERSION 1)
#SET(CMAKE_VERBOSE_MAKEFILE ON)

# specify the cross compiler
set(COMPILER_ROOT_FOLDER "/usr/local/arm/gcc-arm-none-eabi-5_4-2016q3")


#SET(CMAKE_C_COMPILER ${COMPILER_ROOT_FOLDER}/bin/arm-none-eabi-gcc)
#SET(CMAKE_CXX_COMPILER ${COMPILER_ROOT_FOLDER}/bin/arm-none-eabi-g++)

CMAKE_FORCE_C_COMPILER(${COMPILER_ROOT_FOLDER}/bin/arm-none-eabi-gcc STM32_GCC)
CMAKE_FORCE_CXX_COMPILER(${COMPILER_ROOT_FOLDER}/bin/arm-none-eabi-g++ STM32_G++)

SET(STM32_OBJCOPY ${COMPILER_ROOT_FOLDER}/bin/arm-none-eabi-objcopy)
SET(CMAKE_SIZE ${COMPILER_ROOT_FOLDER}/bin/arm-none-eabi-size)

#SET(CMAKE_SIZE ${COMPILER_AR}/bin/arm-none-eabi-ar)
#SET(CMAKE_SIZE ${COMPILER_RANLIB}/bin/arm-none-eabi-ranlib)



# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
