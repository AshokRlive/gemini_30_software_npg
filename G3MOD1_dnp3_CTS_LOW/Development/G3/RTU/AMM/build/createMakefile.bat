REM Remove Debug directory
rd /Q /s Debug

REM Create Debug directory
mkdir Debug

REM Run cmake and create Debug Makefiles
cd Debug
cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug ../src/
cd ..

REM Remove Release directory
REM rd /Q /s Release

REM Create Debug directory
REM mkdir Release

REM Run cmake and create Debug Makefiles
REM cd Release
REM cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release ../../src/
REM cd ..
