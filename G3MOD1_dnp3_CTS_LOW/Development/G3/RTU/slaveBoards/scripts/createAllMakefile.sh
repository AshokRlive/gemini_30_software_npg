#!/bin/sh

cd ../FDMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../HMIBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../PSMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../SCMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../IOMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../FPMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../FPMBoardV2/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../DSMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts

cd ../SSMBoard/build
./createMakefile.sh
cd ../SlaveBootloader/build
./createMakefile.sh
cd ../../AppBootProg/build
./createMakefile.sh
cd ../../../scripts
