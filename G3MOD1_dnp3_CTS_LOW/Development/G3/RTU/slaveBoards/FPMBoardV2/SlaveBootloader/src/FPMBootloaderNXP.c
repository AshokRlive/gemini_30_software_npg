/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief  FPM Bootloader: implementation
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/


/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/


/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/
#include <string.h>

#include "system_LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "debug_frmwrk.h"
#include "systemTime.h"
#include "CANProtocol.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"
#include "Bootloader.h"

#include "systemStatus.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSystemEnum.h"
#include "versions.h"
#include "svnRevision.h"

/* Board specific includes */
#include "CANProtocolDecoder.h"
#include "BoardStatusManager.h"


#include "BoardBootloader.h"
#include "FactoryTest.h"

#include "SlaveBinImage.h"
#include "SlaveBinImageDef.h"
#include "BootNvRam.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

#define DEVICE    MODULE_FPM

#define SERIAL    0x11111111

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/



/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

static CANFramingMapStr CANMapping =
{
    GPIO_INVALID_PIN ,/* peripheralCAN     */
    GPIO_INVALID_PIN ,/* peripheralGPIO    */
    PINSEL_PORT_0    ,/* gpioPortBase      */
    PINSEL_PIN_22    ,/* CanTxPin          */
    PINSEL_FUNC_3    ,/* CanTxConfigurePin */
    PINSEL_PIN_21    ,/* CanRxPin          */
    PINSEL_FUNC_3    ,/* CanRxConfigurePin */
    LPC_CAN1_BASE    ,/* CanBase           */
   CAN_IRQn           /* CanInt            */
};

const volatile __attribute__ ((section (".exebindata"))) SlaveBinImageHeaderStr slaveBinImageHeader =
{
		0x0,  // headerCRC32 CRC32
		SLAVE_IMAGE_MAGIC_SYNC,
		0x0,  // preHeaderCRC32 CRC32
		0x0, // postHeaderCRC32 CRC32
		SLAVE_IMAGE_VERSION,
		MODULE_FPM,
		(lu_uint32_t)&__rom_size,
		(lu_uint32_t)&__rom_start, // BL Image Address
		SLAVE_BIN_ARCH_ARM_NXP_LPC1768,
		SLAVE_IMAGE_TYPE_BOOT_LOADER,
		{FPM_FEATURE_VERSION_MAJOR, FPM_FEATURE_VERSION_MINOR},
		{SLAVE_BOOTLOADER_API_MAJOR, SLAVE_BOOTLOADER_API_MINOR},
		{BOOTLOADER_SOFTWARE_VERSION_RELTYPE, {BOOTLOADER_SOFTWARE_VERSION_MAJOR, BOOTLOADER_SOFTWARE_VERSION_MINOR}, BOOTLOADER_SOFTWARE_VERSION_PATCH},
		_SVN_REVISION,
		{
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0
		}
};

/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/


/*!
*******************************************************************************
* EXPORTED:
*   \name
*******************************************************************************
* @{
*/

int main(void)
{
	lu_uint8_t       deviceID;
    SUIRQPriorityStr CANPriority;
    SUIRQPriorityStr TSynchPriority;
    SUIRQPriorityStr gpDmaPriority;
    SUIRQPriorityStr timePriority;
    SlaveBinImageHeaderStr *slaveBinImageHeaderPtr;
    SlaveBinImageHeaderStr *appBinImageHeaderPtr;
    NVRAMInfoStr *nvramInfoBlkPtr;
    SB_ERROR		  retError;
    lu_bool_t         goodAppImage;

    // Check for bootcommand
    BootloaderCheckStartCommand();

    goodAppImage = LU_FALSE;

    slaveBinImageHeaderPtr = (SlaveBinImageHeaderStr *)&slaveBinImageHeader;

#if BUILD != Release
    // Check image integrity first!!!
   	if (SlaveBinImageCheckIntegrity((lu_uint8_t *)slaveBinImageHeaderPtr->appImageAddress) != SB_ERROR_NONE)
   	{
   		// Loop forever
   		while(1) {};
   	}
#endif

    /* System specific initialisation (set PLL and CPU speed) */
    SystemInit();

    /* ********************************************************************************
     * Very Very important to identify vector table on user application area
     *
     *
     * SCB->VTOR = 0x00020000;
     **********************************************************************************
     */

    /* Enable debug serial port (COM0) */
    debug_frmwrk_init();

    /* Initialize IRQ priorites */
    CANPriority.group          = SU_IRQ_GROUP_0;
    CANPriority.subPriority    = SU_IRQ_SUB_PRIORITY_1;
    TSynchPriority.group       = SU_IRQ_GROUP_7;
    TSynchPriority.subPriority = SU_IRQ_SUB_PRIORITY_3;
    timePriority.group         = SU_IRQ_GROUP_7;
    timePriority.subPriority   = SU_IRQ_SUB_PRIORITY_0;

    gpDmaPriority.group       = SU_IRQ_GROUP_0;
    gpDmaPriority.subPriority = SU_IRQ_SUB_PRIORITY_0;
	SUSetIRQPriority(DMA_IRQn, gpDmaPriority, LU_TRUE);

    /* Initialise IRQ priority */
    SUInitIRQPriority();

    /* Initialise System Time */
    STInit(timePriority);

    /* Init SysAlarm components */
    SysAlarmInitEvents();

    /* Get device ID address */
    deviceID = BoardBootloaderGetModuleID();

    /* Set board serial number */
    SSSetSerial(SERIAL + deviceID);
    SSSetSupplierId(0);

    /* Set module status */
    SSSetBStatus(MODULE_BOARD_STATUS_BOOT_POST);

    /* Set Bootloader SVN Revision */
    SSSetBootSvnRevision(slaveBinImageHeaderPtr->svnRevsion);

    /* Set Module System API */
	SSSetBootSystemAPIMajor(slaveBinImageHeaderPtr->systemAPI.major);
	SSSetBootSystemAPIMinor(slaveBinImageHeaderPtr->systemAPI.minor);

	/* Set Module feature revision */
	SSSetFeatureRevisionMajor(slaveBinImageHeaderPtr->featureRevision.major);
	SSSetFeatureRevisionMinor(slaveBinImageHeaderPtr->featureRevision.minor);

	/* Set Module software version */
	SSSetBootSoftwareReleaseType(slaveBinImageHeaderPtr->softwareVersion.relType);
	SSSetBootSoftwareVersionMajor(slaveBinImageHeaderPtr->softwareVersion.version.major);
	SSSetBootSoftwareVersionMinor(slaveBinImageHeaderPtr->softwareVersion.version.minor);
	SSSetBootSoftwareVersionPatch(slaveBinImageHeaderPtr->softwareVersion.patch);

	appBinImageHeaderPtr = (SlaveBinImageHeaderStr *)(BRD_BL_APP_FLASH_SECTOR + SLAVE_IMAGE_BASE_OFFSET);

	/* APP software version */
	SSSetAppSoftwareReleaseType(VERSION_TYPE_UNKNOWN);
	SSSetAppSoftwareVersionMajor(0);
	SSSetAppSoftwareVersionMinor(0);
	SSSetAppSoftwareVersionPatch(0);

	if (SlaveBinImageCheckIntegrity((lu_uint8_t *)BRD_BL_APP_FLASH_SECTOR) == SB_ERROR_NONE)
	{
		switch (appBinImageHeaderPtr->imageType)
		{
		case SLAVE_IMAGE_TYPE_APPLICATION:
		case SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM:
			if (appBinImageHeaderPtr->moduleType == slaveBinImageHeaderPtr->moduleType)
			{
				/* Set Application SVN Revision */
				SSSetAppSvnRevision(appBinImageHeaderPtr->svnRevsion);

				/* APP software version */
				SSSetAppSoftwareReleaseType(appBinImageHeaderPtr->softwareVersion.relType);
				SSSetAppSoftwareVersionMajor(appBinImageHeaderPtr->softwareVersion.version.major);
				SSSetAppSoftwareVersionMinor(appBinImageHeaderPtr->softwareVersion.version.minor);
				SSSetAppSoftwareVersionPatch(appBinImageHeaderPtr->softwareVersion.patch);

				SSSetAppSystemAPIMajor(appBinImageHeaderPtr->systemAPI.major);
				SSSetAppSystemAPIMinor(appBinImageHeaderPtr->systemAPI.minor);

				/* Now allow Application image to auto-start */
				BoardBootloaderAutoAppStart();

				goodAppImage = LU_TRUE;
			}
			break;

		default:
			break;
		}
	}

	if (goodAppImage == LU_FALSE)
	{
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_APP_INVALID_IMAGE_HED,
							0
						   );
	}

	/* Read Board Identity NVRAM */
	retError = BootNvRamGetIdentityInfoMemoryPtr((lu_uint8_t **)&nvramInfoBlkPtr);

	if (retError != SB_ERROR_NONE)
	{
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_WARNING,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_PRIMARY_FACTORY_NVRAM,
							0
						   );

		retError = BootNvRamGetApplicationInfoMemoryPtr((lu_uint8_t **)&nvramInfoBlkPtr);
	}

	if (retError != SB_ERROR_NONE)
	{
		/* Major error - no factory info is available */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_FACTORY_NVRAM,
							0
						   );
	}
	else
	{
		/* Set serial no / , etc */
		SSSetSerial(nvramInfoBlkPtr->serialNumber);
		SSSetSupplierIdStr(nvramInfoBlkPtr->supplierId);

		/* Set feature revision */
		SSSetFeatureRevisionMajor(nvramInfoBlkPtr->moduleFeatureMajor);
		SSSetFeatureRevisionMinor(nvramInfoBlkPtr->moduleFeatureMinor);
	}

    /* Initialise CAN Codec */
    CANCodecInit( SystemCoreClock,
    			  CAN_BUS_BAUDRATE,
                  DEVICE,
                  deviceID,
                  &CANMapping,
                  CANPriority,
                  TSynchPriority,
                  BoardCANProtocolDecoder
                );

    /* Register CAN decoder */
    SysAlarmInitCanDecoder();

   	/* Initialisation routines */
   	FactoryTestInit();

   	BootloaderInit();

   	/* Perform POST test */
   	BoardBootloaderPOST();

   	/* Set module status */
   	SSSetBStatus(MODULE_BOARD_STATUS_BOOT_READY);

   	/* Call scheduler executive */
    BoardStatusManagerExecutive();

    return 0;
}

/*!
*******************************************************************************
* @}
*/


/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/


/*!
*******************************************************************************
* @}
*/


/*
*********************** End of file *******************************************
*/
