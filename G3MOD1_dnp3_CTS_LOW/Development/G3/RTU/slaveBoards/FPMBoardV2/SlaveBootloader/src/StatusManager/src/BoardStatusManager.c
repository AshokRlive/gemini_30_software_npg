/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"
#include "BoardBootloader.h"

#include "FactoryTest.h"
#include "FactoryTestSPIADE78xx.h"
#include "FactoryTestI2CADE78xx.h"

#include "ATEFactoryTest.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR ReadISUM(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

TestSpiADE78xxReadRegStr ade78xxReadISum = { 1, 1,10, 0x43BF, 4 , 0};

TestSpiADE78xxInitStr ade78xxSpiInitParam  = {  1, 1, 10,
											{
												{1, 14},  // PM0
												{1, 15},  // PM1
												{1, 16},  // RST
												{0, 23},  // IRQ0
												{0, 25},  // IRQ1
												{1,  4},  // CF1
												{1,  8},  // CF2
											}
										  };

/* FPI A */
TestI2cADE78xxInitStr ade78xxAI2cInitParam  = {  2, 0, 0, 16,
											  {
												{1, 14 }, // PM0
												{1, 15 }, // PM1
												{1, 16 }, // RST
												{0, 23},  // IRQ0
												{0, 24},  // IRQ1
												{1, 4},   // CF1
												{1, 8}    // CF2
											  },
											  1
										     };

/* FPI B */
TestI2cADE78xxInitStr ade78xxBI2cInitParam  = {  1, 1, 0, 6,
											  {
												{2, 8 },  // PM0
												{2, 0 },  // PM1
												{2, 1 },  // RST
												{0, 25},  // IRQ0
												{0, 26},  // IRQ1
												{1, 24},  // CF1
												{0,  4}  // CF2
											  },
											  1
										     };



static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				    period(ms)        */
	{CANCDecode, 				 		    0, 					0 			  }, // Run all the time
	{ATEFactoryTestTick, 				    0, 					0			  }, // Run all the time

	//{ReadISUM, 				     	    0, 					0			  }, // Run all the time

	//{FactoryTestADE78xxDMASoftTrigger, 	0,					0			  },

	{CANFramingTick, 			 		0, 				CAN_TICK_MS		      }, // Run every 10ms

	{BoardBootloaderTick,        		0,             BRD_BL_TICK_MS         },

	{(SMThreadRun)0L, 	    	 		0, 					0   			  }  // end of table
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{

	//FactoryTestI2CADE78xxEnable(&ade78xxAI2cInitParam);
	//FactoryTestI2CADE78xxEnable(&ade78xxBI2cInitParam);

	 /*
	  ConfigureSPIADE78xx(ade78xxInitParam.sspBus,
						  ade78xxInitParam.csPort,
						  ade78xxInitParam.csPin,
						  0xF8813AF,
						  0xF8809FF,
						  0xF880EFF
			             );
	 */

	/*
	FactoryTestI2CADE78xxConfigure(  ade78xxAI2cInitParam.i2cChan,
									 ade78xxAI2cInitParam.sspBus,
									 ade78xxAI2cInitParam.csPort,
									 ade78xxAI2cInitParam.csPin,
									 0xF8813AF,
									 0xF8809FF,
									 0xF880EFF
   				       	   	   	   );
   	*/

	/* Enable ATE factory test cmd api */
	ATEFactoryTestInit();

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

SB_ERROR ReadISUM(void)
{
	return FactoryTestSPIADE78xxReadReg(&ade78xxReadISum);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
