/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"

#include "lu_types.h"

#include "systemTime.h"
#include "systemStatus.h"
#include "Bootloader.h"
#include "BoardBootloader.h"
#include "ModuleProtocol.h"
#include "FactoryTest.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define BL_FPM_PORT_FACTORY							2
#define BL_FPM_PIN_FACTORY							(1 << 12)

#define BL_FPM_PORT_MOD_SEL_A							3
#define BL_FPM_PORT_MOD_SEL_B							0
#define BL_FPM_PIN_MOD_SEL1							(1 << 26)
#define BL_FPM_PIN_MOD_SEL2							(1 << 25)
#define BL_FPM_PIN_MOD_SEL3							(1 << 29)
#define BL_FPM_PIN_MOD_SEL4							(1 << 30)

#define BL_FPM_PORT_LED_CTRL_EXTINGUISH				0
#define BL_FPM_PIN_LED_CTRL_EXTINGUISH				(1 << 0)

#define BL_FPM_PORT_LED_CTRL_OK_GREEN				1
#define BL_FPM_PIN_LED_CTRL_OK_GREEN				(1 << 26)

#define BL_FPM_PORT_LED_CTRL_OK_RED					1
#define BL_FPM_PIN_LED_CTRL_OK_RED					(1 << 27)

#define BL_FPM_PORT_LED_CTRL_CAN_GREEN				1
#define BL_FPM_PIN_LED_CTRL_CAN_GREEN				(1 << 28)

#define BL_FPM_PORT_LED_CTRL_CAN_RED				1
#define BL_FPM_PIN_LED_CTRL_CAN_RED					(1 << 29)

#define BL_FPM_I2CEXP_I2C_CHAN						0
#define BL_FPM_I2CEXP_I2C_ADDR						0x00


#define BL_REQ_APP_START_TIMEOUT_MS					100 // Timeout to start app

/* HW watch dog pin definitions */
#define BL_FPM_PORT_WATCHDOG_KICK					1
#define BL_FPM_PIN_WATCHDOG_KICK					(1 << 19)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR FactoryTestI2cExpWriteDDR(TestI2cExpDDRWriteStr *i2cExpParamsPtr, lu_bool_t sendRsp);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_uint32_t startAppTimeout;
static lu_uint32_t autoStartAppTimeout;
static lu_uint32_t ledFlashTimeout , watchdogKickTimeout;
static lu_bool_t   autoStartEnable = LU_FALSE;
static lu_bool_t   factoryMode = LU_FALSE;
static lu_bool_t   bootloaderEnable = LU_TRUE;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardBootloaderAutoAppStart(void)
{
	autoStartEnable     = LU_TRUE;
}

lu_uint8_t BoardBootloaderGetModuleID(void)
{
	lu_uint32_t valPort;
	lu_uint32_t valSel1;
	lu_uint32_t valSel2;
	lu_uint32_t valSel3;
	lu_uint32_t valSel4;

	PINSEL_CFG_Type pinCfg;

	pinCfg.Funcnum    = 0;
	pinCfg.Portnum    = BL_FPM_PORT_MOD_SEL_A;
	pinCfg.Pinnum     = 26;
	pinCfg.OpenDrain  = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode    = PINSEL_PINMODE_PULLUP;
	PINSEL_ConfigPin(&pinCfg);

	pinCfg.Pinnum = 27;
	PINSEL_ConfigPin(&pinCfg);

	pinCfg.Portnum    = BL_FPM_PORT_MOD_SEL_B;
	pinCfg.Pinnum = 29;
	PINSEL_ConfigPin(&pinCfg);

	pinCfg.Pinnum = 30;
	PINSEL_ConfigPin(&pinCfg);


	/* Read IO pins */
	valPort = GPIO_ReadValue(BL_FPM_PORT_MOD_SEL_A);
	valSel1 = (valPort & BL_FPM_PIN_MOD_SEL1) ? 1 : 0;
	valSel2 = (valPort & BL_FPM_PIN_MOD_SEL2) ? 1 : 0;
	valPort = GPIO_ReadValue(BL_FPM_PORT_MOD_SEL_B);
	valSel3 = (valPort & BL_FPM_PIN_MOD_SEL3) ? 1 : 0;
	valSel4 = (valPort & BL_FPM_PIN_MOD_SEL4) ? 1 : 0;

	return BootloaderCalcModuleID(valSel1, valSel2, valSel3, valSel4);
}

SB_ERROR BoardBootloaderPOST(void)
{
	lu_uint32_t         portValue;
	PINSEL_CFG_Type 	pinCfg;

	pinCfg.Funcnum = 0;
	pinCfg.Portnum = BL_FPM_PORT_FACTORY;
	pinCfg.Pinnum = BL_FPM_PIN_FACTORY;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

	PINSEL_ConfigPin(&pinCfg);

	/* POST test is success... */
	GPIO_SetValue(BL_FPM_PORT_LED_CTRL_OK_GREEN, BL_FPM_PIN_LED_CTRL_OK_GREEN);

	/* Sample factory input pin here */
	portValue = GPIO_ReadValue(BL_FPM_PORT_FACTORY);
	if (!(portValue & BL_FPM_PIN_FACTORY))
	{
		autoStartAppTimeout = 0;
		autoStartEnable = LU_FALSE;
		factoryMode = LU_TRUE;
	}

	return SB_ERROR_NONE;
}

SB_ERROR BoardBootloaderTick(void)
{
	static lu_bool_t  ledState, watchdogPinState;

	if (bootloaderEnable == LU_TRUE)
	{
		if (BootloaderGetStartApplicationNow())
		{
			/* Count-up until ready to start App, allow time to send CAN message reply */
			autoStartAppTimeout = 0;
			startAppTimeout += BRD_BL_TICK_MS;
		}
		else
		{
			/* Count-up to autostart, to start charging battery!! */
			autoStartAppTimeout += BRD_BL_TICK_MS;
		}

		/* Start Application if timeout reached */
		if (startAppTimeout >= BL_REQ_APP_START_TIMEOUT_MS ||
			(autoStartEnable && autoStartAppTimeout >= BOOTLOADER_APP_START_TIMEOUT_MS))
		{
			/* Init for next trigger should there be no application available ?*/
			autoStartAppTimeout = 0;
			startAppTimeout = 0;
			autoStartEnable = LU_FALSE;

			/* Clear Green / Set red before launch app */
			GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_OK_GREEN, BL_FPM_PIN_LED_CTRL_OK_GREEN);
			GPIO_SetValue(BL_FPM_PORT_LED_CTRL_OK_RED, BL_FPM_PIN_LED_CTRL_OK_RED);

			/* Attempt to run Application */
			BootloaderStartCommand();

			/* Should never get here!! */
		}

		/* Check if forced to stay in bootloader */
		if (SSGetBootloaderMode() == LU_TRUE)
		{
			autoStartAppTimeout = 0;
			autoStartEnable = LU_FALSE;

			if (SSGetFactoryMode() == LU_TRUE)
			{
				factoryMode = LU_TRUE;
			}
		}

		ledFlashTimeout += BRD_BL_TICK_MS;

		/* Flash OK LED Red <-> Green while in bootloader */
		if (ledFlashTimeout >= BOOTLOADER_LED_FLASH_MS)
		{
			ledFlashTimeout = 0;

			/* Do not allow LED control when in factory test mode */
			if (factoryMode == LU_FALSE)
			{
				if (ledState)
				{
					/* Turn on RED */
					ledState = LU_FALSE;
					GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_OK_RED, BL_FPM_PIN_LED_CTRL_OK_RED);
					GPIO_SetValue(BL_FPM_PORT_LED_CTRL_OK_GREEN, BL_FPM_PIN_LED_CTRL_OK_GREEN);
				}
				else
				{
					/* Turn on Green */
					ledState = LU_TRUE;
					GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_OK_GREEN, BL_FPM_PIN_LED_CTRL_OK_GREEN);
					GPIO_SetValue(BL_FPM_PORT_LED_CTRL_OK_RED, BL_FPM_PIN_LED_CTRL_OK_RED);
				}
			}
		}

		watchdogKickTimeout += BRD_BL_TICK_MS;

		/* Kick HW Watchdog while in bootloader */
		if (watchdogKickTimeout >= BOOTLOADER_WATCHDOG_KICK_MS)
		{
			watchdogKickTimeout = 0;

			if (watchdogPinState)
			{
				/* Turn on RED */
				watchdogPinState = LU_FALSE;
				GPIO_SetValue(BL_FPM_PORT_WATCHDOG_KICK, BL_FPM_PIN_WATCHDOG_KICK);

			}
			else
			{
				/* Turn on Green */
				watchdogPinState = LU_TRUE;
				GPIO_ClearValue(BL_FPM_PORT_WATCHDOG_KICK, BL_FPM_PIN_WATCHDOG_KICK);
			}
		}
	}
	return SB_ERROR_NONE;
}

SB_ERROR BoardBootloaderDisable(void)
{
	bootloaderEnable = LU_FALSE;

	/* Disable CAN */

	/* Re-init all DDR / func code to gpio inputs, except UART3 */

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Board bootloader initialisation
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR BoardBootloaderInit(void)
{
	TestI2cExpDDRWriteStr testI2cExpDDRWrite;

	startAppTimeout     = 0;
	autoStartAppTimeout = 0;

	/* Configure GPIO pins */
	/* Set LED status... */

	/* Set as output */
	GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_EXTINGUISH, BL_FPM_PIN_LED_CTRL_EXTINGUISH);
	GPIO_SetDir(BL_FPM_PORT_LED_CTRL_EXTINGUISH, BL_FPM_PIN_LED_CTRL_EXTINGUISH, 1);

	/* Set as output */
	GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_OK_GREEN, BL_FPM_PIN_LED_CTRL_OK_GREEN);
	GPIO_SetDir(BL_FPM_PORT_LED_CTRL_OK_GREEN, BL_FPM_PIN_LED_CTRL_OK_GREEN, 1);

	/* Set as output */
	GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_OK_RED, BL_FPM_PIN_LED_CTRL_OK_RED);
	GPIO_SetDir(BL_FPM_PORT_LED_CTRL_OK_RED, BL_FPM_PIN_LED_CTRL_OK_RED, 1);

	/* Set as output */
	GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_CAN_GREEN, BL_FPM_PIN_LED_CTRL_CAN_GREEN);
	GPIO_SetDir(BL_FPM_PORT_LED_CTRL_CAN_GREEN, BL_FPM_PIN_LED_CTRL_CAN_GREEN, 1);

	/* Set as output */
	GPIO_ClearValue(BL_FPM_PORT_LED_CTRL_CAN_RED, BL_FPM_PIN_LED_CTRL_CAN_RED);
	GPIO_SetDir(BL_FPM_PORT_LED_CTRL_CAN_RED, BL_FPM_PIN_LED_CTRL_CAN_RED, 1);

	/* Set as output */
	GPIO_ClearValue(BL_FPM_PORT_WATCHDOG_KICK, BL_FPM_PIN_WATCHDOG_KICK);
	GPIO_SetDir(BL_FPM_PORT_WATCHDOG_KICK, BL_FPM_PIN_WATCHDOG_KICK, 1);

	/* Initialise I2C expanders to all inputs */
	testI2cExpDDRWrite.addr.i2cChan = BL_FPM_I2CEXP_I2C_CHAN; // I2C Bus 2
	testI2cExpDDRWrite.addr.addr    = BL_FPM_I2CEXP_I2C_ADDR;
	testI2cExpDDRWrite.value        = 0xffff;
	FactoryTestI2cExpWriteDDR(&testI2cExpDDRWrite, LU_FALSE);

	return SB_ERROR_NONE;
}


/*
 *********************** End of file ******************************************
 */
