/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "NVRam.h"
#include "IOManager.h"
#include "IOManagerIOUpdate.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "IOManagerExtAnalogIOUpdate.h"
#include "IOManagerI2CExtAdcMCP342XOUpdate.h"
#include "IOManagerExtADE78xxInit.h"
#include "IOManagerI2CIOUpdate.h"
#include "IOManagerI2CExtTempIOUpdate.h"
#include "BoardIO.h"
#include "BoardIOMap.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmStatusManagerEnum.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

#include "BoardFPI.h"

#include "Calibration.h"
#include "systemStatus.h"

#include "systemTime.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
#define FPM_LED_MANAGER_MS 100

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR FPMPeriodicManager(void);
SB_ERROR FPMPowerSaveMode(lu_bool_t powerSaveMode);
void CheckIDSwitchChange(void);

void FPMHealthMonitor(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				  0, 				0 				          }, // Run all the time

	{CANFramingTick, 			  0, 				CAN_TICK_MS		          }, // Run every 10ms

	{IOManagerIORunUpdateGPIO, 	  0, 				IOMAN_UPDATE_GPIO_MS      }, // run every 10ms
	{IOManagerIntAnalogIOUpdate,  0, 				IOMAN_UPDATE_INAI_MS      }, // run every 15 ms
	{IOManagerExtAnalogIOUpdate,  0, 				IOMAN_UPDATE_EXT_AI_MS    },
	{IOManagerI2CExtAdcMCP342XIOUpdate, 0, 			IOMAN_UPDATE_I2CEXTADC_MCP342XIO_MS},
	{IOManagerI2CExtTempIOUpdate, 0, 				IOMAN_UPDATE_I2CEXTMPIO_MS}, // run every 100 ms
	{IOManagerIOAIEventUpdate,    0,                 IOMAN_UPDATE_INAI_EVENT_MS}, //run every 50 ms

	{NVRamUpdateTick, 		      0, 				NVRAM_UPDATE_TICK_MS 	  }, // run every 100ms

	{FPMPeriodicManager,          0,                 200                       }, // run every 1000ms

	{(SMThreadRun)0L, 	    	  0, 				0   			          }  // end of table
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{
	/* Init Dual FPI CAN decoder */
	BoardFPIInit();

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FPMPowerSaveMode(lu_bool_t powerSaveMode)
{
	SB_ERROR retError;
	MODULE_BOARD_ERROR  moduleError;
	static MODULE_BOARD_ERROR oldModuleError = MODULE_BOARD_ERROR_TSYNCH;

	/* Turn off All LED's under software control */
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_OK_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_OK_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_CAN_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_CAN_RED, powerSaveMode);

	retError = IOManagerForceOutputOff(IO_ID_LED_FPI1, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_EFI1, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_FPI2, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_EFI2, powerSaveMode);

	/* Extinguish LED's under hardware control / gated by software */
	retError = IOManagerSetValue(IO_ID_LED_CTRL_EXTINGUISH, powerSaveMode ? 1L : 0L);

	/* Update status LED's */
	moduleError = SSGetBError();

	if (oldModuleError != moduleError)
	{
		oldModuleError = moduleError;

		if (moduleError & MODULE_BOARD_ERROR_ALARM_CRITICAL)
		{
			/* Critical - Fast Flash Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 250);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_ERROR)
		{
			/* Error - Slow Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 1000);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_WARNING)
		{
			/* Warning - Fast Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 250);
		} else
		{
			/* Normal - Slow Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 1000);
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
void CheckIDSwitchChange(void)
{
	lu_uint8_t idSwitchNew;
	static lu_uint8_t idSwitch = 10;

	/* Read the ID Switch */
	boardIOGetModuleID(&idSwitchNew);

	if (idSwitch != idSwitchNew)
	{
		idSwitch = idSwitchNew;

		/* Compare ID switch with the ID the CAN driver is using */
		if (idSwitch != CANCGetDeviceID())
		{
			/* Force card to go offline */
			SSSetBStatus(MODULE_BOARD_STATUS_ERROR);

			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_CRITICAL,
								SYS_ALARM_SUBSYSTEM_SYSTEM,
								SYSALC_SYSTEM_ID_SWITCH,
								(lu_uint16_t)idSwitchNew
								);
		}
		else
		{
			SysAlarmSetLogEvent(LU_FALSE,
								SYS_ALARM_SEVERITY_CRITICAL,
								SYS_ALARM_SUBSYSTEM_SYSTEM,
								SYSALC_SYSTEM_ID_SWITCH,
								0
								);
		}
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

SB_ERROR FPMPeriodicManager(void)
{
	SB_ERROR RetVal;
	lu_bool_t powerSaveMode;

	RetVal = SB_ERROR_NONE;

	/* Check if ID switch mis-match */
	CheckIDSwitchChange();

	/* Get state of hardware and write to LEDs  */

    /* Update module status - temporary workaround */
    if(SSGetBStatus() == MODULE_BOARD_STATUS_STARTING)
    {
    	/* Allow the IOManager to report events */
		IOManagerStartEventing();

		/* Start Digital Output Controller subsystem */

        SSSetBStatus(MODULE_BOARD_STATUS_ON_LINE);
    }

    /* Power saving mode check - LED off */
   	powerSaveMode = SSGetPowerSaveMode();
   	FPMPowerSaveMode(powerSaveMode);

	/*
	 * Monitor the handshake and system healthy signals
	 * from the hardware watchdog PIC
	 */
    FPMHealthMonitor();

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FPMHealthMonitor(void)
{
	SB_ERROR 	retError;
	static	lu_bool_t initialised 		 = LU_FALSE;
	static	lu_int32_t lastHandshake 	 = 0;
	static	lu_int32_t lastSystemHealthy = 0;
	static 	lu_int32_t lastHandshakeTime = 0;
	static	lu_bool_t handshakeAlarmSet	 = LU_FALSE;
	static	lu_bool_t sysHealthyAlarmSet = LU_FALSE;

	lu_int32_t 	wdhandshake;
	lu_int32_t 	sysHealthy;
	lu_int32_t 	time;
	lu_int32_t 	elapsedTime;

	/*
	 * Get current time
	 */
	time = STGetTime();

	/*
	 * If this is the first execution of the function then
	 * initialise the times and outputs
	 */
	if( initialised == LU_FALSE )
	{
		/* Initialisation - Wait for handshake to go low */
		retError = IOManagerGetValue(IO_ID_WD_HANDSHAKE, &wdhandshake);
		retError = IOManagerGetValue(IO_ID_SYS_HEALTHY, &sysHealthy);
//		if( wdhandshake == 0 )
		{
			/* Start kicking the PIC watch dog every second */
			retError = IOManagerSetFlash(IO_ID_HWDOG_1HZ_KICK, ONE_HZ_KICK_PERIOD);
			lastHandshake = 0;
			lastSystemHealthy = sysHealthy;

			/* Initialise the timer for the handshake */
			lastHandshakeTime = time;

			initialised = LU_TRUE;
			return;
		}
	}

	/*
	 * Get time since last handshake
	 */
	elapsedTime = STElapsedTime( lastHandshakeTime , time );

	/*
	 * Read the state of the handshake signal from the PIC
	 */
	retError = IOManagerGetValue(IO_ID_WD_HANDSHAKE, &wdhandshake);

	/*
	 * Check for a change of state in the handshake signal
	 */
	if ( lastHandshake != wdhandshake )
	{
		/*
		 * A change of state has been detected so reset timer and handshake state
		 */
		lastHandshake = wdhandshake;
		lastHandshakeTime = time;
		/* Clear alarm if already set */
		if( handshakeAlarmSet == LU_TRUE )
		{
			SysAlarmSetLogEvent(LU_FALSE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
								SYSALC_STATUS_SYSTEM_WD_HANDSHAKE,
								0
							   );
			handshakeAlarmSet = LU_FALSE;
		}
	}
	else if( elapsedTime > PIC_HANDSHAKE_ERROR_PERIOD )
	{
		/*
		 * A change of state of the handshake signal has not been received
		 * so set the alarm if it hasn't been set already
		 */
		if( handshakeAlarmSet == LU_FALSE )
		{
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
								SYSALC_STATUS_SYSTEM_WD_HANDSHAKE,
								0
							   );
			handshakeAlarmSet = LU_TRUE;
		}

	}

	/*
	 * Read the state of the system healthy signal from the PIC
	 */
	retError = IOManagerGetValue(IO_ID_SYS_HEALTHY, &sysHealthy);

	/*
	 * Set the alarm if the system healthy signal goes low
	 * Clear the alarm if it goes high
	 */
	if( sysHealthy != lastSystemHealthy )
	{
		lastSystemHealthy = sysHealthy;
		if( sysHealthy == 0 )
		{
			/* Set the alarm if not already set*/
			if( sysHealthyAlarmSet == LU_FALSE )
			{
				SysAlarmSetLogEvent(LU_TRUE,
									SYS_ALARM_SEVERITY_CRITICAL,
									SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
									SYSALC_STATUS_SYSTEM_HEALTH,
									0
								   );
				sysHealthyAlarmSet = LU_TRUE;
			}
		}
		else
		{
			/* Clear the alarm if already set */
			if( sysHealthyAlarmSet == LU_TRUE )
			{
				SysAlarmSetLogEvent(LU_FALSE,
									SYS_ALARM_SEVERITY_CRITICAL,
									SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
									SYSALC_STATUS_SYSTEM_HEALTH,
									0
								   );
				sysHealthyAlarmSet = LU_FALSE;
			}
		}
	}

}


/*
 *********************** End of file ******************************************
 */
