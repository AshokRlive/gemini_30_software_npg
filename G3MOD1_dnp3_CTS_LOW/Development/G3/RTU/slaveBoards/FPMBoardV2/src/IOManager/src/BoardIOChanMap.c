/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"


/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_COMMON_FAULT,					    IO_ID_FPI1_COMMON_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_FAULT, 			            IO_ID_FPI1_PHASE_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_INST_PHASE_FAULT, 			        IO_ID_FPI1_INST_PHASE_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_EARTH_FAULT, 					    IO_ID_FPI1_EARTH_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_INST_EARTH_FAULT, 		   	        IO_ID_FPI1_INST_EARTH_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_COMMON_FAULT, 						IO_ID_FPI2_COMMON_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_FAULT, 			            IO_ID_FPI2_PHASE_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_INST_PHASE_FAULT, 			        IO_ID_FPI2_INST_PHASE_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_EARTH_FAULT, 					    IO_ID_FPI2_EARTH_FAULT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_INST_EARTH_FAULT, 			        IO_ID_FPI2_INST_EARTH_FAULT),

	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_TEST_ACTIVE, 			            IO_ID_FPI1_TEST_ACTIVE),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_TEST_ACTIVE, 			            IO_ID_FPI2_TEST_ACTIVE),

	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_SENS_EARTH_FAULT, 			        IO_ID_FPI1_SENS_EARTH_FAULT),

	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_A_CURRENT_PRESENT,           IO_ID_FPI1_PHASE_A_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_B_CURRENT_PRESENT,           IO_ID_FPI1_PHASE_B_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_C_CURRENT_PRESENT,           IO_ID_FPI1_PHASE_C_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_COMMON_PHASE_CURRENT_PRESENT,      IO_ID_FPI1_COMMON_PHASE_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_A_CURRENT_ABSENT,            IO_ID_FPI1_PHASE_A_CURRENT_ABSENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_B_CURRENT_ABSENT,            IO_ID_FPI1_PHASE_B_CURRENT_ABSENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_PHASE_C_CURRENT_ABSENT,            IO_ID_FPI1_PHASE_C_CURRENT_ABSENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI1_COMMON_PHASE_CURRENT_ABSENT,       IO_ID_FPI1_COMMON_PHASE_CURRENT_ABSENT),

	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_SENS_EARTH_FAULT, 			        IO_ID_FPI2_SENS_EARTH_FAULT),

	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_A_CURRENT_PRESENT,           IO_ID_FPI2_PHASE_A_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_B_CURRENT_PRESENT,           IO_ID_FPI2_PHASE_B_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_C_CURRENT_PRESENT,           IO_ID_FPI2_PHASE_C_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_COMMON_PHASE_CURRENT_PRESENT,      IO_ID_FPI2_COMMON_PHASE_CURRENT_PRESENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_A_CURRENT_ABSENT,            IO_ID_FPI2_PHASE_A_CURRENT_ABSENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_B_CURRENT_ABSENT,            IO_ID_FPI2_PHASE_B_CURRENT_ABSENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_PHASE_C_CURRENT_ABSENT,            IO_ID_FPI2_PHASE_C_CURRENT_ABSENT),
	IOM_IOCHAN_DI(FPM_CH_DINPUT_FPI2_COMMON_PHASE_CURRENT_ABSENT,       IO_ID_FPI2_COMMON_PHASE_CURRENT_ABSENT),

	IOM_IOCHAN_DI(0, 						    IO_ID_NA)
	};


IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),

	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};


IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */

	IOM_IOCHAN_AI(FPM_CH_AINPUT_5V_MEAS, 							IO_ID_5V_MEAS, \
			CAL_ID_5V_MEAS,    						IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_3_3V_MEAS, 								IO_ID_3_3V_MEAS, \
			CAL_ID_3_3V_MEAS,		   				IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_TEMPERATURE, 						IO_ID_TEMPERATURE, \
			CAL_ID_NA,								IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(FPM_CH_AINPUT_A1, 								IO_ID_AINPUT_A1, \
			CAL_ID_AINPUT_A1,						IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_A2, 								IO_ID_AINPUT_A2, \
			CAL_ID_AINPUT_A1,						IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_A3, 								IO_ID_AINPUT_A3, \
			CAL_ID_AINPUT_A1,						IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_24V_ISO_MEAS, 						IO_ID_AINPUT_24V_ISO_MEAS, \
			CAL_ID_24V_ISO_MEAS,					IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI1_PHASE_A_CURRENT,               IO_ID_FPI1_PHASE_A_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI1_PHASE_B_CURRENT,               IO_ID_FPI1_PHASE_B_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI1_PHASE_C_CURRENT,               IO_ID_FPI1_PHASE_C_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI1_PHASE_N_CURRENT,               IO_ID_FPI1_PHASE_N_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI2_PHASE_A_CURRENT,               IO_ID_FPI2_PHASE_A_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI2_PHASE_B_CURRENT,               IO_ID_FPI2_PHASE_B_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI2_PHASE_C_CURRENT,               IO_ID_FPI2_PHASE_C_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FPM_CH_AINPUT_FPI2_PHASE_N_CURRENT,               IO_ID_FPI2_PHASE_N_CURRENT, \
			CAL_ID_NA,                              IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(0, 		                 						IO_ID_NA, \
			CAL_ID_NA,    0)
};


IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA),

	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
