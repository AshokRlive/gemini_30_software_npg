/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ADE78xx.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_RX, 	0)

	IOM_GPIO_PERIPH(IO_ID_IADC_A_SSP_CLK,	 	 	 	 PINSEL_PORT_0,  PINSEL_PIN_15,   \
			FUNC_SSP0_SCK, 		GPIO_PM_ADC_CH_A)
	IOM_GPIO_PERIPH(IO_ID_IADC_A_SSP_DOUT,		     PINSEL_PORT_0,  PINSEL_PIN_17,   \
			FUNC_SSP0_MISO, 	GPIO_PM_ADC_CH_A)
	IOM_GPIO_PERIPH(IO_ID_IADC_A_SSP_DIN,		     	 PINSEL_PORT_0,  PINSEL_PIN_18,   \
			FUNC_SSP0_MOSI, 	GPIO_PM_ADC_CH_A)

	IOM_GPIO_PERIPH(IO_ID_IADC_B_SSP_CLK,	 	 	 	 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		GPIO_PM_ADC_CH_B)
	IOM_GPIO_PERIPH(IO_ID_IADC_B_SSP_DOUT,		     PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	GPIO_PM_ADC_CH_B)
	IOM_GPIO_PERIPH(IO_ID_IADC_B_SSP_DIN,		     	 PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	GPIO_PM_ADC_CH_B)

	IOM_GPIO_PERIPH(IO_ID_I2C_SCL,				 PINSEL_PORT_0,  PINSEL_PIN_28,  \
			FUNC_I2C_0_SCL,   	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_27,  \
			FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C2_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_11,  \
			FUNC_I2C_2_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C2_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_10,  \
			FUNC_I2C_2_SDA, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   		GPIO_PM_PULL_UP)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_19,   0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
					PINSEL_PORT_2,  PINSEL_PIN_9,  0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   		0)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    		GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_FPI1, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_22,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_EFI1, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_25,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_FPI2, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_20,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_EFI2, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_21,    		GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_INPUT( IO_ID_MOD_SEL1, 			   	IO_CH_NA, \
				PINSEL_PORT_3,  PINSEL_PIN_26,   (GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL2, 			   	IO_CH_NA, \
				PINSEL_PORT_3,  PINSEL_PIN_25,   (GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL3, 			   	IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_29,   (GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL4, 			   	IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_30,   (GPIO_PM_INPUT_INVERT))

	IOM_GPIO_OUTPUT( IO_ID_IADCA_PM0, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_14,   (GPIO_PM_ADC_PM0 | GPIO_PM_ADC_CH_A))
	IOM_GPIO_OUTPUT( IO_ID_IADCA_PM1, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_15,   (GPIO_PM_ADC_PM1 | GPIO_PM_ADC_CH_A))
	IOM_GPIO_OUTPUT( IO_ID_IADCA_RST,				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_16,   (GPIO_PM_ADC_RESET | GPIO_PM_OUTPUT_INVERT | GPIO_PM_ADC_CH_A))

	IOM_GPIO_INPUT( IO_ID_IADCA_CFG1, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_4,   (GPIO_PM_ADC_CFG1 | GPIO_PM_ADC_CH_A ))
	IOM_GPIO_INPUT( IO_ID_IADCA_CFG2, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_8,   (GPIO_PM_ADC_CFG2 | GPIO_PM_ADC_CH_A ))

	IOM_GPIO_INPUT( IO_ID_IADCA_IRQ0, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_23,  (GPIO_PM_ADC_IRQ0 | GPIO_PM_ADC_CH_A | GPIO_PM_OUTPUT_LOW))
	IOM_GPIO_INPUT( IO_ID_IADCA_IRQ1, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_24,  (GPIO_PM_ADC_IRQ1 | GPIO_PM_ADC_CH_A | GPIO_PM_OUTPUT_LOW))

	IOM_GPIO_OUTPUT( IO_ID_IADCB_PM0, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_8,   (GPIO_PM_ADC_PM0 | GPIO_PM_ADC_CH_B))
	IOM_GPIO_OUTPUT( IO_ID_IADCB_PM1, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_0,   (GPIO_PM_ADC_PM1 | GPIO_PM_ADC_CH_B))
	IOM_GPIO_OUTPUT( IO_ID_IADCB_RST,				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_1,   (GPIO_PM_ADC_RESET | GPIO_PM_OUTPUT_INVERT | GPIO_PM_ADC_CH_B))

	IOM_GPIO_INPUT( IO_ID_IADCB_CFG1, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_24,   (GPIO_PM_ADC_CFG1 | GPIO_PM_ADC_CH_B ))
	IOM_GPIO_INPUT( IO_ID_IADCB_CFG2, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_4,   (GPIO_PM_ADC_CFG2 | GPIO_PM_ADC_CH_B ))

	IOM_GPIO_INPUT( IO_ID_IADCB_IRQ0, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_25,  (GPIO_PM_ADC_IRQ0 | GPIO_PM_ADC_CH_B | GPIO_PM_OUTPUT_LOW))
	IOM_GPIO_INPUT( IO_ID_IADCB_IRQ1, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_26,  (GPIO_PM_ADC_IRQ1 | GPIO_PM_ADC_CH_B | GPIO_PM_OUTPUT_LOW))

	IOM_GPIO_INPUT( IO_ID_IADCA_CFG3, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_9,   (0                | GPIO_PM_ADC_CH_A ))
	IOM_GPIO_INPUT( IO_ID_IADCB_CFG3, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_5,   (0                | GPIO_PM_ADC_CH_B ))

	IOM_GPIO_PERIPH(IO_ID_IADCA_CS,	 PINSEL_PORT_0,  PINSEL_PIN_16, \
			        FUNC_SSP0_SEL,    GPIO_PM_ADC_CH_A)

	IOM_GPIO_PERIPH(IO_ID_IADCB_CS,	 PINSEL_PORT_0,  PINSEL_PIN_6,   \
					FUNC_SSP1_SEL, 	GPIO_PM_ADC_CH_B)

	IOM_I2C_ADE7854(IO_ID_I2C_ADE7854_A, IO_BUS_I2C_2, GPIO_PM_ADC_CH_A)
	IOM_HSDC_ADE7854(IO_ID_SPI_ADE7854_A, IO_BUS_SPI_SSPI_0, GPIO_PM_ADC_CH_A)

	IOM_I2C_ADE7854(IO_ID_I2C_ADE7854_B, IO_BUS_I2C_1, GPIO_PM_ADC_CH_B)
	IOM_HSDC_ADE7854(IO_ID_SPI_ADE7854_B, IO_BUS_SPI_SSPI_1, GPIO_PM_ADC_CH_B)

	IOM_PERIPH_AIN( IO_ID_5V_MEAS, 		FPM_CH_AINPUT_5V_MEAS, \
				PINSEL_PORT_1,  PINSEL_PIN_30,  FUNC_AD0_CH_4)
	IOM_PERIPH_AIN( IO_ID_3_3V_MEAS,    		FPM_CH_AINPUT_3_3V_MEAS, \
			PINSEL_PORT_1,  PINSEL_PIN_31,  FUNC_AD0_CH_5)

	IOM_I2CADC_AIN(IO_ID_AINPUT_A1,             FPM_CH_AINPUT_A1, \
			       	   IO_BUS_I2C_0, 0, I2C_ADC_CH_1)
    IOM_I2CADC_AIN(IO_ID_AINPUT_A2,             FPM_CH_AINPUT_A2, \
				   	   IO_BUS_I2C_0, 0, I2C_ADC_CH_2)
	IOM_I2CADC_AIN(IO_ID_AINPUT_A3,             FPM_CH_AINPUT_A3, \
					   IO_BUS_I2C_0, 0, I2C_ADC_CH_3)
	IOM_I2CADC_AIN(IO_ID_AINPUT_24V_ISO_MEAS,   FPM_CH_AINPUT_24V_ISO_MEAS, \
					   IO_BUS_I2C_0, 0, I2C_ADC_CH_4)

	IOM_VIRT_AI(IO_ID_AINPUT_3_3V_ISO_MEAS, 	IO_CH_NA)  // Not used!!

	IOM_I2CTEMP_AIN(IO_ID_TEMPERATURE, 			    FPM_CH_AINPUT_TEMPERATURE, \
					IO_BUS_I2C_1, 	0x01, 0)

	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI1_PHASE_A_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_A_CURRENT, \
						 ADE78XX_CH_A, PHASE_PEAK_A)
	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI1_PHASE_B_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_B_CURRENT, \
						ADE78XX_CH_A, PHASE_PEAK_B)
	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI1_PHASE_C_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_C_CURRENT, \
						ADE78XX_CH_A, PHASE_PEAK_C)
	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI1_PHASE_N_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_N_CURRENT, \
						ADE78XX_CH_A, PHASE_PEAK_N)

	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI2_PHASE_A_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_A_CURRENT, \
						ADE78XX_CH_B, PHASE_PEAK_A)
	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI2_PHASE_B_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_B_CURRENT, \
						ADE78XX_CH_B, PHASE_PEAK_B)
	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI2_PHASE_C_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_C_CURRENT, \
						ADE78XX_CH_B, PHASE_PEAK_C)
	IOM_ADE78XX_HSDC_AIN(IO_ID_FPI2_PHASE_N_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_N_CURRENT, \
						ADE78XX_CH_B, PHASE_PEAK_N)


	IOM_GPIO_INPUT( IO_ID_SYS_HEALTHY, 		   	    IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_13,   0)
	IOM_GPIO_INPUT( IO_ID_WD_HANDSHAKE, 		   	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,   0)

	IOM_GPIO_OUTPUT( IO_ID_TRACE_DA_0, 	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_5,    		0)
	IOM_GPIO_OUTPUT( IO_ID_TRACE_DA_1, 	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_4,    		0)
	IOM_GPIO_OUTPUT( IO_ID_TRACE_DA_2, 	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_3,    		0)
	IOM_GPIO_OUTPUT( IO_ID_TRACE_DA_3, 	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_2,    		0)

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IOM_VIRT_DI(IO_ID_FPI1_COMMON_FAULT,    	FPM_CH_DINPUT_FPI1_COMMON_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_FAULT,         FPM_CH_DINPUT_FPI1_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_INST_PHASE_FAULT,    FPM_CH_DINPUT_FPI1_INST_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_EARTH_FAULT,    		FPM_CH_DINPUT_FPI1_EARTH_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_INST_EARTH_FAULT,    FPM_CH_DINPUT_FPI1_INST_EARTH_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_COMMON_FAULT,    	FPM_CH_DINPUT_FPI2_COMMON_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_FAULT,         FPM_CH_DINPUT_FPI2_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_INST_PHASE_FAULT,    FPM_CH_DINPUT_FPI2_INST_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_EARTH_FAULT,    		FPM_CH_DINPUT_FPI2_EARTH_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_INST_EARTH_FAULT,    FPM_CH_DINPUT_FPI2_INST_EARTH_FAULT)

	IOM_VIRT_DI(IO_ID_FPI1_TEST_ACTIVE,    		FPM_CH_DINPUT_FPI1_TEST_ACTIVE)
	IOM_VIRT_DI(IO_ID_FPI2_TEST_ACTIVE,    		FPM_CH_DINPUT_FPI2_TEST_ACTIVE)

	IOM_VIRT_DI(IO_ID_FPI1_SENS_EARTH_FAULT,    FPM_CH_DINPUT_FPI1_SENS_EARTH_FAULT)

	IOM_VIRT_DI(IO_ID_FPI1_PHASE_A_CURRENT_PRESENT,  FPM_CH_DINPUT_FPI1_PHASE_A_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_B_CURRENT_PRESENT,  FPM_CH_DINPUT_FPI1_PHASE_B_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_C_CURRENT_PRESENT,  FPM_CH_DINPUT_FPI1_PHASE_C_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI1_COMMON_PHASE_CURRENT_PRESENT, FPM_CH_DINPUT_FPI1_COMMON_PHASE_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_A_CURRENT_ABSENT, FPM_CH_DINPUT_FPI1_PHASE_A_CURRENT_ABSENT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_B_CURRENT_ABSENT,  FPM_CH_DINPUT_FPI1_PHASE_B_CURRENT_ABSENT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_C_CURRENT_ABSENT, FPM_CH_DINPUT_FPI1_PHASE_C_CURRENT_ABSENT)
	IOM_VIRT_DI(IO_ID_FPI1_COMMON_PHASE_CURRENT_ABSENT, FPM_CH_DINPUT_FPI1_COMMON_PHASE_CURRENT_ABSENT)

	IOM_VIRT_DI(IO_ID_FPI2_SENS_EARTH_FAULT,         FPM_CH_DINPUT_FPI2_SENS_EARTH_FAULT)

	IOM_VIRT_DI(IO_ID_FPI2_PHASE_A_CURRENT_PRESENT,  FPM_CH_DINPUT_FPI2_PHASE_A_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_B_CURRENT_PRESENT,  FPM_CH_DINPUT_FPI2_PHASE_B_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_C_CURRENT_PRESENT,  FPM_CH_DINPUT_FPI2_PHASE_C_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI2_COMMON_PHASE_CURRENT_PRESENT, FPM_CH_DINPUT_FPI2_COMMON_PHASE_CURRENT_PRESENT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_A_CURRENT_ABSENT, FPM_CH_DINPUT_FPI2_PHASE_A_CURRENT_ABSENT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_B_CURRENT_ABSENT, FPM_CH_DINPUT_FPI2_PHASE_B_CURRENT_ABSENT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_C_CURRENT_ABSENT, FPM_CH_DINPUT_FPI2_PHASE_C_CURRENT_ABSENT)
	IOM_VIRT_DI(IO_ID_FPI2_COMMON_PHASE_CURRENT_ABSENT, FPM_CH_DINPUT_FPI2_COMMON_PHASE_CURRENT_ABSENT)



	/* End of table marker */
	IOM_LAST
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
