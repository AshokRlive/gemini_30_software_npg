typedef struct 	buffDef
{
    UInt16          RxRingReadIndex[MAX_DEVS];
    UInt16          RxRingWriteIndex[MAX_DEVS];
    UInt8           RxRingBuffer[MAX_DEVS][MAX_RX_RING];
}buffStr;

sysDrvHanSMC.RxRingWriteIndex[portNum] =
           ++sysDrvHanSMC.RxRingWriteIndex[portNum] % MAX_RX_RING_BUF_SMC;
		   
		   /* Check for overflow */
        if (sysDrvHanSMC.RxRingWriteIndex[portNum] ==
            sysDrvHanSMC.RxRingReadIndex[portNum]
           )
        {
            /* Diagnostic counter */
            sysDrvHanSMC.Diag[portNum].CountRxRingOV++;

            /* Step on read index to make room */
            sysDrvHanSMC.RxRingReadIndex[portNum] =
               ++sysDrvHanSMC.RxRingReadIndex[portNum] % MAX_RX_RING_BUF_SMC;
        }
		
		
		
		if (sysDrvHanSMC.RxRingReadIndex[portNum] == sysDrvHanSMC.RxRingWriteIndex[portNum])
        {
            /* Still No data */
			
			
			