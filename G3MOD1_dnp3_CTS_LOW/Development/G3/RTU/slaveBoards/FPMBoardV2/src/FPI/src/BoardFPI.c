/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPI Tests]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/04/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "stdlib.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "systemTime.h"

#include "systemUtils.h"

#include "BoardFPI.h"
#include "FPI.h"
#include "BoardIOMap.h"
#include "BoardCalibration.h"
#include "ADE78xxConfig.h"
#include "IOManagerExtADE78xxInit.h"

#include "ADE78xx.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define POST_INRUSH_RESTRAINT_MS 50
#define HALF_CYCLE_LENGTH 20
#define ZERO_VALUE_THRESHOLD 12

#define MAX_NEUTRAL_COMP_PHASE_VALUE 1000
#define NEUTRAL_COMP_ARRAY_SIZE 20
#define NEUTRAL_COMP_ARRAY_STEP_SIZE (MAX_NEUTRAL_COMP_PHASE_VALUE/NEUTRAL_COMP_ARRAY_SIZE)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


typedef struct NeutralCompensationDef
{
	lu_uint32_t		normalizedRatio;

	lu_uint32_t 	NeutralCompArray[NEUTRAL_COMP_ARRAY_SIZE];

	lu_uint32_t		adjustedMaxNeutralCompPhaseValue;

	lu_uint32_t		adjustedNeutralCompArrayStepSize;

}NeutralCompensationStr;


typedef struct BoardFpiDataDef
{
	FPICfgAndParamsStr  	phase[PHASE_PEAK_LAST];

	lu_uint32_t 			selfResetMs;

	lu_bool_t 				globalFpiEnable;

	lu_uint32_t         	dataLengthCounter[PHASE_PEAK_LAST];

	lu_uint32_t         	maxValue[PHASE_PEAK_LAST];

	FPMCTRatioStr 			ratio;

	NeutralCompensationStr 	NeutralComp;
}BoardFpiDataStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void     	BoardFpiPeakDetect(ADE78XX_CH chip, PHASE_PEAK phase, lu_int32_t value, lu_uint32_t sampleTimeuS);
SB_ERROR 	BoardFPIConfig(FPMConfigStr *msgPtr);
SB_ERROR 	BoardFPIOperate(FPIOperateStr *msgPtr);
void     	setFPIFault(ADE78XX_CH chip, PHASE_PEAK phase);
void 		selfResetFPIFault(ADE78XX_CH chip, PHASE_PEAK phase);

void 		CompensateNeutralCurrent(BoardFpiDataStr *paramsPtr, lu_uint32_t chip);
void		BuildNeutralCurrentCompensationArray(NeutralCompensationStr *paramsPtr, lu_uint32_t divided, lu_uint32_t divisor);
void 		setAbsenceAndPresenceIndication(ADE78XX_CH chip, PHASE_PEAK phase);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported CAN messages */
static const filterTableStr BoardFPIModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! FPM commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C            , LU_FALSE , 0      },

    /*! FPI Config */
    {  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_FPM_C                       , LU_FALSE , 1      }
};


static volatile BoardFpiDataStr 	fpiData[ADE78XX_CH_LAST];

//static CircularBufferStr 	circularBfr[ADE78XX_CH_LAST][PHASE_PEAK_LAST];
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardFPICANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_NONE;

	LU_UNUSED( time );

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_FPM_C:
			// Message sanity check
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FPMConfigStr))
			{
				retError = BoardFPIConfig((FPMConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C:
			// Message sanity check
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FPIOperateStr))
			{
				retError = BoardFPIOperate((FPIOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}


SB_ERROR BoardFPIInit(void)
{
	SB_ERROR         retError = SB_ERROR_PARAM;
	ade78xxRegCalStr *fpiGain;
	CalElementStr    calElement;
	ADE78XX_CH       adcChan;
	PHASE_PEAK       phaseIdx;
	lu_uint8_t       ade78xxRegCalIdx;
	lu_uint8_t       ade78xxRegCalMax;

	/* Initialize FPI parameters */
	for (adcChan = 0; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
		fpiData[adcChan].globalFpiEnable = LU_FALSE;

		for (phaseIdx = 0; phaseIdx < PHASE_PEAK_LAST; phaseIdx++)
		{
			FPIChanInit(&fpiData[adcChan].phase[phaseIdx]);
		}
	}

	/* Register the ISR call back for ADE78xx HSDC data */
	IOManagerADE78xxHSDCDecodeRegisterCallBack(BoardFpiPeakDetect);

	/*************************************************
	 * Calibration File Format
	 *
	 * ADE78XX_AIGAIN,gainValue
	 * ADE78XX_BIGAIN,gainValue
	 * ADE78XX_CIGAIN,gainValue
	 * ADE78XX_NIGAIN,gainValue
	 *
	 * ***********************************************/

	/* FPI A Calibration of Gain Registers */
	if (CalibrationGetCalElement(CAL_ID_ADE78XX_A, CAL_TYPE_ADE78XX_REG, &calElement) == SB_ERROR_NONE)
	{
		ade78xxRegCalMax = (calElement.calDatalength / sizeof(ade78xxRegCalStr)) - 1;

		for( ade78xxRegCalIdx = 0;
			 ade78xxRegCalIdx < (ade78xxRegCalMax * sizeof(ade78xxRegCalStr));
			 ade78xxRegCalIdx = ade78xxRegCalIdx + sizeof(ade78xxRegCalStr)
		   )
		{
			fpiGain = (ade78xxRegCalStr*)(calElement.calDataPtr + ade78xxRegCalIdx);

			retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_A].i2cBus,
													  fpiGain->reg,
													  fpiGain->size,
													  fpiGain->calValue
													);
		}
	}

	/* FPI B Calibration of Gain Registers */
	if (CalibrationGetCalElement(CAL_ID_ADE78XX_B, CAL_TYPE_ADE78XX_REG, &calElement) == SB_ERROR_NONE)
	{
		ade78xxRegCalMax = (calElement.calDatalength / sizeof(ade78xxRegCalStr)) - 1;

		for( ade78xxRegCalIdx = 0;
			 ade78xxRegCalIdx < (ade78xxRegCalMax * sizeof(ade78xxRegCalStr));
			 ade78xxRegCalIdx = ade78xxRegCalIdx + sizeof(ade78xxRegCalStr)
		   )
		{
			fpiGain = (ade78xxRegCalStr*)(calElement.calDataPtr + ade78xxRegCalIdx);

			retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_B].i2cBus,
													  fpiGain->reg,
													  fpiGain->size,
													  fpiGain->calValue
													);
		}
	}

	/* Start DSP */
	for (adcChan = ADE78XX_CH_A; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
		retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
												  ADE78XX_RUN,
												  sizeof(lu_uint16_t),
												  0x0001
												);
		STDelayTime(2);
	}

	/* Setup CAN Msg filter */
	retError = CANFramingAddFilter( BoardFPIModulefilterTable,
								    SU_TABLE_SIZE(BoardFPIModulefilterTable, filterTableStr)
								 );

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void BoardFpiPeakDetect(ADE78XX_CH chip, PHASE_PEAK phase, lu_int32_t value, lu_uint32_t sampleTimeMS)
{
	static volatile lu_uint32_t initIOManagerUpdateTime[ADE78XX_CH_LAST][PHASE_PEAK_LAST];
	static volatile lu_uint32_t ioManagerUpdateTime[ADE78XX_CH_LAST][PHASE_PEAK_LAST];
	static volatile lu_uint32_t minInhibitPostInrushMs[ADE78XX_CH_LAST];
	static volatile lu_uint32_t timeofLastInrushMs[ADE78XX_CH_LAST];
	static volatile lu_uint32_t durationOfPostInrushMs[ADE78XX_CH_LAST];

	fpiData[chip].dataLengthCounter[phase]++;
//	CircularBufferRead(&circularBfr[chip][phase], &value);
	value = abs(value);

	// From the measured analog values, keep only the highest one
	if ((lu_uint32_t)value > fpiData[chip].maxValue[phase])
	{
		fpiData[chip].maxValue[phase] = value;
	}

	// We use the highest value over a given length of data points (half mains cycle),
	// which we go on to use for FPI
	if (fpiData[chip].dataLengthCounter[phase] >= HALF_CYCLE_LENGTH)
	{
		/* Check peak value is above 'practical' zero value
			otherwise clear max value */
		if (fpiData[chip].maxValue[phase] <= ZERO_VALUE_THRESHOLD)
		{
			fpiData[chip].maxValue[phase] = 0;
		}

		ioManagerUpdateTime[chip][phase] = STGetTime();

		fpiData[chip].dataLengthCounter[phase] = 0;

		// Remove noise from Neutral value for SEF
		if (phase == PHASE_PEAK_SEF)
		{
			// Get a compensated neutral value
			CompensateNeutralCurrent( &fpiData[chip], chip);
		}

	    /* Run detection for each phase */
	    FPIPhaseFaultDetection(&fpiData[chip].phase[phase], fpiData[chip].maxValue[phase], sampleTimeMS/2);

	    /* Current presence or absence detection */
	    if((phase == PHASE_PEAK_A) || (phase == PHASE_PEAK_B) || (phase == PHASE_PEAK_C))
	    {
	    	 FPICurrentAbsence(&fpiData[chip].phase[phase], fpiData[chip].maxValue[phase], sampleTimeMS/2);
	    	 // Write the values to the IOManager
	    	 setAbsenceAndPresenceIndication(chip, phase);
	    }

	    selfResetFPIFault(chip, phase);

	    if(fpiData[chip].phase[phase].faultParams.InrushDetected >= 1)
	    {
	    	minInhibitPostInrushMs[chip] += POST_INRUSH_RESTRAINT_MS;
	    	timeofLastInrushMs[chip] = STGetTime();
	    }
	    durationOfPostInrushMs[chip] = STGetTime();

	    /* Signal to IOManager any change in phase/earth faults */
	    if (fpiData[chip].globalFpiEnable == LU_TRUE)
	    {
	    	durationOfPostInrushMs[chip] = STElapsedTime (timeofLastInrushMs[chip], durationOfPostInrushMs[chip]);

	    	if((minInhibitPostInrushMs[chip] == 0) ||
	    		(durationOfPostInrushMs[chip] <= 10))
	    	{
	    		setFPIFault(chip, phase);

	    		if (fpiData[chip].phase[phase].faultParams.Alarm == 1)
	    		{
		    		minInhibitPostInrushMs[chip] = 0;
		    		timeofLastInrushMs[chip] = 0;
		    		durationOfPostInrushMs[chip] = 0;
	    		}
	    	}

	       	else if(durationOfPostInrushMs[chip] >= minInhibitPostInrushMs[chip])
	    	{
	    		fpiData[chip].phase[phase].faultParams.Alarm = 0;
	    		fpiData[chip].phase[phase].faultParams.InitTimeofAlarmMs = 0;

	    		minInhibitPostInrushMs[chip] = 0;
	    		timeofLastInrushMs[chip] = 0;
	    		durationOfPostInrushMs[chip] = 0;
	    	}

	    	else if(durationOfPostInrushMs[chip] > 10)
	    	{
	    		fpiData[chip].phase[phase].faultParams.Alarm = 0;
	    		fpiData[chip].phase[phase].faultParams.InitTimeofAlarmMs = 0;
	    	}
	    }

	    ioManagerUpdateTime[chip][phase] = STElapsedTime(initIOManagerUpdateTime[chip][phase], ioManagerUpdateTime[chip][phase]);

	    if (ioManagerUpdateTime[chip][phase] >= 1000)
		{
	    	initIOManagerUpdateTime[chip][phase] = STGetTime();
	    	ioManagerUpdateTime[chip][phase] = 0;

			switch(chip)
			{
			case ADE78XX_CH_A:
				switch(phase)
				{
				case PHASE_PEAK_A:
				IOManagerSetValue(IO_ID_FPI1_PHASE_A_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_A]);
					break;

				case PHASE_PEAK_B:
				IOManagerSetValue(IO_ID_FPI1_PHASE_B_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_B]);
					break;

				case PHASE_PEAK_C:
				IOManagerSetValue(IO_ID_FPI1_PHASE_C_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_C]);
					break;

				case PHASE_PEAK_SEF:
				IOManagerSetValue(IO_ID_FPI1_PHASE_N_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_SEF]);
					break;

				default:
					break;
				}
				break;

			case ADE78XX_CH_B:
				switch(phase)
				{
				case PHASE_PEAK_A:
				IOManagerSetValue(IO_ID_FPI2_PHASE_A_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_A]);
					break;

				case PHASE_PEAK_B:
				IOManagerSetValue(IO_ID_FPI2_PHASE_B_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_B]);
					break;

				case PHASE_PEAK_C:
				IOManagerSetValue(IO_ID_FPI2_PHASE_C_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_C]);
					break;

				case PHASE_PEAK_SEF:
				IOManagerSetValue(IO_ID_FPI2_PHASE_N_CURRENT, fpiData[chip].maxValue[PHASE_PEAK_SEF]);
					break;

				default:
					break;
				}
				break;

			default:
				break;
			}
		}

	    /* reset peak detect */
		if (phase == PHASE_PEAK_SEF)
		{
			for (phase = 0; phase<PHASE_PEAK_LAST; phase++)
			{
				fpiData[chip].maxValue[phase] = 0;
			}
		}
//   	    fpiData[chip].maxValue[phase] = 0;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardFPIConfig(FPMConfigStr *msgPtr)
{
	SB_ERROR 	    retError;
	ModReplyStr     reply;
	PHASE_PEAK   	phase;
	ADE78XX_CH      adcChan;

	adcChan = msgPtr->fpiConfigChannel;
	retError = SB_ERROR_PARAM;

	switch (msgPtr->fpiConfigChannel)
	{
		case FPI_CONFIG_CH_FPI1:
		adcChan = ADE78XX_CH_A;
		break;

		case FPI_CONFIG_CH_FPI2:
		adcChan = ADE78XX_CH_B;
		break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	switch(msgPtr->fpiConfigChannel)
	{
		case FPI_CONFIG_CH_FPI1: // A
		case FPI_CONFIG_CH_FPI2: // B
			fpiData[adcChan].globalFpiEnable = msgPtr->enabled;

			for (phase = 0; phase < PHASE_PEAK_N; phase++)
			{
				fpiData[adcChan].phase[phase].cfg.enabled = msgPtr->enabled;
				fpiData[adcChan].selfResetMs = msgPtr->selfResetMs;

				if(msgPtr->phaseFault.minFaultDurationMs == 0)
				{
					fpiData[adcChan].phase[phase].cfg.minFaultDurationMs =
										msgPtr->phaseFault.minFaultDurationMs;
				}
				else
				{
					fpiData[adcChan].phase[phase].cfg.minFaultDurationMs =
										msgPtr->phaseFault.minFaultDurationMs-10;
				}

				fpiData[adcChan].phase[phase].cfg.instantFaultCurrent =
										msgPtr->phaseFault.InstantFaultCurrent;
				fpiData[adcChan].phase[phase].cfg.timedFaultCurrent =
										msgPtr->phaseFault.timedFaultCurrent;

				if(msgPtr->phaseFault.minOverloadDurationMs == 0)
				{
					fpiData[adcChan].phase[phase].cfg.minInrushDurationMs =
										msgPtr->phaseFault.minOverloadDurationMs;

				}
				else
				{
					fpiData[adcChan].phase[phase].cfg.minInrushDurationMs =
										msgPtr->phaseFault.minOverloadDurationMs + 10;
				}

			fpiData[adcChan].phase[phase].cfg.inrushCurrent=
										msgPtr->phaseFault.overloadCurrent;
			}

			/* Earth Fault */
			fpiData[adcChan].phase[PHASE_PEAK_N].cfg.enabled = msgPtr->enabled;
			fpiData[adcChan].selfResetMs = msgPtr->selfResetMs;

			if(msgPtr->earthFault.minFaultDurationMs == 0)
			{
				fpiData[adcChan].phase[PHASE_PEAK_N].cfg.minFaultDurationMs =
										msgPtr->earthFault.minFaultDurationMs;

			}
			else
			{
				fpiData[adcChan].phase[PHASE_PEAK_N].cfg.minFaultDurationMs =
										msgPtr->earthFault.minFaultDurationMs-10;
			}

			fpiData[adcChan].phase[PHASE_PEAK_N].cfg.instantFaultCurrent =
										msgPtr->earthFault.InstantFaultCurrent;

			fpiData[adcChan].phase[PHASE_PEAK_N].cfg.timedFaultCurrent =
										msgPtr->earthFault.timedFaultCurrent;

			if(msgPtr->phaseFault.minOverloadDurationMs == 0)
			{
				fpiData[adcChan].phase[PHASE_PEAK_N].cfg.minInrushDurationMs =
										msgPtr->phaseFault.minOverloadDurationMs;
			}
			else
			{
				fpiData[adcChan].phase[PHASE_PEAK_N].cfg.minInrushDurationMs =
										msgPtr->phaseFault.minOverloadDurationMs + 10;
			}

			fpiData[adcChan].phase[PHASE_PEAK_N].cfg.inrushCurrent=
										(msgPtr->phaseFault.overloadCurrent/2);


			/* Sensitive Earth Fault */
			fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.enabled = msgPtr->enabled;

			if(msgPtr->earthFault.minFaultDurationMs == 0)
			{
				fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.minFaultDurationMs =
									msgPtr->sensitiveEarthFault.minFaultDurationMs;

			}
			else
			{
				fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.minFaultDurationMs =
									msgPtr->sensitiveEarthFault.minFaultDurationMs-10;
			}

			fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.instantFaultCurrent = 0;

			fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.timedFaultCurrent =
									msgPtr->sensitiveEarthFault.timedFaultCurrent;

			if(msgPtr->phaseFault.minOverloadDurationMs == 0)
			{
				fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.minInrushDurationMs =
											msgPtr->phaseFault.minOverloadDurationMs;
			}
			else
			{
				fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.minInrushDurationMs =
										msgPtr->phaseFault.minOverloadDurationMs + 10;
			}

			fpiData[adcChan].phase[PHASE_PEAK_SEF].cfg.inrushCurrent =
												(msgPtr->phaseFault.overloadCurrent/2);


			// Current Absence and Presence config values
			for (phase = 0; phase < PHASE_PEAK_N; phase++)
			{
				// Presence config values
				if(msgPtr->absenceIndication.minPresenceDurationMs == 0)
				{
					fpiData[adcChan].phase[phase].cfg.minPresenceDurationMs =
										msgPtr->absenceIndication.minPresenceDurationMs;
				}
				else
				{
					fpiData[adcChan].phase[phase].cfg.minPresenceDurationMs =
										msgPtr->absenceIndication.minPresenceDurationMs-10;
				}

				if(msgPtr->absenceIndication.timedPresenceCurrent == 0)
				{
					fpiData[adcChan].phase[phase].cfg.timedPresenceCurrent =
										msgPtr->absenceIndication.timedPresenceCurrent;

					// Set the Current Present virtual points offline if the timedPresenceCurrent
					// has been set to zero
					switch(adcChan)
					{
						case ADE78XX_CH_A: // A
							IOManagerSetOnline( IO_ID_FPI1_PHASE_A_CURRENT_PRESENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI1_PHASE_B_CURRENT_PRESENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI1_PHASE_C_CURRENT_PRESENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI1_COMMON_PHASE_CURRENT_PRESENT, LU_FALSE);
						break;

						case ADE78XX_CH_B: // B
							IOManagerSetOnline( IO_ID_FPI2_PHASE_A_CURRENT_PRESENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI2_PHASE_B_CURRENT_PRESENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI2_PHASE_C_CURRENT_PRESENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI2_COMMON_PHASE_CURRENT_PRESENT, LU_FALSE);
						break;

						default:
							break;
					}

				}
				else
				{
					fpiData[adcChan].phase[phase].cfg.timedPresenceCurrent =
											msgPtr->absenceIndication.timedPresenceCurrent;
				}

				// Absence config values
				if(msgPtr->absenceIndication.minAbsenceDurationMs == 0)
				{
					fpiData[adcChan].phase[phase].cfg.minAbsenceDurationMs =
											msgPtr->absenceIndication.minAbsenceDurationMs;

				}
				else
				{
					fpiData[adcChan].phase[phase].cfg.minAbsenceDurationMs =
										msgPtr->absenceIndication.minAbsenceDurationMs-10;
				}

				if(msgPtr->absenceIndication.timedAbsenceCurrent == 0)
				{
					fpiData[adcChan].phase[phase].cfg.timedAbsenceCurrent =
										msgPtr->absenceIndication.timedAbsenceCurrent;

					// Set the Current Absence virtual points offline if the timedAbsenceCurrent
					// has been set to zero
					switch(adcChan)
					{
						case ADE78XX_CH_A: // A
							IOManagerSetOnline( IO_ID_FPI1_PHASE_A_CURRENT_ABSENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI1_PHASE_B_CURRENT_ABSENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI1_PHASE_C_CURRENT_ABSENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI1_COMMON_PHASE_CURRENT_ABSENT, LU_FALSE);
						break;

						case ADE78XX_CH_B: // B
							IOManagerSetOnline( IO_ID_FPI2_PHASE_A_CURRENT_ABSENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI2_PHASE_B_CURRENT_ABSENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI2_PHASE_C_CURRENT_ABSENT, LU_FALSE);
							IOManagerSetOnline( IO_ID_FPI2_COMMON_PHASE_CURRENT_ABSENT, LU_FALSE);
						break;

						default:
							break;
					}

				}
				else
				{
					fpiData[adcChan].phase[phase].cfg.timedAbsenceCurrent =
											msgPtr->absenceIndication.timedAbsenceCurrent;
				}
			}

			/* CT Ratio */
			fpiData[adcChan].ratio.ctRatioDividend = msgPtr->ctRatio.ctRatioDividend;
			fpiData[adcChan].ratio.ctRatioDivisor = msgPtr->ctRatio.ctRatioDivisor;

			// Build Neutral Compensation Array for SEF
			BuildNeutralCurrentCompensationArray(&fpiData[adcChan].NeutralComp,
													fpiData[adcChan].ratio.ctRatioDividend,
													fpiData[adcChan].ratio.ctRatioDivisor);

			retError = SB_ERROR_NONE;
			break;

		default:
			break;
	}

	reply.channel = msgPtr->fpiConfigChannel;
	reply.status  = retError == SB_ERROR_NONE ? REPLY_STATUS_OKAY : REPLY_STATUS_ERROR;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_FPM_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardFPIOperate(FPIOperateStr *msgPtr)
{
	SB_ERROR        retError;
	ModReplyStr     reply;
	PHASE_PEAK     	phase;
	ADE78XX_CH      adcChan;

	retError = SB_ERROR_NONE;

	switch (msgPtr->channel)
	{
		case FPM_CH_FPI_1_TEST:
		case FPM_CH_FPI_1_RESET:
		adcChan = ADE78XX_CH_A;
		break;

		case FPM_CH_FPI_2_TEST:
		case FPM_CH_FPI_2_RESET:
		adcChan = ADE78XX_CH_B;
		break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	switch (msgPtr->channel)
	{
		case FPM_CH_FPI_1_TEST:
			/* Disable FPI and force all FPI outputs */
			SU_CRITICAL_REGION_ENTER()

			for (phase = 0; phase < PHASE_PEAK_LAST; phase++)
			{
				fpiData[adcChan].phase[phase].cfg.enabled = LU_FALSE;
				fpiData[adcChan].globalFpiEnable = LU_FALSE;
				FPIChanReset(&fpiData[adcChan].phase[phase]);
			}

			SU_CRITICAL_REGION_EXIT()

			retError = IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI1_SENS_EARTH_FAULT, 1);

			retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);
			retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);

			retError = IOManagerSetValue(IO_ID_FPI1_TEST_ACTIVE, 1);

			for (phase = 0; phase < PHASE_PEAK_LAST; phase++)
			{
				FPIChanTest(&fpiData[adcChan].phase[phase]);
				fpiData[adcChan].phase[phase].cfg.enabled = LU_TRUE;
				fpiData[adcChan].globalFpiEnable = LU_TRUE;
			}
			break;

		case FPM_CH_FPI_1_RESET:
			/* Enable FPI and clear all FPI outputs */
			SU_CRITICAL_REGION_ENTER()

			for (phase = 0; phase < PHASE_PEAK_LAST; phase++)
			{
				FPIChanReset(&fpiData[adcChan].phase[phase]);

				fpiData[adcChan].phase[phase].cfg.enabled = LU_TRUE;
				fpiData[adcChan].globalFpiEnable = LU_TRUE;
			}

			SU_CRITICAL_REGION_EXIT()

			retError = IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI1_SENS_EARTH_FAULT, 0);

			retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
			retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);

			retError = IOManagerSetValue(IO_ID_FPI1_TEST_ACTIVE, 0);
			break;

		case FPM_CH_FPI_2_TEST:
			/* Disable FPI and force all FPI outputs */
			SU_CRITICAL_REGION_ENTER()

			for (phase = 0; phase < PHASE_PEAK_LAST; phase++)
			{
				fpiData[adcChan].phase[phase].cfg.enabled = LU_FALSE;
				fpiData[adcChan].globalFpiEnable = LU_FALSE;
				FPIChanReset(&fpiData[adcChan].phase[phase]);
			}

			SU_CRITICAL_REGION_EXIT()

			retError = IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 1);
			retError = IOManagerSetValue(IO_ID_FPI2_SENS_EARTH_FAULT, 1);

			retError = IOManagerSetValue(IO_ID_LED_FPI2, 1);
			retError = IOManagerSetValue(IO_ID_LED_EFI2, 1);

			retError = IOManagerSetValue(IO_ID_FPI2_TEST_ACTIVE, 1);

			for (phase = 0; phase < PHASE_PEAK_LAST; phase++)
			{
				FPIChanTest(&fpiData[adcChan].phase[phase]);
				fpiData[adcChan].phase[phase].cfg.enabled = LU_TRUE;
				fpiData[adcChan].globalFpiEnable = LU_TRUE;
			}
			break;

		case FPM_CH_FPI_2_RESET:
			/* Enable FPI and clear all FPI outputs */
			SU_CRITICAL_REGION_ENTER()

			for (phase = 0; phase < PHASE_PEAK_LAST; phase++)
			{
				FPIChanReset(&fpiData[adcChan].phase[phase]);

				fpiData[adcChan].phase[phase].cfg.enabled = LU_TRUE;
				fpiData[adcChan].globalFpiEnable = LU_TRUE;
			}

			SU_CRITICAL_REGION_EXIT()

			retError = IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);
			retError = IOManagerSetValue(IO_ID_FPI2_SENS_EARTH_FAULT, 0);

			retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
			retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);

			retError = IOManagerSetValue(IO_ID_FPI2_TEST_ACTIVE, 0);
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}


	reply.channel = msgPtr->channel;
	reply.status  = retError == SB_ERROR_NONE ? REPLY_STATUS_OKAY : REPLY_STATUS_ERROR;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
			            MODULE_MSG_ID_CMD_FPI_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return retError;
}

void setFPIFault(ADE78XX_CH chip, PHASE_PEAK phase)
{
	PHASE_PEAK phaseChan;
	static lu_bool_t commonAlarmMask[ADE78XX_CH_LAST];
	static lu_bool_t instantAlarmMask[ADE78XX_CH_LAST];
	static lu_bool_t alarmMask[ADE78XX_CH_LAST];

	instantAlarmMask[chip] = LU_FALSE;
	alarmMask[chip] = LU_FALSE;

	// As we do not have an IOManager flag for each of the Phase Alarms
	// (or for that matter p-p, p-p-p, cross country, etc... type faults)
	// we OR all the 'regular' phase fault alarms into one variable, alarmMask
	// and the instant phase faults into instantAlarm
	for (phaseChan = PHASE_PEAK_A; phaseChan < PHASE_PEAK_N; phaseChan++)
	{
		alarmMask[chip] |= fpiData[chip].phase[phaseChan].faultParams.Alarm;
		instantAlarmMask[chip] |= fpiData[chip].phase[phaseChan].faultParams.InstantAlarm;
	}

	switch (chip)
	{
	case ADE78XX_CH_A:
		switch(phase)
		{
		case PHASE_PEAK_A:
		case PHASE_PEAK_B:
		case PHASE_PEAK_C:
			if(fpiData[chip].phase[phase].faultParams.Alarm == LU_TRUE)
			{
				IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
				IOManagerSetValue(IO_ID_LED_FPI1, fpiData[chip].phase[phase].faultParams.Alarm);
			}
			else if (alarmMask[chip] == LU_FALSE)
			{
				IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.Alarm );
				IOManagerSetValue(IO_ID_LED_FPI1, fpiData[chip].phase[phase].faultParams.Alarm);
			}

			if(fpiData[chip].phase[phase].faultParams.InstantAlarm == LU_TRUE)
			{
				IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.InstantAlarm );
			}
			else if (instantAlarmMask[chip] == LU_FALSE)
			{
				IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.InstantAlarm );
			}
			break;

		case PHASE_PEAK_N:
		case PHASE_PEAK_SEF:
			// Set or clear appropriate IO_ID
			if (phase == PHASE_PEAK_N)
			{
				IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
				IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, fpiData[chip].phase[phase].faultParams.InstantAlarm);
			}

			else if(phase == PHASE_PEAK_SEF)
			{
				IOManagerSetValue(IO_ID_FPI1_SENS_EARTH_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
			}

			// Set or clear the common LED
			if((fpiData[chip].phase[PHASE_PEAK_N].faultParams.Alarm == LU_TRUE) ||
					(fpiData[chip].phase[PHASE_PEAK_SEF].faultParams.Alarm == LU_TRUE))
			{
				IOManagerSetValue(IO_ID_LED_EFI1, 1);
			}

			if((fpiData[chip].phase[PHASE_PEAK_N].faultParams.Alarm == LU_FALSE) &&
					(fpiData[chip].phase[PHASE_PEAK_SEF].faultParams.Alarm == LU_FALSE))
			{
				IOManagerSetValue(IO_ID_LED_EFI1, 0);
			}
			break;

		default:
			break;
		}
		break;

	case ADE78XX_CH_B:
		switch(phase)
		{
		case PHASE_PEAK_A:
		case PHASE_PEAK_B:
		case PHASE_PEAK_C:
			if(fpiData[chip].phase[phase].faultParams.Alarm == LU_TRUE)
			{
				IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
				IOManagerSetValue(IO_ID_LED_FPI2, fpiData[chip].phase[phase].faultParams.Alarm);
			}
			else if (alarmMask[chip] == LU_FALSE)
			{
				IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
				IOManagerSetValue(IO_ID_LED_FPI2, fpiData[chip].phase[phase].faultParams.Alarm);
			}

			if(fpiData[chip].phase[phase].faultParams.InstantAlarm == LU_TRUE)
			{
				IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.InstantAlarm );
			}
			else if (instantAlarmMask[chip] == LU_FALSE)
			{
				IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, fpiData[chip].phase[phase].faultParams.InstantAlarm );
			}
			break;

		case PHASE_PEAK_N:
		case PHASE_PEAK_SEF:
			// Set or clear appropriate IO_ID
			if (phase == PHASE_PEAK_N)
			{
				IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
				IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, fpiData[chip].phase[phase].faultParams.InstantAlarm);
			}

			else if(phase == PHASE_PEAK_SEF)
			{
				IOManagerSetValue(IO_ID_FPI2_SENS_EARTH_FAULT, fpiData[chip].phase[phase].faultParams.Alarm);
			}

			// Set or clear the common LED
			if((fpiData[chip].phase[PHASE_PEAK_N].faultParams.Alarm == LU_TRUE) ||
					(fpiData[chip].phase[PHASE_PEAK_SEF].faultParams.Alarm == LU_TRUE))
			{
				IOManagerSetValue(IO_ID_LED_EFI2, 1);
			}

			if((fpiData[chip].phase[PHASE_PEAK_N].faultParams.Alarm == LU_FALSE) &&
					(fpiData[chip].phase[PHASE_PEAK_SEF].faultParams.Alarm == LU_FALSE))
			{
				IOManagerSetValue(IO_ID_LED_EFI2, 0);
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	commonAlarmMask[chip] = LU_FALSE;

	// OR all Phase and Earth Instant and 'regular' Alarms into one variable, commonAlarmMask
	for (phaseChan = PHASE_PEAK_A; phaseChan < PHASE_PEAK_LAST; phaseChan++)
	{
		commonAlarmMask[chip] |= fpiData[chip].phase[phaseChan].faultParams.InstantAlarm;
		commonAlarmMask[chip] |= fpiData[chip].phase[phaseChan].faultParams.Alarm;
	}

	switch(chip)
	{
	case ADE78XX_CH_A:
		IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, commonAlarmMask[chip]);
		break;

	case ADE78XX_CH_B:
		IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, commonAlarmMask[chip]);
		break;

	default:
		break;
	}
}

void selfResetFPIFault(ADE78XX_CH chip, PHASE_PEAK phase)
{
	if (fpiData[chip].phase[phase].faultParams.TimeSinceAlarmMs >=
											fpiData[chip].selfResetMs)
	{
		//Clear alarms
		fpiData[chip].phase[phase].faultParams.Alarm = LU_FALSE;
		//Reset self timer variables
		fpiData[chip].phase[phase].faultParams.TimeSinceAlarmMs = 0;
		fpiData[chip].phase[phase].faultParams.InitTimeofAlarmMs = 0;
		fpiData[chip].phase[phase].faultParams.FaultEventDetected = 0;

	}

	if (fpiData[chip].phase[phase].faultParams.TimeSinceInstantAlarmMs >=
													fpiData[chip].selfResetMs)
	{
		//Clear alarms
		fpiData[chip].phase[phase].faultParams.InstantAlarm = LU_FALSE;
		//Reset self timer variables
		fpiData[chip].phase[phase].faultParams.TimeSinceInstantAlarmMs = 0;
		fpiData[chip].phase[phase].faultParams.InitTimeSinceInstantAlarmMs = 0;
	}
}

/*!
 ******************************************************************************
 *   \brief Build an array with analog values, used to reduce(/compensate) the
 *			neutral current error.
 *
 *   Detailed description
 *			Neutral Current Error was measured using an omicron CMC 256plus,
 *			injecting a balanced three-phase current. This error was measured
 *			on three different FPM cards.
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void BuildNeutralCurrentCompensationArray(NeutralCompensationStr *paramsPtr, lu_uint32_t divided, lu_uint32_t divisor)
{
	lu_uint32_t ix = 0;
	lu_uint32_t iy = 0;

	paramsPtr->normalizedRatio = (10000/(divided / divisor));
	paramsPtr->adjustedMaxNeutralCompPhaseValue = MAX_NEUTRAL_COMP_PHASE_VALUE * paramsPtr->normalizedRatio;
	paramsPtr->adjustedNeutralCompArrayStepSize = NEUTRAL_COMP_ARRAY_STEP_SIZE * paramsPtr->normalizedRatio;

	// Increment every 50 A
	// Add a zero compensation value for current up to 400A primary
	// No compensation is added from zero to 400A
	// This also saves us from using a more complex 3rd order or more polynomial
	for (ix = 0; ix < 450; ix+= NEUTRAL_COMP_ARRAY_STEP_SIZE)
	{
		paramsPtr->NeutralCompArray[iy] = 0;
		iy++;
	}

	// Compensate accordingly from 450A and above
	for (ix = 450; ix < MAX_NEUTRAL_COMP_PHASE_VALUE; ix+= NEUTRAL_COMP_ARRAY_STEP_SIZE)
	{
		// Apply compensation and normalize
		paramsPtr->NeutralCompArray[iy] = (lu_uint32_t)((ix * 0.0052 - 2) * paramsPtr->normalizedRatio);
		iy++;
	}
}

/*!
 ******************************************************************************
 *   \brief Reduces the neutral current error by about 50%.
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void CompensateNeutralCurrent(BoardFpiDataStr *paramsPtr, lu_uint32_t chip)
{
	lu_uint32_t ix = 0;
	lu_uint32_t iy = 0;

	// Increment every NEUTRAL_COMP_ARRAY_STEP_SIZE (in Primary Amps).
	// We reduce the neutral current error based on the lowest measured phase current
	// i.e If for example the measured current is 600A on all three phases,
	// we will reduce the neutral current by ???? (found in NeutralCompArray).
	for (ix = 0; ix < paramsPtr->NeutralComp.adjustedMaxNeutralCompPhaseValue; ix+= paramsPtr->NeutralComp.adjustedNeutralCompArrayStepSize)
	{
		// Find the index at which ALL phase values have been measured
		if ((paramsPtr->maxValue[PHASE_PEAK_A] >= ix)
				&& (paramsPtr->maxValue[PHASE_PEAK_B] >= ix)
				&& (paramsPtr->maxValue[PHASE_PEAK_C] >= ix) )
		{
			iy++;
		}
	}
	paramsPtr->maxValue[PHASE_PEAK_SEF] = paramsPtr->maxValue[PHASE_PEAK_N]- paramsPtr->NeutralComp.NeutralCompArray[iy];
}

/*!
 ******************************************************************************
 *   \brief Set (or Clear) the IOManager flags relating to Absence And Presence Indication
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void setAbsenceAndPresenceIndication(ADE78XX_CH chip, PHASE_PEAK phase)
{
	PHASE_PEAK phaseChan;
	static lu_bool_t commonAbsenceIndicationMask[ADE78XX_CH_LAST];
	static lu_bool_t commonPresenceIndicationMask[ADE78XX_CH_LAST];

	// Both common indication masks need to be set to true initially
	// due to the ANDing being performed further down
	commonAbsenceIndicationMask[chip] = LU_TRUE;
	commonPresenceIndicationMask[chip] = LU_TRUE;

	switch (chip)
	{
	case ADE78XX_CH_A:
		switch(phase)
		{
		case PHASE_PEAK_A:
			IOManagerSetValue(IO_ID_FPI1_PHASE_A_CURRENT_ABSENT, fpiData[chip].phase[phase].absenceParams.CurrentAbsenceIndication);
			IOManagerSetValue(IO_ID_FPI1_PHASE_A_CURRENT_PRESENT, fpiData[chip].phase[phase].absenceParams.CurrentPresenceIndication);
			break;

		case PHASE_PEAK_B:
			IOManagerSetValue(IO_ID_FPI1_PHASE_B_CURRENT_ABSENT, fpiData[chip].phase[phase].absenceParams.CurrentAbsenceIndication);
			IOManagerSetValue(IO_ID_FPI1_PHASE_B_CURRENT_PRESENT, fpiData[chip].phase[phase].absenceParams.CurrentPresenceIndication);
			break;

		case PHASE_PEAK_C:
			IOManagerSetValue(IO_ID_FPI1_PHASE_C_CURRENT_ABSENT, fpiData[chip].phase[phase].absenceParams.CurrentAbsenceIndication);
			IOManagerSetValue(IO_ID_FPI1_PHASE_C_CURRENT_PRESENT, fpiData[chip].phase[phase].absenceParams.CurrentPresenceIndication);
			break;

		default:
			break;
		}
		break;

	case ADE78XX_CH_B:
		switch(phase)
		{
		case PHASE_PEAK_A:
			IOManagerSetValue(IO_ID_FPI2_PHASE_A_CURRENT_ABSENT, fpiData[chip].phase[phase].absenceParams.CurrentAbsenceIndication);
			IOManagerSetValue(IO_ID_FPI2_PHASE_A_CURRENT_PRESENT, fpiData[chip].phase[phase].absenceParams.CurrentPresenceIndication);
			break;

		case PHASE_PEAK_B:
			IOManagerSetValue(IO_ID_FPI2_PHASE_B_CURRENT_ABSENT, fpiData[chip].phase[phase].absenceParams.CurrentAbsenceIndication);
			IOManagerSetValue(IO_ID_FPI2_PHASE_B_CURRENT_PRESENT, fpiData[chip].phase[phase].absenceParams.CurrentPresenceIndication);
			break;

		case PHASE_PEAK_C:
			IOManagerSetValue(IO_ID_FPI2_PHASE_C_CURRENT_ABSENT, fpiData[chip].phase[phase].absenceParams.CurrentAbsenceIndication);
			IOManagerSetValue(IO_ID_FPI2_PHASE_C_CURRENT_PRESENT, fpiData[chip].phase[phase].absenceParams.CurrentPresenceIndication);
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	// ANDing all the Absence and Presence Indication for all the phases
	for (phaseChan = PHASE_PEAK_A; phaseChan < PHASE_PEAK_N; phaseChan++)
	{
		commonPresenceIndicationMask[chip] &= fpiData[chip].phase[phaseChan].absenceParams.CurrentPresenceIndication;
		commonAbsenceIndicationMask[chip] &= fpiData[chip].phase[phaseChan].absenceParams.CurrentAbsenceIndication;
	}

	switch(chip)
	{
	case ADE78XX_CH_A:
		IOManagerSetValue(IO_ID_FPI1_COMMON_PHASE_CURRENT_PRESENT, commonPresenceIndicationMask[chip]);
		IOManagerSetValue(IO_ID_FPI1_COMMON_PHASE_CURRENT_ABSENT, commonAbsenceIndicationMask[chip]);
		break;

	case ADE78XX_CH_B:
		IOManagerSetValue(IO_ID_FPI2_COMMON_PHASE_CURRENT_PRESENT, commonPresenceIndicationMask[chip]);
		IOManagerSetValue(IO_ID_FPI2_COMMON_PHASE_CURRENT_ABSENT, commonAbsenceIndicationMask[chip]);
		break;

	default:
		break;
	}
}

/*
 *********************** End of file ******************************************
 */
