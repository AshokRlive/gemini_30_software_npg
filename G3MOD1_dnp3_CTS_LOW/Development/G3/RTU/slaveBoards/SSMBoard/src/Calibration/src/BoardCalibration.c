/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "LinearInterpolation.h"
#include "Calibration.h"
#include "BoardCalibration.h"
#include "BoardIOMap.h"

#include "NVRam.h"
#include "systemStatus.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_1DU16U16_CURRENT       2

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR BoardCalTestNvramSelect(CalTstNvramSelStr *calTstNvramPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static const LnInterpTable1DU16S16Str current_CT_A_1dU16U16[MAX_1DU16U16_CURRENT] =
{
		/*
		 * Input value ,       Output value (10th mAmps)
		 */
	    { 0,     				0    },
		{ 4095,  				19819}
};

static const LnInterpTable1DU16S16Str current_CT_B_1dU16U16[MAX_1DU16U16_CURRENT] =
{
		/*
		 * Input value ,       Output value (10th mAmps)
		 */
	    { 0,     				0    },
		{ 4095,  				19819}
};

CalElementStr boardCalTable[MAX_CAL_ID + 1] =
{
	CALID_ELEMENT(CAL_ID_CT_A_I_MEASURE)
	CALID_ELEMENT(CAL_ID_CT_A_TEMP_SLOPE)
	CALID_ELEMENT(CAL_ID_CT_B_I_MEASURE)
	CALID_ELEMENT(CAL_ID_CT_B_TEMP_SLOPE)

	/* End Table Marker */
	CALID_ELEMENT_LAST()
};

/*! List of supported message */
static const filterTableStr BoardCalibrationModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Bootloader test API commands */
	{  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_NVRAM_SELECT_C      , LU_FALSE , 0      }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardCalibrationInit(void)
{
	SB_ERROR      retError;
	lu_uint8_t    *calDataPtr;
	lu_uint16_t   calDataSize;
	lu_bool_t	  primaryFail, secondaryFail;

	/* To add calibration commands to CAN filter */
	CANFramingAddFilter( BoardCalibrationModulefilterTable,
						 SU_TABLE_SIZE(BoardCalibrationModulefilterTable, filterTableStr)
					   );

	primaryFail   = LU_FALSE;
	secondaryFail = LU_FALSE;

	// Get NVRAM data pointer
	calDataSize = NVRAM_ID_BLK_CAL_SIZE;
	retError = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_CAL, &calDataPtr);
	if (retError != SB_ERROR_NONE)
	{
		primaryFail   = LU_TRUE;
	}
	else
	{
		/* To check the sequence of calibration ID and boundary */
		retError = CalibrationInitCalDataPtr(calDataPtr, calDataSize, MAX_CAL_ID);
		if (retError != SB_ERROR_NONE)
		{
			primaryFail   = LU_TRUE;
		}
	}

	if (primaryFail == LU_TRUE)
	{
		calDataSize = NVRAM_APP_BLK_CAL_SIZE;
		retError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_CAL, &calDataPtr);
		if (retError != SB_ERROR_NONE)
		{
			secondaryFail   = LU_TRUE;
		}
		else
		{
			/* To check the sequence of calibration ID and boundary */
			retError = CalibrationInitCalDataPtr(calDataPtr, calDataSize, MAX_CAL_ID);
			if (retError != SB_ERROR_NONE)
			{
				secondaryFail   = LU_TRUE;
			}
		}
	}

	if (secondaryFail == LU_TRUE)
	{
		SysAlarmSetLogEvent(LU_TRUE,
									SYS_ALARM_SEVERITY_WARNING,
									SYS_ALARM_SUBSYSTEM_SYSTEM,
									SYSALC_SYSTEM_FACTORY_CAL,
									0
								   );
	}

	if (primaryFail == LU_TRUE)
	{
		/* Using fixed calibration - since all calibration is corrupted */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_PRIMARY_FACTORY_CAL,
							0
						   );
	}

	if (primaryFail == LU_TRUE && secondaryFail == LU_TRUE)
	{
		/* Using fixed calibration - since all calibration is corrupted */
		retError = CalibrationInitCalDataPtr(calDataPtr, calDataSize, MAX_CAL_ID);

		/* To erase all the calibration data*/
		CalibrationEraseAllElements();

		retError = CalibrationAddElement(CAL_ID_CT_A_I_MEASURE,
										 CAL_TYPE_1DU16U16_CT_TEMP_COMP,
										 sizeof(current_CT_A_1dU16U16),
										 (lu_uint8_t *)current_CT_A_1dU16U16);


		retError = CalibrationAddElement(CAL_ID_CT_B_I_MEASURE,
										 CAL_TYPE_1DU16U16_CT_TEMP_COMP,
										 sizeof(current_CT_B_1dU16U16),
										 (lu_uint8_t *)current_CT_B_1dU16U16);
	}

	/* To check the sequence of calibration ID and boundary */
	CalibrationInitCalDataPtr(calDataPtr, calDataSize, MAX_CAL_ID);

	return SB_ERROR_NONE;
}

SB_ERROR BoardCalibrationGetTemperature(lu_int32_t *tempValuePtr)
{
	return IOManagerGetValue(IO_ID_TEMP_MEASURE, tempValuePtr);
}

SB_ERROR BoardCalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retVal;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CALTST:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CALTST_NVRAM_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(CalTstNvramSelStr))
			{
				BoardCalTestNvramSelect((CalTstNvramSelStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

SB_ERROR BoardCalTestNvramSelect(CalTstNvramSelStr *calTstNvramPtr)
{
	SB_ERROR 	   retError;
	lu_uint8_t    *calDataPtr;

	retError = SB_ERROR_PARAM;

	switch (calTstNvramPtr->nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		retError = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_CAL, &calDataPtr);
		retError = CalibrationInitCalDataPtr(calDataPtr, NVRAM_ID_BLK_CAL_SIZE, MAX_CAL_ID);
		break;

	case NVRAM_TYPE_APPLICATION:
		retError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_CAL, &calDataPtr);
		retError = CalibrationInitCalDataPtr(calDataPtr, NVRAM_APP_BLK_CAL_SIZE, MAX_CAL_ID);
		break;

	default:
		break;
	}

	retError = CANCSendMCM( MODULE_MSG_TYPE_CALTST,
			                MODULE_MSG_ID_CALTST_NVRAM_SELECT_R,
								1,
								(lu_uint8_t*)&retError
							  );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
