/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BoardCalibration.h 2070 2012-11-26 21:33:34Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/IOMBoard/AppBootProg/src/Calibration/include/BoardCalibration.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARD_CALIBRATION_INCLUDED
#define _BOARD_CALIBRATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Calibration.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_CAL_ID 		    CAL_ID_LAST


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! Calibration ID - Calibration Elements */
typedef enum
{
	CAL_ID_NOT_USED,
	CAL_ID_LAST
} CAL_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern CalElementStr boardCalTable[MAX_CAL_ID + 1];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationInit(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

#endif /* _BOARD_CALIBRATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
