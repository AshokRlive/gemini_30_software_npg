/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX			(IO_ID_LAST)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IO_ID_CAN1_RX	                       =0 ,
	IO_ID_CAN1_TX	                          ,

	IO_ID_UART_DEBUG_TX	                      ,
	IO_ID_UART_DEBUG_RX	                      ,

	IO_ID_I2C_SCL                             ,
	IO_ID_I2C_SDA                             ,

	IO_ID_I2C1_CLK                            ,
	IO_ID_I2C1_SDA                            ,

	IO_ID_ADC_CLK                             ,
	IO_ID_ADC_DIN                             ,
	IO_ID_ADC_DOUT                            ,

	/*********************************/
	/* Physically connected IO Pins  */
	/* (Some Virt for NXP dev board) */
	/* (But will be physical on PSM) */
	/*********************************/
	IO_ID_LOCAL_LINE                          ,
	IO_ID_REMOTE_LINE                         ,

	IO_ID_AUTO_CONFIG_IN                      ,
	IO_ID_AUTO_CONFIG_OUT                     ,

	IO_ID_FACTORY                             ,

	IO_ID_HWDOG_1HZ_KICK                      ,

	IO_ID_APPRAM_WEN                          ,

	IO_ID_LED_CTRL_OK_GREEN                   ,
	IO_ID_LED_CTRL_OK_RED                     ,
	IO_ID_LED_CTRL_CAN_GREEN                  ,
	IO_ID_LED_CTRL_CAN_RED                    ,
	IO_ID_LED_CTRL_EXTINGUISH                 ,
	IO_ID_LED_CTRL_BATT_GREEN                 ,
	IO_ID_LED_CTRL_BATT_RED                   ,

	IO_ID_MOD_SEL1							  ,
	IO_ID_MOD_SEL2							  ,
	IO_ID_MOD_SEL3							  ,
	IO_ID_MOD_SEL4							  ,

	IO_ID_ADC_CS                              ,

	IO_ID_I_MEASURE                           ,
	IO_ID_BATT_MEASURE                        ,
	IO_ID_TEMP_MEASURE                        ,

	/* SPI ADC */
	IO_ID_AINPUT_A1                           ,
	IO_ID_AINPUT_A2                           ,
	IO_ID_AINPUT_A3                           ,
	IO_ID_AINPUT_A4                           ,
	IO_ID_AINPUT_A5                           ,
	IO_ID_AINPUT_A6                           ,
	IO_ID_AINPUT_A7                           ,
	IO_ID_AINPUT_A8                           ,

	/* I2C Expanders */
	IO_ID_DO1_RESET                           ,
	IO_ID_DO1_SET                             ,
	IO_ID_DO1_FB                              ,
	IO_ID_DO1_LED                             ,
	IO_ID_DO2_RESET                           ,
	IO_ID_DO2_SET                             ,
	IO_ID_DO2_FB                              ,
	IO_ID_DO2_LED                             ,
	IO_ID_DO3_RESET                           ,
	IO_ID_DO3_SET                             ,
	IO_ID_DO3_FB                              ,
	IO_ID_DO3_LED                             ,

	IO_ID_DI01                                ,
	IO_ID_DI02                                ,
	IO_ID_DI03                                ,
	IO_ID_DI04                                ,
	IO_ID_DI05                                ,
	IO_ID_DI06                                ,
	IO_ID_DI07                                ,
	IO_ID_DI08                                ,
	IO_ID_DI01_LED                            ,
	IO_ID_DI02_LED                            ,
	IO_ID_DI03_LED                            ,
	IO_ID_DI04_LED                            ,
	IO_ID_DI05_LED                            ,
	IO_ID_DI06_LED                            ,
	IO_ID_DI07_LED                            ,
	IO_ID_DI08_LED                            ,

	IO_ID_DI09_LED                            ,
	IO_ID_DI10_LED                            ,
	IO_ID_DI11_LED                            ,
	IO_ID_DI12_LED                            ,
	IO_ID_DI13_LED                            ,
	IO_ID_DI14_LED                            ,
	IO_ID_DI15_LED                            ,
	IO_ID_DI16_LED                            ,
	IO_ID_DI09                                ,
	IO_ID_DI10                                ,
	IO_ID_DI11                                ,
	IO_ID_DI12                                ,
	IO_ID_DI13                                ,
	IO_ID_DI14                                ,
	IO_ID_DI15                                ,
	IO_ID_DI16                                ,

	IO_ID_SYS_HEALTHY                         ,
	IO_ID_WD_HANDSHAKE                        ,

	IO_ID_APPLICATION_NVRAM                   ,
	IO_ID_IDENTITY_NVRAM                      ,

	/******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IO_ID_VIRT_ON_ELAPSED_TIME                ,

	IO_ID_LAST
} IO_ID;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
