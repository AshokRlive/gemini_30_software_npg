/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_TX, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C_SCL,				 PINSEL_PORT_0,  PINSEL_PIN_28,  \
			FUNC_I2C_0_SCL, 	0)
	IOM_GPIO_PERIPH(IO_ID_I2C_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_27,  \
			FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_ADC_CLK,			 	 PINSEL_PORT_1,  PINSEL_PIN_20,  \
				FUNC_SSP0_SCK, 		0)
	IOM_GPIO_PERIPH(IO_ID_ADC_DIN,			     PINSEL_PORT_1,  PINSEL_PIN_24,  \
			FUNC_SSP0_MOSI, 	0)
	IOM_GPIO_PERIPH(IO_ID_ADC_DOUT,		     	 PINSEL_PORT_1,  PINSEL_PIN_23,  \
			FUNC_SSP0_MISO, 	0)


	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_LOCAL_LINE,               IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_17,   0)
	IOM_GPIO_INPUT( IO_ID_REMOTE_LINE, 			 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_15,   0)

	IOM_GPIO_INPUT( IO_ID_AUTO_CONFIG_IN, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_9,    0)
	IOM_GPIO_OUTPUT( IO_ID_AUTO_CONFIG_OUT,			IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_16,   0)

	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,   		GPIO_PM_PULL_UP)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
					PINSEL_PORT_0,  PINSEL_PIN_18,  0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   		0)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_BATT_GREEN, 		IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_30,   	GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_BATT_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_31,   		GPIO_PM_OUTPUT_LOW)


	IOM_GPIO_INPUT( IO_ID_MOD_SEL1, 			   	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_0,   	(GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL2, 			   	IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_1,   (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL3, 			   	IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_4,   (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL4, 			   	IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_8,   (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_OUTPUT( IO_ID_ADC_CS, 					IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_21,   0)


	IOM_PERIPH_AIN( IO_ID_I_MEASURE, 			IOM_CH_AINPUT_CT, \
				PINSEL_PORT_0,  PINSEL_PIN_23,  FUNC_AD0_CH_0)
	IOM_PERIPH_AIN( IO_ID_BATT_MEASURE, 		IOM_CH_AINPUT_BATT_VOLTAGE, \
			PINSEL_PORT_0,  PINSEL_PIN_3,  FUNC_AD0_CH_6)
	IOM_PERIPH_AIN( IO_ID_TEMP_MEASURE,    		IOM_CH_AINPUT_TEMP, \
			PINSEL_PORT_0,  PINSEL_PIN_2,  FUNC_AD0_CH_7)

	/* SPI attached ADC (MCP3204/MCP3208) */
	IOM_SPI_AIN(IO_ID_AINPUT_A1, 				IOM_CH_AINPUT_A1, \
					IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_0, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A2, 				IOM_CH_AINPUT_A2, \
			IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_1, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A3, 				IOM_CH_AINPUT_A3, \
				IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_2, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A4, 				IOM_CH_AINPUT_A4, \
					IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_3, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A5, 				IOM_CH_AINPUT_A5, \
					IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_4, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A6, 				IOM_CH_AINPUT_A6, \
					IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_5, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A7, 				IOM_CH_AINPUT_A7, \
					IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_6, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A8, 				IOM_CH_AINPUT_A8, \
					IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_7, IO_ID_ADC_CS)

	/* I2C IO Expanders */
	IOM_I2CEXP_OUTPUT(IO_ID_DO1_RESET, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_0, GPIO_PM_OUTPUT_LOW)
	IOM_I2CEXP_OUTPUT(IO_ID_DO1_SET, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_1, GPIO_PM_OUTPUT_LOW)
	IOM_I2CEXP_INPUT(IO_ID_DO1_FB, 					IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_2, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DO1_LED, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_3, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT(IO_ID_DO2_RESET, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_4, GPIO_PM_OUTPUT_LOW)
	IOM_I2CEXP_OUTPUT(IO_ID_DO2_SET, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_5, GPIO_PM_OUTPUT_LOW)
	IOM_I2CEXP_INPUT(IO_ID_DO2_FB, 					IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_6, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DO2_LED, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_0_7, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT(IO_ID_DO3_RESET, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_1_0, GPIO_PM_OUTPUT_LOW)
	IOM_I2CEXP_OUTPUT(IO_ID_DO3_SET, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_1_1, GPIO_PM_OUTPUT_LOW)
	IOM_I2CEXP_INPUT(IO_ID_DO3_FB, 					IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_1_2, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DO3_LED, 				IO_CH_NA, IO_BUS_I2C_0, 0x02,
						I2C_EXP_IOPIN_1_3, GPIO_PM_OUTPUT_INVERT)


	IOM_I2CEXP_INPUT(IO_ID_DI01, 				IOM_CH_DINPUT_1, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_0, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI02, 				IOM_CH_DINPUT_2, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_1, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI03, 				IOM_CH_DINPUT_3, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_2, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI04, 				IOM_CH_DINPUT_4, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_3, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI05, 				IOM_CH_DINPUT_5, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_4, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI06, 				IOM_CH_DINPUT_6, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_5, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI07, 				IOM_CH_DINPUT_7, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_6, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI08, 				IOM_CH_DINPUT_8, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_0_7, GPIO_PM_INPUT_INVERT)

	IOM_I2CEXP_OUTPUT(IO_ID_DI01_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_0, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI02_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_1, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI03_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_2, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI04_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_3, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI05_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_4, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI06_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_5, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI07_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_6, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI08_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x01,
						I2C_EXP_IOPIN_1_7, GPIO_PM_OUTPUT_INVERT)


	IOM_I2CEXP_OUTPUT(IO_ID_DI09_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_0, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI10_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_1, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI11_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_2, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI12_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_3, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI13_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_4, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI14_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_5, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI15_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_6, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_DI16_LED, 			IO_CH_NA, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_0_7, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_INPUT(IO_ID_DI09, 				IOM_CH_DINPUT_9, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_0, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI10, 				IOM_CH_DINPUT_10, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_1, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI11, 				IOM_CH_DINPUT_11, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_2, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI12, 				IOM_CH_DINPUT_12, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_3, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI13, 				IOM_CH_DINPUT_13, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_4, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI14, 				IOM_CH_DINPUT_14, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_5, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI15, 				IOM_CH_DINPUT_15, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_6, GPIO_PM_INPUT_INVERT)
	IOM_I2CEXP_INPUT(IO_ID_DI16, 				IOM_CH_DINPUT_16, IO_BUS_I2C_0, 0x00,
						I2C_EXP_IOPIN_1_7, GPIO_PM_INPUT_INVERT)

	IOM_GPIO_INPUT( IO_ID_SYS_HEALTHY, 		   	    IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_18,   0)
	IOM_GPIO_INPUT( IO_ID_WD_HANDSHAKE, 		   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_19,   0)

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IOM_VIRT_AI( IO_ID_VIRT_ON_ELAPSED_TIME,     	 	IOM_CH_AINPUT_ON_ELAPSED_TIME)

	/* End of table marker */
	IOM_LAST
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
