#!/bin/bash
if [ $# -ne 2 ]
  then
    echo "Usage: ./build.sh PROJECT RELEASE_TYPE. E.g. ./build.sh DSMBoard Release"
    exit -1
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT=$1
BUILD_TYPE=$2
BUILD_DIR=${CURRENT_DIR}/${BUILD_TYPE}/${PROJECT}/

# Clean
rm -rf ${BUILD_DIR}

# Generate makefiles for all applications of the slave board
echo "Creating makefile for "$PROJECT"..."
echo "Directory: "$BUILD_DIR""

ALL_APPS=(${PROJECT} ${PROJECT}/SlaveBootloader ${PROJECT}/AppBootProg)
for APP in "${ALL_APPS[@]}"
do	
	# Get directory for the application.
	APP_BUILD_DIR="${BUILD_DIR}/${APP##*/}"
	mkdir -p ${APP_BUILD_DIR}
	cd ${APP_BUILD_DIR}
	cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} ${CURRENT_DIR}/../${APP}/src/
	RESULT=$?

	# Check error
	if [ $RESULT -ne 0 ]; then
		cd -
		exit $RESULT
	fi
	cd -		
done
