#!/bin/bash
# This script is for building app&bootloader firmware of all slave boards

if [ $# -ne 2 ]
  then
    echo "Usage: ./buildAll.sh OUTPUT_DIR RELEASE_TYPE. E.g. ./buildAll.sh /home/usr/install/ Release"
    exit -1
fi

# Environment variables
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
INSTALL_DIR=$1
BUILD_TYPE=$2
ALL_PROJECTS=(PSMBoard SCMBoard DSMBoard IOMBoard HMIBoard FPMBoardV2)

# Function to check the return code, exit if there is an error.
checkErr(){
RESULT=$?
if [ $RESULT -ne 0 ]; then
	exit $RESULT
fi
}

# Create install dir
mkdir -p ${INSTALL_DIR}
checkErr

# Build slave projects
for PROJECT in "${ALL_PROJECTS[@]}"
do	
	echo "==== Building ${PROJECT} ===="
	bash $CURRENT_DIR/createMakefile.sh $PROJECT ${BUILD_TYPE}	
	checkErr	
	
	bash $CURRENT_DIR/build.sh $PROJECT ${BUILD_TYPE}
	checkErr

	cp -v $CURRENT_DIR/${BUILD_TYPE}/$PROJECT/*AppBootProgPlusBL.bin ${INSTALL_DIR}/
	checkErr

	cp -v $CURRENT_DIR/${BUILD_TYPE}/$PROJECT/$PROJECT/*BoardNXP.bin ${INSTALL_DIR}/
	checkErr
done

printf "All slave board firmware have been installed to:${INSTALL_DIR}/"
exit 0
