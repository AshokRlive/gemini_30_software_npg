#!/bin/bash
if [ $# -ne 2 ]
  then
    echo "Usage: ./build.sh PROJECT RELEASE_TYPE. E.g. ./build.sh DSMBoard Release"
    exit -1
fi

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT=$1
BUILD_TYPE=$2
BUILD_DIR=${CURRENT_DIR}/${BUILD_TYPE}/${PROJECT}/

echo "Building $PROJECT...[${BUILD_TYPE}]"
ALL_APPS=(${PROJECT} ${PROJECT}/SlaveBootloader ${PROJECT}/AppBootProg)
for APP in "${ALL_APPS[@]}"
do	
	APP_BUILD_DIR="${BUILD_DIR}/${APP##*/}"
	echo "Build dir: ${APP_BUILD_DIR}"
	cd ${APP_BUILD_DIR}
	#make install/strip # TODO install not configured yet in cmake script
	make

	# Check error
	RESULT=$?	
	if [ $RESULT -ne 0 ]; then
		cd -
		exit $RESULT
	fi

	# Done
	cd -			
done

echo "Concatenate AppProg and Bootloader..."
CON_TARGET=${BUILD_DIR}/${PROJECT/Board/}AppBootProgPlusBL.bin #Replace only first match ${string/pattern/replacement}
cat $BUILD_DIR/AppBootProg/*AppBootProgNXP.bin $BUILD_DIR/SlaveBootloader/*BootloaderNXP.bin > ${CON_TARGET}
chmod +x $CON_TARGET
echo "AppProgPlusBL is genearted to: ${CON_TARGET}"
echo "${PROJECT} building is done!"
