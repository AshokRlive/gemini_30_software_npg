/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"

/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_OPEN, 		   IO_ID_SW_A_OPEN_INPUT   ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_CLOSED, 	   IO_ID_SW_A_CLOSED_INPUT ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_1,  IO_ID_SW_A_INDICATION_1 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_2,  IO_ID_SW_A_INDICATION_2 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_3,  IO_ID_SW_A_INDICATION_3 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_4,  IO_ID_SW_A_INDICATION_4 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_5,  IO_ID_SW_A_INDICATION_5 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_6,  IO_ID_SW_A_INDICATION_6 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_7,  IO_ID_SW_A_INDICATION_7 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_8,  IO_ID_SW_A_INDICATION_8 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_9,  IO_ID_SW_A_INDICATION_9 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_A_INDICATOR_10,  IO_ID_SW_A_INDICATION_10 ),

	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_OPEN, 		   IO_ID_SW_B_OPEN_INPUT   ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_CLOSED, 	   IO_ID_SW_B_CLOSED_INPUT ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_1,  IO_ID_SW_B_INDICATION_1 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_2,  IO_ID_SW_B_INDICATION_2 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_3,  IO_ID_SW_B_INDICATION_3 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_4,  IO_ID_SW_B_INDICATION_4 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_5,  IO_ID_SW_B_INDICATION_5 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_6,  IO_ID_SW_B_INDICATION_6 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_7,  IO_ID_SW_B_INDICATION_7 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_8,  IO_ID_SW_B_INDICATION_8 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_9,  IO_ID_SW_B_INDICATION_9 ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_SWITCH_B_INDICATOR_10,  IO_ID_SW_B_INDICATION_10 ),

	IOM_IOCHAN_DI(DSM_CH_DINPUT_VMOTOR_SW_A_FB_ON,  IO_ID_VMOTOR_SW_A_FB_ON ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_A_UP_DN,  IO_ID_RELAY_FB_SW_A_UP_DN ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_A_OPEN_UP_DN_CLOSED,  IO_ID_RELAY_FB_SW_A_OPEN_UP_DN_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_A_CLOSE_UP_DN_CLOSED,  IO_ID_RELAY_FB_SW_A_CLOSE_UP_DN_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_A_OPEN_LK_CLOSED,  IO_ID_RELAY_FB_SW_A_OPEN_LK_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_A_CLOSE_LK_CLOSED,  IO_ID_RELAY_FB_SW_A_CLOSE_LK_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_VMOTOR_SW_B_FB_ON,  IO_ID_VMOTOR_SW_B_FB_ON ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_B_UP_DN,  IO_ID_RELAY_FB_SW_B_UP_DN ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_B_OPEN_UP_DN_CLOSED,  IO_ID_RELAY_FB_SW_B_OPEN_UP_DN_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_B_CLOSE_UP_DN_CLOSED,  IO_ID_RELAY_FB_SW_B_CLOSE_UP_DN_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_B_OPEN_LK_CLOSED,  IO_ID_RELAY_FB_SW_B_OPEN_LK_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_B_CLOSE_LK_CLOSED,  IO_ID_RELAY_FB_SW_B_CLOSE_LK_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_A_OP_1_LK_CLOSED,  IO_ID_RELAY_FB_SW_A_OP_1_LK_CLOSED ),
	IOM_IOCHAN_DI(DSM_CH_DINPUT_RELAY_FB_SW_B_OP_1_LK_CLOSED,  IO_ID_RELAY_FB_SW_B_OP_1_LK_CLOSED ),

	IOM_IOCHAN_DI(0, 						                IO_ID_NA)
};

IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),
	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};

IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AI(DSM_CH_AINPUT_TEMPERATURE, 				IO_ID_TEMP_MEASURE, \
				  CAL_ID_NA,		   	IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(DSM_CH_AINPUT_CT_A_I, 					IO_ID_CT_A_I_MEASURE, \
				  CAL_ID_CT_A_I_MEASURE,    			IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(DSM_CH_AINPUT_CT_B_I, 					IO_ID_CT_B_I_MEASURE, \
				  CAL_ID_CT_B_I_MEASURE,    			IO_AI_EVENTRATE_MS),
				  
	IOM_IOCHAN_AI(DSM_CH_AINPUT_ON_ELAPSED_TIME, 				IO_ID_VIRT_ON_ELAPSED_TIME, \
				  CAL_ID_NA,		   					IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, \
			      CAL_ID_NA,    0)
};


IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA),

	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
