/*! \file

 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.

 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/04/14      Venkat_s     Initial version.
 *

 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.

 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*

 * INCLUDES - Standard Library Modules Used

 */


/*

 * INCLUDES - Project Specific Modules Used

 */

#include "lu_types.h"

#include "IOManager.h"

/*

 * EXPORTED - Definitions And Macros

 */

#define BOARD_IO_MAX_IDX (IO_ID_LAST)

/*

 * EXPORTED - Typedefs and Structures

 */

/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**/
	/* Processor Peripheral function pins */
	/**/
	IO_ID_CAN1_RX	                       =0 , //--
	IO_ID_CAN1_TX	                          , //--

	IO_ID_UART_DEBUG_TX	                      , //--
	IO_ID_UART_DEBUG_RX	                      , //--

	IO_ID_LED_SCL                             , //--
	IO_ID_LED_SDA                             , //--

	IO_ID_I2C1_CLK                            , //--
	IO_ID_I2C1_SDA                            , //--

	/* Physically connected IO Pins  */

	IO_ID_LOCAL_LINE                          , //--
	IO_ID_REMOTE_LINE                         , //--

	IO_ID_HWDOG_1HZ_KICK                      , //--

	IO_ID_APPRAM_WEN                          , //--

	IO_ID_LED_CTRL_OK_GREEN                   , //--
	IO_ID_LED_CTRL_OK_RED                     , //--
	IO_ID_LED_CTRL_CAN_GREEN                  , //--
	IO_ID_LED_CTRL_CAN_RED                    , //--
	IO_ID_LED_CTRL_EXTINGUISH                 , //--

	IO_ID_MOD_SEL1							  , //--
	IO_ID_MOD_SEL2							  , //--
	IO_ID_MOD_SEL3							  , //--
	IO_ID_MOD_SEL4							  , //--

	IO_ID_SW_B_OPEN_INPUT				      , //--
	IO_ID_SW_B_CLOSED_INPUT			          , //--
	IO_ID_SW_B_INDICATION_1				      , //--
	IO_ID_SW_B_INDICATION_2				      , //--
	IO_ID_SW_B_INDICATION_3				      , //--
	IO_ID_SW_B_INDICATION_4				      , //--
	IO_ID_SW_B_INDICATION_5				      , //--
	IO_ID_SW_B_INDICATION_6				      , //--
	IO_ID_SW_B_INDICATION_7				      , //--
	IO_ID_SW_B_INDICATION_8				      , //--
	IO_ID_SW_B_INDICATION_9				      , //--
	IO_ID_SW_B_INDICATION_10				      , //--

	IO_ID_SW_A_OPEN_INPUT				      , //--
	IO_ID_SW_A_CLOSED_INPUT			          , //--
	IO_ID_SW_A_INDICATION_1				      , //--
	IO_ID_SW_A_INDICATION_2				      , //--
	IO_ID_SW_A_INDICATION_3				      , //--
	IO_ID_SW_A_INDICATION_4				      , //--
	IO_ID_SW_A_INDICATION_5				      , //--
	IO_ID_SW_A_INDICATION_6				      , //--
	IO_ID_SW_A_INDICATION_7				      , //--
	IO_ID_SW_A_INDICATION_8				      , //--
	IO_ID_SW_A_INDICATION_9				      , //--
	IO_ID_SW_A_INDICATION_10			      , //--

	IO_ID_VMOTOR_SW_B_FB_ON					  , //--
	IO_ID_VMOTOR_SW_A_FB_ON					  , //--

	IO_ID_RELAY_FB_SW_B_OPEN_UP_DN_CLOSED	  , //--
	IO_ID_RELAY_FB_SW_B_OPEN_LK_CLOSED		  , //--
	IO_ID_RELAY_FB_SW_B_CLOSE_UP_DN_CLOSED	  , //--
	IO_ID_RELAY_FB_SW_B_CLOSE_LK_CLOSED		  , //--
	IO_ID_RELAY_FB_SW_B_OP_1_UP_DN_CLOSED	  , //--
	IO_ID_RELAY_FB_SW_B_OP_1_LK_CLOSED		  , //--

	IO_ID_RELAY_FB_SW_A_OPEN_UP_DN_CLOSED	  , //--
	IO_ID_RELAY_FB_SW_A_OPEN_LK_CLOSED		  , //--
	IO_ID_RELAY_FB_SW_A_CLOSE_UP_DN_CLOSED	  , //--
	IO_ID_RELAY_FB_SW_A_CLOSE_LK_CLOSED		  , //--
	IO_ID_RELAY_FB_SW_A_OP_1_UP_DN_CLOSED	  , //--
	IO_ID_RELAY_FB_SW_A_OP_1_LK_CLOSED		  , //--

	IO_ID_RELAY_FB_SW_B_UP_DN				  , //--
	IO_ID_RELAY_FB_SW_A_UP_DN				  , //--

	IO_ID_RELAY_CTRL_VMOTOR_SW_B_A            , //--
	IO_ID_RELAY_CTRL_VMOTOR_SW_B_B            , //--
	IO_ID_SW_B_RELAY_OPEN_UP_DN_COMMAND         , //--
	IO_ID_SW_B_RELAY_OPEN_LK_COMMAND            , //--
	IO_ID_SW_B_RELAY_CLOSE_UP_DN_COMMAND        , //--
	IO_ID_SW_B_RELAY_CLOSE_LK_COMMAND           , //--
	IO_ID_SW_B_RELAY_OP_1_UP_DN_COMMAND         , //--
	IO_ID_SW_B_RELAY_OP_1_LK_COMMAND            , //--

	IO_ID_RELAY_CTRL_VMOTOR_SW_A_A            , //--
	IO_ID_RELAY_CTRL_VMOTOR_SW_A_B            , //--
	IO_ID_SW_A_RELAY_OPEN_UP_DN_COMMAND       , //--
	IO_ID_SW_A_RELAY_OPEN_LK_COMMAND          , //--
	IO_ID_SW_A_RELAY_CLOSE_UP_DN_COMMAND      , //--
	IO_ID_SW_A_RELAY_CLOSE_LK_COMMAND         , //--
	IO_ID_SW_A_RELAY_OP_1_UP_DN_COMMAND       , //--
	IO_ID_SW_A_RELAY_OP_1_LK_COMMAND          , //--

	IO_ID_RLY_CTRL_VMOTOR_SW_0V_B             , //
	IO_ID_RLY_CTRL_VMOTOR_SW_0V_A             , //

	IO_ID_SW_A_PULL_UP_DN					  , //--
	IO_ID_SW_B_PULL_UP_DN                     ,
    IO_ID_PLD_OE						      , //--
    IO_ID_PERM_24V_EN						  , //--

	IO_ID_CT_B_I_MEASURE                      , //
	IO_ID_CT_A_I_MEASURE                      , //

	IO_ID_TEMP_MEASURE                        , //

	IO_ID_LED_SW_B_INPUT_SWITCH_OPEN		  , //--
	IO_ID_LED_SW_B_INPUT_SWITCH_CLOSED		  , //--
	IO_ID_LED_SW_B_OUTPUT_SWITCH_OPENING	  , //--
	IO_ID_LED_SW_B_OUTPUT_SWITCH_CLOSING	  , //--
	IO_ID_LED_SW_B_INDICATION_1               , //--
	IO_ID_LED_SW_B_INDICATION_2               , //--
	IO_ID_LED_SW_B_INDICATION_3               , //--
	IO_ID_LED_SW_B_INDICATION_4               , //--
	IO_ID_LED_SW_B_INDICATION_5               , //--
	IO_ID_LED_SW_B_INDICATION_6               , //--
	IO_ID_LED_SW_B_INDICATION_7               , //--
	IO_ID_LED_SW_B_INDICATION_8				  , //--
	IO_ID_LED_SW_B_INDICATION_9				  ,	//--
	IO_ID_LED_SW_B_INDICATION_10			  ,	//--
	IO_ID_LED_SW_B_MOTOR_OUTPUT		          , //--
	IO_ID_LED_SW_B_OUTPUT_1                   , //--

	/* I2C Expanders */
	IO_ID_LED_SW_A_INPUT_SWITCH_OPEN		  , //--
	IO_ID_LED_SW_A_INPUT_SWITCH_CLOSED		  , //--
	IO_ID_LED_SW_A_OUTPUT_SWITCH_OPENING	  , //--
	IO_ID_LED_SW_A_OUTPUT_SWITCH_CLOSING	  , //--
	IO_ID_LED_SW_A_INDICATION_1               , //--
	IO_ID_LED_SW_A_INDICATION_2               , //--
	IO_ID_LED_SW_A_INDICATION_3               , //--
	IO_ID_LED_SW_A_INDICATION_4               , //--
	IO_ID_LED_SW_A_INDICATION_5               , //--
	IO_ID_LED_SW_A_INDICATION_6               , //--
	IO_ID_LED_SW_A_INDICATION_7               , //--
	IO_ID_LED_SW_A_INDICATION_8				  , //--
	IO_ID_LED_SW_A_INDICATION_9				  , //--
	IO_ID_LED_SW_A_INDICATION_10			  , //--
	IO_ID_LED_SW_A_MOTOR_OUTPUT		          , //--
	IO_ID_LED_SW_A_OUTPUT_1                   , //--



	IO_ID_COL1_GREEN                          , //--
	IO_ID_COL2_RED                            , //--

	IO_ID_SYS_HEALTHY                         , //--
	IO_ID_WD_HANDSHAKE                        , //--

	//IO_ID_IP_LED_PWR_EN                     ,
    IO_ID_LED_PWR_FAULT                      , //--

	IO_ID_TEMP_SEN_ALERT                      , //--

	IO_ID_F_P_I2C_P_E_0_CHECK				  , //
	IO_ID_F_P_I2C_P_E_1_CHECK				  , //
	IO_ID_F_P_I2C_P_E_2_CHECK				  , //

	/* NOVRAM */
	IO_ID_APPLICATION_NVRAM                   , //
	IO_ID_IDENTITY_NVRAM                      , //

	//
	/* Virtual IO ID's go here... */
	//

	IO_ID_VIRT_ON_ELAPSED_TIME                ,

	IO_ID_LAST
} IO_ID;

/*

 * EXPORTED - Variables

 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*

 * EXPORTED - Functions

 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 ***** End of file
 */
