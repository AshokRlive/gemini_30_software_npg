/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Front End PIC]
 *
 *    FILE NAME:
 *               $Id: c_template.h 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Front End PIC SPI attached phase current ADC
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/07/14      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _FRONT_END_PIC_INCLUDED
#define _FRONT_END_PIC_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	FEPIC_ADC_CH_L1         = 0,
	FEPIC_ADC_CH_L2            ,
	FEPIC_ADC_CH_L3            ,
	FEPIC_ADC_CH_SUM           ,
	FEPIC_ADC_CH_SUM_PK        ,

	FEPIC_ADC_CH_LAST
}FEPIC_ADC_CH;

//#pragma pack(1)
typedef struct FEPic2NXPPacketDef
{
	lu_uint8_t			packetCount;
	lu_uint8_t			adcChannel;
	lu_int16_t			adcValue;
	lu_uint16_t         pad;
    lu_uint16_t			crc16;
}FEPic2NXPPacketStr;
//#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _FRONT_END_PIC_INCLUDED */

/*
 *********************** End of file ******************************************
 */
