/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Front End PIC]
 *
 *    FILE NAME:
 *               $Id: c_template.h 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Front End PIC SPI attached phase current ADC
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name          Details
 *   --------------------------------------------------------------------------
 *   28/04/15      saravanan_v   Initial version. Original by bogias_a
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _ADE78XX_CONFIG_INCLUDED
#define _ADE78XX_CONFIG_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* These bits determine the phase voltage together with Phase A current in the power path */
#define ADE78XX_CONFIG_VTOIA_A 		((0UL)) /* Phase A voltage       */
#define ADE78XX_CONFIG_VTOIA_B  	((1UL)) /* Phase B voltage       */
#define ADE78XX_CONFIG_VTOIA_C 		((2UL)) /* Phase C voltage       */
#define ADE78XX_CONFIG_VTOIA_Rsrvd  ((3UL)) /* As if Phase A voltage */

/* These bits determine the phase voltage together with Phase B current in the power path */
#define ADE78XX_CONFIG_VTOIB_B 		((0UL)) /* Phase B voltage */
#define ADE78XX_CONFIG_VTOIB_C		((1UL)) /* Phase C voltage */
#define ADE78XX_CONFIG_VTOIB_A  	((2UL)) /* Phase A voltage */
#define ADE78XX_CONFIG_VTOIB_Rsrvd  ((3UL)) /* As if Phase B voltage */

/* These bits determine the phase voltage together with Phase C current in the power path */
#define ADE78XX_CONFIG_VTOIC_C 		((0UL)) /* Phase C voltage */
#define ADE78XX_CONFIG_VTOIC_A  	((1UL)) /* Phase A voltage */
#define ADE78XX_CONFIG_VTOIC_B  	((2UL)) /* Phase B voltage */
#define ADE78XX_CONFIG_VTOIC_Rsrvd  ((3UL)) /* As if Phase C voltage */

/* These bits determine the whether instantaneous V and I or V, I, P, S and Q or
 * only P, S and Q are transmitted
 */
#define ADE78XX_HSDC_HXFER_VIPSQ		((0UL)) /* Instantaneous V, I, P, S, and Q  */
#define ADE78XX_HSDC_HXFER_VI 			((1UL)) /* Instantaneous V and I            */
#define ADE78XX_HSDC_HXFER_PSQ			((2UL)) /* Instantaneous P, S, and Q        */
#define ADE78XX_HSDC_HXFER_VIPSQ_RSRVD	((3UL)) /* Reserved. Transmits Instantaneous V, I, P, S, and Q */

/*
 *********************************************************************
  Macro defines for CONFIG Register (Address 0xE618)
 **********************************************************************
 */
#define ADE78XX_CONFIG_INTEN			((1UL<<0)) 		 /* Integrator Enable */
#define ADE78XX_CONFIG_SWAP				((1UL<<3)) 		 /* Swaps voltage channel outputs with current channel outputs */
#define ADE78XX_CONFIG_MOD1SHORT		((1UL<<4)) 		 /* Voltage channel as if grounded */
#define ADE78XX_CONFIG_MOD2SHORT		((1UL<<5)) 		 /* Current channel as if grounded */
#define ADE78XX_CONFIG_HSDCEN			((1UL<<6)) 		 /* Enable HSDC serial port */
#define ADE78XX_CONFIG_SWRST			((1UL<<7)) 		 /* Initiates software reset */
#define ADE78XX_CONFIG_VTOIA(n)			(((n&0x03)<<8))  /* These bits determine the phase voltage together with Phase A current in the power path */
#define ADE78XX_CONFIG_VTOIB(n)			(((n&0x03)<<10)) /* These bits determine the phase voltage together with Phase B current in the power path */
#define ADE78XX_CONFIG_VTOIC(n)			(((n&0x03)<<12)) /* These bits determine the phase voltage together with Phase C current in the power path */

/* CONFIG registers  bit mask */
#define ADE78XX_CONFIG_BITMASK			((0x3FF9))

/*
 *********************************************************************
 * Macro defines for HSDC_CFG Register (Address 0xE706)
 *********************************************************************
 */
#define ADE78XX_HSDC_HCLK			((1UL<<0)) 		/* Clock speed: is 4 (or 8 MHz) */
#define ADE78XX_HSDC_HSIZE			((1UL<<1)) 		/* Transmit in: 8 (or 32-bit packets). MSB first. */
#define ADE78XX_HSDC_HGAP			((1UL<<2)) 		/* Add a gap of seven HCKL cycles btwn packets */
#define ADE78XX_HSDC_HXFER(n)		(((n&0x03)<<3)) /* Instantaneous measured and calculated values transmitted */
#define ADE78XX_HSDC_HSAPOL			((1UL<<5)) 		/* HS/HSA output pin is active high */

/* HSDC_CFG registers  bit mask */
#define ADE78XX_HSDC_BITMASK		((0x3F))

/* These bits determine the phase voltage together with Phase A current in the power path */
#define PARAM_ADE78XX_CONFIG_VTOIA(n)   ((n=ADE78XX_CONFIG_VTOIA_A ) || (n=ADE78XX_CONFIG_VTOIA_B) \
									    || (n=ADE78XX_CONFIG_VTOIA_C) || (n=ADE78XX_CONFIG_VTOIA_Rsrvd))

/* These bits determine the phase voltage together with Phase B current in the power path */
#define PARAM_ADE78XX_CONFIG_VTOIB(n)	((n=ADE78XX_CONFIG_VTOIB_A ) || (n=ADE78XX_CONFIG_VTOIB_B) \
										|| (n=ADE78XX_CONFIG_VTOIB_C) || (n=ADE78XX_CONFIG_VTOIB_Rsrvd))

/* These bits determine the phase voltage together with Phase C current in the power path */
#define PARAM_ADE78XX_CONFIG_VTOIC(n)	((n=ADE78XX_CONFIG_VTOIC_A ) || (n=ADE78XX_CONFIG_VTOIC_B) \
										|| (n=ADE78XX_CONFIG_VTOIC_C) || (n=ADE78XX_CONFIG_VTOIC_Rsrvd))

/* These bits determine the whether instantaneous V and I or V, I, P, S and Q or
 * only P, S and Q are transmitted
 */
#define PARAM_ADE78XX_HSDC_HXFER(n)		((n=ADE78XX_HSDC_HXFER_VIPSQ) || (n=ADE78XX_HSDC_HXFER_VI) \
										|| (n=ADE78XX_HSDC_HXFER_PSQ) || (n=ADE78XX_HSDC_HXFER_VI_RSRVD))

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef enum
{
    ADE78XX_HSDC_CH_IAWV = 0,
    ADE78XX_HSDC_CH_VAWV    ,
    ADE78XX_HSDC_CH_IBWV    ,
    ADE78XX_HSDC_CH_VBWV    ,
    ADE78XX_HSDC_CH_ICWV    ,
    ADE78XX_HSDC_CH_VCWV    ,
    ADE78XX_HSDC_CH_INWV    ,

    ADE78XX_HSDC_CH_LAST
}ADE78XX_HSDC_CH;

#pragma pack(1)
typedef struct ade78xxRegCalDef
{
	lu_uint16_t reg;
	lu_uint8_t  size;
	lu_uint32_t calValue;
}ade78xxRegCalStr;
#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


#endif /* _ADE78XX_CONFIG_INCLUDED */

/*
 *********************** End of file ******************************************
 */
