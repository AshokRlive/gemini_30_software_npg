/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief Slave board error codes
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   18/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _ERROR_CODES_INCLUDED
#define _ERROR_CODES_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define GPIO_INVALID_PIN  (0xFFFFFFFF)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    /* Global error codes */
    SB_ERROR_NONE              = 0 ,
    SB_ERROR_PARAM                 ,
    SB_ERROR_INITIALIZED           ,
    SB_ERROR_BUSY                  ,
    SB_ERROR_OFFLINE               ,

    /* CAN Framing specific errors */
    SB_ERROR_CANF                  ,
    SB_ERROR_CANF_MSG_TOO_LONG     ,
    SB_ERROR_CANF_HW_TX_QUEUE_FULL ,
    SB_ERROR_CANF_TX_FRAGMENT_ERROR,
    SB_ERROR_CANF_RX_FRAGMENT_ERROR,
    SB_ERROR_CANF_TX_FRAGMENT      ,
    SB_ERROR_CANF_QUEUE_FULL       ,

    /* CAN Codec specific errors */
    SB_ERROR_CANC                  ,
    SB_ERROR_CANC_QUEUE_FULL       ,
    SB_ERROR_CANC_NOT_HANDLED      ,
	SB_ERROR_CANC_NO_DATA		   ,

    /* IOManger specific errors */
    SB_EEROR_IOMAN_PULSE_STARTED   ,
    SB_ERROR_IOMAN_BOARD_DISABLE   ,

    /* SPI specific errors */
    SB_ERROR_SPI_FAIL              ,

    /* I2C specific errors */
    SB_ERROR_I2C_FAIL              ,
    SB_ERROR_I2C_CRC_FAIL          ,

    /* AD78xx specific errors */
    SB_ERROR_AD78XX_RESET          ,

    /* NVRAM block crc error */
    SB_ERROR_NVRAM_CRC             ,

    /* LCD Driver error */
    SB_ERROR_LCD_FAIL              ,

    /* Flash write error */
    SB_ERROR_WRITE_FAIL			   ,

    /* Calibration Test Error */
    SB_ERROR_CAL_NODATA			   ,
    SB_ERROR_CAL_SEQUENCE		   ,
    SB_ERROR_CAL_LENGTH			   ,
    SB_ERROR_CAL_EXCEED			   ,

    SB_ERROR_CRC                   ,

    SB_ERROR_TIMEOUT               ,

    SB_ERROR_LAST
}SB_ERROR;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


#endif /* _ERROR_CODES_INCLUDED */

/*
 *********************** End of file ******************************************
 */
