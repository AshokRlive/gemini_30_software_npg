#include <embUnit/embUnit.h>
#include "stdio.h"
#include "string.h"

#include "systemTime.h"
#include "IOManager.h"
//#include "CANProtocolFraming.h"
#include "DualFPI.h"



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

// Structures initFPI and intiCANFPI below are used
// to initialise the FPI channel 1 to its appropriate
// phase, earth fault, etc... threshold values
static	FPMConfigStr initFPI = {
				FPI_CONFIG_CH_FPI1,
				LU_TRUE,
				{75,100,20000,1700,1700},
				{50,50,50},
				10500
		};

static	CANFramingMsgStr initCANFPI = {
				0,
				MODULE_MSG_TYPE_CFG,
				MODULE_MSG_ID_CFG_FPM_C,
				0,
				0,
				0,
				0,
				MODULE_MESSAGE_SIZE(FPMConfigStr),
				&initFPI
		};
//DualFPIConfigPtr df = DualFPIConfig;
//	(*df)(&initFPI);
//SB_ERROR DualFPIConfig(FPMConfigStr *msgPtr);
//typedef SB_ERROR (*DualFPIConfigPtr)(FPMConfigStr *msgPtr);

/*
    lu_uint32_t time[60]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59
    };
*/

static void setUp(void)
{
    IOManagerSetValue_Create();
	TimeService_Create();
}

static void tearDown(void)
{
	IOManagerSetValue_Destroy();
	TimeService_Destroy();
}

/****************Tests for DualFPI************************************/
/* Test that confirms the correct initialisation for IOID and IOIDState*/
static void ConfirmInitofIOIDandIOIDState(void)
{
	//printf("%d ", IOManagerSetValueSpy_GetLastIOID());
	//printf("%d ", IOManagerSetValueSpy_GetLastIOIDState());

    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOIDState());
    //printf("\t");
}

/* Test that confirms the correct output is passed out of STGetTime*/
static void ConfirmOutputofSTGetTime(void)
{
	lu_uint8_t i;
    lu_uint32_t time[5]=
    {
      // fake time in ms
       0,1,2,3,4
    };

    for (i = 0; i < 5; i++)
    {
		FakesystemTime_SetMs(time[i]);
    }

    TEST_ASSERT_EQUAL_INT(4, STGetTime());
}//ConfirmOutputofSTGetTime

/* Test that confirms the correct output is passed out of STGetTime*/
static void ConfirmOutputofSTElapsedTime(void)
{
	lu_uint8_t 	i;
    lu_uint32_t startTime = 4;
    lu_uint32_t endTime = 5;

    TEST_ASSERT_EQUAL_INT(1, STElapsedTime(startTime, endTime));
}//ConfirmOutputofSTElapsedTime

/* Test the Overcurrent Detect mechanism*/
static void OvercurrentDetect(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr Params;
	memset(&Params, 0, sizeof(PFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	1699,1700
    	//1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,
    	//1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,1800,
    	//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
    	Params = DualFPIPhaseFaultDetection( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(32,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(1,IOManagerSetValueSpy_GetLastIOIDState());
}//OvercurrentDetect

/* Test the Instant Overcurrent Detected mechanism*/
static void InstantOCDetected(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr Params;
	memset(&Params, 0, sizeof(PFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	1699,1700
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
    	Params = DualFPIPhaseFaultDetection( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.InstantOCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1]);
}//InstantOCDetected

/* Test that resets the overcurrent timer once the phase value falls below the OC threshold*/
static void ResetOCTimer(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr Params;
	memset(&Params, 0, sizeof(PFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	1700,1699
    };

    lu_uint32_t time[2]=
    {
      // fake time in ms
       0,1
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultDetection( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.InitOCTimeMs[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1]);
}//ResetOCTimer

/* Test the Overload Detect mechanism*/
static void OverloadDetect(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr Params;
	memset(&Params, 0, sizeof(PFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	19999,20000
    };

    lu_uint32_t time[2]=
    {
      // fake time in ms
       0,1
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultDetection( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.OLEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1]);
}//OverloadDetect

/* Test the value passed from the STGetime into the initial Overload Time variable*/
static void InitialOverloadTime(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr Params;
	memset(&Params, 0, sizeof(PFParamsStr));

    lu_uint32_t adcValues[3]=
    {
        // Input value ie ADC value
    	19999,19999,20001
    };

    lu_uint32_t time[3]=
    {
      // fake time in ms
       0,10,20
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 3; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultDetection( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.OLEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1]);
    TEST_ASSERT_EQUAL_INT(20,Params.InitOLTimeMs[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1]);
}//InitialOverloadTime

/* Test that resets the overload timer once the phase value falls below the OC threshold*/
static void ResetInitOLTime(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr Params;
	memset(&Params, 0, sizeof(PFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	20000,19999
    };

    lu_uint32_t time[2]=
    {
      // fake time in ms
       0,1
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultDetection( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.InitOLTimeMs[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1]);
}//ResetOLTimer


/* Test the Earth Fault Detect mechanism*/
static void EarthFaultDetect(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	EFParamsStr Params;
	memset(&Params, 0, sizeof(EFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	49,50
    };

    lu_uint32_t time[2]=
    {
      // fake time in ms
       0,1
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultDetection( Params, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.EFEventDetected[FPI_CONFIG_CH_FPI1]);
}//Earth Fault Detect

/* Test the Instant Earth Fault Detect mechanism*/
static void InstantEarthFaultDetect(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	EFParamsStr Params;
	memset(&Params, 0, sizeof(EFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	49,50
    };

    lu_uint32_t time[2]=
    {
      // fake time in ms
       0,1
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultDetection( Params, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.InstantEFEventDetected[FPI_CONFIG_CH_FPI1]);
}//Instant Earth Fault Detect

/* Test the Persistent Earth Fault Detect mechanism*/
static void PersistentEarthFaultDetect(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	EFParamsStr Params;
	memset(&Params, 0, sizeof(EFParamsStr));

    lu_uint32_t adcValues[2]=
    {
        // Input value ie ADC value
    	49,50
    };

    lu_uint32_t time[2]=
    {
      // fake time in ms
       0,1
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 2; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultDetection( Params, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.PersistentEFMs[FPI_CONFIG_CH_FPI1]);
}//PersistentEarthFaultDetect

/* Test the reseting of the earth fault timer*/
static void TrigerEarthFaultTimerThenReset(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	EFParamsStr Params;
	memset(&Params, 0, sizeof(EFParamsStr));

    lu_uint32_t adcValues[13]=
    {
        // Input value ie ADC value
    	49,50,49,49,49,49,49,49,49,49,49,49,49
    };

    lu_uint32_t time[13]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 13; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultDetection( Params, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.InitEFTimeMs[FPI_CONFIG_CH_FPI1]);
}//TrigerEarthFaultTimerThenReset

/* Test the persistent Earth Fault Detect mechanism*/
static void SecondEarthFaultDetect(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	EFParamsStr Params;
	memset(&Params, 0, sizeof(EFParamsStr));

    lu_uint32_t adcValues[12]=
    {
        // Input value ie ADC value
    	49,50,49,49,49,49,49,49,49,49,50,49
    };

    lu_uint32_t time[12]=
    {
    	// fake time in ms
    	0,1,2,3,4,5,6,7,8,9,10,11
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 12; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultDetection( Params, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(1,Params.PersistentEFDurationMs[FPI_CONFIG_CH_FPI1]);
}//SecondEarthFaultDetect

/* Test the Earth Fault Detect and then reset*/
static void SecondEarthFaultDetectThenReset(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	EFParamsStr Params;
	memset(&Params, 0, sizeof(EFParamsStr));

    lu_uint32_t adcValues[22]=
    {
        // Input value ie ADC value
    	49,50,49,49,49,49,49,49,49,49,50,49,49,49,49,49,49,49,49,49,49,49
    };

    lu_uint32_t time[22]=
    {
    	// fake time in ms
    	0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21
    };

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 22; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultDetection( Params, FPI_CONFIG_CH_FPI1, &adcValues[i]);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.PersistentEFDurationMs[FPI_CONFIG_CH_FPI1]);
    TEST_ASSERT_EQUAL_INT(0,Params.InitEFTimeMs[FPI_CONFIG_CH_FPI1]);
}//SecondEarthFaultDetectThenReset

/* Test the Overcurrent Fault alarm mechanism*/
static void OCFaultAlarm(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise Overcurrent event
    Params.pf->OCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 76; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT( 75, (Params.pf->InitTimeSinceOCFaultMs[FPI_CONFIG_CH_FPI1]));
}//OCFaultAlarm

/* Test the Overcurrent Fault alarm IOID and IOID State change mechanism*/
static void OCFaultAlarmIOIDandIOIDState(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise Overcurrent event
    Params.pf->OCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 80; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(66,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(1,IOManagerSetValueSpy_GetLastIOIDState());
}//OCFaultAlarmIOIDandIOIDState

/* Test the Instant Overcurrent Fault alarm mechanism*/
static void InstantOCFaultAlarm(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

    lu_uint32_t time[1]=
    {
      // fake time in ms
       1
    };

    // Initialise Instant Overcurrent event
    Params.pf->InstantOCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 1; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT( 1, (Params.pf->InitTimeSinceInstantOCFaultMs[FPI_CONFIG_CH_FPI1]));
}//InstantOCFaultAlarm

/* Test the Instant Overcurrent Fault alarm IOID and IOID State change mechanism*/
static void InstantOCFaultAlarmIOIDandIOIDState(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

    lu_uint32_t time[1]=
    {
      // fake time in ms
       1
    };

    //Initialise the instant Overcurrent event
    Params.pf->InstantOCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 1; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(67,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(1,IOManagerSetValueSpy_GetLastIOIDState());
}//InstantOCFaultAlarmIOIDandIOIDState

/* Test the Overload Fault alarm mechanism*/
static void OLFaultAlarm(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InitOLTimeMs set below
	//is used to calculate the overload duration
	//otherwise STElpasedTime overflows
    lu_uint32_t time[119]=
    {
    	// fake time in ms
    	1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
    	20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
    	40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
    	60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,
    	80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
    	100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119
    };

    // Initialise Instant Overload event
    Params.pf->InitOLTimeMs[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 119; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT( 101, (Params.pf->InitTimeSinceOCFaultMs[FPI_CONFIG_CH_FPI1]));
}//OLFaultAlarm

/* Test the Overload Fault alarm IOID and IOID State change mechanism*/
static void OLFaultAlarmIOIDandIOIDState(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InitOLTimeMs set below
	//is used to calculate the overload duration
	//otherwise STElpasedTime overflows
    lu_uint32_t time[119]=
    {
    	// fake time in ms
    	1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
    	20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
    	40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
    	60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,
    	80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,
    	100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119
    };

    // Initialise Instant Overload event
    Params.pf->InitOLTimeMs[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 119; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(66,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(1,IOManagerSetValueSpy_GetLastIOIDState());
}//OLFaultAlarmIOIDandIOIDState

/* Test the reseting timer mechanism for the overcurrent fault*/
static void ResetOCFault(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InitTimeSinceOCFaultMs set below is
	//used to calculate the overload duration otherwise STElpasedTime overflows.
	//500ms increments used for speed
    lu_uint32_t time[22]=
    {
    	// fake time in ms
    	500,1000,1500,2000,2500,3000,3500,4000,4500,
    	5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,
    	10000,10500,11000
    };

    // Initialise Overcurrent event timer
    Params.pf->InitTimeSinceOCFaultMs[FPI_CONFIG_CH_FPI1] = 500;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 22; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.TimeSinceOCFaultAlarmMs[FPI_CONFIG_CH_FPI1]);
}//ResetOCFault

/* Test the reseting timer mechanism for the instant overcurrent fault*/
static void ResetInstantOCFault(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InitTimeSinceOCFaultMs set below is
	//used to calculate the overload duration otherwise STElpasedTime overflows.
	//500ms increments used for speed
    lu_uint32_t time[22]=
    {
    	// fake time in ms
    	500,1000,1500,2000,2500,3000,3500,4000,4500,
    	5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,
    	10000,10500,11000
    };

    // Initialise the instant Overcurrent event timer
    Params.pf->InitTimeSinceInstantOCFaultMs[FPI_CONFIG_CH_FPI1] = 500;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 22; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.TimeSinceInstantOCFaultAlarmMs[FPI_CONFIG_CH_FPI1]);
}//ResetInstantOCFault

/* Test the Earth Fault alarm mechanism*/
static void EarthFaultAlarm(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise earth fault event
    Params.ef->EFEventDetected[FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 51; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT( 50, (Params.ef->InitTimeSinceEFMs[FPI_CONFIG_CH_FPI1]));
}//EarthFaultAlarm

/* Test the Earth Fault alarm IOID and IOID State change mechanism*/
static void EarthFaultAlarmIOIDandIOIDState(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise earth fault event
    Params.ef->EFEventDetected[FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 51; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(68,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(1,IOManagerSetValueSpy_GetLastIOIDState());
}//EarthFaultAlarmIOIDandIOIDState

/* Test the Instant Earth Fault alarm mechanism*/
static void InstantEarthFaultAlarm(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InstantEFEventDetected set below is
	//used to calculate the overload duration otherwise STElpasedTime overflows.
    lu_uint32_t time[79]=
    {
      // fake time in ms
       1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise earth fault event
    Params.ef->InstantEFEventDetected[FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 1; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT( 1, (Params.ef->InitTimeSinceInstantEFMs[FPI_CONFIG_CH_FPI1]));
}//InstantEarthFaultAlarm

/* Test the Instant Earth Fault alarm IOID and IOID State change mechanism*/
static void InstantEarthFaultAlarmIOIDandIOIDState(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InstantEFEventDetected set below is
	//used to calculate the overload duration otherwise STElpasedTime overflows.
    lu_uint32_t time[79]=
    {
      // fake time in ms
       1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise earth fault event
    Params.ef->InstantEFEventDetected[FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 1; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(69,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(1,IOManagerSetValueSpy_GetLastIOIDState());
}//InstantEarthFaultAlarmIOIDandIOIDState

/* Test the reseting timer mechanism for the earth fault*/
static void ResetEarthFault(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));


	//Has to start from non zero as InitTimeSinceEFMs set below is
	//used to calculate the overload duration otherwise STElpasedTime overflows.
	//500ms increments used for speed
    lu_uint32_t time[22]=
    {
    	// fake time in ms
    	500,1000,1500,2000,2500,3000,3500,4000,4500,
    	5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,
    	10000,10500,11000
    };

    // Initialise the earth event timer
    Params.ef->InitTimeSinceEFMs[FPI_CONFIG_CH_FPI1] = 500;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 22; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.TimeSinceEFAlarmMs[FPI_CONFIG_CH_FPI1]);
}//ResetEarthFault

/* Test the reseting timer mechanism for the instant earth fault*/
static void ResetInstantEarthFault(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	//Has to start from non zero as InitTimeSinceEFMs set below is
	//used to calculate the overload duration otherwise STElpasedTime overflows.
	//500ms increments used for speed
    lu_uint32_t time[22]=
    {
    	// fake time in ms
    	500,1000,1500,2000,2500,3000,3500,4000,4500,
    	5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,
    	10000,10500,11000
    };

    // Initialise the instant earth event timer
    Params.ef->InitTimeSinceInstantEFMs[FPI_CONFIG_CH_FPI1] = 500;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 22; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    TEST_ASSERT_EQUAL_INT(0,Params.TimeSinceInstantEFAlarmMs[FPI_CONFIG_CH_FPI1]);
}//ResetInstantEarthFault

/* Test the Overcurrent Fault alarm IOID and IOID State change mechanism
 * FPM_Basic_Tests_Results
 * \gemini_30_test\G3\Test\FPM_Omicron Test Scripts
 * Test 1.1
 * Phase fault alarm Duration 80ms
 * Actual fault duration 55ms
 * */
static void OCFaultAlarm_1_1_VS(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	// Structures initFPI and intiCANFPI below are used
	// to initialise the FPI channel 1 to its appropriate
	// phase, earth fault, etc... threshold values
	FPMConfigStr initFPI = {
			FPI_CONFIG_CH_FPI1,
			LU_TRUE,
			{80,100,20000,1700,1700},
			{50,50,50},
			10500
	};

	CANFramingMsgStr initCANFPI = {
			0,
			MODULE_MSG_TYPE_CFG,
			MODULE_MSG_ID_CFG_FPM_C,
			0,
			0,
			0,
			0,
			MODULE_MESSAGE_SIZE(FPMConfigStr),
			&initFPI
	};

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise Overcurrent event
    Params.pf->OCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 56; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    //Check for NON operation
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOIDState());
}//OCFaultAlarm_1_1_VS

/* Test the Overcurrent Fault alarm IOID and IOID State change mechanism
 * FPM_Basic_Tests_Results
 * \gemini_30_test\G3\Test\FPM_Omicron Test Scripts
 * Test 1.1
 * Phase fault alarm Duration 150ms
 * Actual fault duration 55ms
 * */
static void OCFaultAlarm_1_2_VS(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	// Structures initFPI and intiCANFPI below are used
	// to initialise the FPI channel 1 to its appropriate
	// phase, earth fault, etc... threshold values
	FPMConfigStr initFPI = {
			FPI_CONFIG_CH_FPI1,
			LU_TRUE,
			{150,100,20000,1700,1700},
			{50,50,50},
			10500
	};

	CANFramingMsgStr initCANFPI = {
			0,
			MODULE_MSG_TYPE_CFG,
			MODULE_MSG_ID_CFG_FPM_C,
			0,
			0,
			0,
			0,
			MODULE_MESSAGE_SIZE(FPMConfigStr),
			&initFPI
	};

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise Overcurrent event
    Params.pf->OCEventDetected[FEPIC_ADC_CH_L1][FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 56; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIPhaseFaultAlarm( Params, FEPIC_ADC_CH_L1, FPI_CONFIG_CH_FPI1);
    }

    //Check for NON operation
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOIDState());
}//OCFaultAlarm_1_2_VS

/* Test the Earth Fault alarm IOID and IOID State change mechanism
 * FPM_Basic_Tests_Results
 * \gemini_30_test\G3\Test\FPM_Omicron Test Scripts
 * Test 1.1
 * Earth fault alarm Duration 50ms
 * Actual fault duration 38ms
 * */
static void EarthFaultAlarm_1_1_VS(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	// Structures initFPI and intiCANFPI below are used
	// to initialise the FPI channel 1 to its appropriate
	// phase, earth fault, etc... threshold values
	FPMConfigStr initFPI = {
			FPI_CONFIG_CH_FPI1,
			LU_TRUE,
			{75,100,20000,1700,1700},
			{50,50,50},
			10500
	};

	CANFramingMsgStr initCANFPI = {
			0,
			MODULE_MSG_TYPE_CFG,
			MODULE_MSG_ID_CFG_FPM_C,
			0,
			0,
			0,
			0,
			MODULE_MESSAGE_SIZE(FPMConfigStr),
			&initFPI
	};

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise earth fault event
    Params.ef->EFEventDetected[FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 39; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    //Check for NON operation
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOIDState());
}//EarthFaultAlarm_1_1_VS

/* Test the Earth Fault alarm IOID and IOID State change mechanism
 * FPM_Basic_Tests_Results
 * \gemini_30_test\G3\Test\FPM_Omicron Test Scripts
 * Test 1.2
 * Earth fault alarm Duration 100ms
 * Actual fault duration 80ms
 * */
static void EarthFaultAlarm_1_2_VS(void)
{
	lu_uint8_t i = 0;
	SB_ERROR retError;
	PFParamsStr ParamsPF;
	EFParamsStr ParamsEF;
	FAParamsStr Params;

	memset(&Params, 0, sizeof(FAParamsStr));

	Params.pf = &ParamsPF;
	Params.ef = &ParamsEF;

	memset((Params.pf), 0, sizeof(PFParamsStr));
	memset((Params.ef), 0, sizeof(EFParamsStr));

	// Structures initFPI and intiCANFPI below are used
	// to initialise the FPI channel 1 to its appropriate
	// phase, earth fault, etc... threshold values
	FPMConfigStr initFPI = {
			FPI_CONFIG_CH_FPI1,
			LU_TRUE,
			{75,100,20000,1700,1700},
			{50,50,50},
			10500
	};

	CANFramingMsgStr initCANFPI = {
			0,
			MODULE_MSG_TYPE_CFG,
			MODULE_MSG_ID_CFG_FPM_C,
			0,
			0,
			0,
			0,
			MODULE_MESSAGE_SIZE(FPMConfigStr),
			&initFPI
	};

    lu_uint32_t time[80]=
    {
      // fake time in ms
       0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
       20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
       40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
       60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79
    };

    //Initialise earth fault event
    Params.ef->EFEventDetected[FPI_CONFIG_CH_FPI1] = 1;

    // Initialise FPI channel 1
    retError = DualFPICANProtocolDecoder(&initCANFPI,0);

    for (i = 0; i < 39; i++)
    {
        FakesystemTime_SetMs(time[i]);
    	Params = DualFPIEarthFaultAlarm( Params, FPI_CONFIG_CH_FPI1);
    }

    //Check for NON operation
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOID());
    TEST_ASSERT_EQUAL_INT(-1,IOManagerSetValueSpy_GetLastIOIDState());
}//EarthFaultAlarm_1_2_VS



/* Test Suite*/
TestRef DualFPITest_tests(void)
{
    EMB_UNIT_TESTFIXTURES(fixtures)
    {
        /* Test Cases*/

        new_TestFixture("ConfirmInitofIOIDandIOIDState",ConfirmInitofIOIDandIOIDState),
		new_TestFixture("ConfirmOutputofSTGetTime",ConfirmOutputofSTGetTime),
		new_TestFixture("ConfirmOutputofSTElapsedTime",ConfirmOutputofSTElapsedTime),
		new_TestFixture("OvercurrentDetect - PhaseFaultDetect",OvercurrentDetect),
		new_TestFixture("InstantOCDetected - PhaseFaultDetect",InstantOCDetected),
		new_TestFixture("ResetOCTimer - PhaseFaultDetect",ResetOCTimer),
		new_TestFixture("OverloadDetect - PhaseFaultDetect",OverloadDetect),
		new_TestFixture("InitialOverloadTime - PhaseFaultDetect",InitialOverloadTime),
		new_TestFixture("ResetInitOLTime - PhaseFaultDetect",ResetInitOLTime),
		new_TestFixture("EarthFaultDetect - EarthFaultDetect",EarthFaultDetect),
		new_TestFixture("InstantEarthFaultDetect - EarthFaultDetect",InstantEarthFaultDetect),
		new_TestFixture("PersistentEarthFaultDetect - EarthFaultDetect",PersistentEarthFaultDetect),
		new_TestFixture("TrigerEarthFaultTimerThenReset - EarthFaultDetect",TrigerEarthFaultTimerThenReset),
		new_TestFixture("SecondEarthFaultDetect - EarthFaultDetect",SecondEarthFaultDetect),
		new_TestFixture("SecondEarthFaultDetectThenReset - EarthFaultDetect",SecondEarthFaultDetectThenReset),
		new_TestFixture("OCFaultAlarm - PhaseFaultAlarm",OCFaultAlarm),
		new_TestFixture("OCFaultAlarmIOIDandIOIDState - PhaseFaultAlarm",OCFaultAlarmIOIDandIOIDState),
		new_TestFixture("InstantOCFaultAlarm - PhaseFaultAlarm",InstantOCFaultAlarm),
		new_TestFixture("InstantOCFaultAlarmIOIDandIOIDState - PhaseFaultAlarm",InstantOCFaultAlarmIOIDandIOIDState),
		new_TestFixture("OLFaultAlarm - PhaseFaultAlarm",OLFaultAlarm),
		new_TestFixture("OLFaultAlarmIOIDandIOIDState - PhaseFaultAlarm",OLFaultAlarmIOIDandIOIDState),
		new_TestFixture("ResetOCFault - PhaseFaultAlarm",ResetOCFault),
		new_TestFixture("ResetInstantOCFault - PhaseFaultAlarm",ResetInstantOCFault),
		new_TestFixture("EarthFaultAlarm - EarthFaultAlarm",EarthFaultAlarm),
		new_TestFixture("EarthFaultAlarmIOIDandIOIDState - EarthFaultAlarm",EarthFaultAlarmIOIDandIOIDState),
		new_TestFixture("InstantEarthFaultAlarm - EarthFaultAlarm",InstantEarthFaultAlarm),
		new_TestFixture("InstantEarthFaultAlarmIOIDandIOIDState - EarthFaultAlarm",InstantEarthFaultAlarmIOIDandIOIDState),
		new_TestFixture("ResetEarthFault - EarthFaultAlarm",ResetEarthFault),
		new_TestFixture("ResetInstantEarthFault - EarthFaultAlarm",ResetInstantEarthFault),
		new_TestFixture("OCFaultAlarm_1_1_VS - PhaseFaultAlarm",OCFaultAlarm_1_1_VS),
		new_TestFixture("OCFaultAlarm_1_2_VS - PhaseFaultAlarm",OCFaultAlarm_1_2_VS),
		new_TestFixture("EarthFaultAlarm_1_1_VS - EarthFaultAlarm",EarthFaultAlarm_1_1_VS),
		new_TestFixture("EarthFaultAlarm_1_2_VS - EarthFaultAlarm",EarthFaultAlarm_1_2_VS),
//		new_TestFixture("ResetInstantEarthFault - EarthFaultAlarm",ResetInstantEarthFault),
//		new_TestFixture("ResetInstantEarthFault - EarthFaultAlarm",ResetInstantEarthFault),
//		new_TestFixture("ResetInstantEarthFault - EarthFaultAlarm",ResetInstantEarthFault),

        // ... more test cases
    };
    EMB_UNIT_TESTCALLER(DualFPITest,"DualFPITest",setUp,tearDown,fixtures);
    return (TestRef)&DualFPITest;
}
