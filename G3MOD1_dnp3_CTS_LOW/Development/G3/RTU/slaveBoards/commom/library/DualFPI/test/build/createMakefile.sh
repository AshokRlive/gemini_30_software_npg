#!/bin/sh

# Remove Debug directory
rm -rf Debug

# Create Debug directory
mkdir Debug

# Run cmake and create Debug Makefiles
cd Debug
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ../
cd ..

