/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <string.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolFraming.h"
#include "CANProtocol.h"
#include "ModuleProtocol.h"

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_can.h"

#include "lpc17xx_clkpwr.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* CAN LEDs */
#define CAN_LED_GREEN_PORT							1
#define CAN_LED_GREEN_PIN							(1 <<28)
#define CAN_LED_RED_PORT							1
#define CAN_LED_RED_PIN								(1 <<29)

/* Increment and wrap. Optimised version: works ONLY with multiple of 2 values */
#define CAN_INCREMENT_AND_WRAP(a, mask) ((a) = (((a)+ 1) & mask))

/* CAN controller buffer layout */

/* The buffer length MUST be a multiple of 2. We use the CAN_INCREMENT_AND_WRAP
 * optimised macro.16 buffers: 0 -15
 */
#define CAN_RX_BUF_MASK       (0x0F)
#define CAN_RX_BUF_LEN        (CAN_RX_BUF_MASK + 1)

#define CAN_TX_BUF_MASK       (0xFF)
#define CAN_TX_BUF_LEN        (CAN_TX_BUF_MASK + 1)

#define CAN_BASE(a)        ((LPC_CAN_TypeDef*)(a))

#define CAN_SHORT_MSG_SLOT    1
#define CAN_FRAGMENT_MSG_SLOT 2

/* Fragmentation Data */
#define CAN_FRAGMENT_HEADER_SIZE    (sizeof(CANFragmentHeaderStr))
#define CAN_FRAGMENT_PAYLOAD        (7)
#define CAN_FRAGMENT_ID_MASK        (0x3F) /* 6 bits */
#define CAN_FRAGMENT_ID             (0xFF)

/* Array size to track fragment numbers for duplicate rejection */
#define CAN_FRAGMENT_ID_MASK_BITS	32
#define CAN_FRAGMENT_ID_MASK_SIZE	((CAN_MAX_FRAGMENTS + CAN_FRAGMENT_ID_MASK_BITS) / CAN_FRAGMENT_ID_MASK_BITS)

/* Check if fragment id is set in mask */
#define CAN_FRAGMENT_ID_MASK_IS_NOT_SET(mask, id) 	( !(mask[(id >> 5) & CAN_FRAGMENT_ID_MASK] & (1 << (id & 0x1f)) ) )

/* Set fragment id in mask */
#define CAN_FRAGMENT_ID_MASK_SET(mask, id) 			( mask[(id >> 5) & CAN_FRAGMENT_ID_MASK] |= (1 << (id & 0x1f)) )

/* Get next fragment ID. Only one fragmented message at a time can be sent.
 * This doesn't need to be atomic
 */
#define CAN_FRAGMENT_NEXT_ID(a) (CAN_INCREMENT_AND_WRAP(a, CAN_FRAGMENT_ID_MASK))

#define UPDATE_STATS(a) ((CANFramingData.stats.a)++)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    CAN_FRAGMENTATION_STATUS_IDLE = 0,
    CAN_FRAGMENTATION_STATUS_START   ,
    CAN_FRAGMENTATION_STATUS_CONT    ,
    CAN_FRAGMENTATION_STATUS_END     ,

    CAN_FRAGMENTATION_STATUS_LAST
}CAN_FRAGMENTATION_STATUS;

/*!
 * CAN Framing statistics
 */
typedef struct CANFramingStatDef
{
    lu_uint32_t canError          ;

    lu_uint32_t	canBusError;
    lu_uint32_t canArbitrationError;
    lu_uint32_t canDataOverrun;
    lu_uint32_t canBusOff;
    lu_uint32_t canErrorPassive;
    lu_uint32_t canErrorBTR;

    /* TX */
    lu_uint32_t TXShortMessageReq ;
    lu_uint32_t TXLongMessageReq  ;
    lu_uint32_t TXLongMessage     ;
    lu_uint32_t TXTotal           ;
    lu_uint32_t TXError           ;
    lu_uint32_t TXFragmentBusy    ;
    lu_uint32_t TXFragmentError   ;
    lu_uint32_t TXTooLong         ;

    /* RX */
    lu_uint32_t RXMessageTotal    ;
    lu_uint32_t RXLongMessageTotal;
    lu_uint32_t RXMessageLost     ;
    lu_uint32_t RXFragmentError   ;

    lu_uint32_t icr;
    lu_uint32_t gsr;
    lu_uint32_t mod;
    lu_uint32_t btr;
    lu_uint32_t ier;

}CANFramingStatStr;

/*!
 * TX Fragmentation State Machine Control Data
 */
typedef struct CANTXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Fragment ID counter */
    lu_uint8_t counter;

    /*! Bytes already sent */
    lu_uint32_t sentData;

    /*! ObjectID (1-32) of the last sent packet */
    lu_int32_t objID;

    /* CAN message Object */
    CAN_MSG_Type CANMsgObject;

    /*! Total length of the buffer */
    lu_uint32_t bufferLen;
    /*! Local TX buffer */
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
}CANTXFragmentationStr;

/*!
 * RX Fragmentation State Machine Control Data
 */
typedef struct CANRXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Expected fragment ID */
    lu_uint8_t fragmentID;
    /*! received data */
    lu_uint32_t recvData;
    /*! Expected CAN header */
    lu_uint32_t expectedHeader;

    /* Count total numb fragments received */
    lu_uint32_t fragmentCount;
    /* Track fragments received to reject duplicates */
    lu_uint32_t fragmentIDMask[CAN_FRAGMENT_ID_MASK_SIZE];

    /*! G3 CAN message structure */
    CANFramingMsgStr  CANFramingMsg;
    /*! Local buffer */
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
}CANRXFragmentationStr;

/*!
 * RX short message pool
 */
typedef struct msgPoolDef
{
    /*! Buffer status */
    lu_bool_t        free;
    /*! G3 CAN message structure */
    CANFramingMsgStr msg;
    /*! Local Buffer */
    lu_uint8_t       msgBuf[CAN_MAX_PAYLOAD];
}msgPoolStr;

/*!
 * TX message pool
 */
typedef struct TXmsgPoolDef
{
    CAN_MSG_Type msgRingBuffer[CAN_TX_BUF_LEN];
    lu_uint32_t readIdx;
    lu_uint32_t writeIdx;
}TXmsgPoolStr;

/*!
 * Library control data
 */
typedef struct CANFramingDef
{
    /*! Library status */
    lu_bool_t   initialzied;
    /*! Device type */
    lu_uint8_t  device     ;
    /*! Device address */
    lu_uint8_t  deviceID   ;

    /*! Pins Map */
    CANFramingMapStr *pinMapPtr;

    /*! ID Mask */
    lu_uint32_t msgIDMask;

    /*! Receive Message Pool */
    msgPoolStr RXMsgPool[CAN_RX_BUF_LEN];

    /* Transmit message queue */
    TXmsgPoolStr TXMsgQueue;//MG

    /*! Receive buffer ID */
    lu_uint32_t rxIdx;

    /*! Fragmentation Control data (TX) */
    CANTXFragmentationStr TXFragment;

    /*! Fragmentation Control data (RX) */
    CANRXFragmentationStr RXFragment;

    /*! Forward data callback */
    CANFramingCB callBack   ;

    /*! Statistics */
    CANFramingStatStr stats;

    /* */
    lu_uint32_t busErrorTimerMs;

    lu_uint32_t canLedFlashTimerMs;

    lu_uint32_t btrReportTimerMs;
}CANFramingStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                          CAN_MSG_Type *CANMsgTxPtr,
                          lu_bool_t fragment
                        );

void initHeaderCANFMsg( CANFramingMsgStr *msgPtr,
                        CANHeaderStr *CANHeaderPtr
                      );

//static SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr);
//static SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID);
static SB_ERROR CANFramingFillFragment(void);
static SB_ERROR CANFramingRecvFragment(CAN_MSG_Type *CANMsgPtr);
static SB_ERROR CANFramingRecvFragmentRelease(CANFramingMsgStr *msgPtr);

static SB_ERROR CANFramingInitTXMsgPool(void);
static CAN_MSG_Type* CANFramingGetTXMsg(void);
static SB_ERROR CANFramingAddMsg(CAN_MSG_Type *msgPtr);

void CANFramingIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANFramingStr CANFramingData;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_uint32_t CANFramingGetRXMessageTotal(void)
{
	return CANFramingData.stats.RXMessageTotal;
}

lu_uint32_t CANFramingGetRXMessageLost(void)
{
	return CANFramingData.stats.RXMessageLost;
}

lu_uint32_t CANFramingGetcanError(void)
{
	return CANFramingData.stats.canError;
}

lu_uint32_t CANFramingGetICR(void)
{
	return CANFramingData.stats.icr;
}

lu_uint32_t CANFramingGetIER(void)
{
	return CANFramingData.stats.ier;
}

lu_uint32_t CANFramingGetGSR(void)
{
	return CANFramingData.stats.gsr;
}

lu_uint32_t CANFramingGetMOD(void)
{
	return CANFramingData.stats.mod;
}

lu_uint32_t CANFramingGetBTR(void)
{
	return CANFramingData.stats.btr;
}

lu_uint32_t CANFramingGetcanBusError(void)
{
	return CANFramingData.stats.canBusError;
}

lu_uint32_t CANFramingGetcanArbitrationError(void)
{
	return CANFramingData.stats.canArbitrationError;
}

lu_uint32_t CANFramingGetcanDataOverrun(void)
{
	return CANFramingData.stats.canDataOverrun;
}

lu_uint32_t CANFramingGetcanBusOff(void)
{
	return CANFramingData.stats.canBusOff;
}

lu_uint32_t CANFramingGetcanErrorPassive(void)
{
	return CANFramingData.stats.canErrorPassive;
}

lu_uint32_t CANFramingGetcanErrorBTR(void)
{
	return CANFramingData.stats.canErrorBTR;
}

lu_uint32_t CANFramingGetTXTotal(void)
{
	return CANFramingData.stats.TXTotal;
}

lu_uint32_t CANFramingGetRxLost(void)
{
	return CANFramingData.stats.RXMessageLost;
}

SB_ERROR CANFramingInit( lu_uint32_t       clock    ,
                         lu_uint32_t       bitRate  ,
		                 lu_uint8_t        device   ,
		                 lu_uint8_t        deviceID ,
		                 CANFramingMapStr *pinMapPtr,
		                 SUIRQPriorityStr  priority ,
		                 CANFramingCB      callBack
		               )
{

}

SB_ERROR CANFramingTick(void)
{

}


SB_ERROR CANFramingPulseCANLed(void)
{

}

SB_ERROR CANFramingAddFilter( const filterTableStr *filterTable,
                              lu_uint32_t size
                            )
{

}

SB_ERROR CANFramingFilterEnable(lu_bool_t enable)
{

}

SB_ERROR CANFramingReleaseMsg(CANFramingMsgStr *msgPtr)
{

}

SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr)
{

}

void CANFramingEnterBusOff(void)
{

}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize CAN message using a G3 CAN Message
 *
 *   \param msgPtr G3 CAN Message used to initialize the CAN object
 *   \param CANMsgTxPtr CAN message to initialize
 *   \param fragment set to LU_TRUE if this is a fragmented message
 *
 *   \return None
 *
 ******************************************************************************
 */
inline void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                                 CAN_MSG_Type *CANMsgTxPtr,
                                 lu_bool_t fragment
                               )
{

}

/*!
 ******************************************************************************
 *   \brief Initialize G3 CAN Framing Message using a CAN object
 *
 *   \param msgPtr CAN object used to initialize the G3 CAN Message
 *   \param MsgObjectTxPtr G3 CAN Message to initialize
 *
 *   \return None
 *
 ******************************************************************************
 */
inline void initHeaderCANFMsg( CANFramingMsgStr *msgPtr,
                               CANHeaderStr *CANHeaderPtr
                             )
{

}

/*!
 ******************************************************************************
 *   \brief Start the transmission of a "long" message
 *
 *   Only one long message at a time can be transmitted. The first fragment
 *   is sent immediately. The successive fragments are automatically sent
 *   in the ISR when the transmission of the previous message is terminated.
 *
 *   \param msgPtr Object to send
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr)
{
	SB_ERROR ret;
	return ret;
}

/*!
 ******************************************************************************
 *   \brief Continue the transmission of the "long" message
 *
 *   This function should be called directly from the ISR. A new fragment
 *   is sent only when a previous fragment has been successfully sent. The
 *   objID is used to identify the transmitted object
 *
 *   \param objID Id of the object that has just been transmitted (1-32)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID)
{

}

/*!
 ******************************************************************************
 *   \brief Prepare a new fragment to send
 *
 *   The buffer of the fragment CAN message is filled with new data.
 *   If necessary the State Machine status is also updated
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingFillFragment(void)
{

}

/*!
 ******************************************************************************
 *   \brief Manage the reception of a fragment
 *
 *   Only one "long" message at a time can be received. When a full message is
 *   correctly received the message is forwarded. The "long" message buffer
 *   should be released using the CANFramingReleaseMsg function.
 *   This function should be called with the ISR.
 *
 *   \param CANMsgPtr Message received.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingRecvFragment(CAN_MSG_Type *CANMsgPtr)
{

}

/*!
 ******************************************************************************
 *   \brief Release a "long message" buffer
 *
 *   \param msgPtr Message to release.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingRecvFragmentRelease(CANFramingMsgStr *msgPtr)
{
}


/*!
 ******************************************************************************
 *   \brief Initialize transmit message pool
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingInitTXMsgPool(void)
{

}

/*!
 ******************************************************************************
 *   \brief Get a new message from the transmit message queue
 *
 *   \return Pointer to a new message. NULL if the queue is empty
 *
 ******************************************************************************
 */
CAN_MSG_Type *CANFramingGetTXMsg(void)
{

}


/*!
 ******************************************************************************
 *   \brief Add a new message to the transmit message queue
 *
 *   \param  msgPtr Pointer to the message to add
 *
 *   \return error code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingAddMsg(CAN_MSG_Type *msgPtr)
{

}


/*!
 ******************************************************************************
 *   \brief CAN0 ISR
 *
 *   \return None
 *
 ******************************************************************************
 */
void CANFramingIntHandler(void)
{

}


/*
 *********************** End of file ******************************************
 */
