/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Codec module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


#include "lpc17xx_timer.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_wdt.h"
#include "systemTime.h"
#include "systemStatus.h"
#include "systemAlarm.h"
#include "CANProtocol.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

//#include "svnRevision.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define CANC_TSYNCH_DBG_ENABLE 1
/* Disable IO Pin diagnostics */
#undef CANC_TSYNCH_DBG_ENABLE

#define CANC_MSG_POOL_SIZE_MASK  0x1F
#define CANC_MSG_POOL_SIZE       (CANC_MSG_POOL_SIZE_MASK + 1)

#define CANC_TIMER_RESOLUTION_US (MODULE_TIME_RESOLUTION_US)

#ifdef CANC_TSYNCH_DBG_ENABLE
#define CANC_TSYNCH_DBG_PORT (1)
#define CANC_TSYNCH_DBG_PIN  (1 << 28)
#endif

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    CANC_TIME_STATUS_NO_SYNCH = 0,
    CANC_TIME_STATUS_IN_SYNCH    ,

    CANC_TIME_STATUS_LAST
}CANC_TIME_STATUS;

typedef enum
{
    HBEAT_STATUS_OK = 0,
    HBEAT_STATUS_ERROR ,

    HBEAT_STATUS_LAST
}HBEAT_STATUS;

/*!
 * Control data for the tyme synch FSM
 */
typedef struct CANCTimeSynchDef
{
    CANC_TIME_STATUS status;

    lu_uint8_t timeSlot;
}CANCTimeSynchStr;

/*!
 * Heart Beat FSM data
 */
typedef struct HBeatDef
{
    HBEAT_STATUS status;
    lu_uint32_t time;
}HBeatStr;

/*!
 * Data structure for the internal message queue
 */
typedef struct CANCMsgPoolDef
{
    volatile lu_uint32_t readIndex;
    volatile lu_uint32_t writeIndex;
    CANFramingMsgStr *msgPtr[CANC_MSG_POOL_SIZE];
}CANCMsgPoolStr;

/*!
 * Data structure for the internal message queue
 */
typedef struct CANCodecDef
{
    /*! Device type */
    lu_uint8_t  device     ;
    /*! Device address */
    lu_uint8_t  deviceID   ;

    /*! Time Synch FSM data */
    CANCTimeSynchStr timeSynch;

    /* Heart Beat status */
    HBeatStr HBeatStatus;

    /*! Receive message pool */
    CANCMsgPoolStr   msgPool;

    CANCCustomDecode customDecoder;
}CANCodecStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*! List of supported message */
static const filterTableStr CANPfilterTable[] =
{
    /* messageType             messageID                            broadcast fragment */
    {  MODULE_MSG_TYPE_HPRIO , MODULE_MSG_ID_HPRIO_TSYNCH         , LU_TRUE , 0      },
    {  MODULE_MSG_TYPE_MD_CMD, MODULE_MSG_ID_MD_CMD_START_MODULE_C, LU_FALSE, 0      },
    {  MODULE_MSG_TYPE_MD_CMD, MODULE_MSG_ID_MD_CMD_RESTART       , LU_FALSE, 0      },
    {  MODULE_MSG_TYPE_MD_CMD, MODULE_MSG_ID_MD_CMD_RESTART       , LU_TRUE,  0      },
    {  MODULE_MSG_TYPE_MD_CMD , MODULE_MSG_ID_MD_CMD_CAN_BUS_OFF  , LU_FALSE, 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_HBEAT_M        , LU_TRUE , 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_TIME_C         , LU_TRUE , 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_TIME_C         , LU_FALSE, 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_PING_C         , LU_FALSE, 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_PING_C         , LU_TRUE , 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_PING_C         , LU_FALSE, 1      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_PING_C         , LU_TRUE , 1      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_MINFO_C        , LU_FALSE, 0      },
    {  MODULE_MSG_TYPE_LPRIO , MODULE_MSG_ID_LPRIO_CAN_STATS_C    , LU_FALSE, 0      }
};


static SB_ERROR CANCNewMessage(CANFramingMsgStr *msgPtr);

static SB_ERROR CANCInitMsgPool(void);
static CANFramingMsgStr* CANCGetMsg(void);
static SB_ERROR CANCAddMsg(CANFramingMsgStr *msgPtr);

static SB_ERROR CANCTimeSynchInit(SUIRQPriorityStr TsynchIRQ);
static SB_ERROR CANCTimeSynch(CANFramingMsgStr *msgPtr);

static SB_ERROR CANCLPrioMsgHandler(CANFramingMsgStr *msgPtr, lu_uint32_t time);
static SB_ERROR CANCMDCmdMsgHandler(CANFramingMsgStr *msgPtr, lu_uint32_t time);

void CANCTSyncIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static CANCodecStr CANCodecData;

#ifdef CANC_TSYNCH_DBG_ENABLE
/*! Tack the output of time synch debug pin */
static lu_uint32_t output = 0;
#endif

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR CANCodecInit( lu_uint32_t       clock        ,
                       lu_uint32_t       bitRate      ,
                       lu_uint8_t        device       ,
                       lu_uint8_t        deviceID     ,
                       CANFramingMapStr *pinMapPtr    ,
                       SUIRQPriorityStr  CANIRQ       ,
                       SUIRQPriorityStr  TsynchIRQ    ,
                       CANCCustomDecode  customDecoder
                     )
{
    SB_ERROR ret = SB_ERROR_NONE;

    if ( (pinMapPtr    == NULL) ||
         (customDecoder== NULL)
       )
    {
        return SB_ERROR_PARAM;
    }

#ifdef CANC_TSYNCH_DBG_ENABLE
    GPIO_SetDir(CANC_TSYNCH_DBG_PORT, CANC_TSYNCH_DBG_PIN, 1);
    GPIO_SetValue(CANC_TSYNCH_DBG_PORT, CANC_TSYNCH_DBG_PIN);
#endif

    /* Save custom decode function */
    CANCodecData.customDecoder = customDecoder;

    /* Save device address */
    CANCodecData.device   = device;
    CANCodecData.deviceID = deviceID;

    /* Initialize message pool */
    CANCInitMsgPool();

    /* Initialize Time synch subsystem */
    CANCTimeSynchInit(TsynchIRQ);

    /* Initialize Heart Beat status  */
    CANCodecData.HBeatStatus.status = HBEAT_STATUS_ERROR;

    /* Initialize CAN Framing module */
    ret = CANFramingInit( clock         ,
                          bitRate       ,
                          device        ,
                          deviceID      ,
                          pinMapPtr     ,
                          CANIRQ        ,
                          CANCNewMessage
                        );

    /* Set the filters for the message handled messages */
    if (ret == SB_ERROR_NONE)
    {
        ret = CANFramingAddFilter( CANPfilterTable,
                                   SU_TABLE_SIZE(CANPfilterTable, filterTableStr)
                                 );
    }

    return ret;
}

SB_ERROR CANCDecode(void)
{
    lu_uint32_t time;
    CANFramingMsgStr *msgPtr;
    SB_ERROR ret = SB_ERROR_NONE;

    /* Get a message */
    msgPtr = CANCGetMsg();
    /* Get current time  */
    time = STGetTime();

    while(msgPtr != NULL)
    {
        switch(msgPtr->messageType)
        {
            case MODULE_MSG_TYPE_LPRIO:
                ret = CANCLPrioMsgHandler(msgPtr, time);
            break;

            case MODULE_MSG_TYPE_MD_CMD:
                ret = CANCMDCmdMsgHandler(msgPtr, time);
            break;

            case MODULE_MSG_TYPE_SYSALARM:
            	ret = SysAlarmCANProtocolDecoder(msgPtr, time);
            	break;

            default:
                ret = SB_ERROR_CANC_NOT_HANDLED;
            break;
        }

        if (ret == SB_ERROR_CANC_NOT_HANDLED)
        {
            /* Forward message */
            ret = CANCodecData.customDecoder(msgPtr, time);
        }

        /* Release Message */
        CANFramingReleaseMsg(msgPtr);

        /* Get a new message */
        msgPtr = CANCGetMsg();
        /* Gert current time  */
        time = STGetTime();
    }

    /* Update Heart Beat status  */
    if( (CANCodecData.HBeatStatus.status == HBEAT_STATUS_OK) &&
        (STElapsedTime(CANCodecData.HBeatStatus.time, time) > MODULE_HBEAT_MAX_DELAY_MS)
      )
    {
        /* Heart Beat  Time out */
        CANCodecData.HBeatStatus.status = HBEAT_STATUS_ERROR;
    }

    return SB_ERROR_NONE;
}

ModuleTimeStr CANCGetTime(void)
{
    ModuleTimeStr time;

    switch(CANCodecData.timeSynch.status)
    {
        case CANC_TIME_STATUS_IN_SYNCH:
            /* Read Timer value */
            SU_ATOMIC( time.time = LPC_TIM0->TC;\
                       time.slot = CANCodecData.timeSynch.timeSlot;\
                     );
        break;

        case CANC_TIME_STATUS_NO_SYNCH:
        default:
            /* Update slot and reset the timer */
            time.time = MODULE_TIME_INVALID;
            time.slot = 0;
        break;
    }

    return time;
}

SB_ERROR CANCSendMCM( MODULE_MSG_TYPE  messageType,
                      MODULE_MSG_ID    messageID  ,
                      lu_uint32_t      msgLen     ,
                      lu_uint8_t      *msgBufPtr
                     )
{
    CANFramingMsgStr CANTXMsg;
    SB_ERROR         retVal;

    SU_CRITICAL_REGION_ENTER()

    /* Fill header */
    CANTXMsg.ID = 0;
    CANTXMsg.messageType = messageType;
    CANTXMsg.messageID   = messageID;
    CANTXMsg.deviceDst   = MODULE_MCM;
    CANTXMsg.deviceIDDst = MODULE_ID_0;
    CANTXMsg.deviceSrc   = CANCodecData.device;
    CANTXMsg.deviceIDSrc = CANCodecData.deviceID;
    CANTXMsg.msgLen      = msgLen;
    CANTXMsg.msgBufPtr   = msgBufPtr;

    /* Send message */
    retVal = CANFramingSendMsg(&CANTXMsg);

    SU_CRITICAL_REGION_EXIT()

    return retVal;
}

lu_uint8_t CANCGetDeviceID(void)
{
	return CANCodecData.deviceID;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief New CAN message callback
 *
 *   This function is registered in the CANFraming module. It manages the
 *   time synch message directly. All the other messages are saved in an
 *   internal queue and managed in the application (CANCDecode)
 *
 *   \param msgPtr Pointer to the received message
 *
 *   \return None
 *
 ******************************************************************************
 */
SB_ERROR CANCNewMessage(CANFramingMsgStr *msgPtr)
{
	SB_ERROR ret;

	ret = SB_ERROR_NONE;

    /* Only the time synch is managed directly into the ISR.
     * Everything else is saved in a local queue and managed out of the
     * ISR
     */
    if ( (msgPtr->messageType == MODULE_MSG_TYPE_HPRIO)     &&
         (msgPtr->messageID   == MODULE_MSG_ID_HPRIO_TSYNCH)
       )
    {
        /* Handle message */
        CANCTimeSynch(msgPtr);
        /* Release buffer */
        CANFramingReleaseMsg(msgPtr);
    }
    else
    {
        /* Save the message in the Application level message queue */
        ret = CANCAddMsg(msgPtr);
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Initialize receive mesasage pool
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANCInitMsgPool(void)
{
    CANCodecData.msgPool.readIndex = 0;
    CANCodecData.msgPool.writeIndex = 0;

    return SB_ERROR_NONE;
}


/*!
 ******************************************************************************
 *   \brief Get a new message from the receive message queue
 *
 *   \return Pointer to a new message. NULL if the queue is empty
 *
 ******************************************************************************
 */
CANFramingMsgStr* CANCGetMsg(void)
{
    CANFramingMsgStr *msgPtr;
    lu_uint32_t readIndex;

    /* Enter Critical Region */
    SU_CRITICAL_REGION_ENTER();

    /* Read current read position */
    readIndex = CANCodecData.msgPool.readIndex;

    if (readIndex == CANCodecData.msgPool.writeIndex)
    {
        /* No new messages.
         * Exit form critical region as soon as possible
         */
        SU_CRITICAL_REGION_EXIT();
        msgPtr = NULL;
    }
    else
    {
        /* New messages available. Get a new message */
        readIndex = (readIndex + 1) & CANC_MSG_POOL_SIZE_MASK;
        /* Save the new read index */
        CANCodecData.msgPool.readIndex = readIndex;
        /* We can now exit from the critical region */
        SU_CRITICAL_REGION_EXIT();

        msgPtr = CANCodecData.msgPool.msgPtr[readIndex];
    }

    return msgPtr;
}

/*!
 ******************************************************************************
 *   \brief Add a new message from the receive message queue
 *
 *   \param  msgPtr Pointer to the message to add
 *
 *   \return error code
 *
 ******************************************************************************
 */
SB_ERROR CANCAddMsg(CANFramingMsgStr *msgPtr)
{
    lu_uint32_t writeIndex;

    /* Only this function updates the writeIndex. This function is called
     * only from one ISR. We don't need to disable interrupts
     */
    writeIndex = (CANCodecData.msgPool.writeIndex + 1) & CANC_MSG_POOL_SIZE_MASK;

    /* Enough space ? */
    if (writeIndex != CANCodecData.msgPool.readIndex)
    {
        /* Yes */
        CANCodecData.msgPool.msgPtr[writeIndex] = msgPtr;
        CANCodecData.msgPool.writeIndex = writeIndex;

        return SB_ERROR_NONE;
    }
    else
    {
        return SB_ERROR_CANC_QUEUE_FULL;
    }
}

/*!
 ******************************************************************************
 *   \brief Initialise time synch module
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANCTimeSynchInit(SUIRQPriorityStr TsynchIRQ)
{
    TIM_TIMERCFG_Type timerConfig;
    TIM_MATCHCFG_Type timerMatchConfig;

    /* Reset synchronisation */
    CANCodecData.timeSynch.status = CANC_TIME_STATUS_NO_SYNCH;

    /* Initialize Timer */
    timerConfig.PrescaleOption = TIM_PRESCALE_USVAL;
    timerConfig.PrescaleValue = CANC_TIMER_RESOLUTION_US;
    TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &timerConfig);

    /* Set timer expiration time */
    timerMatchConfig.MatchChannel = 0;
    timerMatchConfig.IntOnMatch = ENABLE;
    timerMatchConfig.StopOnMatch = ENABLE;
    timerMatchConfig.ResetOnMatch = DISABLE;
    timerMatchConfig.ExtMatchOutputType = 0;
    timerMatchConfig.MatchValue =
                     (MODULE_TIME_SYNCH_MAX_DELAY_US / CANC_TIMER_RESOLUTION_US);
    TIM_ConfigMatch(LPC_TIM0, &timerMatchConfig);

    /* Set CAN Interrupt priority and enable interrupt */
    SUSetIRQPriority(TIMER0_IRQn, TsynchIRQ, LU_TRUE);

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Synchronise the internal timer with the MCM
 *
 *   The internal timer is restarted and the time slot is saved.
 *
 *   \param msgPtr Pointer to the time synch message
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANCTimeSynch(CANFramingMsgStr *msgPtr)
{
    ModuleTsynchStr *CANTsynchPtr;

    /* Message sanity check */
    if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleTsynchStr))
    {
        CANTsynchPtr = (ModuleTsynchStr*)(msgPtr->msgBufPtr);

        switch(CANCodecData.timeSynch.status)
        {
            case CANC_TIME_STATUS_NO_SYNCH:
                /* Reset timer */
                TIM_ResetCounter(LPC_TIM0);
                /* Start Timer */
                TIM_Cmd(LPC_TIM0, ENABLE);

                /* Save Time slot */
                CANCodecData.timeSynch.timeSlot = CANTsynchPtr->timeSlot;

                /* We are in synch */
                CANCodecData.timeSynch.status = CANC_TIME_STATUS_IN_SYNCH;

                /* Reset the TSynch error bit */
                SSSetBError(MODULE_BOARD_ERROR_TSYNCH, LU_FALSE);
            break;

            case CANC_TIME_STATUS_IN_SYNCH:
                /* Reset the timer and Update slot */
                SU_ATOMIC( TIM_ResetCounter(LPC_TIM0);\
                           CANCodecData.timeSynch.timeSlot = CANTsynchPtr->timeSlot;\
                         );

#ifdef CANC_TSYNCH_DBG_ENABLE
                (output == 1) ?
                   GPIO_SetValue(CANC_TSYNCH_DBG_PORT, CANC_TSYNCH_DBG_PIN)  :
                   GPIO_ClearValue(CANC_TSYNCH_DBG_PORT, CANC_TSYNCH_DBG_PIN);
                output ^= 1;
#endif
            break;

            default:
            break;
        }

        return SB_ERROR_NONE;
    }

    return SB_ERROR_CANC;
}

/*!
 ******************************************************************************
 *   \brief Synchronise the internal timer with the MCM
 *
 *   The internal timer is restarted and the time slot is saved.
 *
 *   \param msgPtr Pointer to the time synch message
 *   \param time Current time
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANCLPrioMsgHandler(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
    ModuleHBeatSStr CANHBeat;
    ModuleInfoStr moduleInfo;
    ModuleTimeStr CANTime;
    ModuleHBeatMStr *moduleHBeatMPtr;
    SB_ERROR ret = SB_ERROR_NONE;
    MODULE_BOARD_STATUS status;
    MODULE_BOARD_ERROR error;
    lu_uint32_t serial;
    CanStatsStr canStats;

    switch(msgPtr->messageID)
    {
        case MODULE_MSG_ID_LPRIO_HBEAT_M:
            if (msgPtr->msgLen == 0 || msgPtr->msgLen == sizeof(ModuleHBeatMStr))
            {
            	moduleHBeatMPtr = (ModuleHBeatMStr *)msgPtr->msgBufPtr;

            	// Check if power status info is being sent ? (Remove hack later!!)
            	if (msgPtr->msgLen > 0)
            	{
					SSSetPowerSaveMode(moduleHBeatMPtr->powerSaveMode ? LU_TRUE : LU_FALSE);
					SSSetBootloaderMode(moduleHBeatMPtr->bootloaderMode ? LU_TRUE : LU_FALSE);
            	}

                /* Prepare payload */
                SSGetFullStatus(&status, &error, &serial);

                /* Copy to CAN reply */
                CANHBeat.status = status;
                CANHBeat.error  = error;
                CANHBeat.serial = serial;

                /* Send message */
                CANCSendMCM( MODULE_MSG_TYPE_LPRIO        ,
                             MODULE_MSG_ID_LPRIO_HBEAT_S  ,
                             sizeof(ModuleHBeatSStr)      ,
                             (lu_uint8_t *)&CANHBeat
                           );

                /* Update HBeat FSM status */
                CANCodecData.HBeatStatus.status = HBEAT_STATUS_OK;
                CANCodecData.HBeatStatus.time = time;

                /* Pulse Green CAN LED */
                CANFramingPulseCANLed();
            }
        break;

        /* Send module info */
        case MODULE_MSG_ID_LPRIO_MINFO_C:
            if (msgPtr->msgLen == 0)
            {
                /* Prepare payload */
                moduleInfo.moduleType                         = CANCodecData.device        ;
                moduleInfo.serial                             = SSGetSerial()              ;

                moduleInfo.svnRevisionBoot					  = SSGetBootSvnRevision();
                moduleInfo.svnRevisionApp					  = SSGetAppSvnRevision();

                moduleInfo.systemArch                         = 1; //SLAVE_BIN_ARCH_ARM_NXP_LPC1768;

                moduleInfo.featureRevision.major              = SSGetFeatureRevisionMajor();
                moduleInfo.featureRevision.minor              = SSGetFeatureRevisionMinor();

                moduleInfo.application.systemAPI.major        = SSGetAppSystemAPIMajor();
				moduleInfo.application.systemAPI.minor        = SSGetAppSystemAPIMinor();
				moduleInfo.application.software.version.major = SSGetAppSoftwareVersionMajor();
				moduleInfo.application.software.version.minor = SSGetAppSoftwareVersionMinor();
				moduleInfo.application.software.patch         = SSGetAppSoftwareVersionPatch();
				moduleInfo.application.software.relType       = SSGetAppSoftwareReleaseType();

                moduleInfo.bootloader.systemAPI.major         = SSGetBootSystemAPIMajor();
                moduleInfo.bootloader.systemAPI.minor         = SSGetBootSystemAPIMinor();
                moduleInfo.bootloader.software.version.major  = SSGetBootSoftwareVersionMajor();
                moduleInfo.bootloader.software.version.minor  = SSGetBootSoftwareVersionMinor();
                moduleInfo.bootloader.software.patch          = SSGetBootSoftwareVersionPatch();
                moduleInfo.bootloader.software.relType        = SSGetBootSoftwareReleaseType();

                /* Send message */
                CANCSendMCM( MODULE_MSG_TYPE_LPRIO        ,
                             MODULE_MSG_ID_LPRIO_MINFO_R,
                             sizeof(moduleInfo)           ,
                             (lu_uint8_t *)&moduleInfo
                           );
            }
        break;

        /* Statistics on CAN bus */
        case MODULE_MSG_ID_LPRIO_CAN_STATS_C:
        	if (msgPtr->msgLen == 0)
        	{
        		canStats.canError 				= CANFramingGetcanError();
        		canStats.canArbitrationError 	= CANFramingGetcanArbitrationError();
        		canStats.canBusError            = CANFramingGetcanBusError();
        		canStats.canBusOff              = CANFramingGetcanBusOff();
        		canStats.canDataOverrun         = CANFramingGetcanDataOverrun();
        		canStats.canErrorPassive        = CANFramingGetcanErrorPassive();
        		canStats.canErrorBTR            = CANFramingGetcanErrorBTR();

        		canStats.canTxCount             = CANFramingGetTXTotal();
        		canStats.canRxCount				= CANFramingGetRXMessageTotal();
        		canStats.canRxLost              = CANFramingGetRxLost();

        		/* Send message */
				CANCSendMCM( MODULE_MSG_TYPE_LPRIO       ,
						     MODULE_MSG_ID_LPRIO_CAN_STATS_R,
							 sizeof(CanStatsStr)             ,
							 (lu_uint8_t *)&canStats
						   );
        	}
        break;

        /* Send back the current CAN time */
        case MODULE_MSG_ID_LPRIO_TIME_C:
            if (msgPtr->msgLen == 0)
            {
                /* Prepare payload */
                CANTime = CANCGetTime();

                /* Send message */
                CANCSendMCM( MODULE_MSG_TYPE_LPRIO       ,
                             MODULE_MSG_ID_LPRIO_TIME_R,
                             sizeof(CANTime)             ,
                             (lu_uint8_t *)&CANTime
                           );
            }
        break;

        /* Send back the payload. */
        case MODULE_MSG_ID_LPRIO_PING_C:
            msgPtr->messageID   = MODULE_MSG_ID_LPRIO_PING_R;
            msgPtr->deviceDst   = msgPtr->deviceSrc;
            msgPtr->deviceIDDst = msgPtr->deviceIDSrc;
            msgPtr->deviceSrc   = CANCodecData.device;
            msgPtr->deviceIDSrc = CANCodecData.deviceID;

            CANFramingSendMsg(msgPtr);
        break;

        default:
            ret = SB_ERROR_CANC_NOT_HANDLED;
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Handle "module level" commands
 *
 *
 *   \param msgPtr Pointer to the message
 *   \param time Current time
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANCMDCmdMsgHandler(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
    ModReplyStr replyMsg;
    MDCmdRestartStr *restartCmdPtr;
    SB_ERROR ret = SB_ERROR_NONE;

    LU_UNUSED(time);

    /* Set an invalid channel */
    replyMsg.channel = 0xff;
    replyMsg.status = REPLY_STATUS_OKAY;

    switch(msgPtr->messageID)
    {
        case MODULE_MSG_ID_MD_CMD_START_MODULE_C:
            if (msgPtr->msgLen == 0 || msgPtr->msgLen == sizeof(MDCmdStartModuleStr))
            {
                if(SSGetBStatus() == MODULE_BOARD_STATUS_WAIT_INIT)
                {
                    SSSetBStatus(MODULE_BOARD_STATUS_STARTING);
                }
                else
                {
                    replyMsg.status = REPLY_STATUS_ERROR;
                }

                /* Send message */
                CANCSendMCM( MODULE_MSG_TYPE_MD_CMD                ,
                             MODULE_MSG_ID_MD_CMD_START_MODULE_R,
                             sizeof(replyMsg)                   ,
                             (lu_uint8_t *)&replyMsg
                           );
            }
        break;

        case MODULE_MSG_ID_MD_CMD_RESTART:
            if(msgPtr->msgLen != MODULE_MESSAGE_SIZE(MDCmdRestartStr))
            {
                /* Wrong Message length */
                break;
            }

            /* Valid message size. Decode message */
            restartCmdPtr = (MDCmdRestartStr*)(msgPtr->msgBufPtr);

            if(restartCmdPtr->type == MD_RESTART_WARM)
            {
//#ifndef NDEBUG
//            	SSSetBStatus(MODULE_BOARD_STATUS_WAIT_INIT);
//#else

//           	CAN_DeInit(LPC_CAN1_BASE);

                /* Warm restart */
                /* Initialise watchdog */
                WDT_Init(WDT_CLKSRC_IRC, WDT_MODE_RESET);
                /* Start watchdog (1ms timeout) */
                WDT_Start(1000);

                /* Wait for reset */
                while(1);
//#endif
            }
        break;

        case MODULE_MSG_ID_MD_CMD_CAN_BUS_OFF:
        	if (msgPtr->msgLen == 0)
			{
        		CANFramingEnterBusOff();
			}
        	break;

        default:
            ret = SB_ERROR_CANC_NOT_HANDLED;
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Out of synch ISR
 *
 *   If a time synch message is not received for more than 1.5 sec the time
 *   synch status is changed to "out of synch"
 *
 *   \return None
 *
 ******************************************************************************
 */
void CANCTSyncIntHandler(void)
{
    static lu_uint32_t errorCounter = 0;


    if (TIM_GetIntStatus(LPC_TIM0,TIM_MR0_INT) == SET)
    {
        /* Timer 0 expired. We are out of synch !! */
        CANCodecData.timeSynch.status = CANC_TIME_STATUS_NO_SYNCH;

        /* Set the TSynch error bit */
        SSSetBError(MODULE_BOARD_ERROR_TSYNCH, LU_TRUE);

        TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);

        errorCounter++;
    }

    return;
}

/*
 *********************** End of file ******************************************
 */
