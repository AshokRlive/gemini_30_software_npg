/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Dual FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.h 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/07/14      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _DUAL_FPI_INCLUDED
#define _DUAL_FPI_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"
#include "FrontEndPIC.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
//Sum sample rate per half cycle
#define SUM_SMPL_RATE_PER_HL_CL 32
//Sum time per half cycle in ms
#define SUM_TIME_PER_HL_CL_MS 10

/*
//Reference phase threshold for error correction function
#define PHASE_VALUE_400_A 800
#define PHASE_VALUE_450_A 900
#define PHASE_VALUE_500_A 1000
#define PHASE_VALUE_550_A 1100
#define PHASE_VALUE_600_A 1200
#define PHASE_VALUE_650_A 1300
#define PHASE_VALUE_700_A 1400
#define PHASE_VALUE_750_A 1500
// Sum error correction factor
#define SUM_ERROR_CORRECTION_FACTOR_5_A 70

//Tolerance value for sum error correction function
#define PHASE_TOLERANCE_VALUE_25_A 50
*/

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR DualFPIInit(void);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 extern SB_ERROR DualFPICANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

 /*!
   ******************************************************************************
   *   \brief Brief description
   *
   *   Detailed description
   *
   *   \param parameterName Description
   *
   *
   *   \return
   *
   ******************************************************************************
   */
extern SB_ERROR DualFPIOvercurrentFaultDetection( FEPIC_ADC_CH adcChan, FPI_CONFIG_CH fpiChan, lu_uint32_t *value);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR DualFPIEarthFaultDetection( FPI_CONFIG_CH fpiChan, lu_int32_t *value);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR DualFPIOvercurrentFaultAlarm( FEPIC_ADC_CH adcChan, FPI_CONFIG_CH fpiChan);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR DualFPIEarthFaultAlarm( FPI_CONFIG_CH fpiChan);

#endif /* _DUAL_FPI_INCLUDED */

/*
 *********************** End of file ******************************************
 */
