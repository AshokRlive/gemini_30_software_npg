/*
 * IOManagerSpy.h
 *
 *  Created on: 27 Oct 2014
 *      Author: bogias_a
 */

#ifndef IOMANAGERSPY_H_
#define IOMANAGERSPY_H_

#include "lu_types.h"
#include "errorCodes.h"
//#include "IOManager.h"
//#include "IOManagerIO.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* IOGPIOAddressStr - pinMode bit defines */
#define GPIO_PM_OUTPUT_HIGH			0x00000000L   /* Initialise physical output pin high (default) */
#define GPIO_PM_OUTPUT_LOW			0x00000001L	  /* Initialise physical output pin low */
#define GPIO_PM_OPEN_DRAIN			0x00000002L
#define GPIO_PM_PULL_UP				0x00000004L
#define GPIO_PM_PULL_DOWN			0x00000008L
#define GPIO_PM_FEPIC_RST1			0x00100000L
#define GPIO_PM_FEPIC_RST2			0x00200000L
#define GPIO_PM_ADC_CFG1			0x00100000L
#define GPIO_PM_ADC_CFG2			0x00200000L
#define GPIO_PM_ADC_PM0				0x00400000L
#define GPIO_PM_ADC_PM1				0x00800000L
#define GPIO_PM_ADC_IRQ0			0x00010000L
#define GPIO_PM_ADC_IRQ1			0x00020000L
#define GPIO_PM_ADC_RESET			0x00040000L
#define GPIO_PM_ADC_REG_32SE        0x00000000L
#define GPIO_PM_ADC_REG_24ZP        0x00080000L
#define GPIO_PM_ADC_REG_28ZP        0x00100000L
#define GPIO_PM_ADC_REG_24ZP_PKA    0x01000000L
#define GPIO_PM_ADC_REG_24ZP_PKB    0x02000000L
#define GPIO_PM_ADC_REG_24ZP_PKC    0x04000000L
#define GPIO_PM_BUS_400KHZ			0x10000000L
#define GPIO_PM_OUTPUT_INVERT  		0x20000000L /* Physical output pin is inverted relative to value in boardIotable[] */
#define GPIO_PM_EXT_EQUIP    		0x40000000L
#define GPIO_PM_INPUT_INVERT		0x80000000L

#define IO_CH_NA					0xffffffffL

#define IO_ID_NA					(IO_ID_LAST)

/* IODebounceStr - default debounce value */
#define IO_DB_DEFAULT_MS			(20)

/*! Utility macros to fill in IOMap */
#define IOM_GPIO_PERIPH(ioID, port, pin, func, pinMode) \
	{ioID, IO_CH_NA, IO_CLASS_GPIO_PERIPH, IO_DEV_PERIPH, {{port, pin, func, pinMode}}, \
	{0, 0}},

#define IOM_GPIO_OUTPUT(ioID, ioChan, port, pin, pinMode) \
	{ioID, ioChan, IO_CLASS_DIGITAL_OUTPUT, IO_DEV_GPIO, {{port, pin, 0, pinMode}}, \
	{0, 0}},

#define IOM_GPIO_INPUT(ioID, ioChan, port, pin, pinMode) \
	{ioID, ioChan, IO_CLASS_DIGITAL_INPUT, IO_DEV_GPIO, {{port, pin, 0, pinMode}}, \
	{IO_DB_DEFAULT_MS, IO_DB_DEFAULT_MS}},

#define IOM_GPIO_INOUT(ioID, ioChan, port, pin, pinMode) \
	{ioID, ioChan, IO_CLASS_DIGITAL_INPUT_OUTPUT, IO_DEV_GPIO, {{port, pin, 0, pinMode}}, \
	{IO_DB_DEFAULT_MS, IO_DB_DEFAULT_MS}},

#define IOM_I2CEXP_OUTPUT(ioID, ioChan, busI2c, address, ioPin, pinMode) \
	{ioID, ioChan, IO_CLASS_DIGITAL_OUTPUT, IO_DEV_I2C_IO_EXPANDER, {{busI2c, ioPin, address, pinMode}}, \
	{0, 0}},

#define IOM_I2CEXP_INPUT(ioID, ioChan, busI2c, address, ioPin, pinMode) \
	{ioID, ioChan, IO_CLASS_DIGITAL_INPUT, IO_DEV_I2C_IO_EXPANDER, {{busI2c, ioPin, address, pinMode}}, \
	{IO_DB_DEFAULT_MS, IO_DB_DEFAULT_MS}},

#define IOM_PERIPH_AIN(ioID, ioChan, port, pin, func) \
	{ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_PERIPH, {{port, pin, func, IO_ID_NA}}, \
	{0, 0}},

#define IOM_PERIPH_AIN_MUX(ioID, ioChan, port, pin, func, muxIoID) \
	{ioID, ioChan, IO_CLASS_ANALOG_INPUT_MUX, IO_DEV_PERIPH, {{port, pin, func, muxIoID}}, \
	{0, 0}},

#define IOM_PERIPH_AOUT(ioID, ioChan, port, pin, func) \
	{ioID, ioChan, IO_CLASS_ANALOG_OUTPUT, IO_DEV_PERIPH, {{port, pin, func, 0}}, \
	{0, 0}},

#define IOM_DIGPOT_OUT(ioID, ioChan, sspChan, potChan, csIoID) \
	{ioID, ioChan, IO_CLASS_ANALOG_OUTPUT, IO_DEV_SPI_DIGIPOT, {{sspChan, potChan, csIoID, 0}}, \
	{0, 0}},

#define IOM_ADPOT_OUT(ioID, ioChan, sspChan, potChan, csIoID) \
	{ioID, ioChan, IO_CLASS_ANALOG_OUTPUT, IO_DEV_SPI_AD_DIGITAL_POT, {{sspChan, potChan, csIoID, 0}}, \
	{0, 0}},

#define IOM_SPI_AIN(ioID, ioChan, sspChan, adcChan, csIoID) \
	{ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_SPI_ADC, {{sspChan, adcChan, csIoID, 0}}, \
	{0, 0}},

#define IOM_ADE78XX_AIN(ioID, ioChan, i2cChan, reg, mode) \
	{ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_I2C_ADE7854, {{i2cChan, (reg >> 8), (reg & 0xff), mode}}, \
	{0, 0}},


#define IOM_ADE78XX_HSDC_AIN(ioID, ioChan, hsdcChan) \
    {ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_HSDC_ADE7854, {{hsdcChan, 0, 0, 0}}, \
    {0, 0}},

#define IOM_SPI_FEPIC_AIN(ioID, ioChan, sspChan, adcChan) \
    {ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_SPI_FEPIC, {{sspChan, adcChan, 0, 0}}, \
    {0, 0}},

#define IOM_I2CTEMP_AIN(ioID, ioChan, busI2c, address) \
	{ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_I2C_TEMP_LM73, {{busI2c, address, 0, 0}}, \
	{0, 0}},

#define IOM_I2C_ADE7854(ioID, i2cChan) \
	{ioID, IO_CH_NA, IO_CLASS_GPIO_PERIPH, IO_DEV_I2C_ADE7854, {{i2cChan, 0, 0, 0}}, \
	{0, 0}},

#define IOM_SPI_ADE7854(ioID, sspChan) \
	{ioID, IO_CH_NA, IO_CLASS_GPIO_PERIPH, IO_DEV_SPI_ADE7854, {{sspChan, 0, 0, 0}}, \
	{0, 0}},

#define IOM_PERIPH_NVRAM(ioID, i2cChan, address, wenIoID) \
	{ioID, IO_CH_NA, IO_CLASS_GPIO_PERIPH, IO_DEV_I2C_NVRAM, {{i2cChan, wenIoID, address, 0}}, \
	{0, 0}},

#define IOM_VIRT_DO(ioID, ioChan) \
	{ioID, ioChan, IO_CLASS_DIGITAL_OUTPUT, IO_DEV_VIRTUAL, {{0, 0, FUNC_NONE, 0}}, \
	{0, 0}},

#define IOM_VIRT_DI(ioID, ioChan) \
	{ioID, ioChan, IO_CLASS_DIGITAL_INPUT, IO_DEV_VIRTUAL, {{0, 0, FUNC_NONE, 0}}, \
	{0, 0}},

#define IOM_VIRT_AI(ioID, ioChan) \
	{ioID, ioChan, IO_CLASS_ANALOG_INPUT, IO_DEV_VIRTUAL, {{0, 0, FUNC_NONE, 0}}, \
	{0, 0}},

#define IOM_VIRT_AO(ioID, ioChan) \
	{ioID, ioChan, IO_CLASS_ANALOG_OUTPUT, IO_DEV_VIRTUAL, {{0, 0, FUNC_NONE, 0}}, \
	{0, 0}},

#define IOM_LAST \
	{0, IO_CH_NA, IO_CLASS_LAST, IO_DEV_NONE, {{0, 0, FUNC_NONE, 0}}, \
	{0, 0}}


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! Local/Remote status */
typedef enum
{
	IO_STAT_LOR_OFF		= 0,
	IO_STAT_LOR_LOCAL	   ,
	IO_STAT_LOR_REMOTE     ,

	IO_STAT_LOR_INVALID
} IO_STAT_LOR;


typedef enum
{
	FUNC_NONE         	= 0	,

	FUNC_CAN_1_TX			,
	FUNC_CAN_1_RX			,
	FUNC_CAN_2_TX			,
	FUNC_CAN_2_RX			,

	FUNC_I2C_0_SDA          ,
	FUNC_I2C_0_SCL          ,
	FUNC_I2C_1_SDA          ,
	FUNC_I2C_1_SCL          ,
	FUNC_I2C_2_SDA          ,
	FUNC_I2C_2_SCL          ,

	FUNC_UART_0_TX          ,
	FUNC_UART_0_RX          ,

	FUNC_UART_1_TX          ,
	FUNC_UART_1_RX          ,
	FUNC_UART_1_CTS         ,
	FUNC_UART_1_DCD         ,
	FUNC_UART_1_DSR         ,
	FUNC_UART_1_DTR         ,
	FUNC_UART_1_RI          ,
	FUNC_UART_1_RTS         ,

	FUNC_UART_2_TX          ,
	FUNC_UART_2_RX          ,

	FUNC_UART_3_TX          ,
	FUNC_UART_3_RX          ,

	FUNC_SSP0_SEL           ,
	FUNC_SSP0_SCK           ,
	FUNC_SSP0_MISO          ,
	FUNC_SSP0_MOSI          ,

	FUNC_SSP1_SEL           ,
	FUNC_SSP1_SCK           ,
	FUNC_SSP1_MISO          ,
	FUNC_SSP1_MOSI          ,

	FUNC_AD0_CH_0           ,
	FUNC_AD0_CH_1           ,
	FUNC_AD0_CH_2           ,
	FUNC_AD0_CH_3           ,
	FUNC_AD0_CH_4           ,
	FUNC_AD0_CH_5           ,
	FUNC_AD0_CH_6           ,
	FUNC_AD0_CH_7           ,

	FUNC_PWM1_CH1           ,
	FUNC_PWM1_CH2           ,
	FUNC_PWM1_CH3           ,
	FUNC_PWM1_CH4           ,
	FUNC_PWM1_CH5           ,
	FUNC_PWM1_CH6           ,

	FUNC_TIM0_CAP0          ,
	FUNC_TIM0_CAP1          ,

	FUNC_TIM1_CAP0          ,
	FUNC_TIM1_CAP1          ,

	FUNC_TIM2_CAP0          ,
	FUNC_TIM2_CAP1          ,

	FUNC_TIM3_CAP0          ,
	FUNC_TIM3_CAP1          ,

	FUNC_TIM0_MAT0          ,
	FUNC_TIM0_MAT1          ,

	FUNC_TIM1_MAT0          ,
	FUNC_TIM1_MAT1          ,

	FUNC_TIM2_MAT0          ,
	FUNC_TIM2_MAT1          ,
	FUNC_TIM2_MAT2          ,

	FUNC_TIM3_MAT0          ,
	FUNC_TIM3_MAT1          ,

	FUNC_EINT0              ,
	FUNC_EINT1              ,
	FUNC_EINT2              ,
	FUNC_EINT3              ,

	FUNC_CLKOUT             ,
	FUNC_DAC_A              ,

	FUNC_SPI_SEL            ,
	FUNC_SPI_SCK            ,
	FUNC_SPI_MISO           ,
	FUNC_SPI_MOSI           ,

	FUNC_LAST
}GPIO_PIN_FUNC;

typedef enum
{
	IO_BUS_SPI_SSPI_0				= 0,
	IO_BUS_SPI_SSPI_1                  ,
    IO_BUS_SPI_SPI                      // Use legacy SPI peripheral
}IO_BUS_SPI;

typedef enum
{
	IO_BUS_I2C_0				= 0,
	IO_BUS_I2C_1                   ,
	IO_BUS_I2C_2
}IO_BUS_I2C;

typedef enum
{
	I2C_EXP_IOPIN_0_0			= 0,

	I2C_EXP_IOPIN_0_1              ,
	I2C_EXP_IOPIN_0_2              ,
	I2C_EXP_IOPIN_0_3              ,
	I2C_EXP_IOPIN_0_4              ,
	I2C_EXP_IOPIN_0_5              ,
	I2C_EXP_IOPIN_0_6              ,
	I2C_EXP_IOPIN_0_7              ,

	I2C_EXP_IOPIN_1_0              ,
	I2C_EXP_IOPIN_1_1              ,
	I2C_EXP_IOPIN_1_2              ,
	I2C_EXP_IOPIN_1_3              ,
	I2C_EXP_IOPIN_1_4              ,
	I2C_EXP_IOPIN_1_5              ,
	I2C_EXP_IOPIN_1_6              ,
	I2C_EXP_IOPIN_1_7
}I2C_EXP_IOPIN;

typedef enum
{
    /*! Single-ended configuration */
	SPI_ADC_CH_SE_0	= (0x00 | 0x08),
	SPI_ADC_CH_SE_1 = (0x01 | 0x08),
	SPI_ADC_CH_SE_2 = (0x02 | 0x08),
	SPI_ADC_CH_SE_3 = (0x03 | 0x08),
	SPI_ADC_CH_SE_4 = (0x04 | 0x08),
	SPI_ADC_CH_SE_5 = (0x05 | 0x08),
	SPI_ADC_CH_SE_6 = (0x06 | 0x08),
	SPI_ADC_CH_SE_7 = (0x07 | 0x08),

	/*! Differential configuration: CH0 = IN+, CH1 = IN- */
    SPI_ADC_CH_DF_0 = 0x00,
    /*! Differential configuration: CH0 = IN-, CH1 = IN+ */
    SPI_ADC_CH_DF_0_INV = 0x01,
    /*! Differential configuration: CH2 = IN+, CH3 = IN- */
    SPI_ADC_CH_DF_2 = 0x02,
    /*! Differential configuration: CH2 = IN-, CH3 = IN+ */
    SPI_ADC_CH_DF_2_INV = 0x03,
    /*! Differential configuration: CH4 = IN+, CH5 = IN- */
	SPI_ADC_CH_DF_4 = 0x04,
	/*! Differential configuration: CH4 = IN-, CH5 = IN+ */
	SPI_ADC_CH_DF_4_INV = 0x05,
	/*! Differential configuration: CH6 = IN+, CH7 = IN- */
	SPI_ADC_CH_DF_6 = 0x06,
	/*! Differential configuration: CH6 = IN-, CH7 = IN+ */
	SPI_ADC_CH_DF_6_INV = 0x07
}SPI_ADC_CH;

typedef enum
{
	IO_DEV_NONE                          = 0 ,

	IO_DEV_GPIO			                     ,  // Processor local GPIO
	IO_DEV_PERIPH                            ,  // Processor local Peripheral

	IO_DEV_SPI_DIGIPOT	                     ,  // SPI attached Microchip Digipot
	IO_DEV_SPI_AD_DIGITAL_POT                ,  // SPI attached AD84xx Ditital Pot

	IO_DEV_I2C_IO_EXPANDER                   ,  // I2C attached IO expander

	IO_DEV_SPI_ADC  	                     ,  // SPI ADC - MCP3204

	IO_DEV_I2C_TEMP_LM73					 ,  // I2C LM73 Temperature sensor

	IO_DEV_I2C_ADE7854                       ,
	IO_DEV_SPI_ADE7854                       ,
	IO_DEV_HSDC_ADE7854                      , //ADE78XX HSDC instantaneous channel

	IO_DEV_SPI_FEPIC                         , // FPM Front End PIC (ADC)

	IO_DEV_I2C_NVRAM                         ,

	IO_DEV_VIRTUAL							 ,  // Virtual IO

    IO_DEV_LAST
} IO_DEV;

typedef enum
{
	IO_CLASS_NONE                          = 0 ,

	IO_CLASS_GPIO_PERIPH                       ,

	IO_CLASS_DIGITAL_INPUT	                   ,
	IO_CLASS_DIGITAL_OUTPUT	                   ,
	IO_CLASS_DIGITAL_OUTPUT_SET_DIR_ONLY       ,
	IO_CLASS_DIGITAL_INPUT_OUTPUT              ,
	IO_CLASS_ANALOG_INPUT                      ,
	IO_CLASS_ANALOG_INPUT_MUX                  ,
	IO_CLASS_ANALOG_OUTPUT                     ,

	IO_CLASS_LAST
} IO_CLASS;

/*! IO GPIO Pin Address structure */
typedef struct IOGPIOAddressDef
{
	lu_int8_t         port;
    lu_int8_t         pin;
    lu_int8_t         periphFunc;  // GPIO = 0
    lu_uint32_t       pinMode;
}IOGPIOAddressStr;

/*! IO ADC Pin Address structure */
typedef struct IOADCAddressDef
{
	lu_int8_t         port;
    lu_int8_t         pin;
    lu_int8_t         periphFunc;  // GPIO = 0
    lu_uint32_t       muxIoID; //
}IOADCAddressStr;

/*! IO SPI Digipot Address structure */
typedef struct IOSPIDigiPotAddressDef
{
	lu_int8_t         busSspi;   // periphFunc ??
    lu_int8_t         chan;      // Pot Channel
    lu_int8_t         csIoID;    // CS pin ioID
    lu_uint32_t       mode;
}IOSPIDigiPotAddressStr;

/*! IO SPI ADC (MCP3204) Address structure */
typedef struct IOSPIADCAddressDef
{
	lu_int8_t         busSspi;  // periphFunc ??
    lu_int8_t         adcChan;  // ADC Channel
    lu_int8_t         csIoID;   // CS pin ioID
    lu_uint32_t       mode;
}IOSPIADCAddressStr;

/*! IO I2C Expander Address structure */
typedef struct IOI2CExpanderAddressDef
{
	lu_int8_t         busI2c;   // periphFunc ??
    lu_int8_t         ioPin;     //
    lu_int8_t         address;  // I2C address
    lu_uint32_t       mode;
}IOI2CExpanderAddressStr;

/*! IO I2C ADE78xx Address structure */
typedef struct IOI2CADE78xxAddressDef
{
	lu_int8_t         busI2c;   // periphFunc ??
    lu_int8_t         regHigh;  // I2C register
    lu_int8_t         regLow;   // I2C register
    lu_uint32_t       mode;
}IOI2CADE78xxAddressStr;

/*! IO I2C LM73 Temperature Sensor Address structure */
typedef struct IOI2CTempLM73AddressDef
{
	lu_int8_t         busI2c;   // periphFunc ??
	lu_int8_t         address;  // I2C address
    lu_int8_t         unused0;     //
    lu_uint32_t       unused1;
}IOI2CTempLM73AddressStr;

/*! IO HSDC ADE78xx Address structure */
typedef struct IOHSDCADE78xxAddressDef
{
    lu_int8_t         hsdcChan; // HSDC Channel see ADE78XX_HSDC_CH in ADE78xx.h
    lu_int8_t         unused0;
    lu_int8_t         unused1;
    lu_uint32_t       unused2;
}IOHSDCADE78xxAddressStr;

/*! IO FEPIC Address structure */
typedef struct IOFEPICAddressDef
{
    lu_int8_t         sspChan; // IO_BUS_SPI_SSPI_0 or IO_BUS_SPI_SSPI_1
    lu_int8_t         adcChan; // ADC channel see FEPIC_ADC_CH in FrontEndPic.h
    lu_int8_t         unused1;
    lu_uint32_t       unused2;
}IOFEPICAddressAddressStr;

/*! IO I2C NVRAM Address structure */
typedef struct IOI2CNVRAMAddressDef
{
	lu_int8_t         busI2c;   // periphFunc ??
    lu_int8_t         wenIoID;  // WEN pin IOID
    lu_int8_t         address;  // I2C address
    lu_uint32_t       mode;
}IOI2CNVRAMAddressStr;

typedef union IOAddressDef
{
		IOGPIOAddressStr			gpio;
		IOADCAddressStr             adc;
		IOSPIDigiPotAddressStr		spiDigiPot;
		IOI2CExpanderAddressStr		i2cExpander;
		IOSPIADCAddressStr          spiAdc;
		IOI2CADE78xxAddressStr      i2cADE78xx;
		IOHSDCADE78xxAddressStr     hsdcDE78xx;
		IOFEPICAddressAddressStr    spiFEPIC;
		IOI2CNVRAMAddressStr		i2cNVRAM;
		IOI2CTempLM73AddressStr     i2cTempLM73;
}IOAddressStr;

typedef struct IODebounceDef
{
	lu_uint16_t			high2LowMs;
	lu_uint16_t			low2HighMs;
}IODebounceStr;

/*! IO MAP element table structure */
typedef struct IOMapDef
{
	lu_uint16_t	     ioID;
	lu_uint32_t		 ioChan;
	IO_CLASS         ioClass;
	IO_DEV           ioDev;
	IOAddressStr     ioAddr;  // union GPIO/Digipot/I2CIO/I2CNVRAM/SPIADC
	IODebounceStr	 db;      // Default debounce
}IOMapStr;

//Added from commom/library/IOManager/IO/include/IOManagerIO.h
/* IO Table (Shadow copy of all IO) */
/*
typedef struct IOTableDef
{
	lu_int32_t			value;
	lu_int32_t			rawValue;
	lu_int8_t			InitAvgSamples;
	lu_uint32_t         averageSumValue;
	lu_uint16_t			countHigh2Low;
	lu_uint16_t			countLow2High;
	lu_uint16_t			pulseMs;
	lu_uint32_t         pulseTimerCountMs;
	lu_uint8_t			updateFlags;
}IOTableStr;
*/

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
//Added from FMPBoard/src/IOManager/BoardIO/include/
//extern IOTableStr	boardIOTable[];


enum
{
	IO_ID_UNKNOWN = -1, IO_ID_STATE_UNKNOWN = -1,
    IO_ID_OFF = 0, IO_ID_ON = 1
};

extern void IOManagerSetValue_Create(void);
extern void IOManagerSetValue_Destroy(void);
extern lu_uint32_t IOManagerSetValueSpy_GetLastIOID(void);
extern lu_int32_t IOManagerSetValueSpy_GetLastIOIDState(void);
extern SB_ERROR IOManagerSetValue(lu_uint32_t ioID, lu_int32_t ioIDState);
extern SB_ERROR IOManagerGetCalibratedUnitsValue(lu_uint32_t ioID, lu_int32_t *retValuePtr);
extern SB_ERROR IOManagerGetValue(lu_uint32_t ioID, lu_int32_t *retValuePtr);

#endif /* IOMANAGERSPY_H_ */
