/*
 * FakesystemTime.c
 *
 *  Created on: 27 Oct 2014
 *      Author: bogias_a
 */
#include "lu_types.h"
#include "systemTime.h"

static lu_uint32_t fakeTimeMs;


void TimeService_Create(void)
{
    fakeTimeMs = -1;
}

void TimeService_Destroy(void)
{
	fakeTimeMs = 0;
}


void FakesystemTime_SetMs(lu_uint32_t millisecond)
{
	fakeTimeMs = millisecond;
}


lu_uint32_t STGetTime(void)
{
   return fakeTimeMs;
}

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   Time resolution is SysClk . Maximum elapsed time 24 bits
 *
 *   \return Tiem in us
 *
 ******************************************************************************
 */
inline static lu_uint32_t STElapsedTimeTick(lu_uint32_t timeStartTick, lu_uint32_t timeEndTick)
{
    if (timeEndTick > timeStartTick)
    {
        return (timeEndTick - timeStartTick);
    }
    else
    {
        return ( (ST_TIME_TICK_OVERFLOW - timeStartTick + timeEndTick) + 1);
    }
}
