#ifndef _CAN_FRAMING_INCLUDED
#define _CAN_FRAMING_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "systemUtils.h"

//#include "ModuleProtocol.h"



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define CAN_TICK_MS			10

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*! Filter table structure */
typedef struct filterTableDef
{
    lu_uint8_t messageType;
    lu_uint8_t messageID  ;
    lu_bool_t  broadcast  ;
    lu_bool_t  fragmented ;
}filterTableStr;

/*!
 * Pin Map for CAN Controller
 */
typedef struct CANFramingMapDef
{
    lu_uint32_t peripheralCAN;
    lu_uint32_t peripheralGPIO;
    lu_uint32_t gpioPortBase;
    lu_uint32_t CanTxPin;
    lu_uint32_t CanTxConfigurePin;
    lu_uint32_t CanRxPin;
    lu_uint32_t CanRxConfigurePin;
    lu_uint32_t CanBase;
    lu_uint32_t CanInt;
}CANFramingMapStr;


/*!
 * Structure used to define a CAN message
 */
typedef struct CANFramingMsgDef
{
    /* Buffer ID. Used by the CAN Framing library */
    lu_uint8_t  ID         ;
    /* Message type */
    lu_uint8_t  messageType;
    /* Message ID */
    lu_uint8_t  messageID  ;
    /* Source address */
    lu_uint8_t  deviceIDSrc;
    /* Source device type */
    lu_uint8_t  deviceSrc  ;
    /* Destination address */
    lu_uint8_t  deviceIDDst;
    /* Destination device type */
    lu_uint8_t  deviceDst  ;
    /* Message length */
    lu_uint32_t  msgLen    ;
    /* Message buffer */
    lu_uint8_t *msgBufPtr  ;
}CANFramingMsgStr;

/*!
 * CAN Framing callback signature
 */
typedef SB_ERROR (*CANFramingCB)(CANFramingMsgStr *msgPtr);


extern SB_ERROR CANFramingInit( lu_uint32_t       clock    ,
                         lu_uint32_t       bitRate  ,
		                 lu_uint8_t        device   ,
		                 lu_uint8_t        deviceID ,
		                 CANFramingMapStr *pinMapPtr,
		                 SUIRQPriorityStr  priority ,
		                 CANFramingCB      callBack
		               );

extern SB_ERROR CANFramingTick(void);

extern SB_ERROR CANFramingAddFilter( const filterTableStr *filterTable,
                              lu_uint32_t size
                            );

extern SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr);

extern void CANFramingEnterBusOff(void);

extern SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr);

extern SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID);

extern void CANFramingIntHandler(void);

#endif /* _CAN_FRAMING_INCLUDED */
