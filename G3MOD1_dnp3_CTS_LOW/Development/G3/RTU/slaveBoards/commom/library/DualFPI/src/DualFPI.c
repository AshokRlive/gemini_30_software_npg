/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Dual FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/07/14      fryers_j     Initial version. 
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include "lpc17xx_gpio.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"
#include "BoardIOMap.h"
#include "BoardIO.h"
#include "IOManagerFrontEndPICIOUpdate.h"
#include "systemTime.h"

#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"

#include "DualFPI.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct FPMConfigParamsDef
{

	FPMConfigStr cfg;

	//lu_uint32_t timeSinceFaultMs;

	/* Define any other stuff required for timming FPI before trigger event ....*/
}FPMConfigParamsStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR DualFPIConfig(FPMConfigStr *msgPtr);
SB_ERROR DualFPIOperate(FPIOperateStr *msgPtr);

void DualFPIAdcSampleHandler(lu_uint32_t ioID, ModuleTimeStr canTime);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported CAN messages */
static const filterTableStr DualFPIModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! FPM commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C            , LU_FALSE , 0      },

    /*! FPI Config */
    {  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_FPM_C                       , LU_FALSE , 1      }
};

static FPMConfigParamsStr fpmConfigParams[FPI_CONFIG_CH_LAST];
static lu_uint32_t InitTimeSinceOCFaultMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t TimeSinceOCFaultAlarmMs[FPI_CONFIG_CH_LAST];

static lu_uint32_t InitTimeSinceEarthFaultMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t TimeSinceEarthFaultAlarmMs[FPI_CONFIG_CH_LAST];

static lu_uint32_t InitTimeSinceInstantOCFaultMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t TimeSinceInstantOCFaultAlarmMs[FPI_CONFIG_CH_LAST];
static lu_uint8_t  InstantOvercurrentEventDetected[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];

static lu_uint32_t InitTimeSinceInstantEarthFaultMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t TimeSinceInstantEarthFaultAlarmMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t InstantEarthFaultEventDetected[FPI_CONFIG_CH_LAST];

static lu_uint32_t OverloadDurationMs[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];
static lu_uint32_t InitOverloadTimeMs[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];
static lu_uint8_t  OverloadEventDetected[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];
static lu_uint32_t OvercurrentDurationMs[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];
static lu_uint32_t InitOvercurrentTimeMs[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];
static lu_uint8_t  OvercurrentEventDetected[FEPIC_ADC_CH_LAST][FPI_CONFIG_CH_LAST];

static lu_uint32_t EarthFaultDurationMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t InitEarthFaultTimeMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t EarthFaultEventDetected[FPI_CONFIG_CH_LAST];

static lu_uint32_t PersistentEarthFaultMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t PersistentEarthFaultDurationMs[FPI_CONFIG_CH_LAST];
static lu_uint32_t CurrentEarthFaultTimeMs[FPI_CONFIG_CH_LAST];

//static lu_uint32_t MaxValue[BOARD_IO_MAX_IDX + 1];
//static lu_uint32_t SumLengthCounter[FPI_CONFIG_CH_LAST];

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR DualFPIInit(void)
{
	SB_ERROR retError;

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled = LU_TRUE;
	/*FPI config parameter for development purposes only*/

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.minFaultDurationMs = 71;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.timedFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.InstantFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.minOverloadDurationMs = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.overloadCurrent = 1700;

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.earthFault.minFaultDurationMs = 50;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.earthFault.timedFaultCurrent = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.earthFault.InstantFaultCurrent = 100;

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.selfResetMs = 10500;

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled = LU_TRUE;

	/*FPI config parameter for development purposes only*/

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.minFaultDurationMs = 71;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.timedFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.InstantFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.minOverloadDurationMs = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.overloadCurrent = 1700;

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.earthFault.minFaultDurationMs = 50;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.earthFault.timedFaultCurrent = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.earthFault.InstantFaultCurrent = 100;

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.selfResetMs = 10500;

	/* Setup CAN Msg filter */
	retError = CANFramingAddFilter( DualFPIModulefilterTable,
								    SU_TABLE_SIZE(DualFPIModulefilterTable,
											      filterTableStr)
								 );

	/* Register ISR Call back */
	IOManagerFEPICIORegisterCallBack(DualFPIAdcSampleHandler);

	return retError;
}

SB_ERROR DualFPICANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC;

	LU_UNUSED( time );

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_FPM_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FPMConfigStr))
			{
				retError = DualFPIConfig((FPMConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FPIOperateStr))
			{
				retError = DualFPIOperate((FPIOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}

SB_ERROR DualFPIOvercurrentFaultDetection( FEPIC_ADC_CH adcChan, FPI_CONFIG_CH fpiChan, lu_uint32_t *value)
{
	SB_ERROR 	retError;

	// Disable the instant overcurrent function if it has found a fault
	// The function is reset remotely or through a timer
	if ( InitTimeSinceInstantOCFaultMs[fpiChan] <= 0)
	{
		if ((*value) >= fpmConfigParams[fpiChan].cfg.phaseFault.InstantFaultCurrent)
		{
			InstantOvercurrentEventDetected[adcChan][fpiChan]++;
		}
	}

	//Reset overload timer if the phase current value
	//has fallen below the overload threshold level
	if ((OverloadDurationMs[adcChan][fpiChan] >= 1) && ((*value) < fpmConfigParams[fpiChan].cfg.phaseFault.overloadCurrent))
	{
		//reset overload timer
		OverloadDurationMs[adcChan][fpiChan] = 0;
		InitOverloadTimeMs[adcChan][fpiChan] = 0;
		OverloadEventDetected[adcChan][fpiChan] = 0;
	}

	//Reset the overcurrent timer if the phase value
	//has fallen below the overcurrent threshold level
	//once the counter has been triggered at least once
	if ((OvercurrentDurationMs[adcChan][fpiChan]>= 1) && ((*value) < fpmConfigParams[fpiChan].cfg.phaseFault.timedFaultCurrent))
	{
		//Reset overcurrent timer
		OvercurrentDurationMs[adcChan][fpiChan] = 0;
		InitOvercurrentTimeMs[adcChan][fpiChan] = 0;
		OvercurrentEventDetected[adcChan][fpiChan] = 0;
	}

	// Disable the overcurrent function if it has found a fault
	// The function is reset remotely or through a timer
	if ( InitTimeSinceOCFaultMs[fpiChan] <= 0)
	{
		//Check for motor and transformer inrush(/overload) current
		if ((*value) >= fpmConfigParams[fpiChan].cfg.phaseFault.overloadCurrent)
		{
			//OverloadDurationMs[ioID] = STGetTime();
			OverloadEventDetected[adcChan][fpiChan]++;

			//first overload event detected
			//get initial time
			if(OverloadEventDetected[adcChan][fpiChan] == 1)
			{
				InitOverloadTimeMs[adcChan][fpiChan] = STGetTime();
			}

			//start overload timer
			OverloadDurationMs[adcChan][fpiChan] = STGetTime();
			//measure the time since the first detected overload event
			OverloadDurationMs[adcChan][fpiChan] = STElapsedTime( InitOverloadTimeMs[adcChan][fpiChan], OverloadDurationMs[adcChan][fpiChan]);

			//Check the current overload duration has not exceeded the threshold
			if (OverloadDurationMs[adcChan][fpiChan] >= fpmConfigParams[fpiChan].cfg.phaseFault.minOverloadDurationMs)
			{
				//Alarm for overcurrent event
				//Load global fault timer
				InitTimeSinceOCFaultMs[fpiChan] = STGetTime();

				//Reset timer variables
				InitOverloadTimeMs[adcChan][fpiChan] = 0;
				OverloadDurationMs[adcChan][fpiChan] = 0;
				OverloadEventDetected[adcChan][fpiChan] = 0;

				//Set virtual point alarm
				switch (fpiChan)
				{
				case FPI_CONFIG_CH_FPI1:
					retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 1);
					break;

				case FPI_CONFIG_CH_FPI2:
					retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 1);
					break;

				default:
					retError = SB_ERROR_PARAM;
					break;
				}
			}

			//Print overload warning each time APART from the last
			//when the Alarm should be printed in the above if loop
			else
			{
				//Do nothing for now
 				//OR inform of Overload warning
			}
		}

		//Check the measured phase value is between Overcurrent threshold level
		//AND below fleeting overload current level
		if (((*value) >= fpmConfigParams[fpiChan].cfg.phaseFault.timedFaultCurrent) && (((*value) <  fpmConfigParams[fpiChan].cfg.phaseFault.overloadCurrent)))
		{
			//increment counter
			OvercurrentEventDetected[adcChan][fpiChan]++;

			//first time
			if(OvercurrentEventDetected[adcChan][fpiChan] == 1)
			{
				InitOvercurrentTimeMs[adcChan][fpiChan] = STGetTime();
				//Remove line below for release code
//				retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);
			}
		}
	}//disable fpi

	else
	{
		retError = SB_ERROR_PARAM;
	}

	return retError;
}//overcurrent_fault_detection

SB_ERROR DualFPIEarthFaultDetection( FPI_CONFIG_CH fpiChan, lu_int32_t *value)
{
	SB_ERROR 	retError;

	retError = SB_ERROR_NONE;

	//Remove line below for release code
	//GPIO_ClearValue(FPI1_LED_RED_PORT, FPI1_LED_RED_PIN);

	if (PersistentEarthFaultMs[fpiChan] >= 1)
	{
		CurrentEarthFaultTimeMs[fpiChan] = STGetTime();
		PersistentEarthFaultDurationMs[fpiChan]= STElapsedTime(PersistentEarthFaultMs[fpiChan], CurrentEarthFaultTimeMs[fpiChan]);
	}

	// Disable the Instant Earth Fault function if it has found a fault
	// The function is reset remotely or through a timer
	if (InitTimeSinceInstantEarthFaultMs[fpiChan] <= 0)
	{
		if ((*value) >= fpmConfigParams[fpiChan].cfg.earthFault.InstantFaultCurrent)
		{
			InstantEarthFaultEventDetected[fpiChan]++;
		}
	}

	// Disable the Earth Fault function if it has found a fault
	// The function is reset remotely or through a timer
	if (InitTimeSinceEarthFaultMs[fpiChan] <= 0)
	{
		//Reset the earth fault counter if the sum value
		//has fallen below the earth fault threshold
		//once the timer has been triggered at least once
		if ((EarthFaultDurationMs[fpiChan] >= 1) &&
			((*value) < fpmConfigParams[fpiChan].cfg.earthFault.timedFaultCurrent) &&
			(PersistentEarthFaultDurationMs[fpiChan] > SUM_TIME_PER_HL_CL_MS))
		{
			//Reset timer
			EarthFaultDurationMs[fpiChan] = 0;
			InitEarthFaultTimeMs[fpiChan] = 0;
			EarthFaultEventDetected[fpiChan] = 0;
			PersistentEarthFaultMs[fpiChan] = 0;
			PersistentEarthFaultDurationMs[fpiChan] = 0;
		}

		//Check for Earth Fault
		if ((*value) >= fpmConfigParams[fpiChan].cfg.earthFault.timedFaultCurrent)
		{
			EarthFaultEventDetected[fpiChan]++;

			//first earth fault event?
			if(EarthFaultEventDetected[fpiChan] == 1)
			{
				//set the timer value point at which a fault first occurred
				InitEarthFaultTimeMs[fpiChan] = STGetTime();
				PersistentEarthFaultMs[fpiChan] = STGetTime();

				//Remove line below for release code
//				retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);
			}

			if (PersistentEarthFaultDurationMs[fpiChan] >= (SUM_TIME_PER_HL_CL_MS-1))
			{
				PersistentEarthFaultMs[fpiChan] = STGetTime();
			}
		}
	}//if (InitTimeSinceEarthFaultMs[fpiChan] <= 0)

	return retError;
}// Earth fault function

SB_ERROR DualFPIOvercurrentFaultAlarm( FEPIC_ADC_CH adcChan, FPI_CONFIG_CH fpiChan)
{
	SB_ERROR 	retError;

	if (OvercurrentEventDetected[adcChan][fpiChan] >= 1)
	{
		//Load timer
		OvercurrentDurationMs[adcChan][fpiChan] = STGetTime();
		//measure the time since the first detected overcurrent event
		OvercurrentDurationMs[adcChan][fpiChan] = STElapsedTime( InitOvercurrentTimeMs[adcChan][fpiChan], OvercurrentDurationMs[adcChan][fpiChan]);

		//Are the Overcurrent fault alarm condition met
		if (OvercurrentDurationMs[adcChan][fpiChan] >= (fpmConfigParams[fpiChan].cfg.phaseFault.minFaultDurationMs + 1))
		{
			//Alarm for overcurrent event
			//Load global fault timer
			InitTimeSinceOCFaultMs[fpiChan] = STGetTime();

			//Remove line below for release code
//			retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);

			//Reset timer variables
			OvercurrentDurationMs[adcChan][fpiChan] = 0;
			InitOvercurrentTimeMs[adcChan][fpiChan] = 0;
			OvercurrentEventDetected[adcChan][fpiChan] = 0;

			//Set virtual point alarm
			switch (fpiChan)
			{
			case FPI_CONFIG_CH_FPI1:
				retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);

				retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 1);
				break;

			case FPI_CONFIG_CH_FPI2:
				retError = IOManagerSetValue(IO_ID_LED_FPI2, 1);

				retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 1);
				break;

			default:
				retError = SB_ERROR_PARAM;
				break;
			}
		}
	}//OvercurrentEventDetected[ioID] >= 1

	//Print instant overcurrent warning
	if (InstantOvercurrentEventDetected[adcChan][fpiChan] > 0)
	{
		InstantOvercurrentEventDetected[adcChan][fpiChan] = 0;
		InitTimeSinceInstantOCFaultMs[fpiChan] = STGetTime();
		//Set virtual point 'instantaneous' alarm
		switch (fpiChan)
		{
		case FPI_CONFIG_CH_FPI1:
			retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 1);

			retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);
			break;

		case FPI_CONFIG_CH_FPI2:
			retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 1);

			retError = IOManagerSetValue(IO_ID_LED_FPI2, 1);
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
	}

	//Reset all Alarms at the same time irrespective of their order of triggering
	if( InitTimeSinceOCFaultMs[fpiChan] > 0)
	{
		//
		TimeSinceOCFaultAlarmMs[fpiChan] = STGetTime();
		TimeSinceOCFaultAlarmMs[fpiChan] = STElapsedTime( InitTimeSinceOCFaultMs[fpiChan], TimeSinceOCFaultAlarmMs[fpiChan]);

		if (TimeSinceOCFaultAlarmMs[fpiChan] >= fpmConfigParams[fpiChan].cfg.selfResetMs)
		{
			//Clear virtual point alarms
			switch (fpiChan)
			{
			case FPI_CONFIG_CH_FPI1:
				retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);
				break;

			case FPI_CONFIG_CH_FPI2:
				retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);
				break;

			default:
				retError = SB_ERROR_PARAM;
				break;
			}

			//Reset self timer variables
			TimeSinceInstantOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceInstantOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
			TimeSinceOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
		}
	}

	//Reset all Alarms at the same time irrespective of their order of triggering
	if( InitTimeSinceInstantOCFaultMs[fpiChan] > 0)
	{
		//
		TimeSinceInstantOCFaultAlarmMs[fpiChan] = STGetTime();
		TimeSinceInstantOCFaultAlarmMs[fpiChan] = STElapsedTime( InitTimeSinceInstantOCFaultMs[fpiChan], TimeSinceInstantOCFaultAlarmMs[fpiChan]);

		if (TimeSinceInstantOCFaultAlarmMs[fpiChan] >= fpmConfigParams[fpiChan].cfg.selfResetMs)
		{
			//Clear virtual point alarms
			switch (fpiChan)
			{
			case FPI_CONFIG_CH_FPI1:
				retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);
				break;

			case FPI_CONFIG_CH_FPI2:
				retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);
				break;

			default:
				retError = SB_ERROR_PARAM;
				break;
			}

			//Reset self timer variables
			TimeSinceInstantOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceInstantOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
			TimeSinceOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
		}

	}

	return SB_ERROR_NONE;
}


SB_ERROR DualFPIEarthFaultAlarm( FPI_CONFIG_CH fpiChan)
{
	SB_ERROR 	retError;

	if (EarthFaultEventDetected[fpiChan] >= 1)
	{
		//start overload timer
		EarthFaultDurationMs[fpiChan] = STGetTime();
		EarthFaultDurationMs[fpiChan]= STElapsedTime( InitEarthFaultTimeMs[fpiChan], EarthFaultDurationMs[fpiChan]);


		//Are the Earth Fault timing alarm condition met
		if (EarthFaultDurationMs[fpiChan] >= fpmConfigParams[fpiChan].cfg.earthFault.minFaultDurationMs)
		{
			//Alarm for earth fault event
			//Load global fault timer
			InitTimeSinceEarthFaultMs[fpiChan] = STGetTime();

			//Remove line below for release code
//			retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);

			//Reset timer variables
			EarthFaultDurationMs[fpiChan] = 0;
			InitEarthFaultTimeMs[fpiChan] = 0;
			EarthFaultEventDetected[fpiChan] = 0;
			PersistentEarthFaultMs[fpiChan] = 0;
			PersistentEarthFaultDurationMs[fpiChan] = 0;

			//Set virtual point alarm
			switch (fpiChan)
			{
			case FPI_CONFIG_CH_FPI1:
				retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);

				retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 1);
				break;

			case FPI_CONFIG_CH_FPI2:
				retError = IOManagerSetValue(IO_ID_LED_EFI2, 1);

				retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 1);
				break;

			default:
				retError = SB_ERROR_PARAM;
				break;
			}
		}//Check for Earth Fault timing

	}//if (firstEarthFaultEventDetected[ioID] >= 1)

	//Print Earth Fault warning each time APART from the last
	//when the Alarm should be printed in the above if loop
	if (InstantEarthFaultEventDetected[fpiChan] > 0)
	{
		InstantEarthFaultEventDetected[fpiChan] = 0;
		InitTimeSinceInstantEarthFaultMs[fpiChan] = STGetTime();

		//Set virtual point 'instantaneous' alarm
		switch (fpiChan)
		{
		case FPI_CONFIG_CH_FPI1:
			retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 1);

			retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);
			break;

		case FPI_CONFIG_CH_FPI2:
			retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 1);

			retError = IOManagerSetValue(IO_ID_LED_EFI2, 1);
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
	}//Alarm for instantaneous earth fault

	//Reset all Alarms at the same time irrespective of their order of triggering
	if (InitTimeSinceEarthFaultMs[fpiChan] > 0)
	{
		TimeSinceEarthFaultAlarmMs[fpiChan] = STGetTime();
		TimeSinceEarthFaultAlarmMs[fpiChan] = STElapsedTime( InitTimeSinceEarthFaultMs[fpiChan], TimeSinceEarthFaultAlarmMs[fpiChan]);

		//Clear self reset timer when conditions are met
		if (TimeSinceEarthFaultAlarmMs[fpiChan] >= fpmConfigParams[fpiChan].cfg.selfResetMs)
		{
			//Clear virtual point alarms
			switch (fpiChan)
			{
			case FPI_CONFIG_CH_FPI1:
				retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);
				break;

			case FPI_CONFIG_CH_FPI2:
				retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);
				break;

			default:
				retError = SB_ERROR_PARAM;
				break;
			}
			//Reset self timer variables
			TimeSinceInstantOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceInstantOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
			TimeSinceOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
		}
	}

	//Reset all Alarms at the same time irrespective of their order of triggering
	if( InitTimeSinceInstantEarthFaultMs[fpiChan] > 0)
	{
		//
		TimeSinceInstantEarthFaultAlarmMs[fpiChan] = STGetTime();
		TimeSinceInstantEarthFaultAlarmMs[fpiChan] = STElapsedTime( InitTimeSinceInstantEarthFaultMs[fpiChan], TimeSinceInstantEarthFaultAlarmMs[fpiChan]);

		if (TimeSinceInstantEarthFaultAlarmMs[fpiChan] >= fpmConfigParams[fpiChan].cfg.selfResetMs)
		{
			//Clear virtual point alarms
			switch (fpiChan)
			{
			case FPI_CONFIG_CH_FPI1:
				retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);
				break;

			case FPI_CONFIG_CH_FPI2:
				retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);
				retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);

				retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
				retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);
				break;

			default:
				retError = SB_ERROR_PARAM;
				break;
			}

			//Reset self timer variables
			TimeSinceInstantOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceInstantOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
			TimeSinceOCFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceOCFaultMs[fpiChan] = 0;
			TimeSinceEarthFaultAlarmMs[fpiChan] = 0;
			InitTimeSinceEarthFaultMs[fpiChan] = 0;
		}

	}

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR DualFPIConfig(FPMConfigStr *msgPtr)
{
	SB_ERROR retError;
	ModReplyStr reply;

	retError = SB_ERROR_NONE;

	switch (msgPtr->fpiConfigChannel)
	{
	case FPI_CONFIG_CH_FPI1:
	case FPI_CONFIG_CH_FPI2:
		/* Save configuration + Enable FPI */
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.earthFault = msgPtr->earthFault;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.phaseFault = msgPtr->phaseFault;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.phaseFault.overloadCurrent = 20000;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.selfResetMs = msgPtr->selfResetMs;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.enabled    = msgPtr->enabled;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	reply.channel = msgPtr->fpiConfigChannel;
	reply.status  = retError == SB_ERROR_NONE ? REPLY_STATUS_OKAY : REPLY_STATUS_ERROR;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
			            MODULE_MSG_ID_CFG_FPM_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualFPIOperate(FPIOperateStr *msgPtr)
{
	lu_uint16_t i = 0;
	SB_ERROR retError;
	ModReplyStr reply;

	retError = SB_ERROR_NONE;

	switch (msgPtr->channel)
	{
	case FPM_CH_FPI_1_TEST:
		/* Disable FPI and force all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled = LU_FALSE;

		retError = IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 1);

		retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);
		retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);

		retError = IOManagerSetValue(IO_ID_FPI1_TEST_ACTIVE, 1);
		break;

	case FPM_CH_FPI_1_RESET:
		/* Enable FPI and clear all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled = LU_TRUE;

		retError = IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);

		retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
		retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);

		retError = IOManagerSetValue(IO_ID_FPI1_TEST_ACTIVE, 0);

		//Clear all FPI1 Earth, Overload and overcurrent variables
		TimeSinceOCFaultAlarmMs[FPI_CONFIG_CH_FPI1] = 0;
		InitTimeSinceOCFaultMs[FPI_CONFIG_CH_FPI1] = 0;
		TimeSinceEarthFaultAlarmMs[FPI_CONFIG_CH_FPI1] = 0;
		InitTimeSinceEarthFaultMs[FPI_CONFIG_CH_FPI1] = 0;
		TimeSinceInstantEarthFaultAlarmMs[FPI_CONFIG_CH_FPI1] = 0;
		InitTimeSinceInstantEarthFaultMs[FPI_CONFIG_CH_FPI1] = 0;
		TimeSinceInstantOCFaultAlarmMs[FPI_CONFIG_CH_FPI1] = 0;
		InitTimeSinceInstantOCFaultMs[FPI_CONFIG_CH_FPI1] = 0;
		CurrentEarthFaultTimeMs[FPI_CONFIG_CH_FPI1] = 0;
		EarthFaultDurationMs[FPI_CONFIG_CH_FPI1] = 0;
		InitEarthFaultTimeMs[FPI_CONFIG_CH_FPI1] = 0;
		EarthFaultEventDetected[FPI_CONFIG_CH_FPI1] = 0;
		PersistentEarthFaultMs[FPI_CONFIG_CH_FPI1] = 0;
		PersistentEarthFaultDurationMs[FPI_CONFIG_CH_FPI1] = 0;
		InstantEarthFaultEventDetected[FPI_CONFIG_CH_FPI1] = 0;

		for (i = FEPIC_ADC_CH_L1; i< FEPIC_ADC_CH_SUM+1; i++)
		{
			OvercurrentDurationMs[i][FPI_CONFIG_CH_FPI1] = 0;
			InitOvercurrentTimeMs[i][FPI_CONFIG_CH_FPI1] =  0;
			OvercurrentEventDetected[i][FPI_CONFIG_CH_FPI1] = 0;
			OverloadDurationMs[i][FPI_CONFIG_CH_FPI1] = 0;
			InitOverloadTimeMs[i][FPI_CONFIG_CH_FPI1] = 0;
			OverloadEventDetected[i][FPI_CONFIG_CH_FPI1] = 0;
			InstantOvercurrentEventDetected[i][FPI_CONFIG_CH_FPI1] = 0;
		}
		break;

	case FPM_CH_FPI_2_TEST:
		/* Disable FPI and force all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled = LU_FALSE;

		retError = IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 1);

		retError = IOManagerSetValue(IO_ID_LED_FPI2, 1);
		retError = IOManagerSetValue(IO_ID_LED_EFI2, 1);

		retError = IOManagerSetValue(IO_ID_FPI2_TEST_ACTIVE, 1);
		break;

	case FPM_CH_FPI_2_RESET:
		/* Enable FPI and clear all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled = LU_TRUE;

		retError = IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);

		retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
		retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);

		retError = IOManagerSetValue(IO_ID_FPI2_TEST_ACTIVE, 0);

		//Clear all FPI1 Earth, Overload and overcurrent variables
		TimeSinceOCFaultAlarmMs[FPI_CONFIG_CH_FPI2] = 0;
		InitTimeSinceOCFaultMs[FPI_CONFIG_CH_FPI2] = 0;
		TimeSinceEarthFaultAlarmMs[FPI_CONFIG_CH_FPI2] = 0;
		InitTimeSinceEarthFaultMs[FPI_CONFIG_CH_FPI2] = 0;
		TimeSinceInstantEarthFaultAlarmMs[FPI_CONFIG_CH_FPI2] = 0;
		InitTimeSinceInstantEarthFaultMs[FPI_CONFIG_CH_FPI2] = 0;
		TimeSinceInstantOCFaultAlarmMs[FPI_CONFIG_CH_FPI2] = 0;
		InitTimeSinceInstantOCFaultMs[FPI_CONFIG_CH_FPI2] = 0;
		CurrentEarthFaultTimeMs[FPI_CONFIG_CH_FPI2] = 0;
		EarthFaultDurationMs[FPI_CONFIG_CH_FPI2] = 0;
		InitEarthFaultTimeMs[FPI_CONFIG_CH_FPI2] = 0;
		EarthFaultEventDetected[FPI_CONFIG_CH_FPI2] = 0;
		PersistentEarthFaultMs[FPI_CONFIG_CH_FPI2] = 0;
		PersistentEarthFaultDurationMs[FPI_CONFIG_CH_FPI2] = 0;
		InstantEarthFaultEventDetected[FPI_CONFIG_CH_FPI2] = 0;

		for (i = FEPIC_ADC_CH_L1; i< FEPIC_ADC_CH_SUM+1; i++)
		{
			OvercurrentDurationMs[i][FPI_CONFIG_CH_FPI2] = 0;
			InitOvercurrentTimeMs[i][FPI_CONFIG_CH_FPI2] = 0;
			OvercurrentEventDetected[i][FPI_CONFIG_CH_FPI2] = 0;
			OverloadDurationMs[i][FPI_CONFIG_CH_FPI2] = 0;
			InitOverloadTimeMs[i][FPI_CONFIG_CH_FPI2] = 0;
			OverloadEventDetected[i][FPI_CONFIG_CH_FPI2] = 0;
			InstantOvercurrentEventDetected[i][FPI_CONFIG_CH_FPI2] = 0;
		}
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	reply.channel = msgPtr->channel;
	reply.status  = retError == SB_ERROR_NONE ? REPLY_STATUS_OKAY : REPLY_STATUS_ERROR;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
			            MODULE_MSG_ID_CMD_FPI_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief
 *
 *   Called from ISR!!
 *
 *   \param ioID
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualFPIAdcSampleHandler( lu_uint32_t ioID, ModuleTimeStr canTime)
{
	lu_int32_t  		value;
	SB_ERROR    		retError;
	lu_uint32_t 		time;
	ModuleDEventStr     digEvent;
	lu_uint32_t	 		digIoID;
	lu_uint32_t			digIoChan;
	FEPIC_ADC_CH 		adcChan;
	FPI_CONFIG_CH 		fpiChan;

	time = STGetTime();

	retError = SB_ERROR_NONE;

    /* Configure map of ADC channels via ioID */
	adcChan = BoardIOMap[ioID].ioAddr.spiFEPIC.adcChan;
	fpiChan = BoardIOMap[ioID].ioAddr.spiFEPIC.sspChan;

	// use to calculate elapsed time : STElapsedTime( initTime , time);

	if (retError == SB_ERROR_NONE)
	{
		switch (ioID)
		{
		case IO_ID_FPI1_PHASE_A_CURRENT:
		case IO_ID_FPI1_PHASE_B_CURRENT:
		case IO_ID_FPI1_PHASE_C_CURRENT:
		case IO_ID_FPI2_PHASE_A_CURRENT:
		case IO_ID_FPI2_PHASE_B_CURRENT:
		case IO_ID_FPI2_PHASE_C_CURRENT:
			if (fpmConfigParams[fpiChan].cfg.enabled == LU_TRUE)
			{
				retError = IOManagerGetCalibratedUnitsValue(ioID, &value);
				retError = DualFPIOvercurrentFaultDetection(adcChan, fpiChan, &value);
				//Check all phases and neutrals for Alarm violations
				retError = DualFPIOvercurrentFaultAlarm (FEPIC_ADC_CH_L1, fpiChan);
				retError = DualFPIOvercurrentFaultAlarm (FEPIC_ADC_CH_L2, fpiChan);
				retError = DualFPIOvercurrentFaultAlarm (FEPIC_ADC_CH_L3, fpiChan);
			}
			break;

		case IO_ID_FPI1_PHASE_N_CURRENT_DETECT:
			if (fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled == LU_TRUE)
			{
				retError = IOManagerGetValue(ioID, &value);
				retError = CalibrationConvertValue(ioChanAIMap[BoardIOMap[IO_ID_FPI1_PHASE_N_CURRENT].ioChan].calID, value, &value);
				retError = DualFPIEarthFaultAlarm(fpiChan);
				retError = DualFPIEarthFaultDetection(fpiChan, &value);
			}
			break;



		case IO_ID_FPI2_PHASE_N_CURRENT_DETECT:
			if (fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled == LU_TRUE)
			{
				retError = IOManagerGetValue(ioID, &value);
				retError = CalibrationConvertValue(ioChanAIMap[BoardIOMap[IO_ID_FPI2_PHASE_N_CURRENT].ioChan].calID, value, &value);
				retError = DualFPIEarthFaultAlarm( fpiChan);
				retError = DualFPIEarthFaultDetection( fpiChan, &value);
			}
			break;

		default:
			break;
		}

		/* Generate CAN digital events for Virtual IO channels */
		for (digIoID = IO_ID_FPI1_COMMON_FAULT; ioID <= IO_ID_FPI2_INST_EARTH_FAULT; ioID++)
		{
			digIoChan = BoardIOMap[digIoID].ioChan;

			/* Is enabled, so report event */
			if (ioManagerIOEventingEnable == LU_TRUE &&
				ioChanDIMap[digIoChan].eventEnable &&
				(boardIOTable[digIoID].updateFlags & IOM_UPDATEFLAG_INPUT_EVENT)
			   )
			{
				/* Generate an event */
				digEvent.channel 	= digIoChan;
				digEvent.time		= canTime;
				digEvent.value		= boardIOTable[digIoID].value;
				digEvent.isOnline   = (boardIOTable[digIoID].updateFlags & IOM_UPDATEFLAG_OFFLINE) ? 0 : 1;

				CANCSendMCM( MODULE_MSG_TYPE_EVENT,
							 MODULE_MSG_ID_EVENT_DIGITAL,
							 sizeof(ModuleDEventStr),
							 (lu_uint8_t *)&digEvent);

				/* Clear the event flag */
				boardIOTable[digIoID].updateFlags &= ~IOM_UPDATEFLAG_INPUT_EVENT;
			}
		}
	}
}
/*
lu_bool_t DualFPISumErrorCorrection(lu_uint32_t ioID, lu_uint32_t FPI_CONFIG_CH, lu_uint8_t length, lu_int16_t *value)
{
	static lu_int16_t phaseValueA;

	switch (ioID)
	{
		case IO_ID_FPI1_PHASE_A_CURRENT:
		phaseValueA = *value;
			break;

		case IO_ID_FPI1_PHASE_B_CURRENT:

			break;

		case IO_ID_FPI1_PHASE_C_CURRENT:

			break;

		case IO_ID_FPI1_PHASE_N_CURRENT:
			SumLengthCounter[FPI_CONFIG_CH]++;

			if (SumLengthCounter[FPI_CONFIG_CH] < length)
			{
				return LU_FALSE;
			}

			else if (SumLengthCounter[FPI_CONFIG_CH] > length)
			{
				if (abs((phaseValueA - PHASE_VALUE_400_A) <= PHASE_TOLERANCE_VALUE_25_A))
				{
					//Do nothing
				}

				else
				{
					//Correct for sum error
					*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_5_A);
					SumLengthCounter[FPI_CONFIG_CH] = 0;
				}
				//This will allow the earth fault function to run irrespective
				//of whether correction is needed or not
				return LU_TRUE;
			}
			break;

		case IO_ID_FPI2_PHASE_A_CURRENT:

			break;

		case IO_ID_FPI2_PHASE_B_CURRENT:

			break;

		case IO_ID_FPI2_PHASE_C_CURRENT:

			break;

		default:
			break;
	}
}
*/

/*
 *********************** End of file ******************************************
 */
