
#include <embUnit/embUnit.h>
#include <textui/TextOutputter.h>
#include <textui/TextUIRunner.h>


TestRef DualFPITest_tests(void);

int main(int argc, const char* argv[])
{
    TextUIRunner_setOutputter (TextOutputter_outputter());
    TextUIRunner_start();
    TextUIRunner_runTest(DualFPITest_tests());
    TextUIRunner_end();
    return 0;
}
