cmake_minimum_required(VERSION 2.8)

add_library( FakeSysTime STATIC
             systemTime.c
             IOManager.c
             CANProtocolFraming.c
             #CANProtocolCodec.c
           ) 
           
ADD_DEPENDENCIES(FakeSysTime globalHeaders)

include_directories(./)

# Generate Doxygen documentation
# gen_doxygen("main" "")



