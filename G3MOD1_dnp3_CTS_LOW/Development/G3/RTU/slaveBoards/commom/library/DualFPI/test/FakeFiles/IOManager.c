/*
 * IOManagerSpy.c
 *
 *  Created on: 27 Oct 2014
 *      Author: bogias_a
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "IOManager.h"
//#include "IOManagerIO.h"
#include "ModuleProtocol.h"

//#include "BoardIO.h"
//#include "BoardIOChanMap.h"
#include "BoardIOMap.h"


static lu_uint32_t lastIOID;
static lu_int32_t lastIOIDState;

//Added from FMPBoard/src/IOManager/BoardIO/src/
//IOTableStr	boardIOTable[IO_ID_LAST];

void IOManagerSetValue_Create(void)
{
	lastIOID = IO_ID_UNKNOWN;
    lastIOIDState = IO_ID_STATE_UNKNOWN;
}

void IOManagerSetValue_Destroy(void)
{
	lastIOID = 0;
    lastIOIDState = 0;
}

lu_uint32_t IOManagerSetValueSpy_GetLastIOID(void)
{
    return lastIOID;
}

lu_int32_t IOManagerSetValueSpy_GetLastIOIDState(void)
{
    return lastIOIDState;
}

SB_ERROR IOManagerSetValue(lu_uint32_t ioID, lu_int32_t ioIDState)
{
	 SB_ERROR 			retError;

	 lastIOID = ioID;
	 lastIOIDState = ioIDState;
	 retError = SB_ERROR_NONE;
}


SB_ERROR IOManagerGetCalibratedUnitsValue(lu_uint32_t ioID, lu_int32_t *retValuePtr)
{

}

SB_ERROR IOManagerGetValue(lu_uint32_t ioID, lu_int32_t *retValuePtr)
{

}

