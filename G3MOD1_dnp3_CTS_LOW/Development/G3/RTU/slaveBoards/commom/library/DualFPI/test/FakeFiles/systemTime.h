/*
 * FakesystemTime.h
 *
 *  Created on: 27 Oct 2014
 *      Author: bogias_a
 */

#ifndef FAKESYSTEMTIME_H_
#define FAKESYSTEMTIME_H_

#include "lu_types.h"
//#include "systemTime.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define ST_TIME_OVERFLOW        (0xFFFFFFFF)

#define ST_TIME_TICK_OVERFLOW   (0x00FFFFFF) // 24 bit tick counter


void TimeService_Create(void);
void TimeService_Destroy(void);
void FakesystemTime_SetMs(lu_uint32_t);

lu_uint32_t STGetTime(void);

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   Time resolution is 1ms. Maximum elapsed time ~49.7 days
 *
 *   \return Tiem in ms
 *
 ******************************************************************************
 */
inline static lu_uint32_t STElapsedTime(lu_uint32_t timeStart, lu_uint32_t timeEnd);
inline static lu_uint32_t STElapsedTime(lu_uint32_t timeStart, lu_uint32_t timeEnd)
{
    if (timeEnd > timeStart)
    {
        return (timeEnd - timeStart);
    }
    else
    {
        return ( (ST_TIME_OVERFLOW - timeStart + timeEnd) + 1);
    }
}

#endif /* FAKESYSTEMTIME_H_ */
