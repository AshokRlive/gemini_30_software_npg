/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Battery Charger module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "BatteryCharger.h"
#include "ChargerFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "LinearInterpolation.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmBatteryChargerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define ONE_HOUR_MS (1000 * 60 * 60)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR BatteryChargerSelect(BatterySelectStr *);
SB_ERROR BatteryChargerOperate(BatteryOperateStr *);
SB_ERROR BatteryChargerCancel(BatteryCancelStr *);

SB_ERROR BatteryChargerConfig(ChargerConfigStr *);

lu_uint8_t BatteryDisconnectSelectHandler(BatterySelectStr *);
lu_uint8_t BatteryDisconnectOperateHandler(BatteryOperateStr *);
lu_uint8_t BatteryDisconnectCancelHandler(BatteryCancelStr *);

lu_uint8_t BatteryTestSelectHandler(BatterySelectStr *);
lu_uint8_t BatteryTestOperateHandler(BatteryOperateStr *);
lu_uint8_t BatteryTestCancelHandler(BatteryCancelStr *);

lu_uint8_t ChargerTestSelectHandler(BatterySelectStr *);
lu_uint8_t ChargerTestOperateHandler(BatteryOperateStr *);
lu_uint8_t ChargerTestCancelHandler(BatteryCancelStr *);

lu_uint8_t ChargerInhibitSelectHandler(BatterySelectStr *);
lu_uint8_t ChargerInhibitOperateHandler(BatteryOperateStr *);
lu_uint8_t ChargerInhibitCancelHandler(BatteryCancelStr *);

lu_uint8_t BatteryResetStats(BatteryResetStr *);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

lu_bool_t 			SwEventParamsInProgress;
lu_bool_t			batteryTestInProgress;
lu_bool_t			chargerTestInProgress;
NVRAMOptPSMStr    	*NVRAMOptPSMPtr;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static NVRAMUserPSMStr *nvramAppUserBlkPtr;

lu_uint32_t batteryDisconnectSelectTime;
lu_uint32_t batteryDisconnectSelectPeriod;
lu_uint32_t batteryDisconnectOperateTime;
lu_uint32_t batteryDisconnectOperatePeriod;
lu_bool_t  	batteryDisconnectSelected;
lu_bool_t  	batteryDisconnectOperate;

lu_uint32_t batteryTestScheduleTime;
lu_uint16_t batteryTestPeriodHours;
lu_bool_t   batteryTestScheduleStart = LU_FALSE;

lu_uint32_t batteryTestSelectTime;
lu_uint32_t batteryTestSelectPeriod;
lu_uint32_t batteryTestOperateTime;
lu_uint32_t batteryTestOperatePeriod;
lu_bool_t  	batteryTestSelected;
lu_bool_t  	batteryTestOperate;

lu_uint32_t chargerTestSelectTime;
lu_uint32_t chargerTestSelectPeriod;
lu_uint32_t chargerTestOperateTime;
lu_uint32_t chargerTestOperatePeriod;
lu_bool_t  	chargerTestSelected;
lu_bool_t  	chargerTestOperate;

lu_uint32_t chargerInhibitSelectTime;
lu_uint32_t chargerInhibitSelectPeriod;
lu_uint32_t chargerInhibitOperateTime;
lu_uint32_t chargerInhibitOperatePeriod;
lu_bool_t  	chargerInhibitSelected;
lu_bool_t  	chargerInhibitOperate;

/*! List of supported message */
static const filterTableStr ChargerModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Battery Charger API commands */
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_SELECT_C     	, LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C     	, LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_RESET_C 		, LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_BATTERY_CHARGER_C 	, LU_FALSE , 0      }
};


const LnInterpTable1DU16U16Str BattTestLowTable1dU16U16[MAX_BATT_TEST_LOW_1DU16U16] =
{
		/* MAX_BATT_TEST_LOW_1DU16U16 3 Points - BATT_LOAD_TYPE_2AMP_PTC */

		/*
		 * Input TimeS value ,       Output value(mA)
		 */
		{0,2600},    // First 10 seconds 10 amps
		{10,1800}, // Over next 30 seconds settle downto 2.2 amps
		{40,1760},
		{150,1600},
		{298,600},
		{299,400}
};

const LnInterpTable1DU16U16Str BattTestPtcHighTable1dU16U16[MAX_BATT_TEST_HIGH_1DU16U16] =
{
		/* MAX_BATT_TEST_HIGH_1DU16U16 n Points - BATT_LOAD_TYPE_10AMP_PTC_R */

		/*
		 * Input TimeS value ,       Output value(mA)
		 */
		{0,7000},
		{3,6000},
		{7,5000},
		{36,1200},
		{58,800},
		{59, 400}
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BatteryChargerInit(void)
{
	SB_ERROR sbError=SB_ERROR_NONE;

	/* Read NVRAM to determine if current control charging board is fitted  */
	sbError = SB_ERROR_NONE;
	sbError = NVRamIndenityGetMemoryPointer(NVRAM_APP_BLK_OPTS, (lu_uint8_t **)&NVRAMOptPSMPtr);

	if (sbError != SB_ERROR_NONE)
	{
		NVRAMOptPSMPtr->optionBitField  |= NVRAMOPTPSMDEF_CHARGERCURRENTCONTROLFITTED_BM;
	}

	sbError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_USER_A, (lu_uint8_t **)&nvramAppUserBlkPtr);
	if (sbError != SB_ERROR_NONE)
	{
		/*
		 * Initialise configuration table with default values until config
		 * message received.
		 */
		nvramAppUserBlkPtr->batteryTestPeriodHours = 0;
		nvramAppUserBlkPtr->batteryTestDurationSecs = 0;

		/* Call function to indicate NVRAM should be updated */
		sbError = NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );
	}
	else
	{
		batteryTestPeriodHours = nvramAppUserBlkPtr->batteryTestPeriodHours;
	}

	batteryTestScheduleTime = STGetTime();

	sbError = CANFramingAddFilter( ChargerModulefilterTable,
				                SU_TABLE_SIZE(ChargerModulefilterTable, filterTableStr)
	                          );

	ChargerFSMInit();

	return sbError;
}

SB_ERROR BatteryChargerTick(void)
{
	SB_ERROR RetVal;
	lu_uint32_t elapsedTime;
	lu_uint32_t time;
	lu_int32_t fullyCharged;
	lu_int32_t motorSupplyOn;
	lu_int32_t acOff;
	lu_int32_t battDisconnect;

	/*! Get the current time */
	time = STGetTime();	/* Record the tick number */

	/* Set virtual elapsed time value */
	RetVal = IOManagerSetValue(IO_ID_VIRT_ON_ELAPSED_TIME, STGetRunningTimeSecs());

	/*! Call the FSM tick event */
	ChargerFSMTick();

	/* Check for battery test scheduled */
	if(batteryTestPeriodHours)
	{
		elapsedTime = STElapsedTime(batteryTestScheduleTime, time);

		if(elapsedTime > ONE_HOUR_MS)
		{
			/* Reset for next hour period */
			batteryTestScheduleTime = STGetTime();

			batteryTestPeriodHours--;

			/* Is it time for battery test */
			if(!batteryTestPeriodHours)
			{
				batteryTestScheduleStart = LU_TRUE;
			}
		}
	}

	/* Now start scheduled battery test when battery is fully charged and no operation in progress */
	if(batteryTestScheduleStart == LU_TRUE && batteryTestOperate == LU_FALSE)
	{
		RetVal = IOManagerGetValue(IO_ID_VIRT_BT_FULLY_CHARGED, &fullyCharged);
		RetVal = IOManagerGetValue(IO_ID_VIRT_MS_RELAY_ON,&motorSupplyOn);
		RetVal = IOManagerGetValue(IO_ID_VIRT_AC_OFF,&acOff);
		RetVal = IOManagerGetValue(IO_ID_VIRT_BT_DISCONNECTED,&battDisconnect);


		if (fullyCharged && !motorSupplyOn && !acOff && !battDisconnect)
		{
			/* Start the battery test */
			SendBatteryTestEvent();

			batteryTestScheduleStart = LU_FALSE;

			/* Reset for next battery test */
			batteryTestPeriodHours = nvramAppUserBlkPtr->batteryTestPeriodHours;
			batteryTestScheduleTime = STGetTime();
		}
	}

	/*! If battery disconnect selected then check for select time exceeded */
	elapsedTime = STElapsedTime( batteryDisconnectSelectTime , time );
	if ( (batteryDisconnectSelected == LU_TRUE) && ( elapsedTime  >   batteryDisconnectSelectPeriod ) )
	{
		/* Set select timeout error */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
							SYSALC_BATTERY_CHARGER_DISCON_SELOP_TIMEOUT,
							0
						   );

		/* Cancel battery disconnect operating flag */
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
	}

	/* If battery disconnect operating then check for time finished */
	elapsedTime = STElapsedTime( batteryDisconnectOperateTime , time );
	if ( ( batteryDisconnectOperate == LU_TRUE) && ( elapsedTime  > batteryDisconnectOperatePeriod ) )
	{
		/*
		 * Reconnect the battery
		 */
		RetVal = IOManagerSetPulse(IO_ID_BAT_CONNECT, DISCONNECT_PULSE_MS);
		batteryStatePtr->BatteryConnected 			= LU_TRUE;
		batteryStatePtr->BatteryDisConnected 		= LU_FALSE;

		/* Cancel battery disconnect operating flag */
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;

		/* Set virtual point to indicate battery is connected */
		RetVal = IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, 0);
	}

	/* If charger inhibit selected then check for select time exceeded */
	elapsedTime = STElapsedTime( chargerInhibitSelectTime , time );
	if ( (chargerTestSelected == LU_TRUE) && ( elapsedTime  >   chargerTestSelectPeriod ) )
	{
//		RetVal = IOManagerSetValue(IO_ID_CHG_INHIBIT_SELECT_OPERATE_TIMEOUT, 1);
		/* Cancel battery test operating flag */
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
	}

	/* If charger inhibit operating then check for time finished */
	elapsedTime = STElapsedTime( chargerInhibitOperateTime , time );
	if ( ( chargerInhibitOperate == LU_TRUE) && ( elapsedTime  > chargerInhibitOperatePeriod ) )
	{
		/*
		 * Cancel the inhibit from the charger
		 */
		SendChargerInhibitCancelEvent();

		/* Cancel battery disconnect operating flag */
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;

		/* Set virtual point to indicate charger is no longer inhibited */
//		RetVal = IOManagerSetValue(IO_ID_VIRT_CHG_INHIBITED, LU_FALSE);
	}

	/* If battery test selected then check for select time exceeded */
	elapsedTime = STElapsedTime( batteryTestSelectTime , time );
	if ( (batteryTestSelected == LU_TRUE) && ( elapsedTime  >   batteryTestSelectPeriod ) )
	{
		/* Set select timeout error */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
							SYSALC_BATTERY_CHARGER_TEST_SELOP_TIMEOUT,
							0
						   );

		/* Cancel battery test operating flag */
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
	}

	/* If battery test is operating check the batteryTestIn Progress flag */
	if( (batteryTestOperate ==  LU_TRUE) && ( batteryTestInProgress == LU_FALSE) )
	{
		/* Cancel battery test operating flag */
		batteryTestOperate = LU_FALSE;
		batteryTestOperateTime = 0;
		/* Cancel battery test operation selected flag */
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
	}

	/* If charger test selected then check for select time exceeded */
	elapsedTime = STElapsedTime( chargerTestSelectTime , time );
	if ( (chargerTestSelected == LU_TRUE) && ( elapsedTime  >   chargerTestSelectPeriod ) )
	{
		/* Set select timeout error */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
							SYSALC_BATTERY_CHARGER_TEST_SELOP_TIMEOUT,
							0
						   );

		/* Cancel battery test operating flag */
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
	}

	return RetVal;
}

SB_ERROR BatteryChargerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retVal;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_BAT_CH_SELECT_C:
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatterySelectStr))
			{
				#ifdef DEBUG_OUTPUT
				canpos;
				_printf("CAN message received - MODULE_MSG_ID_CMD_BAT_CH_SELECT_C\n\n\r" );
				#endif
				retVal = BatteryChargerSelect((BatterySelectStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C:
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatteryOperateStr))
			{
				#ifdef DEBUG_OUTPUT
				canpos;
				_printf("CAN message received - MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C\n\n\r" );
				#endif
				retVal = BatteryChargerOperate((BatteryOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatteryCancelStr))
			{
				#ifdef DEBUG_OUTPUT
				canpos;
				_printf("CAN message received - MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C\n\r" );
				#endif
				retVal = BatteryChargerCancel((BatteryCancelStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_BAT_CH_RESET_C:
			/*! Message sanity check */
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatteryResetStr))
			{
				#ifdef DEBUG_OUTPUT
				canpos;
				_printf("CAN message received - MODULE_MSG_ID_CMD_BAT_CH_RESET_C\n\r" );
				#endif
				retVal = BatteryResetStats((BatteryResetStr *)msgPtr->msgBufPtr);
			}
			break;


		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_BATTERY_CHARGER_C:
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ChargerConfigStr))
			{
				#ifdef DEBUG_OUTPUT
				canpos;
				_printf("CAN message received - MODULE_MSG_ID_CFG_BATTERY_CHARGER_C\n\r" );
				#endif
				retVal = BatteryChargerConfig((ChargerConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;

	default:
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BatteryChargerSelect(BatterySelectStr *BatterySelect)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= BatterySelect->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	/*
	 * Check for a switch operation in progress.
	 * Return error if a switch operation in progress
	 */
	if( SwEventParamsInProgress )
	{
		reply.status  	= REPLY_STATUS_OPERATION_IN_PROGRESS;
	}
	else
	{
		switch(BatterySelect->channel)
		{
		case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
			reply.status  	= BatteryDisconnectSelectHandler(BatterySelect);
			break;

		case PSM_CH_BCHARGER_BATTERY_TEST:
			reply.status  	= BatteryTestSelectHandler(BatterySelect);
			break;

		case PSM_CH_BCHARGER_TEST:
			reply.status  	= ChargerTestSelectHandler(BatterySelect);
			break;

		case PSM_CH_BCHARGER_INHIBIT:
			reply.status  	= ChargerInhibitSelectHandler(BatterySelect);
			break;

		default:
			reply.status  	= REPLY_STATUS_ERROR;
			break;
		}
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					   );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BatteryChargerOperate(BatteryOperateStr *BatteryOperate)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= BatteryOperate->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	/*
	 * Check for a switch operation in progress.
	 * Return error if a switch operation in progress
	 */
	if( SwEventParamsInProgress )
	{
		reply.status  	= REPLY_STATUS_OPERATION_IN_PROGRESS;
	}
	else
	{
		switch(BatteryOperate->channel)
		{
		case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
			reply.status  	= BatteryDisconnectOperateHandler(BatteryOperate);
			break;

		case PSM_CH_BCHARGER_BATTERY_TEST:
			reply.status  	= BatteryTestOperateHandler(BatteryOperate);
			break;

		case PSM_CH_BCHARGER_TEST:
			reply.status  	= ChargerTestOperateHandler(BatteryOperate);
			break;

		case PSM_CH_BCHARGER_INHIBIT:
			reply.status  	= ChargerInhibitOperateHandler(BatteryOperate);
			break;

		default:
			reply.status  	= REPLY_STATUS_ERROR;
			break;
		}
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					   );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BatteryChargerCancel(BatteryCancelStr *batteryCancel)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= batteryCancel->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(batteryCancel->channel)
	{
	case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
		reply.status  	= BatteryDisconnectCancelHandler(batteryCancel);
		break;

	case PSM_CH_BCHARGER_BATTERY_TEST:
		reply.status  	= BatteryTestCancelHandler(batteryCancel);
		break;

	case PSM_CH_BCHARGER_TEST:
		reply.status  	= ChargerTestCancelHandler(batteryCancel);
		break;
	case PSM_CH_BCHARGER_INHIBIT:
		reply.status  	= ChargerInhibitCancelHandler(batteryCancel);
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_CANCEL_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					   );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BatteryChargerConfig(ChargerConfigStr *batteryConfigPtr)
{
	ModReplyStr reply;

	if (nvramAppUserBlkPtr->batteryTestPeriodHours != batteryConfigPtr->batteryTestPeriodHours ||
		nvramAppUserBlkPtr->batteryTestDurationSecs != batteryConfigPtr->batteryTestDurationSecs
	   )
	{
		nvramAppUserBlkPtr->batteryTestPeriodHours = batteryConfigPtr->batteryTestPeriodHours;
		nvramAppUserBlkPtr->batteryTestDurationSecs = batteryConfigPtr->batteryTestDurationSecs;

		batteryTestPeriodHours = nvramAppUserBlkPtr->batteryTestPeriodHours;

		/* Call function to indicate NVRAM should be updated */
		NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );
	}

	reply.channel 	= 0;
	reply.status  	= REPLY_STATUS_OKAY;

	/* No battery charger configuration command has been defined so return okay */

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_BATTERY_CHARGER_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

lu_uint8_t BatteryDisconnectSelectHandler(BatterySelectStr * batteryDisconnectSelect)
{
	SB_ERROR retError;
	lu_int32_t local,remote;
	lu_uint8_t RetVal;

	retError = IOManagerGetValue(IO_ID_LOCAL_LINE, &local);
	retError = IOManagerGetValue(IO_ID_REMOTE_LINE, &remote);

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_DISCON_SELOP_TIMEOUT,
						0
					   );

	/* Check that an operation isn't in progress already */
	if ( batteryDisconnectOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check for valid state of local/remote switch */
	else if ( (local ^ remote) == 0 )
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	else  if ( !(((batteryDisconnectSelect->local) && local ) || !((!batteryDisconnectSelect->local) && remote)) )
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( batteryDisconnectSelected == LU_TRUE)
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		batteryDisconnectSelectPeriod = (lu_uint32_t)(batteryDisconnectSelect->SelectTimeout) * MSEC;
		batteryDisconnectSelectTime = STGetTime();

		/* Set battery operation selected flag to indicate an operation is pending */
		batteryDisconnectSelected = LU_TRUE;

		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;

}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t BatteryTestSelectHandler(BatterySelectStr * batteryTestSelect)
{
	lu_uint8_t RetVal;

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_TEST_SELOP_TIMEOUT,
						0
					   );


	/* Check that an operation isn't in progress already */
	if ( batteryTestOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( batteryTestSelected == LU_TRUE)
	{
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
		batteryTestOperate = LU_FALSE;
		batteryTestOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		batteryTestSelectPeriod = (lu_uint32_t)(batteryTestSelect->SelectTimeout) * MSEC;
		batteryTestSelectTime = STGetTime();

		/* Set battery operation selected flag to indicate an operation is pending */
		batteryTestSelected = LU_TRUE;

		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t ChargerTestSelectHandler(BatterySelectStr * chargerTestSelect)
{
	lu_uint8_t RetVal;

//	retError = IOManagerSetValue(IO_ID_CHG_TEST_SELECT_OPERATE_TIMEOUT, 0);

	/* Check that an operation isn't in progress already */
	if ( chargerTestOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( chargerTestSelected == LU_TRUE)
	{
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
		chargerTestOperate = LU_FALSE;
		chargerTestOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		chargerTestSelectPeriod = (lu_uint32_t)(chargerTestSelect->SelectTimeout) * MSEC;
		chargerTestSelectTime = STGetTime();

		/* Set battery operation selected flag to indicate an operation is pending */
		chargerTestSelected = LU_TRUE;

		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t ChargerInhibitSelectHandler(BatterySelectStr * chargerInhibitSelect)
{
	lu_uint8_t RetVal;

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_INHIB_SELOP_TIMEOUT,
						0
					   );


	/* Check that an operation isn't in progress already */
	if ( chargerInhibitOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( chargerInhibitSelected == LU_TRUE)
	{
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		chargerInhibitSelectPeriod = (lu_uint32_t)(chargerInhibitSelect->SelectTimeout) * MSEC;
		chargerInhibitSelectTime = STGetTime();

		/* Set battery operation selected flag to indicate an operation is pending */
		chargerInhibitSelected = LU_TRUE;

		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t BatteryDisconnectOperateHandler(BatteryOperateStr * batteryDisconnectOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	lu_int32_t local,remote;
	SB_ERROR retError;
	time = STGetTime();

	/* Check that battery disconnect not operating already */
	if( batteryDisconnectOperate == LU_TRUE )
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that battery disconnect has been selected */
	else if ( batteryDisconnectSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( batteryDisconnectSelectTime , time );
		if ( elapsedTime >   batteryDisconnectSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			batteryDisconnectSelected = LU_FALSE;
			batteryDisconnectSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_DISCON_SELOP_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Get state of local/remote switch */
			retError = IOManagerGetValue(IO_ID_LOCAL_LINE, &local);
			retError = IOManagerGetValue(IO_ID_REMOTE_LINE, &remote);

			/* Check for valid state of local/remote switch */
			if ( (local ^ remote) == 0 )
			{
				batteryDisconnectSelected = LU_FALSE;
				batteryDisconnectSelectTime = 0;
				batteryDisconnectOperate = LU_FALSE;
				batteryDisconnectOperateTime = 0;
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
			/* Check if local/remote switch is in the correct state */
			else if ( ((batteryDisconnectOperatePtr->local) && local ) || !((!batteryDisconnectOperatePtr->local) && remote) )
			{
				/* Start Battery Disconnection */
				if ( batteryDisconnectOperatePtr->operationDuration > 0 )
				{
					/* Save time of disconnection */
					batteryDisconnectOperatePeriod = (lu_uint32_t)(batteryDisconnectOperatePtr->operationDuration) * MSEC;
					batteryDisconnectOperateTime = time;
					/* Set operate flag */
					batteryDisconnectOperate = LU_TRUE;
					batteryDisconnectSelected = LU_FALSE;
				}

				/*
				 * Battery Disconnection Operation
				 */
				RetVal = IOManagerSetPulse(IO_ID_BAT_DISCONNECT, DISCONNECT_PULSE_MS);
				batteryStatePtr->BatteryConnected 	 = LU_FALSE;		/* Indicates if battery is connected	*/
				batteryStatePtr->BatteryDisConnected = LU_TRUE;			/* Indicates if battery is disconnected	*/

				/* Set virtual point to indicate battery is disconnected */
				retError = IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, 1);
				retError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);

				RetVal = REPLY_STATUS_OKAY;
			}
			else /* local/remote switch is not in the correct state so return an error */
			{
				batteryDisconnectSelected = LU_FALSE;
				batteryDisconnectSelectTime = 0;
				batteryDisconnectOperate = LU_FALSE;
				batteryDisconnectOperateTime = 0;
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
		}
	}
	else  /* Battery has not been selected so return an error */
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t BatteryTestOperateHandler(BatteryOperateStr * batteryTestOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	time = STGetTime();

	/* Check that battery test not operating already */
	if( batteryTestOperate == LU_TRUE )
	{
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that battery test has been selected */
	else if ( batteryTestSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( batteryTestSelectTime , time );
		if ( elapsedTime >   batteryTestSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			batteryTestSelected = LU_FALSE;
			batteryTestSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_TEST_SELOP_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Start Battery Test */
			if ( batteryTestOperatePtr->operationDuration > 0 )
			{
				/*
				 * Check that battery test period does not exceed the maximum value
				 * If it does then limit the period to the maximum allowed
				 */
				if( batteryTestOperatePtr->operationDuration > batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes)
				{
					batteryTestOperatePtr->operationDuration = batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes;
				}

				/*
				 * Save time for start of test
				 * Operation time is in minutes so calculate number of msec
				 * for battery test duration.
				 */
				batteryTestOperatePeriod = (lu_uint32_t)(batteryTestOperatePtr->operationDuration) * MSEC * SEC;

				testStatePtr->BatteryTestDuration = batteryTestOperatePeriod;
				batteryTestOperateTime = time;
				/* Set operate flag */
				batteryTestOperate = LU_TRUE;
				batteryTestSelected = LU_FALSE;

			}

			SendBatteryTestEvent();

			RetVal = REPLY_STATUS_OKAY;
		}
	}
	else  /* Battery has not been selected so return an error */
	{
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
		batteryTestOperate = LU_FALSE;
		batteryTestOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t ChargerTestOperateHandler(BatteryOperateStr * chargerTestOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	time = STGetTime();

	/* Check that battery test not operating already */
	if( chargerTestOperate == LU_TRUE )
	{
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that battery test has been selected */
	else if ( chargerTestSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( chargerTestSelectTime , time );
		if ( elapsedTime >   chargerTestSelectPeriod )
		{
			/* select time has been exceeded so cancel operation. */
			chargerTestSelected = LU_FALSE;
			chargerTestSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_TEST_OPERATE_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Start charger Test */
			if ( chargerTestOperatePtr->operationDuration > 0 )
			{
				/* Save time for start of test */
				chargerTestOperatePeriod = (lu_uint32_t)(chargerTestOperatePtr->operationDuration) * MSEC;
				chargerTestOperateTime = time;
				/* Set operate flag */
				chargerTestOperate = LU_TRUE;
				chargerTestSelected = LU_FALSE;
			}

			/*
			 * Initiate Charger Test Operation to do the following:
			 * Leave the charger on but disconnect the charger from the battery.
			 * Measure the battery voltage.
			 * Set the virtual channel if the voltage is too low.
			 */
			chargerTestInProgress = LU_TRUE;
			SendChargerTestEvent();

			/*
			 * As there will be no cancel command needed cancel the battery test operating flags
			 * immediately
			 */
			chargerTestOperate = LU_FALSE;
			chargerTestOperateTime = 0;
			/* Cancel charger test operation selected flag */
			chargerTestSelected = LU_FALSE;
			chargerTestSelectTime = 0;

			RetVal = REPLY_STATUS_OKAY;
		}
	}
	else  /* charger has not been selected so return an error */
	{
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
		chargerTestOperate = LU_FALSE;
		chargerTestOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t ChargerInhibitOperateHandler(BatteryOperateStr * chargerInhibitOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	lu_int32_t local,remote;
	SB_ERROR retError;
	time = STGetTime();

	/* Check that ChargerInhibit not operating already */
	if( chargerInhibitOperate == LU_TRUE )
	{
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that charger inhibit has been selected */
	else if ( chargerInhibitSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( chargerInhibitSelectTime , time );
		if ( elapsedTime >   chargerInhibitSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			chargerInhibitSelected = LU_FALSE;
			chargerInhibitSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_INHIB_SELOP_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{			/* select time has not been exceeded so operation allowed */
			/* Get state of local/remote switch */
			retError = IOManagerGetValue(IO_ID_LOCAL_LINE, &local);
			retError = IOManagerGetValue(IO_ID_REMOTE_LINE, &remote);

				/* Start charger inhibit */
				if ( chargerInhibitOperatePtr->operationDuration > 0 )
				{
					/* Save time of disconnection */
					chargerInhibitOperatePeriod = (lu_uint32_t)(chargerInhibitOperatePtr->operationDuration) * MSEC;
					chargerInhibitOperateTime = time;
					/* Set operate flag */
					chargerInhibitOperate = LU_TRUE;
					chargerInhibitSelected = LU_FALSE;
				}

				/*
				 * Send Inhibit event to the charging state machine
				 */
				SendChargerInhibitEvent();

				/* Set virtual point to indicate charger is inhibited */
				retError = IOManagerSetValue(IO_ID_VIRT_BT_CH_INHIBITED, LU_TRUE);
				RetVal = REPLY_STATUS_OKAY;
		}
	}
	else  /* Has not been selected so return an error */
	{
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t BatteryDisconnectCancelHandler(BatteryCancelStr * batteryDisconnectCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(batteryDisconnectCancel);

	/* Cancel operation */
	if ( (batteryDisconnectOperate == LU_TRUE) || (batteryDisconnectSelected == LU_TRUE) )
	{
		/*
		 * Reconnect the battery
		 */
		RetVal = IOManagerSetPulse(IO_ID_BAT_CONNECT, DISCONNECT_PULSE_MS);
		batteryStatePtr->BatteryConnected 			= LU_TRUE;
		batteryStatePtr->BatteryDisConnected 		= LU_FALSE;

		/* Set virtual point to indicate battery is connected */
		RetVal = IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, 0);

		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		/* Cancel battery disconnect operating flag */
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		/* Cancel battery disconnect operation selected flag */
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel battery disconnect operating flag */
	batteryDisconnectOperate = LU_FALSE;
	batteryDisconnectOperateTime = 0;
	/* Cancel battery disconnect operation selected flag */
	batteryDisconnectSelected = LU_FALSE;
	batteryDisconnectSelectTime = 0;

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t BatteryTestCancelHandler(BatteryCancelStr * batteryTestCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(batteryTestCancel);

	/* Cancel operation */
	if ( (batteryTestOperate == LU_TRUE) || (batteryTestSelected == LU_TRUE) )
	{
		/* Set flag in battery test structure to indicate a cancel has been received */
		testStatePtr->BatteryTestCancelled = LU_TRUE;
		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel battery test operating flag */
	batteryTestOperate = LU_FALSE;
	batteryTestOperateTime = 0;
	/* Cancel battery test operation selected flag */
	batteryTestSelected = LU_FALSE;
	batteryTestSelectTime = 0;
	/* Cancel battery test global flag */
	batteryTestInProgress = LU_FALSE;
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t ChargerTestCancelHandler(BatteryCancelStr * chargerTestCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(chargerTestCancel);

	/* Cancel operation */
	if ( (chargerTestOperate == LU_TRUE) || (chargerTestSelected == LU_TRUE) )
	{
		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		/* Cancel charger test operating flag */
		batteryTestOperate = LU_FALSE;
		batteryTestOperateTime = 0;
		/* Cancel charger test operation selected flag */
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel charger test operating flag */
	chargerTestOperate = LU_FALSE;
	chargerTestOperateTime = 0;
	/* Cancel charger test operation selected flag */
	chargerTestSelected = LU_FALSE;
	chargerTestSelectTime = 0;
	/* Cancel charger test global flag */
	chargerTestInProgress = LU_TRUE;
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t ChargerInhibitCancelHandler(BatteryCancelStr * chargerInhibitCancel)
{
	lu_uint8_t RetVal;
	SB_ERROR retError;
	LU_UNUSED(chargerInhibitCancel);

	/* Cancel operation */
	if ( (chargerInhibitOperate == LU_TRUE) || (chargerInhibitSelected == LU_TRUE) )
	{
		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		/* Cancel charger Inhibit operating flag */
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		/* Cancel charger test operation selected flag */
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/*
	 * Send cancel inhibit event to the battery charging state machine
	 */
	SendChargerInhibitCancelEvent();

	/* Set virtual point to indicate charger is inhibited */
	retError = IOManagerSetValue(IO_ID_VIRT_BT_CH_INHIBITED, LU_FALSE);

	/* Cancel inhibit operating flag */
	chargerInhibitOperate = LU_FALSE;
	chargerInhibitOperateTime = 0;
	/* Cancel charger test operation selected flag */
	chargerInhibitSelected = LU_FALSE;
	chargerInhibitSelectTime = 0;
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t  BatteryResetStats(BatteryResetStr *batteryresetStr)
{
	lu_uint8_t RetVal = REPLY_STATUS_OKAY;
	ModReplyStr reply;
	LU_UNUSED(batteryresetStr);

	/*
	 * Initiate reseting the battery statistics held in NVRAM
	 */
	if( SendBatteryResetStatsEvent() )
	{
		reply.status  	= REPLY_STATUS_OKAY;
	}
	else
	{
		reply.status  	= REPLY_STATUS_ERROR;
	}
	reply.channel 	= 0;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_RESET_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return RetVal;
}

/*
 *********************** End of file ******************************************
 */
