/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BATTERYCHARGER_INCLUDED
#define _BATTERYCHARGER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"
#include "NVRam.h"
#include "LinearInterpolation.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DISCONNECT_PULSE_MS					10

#define MAX_BATT_VOLTAGE_1DU16U16		    (2)

#define MAX_BATT_CURRENT_LIMIT_1DU16U16		(2)

#define BATT_CH_TICK_MS						1000 	/* Schedule every n ms */

#define BATTERY_PRESENT						5000 	/* Level in mV at which battery is assumed disconnected */

#define LV_SUPPLY_THRESHOLD_VOLTAGE			15000	/* Level in mV at which LV supply is off */

#define MSEC								1000

#define SEC									60

#define BATT_DEFAULT_TEMP					28

#define PSU_DEFAULT_TEMP					28

#define RTU_DEFAULT_TEMP					28

#define BATT_MAX_VALID_TEMP					65
#define BATT_MIN_VALID_TEMP					18

#define LEAD_ACID_BATT_TEMP_HYSTERISIS		2
#define NIMH_BATT_TEMP_HYSTERISIS			5
#define UKNOWN_BATT_TEMP_HYSTERISIS			2

#define CHARGER_INIT_DELAY					2000


#define MAX_BATT_TEST_LOW_1DU16U16  6

#define MAX_BATT_TEST_HIGH_1DU16U16  6

#ifdef NEW_STUFF
#define MAX_BATT_TEST_HIGH_1DU16U16  5
#endif

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Battery chemistry */
typedef enum
{
	BATT_TYPE_UNKNOWN = 0,
	BATT_TYPE_NI_MH,
	BATT_TYPE_LEAD_ACID
}BATT_TYPE;

typedef enum
{
	NORMAL = 0,
	CONDITIONING
}BATT_CHARGE_STATE;


typedef enum
{
	BATT_CHARGE_METHOD_SENSOR = 0,
	BATT_CHARGE_METHOD_DEFAULT,
	BATT_CHARGE_METHOD_FLOAT
}BATT_CHARGE_METHOD;

typedef enum
{
	BATT_HEALTHY = 0,
	BATT_WARNING,
	BATT_UNHEALTHY
}BATT_CONDITION;

/* Reason for charging stopped */
typedef enum
{
	BATT_CHARGE_STOP_INVALID = 0,
	BATT_CHARGE_STOP_MAX_TIME,
	BATT_CHARGE_STOP_MIN_CURRENT,
	BATT_CHARGE_STOP_UNSTABLE_CURRENT,
	BATT_CHARGE_STOP_DELTA_TEMP,
	BATT_CHARGE_STOP_DELTA_VOLTS,
	BATT_CHARGE_STOP_MAX_CAPACITY,
	BATT_CHARGE_STOP_TOP_UP_TIME,
	BATT_CHARGE_STOP_TEST_RUNNING
}BATT_CHARGE_STOP;

/* Which charger test is in progress */
typedef enum
{
	CH_TEST_INITIAL = 0,

			CH_TEST_INITIAL0,
			CH_TEST_INITIAL1,
			CH_TEST_INITIAL2,
			CH_TEST_INITIAL3,
			CH_TEST_INITIAL4,
			CH_TEST_INITIAL5,
	CH_TEST_CHARGER_FAIL,

	CH_TEST_NONE,
	CH_TEST_VOLTAGE,
	CH_TEST_CURRENT
}CH_TEST;

/* Reason for battery test stopped */
typedef enum
{
	BATT_TEST_TEST_RUNNING = 0,
	BATT_TEST_STOP_PASSED,
	BATT_TEST_STOP_UNDER_THRESHOLD,
	BATT_TEST_STOP_OPERATION,
	BATT_TEST_STOP_LV_LOST,
	BATT_TEST_STOP_BATT_DISCONNECTED,
	BATT_TEST_STOP_TIME_FINISHED,
	BATT_TEST_STOP_MAX_DISCHARGE,
	BATT_TEST_STOP_MAX_DURATION,
	BATT_TEST_STOP_CANCELED,
	BATT_TEST_STOP_LOW_CURRENT,
	BATT_TEST_STOP_OVERLOAD_CURRENT
}BATT_TEST_STOP;

/* Battery Parameters - from battery NVRAM */
typedef struct BatteryParamsDef
{
	lu_int32_t 	BatteryChargeTrigger;		/* Normal battery charging threshold 						*/
	lu_int32_t 	BatteryThresholdVolts;		/* Lower battery charging threshold for immediate charging	*/
	lu_int32_t 	BatteryChargedVolts;		/* Voltage at which battery can be considered fully charged */
	lu_int32_t 	BatteryMaxTemp;				/* Maximum temperature at which normal charging is allowed	*/
	lu_uint32_t BatteryOverideTime;			/* Maximum time for which battery charging can be delayed 	*/
	lu_int32_t 	BatteryMaxCharge;			/* Maximum capacity of the battery							*/
	lu_int32_t 	BatteryMaxChargeTime;		/* Maximum charge time of the battery						*/
	lu_uint32_t BatteryTimeBetweenCharge;	/* Minimum  time between charges of the battery			*/
	lu_int32_t 	BatteryChargeRate;			/* Charging current specified for the battery				*/
	lu_int32_t 	BatteryChargeVolts;			/* Charging voltage specified for the battery				*/
	lu_int32_t 	BatteryFullChargeVolts;		/* Charging fully charged voltage specified for the battery	*/
	BATT_TYPE  	BatteryType;				/* Indicates type of battery in use							*/
	lu_int32_t 	BatteryLowLevel;			/* Level below operations cannot be performed				*/
	lu_int32_t 	BatteryLeakage;				/* Leakage current of battery 								*/
	lu_int32_t 	BatteryFloatChargeRate;		/* Charging current when float charging						*/

    lu_uint16_t BatteryTestTimeMinutes;                   /* Battery Test Time Minutes 					*/
    lu_uint16_t BatteryTestCurrentMinimum;                /* Battery Test Current Minimum (mA) 			*/
    lu_uint16_t BatteryTestSuspendTimeMinutes;            /* Battery Test Suspend Time Minutes 			*/
    lu_uint16_t BatteryTestMaximumDurationTimeMinutes;    /* Battery Test Maximum Duration Time Minutes */
    lu_uint8_t 	BatteryTestDischargeCapacity;             /* Battery Test Discharge Capacity 			*/
    lu_int32_t  BatteryTestThresholdVolts;                /* Battery Test Threshold Volts 				*/
    lu_uint8_t 	BatteryTestLoadType;                       /* Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
    lu_uint16_t BatteryPackShutdownLevelVolts;            /* Battery Pack Shutdown Level Volts */
    lu_uint16_t BatteryPackDeepDischargeVolts;            /* Battery Pack Deep Discharge Protection Level Volts */
} BatteryParamsStr;

/* Battery State */
typedef struct BatteryStateDef
{
	lu_int32_t BatteryVoltage;				/* Current battery voltage level 							*/
	lu_int32_t BatteryVoltageAverage;		/* Averaged battery voltage level 							*/
	lu_int32_t BatteryChargingVolts;		/* Current voltage output of charger						*/
	lu_int32_t BatteryTemp;					/* Current temperature of the battery						*/
	lu_int32_t BatteryTempAverage;			/* Averaged battery temperature								*/
	lu_int32_t RTUTempAverage;				/* Averaged rtu temperature									*/
	lu_int32_t PSMTemp;						/* Current temperature of the Power Supply Module			*/
	lu_int32_t RTUTemp;						/* Current temperature of the RTU							*/
	lu_int32_t BatteryCharge;				/* Current charge applied to the battery					*/
	lu_int32_t BatteryChargeRate;			/* Charging current specified for the battery				*/
	lu_uint32_t BatteryChargeTime;			/* Charging time specified for the battery					*/
	lu_int32_t BatteryChargeVolts;			/* Charging voltage specified for the battery				*/
	BATT_TYPE  BatteryType;					/* Indicates type of battery in use							*/
	lu_bool_t  BatteryOverride;				/* Indicate if charging override is in operation			*/
	lu_bool_t  BatteryConnected;
	lu_bool_t  BatteryDisConnected;
	lu_int32_t BatteryChgCycleTotalChargeCurrent;	/* Cycle total of battery charge i.e. current * time		*/
	lu_int32_t BatteryTotalChargeCurrent;	/* Running total of battery charge i.e. current * time		*/
	lu_int32_t BatteryTotalDrainCurrent;	/* Running total of battery drain i.e. current * time		*/
	lu_int32_t BatteryTotalCharge;			/* Total battery charge i.e. state of battery				*/
	lu_bool_t  BatteryTempValid;			/* Indicates battery temperature sensor is fitted & working	*/
	lu_int32_t BatteryLowLevel;				/* Level below operations cannot be performed				*/
	lu_int32_t BatteryChargerVoltage;		/* Output voltage of charger								*/
	lu_int32_t BatteryCorrectedChargingVolts;
	lu_int32_t BatteryDeltaTemp;
	lu_int32_t RtuDeltaTemp;
	lu_int32_t BatteryDeltaVolts;
	lu_int32_t BatteryChargerVoltLimitOutput;
	lu_int32_t MeasuredChargingVolts;
	lu_int32_t BatteryAverageVoltage;
	lu_bool_t  RtuTempValid;
	lu_uint32_t BatteryTopUpTime;			/* Top up time											*/

} BatteryStateStr;

/* Charger State */
typedef struct ChargerStateDef
{
	lu_bool_t  ChargerEnabled;			/* Indicates is charger is enabled							*/
	lu_uint8_t ChargerState;			/* State of state machine									*/
	lu_bool_t  ChargerCurrentUnStable;	/* Indicate if charging current not changing 				*/
	lu_bool_t  ChargerCurrentMinimum;	/* Indicate if charging current below minimum				*/
	lu_uint8_t ChargerEndReason;		/* Indicate why charging cycle ended						*/
	lu_uint8_t ChargerSuspendOccurred;	/* Flag to indicate a suspend has occurred					*/
	lu_uint32_t ChargerSuspendStart;		/* Time entered suspend state								*/
	lu_uint32_t ChargerSuspendTime;		/* Time spent in suspend state								*/
} ChargerStateStr;

/* Battery Test State */
typedef struct BatteryTestStateDef
{
	lu_int32_t 	BatteryVoltage;				/* Current battery voltage level 							*/
	lu_int32_t 	BatteryStartVoltage;		/* Starting battery voltage level 							*/
	lu_int32_t 	BatteryEndVoltage;			/* Finishing battery voltage level 							*/
	lu_int32_t 	BatteryDischargeRate;		/* Discharge current via dummy load           				*/
	lu_uint32_t BatteryTestTime;			/* Time duration of current battery test					*/
	lu_int32_t 	BatteryTotalDrainCurrent;	/* Running total of battery drain i.e. current * time		*/
	lu_int32_t 	BatteryTestThreshold;		/* Voltage at which test fails								*/
	lu_uint32_t BatteryTestStartTime;		/* Time at which battery test was started					*/
	lu_uint32_t BatteryTestDuration;		/* Battery test time specified								*/
	lu_uint8_t 	BatteryTestEndReason;		/* Indicate why battery test ended							*/
	lu_bool_t  	BatteryTestInProgress;		/* Indicates if battery test is in progress					*/
	lu_bool_t  	BatteryTestCancelled;		/* Battery test cancelled flag								*/
	lu_bool_t	BatteryTestSuspended;		/* Battery test suspended flag								*/
	lu_uint32_t BatteryTestSuspendedTime;	/* Battery test suspension  start time						*/
} BatteryTestStateStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern BatteryParamsStr  	batteryParams, *batteryParamsPtr;
extern BatteryStateStr	  	batteryState , *batteryStatePtr;
extern ChargerStateStr	  	chargerState , *chargerStatePtr;
extern BatteryTestStateStr  testState    , *testStatePtr;
extern lu_bool_t 			batteryTestInProgress;
extern lu_bool_t 			chargerTestInProgress;
extern NVRAMOptPSMStr    	*NVRAMOptPSMPtr;

extern const LnInterpTable1DU16U16Str battVoltageTable1dU16U16[MAX_BATT_VOLTAGE_1DU16U16];
extern const LnInterpTable1DU16U16Str battCurrentLimitTable1dU16U16[MAX_BATT_CURRENT_LIMIT_1DU16U16];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR BatteryChargerInit(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR BatteryChargerTick(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR BatteryChargerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

#endif /* _BATTERYCHARGER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
