/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Charger FSM module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

/******************************
 * Testing and debug only     *
 ******************************/
//#define CHARGER_TESTING				/* Force the battery type to be NiMH */

/*****************************/

#include "systemStatus.h"

#include "ChargerFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "BatteryCharger.h"
#include "systemTime.h"
#include "LinearInterpolation.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmBatteryChargerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

lu_int32_t GetTempCompVolts(lu_int32_t volts);
SB_ERROR SuspendChargingBattery(void);
SB_ERROR StopChargingBattery(void);
SB_ERROR StartChargingBattery(void);
SB_ERROR GetTemperature(void);
SB_ERROR ChargerInit(void);
lu_bool_t ChargerVoltsCheck(void);
lu_bool_t BatteryVoltsCheck(void);
lu_bool_t BatteryFullChargeCheck(void);
lu_bool_t ChargerTest(void);
lu_bool_t CheckTemperatureChange(void);
lu_bool_t CheckVoltageChange(void);
lu_uint16_t CalculateBatteryCapacity(void);
void ConnectChgToBatt(void);
void DisconnectChgFromBatt(void);
void SetBatteryDafaultParams(void);
void GetChargerVoltsReq(void);
void RecordChargeTime(void);
void RecordChargeCycle(void);
void ClearDebugDisplay(void);
void batteryType(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

NVRAMInfoStr      *batteryIdNVRamBlkPtr;
NVRAMDefOptBatStr *batteryOpts;

BatteryDataNVRAMBlkStr	*batteryDataNVRamBlkPtr;
BatteryParamsStr  	batteryParams, *batteryParamsPtr;
BatteryStateStr	  	batteryState , *batteryStatePtr;
ChargerStateStr	  	chargerState , *chargerStatePtr;
BatteryTestStateStr testState    , *testStatePtr;


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint32_t 	ChargeStartTime;
static lu_uint32_t 	ChargeStopTime;
static lu_uint32_t 	LVFailTime;
static lu_int32_t 	ChargeCurrent;
static lu_int32_t 	DrainCurrent;
static lu_int32_t 	ChargeTime;
static lu_uint8_t 	initialPass;
static lu_bool_t 	chargerInitialised;
static lu_uint32_t	chargerInitTime;
static lu_bool_t	updateDataNVRAM;
static lu_uint16_t 	battCapacity;
static lu_uint32_t 	ShutdownStartTime;
static lu_uint32_t 	disconnectStartTime;
static lu_int32_t   chargerVoltLimitOutput;
static lu_uint8_t 	chargerMode;
static lu_float32_t	chargerVoltsCal;
static lu_bool_t	LVFailedFlag;
static lu_int32_t	chargerCurrentLimit;
static lu_uint32_t  timeoutFlag = 0;
static lu_bool_t 	battTestFirstTime = LU_TRUE;
static lu_uint32_t 	loadStartTime;
static lu_uint8_t	battState;
static struct Charger chargerFsm;
static lu_bool_t	currentControlEnabled = LU_FALSE;
static lu_uint16_t	chargerBoardRevisionMajor;
static lu_uint16_t	chargerBoardRevisionMinor;
static lu_int32_t 	TopUpTime;
static lu_int32_t 	TopUpStartTime;
static lu_bool_t	topUpState = LU_FALSE;
static lu_int32_t 	balancingTime;

static const LnInterpTable1DU16U16Str BatteryCapacityPbTable1DU16U16[BATT_CAPACITY_MAX] = {
		/*
		 * Battery Voltage	   Battery capacity expressed in %
		 * Input value ,       Output value
		 */
		{7500,                     0},
		{10000,                   20},
		{11000,                   40},
		{12000,                   60},
		{12500,                   70},
		{13000,                   80},
		{13500,                   90},
		{13800,                  100}
};

static const LnInterpTable1DU16U16Str BatteryCapacityNiMHTable1DU16U16[BATT_CAPACITY_MAX] = {
		/*
		 * Battery Voltage	   Battery capacity expressed in %
		 * Input value ,       Output value
		 */
		{1000,                    0},
		{1100,                   10},
		{1180,                   25},
		{1190,                   50},
		{1200,                   75},
		{1220,                   90},
		{1230,                   95},
		{1240,                  100}
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void ChargerFSMInit(void)
{
	lu_uint32_t time;
	SB_ERROR 	sbError;

	batteryParamsPtr 	= &batteryParams ;
	batteryStatePtr 	= &batteryState ;
	chargerStatePtr 	= &chargerState ;
	testStatePtr 		= &testState ;

	battState			= BATT_HEALTHY;

	updateDataNVRAM		= LU_FALSE;
	chargerVoltsCal		= 1.0;

	/*
	 * Get battery type and charging parameters from non-volatile memory
	 * to initialise charging regime.
	 * If pointer not valid then fill NVRAM structure with default values.
	 */
	sbError = NVRamBatteryIndenityGetMemoryPointer(NVRAM_BATT_ID_BLK_OPTS, (lu_uint8_t**)&batteryOpts);
	if (sbError != SB_ERROR_NONE)
	{
		sbError = NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK_OPTS, (lu_uint8_t**)&batteryOpts);

		if (sbError != SB_ERROR_NONE)
		{
			/* Set virtual point to indicate NVRAM failure */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_ID_NVRAM_FAIL, LU_TRUE);

			/* Major error - no factory info is available */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_TEMP_SENSOR_NVRAM,
								0
							   );

			/* Load default parameters since NVRAM is not detected */
			SetBatteryDafaultParams();
		}
		else
		{
			/* Reset virtual point to indicate NVRAM access is okay */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_ID_NVRAM_FAIL, LU_FALSE);
		}
	}
	else
	{
		/* Reset virtual point to indicate NVRAM access is okay */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_ID_NVRAM_FAIL, LU_FALSE);
	}

	sbError = NVRamBatteryIndenityGetMemoryPointer(NVRAM_BATT_ID_BLK_INFO, (lu_uint8_t**)&batteryIdNVRamBlkPtr);

	if (sbError != SB_ERROR_NONE)
	{
		sbError = NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK_INFO, (lu_uint8_t    **)&batteryIdNVRamBlkPtr);
	}

	/*
	 * Fill local battery parameter structure from values in NVRAM ID Structure (or default values if no NVRAM found)
	 */
	batteryParamsPtr->BatteryType 				=	  (BATT_TYPE)batteryOpts->BatteryChemistry;		/* Indicates type of battery in use	e.g. Lead Acid, NiMH	*/

	batteryParamsPtr->BatteryChargedVolts 		= 	  ( batteryOpts->BatteryCellNominalVolts *
													    batteryOpts->BatteryPackNoOfRows );

	batteryParamsPtr->BatteryFullChargeVolts	= 	  batteryOpts->BatteryCellFullChargeVolts
													* batteryOpts->BatteryPackNoOfRows;

	batteryParamsPtr->BatteryOverideTime		=     batteryOpts->BatteryPackOverideTime;			/* Maximum time for which battery charging can be delayed 	*/

	batteryParamsPtr->BatteryMaxCharge 			= 	  batteryOpts->BatteryCellMaxCapacity			/* Maximum capacity of each cell							*/
													* batteryOpts->BatteryPackNoOfCols
													* batteryOpts->BatteryPackNoOfRows;

	batteryParamsPtr->BatteryMaxTemp			=	  batteryOpts->BatteryPackMaxChargeTemp;		/* Maximum temperature at which normal charging is allowed	*/

	batteryParamsPtr->BatteryMaxChargeTime		= 	  batteryOpts->BatteryPackMaxChargeTime;		/* Maximum charge time of the battery						*/

	batteryParamsPtr->BatteryChargeRate			=	  batteryOpts->BatteryCellChargingCurrent		/* Charging current specified for each cell 				*/
													* batteryOpts->BatteryPackNoOfCols;

	batteryParamsPtr->BatteryChargeVolts		=	  batteryOpts->BatteryCellChargingVolts
													* batteryOpts->BatteryPackNoOfRows;

	batteryParamsPtr->BatteryChargeTrigger 		= 	  batteryOpts->BatteryPackChargeTrigger;		/* Normal battery charging threshold 						*/

	batteryParamsPtr->BatteryThresholdVolts 	=     batteryOpts->BatteryPackThresholdVolts;		/* Lower battery charging threshold for immediate charging	*/

	batteryParamsPtr->BatteryTimeBetweenCharge	=	  batteryOpts->BatteryPackTimeBetweenCharge;	/* Minimum  time between charges of the battery				*/

	batteryParamsPtr->BatteryLowLevel			= 	  batteryOpts->BatteryPackLowLevel;				/* Level below which an operation cannot be performed		*/

	batteryParamsPtr->BatteryLeakage			=	  batteryOpts->BatteryCellLeakage				/* Leakage current of battery 								*/
													* batteryOpts->BatteryPackNoOfCols;

	batteryParamsPtr->BatteryFloatChargeRate	=	  batteryOpts->BatteryCellFloatCharge
													* batteryOpts->BatteryPackNoOfCols;


	batteryParamsPtr->BatteryTestTimeMinutes				= batteryOpts->BatteryTestTimeMinutes;    				/* Battery Test Time Minutes 					*/

	batteryParamsPtr->BatteryTestCurrentMinimum				= batteryOpts->BatteryTestCurrentMinimum;    			/* Battery Test Current Minimum (mA) 			*/
	batteryParamsPtr->BatteryTestSuspendTimeMinutes			= batteryOpts->BatteryTestSuspendTimeMinutes;    		/* Battery Test Suspend Time Minutes 			*/
	batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes	= batteryOpts->BatteryTestMaximumDurationTimeMinutes; 	/* Battery Test Maximum Duration Time Minutes  	*/
	batteryParamsPtr->BatteryTestDischargeCapacity			= batteryOpts->BatteryTestDischargeCapacity;    		/* Battery Test Discharge Capacity 				*/
	batteryParamsPtr->BatteryTestThresholdVolts				= batteryOpts->BatteryTestThresholdVolts;    			/* Battery Test Threshold Volts 				*/

	batteryParamsPtr->BatteryTestThresholdVolts				= batteryOpts->BatteryTestThresholdVolts;                /* Battery Test Threshold Volts 				*/
	batteryParamsPtr->BatteryTestLoadType					= batteryOpts->BatteryTestLoadType;                      /* Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
	batteryParamsPtr->BatteryPackShutdownLevelVolts			= batteryOpts->BatteryPackShutdownLevelVolts;            /* Battery Pack Shutdown Level Volts */
	batteryParamsPtr->BatteryPackDeepDischargeVolts			= batteryOpts->BatteryPackDeepDischargeVolts;            /* Battery Pack Deep Discharge Protection Level Volts */


	if (NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK_USER_A, (lu_uint8_t    **)&batteryDataNVRamBlkPtr) != SB_ERROR_NONE)
	{
		/* As a valid pointer was not returned the NVRAM data block must be initialised */
		memset( batteryDataNVRamBlkPtr,  0 , sizeof(BatteryDataNVRAMBlkStr) );		/* Fill the data block with zeroes							*/
		updateDataNVRAM					 		= LU_TRUE;							/* Set flag to indicate the NVRAM should be updated			*/
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_DATA_NVRAM_FAIL, 1);
	}
	else
	{
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_DATA_NVRAM_FAIL, 0);
	}

	/*
	 * Perform sanity checks on battery parameters
	 */
	if(	(batteryParamsPtr->BatteryType != BATT_TYPE_UNKNOWN   )	&&
		(batteryParamsPtr->BatteryType != BATT_TYPE_NI_MH     )	&&
		(batteryParamsPtr->BatteryType != BATT_TYPE_LEAD_ACID )
	  )
	{
		/* Battery type is invalid so use default values */
		SetBatteryDafaultParams();
	}

	if(
		(batteryParamsPtr->BatteryChargedVolts    > 36000 ) ||
		(batteryParamsPtr->BatteryFullChargeVolts > 36000 ) ||
		(batteryParamsPtr->BatteryMaxTemp         > 70    ) ||
		(batteryParamsPtr->BatteryChargeVolts     > 36000 )
	  )
	{
		/* Battery type is invalid so use default values */
		SetBatteryDafaultParams();
	}


	if( (batteryParamsPtr->BatteryPackShutdownLevelVolts < 10000 ) ||
			(batteryParamsPtr->BatteryPackShutdownLevelVolts > 30000) )
	{
		batteryParamsPtr->BatteryPackShutdownLevelVolts = 22500;
	}

	if( (batteryParamsPtr->BatteryPackDeepDischargeVolts < 10000 ) ||
			(batteryParamsPtr->BatteryPackDeepDischargeVolts > 30000) )
	{
		batteryParamsPtr->BatteryPackDeepDischargeVolts = 19000;
	}



#ifdef CHARGER_TESTING
	batteryParamsPtr->BatteryType = BATT_TYPE_NI_MH;
	/********************************************************
	 ********** Temporary test code only ********************
	 ********** *********************************************/
	if( batteryParamsPtr->BatteryType == BATT_TYPE_LEAD_ACID)
	{
		batteryParamsPtr->BatteryChargeVolts		= 27500; // BatteryCellChargingVolts * 2
		batteryParamsPtr->BatteryChargedVolts 		= 27000; // NOT USED
		batteryParamsPtr->BatteryMaxChargeTime 		= 86400; // BatteryPackMaxChargeTime
		batteryParamsPtr->BatteryFullChargeVolts 	= 25800; // BatteryCellFullChargeVolts * 2
		batteryParamsPtr->BatteryChargeTrigger 		= 25800; // BatteryPackChargeTrigger
		batteryParamsPtr->BatteryTimeBetweenCharge 	= 120;   // BatteryPackTimeBetweenCharge
		batteryParamsPtr->BatteryChargeRate			= 750;   // BatteryCellChargingCurrent
	}
	else if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		batteryParamsPtr->BatteryChargeVolts		= 29000;
		batteryParamsPtr->BatteryChargedVolts 		= 27600; // NOT USED
		batteryParamsPtr->BatteryMaxChargeTime 		= 57600;
		batteryParamsPtr->BatteryFullChargeVolts 	= 27000;
		batteryParamsPtr->BatteryChargeTrigger 		= 25800;
		batteryParamsPtr->BatteryTimeBetweenCharge 	= 120;
		batteryParamsPtr->BatteryChargeRate			= 1500;
	}

	else if( batteryParamsPtr->BatteryType == BATT_TYPE_UNKNOWN)
	{
		batteryParamsPtr->BatteryChargeVolts		= 27500;
		batteryParamsPtr->BatteryFloatChargeRate	= 250;
		batteryParamsPtr->BatteryChargeRate			= 750;
	}
	/*******************************************************
	 *******************************************************/
#endif

	batteryType();

	/*
	 * Get the revision level for the charger board
	 */
	chargerBoardRevisionMajor = SSGetFeatureRevisionMajor();
	chargerBoardRevisionMinor = SSGetFeatureRevisionMinor();

	/************************************************************************************
	 * Newer versions of the charger board have a bit set in the optionBitField
	 * in the PSM NVRAM to indicate the charging current is software controlled
	 * A special case is minor revision 3 boards which can't control the current but
	 * will provide enough Volts to charge NiMH batteries.
	 * Older boards with minor revision other than 3 can only support lead acid charging
	 ************************************************************************************/
	if ( NVRAMOptPSMPtr->optionBitField & NVRAMOPTPSMDEF_CHARGERCURRENTCONTROLFITTED_BM )
	{
		/*
		 * The digipots for controlling the charging current
		 * limit are enabled on this version of the charger board.
		 */
		currentControlEnabled = LU_TRUE;

	}
	else
	{
		/*
		 * If the battery type is NiMH check that the charger board hardware can support
		 * charging this type of battery.
		 */
		if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH )
		{
			/* Error - Hardware doesn't support NiMH charging */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_BATT_NIHM_NOT_SUPPORTED,
								0
							   );
		}
	}

	/* Set test load current */
	sbError = IOManagerSetValue(IO_ID_DAC_OUT_BT_TEST_LOAD, 500);

	/* Set initial charge current */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET, batteryParamsPtr->BatteryChargeRate);

	/*
	 * Initialise battery status structure.
	 */
	batteryStatePtr->BatteryVoltage 			= 0;								/* Current battery voltage level 							*/
	batteryStatePtr->BatteryChargingVolts 		= 0;								/* Current voltage output of charger						*/
	batteryStatePtr->BatteryTemp 				= 0;								/* Current temperature of the battery						*/
	batteryStatePtr->BatteryCharge 				= 0;								/* Current charge applied to the battery					*/
	batteryStatePtr->BatteryChargeRate 			= 0;								/* Charging current specified for the battery				*/
	batteryStatePtr->BatteryChargeTime 			= 0;								/* Maximum Charging time specified for the battery			*/
	batteryStatePtr->BatteryChargeVolts 		= 0;								/* Charging voltage specified for the battery				*/
	batteryStatePtr->BatteryType 				= batteryParamsPtr->BatteryType;	/* Indicates type of battery in use							*/
	batteryStatePtr->BatteryConnected 			= LU_FALSE;							/* Indicates if battery is connected						*/
	batteryStatePtr->BatteryDisConnected 		= LU_TRUE;							/* Indicates if battery is disconnected						*/
	batteryStatePtr->BatteryOverride			= LU_FALSE;							/* Indicate if charging override is in operation			*/
	batteryStatePtr->BatteryChgCycleTotalChargeCurrent = 0;
	batteryStatePtr->BatteryTotalChargeCurrent	= 0;
	batteryStatePtr->BatteryTotalDrainCurrent	= 0;
	batteryStatePtr->BatteryTotalCharge			= 0;

	chargerStatePtr->ChargerEnabled				= 0;
	chargerStatePtr->ChargerState				= 0;
	chargerStatePtr->ChargerCurrentUnStable		= LU_FALSE;							/* Indicate if charging current not changing 				*/
	chargerStatePtr->ChargerEndReason			= BATT_CHARGE_STOP_INVALID;

	/* Init FSM context */
	ChargerContext_Init(&chargerFsm._fsm, &chargerFsm);

	/* Enable debug */
//	setDebugFlag(&chargerFsm._fsm, 1);

	/* Initialise charger variables */
	ChargeStartTime = 0;
	ChargeStopTime  = 0;
	ChargeCurrent	= 0;
	ChargeTime		= 0;
	TopUpTime		= 0;
	TopUpStartTime	= 0;
	initialPass		= LU_TRUE;
	batteryStatePtr->BatteryTotalCharge	= 0;
	batteryStatePtr->BatteryTotalDrainCurrent = 0;

	/* Indicate initialisation not done */
	chargerInitialised = LU_FALSE;

	/* Get current time */
	time = STGetTime();
	chargerInitTime = time;

	batteryDataNVRamBlkPtr->estimatedCapacity 	= batteryParamsPtr->BatteryMaxCharge;

	/* Load virtual IO_ID values from Battery Data NVRAM */
	sbError = IOManagerSetValue( IO_ID_ACCUMLATED_DISCHARGE_TIME       , batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin);
	sbError = IOManagerSetValue( IO_ID_ACCUMULATED_DISCHARGE_CURRENT   , batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours);

	sbError = IOManagerSetValue( IO_ID_ACCUMULATED_CHARGE_CURRENT      , batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours);
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME, batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin);
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_CHARGE_CYCLES           , batteryDataNVRamBlkPtr->chargeCycles);
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_ESTIMATED_CAPACITY      , batteryDataNVRamBlkPtr->estimatedCapacity);

	/*
	 * Sanity checks for battery parameters - Use defaults if any parameters fail
	 */

	/*
	 * Set channel value for battery type
	 */
	if( batteryParamsPtr->BatteryType == BATT_TYPE_LEAD_ACID)
	{
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_TYPE_NIHM,		LU_FALSE);
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_TYPE_LEAD_ACID,	LU_TRUE);
	}
	else if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_TYPE_NIHM,		LU_TRUE);
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_TYPE_LEAD_ACID,	LU_FALSE);
	}
	else if( batteryParamsPtr->BatteryType == BATT_TYPE_UNKNOWN)
	{
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_TYPE_NIHM,		LU_FALSE);
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_TYPE_LEAD_ACID,	LU_FALSE);
	}
}

void ChargerFSMTick(void)
{
	static lu_int32_t chargeCurrentPrevious;
	static lu_uint32_t lastCheckTime;
	static lu_uint32_t elapsedTime;
	static lu_bool_t firstTime = LU_TRUE;
#ifdef DEBUG_OUTPUT
	static lu_uint8_t printCount = 0;
#endif
	static lu_uint8_t printCountLogging = 0;
	SB_ERROR sbError;
	SB_ERROR RetVal;
	lu_int32_t 	batteryVolts = 0;
	lu_int32_t  chargerCurrent = 0;
	lu_int32_t  motorCurrent = 0;
	lu_int32_t  logicCurrent = 0;
	lu_int32_t  mainDcCurrent = 0;
	lu_int32_t  powerPSU = 0;
	lu_int32_t  LVAbsent = 0;
	lu_int32_t  maxPower = 0;
	lu_int32_t  comms1Volts = 0;
	lu_int32_t  comms2Volts = 0;
	lu_int32_t  batteryCurrent = 0;
	lu_int32_t  batteryTestCurrent;
	lu_int32_t  batTestEn;

	lu_uint32_t time;
	lu_int32_t 	batTemp;
	lu_int32_t  tempRange;
	lu_int32_t  totalChargeCurrent = 0;
	lu_int32_t  totalDrainCurrent = 0;
	lu_int32_t	ChargerOutput=0;
	lu_int32_t	BatteryCurrent=0;
	lu_int32_t	ChargerCycleCharge=0;
	lu_int32_t	ChargerCycleTime=0;
	lu_int32_t	chargerVolts=0;
	lu_int32_t  battDisconected;

	/*
	 * Periodic Tick to initiate events in state machine.
	 */
	ChargerContext_TimerTickEvent(&chargerFsm._fsm);

	/* Get current time */
	time = STGetTime();

	/*
	 * The initialisation can't happen until the IO manager has populated the data.
	 * Get time since the first execution of the tick function and only call the
	 * initialisation function if the required time has passed. Only call the initialisation once.
	 */

	if( chargerInitialised == LU_FALSE )
	{
		elapsedTime = STElapsedTime( chargerInitTime , time);
		/* If enough time has passed since the first tick then initialise  charger */
		if( elapsedTime > CHARGER_INIT_DELAY )
		{
			/* Initialise charger */
			RetVal = ChargerInit();
			chargerInitialised = LU_TRUE;
		}
	}
	else	// if( chargerInitialised == LU_FALSE )
	{
		/* Read the temperature sensor on the PSM and the battery */
		GetTemperature();

		/*
		 *  Get current and voltage values
		 */

		/* Get motor supply current only if the motor supply is turned on */
		if( motorSupplyOn() == LU_TRUE )
		{
			sbError = IOManagerGetCalibratedUnitsValue(IO_ID_MOT_I_SENSE,&motorCurrent);
		}
		else
		{
			motorCurrent = 0;
		}

		/*
		 * Get calibrated analogue values from IO manager
		 */
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_LOGIC_I_SENSE,&logicCurrent);
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_MAIN_DC_I_SENSE,&mainDcCurrent);
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE,&chargerCurrent);
		sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_V_SENSE, &batteryVolts);
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_COMMS_1_V_SENSE,&comms1Volts);
		sbError = IOManagerGetCalibratedUnitsValue( IO_ID_COMMS_2_V_SENSE, &comms2Volts);
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_TEST_I_SENSE, &batteryTestCurrent);

		sbError = IOManagerGetValue(IO_ID_VIRT_AC_OFF,&LVAbsent);
		sbError = IOManagerGetValue(IO_ID_BAT_TEST_EN, &batTestEn);

		/*
		 * If the battery is disconected then set filtered battery voltage
		 * to zero so the charger Voltage is not returned to the SCADA
		 */

		/* Check for battery disconected */
		sbError = IOManagerGetValue( IO_ID_VIRT_BT_DISCONNECTED, &battDisconected);

		if(battDisconected)
		{
			/* Battery is not present so set filtered battery volts to zero volts */
			sbError = IOManagerSetValue(IO_ID_VIRT_BATTERY_VOLTAGE, 0 );
		}
		else
		{
			/* Battery is present so set filtered battery volts to physical volts */
			sbError = IOManagerSetValue(IO_ID_VIRT_BATTERY_VOLTAGE, batteryVolts);
		}

//	_printf("%c[%d;%dH",esc,4,101);		\
//	sbError = IOManagerGetValue( IO_ID_VIRT_BATTERY_VOLTAGE, &batteryVolts);
//	_printf(" Batt Virt Volts [%05d]",batteryVolts);

		/*
		 * Record the battery current drain and the battery charging current
		 * to keep a running total of the battery charge remaining.
		 *
		 * If the charger is on then add the current to the total
		 * Tick runs every second so adding mAH/3600 to total
		 *
		 * If the charger isn't on then subtract the drain current from the
		 * running total of battery charge remaining.
		 */

		DrainCurrent =  motorCurrent;
		if (batTestEn)
		{
			DrainCurrent += batteryTestCurrent;
		}
		if (LVAbsent)
		{
			DrainCurrent += mainDcCurrent;
		}
		
		if( chargerStatePtr->ChargerEnabled == LU_TRUE )
		{
			if (DrainCurrent > chargerCurrent)
			{
				DrainCurrent -= chargerCurrent;
			}
			else
			{
				/* Putting in more charge than current drain from battery */
				DrainCurrent = 0;
			}
			
			/* Add the number of mA seconds to the total charge. */
			batteryStatePtr->BatteryTotalChargeCurrent += chargerCurrent;

			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds += chargerCurrent;

			if(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds > MA_SEC_IN_A_HOUR)
			{
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds -= MA_SEC_IN_A_HOUR;
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours += 1;
			}

			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.seconds += 1;
			if( (batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.seconds) > SEC_IN_MIN )
			{
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.seconds -= SEC_IN_MIN;
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin += 1;
			}
			updateDataNVRAM = LU_TRUE;

			sbError = IOManagerSetValue( IO_ID_ACCUMULATED_CHARGE_CURRENT, batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours);

			sbError = IOManagerSetValue( IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME,batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin );

			/* Add charging time to correct temperature band */
			batTemp = batteryStatePtr->BatteryTemp;
			if( batTemp < 0 )
			{
				tempRange = BAT_TEMP_RANGE_BELOW_0;
			}
			else if( (batTemp < 0) && (batTemp < 40 ) )
			{
				tempRange = BAT_TEMP_RANGE_00_40;
			}
			else if( (batTemp < 0) && (batTemp < 40 ) )
			{
				tempRange = BAT_TEMP_RANGE_40_60;
			}
			else
			{
				tempRange = BAT_TEMP_RANGE_ABOVE_60;
			}

			batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.seconds	+=  ChargeTime;

			/* Convert seconds to hours and seconds if needed */
			if( batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.seconds > SEC_IN_MIN )
			{
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.seconds -= SEC_IN_MIN;
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.totalMin   += 1;
			}

			/* Add the latest charge current to the correct temperature band */
			batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.mAmpSeconds	+=  batteryStatePtr->BatteryTotalChargeCurrent;

			/* Convert mA seconds to Amp hours and mA seconds if needed */
			if( batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.mAmpSeconds > MA_SEC_IN_A_HOUR )
			{
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.mAmpSeconds -= MA_SEC_IN_A_HOUR;
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.total100mAmpHours += 1;
			}
		}		// if( chargerStatePtr->ChargerEnabled == LU_TRUE )
		else
		{
			firstTime = LU_TRUE;
			chargeCurrentPrevious = 0;
		}

		/* Set positive for current drain / negative for battery charge current */
		if (LVAbsent)
		{
			/* No LV so just set to positive drain current */
			batteryCurrent = DrainCurrent;
		}
		else
		{
			if (DrainCurrent > 0)
			{
				batteryCurrent = DrainCurrent;
			}
			else if (chargerStatePtr->ChargerEnabled == LU_TRUE)
			{
				batteryCurrent = (0 - chargerCurrent);

				if (chargerCurrent)
				{
					/* Count battery cycle charge current */
					batteryStatePtr->BatteryChgCycleTotalChargeCurrent += chargerCurrent;

					sbError = IOManagerSetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE,
							                    (batteryStatePtr->BatteryChgCycleTotalChargeCurrent / (60 * 60))
							                   );
				}

			}
		}
		
		/* Record battery current */
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_BATTERY_CURRENT, batteryCurrent);
		
		/* Record the drain current */
		sbError = IOManagerSetValue(IO_ID_VIRT_DRAIN_CURRENT, DrainCurrent);

		/*
		 * Calculate the drain current and discharge time
		 */
		if (DrainCurrent > 0)
		{
			/* Add the number of mA seconds to the total discharge. */
			//batteryStatePtr->BatteryTotalDrainCurrent += DrainCurrent;

			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds += DrainCurrent;

			if(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds > MA_SEC_IN_A_HOUR)
			{
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds -= MA_SEC_IN_A_HOUR;
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours += 1;
			}

			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.seconds += 1;
			if( (batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.seconds) > SEC_IN_MIN )
			{
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.seconds -= SEC_IN_MIN;
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin += 1;
			}
			updateDataNVRAM = LU_TRUE;

			sbError = IOManagerSetValue( IO_ID_ACCUMULATED_DISCHARGE_CURRENT, batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours);
			sbError = IOManagerSetValue( IO_ID_ACCUMLATED_DISCHARGE_TIME,     batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin);
		}		// if (DrainCurrent > 0)

		/*
		 * Calculate the present charge state of the battery
		 */
		batteryStatePtr->BatteryTotalCharge = batteryStatePtr->BatteryTotalChargeCurrent - batteryStatePtr->BatteryTotalDrainCurrent;
		/* Set sign bit to zero as negative value not valid */
		batteryDataNVRamBlkPtr->estimatedCapacity &= 0x7fffffff;
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_ESTIMATED_CAPACITY      , batteryDataNVRamBlkPtr->estimatedCapacity);

		/* If the charger is on then check PSU power */
		if( chargerStatePtr->ChargerEnabled == LU_TRUE  )
		{
			/*
			 * Check the total power drawn on the PSU.
			 * This must not exceed the maximum for the PSU.
			 * The maximum power that can be drawn is reduced with temperature.
			 * If the total power being drawn approaches the limit, the charging current
			 * limit must be reduced to keep within the safe limit.
			 */

			/*
			 * Calculate the power being drawn from the PSU
			 * PSU supplies. This is:
			 *  (Main DC current * 36V) + (battery charger current * battery charger volts)
			 */
			powerPSU = (mainDcCurrent * MAIN_DC_VOLTS) + ((chargerCurrent * batteryVolts)/1000);

			/*
			 * Calculate max allowed power at current temperature
			 */
			if(  (batteryStatePtr->BatteryTemp) > PSU_DERATE_TEMP )
			{
				maxPower = MAX_PSU_POWER / 2;
			}
			else
			{
				maxPower = MAX_PSU_POWER;
			}

			/*
			 * Calculate max charging current to stay within max power
			 * maxPower = (mainDcCurrent * MAIN_DC_VOLTS) + (chargerCurrent * batteryVolts)
			 */
			chargerCurrentLimit = (( maxPower - (mainDcCurrent * MAIN_DC_VOLTS) ) * 1000 ) / batteryVolts;
		}		// if( chargerStatePtr->ChargerEnabled == LU_TRUE  )

		/* If the charger is on then check for charging current changing */
		if( chargerStatePtr->ChargerEnabled == LU_TRUE  )
		{
			if( firstTime == LU_TRUE)
			{
				lastCheckTime = STGetTime();
				firstTime = LU_FALSE;
			}

			/* Record charging current */
			sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_CHARGER_I_SENSE, &ChargeCurrent);

			elapsedTime = STElapsedTime( lastCheckTime, STGetTime() );
			/* Only check the charging current if the required time period has passed */
			if( elapsedTime > (BATT_CHARGING_HYSTERISIS_TIME * MSEC) )
			{
				/* The time period has passed so check if the charging current has changed */
				if( (ChargeCurrent < chargeCurrentPrevious - BATT_CHARGING_HYSTERISIS_CURRENT) ||
					(ChargeCurrent > chargeCurrentPrevious + BATT_CHARGING_HYSTERISIS_CURRENT) 	)
				{
					/* The charging current is not stable so set flag true */
					chargerStatePtr->ChargerCurrentUnStable = LU_TRUE;
					chargeCurrentPrevious = ChargeCurrent;
				}
				if( (ChargeCurrent <  BATT_MINIMUM_CHARGING_CURRENT ) && (chargeCurrentPrevious < BATT_MINIMUM_CHARGING_CURRENT))
				{
					/* The charging current has stayed below the minimum so set flag true */
					chargeCurrentPrevious = 0;
					chargerStatePtr->ChargerCurrentMinimum = LU_TRUE;
				}
				else
				{
					/* The charging current hasn't changed so set flag false */
					chargeCurrentPrevious = ChargeCurrent;
					chargerStatePtr->ChargerCurrentUnStable = LU_FALSE;
					chargerStatePtr->ChargerCurrentMinimum  = LU_FALSE;
				}
				/* Reset the last checked time */
				lastCheckTime = STGetTime();
			}		// if( elapsedTime > (BATT_CHARGING_HYSTERISIS_TIME * MSEC) )
		}		// if( chargerStatePtr->ChargerEnabled == LU_TRUE  )
	}  // if( chargerInitialised == LU_FALSE )

	totalChargeCurrent = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds;
	totalDrainCurrent = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds;

	#ifdef DEBUG_OUTPUT
	if( printCount++ >= 5 )
	{
		printCount = 0;
		if( chargerStatePtr->ChargerEnabled == LU_TRUE  )
		{
			batteryType();
			pos((CHARGER_POSITION+2),1);
			_printf("\tCharge Current mA  [%8d]\t      DrainCurrent mA [%8d]\t\t   Charge Cycles [%8d]\n\r",ChargeCurrent,DrainCurrent,batteryDataNVRamBlkPtr->chargeCycles);
			_printf("\tCurrent limit mA   [%8d]\t\t    PSU OP mW [%8d]\t\t       Max Power [%8d]\n\r",chargerCurrentLimit,powerPSU,maxPower);
			_printf("Total Charge 100mAHours    [%8d]\t",(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING]		.current.total100mAmpHours));
			_printf("Total Charge mAsec    [%8d]\t",totalChargeCurrent);
			_printf("   Total Charge Time min [%8d]\n\r",(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING]		.time.totalMin));
			_printf("Total Discharge 100mAHours [%8d]\t",(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING]	.current.total100mAmpHours));
			_printf("Total Discharge mAsec   [%8d]\t",totalDrainCurrent);
			_printf("Total Discharge Time min [%8d]\n\r",(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING]	.time.totalMin));
			_printf("Estimated Battery Capacity [%8d]\t",batteryDataNVRamBlkPtr->estimatedCapacity);
			_printf("Maximum Charge Time   [%8d]\n\n\r",batteryParamsPtr->BatteryMaxChargeTime);
		}
	}

//	#else
//	if( chargerStatePtr->ChargerEnabled == LU_TRUE  )
//	{
//		pos((CHARGER_POSITION+2),1);
//		_printf("\r\tCharge Current mA  [%8d]\t    \r",ChargeCurrent,DrainCurrent);
//	}
	#endif


	sbError = IOManagerGetValue(IO_ID_BAT_CHARGER_EN, &ChargerOutput);

	sbError = IOManagerGetValue(IO_ID_VIRT_BT_BATTERY_CURRENT, &BatteryCurrent);
	sbError = IOManagerGetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE, &ChargerCycleCharge);
	sbError = IOManagerGetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_TIME, &ChargerCycleTime);

	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE, &chargerCurrent);
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_CHARGER_V_SENSE, &chargerVolts);
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_V_SENSE, &batteryVolts);


	#ifdef LOGGING_OUTPUT_ENABLED
	if( printCountLogging++ >= 5 )
	{
		#ifdef DEBUG_OUTPUT
		logging;
		#endif
		printCountLogging = 0;
		/*
		 * Output CSV logging data to the serial port rather than debug to the screen
		 */
		_printf("%d,",batteryStatePtr->BatteryType   					);
		_printf("%d,",ChargerOutput   									);
		_printf("%d,",batteryStatePtr->BatteryChargerVoltLimitOutput   	);
		_printf("%d,",chargerVolts  						 			);
		_printf("%d,",batteryVolts  									);
		_printf("%d,",chargerCurrent									);
		_printf("%d,",DrainCurrent   									);
		_printf("%d,",batteryStatePtr->BatteryTempAverage   			);
		_printf("%d,",batteryStatePtr->RTUTemp   						);
		_printf("%d,",batteryStatePtr->BatteryDeltaTemp  				);
		_printf("%d,",batteryStatePtr->BatteryDeltaVolts 				);
		_printf("%d,",batteryStatePtr->BatteryAverageVoltage			);
		_printf("%d,",BatteryCurrent 									);
		_printf("%d,",batteryStatePtr->BatteryChgCycleTotalChargeCurrent);
		_printf("%d,",batteryStatePtr->BatteryChargeTime				);
		_printf("%d", chargerStatePtr->ChargerEndReason					);
		_printf("\r\n");
	}
	#endif

	/* If the NVRAM data block has been changed then call the API function to update the NVRAM */
	if ( updateDataNVRAM == LU_TRUE)
	{
		sbError = NVRamBatteryDataWriteUpdate( NVRAM_BATT_DATA_BLK_USER_A );
		updateDataNVRAM = LU_FALSE;
	}
}

lu_uint32_t DisconnectBattery(void)
{
	lu_uint32_t retVal;
	lu_uint32_t time;
	lu_uint32_t elapsedTime;
	SB_ERROR sbError;

	/*
	 * Check for disconnect delay finished and disconnect the battery if it has
	 * This will power down the PSM unless the LV supply has come back on since the
	 * shutdown was instigated
	 */
	time = STGetTime();

	elapsedTime = STElapsedTime( disconnectStartTime , time );
	if(elapsedTime > BATTERY_DISCONNECT_DELAY )
	{
		/* Period expired disconnect the battery */
		sbError = IOManagerSetPulse(IO_ID_BAT_DISCONNECT, DISCONNECT_PULSE_MS);
		batteryStatePtr->BatteryConnected 	 = LU_FALSE;		/* Indicates if battery is connected	*/
		batteryStatePtr->BatteryDisConnected = LU_TRUE;			/* Indicates if battery is disconnected	*/
		/* Set virtual point to indicate battery is disconnected */

		sbError = IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, LU_TRUE);
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);

		retVal = LU_TRUE;
	}
	else
	{
		retVal = LU_FALSE;
	}

	return retVal;
}

lu_uint32_t chargerTestComplete(void)
{
	lu_int32_t retValue = LU_FALSE;

	/*
	 * Call function to test the battery charger.
	 * The normal battery charging is regime is stopped and the charger turned off.
	 * After the test ends the charging regime is reset to the initial state.
	 */
	retValue = ChargerTest();

	return retValue;
}

lu_uint32_t batteryTestComplete(void)
{

	static lu_uint32_t previousTime = 0;
    static lu_bool_t   batteryTestOverload = LU_FALSE;

	lu_int32_t retValue = LU_FALSE;

	lu_int32_t batteryVolts;
	lu_int32_t drainCurrent;
	lu_int32_t current;
	lu_int32_t maxdrainCurrent;
	lu_int32_t time;
	lu_uint32_t elapsedTime;
	lu_int32_t timeDiff;
	lu_uint32_t suspendedTime;
	lu_uint32_t loadTime;
	lu_int32_t testCurrentMa = 0;
	lu_uint16_t testTimeSecs;

	SB_ERROR sbError;

	/*  Valid reasons to stop the battery test
			BATT_TEST_STOP_PASSED
			BATT_TEST_STOP_UNDER_THRESHOLD
			BATT_TEST_STOP_OPERATION
			BATT_TEST_STOP_LV_LOST
			BATT_TEST_STOP_BATT_DISCONNECTED
			BATT_TEST_STOP_TIME_FINISHED
			BATT_TEST_STOP_CANCELED
	*/

	if (batteryTestOverload == LU_TRUE)
	{
		sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

		/* Battery test is inhibited, due to load over current */
		retValue = LU_TRUE;

		return retValue;
	}

	/*
	 * Get and record actual battery voltage
	 */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
	batteryStatePtr->BatteryVoltage 		= batteryVolts;
	testStatePtr->BatteryVoltage 			= batteryVolts;

	/* Get and record drain current */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_TEST_I_SENSE, &drainCurrent);

	/* Get current time */
	time = STGetTime();
	elapsedTime = STElapsedTime(testStatePtr->BatteryTestStartTime, time);

	/* Calculate the length of time since last test started */
	loadTime = STElapsedTime(loadStartTime, time);

	testStatePtr->BatteryTestTime = elapsedTime;

	// Used for battery test load only!!
	testTimeSecs = (loadTime / 1000);

	switch (batteryParamsPtr->BatteryTestLoadType)
	{
	case BATT_LOAD_TYPE_2AMP_PTC:
	default:
		if (testTimeSecs > BattTestLowTable1dU16U16[MAX_BATT_TEST_LOW_1DU16U16 - 1].distributionX)
		{
			testTimeSecs = BattTestLowTable1dU16U16[MAX_BATT_TEST_LOW_1DU16U16 - 1].distributionX;
		}
		testCurrentMa = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)&BattTestLowTable1dU16U16[0],
				                         MAX_BATT_TEST_LOW_1DU16U16,
				                         testTimeSecs);
		break;

	case BATT_LOAD_TYPE_10AMP_PTC_R:
		if (testTimeSecs > BattTestPtcHighTable1dU16U16[MAX_BATT_TEST_HIGH_1DU16U16 - 1].distributionX)
		{
			testTimeSecs = BattTestPtcHighTable1dU16U16[MAX_BATT_TEST_HIGH_1DU16U16 - 1].distributionX;
		}

		testCurrentMa = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)&BattTestPtcHighTable1dU16U16[0],
				                         MAX_BATT_TEST_HIGH_1DU16U16,
				                         testTimeSecs);
		break;
	}

	/* Set test load current */
	sbError = IOManagerSetValue(IO_ID_DAC_OUT_BT_TEST_LOAD, (lu_int32_t)testCurrentMa);

	/* Read the temperature sensor on the PSM and the battery */
	GetTemperature();

	/* Calculate the maximum drain allowed for specified percentage in mAHours */
	maxdrainCurrent = (batteryParamsPtr->BatteryMaxCharge * (lu_int32_t)batteryParamsPtr->BatteryTestDischargeCapacity ) / 100;

	/*
	 * If this is the first time this function is called then check
	 * that the conditions are okay for the battery test to take place
	 * Otherwise report the error via the Battery Test End Reason
	 */
	if( battTestFirstTime == LU_TRUE)
	{
		/* Reset low current alarm if that is what the last fail was caused by */
		if( testStatePtr->BatteryTestEndReason  == BATT_TEST_STOP_LOW_CURRENT )
		{
			SysAlarmSetLogEvent(LU_FALSE,
							SYS_ALARM_SEVERITY_INFO,
							SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
							SYSALC_BATTERY_CHARGER_BATT_TEST_LOW_CURRENT,
							0
						   );
		}

		/*
		 * Reset all the test data
		 */
		testStatePtr->BatteryTotalDrainCurrent = 0;

		/* Check for LV fail */
		if( LVFailedFlag == LU_TRUE )
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LV_LOST;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_LV_LOST\n\r");
			#endif
		}
		/* Check for battery disconnected */
		else if( batteryStatePtr->BatteryDisConnected == LU_TRUE )
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_BATT_DISCONNECTED;
			retValue = LU_TRUE;
			/* The test is deemed to have failed if the battery is seen to be disconnected */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL,1);
			batteryDataNVRamBlkPtr->batteryTestFailed = LU_TRUE;
			updateDataNVRAM = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_BATT_DISCONNECTED\n\r");
			#endif
		}
		/* Check for Operation in progress */
		else if( motorSupplyOn() == LU_TRUE )
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_OPERATION;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_OPERATION\n\r");
			#endif
		}
		/* Check for cancel command received */
		else if(testStatePtr->BatteryTestCancelled == LU_TRUE)
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_CANCELED;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_CANCELED\n\r");
			#endif
		}
		else if( batteryStatePtr->BatteryVoltage < batteryParamsPtr->BatteryTestThresholdVolts)
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_UNDER_THRESHOLD;
			retValue = LU_TRUE;

			/* Battery voltage below threshold so set battery test fail flag */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL, 1);

			batteryDataNVRamBlkPtr->batteryTestFailed = LU_TRUE;
			updateDataNVRAM = LU_TRUE;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_UNDER_THRESHOLD\n\r");
			#endif
		}
		/* Battery test can proceed so connect dummy load */
		else
		{
			/* Connect dummy load across battery */
			sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 1);
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_IN_PROGRESS, 1);

			testStatePtr->BatteryTestEndReason  = BATT_TEST_TEST_RUNNING;
			/* Record the start time of this part of the test */
			loadStartTime = time;
			retValue = LU_FALSE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test BATT_TEST_TEST_RUNNING\r");
			#endif
		}
		/* Initialise previous time */
		previousTime = time;
		/* Reset first time flag */
		battTestFirstTime = LU_FALSE;
	}
	else				/* Not the first time so battery test already running */
	{
		/* Check for reasons to stop the test */

		/* Check for load over-current failure */
		if (testTimeSecs == 5 && drainCurrent > 8200)
		{
			sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

			batteryTestOverload = LU_TRUE;

			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_BATT_TEST_OVERLOAD_CURRENT,
								0
							   );

			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_OVERLOAD_CURRENT;
			testStatePtr->BatteryTestSuspended  = LU_FALSE;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_OVERLOAD_CURRENT\n\r");
			#endif
		}
		/* Check for LV fail */
		else if( LVFailedFlag == LU_TRUE )
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LV_LOST;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_LV_LOST\n\r");
			#endif
		}
		/* Check for battery disconnected */
		else if( batteryStatePtr->BatteryDisConnected == LU_TRUE )
		{
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_BATT_DISCONNECTED;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;
			/* The test is deemed to have failed if the battery is seen to be disconnected */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL,1);
			batteryDataNVRamBlkPtr->batteryTestFailed = LU_TRUE;
			updateDataNVRAM = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_BATT_DISCONNECTED\n\r");
			#endif
		}
		/* Check for Operation in progress */
		else if( motorSupplyOn() == LU_TRUE )
		{
			/*
			 * If the motor supply is on this indicates that a switch operation
			 * is taking place. Therefore, stop the battery test.
			 */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_OPERATION;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_OPERATION\n\r");
			#endif
		}
		/* Check for cancel command received */
		else if(testStatePtr->BatteryTestCancelled == LU_TRUE)
		{
			/* The cancel command has been received so stop battery test */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_CANCELED;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;
			/* Reset the cancel flag */
			testStatePtr->BatteryTestCancelled = LU_FALSE;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_CANCELED\n\r");
			#endif
		}
		/*
		 *  Check that drain current is not below minimum value
		 *  Allow 5 seconds for the current to steady
		 *  Only check when the load is connected
		 */
		else if( (elapsedTime > 5000) && (drainCurrent < (lu_int32_t)(batteryParamsPtr->BatteryTestCurrentMinimum) )
				&& (  testStatePtr->BatteryTestSuspended == LU_FALSE  ) )
		{
			/* The drain current has fallen below the threshold so the test has failed */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LOW_CURRENT;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;

			/* Drain current below threshold so set battery test fail flag */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL,1);

			batteryDataNVRamBlkPtr->batteryTestFailed = LU_TRUE;
			updateDataNVRAM = LU_TRUE;

			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_INFO,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_BATT_TEST_LOW_CURRENT,
								0
							   );

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_LOW_CURRENT\n\r");
			#endif
		}
		else if( batteryStatePtr->BatteryVoltage < batteryParamsPtr->BatteryTestThresholdVolts)
		{
			/* The battery voltage has fallen below the threshold so has failed */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_UNDER_THRESHOLD;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;

			/* Battery voltage below threshold so set battery test fail flag */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL,1);

			batteryDataNVRamBlkPtr->batteryTestFailed = LU_TRUE;
			updateDataNVRAM = LU_TRUE;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_UNDER_THRESHOLD\n\r");
			#endif
		}
		else if( (lu_uint32_t)elapsedTime > testStatePtr->BatteryTestDuration)
		{
			/* The maximum duration of the battery test has been reached */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_TIME_FINISHED;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL, 0);
			batteryDataNVRamBlkPtr->batteryTestFailed = LU_FALSE;
			updateDataNVRAM = LU_TRUE;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_TIME_FINISHED\n\r");
			#endif
		}
		else if(( batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes != 0 ) &&
				( elapsedTime > ((lu_uint32_t)batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes * MSEC * SEC)  ) )
		{
			/* The maximum duration of the battery test has been reached */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_MAX_DURATION;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL, 0);
			batteryDataNVRamBlkPtr->batteryTestFailed = LU_FALSE;
			updateDataNVRAM = LU_TRUE;
			retValue = LU_TRUE;
			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_MAX_DURATION\n\r");
			#endif
		}
		else if( (testStatePtr->BatteryTotalDrainCurrent / SEC_IN_HOUR) > maxdrainCurrent  )
		{
			/* The discharge has reached 10% of the nominal battery capacity */
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_PASSED;
			testStatePtr->BatteryTestSuspended = LU_FALSE;
			retValue = LU_TRUE;

			/* Battery has reached 10% without falling below threshold so reset battery test fail flag */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL, 0);

			batteryDataNVRamBlkPtr->batteryTestFailed = LU_FALSE;
			updateDataNVRAM = LU_TRUE;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_MAX_DISCHARGE\n\r");
			#endif
		}
		else if( testStatePtr->BatteryTestSuspended == LU_TRUE )
		{
			/*
			 *  The battery test is suspended so check that the minimum time has elapsed
			 *  and restart the test if it has.
			 */
			suspendedTime = STElapsedTime( testStatePtr->BatteryTestSuspendedTime,time );
			if(suspendedTime < ((lu_uint32_t)batteryParamsPtr->BatteryTestSuspendTimeMinutes * MSEC * SEC) )
			{
				/* Battery test still suspended */
				#ifdef DEBUG_BATT_TEST_OUTPUT
				batttestpos;
				_printf("Battery Test Suspended - %d secs.\r",suspendedTime/MSEC);
				#endif

			}
			else
			{
				/*
				 * Restart the battery test -  reset flags and the suspend start time
				 */
				testStatePtr->BatteryTestSuspended = LU_FALSE;
				testStatePtr->BatteryTestSuspendedTime = 0;
				/* Connect dummy load across battery */
				sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 1);
				testStatePtr->BatteryTestEndReason  = BATT_TEST_TEST_RUNNING;
				loadStartTime = time;

				/*
				 * Record time so the drain current can be calculated
				 */
				previousTime = time;

				#ifdef DEBUG_BATT_TEST_OUTPUT
				batttestpos;
				_printf("Battery Test BATT_TEST_TEST_RUNNING\r");
				#endif
			}

			/* Battery test still in operation so don't change state */
			retValue = LU_FALSE;

		}
		else		/* No reason to stop the battery test */
		{
			/* Get and record drain current */
			sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_TEST_I_SENSE, &drainCurrent);

			testStatePtr->BatteryDischargeRate   = drainCurrent;

			/*
			 * Calculate the drain current since last the measurement
			 * and add it to the total.
			 */
			timeDiff= STElapsedTime( previousTime,time );
			current  = timeDiff * drainCurrent;
			/* Calculate mAseconds since last check. */
			current  = current / 1000;

			/* Add the number of mA seconds to the total discharge for battery test. */
			testStatePtr->BatteryTotalDrainCurrent += current;

			/* Update channel */
			sbError = IOManagerSetValue(IO_ID_VIRT_BATTERY_TEST_TOTAL_DISCHARGE,
					                    (testStatePtr->BatteryTotalDrainCurrent / SEC_IN_HOUR));

			retValue = LU_FALSE;

			/*
			 * Record time so the drain current can be calculated
			 */
			previousTime = time;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test BATT_TEST_TEST_RUNNING Duration [%d - %d] Discharge [%d] mAH Drain current [%d] Battery voltage [%d]    \r",
					(elapsedTime/1000),(loadTime/1000),(testStatePtr->BatteryTotalDrainCurrent/SEC_IN_HOUR),drainCurrent, batteryVolts );
			#endif

			/*
			 * Check to determine if the battery test should be suspended.
			 */
			if(testStatePtr->BatteryTestSuspended == LU_FALSE)
			{
				/*
				 * Battery test running.
				 * Check for the drain current indicating a high temperature has been reached.
				 * Repeat this until the required discharge level has been reached.
				 */
				if(  loadTime > ( (((lu_uint32_t)batteryParamsPtr->BatteryTestTimeMinutes)  * MSEC * SEC) + 1000)   )
				{
					/* Suspend the battery test but stay in battery test state */

					/* Check to see if using a Dynamic load */
					if (drainCurrent > 1000)
					{
						/* Is a not a dynamic load, need to suspend */

						/* Disconnect dummy load from battery */
						sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

						/* Set flag to indicate the battery test has been suspended */
						testStatePtr->BatteryTestSuspended = LU_TRUE;
						/* record the start time of the suspension */
						testStatePtr->BatteryTestSuspendedTime = time;

						/* Battery test still in operation so don't change state */
						retValue = LU_FALSE;
						/* Battery test suspended */
						#ifdef DEBUG_BATT_TEST_OUTPUT
						batttestpos;
						_printf("Battery Test Suspended \r");
						#endif
					}
				}
			}
		}
	}
	/*
	 * If the test has finished then remove the dummy load and enable the charger
	 */
	if(retValue == LU_TRUE)
	{
		/* Disconnect dummy load from battery */
		sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

		/* Reset the test in progress flag */
		testStatePtr->BatteryTestInProgress = LU_FALSE;
		batteryTestInProgress = LU_FALSE;

		sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_IN_PROGRESS, 0);

		/* Reset first time flag */
		battTestFirstTime = LU_TRUE;

		/* Set previous charging voltage */
		sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
		batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;

		#ifdef DEBUG_BATT_TEST_OUTPUT
		canpos;
		_printf("Battery Test Finished\n\r");
		#endif
	}

	return retValue;
}

lu_uint32_t batteryResetStatsComplete(void)
{
	lu_int32_t retValue = LU_TRUE;
	SB_ERROR sbError;

	/* Clear the local battery stats */
	#ifdef DEBUG_OUTPUT
	canpos;
	_printf("Reset battery stats event received\n\r");
	#endif

	/* Clear the battery stats NVRAM */
	memset( batteryDataNVRamBlkPtr,  0 , sizeof(BatteryDataNVRAMBlkStr) );		/* Fill the data block with zeroes	*/
	updateDataNVRAM	= LU_TRUE;							/* Set flag to indicate the NVRAM should be updated			*/

	/* Load virtual IO_ID values from Battery Data NVRAM */
	sbError = IOManagerSetValue( IO_ID_ACCUMLATED_DISCHARGE_TIME       , batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin);
	sbError = IOManagerSetValue( IO_ID_ACCUMULATED_DISCHARGE_CURRENT   , batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours);

	sbError = IOManagerSetValue( IO_ID_ACCUMULATED_CHARGE_CURRENT      , batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours);
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME, batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin);
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_CHARGE_CYCLES           , batteryDataNVRamBlkPtr->chargeCycles);
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_ESTIMATED_CAPACITY      , batteryDataNVRamBlkPtr->estimatedCapacity);

	return retValue;
}

lu_uint32_t chargingFinished(void)
{
	SB_ERROR sbError;
	lu_int32_t chargeCurrentScaled = 0;
	lu_int32_t retValue = LU_FALSE;
	lu_int32_t batteryVoltsScaled;

	/*
	 * Only execute if not NiMH batteries otherwise return false
	 */
    if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
    {
    	retValue = LU_FALSE;
    	return retValue;
    }

	/*
	 * Check to see if the battery has been charged to the maximum allowed for that battery.
	 *
	 * Check for:
	 * 	Maximum charge time reached
	 *  Charging current has dropped to minimum level
	 *  Charging current has changed over a set time
	 *
	 *  Any one of these conditions being satisfied will cause the charging cycle to be terminated.
	 */

	/* Record charging current */
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_CHARGER_I_SENSE, &ChargeCurrent);
	chargeCurrentScaled = ChargeCurrent;
	ChargeTime = STElapsedTime( ChargeStartTime, STGetTime() ) / MSEC;
	batteryStatePtr->BatteryChargeTime = ChargeTime;
	/* Get and scale battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVoltsScaled);

	/*
	 * If any of the conditions for battery finished are matched, the normal charging
	 * is considered finished but the top-up charge will then start.
	 * Charging will be stopped and recorded at the end of the top-up charge time
	 */

	/* Indicate if max charging time reached */
	if( ChargeTime > batteryParamsPtr->BatteryMaxChargeTime )
	{
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_MAX_TIME;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Max Time\n\r" );
		#endif

		retValue =  LU_TRUE;
	}
	/* Indicate if charging current is below minimum over the specified period */
	else if( chargerStatePtr->ChargerCurrentMinimum == LU_TRUE  )
	{
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_MIN_CURRENT;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Min Current\n\r" );
		#endif
		chargerStatePtr->ChargerCurrentMinimum = LU_FALSE;

		retValue =  LU_TRUE;
	}

	/* Indicate if charging current has changed over the specified period */
	else if(  chargerStatePtr->ChargerCurrentUnStable == LU_TRUE )
	{
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_UNSTABLE_CURRENT;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Unstable Current\n\r" );
		#endif

		/* Reset the flag indicating an unstable current */
		chargerStatePtr->ChargerCurrentUnStable = LU_FALSE;

		retValue =  LU_TRUE;
		/* Set virtual point to indicate battery fault */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 1);
	}

	/* No conditions matched so continue charging */
	else
	{
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_INVALID;
		retValue =   LU_FALSE;
	}

	return retValue;
}


lu_uint32_t chargingFinishedNiMH(void)
{
	SB_ERROR sbError;
	lu_int32_t chargeCurrentScaled = 0;
	lu_int32_t retValue = LU_FALSE;
	lu_int32_t batteryVoltsScaled;

	/*
	 * Only execute if NiMH batteries otherwise return false
	 */
    if( batteryParamsPtr->BatteryType != BATT_TYPE_NI_MH)
    {
    	retValue = LU_FALSE;
    	return retValue;
    }

	/*
	 * Check to see if the battery has been charged to the maximum allowed for that battery.
	 *
	 * Check for:
	 * 	Maximum charge time reached
	 *  Charging current has dropped to minimum level
	 *  Charging current has changed over a set time
	 *  dv/dt has dropped to zero - NOTE: To Be Implemented
	 *  dt/dt has increased
	 *
	 *  Any one of these conditions being satisfied will cause the charging cycle to be terminated.
	 */

	/* Record charging current */
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_CHARGER_I_SENSE, &ChargeCurrent);
	chargeCurrentScaled = ChargeCurrent;
	ChargeTime = STElapsedTime( ChargeStartTime, STGetTime() ) / MSEC;
	batteryStatePtr->BatteryChargeTime = ChargeTime;
	/* Get and scale battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVoltsScaled);

	/*
	 * If any of the conditions for battery finished are matched, the normal charging
	 * is considered finished but the balancing charge will then start.
	 * Because the battery is considered fully charged the charging cycle will be considrerd
	 * to be stopped and recorded now and not at the end of the balancing charge time
	 */

	/* Indicate if max charging time reached */
	if( ChargeTime > batteryParamsPtr->BatteryMaxChargeTime )
	{
		StopChargingBattery();
		RecordChargeCycle();
		/* Record time at which charger is turned off */
		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_MAX_TIME;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Max Time\n\r" );
		#endif

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		retValue =  LU_TRUE;
	}
	/* Indicate if charging current is below minimum over the specified period */
	else if( chargerStatePtr->ChargerCurrentMinimum == LU_TRUE  )
	{
 		StopChargingBattery();
 		RecordChargeCycle();
 		/* Record time at which charger is turned off */
 		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_MIN_CURRENT;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Min Current\n\r" );
		#endif
		chargerStatePtr->ChargerCurrentMinimum = LU_FALSE;

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		retValue =  LU_TRUE;
	}

	/* Indicate if charging current has changed over the specified period */
	else if(  chargerStatePtr->ChargerCurrentUnStable == LU_TRUE )
	{
		StopChargingBattery();
		RecordChargeCycle();
		/* Record time at which charger is turned off */
		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_UNSTABLE_CURRENT;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Unstable Current\n\r" );
		#endif
		chargerStatePtr->ChargerCurrentUnStable = LU_FALSE;

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		retValue =  LU_TRUE;
		/* Set virtual point to indicate battery fault */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 1);
	}
	/*
	 * For NiMH batteries only
	 * Indicate if the rate of change of battery temperature has been exceeded
	 */
	else if(CheckTemperatureChange() == LU_TRUE )
	{
		StopChargingBattery();
		RecordChargeCycle();
		/* Record time at which charger is turned off */
		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_DELTA_TEMP;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Delta temp\n\r" );
		#endif

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		retValue =  LU_TRUE;
	}

	/*
	 * For NiMH batteries only
	 * Indicate if the rate of change of battery voltage has reached zero
	 */
	else if( (CheckVoltageChange() == LU_TRUE) && (currentControlEnabled == LU_TRUE))
	{
		StopChargingBattery();
		RecordChargeCycle();
		/* Record time at which charger is turned off */
		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_DELTA_VOLTS;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Delta Volts\n\r" );
		#endif

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		retValue =  LU_TRUE;
	}

#ifdef DELTA_V_REMOVED
	else if(	batteryVoltsScaled >=   batteryParamsPtr->BatteryFullChargeVolts  )
	{
		StopChargingBattery();
		RecordChargeCycle();
		/* Record time at which charger is turned off */
		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_MAX_CAPACITY;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Max Volts\n\r" );
		#endif

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		retValue =  LU_TRUE;

	}
#endif

	/* No conditions matched so continue charging */
	else
	{
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_INVALID;
		retValue =   LU_FALSE;
	}

	return retValue;
}

lu_uint32_t topUpFinished(void)
{
	SB_ERROR sbError;
	lu_int32_t chargeCurrentScaled = 0;
	lu_int32_t retValue = LU_FALSE;
	lu_int32_t batteryVoltsScaled;
	/*
	 * Check to see if the battery charge top-up has been finished
	 * Check for: Maximum top-up time reached
	 */

	/* Record charging current */
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_CHARGER_I_SENSE, &ChargeCurrent);
	chargeCurrentScaled = ChargeCurrent;
	TopUpTime = STElapsedTime( TopUpStartTime, STGetTime() ) / MSEC;
	batteryStatePtr->BatteryTopUpTime = TopUpTime;

	/* Get and scale battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVoltsScaled);

	/* Indicate if max charging time reached */
	if( TopUpTime > balancingTime )
	{
	    if( batteryParamsPtr->BatteryType != BATT_TYPE_NI_MH)
	    {
			StopChargingBattery();
			RecordChargeCycle();
			/* Record time at which charger is turned off */
			RecordChargeTime();
	    }

		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_TOP_UP_TIME;
		#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging top up stopped - Max Time\n\r" );
		#endif

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);
		topUpState = LU_FALSE;
		retValue =  LU_TRUE;
	}

	/* No conditions matched so continue charging */
	else
	{
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_INVALID;
		retValue =   LU_FALSE;
	}

	return retValue;
}

lu_uint32_t initComplete(void)
{
	lu_uint32_t retVal;

	/*
	 * Check that the charger has been initialised
	 * Return True if if it has
	 * Return False if not
	 */

	if( chargerInitialised == LU_TRUE)
	{
		retVal= LU_TRUE;
	}
	else
	{
		retVal= LU_FALSE;
	}

	return retVal;
}


lu_uint32_t preNiMHComplete(void)
{
	SB_ERROR sbError;
	lu_uint32_t retVal = LU_TRUE;

	/*
	 * For NiMH set the required charging Volts to the value specified in the NVRAM
	 * There is no temperature compensation, calibration or control of the charging current
	 * using the charging Voltage so the charging Voltage will not change.
	 */
	batteryStatePtr->BatteryChargeVolts				= batteryParamsPtr->BatteryChargeVolts;
	batteryStatePtr->BatteryChargerVoltLimitOutput 	= batteryParamsPtr->BatteryChargeVolts;
	chargerVoltLimitOutput 							= batteryParamsPtr->BatteryChargeVolts;
	batteryStatePtr->BatteryCorrectedChargingVolts 	= batteryParamsPtr->BatteryChargeVolts;
	chargerVoltsCal 								= 1;	/* No calibration */

	/* Set the charging voltage  */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));

	/* Set the charging current */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET,  batteryParamsPtr->BatteryChargeRate  );

	/* Reset the charger fault flag */
	sbError = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,0);

	/* Turn charger on */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 1);
	chargerStatePtr->ChargerEnabled = TRUE;

	/* Turn on charging green LED */
	sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 1);

	/* Connect the charger to the battery */
	sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_TRUE);

	return retVal;
}

lu_uint32_t preCheckComplete(void)
{
	lu_uint32_t retVal;
#ifdef DEBUG_OUTPUT
	lu_uint8_t i;
#endif


	/*
	 * Check that the actual charger voltage output matches the set voltage
	 * Return True if okay
	 * Return False if not
	 */
	retVal = ChargerVoltsCheck();


	if( retVal == LU_TRUE)
	{
		retVal= LU_TRUE;
		#ifdef DEBUG_OUTPUT
		for(i=0;i<9;i++)
		{
			pos( (CHARGER_POSITION + i) , 1);
			cll;
		}
		#endif
	}
	else
	{
		retVal= LU_FALSE;
	}

	return retVal;
}

lu_uint32_t statusCheckComplete(void)
{
	lu_int32_t retVal;
	lu_uint32_t time;
	lu_uint32_t elapsedTime;

	static lu_uint32_t lastCheckTime;
	static lu_bool_t firstTime = LU_TRUE;

	/*
	 * Disconnect the charger from the battery.
	 * Wait for the volts to drop.
	 * Check the battery voltage and the charger voltage
	 */

	/* Get current time */
	time = STGetTime();
	elapsedTime = STElapsedTime( lastCheckTime , time);

	if( firstTime == LU_TRUE )
	{
		/* First time called so reset parameters */
		lastCheckTime = time;
		retVal = LU_FALSE;
		firstTime = LU_FALSE;
	}
	else if( elapsedTime > (BATTERY_SETTLE_PERIOD * MSEC) )
	{
		retVal = LU_TRUE;
		lastCheckTime = time;
		firstTime = LU_TRUE;
	}
	else
	{
		retVal = LU_FALSE;
	}

    return retVal;
}

lu_uint32_t batteryCharged(void)
{
	SB_ERROR sbError;
	lu_uint32_t retVal;
	lu_int32_t batteryVoltsScaled;
	lu_int32_t batteryVolts;
	lu_int32_t loadCurrentScaled;
	lu_int32_t loadCurrent;

	retVal = LU_FALSE;

	/* Get and scale battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
	batteryVoltsScaled = batteryVolts;

	/* Get and scale discharge current */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_TEST_I_SENSE, &loadCurrent);
	loadCurrentScaled = loadCurrent;

#ifdef DEBUG_OUTPUT
	messagepos;
	_printf("Battery volts [%d]"    ,batteryVolts);
	_printf("\tFull charge volts [%d]",batteryParamsPtr->BatteryFullChargeVolts );
#endif

	/*	Check if battery voltage has reached full charge voltage */
	if(	batteryVoltsScaled >=   batteryParamsPtr->BatteryFullChargeVolts  )
	{
		retVal = LU_TRUE;
		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);
		RecordChargeCycle();
		/* Record time at which charger is turned off */
		RecordChargeTime();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_MAX_CAPACITY;

#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("Battery Charging stopped - Full charge reached.");
#endif
	}
	else
	{
		retVal = LU_FALSE;
#ifdef DEBUG_OUTPUT
		chargeendpos;
		_printf("\tFull charge not reached so continue charging.");
#endif
	}

	return retVal;
}

lu_uint32_t LVSupplyConnected(void)
{
	SB_ERROR sbError;
	lu_int32_t dcVolts;
	lu_int32_t mainDCVoltsScaled;
	lu_uint32_t time;
	lu_uint32_t currentTime;

	/*
	 * Check that the the Low voltage supply is present
	 */

	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_36V_RAW_DC_V_SENSE, &dcVolts);
	mainDCVoltsScaled =  dcVolts;
	if( mainDCVoltsScaled < LV_SUPPLY_THRESHOLD_VOLTAGE )
	{
		/* Turn on red Mains LED */
		sbError = IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, 0);
		sbError = IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_RED, 1);

		/* Set AC voltage virtual point to show AC volts not present */
		sbError = IOManagerSetValue(IO_ID_VIRT_AC_OFF,LU_TRUE);

		/* If enabled then turn charger off */
		if( chargerStatePtr->ChargerEnabled == LU_TRUE  )
		{
			StopChargingBattery();
			/* Record time at which charger is turned off */
			RecordChargeTime();
		}

		/*
		 * If the battery test was running when the LV was lost then
		 * stop the test and set the reason.
		 */
		if( batteryTestInProgress == LU_TRUE )
		{

			/* Disconnect dummy load from battery */
			sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

			/* Reset the test in progress flag */
			testStatePtr->BatteryTestInProgress = LU_FALSE;
			batteryTestInProgress = LU_FALSE;
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LV_LOST;

			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_IN_PROGRESS, 0);

			/* Reset first time flag */
			battTestFirstTime = LU_TRUE;

			/* Set previous charging voltage */
			sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
			batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_LV_LOST\n\r");
			#endif


		}

		return LU_FALSE;
	}
	else
	{
		/* Turn on green Mains LED */
		sbError = IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, 1);
		sbError = IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_RED, 0);

		/* Set AC voltage virtual point to show AC volts present */
		sbError = IOManagerSetValue(IO_ID_VIRT_AC_OFF,LU_FALSE);

		/* Check for LV failed flag and calculate the failure time */
		if(LVFailedFlag)
		{
			currentTime = STGetTime();
			time = STElapsedTime( LVFailTime, currentTime );
			if( time < (LV_FAILED_THRESHOLD * MSEC))
			{
				LVFailedFlag = LU_FALSE;
			}
		}

		/* Return true if the LV supply is detected else return false if running on battery. */
		return LU_TRUE;
	}
}

lu_uint32_t batteryNeedsCharging(void)
{
	SB_ERROR sbError;
	lu_int32_t batteryVolts;
	lu_int32_t dcVolts;
	lu_int32_t batteryVoltsScaled;
	lu_int32_t mainDCVoltsScaled;
	lu_int32_t retVal= LU_FALSE;
	lu_uint32_t time;
	lu_uint32_t onTime;
	lu_uint32_t currentTime;
	lu_int32_t value;

	/* Get and scale Main DC voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_36V_RAW_DC_V_SENSE, &dcVolts);
	mainDCVoltsScaled = dcVolts;

	/* Get and scale battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
	batteryVoltsScaled = batteryVolts;

	/*
	 * Compare current battery voltage against the charging threshold voltage for this battery.
	 */
	currentTime = STGetTime();
	onTime = STElapsedTime( chargerInitTime, currentTime );
	if((batteryParamsPtr->BatteryTimeBetweenCharge * MSEC ) > onTime)
	{
		time = batteryParamsPtr->BatteryTimeBetweenCharge * MSEC;
	}
	else
	{
		time = STElapsedTime( ChargeStopTime, currentTime );
	}

	/*
	 * Check to see if battery needs charging
	 */
	if(	batteryVoltsScaled >  ( batteryParamsPtr->BatteryChargeTrigger - CHARGER_HYSTERESIS) )
	{
		/* Indicate battery doesn't need charging */
		retVal = LU_FALSE;

		sbError = IOManagerGetValue(IO_ID_VIRT_BT_FULLY_CHARGED, &value);
		if (value != 1)
		{
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 1);
		}

#ifdef DEBUG_OUTPUT
		messagepos;
		_printf("Battery doesn't need charging.  \tBattery Volts[%d]\tBattery Charge Trigger[%d]",batteryVoltsScaled, batteryParamsPtr->BatteryChargeTrigger);
#endif
	}
	/*
	 * Check if the battery has previously been charged for a period long
	 * enough to be considered a charge cycle and if it was don't charge until
	 * the minimum time between charges has been reached
	 */
	else if( ((batteryParamsPtr->BatteryTimeBetweenCharge * MSEC ) >  time) && (initialPass != LU_TRUE) )
	{
		/* Minimum time between charges has not elapsed so don't start charging */
		retVal = LU_FALSE;
#ifdef DEBUG_OUTPUT
		messagepos;
		_printf("Battery needs charging but minimum time not elapsed [%d]\t",((batteryParamsPtr->BatteryTimeBetweenCharge * MSEC ) -  time));
		_printf("Charge stop time [%d]\tCurrent time [%d]\n\r",ChargeStopTime,currentTime);
#endif
	}
	/*
	 * If battery needs charging turn on the charger
	 */
	else if(	batteryVoltsScaled <  ( batteryParamsPtr->BatteryChargeTrigger + CHARGER_HYSTERESIS) )
	{
		/* Record time at which charger is turned on.  */
		ChargeStartTime = currentTime;

#ifdef DEBUG_OUTPUT
		messagepos;
		_printf("Battery needs charging. Start time [%d].  Battery Volts[%d]\tBattery Charge Trigger[%d]\n\r",currentTime,batteryVoltsScaled, batteryParamsPtr->BatteryChargeTrigger );
#endif

		/* Reset charge cycle time */
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_TIME, 0);
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE, 0);
		batteryStatePtr->BatteryChgCycleTotalChargeCurrent = 0;

		sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);

		/* Indicate battery needs charging */
		retVal = LU_TRUE;

		/* Set virtual point to indicate charge cycle running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 1);
	}

	/* Initial pass finished so reset flag */
	if(initialPass)
	{
		initialPass = LU_FALSE;
	}

	return retVal;
}

lu_uint32_t batteryTypeKnown(void)
{
	lu_uint32_t retValue;

	/*
	 * Return true only if the battery type is known.
	 * Otherwise if battery is unknown return false
	 */
	if( batteryParamsPtr->BatteryType == BATT_TYPE_UNKNOWN)
	{
		retValue = LU_FALSE;
	}
	else
	{
		retValue = LU_TRUE;
	}

	return retValue;
}

lu_uint32_t batteryTypeNiMH(void)
{
	lu_uint32_t retValue;

	/*
	 * Return true only if the battery type is known to be NiMH.
	 * Otherwise return false
	 */
	if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		retValue = LU_TRUE;
	}
	else
	{
		retValue = LU_FALSE;
	}

	return retValue;
}

lu_uint32_t batteryTypeLeadAcid(void)
{
	lu_uint32_t retValue;

	/*
	 * Return true only if the battery type is known to be Lead Acid.
	 * Otherwise return false
	 */
	if( batteryParamsPtr->BatteryType == BATT_TYPE_LEAD_ACID)
	{
		retValue = LU_TRUE;
	}
	else
	{
		retValue = LU_FALSE;
	}

	return retValue;
}

lu_uint32_t batteryCheck(void)
{
	SB_ERROR sbError;
	lu_uint32_t retVal;
	lu_uint32_t time;
	lu_uint32_t elapsedTime;
	lu_int32_t batteryVolts;
	lu_int32_t batteryVoltsScaled;
	lu_int32_t chargingCurrent;
	lu_int32_t chargingCurrentScaled;

	static lu_uint32_t lastCheckTime;
	static lu_bool_t checkInProgress=LU_FALSE;
	static lu_bool_t firstTime=LU_TRUE;

	/* Get current time */
	time = STGetTime();
	elapsedTime = STElapsedTime( lastCheckTime , time);

	/* Get charging current */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE, &chargingCurrent);
	chargingCurrentScaled = chargingCurrent;

	if( firstTime == LU_TRUE )
	{
		firstTime 		= LU_FALSE;
		retVal 			= LU_FALSE;
		checkInProgress = LU_FALSE;
		lastCheckTime 	= time;
	}
	/* If enough time has passed since the last check then turn on charger */
	else if( elapsedTime > (BATTERY_STATUS_CHECK_PERIOD * MSEC) )
	{
		/*Record the start time of the test */
		lastCheckTime = time;

		/* Apply the temperature compensation to the charger voltage */
		GetChargerVoltsReq();

		/*
		 * Turn on charger and wait for charger voltage to rise before checking current
		 */

		/* Set Initial Value for Battery charger voltage */
		chargerVoltLimitOutput = batteryStatePtr->BatteryCorrectedChargingVolts;
		sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
		batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;

		/* Turn charger on and connect to battery */
		StartChargingBattery();
		ConnectChgToBatt();

		/* Set flag to indicate the test has started */
		checkInProgress = LU_TRUE;

		retVal = LU_FALSE;
	}
	else if( checkInProgress == LU_TRUE )
	{
		/*
		 * Check for sufficient time passed for the charger voltage to rise
		 * or for charging current detected
		 */
		if( elapsedTime > ( BATTERY_STATUS_CHECK_CHARGE_TIME * MSEC ) )
		{
			/*
			 * Check for charging current -
			 * No charging current indicates no battery is connected
			 */
			if(	chargingCurrentScaled > BATT_MINIMUM_PRESENT_CURRENT )
			{
				/* Set virtual point to indicate battery present */
				sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 0);
				/* Set virtual point to indicate no battery fault */
				sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 0);
				retVal =  LU_FALSE;
			}
			else
			{
				/* The battery is not connected so turn charger off */
				StopChargingBattery();

				/* Set virtual point to indicate battery disconnected */
				sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 1);
				sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);

				retVal = LU_TRUE;

				// State: Battery disconnected
			}

			/* Set flag to indicate the test has finished */
			checkInProgress = LU_FALSE;
//			firstTime 		= LU_TRUE;
		}
		else
		{
			retVal = LU_FALSE;
		}
	}
	else
	{
		/*
		 * Check for battery present. Return true if present else return false.
		 * Note: Only valid when charger not connected to the battery
		 * otherwise will only read the charger voltage
		 */

		/* Current battery voltage */
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);

		batteryVoltsScaled = batteryVolts;
		if(	batteryVoltsScaled > BATTERY_PRESENT )
		{
			/* Set virtual point to indicate battery present */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 0);
			/* Set virtual point to indicate no battery fault */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 0);
			retVal =  LU_TRUE;
		}
		else
		{
			/* Set virtual point to indicate battery disconnected */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 1);
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);

			retVal = LU_FALSE;
		}
	}

	return retVal;
}

lu_uint32_t batteryDisconnected(void)
{
	SB_ERROR sbError;
	lu_uint32_t retVal;
	lu_int32_t chargingCurrent,chargerVolts,batteryVolts;
	lu_int32_t chargingCurrentScaled;
	lu_uint32_t time,elapsedTime;
	static lu_uint32_t lastCheckTime;
	static lu_bool_t checkInProgress=LU_FALSE,firstTime=LU_TRUE;

	/*
	 * Check for battery disconnected. Return true if disconnected else return false.
	 * Note Only valid when charger is on otherwise there will be no charging current.
	 */

	/* Get current time */
	time = STGetTime();
	elapsedTime = STElapsedTime( lastCheckTime , time);

	/* Get charging current */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE, &chargingCurrent);
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_CHARGER_V_SENSE, &chargerVolts);
	sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_V_SENSE, &batteryVolts);

	batteryStatePtr->MeasuredChargingVolts = chargerVolts;

	if( firstTime == LU_TRUE )
	{
		firstTime 		= LU_FALSE;
		retVal 			= LU_FALSE;
		checkInProgress = LU_FALSE;
		lastCheckTime 	= time;
	}
	/* If enough time has passed since the last check then check the current */
	else if( elapsedTime > (10 * MSEC) )
	{
		lastCheckTime 	= time;

		chargingCurrentScaled = chargingCurrent;
		if(	chargingCurrentScaled > BATT_MINIMUM_PRESENT_CURRENT )
		{
			/* Set virtual point to indicate battery present */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 0);
			/* Set virtual point to indicate no battery fault */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 0);
			retVal =  LU_FALSE;
		}
		else
		{
			/* The battery is not connected so turn charger off */
			StopChargingBattery();

			/* Set virtual point to indicate battery disconnected */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 1);
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);
			retVal = LU_TRUE;

			// State: Battery disconnected

			/* Set virtual point to indicate charge cycle not running */
			sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);
			firstTime = LU_TRUE;
		}
	}
	else
	{
		retVal =  LU_FALSE;
	}

#ifdef DEBUG_OUTPUT
	chargepos;
	_printf("\tSet Charging Volts [%8d]"    ,chargerVoltLimitOutput);
	_printf("\tActual Charging Volts [%8d]",chargerVolts );
	_printf("\t\t   Battery Volts [%8d]\n\r"  ,batteryVolts );
	cll;
	if(topUpState != LU_TRUE)
	{
		_printf("\t       Charge time [%8d]",ChargeTime);
	}
	else
	{
		_printf("\t       Top up time [%8d]",TopUpTime);
	}

	_printf("\t    Charge start time [%8d]",ChargeStartTime);
#endif
	return retVal;
}

lu_uint32_t batteryHealth(void)
{
	SB_ERROR sbError;
	lu_uint32_t retVal;
	lu_int32_t batteryVolts;
	lu_int32_t batteryVoltsScaled;
	lu_uint32_t time = 0,elapsedTimeLow = 0,elapsedTimeShut = 0;
	static lu_uint8_t	batteryLowFlag = LU_FALSE,batteryLowOkFlag = LU_FALSE;
	static lu_uint32_t	batteryLowStartTime = 0,batteryLowOkStartTime = 0;

	static lu_uint8_t	batteryShutFlag = LU_FALSE,batteryShutOkFlag ;
	static lu_uint32_t	batteryShutStartTime = 0,batteryShutOkStartTime = 0;


	/* Current battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
	batteryVoltsScaled = batteryVolts;
	batteryStatePtr->BatteryVoltage = batteryVoltsScaled;

	#ifdef DEBUG_OUTPUT
		battvolts;
		_printf("Battery volts [%05d]",batteryVoltsScaled);
	#endif

	/* Get current time */
	time = STGetTime();

	/*
	 * If an operation is in progress then don't check the battery health
	 */
	if( motorSupplyOn() == FALSE )
	{

		/*
		 * Check for battery voltage having limited operation time left
		 */
		if( battState == BATT_HEALTHY )
		{
			if( batteryVoltsScaled < (batteryParamsPtr->BatteryPackShutdownLevelVolts) )
			{
				/*
				 * If the flag is already set then check the elapsed time
				 * otherwise set the flag and start the timer
				 */
				if( batteryShutFlag == LU_FALSE )
				{
					batteryShutFlag = LU_TRUE;
					batteryShutOkFlag  = LU_FALSE;
					batteryShutStartTime = time;
					#ifdef DEBUG_OUTPUT
					messagepos;
					_printf("Battery shutdown warning - Timer started ");
					#endif
				}
				else
				{
					/* Check that the battery low period has elapsed */
					elapsedTimeShut = STElapsedTime( batteryShutStartTime , time );
					if( elapsedTimeShut > (BATT_SHUT_LEVEL_PERIOD * MSEC ) )
					{
						/* Set Virtual channel to indicate low battery voltage */

						// State: Battery low shutdown soon
						sbError = IOManagerSetValue(IO_ID_BT_SHUTDOWN_APPROACHING, 1);

						#ifdef DEBUG_OUTPUT
						messagepos;
						_printf("Warning - Battery not healthy imminent - Threshold [%05d]",(batteryParamsPtr->BatteryPackShutdownLevelVolts));
						#endif
						battState = BATT_WARNING;
					}
					else
					{
						#ifdef DEBUG_OUTPUT
						messagepos;
						_printf("Battery shutdown warning - Timer [%d] ", elapsedTimeShut / MSEC);
						#endif
					}
				}
			}
			else
			{
				/* Reset the flag and start time */
				batteryShutFlag = LU_FALSE;
				batteryShutStartTime = 0;
			}
		}
		else if( battState != BATT_UNHEALTHY )
		{
			if( batteryVoltsScaled > (batteryParamsPtr->BatteryPackShutdownLevelVolts + BATT_WARNING_HYSTERESIS) )
			{
				if( batteryShutOkFlag == LU_FALSE )
				{
					batteryShutFlag = LU_FALSE;
					batteryShutOkFlag  = LU_TRUE;
					batteryShutOkStartTime = time;
					#ifdef DEBUG_OUTPUT
					messagepos;
					_printf("Battery no imminent shutdown - Timer started ");
					#endif
				}
				else
				{
					/* Check that the battery okay period has elapsed */
					elapsedTimeShut = STElapsedTime( batteryShutOkStartTime , time );
					if( elapsedTimeShut > (BATT_SHUT_LEVEL_PERIOD * MSEC ) )
					{
						/* Reset Virtual channel to indicate battery voltage okay*/
						sbError = IOManagerSetValue(IO_ID_BT_SHUTDOWN_APPROACHING, LU_FALSE);

						// State: Battery good
						/* Reset the flag and start time */
						batteryShutOkFlag = LU_FALSE;
						batteryShutFlag = LU_FALSE;
						batteryShutOkStartTime = 0;

						battState = BATT_HEALTHY;
						#ifdef DEBUG_OUTPUT
						messagepos;
						_printf("Battery Healthy");
						#endif
					}
					else
					{
							#ifdef DEBUG_OUTPUT
							messagepos;
							_printf("Battery no imminent shutdown - Timer [%d] ", elapsedTimeShut / MSEC);
							#endif
					}
				}
			}
			else
			{
				/* Reset the flag and start time */
				batteryShutOkFlag = LU_FALSE;
				batteryShutOkStartTime = 0;
			}
		}

		/*
		 * Check for battery voltage being less than value required for operations.
		 * Must be less for at least the specified period
		 */
		if( battState != BATT_UNHEALTHY )
		{
			if( batteryVoltsScaled < batteryParamsPtr->BatteryLowLevel  )
			{
				/*
				 * If the flag is already set then check the elapsed time
				 * otherwise set the flag and start the timer
				 */
				if( batteryLowFlag == LU_FALSE )
				{
					batteryLowFlag = LU_TRUE;
					batteryLowOkFlag  = LU_FALSE;
					batteryLowStartTime = time;
					#ifdef DEBUG_OUTPUT
					messagepos;
					_printf("Battery low - Timer started ");
					#endif
				}
				else
				{
					/* Check that the battery low period has elapsed */
					elapsedTimeLow = STElapsedTime( batteryLowStartTime , time );
					if( elapsedTimeLow > (BATT_LOW_LEVEL_PERIOD * MSEC ) )
					{
							/* Set Virtual channel to indicate low battery voltage */
							sbError = IOManagerSetValue(IO_ID_VIRT_BT_LOW, 1);
							// State: Battery low
							#ifdef DEBUG_OUTPUT
							messagepos;
							_printf("Battery not healthy - Healthy threshold [%d]",batteryParamsPtr->BatteryLowLevel);
							#endif
							battState = BATT_UNHEALTHY;
					}
					else
					{
							#ifdef DEBUG_OUTPUT
							messagepos;
							_printf("Battery low - Timer [%d] ", elapsedTimeLow / MSEC);
							#endif
					}
				}
			}
			else
			{
				/* Reset the flag and start time */
				batteryLowFlag = LU_FALSE;
				batteryLowStartTime = 0;
			}
		}
		else	/* Battery not healthy */
		{
			if(batteryVoltsScaled > (batteryParamsPtr->BatteryLowLevel + BATT_LOW_LEVEL_HYSTERESIS) )
			{
				if( batteryLowOkFlag == LU_FALSE )
				{
					batteryLowFlag = LU_FALSE;
					batteryLowOkFlag  = LU_TRUE;
					batteryLowOkStartTime = time;
					#ifdef DEBUG_OUTPUT
					messagepos;
					_printf("Battery not low - Timer started ");
					#endif
				}
				else
				{
					/* Check that the battery okay period has elapsed */
					elapsedTimeLow = STElapsedTime( batteryLowOkStartTime , time );
					if( elapsedTimeLow > (BATT_LOW_LEVEL_PERIOD * MSEC ) )
					{
						/* Reset Virtual channel to indicate battery voltage okay*/
						sbError = IOManagerSetValue(IO_ID_VIRT_BT_LOW, 0);
						// State: Battery good
						/* Reset the flag and start time */
						batteryLowOkFlag = LU_FALSE;
						batteryLowFlag = LU_FALSE;
						batteryLowOkStartTime = 0;

						battState = BATT_WARNING;
						#ifdef DEBUG_OUTPUT
						messagepos;
						_printf("Battery Warning");
						#endif
					}
					else
					{
							#ifdef DEBUG_OUTPUT
							messagepos;
							_printf("Battery not low - Timer [%d] ", elapsedTimeLow / MSEC);
							#endif
					}
				}
			}
			else
			{
				/* Reset the flag and start time */
				batteryLowOkFlag = LU_FALSE;
				batteryLowOkStartTime = 0;
			}
		}

	}	/* End of if motor supply not on */
	else
	{
		/* An operation is in progress so reset the flags and start times */
		batteryLowOkFlag = LU_FALSE;
		batteryLowOkStartTime = 0;
		batteryLowFlag = LU_FALSE;
		batteryLowStartTime = 0;
		elapsedTimeLow = 0;

		batteryShutFlag = LU_FALSE;
		batteryShutStartTime = 0;
		batteryShutOkFlag = LU_FALSE;
		batteryShutOkStartTime = 0;
		elapsedTimeShut = 0;
	}

	retVal =  LU_FALSE;
	return retVal;
}

lu_uint32_t batteryPresent(void)
{
	SB_ERROR sbError;
	lu_uint32_t retVal;
	lu_int32_t batteryVolts;
	lu_int32_t batteryVoltsScaled;

	/*
	 * Check for battery present. Return true if present else return false.
	 * Note Only valid when charger not on otherwise will only read the charger voltage
	 */

	/* Current battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);

	batteryVoltsScaled = batteryVolts;
	if(	batteryVoltsScaled > BATTERY_PRESENT )
	{
		/* Set virtual point to indicate battery present */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 0);
		/* Set virtual point to indicate no battery fault */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 0);
		retVal =  LU_TRUE;
	}
	else
	{
		/* Set virtual point to indicate battery disconnected */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_DISCONNECTED, 1);
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, 0);

		retVal = LU_FALSE;

		// State: Battery disconnected

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		/*
		 * If the battery test was running when the LV was lost then
		 * stop the test and set the reason.
		 */
		if( batteryTestInProgress  == LU_TRUE )
		{
			/* Disconnect dummy load from battery */
			sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

			/* Reset the test in progress flag */
			testStatePtr->BatteryTestInProgress = LU_FALSE;
			batteryTestInProgress = LU_FALSE;
			testStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_BATT_DISCONNECTED;
			testStatePtr->BatteryTestSuspended = LU_FALSE;

			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_IN_PROGRESS, 0);
			/* The test is deemed to have failed if the battery is seen to be disconnected */
			sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL,1);
			batteryDataNVRamBlkPtr->batteryTestFailed = LU_TRUE;
			updateDataNVRAM = LU_TRUE;

			/* Reset first time flag */
			battTestFirstTime = LU_TRUE;

			/* Set previous charging voltage */
			sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
			batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;

			#ifdef DEBUG_BATT_TEST_OUTPUT
			batttestpos;
			_printf("Battery Test Stopped BATT_TEST_STOP_BATT_DISCONNECTED\n\r");
			#endif
		}
	}

	return retVal;
}

lu_uint32_t batteryOverTemperature(void)
{
	SB_ERROR sbError;

	lu_int32_t 		batTemperature = 0;

	batTemperature	= batteryStatePtr->BatteryTemp;

	/* Check that battery temperature is okay for charging */
	if( batTemperature > batteryParamsPtr->BatteryMaxTemp)
	{
		if( batteryParamsPtr->BatteryType == !BATT_TYPE_NI_MH)
		{
			/* Turn charger off */
			StopChargingBattery();
		}

		/* Set virtual point to indicate battery over temperature */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_OVER_TEMPERATURE, 1);

		/* Set virtual point to indicate charge cycle not running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 0);

		return LU_TRUE;
	}
	else
	{
		return LU_FALSE;
	}
}

lu_uint32_t batteryUnderTemperature(void)
{
	SB_ERROR sbError;
	lu_int32_t		battTempHysterisis;
	lu_int32_t 		batTemperature = 0;

	batTemperature	= batteryStatePtr->BatteryTemp;


	if( batteryParamsPtr->BatteryType == BATT_TYPE_LEAD_ACID)
	{
		battTempHysterisis = LEAD_ACID_BATT_TEMP_HYSTERISIS;
	}
	else if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		battTempHysterisis = NIMH_BATT_TEMP_HYSTERISIS;
	}
	else
	{
		battTempHysterisis = UKNOWN_BATT_TEMP_HYSTERISIS;
	}

	/*
	 * Check the battery temperature against the maximum temperature for that battery
	 */

	if( batTemperature  < batteryParamsPtr->BatteryMaxTemp - battTempHysterisis )
	{
		/* Set virtual point to indicate battery not over temperature */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_OVER_TEMPERATURE, 0);

		/* Set virtual point to indicate charge cycle running */
		sbError = IOManagerSetValue( IO_ID_VIRT_CH_SYSTEM_RUNNING, 1);

		return LU_TRUE;
	}
	else
	{
		return LU_FALSE;
	}

	/* Return true if under temperature */
	return LU_TRUE;
}

lu_uint32_t batteryChargeRemaining(void)
{
	SB_ERROR sbError;

	lu_int32_t batteryVolts, batteryVoltsScaled,time;
	lu_uint32_t retVal,elapsedTime;
	static lu_bool_t shutdownRequired = LU_FALSE;

	/*
	 * Check charge remaining in battery and make decision on whether to shutdown.
	 * (Only relevant when running on battery)
	 */

	time = STGetTime();
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
	batteryVoltsScaled = batteryVolts;

	/*
	 * If an operation is in progress then don't check the battery state
	 */
	if( motorSupplyOn() == FALSE )
	{
		/*
		 * Check for battery voltage below the deep discharge level
		 */
		if( batteryVoltsScaled < batteryParamsPtr->BatteryPackDeepDischargeVolts )
		{
			/*
			 * If already below the level then check for time expired
			 */
			if( shutdownRequired == LU_FALSE )
			{
				/*
				 * voltage has reached limit so start delay time until shutdown
				 * Set digital point to indicate to the MCM that a shutdown is about to happen
				 */
				sbError = IOManagerSetValue( IO_ID_VIRT_BT_CH_SHUTDOWN, LU_TRUE);
				retVal =  LU_FALSE;
				/* Set flag and initialise time period */
				shutdownRequired = LU_TRUE;
				ShutdownStartTime = time;
				#ifdef DEBUG_OUTPUT
				messagepos1;
				_printf("Warning - Battery deep discharge relay timer started - Threshold [%05d]",(batteryParamsPtr->BatteryPackShutdownLevelVolts));
				#endif
			}
			else
			{
				elapsedTime = STElapsedTime( ShutdownStartTime , time );
				if(elapsedTime > (SHUTDOWN_DELAY * MSEC))
				{
					/* Period expired so go into shutdown state */
					retVal = LU_TRUE;
					disconnectStartTime = time;
					#ifdef DEBUG_OUTPUT
					messagepos1;
					_printf("Warning - Battery deep discharge relay shutdown time expired ");
					#endif
				}
				else
				{
					retVal = LU_FALSE;
					#ifdef DEBUG_OUTPUT
					messagepos1;
					_printf("Warning - Battery deep discharge relay shutdown time [%d] ",elapsedTime/MSEC);
					#endif
				}
			}
		}
		else
		{
			/* Reset the indication of shutdown */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_CH_SHUTDOWN, LU_FALSE);
			/* Voltage okay so return false to continue on battery */
			shutdownRequired = LU_FALSE;
			ShutdownStartTime = 0;
			retVal =  LU_FALSE;
		}
	}
	else
	{
		/* Because an operation is in progress reset the indication of shutdown */
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_CH_SHUTDOWN, LU_FALSE);
		/* Voltage okay so return false to continue on battery */
		shutdownRequired = LU_FALSE;
		ShutdownStartTime = 0;
		retVal =  LU_FALSE;
	}
	return retVal;
}

lu_uint32_t motorSupplyOn(void)
{
	SB_ERROR sbError;
	lu_int32_t motorSupplyOn;
	lu_int32_t returnVal;

	sbError = IOManagerGetValue(IO_ID_VIRT_MS_RELAY_ON,&motorSupplyOn);

    if( motorSupplyOn != LU_FALSE )
    {
    	returnVal = LU_TRUE;
    }
    else
    {
    	returnVal = LU_FALSE;
    }

    return returnVal;
}

lu_uint32_t statusCheckDue(void)
{
	lu_int32_t retVal;
	lu_uint32_t time,elapsedTime;
	static lu_uint32_t lastCheckTime;
	static lu_bool_t firstTime = LU_TRUE;

	/* Get current time */
	time = STGetTime();
	elapsedTime = STElapsedTime( lastCheckTime , time);

	if( firstTime == LU_TRUE )
	{
		/* First time called so reset parameters */
		lastCheckTime = time;
		retVal = LU_FALSE;
		firstTime = LU_FALSE;
	}
	else if( elapsedTime > (STATUS_CHECK_INTERVAL * MSEC) )
	{
		/* Time for a status check so return true and reset time */
		retVal = LU_TRUE;
		lastCheckTime = time;
	}
	else
	{
		retVal = LU_FALSE;
	}

    return retVal;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialises the battery charger
 *
 *   Turns off the battery charger
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR ChargerInit(void)
{
	SB_ERROR RetVal;

	/*!
	 * Set initial state of Charger Enable pin to off. Only turn on when required
	 */
	RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 0);
	chargerStatePtr->ChargerEnabled = FALSE;

	// State: Battery good

	/* Apply the temperature compensation to the charger voltage */
	GetChargerVoltsReq();

	/* Set Initial Value for Battery charger voltage */
	chargerVoltLimitOutput = batteryStatePtr->BatteryCorrectedChargingVolts;
	RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, chargerVoltLimitOutput);
	batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;

	/* Reset virtual point that indicates battery test failure  */
	RetVal = IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL, batteryDataNVRamBlkPtr->batteryTestFailed);

	/*
	 * Connect the battery
	 */
	RetVal = IOManagerSetPulse(IO_ID_BAT_CONNECT, DISCONNECT_PULSE_MS);
	batteryStatePtr->BatteryConnected 			= LU_TRUE;
	batteryStatePtr->BatteryDisConnected 		= LU_FALSE;

	/* Set virtual point to indicate battery is connected */
	RetVal = IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, 0);

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Turns on the battery charger
 *
 *   Turns on the battery charger
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR StartChargingBattery(void)
{
	SB_ERROR sbError;

	/* Reset virtual point that indicates battery fault */
	sbError = IOManagerSetValue( IO_ID_VIRT_BT_FAULT, 0);

	/* Turn charger on */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 1);
	chargerStatePtr->ChargerEnabled = TRUE;

	/* Turn on charging green LED */
	sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 1);

	/* Set virtual point to indicate battery is charging */
	sbError = IOManagerSetValue( IO_ID_VIRT_CH_ON, 1);

	return sbError;
}

/*!
 ******************************************************************************
 *   \brief Turns off the battery charger
 *
 *   Turns off the battery charger
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR StopChargingBattery(void)
{
	SB_ERROR sbError;

	/* Turn charger off */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 0);

	/* Turn off battery test*/
	sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

	chargerStatePtr->ChargerEnabled = FALSE;

	/* Disconnect the charger from the battery */
	sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, 0);

	/* Turn off charging green LED */
	sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 0);

	ChargeTime = STElapsedTime( ChargeStartTime, STGetTime() ) / MSEC;

	/* Set virtual point to indicate battery is not charging */
	sbError = IOManagerSetValue( IO_ID_VIRT_CH_ON, LU_FALSE);

	/* Reset charger current */
	IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET, batteryParamsPtr->BatteryChargeRate);

	ClearDebugDisplay();


	return sbError;
}

/*!
 ******************************************************************************
 *   \brief Suspends the battery charger
 *
 *   Turns off the battery charger while the battery is over temperature
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR SuspendChargingBattery(void)
{
	SB_ERROR sbError;

	/* Turn charger off */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 0);

	/* Turn off battery test*/
	sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

	chargerStatePtr->ChargerEnabled = FALSE;

	/* Disconnect the charger from the battery */
	sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, 0);

	/* Flash charging green LED */
	sbError = IOManagerSetFlash(IO_ID_LED_CTRL_BAT_CHRG_GREEN, OVER_TEMP_FLASH_TIME);

	/* Set virtual point to indicate battery is not charging */
	sbError = IOManagerSetValue( IO_ID_VIRT_CH_ON, LU_FALSE);

	/* Set the charging has been suspended flag */
	chargerStatePtr->ChargerSuspendOccurred = LU_TRUE;

	/* Save the time at which the suspend occurred */
	chargerStatePtr->ChargerSuspendStart =  STGetTime();

	ClearDebugDisplay();


	return sbError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Records the charging time when the charging cycle stops
 *
 *   \param
 *
 *   None
 *
 *   \return
 *
 *   None
 *
 ******************************************************************************
 */
void RecordChargeTime(void)
{
	/* Record charging cycles if long enough */
	if( ChargeTime > BATT_MIN_CHARGE_TIME )
	{
		/* Record time at which charging finishes only if a full charge has occurred */
		ChargeStopTime = STGetTime();
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Records the charging time when the charging cycle stops
 *
 *   \param
 *
 *   None
 *
 *   \return
 *
 *   None
 *
 ******************************************************************************
 */
void RecordChargeCycle(void)
{
	SB_ERROR sbError;

	if(LVFailedFlag)
	{
		/* Increment the number of charging cycles the battery has had */
		batteryDataNVRamBlkPtr->chargeCycles ++;
		sbError = IOManagerSetValue( IO_ID_VIRT_BT_CHARGE_CYCLES           , batteryDataNVRamBlkPtr->chargeCycles);
		updateDataNVRAM = LU_TRUE;
		LVFailedFlag = LU_FALSE;
		#ifdef DEBUG_OUTPUT
		messagepos;
		_printf("Charge cycles incremented [%d]\n\n\r",batteryDataNVRamBlkPtr->chargeCycles );
		#endif
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Temperature compensate voltages
 *
 *   \param
 *
 *   None
 *
 *   \return
 *
 *   None
 *
 ******************************************************************************
 */
lu_int32_t GetTempCompVolts(lu_int32_t volts)
{
	lu_int32_t compVolts;
	lu_int32_t batTemp,comp;

	GetTemperature();
	batTemp = batteryStatePtr->BatteryTemp;

	switch( batteryParamsPtr->BatteryType )
	{
	case BATT_TYPE_LEAD_ACID:
		comp = (( QUOTED_CHARGING_TEMPERATURE - batTemp ) * TEMP_COMP_LEAD_ACID);
		compVolts = volts + comp;
		break;

	case BATT_TYPE_NI_MH:
		comp = (( QUOTED_CHARGING_TEMPERATURE - batTemp ) * TEMP_COMP_NIMH);
		compVolts = volts + comp;
		break;

	case BATT_TYPE_UNKNOWN:
		comp = (( QUOTED_CHARGING_TEMPERATURE - batTemp ) * TEMP_COMP_UNKNOWN);
		compVolts = volts + comp;
		break;

	default:
		compVolts = volts;
		break;
	}

	#ifdef DEBUG_BATT_TEST_OUTPUT
	tempcomppos;
	_printf("Charging Voltage Temp Comp [%5d] Temperature [%5d] Charging Voltage [%5d] Compensated Charging Voltage [%5d]    \r",comp,batTemp,volts,compVolts );
	#endif

	return compVolts;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Reads the temperature sensors on the PSM and battery
 *
 *   \param
 *
 *   None
 *
 *   \return
 *
 *   None
 *
 ******************************************************************************
 */
void GetChargerVoltsReq(void)
{
	/*
	 * Get the temperature of the batteries in order to apply the
	 * temperature compensation to the charging voltage
	 */
	batteryStatePtr->BatteryCorrectedChargingVolts = GetTempCompVolts(batteryParamsPtr->BatteryChargeVolts);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Reads the temperature sensors on the PSM and battery
 *
 *   \param
 *
 *   None
 *
 *   \return
 *
 *   None
 *
 ******************************************************************************
 */
SB_ERROR GetTemperature(void)
{
	SB_ERROR sbError;
	lu_int32_t				batteryTempRaw = 0;
	lu_int32_t 				batTemp = 0;
	lu_int32_t 				psuTemp = 0;
	lu_int32_t 				rtuTemp = 0;
	lu_int32_t				total = 0;
	lu_int32_t				rtu_total = 0;
	lu_bool_t				rtuTempValid = 0;
	static lu_int32_t		average[BATT_AVERAGE_DELTA_T_COUNT];
	static uint8_t			i = 0, j;
	static uint8_t			k = 0, l;
	static uint8_t			battteryAverageTempValid = LU_FALSE;
	static lu_int32_t		rtuAverage[RTU_AVERAGE_TEMP_COUNT];
	static uint8_t			rtuAverageTempValid = LU_FALSE;

	/*
	 * Get the current external temperature if the sensor is present
	 * otherwise set to default value
	 */
	sbError = IOManagerGetValue(IO_ID_EXTERNAL_TEMPERATURE, &rtuTemp);
	if( sbError != SB_ERROR_NONE)
	{
		/* Use default value if there is an error reading the sensor */
		rtuTemp =  RTU_DEFAULT_TEMP;
		rtuTempValid = LU_FALSE;
		batteryStatePtr->RTUTemp =  rtuTemp;
		batteryStatePtr->RtuTempValid = rtuTempValid;
	}
	else
	{
		rtuTemp *= RTU_TEMP_RES;
		rtuTempValid = LU_TRUE;
		batteryStatePtr->RTUTemp =  rtuTemp;
		batteryStatePtr->RtuTempValid = rtuTempValid;

		/* Average the rtu temperature */
		rtuAverage[k++] = rtuTemp;
		total =0;
		for( l=0; l<RTU_AVERAGE_TEMP_COUNT; l++)
		{
			rtu_total += average[j];
		}
		if(k >= RTU_AVERAGE_TEMP_COUNT)
		{
			rtuAverageTempValid = LU_TRUE;
			k = 0;
		}

		if( battteryAverageTempValid != LU_TRUE )
		{
			batteryStatePtr->RTUTempAverage = rtu_total / RTU_AVERAGE_TEMP_COUNT;
		}
		else
		{
			batteryStatePtr->RTUTempAverage = rtuTemp;
		}

	}

	/* Get the current temperature of the PSM */
	sbError = IOManagerGetValue(IO_ID_PSU_TEMP, &psuTemp);
	psuTemp /= TEMP_SENSOR_RESOLUTION;
	if( sbError != SB_ERROR_NONE)
	{
		/* Use default value if there is an error reading the sensor */
		psuTemp =  PSU_DEFAULT_TEMP;
		batteryStatePtr->RTUTemp =  rtuTemp;
	}

	if(rtuTempValid == LU_FALSE)
	{
		rtuTemp = psuTemp;
		batteryStatePtr->RTUTemp = psuTemp;
	}


	/* Get the current temperature of the battery pack */
	sbError = IOManagerGetValue(IO_ID_BAT_TEMP, &batTemp);
	batteryTempRaw =  batTemp;
	batTemp /= TEMP_SENSOR_RESOLUTION;
	if( sbError != SB_ERROR_NONE)
	{
		/* Use default value if there is an error reading the sensor */
		batTemp = psuTemp;

		/* Indicate that the temperature sensor isn't fitted or isn't working */
		batteryStatePtr->BatteryTempValid = LU_FALSE;
		batteryStatePtr->BatteryTemp = batTemp;

		/* Set virtual points to indicate no communication with battery NVRAM */
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_COMMS_FAIL, LU_TRUE);
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEMP_FAIL, LU_TRUE);
	}
	else
	{
		batteryStatePtr->BatteryTemp = batTemp;
		batteryStatePtr->BatteryTempValid = LU_TRUE;

		/* Average the battery temperature */
		average[i++] = batteryTempRaw;
		total =0;
		for( j=0; j<BATT_AVERAGE_DELTA_T_COUNT; j++)
		{
			total += average[j];
		}
		if(i >= BATT_AVERAGE_DELTA_T_COUNT)
		{
			battteryAverageTempValid = LU_TRUE;
			i = 0;
		}

		if( battteryAverageTempValid != LU_TRUE )
		{
			batteryStatePtr->BatteryTempAverage = total / BATT_AVERAGE_DELTA_T_COUNT;
		}
		else
		{
			batteryStatePtr->BatteryTempAverage = batteryTempRaw;
		}

		/* Set virtual points to indicate communication okay with battery NVRAM */
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_COMMS_FAIL, LU_FALSE);
		sbError = IOManagerSetValue(IO_ID_VIRT_BT_TEMP_FAIL, LU_FALSE);
	}

	return sbError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint32_t chargerCurrentCheck(void)
{
	SB_ERROR sbError;
	lu_int32_t actualChargerCurrent;
	lu_bool_t retVal = LU_FALSE;
	lu_int32_t time, elapsedTime;
	static lu_int32_t maxChargerCurrent;
	static lu_bool_t firstTimeFlag = LU_TRUE;
	static lu_int32_t lastCheckTime = 0;

	time = STGetTime();


	/*****************************************************
	 * Don't perform a current check for NiMH batteries
	 *****************************************************/
    if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
    {
    	retVal = LU_FALSE;
    	return retVal;
    }

	/******************************************************************************************
	 * The charging current can be controlled via a digi-pot on newer charging boards
	 * ***************************************************************************************/
    if(	currentControlEnabled == LU_TRUE )
    {
    	if(firstTimeFlag == LU_TRUE)
    	{
    		/* Initialise test parameters */
    		lastCheckTime = time;
    		firstTimeFlag = LU_FALSE;
    		retVal = LU_FALSE;
    	}
    	else
    	{
			elapsedTime = STElapsedTime( lastCheckTime , time );
			if( elapsedTime > 10000 )
			{
				/* Reset timer */
				lastCheckTime = time;

				/* Get the maximum charger voltage allowed at the current temperature */
				GetChargerVoltsReq();
				/* Set the temperature corrected charging voltage */
				chargerVoltLimitOutput = batteryStatePtr->BatteryCorrectedChargingVolts;
				/* Set the charging voltage output of the charger */
				sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
			}
    	}
    	retVal = LU_FALSE;
    	return retVal;
    }

	/******************************************************************************************
	 * The charging current can not be controlled via a digi-pot on older charging boards
	 * The lower the charging voltage the lower the current.
	 * As the charger is on and connected at this point the charger and battery voltages will
	 * be the same thing.
	 ******************************************************************************************/

	/*
	 * Get the maximum allowable charging current from the battery parameters
	 * If the power supply power limit is less than the parameter then
	 * use the power supply power limit
	 */
	if( batteryParamsPtr->BatteryChargeRate > chargerCurrentLimit)
	{
		maxChargerCurrent = chargerCurrentLimit;
	}
	else
	{
		maxChargerCurrent = batteryParamsPtr->BatteryChargeRate;
	}

	/* Get the maximum charger voltage allowed at the current temperature */
	GetChargerVoltsReq();

	/* get the actual charging current */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE,&actualChargerCurrent);

	/*
	 * Check for first time call.
	 * If it is the first call then set the current limit to initial value.
	 */
	if(firstTimeFlag == LU_TRUE)
	{
		/* Initialise test parameters */
		lastCheckTime = time;
		firstTimeFlag = LU_FALSE;
		retVal = LU_FALSE;
	}
	else
	{
		elapsedTime = STElapsedTime( lastCheckTime , time );
		if( elapsedTime > CHARGER_CURRENT_CHECK_PERIOD )
		{
			/* Reset timer */
			lastCheckTime = time;

			/*
			 * Compare the actual charging current with the maximum allowable charging current
			 * If the actual current is greater then reduce the charging voltage.
			 * If the actual current is less than the max and the voltage is less than
			 * the normal charging voltage then increase the charging voltage.
			 */
			if( actualChargerCurrent > (batteryParamsPtr->BatteryChargeRate + CHARGER_I_TOL  ))
			{
				/*
				 * Reduce the charger voltage to decrease the charging current
				 * If the actual charging current is much larger than the required
				 * current then increase the voltage steps
				 */
				if( (actualChargerCurrent - batteryParamsPtr->BatteryChargeRate ) > 100)
				{
					chargerVoltLimitOutput -= (BATT_VOLTS_FEEDBACK * 4);
				}
				else
				{
					chargerVoltLimitOutput -= BATT_VOLTS_FEEDBACK;
				}
				/* Set the charging voltage output of the charger */
				sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
				batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;
				retVal = LU_FALSE;
			}
			else if(
						(actualChargerCurrent < ( batteryParamsPtr->BatteryChargeRate - CHARGER_I_TOL ))
						&&
						(chargerVoltLimitOutput < (batteryStatePtr->BatteryCorrectedChargingVolts - BATT_VOLTS_FEEDBACK) )
			       )
			{
				/*
				 * Increase the charger voltage to increase the charging current
				 * If the actual charging current is much smaller than the required
				 * current then increase the voltage steps
				 */
				if( (batteryParamsPtr->BatteryChargeRate - actualChargerCurrent ) > 100)
				{
					chargerVoltLimitOutput += (BATT_VOLTS_FEEDBACK * 4);
				}
				else
				{
					chargerVoltLimitOutput += BATT_VOLTS_FEEDBACK;
				}
				/* Set the charging voltage output of the charger */
				sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
				batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;
				retVal = LU_FALSE;
			}
			else if( chargerVoltLimitOutput > batteryStatePtr->BatteryCorrectedChargingVolts )
			{
				/*
				 * Charger voltage is already at the limit so set to maximum compensated for temperature
				 */
				chargerVoltLimitOutput = batteryStatePtr->BatteryCorrectedChargingVolts;
				/* Set the charging voltage output of the charger */
				sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
				batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;
				retVal = LU_FALSE;
			}
		}
	}
	#ifdef DEBUG_BATT_TEST_OUTPUT
	currentcheck;
	_printf("Charging Voltage [%5d]    \r",chargerVoltLimitOutput );
	#endif

	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_bool_t ChargerVoltsCheck(void)
{
	lu_int32_t actualChargerVolts, batteryVolts, batteryVoltsScaled;
	static lu_int32_t chargerEnabled;
	SB_ERROR sbError;
	lu_bool_t voltsSet = LU_FALSE;
	static lu_bool_t firstTime = LU_TRUE;
	static lu_bool_t lowBattery = LU_FALSE;
	static lu_bool_t correctBatteryVolts = LU_FALSE;

	/*
	 * Call the function to apply the
	 * temperature compensation to the charging voltage
	 */
	GetChargerVoltsReq();

	if(firstTime == LU_TRUE)
	{
		chargerVoltLimitOutput 	= batteryStatePtr->BatteryCorrectedChargingVolts;
		firstTime = LU_FALSE;
		lowBattery = LU_FALSE;
		chargerVoltsCal = 1.0;
		timeoutFlag = 0;
	}

	/*
	 * Check for first time call from pre-check state.
	 * If it is the first call then disconnect the charger from the battery,
	 * turn the charger on and set the timeout flag to zero.
	 * This allows the charger voltage to settle before indicating an error.
	 */
	if(timeoutFlag == 0)
	{
		#ifdef PRECHARGE_DEBUG_OUTPUT
		messagepos;
		_printf("Start charger voltage checking - Charger volts required[%d] Actual charger volts[%d]\n\r"    ,chargerVoltLimitOutput,actualChargerVolts);
		#endif

		/* Initialise low battery flag and charger voltage calibration */
		lowBattery = LU_FALSE;
		chargerVoltsCal = 1.0;
		correctBatteryVolts = LU_FALSE;

		/* Set charger voltage for normal charging */
		chargerVoltLimitOutput 	= batteryStatePtr->BatteryCorrectedChargingVolts;

		/* Reset the charger fault flag */
		sbError = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,0);
		/* Reset the charger Voltage setting fault */
		sbError = IOManagerSetValue(IO_ID_VIRT_CH_VOLTAGE_FAULT,0);


		sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal) );
		batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;

		/* Turn charger on */
		sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 1);
		chargerStatePtr->ChargerEnabled = TRUE;

		/* Turn on charging green LED */
		sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 1);

		/* Before checking the charger output volts disconnect the charger from the battery */
		sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_FALSE);
		voltsSet = LU_FALSE;

		/* Get actual charger voltage */
		sbError = IOManagerGetCalibratedUnitsValue( IO_ID_CHARGER_V_SENSE, &actualChargerVolts);
		batteryStatePtr->BatteryChargerVoltage = actualChargerVolts;

		/* Get actual battery voltage */
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
		batteryVoltsScaled = batteryVolts;
	}
	else
	{
		sbError = IOManagerGetValue(IO_ID_VIRT_CH_FAULT,&chargerEnabled);

		/* Before checking the charger output volts disconnect the charger from the battery */
		sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_FALSE);

		/*
		 * Get actual charger voltage
		 */
		sbError = IOManagerGetCalibratedUnitsValue( IO_ID_CHARGER_V_SENSE, &actualChargerVolts);
		batteryStatePtr->BatteryChargerVoltage = actualChargerVolts;

		/*
		 * Get battery voltage
		 */
		sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
		batteryVoltsScaled = batteryVolts;

		/*
		 * For older style charger boards:
		 *  If target charging voltage is more than a set limit greater than the actual battery volts then
		 * reduce the target voltage to the actual battery volts plus a fixed differential.
		 * For new charger boards:
		 * No minimum Voltage differential is required.
		 */
		if( (chargerVoltLimitOutput > (batteryVoltsScaled + BATTERY_CHARGING_DIFFERENTIAL) ) &&
				(lowBattery == LU_FALSE )	&&
				( currentControlEnabled == LU_FALSE)
		  )
		{
			chargerVoltLimitOutput = batteryVoltsScaled + BATTERY_CHARGING_DIFFERENTIAL;
			sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
			batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;
			chargerMode = CONDITIONING;
			#ifdef PRECHARGE_DEBUG_OUTPUT
			messagepos;
			_printf("Low battery voltage so charger voltage reduced to [%d]\n\r"    ,chargerVoltLimitOutput);
			#endif
			lowBattery = LU_TRUE;
		}
		else //if(chargerVoltLimitOutput > (batteryStatePtr->BatteryCorrectedChargingVolts - BATTERY_CHARGING_DIFFERENTIAL))
		{
			chargerMode = NORMAL;

			/*
			 * Compare actual value with required value.
			 */
			if(
					(actualChargerVolts <= (chargerVoltLimitOutput + CHARGER_V_TOL) )
					&&
					(actualChargerVolts >= (chargerVoltLimitOutput - CHARGER_V_TOL) )
			  )
			{
				/* Correct charging voltage so set flag */
				voltsSet = LU_TRUE;
				timeoutFlag = 0;
				/* Reset charger fault flag */
				sbError = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,0);
				/* Reset charger Voltage setting fault */
				sbError = IOManagerSetValue(IO_ID_VIRT_CH_VOLTAGE_FAULT,0);

				#ifdef PRECHARGE_DEBUG_OUTPUT
				messagepos;
				_printf("Charger voltage is within tolerance - Required [%d] Output [%d] Cal [%1.2f]\n\r" ,chargerVoltLimitOutput, actualChargerVolts, chargerVoltsCal);
				#endif
				correctBatteryVolts = LU_TRUE;
			}
			else
			{
				/*
				 * If the voltage is incorrect this is probably due to temperature
				 * so try to adjust the digipot to achieve the correct output
				 * A maximum swing is allowed before setting an error.
				 */
//				if(actualChargerVolts < (batteryStatePtr->BatteryCorrectedChargingVolts-CHARGER_V_TOL) )
				if(actualChargerVolts < (chargerVoltLimitOutput-CHARGER_V_TOL) )
				{
					/* Increase the required voltage to compensate. */
					chargerVoltsCal += BATT_VOLTS_FEEDBACK_AMMOUNT;

					/* Apply change only if compensation limit not reached */
					if(chargerVoltsCal  <  BATT_CHG_VOLTS_COMPENSATION_MAX )
					{
						sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
						batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;
					}
					#ifdef PRECHARGE_DEBUG_OUTPUT
					messagepos;
					_printf("Charger voltage is too low - Required [%d] Charger Volts [%d] Cal [%1.2f]\n\r" ,chargerVoltLimitOutput, actualChargerVolts, chargerVoltsCal);
					#endif
				}
//				else if(actualChargerVolts > (batteryStatePtr->BatteryCorrectedChargingVolts+CHARGER_V_TOL) )
				else if(actualChargerVolts > (chargerVoltLimitOutput+CHARGER_V_TOL) )
				{
					/* Decrease the required voltage to compensate. */
					chargerVoltsCal -= BATT_VOLTS_FEEDBACK_AMMOUNT;

					/* Apply change only if compensation limit not reached */
					if(chargerVoltsCal  >  BATT_CHG_VOLTS_COMPENSATION_MIN )
					{
						sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, (lu_int32_t) (chargerVoltLimitOutput * chargerVoltsCal));
						batteryStatePtr->BatteryChargerVoltLimitOutput = chargerVoltLimitOutput;
					}
					#ifdef PRECHARGE_DEBUG_OUTPUT
					messagepos;
					_printf("Charger voltage is too high - Required [%d] Charger Volts [%d] Cal [%1.2f]\n\r" ,chargerVoltLimitOutput, actualChargerVolts, chargerVoltsCal);
					#endif
				}
				else
				{
					#ifdef PRECHARGE_DEBUG_OUTPUT
					messagepos;
					_printf("Charger voltage required [%d] Charger Volts [%d] Cal [%1.2f]\n\r" ,chargerVoltLimitOutput, actualChargerVolts), chargerVoltsCal;
					#endif
				}
			}
		}
	}

	/*
	 * If the timeout has passed then return true anyway but indicate an error.
	 */
	if( correctBatteryVolts == LU_TRUE )
	{
		#ifdef PRECHARGE_DEBUG_OUTPUT
		messagepos;
		_printf("Charger voltage is okay so connect to battery\n\r");
		#endif
		timeoutFlag = 0;
	}
	else if( timeoutFlag > BATT_CHARGER_SETTLING_TIME_MS )
	{
		/* Incorrect charging voltage but set flag anyway and set error flag */
		voltsSet = LU_TRUE;

		/* Set charger fault flag */
		sbError = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,1);
		/* Set charger Voltage setting fault */
		sbError = IOManagerSetValue(IO_ID_VIRT_CH_VOLTAGE_FAULT,1);

		#ifdef PRECHARGE_DEBUG_OUTPUT
		messagepos;
		_printf("Charger voltage incorrect so indicate Charger Fault\n\r");
		chargepos;
		_printf("   Required Charging Volts [%8d]"  ,chargerVoltLimitOutput);
		_printf("\t   Charging Volts Cal [     %1.2f]"  ,chargerVoltsCal);
		_printf("\t Measured charging Volts [%8d]\n\r" ,actualChargerVolts );
		_printf("    Measured Battery Volts [%8d]"  	,batteryVolts );
		#endif
		timeoutFlag = 0;
	}
	else
	{
		#ifdef PRECHARGE_DEBUG_OUTPUT
		chargepos;
		_printf("   Required Charging Volts [%8d]"  ,chargerVoltLimitOutput);
		_printf("\t   Charging Volts Cal [    %1.2f]"   ,chargerVoltsCal);
		_printf("\t Measured charging Volts [%8d]\n\r"   ,actualChargerVolts );
		_printf("    Measured Battery Volts [%8d]"  	,batteryVolts );

		#endif
		timeoutFlag += BATT_CH_TICK_MS;
	}

	/*
	 * If the correct voltage range has been achieved or the timeout has expired then connect
	 * the charger to the battery. Both these conditions are indicated by
	 * the timeout flag being zero.
	 */
	if( timeoutFlag == 0)
	{
		/* Connect the charger to the battery */
		sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_TRUE);
		#ifdef PRECHARGE_DEBUG_OUTPUT
		messagepos;
		_printf("Charger connected to battery\n\r");
		#endif
		lowBattery = LU_FALSE;
	}

	return voltsSet;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t BatteryVoltsCheck(void)
{
	lu_int32_t actualBatteryVolts,requiredBatteryVolts;
	SB_ERROR RetVal;
	lu_bool_t batTestFail = LU_FALSE;
	static lu_uint32_t timeoutFlag = 0;

	/*
	 * Check for first time call.
	 * If it is the first call then disconnect the charger from the battery,
	 */
	if(timeoutFlag == 0)
	{
		/*Turn the charger off */
		StopChargingBattery();

		batteryTestInProgress = LU_TRUE;
	}

	/*
	 * Get actual battery voltage
	 */
	RetVal = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_V_SENSE, &actualBatteryVolts);
	batteryStatePtr->BatteryVoltage = actualBatteryVolts;
	requiredBatteryVolts = batteryParamsPtr->BatteryThresholdVolts;

	 /*
	  * Compare actual value with required value.
	  */
	if( actualBatteryVolts < requiredBatteryVolts )
	{
		/* Incorrect battery voltage so set flag */
		batTestFail = LU_TRUE;
		timeoutFlag = 0;
		batteryTestInProgress = LU_FALSE;
		/* Set battery fault condition */
		RetVal = IOManagerSetValue(IO_ID_VIRT_BT_VOLTAGE_FAULT, LU_TRUE);
	}
	/*
	 * If the timeout has passed then return true to indicate error.
	 */
	else if(timeoutFlag++ > 10)
	{
		/* Correct battery voltage so reset battery fault flag */
		RetVal = IOManagerSetValue(IO_ID_VIRT_BT_VOLTAGE_FAULT, LU_FALSE);
		batTestFail = LU_FALSE;
		timeoutFlag = 0;
		batteryTestInProgress = LU_FALSE;
	}

	/*
	 * Calculate the battery capacity from the battery voltage
	 */
	battCapacity = CalculateBatteryCapacity();

	/*
	 * If the battery voltage is correct and the timeout has expired then reconnect the charger.
	 * Both these conditions are indicated by the timeout flag being zero.
	 */
	if( timeoutFlag == 0)
	{
		/* After checking the battery output volts connected the charger to the battery */
		RetVal = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_TRUE);
	}

	return batteryTestInProgress;
}

#ifdef IS_USED
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t BatteryFullChargeCheck(void)
{
	lu_int32_t actualBatteryVolts,requiredBatteryVolts;
	SB_ERROR sbError;
	lu_bool_t batCharged = LU_FALSE;
	static lu_int32_t startTime;
	static lu_bool_t firstTime = LU_TRUE;
	lu_int32_t time=0,elapsedTime=0;

	/*
	 * Check for first time call.
	 * If it is the first call then disconnect the charger from the battery,
	 */

	if(firstTime  == LU_TRUE)
	{
		/* Disconnect the charger from the battery */
		sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_FALSE);
		firstTime = LU_FALSE;
		time = STGetTime();
		startTime = time;
	}
	else
	{
		/* Wait for charger to disconnect and battery voltage to settle */
		elapsedTime = STElapsedTime( startTime , time );
		if( elapsedTime > (BATTERY_SETTLE_PERIOD * 1000) )
		{

			/*
			 * Get actual battery voltage
			 */
			sbError = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_V_SENSE, &actualBatteryVolts);
			batteryStatePtr->BatteryVoltage = actualBatteryVolts;
			requiredBatteryVolts = batteryParamsPtr->BatteryFullChargeVolts;

			 /*
			  * Compare actual value with fully charged value.
			  */
			if( actualBatteryVolts >= requiredBatteryVolts )
			{
				/* The battery is fully charged so stop the charging cycle */
				batCharged = LU_TRUE;
				/*Turn the charger off */
				StopChargingBattery();
				/* Record time at which charger is turned off */
				RecordChargeTime();

			}
			/*
			 * If the battery voltage is not indicating fully charged then reconnect the charger.
			 */
			else
			{
				/* After checking the battery output volts connect the charger to the battery */
				sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_TRUE);
			}
			/* Reset first time flag for next call */
			firstTime = LU_TRUE;
		}
	}

	return batCharged;
}
#endif

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void SendChargerInhibitEvent(void)
{
	ChargerContext_ChargerInhibitEvent(&chargerFsm._fsm);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void SendChargerInhibitCancelEvent(void)
{
	ChargerContext_ChargerInhibitCancelEvent(&chargerFsm._fsm);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void SendBatteryTestEvent(void)
{
	ChargerContext_BatteryTestEvent(&chargerFsm._fsm);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void SendChargerTestEvent(void)
{
	#ifdef DEBUG_OUTPUT
	canpos;
	_printf("Battery Test event sent\n\r");
	#endif
	ChargerContext_ChargerTestEvent(&chargerFsm._fsm);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t SendBatteryResetStatsEvent(void)
{
	lu_bool_t retVal = LU_FALSE;

	#ifdef DEBUG_OUTPUT
	canpos;
	_printf("Reset battery stats event sent\n\r");
	#endif
	ChargerContext_BatteryResetStatsEvent(&chargerFsm._fsm);
	retVal = LU_TRUE;

	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t ChargerTest(void)
{
	lu_int32_t actualChargerVolts, actualChargerCurrent;
	lu_int32_t setChargerVolts;
	lu_int32_t time;
	SB_ERROR RetVal;
	lu_bool_t voltsSet = LU_FALSE;

	static lu_uint32_t timeoutFlag = 0;
	static lu_uint32_t chargeTestStartTime = 0;
	static lu_uint8_t chargerTest = CH_TEST_INITIAL;

	/*
	 * Check for first time call from state machine tick state following charger test request.
	 * If it is the first call then disconnect the charger from the battery,
	 * turn the charger on and set the timeout flag to zero.
	 * This allows the charger voltage to settle before indicating an error.
	 */
	if(timeoutFlag == 0)
	{
		/* Apply the temperature compensation to the charger voltage */
		GetChargerVoltsReq();

		switch(chargerTest)
		{
		default:
		case CH_TEST_INITIAL:
			/* Pulse charger fail */
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_FAULT, 1);

			//RetVal = IOManagerSetPulse(IO_ID_VIRT_CH_FAULT, 1000);

			chargerTest++;

			return LU_FALSE;
			break;

		case CH_TEST_CHARGER_FAIL:
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_FAULT, 0);

			chargerTest = CH_TEST_INITIAL;

			/* Bypass all code below since it is broken anyway!! */
			return LU_TRUE;

			break;

		case CH_TEST_NONE:
			/* Set Initial Value for Battery charger voltage */
			RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, batteryStatePtr->BatteryCorrectedChargingVolts * chargerVoltsCal);

			/* Turn charger on */
			StartChargingBattery();
			ConnectChgToBatt();

			/* Before checking the charger output volts disconnect the charger from the battery */
			RetVal = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_FALSE);

			/* Record the charger test start time */
			/* Get current time */
			time = STGetTime();
			chargeTestStartTime = time;

			/* Start with voltage test */
			chargerTest = CH_TEST_VOLTAGE;
			break;

		case CH_TEST_VOLTAGE:
			chargerTest = CH_TEST_CURRENT;

			/* Set the current limit for the charger */
			RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET, CHARGER_CURRENT_TEST_CURRENT);

			/*
			 * Set high value for battery charger voltage to ensure a charging current
			 * even if the battery is fully charged
			 */
			RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, CHARGER_CURRENT_TEST_VOLTAGE * chargerVoltsCal);

			/* Turn charger on */
			StartChargingBattery();
			ConnectChgToBatt();
			break;
		}
	}

	switch(chargerTest)
	{
	case CH_TEST_VOLTAGE:
		/* Get required charger voltage value from parameters */
		setChargerVolts = batteryStatePtr->BatteryCorrectedChargingVolts;

		/*
		 * Get actual charger voltage
		 */
		RetVal = IOManagerGetCalibratedUnitsValue( IO_ID_CHARGER_V_SENSE, &actualChargerVolts);

		batteryStatePtr->BatteryChargerVoltage = actualChargerVolts;

		 /*
		  * Compare actual value with required value.
		  */
		if( (actualChargerVolts < (batteryStatePtr->BatteryCorrectedChargingVolts+CHARGER_V_TOL))
				&& (actualChargerVolts > (batteryStatePtr->BatteryCorrectedChargingVolts-CHARGER_V_TOL)) )
		{
			/* Correct charging voltage so set flag */
			timeoutFlag = 0;
		}
		/*
		 * If the timeout has passed then return true anyway but indicate an error.
		 */
		else if( timeoutFlag > BATT_CHARGER_SETTLING_TIME_MS )
		{
			/* Incorrect charging voltage but set flag anyway and set error flags */
			timeoutFlag = 0;
			/* Set charger fault flag */
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,1);
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_TEST_FAIL, LU_TRUE);
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_VOLTAGE_FAULT, LU_TRUE);
			#ifdef DEBUG_OUTPUT
			messagepos;
			_printf("Charger voltage incorrect so indicate Charger Fault\n\r");
			#endif
		}
		else
		{
			timeoutFlag += BATT_CH_TICK_MS;
		}

		/*
		 * Calculate the battery capacity from the battery voltage
		 */
		battCapacity = CalculateBatteryCapacity();

		/*
		 * If the correct voltage range has been achieved or the timeout has expired then turn
		 * off the charger and reconnect to the battery. Both these conditions are indicated by
		 * the timeout flag being zero.
		 */
		if( timeoutFlag == 0)
		{

			/* Set Value for Battery charger voltage */
			RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET, batteryStatePtr->BatteryCorrectedChargingVolts * chargerVoltsCal);

			/* Turn the charger off */
			StopChargingBattery();
		}
		break;

	case CH_TEST_CURRENT:
		/* Get actual charging current */
		RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE,&actualChargerCurrent);

		/*
		 * Check that there is a charging current being measured and compare the two values and check that
		 * the actual charging current is not exceeding the set value
		 */
		if( (actualChargerCurrent > BATT_MINIMUM_CHARGING_CURRENT) && (actualChargerCurrent < (CHARGER_CURRENT_TEST_CURRENT+CHARGER_I_TOL) ) )
		{
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,1);
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_TEST_FAIL, LU_TRUE);
			#ifdef DEBUG_OUTPUT
			messagepos;
			_printf("Charger current incorrect so indicate Charger Fault\n\r");
			#endif

			/* Correct charging voltage so set flag */
			voltsSet = LU_TRUE;

			/* Reset the test flags */
			chargerTest = CH_TEST_NONE;
			timeoutFlag = 0;
		}
		else if( timeoutFlag > BATT_CHARGER_SETTLING_TIME_MS )
		{
			/* Incorrect charging voltage but set flag anyway and set error flags */
			voltsSet = LU_TRUE;
			timeoutFlag = 0;
			/* Set charger fault flag */
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,1);
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_TEST_FAIL, LU_TRUE);
			RetVal = IOManagerSetValue(IO_ID_VIRT_CH_CURRENT_FAULT, LU_TRUE);
			#ifdef DEBUG_OUTPUT
			messagepos;
			_printf("Charger voltage incorrect so indicate Charger Fault\n\r");
			#endif

			/* Incorrect charging current but set flag anyway because time has expired */
			voltsSet = LU_TRUE;

			/* Reset the test flags */
			chargerTest = CH_TEST_NONE;
			timeoutFlag = 0;
		}
		else
		{
			timeoutFlag += BATT_CH_TICK_MS;
		}

		/*
		 * If the correct current range has been achieved or the timeout has expired then turn
		 * off the charger and reconnect to the battery. Both these conditions are indicated by
		 * the timeout flag being zero.
		 */
		if( timeoutFlag == 0)
		{
			/* After checking the charger output volts leave the charger disconnected from the battery */
			RetVal = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_FALSE);

			/* Set Value for Battery charger voltage */
			RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET,    batteryStatePtr->BatteryCorrectedChargingVolts * chargerVoltsCal);

			/* Set Value for Battery charger current limit */
			RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET, batteryParamsPtr->BatteryChargeRate);

			/* Turn the charger off */
			StopChargingBattery();
		}
		break;

	default:
		break;
	}

	return voltsSet;
}

void ConnectChgToBatt(void)
{
	SB_ERROR sbError;
	/* Connect the charger to the battery */
	sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_TRUE);
}

void DisconnectChgFromBatt(void)
{
	SB_ERROR sbError;
	/* Disconnect the charger from the battery */
	sbError = IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT, LU_FALSE);
}

/******************************************************************************
 * Action functions - called on entry to a state.
 ******************************************************************************/

void Charger_RestartSuppliesAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	SB_ERROR retVal;

	/*
	 * Turn ON logic supply and comms/aux supplies
	 */

	/* Turn on logic supply */
	retVal = IOManagerSetPulse(IO_ID_LOGIC_PS_ON, LOGIC_PULSE_MS);
	/* Turn off logic green LED */
	retVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 1);
	/* Turn on logic supply red LED */
	retVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);

	/*
	 * Need to add code to determine which comms supplies are fitted
	 */
	retVal = IOManagerSetValue(IO_ID_COMMS_1_EN, LU_TRUE);

	retVal = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
	retVal = IOManagerSetValue(IO_ID_24V_AUX_EN, LU_TRUE);
	/* Turn off COM2/AUX LED */
	retVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
}

void Charger_ShutdownSuppliesAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	SB_ERROR retVal;

	/*
	 * Turn off logic supply and comms/aux supplies
	 */
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Shut Down State\n\r");
	#endif

	/* Turn off logic supply */
	retVal = IOManagerSetPulse(IO_ID_LOGIC_PS_OFF, LOGIC_PULSE_MS);
	/* Turn off logic green LED */
	retVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 0);
	/* Turn on logic supply red LED */
	retVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 1);

	/*
	 * Need to add code to determine which comms supplies are fitted
	 */
	retVal = IOManagerSetValue(IO_ID_COMMS_1_EN,            LU_FALSE);

	retVal = IOManagerSetValue(IO_ID_COMMS_2_EN,            LU_FALSE);
	retVal = IOManagerSetValue(IO_ID_24V_AUX_EN,            LU_FALSE);
	/* Turn off COM2/AUX LED */
	retVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_FALSE);
}

void Charger_ChgStopAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that puts the charger in a safe state by shutting it down
	 */
	LU_UNUSED( fsm );
	StopChargingBattery();
}

void Charger_ChgSuspendAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever the battery
	 * charging needs to be suspended due to battery over temperature
	 */
	LU_UNUSED( fsm );
	SuspendChargingBattery();
}

void Charger_ChgStartAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that initialises and turns on the charger
	 */
	LU_UNUSED( fsm );

	#ifdef DEBUG_OUTPUT
	deltatemppos;
	deltavoltspos;
	chargeendpos;
	#endif

//	StartChargingBattery();
}


void Charger_ChgStartTopUpPbAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );

	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that ithat starts the lead acid top-up charging.
	 */

	/*
	 * Do not set charger current limit, since it has already been set!!!
	 */

	/* Record time at which charger is turned on.  */
	TopUpStartTime = STGetTime();
	/*
	 * Calculate top up time in seconds required to give the specified top-up
	 * percentage at the specified rate
	 * (60 * 60 * capacity * percentage * 100) / (capacity/rate)
	 * or simplified
	 * ( 36 * percentage * rate )
	 */
	balancingTime  = ( TOP_UP_PERCENT_LEAD_ACID  *  TOP_UP_RATE_LEAD_ACID *  36 );

	#ifdef DEBUG_OUTPUT
	deltatemppos;
	deltavoltspos;
	chargeendpos;
	#endif
	topUpState = LU_TRUE;
}

void Charger_ChgStartTopUpNiMHAction(struct Charger *fsm)
{
	SB_ERROR sbError;
	LU_UNUSED( fsm );

	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that starts the NiMH balancing charging.
	 */

	/*
	 * Set charging current limit to the correct value to balance the cells
	 * i.e. battery capacity / balancing divisor
	 */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET, ( batteryParamsPtr->BatteryMaxCharge / TOP_UP_RATE_NIMH ) );
	/* Record time at which charger is turned on.  */
	TopUpStartTime = STGetTime();
	/*
	 * Calculate top up time in seconds required to give the specified balancing
	 * percentage at the specified balancing rate
	 * (60 * 60 * capacity * percentage * 100) / (capacity/rate)
	 * or simplified
	 * ( 36 * percentage * rate )
	 */
	balancingTime  = ( TOP_UP_PERCENT_NIMH  *  TOP_UP_RATE_NIMH *  36 );

	#ifdef DEBUG_OUTPUT
	deltatemppos;
	deltavoltspos;
	chargeendpos;
	#endif
	topUpState = LU_TRUE;
	StartChargingBattery();
}


void Charger_ChgConnectAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that needs the charger connected to the battery
	 */
	LU_UNUSED( fsm );

	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Charging Normal State\n\r");
	#endif

	ConnectChgToBatt();
}

void Charger_ChgTopUpAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that needs the charger connected to the battery
	 */
	LU_UNUSED( fsm );

	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Charging Top Up State\n\r");
	#endif

	ConnectChgToBatt();
}


void Charger_ChgLVFailAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that needs the LV has failed flag set.
	 */
	LU_UNUSED( fsm );
	LVFailedFlag = LU_TRUE;

	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery LV Fail State\n\r");
	#endif

	/*
	 * Record the time of the LV failure
	 */
	LVFailTime = STGetTime();
}

void Charger_ChgDisconnectAction(struct Charger *fsm)
{
	/*
	 * This is an action function that can be called whenever a state transition
	 * happens that needs the charger disconnected from the battery
	 */
	LU_UNUSED( fsm );
	DisconnectChgFromBatt();
}

void Charger_BatteryTestStartAction(struct Charger *fsm)
{
	lu_int32_t testCurrentMa = 0;
	SB_ERROR RetVal;
	lu_int32_t batteryVolts;
	lu_int32_t time;

	LU_UNUSED( fsm );
	/*
	 * This is the action function that is called whenever a state transition
	 * to the battery test state occurs
	 */


	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Test State\n\r");
	#endif

	/* Reset all relevant battery test structure elements */
	testStatePtr->BatteryVoltage			= 0;											/* Current battery voltage level 						*/
	testStatePtr->BatteryStartVoltage		= 0;											/* Starting battery voltage level 						*/
	testStatePtr->BatteryEndVoltage			= 0;											/* Finishing battery voltage level 						*/
	testStatePtr->BatteryDischargeRate		= 0;											/* Discharge current via dummy load           			*/
	testStatePtr->BatteryTestTime			= 0;											/* Time duration of current battery test				*/
	testStatePtr->BatteryTotalDrainCurrent	= 0 ;											/* Running total of battery drain i.e. current * time	*/
	testStatePtr->BatteryTestThreshold		= batteryParamsPtr->BatteryTestThresholdVolts;	/* Voltage at which test fails					*/
	testStatePtr->BatteryTestStartTime		= 0;											/* Time at which battery test was started				*/
	testStatePtr->BatteryTestInProgress 	= LU_TRUE;										/* Indicates if battery test is in progress				*/
	testStatePtr->BatteryTestCancelled		= LU_FALSE;										/* Battery test cancelled flag							*/

	/* Indicate that battery test is in progress	*/
	testStatePtr->BatteryTestInProgress = LU_TRUE;
	batteryTestInProgress = LU_TRUE;

	RetVal = IOManagerSetValue(IO_ID_VIRT_BATTERY_TEST_TIME, 0);
	RetVal = IOManagerSetValue(IO_ID_VIRT_BATTERY_TEST_TOTAL_DISCHARGE, 0);

	/* Turn charger off */
	RetVal = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 0);
	chargerStatePtr->ChargerEnabled = LU_FALSE;

	/* Turn off charging green LED */
	RetVal = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 0);

	/* Set virtual point to indicate battery isn't charging */
	RetVal = IOManagerSetValue( IO_ID_VIRT_CH_ON, 0);

	/* Set initial battery test current */
	if (!(SSGetFeatureRevisionMajor() == 0 &&
		SSGetFeatureRevisionMinor() == 1))
	{
		/* If feature revision is 0.2 or greater then it can have dynamic load */

		switch (batteryParamsPtr->BatteryTestLoadType)
		{
		case BATT_LOAD_TYPE_2AMP_PTC:
		default:
			testCurrentMa = BattTestLowTable1dU16U16[0].interpolateToX;
			break;

		case BATT_LOAD_TYPE_10AMP_PTC_R:
			testCurrentMa = BattTestPtcHighTable1dU16U16[0].interpolateToX;
			break;
		}

		/* Set initial test load current */
		RetVal = IOManagerSetValue(IO_ID_DAC_OUT_BT_TEST_LOAD, (lu_int32_t)testCurrentMa);
	}

	/* Disconnect the charger from the battery */
	DisconnectChgFromBatt();

	/* Get actual battery voltage before the dummy load is applied */
	RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
	batteryStatePtr->BatteryVoltage 		= batteryVolts;
	testStatePtr->BatteryVoltage 			= batteryVolts;
	testStatePtr->BatteryStartVoltage		= batteryVolts;

	/* Record battery test start time */
	time = STGetTime();
	testStatePtr->BatteryTestStartTime = time;

	/* Set initial test start */
	loadStartTime = time;

	/* Set NVRAM records to zero*/
}

void Charger_ChargerTestStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	SB_ERROR sbError;
	lu_int32_t charging;

	/* Reset all charger test fault indicators before starting test */
	sbError = IOManagerSetValue(IO_ID_VIRT_CH_FAULT,             0);
	sbError = IOManagerSetValue(IO_ID_VIRT_CH_TEST_FAIL,         0);
	sbError = IOManagerSetValue(IO_ID_VIRT_CH_VOLTAGE_FAULT,     0);
	sbError = IOManagerSetValue(IO_ID_VIRT_CH_CURRENT_FAULT,     0);

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_TEST_OPERATE_TIMEOUT,
						0
					   );

	/* Read virtual point to determine if battery is charging */
	sbError = IOManagerGetValue(IO_ID_VIRT_CH_ON, &charging);
	/* If charging then turn charger off */
	if( charging != LU_FALSE )
	{
		/* Stop the charging cycle */
		StopChargingBattery();
		chargerStatePtr->ChargerEndReason = BATT_CHARGE_STOP_TEST_RUNNING;
	}

	/* Indicate that the charger test is in progress */
	chargerTestInProgress = LU_TRUE;
}


void Charger_ChgConnectLoadAction(struct Charger *fsm)
{
	SB_ERROR   sbError;

	LU_UNUSED( fsm );

	/* Set test load current */
	sbError = IOManagerSetValue(IO_ID_DAC_OUT_BT_TEST_LOAD, BATTERY_STATUS_CHECK_LOAD_CURRENT);

	/* Turn charger off */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 0);
	chargerStatePtr->ChargerEnabled = LU_FALSE;

	/* Turn off charging green LED */
	sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 0);

	/* Disconnect the charger from the battery */
	DisconnectChgFromBatt();

	/*
	 * Connect the test load across the battery.
	 */
	sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 1);

	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	ClearDebugDisplay();
	statepos;
	_printf("Entered Status Check State\n\r");
	chargeendpos;
	_printf("Connect Load for Battery Status Check\n\r");
	#endif


}

void Charger_ChgResumeAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );

	lu_uint32_t time;
	SB_ERROR sbError;


	time = STGetTime();

	/*
	 * Disconnect the test load across the battery.
	 */
	sbError = IOManagerSetValue(IO_ID_BAT_TEST_EN, 0);

	/* Calculate time spent in suspend state */
	chargerStatePtr->ChargerSuspendTime = STElapsedTime(chargerStatePtr->ChargerSuspendStart, time);

	/* Add suspended time to start time to give correct charge time calculation */
	ChargeStartTime += chargerStatePtr->ChargerSuspendTime;

	/* Reset the suspend start time, suspended time and flag */
	chargerStatePtr->ChargerSuspendTime = 0;
	chargerStatePtr->ChargerSuspendStart = 0;
	chargerStatePtr->ChargerSuspendOccurred = LU_FALSE;

	/* Connect the charger to the battery */
	ConnectChgToBatt();

	/* Turn on charging green LED */
	sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 1);

	/* Turn charger on */
	sbError = IOManagerSetValue(IO_ID_BAT_CHARGER_EN, 1);
	chargerStatePtr->ChargerEnabled = LU_TRUE;


}

void Charger_ChgClearDebugAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	ClearDebugDisplay();
}

void Charger_BatteryInitStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	cls;
	statepos;
	_printf("Leaving Battery Init State\n\r");
	#endif

#ifdef LOGGING_OUTPUT_ENABLED
#ifdef DEBUG_OUTPUT
	pos((TEMP_COMP_POSITION+4),1);
#endif
	_printf( "BatteryType,ChargerEnabled,ChargerVoltsRequired,MeasuredChargingVolts,BatteryVoltage,ChargeCurrent,DrainCurrent,BatteryTemp,RTUTemp,DeltaTemp,DeltaVolts,BatteryAverageVolts,BatteryCurrent,ChargerCycleCharge,ChargerCycleTime,ChargeStoppedReason\r\n");
#endif
}

void Charger_BatteryNotChargingStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	batteryType();
	statepos;
	_printf("Entered Battery Not Charging State\n\r");
	#endif
}

void Charger_BatteryDisconnectedStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Disconnected State\n\r");
	#endif
}

void Charger_BatteryOverTempStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Over Temp State\n\r");
	#endif
}

void Charger_BatterySuspendStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Charger Suspended State\n\r");
	#endif
}

void Charger_BatteryPreChargeNiMHStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Pre-Charge NiMH State\n\r");
	#endif

	timeoutFlag = 0;
}

void Charger_BatteryPreChargeCheckingStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Pre-Charge Checking State\n\r");
	#endif

	timeoutFlag = 0;
}

void Charger_BatteryCheckBatteryChargedStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Check Battery Charged State\n\r");
	#endif
}

void Charger_BatteryChargingInhibitedStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Charging Inhibited State\n\r");
	#endif
}

void Charger_BatteryResetStatsStartAction(struct Charger *fsm)
{
	LU_UNUSED( fsm );
	#ifdef DEBUG_STATE_MACHINE_OUTPUT
	statepos;
	_printf("Entered Battery Reset Stats State\n\r");
	#endif
}

/*********************************************************************************************************
 * Local Functions
 *********************************************************************************************************/

lu_uint16_t CalculateBatteryCapacity(void)
{
	lu_uint16_t capacity;
	lu_int32_t batteryVolts,rowVoltage;
	SB_ERROR sbError;

	/* Get actual measured battery voltage */
	sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);

	rowVoltage = (batteryVolts / batteryOpts->BatteryPackNoOfRows);
	/*
	 * Use correct linear interpolation table depending on battery type
	 */
	if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH )
	{
		/* Use LnInterpTable1DU16U16Str BatteryCapacityNiMHTable1DU16U16 */
		capacity     = lnInterp1DU16U16((LnInterpTable1DU16U16Str*)BatteryCapacityNiMHTable1DU16U16 , BATT_CAPACITY_MAX , rowVoltage);
		battCapacity = capacity;
	}
	else
	{
		/* Use LnInterpTable1DU16U16Str BatteryCapacityPbTable1DU16U16 */
		capacity     = lnInterp1DU16U16((LnInterpTable1DU16U16Str*)BatteryCapacityPbTable1DU16U16   , BATT_CAPACITY_MAX , rowVoltage);
		battCapacity = capacity;
	}

	#ifdef DEBUG_OUTPUT
	_printf("\tBattery capacity [%8d]\n\r",capacity);
	#endif

	return capacity;
}

lu_bool_t CheckTemperatureChange(void)
{
			lu_int32_t 			BatteryTemp=0;					/* Current scaled battery temperature */
			lu_int32_t 			RtuTemp=0;						/* Current scaled RTU temperature */
    static 	lu_int32_t 			BatteryTempPrev=0;				/* Previous scaled battery temperature */
    static 	lu_int32_t 			RTUTempPrev=0;					/* Previous scaled RTU temperature */
    		lu_int32_t			BatteryDeltaTemp=0;				/* Change in battery temperature */
    		lu_int32_t			RTUDeltaTemp=0;					/* Change in RTU temperature */
    static	lu_bool_t 			firstTime = LU_TRUE;			/* Indicate fist time called in charge cycle */
    static	lu_int8_t			BattDeltaTempCnt=0;				/* Change in battery temperature over period */
    		lu_bool_t			chargeFinished = LU_FALSE;		/* Returns TRUE id fully charged otherwise returns FALSE */
    static  lu_uint32_t 		lastCheckTime,elapsedTime,time;	/* Used to time the check period */

    /*
     * This test for charging finished is only applicable for NiMH batteries
     * Return false for any other battery type.
     */
    if( batteryParamsPtr->BatteryType != BATT_TYPE_NI_MH)
    {
    	chargeFinished = LU_FALSE;
    	return chargeFinished;
    }

    /* Get the battery and rtu temperature */
	GetTemperature();

	/* Indicate that the temperature sensor isn't fitted or isn't working */
	if( batteryStatePtr->BatteryTempValid == LU_FALSE )
	{
    	chargeFinished = LU_FALSE;
    	return chargeFinished;
	}

	BatteryTemp = batteryStatePtr->BatteryTempAverage;
	RtuTemp		= batteryStatePtr->RTUTempAverage;

	/* Get current time */
	time = STGetTime();

	if( firstTime )
	{
		firstTime  			= LU_FALSE;
		BatteryTempPrev 	= BatteryTemp;
		RTUTempPrev			= RtuTemp;
		BattDeltaTempCnt 	= 0;
		chargeFinished		= LU_FALSE;
		lastCheckTime		= time;
	}
	else
	{
		elapsedTime = STElapsedTime( lastCheckTime , time);

		/* If NiMH battery with temperature sensor then check for battery fully charged */
		if( elapsedTime > (BATT_TEMP_DELTA_TIME * MSEC) )
		{
		  /* Calculate the change in battery temperature over a set period and compare it with the change
		   *  in RTU temp for the same period
		   * Temperature must  rise by at least the threshold twice in succession */
		  BatteryDeltaTemp 	= BatteryTemp - BatteryTempPrev;
		  RTUDeltaTemp 		= (RtuTemp 	  - RTUTempPrev);
		  lastCheckTime 	= time;			/* Reset previous time */

		  batteryStatePtr->BatteryDeltaTemp = BatteryDeltaTemp;
		  batteryStatePtr->RtuDeltaTemp     = RTUDeltaTemp;

		  #ifdef DEBUG_OUTPUT
		  deltatemppos;
		  _printf("BatteryDeltaTemp [%05d] RTUDeltaTemp [%05d]  Combined Delta Temp [%05d]\n\r",BatteryDeltaTemp,RTUDeltaTemp,(BatteryDeltaTemp - RTUDeltaTemp) );
		  #endif

		  /* If battery delta temp minus the RTU delta temp exceeds the set value */
		  if( (BatteryDeltaTemp - RTUDeltaTemp) >= BATT_CHARGED_TEMP)
		  {
			  /* Check that the value has been exceeded on two successive times */
			  if( BattDeltaTempCnt >= BATT_DELTA_T_CNT )
			  {
				  /* The battery has reached full charge so charging can be stopped */
				  #ifdef DEBUG_OUTPUT
				  deltatemppos;
				  _printf("BatteryDeltaTemp above maximum so stop charging. Count [%d]",BattDeltaTempCnt );
				  #endif
				  chargeFinished = LU_TRUE;
				  BattDeltaTempCnt = 0;
				  firstTime = LU_TRUE;
			  }
			  else
			  {
				  /* Increment count of times the delta has exceeded the full charge value */
				  BattDeltaTempCnt += 1;
				  #ifdef DEBUG_OUTPUT
				  deltatemppos;
				  _printf("BatteryDeltaTemp above maximum. Count [%d]",BattDeltaTempCnt );
				  #endif
			  }
		  }
		  else
		  {
			  /* Reset count as the change has not been exceeded two successive times */
			  BattDeltaTempCnt = 0;
			  chargeFinished = LU_FALSE;

		  }
		  BatteryTempPrev = BatteryTemp;
		  RTUTempPrev	  = RtuTemp;
		}
	}
	return chargeFinished;
}

lu_bool_t CheckVoltageChange(void)
{
	SB_ERROR sbError;
			lu_int32_t 			BatteryVolts=0;					/* Current scaled battery voltage */
			lu_int32_t 			batteryVoltsAverage;			/* Averaged battery voltage */
    static 	lu_int32_t 			BatteryVoltsPrev=0;				/* Previous scaled battery voltage */
    		lu_int32_t			BatteryDeltaVolts=0;			/* Change in battery voltage */
    static	lu_bool_t 			firstTime = LU_TRUE;			/* Indicate fist time called in charge cycle */
    static	lu_int8_t			BattDeltaVoltsCnt=0;			/* Change in battery voltage over period */
    		lu_bool_t			chargeFinished = LU_FALSE;		/* Returns TRUE if fully charged otherwise returns FALSE */
    static  lu_uint32_t 		lastCheckTime,elapsedTime,time;	/* Used to time the check period */
    static  lu_uint32_t			total = 0,average[16];
    static	lu_uint8_t			i=0,j;

    /*
     * This test for charging finished is only applicable for NiMH batteries
     * Return false for any other battery type.
     */
    if( batteryParamsPtr->BatteryType != BATT_TYPE_NI_MH)
    {
    	chargeFinished = LU_FALSE;
    	return chargeFinished;
    }
    sbError = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &BatteryVolts);

	/* Get current time */
	time = STGetTime();

	if( firstTime )
	{
		firstTime  			= LU_FALSE;
		BatteryVoltsPrev 	= BatteryVolts;
		BattDeltaVoltsCnt 	= 0;
		chargeFinished		= LU_FALSE;
		lastCheckTime		= time;
	    /* Fill average array with current battery volts */
	    for( j=0; j<16 ; j++)
	    {
	    	average[j] = BatteryVolts;
	    }
		#ifdef DEBUG_OUTPUT
		deltavoltspos;
		#endif
	}
	else
	{
		elapsedTime = STElapsedTime( lastCheckTime , time);

		/* Put current battery voltage into the array */
	    average[i++] = BatteryVolts;
	    total = 0;
	    /* Calculate average battery volts */
	    for( j=0; j<BATT_AVERAGE_DELTA_V_COUNT ; j++)
	    {
	    	total += average[j];
	    }
	    batteryVoltsAverage = total / BATT_AVERAGE_DELTA_V_COUNT;

	    batteryStatePtr->BatteryAverageVoltage = batteryVoltsAverage;

		/* If NiMH battery then check for battery fully charged */
		if( elapsedTime > (BATT_VOLT_DELTA_TIME * MSEC) )
		{
		  /* Calculate the change in battery Volts over a set period
		   * Volts must not rise by more than the threshold twice in succession */
		  BatteryDeltaVolts 	= batteryVoltsAverage - BatteryVoltsPrev;
		  lastCheckTime 	= time;			/* Reset previous time */

		  batteryStatePtr->BatteryDeltaVolts = BatteryDeltaVolts;

		#ifdef DEBUG_OUTPUT
		 deltavoltspos;
		_printf("BatteryDeltaVolts [%05d]\n\r",BatteryDeltaVolts );
		#endif

		  /* If delta Volts below the set value */
		  if( BatteryDeltaVolts <= BATT_CHARGED_DELTA_VOLTS)
		  {
			  /* Check that the value is less than the threshold on four successive times */
			  if( BattDeltaVoltsCnt >= BATT_DELTA_V_CNT )
			  {
				  /* The battery has reached full charge so charging can be stopped */
				  #ifdef DEBUG_OUTPUT
				  deltavoltspos;
				  _printf("BatteryDeltaVolts below minimum so charging stopped [%05d] Count [%d]",BatteryDeltaVolts, BattDeltaVoltsCnt );
				  #endif
				  chargeFinished = LU_TRUE;
				  BattDeltaVoltsCnt = 0;
				  firstTime = LU_TRUE;
			  }
			  else
			  {
				  /* Increment count of times the delta has exceeded the full charge value */
				  BattDeltaVoltsCnt += 1;
				  #ifdef DEBUG_OUTPUT
				  deltavoltspos;
				  _printf("BatteryDeltaVolts below minimum  [%05d] Count [%d]",BatteryDeltaVolts, BattDeltaVoltsCnt );
				#endif
			  }
		  }
		  else
		  {
			  /* Reset count as the change has not been exceeded two successive times */
			  BattDeltaVoltsCnt = 0;
			  chargeFinished = LU_FALSE;

		  }
		  BatteryVoltsPrev = batteryVoltsAverage;
		}
	}

	/* Reset counter  if required */
    if( i >= 16)
    {
    	i = 0;
    }


	return chargeFinished;
}

void SetBatteryDafaultParams(void)
{
	//#define NIMH
	#define LEAD_ACID
	//#define UNKNOWN

	#ifdef LEAD_ACID
			batteryOpts->BatteryChemistry				= BATT_TYPE_LEAD_ACID;/* Indicates type of battery in use	e.g. Lead Acid, NiMH	*/
			batteryOpts->BatteryCellChargingMethod		= BATT_CHARGE_METHOD_SENSOR; /* Sensor based or float charging scheme	*/

			batteryOpts->BatteryCellNominalVolts		= 12000;	/* Nominal Voltage of each cell								*/
			batteryOpts->BatteryCellFullChargeVolts		= 13250;	/* Fully charged Voltage of each cell						*/
			batteryOpts->BatteryCellMaxCapacity			= 7000;		/* Maximum capacity of each cell							*/
			batteryOpts->BatteryCellChargingCurrent		= 750;		/* Charging current specified for each cell 				*/
			batteryOpts->BatteryCellChargingVolts		= 13750;	/* Charging voltage specified for each cell 				*/
			batteryOpts->BatteryCellLeakage				= 10;		/* Leakage current of the battery with no discharge current */
			batteryOpts->BatteryCellFloatCharge			= 250;		/* Charge current if continuous float charging				*/

			batteryOpts->BatteryPackNoOfCols			= 1;		/* Number of cells in parallel								*/
			batteryOpts->BatteryPackNoOfRows			= 2;		/* Number of cells in series								*/
			batteryOpts->BatteryPackMaxChargeTemp		= 50;		/* Maximum temperature at which normal charging is allowed	*/
			batteryOpts->BatteryPackMaxChargeTime		= 7200;		/* Maximum charge time of the battery						*/
			batteryOpts->BatteryPackTimeBetweenCharge	= 120;		/* Minimum  time between charges of the battery				*/
			batteryOpts->BatteryPackOverideTime			= 3600;		/* Maximum time for which battery charging can be delayed 	*/
			batteryOpts->BatteryPackChargeTrigger		= 25500;	/* Normal battery charging threshold 						*/
			batteryOpts->BatteryPackThresholdVolts		= 24000;	/* Lower battery charging threshold for immediate charging	*/
			batteryOpts->BatteryPackLowLevel			= 22000;	/* Level below operations cannot be performed				*/

			batteryOpts->BatteryTestTimeMinutes					= 10;     /* Battery Test Load On Time Minutes */
			batteryOpts->BatteryTestCurrentMinimum				= 300;    /* Battery Test Current Minimum (mA) */
			batteryOpts->BatteryTestSuspendTimeMinutes			= 10;     /* Battery Test Suspend Time Minutes */
			batteryOpts->BatteryTestMaximumDurationTimeMinutes	= 240;    /* Battery Test Maximum Duration Time Minutes */
			batteryOpts->BatteryTestDischargeCapacity			= 10;     /* Battery Test Discharge Capacity */
			batteryOpts->BatteryTestThresholdVolts				= 22500;  /* Battery Test Threshold Volts */

			batteryParamsPtr->BatteryTestLoadType				= 1;      /* Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
			batteryParamsPtr->BatteryPackShutdownLevelVolts		= 22500;  /* Battery Pack Shutdown Level Volts */
			batteryParamsPtr->BatteryPackDeepDischargeVolts		= 19000;  /* Battery Pack Deep Discharge Protection Level Volts */

	#endif

	#ifdef NIMH
			batteryOpts->BatteryChemistry				= BATT_TYPE_NI_MH;	/* Indicates type of battery in use	e.g. Lead Acid, NiMH	*/
			batteryOpts->BatteryCellChargingMethod		= BATT_CHARGE_METHOD_SENSOR; /* Sensor based or float charging scheme	*/

			batteryOpts->BatteryCellNominalVolts		= 1200;		/* Nominal Voltage of each cell								*/
			batteryOpts->BatteryCellFullChargeVolts		= 1380;		/* Fully charged Voltage of each cell						*/
			batteryOpts->BatteryCellMaxCapacity			= 550;		/* Maximum capacity of each cell							*/
			batteryOpts->BatteryCellChargingCurrent		= 1000;		/* Charging current specified for each cell 				*/
			batteryOpts->BatteryCellChargingVolts		= 1450;		/* Charging voltage specified for each cell 				*/
			batteryOpts->BatteryCellLeakage				= 1;		/* Leakage current of the battery with no discharge current */
			batteryOpts->BatteryCellFloatCharge			= 250;		/* Charge current if continuous float charging				*/

			batteryOpts->BatteryPackNoOfCols			= 1;		/* Number of cells in parallel								*/
			batteryOpts->BatteryPackNoOfRows			= 20;		/* Number of cells in series								*/
			batteryOpts->BatteryPackMaxChargeTemp		= 45;		/* Maximum temperature at which normal charging is allowed	*/
			batteryOpts->BatteryPackMaxChargeTime		= 3600;		/* Maximum charge time of the battery						*/
			batteryOpts->BatteryPackTimeBetweenCharge	= 60;		/* Minimum  time between charges of the battery				*/
			batteryOpts->BatteryPackOverideTime			= 3600;		/* Maximum time for which battery charging can be delayed 	*/
			batteryOpts->BatteryPackChargeTrigger		= 26500;	/* Normal battery charging threshold 						*/
			batteryOpts->BatteryPackThresholdVolts		= 24000;	/* Lower battery charging threshold for immediate charging	*/
			batteryOpts->BatteryPackLowLevel			= 22000;	/* Level below operations cannot be performed				*/

			batteryOpts->BatteryTestTimeMinutes					= 10;     /* Battery Test Load On Time Minutes */
			batteryOpts->BatteryTestCurrentMinimum				= 300;    /* Battery Test Current Minimum (mA) */
			batteryOpts->BatteryTestSuspendTimeMinutes			= 10;     /* Battery Test Suspend Time Minutes */
			batteryOpts->BatteryTestMaximumDurationTimeMinutes	= 240;    /* Battery Test Maximum Duration Time Minutes */
			batteryOpts->BatteryTestDischargeCapacity			= 10;     /* Battery Test Discharge Capacity */
			batteryOpts->BatteryTestThresholdVolts				= 22500;  /* Battery Test Threshold Volts */

			batteryParamsPtr->BatteryTestLoadType				= 1;      /* Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
			batteryParamsPtr->BatteryPackShutdownLevelVolts		= 22500;  /* Battery Pack Shutdown Level Volts */
			batteryParamsPtr->BatteryPackDeepDischargeVolts		= 19000;  /* Battery Pack Deep Discharge Protection Level Volts */
	#endif

	#ifdef UNKNOWN
			batteryOpts->BatteryChemistry				= BATT_TYPE_UNKNOWN;	/* Indicates type of battery in use	e.g. Lead Acid, NiMH	*/
			batteryOpts->BatteryCellChargingMethod		= BATT_CHARGE_METHOD_DEFAULT; /* Default because type can't be determined from the NVRAM	*/

			batteryOpts->BatteryCellNominalVolts		= 12000;	/* Nominal Voltage of each cell								*/
			batteryOpts->BatteryCellFullChargeVolts		= 13800;	/* Fully charged Voltage of each cell						*/
			batteryOpts->BatteryCellMaxCapacity			= 8500;		/* Maximum capacity of each cell							*/
			batteryOpts->BatteryCellChargingCurrent		= 1000;		/* Charging current specified for each cell 				*/
			batteryOpts->BatteryCellChargingVolts		= 13750;	/* Charging voltage specified for each cell 				*/
			batteryOpts->BatteryCellLeakage				= 10;		/* Leakage current of the battery with no discharge current */
			batteryOpts->BatteryCellFloatCharge			= 250;		/* Charge current if continuous float charging				*/

			batteryOpts->BatteryPackNoOfCols			= 1;		/* Number of cells in parallel								*/
			batteryOpts->BatteryPackNoOfRows			= 2;		/* Number of cells in series								*/
			batteryOpts->BatteryPackMaxChargeTemp		= 40;		/* Maximum temperature at which normal charging is allowed	*/
			batteryOpts->BatteryPackMaxChargeTime		= 3600;		/* Maximum charge time of the battery						*/
			batteryOpts->BatteryPackTimeBetweenCharge	= 60;		/* Minimum  time between charges of the battery				*/
			batteryOpts->BatteryPackOverideTime			= 3600;		/* Maximum time for which battery charging can be delayed 	*/
			batteryOpts->BatteryPackChargeTrigger		= 26500;	/* Normal battery charging threshold 						*/
			batteryOpts->BatteryPackThresholdVolts		= 24000;	/* Lower battery charging threshold for immediate charging	*/
			batteryOpts->BatteryPackLowLevel			= 22000;	/* Level below operations cannot be performed				*/

			batteryOpts->BatteryTestTimeMinutes					= 10;     /* Battery Test Load On Time Minutes */
			batteryOpts->BatteryTestCurrentMinimum				= 300;    /* Battery Test Current Minimum (mA) */
			batteryOpts->BatteryTestSuspendTimeMinutes			= 10;     /* Battery Test Suspend Time Minutes */
			batteryOpts->BatteryTestMaximumDurationTimeMinutes	= 240;    /* Battery Test Maximum Duration Time Minutes */
			batteryOpts->BatteryTestDischargeCapacity			= 10;     /* Battery Test Discharge Capacity */
			batteryOpts->BatteryTestThresholdVolts				= 22500;  /* Battery Test Threshold Volts */

			batteryParamsPtr->BatteryTestLoadType				= 1;      /* Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
			batteryParamsPtr->BatteryPackShutdownLevelVolts		= 22500;  /* Battery Pack Shutdown Level Volts */
			batteryParamsPtr->BatteryPackDeepDischargeVolts		= 19000;  /* Battery Pack Deep Discharge Protection Level Volts */
#endif
}

void batteryType(void)
{
#ifdef DEBUG_OUTPUT
	if( batteryParamsPtr->BatteryType == BATT_TYPE_LEAD_ACID)
	{
		_printf("%c[%d;%dH",esc,2,75);		\
		_printf("- Lead Acid Battery\n\r");	\


	}
	else if( batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		_printf("%c[%d;%dH",esc,2,75);		\
		_printf("- NiMH Battery\t Battery Temperature [%05d]\n\r",batteryStatePtr->BatteryTempAverage);									\
		_printf("%c[%d;%dH",esc,3,101);		\
		_printf(" RTU Temperature [%05d]",batteryStatePtr->RTUTemp);
	}

	else if( batteryParamsPtr->BatteryType == BATT_TYPE_UNKNOWN)
	{
		_printf("%c[%d;%dH",esc,2,75);		\
		_printf("- Unknown Battery\n\r");									\
	}
#endif
}


void ClearDebugDisplay(void)
{
#ifdef DEBUG_OUTPUT
	lu_uint8_t i;

	/* clear the charger debug display */
	for( i=0 ; i<7 ; i++)
	{
		pos( (CHARGER_POSITION+i) , 1);
		cll;
	}
#endif
}

/*
 *********************** End of file ******************************************
 */
