/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Charger header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CHARGER_INCLUDED
#define _CHARGER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "debug_frmwrk.h"

/* Define tracing to use printf to serial port */
#define TRACE		_printf

#include "lu_types.h"
#include "Charger_sm.h"

#include "LinearInterpolation.h"

#include "BatteryCharger.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define	STATE_POSITION			5
#define	CHARGER_POSITION		9
#define MESSAGE_POSTION			18
#define BATT_TEST_POSITION		23
#define CAN_POSITION			26
#define MOTOR_SUPPLY_POSITION	38
#define CHARGE_END				40
#define TEMP_COMP_POSITION		44

#define esc 27

#define cls 			_printf("%c[2J",esc);									\
						_printf("%c[%d;%dH",esc,2,55);							\
						_printf("Power Supply Module");							\
						_printf("%c[%d;%dH",esc,3,55);							\
						_printf("===================");							\
						_printf("%c[%d;%dH",esc,STATE_POSITION,5);				\
						_printf("Charger State Machine - ");					\
						_printf("%c[%d;%dH",esc,(CHARGER_POSITION - 2),5);		\
						_printf("Charger\n\r");									\
						_printf("===================");							\
						_printf("%c[%d;%dH",esc,(BATT_TEST_POSITION - 2),5);	\
						_printf("Battery Test\n\r");							\
						_printf("===================");							\
						_printf("%c[%d;%dH",esc,(MOTOR_SUPPLY_POSITION - 2),5);	\
						_printf("Motor Supply\n\r");							\
						_printf("===================");							\
						_printf("%c[%d;%dH",esc,(MOTOR_SUPPLY_POSITION + 2),1);	\
						_printf("Motor Supply Off\n\n\r" );


#define cll				_printf("%c[K",esc)
#define home			_printf("%c[H",esc)
#define pos(row,col) 	_printf("%c[%d;%dH",esc,row,col)

#define statepos		pos(STATE_POSITION,30);cll;
#define batttestpos		pos(BATT_TEST_POSITION,1);cll;
#define canpos			pos(CAN_POSITION,1);cll;
#define chargepos		pos(CHARGER_POSITION,1);cll;
#define messagepos		pos(MESSAGE_POSTION,1);cll;
#define messagepos1		pos((MESSAGE_POSTION+1),1);cll;
#define battvolts		pos((MESSAGE_POSTION-1),1);cll;
#define deltatemppos	pos((MESSAGE_POSTION+1),1);cll;
#define deltavoltspos	pos((MESSAGE_POSTION+2),1);cll;
#define chargeendpos	pos((CHARGE_END),1);cll;
#define tempcomppos		pos((TEMP_COMP_POSITION),1);cll;
#define currentcheck	pos((TEMP_COMP_POSITION+1),1);cll;
#define logging			pos((TEMP_COMP_POSITION+5),1);cll;
/*
 * Define the maximum power output from the PSU @40C
 */
#define MAX_PSU_POWER						(150*1000) /* milli Watts */
#define MAIN_DC_VOLTS						36
#define PSU_DERATE_TEMP						45		/* Temperature at which to de-rate PSU */

/* If defined turn on diagnostic printing */
//#define DEBUG_OUTPUT
//#define DEBUG_CHARGING_OUTPUT
//#define DEBUG_CHARGING_VALUES
//#define DEBUG_STATE_MACHINE_OUTPUT
//#define DEBUG_BATT_TEST_OUTPUT

/* If defined turn on logging output */
#define LOGGING_OUTPUT_ENABLED

/*
 * When the charging current falls below below a minimum level turn off the charger
 * Define lowest current at which the battery is charged.
 */
#define BATT_MINIMUM_CHARGING_CURRENT		200		/* mA 			*/
#define BATT_MINIMUM_PRESENT_CURRENT		20		/* mA 			*/

/*
 * Threshold in mV for warning of a shutdown - i.e. in 1-2 hours
 */
#define BATT_WARNING_THRESHOLD		500

/*
 * When performing the current test for the battery charger set the charging voltage
 * to this higher than normal value to ensure that there is a current flow even if
 * the battery is fully charged.
 */
#define CHARGER_CURRENT_TEST_VOLTAGE 30000

/*
 * When performing the current test for the battery charger set the charging current limit
 * to this lower than normal value
 */
#define CHARGER_CURRENT_TEST_CURRENT 500

/*
 * Temperature compensation values for charging batteries
 */
#define QUOTED_CHARGING_TEMPERATURE (20)	/* temperature at which NVM charging voltage is quoted */
#define TEMP_COMP_LEAD_ACID  (40.0)			/* Temperature compensation for charging in milli-volts per degree */
#define TEMP_COMP_NIMH		 (00.0)			/* Temperature compensation for charging in milli-volts per degree */
#define TEMP_COMP_UNKNOWN	 (40.0)			/* Temperature compensation for charging in milli-volts per degree */

/* Divisor for temperature sensor resolution */
#define TEMP_SENSOR_RESOLUTION 1000
/*
 * Define the battery status checks parameters for disconnected battery state
 */
#define BATTERY_STATUS_CHECK_PERIOD 		30		/* Seconds	*/
#define BATTERY_STATUS_CHECK_CHARGE_TIME 	10		/* Seconds	*/

/*
 * Define the maximum differential between the actual battery voltage and the charger voltage limit
 */
#define BATTERY_CHARGING_DIFFERENTIAL 3000

/* NiMH High Temperature Battery Parameters */
#define BATT_DELTA_T_CNT     		1       /* The number of times in succession that the delta temp must exceed set value  			*/
#define BATT_DELTA_V_CNT			3		/* The number of times the delta V must be below minimum set value							*/
#define BATT_CHARGED_TEMP   		1000	/* Change in temperature over the period that indicates the battery is charged degrees/1000	*/
#define BATT_TEMP_DELTA_TIME		900		/* Period(seconds) over which to measure temperature change in seconds						*/
#define BATT_VOLT_DELTA_TIME		300		/* Period(seconds) over which to measure voltage change in seconds							*/
#define BATT_CHARGED_DELTA_VOLTS	5		/* If the battery voltage(mV) change does not exceed this figure then fully charged 		*/
#define BATT_AVERAGE_DELTA_V_COUNT	16		/* The number of samples over which the battery voltage is averaged							*/
#define BATT_AVERAGE_DELTA_T_COUNT	16		/* The number of samples over which the battery temperature is averaged						*/
#define RTU_AVERAGE_TEMP_COUNT		16		/* The number of samples over which the rtu temperature is averaged							*/
#define DEG_C_TO_MILLI_DEG_C 		1000
#define RTU_TEMP_RES				100		/* Factor to convert degrees C/ten to mill degrees C										*/

#define TOP_UP_RATE_NIMH			40		/* Charge rate divisor topping up NiMH batteries											*/
#define TOP_UP_PERCENT_NIMH			5		/* Charge capacity percentage for balancing NiMH batteries									*/
#define TOP_UP_RATE_LEAD_ACID		7		/* Charge rate divisor topping up lead acid batteries										*/
#define TOP_UP_PERCENT_LEAD_ACID	20		/* Charge capacity percentage for balancing lead acid batteries								*/

#define BATT_CHARGER_CURENT_CONTROLLED 4	/* The revision of charger board capable of software current control for NiMH batteries		*/
/*
 * Define the time between charger current checks in ms
 */
#define CHARGER_CURRENT_CHECK_PERIOD 5000

/* Flash rate of charging LED when the battery is over temperature */
#define OVER_TEMP_FLASH_TIME 250

/*
 * Define the time between status checks in seconds
 */
#define STATUS_CHECK_INTERVAL 1200
#define BATTERY_SETTLE_PERIOD 10
#define BATTERY_STATUS_CHECK_LOAD_CURRENT 	2000	/* mAmps	*/

/*
 * When the charging current no longer changes turn off the charger.
 * Define the hysteresis and time for monitoring the charging current.
 */
#define BATT_CHARGING_HYSTERISIS_CURRENT 	200		/*  mA 			*/
#define BATT_CHARGING_HYSTERISIS_TIME		60		/* seconds 		*/

#define BATT_CHARGER_SETTLING_TIME_MS		30000    /* Time allowed for the battery charger to settle in ms */

#define BATT_MIN_CHARGE_TIME				600		/* Minimum charge time in seconds that is recorded as a charge cycle */
#define BATT_VOLTS_FEEDBACK_AMMOUNT			0.015	/* This is the change applied in calibrating the charging voltage  */

#define BATT_VOLTS_FEEDBACK					25		/* This is the change applied in required charging voltage when in closed loop current feedback */

#define CHARGER_HYSTERESIS                 (50)		/* Hysteresis used when determining if the battery need charging */

/* Set tolerance on charger voltage in mV */
#define CHARGER_V_TOL	250
/* Set tolerance on charger current in mA */
#define CHARGER_I_TOL	40

#define BATT_CHG_VOLTS_COMPENSATION_MAX 1.5
#define BATT_CHG_VOLTS_COMPENSATION_MIN 0.5

#define BATT_CHG_CURRENT_COMPENSATION_MIN 0.8

#define LV_FAILED_THRESHOLD		600					/* Time in seconds over which an LV failure is considered battery draining */

/* Set hysteresis on low battery level */
#define BATT_LOW_LEVEL_HYSTERESIS	2000

/* Set hysteresis on low battery level */
#define BATT_WARNING_HYSTERESIS	1500

/* Set period for battery to be low before alarm is set in seconds */
#define BATT_LOW_LEVEL_PERIOD	60
/* Set period for battery to be below imminent shutdown level before alarm is set in seconds */
#define BATT_SHUT_LEVEL_PERIOD	60

/* Pulse length to switch logic supply */
#define LOGIC_PULSE_MS						10

/* Delay in seconds after shutdown limit is reached to give MCM time to shutdown */
#define SHUTDOWN_DELAY 60

/* Delay in ms after supplies shutdown until battery disconnect */
#define BATTERY_DISCONNECT_DELAY 500


/* Define the number of milli Amp seconds in 0.1 Amp hour */
#define MA_SEC_IN_A_HOUR	( 100 * 60 * 60 )
#define SEC_IN_HOUR		( 60 * 60 )
#define SEC_IN_MIN		( 60 )

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct  Charger
{
	struct ChargerContext _fsm;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern int batteryVoltage;
extern int batteryThreshold;

extern const LnInterpTable1DU16U16Str BattTestLowTable1dU16U16[MAX_BATT_TEST_LOW_1DU16U16];
extern const LnInterpTable1DU16U16Str BattTestPtcHighTable1dU16U16[MAX_BATT_TEST_HIGH_1DU16U16];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern void ChargerFSMInit(void);
extern void ChargerFSMTick(void);
extern void SendBatteryTestEvent(void);
extern lu_bool_t  SendBatteryResetStatsEvent(void);
extern void SendChargerTestEvent(void);
extern void SendChargerInhibitEvent(void);
extern void SendChargerInhibitCancelEvent(void);

extern lu_uint32_t initComplete(void);
extern lu_uint32_t preNiMHComplete(void);
extern lu_uint32_t preCheckComplete(void);
extern lu_uint32_t statusCheckComplete(void);
extern lu_uint32_t chargingFinished(void);
extern lu_uint32_t chargingFinishedNiMH(void);
extern lu_uint32_t topUpFinished(void);
extern lu_uint32_t LVSupplyConnected(void);
extern lu_uint32_t batteryCheck(void);
extern lu_uint32_t batteryNeedsCharging(void);
extern lu_uint32_t batteryDisconnected(void);
extern lu_uint32_t batteryHealth(void);
extern lu_uint32_t chargerCurrentCheck(void);
extern lu_uint32_t batteryPresent(void);
extern lu_uint32_t batteryOverTemperature(void);
extern lu_uint32_t batteryUnderTemperature(void);
extern lu_uint32_t batteryTypeKnown(void);
extern lu_uint32_t batteryTypeNiMH(void);
extern lu_uint32_t batteryTypeLeadAcid(void);
extern lu_uint32_t chargingOveride(void);
extern lu_uint32_t batteryChargeRemaining(void);
extern lu_uint32_t motorSupplyOn(void);
extern lu_uint32_t chargerTestComplete(void);
extern lu_uint32_t batteryTestComplete(void);
extern lu_uint32_t batteryResetStatsComplete(void);
extern lu_uint32_t statusCheckDue(void);
extern lu_uint32_t batteryCharged(void);
extern lu_uint32_t DisconnectBattery(void);

/********************************************************************************************
 *                              Action functions
 ********************************************************************************************/


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgConnectAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void Charger_ChgTopUpAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgLVFailAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgDisconnectAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_RestartSuppliesAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ShutdownSuppliesAction(struct Charger *fsm);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_BatteryTestStartAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChargerTestStartAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgStopAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgSuspendAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgStartAction(struct Charger *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *#ref 3333
 ******************************************************************************
 */
extern void Charger_ChgStartTopUpNiMHAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgStartTopUpPbAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void Charger_ChgConnectLoadAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgResumeAction(struct Charger *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Charger_ChgClearDebugAction(struct Charger *fsm);

extern void Charger_BatteryInitStartAction(struct Charger *fsm);
extern void Charger_BatteryNotChargingStartAction(struct Charger *fsm);
extern void Charger_BatteryDisconnectedStartAction(struct Charger *fsm);
extern void Charger_BatteryOverTempStartAction(struct Charger *fsm);
extern void Charger_BatterySuspendStartAction(struct Charger *fsm);
extern void Charger_BatteryPreChargeNiMHStartAction(struct Charger *fsm);
extern void Charger_BatteryPreChargeCheckingStartAction(struct Charger *fsm);
extern void Charger_BatteryCheckBatteryChargedStartAction(struct Charger *fsm);
extern void Charger_BatteryChargingInhibitedStartAction(struct Charger *fsm);
extern void Charger_BatteryResetStatsStartAction(struct Charger *fsm);


#endif /* _CHARGER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
