/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "system_LPC17xx.h"
#include "debug_frmwrk.h"
#include "lpc17xx_timer.h"
#include "IIRFilter.h"
#include "peakDetector.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const int testData[8000][3];

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static IIRFilter3HistoryStr IIR1;

static filteredPeakDetectorDataStr filteredPeackDetector1;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



int main(void)
{
    TIM_TIMERCFG_Type timerConfig;

    lu_uint32_t i;
    lu_int32_t result;
    lu_int32_t x;
    lu_uint32_t startTime, endTime, total;
    lu_bool_t ret;

    SystemInit();
    debug_frmwrk_init();

    /* Initialize Timer */
    timerConfig.PrescaleOption = TIM_PRESCALE_TICKVAL;
    timerConfig.PrescaleValue = 1;
    TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &timerConfig);

    _printf("Test started...\r\n");

    /* Initialize filter */
    IIRFilter3FullReset(&IIR1);
    IIR1.xc[0] = XC1_3;
    IIR1.xc[1] = XC2_3;
    IIR1.xc[2] = XC3_3;
    IIR1.xc[3] = XC4_3;
    IIR1.yc[0] = YC1_3;
    IIR1.yc[1] = YC2_3;
    IIR1.yc[2] = YC3_3;
    IIR1.yc[3] = 0;

    filteredPeakDetectorFullReset(&filteredPeackDetector1);
    filteredPeackDetector1.IIRdata.xc[0] = XC1_3;
    filteredPeackDetector1.IIRdata.xc[1] = XC2_3;
    filteredPeackDetector1.IIRdata.xc[2] = XC3_3;
    filteredPeackDetector1.IIRdata.xc[3] = XC4_3;
    filteredPeackDetector1.IIRdata.yc[0] = YC1_3;
    filteredPeackDetector1.IIRdata.yc[1] = YC2_3;
    filteredPeackDetector1.IIRdata.yc[2] = YC3_3;
    filteredPeackDetector1.IIRdata.yc[3] = 0;

    while(LU_TRUE)
    {
        IIRFilter3Reset(&IIR1);
        filteredPeakDetectorReset(&filteredPeackDetector1);
        total = 0;
        /* Reset timer */
        TIM_ResetCounter(LPC_TIM0);
        /* Start Timer */
        TIM_Cmd(LPC_TIM0, ENABLE);

        for(i = 0; i < 8000; i++)
        {
            /* run the filter alone */
            x = testData[i][0] << INPUT_MULTIPLIER_POW;
            //startTime = LPC_TIM0->TC;
            result = IIRFilter3(x, &IIR1);
            //endTime = LPC_TIM0->TC;
            if(result != testData[i][1])
            {
                _printf( "Result: %i - Expected: %i - Error: %i\r\n",
                         result, testData[i][1], result - testData[i][1]
                       );
            }

            /* Run filter + peak detector */
            startTime = LPC_TIM0->TC;
            ret = filteredPeakDetector(testData[i][0], &result, &filteredPeackDetector1);
            endTime = LPC_TIM0->TC;

            total += (endTime - startTime);

            if(ret)
            {
                if(result != testData[i][2])
                {
                    _printf("Peak error. Found %i, Expected: %i - Idx: %i\r\n", result, testData[i][2], i);
                }
            }
            else
            {
                if(testData[i][2] != -1)
                {
                    _printf("Peak error. Not Found, Expected: %i - Idx: %i\r\n", testData[i][2], i);
                }
            }
        }

        _printf("Test finished. Average cycles: %i\r\n", (total/8000)*4);
    }

    return 0;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
