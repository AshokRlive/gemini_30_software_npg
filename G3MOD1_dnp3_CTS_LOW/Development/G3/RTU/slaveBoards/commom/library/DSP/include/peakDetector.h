/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Peak Detector interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _C_PEAKDETECTOR_INCLUDED
#define _C_PEAKDETECTOR_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "IIRFilter.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define PEAK_DETECTOR_BUFFER_LEN 3

/*! Input data multiplier */
#define INPUT_MULTIPLIER_POW 8
#define INPUT_MULTIPLIER     (1 << INPUT_MULTIPLIER_POW)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * Peak detector history data buffer
 */
typedef struct peakDetectorHistoryDef
{
    /*! 3 slots buffer. Used for peck detection */
    lu_int32_t buffer[PEAK_DETECTOR_BUFFER_LEN];
}peakDetectorHistoryStr;

/*!
 * Peak detector history data buffer and
 * IIR filter buffer
 */
typedef struct filteredPeakDetectorDataDef
{
    /*! IIR filter data */
    IIRFilter3HistoryStr IIRdata;
    /*! Peak detector buffer */
    peakDetectorHistoryStr peakDetectorData;
}filteredPeakDetectorDataStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern inline void peakDetectorReset(peakDetectorHistoryStr *history);
inline void peakDetectorReset(peakDetectorHistoryStr *history)
{
    memset(history, 0, sizeof(*history));
}

/*!
 ******************************************************************************
 *   \brief Peak detector
 *
 *   Simple and fast implementation of a peck detector. It works only if
 *   the input sinusoid is filtered.
 *
 *   A peak is identified as change of slot from positive to negative.
 *
 *   Before applying the algorithm a full-wave rectification is applied.
 *
 *   Execution time (-O2 optimisation): ~36 clock cycles (@100Mhz: 360ns)
 *
 *   \param x New input value
 *   \param y buffer where the new (rectified) peck is saved
 *   \param history pointer to the buffer where the peak detector history
 *   is saved
 *
 *   \return LU_TRUE if a new peak is found
 *
 ******************************************************************************
 */
extern inline lu_bool_t peakDetector(lu_int32_t x, lu_int32_t *y, peakDetectorHistoryStr *history);
inline lu_bool_t peakDetector(lu_int32_t x, lu_int32_t *y, peakDetectorHistoryStr *history)
{
    register lu_int32_t *bufferPtr = history->buffer;
    register lu_int32_t a, b;

    /* Full wave rectifier */
    x = abs(x);

    /* Load history data */
    a = bufferPtr[1];
    b = bufferPtr[2];

    /* Save history */
    bufferPtr[0] = a;
    bufferPtr[1] = b;
    bufferPtr[2] = x;

    /* Check peak */
    if( (b >= a) && (b >= x))
    {
        /* Peak found */
        *y = b;
        return LU_TRUE;
    }

    return LU_FALSE;
}

extern inline void filteredPeakDetectorReset(filteredPeakDetectorDataStr *history);
inline void filteredPeakDetectorReset(filteredPeakDetectorDataStr *history)
{
    IIRFilter3Reset(&history->IIRdata);
    peakDetectorReset(&history->peakDetectorData);
}

extern inline void filteredPeakDetectorFullReset(filteredPeakDetectorDataStr *history);
inline void filteredPeakDetectorFullReset(filteredPeakDetectorDataStr *history)
{
    IIRFilter3FullReset(&history->IIRdata);
    peakDetectorReset(&history->peakDetectorData);
}

/*!
 ******************************************************************************
 *   \brief Filtered Peak detector
 *
 *   The input signal is conditioned(amplified and filtered) before executing
 *   the peak detector algorithm.
 *
 *   Input value: 24 bits 2's complement value signed extended to 32 bits
 *
 *   In order to increase the precision of the filter the input value is
 *   multiplied by 2^8 (since the input value is a 24 bits value there is no
 *   overflow). The peak value is scaled back before being returned.
 *
 *   Filter: 3rd order IIR filter (see IIRFilter3 for a full
 *   description of the filter)
 *
 *   Peak detector: see peakDetector
 *
 *   Average execution time (-O2 optimisation): ~56 clock cycles (@100Mhz: 560ns)
 *   See IIRFilter3
 *
 *   \param x New input value
 *   \param y buffer where the new (rectified) peck is saved (the peak is scaled
 *   back before returning)
 *   \param history pointer to the buffer where the peak detector and IIR
 *   filter history is saved
 *
 *   \return LU_TRUE if a new peak is found
 *
 ******************************************************************************
 */
extern inline lu_bool_t filteredPeakDetector(lu_int32_t x, lu_int32_t *y, filteredPeakDetectorDataStr *history);
inline lu_bool_t filteredPeakDetector(lu_int32_t x, lu_int32_t *y, filteredPeakDetectorDataStr *history)
{
    lu_int32_t result;

    /* Amply the signal and it to the IIR filter */
    x = IIRFilter3(x << INPUT_MULTIPLIER_POW, &history->IIRdata);
    /* Run the peak detector using the filtered signal */
    if (peakDetector(x, &result, &history->peakDetectorData) == LU_TRUE)
    {
        /* Peak detected: rescale and save result */
        *y = result >> INPUT_MULTIPLIER_POW;
        return LU_TRUE;
    }

    return LU_FALSE;
}
#endif /* _C_PEAKDETECTOR_INCLUDED */

/*
 *********************** End of file ******************************************
 */
