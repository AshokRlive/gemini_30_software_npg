clear all;

deg2radiant = 2*pi/360;

% Sampling frequency
Fs = 8000;
% Sample time
T = 1/Fs;
% Length of signal
L = 8000;
% Time vector
t = (0:L-1)*T;
% fundamental frequency
f = 50;

% Input signal multiplier
xMult = 1000;

% Saturation level
satLevel = 10 * xMult;

% White noise amplitude
noiseAmplitude = 0.5;

%Main harmonic added tot the input signal
harmonicAmplitude =  0.4; %percentage of the main signal
harmonicLevel = 3;
harmonicPhaseShift = 0; %degrees

% Number of additionals harmonics
additionalHarmonics = 7;

% 2nd order Chebyshev type I filter
CCutOffFreq_2 = 60;
CRippledB_2 = 0.4;

% 3rd order Chebyshev type I filter
CCutOffFreq_3 = 59;
CRippledB_3 = 0.07;
COrder = 3;

% Simulate a Fault
faultStart = (2761 - 24); %sample index
faultStop = 6000; %sample index
faultAmplitude = 3;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                               DON'T CHANGE                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Noise FIR Filter. Used to create a 2KHz band-limited white noise
noiseFilterFreq = 2000;
noiseFilterOrder = 500;
noiseLen = 2 * L;

% Plot
plotStart = 1;
plotStop = 8000;

% Generate band-limited white noise
for i = 1:noiseLen
           wNoise(i) = (rand() - 0.5) * noiseAmplitude;
endfor
hNoise = fir1(noiseFilterOrder, (noiseFilterFreq/(Fs/2)) );
LPNoise = filter(hNoise, 1, wNoise);

%Generate fundamental signal
x1 = sin(2*pi*f*t);

%Generate harmonic
x2 = harmonicAmplitude*sin(2*pi*f*harmonicLevel*t + (harmonicPhaseShift*deg2radiant));

% Generate additionals harmonics
if(additionalHarmonics > 0)
    for k=1:additionalHarmonics
        x2 += harmonicAmplitude/(k+1)*sin(2*pi*f*(harmonicLevel+k)*t + (harmonicPhaseShift*deg2radiant));
    endfor;
endif;

% geneate fault
x1(faultStart:faultStop) = x1(faultStart:faultStop) * faultAmplitude;

% Generate final signal: fundamental + harmonic + noise
xt =  x1 + x2 + LPNoise(L:(2*L)-1);

% Amplify the signal
xt = xt * xMult;

% Simulate saturation
for j=1:L
    if xt(j) > satLevel xt(j) = satLevel; endif;
    if xt(j) < -satLevel xt(j) = -satLevel; endif;
endfor

% Simulate a real ADC: convert input signal (xt) to integer
xt = round(xt);

% Generate 2nd Chebyshev type 1 filter
[b2, a2] = cheby1(2, CRippledB_2, CCutOffFreq_2 / (Fs/2));
% Generate 3rd Chebyshev type 1 filter
[b3, a3] = cheby1(3, CRippledB_3, CCutOffFreq_3 / (Fs/2));

% Filter signal using 2nd order Chebyshev filter
y2 = filter(b2, a2, xt);
% Filter signal using rdd order Chebyshev filter
y3 = filter(b3, a3, xt);

% Filter signal using Chebyshev filter. Apply phase compensation
y2Comp = filtfilt(b2, a2, xt);


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implement IIR filter using fixed point math   %
% The FDM implementation should produce    %
% the same values                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%
%        2nd ordef filter Start         %
%%%%%%%%%%%%%%%%%%%%

%coefficients multiplier
cMultiplier_2 = (2^24);

%Input scale
iMultiplier_2 = (2^8);

NZEROS_2 = 2;
NPOLES_2 = 2;

% history buffer
xv_2(1:NZEROS_2 +1) = 0;
yv_2(1:NPOLES_2 +1) = 0;

% Generate  coefficients
xc1_2 = round(b2(3) * cMultiplier_2)
xc2_2 = round(b2(2) * cMultiplier_2)
xc3_2 = round(b2(1) * cMultiplier_2)
yc1_2 = round(-(a2(3) * cMultiplier_2))
yc2_2 = round(-(a2(2) * cMultiplier_2))

% filter loop         
for j=1:L,
   xv_2(1) = xv_2(2);
   xv_2(2) = xv_2(3); 
   xv_2(3) = xt(j) *iMultiplier_2; %Amplify input to improve precision
   yv_2(1) = yv_2(2);
   yv_2(2) = yv_2(3);
   yv_2(3) =  floor((xc1_2*xv_2(1) + xc2_2*xv_2(2) + xc3_2*xv_2(3) + yc1_2*yv_2(1) + yc2_2*yv_2(2) ) / cMultiplier_2);
   y2_fpm(j) = yv_2(3) / iMultiplier_2; % Rescale output
endfor
 
 
 % Maximum error
 y2Diff = y2_fpm - y2;
 y2DiffP = (y2Diff .* 100) ./ y2;
maxError2 = max(y2DiffP);

%%%%%%%%%%%%%%%%%%%%
%        2nd ordef filter End           %
%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%&&&%
%        3rd ordef filter  Start        %
%%%%%%%%%%%%%%%&&&%%

NZEROS_3 = 3;
NPOLES_3= 3;

% history buffer
xv_3(1:NZEROS_3 +1) = 0;
yv_3(1:NPOLES_3 +1) = 0;

%coefficients multiplier
cMultiplier_3 = (2^24);

%Input scale
iMultiplier_3 = (2^8);

% Generate   coefficients
xc1_3 = round(b3(4) * cMultiplier_3)
xc2_3 = round(b3(3) * cMultiplier_3)
xc3_3 = round(b3(2) * cMultiplier_3)
xc4_3 = round(b3(1) * cMultiplier_3)
yc1_3 = round(-(a3(4) * cMultiplier_3))
yc2_3 = round(-(a3(3) * cMultiplier_3))
yc3_3 = round(-(a3(2) * cMultiplier_3))


% filter loop         
for j=1:L,
    xv_3(1) = xv_3(2);
    xv_3(2) = xv_3(3);
    xv_3(3) = xv_3(4); 
    xv_3(4) = xt(j) * iMultiplier_3;

    yv_3(1) = yv_3(2);   
    yv_3(2) = yv_3(3);
    yv_3(3) = yv_3(4);
    yv_3(4) =  floor((xc1_3*xv_3(1) + xc2_3*xv_3(2) + xc3_3*xv_3(3) + xc4_3*xv_3(4) + yc1_3*yv_3(1) + yc2_3*yv_3(2) +  yc3_3*yv_3(3)) / cMultiplier_3);
    y3_fpm(j) = floor(yv_3(4) / iMultiplier_3);
    y3_fpm_no_scaling(j) =   floor(yv_3(4));
   
    y3Diff(j) =  y3_fpm(j) - y3(j);
    y3DiffP(j) = (y3Diff(j) * 100) / y3(j);   
endfor

% Maximum error
maxError3 = max(y3DiffP);

%%%%%%%%%%%%%%%%&&&%
%        3rd ordef filter  End          %
%%%%%%%%%%%%%%%&&&%%


%Generate plots
plot( (plotStart:plotStop), x1(plotStart:plotStop)*xMult,';Foundamental;',
        (plotStart:plotStop), xt(plotStart:plotStop),';Input signal;',
        %(plotStart:plotStop),y2(plotStart:plotStop),';Filtered Input 2nd order;',
        (plotStart:plotStop),y3(plotStart:plotStop),';Filtered Input 3rd order;',
        %(plotStart:plotStop),y2_fm(plotStart:plotStop),';Filtered Input 2nd order - fixed point;',
        (plotStart:plotStop),y3_fpm(plotStart:plotStop),';Filtered Input 3rd order - fixed point;'
        % (plotStart:plotStop),y3Diff(plotStart:plotStop),';Error;',
        % (plotStart:plotStop),y3DiffP(plotStart:plotStop),';Error %;'
        % (plotStart:plotStop),y2Comp(plotStart:plotStop),';cheby - phase compensation;'
       );
xlabel('Samples');


%%%%%%%%%%%%%%%%%%%%%%%%
% Peak detector: a positive to negative %
% change in the slot identifies a peak    %
%%%%%%%%%%%%%%%%%%%%%%%%
peakNum_fpm = 0;
peakNum = 0;

peakBuf_fpm(1:3) = 0;
peakBuf(1:3) = 0;

%Full-wave rectifier
yPeak_fpm = abs(y3_fpm_no_scaling);
yPeak = abs(y3);

% Save Input/Output data in a file as a C struct.
% It can be used to test the FDM filter implementation
fid = fopen( 'data.c', 'wt');
fprintf(fid, '/* Data automatically generated in matlab/octave */\n');
fprintf(fid, 'const int testData[%i][%i] =\n{\n', L, 3);
fprintf(fid, '/*    Input    - Filtered&Amplified Input - Peak */\n');

for j=1:8000,
peakBuf_fpm(1) = peakBuf_fpm(2);	
peakBuf_fpm(2) = peakBuf_fpm(3);
peakBuf_fpm(3) = yPeak_fpm(j);
	
peakBuf(1) = peakBuf(2);	
peakBuf(2) = peakBuf(3);
peakBuf(3) = yPeak(j);

 fprintf( fid, '    {%8i,  %8i,              ', xt(j), y3_fpm_no_scaling(j));

if( (peakBuf_fpm(2) >= peakBuf_fpm(1)) && (peakBuf_fpm(2) >= peakBuf_fpm(3)))
    %peak found
    peakNum_fpm++;
    %printf('Peak fpm found:%i - %i\n', peakBuf_fpm(2)/(2^8), j-1);
    fprintf( fid, '%8i},\n',  floor(peakBuf_fpm(2)/(2^8)));
else
    fprintf( fid, '%8i},\n',  -1);	
endif;

if( (peakBuf(2) >= peakBuf(1)) && (peakBuf(2) >= peakBuf(3)))
    %peak found
    peakNum++;
    %printf('Peak found:    %i - %i\n', peakBuf(2), j-1);
endif;
			
endfor

peakNum_fpm
peakNum

fprintf(fid, '};\n');
fclose( fid); 


