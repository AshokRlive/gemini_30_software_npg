/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IIR Filter interface
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/08/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _C_IIRFILTER_INCLUDED
#define _C_IIRFILTER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*! Coefficients multiplier  */
#define COEFFICIENT_MULTIPLIER_POW 24
#define COEFFICIENT_MULTIPLIER     (1 << COEFFICIENT_MULTIPLIER_POW)

/* Second order filter */
#define NZEROS_2 2
#define NPOLES_2 2
#define XLEN_2 (NZEROS_2 + 1)
#define YLEN_2 (NPOLES_2 + 1)

/* Third order filter */
#define NZEROS_3 3
#define NPOLES_3 3
#define XLEN_3 (NZEROS_3 + 1)
#define YLEN_3 (NPOLES_3 + 1)


/*! Chebyshev type 1 2nd order filter default coefficients
 *  Cut Off Frequency: 60 hz
 *  Pass band ripple: 0.4 db
 *  Sample rate: 8Khz
 */
#define XC1_2 (14461)
#define XC2_2 (28921)
#define XC3_2 (14461)
#define YC1_2 (-15603257)
#define YC2_2 (32319904)


/*! Chebyshev type 1 3rd order filter coefficients
 *  Cut Off Frequency: 59 hz
 *  Pass band ripple: 0.07 db
 *  Sample rate: 8Khz
 */
#define XC1_3 (390)
#define XC2_3 (1169)
#define XC3_3 (1169)
#define XC4_3 (390)
#define YC1_3 (15216127)
#define YC2_3 (-47109082)
#define YC3_3 (48667053)

/*!
 * History structure for 2md order IIR filter
 */
typedef struct IIRFilter2HistoryDef
{
     /*! Input data buffer */
     lu_int32_t x[XLEN_2];
     /*! Output data buffer */
     lu_int32_t y[YLEN_2];
     /*! Filter coefficients for input data. See IIR filter
      * for a detailed description of the format
      */
     lu_int32_t xc[XLEN_2];
     /*! Filter coefficients for output data. See IIR filter
      * for a detailed description of the format
      */
     lu_int32_t yc[YLEN_2];
}IIRFilter2HistoryStr;

typedef struct IIRFilter3HistoryDef
{
    /*! Input data buffer */
     lu_int32_t x[XLEN_3];
     /*! Output data buffer */
     lu_int32_t y[YLEN_3];
     /*! Filter coefficients for input data. See IIR filter
      *  for a detailed description of the format.
      */
     lu_int32_t xc[XLEN_3];
     /*! Filter coefficients for output data. See IIR filter
      *  for a detailed description of the format.
      */
     lu_int32_t yc[YLEN_3];
}IIRFilter3HistoryStr;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern inline void IIRFilter2Reset(IIRFilter2HistoryStr *history);
inline void IIRFilter2Reset(IIRFilter2HistoryStr *history)
{
    memset(history->x, 0, sizeof(history->x));
    memset(history->y, 0, sizeof(history->y));
}

extern inline void IIRFilter2FullReset(IIRFilter2HistoryStr *history);
inline void IIRFilter2FullReset(IIRFilter2HistoryStr *history)
{
    memset(history, 0, sizeof(*history));
}

/*!
 ******************************************************************************
 *   \brief Second order IIR filter
 *
 *   Fixed point math implementation of a third order IIR filer
 *
 *   Input value: 32 bits 2's complement
 *   Input coefficients are multiplied by COEFFICIENT_MULTIPLIER
 *
 *   The absolute precision of the filter is proportional to the input/peak value
 *
 *   history->x: input history. Index 0: oldest value Index 2: newest value
 *   history->y: output history. Index 0: oldest value Index 2: newest value
 *
 *   Filter history:
 *   The filter needs a buffer where the input/output is saved. Never access
 *   the buffer directly. The values in the history are not meant to be used
 *   outside the filter itself (all the value are still scaled in order to
 *   improve precision and performance).
 *
 *   Input coefficients XCx_3: the coefficients generated in matlab/octave
 *   should be rescaled. The coefficients generated by the
 *   IIRFilter_peakDetector.m matlab script matlab script are already scaled.
 *
 *   Output coeffcients: YCx_3: the coefficients generated in matlab/octave
 *   should be rescaled and inverted. The coefficients generated by the
 *   IIRFilter_peakDetector.m matlab script matlab script are already scaled
 *   and inverted.
 *
 *   Filter reset: manually clear all the value in the the filter history or
 *   use the clear filter function
 *
 *   Filter execution time: see 3rd order filter
 *
 *   The basic filter used for this "optimized" version is:
 *   xHistory[0] = xHistory[1];
 *   xHistory[1] = xHistory[2];
 *   xHistory[2] = (x << INPUT_MULTIPLIER_POW);
 *
 *   yHistory[0] = yHistory[1];
 *   yHistory[1] = yHistory[2];
 *
 *   tmpLong = (lu_int64_t)((lu_int64_t)XC1_3 * (lu_int64_t)xHistory[0]) +
 *             (lu_int64_t)((lu_int64_t)XC2_3 * (lu_int64_t)xHistory[1]) +
 *             (lu_int64_t)((lu_int64_t)XC4_3 * (lu_int64_t)xHistory[3]) +
 *             (lu_int64_t)((lu_int64_t)YC1_3 * (lu_int64_t)yHistory[0]) +
 *             (lu_int64_t)((lu_int64_t)YC2_3 * (lu_int64_t)yHistory[1]);
 *
 *   yHistory[2] = (lu_int32_t)((lu_int64_t)tmpLong >> (lu_int64_t)MULTIPLIER_POW);
 *
 *   return (yHistory[2] >> INPUT_MULTIPLIER_POW);
 *
 *   \param x New input value
 *   \param history pointer to the buffer where the filter history is saved
 *
 *   \return Filter output
 *
 ******************************************************************************
 */
extern inline lu_int32_t IIRFilter2(lu_int32_t x, IIRFilter2HistoryStr *history);
inline lu_int32_t IIRFilter2(lu_int32_t x, IIRFilter2HistoryStr *history)
{
    register lu_int64_t tmpLong;
    register lu_int32_t a1, a2;
    register lu_int32_t *cPtr, c;
    register lu_int32_t *xHistory = history->x;
    register lu_int32_t *yHistory = history->y;

    cPtr= history->xc;

    a1 = xHistory[1];
    c = cPtr[0];
    tmpLong = (lu_int64_t)((lu_int64_t)c * (lu_int64_t)a1);

    /* Rescale the input value */
    a2 = x;
    c = cPtr[2];
    tmpLong += (lu_int64_t)((lu_int64_t)c * (lu_int64_t)a2);

    xHistory[0] = a1;

    a1 = xHistory[2];
    c = cPtr[1];
    tmpLong += (lu_int64_t)((lu_int64_t)c * (lu_int64_t)a1);
    xHistory[1] = a1;
    xHistory[2] = a2;

    cPtr= history->yc;
    a1 = yHistory[1];
    a2 = yHistory[2];

    c = cPtr[0];
    tmpLong += (lu_int64_t)((lu_int64_t)c * (lu_int64_t)a1);
    c = cPtr[1];
    tmpLong += (lu_int64_t)((lu_int64_t)c * (lu_int64_t)a2);

    yHistory[0] = a1;
    yHistory[1] = a2;

    a1 = (lu_int32_t)((lu_int64_t)tmpLong >> (lu_int64_t)COEFFICIENT_MULTIPLIER_POW);

    yHistory[2] = a1;

    /* Rescale output */
    return a1;
}

extern inline void IIRFilter3Reset(IIRFilter3HistoryStr *history);
inline void IIRFilter3Reset(IIRFilter3HistoryStr *history)
{
    memset(history->x, 0, sizeof(history->x));
    memset(history->y, 0, sizeof(history->y));
}

extern inline void IIRFilter3FullReset(IIRFilter3HistoryStr *history);
inline void IIRFilter3FullReset(IIRFilter3HistoryStr *history)
{
    memset(history, 0, sizeof(*history));
}

/*!
 ******************************************************************************
 *   \brief Third order IIR filter
 *
 *   Fixed point math implementation of a third order IIR filer
 *
 *   Input value: 32 bits 2's complement
 *   Input coefficients are multiplied by COEFFICIENT_MULTIPLIER
 *
 *   The absolute precision of the filter is proportional to the input/peak value
 *
 *   history->x: input history. Index 0: oldest value Index 3: newest value
 *   history->y: output history. Index 0: oldest value Index 3: newest value
 *
 *   Filter history:
 *   The filter needs a buffer where the input/output is saved. Never access
 *   the buffer directly. The values in the history are not meant to be used
 *   outside the filter itself (all the value are still scaled in order to
 *   improve precision and performance).
 *
 *   Input coefficients XCx_3: the coefficients generated in matlab/octave
 *   should be rescaled. The coefficients generated by the
 *   IIRFilter_peakDetector.m matlab script are already scaled.
 *
 *   Output coeffcients: YCx_3: the coefficients generated in matlab/octave
 *   should be rescaled and inverted. The coefficients generated by the
 *   IIRFilter_peakDetector.m matlab script are already scaled and inverted.
 *
 *   Filter reset: manually clear all the value in the the filter history or
 *   use the clear filter function
 *
 *   Average filter execution time (-O2 optimisation): ~48 clock cycles (@100Mhz: 480ns)
 *   It may take longer depending on the absolute value of the inputs:
 *   the MAC unit of the Cortex-M3 takes 3-7 clock cycles
 *
 *   The basic filter used for this "optimized" version is:
 *   xHistory[0] = xHistory[1];
 *   xHistory[1] = xHistory[2];
 *   xHistory[2] = xHistory[3];
 *   xHistory[3] = x;
 *
 *   yHistory[0] = yHistory[1];
 *   yHistory[1] = yHistory[2];
 *   yHistory[2] = yHistory[3];
 *
 *   tmpLong = (lu_int64_t)((lu_int64_t)XC1_3 * (lu_int64_t)xHistory[0]) +
 *             (lu_int64_t)((lu_int64_t)XC2_3 * (lu_int64_t)xHistory[1]) +
 *             (lu_int64_t)((lu_int64_t)XC3_3 * (lu_int64_t)xHistory[2]) +
 *             (lu_int64_t)((lu_int64_t)XC4_3 * (lu_int64_t)xHistory[3]) +
 *             (lu_int64_t)((lu_int64_t)YC1_3 * (lu_int64_t)yHistory[0]) +
 *             (lu_int64_t)((lu_int64_t)YC2_3 * (lu_int64_t)yHistory[1]) +
 *             (lu_int64_t)((lu_int64_t)YC3_3 * (lu_int64_t)yHistory[2]);
 *
 *   yHistory[3] = (lu_int32_t)((lu_int64_t)tmpLong >> (lu_int64_t)COEFFICIENT_MULTIPLIER_POW);
 *
 *   return (yHistory[3]);
 *
 *   \param x New input value
 *   \param history pointer to the buffer where the filter history is saved
 *
 *   \return Filter output
 *
 ******************************************************************************
 */
extern inline lu_int32_t IIRFilter3(lu_int32_t x, IIRFilter3HistoryStr *history);
inline lu_int32_t IIRFilter3(lu_int32_t x, IIRFilter3HistoryStr *history)
{
    register lu_int64_t tmpLong;
    register lu_int32_t a1;
    register lu_int32_t *cPtr, c;
    register lu_int32_t *xHistory = history->x;
    register lu_int32_t *yHistory = history->y;

    cPtr= history->xc;

    /* xHistory[0] = xHistory[1];
     * tmpLong = (lu_int64_t)((lu_int64_t)XC1_3 * (lu_int64_t)xHistory[0]);
     */
    a1 = xHistory[1];
    c = cPtr[0];
    tmpLong = (lu_int64_t)((lu_int64_t)c * a1);
    xHistory[0] = a1;

    /* xHistory[1] = xHistory[2];
     * tmpLong += (lu_int64_t)((lu_int64_t)XC2_3 * (lu_int64_t)xHistory[1]);
     */
    a1 = xHistory[2];
    c = cPtr[1];
    tmpLong += (lu_int64_t)((lu_int64_t)c * a1);
    xHistory[1] = a1;

    /* xHistory[2] = xHistory[3];
     * tmpLong += (lu_int64_t)((lu_int64_t)XC3_3 * (lu_int64_t)xHistory[2]);
     */
    a1 = xHistory[3];
    c = cPtr[2];
    tmpLong += (lu_int64_t)((lu_int64_t)c * a1);
    xHistory[2] = a1;

    /* xHistory[3] = x;
     * tmpLong += (lu_int64_t)((lu_int64_t)XC4_3 * (lu_int64_t)xHistory[3]);
     */
    c = cPtr[3];
    tmpLong += (lu_int64_t)((lu_int64_t)c * x);
    xHistory[3] = x;

    cPtr= history->yc;

    /* yHistory[0] = yHistory[1];
     * tmpLong += (lu_int64_t)((lu_int64_t)YC1_3 * (lu_int64_t)yHistory[0]);
     */
    a1 = yHistory[1];
    c = cPtr[0];
    tmpLong += (lu_int64_t)((lu_int64_t)c * a1);
    yHistory[0] = a1;

    /* yHistory[1] = yHistory[2];
     * tmpLong += (lu_int64_t)((lu_int64_t)YC2_3 * (lu_int64_t)yHistory[1]);
     */
    a1 = yHistory[2];
    c = cPtr[1];
    tmpLong += (lu_int64_t)((lu_int64_t)c * a1);
    yHistory[1] = a1;

    /* yHistory[2] = yHistory[3];
     * tmpLong += (lu_int64_t)((lu_int64_t)YC3_3 * (lu_int64_t)yHistory[2]);
     */
    a1 = yHistory[3];
    c = cPtr[2];
    tmpLong += (lu_int64_t)((lu_int64_t)c * a1);
    yHistory[2] = a1;

    /* yHistory[3] = (lu_int32_t)((lu_int64_t)tmpLong >> (lu_int64_t)COEFFICIENT_MULTIPLIER_POW);
     * return (yHistory[3]);
     */
    a1 = (lu_int32_t)(tmpLong >> COEFFICIENT_MULTIPLIER_POW);
    yHistory[3] = a1;
    return a1;
}

#endif /* _C_IIRFILTER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
