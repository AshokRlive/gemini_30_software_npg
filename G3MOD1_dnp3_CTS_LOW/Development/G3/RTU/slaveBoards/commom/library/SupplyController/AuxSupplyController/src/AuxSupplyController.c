/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/07/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "AuxSupplyController.h"

#include "ModuleProtocol.h"

#include "NVRam.h"
#include "NVRAMDef.h"

#include "systemStatus.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSystemEnum.h"
#include "SysAlarm/SysAlarmSupplyControllerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR InitCommsAux(void);
static SB_ERROR CheckCommsAux(void);

static SB_ERROR initOperationalState(auxSupplyStateStr **);
static SB_ERROR setOperationalState(lu_uint8_t, lu_uint8_t, lu_uint32_t, lu_uint32_t );
static SB_ERROR getOperationalState(lu_uint8_t, lu_uint8_t *, lu_uint32_t *, lu_uint32_t *);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint8_t primaryComms;
lu_uint8_t secondaryComms;

static NVRAMOptPSMStr    *NVRAMOptPSMPtr;
static auxSupplyStateStr *auxSupplyStatePtr;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR AuxSupplyControlInit(void)
{
	SB_ERROR RetVal;

	RetVal = SB_ERROR_NONE;

	RetVal = initOperationalState( &auxSupplyStatePtr );

	/* Read NVRAM to determine if COMMS1 or COMS2 Isolated supplies should be fitted.  */
	RetVal = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_OPTS, (lu_uint8_t **)&NVRAMOptPSMPtr);

	if (RetVal != SB_ERROR_NONE)
	{
		/* Alert Primary options NVRAM failed */
		SysAlarmSetLogEvent(LU_TRUE,
    			            SYS_ALARM_SEVERITY_WARNING,
    			            SYS_ALARM_SUBSYSTEM_SYSTEM,
    			            SYSALC_SYSTEM_PRIMARY_FACTORY_OPTS,
						    0
						   );
						   
		RetVal = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_OPTS, (lu_uint8_t **)&NVRAMOptPSMPtr);

		if (RetVal != SB_ERROR_NONE)
		{
			/* Alert Primary + Secondary options NVRAM failed */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_SYSTEM,
								SYSALC_SYSTEM_FACTORY_OPTS,
								0
							   );	
		}
	}
	
	if (RetVal != SB_ERROR_NONE)
	{
		NVRAMOptPSMPtr->optionBitField  = NVRAMOPTPSMDEF_COMMSPRIMARYFITTED_BM;
		//NVRAMOptPSMPtr->optionBitField  |= NVRAMOPTPSMDEF_CHARGERCURRENTCONTROLFITTED_BM;
		NVRAMOptPSMPtr->commsSecondaryFitted = LU_TRUE;
		NVRAMOptPSMPtr->commsPrimaryVolts = 12;
		NVRAMOptPSMPtr->commsSecondaryVolts = 12;
	}

	/* Get state of PSM board i.e. what isolated supplies are physically present*/
	RetVal = InitCommsAux();

	return RetVal;
}

lu_uint8_t AuxSupplySelectHandler(SupplySelectStr *auxSupplySelect)
{
	lu_uint32_t auxSelectTime, auxSelectPeriod;
	lu_uint8_t RetVal = REPLY_STATUS_OKAY;
	lu_uint8_t state;

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
						SYSALC_SUPPLY_CONTROL_COMMS_SELOP_TIMEOUT,
						0
					   );

	getOperationalState(auxSupplySelect->channel, &state, &auxSelectTime, &auxSelectPeriod);

	/* Check that a channel isn't selected or operating */
	if(state != AUXSUPPLY_IDLE)
	{
		/* A channel is already selected or operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		auxSelectPeriod = (lu_uint32_t)(auxSupplySelect->SelectTimeout) * MSEC;
		auxSelectTime = STGetTime();

		setOperationalState(auxSupplySelect->channel, AUXSUPPLY_SELECTED, auxSelectTime, auxSelectPeriod);
		RetVal = REPLY_STATUS_OKAY;
	}

	return RetVal;
}

lu_uint8_t AuxSupplyOperateHandler(SupplyOperateStr *auxSupplyOperate)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	SB_ERROR retError;
	lu_uint32_t auxSelectTime, auxSelectPeriod;
	lu_uint32_t auxOperateTime, auxOperatePeriod;
	lu_uint8_t state;

	time = STGetTime();

	getOperationalState(auxSupplyOperate->channel, &state, &auxSelectTime, &auxSelectPeriod);

	/* Check that the channel has been selected and ready to operate */
	if(state != AUXSUPPLY_SELECTED)
	{
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	else
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( auxSelectTime , time );
		if ( elapsedTime > auxSelectPeriod)
		{
			/* select time has been exceeded so cancel operation. */
			// auxSelected = LU_FALSE;
			auxSelectTime = 0;

			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
								SYSALC_SUPPLY_CONTROL_COMMS_SELOP_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */

			/* Turn off aux supply indefinitely if duration is zero */
			/* Save time for power to be turned back on */
			auxOperatePeriod = (lu_uint32_t)(auxSupplyOperate->supplyDuration) * MSEC;
			auxOperateTime = time;

			/* Change state from Selected to Operating */
			state = AUXSUPPLY_OPERATING;

			/* Turn off supply */
			switch (auxSupplyOperate->channel)
			{
				case PSM_CH_PSUPPLY_COMMS_PRIM:
					retError = IOManagerSetValue(IO_ID_COMMS_1_EN, LU_FALSE);
					retError = IOManagerSetValue(IO_ID_VIRT_COMMS1_ON, LU_FALSE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_GREEN, LU_FALSE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_RED, LU_FALSE);
					break;

				case PSM_CH_PSUPPLY_COMMS_SEC:
					if (SSGetFeatureRevisionMajor() == 0 &&
						SSGetFeatureRevisionMinor() == 1)
					{
						/* Combined AUX/COMM2 support */
						if( secondaryComms == COMMS_COMMS2 )
						{
							retError = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_FALSE);
							retError = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_FALSE);
						}
						else if( secondaryComms == COMMS_AUX )
						{
							retError = IOManagerSetValue(IO_ID_24V_PS_EN, LU_FALSE);
							retError = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_FALSE);
						}
					}
					else
					{
						/* Independant AUX/COMMS2 control */
						retError = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_FALSE);
						retError = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_FALSE);
					}

					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_FALSE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
					break;

				case PSM_CH_PSUPPLY_24V_AUX:
					if (SSGetFeatureRevisionMajor() == 0 &&
						SSGetFeatureRevisionMinor() == 1)
					{
						/* Combined AUX/COMM2 support */
						if( secondaryComms == COMMS_COMMS2 )
						{
							retError = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_FALSE);
							retError = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_FALSE);
						}
						else if( secondaryComms == COMMS_AUX )
						{
							retError = IOManagerSetValue(IO_ID_24V_PS_EN, LU_FALSE);
							retError = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_FALSE);
						}

						retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_FALSE);
						retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
					}
					else
					{
						/* Independant AUX/COMMS2 control */
						retError = IOManagerSetValue(IO_ID_24V_PS_EN, LU_FALSE);
						retError = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_FALSE);

						retError = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, LU_FALSE);
						retError = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, LU_FALSE);
					}
					break;

				default:
					break;
			}

			RetVal = REPLY_STATUS_OKAY;
		}
	}

	setOperationalState(auxSupplyOperate->channel, state, auxOperateTime, auxOperatePeriod);

	return RetVal;
}

lu_uint8_t AuxSupplyCancelHandler(SupplyCancelStr *auxSupplyCancel)
{
	lu_uint32_t auxActionTime, auxActionPeriod;
	lu_uint8_t state;
	lu_uint8_t RetVal;
	SB_ERROR retError;

	getOperationalState(auxSupplyCancel->channel, &state, &auxActionTime, &auxActionPeriod);

	/* Check if the channel is already operating */
	if(state != AUXSUPPLY_OPERATING)
	{
		/* The channel is not operating so this is an error */
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}
	else
	{
		/* Turn on supply */
		switch (auxSupplyCancel->channel)
		{
			case PSM_CH_PSUPPLY_COMMS_PRIM:
				retError = IOManagerSetValue(IO_ID_COMMS_1_EN, LU_TRUE);
				retError = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_GREEN, LU_TRUE);
				retError = IOManagerSetValue(IO_ID_VIRT_COMMS1_ON, LU_TRUE);
				retError = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_RED, LU_FALSE);
				break;

			case PSM_CH_PSUPPLY_COMMS_SEC:
				if (SSGetFeatureRevisionMajor() == 0 &&
					SSGetFeatureRevisionMinor() == 1)
				{
					/* Combined AUX/COMM2 support */
					if( secondaryComms == COMMS_COMMS2 )
					{
						retError = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
						retError = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);
					}
					else if( secondaryComms == COMMS_AUX )
					{
						retError = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
						retError = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);
					}
					/* Turn on COM2/AUX LED */
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
				}
				else
				{
					/* Independent AUX/COMMS2 control */
					retError = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
					retError = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);

					/* Turn on COM2 LED */
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
				}
				break;

			case PSM_CH_PSUPPLY_24V_AUX:
				if (SSGetFeatureRevisionMajor() == 0 &&
					SSGetFeatureRevisionMinor() == 1)
				{
					/* Combined AUX/COMM2 support */
					if( secondaryComms == COMMS_COMMS2 )
					{
						retError = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
						retError = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);
					}
					else if( secondaryComms == COMMS_AUX )
					{
						retError = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
						retError = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);
					}
					/* Turn on COM2/AUX LED */
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
				}
				else
				{
					/* Independent AUX/COMMS2 control */
					retError = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
					retError = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);

					/* Turn on COM2 LED */
					retError = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, LU_TRUE);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, LU_FALSE);
				}
				break;

			default:
				break;
		}

		// Back to IDLE
		setOperationalState(auxSupplyCancel->channel, AUXSUPPLY_IDLE, 0, 0);

		RetVal = REPLY_STATUS_OKAY;
	}

	return RetVal;
}

SB_ERROR AuxSupplyControlTick(void)
{
	static lu_uint32_t statusCheckTime;
	lu_uint32_t auxStartTime, auxPeriod;
	lu_uint32_t elapsedTime, time;
	lu_uint8_t state;
	lu_uint8_t channelSelected;
	SB_ERROR RetVal;

	/* Get current time */
	time = STGetTime();

	/* Cycle through all AUX Channels */
	for(channelSelected = PSM_CH_PSUPPLY_COMMS_PRIM; channelSelected < PSM_CH_PSUPPLY_LAST; channelSelected++)
	{
		getOperationalState(channelSelected, &state, &auxStartTime, &auxPeriod);

		/* If selected then check for select time exceeded */
		elapsedTime = STElapsedTime( auxPeriod , time );

		if ((state == AUXSUPPLY_SELECTED) && ( elapsedTime > auxPeriod))
		{
			SysAlarmSetLogEvent(LU_FALSE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
								SYSALC_SUPPLY_CONTROL_COMMS_SELOP_TIMEOUT,
								0
							   );

			/* Reset the channel information */
			setOperationalState(channelSelected, AUXSUPPLY_IDLE, 0, 0);
		}
		else if ((state == AUXSUPPLY_OPERATING) && (elapsedTime  > auxPeriod) && (auxPeriod > 0))
		{
			switch (channelSelected)
			{
				case PSM_CH_PSUPPLY_COMMS_PRIM:
					RetVal = IOManagerSetValue(IO_ID_COMMS_1_EN, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_GREEN, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS1_ON, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_RED, LU_FALSE);
					break;

				case PSM_CH_PSUPPLY_COMMS_SEC:
					if (SSGetFeatureRevisionMajor() == 0 &&
						SSGetFeatureRevisionMinor() == 1)
					{
						/* Combined AUX/COMM2 support */
						if( secondaryComms == COMMS_COMMS2 )
						{
							RetVal = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
							RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);
						}
						else if( secondaryComms == COMMS_AUX )
						{
							RetVal = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
							RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);
						}
						/* Turn on COM2/AUX LED */
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
					}
					else
					{
						/* Independant AUX/COMMS2 control */
						RetVal = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
						RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);

						/* Turn on COM2 LED */
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
					}
					break;

				case PSM_CH_PSUPPLY_24V_AUX:
					if (SSGetFeatureRevisionMajor() == 0 &&
						SSGetFeatureRevisionMinor() == 1)
					{
						/* Combined AUX/COMM2 support */
						if( secondaryComms == COMMS_COMMS2 )
						{
							RetVal = IOManagerSetValue(IO_ID_COMMS_2_EN, LU_TRUE);
							RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);
						}
						else if( secondaryComms == COMMS_AUX )
						{
							RetVal = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
							RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);
						}
						/* Turn on COM2/AUX LED */
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
					}
					else
					{
						/* Independant AUX/COMMS2 control */
						RetVal = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
						RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);

						/* Turn on COM2 LED */
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, LU_TRUE);
						RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, LU_FALSE);
					}
					break;

				default:
					break;
			}

			setOperationalState(channelSelected, AUXSUPPLY_IDLE, 0, 0);
		}

		/*
		 *  Check comms supplies voltages and status.
		 *  Check which supplies are being used
		 */

		/* Check status if due */
		elapsedTime = STElapsedTime( statusCheckTime , time );
		if( elapsedTime > (AUX_SUPPLY_STATUS_TIME * MSEC) )
		{
			CheckCommsAux();
			statusCheckTime = time;
		}
	}

	RetVal = SB_ERROR_NONE;

	return RetVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR CheckCommsAux(void)
{
	SB_ERROR RetVal;
	lu_int32_t comms1supplyPresent;
	lu_int32_t comms2supplyPresent;
	lu_int32_t comms1Volts;
	lu_int32_t comms2AuxVolts;
	lu_int32_t auxVolts;
	lu_int32_t auxSupplyStatus;
	lu_int32_t auxSupplyPwrGood;
	lu_int32_t comms1Enable;
	lu_int32_t comms2Enable;
	lu_int32_t auxEnable;
	lu_bool_t comms2VoltOkay = LU_TRUE;
	lu_bool_t auxVoltOkay = LU_TRUE;

	/*
	 *  Check comms supplies voltages
	 *  Check which supplies are being used
	 */

	/* Get state of PSM board i.e. what isolated supplies are physically present*/
	RetVal = IOManagerGetValue(IO_ID_COMMS_1_PRESENT, &comms1supplyPresent);
	if( comms1supplyPresent )
	{
		RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS1_INSTALLED, 1);
	}

	RetVal = IOManagerGetValue(IO_ID_COMMS_2_PRESENT, &comms2supplyPresent);
	if( comms2supplyPresent )
	{
		RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_INSTALLED, 1);
	}

	/* Get COMM voltages  */
	RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_COMMS_1_V_SENSE, &comms1Volts);
	RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_COMMS_2_V_SENSE, &comms2AuxVolts);
	RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_24V_AUX_V_SENSE, &auxVolts);

	RetVal = IOManagerGetValue(IO_ID_COMMS_1_EN, &comms1Enable);
	RetVal = IOManagerGetValue(IO_ID_COMMS_2_EN, &comms2Enable);
	RetVal = IOManagerGetValue(IO_ID_24V_PS_EN, &auxEnable);

	/*
	 * Check voltage present
	 */

	/* Check primary Voltage if fitted */
	if( primaryComms == COMMS_COMMS1 )
	{
		if (comms1Enable)
		{
			if ( comms1Volts < SUPPLY_VOLTAGE_PRESENT  )
			{
				RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS1_FAULT, 1);
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_RED, LU_TRUE);
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_GREEN, LU_FALSE);
			}
			else
			{
				RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS1_FAULT, 0);
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_RED, LU_FALSE);
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_GREEN, LU_TRUE);
			}
		}
	}

	if (comms2Enable || auxEnable)
	{
		if (SSGetFeatureRevisionMajor() == 0 &&
			SSGetFeatureRevisionMinor() == 1)
		{
			/* Combined AUX/COMM2 support */

			/* Check secondary voltage - COMMS2 if fitted else aux voltage */
			if ( comms2AuxVolts < SUPPLY_VOLTAGE_PRESENT  )
			{
				if (comms2Enable)
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_FAULT, 1);
				}
				else
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, 1);
				}
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_FALSE);
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_TRUE);
				comms2VoltOkay = LU_FALSE;
				auxVoltOkay = LU_FALSE;
			}
			else
			{
				if (comms2Enable)
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_FAULT, 0);
				}
				else
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, 0);
				}
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
				comms2VoltOkay = LU_TRUE;
				auxVoltOkay = LU_TRUE;
			}


			/*
			 * Check that if the AUX supply is being used, the status and power are okay
			 */
			if( secondaryComms == COMMS_AUX )
			{
				/* Aux so check aux supply status, power good and voltage flag */
				RetVal = IOManagerGetValue(IO_ID_24V_AUX_ST, &auxSupplyStatus);
				RetVal = IOManagerGetValue(IO_ID_24V_AUX_PWR_GOOD, &auxSupplyPwrGood);

				/* Work-around since ST is returned incorrectly on latest revision boards ? */
				auxSupplyStatus = LU_TRUE;

				if( (auxSupplyStatus == LU_TRUE) && (auxSupplyPwrGood == LU_TRUE) && (auxVoltOkay == LU_TRUE) )
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, LU_FALSE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
				}
				else
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_FALSE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_TRUE);
				}
			}
			else if(comms2VoltOkay == LU_FALSE)
			{
				/* Comms2 so check voltage flag only */
				RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, LU_TRUE);
			}
		}
		else
		{
			/* Independant AUX/COMMS2 control */

			if (comms2Enable)
			{
				/* Check secondary voltage - COMMS2 if fitted else aux voltage */
				if ( comms2AuxVolts < SUPPLY_VOLTAGE_PRESENT  )
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_FAULT, 1);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_FALSE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_TRUE);
					comms2VoltOkay = LU_FALSE;
				}
				else
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_FAULT, 0);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, LU_FALSE);
					comms2VoltOkay = LU_TRUE;
				}
			}

			if (auxEnable)
			{
				/* Check secondary voltage - COMMS2 if fitted else aux voltage */
				if ( auxVolts < SUPPLY_VOLTAGE_PRESENT  )
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, 1);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, 0);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, 1);
					auxVoltOkay = LU_FALSE;
				}
				else
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, 0);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, 1);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, 0);
					auxVoltOkay = LU_TRUE;
				}

				/* Aux so check aux supply status, power good and voltage flag */
				RetVal = IOManagerGetValue(IO_ID_24V_AUX_ST, &auxSupplyStatus);
				RetVal = IOManagerGetValue(IO_ID_24V_AUX_PWR_GOOD, &auxSupplyPwrGood);

				/* Work-around since ST is returned incorrectly on latest revision boards ? */
				auxSupplyStatus = LU_TRUE;

				if( (auxSupplyStatus == LU_TRUE) && (auxSupplyPwrGood == LU_TRUE) && (auxVoltOkay == LU_TRUE) )
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, LU_FALSE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, LU_FALSE);
				}
				else
				{
					RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_FAULT, LU_TRUE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, LU_FALSE);
					RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_RED, LU_TRUE);
				}
			}
		}
	}

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR InitCommsAux(void)
{
	SB_ERROR RetVal;
	lu_int32_t comms1supplyPresent,comms2supplyPresent;

	/* Get state of PSM board i.e. what isolated supplies are physically present*/
	RetVal = IOManagerGetValue(IO_ID_COMMS_1_PRESENT, &comms1supplyPresent);

	RetVal = IOManagerGetValue(IO_ID_COMMS_2_PRESENT, &comms2supplyPresent);

	/* Note: Comms supplies turned on with level not with pulse */

	/* Primary comms supply on by default if it is fitted */
	if( comms1supplyPresent && NVRAMOptPSMPtr->optionBitField & NVRAMOPTPSMDEF_COMMSPRIMARYFITTED_BM )
	{
		primaryComms = COMMS_COMMS1;
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_COMMS_GREEN, LU_TRUE);
		RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS1_ON, LU_TRUE);
	}
	else
	{
		primaryComms = COMMS_NOT_PRESENT;
	}

	/* Secondary comms supply on by default if it is fitted */

	if (SSGetFeatureRevisionMajor() == 0 &&
		SSGetFeatureRevisionMinor() == 1)
	{
		/* Combined AUX/COMM2 support */
		if( comms1supplyPresent && NVRAMOptPSMPtr->commsSecondaryFitted )
		{
			secondaryComms = COMMS_COMMS2;
			RetVal = IOManagerSetValue(IO_ID_24V_PS_EN, 0);
			RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_ON, 0);
			RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, 1);
		}
		else
			/* If secondary supply not fitted then 24V Aux supply on */
		{
			secondaryComms = COMMS_AUX;
			RetVal = IOManagerSetValue(IO_ID_COMMS_2_EN, 0);
			RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, 0);
			RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_ON, 1);
		}
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
	}
	else
	{
		/* Independant AUX / COMMS2 control */
		if( comms1supplyPresent && NVRAMOptPSMPtr->commsSecondaryFitted )
		{
			secondaryComms = COMMS_COMMS2;

			RetVal = IOManagerSetValue(IO_ID_VIRT_COMMS2_ON, LU_TRUE);
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, LU_TRUE);
		}
		else
		{
			RetVal = IOManagerSetValue(IO_ID_COMMS_2_EN, 0);
		}

		RetVal = IOManagerSetValue(IO_ID_VIRT_AUX_ON, LU_TRUE);
		RetVal = IOManagerSetValue(IO_ID_24V_PS_EN, LU_TRUE);
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_24VAUX_GREEN, LU_TRUE);
	}
	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *   Initiate a state array used for controlling each auxiliary power channel,
 *   and initialse the values
 *
 *   \param
 *   auxSupplyStateStr **statePtr - Pointer to the state array for all auxiliary
 *   power channels
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************/
 SB_ERROR initOperationalState(auxSupplyStateStr **statePtr)
{
	static auxSupplyStateStr auxSupplyState[COMMS_AUX];
	lu_uint8_t channelSelected;
	SB_ERROR RetVal;

	RetVal = SB_ERROR_NONE;

	/* Cycle through all PSUPPLY Channels _COMMS_PRIM to _LAST, Adjust array indexing to 0 */
	for(channelSelected = 0; channelSelected < (PSM_CH_PSUPPLY_LAST - PSM_CH_PSUPPLY_COMMS_PRIM); channelSelected++)
	{
		auxSupplyState[channelSelected].state = AUXSUPPLY_IDLE;
		auxSupplyState[channelSelected].time = 0;
		auxSupplyState[channelSelected].timeout = 0;
	}

	*statePtr = auxSupplyState;

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *   Update the current state information for a aux power channel
 *
 *   \param
 *   channel	- channel number to update
 *   state 		- the updated state
 *   time		- time of the change
 *   duration	- timeout for the operation, 0 indication no timeout
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************/
SB_ERROR setOperationalState(lu_uint8_t channel, lu_uint8_t state, lu_uint32_t time, lu_uint32_t duration)
{
	/* Data starts at index 0 in the array, so adjust channel number */
	lu_uint8_t auxChannel = channel - PSM_CH_PSUPPLY_COMMS_PRIM;

	auxSupplyStatePtr[auxChannel].state = state;
	auxSupplyStatePtr[auxChannel].time = time;
	auxSupplyStatePtr[auxChannel].timeout = duration;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *   Read the current state information for a aux power channel
 *
 *   \param
 *   channel	- channel number to update
 *   *state 	- pointer to return the current state
 *   *time		- ponter to time stamp of opeation
 *   *duration	- pointer to return timeout for the operation
 *
 *   \return SB_ERROR
 *
 ******************************************************************************/
SB_ERROR getOperationalState(lu_uint8_t channel, lu_uint8_t *state, lu_uint32_t *starttime, lu_uint32_t *duration)
{
	/* Data starts at index 0 in the array, so adjust channel number */
	lu_uint8_t auxChannel = channel - PSM_CH_PSUPPLY_COMMS_PRIM;

	*state = auxSupplyStatePtr[auxChannel].state;
	*starttime = auxSupplyStatePtr[auxChannel].time;
	*duration = auxSupplyStatePtr[auxChannel].timeout;

	return SB_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */
