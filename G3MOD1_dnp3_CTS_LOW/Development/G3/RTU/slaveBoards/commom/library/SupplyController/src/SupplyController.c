/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Supply Controller module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "IOManager.h"
#include "BoardIOMap.h"

#include "ModuleProtocol.h"
#include "NVRam.h"
#include "NVRAMDef.h"

#include "SupplyController.h"

#include "ACSupplyController.h"
#include "LogicSupplyController.h"
#include "MotorSupplyController.h"
#include "AuxSupplyController.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR SupplySelectHandler(SupplySelectStr *);
SB_ERROR SupplyOperateHandler(SupplyOperateStr *);
SB_ERROR SupplyCancelHandler(SupplyCancelStr *);

SB_ERROR SupplyConfig(SupplyConfigStr *);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static NVRAMUserPSMStr *nvramAppUserBlkPtr;

SupplyConfigStr	supplyParamsStr;
lu_bool_t 	suppliesInitialised;
lu_uint32_t	suppliesInitTime;

/*! List of supported message */
static const filterTableStr SupplyControllerrModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! IOManager commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_PSC_CH_SELECT_C             , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C            , LU_FALSE , 1      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C             , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CFG_POWER_SUPPLIES_C            , LU_FALSE , 0      }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR SupplyControlInit(void)
{

	lu_uint32_t time;
	SB_ERROR	retError;

	/* Indicate initialisation not done */
	suppliesInitialised = LU_FALSE;

	/* Get current time */
	time = STGetTime();
	suppliesInitTime = time;


	retError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_USER_A, (lu_uint8_t **)&nvramAppUserBlkPtr);
	if (retError != SB_ERROR_NONE)
	{
		/*
		 * Initialise configuration table with default values until config
		 * message received.
		 */
		nvramAppUserBlkPtr->motorSupplyEnabled 	     = MOTOR_SUPPLY_ENABLED;
		nvramAppUserBlkPtr->auxComms2SupplyEnabled   = COMMS2_AUX_SUPPLY_ENABLED;
		nvramAppUserBlkPtr->comms1SupplyEnabled	     = COMMS1_SUPPLY_ENABLED;

		/* Call function to indicate NVRAM should be updated */
		retError = NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );

	}

	/* Setup CAN Msg filter */
	return CANFramingAddFilter( SupplyControllerrModulefilterTable,
								SU_TABLE_SIZE(SupplyControllerrModulefilterTable,
										      filterTableStr)
							   );
}

lu_bool_t SupplyControlIsInitialised(void)
{
	return suppliesInitialised;
}

SB_ERROR SupplyControlCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	LU_UNUSED( time );

	retError = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_PSC_CH_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplySelectStr))
			{
				retError = SupplySelectHandler((SupplySelectStr *)msgPtr->msgBufPtr);
			}
			break;
			
		case MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplyOperateStr))
			{
				retError = SupplyOperateHandler((SupplyOperateStr *)msgPtr->msgBufPtr);
			}
			break;
			
		case MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplyCancelStr))
			{
				retError = SupplyCancelHandler((SupplyCancelStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;



	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_POWER_SUPPLIES_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplyConfigStr))
			{
				retError = SupplyConfig((SupplyConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;


	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}

SB_ERROR SupplyControlTick(void)
{
	SB_ERROR RetVal;
	static lu_uint8_t 	logicRepRate = LOGIC_REP_RATE,
						motorRepRate = MOTOR_REP_RATE,
						acRepRate    = AC_REP_RATE,
						auxRepRate   = AUX_REP_RATE;

	lu_uint32_t 		elapsedTime,
						time;



	/* Get current time */
	time = STGetTime();


	/*
	 * The initialisation can't happen until the IO manager has populated the data.
	 * Get time since the first execution of the tick function and only call the
	 * initialisation function if the required time has passed. Only call the initialisation once.
	 */

	if( suppliesInitialised == LU_FALSE )
	{
		elapsedTime = STElapsedTime( suppliesInitTime , time);
		/* If enough time has passed since the first tick then initialise  supplies */
		if( (elapsedTime > SUPPLIES_INIT_DELAY ) )
		{
			/* Initialise each of the supply controllers */
			RetVal = LogicSupplyControlInit();
			RetVal = ACSupplyControlInit();
			RetVal = MotorSupplyControlInit();
			RetVal = AuxSupplyControlInit();
			/* Set flag so the initialisation is not called again */
			suppliesInitialised = LU_TRUE;
		}
	}
	else
	{

		if( --logicRepRate == 0 )
		{
			RetVal = LogicSupplyControlTick();
			logicRepRate = LOGIC_REP_RATE;
		}

		if( --motorRepRate == 0 )
		{
			RetVal = MotorSupplyControlTick();
			motorRepRate = MOTOR_REP_RATE;
		}

		if( --acRepRate == 0 )
		{
			RetVal = AcSupplyControlTick();
			acRepRate = AC_REP_RATE;
		}

		if( --auxRepRate == 0 )
		{
			RetVal = AuxSupplyControlTick();
			auxRepRate = AUX_REP_RATE;
		}
	}
	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR SupplySelectHandler(SupplySelectStr *supplySelect)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= supplySelect->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(supplySelect->channel)
	{
	case PSM_CH_PSUPPLY_LOGIC:
		reply.status  	= LogicSupplySelectHandler(supplySelect);
		break;

	case PSM_CH_PSUPPLY_MOTOR:
		reply.status  	= MotorSupplySelectHandler(supplySelect);
		break;

	case PSM_CH_PSUPPLY_AC:
		reply.status  	= AcSupplySelectHandler(supplySelect);
		break;

	case PSM_CH_PSUPPLY_COMMS_PRIM:
	case PSM_CH_PSUPPLY_COMMS_SEC:
	case PSM_CH_PSUPPLY_24V_AUX:
		reply.status  	= AuxSupplySelectHandler(supplySelect);
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_PSC_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SupplyOperateHandler(SupplyOperateStr *supplyOperate)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= supplyOperate->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(supplyOperate->channel)
	{
	case PSM_CH_PSUPPLY_LOGIC:
		reply.status  	= LogicSupplyOperateHandler(supplyOperate);
		break;

	case PSM_CH_PSUPPLY_MOTOR:
		reply.status  	= MotorSupplyOperateHandler(supplyOperate);
		break;

	case PSM_CH_PSUPPLY_AC:
		reply.status  	= AcSupplyOperateHandler(supplyOperate);
		break;

	case PSM_CH_PSUPPLY_COMMS_PRIM:
	case PSM_CH_PSUPPLY_COMMS_SEC:
	case PSM_CH_PSUPPLY_24V_AUX:
		reply.status  	= AuxSupplyOperateHandler(supplyOperate);
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}


	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SupplyCancelHandler(SupplyCancelStr *supplyCancel)
{
	SupplyCancelReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 			= supplyCancel->channel;
	reply.status  			= REPLY_STATUS_OKAY;
	reply.peakCurrent		= 0;
	reply.operationID		= 0;

	switch(supplyCancel->channel)
	{
	case PSM_CH_PSUPPLY_LOGIC:
		reply.status  		= LogicSupplyCancelHandler(supplyCancel);
		break;
		
	case PSM_CH_PSUPPLY_MOTOR:
		reply.status  		= MotorSupplyCancelHandler(supplyCancel, &reply.peakCurrent, &reply.operationID);
		break;
		
	case PSM_CH_PSUPPLY_AC:
		reply.status  		= AcSupplyCancelHandler(supplyCancel);
		break;
		
	case PSM_CH_PSUPPLY_COMMS_PRIM:
	case PSM_CH_PSUPPLY_COMMS_SEC:
	case PSM_CH_PSUPPLY_24V_AUX:
		reply.status  		= AuxSupplyCancelHandler(supplyCancel);
		break;
		
	default:
		reply.status  		= REPLY_STATUS_ERROR;
		break;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R,
						sizeof(SupplyCancelReplyStr),
						(lu_uint8_t *)&reply
					  );
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SupplyConfig(SupplyConfigStr *supplyConfigPtr)
{
	ModReplyStr reply;
	SB_ERROR sbError;
	reply.channel 	= 0;
	reply.status  	= REPLY_STATUS_OKAY;

	/*
	 * Copy supply parameters to local structure
	 */

	nvramAppUserBlkPtr->motorSupplyEnabled			= supplyConfigPtr->motorSupplyEnabled;
	nvramAppUserBlkPtr->auxComms2SupplyEnabled		= supplyConfigPtr->auxComms2SupplyEnabled;
	nvramAppUserBlkPtr->comms1SupplyEnabled		    = supplyConfigPtr->comms1SupplyEnabled;

	/* Call function to indicate NVRAM should be updated */
	sbError = NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );


	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CFG_POWER_SUPPLIES_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*
 *********************** End of file ******************************************
 */
