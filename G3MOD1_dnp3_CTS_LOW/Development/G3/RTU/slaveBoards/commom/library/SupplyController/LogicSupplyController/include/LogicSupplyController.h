/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _LOGICSUPPLYCONTROLLER_INCLUDED
#define _LOGICSUPPLYCONTROLLER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "debug_frmwrk.h"

/* Define tracing to use printf to serial port */
#define TRACE		_printf

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

//#define DEBUG_OUTPUT

/* Pulse length to switch logic supply */
#define LOGIC_PULSE_MS						10

#define LOGIC_SUPPLY_ENABLED 				LU_TRUE
#define	LOGIC_SUPPLY_VOLTAGE_THRESHOLD		24
#define	LOGIC_SUPPLY_CURRENT_THRESHOLD		10
#define LOGIC_SUPPLY_VOLTS_HIGH				1.1
#define LOGIC_SUPPLY_VOLTS_LOW				0.9
#define LOGIC_SUPPLY_HOLD_DOWN_TIME			20
#define LOGIC_SUPPLY_HOLD_UP_TIME			20
#define MSEC								1000

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR LogicSupplyControlInit(void);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern lu_uint8_t LogicSupplySelectHandler(SupplySelectStr *);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern lu_uint8_t LogicSupplyOperateHandler(SupplyOperateStr *);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern lu_uint8_t LogicSupplyCancelHandler(SupplyCancelStr *);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR LogicSupplyControlTick(void);

#endif /* _LOGICSUPPLYCONTROLLER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
