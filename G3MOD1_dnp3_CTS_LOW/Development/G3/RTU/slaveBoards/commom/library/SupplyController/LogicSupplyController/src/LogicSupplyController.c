/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "LogicSupplyController.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSupplyControllerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void ResetFlagsTimer(void);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_uint32_t logicSelectTime,logicSelectPeriod;
lu_uint32_t logicOperateTime,logicOperatePeriod,logicOperateDelayTime;
lu_bool_t  	logicSelected;
lu_bool_t  	logicOperate;
lu_bool_t  	logicDelay;
lu_bool_t	logicSupplyOff;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR LogicSupplyControlInit(void)
{
	SB_ERROR RetVal;

	/* Initialise timers and flags */
	logicSelectTime		= 0;
	logicOperateTime	= 0;
	logicDelay			= 0;
	logicSelected		= LU_FALSE;
	logicOperate		= LU_FALSE;
	logicSupplyOff		= LU_FALSE;

	RetVal = SB_ERROR_NONE;

	/* Turn on logic supply by default */
	RetVal = IOManagerSetPulse(IO_ID_LOGIC_PS_ON, LOGIC_PULSE_MS);

	/* Turn on logic green LED */
	RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 1);
	/* Turn off logic supply red LED */
	RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);


	return RetVal;
}

lu_uint8_t LogicSupplySelectHandler(SupplySelectStr *logicSupplySelect)
{
	SB_ERROR retError;
	IO_STAT_LOR locRem;
	lu_uint8_t RetVal;

	/* Reset the timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
						SYSALC_SUPPLY_CONTROL_LOGIC_SELOP_TIMEOUT,
						0
					   );


	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	/* Check that an operation isn't in progress already */
	if ( logicOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check for valid state of local/remote switch */
	else if ( (locRem == IO_STAT_LOR_OFF) ||  (locRem == IO_STAT_LOR_INVALID) )
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( !(logicSupplySelect->local) && (locRem != IO_STAT_LOR_REMOTE)  )
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( (logicSupplySelect->local) && (locRem != IO_STAT_LOR_LOCAL) )
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( logicSelected == LU_TRUE)
	{
		ResetFlagsTimer();
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		logicSelectPeriod = (lu_uint32_t)(logicSupplySelect->SelectTimeout) * MSEC;
		logicSelectTime = STGetTime();

		/* Set logic supply operation selected flag to indicate an operation is pending */
		logicSelected = LU_TRUE;

		/* Turn on logic green LED to indicate power is still on */
//		retError = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 1);
		/* Turn on logic supply red LED indicate an operation is pending */
		retError = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 1);

		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

lu_uint8_t LogicSupplyOperateHandler(SupplyOperateStr *logicSupplyOperate)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	IO_STAT_LOR locRem;
	SB_ERROR retError;
	time = STGetTime();

	/* Check that supply not operating already */
	if( logicOperate == LU_TRUE )
	{
		logicSelected = LU_FALSE;
		logicSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that logic supply has been selected */
	else if ( logicSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( logicSelectTime , time );
		if ( elapsedTime >   logicSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			logicSelected = LU_FALSE;
			logicSelectTime = 0;

			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
								SYSALC_SUPPLY_CONTROL_LOGIC_SELOP_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Get state of physical local/remote switch. */
			retError = boardIOGetLocalRemoteStatus(&locRem);

			/* Check for valid state of local/remote switch */
			if ( (locRem == IO_STAT_LOR_OFF) ||  (locRem == IO_STAT_LOR_INVALID) )
			{
				ResetFlagsTimer();
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
			/* Check if local/remote switch is in the correct state */
			else if ( ((logicSupplyOperate->local) && (locRem == IO_STAT_LOR_LOCAL) ) || ( (!logicSupplyOperate->local) && (locRem == IO_STAT_LOR_REMOTE)) )
			{
				/* Set operate flag */
				logicOperate 	= LU_TRUE;
				logicSupplyOff 	= LU_TRUE;
				logicOperateTime= time;			/* Record the start time for the delay */
				logicDelay		= LU_TRUE;

				/*
				 * Turn off logic supply indefinitely after a set period if duration is zero
				 * If duration is not zero then turn off logic supply for a set period after the
				 * duration period.
				 */
				if ( logicSupplyOperate->supplyDuration > 0 )
				{
					/* Save time for power to be turned off */
					logicOperateDelayTime 	= (lu_uint32_t)(logicSupplyOperate->supplyDuration) * MSEC;
					logicOperatePeriod 		= LOGIC_SUPPLY_HOLD_DOWN_TIME * MSEC;
					#ifdef DEBUG_OUTPUT
					_printf("Turn off logic supply for[%d]ms\t",logicOperatePeriod);
					_printf("after[%d]ms\n\r",logicOperateDelayTime);
					#endif
				}
				else
				{
					/* Save time for power to be turned off */
					logicOperateDelayTime 	= LOGIC_SUPPLY_HOLD_UP_TIME * MSEC;
					logicOperatePeriod 		= 0;
					#ifdef DEBUG_OUTPUT
					_printf("Turn off logic supply after[%d]ms\n\r",logicOperateDelayTime);
					#endif
				}


				RetVal = REPLY_STATUS_OKAY;
			}
			else /* local/remote switch is not in the correct state so return an error */
			{
				ResetFlagsTimer();
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
		}
	}
	else  /* Logic supply has not been selected so return an error */
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

void ResetFlagsTimer(void)
{
	logicSelected 			= LU_FALSE;
	logicSelectTime 		= 0;
	logicOperate 			= LU_FALSE;
	logicOperateTime		= 0;
	logicSupplyOff 			= LU_FALSE;
	logicDelay				= LU_FALSE;
	logicOperateDelayTime	= 0;
}

lu_uint8_t LogicSupplyCancelHandler(SupplyCancelStr *logicSupplyCancel)
{
	lu_uint8_t RetVal;
	SB_ERROR retError;
	LU_UNUSED(logicSupplyCancel);

	/* Cancel operation */
	if ( (logicOperate == LU_TRUE) || (logicSelected == LU_TRUE) )
	{
		/* Turn on logic supply */
		retError = IOManagerSetPulse(IO_ID_LOGIC_PS_ON, LOGIC_PULSE_MS);
		/* Turn on logic green LED */
		retError = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 1);
		/* Turn off logic supply red LED */
		retError = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);
		RetVal = REPLY_STATUS_OKAY;
		logicSupplyOff = LU_FALSE;
	}
	else
	{
		/* Cancel logic supply operating flag */
		logicOperate = LU_FALSE;
		logicOperateTime = 0;
		/* Cancel logic supply operation selected flag */
		logicSelected = LU_FALSE;
		logicSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel logic supply operating flag */
	logicOperate = LU_FALSE;
	logicOperateTime = 0;
	/* Cancel logic supply operation selected flag */
	logicSelected = LU_FALSE;
	logicSelectTime = 0;

	return RetVal;
}

SB_ERROR LogicSupplyControlTick(void)
{
	SB_ERROR RetVal;
	lu_int32_t lsCurrent,batteryVolts,rawDcVolts,dcVolts;
	lu_int32_t logicSupplyPwrGood;
	lu_uint32_t elapsedTime, time;

	/* get current time */
	time = STGetTime();

	/* If selected then check for select time exceeded */
	elapsedTime = STElapsedTime( logicSelectTime , time );
	if ( (logicSelected == LU_TRUE) && ( elapsedTime  >   logicSelectPeriod ) )
	{
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
							SYSALC_SUPPLY_CONTROL_LOGIC_SELOP_TIMEOUT,
							0
						   );

		/* Turn off logic supply red LED */
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);
		/* Cancel logic supply operating flag */
		logicSelected = LU_FALSE;
		logicSelectTime = 0;
	}

	/* Get the time since the operate command was received */
	elapsedTime = STElapsedTime( logicOperateTime , time );

	/* If operating then check for times finished */
	if( logicOperate == LU_TRUE )
	{

		if( (elapsedTime  > logicOperateDelayTime) && (logicDelay == LU_TRUE) )
		{
			/* Turn off logic supply */
			RetVal = IOManagerSetPulse(IO_ID_LOGIC_PS_OFF, LOGIC_PULSE_MS);

			logicSelected = LU_FALSE;
			/* Turn off logic green LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 0);
			/* Turn on logic supply red LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);
			logicDelay		= LU_FALSE;
			logicSupplyOff  = LU_TRUE;
			/*
			 * If the operation duration is greater than zero turn off the logic supply
			 * for the defined period then turn back on.
			 * If the duration is zero turn off the logic supply and leave it off
			 */
			if( logicOperatePeriod > 0 )
			{
				/* Reset timer for operate period after which the logic supply is restored */
				logicOperateTime = time;

				#ifdef DEBUG_OUTPUT
				_printf("Logic supply off for[%d]ms\n\r",logicOperatePeriod);
				#endif
			}
			else
			{
				/* Cancel logic supply operating flag as the operation is finished */
				ResetFlagsTimer();
				logicOperate = LU_FALSE;
				logicOperateTime = 0;
				logicSupplyOff  = LU_TRUE;
				#ifdef DEBUG_OUTPUT
				_printf("Logic supply off indefinitely \n\r");
				#endif
			}
		}
		else if( (elapsedTime  > logicOperatePeriod) && (logicDelay == LU_FALSE))
		{
			/* Turn on logic supply */
			RetVal = IOManagerSetPulse(IO_ID_LOGIC_PS_ON, LOGIC_PULSE_MS);

			/* Turn on logic supply Green LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 1);
			/* Turn off logic supply red LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);
			/* Cancel logic supply operating flag */
			ResetFlagsTimer();
			logicOperate = LU_FALSE;
			logicSupplyOff = LU_FALSE;
			logicOperateTime = 0;
			#ifdef DEBUG_OUTPUT
			_printf("logic supply on\n\r");
			#endif

		}
	}


	/* Get logic supply current */
	RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_LOGIC_I_SENSE, &lsCurrent);

	/* Get Main DC voltage */
	RetVal = IOManagerGetCalibratedUnitsValue( IO_ID_BAT_V_SENSE, &batteryVolts);

	RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_36V_RAW_DC_V_SENSE, &rawDcVolts);

	if( batteryVolts > rawDcVolts)
	{
		dcVolts = batteryVolts;
	}
	else
	{
		dcVolts = rawDcVolts;
	}


	/*
	 * If the logic supply is supposed to be on then check the status
	 */
	if( logicSupplyOff == LU_FALSE)
	{
		logicSupplyPwrGood = 1;
		//RetVal = IOManagerGetValue(IO_ID_LOGIC_PWR_GOOD, &logicSupplyPwrGood);
		if(logicSupplyPwrGood == 0)
		{
			/* Turn off logic green LED  */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 0);
			/* Turn on logic supply red LED to indicate a fault */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 1);
		}
		else
		{
			/* Turn on logic green LED  */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_GREEN, 1);
			/* Turn off logic supply red LED to indicate a fault */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_LOGIC_RED, 0);
		}

	}

	return RetVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
