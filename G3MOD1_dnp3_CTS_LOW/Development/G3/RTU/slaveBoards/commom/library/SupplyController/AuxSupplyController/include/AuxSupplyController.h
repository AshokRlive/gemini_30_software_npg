/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   27/07/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _AUXSUPPLYCONTROLLER_INCLUDED
#define _AUXSUPPLYCONTROLLER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* Pulse length to switch supply */
//#define AUX_PULSE_MS						1

#define AUX_SUPPLY_ENABLED 				LU_TRUE
#define	AUX_SUPPLY_VOLTAGE_THRESHOLD		24
#define	AUX_SUPPLY_CURRENT_THRESHOLD		10
#define AUX_SUPPLY_VOLTS_HIGH				1.25
#define AUX_SUPPLY_VOLTS_LOW				0.75
#define AUX_SUPPLY_STATUS_TIME				10
#define MSEC								1000
#define SUPPLY_VOLTAGE_PRESENT				1000

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    COMMS_PRIMARY = 0,
    COMMS_SECONDARY,
    COMMS_NONE
}CommsSelected;

typedef enum
{
	COMMS_NOT_PRESENT = 0,
	COMMS_COMMS1,
    COMMS_COMMS2,
    COMMS_AUX
}CommsSupplies;

typedef enum
{
    AUXSUPPLY_IDLE = 0,
    AUXSUPPLY_SELECTED,
    AUXSUPPLY_OPERATING,
    AUXSUPPLY_CANCEL
}CommsState;


typedef struct auxSupplyStateDef
{
	lu_uint8_t state;
	lu_uint32_t time;
	lu_uint32_t timeout;
} auxSupplyStateStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR AuxSupplyControlInit(void);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern lu_uint8_t AuxSupplySelectHandler(SupplySelectStr *);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern lu_uint8_t AuxSupplyOperateHandler(SupplyOperateStr *);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern lu_uint8_t AuxSupplyCancelHandler(SupplyCancelStr *);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR AuxSupplyControlTick(void);

#endif /* _LOGICSUPPLYCONTROLLER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
