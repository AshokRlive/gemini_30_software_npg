/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "ACSupplyController.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSupplyControllerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static void ResetFlagsTimer(void);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_uint32_t acSelectTime,acSelectPeriod;
lu_uint32_t acOperateTime,acOperatePeriod;
lu_bool_t  acSelected;
lu_bool_t  acOperate;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ACSupplyControlInit(void)
{
	SB_ERROR RetVal;

	/* Initialise timers and flags */
	acSelectTime		= 0;
	acOperateTime		= 0;
	acSelected			= LU_FALSE;
	acOperate			= LU_FALSE;

	RetVal = SB_ERROR_NONE;

	return RetVal;
}

lu_uint8_t AcSupplySelectHandler(SupplySelectStr *acSupplySelect)
{
	SB_ERROR retError;
	IO_STAT_LOR locRem;
	lu_uint8_t RetVal;

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
						SYSALC_SUPPLY_CONTROL_AC_SELOP_TIMEOUT,
						0
					   );

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	/* Check that it isn't operating already */
	if ( acOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check for valid state of local/remote switch */
	else if ( (locRem == IO_STAT_LOR_OFF) ||  (locRem == IO_STAT_LOR_INVALID) )
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( !(acSupplySelect->local) && (locRem != IO_STAT_LOR_REMOTE)  )
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( (acSupplySelect->local) && (locRem != IO_STAT_LOR_LOCAL) )
	{
		ResetFlagsTimer();
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( acSelected == LU_TRUE)
	{
		ResetFlagsTimer();
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		acSelectPeriod = (lu_uint32_t)(lu_uint32_t)(acSupplySelect->SelectTimeout) * MSEC;
		acSelectTime = STGetTime();

		/* Set ac supply operation selected flag */
		acSelected = LU_TRUE;

		/* Turn on AC green LED to indicate power is still on */
		//retError = IOManagerSetValue(IO_ID_LED_CTRL_AC_GREEN, 1);
		/* Turn on AC supply red LED indicate an operation is pending */
		//retError = IOManagerSetValue(IO_ID_LED_CTRL_AC_RED, 1);

		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

lu_uint8_t AcSupplyOperateHandler(SupplyOperateStr *acSupplyOperate)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	IO_STAT_LOR locRem;
	SB_ERROR retError;
	time = STGetTime();

	/* Check that supply not operating already */
	if( acOperate == LU_TRUE )
	{
		acSelected = LU_FALSE;
		acSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that ac supply has been selected */
	else if ( (acSelected == LU_TRUE) && (acOperate != LU_TRUE) )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( acSelectTime , time );
		if ( elapsedTime >   acSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			acSelected = LU_TRUE;
			acSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
								SYSALC_SUPPLY_CONTROL_AC_SELOP_TIMEOUT,
								0
							   );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Get state of physical local/remote switch. */
			retError = boardIOGetLocalRemoteStatus(&locRem);

			/* Check for valid state of local/remote switch */
			if ( (locRem == IO_STAT_LOR_OFF) ||  (locRem == IO_STAT_LOR_INVALID) )
			{
				ResetFlagsTimer();
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
			/* Check if local/remote switch is in the correct state */
			else if ( ((acSupplyOperate->local) && (locRem == IO_STAT_LOR_LOCAL) ) || ( (!acSupplyOperate->local) && (locRem == IO_STAT_LOR_REMOTE)) )
			{
				/* Turn off ac supply indefinitely if duration is zero */
				if ( acSupplyOperate->supplyDuration > 0 )
				{
					/* Save time for power to be turned back on */
					acOperatePeriod = (lu_uint32_t)(acSupplyOperate->supplyDuration) * MSEC;
					acOperateTime = time;
					/* Set operate flag */
					acOperate = LU_TRUE;
				}

				/* Turn off ac supply */
				acSelected = LU_FALSE;
				retError = IOManagerSetValue(IO_ID_VIRT_AC_OFF, 1);

				RetVal = REPLY_STATUS_OKAY;
			}
			else /* local/remote switch is not in the correct state so return an error */
			{
				acSelected = LU_FALSE;
				acSelectTime = 0;
				acOperate = LU_FALSE;
				acOperateTime = 0;
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
		}
	}
	else /* AC supply has not been selected so return an error */
	{
		acSelected = LU_FALSE;
		acSelectTime = 0;
		acOperate = LU_FALSE;
		acOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

void ResetFlagsTimer(void)
{
	acSelected = LU_FALSE;
	acSelectTime = 0;
	acOperate = LU_FALSE;
	acOperateTime = 0;
}

lu_uint8_t AcSupplyCancelHandler(SupplyCancelStr *acSupplyCancel)
{
	SB_ERROR retError;
	lu_uint8_t RetVal;
	LU_UNUSED(acSupplyCancel);

	/* Cancel operation */
	if ( (acOperate == LU_TRUE) || (acSelected == LU_TRUE) )
	{
		/* Turn on ac supply */
 		retError = IOManagerSetValue(IO_ID_VIRT_AC_OFF, 0);

		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel ac supply operating flag */
	acOperate = LU_FALSE;
	acOperateTime = 0;
	/* Cancel ac supply operation selected flag */
	acSelected = LU_FALSE;
	acSelectTime = 0;

	return RetVal;
}

SB_ERROR AcSupplyControlTick(void)
{
	SB_ERROR RetVal;
//	lu_int32_t acVolts;
	lu_uint32_t elapsedTime, time;
	time = STGetTime();


	/* If selected then check for select time exceeded */
	elapsedTime = STElapsedTime( acSelectTime , time );
	if ( (acSelected == LU_TRUE) && ( elapsedTime  >   acSelectPeriod ) )
	{
		/* Set select timeout error */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
							SYSALC_SUPPLY_CONTROL_AC_SELOP_TIMEOUT,
							0
						   );

		/* Cancel ac supply operating flag */
		acSelected = LU_FALSE;
		acSelectTime = 0;
	}


	/* If operating then check for time finished */
	elapsedTime = STElapsedTime( acOperateTime , time );
	if ( ( acOperate == LU_TRUE) && ( elapsedTime > acOperatePeriod) )
	{
		/* Turn on ac supply */
		RetVal = IOManagerSetValue(IO_ID_VIRT_AC_OFF, 0);

		acOperate = LU_FALSE;
		acOperateTime = 0;
	}


	/* Get ac supply voltage - Only via serial to OVP */
//	RetVal = IOManagerGetValue(IO_ID_AC_V_SENSE, &acVolts);
	/* Check for over or under voltage */
//	RetVal = IOManagerSetValue(IO_ID_VIRT_AC_OVERVOLTAGE, 1);
//	RetVal = IOManagerSetValue(IO_ID_VIRT_AC_UNDERVOLTAGE, 1);

	RetVal = SB_ERROR_NONE;

	return RetVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
