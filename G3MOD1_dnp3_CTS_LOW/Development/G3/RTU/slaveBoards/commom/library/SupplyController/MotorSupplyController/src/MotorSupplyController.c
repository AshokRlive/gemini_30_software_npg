/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "MotorSupplyController.h"
#include "../../../BatteryCharger/include/BatteryToFsmInterface.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSupplyControllerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
static void ResetFlagsTimer(void);
static void ResetFlagsStruct(lu_bool_t);
static void MotorSupplyOff(void);

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern lu_bool_t 			SwEventParamsInProgress;


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_uint32_t				motorSelectTime,motorSelectPeriod;
lu_uint32_t				motorOperateTime,motorOperatePeriod;
MSMotorOperationsStr	msMotorOperationParameters[MAX_OPERATIONS];
lu_bool_t  				motorSelected;
lu_bool_t  				motorOperate;
lu_bool_t				motorRetriggered;
lu_int32_t 				motorVoltageLimit;
lu_int32_t 				motorCurrent,peakMotorCurrent,motorCurrentLimit;
lu_uint32_t				motorCurrentLimitTime;
lu_uint16_t				motorSelectID,motorOperateID,motorCancelID;
lu_uint16_t				operationInProgressID;
MS_OPERATION			operationInProgressIndex;
lu_bool_t  				motorCurrentExceeded;
lu_int32_t				motorCurrentExceededStart;

static lu_float32_t motorVoltsScaled = 0,batteryVoltsScaled = 0;
static lu_uint8_t averageCount = 0;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR MotorSupplyControlInit(void)
{
	SB_ERROR RetVal;
	lu_uint8_t i;

	for( i=0 ; i<MAX_OPERATIONS ; i++)
	{
		ResetFlagsStruct(i);
	}

	/* Initialise timers and flags */
	ResetFlagsTimer();


	RetVal = SB_ERROR_NONE;

	/* Turn off motor supply by default */
	RetVal = IOManagerSetPulse(IO_ID_MOT_PS_OFF, MOTOR_PULSE_MS);

	/* Set relay on virtual point */
	RetVal = IOManagerSetValue(IO_ID_VIRT_MS_RELAY_ON, 0);

	/* Turn off motor supply green LED */
	RetVal = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_GREEN, 0);
	/* Turn off motor supply red LED */
	RetVal = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_RED, 0);

	return RetVal;
}

lu_uint8_t MotorSupplySelectHandler(SupplySelectStr *motorSupplySelect)
{
	SB_ERROR retError;
	lu_int32_t batteryLow,batteryDisconnected,batteryFault,batteryShutdownLimitReached;
	IO_STAT_LOR locRem;
	lu_uint8_t RetVal;

	/* Save the select operation ID to compare with the operate command */
	motorSelectID = motorSupplySelect->operationID;

	#ifdef DEBUG_MOTOR_SUPPLY
	statepos;
	_printf("CAN message received\t - Motor Supply Select\tID = %4x\tIn progress ID = %4x\n\n\r", motorSelectID, operationInProgressID);
	#endif

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	retError = IOManagerGetValue(IO_ID_VIRT_BT_DISCONNECTED, &batteryDisconnected);
	retError = IOManagerGetValue(IO_ID_VIRT_BT_LOW, &batteryLow);
	retError = IOManagerGetValue(IO_ID_VIRT_BT_FAULT, &batteryFault);
	retError = IOManagerGetValue(IO_ID_VIRT_BT_CH_SHUTDOWN, &batteryShutdownLimitReached);

	/* Reset the local remote error point */
	retError = IOManagerSetValue(IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT, 0);

	/* Reset the select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
									SYS_ALARM_SEVERITY_ERROR,
									SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
									SYSALC_SUPPLY_CONTROL_MOTOR_SELOP_TIMEOUT,
									0
								   );


	/* Check if the motor is operating already */
	if ( motorOperate == LU_TRUE)
	{
		/* Already operating so this is a re-trigger */
		motorRetriggered	= LU_TRUE;
	}

	/* Check for valid state of local/remote switch */
	if ( (locRem == IO_STAT_LOR_OFF) ||  (locRem == IO_STAT_LOR_INVALID) )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( !(motorSupplySelect->local) && (locRem != IO_STAT_LOR_REMOTE)  )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( (motorSupplySelect->local) && (locRem != IO_STAT_LOR_LOCAL) )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	// KMG We now allow motor operation when low, removed || batteryLow from the following if
	// We do not allow motor operation when shutdown Limit has been reached
	else if(batteryDisconnected || batteryFault || batteryShutdownLimitReached)
	{
		/* Battery problem so return error */
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_BATTERY_SUPPLY_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_BATTERY_SUPPLY_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( motorSelected == LU_TRUE)
	{
		/* Already selected but not operating so this is an error */
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	/* No errors detected so do select */
	else
	{
		/* Save the operation parameters */
		msMotorOperationParameters[motorRetriggered].channel				= 0;
		msMotorOperationParameters[motorRetriggered].local					= motorSupplySelect->local;
		msMotorOperationParameters[motorRetriggered].OperationID			= motorSupplySelect->operationID;
		msMotorOperationParameters[motorRetriggered].motorSelected			= LU_TRUE;
		msMotorOperationParameters[motorRetriggered].motorSelectStartTime	= STGetTime();
		msMotorOperationParameters[motorRetriggered].motorSelectTimeout		= (lu_uint32_t)(motorSupplySelect->SelectTimeout) * MSEC;;
		msMotorOperationParameters[motorRetriggered].motorSelectEndTime		= 0;
		msMotorOperationParameters[motorRetriggered].motorOperateStartTime	= 0;
		msMotorOperationParameters[motorRetriggered].motorOperateDuration	= 0;
		msMotorOperationParameters[motorRetriggered].motorOperateEndTime	= 0;
		msMotorOperationParameters[motorRetriggered].peakMotorCurrent		= 0;
		msMotorOperationParameters[motorRetriggered].inProgress				= 0;


		/* Save the period after which the select expires */
		motorSelectPeriod = (lu_uint32_t)(motorSupplySelect->SelectTimeout) * MSEC;
		motorSelectTime = STGetTime();

		/* Set motor supply operation selected flag */
		motorSelected = LU_TRUE;

		if( motorRetriggered == LU_FALSE)
		{
			/* Turn off motor green LED */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_GREEN, 0);
			/* Turn off motor supply red LED */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_RED, 0);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("Motor Supply Selected\t - Motor Supply Select\tID = %4x\tIn progress ID = %4x\n\n\r", motorSelectID, operationInProgressID);
		#endif

		RetVal = REPLY_STATUS_OKAY;
	}

	/* If local/remote error detected set the virtual point to indicate error */
	if( RetVal == REPLY_STATUS_LOCAL_REMOTE_ERROR)
	{
		/* Set the local remote error point */
		retError = IOManagerSetValue(IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT, 1);
	}
	return RetVal;
}

lu_uint8_t MotorSupplyOperateHandler(SupplyOperateStr *motorSupplyOperate)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	lu_int32_t batteryLow,batteryDisconnected,batteryFault;
	IO_STAT_LOR locRem;
	SB_ERROR retError;
	time = STGetTime();

	motorOperateID = motorSupplyOperate->operationID;

	#ifdef DEBUG_MOTOR_SUPPLY
	statepos;
	_printf("CAN message received\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
	#endif

	retError = IOManagerGetValue(IO_ID_VIRT_BT_DISCONNECTED, &batteryDisconnected);
	retError = IOManagerGetValue(IO_ID_VIRT_BT_LOW, &batteryLow);
	retError = IOManagerGetValue(IO_ID_VIRT_BT_FAULT, &batteryFault);

	if( motorRetriggered == LU_FALSE)
	{
		/* Reset the over current error point */
		retError = IOManagerSetValue(IO_ID_VIRT_MS_OVERCURRENT, 0);

		/* Reset the local remote error point */
		retError = IOManagerSetValue(IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT, 0);

		/* Reset the timeout error */
		SysAlarmSetLogEvent(LU_FALSE,
										SYS_ALARM_SEVERITY_ERROR,
										SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
										SYSALC_SUPPLY_CONTROL_MOTOR_SELOP_TIMEOUT,
										0
									   );

		/* Reset the motor on timeout error */
		SysAlarmSetLogEvent(LU_FALSE,
											SYS_ALARM_SEVERITY_ERROR,
											SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
											SYSALC_SUPPLY_CONTROL_MOTOR_ON_TIMEOUT,
											0
										   );

		SwEventParamsInProgress = LU_FALSE;

		/*
		 * This is the first operation so make this the one in progress
		 */
		msMotorOperationParameters[motorRetriggered].inProgress = LU_TRUE;
	}

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	/*
	 * Check that the operate command has the same operation ID
	 *  as the previously received select command
	 */
	if( motorSelectID != motorOperateID )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_SELECT_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	/* Check that motor supply not operating already if no re-trigger received */
	else if( (motorOperate == LU_TRUE) &&  (motorRetriggered == LU_FALSE) )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_ALREADY_OPERATING_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that battery state is good */
	// KMG We now allow motor operation when low, removed || batteryLow from the following if
	else if(batteryDisconnected || batteryFault)
	{
		/* Battery problem so return error */
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_BATTERY_SUPPLY_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_BATTERY_SUPPLY_ERROR;
	}
	/* Check for valid state of local/remote switch */
	else if ( (locRem == IO_STAT_LOR_OFF) ||  (locRem == IO_STAT_LOR_INVALID) )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_LOCAL_REMOTE_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check if local/remote switch is in the correct state */
	else  if ( !(motorSupplyOperate->local) && (locRem != IO_STAT_LOR_REMOTE)  )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_LOCAL_REMOTE_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that local/remote switch matches the select command */
	else  if ( (motorSupplyOperate->local) && (locRem != IO_STAT_LOR_LOCAL) )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_LOCAL_REMOTE_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that motor supply has been selected */
	else if ( motorSelected != LU_TRUE )
	{
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_SELECT_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	/* Get time elapsed since the select received and check that the timeout has not been exceeded */
	else if ( (elapsedTime = STElapsedTime( motorSelectTime , time )) > motorSelectPeriod)
	{
		/* Set the timeout error */
		SysAlarmSetLogEvent(LU_TRUE,
											SYS_ALARM_SEVERITY_ERROR,
											SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
											SYSALC_SUPPLY_CONTROL_MOTOR_SELOP_TIMEOUT,
											0
										   );
		if( motorRetriggered == LU_FALSE)
		{
			ResetFlagsTimer();
			ResetFlagsStruct(motorRetriggered);
		}
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("REPLY_STATUS_SELECT_ERROR\t - Motor Supply Operate\tID = %4x\tIn progress ID = %4x\n\n\r", motorOperateID, operationInProgressID);
		#endif
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/*
		 * The operate has been checked and can be implemented
		 */
		SwEventParamsInProgress = LU_TRUE;

		if( motorRetriggered == LU_FALSE)
		{
			/* Reset peak current value */
			peakMotorCurrent = 0;
			/* Reset Peak current channel value */
			RetVal = IOManagerSetValue(IO_ID_VIRT_MS_PEAK_HOLD_CURRENT, 0);

			/* Reset over-current virtual points */
			RetVal = IOManagerSetValue(IO_ID_VIRT_MS_OVERCURRENT_LIMIT, LU_FALSE);
			RetVal = IOManagerSetValue(IO_ID_VIRT_MS_OVERCURRENT, LU_FALSE);

			/* Reset the voltage averaging values */
			motorVoltsScaled = 0;
			batteryVoltsScaled = 0;
			averageCount = 0;

			/* Turn on motor supply for the required duration */
//SRM
			/*Tell the Battery Manager FSM*/
			SendMotorSupplyOnEventToFSM();
			/* Update the time stamp for starting the operation */
			time = STGetTime();
			retError = IOManagerSetPulse(IO_ID_MOT_PS_ON, MOTOR_PULSE_MS);
			#ifdef DEBUG_MOTOR_SUPPLY
			messagepos;
			_printf("Motor Supply On\n\n\r" );
			#endif

			/* Set relay on virtual point */
			RetVal = IOManagerSetValue(IO_ID_VIRT_MS_RELAY_ON, 1);
			/* Turn on motor green LED */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_GREEN, 1);
			/* Turn off motor supply red LED */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_RED, 0);
			SwEventParamsInProgress = LU_TRUE;
			/* Set the operation ID */
			operationInProgressIndex = motorRetriggered;
			operationInProgressID = motorOperateID;
			/* Save time at which the power is to be turned off */
			motorOperatePeriod = (lu_uint32_t)(motorSupplyOperate->supplyDuration) * MSEC ;
			motorOperateTime = time;
			motorCurrentLimit = motorSupplyOperate->currentLimit;
			motorCurrentLimitTime = motorSupplyOperate->currentLimitDuration;

			/*
			 * Save the motor supply ID, start time and duration
			 */
			msMotorOperationParameters[motorRetriggered].OperationID			= motorOperateID;
			msMotorOperationParameters[motorRetriggered].motorOperateStartTime 	= time;
			msMotorOperationParameters[motorRetriggered].motorOperateDuration  	= (lu_uint32_t)(motorSupplyOperate->supplyDuration) * MSEC;
			msMotorOperationParameters[motorRetriggered].motorOperateEndTime 	= msMotorOperationParameters[motorRetriggered].motorOperateDuration + time;
			msMotorOperationParameters[motorRetriggered].inProgress 			= LU_TRUE;
			msMotorOperationParameters[motorRetriggered].motorCurrentLimit		= motorSupplyOperate->currentLimit;
			msMotorOperationParameters[motorRetriggered].motorCurrentLimitTime	= motorSupplyOperate->currentLimitDuration;
		}
		else
		{
			/*
			 * Save the motor supply ID, start time and duration
			 */
			msMotorOperationParameters[motorRetriggered].OperationID			= motorOperateID;
			msMotorOperationParameters[motorRetriggered].motorOperateStartTime 	= time;
			msMotorOperationParameters[motorRetriggered].motorOperateDuration  	= (lu_uint32_t)(motorSupplyOperate->supplyDuration) * MSEC;
			msMotorOperationParameters[motorRetriggered].motorOperateEndTime 	= msMotorOperationParameters[motorRetriggered].motorOperateDuration + time;
			msMotorOperationParameters[motorRetriggered].motorCurrentLimit		= motorSupplyOperate->currentLimit;
			msMotorOperationParameters[motorRetriggered].motorCurrentLimitTime	= motorSupplyOperate->currentLimitDuration;
			motorCurrentLimit = msMotorOperationParameters[MS_OP_ORIGINAL].motorCurrentLimit +
								msMotorOperationParameters[MS_OP_RETRIGGER].motorCurrentLimit;

			/*
			 * Check to see if the new operation has a duration that will
			 * end after the current operation. If it does then make the new
			 * operation the current operation and reset the operate duration
			 */
			if(		(time  + ( (lu_uint32_t)(motorSupplyOperate->supplyDuration) * MSEC) )
					>=
					(motorOperateTime + motorOperatePeriod)   )
			{
				/*
				 * Make the new operation the current operation
				 * and update the time to finish
				 */
				operationInProgressIndex 	= motorRetriggered;
				operationInProgressID 		= motorOperateID;
				motorOperatePeriod 			= (lu_uint32_t)(motorSupplyOperate->supplyDuration) * MSEC ;
				motorOperateTime 			= time;
				/*
				 * The new operation will finish last so make this the one in progress
				 */
				msMotorOperationParameters[motorRetriggered].inProgress = LU_TRUE;

			}
		}
		/* Set operate flag */
		motorOperate = LU_TRUE;
		motorSelected = LU_FALSE;
		RetVal = REPLY_STATUS_OKAY;
		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("Motor Supply Operating\t - Motor Supply Operate\tID = %4x\tIn Progress ID = %4x\t - Start time = %d Duration = %d\n\n\r",
							motorOperateID, operationInProgressID, motorOperateTime/MSEC, motorOperatePeriod/MSEC);
		#endif

	}

	return RetVal;
}

lu_uint8_t MotorSupplyCancelHandler(SupplyCancelStr *motorSupplyCancel, lu_int32_t *peak, lu_uint16_t *id)
{
	lu_uint8_t RetVal;
	LU_UNUSED(motorSupplyCancel);
	lu_uint8_t i;
	lu_uint32_t time;
	time = STGetTime();

	motorCancelID = motorSupplyCancel->operationID;
	#ifdef DEBUG_MOTOR_SUPPLY
	statepos;
	_printf("CAN message received\t - Motor Supply Cancel\tID = %4x\tIn progress ID = %4x\n\n\r",motorCancelID , operationInProgressID );
	#endif

	/* Cancel one or all operations operation */
	if ( (motorOperate == LU_TRUE) || (motorSelected == LU_TRUE) )
	{
		if(motorCancelID == 0)
		{
			/* Cancel ID is zero so all operations are to be cancelled and the motor turned off */

			/* Turn off motor supply */
			MotorSupplyOff();

			/* Clear the motor operation parameters */
			for( i = 0 ; i < MAX_OPERATIONS; i++)
			{
				ResetFlagsTimer();
				ResetFlagsStruct(i);
			}

			/* Reset the flag that indicates a re-trigger */
			motorRetriggered = LU_FALSE;

			/* No operations in progress so reset the In Progress ID */
			operationInProgressID = 0;

			#ifdef DEBUG_MOTOR_SUPPLY
			statepos;
			_printf("Motor Supply Cancelled\t - Motor Supply Cancel\tID = %4x\tIn progress ID = %4x\t - Time = %d\n\n\r",motorCancelID, operationInProgressID,time/MSEC );
			messagepos;
			_printf("Motor Supply Off\n\n\r" );
			#endif
		}
		else if( motorCancelID == msMotorOperationParameters[MS_OP_ORIGINAL].OperationID )
		{
			/* Clear the motor operation parameters */
			ResetFlagsStruct(0);

			#ifdef DEBUG_MOTOR_SUPPLY
			statepos;
			_printf("Motor Supply Cancelled\t - Motor Supply Cancel\tID = %4x\tIn progress ID = %4x\t - Time = %d\n\n\r",motorCancelID, operationInProgressID,time/MSEC );
			#endif

			if( msMotorOperationParameters[MS_OP_RETRIGGER].OperationID == 0 )
			{
				/* Turn off motor supply */
				MotorSupplyOff();
				ResetFlagsTimer();
				/* Reset the flag that indicates a re-trigger */
				motorRetriggered = LU_FALSE;
				/* No operations in progress so reset the In Progress ID */
				operationInProgressID = 0;
				#ifdef DEBUG_MOTOR_SUPPLY
				messagepos;
				_printf("Motor Supply Off\n\n\r" );
				#endif
			}
			else
			{
				motorOperatePeriod  	= msMotorOperationParameters[MS_OP_RETRIGGER].motorOperateDuration;
				motorOperateTime 		= msMotorOperationParameters[MS_OP_RETRIGGER].motorOperateStartTime;
				operationInProgressID 	= msMotorOperationParameters[MS_OP_RETRIGGER].OperationID;
				motorCurrentLimit		= msMotorOperationParameters[MS_OP_RETRIGGER].motorCurrentLimit;
			}
		}
		else if( motorCancelID == msMotorOperationParameters[MS_OP_RETRIGGER].OperationID )
		{
			/* Clear the motor operation parameters */
			ResetFlagsStruct(1);

			#ifdef DEBUG_MOTOR_SUPPLY
			statepos;
			_printf("Motor Supply Cancelled\t - Motor Supply Cancel\tID = %4x\tIn progress ID = %4x\t - Time = %d\n\n\r",motorCancelID, operationInProgressID,time/MSEC );
			#endif


			if( msMotorOperationParameters[MS_OP_ORIGINAL].OperationID == 0 )
			{
				/* Turn off motor supply */
				MotorSupplyOff();
				ResetFlagsTimer();
				/* Reset the flag that indicates a re-trigger */
				motorRetriggered = LU_FALSE;
				/* No operations in progress so reset the In Progress ID */
				operationInProgressID = 0;
				#ifdef DEBUG_MOTOR_SUPPLY
				messagepos;
				_printf("Motor Supply Off\n\n\r" );
				#endif
			}
			else
			{
				motorOperatePeriod  	= msMotorOperationParameters[MS_OP_ORIGINAL].motorOperateDuration;
				motorOperateTime 		= msMotorOperationParameters[MS_OP_ORIGINAL].motorOperateStartTime;
				operationInProgressID 	= msMotorOperationParameters[MS_OP_ORIGINAL].OperationID;
				motorCurrentLimit		= msMotorOperationParameters[MS_OP_ORIGINAL].motorCurrentLimit;
				msMotorOperationParameters[MS_OP_ORIGINAL].inProgress = LU_TRUE;
			}
		}
		else
		{
			#ifdef DEBUG_MOTOR_SUPPLY
			statepos;
			_printf("Motor Supply Cancel Error\t - Motor Supply Cancel\tID = %4x\tIn progress ID = %4x\t - Time = %d\n\n\r",motorCancelID, operationInProgressID,time/MSEC );
			#endif
		}

		/* Set Peak current channel value */
		RetVal = IOManagerSetValue(IO_ID_VIRT_MS_PEAK_HOLD_CURRENT, peakMotorCurrent);

		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Set peak current value for reply message */
	*peak = peakMotorCurrent;
	*id   = motorCancelID;

	return RetVal;
}

SB_ERROR MotorSupplyControlTick(void)
{
	SB_ERROR RetVal;
	lu_int32_t motorVoltage, motorEnabled, batteryVolts;
	lu_uint32_t elapsedTime,overLimitTime, time;

	/* Get current time */
	time = STGetTime();

	/* If selected then check for select time exceeded */
	elapsedTime = STElapsedTime( motorSelectTime , time );
	if ( (motorSelected == LU_TRUE) && ( elapsedTime  >   motorSelectPeriod ) )
	{
		SysAlarmSetLogEvent(LU_TRUE,
													SYS_ALARM_SEVERITY_ERROR,
													SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
													SYSALC_SUPPLY_CONTROL_MOTOR_SELOP_TIMEOUT,
													0
												   );

		/* Only turn off the LEDs and reset the flags if not re-triggered */
		if( motorRetriggered == LU_FALSE)
		{
			/* Turn off motor supply green LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_GREEN, 0);
			/* Turn off motor supply red LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_RED, 0);
			/* Cancel motor supply operating flag */
			motorSelected = LU_FALSE;
			motorSelectTime = 0;
			SwEventParamsInProgress = LU_FALSE;
			/* Reset the flag to indicate a re-trigger */
			motorRetriggered = LU_FALSE;
		}
	}


	/* If operating then check for time finished */
	elapsedTime = STElapsedTime( motorOperateTime , time );
	if ( ( motorOperate == LU_TRUE) && (elapsedTime  > motorOperatePeriod ) )
	{
		/* Turn off motor supply if Operatetime is not ¨0¨*/
		if(motorOperatePeriod)
		{
			MotorSupplyOff();
		}
		ResetFlagsTimer();
		ResetFlagsStruct(MS_OP_ORIGINAL);
		ResetFlagsStruct(MS_OP_RETRIGGER);

		/* Reset the flag that indicates a re-trigger */
		motorRetriggered = LU_FALSE;

		#ifdef DEBUG_MOTOR_SUPPLY
		statepos;
		_printf("Motor Supply Operation\t- Timed out\t\t\t\tIn progress ID = %4x\t - Time = %d\n\n\r",operationInProgressID,time/MSEC );
		#endif

		/* No operations in progress so reset the In Progress ID */
		operationInProgressID = 0;

		/* Turn off motor supply if Operatetime is not ¨0¨*/
		/*if(motorOperatePeriod){

		SysAlarmSetLogEvent(LU_TRUE,
											SYS_ALARM_SEVERITY_ERROR,
											SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER,
											SYSALC_SUPPLY_CONTROL_MOTOR_ON_TIMEOUT,
											0
										   );
		}*/
	}

	else if ( motorOperate == LU_TRUE )
	{
		/* Get peak Motor current */
		RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_MOT_I_SENSE, &motorCurrent);
		if (motorCurrent > peakMotorCurrent)
		{
			peakMotorCurrent = motorCurrent;
		}

		/*
		 * If operating then check that sample delay time has been exceeded
		 * before checking limits
		 */
		elapsedTime = STElapsedTime( motorOperateTime , time );
		if( elapsedTime > MOTOR_SUPPLY_OPERATE_SAMPLE_DELAY_MS )
		{
			/* Check for motor current limit reached */
			RetVal = IOManagerGetValue(IO_ID_MOT_PS_EN , &motorEnabled);

			/*
			 * If the motor over current has been activated the the MOT_PS_EN input
			 * will go low.
			 */
			if(motorEnabled != LU_TRUE)
			{
				RetVal = IOManagerSetValue(IO_ID_VIRT_MS_OVERCURRENT, 1);
			}

			/*
			 * Check that the motor current hasn't exceeded the combined limit for more than the
			 * specified duration. If the current limit is zero then assume no current limit checking
			 * is to take place.
			 */
			if( (motorCurrent > motorCurrentLimit) && (motorCurrentLimit > 0 ) )
			{
				/* Check that the motor current hasn't already been exceeded */
				if( motorCurrentExceeded == LU_FALSE )
				{
					/* Set flag and start timer for the current limit duration */
					motorCurrentExceeded = LU_TRUE;
					motorCurrentExceededStart = time;
				}
				else
				{
					/* Check to see if the current limit has been exceeded for the specified duration */
					overLimitTime = STElapsedTime( motorCurrentExceededStart , time );
					if( overLimitTime > motorCurrentLimitTime )
					{
						/*
						 * Set virtual point to indicate an over-current
						 */
						RetVal = IOManagerSetValue(IO_ID_VIRT_MS_OVERCURRENT_LIMIT, LU_TRUE);
						/* Motor current limit has been exceeded for period specified */
						#ifdef DEBUG_MOTOR_SUPPLY
						statepos;
						_printf("Motor Supply Current\t - Current exceeded\t - Time = %d\n\n\r",time/MSEC );
						#endif


					}
				}
			}
			else
			{
				/* Current below the limit so reset the flag and time */
				motorCurrentExceeded = LU_FALSE;
				motorCurrentExceededStart = 0;
				/*
				 * Reset virtual point to indicate no over-current
				 */
				RetVal = IOManagerSetValue(IO_ID_VIRT_MS_OVERCURRENT_LIMIT, LU_FALSE);
			}


			/*
			 * Check motor supply voltage against battery voltage
			 * Note: Must be made an average value
			 */

			RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE, &batteryVolts);
			RetVal = IOManagerGetCalibratedUnitsValue(IO_ID_MOT_V_SENSE, &motorVoltage);
			motorVoltsScaled   += motorVoltage;
			batteryVoltsScaled += batteryVolts;

			if( ++averageCount >= MS_VOLTAGE_AVERAGE)
			{
				/* Calculate the average values */
				motorVoltsScaled /= MS_VOLTAGE_AVERAGE;
				batteryVoltsScaled /= MS_VOLTAGE_AVERAGE;

				/* Reset counter and averaged values */
				averageCount = 0;
				motorVoltsScaled = 0;
				batteryVoltsScaled = 0;
			}
		}
	}


	RetVal = SB_ERROR_NONE;

	return RetVal;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
void ResetFlagsStruct(lu_bool_t index)
{
	/* Reset operate parameter structure */
	msMotorOperationParameters[index].channel				= 0;
	msMotorOperationParameters[index].local					= 0;
	msMotorOperationParameters[index].OperationID			= 0;
	msMotorOperationParameters[index].motorSelected			= 0;
	msMotorOperationParameters[index].motorSelectStartTime	= 0;
	msMotorOperationParameters[index].motorSelectTimeout	= 0;
	msMotorOperationParameters[index].motorSelectEndTime	= 0;
	msMotorOperationParameters[index].motorOperateStartTime	= 0;
	msMotorOperationParameters[index].motorOperateDuration	= 0;
	msMotorOperationParameters[index].motorOperateEndTime	= 0;
	msMotorOperationParameters[index].peakMotorCurrent		= 0;
	msMotorOperationParameters[index].inProgress			= 0;
	msMotorOperationParameters[index].motorCurrentLimit		= 0;
	msMotorOperationParameters[index].motorCurrentLimitTime	= 0;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void ResetFlagsTimer(void)
{
	/* Reset timers and flags */
	motorSelected 				= LU_FALSE;
	motorSelectTime 			= 0;
	motorOperate 				= LU_FALSE;
	motorOperateTime 			= 0;
	motorCurrentLimit			= 0;
	motorCurrentExceeded		= LU_FALSE;
	motorCurrentExceededStart	= 0;
	SwEventParamsInProgress 	= LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void MotorSupplyOff(void)
{
	SB_ERROR retError;

//SRM
	/*Tell the Battery Manager FSM*/
	SendMotorSupplyCancelEventToFSM();
	/* Turn off motor supply */
	retError = IOManagerSetPulse(IO_ID_MOT_PS_OFF, MOTOR_PULSE_MS);
	/* Set relay on virtual point */
	retError = IOManagerSetValue(IO_ID_VIRT_MS_RELAY_ON, 0);

	/* Set Peak current channel value */
	retError = IOManagerSetValue(IO_ID_VIRT_MS_PEAK_HOLD_CURRENT, peakMotorCurrent);

	/* Turn off motor green LED */
	retError = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_GREEN, 0);
	/* Turn off motor supply red LED */
	retError = IOManagerSetValue(IO_ID_LED_CTRL_MOTOR_RED, 0);
}

/*
 *********************** End of file ******************************************
 */
