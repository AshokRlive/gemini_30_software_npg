/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   19/07/11      fryers_     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _MOTORSUPPLYCONTROLLER_INCLUDED
#define _MOTORSUPPLYCONTROLLER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"

#include "debug_frmwrk.h"
/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* Define tracing to use printf to serial port */
#define TRACE					_printf

#define	STATE_POSITION			38
#define MESSAGE_POSITION		40
#define CAN_POSITION			42
#define esc 27
#define cls 					_printf("%c[2J",esc)
#define cll						_printf("%c[K",esc)
#define pos(row,col) 			_printf("%c[%d;%dH",esc,row,col)

#define statepos				pos(STATE_POSITION,1);cll;
#define canpos					pos(CAN_POSITION,1);cll;
#define messagepos				pos(MESSAGE_POSITION,1);cll;

#define DEBUG_MOTOR_SUPPLY

/* Define the number of simultaneous operations allowed */
#define MAX_OPERATIONS						2

/* Pulse length to switch motor supply */
#define MOTOR_PULSE_MS						30

#define MOTOR_SUPPLY_ENABLED 				LU_TRUE
#define COMMS2_AUX_SUPPLY_ENABLED 			LU_TRUE
#define COMMS1_SUPPLY_ENABLED 				LU_TRUE

#define	MOTOR_SUPPLY_VOLTAGE_THRESHOLD		24
#define	MOTOR_SUPPLY_CURRENT_THRESHOLD		10

/* Percentage tolerance allowed in motor supply voltage before indicating a problem */
#define MOTOR_SUPPLY_OPERATE_SAMPLE_DELAY_MS	100

/* Number of readings used to take average*/
#define MS_VOLTAGE_AVERAGE					3

#define MSEC								1000

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 * Stores operation parameters for multiple simultaneous operations
 */
typedef struct MSMotorOperationsDef
{
	lu_uint8_t	channel;
	lu_bool_t	local;
	lu_uint16_t	OperationID;
	lu_bool_t 	motorSelected;
	lu_uint32_t motorSelectStartTime;
	lu_uint32_t motorSelectTimeout;
	lu_uint32_t motorSelectEndTime;
	lu_uint32_t motorOperateStartTime;
	lu_uint32_t motorOperateDuration;
	lu_uint32_t motorOperateEndTime;
	lu_uint32_t	motorCurrentLimit;
	lu_uint32_t	motorCurrentLimitTime;
	lu_uint32_t	peakMotorCurrent;
	lu_bool_t	inProgress;
}MSMotorOperationsStr;

/*
 * Used to index the operation parameters array of structures
 */
typedef enum
{
    MS_OP_ORIGINAL 	= 0,
    MS_OP_RETRIGGER = 1
}MS_OPERATION;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
//lu_bool_t SwEventParamsInProgress;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR MotorSupplyControlInit(void);

#endif /* _MOTORSUPPLYCONTROLLER_INCLUDED */

 /*!
   ******************************************************************************
   *   \brief Brief description
   *
   *   Detailed description
   *
   *   \param parameterName Description
   *
   *
   *   \return
   *
   ******************************************************************************
   */
 extern lu_uint8_t MotorSupplySelectHandler(SupplySelectStr *);

 /*!
    ******************************************************************************
    *   \brief Brief description
    *
    *   Detailed description
    *
    *   \param parameterName Description
    *
    *
    *   \return
    *
    ******************************************************************************
    */
  extern lu_uint8_t MotorSupplyOperateHandler(SupplyOperateStr *);


 /*!
    ******************************************************************************
    *   \brief Brief description
    *
    *   Detailed description
    *
    *   \param parameterName Description
    *
    *
    *   \return
    *
    ******************************************************************************
    */
  extern lu_uint8_t MotorSupplyCancelHandler(SupplyCancelStr *, lu_int32_t *, lu_uint16_t *);

  /*!
    ******************************************************************************
    *   \brief Brief description
    *
    *   Detailed description
    *
    *   \param parameterName Description
    *
    *
    *   \return
    *
    ******************************************************************************
    */
  extern SB_ERROR MotorSupplyControlTick(void);


/*
 *********************** End of file ******************************************
 */
