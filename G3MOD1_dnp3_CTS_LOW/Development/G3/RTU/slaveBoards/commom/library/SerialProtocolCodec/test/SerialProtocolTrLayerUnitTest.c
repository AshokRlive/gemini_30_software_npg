/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief  Serial Protocol Test module
*
*       
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY
*
*   Date        EAR/Reason      Name        Details
*   ---------------------------------------------------------------------------
*   20/10/09                    fryers_j     Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/

#include "SerialProtocolTrLayerDecEnc.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/

static lu_uint8_t dataBuffer[64];

/* Packet 1 */
static lu_uint8_t packet1[] =
{
    SP_TRS_START_FRAME,
    SP_TRS_TEST_PROTOCOL,
    0x05, /* size */
    0x01,
    0x02,
    0x03,
    0x04,
    0x05,
    0x30, /* CRC1 */
    0xc7 /* CRC2 */
};

/* Packet 2 */
static lu_uint8_t packet2[] =
{
    SP_TRS_START_FRAME,
    0x34, /* Wrong service */
    0x05, /* size */
    0x01,
    0x02,
    0x03,
    0x04,
    0x05,
    0x30, /* CRC1 */
    0xc7, /* CRC2 */
};

/* Packet 3: wrong CRC */
static lu_uint8_t packet3[] =
{
    SP_TRS_START_FRAME,
    SP_TRS_TEST_PROTOCOL,
    0x05, /* size */
    0x01,
    0x02,
    0x03,
    0x04,
    0x05,
    0x31, /* CRC1 */
    0xc6  /* CRC2 */
};

/* Packet 3: wrong CRC */
static lu_uint8_t payload1[] =
{
    0x01,
    0x02,
    0x03,
    0x04,
    0x05
};


/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/



/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/

static void packetFound(lu_uint8_t service, lu_uint8_t *buffer, lu_uint8_t size, void *arg);
static lu_uint8_t sendFunc(lu_uint8_t *buffer, lu_uint8_t size, void *arg);

/*!
*******************************************************************************
* EXPORTED:
*   \name Public Functions
*******************************************************************************
* @{
*/

int main(int argc ,char **argv)
{
    SPTrsHandler handler;

    LU_UNUSED(argc);
    LU_UNUSED(argv);

    handler = I2CTrsInit(dataBuffer, sizeof(dataBuffer));
    SPTrsDecode(handler, packet1, sizeof(packet1), packetFound, NULL);
    SPTrsDecode(handler, packet2, sizeof(packet1), packetFound, NULL);
    SPTrsDecode(handler, packet3, sizeof(packet1), packetFound, NULL);

    SPTrsSend(SP_TRS_CMD_PROTOCOL, payload1, sizeof(payload1), sendFunc, NULL);

    return 0;
}

/*!
*******************************************************************************
* @}
*/

/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/


void packetFound(lu_uint8_t service, lu_uint8_t *buffer, lu_uint8_t size, void *arg)
{
    LU_UNUSED(buffer);
    LU_UNUSED(arg);
    printf("Packet decoded correctly. Service: %i - Size: %i\n", service, size);
}

lu_uint8_t sendFunc(lu_uint8_t *buffer, lu_uint8_t size, void *arg)
{
    lu_uint8_t i;

    LU_UNUSED(arg);

    for(i = 0; i < size; i++)
    {
        printf("0x%02x ", buffer[i]);
    }

    printf("\n");

    return size;
}
/*!
*******************************************************************************
* @}
*/


/*
*********************** End of file *******************************************
*/

