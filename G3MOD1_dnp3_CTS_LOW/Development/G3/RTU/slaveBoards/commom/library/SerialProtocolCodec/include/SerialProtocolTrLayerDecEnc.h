/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id: SerialProtocolTrLayerDecEnc.h 1787 2012-10-01 19:56:19Z fryers_j $
*               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/commom/library/SerialProtocolCodec/include/SerialProtocolTrLayerDecEnc.h $
*
*    DESCRIPTION:
*       \brief  Brief description of this template module
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY
*
*   Date        EAR/Reason      Name        Details
*   ---------------------------------------------------------------------------
*   29/10/08                    walde_s     Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

#ifndef SP_TRANSPORT_LAYER_DEC_ENC_H
#define SP_TRANSPORT_LAYER_DEC_ENC_H 1

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/

#include "SerialProtocolTransportLayer.h"

/*
*******************************************************************************
* EXPORTED - Public Definitions
*******************************************************************************
*/

#ifdef __SWAP_NOT_NEEDED
#define BYTE_SWAP(value, size)
#else
#define BYTE_SWAP(value, size)\
{\
    lu_uint8_t i;\
    lu_uint8_t tmp;\
    for(i = 0; i < size / 2; i++)\
    {\
        tmp = value[i];\
        value[i] = value[size - i - 1];\
        value[size - i - 1] = tmp;\
    }\
}
#endif

/*
*******************************************************************************
* EXPORTED - Public Types
*******************************************************************************
*/

typedef enum
{
    SP_TRS_ERROR_NONE = 0,
    SP_TRS_ERROR_PARAM   ,
    SP_TRS_ERROR_SEND    ,

    SP_TRS_ERROR_MAX
}SP_TRS_ERROR;

typedef void (*spTrsCallBack)(lu_uint8_t service, lu_uint8_t *buffer, lu_uint8_t size, void *arg);
typedef lu_uint8_t (*spSendFunction)(lu_uint8_t *buffer, lu_uint8_t size, void *arg);

typedef void* SPTrsHandler;

/*
*******************************************************************************
* EXPORTED - Public Constants
*******************************************************************************
*/


/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/




/*
*******************************************************************************
* EXPORTED - Functions
*******************************************************************************
*/

extern SPTrsHandler SPTrsInit(lu_uint8_t *buffer, lu_uint8_t size);

extern SP_TRS_ERROR SPTrsDecode( I2CTrsHandler   handler ,
                                 lu_uint8_t     *buffer  ,
                                 lu_uint8_t      size    ,
                                 spTrsCallBack   packetCB,
                                 void           *CBArg
                                );

extern SP_TRS_ERROR SPTrsSend( lu_uint8_t       service,
                               lu_uint8_t      *buffer ,
                               lu_uint8_t       size   ,
                               spSendFunction   sendFunc,
                               void            *sendArg
                             );


#endif /* SP_TRANSPORT_LAYER_DEC_ENC_H */

/*
*********************** End of file *******************************************
*/
