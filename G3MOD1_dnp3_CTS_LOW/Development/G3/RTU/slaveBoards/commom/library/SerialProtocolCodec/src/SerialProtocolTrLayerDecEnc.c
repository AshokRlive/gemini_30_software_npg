/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief  Brief description of this template module
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY
*
*   Date        EAR/Reason      Name        Details
*   ---------------------------------------------------------------------------
*   29/10/08                    walde_s     Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/

#include <stdlib.h>
#include <string.h>


/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/

#include "SerialProtocolTrLayerDecEnc.h"
#include "crc16.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/

typedef enum
{
    FSM_STATE_FIND_FRAME = 0,
    FSM_STATE_GET_SERVICE   ,
    FSM_STATE_GET_SIZE      ,
    FSM_STATE_GET_PAYLOAD   ,
    FSM_STATE_GET_TRAILER   ,

    FSM_STATE_MAX
}SPTrsFsmState;

/*!
 * Decoder FSM data
 */
typedef struct
{
    SPTrsFsmState state;
    lu_uint8_t     service;
    lu_uint8_t     size;
    lu_uint8_t     counter;
    lu_uint8_t     payload[SP_TRS_MAX_PAYLOAD_SIZE];
    lu_uint8_t     CRC16[2];
}SPTrsFsm, *SPTrsFsmStatePtr;

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/



/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/

static void resetHandler(SPTrsHandler  handler);

/*!
*******************************************************************************
* EXPORTED:
*   \name Public Functions
*******************************************************************************
* @{
*/

/*!
*******************************************************************************
* Function Name:
*   SPTrsInit()
*
* Description:
*   \brief   Initialize Serial Protocol Transport Layer decoder handler
*
*   The provided buffer (if big enough) is initialized and returned as I2C
*   Transport Layer decoder handler
*
* Parameters:
*   \param buffer User buffer
*   \param size Buffer size
*
* Return Value:
*   \return The handler if the provided buffer is big enough. NULL otherwise
*******************************************************************************
*/
I2CTrsHandler SPTrsInit(lu_uint8_t *buffer, lu_uint8_t size)
{
    if ( (buffer == NULL)               ||
         (size < sizeof(SPTrsFsmState))
       )
    {
        return NULL;
    }

    memset(buffer, 0, size);
    resetHandler(buffer);

    return (SPTrsHandler)(&buffer[0]);
}

/*!
*******************************************************************************
* Function Name:
*   SPTrsDecode()
*
* Description:
*   \brief   Decode the provided buffer
*
*   The input byte stream is decoded. Once a valid packet is found the provided
*   callback is called and the message payload is passed along with a custom
*   parameter (void pointer). The handler is used to store the internal FSM
*   state and all the relevant data (the payload for example)
*
* Parameters:
*   \param handler Decode handler
*   \param buffer  Buffer to decode
*   \param size Buffer size
*   \param packetCB Callback called when a packet is found
*   \param CBArg Callback custom argument
*
* Return Value:
*   \return Error code or FT_ERROR_NONE if everything is fine
*******************************************************************************
*/
I2C_TRS_ERROR SPTrsDecode( SPTrsHandler   handler ,
                           lu_uint8_t     *buffer  ,
                           lu_uint8_t      size    ,
                           spTrsCallBack  packetCB,
                           void           *CBArg
                          )
{
    lu_uint8_t        i;
    SPTrsFsmStatePtr fsmState;

    /* Check input parameters */
    if ( (handler  == NULL) ||
         (buffer   == NULL) ||
         (packetCB == NULL)
       )
    {
        return SP_TRS_ERROR_PARAM;
    }

    fsmState = (SPTrsFsmStatePtr)handler;

    for (i = 0; i < size; i++)
    {
        switch (fsmState->state)
        {
            /* Search for the start frame */
            case FSM_STATE_FIND_FRAME:
                if (*buffer == SP_TRS_START_FRAME)
                {
                    /* Start frame found */
                    fsmState->state = FSM_STATE_GET_SERVICE;
                }
            break;

            /* Get the service */
            case FSM_STATE_GET_SERVICE:
                if ( (*buffer == SP_TRS_TEST_PROTOCOL) ||
                     (*buffer == SP_TRS_OLD_PROTOCOL)
                   )
                {
                  fsmState->service = *buffer;
                  fsmState->state = FSM_STATE_GET_SIZE;
                }
                else
                {
                    /* Invalid service. Reset FSM*/
                    resetHandler(handler);
                }
            break;

            /* Get payload size */
            case FSM_STATE_GET_SIZE:
                if ( (*buffer <= SP_TRS_MAX_PAYLOAD_SIZE) &&
                     (*buffer != 0)
                   )
                {
                    fsmState->size = *buffer;
                    fsmState->state = FSM_STATE_GET_PAYLOAD;
                }
                else
                {
                    /* Payload too big. Reset FSM*/
                    resetHandler(handler);
                }
            break;

            /* Save payload */
            case FSM_STATE_GET_PAYLOAD:
                fsmState->payload[fsmState->counter++] = *buffer;

                if (fsmState->counter == fsmState->size)
                {
                    fsmState->counter = 0;
                    fsmState->state   = FSM_STATE_GET_TRAILER;
                }
            break;

            /* Get the trailer */
            case FSM_STATE_GET_TRAILER:
                fsmState->CRC16[fsmState->counter++] = *buffer;
                if (fsmState->counter == 2)
                {
                    /* Full packet received. Check CRC */
                    crc16 = SP_TRS_CRC16_START_VALUE;
                    crc16_byte(fsmState->service);
                    crc16_byte(fsmState->size);
                    crc16_calc16(fsmState->payload, fsmState->size);
                    BYTE_SWAP((fsmState->CRC16), 2);
                    if (crc16 == *((lu_uint16_t*)fsmState->CRC16))
                    {
                        /* Valid packet received. Forward it */
                        packetCB( fsmState->service,
                                  fsmState->payload,
                                  fsmState->size   ,
                                  CBArg
                                );
                    }

                    /* Full packet decoded: reset FSM */
                    resetHandler(handler);
                }
            break;

            default:
                resetHandler(handler);
            break;
        }

        buffer++;
    }

    return SP_TRS_ERROR_NONE;
}

/*!
*******************************************************************************
* Function Name:
*   SPTrsSend()
*
* Description:
*   \brief   Send the payload using the Serial Protocol Transport Layer protocol
*
*   The SP transport layer protocol layer header and trailer are created.
*   Everything is sent using the provided send function. The send function
*   should retrun the number of bytes sent and -1 in case of error
*
* Parameters:
*   \param service Payload service type
*   \param buffer Payload
*   \param size Payload size
*   \param sendFunc Send function
*   \param sendArg Send function custom argument
*
* Return Value:
*   \return Error code in case of error or FT_ERROR_NONE if everything is fine
*******************************************************************************
*/
SP_TRS_ERROR SPTrsSend( lu_uint8_t       service,
                        lu_uint8_t      *buffer ,
                        lu_uint8_t       size   ,
                        spSendFunction   sendFunc,
                        void            *sendArg
                      )
{
    SPTrsHeader header;

    if ( (buffer   == NULL)               ||
         (sendFunc == NULL)               ||
         (size > SP_TRS_MAX_PAYLOAD_SIZE)
       )
    {
        return SP_TRS_ERROR_PARAM;
    }

    /* Build header */
    header.frame = SP_TRS_START_FRAME;
    header.service = service;
    header.size = size;

    /* Send header */
    if ( sendFunc((lu_uint8_t*)&header, sizeof(header), sendArg) != sizeof(header))
    {
        return SP_TRS_ERROR_SEND;
    }

    /* Send payload */
    if ( sendFunc(buffer, size, sendArg) != size)
    {
        return SP_TRS_ERROR_SEND;
    }

    /* Calculate crc16 */
    crc16 = SP_TRS_CRC16_START_VALUE;
    crc16_byte(service);
    crc16_byte(size);
    crc16_calc16(buffer, size);

    /* Swap crc16 and send */
    BYTE_SWAP(((lu_uint8_t*)&crc16), sizeof(crc16));
    if ( sendFunc((lu_uint8_t*)&crc16, sizeof(crc16), sendArg) != sizeof(crc16))
    {
        return SP_TRS_ERROR_SEND;
    }

    return SP_TRS_ERROR_NONE;
}

/*!
*******************************************************************************
* @}
*/

/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/
/*!
*******************************************************************************
* Function Name:
*   resetHandler()
*
* Description:
*   \brief   Reset decoder FSM
*
* Parameters:
*   \param handler FSM to reset
*
* Return Value:
*   \return None
*******************************************************************************
*/
void resetHandler(SPTrsHandler  handler)
{
    SPTrsFsmStatePtr fsmState;

    if (handler == NULL)
    {
        return;
    }

    fsmState = (SPTrsFsmStatePtr)handler;

    fsmState->state = FSM_STATE_FIND_FRAME;
    fsmState->counter = 0;
}

/*!
*******************************************************************************
* @}
*/


/*
*********************** End of file *******************************************
*/
