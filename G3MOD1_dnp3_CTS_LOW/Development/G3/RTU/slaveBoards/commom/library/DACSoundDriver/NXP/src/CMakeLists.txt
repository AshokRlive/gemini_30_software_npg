cmake_minimum_required(VERSION 2.8)

add_library( DACSoundDriver STATIC
             ProcessorDACDMA.c
             ../../src/DACSoundDriver.c
           ) 

ADD_DEPENDENCIES(DACSoundDriver globalHeaders)

# Global include
include_directories (../../../../../../../common/include/)

# Generated header include
include_directories(${CMAKE_G3_GENERATED_HEADER_INCLUDE}/)

# Slave Boards Global include
include_directories (../../../../include/)

# System Utils
include_directories (../../../systemUtils/include/)

#DSP functions
include_directories (../../../DSP/include/)

# NXP drivers
include_directories (SYSTEM ../../../../../thirdParty/LPC1700CMSIS/Core/CM3/DeviceSupport/NXP/LPC17xx/)
include_directories (SYSTEM ../../../../../thirdParty/LPC1700CMSIS/Core/CM3/CoreSupport/)
include_directories (SYSTEM ../../../../../thirdParty/LPC1700CMSIS/Drivers/include/)

# Slave library include
include_directories (../../../CANProtocolCodec/include/)
include_directories (../../../CANProtocolFraming/include/)

# Module public include
include_directories (../../include/)

# Module library components include
include_directories (../../../IOManager/include/)
include_directories (../../../IOManager/NXP/include/)

# Module local include
include_directories (../include/)

# Slave Board specific includes
include_directories (${SLAVEBOARD_PATH}/IOManager/include/)

# Generate Doxygen documentation
gen_doxygen("DACSoundDriver" "")
