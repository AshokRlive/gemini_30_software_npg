/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/01/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IOManager.h"

#include "ProcessorGPIOMap.h"
#include "ProcessorDACDMA.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static GPDMA_Channel_CFG_Type GPDMACfg;
static GPDMA_LLI_Type 		  DMA_LLI_Struct;
static DAC_CONVERTER_CFG_Type DAC_ConverterConfigStruct;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ProcDACDMAInit(void)
{
	/* Disable DMA Interrupt */
	NVIC_DisableIRQ (DMA_IRQn);

    /* Initialise GPDMA controller */
    GPDMA_Init();

    return SB_ERROR_NONE;
}


SB_ERROR ProcDACDMAConfigure(
		                  lu_uint8_t 		port,
	                      lu_uint8_t 		pin,
		                  GPIO_PIN_FUNC     func
		                 )
{
	SB_ERROR  		    retError;
	lu_uint8_t  		portPinIdx;
	lu_uint8_t			pinDir;
	lu_uint8_t          pinFunc;
	PINSEL_CFG_Type 	pinCfg;
	lu_uint32_t	        pinShift;

	retError = SB_ERROR_INITIALIZED;

	pinDir = 0; /* Default to input */

	pinShift = 1;
	pinShift = pinShift << (pin & 0x1f);

	/* Now do the pin configuration */
	if (port < MAX_GPIO_PORTS && pin < MAX_GPIO_PORT_BITS)
	{
		portPinIdx = pin + (port << 5);
		if (portPinIdx < NUM_GPIO_PINPORT_IDX)
		{
			pinFunc = ProcGPIOCheckPeripheralPinPort(func, port, pin);

			if (pinFunc)
			{
				/* Set pin configuration */
				pinCfg.Funcnum   = pinFunc;
				pinCfg.Portnum   = port;
				pinCfg.Pinnum    = pin;
				pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
				pinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
				pinCfg.Pinmode   = 0;

				/* Configure pin type */
				PINSEL_ConfigPin(&pinCfg);

				/* pin configuration was successful */
				retError = SB_ERROR_NONE;
			}
		}
	}

	return retError;
}

SB_ERROR ProcDACDMAStartDMA(lu_uint32_t *srcDataPtr, lu_uint32_t NumDataSamples, lu_uint32_t sampFrequency, lu_bool_t replay)
{
	SB_ERROR  		    	retError;
	lu_uint32_t				tmp;

	/* Disable the DMA */
	GPDMA_ChannelCmd(0, DISABLE);

	/* Configure DMA link list structure */
	DMA_LLI_Struct.SrcAddr= (uint32_t)srcDataPtr;
	DMA_LLI_Struct.DstAddr= (uint32_t)&(LPC_DAC->DACR);
	if (replay)
	{
		/* Continue to replay */
		DMA_LLI_Struct.NextLLI= (uint32_t)&DMA_LLI_Struct;
	}
	else
	{
		DMA_LLI_Struct.NextLLI= (uint32_t)0L;
	}

	DMA_LLI_Struct.Control= ( NumDataSamples |
								GPDMA_DMACCxControl_SWidth(2) |
								GPDMA_DMACCxControl_DWidth(2) |
								GPDMA_DMACCxControl_SI
							);

	// Setup GPDMA channel --------------------------------
	// channel 0
	GPDMACfg.ChannelNum = 0;
	// Source memory
	GPDMACfg.SrcMemAddr = (uint32_t)(srcDataPtr);
	// Destination memory - unused
	GPDMACfg.DstMemAddr = 0;
	// Transfer size
	GPDMACfg.TransferSize = NumDataSamples;
	// Transfer width - unused
	GPDMACfg.TransferWidth = 0;
	// Transfer type
	GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
	// Source connection - unused
	GPDMACfg.SrcConn = 0;
	// Destination connection
	GPDMACfg.DstConn = GPDMA_CONN_DAC;
	// Linker List Item - unused
	GPDMACfg.DMALLI = (uint32_t)&DMA_LLI_Struct;
	// Setup channel with given parameter
	GPDMA_Setup(&GPDMACfg);

	DAC_ConverterConfigStruct.CNT_ENA = SET;
	DAC_ConverterConfigStruct.DMA_ENA = SET;
	DAC_Init(LPC_DAC);

	/* Setup DAC timeout */
	tmp = (SystemCoreClock / 4) / (sampFrequency * NumDataSamples);
	DAC_SetDMATimeOut(LPC_DAC,tmp);
	DAC_ConfigDAConverterControl(LPC_DAC, &DAC_ConverterConfigStruct);

	/* Enable GPDMA channel 0 */
	GPDMA_ChannelCmd(0, ENABLE);

	retError = SB_ERROR_NONE;

	return retError;
}

SB_ERROR ProcDACDMAStopDMA(void)
{
	SB_ERROR  		    retError;

	retError = SB_ERROR_NONE;

	/* Disable the DMA */
	GPDMA_ChannelCmd(0, DISABLE);

	return retError;
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
