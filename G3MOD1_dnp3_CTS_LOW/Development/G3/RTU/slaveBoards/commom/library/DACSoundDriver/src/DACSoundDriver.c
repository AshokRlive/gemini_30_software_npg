/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"
#include "BoardIOMap.h"

#include "systemTime.h"
#include "DACSoundDriver.h"
#include "ProcessorDACDMA.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define NUM_SAMPLES				64
#define MAX_NUM_DAC_VALUES		1024

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static lu_uint32_t			soundBuffer[NUM_SAMPLES];
static const lu_uint32_t 	sin_0_to_90_16_samples[16]=
{
		0,1045,2079,3090,4067,
		5000,5877,6691,7431,8090,
		8660,9135,9510,9781,9945,10000
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR DACSoundDriverInit(lu_uint32_t ioIDDAC)
{
	SB_ERROR retError;

	/* Init DAC DMA engine */
	retError = ProcDACDMAInit();

	if (ioIDDAC < BOARD_IO_MAX_IDX)
	{
		/* Configure the peripheral now */
		ProcDACDMAConfigure( BoardIOMap[ioIDDAC].ioAddr.gpio.port,
				          BoardIOMap[ioIDDAC].ioAddr.gpio.pin,
				          BoardIOMap[ioIDDAC].ioAddr.gpio.periphFunc
				        );
	}
	return retError;
}

SB_ERROR DACSoundPlaySound(lu_uint32_t frequency)
{
	lu_uint32_t	idx;
//	lu_uint32_t offset;

	for (idx = 0; idx < NUM_SAMPLES; idx++)
	{
		if (idx <= 15)
		{
			soundBuffer[idx] = (MAX_NUM_DAC_VALUES / 2) +
					           ((MAX_NUM_DAC_VALUES / 2) * sin_0_to_90_16_samples[idx]) / 10000;
			if (idx == 15)
			{
				soundBuffer[idx] = (MAX_NUM_DAC_VALUES / 2);
			}
		}
		else if (idx <= 30)
		{
			soundBuffer[idx] = (MAX_NUM_DAC_VALUES / 2) +
							   ((MAX_NUM_DAC_VALUES / 2) * sin_0_to_90_16_samples[30 - idx]) / 10000;
		}
		else if (idx <= 45)
		{
			soundBuffer[idx] = (MAX_NUM_DAC_VALUES / 2) -
							   ((MAX_NUM_DAC_VALUES / 2) * sin_0_to_90_16_samples[idx - 30]) / 10000;
		}
		else
		{
			soundBuffer[idx] = (MAX_NUM_DAC_VALUES / 2) -
							   ((MAX_NUM_DAC_VALUES / 2) * sin_0_to_90_16_samples[60 - idx]) / 10000;
		}
		soundBuffer[idx] = (soundBuffer[idx] << 6);
	}

	ProcDACDMAStartDMA(soundBuffer, NUM_SAMPLES, frequency, LU_TRUE);

	return SB_ERROR_NONE;
}

SB_ERROR DACSoundStopSound(void)
{
	return ProcDACDMAStopDMA();
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
