/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       NVRAM driver / manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "string.h"
#include "lu_types.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "errorCodes.h"
#include "CANProtocolCodec.h"
#include "NVRam.h"
#include "NVRAMDef.h"

#include "systemTime.h"

#include "crc32.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_PAGE_WRITE					(16)
#define MAX_PAGE_READ					(100)



// Max update rate approx every 21 Minutes for service life of 40 years running time
#define NVRAM_MAX_WRITE_CYCLES			(1000000L)
#define NVRAM_MAX_SERVICE_LIFE_YEARS    (40L)
#define NVRAM_MAX_SERVICE_LIFE_SECS		(NVRAM_MAX_SERVICE_LIFE_YEARS * (((60L * 60L) * 24L) * 365L) )
#define NVRAM_UPDATE_RATE_SECS			(NVRAM_MAX_SERVICE_LIFE_SECS / NVRAM_MAX_WRITE_CYCLES)
#define NVRAM_UPDATE_RATE_MS			(NVRAM_UPDATE_RATE_SECS * 1000L)


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/* Map for NVRAM block offsets */
typedef struct NvramBlkOffsetMapDef
{
	lu_uint8_t      blockId;
	/*! Absolute offset to data block*/
	lu_uint16_t     offset;
	/*! Total size of data block including blk header */
	lu_uint16_t		length;
}NvramBlkOffsetMapStr;

/* NVRAM Update */
typedef struct NvramUpdateDef
{
	lu_uint32_t     updateTimerMs;
	lu_bool_t		triggerUpdate;
	lu_uint8_t		updateBlkMask;
}NvramUpdateStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR NVRAMUpdateCmd(NVRAMCmdStr *nvramUpdateCmdPtr);
SB_ERROR NVRAMInvalidateCRCCmd(NVRAMCmdStr *nvramUpdateCmdPtr);
SB_ERROR NVRAMWriteCmd(lu_uint8_t *memPtr, lu_uint16_t dataSize);
SB_ERROR NVRAMReadCmd(NVRAMCmdStr *nvramReadCmdPtr);
SB_ERROR NVRamTriggerWriteUpdate(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk);
SB_ERROR NVRamGetMemoryPointer(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk, lu_uint8_t **memPtr, lu_uint16_t *dataSizePtr);
SB_ERROR NVRamGetMemoryHeaderPointer(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk, lu_uint8_t **memPtr);
LPC_I2C_TypeDef *findNVRAMI2CChannel(NVRAM_TYPE nvramType, lu_uint32_t *ioIDPtr, SB_ERROR *retValPtr);
SB_ERROR NVRamWriteBlockType(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk);
SB_ERROR NVRamWrite(lu_uint8_t *dataPtr, lu_uint16_t size, NVRAM_TYPE nvramType, lu_uint16_t offset);
SB_ERROR NVRamWriteByte(lu_uint8_t value, lu_uint16_t offset, LPC_I2C_TypeDef *i2cChanType, lu_uint32_t	ioID);
SB_ERROR NVRamWritePage(lu_uint8_t *valuePtr, lu_uint16_t offset, LPC_I2C_TypeDef *i2cChanType, lu_uint32_t	ioID);
SB_ERROR NVRamReadPage(lu_uint8_t *dataPtr, lu_uint16_t offset, lu_uint16_t readSize, LPC_I2C_TypeDef *i2cChanType, lu_uint32_t	ioID);
SB_ERROR NVRamRead(lu_uint8_t *dataPtr, lu_uint16_t size, NVRAM_TYPE nvramType, lu_uint16_t offset);
SB_ERROR configureCheckI2cNVRam(lu_uint8_t i2cBus);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported message */
static const filterTableStr NVRAMManagerModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! NVRAMManager commands */
    {  MODULE_MSG_TYPE_NVRAM, MODULE_MSG_ID_NVRAM_UPDATE_BLK_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_NVRAM, MODULE_MSG_ID_NVRAM_WRITE_RAM_BLK_C     , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_NVRAM, MODULE_MSG_ID_NVRAM_READ_RAM_BLK_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_NVRAM, MODULE_MSG_ID_NVRAM_FORCE_BAD_CRC_BLK_C , LU_FALSE , 0      },
};


static const NvramBlkOffsetMapStr appBlkOffsetTable[NVRAM_APP_BLK_LAST] =
{
		{NVRAM_APP_BLK_INFO,   NVRAM_APP_BLK_INFO_OFFSET,   NVRAM_APP_BLK_INFO_SIZE   },
		{NVRAM_APP_BLK_USER_A, NVRAM_APP_BLK_USER_A_OFFSET, NVRAM_APP_BLK_USER_A_SIZE },
		{NVRAM_APP_BLK_CAL,    NVRAM_APP_BLK_CAL_OFFSET,    NVRAM_APP_BLK_CAL_SIZE    },
		{NVRAM_APP_BLK_OPTS,   NVRAM_APP_BLK_OPTS_OFFSET,   NVRAM_APP_BLK_OPTS_SIZE   },
		{NVRAM_APP_BLK_REG,    NVRAM_APP_BLK_REG_OFFSET,    NVRAM_APP_BLK_REG_SIZE    }
};

static const NvramBlkOffsetMapStr idBlkOffsetTable[NVRAM_ID_BLK_LAST] =
{
		{NVRAM_ID_BLK_INFO,  NVRAM_ID_BLK_INFO_OFFSET,  NVRAM_ID_BLK_INFO_SIZE },
		{NVRAM_ID_BLK_TEST,  NVRAM_ID_BLK_TEST_OFFSET,  NVRAM_ID_BLK_TEST_SIZE },
		{NVRAM_ID_BLK_CAL,   NVRAM_ID_BLK_CAL_OFFSET,   NVRAM_ID_BLK_CAL_SIZE  },
		{NVRAM_ID_BLK_OPTS,  NVRAM_ID_BLK_OPTS_OFFSET,  NVRAM_ID_BLK_OPTS_SIZE }
};

static const NvramBlkOffsetMapStr battIdBlkOffsetTable[NVRAM_BATT_ID_BLK_LAST] =
{
		{NVRAM_BATT_ID_BLK_INFO,  NVRAM_BATT_ID_BLK_INFO_OFFSET,  NVRAM_BATT_ID_BLK_INFO_SIZE },
		{NVRAM_BATT_ID_BLK_TEST,  NVRAM_BATT_ID_BLK_TEST_OFFSET,  NVRAM_BATT_ID_BLK_TEST_SIZE },
		{NVRAM_BATT_ID_BLK_CAL,   NVRAM_BATT_ID_BLK_CAL_OFFSET,   NVRAM_BATT_ID_BLK_CAL_SIZE  },
		{NVRAM_BATT_ID_BLK_OPTS,  NVRAM_BATT_ID_BLK_OPTS_OFFSET,  NVRAM_BATT_ID_BLK_OPTS_SIZE }
};

static const NvramBlkOffsetMapStr battDataBlkOffsetTable[NVRAM_BATT_DATA_BLK_LAST] =
{
		{NVRAM_BATT_DATA_BLK_INFO,   NVRAM_BATT_DATA_BLK_INFO_OFFSET,   NVRAM_BATT_DATA_BLK_INFO_SIZE   },
		{NVRAM_BATT_DATA_BLK_USER_A, NVRAM_BATT_DATA_BLK_USER_A_OFFSET, NVRAM_BATT_DATA_BLK_USER_A_SIZE },
		{NVRAM_BATT_DATA_BLK_USER_B, NVRAM_BATT_DATA_BLK_USER_B_OFFSET, NVRAM_BATT_DATA_BLK_USER_B_SIZE },
		{NVRAM_BATT_DATA_BLK_OPTS,   NVRAM_BATT_DATA_BLK_OPTS_OFFSET,   NVRAM_BATT_DATA_BLK_OPTS_SIZE }
};


lu_uint8_t eepromIdMemoryCache[NVRAM_EEPROM_SIZE] __attribute__((section("ahbsram0")));
lu_uint8_t eepromAppMemoryCache[NVRAM_EEPROM_SIZE] __attribute__((section("ahbsram0")));
lu_uint8_t eepromBattIdMemoryCache[NVRAM_EEPROM_SIZE] __attribute__((section("ahbsram0")));
lu_uint8_t eepromBattDataMemoryCache[NVRAM_EEPROM_SIZE] __attribute__((section("ahbsram0")));

NvramUpdateStr nvramTriggerUpdate[NVRAM_TYPE_LAST];

NVRamInitParamsStr nvRamInitParams;

lu_bool_t		   nvRamBatteryDetected;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR NVRamInit(NVRamInitParamsStr *initParamPtr)
{
	 SB_ERROR	 retError;
	 lu_uint32_t idx;

	 retError = SB_ERROR_NONE;

	 nvRamBatteryDetected = LU_FALSE;

	 /* Store init params */
	 nvRamInitParams.ioIdBatteryDataNVRam = initParamPtr->ioIdBatteryDataNVRam;
	 nvRamInitParams.ioIdBatteryIdNVRam   = initParamPtr->ioIdBatteryIdNVRam;
	 nvRamInitParams.ioIdBatTemp          = initParamPtr->ioIdBatTemp;

	 /* Init NRAM update flags */
	 for (idx = 0; idx < NVRAM_TYPE_LAST; idx ++)
	 {
		 nvramTriggerUpdate[idx].updateBlkMask = 0;
		 nvramTriggerUpdate[idx].updateTimerMs = 0;
		 nvramTriggerUpdate[idx].triggerUpdate = LU_FALSE;
	 }

	 /* Verify NVRAM (APP) offset map table */
	 for (idx = 0; idx < NVRAM_APP_BLK_LAST; idx++)
	 {
		 if (appBlkOffsetTable[idx].blockId != idx)
		 {
			 retError = SB_ERROR_INITIALIZED;
			 break;
		 }
	 }

	 /* Verify NVRAM (ID) offset map table */
	 for (idx = 0; idx < NVRAM_ID_BLK_LAST; idx++)
	 {
		 if (idBlkOffsetTable[idx].blockId != idx)
		 {
			 retError = SB_ERROR_INITIALIZED;
			 break;
		 }
	 }

	 /* Verify NVRAM (Battery ID) offset map table */
	 for (idx = 0; idx < NVRAM_BATT_ID_BLK_LAST; idx++)
	 {
		 if (battIdBlkOffsetTable[idx].blockId != idx)
		 {
			 retError = SB_ERROR_INITIALIZED;
			 break;
		 }
	 }

	 /* Verify NVRAM (Battery Data) offset map table */
	 for (idx = 0; idx < NVRAM_BATT_DATA_BLK_LAST; idx++)
	 {
		 if (battDataBlkOffsetTable[idx].blockId != idx)
		 {
			 retError = SB_ERROR_INITIALIZED;
			 break;
		 }
	 }

	 if (retError == SB_ERROR_NONE)
	 {
		/* Configure I2C pins & I2C peripheral */
		configureCheckI2cNVRam(IO_BUS_I2C_0);
		configureCheckI2cNVRam(IO_BUS_I2C_1);

		/* Read NVRAM in memory cache */
		retError = NVRamRead((lu_uint8_t *)&eepromIdMemoryCache[0], NVRAM_EEPROM_SIZE, NVRAM_TYPE_IDENTITY, 0);
		retError = NVRamRead((lu_uint8_t *)&eepromAppMemoryCache[0], NVRAM_EEPROM_SIZE, NVRAM_TYPE_APPLICATION, 0);

		/* Read Battery NVRAM if configured */
		if (nvRamInitParams.ioIdBatteryIdNVRam != IO_ID_NA)
		{
			retError = NVRamRead((lu_uint8_t *)&eepromBattIdMemoryCache[0], NVRAM_EEPROM_SIZE, NVRAM_TYPE_BATTERY_ID, 0);
		}

		if (nvRamInitParams.ioIdBatteryDataNVRam != IO_ID_NA)
		{
			retError = NVRamRead((lu_uint8_t *)&eepromBattDataMemoryCache[0], NVRAM_EEPROM_SIZE, NVRAM_TYPE_BATTERY_DATA, 0);
		}

		if (retError == SB_ERROR_NONE)
		{
			/* Set if Battery NVRAM read is success */
			nvRamBatteryDetected = LU_TRUE;
		}
	 }

	 return retError;
}

SB_ERROR NVRAMInitCANFilter(void)
{
	SB_ERROR                    retError;

	 /* Setup CAN Msg filter */
	retError = CANFramingAddFilter( NVRAMManagerModulefilterTable,
									SU_TABLE_SIZE(NVRAMManagerModulefilterTable, filterTableStr)
								  );
	return retError;
}

SB_ERROR NVRAMCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
 	SB_ERROR	retError;
 	lu_uint16_t dataSize;

 	LU_UNUSED(time);

 	retError = SB_ERROR_CANC;

 	switch (msgPtr->messageType)
	{

	case MODULE_MSG_TYPE_NVRAM:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_NVRAM_UPDATE_BLK_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(NVRAMCmdStr))
			{
				retError = NVRAMUpdateCmd((NVRAMCmdStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_NVRAM_WRITE_RAM_BLK_C:
			/* Message sanity check */
			if (msgPtr->msgLen > MODULE_MESSAGE_SIZE(NVRAMCmdStr))
			{
				dataSize = (msgPtr->msgLen - MODULE_MESSAGE_SIZE(NVRAMCmdStr));
				retError = NVRAMWriteCmd(msgPtr->msgBufPtr, dataSize);
			}
			break;

		case MODULE_MSG_ID_NVRAM_READ_RAM_BLK_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(NVRAMCmdStr))
			{
				retError = NVRAMReadCmd((NVRAMCmdStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_NVRAM_FORCE_BAD_CRC_BLK_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(NVRAMCmdStr))
			{
				retError = NVRAMInvalidateCRCCmd((NVRAMCmdStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

 	return retError;
}

SB_ERROR NVRamUpdateTick(void)
{
	lu_uint32_t  idx;
	lu_int32_t   value;
	SB_ERROR	 retError;

	/* Write to NVRAM Blk offset, when notified of change. Calculate CRC for Block ...*/

	for (idx = 0; idx < NVRAM_TYPE_LAST; idx ++)
	{
		switch (idx)
		{
		case NVRAM_TYPE_APPLICATION:
		case NVRAM_TYPE_BATTERY_DATA:
			if (nvramTriggerUpdate[idx].triggerUpdate == LU_TRUE)
			{
				 nvramTriggerUpdate[idx].updateTimerMs += NVRAM_UPDATE_TICK_MS;

				 if (nvramTriggerUpdate[idx].updateTimerMs >= NVRAM_UPDATE_RATE_MS)
				 {
					 nvramTriggerUpdate[idx].triggerUpdate = LU_FALSE;
					 nvramTriggerUpdate[idx].updateTimerMs = 0;

					 /* Do the update NVRAM write... */
					 switch (idx)
					 {
					 case NVRAM_TYPE_APPLICATION:
						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_APP_BLK_INFO))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_APPLICATION, NVRAM_APP_BLK_INFO);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_APP_BLK_INFO);
						 }

						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_APP_BLK_OPTS))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_APPLICATION, NVRAM_APP_BLK_OPTS);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_APP_BLK_OPTS);
						 }

						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_APP_BLK_USER_A))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_APPLICATION, NVRAM_APP_BLK_USER_A);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_APP_BLK_USER_A);
						 }

						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_APP_BLK_REG))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_APPLICATION, NVRAM_APP_BLK_REG);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_APP_BLK_REG);
						 }

						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_APP_BLK_CAL))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_APPLICATION, NVRAM_APP_BLK_CAL);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_APP_BLK_CAL);
						 }
						 break;

					 case NVRAM_TYPE_BATTERY_DATA:
						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_BATT_DATA_BLK_INFO))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_BATTERY_DATA, NVRAM_BATT_DATA_BLK_INFO);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_BATT_DATA_BLK_INFO);
						 }

						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_BATT_DATA_BLK_USER_A))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_BATTERY_DATA, NVRAM_BATT_DATA_BLK_USER_A);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_BATT_DATA_BLK_USER_A);
						 }

						 if (nvramTriggerUpdate[idx].updateBlkMask & (1 << NVRAM_BATT_DATA_BLK_USER_B))
						 {
							 NVRamWriteBlockType(NVRAM_TYPE_BATTERY_DATA, NVRAM_BATT_DATA_BLK_USER_B);
							 nvramTriggerUpdate[idx].updateBlkMask &= ~(1 << NVRAM_BATT_DATA_BLK_USER_B);
						 }
						 break;

					 default:
						 break;
					 }

				 }
			}
			break;

		default:
			break;
		}
	}

	/* Read Battery NVRAM when detected */
	if (nvRamInitParams.ioIdBatTemp < IO_ID_NA)
	{
		if (IOManagerGetValue(nvRamInitParams.ioIdBatTemp, &value) != SB_ERROR_NONE)
		{
			if (nvRamBatteryDetected == LU_FALSE)
			{
				nvRamBatteryDetected = LU_TRUE;

				/* Read Battery NVRAM */
				if (nvRamInitParams.ioIdBatteryIdNVRam != IO_ID_NA)
				{
					retError = NVRamRead((lu_uint8_t *)&eepromBattIdMemoryCache[0], NVRAM_EEPROM_SIZE, NVRAM_TYPE_BATTERY_ID, 0);
				}

				if (nvRamInitParams.ioIdBatteryDataNVRam != IO_ID_NA)
				{
					retError = NVRamRead((lu_uint8_t *)&eepromBattDataMemoryCache[0], NVRAM_EEPROM_SIZE, NVRAM_TYPE_BATTERY_DATA, 0);
				}
			}
		}
		else
		{
			nvRamBatteryDetected = LU_FALSE;
		}
	}

	return SB_ERROR_NONE;
}

SB_ERROR NVRamApplicationWriteUpdate(NVRAM_APP_BLK nvramBlk)
{
	return NVRamTriggerWriteUpdate(NVRAM_TYPE_APPLICATION, nvramBlk);
}

SB_ERROR NVRamBatteryDataWriteUpdate(NVRAM_BATT_DATA_BLK nvramBlk)
{
	return NVRamTriggerWriteUpdate(NVRAM_TYPE_BATTERY_DATA, nvramBlk);
}

SB_ERROR NVRamApplicationGetHeaderMemoryPointer(NVRAM_APP_BLK nvramBlk, lu_uint8_t **memPtr)
{
	SB_ERROR    retError;

	retError = SB_ERROR_PARAM;

	if (memPtr != NULL)
	{
		*memPtr = NULL;

		retError = NVRamGetMemoryHeaderPointer(NVRAM_TYPE_APPLICATION, nvramBlk, memPtr);
	}

	return retError;
}

SB_ERROR NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK nvramBlk, lu_uint8_t **memPtr)
{
	SB_ERROR    retError;
	lu_uint16_t dataSize;

	retError = SB_ERROR_PARAM;

	if (memPtr != NULL)
	{
		*memPtr = NULL;

		retError = NVRamGetMemoryPointer(NVRAM_TYPE_APPLICATION, nvramBlk, memPtr, &dataSize);
	}

	return retError;
}

SB_ERROR NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK nvramBlk, lu_uint8_t **memPtr)
{
	SB_ERROR    retError;
	lu_uint16_t dataSize;

	retError = SB_ERROR_PARAM;

	if (memPtr != NULL)
	{
		*memPtr = NULL;

		retError = NVRamGetMemoryPointer(NVRAM_TYPE_IDENTITY, nvramBlk, memPtr, &dataSize);
	}

	return retError;
}

SB_ERROR NVRamBatteryIndenityGetMemoryPointer(NVRAM_BATT_ID_BLK nvramBlk, lu_uint8_t **memPtr)
{
	SB_ERROR    retError;
	lu_uint16_t dataSize;

	retError = SB_ERROR_PARAM;

	if (memPtr != NULL)
	{
		*memPtr = NULL;

		retError = NVRamGetMemoryPointer(NVRAM_TYPE_BATTERY_ID, nvramBlk, memPtr, &dataSize);
	}

	return retError;
}

SB_ERROR NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK nvramBlk, lu_uint8_t **memPtr)
{
	SB_ERROR    retError;
	lu_uint16_t dataSize;

	retError = SB_ERROR_PARAM;

	if (memPtr != NULL)
	{
		*memPtr = NULL;

		retError = NVRamGetMemoryPointer(NVRAM_TYPE_BATTERY_DATA, nvramBlk, memPtr, &dataSize);
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR NVRAMUpdateCmd(NVRAMCmdStr *nvramUpdateCmdPtr)
{
	SB_ERROR          retError;
	ModStatusReplyStr retStatus;

	retError = NVRamWriteBlockType(nvramUpdateCmdPtr->nvramType, nvramUpdateCmdPtr->nvramBlk);

	switch (retError)
	{
	case SB_ERROR_PARAM:
	default:
		retStatus.status = REPLY_STATUS_PARAM_ERROR;
		break;

	case SB_ERROR_I2C_FAIL:
		retStatus.status = REPLY_STATUS_WRITE_ERROR;
		break;

	case SB_ERROR_NONE:
		retStatus.status = REPLY_STATUS_OKAY;
		break;
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_NVRAM,
							MODULE_MSG_ID_NVRAM_UPDATE_BLK_R,
							sizeof(ModStatusReplyStr),
							(lu_uint8_t *)&retStatus
						   );

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR NVRAMInvalidateCRCCmd(NVRAMCmdStr *nvramUpdateCmdPtr)
{
	SB_ERROR          retError;
	ModStatusReplyStr retStatus;
	lu_uint8_t *nvramBlkPtr;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;
	lu_uint16_t offset;

	retError = SB_ERROR_PARAM;

	/* Get base address to memory */
	switch (nvramUpdateCmdPtr->nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		nvramBlkPtr       = (lu_uint8_t *)&eepromIdMemoryCache[0];

		switch (nvramUpdateCmdPtr->nvramBlk)
		{
		case NVRAM_ID_BLK_INFO:
		case NVRAM_ID_BLK_TEST:
		case NVRAM_ID_BLK_CAL:
		case NVRAM_ID_BLK_OPTS:
			nvramBlkPtr      += idBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			offset            = idBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_APPLICATION:
		nvramBlkPtr       = (lu_uint8_t *)&eepromAppMemoryCache[0];

		switch (nvramUpdateCmdPtr->nvramBlk)
		{
		case NVRAM_APP_BLK_INFO:
		case NVRAM_APP_BLK_USER_A:
		case NVRAM_APP_BLK_REG:
		case NVRAM_APP_BLK_CAL:
		case NVRAM_APP_BLK_OPTS:
			nvramBlkPtr      += appBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			offset            = appBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_ID:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattIdMemoryCache[0];

		switch (nvramUpdateCmdPtr->nvramBlk)
		{
		case NVRAM_BATT_ID_BLK_INFO:
		case NVRAM_BATT_ID_BLK_TEST:
		case NVRAM_BATT_ID_BLK_CAL:
			nvramBlkPtr      += battIdBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			offset            = battIdBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattDataMemoryCache[0];

		switch (nvramUpdateCmdPtr->nvramBlk)
		{
		case NVRAM_BATT_DATA_BLK_INFO:
		case NVRAM_BATT_DATA_BLK_USER_A:
		case NVRAM_BATT_DATA_BLK_USER_B:
			nvramBlkPtr      += battDataBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			offset            = battDataBlkOffsetTable[nvramUpdateCmdPtr->nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	default:
		break;
	}

	/* Invalidate the CRC!! */
	switch (nvramUpdateCmdPtr->nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
	case NVRAM_TYPE_APPLICATION:
	case NVRAM_TYPE_BATTERY_ID:
	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkHeaderPtr = (NVRAMBlkHeadStr *)nvramBlkPtr;

		nvramBlkHeaderPtr->dataCrc32 = ~nvramBlkHeaderPtr->dataCrc32;

		retError = NVRamWrite((lu_uint8_t *)&nvramBlkHeaderPtr->dataCrc32, (sizeof(nvramBlkHeaderPtr->dataCrc32)), nvramUpdateCmdPtr->nvramType, (offset + 2));
		break;

	default:
		break;
	}

	switch (retError)
	{
	case SB_ERROR_PARAM:
	default:
		retStatus.status = REPLY_STATUS_PARAM_ERROR;
		break;

	case SB_ERROR_I2C_FAIL:
		retStatus.status = REPLY_STATUS_WRITE_ERROR;
		break;

	case SB_ERROR_NONE:
		retStatus.status = REPLY_STATUS_OKAY;
		break;
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_NVRAM,
							MODULE_MSG_ID_NVRAM_FORCE_BAD_CRC_BLK_R,
							sizeof(ModStatusReplyStr),
							(lu_uint8_t *)&retStatus
						   );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRAMWriteCmd(lu_uint8_t *memPtr, lu_uint16_t dataSize)
{
	SB_ERROR            retError;
	ModStatusReplyStr  	retStatus;
	lu_uint16_t         maxDataSize;
	NVRAMCmdStr         *nvramCmdPtr;
	lu_uint8_t			*dataSrcPtr;
	lu_uint8_t          *nvramDataPtr;

	nvramCmdPtr = (NVRAMCmdStr *)memPtr;
	dataSrcPtr  = (memPtr + sizeof(NVRAMCmdStr));

	retError = NVRamGetMemoryPointer(nvramCmdPtr->nvramType, nvramCmdPtr->nvramType, &nvramDataPtr, &maxDataSize);

	retStatus.status = REPLY_STATUS_PARAM_ERROR;
	if (retError == SB_ERROR_NONE && dataSize <= maxDataSize)
	{
		retStatus.status = REPLY_STATUS_OKAY;

		/* Copy data to NVRAM memory buffer */
		memcpy(nvramDataPtr, dataSrcPtr, dataSize);
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_NVRAM,
							MODULE_MSG_ID_NVRAM_WRITE_RAM_BLK_R,
							sizeof(ModStatusReplyStr),
							(lu_uint8_t *)&retStatus
						   );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRAMReadCmd(NVRAMCmdStr *nvramReadCmdPtr)
{
	SB_ERROR            retError;
	lu_uint16_t			dataSize;
	lu_uint8_t          *nvramDataPtr;

	/* Default to zero length response , should it fail */
	dataSize     = 0;
	nvramDataPtr = 0L;

	retError = NVRamGetMemoryPointer(nvramReadCmdPtr->nvramType, nvramReadCmdPtr->nvramType, &nvramDataPtr, &dataSize);

	if (retError == SB_ERROR_NONE)
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_NVRAM,
								MODULE_MSG_ID_NVRAM_READ_RAM_BLK_R,
								dataSize,
								(lu_uint8_t *)nvramDataPtr
							   );
	}
	else
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_NVRAM,
								MODULE_MSG_ID_NVRAM_READ_RAM_BLK_R,
								0,
								0L
							   );
	}
	return retError;
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamTriggerWriteUpdate(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk)
{
	SB_ERROR    retError;

	retError = SB_ERROR_PARAM;

	switch (nvramType)
	{
	case NVRAM_TYPE_APPLICATION:

		switch (nvramBlk)
		{
		case NVRAM_APP_BLK_INFO:
		    retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_APP_BLK_INFO);
			break;

		case NVRAM_APP_BLK_USER_A:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_APP_BLK_USER_A);
			break;

		case NVRAM_APP_BLK_REG:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_APP_BLK_REG);
			break;

		case NVRAM_APP_BLK_CAL:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_APP_BLK_CAL);
			break;

		case NVRAM_APP_BLK_OPTS:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_APP_BLK_OPTS);
			break;

		default:
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_DATA:
		switch (nvramBlk)
		{
		case NVRAM_BATT_DATA_BLK_INFO:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_BATT_DATA_BLK_INFO);
			break;

		case NVRAM_BATT_DATA_BLK_USER_A:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_BATT_DATA_BLK_USER_A);
			break;

		case NVRAM_BATT_DATA_BLK_USER_B:
			retError = SB_ERROR_NONE;
			nvramTriggerUpdate[nvramType].triggerUpdate = LU_TRUE;
			nvramTriggerUpdate[nvramType].updateBlkMask |= (1 << NVRAM_BATT_DATA_BLK_USER_B);
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR NVRamGetMemoryPointer(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk, lu_uint8_t **memPtr, lu_uint16_t *dataSizePtr)
{
	SB_ERROR    retError;
	lu_uint8_t *nvramBlkPtr;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;
	lu_uint16_t dataSize;
	lu_uint32_t crc32;

	retError = SB_ERROR_PARAM;

	/* Get base address to memory */
	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		nvramBlkPtr       = (lu_uint8_t *)&eepromIdMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_ID_BLK_INFO:
		case NVRAM_ID_BLK_TEST:
		case NVRAM_ID_BLK_CAL:
		case NVRAM_ID_BLK_OPTS:
			nvramBlkPtr      += idBlkOffsetTable[nvramBlk].offset;
			dataSize          = (idBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_APPLICATION:
		nvramBlkPtr       = (lu_uint8_t *)&eepromAppMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_APP_BLK_INFO:
		case NVRAM_APP_BLK_USER_A:
		case NVRAM_APP_BLK_REG:
		case NVRAM_APP_BLK_CAL:
		case NVRAM_APP_BLK_OPTS:
			nvramBlkPtr      += appBlkOffsetTable[nvramBlk].offset;
			dataSize          = (appBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_ID:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattIdMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_BATT_ID_BLK_INFO:
		case NVRAM_BATT_ID_BLK_TEST:
		case NVRAM_BATT_ID_BLK_CAL:
		case NVRAM_BATT_ID_BLK_OPTS:
			nvramBlkPtr      += battIdBlkOffsetTable[nvramBlk].offset;
			dataSize          = (battIdBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattDataMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_BATT_DATA_BLK_INFO:
		case NVRAM_BATT_DATA_BLK_USER_A:
		case NVRAM_BATT_DATA_BLK_USER_B:
		case NVRAM_BATT_DATA_BLK_OPTS:
			nvramBlkPtr      += battDataBlkOffsetTable[nvramBlk].offset;
			dataSize          = (battDataBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	default:
		break;
	}


	/* Calculate & check CRC */
	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
	case NVRAM_TYPE_APPLICATION:
	case NVRAM_TYPE_BATTERY_ID:
	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkHeaderPtr = (NVRAMBlkHeadStr *)nvramBlkPtr;
		nvramBlkPtr      += sizeof(NVRAMBlkHeadStr);

		/* Check data block integrity crc ?? */
		crc32_calc32((lu_uint8_t * )nvramBlkPtr, dataSize, &crc32);

		/* Return pointer to data + size */
		*memPtr      = nvramBlkPtr;
		*dataSizePtr = dataSize;

		if (nvramBlkHeaderPtr->dataCrc32 == crc32)
		{
			retError = SB_ERROR_NONE;
		}
		else
		{
			retError = SB_ERROR_NVRAM_CRC;
		}
		break;

	default:
		break;
	}

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR NVRamGetMemoryHeaderPointer(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk, lu_uint8_t **memPtr)
{
	SB_ERROR    retError;
	lu_uint8_t *nvramBlkPtr;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;
	lu_uint16_t dataSize;
	lu_uint32_t crc32;

	retError = SB_ERROR_PARAM;

	/* Get base address to memory */
	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		nvramBlkPtr       = (lu_uint8_t *)&eepromIdMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_ID_BLK_INFO:
		case NVRAM_ID_BLK_TEST:
		case NVRAM_ID_BLK_CAL:
		case NVRAM_ID_BLK_OPTS:
			nvramBlkPtr      += idBlkOffsetTable[nvramBlk].offset;
			dataSize          = (idBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_APPLICATION:
		nvramBlkPtr       = (lu_uint8_t *)&eepromAppMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_APP_BLK_INFO:
		case NVRAM_APP_BLK_USER_A:
		case NVRAM_APP_BLK_REG:
		case NVRAM_APP_BLK_CAL:
		case NVRAM_APP_BLK_OPTS:
			nvramBlkPtr      += appBlkOffsetTable[nvramBlk].offset;
			dataSize          = (appBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_ID:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattIdMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_BATT_ID_BLK_INFO:
		case NVRAM_BATT_ID_BLK_TEST:
		case NVRAM_BATT_ID_BLK_CAL:
			nvramBlkPtr      += battIdBlkOffsetTable[nvramBlk].offset;
			dataSize          = (battIdBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattDataMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_BATT_DATA_BLK_INFO:
		case NVRAM_BATT_DATA_BLK_USER_A:
		case NVRAM_BATT_DATA_BLK_USER_B:
			nvramBlkPtr      += battDataBlkOffsetTable[nvramBlk].offset;
			dataSize          = (battDataBlkOffsetTable[nvramBlk].length - sizeof(NVRAMBlkHeadStr));
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	default:
		break;
	}


	/* Calculate & check CRC */
	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
	case NVRAM_TYPE_APPLICATION:
	case NVRAM_TYPE_BATTERY_ID:
	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkHeaderPtr = (NVRAMBlkHeadStr *)nvramBlkPtr;
		nvramBlkPtr      += sizeof(NVRAMBlkHeadStr);

		/* Check data block integrity crc ?? */
		crc32_calc32((lu_uint8_t * )nvramBlkPtr, dataSize, &crc32);

		/* Return pointer to header */
		*memPtr      = (lu_uint8_t *)nvramBlkHeaderPtr;

		if (nvramBlkHeaderPtr->dataCrc32 == crc32)
		{
			retError = SB_ERROR_NONE;
		}
		else
		{
			retError = SB_ERROR_NVRAM_CRC;
		}
		break;

	default:
		break;
	}

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
LPC_I2C_TypeDef *findNVRAMI2CChannel(NVRAM_TYPE nvramType, lu_uint32_t *ioIDPtr, SB_ERROR *retValPtr)
 {
	 lu_uint32_t				ioID;
	 LPC_I2C_TypeDef           *i2cChanType;

	 *retValPtr = SB_ERROR_PARAM;

	 for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	 {
		 /* Is an Peripheral */
		 if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH)
		 {
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_I2C_NVRAM:
				switch (nvramType)
				{
				case NVRAM_TYPE_IDENTITY:
					if (ioID == IO_ID_IDENTITY_NVRAM)
					{
						*retValPtr = SB_ERROR_NONE;
						*ioIDPtr  = ioID;
					}
					break;

				case NVRAM_TYPE_APPLICATION:
					if (ioID == IO_ID_APPLICATION_NVRAM)
					{
						*retValPtr = SB_ERROR_NONE;
						*ioIDPtr  = ioID;
					}
					break;

				case NVRAM_TYPE_BATTERY_ID:
					if (ioID == nvRamInitParams.ioIdBatteryIdNVRam)
					{
						*retValPtr = SB_ERROR_NONE;
						*ioIDPtr  = ioID;
					}
					break;

				case NVRAM_TYPE_BATTERY_DATA:
					if (ioID == nvRamInitParams.ioIdBatteryDataNVRam)
					{
						*retValPtr = SB_ERROR_NONE;
						*ioIDPtr  = ioID;
					}
					break;

				default:
					break;
				}
				break;

			default:
				break;
			}
		 }
	 }

	 if (*retValPtr == SB_ERROR_NONE)
	 {
		 switch (BoardIOMap[*ioIDPtr].ioAddr.i2cNVRAM.busI2c)
		 {
		 case IO_BUS_I2C_0:
			i2cChanType = LPC_I2C0;
			break;

		 case IO_BUS_I2C_1:
			i2cChanType = LPC_I2C1;
			break;

		 case IO_BUS_I2C_2:
			i2cChanType = LPC_I2C2;
			break;

		 default:
			*retValPtr = SB_ERROR_PARAM;
			break;
		 }
	 }

	 return i2cChanType;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamWriteBlockType(NVRAM_TYPE nvramType, lu_uint8_t nvramBlk)
{
	SB_ERROR				retError;
	lu_uint8_t *nvramBlkPtr;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;
	lu_uint16_t dataSize;
	lu_uint16_t offset;
	lu_uint32_t crc32;

	retError = SB_ERROR_PARAM;

	/* Get base address to memory */
	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		nvramBlkPtr       = (lu_uint8_t *)&eepromIdMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_ID_BLK_INFO:
		case NVRAM_ID_BLK_TEST:
		case NVRAM_ID_BLK_CAL:
		case NVRAM_ID_BLK_OPTS:
			nvramBlkPtr      += idBlkOffsetTable[nvramBlk].offset;
			dataSize          = idBlkOffsetTable[nvramBlk].length;
			offset            = idBlkOffsetTable[nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_APPLICATION:
		nvramBlkPtr       = (lu_uint8_t *)&eepromAppMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_APP_BLK_INFO:
		case NVRAM_APP_BLK_USER_A:
		case NVRAM_APP_BLK_REG:
		case NVRAM_APP_BLK_CAL:
		case NVRAM_APP_BLK_OPTS:
			nvramBlkPtr      += appBlkOffsetTable[nvramBlk].offset;
			dataSize          = appBlkOffsetTable[nvramBlk].length;
			offset            = appBlkOffsetTable[nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_ID:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattIdMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_BATT_ID_BLK_INFO:
		case NVRAM_BATT_ID_BLK_TEST:
		case NVRAM_BATT_ID_BLK_CAL:
			nvramBlkPtr      += battIdBlkOffsetTable[nvramBlk].offset;
			dataSize          = battIdBlkOffsetTable[nvramBlk].length;
			offset            = battIdBlkOffsetTable[nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkPtr       = (lu_uint8_t *)&eepromBattDataMemoryCache[0];

		switch (nvramBlk)
		{
		case NVRAM_BATT_DATA_BLK_INFO:
		case NVRAM_BATT_DATA_BLK_USER_A:
		case NVRAM_BATT_DATA_BLK_USER_B:
			nvramBlkPtr      += battDataBlkOffsetTable[nvramBlk].offset;
			dataSize          = battDataBlkOffsetTable[nvramBlk].length;
			offset            = battDataBlkOffsetTable[nvramBlk].offset;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
		}
		break;

	default:
		break;
	}

	/* re-Calculate the CRC */
	switch (nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
	case NVRAM_TYPE_APPLICATION:
	case NVRAM_TYPE_BATTERY_ID:
	case NVRAM_TYPE_BATTERY_DATA:
		nvramBlkHeaderPtr = (NVRAMBlkHeadStr *)nvramBlkPtr;
		nvramBlkPtr      += sizeof(NVRAMBlkHeadStr);

		dataSize                    -= sizeof(NVRAMBlkHeadStr);
		nvramBlkHeaderPtr->dataSize  = dataSize;

		/* Increment counter */
		nvramBlkHeaderPtr->version.updateCounter++;

		/* Fix data block crc32 */
		crc32_calc32((lu_uint8_t * )nvramBlkPtr, nvramBlkHeaderPtr->dataSize, &crc32);

		nvramBlkHeaderPtr->dataCrc32 = crc32;

		retError = NVRamWrite((lu_uint8_t *)nvramBlkHeaderPtr, (dataSize + sizeof(NVRAMBlkHeadStr)), nvramType, offset);
		break;

	default:
		break;
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamWrite(lu_uint8_t *dataPtr, lu_uint16_t size, NVRAM_TYPE nvramType, lu_uint16_t offset)
{
	SB_ERROR				retErr;
	lu_uint32_t				ioID;
	LPC_I2C_TypeDef			*i2cChanType;
	lu_uint16_t				count;
	lu_uint8_t              *writeDataPtr;

	retErr = SB_ERROR_PARAM;

	i2cChanType = findNVRAMI2CChannel(nvramType, &ioID, &retErr);

	if (retErr == SB_ERROR_NONE)
	{
		/* Enable EEPROM writes */
		if (BoardIOMap[ioID].ioAddr.i2cNVRAM.wenIoID < BOARD_IO_MAX_IDX)
		{
			IOManagerSetValue(BoardIOMap[ioID].ioAddr.i2cNVRAM.wenIoID, 0);
		}

		count        = size;
		writeDataPtr = dataPtr;

		while (count && retErr == SB_ERROR_NONE)
		{
			if ((offset & 0x0f) == 0 && count >= MAX_PAGE_WRITE)
			{
				/* Aligned, so Write a page */
				retErr = NVRamWritePage(writeDataPtr, offset, i2cChanType, ioID);
				count        -= MAX_PAGE_WRITE;
				writeDataPtr += MAX_PAGE_WRITE;
				offset       += MAX_PAGE_WRITE;
			}
			else
			{
				/* Not aligned, so write bytes */
				retErr = NVRamWriteByte(*writeDataPtr, offset, i2cChanType, ioID);
				count--;
				writeDataPtr++;
				offset++;
			}
		}

		/* Disable EEPROM writes */
		if (BoardIOMap[ioID].ioAddr.i2cNVRAM.wenIoID < BOARD_IO_MAX_IDX)
		{
			IOManagerSetValue(BoardIOMap[ioID].ioAddr.i2cNVRAM.wenIoID, 0);
		}
	}
	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamWriteByte(lu_uint8_t value, lu_uint16_t offset, LPC_I2C_TypeDef *i2cChanType, lu_uint32_t	ioID)
{
	SB_ERROR				retErr;
	lu_uint32_t				slaveAddrOffset;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint16_t				count;
	lu_uint8_t			    txData[3];

	retErr = SB_ERROR_NONE;

	/* AT24C08 type EEPROM */

	/* Slave address + offset bits */
	slaveAddrOffset  = ((BoardIOMap[ioID].ioAddr.i2cNVRAM.address << 2) & 0xfc);
	slaveAddrOffset |= ((offset >> 8)) & 0x03;

	/* Word Address */
	txData[0]						= (offset & 0xff);
	/* Data Value */
	txData[1]						= value;

	txferCfg.sl_addr7bit     	    = slaveAddrOffset;
	txferCfg.tx_data		  		= txData;
	txferCfg.tx_length       		= 2;
	txferCfg.rx_data		 	 	= NULL;
	txferCfg.rx_length       		= 0;
	txferCfg.retransmissions_max 	= 2;

	if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	{
		retErr = SB_ERROR_I2C_FAIL;
	}
	else
	{
		STDelayTime(5);
#if 0
		 /* Must wait for transfer to complete!! */
		 for (count = 0; count < 10000; count++)
		 {
			 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
				 break;
		 }
#endif
	}

	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamWritePage(lu_uint8_t *valuePtr, lu_uint16_t offset, LPC_I2C_TypeDef *i2cChanType, lu_uint32_t	ioID)
{
	SB_ERROR				retErr;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint16_t				count;
	lu_uint32_t				slaveAddrOffset;
	lu_uint8_t			    txData[MAX_PAGE_WRITE + 3];

	retErr = SB_ERROR_NONE;

	/* AT24C08 type EEPROM */

	/* Slave address + offset bits */
	slaveAddrOffset  = ((BoardIOMap[ioID].ioAddr.i2cNVRAM.address << 2) & 0xfc);
	slaveAddrOffset |= ((offset >> 8)) & 0x03;

	/* Word Address */
	txData[0]						= (offset & 0xff);

	/* Data Value */
	memcpy(&txData[1], valuePtr, MAX_PAGE_WRITE);

	txferCfg.sl_addr7bit     	    = slaveAddrOffset;
	txferCfg.tx_data		  		= txData;
	txferCfg.tx_length       		= (MAX_PAGE_WRITE + 1);
	txferCfg.rx_data		 	 	= NULL;
	txferCfg.rx_length       		= 0;
	txferCfg.retransmissions_max 	= 2;

	if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	{
		retErr = SB_ERROR_I2C_FAIL;
	}
	else
	{
		/* Now do Acknowledge polling to wait for write to complete - should be no more than 5ms */
		STDelayTime(6);
#if 0
		 /* Must wait for transfer to complete!! */
		 for (count = 0; count < 10000; count++)
		 {
			 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
				 break;
		 }
#endif
	}

	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamReadPage(lu_uint8_t *dataPtr, lu_uint16_t offset, lu_uint16_t readSize, LPC_I2C_TypeDef *i2cChanType, lu_uint32_t	ioID)
{
	SB_ERROR				retErr;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint16_t				count;
	lu_uint32_t				slaveAddrOffset;
	lu_uint8_t			    txData[3];

	retErr = SB_ERROR_NONE;

	/* AT24C08 type EEPROM */

	/* Slave address + offset bits */
	slaveAddrOffset  = ((BoardIOMap[ioID].ioAddr.i2cNVRAM.address << 2) & 0xfc);
	slaveAddrOffset |= ((offset >> 8)) & 0x03;

	/* Word Address */
	txData[0]						= (offset & 0xff);

	txferCfg.sl_addr7bit     	    = slaveAddrOffset;
	txferCfg.tx_data		  		= txData;
	txferCfg.tx_length       		= 1;
	txferCfg.rx_data		 	 	= dataPtr;
	txferCfg.rx_length       		= readSize;
	txferCfg.retransmissions_max 	= 2;

	if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	{
		retErr = SB_ERROR_I2C_FAIL;
	}
	else
	{
		STDelayTime(6);
#if 0
		 /* Must wait for transfer to complete!! */
		 for (count = 0; count < 10000; count++)
		 {
			 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
				 break;
		 }
#endif
	}

	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR NVRamRead(lu_uint8_t *dataPtr, lu_uint16_t size, NVRAM_TYPE nvramType, lu_uint16_t offset)
{
	SB_ERROR				retErr;
	lu_uint32_t				ioID;
	LPC_I2C_TypeDef			*i2cChanType;
	lu_uint16_t				count;
	lu_uint8_t              *readDataPtr;

	retErr = SB_ERROR_PARAM;

	i2cChanType = findNVRAMI2CChannel(nvramType, &ioID, &retErr);

	if (retErr == SB_ERROR_NONE)
	{
		count        = size;
		readDataPtr = dataPtr;

		while (count)
		{
			if (count >= MAX_PAGE_READ)
			{
				/* Read a max page size */
				retErr = NVRamReadPage(readDataPtr, offset, MAX_PAGE_READ, i2cChanType, ioID);
				if (retErr != SB_ERROR_NONE)
				{
					break;
				}
				count        -= MAX_PAGE_READ;
				readDataPtr  += MAX_PAGE_READ;
				offset       += MAX_PAGE_READ;
			}
			else
			{
				/* Read less than a page */
				retErr = NVRamReadPage(readDataPtr, offset, count, i2cChanType, ioID);
				if (retErr != SB_ERROR_NONE)
				{
					break;
				}
				readDataPtr += count;
				offset      += count;
				count        = 0;
			}
		}
	}

	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Check for NVRAM I2C device & configure I2C if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckI2cNVRam(lu_uint8_t i2cBus)
{
	SB_ERROR				retVal;
	lu_uint32_t				ioID;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		 /* Is an Peripheral */
		 if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH)
		 {
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_I2C_NVRAM:
				if (BoardIOMap[ioID].ioAddr.i2cNVRAM.busI2c == i2cBus)
				{
					// find & configure peripheral pins
					retVal = configureI2CPeripheral(i2cBus);
					ioID = BOARD_IO_MAX_IDX;
				}
				break;

			default:
				break;
			}
		 }
	 }

	return SB_ERROR_NONE;
}



/*
 *********************** End of file ******************************************
 */
