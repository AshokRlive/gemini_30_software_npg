/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       NVRAM driver library module header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/09/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _NVRAM_INCLUDED
#define _NVRAM_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "NVRAMDef.h"
#include "errorCodes.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define NVRAM_UPDATE_TICK_MS		500 // NVRAM update check


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* NVRAM Initialisation Parameters */
typedef struct NVRamInitParamsDef
{
	/* Battery related ioID's */
	lu_uint32_t ioIdBatteryIdNVRam;
	lu_uint32_t ioIdBatteryDataNVRam;
	lu_uint32_t ioIdBatTemp;
}NVRamInitParamsStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Init NVRAM module
 *
 *   Initialise the I2C peripheral for the NVRAM module
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamInit(NVRamInitParamsStr *initParamPtr);

/*!
 ******************************************************************************
 *   \brief Init NVRAM module CAN
 *
 *   Initialise the NVRAM module CAN decoder
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRAMInitCANFilter(void);

/*!
 ******************************************************************************
 *   \brief Init NVRAM module CAN
 *
 *   Initialise the NVRAM module CAN decoder
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRAMCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief NVRAM Update manager
 *
 *   Write to the I2C peripheral for the NVRAM module, scheduled tick
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamUpdateTick(void);

/*!
 ******************************************************************************
 *   \brief Application Get pointer to NVRAM data block
 *
 *   Get pointer to NVRAM data block for specified block type
 *
 *   \param nvramBlk
 *   \param memPtr return pointer to ptr to return memory addr where nvdata stored
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK nvramBlk, lu_uint8_t **memPtr);

/*!
 ******************************************************************************
 *   \brief Application trigger update to NVRAM data block
 *
 *
 *
 *   \param nvramBlk
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamApplicationWriteUpdate(NVRAM_APP_BLK nvramBlk);

/*!
 ******************************************************************************
 *   \brief Battery Data triger update to NVRAM data block
 *
 *
 *
 *   \param nvramBlk
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamBatteryDataWriteUpdate(NVRAM_BATT_DATA_BLK nvramBlk);

/*!
 ******************************************************************************
 *   \brief Get pointer to NVRAM block header
 *
 *   Get pointer to NVRAM block header for specified block type
 *
 *   \param nvramBlk
 *   \param memPtr return pointer to ptr to return memory addr where NV header stored
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamApplicationGetHeaderMemoryPointer(NVRAM_APP_BLK nvramBlk, lu_uint8_t **memPtr);

/*!
 ******************************************************************************
 *   \brief ID Get pointer to NVRAM data block
 *
 *   Get pointer to NVRAM data block for specified block type
 *
 *   \param nvramBlk
 *   \param memPtr return pointer to ptr to return memory addr where nvdata stored
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK nvramBlk, lu_uint8_t **memPtr);

/*!
 ******************************************************************************
 *   \brief Battery ID Get pointer to NVRAM data block
 *
 *   Get pointer to NVRAM data block for specified block type
 *
 *   \param nvramBlk
 *   \param memPtr return pointer to ptr to return memory addr where nvdata stored
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamBatteryIndenityGetMemoryPointer(NVRAM_BATT_ID_BLK nvramBlk, lu_uint8_t **memPtr);

/*!
 ******************************************************************************
 *   \brief Battery Data Get pointer to NVRAM data block
 *
 *   Get pointer to NVRAM data block for specified block type
 *
 *   \param nvramBlk
 *   \param memPtr return pointer to ptr to return memory addr where nvdata stored
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK nvramBlk, lu_uint8_t **memPtr);

#endif /* _NVRAM_INCLUDED */

/*
 *********************** End of file ******************************************
 */
