/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Controller common definitions.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/012/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SWITCHCOMMON_INCLUDED
#define _SWITCHCOMMON_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef enum
{
	SW_CON_CHAN_OPEN = 0,
	SW_CON_CHAN_CLOSE,

	SW_CON_CHAN_LAST
}SW_CON_CHAN;

/* Switch Control Parameters */
typedef struct SwConChanParamsDef
{
	lu_int32_t ioIdRelayUp;
	lu_int32_t ioIdRelayDown;
	lu_int32_t ioIdRelayLink;
}SwConChanParamsStr;

/* Switch Control Initialisation Parameters */
typedef struct SwConInitParamsDef
{
	SwConChanParamsStr SwitchConParams[SW_CON_CHAN_LAST];
	/* Front Panel LEDs */
	lu_uint32_t ioIdLedFPCmdOpen;
	lu_uint32_t ioIdLedFPCmdClose;
	lu_uint32_t ioIdLedFPMotorSupply;
	/* LEDs */
	lu_uint32_t ioIdLedCtrlOkGreen;
	lu_uint32_t ioIdLedCtrlOkRed;
	/* Relay FB Inputs */
	lu_uint32_t ioIdRelayFbOpenUpClosed;
	lu_uint32_t ioIdRelayFbOpenDnClosed;
	lu_uint32_t ioIdRelayFbOpenLkClosed;
	lu_uint32_t ioIdRelayFbCloseUpClosed;
	lu_uint32_t ioIdRelayFbCloseDnClosed;
	lu_uint32_t ioIdRelayFbCloseLkClosed;
	lu_uint32_t ioIdRelayFbFpiexUpClosed;
	lu_uint32_t ioIdRelayFbFpiexDnClosed;
	lu_uint32_t ioIdRelayFbFpiexLkClosed;
	lu_uint32_t ioIdRelayFbAuxUpClosed;
	lu_uint32_t ioIdRelayFbAuxDnClosed;
	lu_uint32_t ioIdRelayFbAuxLkClosed;
	lu_uint32_t ioIdVMotorFbOn;
	/* Relay Control Outputs */
	lu_uint32_t ioIdRelayCtrlVMotorA;
	lu_uint32_t ioIdRelayCtrlVMotorB;
	/* Inputs */
	lu_uint32_t ioIdExternalFPIInput;
	lu_uint32_t ioIdInsulationLowInput;
	lu_uint32_t ioIdSwitchOpenInput;
	lu_uint32_t ioIdSwitchClosedInput;
	lu_uint32_t ioIdActuatorDisabledInput;
	lu_uint32_t ioIdSpare1_Input;
	lu_uint32_t ioIdSpare2_Input;
	lu_uint32_t ioIdSwitchEarthedInput;

	lu_uint32_t ChannelIOD[16];

}SwConInitParamsStr;

/* User Event Parameters */
typedef struct SwEventParamsDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out  in seconds */
	lu_uint8_t		timeout;

	/*! Time out  in milliseconds */
	lu_uint16_t		preOpTime;

	/*! Local or remote operation indicator */
	lu_bool_t		local;

	/*! Status - okay or error */
	lu_uint8_t		status;

	/* Time message received */
	lu_uint32_t 	receiveTime;

}SwEventParamsStr;

/* User defined state machine context info */
typedef struct SwitchUserContextDef
{
	lu_uint8_t activeChannel;
	SwConInitParamsStr swConInitParams;
}SwitchUserContextStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _SWITCHCOMMON_INCLUDED */

/*
 *********************** End of file ******************************************
 */
