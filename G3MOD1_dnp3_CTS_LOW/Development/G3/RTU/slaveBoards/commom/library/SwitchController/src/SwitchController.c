/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Controller module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "SwitchController.h"
#include "SwitchFSM.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR SwitchSelectHandler(SwitchSelectStr *);
SB_ERROR SwitchOperateHandler(SwitchOperateStr *);
SB_ERROR SwitchCancelHandler(SwitchCancelStr *);
SB_ERROR SwitchSynchHandler(SwitchSynchStr *);
SB_ERROR SwitchConfig(SwitchConfigStr *);
void 	 SwitchFSMInit(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
SwitchConfigStr SwitchConfiguration[MAX_SWITCH_CHANNELS], *SwitchConfigurationPtr;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static struct Switch switchFsm;


/*! List of supported CAN messages */
static const filterTableStr SwitchControlModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Switch commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_SELECT_C             , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C            , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C             , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_SYNCH_C              , LU_FALSE , 0   	  },
	{  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_SWITCH_C            		  , LU_FALSE , 1   	  }

};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void SwitchFSMTick(void)
{
	/*
	 * Periodic Tick to initiate events in state machine.
	 */
	SwitchContext_TimerTickEvent(&switchFsm._fsm);

}

lu_bool_t SwitchControlStart(void)
{
	lu_uint8_t i;
	lu_bool_t  retVal = LU_TRUE;

	/*
	 * Determine if all the channels have been configured.
	 */
	for(i = 0; i < MAX_SWITCH_CHANNELS; i++)
	{
		/* Set configuration pointer to correct channel */
		SwitchConfigurationPtr = &SwitchConfiguration[i];
		if (SwitchConfigurationPtr->SwitchEnabled != LU_TRUE)
		{
			/* If any channel hasn't been configured return false */
			retVal = LU_FALSE;
		}
	}

	/*
	 * If all channels have been configured then send event to state machine
	 */
	if(retVal == LU_TRUE )
	{
		/*
		 * Initiate start event in state machine.
		 */
		SwitchContext_StartEvent(&switchFsm._fsm);
	}

	return retVal;

}

SB_ERROR SwitchControlInit(SwConInitParamsStr *initParamPtr)
{
	SB_ERROR sbError;

	/* Setup CAN Msg filter */
	sbError = CANFramingAddFilter( SwitchControlModulefilterTable,
								   SU_TABLE_SIZE(SwitchControlModulefilterTable,
											  filterTableStr)
							     );

	/* Copy initialisation parameters to the context structure */
	switchFsm.switchStatus.swConInitParams = *initParamPtr;

	/* Initialise state-machine context */
	SwitchFSMInit();

	return sbError;
}

SB_ERROR SwitchControlCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC;

	LU_UNUSED( time );

	/* Force a timer tick in the state machine */
	SwitchFSMTick();

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_SWC_CH_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SwitchSelectStr))
			{
				retError = SwitchSelectHandler((SwitchSelectStr *)msgPtr->msgBufPtr);
			}
			break;
		case MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SwitchOperateStr))
			{
				retError = SwitchOperateHandler((SwitchOperateStr *)msgPtr->msgBufPtr);
			}
			break;
		case MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SwitchCancelStr))
			{
				retError = SwitchCancelHandler((SwitchCancelStr *)msgPtr->msgBufPtr);
			}
			break;
		case MODULE_MSG_ID_CMD_SWC_CH_SYNCH_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SwitchSynchStr))
			{
				retError = SwitchSynchHandler((SwitchSynchStr *)msgPtr->msgBufPtr);
			}
			break;



		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;


	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_SWITCH_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SwitchConfigStr))
			{
				retError = SwitchConfig((SwitchConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;


	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;

}


SB_ERROR SwitchControlTick(void)
{

	SwitchFSMTick();


	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void SwitchFSMInit(void)
{
	/* Init FSM context */
	SwitchContext_Init(&switchFsm._fsm, &switchFsm);

	/* Enable state machine debug */
	setDebugFlag(&switchFsm._fsm, 1);

	/* Set default configuration parameters for switch channels */

	SwitchConfigurationPtr = &SwitchConfiguration[SW_OPEN_CHAN];
	SwitchConfigurationPtr->SwitchEnabled  	= SWITCH_DISABLED;
	SwitchConfigurationPtr->inhibitBI  		= INHIBIT_BI;      /* Bit mask of channel numbers */
	SwitchConfigurationPtr->polarity  		= POLARITY;
	SwitchConfigurationPtr->pulseLengthMS  	= PULSELENGTH_MS;
	SwitchConfigurationPtr->overrunMS		= OVERRUN_MS;
	SwitchConfigurationPtr->opTimeoutS		= OPERATION_TIMEOUT_S;

	SwitchConfigurationPtr = &SwitchConfiguration[SW_CLOSE_CHAN];
	SwitchConfigurationPtr->SwitchEnabled  	= SWITCH_DISABLED;
	SwitchConfigurationPtr->inhibitBI  		= INHIBIT_BI;      /* Bit mask of channel numbers */
	SwitchConfigurationPtr->polarity  		= POLARITY;
	SwitchConfigurationPtr->pulseLengthMS  	= PULSELENGTH_MS;
	SwitchConfigurationPtr->overrunMS		= OVERRUN_MS;
	SwitchConfigurationPtr->opTimeoutS		= OPERATION_TIMEOUT_S;



	/* Initialise the active channel */
	switchFsm.switchStatus.activeChannel	= SW_CON_CHAN_LAST;

}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SwitchSelectHandler(SwitchSelectStr *switchSelect)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 		= switchSelect->channel;
	switchFsm.swEventParams.local 			= switchSelect->local;
	switchFsm.swEventParams.timeout 		= switchSelect->SelectTimeout;
	switchFsm.swEventParams.receiveTime		= STGetTime();

	reply.channel 			= switchSelect->channel;
	reply.status  			= REPLY_STATUS_OKAY;

	switch(switchSelect->channel)
	{
	case SCM_CH_SWOUT_OPEN_SWITCH:
		/* Call function to send open select event to state machine */
		if( switchFsm.swEventParams.local )
		{
			SwitchContext_SelectLocalEvent(&switchFsm._fsm);
			reply.status = switchFsm.swEventParams.status;
		}
		else
		{
			SwitchContext_SelectRemoteEvent(&switchFsm._fsm);
			reply.status = switchFsm.swEventParams.status;
		}
		break;

	case SCM_CH_SWOUT_CLOSE_SWITCH:
		/* Call function to send close select event to state machine */
		if( switchFsm.swEventParams.local )
		{
			SwitchContext_SelectLocalEvent(&switchFsm._fsm);
			reply.status = switchFsm.swEventParams.status;
		}
		else
		{
			SwitchContext_SelectRemoteEvent(&switchFsm._fsm);
			reply.status = switchFsm.swEventParams.status;
		}
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SwitchOperateHandler(SwitchOperateStr *SwitchOperate)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 		= SwitchOperate->channel;
	switchFsm.swEventParams.local 			= SwitchOperate->local;
	switchFsm.swEventParams.timeout 		= SwitchOperate->SwitchDelay;
	switchFsm.swEventParams.preOpTime 		= SwitchOperate->SwitchPreOpDelay;
	switchFsm.swEventParams.receiveTime		= STGetTime();

	reply.channel 	= SwitchOperate->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(SwitchOperate->channel)
	{
	case SCM_CH_SWOUT_OPEN_SWITCH:
		/* Call function to send open operate event to state machine */
		SwitchContext_OperateEvent(&switchFsm._fsm);
		reply.status = switchFsm.swEventParams.status;
		break;

	case SCM_CH_SWOUT_CLOSE_SWITCH:
		/* Call function to send open operate event to state machine */
		SwitchContext_OperateEvent(&switchFsm._fsm);
		reply.status  = switchFsm.swEventParams.status;
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SwitchCancelHandler(SwitchCancelStr *switchCancel)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 		= switchCancel->channel;
	switchFsm.swEventParams.local 			= switchCancel->local;
	switchFsm.swEventParams.timeout 		= 0;
	switchFsm.swEventParams.receiveTime		= STGetTime();

	reply.channel 			= switchCancel->channel;
	reply.status  			= REPLY_STATUS_OKAY;

	/*
	 * Get channel from message
	 */
	reply.channel 	= switchCancel->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(switchCancel->channel)
	{
	case SCM_CH_SWOUT_OPEN_SWITCH:
		/* Call function to send open cancel event to state machine */
		if( switchCancel->local )
			SwitchContext_CancelLocalEvent(&switchFsm._fsm);
		else
			SwitchContext_CancelRemoteEvent(&switchFsm._fsm);
		reply.status  	= switchFsm.swEventParams.status;
		break;

	case SCM_CH_SWOUT_CLOSE_SWITCH:
		/* Call function to send close cancel event to state machine */
		if( switchCancel->local )
			SwitchContext_CancelLocalEvent(&switchFsm._fsm);
		else
			SwitchContext_CancelRemoteEvent(&switchFsm._fsm);
		reply.status  	= switchFsm.swEventParams.status;
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_CANCEL_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}


SB_ERROR SwitchSynchHandler(SwitchSynchStr *switchSynch)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 		= switchSynch->channel;
	switchFsm.swEventParams.local 			= 0;
	switchFsm.swEventParams.timeout 		= 0;
	switchFsm.swEventParams.receiveTime		= STGetTime();

	reply.channel 	= switchSynch->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_SYNCH_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SwitchConfig(SwitchConfigStr *switchConfigPtr)
{
	ModReplyStr reply;

	reply.channel 	= switchConfigPtr->channel;

	/* Set the pointer to the channel configuration structure from the channel number */
	SwitchConfigurationPtr = &SwitchConfiguration[switchConfigPtr->channel];
	/* Fill the channel configuration structure from the message body */
	SwitchConfigurationPtr->SwitchEnabled  	= switchConfigPtr->SwitchEnabled;
	SwitchConfigurationPtr->inhibitBI  		= switchConfigPtr->inhibitBI;
	SwitchConfigurationPtr->polarity  		= switchConfigPtr->polarity;
	SwitchConfigurationPtr->pulseLengthMS  	= switchConfigPtr->pulseLengthMS;
	SwitchConfigurationPtr->overrunMS  		= switchConfigPtr->overrunMS;
	SwitchConfigurationPtr->opTimeoutS  	= switchConfigPtr->opTimeoutS;

	reply.status  	= REPLY_STATUS_OKAY;


	/* Generate event for state machine */
	SwitchContext_ConfigEvent(&switchFsm._fsm);


	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_SWITCH_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}



/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
