/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch FSM header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SWITCHFSM_INCLUDED
#define _SWITCHFSM_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "debug_frmwrk.h"

/* Define tracing to use printf to serial port */
#define TRACE		_printf

#include "lu_types.h"
#include "Switch_sm.h"
#include "SwitchCommon.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DELAYED_ACTION_PULSE_MS		250  /* Flash rate for delayed action */
#define MAX_IP_CHANNELS				8

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


struct  Switch
{
	SwEventParamsStr 		swEventParams;
	SwitchUserContextStr 	switchStatus;
	struct SwitchContext 	_fsm;
};

struct SwConBinInputs
{
	/* Relay FB Inputs */
	lu_uint32_t ioIdRelayFbOpenUpClosed;
	lu_uint32_t ioIdRelayFbOpenDnClosed;
	lu_uint32_t ioIdRelayFbOpenLkClosed;
	lu_uint32_t ioIdRelayFbCloseUpClosed;
	lu_uint32_t ioIdRelayFbCloseDnClosed;
	lu_uint32_t ioIdRelayFbCloseLkClosed;
	lu_uint32_t ioIdRelayFbFpiexUpClosed;
	lu_uint32_t ioIdRelayFbFpiexDnClosed;
	lu_uint32_t ioIdRelayFbFpiexLkClosed;
	lu_uint32_t ioIdRelayFbAuxUpClosed;
	lu_uint32_t ioIdRelayFbAuxDnClosed;
	lu_uint32_t ioIdRelayFbAuxLkClosed;
	lu_uint32_t ioIdVMotorFbOn;
	/* Binary Inputs */
	lu_uint32_t ioIdExternalFPIInput;
	lu_uint32_t ioIdInsulationLowInput;
	lu_uint32_t ioIdSwitchOpenInput;
	lu_uint32_t ioIdSwitchClosedInput;
	lu_uint32_t ioIdActuatorDisabledInput;
	lu_uint32_t ioIdSpare1_Input;
	lu_uint32_t ioIdSpare2_Input;
	lu_uint32_t ioIdSwitchEarthedInput;
};


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/* Guard functions */
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectLocal(struct SwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectRemote(struct SwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperateLocal(struct SwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperateRemote(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t CancelLocal(struct SwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t CancelRemote(struct SwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectTimeoutLocal(struct SwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectTimeoutRemote(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t DelayTimeout(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t PreOpDelayTimeout(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperationTimeout(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OverrunTimeout(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperationComplete(struct SwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t ErrorCode(struct SwitchContext *fsm);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t  CheckRelayError(struct SwitchContext *fsm);




/********************************************************************************************
 *                              Action functions
 ********************************************************************************************/
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_OperationCommittedAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_OperationUnCommittedAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_ResetAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_InitAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_LocalSelectedAction(struct Switch *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_RemoteSelectedAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_SelectTimedOutAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void Switch_OperationTimeoutAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_RemoteCancelledAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_LocalCancelledAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_PowerMotorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_StartMotorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_DelayedOperationAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_OperationCompleteAction(struct Switch *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void Switch_OperationOverrunAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_SelectErrorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_OperateErrorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_CancelErrorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void Switch_OperatingFeedbackAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_ErrorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_RelayErrorAction(struct Switch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_ConfigErrorAction(struct Switch *fsm);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void Switch_CompleteErrorAction(struct Switch *fsm);


#endif /* _SWITCHFSM_INCLUDED */

/*
 *********************** End of file ******************************************
 */
