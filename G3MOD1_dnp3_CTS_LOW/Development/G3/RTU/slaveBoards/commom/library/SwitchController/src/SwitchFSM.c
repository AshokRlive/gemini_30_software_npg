/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch FSM module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


#include "SwitchFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSwitchControllerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern SwitchConfigStr SwitchConfiguration[], *SwitchConfigurationPtr;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_uint32_t	overrunStartTime = 0;
lu_bool_t 	operationCommitted = LU_FALSE;
lu_uint16_t errorCode = 0;
lu_bool_t 	vMotorError = LU_FALSE;
lu_bool_t   relayError = LU_FALSE;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
void 		MotorSupplyOn(struct Switch *fsm);
void 		MotorSupplyOff(struct Switch *fsm);
lu_bool_t 	GetRelayFeedback(struct SwitchContext *fsm, struct SwConBinInputs *binInp);
SB_ERROR 	ReadBinaryInputs(struct SwitchContext *fsm, struct SwConBinInputs *binInp);


/* SelectLocal & SelectRemote are almost identical - a common function is required here!!!
 * (Same is also true for operate / cancel / select timeout )
 * */

/*
 * Guard functions - called on event to check transition to new state is valid.
 */

lu_bool_t SelectLocal(struct SwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   localSelect;
	lu_uint8_t  binInhibit;
	lu_uint8_t  channelNo;
	lu_uint32_t ioID;
	lu_int32_t  binIP;

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &SwitchConfiguration[fsm->_owner->swEventParams.channel];

	/* Check for operation already in progress */
	if( fsm->_owner->switchStatus.activeChannel < SW_CON_CHAN_LAST)
	{
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = LU_FALSE;
		}
		/* Return false  to indicate a operation in progress */
		return LU_FALSE;
	}

	/*
	 * Check binary inhibit
	 */

	/* Get binary inhibit bit mask of channel numbers */
	binInhibit = SwitchConfigurationPtr->inhibitBI;
	/* Check maximum number of channels */
	for( channelNo=0 ; channelNo<MAX_IP_CHANNELS ; channelNo++ )
	{
		/* Check for bit set in bit mask */
		if( binInhibit & (1<<channelNo) )
		{
			/* Get Iod from channel number */
			ioID = fsm->_owner->switchStatus.swConInitParams.ChannelIOD[channelNo];
			/* Get value of binary input */
			retError = IOManagerGetValue(ioID, &binIP);

			/* Need to check retError is ok for getValue ??????? & if error return a different error code */
			/* Generate error code if set */
			if( binIP)
			{
				fsm->_owner->swEventParams.status = REPLY_STATUS_INHIBIT_ERROR;
				if( operationCommitted == LU_TRUE)
				{
					errorCode = REPLY_STATUS_INHIBIT_ERROR;
				}
				/* Return false  to indicate a operation in progress */
				return LU_FALSE;
			}
		}
	}


	/* Get select message local/remote */
	localSelect = fsm->_owner->swEventParams.local;

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	/* Return true if local otherwise return an error  */
	if( ( locRem == IO_STAT_LOR_LOCAL ) && localSelect )
	{
		/* Set active channel */
		fsm->_owner->switchStatus.activeChannel = fsm->_owner->swEventParams.channel;

		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_OKAY;
		}
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		/* Return false  to indicate a local/remote error */
		return LU_FALSE;
	}
}

lu_bool_t SelectRemote(struct SwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   remoteSelect;
	lu_uint8_t  binInhibit;
	lu_uint8_t  channelNo;
	lu_uint32_t ioID;
	lu_int32_t  binIP;


	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &SwitchConfiguration[fsm->_owner->swEventParams.channel];

	/* Check for operation already in progress */
	if( fsm->_owner->switchStatus.activeChannel < SW_CON_CHAN_LAST)
	{
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_SELECT_ERROR;
		}
		/* Return false  to indicate a operation in progress */
		return LU_FALSE;
	}

	/* Get binary inhibit bit mask of channel numbers */
	binInhibit = SwitchConfigurationPtr->inhibitBI;
	/* Check maximum number of channels */
	for( channelNo=0 ; channelNo<MAX_IP_CHANNELS ; channelNo++ )
	{
		/* Check for bit set in bit mask */
		if( binInhibit & (1<<channelNo) )
		{
			/* Get Iod from channel number */
			ioID = fsm->_owner->switchStatus.swConInitParams.ChannelIOD[channelNo];
			/* Get value of binary input */
			retError = IOManagerGetValue(ioID, &binIP);
			/* Generate error code if set */
			if( binIP)
			{
				fsm->_owner->swEventParams.status = REPLY_STATUS_INHIBIT_ERROR;
				if( operationCommitted == LU_TRUE)
				{
					errorCode = REPLY_STATUS_INHIBIT_ERROR;
				}
				/* Return false  to indicate a operation in progress */
				return LU_FALSE;
			}
		}
	}

	/* Get select message local/remote */
	remoteSelect = !(fsm->_owner->swEventParams.local);

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);


	if( (locRem == IO_STAT_LOR_REMOTE) && remoteSelect )
	{
		/* Set active channel */
		fsm->_owner->switchStatus.activeChannel = fsm->_owner->swEventParams.channel;

		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		/* Return false  to indicate a local/remote error */
		return LU_FALSE;
	}
}

lu_bool_t OperateLocal(struct SwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   localOperate;

	/*
	 * Check if the the switch operate local is valid
	 */

	/* Get operate message local/remote */
	localOperate = fsm->_owner->swEventParams.local;

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if( (locRem == IO_STAT_LOR_LOCAL) && localOperate )
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		return LU_TRUE;
	}
	else
	/* Return true switch operate local message has been received */
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_FALSE;
	}
}

lu_bool_t OperateRemote(struct SwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   remoteOperate;

	/*
	 * Check if the the switch operate remote is valid
	 */

	/* Get operate message local/remote */
	remoteOperate = !(fsm->_owner->swEventParams.local);

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if( (locRem == IO_STAT_LOR_REMOTE) && remoteOperate )
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		return LU_TRUE;
	}
	else
	/* Return true switch operate message has been received */
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_FALSE;
	}
}

lu_bool_t CancelLocal(struct SwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   localCancel;


	/*
	 * Check if the the switch cancel local is valid
	 */

	/* Get operate message local/remote */
	localCancel = fsm->_owner->swEventParams.local;

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if( (locRem == IO_STAT_LOR_LOCAL) && localCancel )
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return true - switch cancel local message has been received */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_TRUE;
	}
}

lu_bool_t CancelRemote(struct SwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   remoteCancel;

	/*
	 * Check if the the switch cancel remote is valid
	 */

	/* Get operate message local/remote */
	remoteCancel = !(fsm->_owner->swEventParams.local);

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if( (locRem == IO_STAT_LOR_REMOTE) && remoteCancel )
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return true switch - cancel message has been received */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_TRUE;
	}
}

lu_bool_t SelectTimeoutLocal(struct SwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t selectTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();


	/* Get select start time */
	startTime 		= fsm->_owner->swEventParams.receiveTime;
	selectTimeout 	= fsm->_owner->swEventParams.timeout;

	elapsedTime = STElapsedTime( startTime , time );

	if( elapsedTime > (selectTimeout*1000) )
	{
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_SELECT_ERROR;
		}
		/* Return true  to indicate a select timeout error */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return false to indicate timeout not exceeded */
		return LU_FALSE;
	}

}

lu_bool_t SelectTimeoutRemote(struct SwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t selectTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();

	/* Get select start time */
	startTime 		= fsm->_owner->swEventParams.receiveTime;
	selectTimeout 	= fsm->_owner->swEventParams.timeout;

	elapsedTime = STElapsedTime( startTime , time );

	if( elapsedTime > selectTimeout*1000 )
	{
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_SELECT_ERROR;
		}
		/* Return true to indicate a select timeout error */
		return LU_TRUE;

	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return false to indicate timeout not exceeded */
		return LU_FALSE;
	}

}

lu_bool_t DelayTimeout(struct SwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t delayTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();
	lu_uint32_t ioId;
	SB_ERROR    RetVal;

	/* Get select start time */
	startTime 		= fsm->_owner->swEventParams.receiveTime;
	delayTimeout 	= fsm->_owner->swEventParams.timeout;

	elapsedTime = STElapsedTime( startTime , time );

	/*
	 * If there is no delayed operation then go straight to pre-operating state.
	 * Otherwise wait for delayed operation to timeout.
	 */
	if((delayTimeout == 0) || ( elapsedTime > (delayTimeout*1000) ) )
	{
		/* Stop LED flashing */
		ioId = fsm->_owner->switchStatus.swConInitParams.ioIdLedFPCmdOpen;
		RetVal = IOManagerSetValue(ioId, 0);
		ioId = fsm->_owner->switchStatus.swConInitParams.ioIdLedFPCmdClose;
		RetVal = IOManagerSetValue(ioId, 0);

		/*
		 * Set the time at which the operation started.
		 * This will be used for the operation timeout.
		 */
		time = STGetTime();
		fsm->_owner->swEventParams.receiveTime = time;

		/* Return TRUE  to indicate no delay or delay finished */
		return LU_TRUE;
	}
	else
	{
		/* Return true to indicate delay  not yet completed */
		return LU_FALSE;
	}
}

lu_bool_t PreOpDelayTimeout(struct SwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t delayTimeout;
	lu_uint32_t preOpTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();

	/* Get select start time */
	/* Note that receiveTime is NOT the Operate received time stamp at this point */
	startTime 		= fsm->_owner->swEventParams.receiveTime; /* modified by DelayTimeout() */
	delayTimeout 	= fsm->_owner->swEventParams.timeout;
	preOpTimeout 	= fsm->_owner->swEventParams.preOpTime;
	elapsedTime 	= STElapsedTime( startTime , time );

	/*
	 * If there is no pre-op delay then go straight to operating state.
	 * Otherwise wait for delayed operation to timeout.
	 */
	if(  (preOpTimeout == 0) || (elapsedTime > preOpTimeout) )
	{
		/* Return TRUE  to indicate no delay or delay finished */
		return LU_TRUE;
	}
	else
	{
		/* Return true to indicate delay  not yet completed */
		return LU_FALSE;
	}
}


lu_bool_t OperationTimeout(struct SwitchContext *fsm)
{
    lu_uint32_t opStartTime;
    lu_uint32_t operationTimeout;
    lu_uint32_t elapsedTime;
    lu_uint32_t time = STGetTime();

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &SwitchConfiguration[fsm->_owner->switchStatus.activeChannel];

	/* Get operate start time */
	opStartTime 		= fsm->_owner->swEventParams.receiveTime;
	operationTimeout 	= (lu_int32_t)SwitchConfigurationPtr->opTimeoutS;

	elapsedTime = STElapsedTime( opStartTime , time );

	if( elapsedTime > (operationTimeout*1000) )
	{
		fsm->_owner->swEventParams.status = REPLY_STATUS_OPERATE_ERROR;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_OPERATE_ERROR;
		}
		/* Return false  to indicate a select timeout error */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return true to indicate timeout not exceeded */
		return LU_FALSE;
	}

}


lu_bool_t OverrunTimeout(struct SwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t delayTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();
	SB_ERROR    RetVal;

	LU_UNUSED(fsm);

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &SwitchConfiguration[fsm->_owner->switchStatus.activeChannel];

	/* Get select start time */
	startTime 		= overrunStartTime;
	delayTimeout 	= (lu_uint32_t)SwitchConfigurationPtr->overrunMS;

	elapsedTime = STElapsedTime( startTime , time );

	if(delayTimeout == 0)
	{
		/* Return TRUE  to indicate no delay */
		RetVal = LU_TRUE;
	}
	else if( elapsedTime > (delayTimeout) )
	{
		/* Return TRUE  to indicate the delay has completed */
		RetVal = LU_TRUE;
	}
	else
	{
		/* Return true to indicate delay  not yet completed */
		RetVal = LU_FALSE;
	}

	return RetVal;
}

extern lu_bool_t ErrorCode(struct SwitchContext *fsm)
{
	LU_UNUSED(fsm);

	/* Return true if there has been an error during the operation */
	if( errorCode != REPLY_STATUS_OKAY )
	{
		return LU_TRUE;
	}
	return LU_FALSE;
}

extern lu_bool_t  OperationComplete(struct SwitchContext *fsm)
{
	SB_ERROR    RetVal;
	lu_int32_t  open = 0;
	lu_int32_t  close = 0;
	lu_uint32_t ioId;
	lu_uint8_t  channel;

	/* Is the operation in progress an open or close */
	channel = fsm->_owner->switchStatus.activeChannel;

	/* Get state of inputs i.e. open or closed */
	ioId = fsm->_owner->switchStatus.swConInitParams.ioIdSwitchOpenInput;
	RetVal = IOManagerGetValue(ioId, &open);
	ioId = fsm->_owner->switchStatus.swConInitParams.ioIdSwitchClosedInput;
	RetVal = IOManagerGetValue(ioId, &close);

	/* check state of switch against current operation */
	if( (channel == SW_CON_CHAN_CLOSE ) && close )
	{
		/* Return true to indicate close operation is complete */
		return LU_TRUE;
	}
	else if( (channel == SW_CON_CHAN_OPEN ) && open  )
	{
		/* Generate event to indicate open operation is complete */
		return LU_TRUE;
	}

	/* Operation not completed so return false */
	return LU_FALSE;

}

extern lu_bool_t  CheckRelayError(struct SwitchContext *fsm)
{
	struct SwConBinInputs binInp;

	/* If any relay is not in its correct initial position then indicate an error */
	if( GetRelayFeedback(fsm,&binInp) )
	{
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_RELAY_ERROR;
		}
		return LU_TRUE;
	}

	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_OKAY;
	}

	return LU_FALSE;

}

/******************************************************************************
 * Action functions - called on entry to a state.
 ******************************************************************************/

void Switch_OperationCommittedAction(struct Switch *fsm)
{
	LU_UNUSED( fsm );
	/*
	 * Set flag to indicate the operation is committed and
	 * can't be interrupted. An error code will be set instead
	 * for later reporting
	 */
	operationCommitted = LU_TRUE;
}

void Switch_OperationUnCommittedAction(struct Switch *fsm)
{
	LU_UNUSED( fsm );
	/*
	 * Set flag to indicate the operation is no longer committed
	 */
	operationCommitted = LU_FALSE;

}

void Switch_InitAction(struct Switch *fsm)
{

	/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
	MotorSupplyOff(fsm);

	/* Set active channel to indicate no operation active */
	fsm->switchStatus.activeChannel = SW_CON_CHAN_LAST;

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_OKAY;

	errorCode = REPLY_STATUS_OKAY;
	operationCommitted = LU_FALSE;

}

void Switch_LocalSelectedAction(struct Switch *fsm)
{
	/* Set channel activated */
	fsm->switchStatus.activeChannel = fsm->swEventParams.channel;
}

void Switch_RemoteSelectedAction(struct Switch *fsm)
{
	/* Set channel activated */
	fsm->switchStatus.activeChannel = fsm->swEventParams.channel;
}

void Switch_SelectTimedOutAction(struct Switch *fsm)
{
	/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
	MotorSupplyOff(fsm);

	/* Generate event to MCM */
	SysAlarmSetLogEvent(LU_TRUE,
						SYS_ALARM_SEVERITY_WARNING,
						SYS_ALARM_SUBSYSTEM_SWITCH_CONTROLLER,
						SYSALC_SWITCH_CONTROLLER_SELECT_TIMEOUT_ERR,
						0
					   );

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_TIMEOUT_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_TIMEOUT_ERROR;
	}
}

void Switch_RemoteCancelledAction(struct Switch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CANCEL_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_CANCEL_ERROR;
	}
}

void Switch_LocalCancelledAction(struct Switch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CANCEL_ERROR;

	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_CANCEL_ERROR;
	}
}

void Switch_ResetAction(struct Switch *fsm)
{
	/* The operation is finished so reset the committed flag */
	operationCommitted = LU_FALSE;

	/* Set active channel to indicate no operation active */
	fsm->switchStatus.activeChannel = SW_CON_CHAN_LAST;

	/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
	MotorSupplyOff(fsm);

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_OKAY;
	errorCode = REPLY_STATUS_OKAY;
}

void Switch_PowerMotorAction(struct Switch *fsm)
{
	/* Switch on 24V Motor supply and turn on 24V Motor Supply LED */
	MotorSupplyOn(fsm);
}

void Switch_StartMotorAction(struct Switch *fsm)
{
	SB_ERROR    RetVal;
	lu_uint16_t pulseLength;
	lu_uint32_t ioId;

	/* Default relay error to true. Will be set to false when the feedback goes true */
// refs # 2660 disable relay error feedback
//	relayError = LU_TRUE;

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &SwitchConfiguration[fsm->switchStatus.activeChannel];

	/*
	 * Turn Motor on to open or close the switch depending on the channel
	 * Turn on for length of pulse specified in Config command
	 */
	pulseLength = SwitchConfigurationPtr->pulseLengthMS;

	if( fsm->switchStatus.activeChannel == SW_CON_CHAN_OPEN )
	{
		/* Determine which relay to pulse from the polarity specified in the config */
		switch( SwitchConfigurationPtr->polarity)
		{
			case SWITCH_POLARITY_UP:
				ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_OPEN].ioIdRelayUp;
				break;
			case SWITCH_POLARITY_DOWN:
				ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_OPEN].ioIdRelayDown;
				break;
			case SWITCH_POLARITY_LINK:
				ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_OPEN].ioIdRelayLink;
				break;
			default:
				if( operationCommitted == LU_TRUE)
				{
					errorCode = REPLY_STATUS_PARAM_ERROR;
				}
		}
		/* Close specified relay for the specified pulse length */
		RetVal = IOManagerSetPulse(ioId, pulseLength);

		/* Switch on LED */
		ioId = fsm->switchStatus.swConInitParams.ioIdLedFPCmdOpen;
		RetVal = IOManagerSetValue(ioId, 1);

	}
	else if( fsm->switchStatus.activeChannel == SW_CON_CHAN_CLOSE )
	{
		/* Determine which relay to pulse from the polarity specified in the config */
		switch( SwitchConfigurationPtr->polarity)
		{
			case SWITCH_POLARITY_UP:
				ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_CLOSE].ioIdRelayUp;
				break;
			case SWITCH_POLARITY_DOWN:
				ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_CLOSE].ioIdRelayDown;
				break;
			case SWITCH_POLARITY_LINK:
				ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_CLOSE].ioIdRelayLink;
				break;
			default:
				if( operationCommitted == LU_TRUE)
				{
					errorCode = REPLY_STATUS_PARAM_ERROR;
				}
		}
		/* Close specified relay for the specified pulse length */
		RetVal = IOManagerSetPulse(ioId, pulseLength);

		/* Switch on LED */
		ioId = fsm->switchStatus.swConInitParams.ioIdLedFPCmdClose;
		RetVal = IOManagerSetValue(ioId, 1);

	}
}

extern void Switch_ErrorAction(struct Switch *fsm)
{
	/*
	 * Set CAN error status in reply to the error detected during operation
	 * to indicate to MCM what went wrong
	 */
	fsm->swEventParams.status = errorCode;
	/* Reset the error code so that on the next cancel command the okay reply is returned */
	errorCode = REPLY_STATUS_OKAY;
	/* The operation is finished so reset the committed flag */
	operationCommitted = LU_FALSE;
}

void Switch_DelayedOperationAction(struct Switch *fsm)
{
	SB_ERROR    RetVal;
	lu_uint32_t ioId;
	lu_uint8_t  channel;
	lu_uint32_t delayTimeout;

	channel = fsm->switchStatus.activeChannel;
	delayTimeout 	= fsm->swEventParams.timeout;

	/*
	 * If delayed operation is required i.e. delayTimeout > 0
	 * then set the LED flashing.
	 * Otherwise do nothing.
	 */
	if( delayTimeout > 0 )
	{
		/* Flash front panel LED to indicate delayed action */
		if( channel == SW_CON_CHAN_OPEN )
		{
			ioId = fsm->switchStatus.swConInitParams.ioIdLedFPCmdOpen;
			RetVal = IOManagerSetFlash(ioId, DELAYED_ACTION_PULSE_MS);
		}
		else if(channel == SW_CON_CHAN_CLOSE )
		{
			ioId = fsm->switchStatus.swConInitParams.ioIdLedFPCmdClose;
			RetVal = IOManagerSetFlash(ioId, DELAYED_ACTION_PULSE_MS);
		}
	}
}

void Switch_RelayErrorAction(struct Switch *fsm)
{
	LU_UNUSED(fsm);

	if( (vMotorError == LU_TRUE) && (operationCommitted == LU_TRUE))
	{
		/* Set CAN error status in reply */
		fsm->swEventParams.status = REPLY_STATUS_VMOTOR_ERROR;
		errorCode = REPLY_STATUS_VMOTOR_ERROR;
	}
	else if( (relayError == LU_TRUE) && (operationCommitted == LU_TRUE)  )
	{
		/* Set CAN error status in reply */
		fsm->swEventParams.status = REPLY_STATUS_RELAY_ERROR;
		errorCode = REPLY_STATUS_RELAY_ERROR;
	}
}

void Switch_OperationCompleteAction(struct Switch *fsm)
{
	/* Turn off motor power supply */
	MotorSupplyOff(fsm);
}

void Switch_OperationOverrunAction(struct Switch *fsm)
{
	LU_UNUSED(fsm);
	overrunStartTime = STGetTime();
}

void Switch_OperationTimeoutAction(struct Switch *fsm)
{
	LU_UNUSED(fsm);

	// DO NOT SWITCH OFF MOTOR SUPPLY!!

	fsm->swEventParams.status = REPLY_STATUS_OPERATE_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_OPERATE_ERROR;
	}
}

void Switch_SelectErrorAction(struct Switch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_SELECT_ERROR;
	}
}

void Switch_OperateErrorAction(struct Switch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_OPERATE_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_OPERATE_ERROR;
	}
}

void Switch_CancelErrorAction(struct Switch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CANCEL_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_CANCEL_ERROR;
	}
}

void Switch_OperatingFeedbackAction(struct Switch *fsm)
{
	SB_ERROR    retError;
	lu_uint32_t ioId;
	lu_int32_t  vMotorOnFb;
	lu_int32_t  relayClosedFb;

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &SwitchConfiguration[fsm->switchStatus.activeChannel];

	/*
	 * Check that V Motor is closed and check that only the relay
	 * specified by the operation is closed. All others must be open.
	 */

	/* Get the V Motor ioID- */
	ioId = fsm->switchStatus.swConInitParams.ioIdVMotorFbOn;
	retError = IOManagerGetValue(ioId , &vMotorOnFb);
// refs # 2660 disable vmotor relay error feedback
//	if( (vMotorOnFb == LU_TRUE) && (vMotorError == LU_TRUE) )
//	{
//		vMotorError = LU_FALSE;
//	}
//	else if( (vMotorOnFb == LU_FALSE) && (vMotorError == FALSE) )
//	{
//		vMotorError = LU_TRUE;
//	}

	/* Get the ioId of the relay specified by the operation */
	if( fsm->switchStatus.activeChannel == SW_CON_CHAN_OPEN )
	{
		/* Determine which relay to check from the polarity specified in the config */
		switch( SwitchConfigurationPtr->polarity)
		{
			case SWITCH_POLARITY_UP:
				ioId = fsm->switchStatus.swConInitParams.ioIdRelayFbOpenUpClosed;
				break;
			case SWITCH_POLARITY_DOWN:
				ioId = fsm->switchStatus.swConInitParams.ioIdRelayFbOpenDnClosed;
				break;
			case SWITCH_POLARITY_LINK:
				ioId = fsm->switchStatus.swConInitParams.ioIdRelayFbOpenLkClosed;
				break;
			default:
				if( operationCommitted == LU_TRUE)
				{
					errorCode = REPLY_STATUS_PARAM_ERROR;
				}
		}
	}
	else if( fsm->switchStatus.activeChannel == SW_CON_CHAN_CLOSE )
	{
		/* Determine which relay to check from the polarity specified in the config */
		switch( SwitchConfigurationPtr->polarity)
		{
			case SWITCH_POLARITY_UP:
				ioId = fsm->switchStatus.swConInitParams.ioIdRelayFbCloseUpClosed;
				break;
			case SWITCH_POLARITY_DOWN:
				ioId = fsm->switchStatus.swConInitParams.ioIdRelayFbCloseDnClosed;
				break;
			case SWITCH_POLARITY_LINK:
				ioId = fsm->switchStatus.swConInitParams.ioIdRelayFbCloseLkClosed;
				break;
			default:
				if( operationCommitted == LU_TRUE)
				{
					errorCode = REPLY_STATUS_PARAM_ERROR;
				}
		}
	}
	retError = IOManagerGetValue(ioId, &relayClosedFb);
	if( (relayClosedFb == LU_TRUE) && (relayError == LU_TRUE) )
	{
		relayError = LU_FALSE;
	}
}

void Switch_ConfigErrorAction(struct Switch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CONFIG_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_CONFIG_ERROR;
	}
}

void Switch_CompleteErrorAction(struct Switch *fsm)
{
	/* Turn off motor power supply */
	MotorSupplyOff(fsm);

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_COMPLETE_ERROR;
	if( operationCommitted == LU_TRUE)
	{
		errorCode = REPLY_STATUS_COMPLETE_ERROR;
	}
}

/*
 * Local functions
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void MotorSupplyOn(struct Switch *fsm)
{
	SB_ERROR    RetVal;
	lu_uint32_t ioId;

	/* Switch on 24V Motor supply and turn on 24V Motor Supply LED */
	ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotorA;
	RetVal = IOManagerSetValue(ioId, 1);
	ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotorB;
	RetVal = IOManagerSetValue(ioId, 1);
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFPMotorSupply;
	RetVal = IOManagerSetValue(ioId, 1);

	/*
	 * Default V motor error to true. Will be set to false when the feedback goes true.
	 * Will be set back to true if the feedback goes low before the end of the operation.
	 */
// refs # 2660 disable vmotor relay error feedback
//	vMotorError = LU_TRUE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void MotorSupplyOff(struct Switch *fsm)
{
	SB_ERROR    RetVal;
	lu_uint32_t ioId;

	/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
	ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotorA;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotorB;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFPMotorSupply;
	RetVal = IOManagerSetValue(ioId, 0);

	/* Open All relays */
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_OPEN].ioIdRelayUp;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_OPEN].ioIdRelayDown;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_OPEN].ioIdRelayLink;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_CLOSE].ioIdRelayUp;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_CLOSE].ioIdRelayDown;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[SW_CON_CHAN_CLOSE].ioIdRelayLink;
	RetVal = IOManagerSetValue(ioId, 0);

	/* Turn off command LEDs */
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFPCmdOpen;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFPCmdClose;
	RetVal = IOManagerSetValue(ioId, 0);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t GetRelayFeedback(struct SwitchContext *fsm, struct SwConBinInputs *binInp)
{
	SB_ERROR  retError;
	lu_bool_t retVal = LU_FALSE;

	/* Take snapshot of all binary inputs */
	retError = ReadBinaryInputs(fsm,binInp);
	if(retError != SB_ERROR_NONE)
	{
		/* Error detected in reading binary inputs */
		retVal = LU_TRUE;
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_IOMANAGER_ERROR;
		}
	}

	if( binInp->ioIdRelayFbOpenUpClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbOpenDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbOpenDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbCloseUpClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbCloseDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbCloseLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbFpiexUpClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbFpiexDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbFpiexLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbAuxUpClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbAuxDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFbAuxLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( retVal == LU_TRUE )
	{
		if( operationCommitted == LU_TRUE)
		{
			errorCode = REPLY_STATUS_RELAY_ERROR;
		}
	}

	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR ReadBinaryInputs(struct SwitchContext *fsm, struct SwConBinInputs *binInp)
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint32_t ioID;
	lu_int32_t binIP;

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbOpenUpClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbOpenUpClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbOpenDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbOpenDnClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbOpenLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbOpenLkClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbCloseUpClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbCloseUpClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbCloseDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbCloseDnClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbCloseLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbCloseLkClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbFpiexUpClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbFpiexUpClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbFpiexDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbFpiexDnClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbFpiexLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbFpiexLkClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbAuxUpClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbAuxUpClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbAuxDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbAuxDnClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFbAuxLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFbAuxLkClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdVMotorFbOn;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdVMotorFbOn				= binIP;
	}


	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdExternalFPIInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdExternalFPIInput		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdInsulationLowInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdInsulationLowInput		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdSwitchOpenInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdSwitchOpenInput			= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdSwitchClosedInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdSwitchClosedInput		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdActuatorDisabledInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdActuatorDisabledInput	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdSpare1_Input;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdSpare1_Input			= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdSpare2_Input;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdSpare2_Input			= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdSwitchEarthedInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdSwitchEarthedInput		= binIP;
	}

	return retError;
}

/*
 *********************** End of file ******************************************
 */
