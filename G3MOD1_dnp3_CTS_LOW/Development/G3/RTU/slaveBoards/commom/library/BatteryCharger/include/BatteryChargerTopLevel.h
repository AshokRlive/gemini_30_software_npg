/*
 * BatterChargerTopLevel.h
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */

#ifndef BATTERCHARGERTOPLEVEL_H_
#define BATTERCHARGERTOPLEVEL_H_

#include "lu_types.h"
#include "errorCodes.h"
#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"
#include "NVRam.h"
#include "LinearInterpolation.h"

extern lu_bool_t 			SwEventParamsInProgress;
extern lu_bool_t			batteryTestInProgress;
extern lu_bool_t			chargerTestInProgress;
extern lu_bool_t            updateDataNVRAM;
extern lu_uint32_t          batteryTestScheduleTime;
extern lu_uint16_t          batteryTestPeriodHours;
extern lu_uint32_t          batteryTestDurationTime;



/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define DISCONNECT_PULSE_MS					10
#define CONNECT_PULSE_MS					10

#define MAX_BATT_VOLTAGE_1DU16U16		    (2)

#define MAX_BATT_CURRENT_LIMIT_1DU16U16		(2)

#define BATT_CH_TICK_MS						1000 	/* Schedule every n ms */

#define BATTERY_PRESENT						5000 	/* Level in mV at which battery is assumed disconnected */

#define LV_SUPPLY_THRESHOLD_VOLTAGE			15000	/* Level in mV at which LV supply is off */

#define MSEC								1000

#define SEC									60

#define BATT_DEFAULT_TEMP					28

#define PSU_DEFAULT_TEMP					28

#define RTU_DEFAULT_TEMP					28

#define BATT_MAX_VALID_TEMP					65
#define BATT_MIN_VALID_TEMP					18

#define LEAD_ACID_BATT_TEMP_HYSTERISIS		2
#define NIMH_BATT_TEMP_HYSTERISIS			5
#define UKNOWN_BATT_TEMP_HYSTERISIS			2

#define CHARGER_INIT_DELAY					2000

#define LV_FAILED_THRESHOLD					600	/* Time in seconds over which an LV failure is considered battery draining */
#define BATT_CHARGING_HYSTERISIS_CURRENT	200
#define BATT_CHARGING_HYSTERISIS_TIME		60
#define BATT_MINIMUM_CHARGING_CURRENT		200

#define SECONDS_PER_MINUTE					60
#define SECONDS_PER_HOUR					3600 /* 60 Seconds in a minute, 60 minutes in an hour */

#define MAIN_DC_VOLTS						36
#define MAX_PSU_POWER						(150 * 1000) /* mW ???? */
#define PSU_DERATE_TEMP						45			/* Meaningful comment */


/*We detect no LV by checking the raw DC volts derived from it      - used for AC SUPPLY LED*/
#define NO_LV_DETECT_THRESHOLD 15000
/*Type 1 charger will not be able to reach highest charging voltage - used for AC SUPPLY LED*/
#define LOW_LV_AC_LEVEL 33000

/*Type 1 charger will not work with less than 30v - used for BATT CHARGER LED*/
#define UNHEALTHY_LV_DETECT_THRESHOLD 30000


#define MAX_BATT_TEST_LOW_1DU16U16  6

#define MAX_BATT_TEST_HIGH_1DU16U16  6

#ifdef NEW_STUFF
#define MAX_BATT_TEST_HIGH_1DU16U16  5
#endif

#define FLOAT_VOLTAGE_PERCENTAGE	97	/* Float voltage, as a percentage of full charging voltage */

#define MILLIDEGREES_TO_DEGREES 1000

#define PARAM_NOT_USED(x) x=x

#define FLAG_SET (1u)
#define FLAG_CLR (0u)
#define CTRL_ON  (1u)
#define CTRL_OFF (0u)

#define ONE_HOUR_MS (1000 * 60 * 60)

#define LED_ON (1u)
#define LED_OFF (0u)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	BATTERY_NOT_PRESENT=0,       /*We can't use this. Its voltage is too low*/
	BATTERY_DEEP_DISCHARGE,      /*All we can do is hope that whatever is stopping us charging goes away*/
	BATTERY_LOW,                 /*We can not support motor supply operations but we can power the RTU*/
	BATTERY_WARNING,             /*We can support motor supply operations and power the RTU but we are on the limit*/
	BATTERY_HEALTHY,             /*We can easily support motor operations and the RTU*/
	BATTERY_FULL  	             /*We can support motor operations the RTU and scheduled tests*/
}battHlthStatus;

typedef enum
{
	CHARGER_LV_OFF=0,            /*If the chargers's LV (ie LV derived 36V DC supply is down its not a charger fault but we have no charger*/
	CHARGER_UNHEALTHY,           /*Unloaded charger voltage is too low or too high to use safely*/
	CHARGER_WARNING,             /*Unloaded charger voltage is not as requested but can keep the battery charging*/
	CHARGER_HEALTHY              /*We can charge the battery*/
}chrgHlthStatus;




/* Battery chemistry */
typedef enum
{
	BATT_TYPE_UNKNOWN = 0,
	BATT_TYPE_NI_MH,
	BATT_TYPE_LEAD_ACID
}BATT_TYPE;

typedef enum
{
	NORMAL = 0,
	CONDITIONING
}BATT_CHARGE_STATE;


typedef enum
{
	BATT_CHARGE_METHOD_SENSOR = 0,
	BATT_CHARGE_METHOD_DEFAULT,
	BATT_CHARGE_METHOD_FLOAT
}BATT_CHARGE_METHOD;

typedef enum
{
	BATT_HEALTHY = 0,
	BATT_WARNING,
	BATT_UNHEALTHY
}BATT_CONDITION;

/* Reason for charging stopped */
typedef enum
{
	BATT_CHARGE_STOP_INVALID = 0,
	BATT_CHARGE_STOP_MAX_TIME,
	BATT_CHARGE_STOP_MIN_CURRENT,
	BATT_CHARGE_STOP_UNSTABLE_CURRENT,
	BATT_CHARGE_STOP_DELTA_TEMP,
	BATT_CHARGE_STOP_DELTA_VOLTS,
	BATT_CHARGE_STOP_MAX_CAPACITY,
	BATT_CHARGE_STOP_TOP_UP_TIME,
	BATT_CHARGE_STOP_TEST_RUNNING
}BATT_CHARGE_STOP;

/* Which charger test is in progress */
typedef enum
{
	CH_TEST_INITIAL = 0,

			CH_TEST_INITIAL0,
			CH_TEST_INITIAL1,
			CH_TEST_INITIAL2,
			CH_TEST_INITIAL3,
			CH_TEST_INITIAL4,
			CH_TEST_INITIAL5,
	CH_TEST_CHARGER_FAIL,

	CH_TEST_NONE,
	CH_TEST_VOLTAGE,
	CH_TEST_CURRENT
}CH_TEST;

/* Battery Parameters - from battery NVRAM */
typedef struct BatteryParamsDef
{
	lu_int32_t 	BatteryChargeTrigger;		/* Normal battery charging threshold 						*/
	lu_int32_t 	BatteryThresholdVolts;		/* Lower battery charging threshold for immediate charging	*/
	lu_int32_t 	BatteryChargedVolts;		/* Voltage at which battery can be considered fully charged */
	lu_int32_t 	BatteryChargedCurrent;		/* Current below which battery can be considered fully charged */
	lu_int32_t 	BatteryMaxTemp;		     	/* Maximum temperature at which normal charging is allowed	*/
	lu_uint32_t BatteryOverideTime;			/* Maximum time for which battery charging can be delayed 	*/
	lu_int32_t 	BatteryNomCapacity;			/* Nominal capacity of the battery							*/
	lu_int32_t 	BatteryMaxChargeTime;		/* Maximum charge time of the battery						*/
	lu_uint32_t BatteryTimeBetweenCharge;	/* Minimum  time between charges of the battery			*/
	lu_int32_t 	BatteryChargeRate;			/* Charging current specified for the battery				*/
	lu_int32_t 	BatteryChargeVolts;			/* Charging voltage specified for the battery				*/
	lu_int32_t 	BatteryFullChargeVolts;		/* Charging fully charged voltage specified for the battery	*/
	BATT_TYPE  	BatteryType;				/* Indicates type of battery in use							*/
	lu_int32_t 	BatteryLowLevel;			/* Level below operations cannot be performed				*/
	lu_int32_t 	BatteryLeakage;				/* Leakage current of battery 								*/
	lu_int32_t 	BatteryFloatChargeRate;		/* Charging current when float charging						*/
	lu_int32_t	BatteryCellFloatChargeVolts;	/* Charging voltage when float charging					*/

    lu_uint16_t BatteryTestTimeMinutes;                   /* Battery Test Time Minutes 					*/
    lu_uint16_t BatteryTestCurrentMinimum;                /* Battery Test Current Minimum (mA) 			*/
    lu_uint16_t BatteryTestSuspendTimeMinutes;            /* Battery Test Suspend Time Minutes 			*/
    lu_uint16_t BatteryTestMaximumDurationTimeMinutes;    /* Battery Test Maximum Duration Time Minutes */
    lu_uint8_t 	BatteryTestDischargeCapacity;             /* Battery Test Discharge Capacity 			*/
    lu_int32_t  BatteryTestThresholdVolts;                /* Battery Test Threshold Volts 				*/
    lu_uint8_t 	BatteryTestLoadType;                       /* Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
    lu_uint16_t BatteryPackShutdownLevelVolts;            /* Battery Pack Shutdown Level Volts */
    lu_uint16_t BatteryPackDeepDischargeVolts;            /* Battery Pack Deep Discharge Protection Level Volts */
    lu_int32_t  BatteryMinStartVoltsForTestRqst;
} BatteryParamsStr;

/* Battery State */
typedef struct BatteryStateDef
{
	lu_int32_t BatteryVoltage;				/* Current battery voltage level 							*/
	lu_int32_t BatteryVoltageAverage;		/* Averaged battery voltage level 							*/
	lu_int32_t BatteryChargingVolts;		/* Current voltage output of charger						*/
	lu_int32_t BatteryTemp;					/* Current temperature of the battery						*/
	lu_int32_t BatteryTempAverage;			/* Averaged battery temperature								*/
	lu_int32_t RTUTempAverage;				/* Averaged rtu temperature									*/
	lu_int32_t PSMTemp;						/* Current temperature of the Power Supply Module			*/
	lu_int32_t RTUTemp;						/* Current temperature of the RTU							*/
	lu_int32_t BatteryCharge;				/* Current charge applied to the battery					*/
	lu_int32_t BatteryChargeRate;			/* Charging current specified for the battery				*/
	lu_uint32_t BatteryChargeTime;			/* Charging time specified for the battery					*/
	lu_int32_t BatteryChargeVolts;			/* Charging voltage specified for the battery				*/
	BATT_TYPE  BatteryType;					/* Indicates type of battery in use							*/
	lu_bool_t  BatteryOverride;				/* Indicate if charging override is in operation			*/
	lu_bool_t  BatteryConnected;
	lu_bool_t  BatteryDisConnected;
	lu_int32_t BatteryChgCycleTotalChargeCurrent;	/* Cycle total of battery charge i.e. current * time		*/
	lu_int32_t BatteryTotalChargeCurrent;	/* Running total of battery charge i.e. current * time		*/
	lu_int32_t BatteryTotalDrainCurrent;	/* Running total of battery drain i.e. current * time		*/
	lu_int32_t BatteryTotalCharge;			/* Total battery charge i.e. state of battery				*/
	lu_bool_t  BatteryTempValid;			/* Indicates battery temperature sensor is fitted & working	*/
	lu_int32_t BatteryLowLevel;				/* Level below operations cannot be performed				*/
	lu_int32_t BatteryChargerVoltage;		/* Output voltage of charger								*/
	lu_int32_t BatteryCorrectedChargingVolts;
	lu_int32_t BatteryDeltaTemp;
	lu_int32_t RtuDeltaTemp;
	lu_int32_t BatteryDeltaVolts;
	lu_int32_t BatteryChargerVoltLimitOutput;
	lu_int32_t MeasuredChargingVolts;
	lu_int32_t BatteryAverageVoltage;
	lu_bool_t  RtuTempValid;
	lu_uint32_t BatteryTopUpTime;			/* Top up time											*/

} BatteryStateStr;

/* Charger State */
typedef struct ChargerStateDef
{
	lu_bool_t  ChargerEnabled;			/* Indicates is charger is enabled							*/
	lu_uint8_t ChargerState;			/* State of state machine									*/
	lu_bool_t  ChargerCurrentUnStable;	/* Indicate if charging current not changing 				*/
	lu_bool_t  ChargerCurrentMinimum;	/* Indicate if charging current below minimum				*/
	lu_uint8_t ChargerEndReason;		/* Indicate why charging cycle ended						*/
	lu_uint8_t ChargerSuspendOccurred;	/* Flag to indicate a suspend has occurred					*/
	lu_uint32_t ChargerSuspendStart;		/* Time entered suspend state								*/
	lu_uint32_t ChargerSuspendTime;		/* Time spent in suspend state								*/
} ChargerStateStr;

/* Battery Test State */
typedef struct BatteryTestStateDef
{
	lu_int32_t 	BatteryVoltage;				/* Current battery voltage level 							*/
	lu_int32_t 	BatteryStartVoltage;		/* Starting battery voltage level 							*/
	lu_int32_t 	BatteryEndVoltage;			/* Finishing battery voltage level 							*/
	lu_int32_t 	BatteryDischargeRate;		/* Discharge current via dummy load           				*/
	lu_uint32_t BatteryTestTime;			/* Time duration of current battery test					*/
	lu_int32_t 	BatteryTotalDrainCurrent;	/* Running total of battery drain i.e. current * time		*/
	lu_int32_t 	BatteryTestThreshold;		/* Voltage at which test fails								*/
	lu_uint32_t BatteryTestStartTime;		/* Time at which battery test was started					*/
	lu_uint32_t BatteryTestDuration;		/* Battery test time specified								*/
	lu_uint8_t 	BatteryTestEndReason;		/* Indicate why battery test ended							*/
	lu_bool_t  	BatteryTestInProgress;		/* Indicates if battery test is in progress					*/
	lu_bool_t  	BatteryTestCancelled;		/* Battery test cancelled flag								*/
	lu_bool_t	BatteryTestSuspended;		/* Battery test suspended flag								*/
	lu_uint32_t BatteryTestSuspendedTime;	/* Battery test suspension  start time						*/
} BatteryTestStateStr;

extern BatteryParamsStr  	batteryParams, *batteryParamsPtr;
extern BatteryStateStr	  	batteryState , *batteryStatePtr;
extern ChargerStateStr	  	chargerState , *chargerStatePtr;
extern BatteryTestStateStr  batTestState , *batTestStatePtr;
extern lu_bool_t 			batteryTestInProgress;
extern lu_bool_t 			chargerTestInProgress;
extern NVRAMOptPSMStr    	*NVRAMOptPSMPtr;

extern const LnInterpTable1DU16U16Str battVoltageTable1dU16U16[MAX_BATT_VOLTAGE_1DU16U16];
extern const LnInterpTable1DU16U16Str battCurrentLimitTable1dU16U16[MAX_BATT_CURRENT_LIMIT_1DU16U16];

extern SB_ERROR BatteryChargerInit(void);

extern SB_ERROR BatteryChargerTick(void);

extern SB_ERROR BatteryChargerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

extern void upDateInputConditions(void);
extern void upDateOutputConditions(void);
extern void updateBatteryCurrent(void);
extern lu_bool_t is_LV_Present( void );

extern lu_int32_t RAW_DC_V_SENSE;
extern lu_int32_t BAT_CHARGER_I_SENSE;
extern lu_int32_t BAT_TEST_I_SENSE;
extern lu_int32_t BAT_V_SENSE;
extern lu_int32_t CHARGER_V_SENSE;
extern lu_int32_t MAIN_DC_I_SENSE;
extern lu_int32_t MOT_I_SENSE;
extern lu_int32_t BAT_CHARGER_EN;
extern lu_int32_t BAT_TEMP;
extern SB_ERROR    BAT_TEMP_SENSOR_ERROR;
extern lu_int32_t BAT_TO_MOT_CONNECT;
extern lu_int32_t LOCAL_LINE;
extern lu_int32_t REMOTE_LINE;
extern lu_int32_t VIRT_BT_DISCONNECTED;
extern lu_int32_t VIRT_BT_ID_NVRAM_FAIL;
extern lu_int32_t VIRT_CH_FAULT;
extern lu_int32_t VIRT_CH_ON;
extern lu_int32_t VIRT_CH_SYSTEM_RUNNING;
extern lu_int32_t LED_CTRL_BAT_CHRG_GREEN;
extern lu_bool_t LED_CTRL_BAT_CHRG_GREEN_UPDATE;
extern lu_int32_t LED_CTRL_BAT_CHRG_GREEN_TIME;
extern lu_bool_t LED_CTRL_BAT_CHRG_GREEN_TIME_UPDATE;
extern lu_int32_t LED_CTRL_BAT_CHRG_RED;
extern lu_bool_t LED_CTRL_BAT_CHRG_RED_UPDATE;
extern lu_int32_t LED_CTRL_BAT_CHRG_RED_TIME;
extern lu_bool_t LED_CTRL_BAT_CHRG_RED_TIME_UPDATE;
extern lu_int32_t LED_CTRL_BAT_HEALTH_GREEN;
extern lu_bool_t LED_CTRL_BAT_HEALTH_GREEN_UPDATE;
extern lu_int32_t LED_CTRL_BAT_HEALTH_GREEN_TIME;
extern lu_bool_t LED_CTRL_BAT_HEALTH_GREEN_TIME_UPDATE;
extern lu_int32_t LED_CTRL_BAT_HEALTH_RED;
extern lu_bool_t LED_CTRL_BAT_HEALTH_RED_UPDATE;
extern lu_int32_t LED_CTRL_BAT_HEALTH_RED_TIME;
extern lu_bool_t LED_CTRL_BAT_HEALTH_RED_TIME_UPDATE;
extern lu_bool_t BAT_CONNECT_UPDATE;
extern lu_bool_t BAT_DISCONNECT_UPDATE;
extern lu_int32_t LED_CTRL_MOTOR_GREEN;
extern lu_int32_t LED_CTRL_MOTOR_RED;
extern lu_int32_t ACCUMLATED_DISCHARGE_TIME;
extern lu_int32_t ACCUMULATED_CHARGE_CURRENT;
extern lu_int32_t ACCUMULATED_DISCHARGE_CURRENT;
extern lu_int32_t BAT_CHARGER_CUR_LIM_SET;
extern lu_int32_t BAT_CHARGER_VOLT_SET;
extern lu_int32_t BAT_CONNECT;
extern lu_int32_t BAT_CONNECT_TIME;
extern lu_int32_t BAT_DISCONNECT_TIME;
extern lu_int32_t BAT_TEST_EN;
extern lu_int32_t BT_SHUTDOWN_APPROACHING;
extern lu_int32_t DAC_OUT_BT_TEST_LOAD;
extern lu_int32_t VIRT_AC_OFF;
extern lu_int32_t VIRT_BATTERY_TEST_TIME;
extern lu_int32_t VIRT_BATTERY_TEST_TOTAL_DISCHARGE;
extern lu_int32_t VIRT_BATTERY_VOLTAGE;
extern lu_int32_t VIRT_BT_ACCUMMULATED_CHARGE_TIME;
extern lu_int32_t VIRT_BT_BATTERY_CURRENT;
extern lu_int32_t VIRT_BT_CH_INHIBITED;
extern lu_int32_t VIRT_BT_CH_SHUTDOWN;
extern lu_int32_t VIRT_BT_CHARGE_CYCLES;
extern lu_int32_t VIRT_BT_COMMS_FAIL;
extern lu_int32_t VIRT_BT_DATA_NVRAM_FAIL;
extern lu_int32_t VIRT_BT_ESTIMATED_CAPACITY;
extern lu_int32_t VIRT_BT_FAULT;
extern lu_int32_t VIRT_BT_FULLY_CHARGED;
extern lu_int32_t VIRT_BT_LOW;
extern lu_int32_t VIRT_BT_OVER_TEMPERATURE;
extern lu_int32_t VIRT_BT_TEMP_FAIL;
extern lu_int32_t VIRT_BT_TEST_FAIL;
extern lu_int32_t VIRT_BT_TEST_IN_PROGRESS;
extern lu_int32_t VIRT_BT_TYPE_LEAD_ACID;
extern lu_int32_t VIRT_BT_TYPE_NIHM;
extern lu_int32_t VIRT_CH_CURRENT_FAULT;
extern lu_int32_t VIRT_CH_VOLTAGE_FAULT;
extern lu_int32_t VIRT_DRAIN_CURRENT;
extern lu_int32_t VIRT_MS_PEAK_HOLD_CURRENT;
extern lu_int32_t VIRT_MS_RELAY_ON;
extern lu_int32_t VIRT_ON_ELAPSED_TIME;
extern lu_int32_t VIRT_BT_TEST_HARDWARE_FAULT;
extern lu_int32_t VIRT_BT_TEST_PASS;
extern lu_int32_t VIRT_BT_TEST_FAIL_OR_RUN;
extern lu_int32_t VIRT_BT_TEST_PASS_OR_RUN;
extern lu_int32_t VIRT_CHARGER_EN;
extern lu_int32_t VIRT_CHARGER_CON;
extern lu_int32_t VIRT_BT_TYPE_DEFAULT;
extern lu_int32_t VIRT_CH_SET_VOLTAGE;
extern lu_int32_t VIRT_CH_SET_CURRENT;
extern lu_int32_t VIRT_BT_MGR_FSM_STATE;
extern lu_int32_t VIRT_BT_CHGR_FSM_STATE;
extern lu_int32_t VIRT_BT_TEST_END_REASON;
extern lu_bool_t VIRT_NiMH_FAST_CHRG_COMPLETE;


#endif /* BATTERCHARGERTOPLEVEL_H_ */
