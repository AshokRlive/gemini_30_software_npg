/*
 * BatManagerFSM.c
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */
/*****************************************************************************************************************************************
Description of this Finite State Machine (FSM) as in original template :
Finite State Machines are a collection of distinct "states" or "modes of operation". There can only ever be one active state at a time.
Transit from the active state to the next active state is determined by the exits defined in the active state in terms of events
or commands received and prevailing conditions such as flags and variables etc.
This FSM template caters for a number of system events such as a regular timer event or some CAN command events etc.
This FSM template caters for a state to execute code exclusively at entry to the state where it will ignore transitions back to itself.
This FSM template caters for the execution of code on every contiguous call to the same active state.
This FSM template caters for the testing of a number of possible exits where upon it will execute exit actions for the particular exit.
This FSM is always called via a function pointer that points to the present active state function.


FSM function pointer                                          <-- Called for specified system events and updated to point to a state
      |                                                           function (becoming the active state) by a condition function when
	  |                                                           it tests for a validates the condition it is looking for.
	  |
	  |_____state 1 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 1 entry action code        <-- Called exclusively on first entry to state 1 function
	  |             |
	  |             |_______state 1 unconditional code       <-- Called for every event call to FSM whilst in state 1
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 1 condition 1 function     <--| All condition functions get called until one returns "LU_TRUE".
	  |             |_______state 1 condition 2 function     <--| If "LU_TRUE" it changes active state ie function pointed to by "FSM function pointer"
	  |             |                                        <--| to point to new active state and executes any exit actions. The next event to call
	  |             |_______state 1 condition m function     <--| "FSM function pointer" will call the new active state function.
	  |
	  |
	  |_____state 2 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 2 entry action code        <-- Called exclusively on first entry to state 2 function
	  |             |
	  |             |_______state 2 unconditional code       <-- Called for every event call to FSM while in state 2
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 2 condition 1 function
	  |             |_______state 2 condition 2 function
	  |             |
	  |             |_______state 2 condition m function
	  |
	  |_____state n function

The original template caters for 6 state functions each of which has 3 exit condition functions

To create your a new state machine you need to
1)Re-create this file [newFileNameFSM.c]
2)Re-create its header file [newFileNameFSM.h]  and include the header here.
3)Re-create any timeout timers you will require
4)Re-create the timeout timers processing array with the names of your new timers.
5)Create any Global State exit functions that you require.
6)Create an array list of pointers to the Global State exit functions for processing them
7)For each state you require create a block of functions as below
7a)The StateFunc
7b)The StateEntryFunc
7c)The StateUncondFunc
7d)A StateExit function for each exit from the state
7e)An array of StateExit function pointers listing all the stateExit functions
8)Declare a state struct for each state and initialise it with the components created in the previous steps
9)Re-Create the array of pointers to state structures (allStates[]) and initialise with the address of all the states
10)Re-create the FSM struct for this FSM with its own name and initialise with the components created previously
11)Re-create the FSM_MAIN function with an appropriate name. This is the only function called from outside this file.
12)Write function definitions for all the files declared in the previous steps.
******************************************************************************************************************************************/

#include "BatteryChargerTopLevel.h"
#include "BatteryToFsmInterface.h"
#include "BatManagerFSM.h"
#include "LeadAcidChargerFSM.h"
#include "NiMHChargerFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "NVRam.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmBatteryChargerEnum.h"


/**********************************************************/
/*Actual storage for the Event input to this state machine*/
/**********************************************************/
static EventID event;

/***************************************************************************/
/*Declare internal fixed timers for use in this FSM. These decrement every */
/*timer tick event fed into the FSM. This is expected to be a timed call to*/
/*this FSM eg every second. The programmer must take proper account of this*/
/***************************************************************************/
static lu_int32_t startDelayAfterPowerUp = 2; /*2 second delay from power up before running this FSM*/
static lu_int32_t batTestRepeatTimer = 0;
static lu_int32_t batTestDurationTimer = 0;
static lu_int32_t batMaxTestDurationTimer = 0;
static lu_int32_t isBatWithSensorPresent = 0;
static lu_int32_t canBatTestRqstTimeValue = 0;
static lu_int32_t canBatTestRqstCapacityValue = 0;
static lu_int32_t periodicHealthCheckTimer = 0; 
static lu_int32_t batteryDisconnectTimer=0;
/*Timer re-charge values...set at via access function at init*/
static lu_int32_t batteryTestScheduleInSecs;
static lu_int32_t batteryTestDurationInSecs;


/*Add all internal fixed timers declared above here for processing and terminate with a NULL_FSM_TIMER*/
static lu_int32_t *fsmTimers_p[]={&startDelayAfterPowerUp,
								  &batTestRepeatTimer,
                                  &isBatWithSensorPresent, 
								  &batTestDurationTimer,
								  &batMaxTestDurationTimer,
								  &periodicHealthCheckTimer,
								  &batteryDisconnectTimer,
								  NULL_FSM_TIMER};


/*Internal timer functions*/
static void       fsmTimersUpdate(void);
static void       fsmTimerSet( lu_int32_t* timerName, lu_int32_t timerValue);
static lu_int32_t fsmTimerGet( lu_int32_t* timerName );
/*Access to allow the CAN battery test request to set its own timer*/
void fsmBatCANTestDurationAndCapacity( lu_int32_t timerValue, lu_int32_t capacity);


/************************/
/*User support variables*/
/************************/
static lu_bool_t chrgTemperatureDisable = LU_FALSE;


/*State Exit Steering Flags for complex derived conditions*/
/*These flags are used by the FSM exits and are usually set*/
/*or cleared in the unconditional function of a given state*/
static lu_bool_t  BAT_TEST_RQST;
static lu_bool_t  BAT_TEST_SCHED;
static lu_bool_t  DEPLOY_ALT_LV_MOTOR_SUPPLY;
static lu_bool_t  MOTOR_SUPPLY_RQST;


/*FSM function pointer to access the correct charger level FSM*/
fsmFunc CHARGER_FSM_FUNC_PTR;

/*Battery Test End Reasons*/
static lu_uint8_t batteryTestEndReason;

/* Battery test state and exit reasons */
enum
{
	BATT_TEST_TEST_RUNNING = 0,
	BATT_TEST_STOP_PASSED,
	BATT_TEST_STOP_LOW_VOLTAGE,
	BATT_TEST_STOP_OPERATION,
	BATT_TEST_STOP_LV_LOST,
	BATT_TEST_STOP_MAX_DISCHARGE,
	BATT_TEST_STOP_MAX_DURATION,
	BATT_TEST_STOP_CANCELED,
	BATT_TEST_STOP_LOW_CURRENT,
	BATT_TEST_STOP_OVERLOAD_CURRENT,
	BATT_TEST_STOP_ISO_VOLTS_TOO_LOW,
	BATT_TEST_STOP_LOAD_NOT_CONNECTED
};

/********************************************/
/*Battery Test Time vs Load Current profile */
/*A 1 Dimensional table of times vs current */
/*loads to give a load profile to mimic a  */
/*PTC load application for a period of time */
/********************************************/
const LnInterpTable1DU16U16Str BattTestLowTable1dU16U16[] =
{
		/*BATT_LOAD_TYPE_2AMP_PTC*/

        /********************************************************/
		/*Time of inflection point in seconds                   */
		/*  |       Load level for inflection point in milliAmps*/
		/*  |         |                                         */
		/*  V         V                                         */
		{	0,		2600},  /*Start load is 2600mA dropping to 1800mA at 10 seconds*/
		{	10,		1800},  /*From 10seconds load is 1800 dropping to 1760mA at 40 seconds*/
		{	40,		1760},
		{	150,	1600},
		{	298,	600 },
		{	299,	400 }
};
/*NUMBER OF INFLECTIONS/POINTS IN TABLE BELOW*/
#define MAX_BATT_TEST_LOW_INFLECTION_POINTS  (lu_uint8_t)(sizeof(BattTestLowTable1dU16U16)/sizeof(BattTestLowTable1dU16U16[0]))


/********************************************/
/*Battery Test Time vs Load Current profile */
/*A 1 Dimensional table of times vs current */
/*loads to give a load profile to mimick an */
/*actual load application for a period of   */
/*time                                      */
/********************************************/
const LnInterpTable1DU16U16Str BattTestPtcHighTable1dU16U16[] =
{
		/*BATT_LOAD_TYPE_10AMP_PTC_R */

        /********************************************************/
		/*Time of inflection point in seconds                   */
		/*  |       Load level for inflection point in milliAmps*/
		/*  |         |                                         */
		/*  V         V                                         */
		{	0,		7000},  /*Start load is 7000mA dropping to 6000mA at 3 seconds*/
		{	3,		6000},  /*From 3seconds load is 6000 dropping to 5000mA at 7 seconds*/
		{	7,		5000},
		{	36,		1200},
		{	58,		800 },
		{	59, 	400 }
};
/*NUMBER OF INFLECTIONS/POINTS IN TABLE BELOW*/
#define MAX_BATT_TEST_HIGH_INFLECTION_POINTS (lu_uint8_t)(sizeof(BattTestPtcHighTable1dU16U16)/sizeof(BattTestPtcHighTable1dU16U16[0]))

/************************/
/*User support functions*/
/************************/
/*Access function pointers BatManager FSM to appropriate Charger FSM*/
lu_bool_t(*getScheduledTestAcknowledge)(void);
lu_bool_t(*getBatteryPresenceIndicator)(void);
/*Access function pointers appropriate Charger FSM to this BatManager FSM */
lu_bool_t setChargerTemperatureDisable(lu_bool_t tempStopCharge);

lu_bool_t setChargerTemperatureDisable(lu_bool_t tempStopCharge);

lu_bool_t getFlag_BattChrgrDisconnetDetectFlag( void );

static lu_bool_t offChargeIsBatteryPresent( void );
static lu_bool_t isBatteryAboveDeepDischarge(void);
static lu_bool_t is_LvMotorDriveAvailable( void );
static lu_uint16_t getRequiredCurrentLoadAtTime(lu_uint16_t presTime, const LnInterpTable1DU16U16Str *interpTable, lu_uint8_t numberOfInflections);
static lu_int32_t startBatteryTest(BatteryTestStateStr *batTestStatePtr);
static lu_uint8_t monitorBatteryTest(fsm *this, BatteryTestStateStr *batTestStatePtr);


/************************************************************************/
/*Only one Function to terminate exit searches in all states of this FSM*/
/************************************************************************/
static lu_bool_t stateExitSearchEnd(fsm *this);
/************************************************************************/

/****************************************/
/*Global exits processing function      */
/****************************************/
static lu_bool_t BAT_MAN_GLOBAL_Exit_Conditions(fsm *this);

/*****************************/
/*Global State exit functions*/
/*****************************/
static lu_bool_t BAT_MAN_GLOBAL_ExitFunc1(fsm *this);
/*Add more global exits here if needed*/
/*One array listing all the global exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs BAT_MAN_GLOBAL_ExitFuncs[]={BAT_MAN_GLOBAL_ExitFunc1,
                                               stateExitSearchEnd};

/*****************************************************************/
/*****************************************************************/
/*One of these blocks for each state in this Finite State Machine*/
/*****************************************************************/
/*****************************************************************/

/**********************************************************/
/*    INITIALISE_ state function prototypes and initialisers*/
/**********************************************************/
static void INITIALISE_StateFunc(fsm *this);
static void INITIALISE_StateEntryFunc(fsm *this);
static void INITIALISE_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t INITIALISE_StateExitTo_BAT_NOT_PRESENT(fsm *this);
static lu_bool_t INITIALISE_StateExitTo_NO_LV(fsm *this);
static lu_bool_t INITIALISE_StateExitTo_BAT_CHARGING(fsm *this);
/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs INITIALISE_StateExitFuncs[]={INITIALISE_StateExitTo_BAT_NOT_PRESENT,
		                                        INITIALISE_StateExitTo_NO_LV,
											    INITIALISE_StateExitTo_BAT_CHARGING,
                                                stateExitSearchEnd};

/***************************************************************/
/*    OPERATE_SWITCHGEAR_ state function prototypes and initialisers*/
/***************************************************************/
static void OPERATE_SWITCHGEAR_StateFunc(fsm *this);
static void OPERATE_SWITCHGEAR_StateEntryFunc(fsm *this);
static void OPERATE_SWITCHGEAR_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t OPERATE_SWITCHGEAR_StateExitTo_BAT_CHARGING_Via_Cancel(fsm *this);
/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs OPERATE_SWITCHGEAR_StateExitFuncs[]={OPERATE_SWITCHGEAR_StateExitTo_BAT_CHARGING_Via_Cancel,
                                                        stateExitSearchEnd};

/*************************************************************/
/*   BAT_CHARGING state function prototypes and initialisers*/
/*************************************************************/
static void BAT_CHARGING_StateFunc(fsm *this);
static void BAT_CHARGING_StateEntryFunc(fsm *this);
static void BAT_CHARGING_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t BAT_CHARGING_StateExitTo_OPERATE_SWITCHGEAR(fsm *this);
static lu_bool_t BAT_CHARGING_StateExitTo_BAT_NOT_PRESENT(fsm *this);
static lu_bool_t BAT_CHARGING_StateExitTo_NO_LV(fsm *this);
static lu_bool_t BAT_CHARGING_StateExitTo_BAT_TEST_Sched(fsm *this);
static lu_bool_t BAT_CHARGING_StateExitTo_BAT_TEST_Rqst(fsm *this);
/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs BAT_CHARGING_StateExitFuncs[]={BAT_CHARGING_StateExitTo_BAT_NOT_PRESENT,
													  BAT_CHARGING_StateExitTo_NO_LV,
													  BAT_CHARGING_StateExitTo_OPERATE_SWITCHGEAR,
													  BAT_CHARGING_StateExitTo_BAT_TEST_Rqst,
                                                      BAT_CHARGING_StateExitTo_BAT_TEST_Sched,
                                                      stateExitSearchEnd};

/**************************************************************/
/*    NO_LV_ state function prototypes and initialisers*/
/**************************************************************/
static void NO_LV_StateFunc(fsm *this);
static void NO_LV_StateEntryFunc(fsm *this);
static void NO_LV_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t NO_LV_StateExitTo_DEEP_DISCHARGE(fsm *this);
static lu_bool_t NO_LV_StateExitTo_INITIALISE(fsm *this);
static lu_bool_t NO_LV_StateExitTo_OPERATE_SWITCHGEAR(fsm *this);
/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs NO_LV_StateExitFuncs[]={NO_LV_StateExitTo_DEEP_DISCHARGE,
												      NO_LV_StateExitTo_INITIALISE,
												      NO_LV_StateExitTo_OPERATE_SWITCHGEAR,
                                                      stateExitSearchEnd};

/*********************************************************/
/*   BAT_TEST_ state function prototypes and initialisers*/
/*********************************************************/
static void BAT_TEST_StateFunc(fsm *this);
static void BAT_TEST_StateEntryFunc(fsm *this);
static void BAT_TEST_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t BAT_TEST_StateExitTo_OPERATE_SWITCHGEAR(fsm *this);
static lu_bool_t BAT_TEST_StateExitTo_NO_LV(fsm *this);
static lu_bool_t BAT_TEST_StateExitTo_BAT_CHARGING_Test_End(fsm *this);

/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs BAT_TEST_StateExitFuncs[]={	BAT_TEST_StateExitTo_OPERATE_SWITCHGEAR,
											    BAT_TEST_StateExitTo_NO_LV,
											    BAT_TEST_StateExitTo_BAT_CHARGING_Test_End,
												stateExitSearchEnd};

/*********************************************************/
/*   DEEP_DISCHARGE_ state function prototypes and initialisers*/
/*********************************************************/
static void DEEP_DISCHARGE_StateFunc(fsm *this);
static void DEEP_DISCHARGE_StateEntryFunc(fsm *this);
static void DEEP_DISCHARGE_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t DEEP_DISCHARGE_StateExitTo_INITIALISE(fsm *this);
/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs DEEP_DISCHARGE_StateExitFuncs[]={ DEEP_DISCHARGE_StateExitTo_INITIALISE,
		                                             stateExitSearchEnd};

/**************************************************************/
/*    BAT_NOT_PRESENT_ state function prototypes and initialisers*/
/**************************************************************/
static void BAT_NOT_PRESENT_StateFunc(fsm *this);
static void BAT_NOT_PRESENT_StateEntryFunc(fsm *this);
static void BAT_NOT_PRESENT_StateUncondFunc(fsm *this);
/*One for each exit from the state*/
static lu_bool_t BAT_NOT_PRESENT_StateExitTo_INITIALISE(fsm *this);
/*Add more exits for this state here if needed*/
/*One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs BAT_NOT_PRESENT_StateExitFuncs[]={BAT_NOT_PRESENT_StateExitTo_INITIALISE,
                                                     stateExitSearchEnd};

/***************************************************************/
/*   ANY_ADDITIONAL_ state function prototypes and initialisers*/
/***************************************************************/

/************************************************************/
/*InitiaLISE each of the states in this Finite State Machine*/
/************************************************************/
state INITIALISE            ={INITIALISE_StateFunc,         INITIALISE_StateEntryFunc,         INITIALISE_StateUncondFunc,         INITIALISE_StateExitFuncs};
state OPERATE_SWITCHGEAR    ={OPERATE_SWITCHGEAR_StateFunc, OPERATE_SWITCHGEAR_StateEntryFunc, OPERATE_SWITCHGEAR_StateUncondFunc, OPERATE_SWITCHGEAR_StateExitFuncs};
state BAT_CHARGING          ={BAT_CHARGING_StateFunc,       BAT_CHARGING_StateEntryFunc,       BAT_CHARGING_StateUncondFunc,       BAT_CHARGING_StateExitFuncs};
state NO_LV                 ={NO_LV_StateFunc,              NO_LV_StateEntryFunc,              NO_LV_StateUncondFunc,              NO_LV_StateExitFuncs};
state BAT_TEST              ={BAT_TEST_StateFunc,           BAT_TEST_StateEntryFunc,           BAT_TEST_StateUncondFunc,           BAT_TEST_StateExitFuncs};
state DEEP_DISCHARGE        ={DEEP_DISCHARGE_StateFunc,     DEEP_DISCHARGE_StateEntryFunc,     DEEP_DISCHARGE_StateUncondFunc,     DEEP_DISCHARGE_StateExitFuncs};
state BAT_NOT_PRESENT       ={BAT_NOT_PRESENT_StateFunc,    BAT_NOT_PRESENT_StateEntryFunc,    BAT_NOT_PRESENT_StateUncondFunc,    BAT_NOT_PRESENT_StateExitFuncs};
/*ADD MORE STATE INITIALISERS HERE IF REQUIRED*/



/***********************************************************************************************/
/*Initialise an array list of pointers to state for all the states in this finite state machine*/
/***********************************************************************************************/
static state* allStates[]={&INITIALISE,
                           &OPERATE_SWITCHGEAR,
				           &BAT_CHARGING,
					       &NO_LV,
					       &BAT_TEST,
					       &DEEP_DISCHARGE,
						   &BAT_NOT_PRESENT
					      };           /*ADD MORE STATE NAMES TO THE INTIALISER HERE*/


/******************************************************************************************/
/*Initialise present state timer/counter...reset every state change and does not roll over*/
/******************************************************************************************/
static lu_uint32_t presStateCounter=0;

/*****************************************************************/
/*Initialise a state machine structure with components from above*/
/*****************************************************************/

static fsm BAT_MANAGER_FSM = { &event,                        /*Makes the FSM event pointer point to actual event storage*/
                               &INITIALISE.stateCall,         /*presActiveState initialisation - should be the intended initialising state*/
                               &OPERATE_SWITCHGEAR.stateCall, /*prevActiveState initialisation - can be any different state to presActive for initialisation*/
                               allStates,                     /*statesList initialisation*/
                               &presStateCounter,     	      /*Reset every time state changes and does not roll over*/
                               REPLY_STATUS_OKAY
                             };


/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/*FINITE STATE MACHINE FUNCTION DEFINITIONS                                                           */
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/


/**************************************************/
/*Call the finite state machine for each new event*/
/*This is the only call to the FSM from outside   */
/*except for access to some external group timers.*/
/*The return from this function allows specific   */
/*events (events other than timerTick) to return  */
/*an acknowledgement as to whether or not they    */
/*were acted on.                                  */
/**************************************************/
REPLY_STATUS BAT_MANAGER_FSM_MAIN(EventID eventIn)
{
	/*Add the new FSM event to this FSM which is statically declared in this file*/
	*(BAT_MANAGER_FSM.event) = eventIn;

	/*If we have just changed state...*/
	if(*(BAT_MANAGER_FSM.presActiveState) != *(BAT_MANAGER_FSM.prevActiveState) )
	{
		/*...reset state seconds counter*/
		*(BAT_MANAGER_FSM.presActiveStateSecsCounter)=0;
	}

	/*Process all the fsm timers*/
	if(*(BAT_MANAGER_FSM.event) == TimerTickEvent )
	{
		/*Timer tick events are not specific CAN requests. TheBATTERY_PRESENTy update conditions and times and so are always REPLY_STATUS_OKAY*/
		BAT_MANAGER_FSM.eventAcknowledge = REPLY_STATUS_OKAY;
		/*Update the internal fixed timers*/
		fsmTimersUpdate();

		/*Provided we won't roll over...*/
		if(*(BAT_MANAGER_FSM.presActiveStateSecsCounter) != 0xffffffff )
		{
			/*...update time spent in the present state*/
			/*It would take over a hundred years in a single state to roll over :^)*/
			*(BAT_MANAGER_FSM.presActiveStateSecsCounter)+=1;
		}
	}
	else
	{
		/*Events that require an acknowledge must assign this to be REPLY_STATUS_OKAY when they know*/
		/*that the event will be acted on in the unconditional or an exit state function*/
		BAT_MANAGER_FSM.eventAcknowledge = REPLY_STATUS_OPERATE_REJECT_ERROR;
	}

	/*Call the present active state to check for exits*/
	(*BAT_MANAGER_FSM.presActiveState)( &BAT_MANAGER_FSM );

	return BAT_MANAGER_FSM.eventAcknowledge;
}

/*********************************************************************************************/
/*This function simply terminates the exit search for any list of state condition exits      */
/*It must always be positioned at the end of the array of function pointers to exit condition*/
/*********************************************************************************************/
static lu_bool_t stateExitSearchEnd(fsm *this)
{
	PARAM_NOT_USED(this);

    return LU_TRUE;
}

/******************************************************/
/* Process global condition function exits*/
/******************************************************/
static lu_bool_t BAT_MAN_GLOBAL_Exit_Conditions(fsm *this)
{
	lu_bool_t retValue=LU_FALSE;
	lu_uint8_t nextExitFunc=0;
    /*Execute each global conditional exit function test in turn*/
    /*If valid exit found or search function terminated search*/
    while(BAT_MAN_GLOBAL_ExitFuncs[nextExitFunc](this)==LU_FALSE)
    {
        nextExitFunc++;
    }
	/*Filter out a "LU_TRUE" returned from the search terminating function*/
	if(BAT_MAN_GLOBAL_ExitFuncs[nextExitFunc] != stateExitSearchEnd)
	{
        /*If here and not the exit search terminating function it must be a valid global exit*/
		retValue=LU_TRUE;
	}
	return retValue;
}

/******************************************************/
/* Global exit condition function - GLOBAL_ExitFunc1  */
/******************************************************/
static lu_bool_t BAT_MAN_GLOBAL_ExitFunc1(fsm *this)
{
	PARAM_NOT_USED(this);
	lu_bool_t retValue = LU_FALSE;

	return retValue;
}

/******************************************************/
/******************************************************/
/* INITIALISE_ state function definitions         */
/******************************************************/
/******************************************************/
static void INITIALISE_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
    /*Check if at entry and if so execute entry code */
    if(*(this->presActiveState) != *(this->prevActiveState))
    {
		INITIALISE_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }												
    /*Execute any unconditional code */
    INITIALISE_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(INITIALISE_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/*ENTRY actions for INITIALISE                    */
/******************************************************/
static void INITIALISE_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the battery voltage*/
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;

	VIRT_BT_MGR_FSM_STATE = INITIALISE_STATE;

	/* Clear the following indications */
	VIRT_BT_CH_SHUTDOWN = LU_FALSE;
	BT_SHUTDOWN_APPROACHING = LU_FALSE;
	VIRT_BT_LOW = LU_FALSE;

	/*Send a connection pulse to the SR Flip flop to connect the battery in parallel to RTU power supply*/
	BAT_CONNECT_UPDATE = LU_TRUE;
}
/******************************************************/
/*UNCONDITIONAL actions for INITIALISE            */
/******************************************************/
static void INITIALISE_StateUncondFunc(fsm *this)
{
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;

	/*Send a connection pulse to the SR Flip flop to connect the battery in parallel to RTU power supply*/
	BAT_CONNECT_UPDATE = LU_TRUE;

	/*Turn test load off*/
	BAT_TEST_EN = CTRL_OFF;
	/* Reset the DAC to Zero */
	DAC_OUT_BT_TEST_LOAD = 0;

	/*Process any timer tick events*/
	if(*(this->event) == TimerTickEvent)
    {

    }
}
/******************************************************/
/*EXIT 1 actions for INITIALISE                   */
/******************************************************/
static lu_bool_t INITIALISE_StateExitTo_BAT_NOT_PRESENT(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

	/*If there is no battery detected or no battery NVRam\sensor ...*/
    if( (*(this->presActiveStateSecsCounter) > INITIALISE_STABILISATION_TIME) &&
    	(offChargeIsBatteryPresent() != LU_TRUE)                               )
	{
		/*...move on to battery unusable state*/
		*(this->presActiveState)=BAT_NOT_PRESENT_StateFunc;
		/*CONTROL ACTIONS*/
		/* NONE */

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
	}
    return retVal;
}
/******************************************************/
/*EXIT 2 actions for INITIALISE                       */
/******************************************************/
static lu_bool_t INITIALISE_StateExitTo_NO_LV(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

	/*If there is a battery we can work with...*/
    if( (*(this->presActiveStateSecsCounter) > INITIALISE_STABILISATION_TIME) &&
    	(is_LV_Present() != LU_TRUE )                                             )
    {
		/*...move on to the NO_LV state*/
		*(this->presActiveState)=NO_LV_StateFunc;
		/*CONTROL ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 3 actions for INITIALISE                       */
/******************************************************/
static lu_bool_t INITIALISE_StateExitTo_BAT_CHARGING(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
    /* Battery present and LV have already been tested for */
    if( (offChargeIsBatteryPresent() == LU_TRUE)                            &&
    	(is_LV_Present() == LU_TRUE)                                          &&
    	(*(this->presActiveStateSecsCounter) > INITIALISE_STABILISATION_TIME)    )
    {
		/*...move on to the charging state*/
		*(this->presActiveState)=BAT_CHARGING_StateFunc;
		/*CONTROL ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}

/******************************************************/
/******************************************************/
/* OPERATE_SWITCHGEAR_ state function definitions       */
/******************************************************/
/******************************************************/
static void OPERATE_SWITCHGEAR_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
	
    /*Check if at entry and if so execute entry code */
	if(*(this->presActiveState) != *(this->prevActiveState))
    {
		OPERATE_SWITCHGEAR_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }	
    /*Execute any unconditional code */
    OPERATE_SWITCHGEAR_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not, see if there are any state dependent exits*/
		while(OPERATE_SWITCHGEAR_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/*ENTRY actions for OPERATE_SWITCHGEAR                  */
/******************************************************/
static void OPERATE_SWITCHGEAR_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	VIRT_BT_MGR_FSM_STATE = OPERATE_SWITCHGEAR_STATE;
}
/******************************************************/
/*UNCONDITIONAL actions for OPERATE_SWITCHGEAR          */
/******************************************************/
static void OPERATE_SWITCHGEAR_StateUncondFunc(fsm *this)
{
	PARAM_NOT_USED(this);
	lu_int32_t iBatteryRtuDrain = 0;
	lu_int32_t iBatteryMotDrain = 0;
	lu_int32_t iBatteryDrain = 0;

	/*Publish the battery voltage*/
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;

	/*If it looks like we have no LV AC  the battery must be powering the RTU*/
	if( is_LV_Present() != LU_TRUE)
	{
		/*Read back the the current into the RTU. (because LV is not present its supplied from battery)*/
		iBatteryRtuDrain = MAIN_DC_I_SENSE;
	}
	/*Read back the motor current*/
	iBatteryMotDrain = MOT_I_SENSE;

	/*The total drain on the battery in this state is ...*/
	iBatteryDrain = iBatteryRtuDrain + iBatteryMotDrain;
	/*Publish RTU current as the battery drain current i.e. battery is supplying the RTU*/
	VIRT_BT_BATTERY_CURRENT = iBatteryDrain;
	/**/
	VIRT_DRAIN_CURRENT = iBatteryDrain;
}
/******************************************************/
/*EXIT 1 actions for OPERATE_SWITCHGEAR                 */
/******************************************************/
static lu_bool_t OPERATE_SWITCHGEAR_StateExitTo_BAT_CHARGING_Via_Cancel(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
	
	/*If the motor drive has timed out...*/
    if(*(this->event) == MotorSupplyCancelEvent)
    {
		/*...return to the BAT_BAT_CHARGING state*/
		*(this->presActiveState)=BAT_CHARGING_StateFunc;
		/*CONTROL ACTIONS*/

		/*INDICATOR ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;	
    }
    return retVal;
}


/******************************************************/
/******************************************************/
/* BAT_CHARGING state function definitions        */
/******************************************************/
/******************************************************/
static void BAT_CHARGING_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
	
    /*Check if at entry and if so execute entry code */
	if(*(this->presActiveState) != *(this->prevActiveState))
    {
		BAT_CHARGING_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }	
    /*Execute any unconditional code */
    BAT_CHARGING_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(BAT_CHARGING_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/*ENTRY actions for BAT_CHARGING                  */
/******************************************************/
static void BAT_CHARGING_StateEntryFunc(fsm *this)
{	
	PARAM_NOT_USED(this);

	VIRT_BT_MGR_FSM_STATE = BAT_CHARGING_STATE;
}
/******************************************************/
/*UNCONDITIONAL actions for BAT_CHARGING*/
/******************************************************/
static void BAT_CHARGING_StateUncondFunc(fsm *this)
{
	/****************************************************************/
	/*Decide, based on battery chemistry, which charging FSM to call*/
	/****************************************************************/
	switch(batteryParamsPtr->BatteryType)
	{
		case BATT_TYPE_LEAD_ACID:
		{
			/*Get function pointer to correct charger level FSM*/
			CHARGER_FSM_FUNC_PTR = CHRG_LEAD_ACID_FSM_MAIN;
			/*Pass event through to lead acid charging sub state FSM*/
			CHARGER_FSM_FUNC_PTR( *(this->event) );
			/*Publish battery type for the CONFIG tool*/
			VIRT_BT_TYPE_NIHM = LU_FALSE;
			VIRT_BT_TYPE_LEAD_ACID = LU_TRUE;
			/*Point to the correct charger support function*/
			getScheduledTestAcknowledge = getLdAcidScheduledTestAcknowledge;
			getBatteryPresenceIndicator = getLdAcidBatteryPresenceIndicator;
		}
		break;

		case BATT_TYPE_NI_MH:
		{
			/*Get function pointer to correct charger level FSM*/
			CHARGER_FSM_FUNC_PTR = CHRG_NICKEL_METAL_FSM_MAIN;
			/*Pass event through to Nikel MH charging sub state FSM*/
		    CHARGER_FSM_FUNC_PTR( *(this->event) );
			/*Publish battery type for the CONFIG tool*/
			VIRT_BT_TYPE_NIHM = LU_TRUE;
			VIRT_BT_TYPE_LEAD_ACID = LU_FALSE;
			/*Point to the correct charger support function*/
			getScheduledTestAcknowledge = getNiMHScheduledTestAcknowledge;
			getBatteryPresenceIndicator = getNiMHBatteryPresenceIndicator;
		}
		break;

		default:
		{

		}
		break;
	};

	MOTOR_SUPPLY_RQST = LU_FALSE;
	/*If we receive a motor supply request...*/
    if(*(this->event) == MotorSupplyOnEvent)
    {
		/*If the battery status allows... */
		/*...we can flag up this exit*/
		MOTOR_SUPPLY_RQST = LU_TRUE;

	}	
	
	BAT_TEST_RQST = LU_FALSE;
	/*If we receive a battery test REQUEST (i.e. not a schedule test)*/
	if(*(this->event) == BatteryTestEvent)
	{			
		/*Set steering flag to exit to BAT_TEST state*/
		BAT_TEST_RQST =  LU_TRUE;
	}

	BAT_TEST_SCHED = LU_FALSE;
	/*If we get a timer tick event... */
	if(*(this->event) == TimerTickEvent)
	{
		/*Request permission from the charger for a SCHEDULED battery test.*/
		/*Note... "batteryTestScheduleInSecs > 0" means automatic test schedule is active */
		if( fsmTimerGet(&batTestRepeatTimer) ==  FSM_TIMER_TIMED_OUT &&
		    (batteryTestScheduleInSecs > 0)  )
		{
			/* Inform the Charger FSM that a scheduled battery test is pending */
			CHARGER_FSM_FUNC_PTR(ScheduledTestEvent);

			/* Check to see if the Charger FSM has given permission to do a SCHEDULED test */
			if(getScheduledTestAcknowledge() == LU_TRUE    )
			{
				/*OK...the Charger FSM has given permission...set steering flag to change to BAT TEST state*/
				BAT_TEST_SCHED =  LU_TRUE;
			}
		}
	}

	/*if the battery is less than 22v for typical lead acid*/
	if(BAT_V_SENSE < batteryParamsPtr->BatteryLowLevel )
	{
		/*BAT LOW flag takes over ...*/
		VIRT_BT_LOW = LU_TRUE;
	}
	else if(BAT_V_SENSE > (batteryParamsPtr->BatteryLowLevel+HYSTERESIS100mV) )
	{
		VIRT_BT_LOW = LU_FALSE;
	}

}
/******************************************************/
/*EXIT 1 actions for BAT_CHARGING                 */
/******************************************************/
static lu_bool_t BAT_CHARGING_StateExitTo_BAT_NOT_PRESENT(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

	/*If the battery status is showing no battery detected */
    if(getBatteryPresenceIndicator() != LU_TRUE)
	{
		/*...move on to battery unusable state*/
		*(this->presActiveState)=BAT_NOT_PRESENT_StateFunc;
		/*CONTROL ACTIONS*/
		/*Extra event to close down the charger level FSM*/
		CHARGER_FSM_FUNC_PTR( GoToIdleEvent );

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
	}
    
    return retVal;
}
/******************************************************/
/*EXIT 2 actions for BAT_CHARGING                 */
/******************************************************/
static lu_bool_t BAT_CHARGING_StateExitTo_NO_LV(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

	/*if no DC LV SUPPLY */
	if( is_LV_Present() == LU_FALSE )
	{
		/*...move on to battery unusable state*/
		*(this->presActiveState)=NO_LV_StateFunc;
		/*CONTROL ACTIONS*/
		/*Extra event to close down the charger level FSM*/
		CHARGER_FSM_FUNC_PTR( GoToIdleEvent );

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
	}
    
    return retVal;
}
/******************************************************/
/*EXIT 3 actions for BAT_CHARGING                 */
/******************************************************/
static lu_bool_t BAT_CHARGING_StateExitTo_OPERATE_SWITCHGEAR(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
    if( MOTOR_SUPPLY_RQST==LU_TRUE )
    {
    	*(this->presActiveState)=OPERATE_SWITCHGEAR_StateFunc;
    	/*CONTROL ACTIONS*/
    	/*This event came from a CAN request message and requires a confirm flag setting to be returned*/
    	this->eventAcknowledge = REPLY_STATUS_OKAY;
		/*Extra event to close down the charger level FSM*/
    	CHARGER_FSM_FUNC_PTR( GoToIdleEvent );

    	/*INDICATOR ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 4 actions for BAT_CHARGING                 */
/******************************************************/	   
static lu_bool_t BAT_CHARGING_StateExitTo_BAT_TEST_Rqst(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(BAT_TEST_RQST == LU_TRUE)
    {
		/*Change present active state to BAT_TEST ready for next call*/
		*(this->presActiveState)=BAT_TEST_StateFunc;

		/*CONTROL ACTIONS*/
    	/*This event came from a CAN request and requires an acknowledge flag setting to be returned later*/
    	this->eventAcknowledge = REPLY_STATUS_OKAY;
		/*Extra event to close down the charger level FSM*/
		CHARGER_FSM_FUNC_PTR( GoToIdleEvent );

		/*The fsmTimers run in seconds so adjust duration time from minutes to seconds*/
		fsmTimerSet(&batTestDurationTimer,canBatTestRqstTimeValue * SECONDS_PER_MINUTE );

		/*INDICATOR ACTIONS*/
		VIRT_BATTERY_TEST_TIME = 0;
		VIRT_BATTERY_TEST_TOTAL_DISCHARGE = 0;

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 5 actions for BAT_CHARGING                 */
/******************************************************/
static lu_bool_t BAT_CHARGING_StateExitTo_BAT_TEST_Sched(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(BAT_TEST_SCHED == LU_TRUE)
    {
		/*Change present active state to BAT_TEST ready for next call*/
		*(this->presActiveState)=BAT_TEST_StateFunc;

		/*CONTROL ACTIONS		 */
		/*Extra event to close down the charger level FSM*/
		CHARGER_FSM_FUNC_PTR( GoToIdleEvent );

		/*The fsmTimers run in seconds so adjust duration time from minutes to seconds*/
		/*It continues to run even with excursions from this state.*/
		/*When it reaches 0 and the conditions for test fall into line it will do the test.*/
		/*The repeat test timer is only recharged here as the test actually commences*/
		fsmTimerSet(&batTestDurationTimer,batteryTestDurationInSecs);
		/*Set the test repeat time for next test*/
		/*The fsmTimer works in Seconds */
		fsmTimerSet(&batTestRepeatTimer,(lu_int32_t)(batteryTestScheduleInSecs));

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}

/******************************************************/
/******************************************************/
/* NO_LV_ state function definitions       */
/******************************************************/
/******************************************************/
static void NO_LV_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
    /*Check if at entry and if so execute entry code */
	if(*(this->presActiveState) != *(this->prevActiveState))
    {
		NO_LV_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }	
    /*Execute any unconditional code */
    NO_LV_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(NO_LV_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}

/******************************************************/
/*ENTRY actions for NO_LV                  */
/******************************************************/
static void NO_LV_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	VIRT_BT_MGR_FSM_STATE = NO_LV_STATE;
}

/******************************************************/
/*UNCONDITIONAL actions for NO_LV          */
/******************************************************/
static void NO_LV_StateUncondFunc(fsm *this)
{
	/*Publish the battery voltage*/
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;

	/*If we get a timer tick event... */
	if(*(this->event) == TimerTickEvent)
	{
		/*Shutdown approaching threshold typically 22.5v for lead acid*/
		if(BAT_V_SENSE < batteryParamsPtr->BatteryPackShutdownLevelVolts)
		{
			BT_SHUTDOWN_APPROACHING = LU_TRUE;
		}
		else if(BAT_V_SENSE > (batteryParamsPtr->BatteryPackShutdownLevelVolts+HYSTERESIS100mV))
		{
			BT_SHUTDOWN_APPROACHING = LU_FALSE;
		}

		/*if the battery is less than 22v for typical lead acid*/
		if(BAT_V_SENSE < batteryParamsPtr->BatteryLowLevel )
		{
			/*BAT LOW flag takes over ...*/
			VIRT_BT_LOW = LU_TRUE;
		}
		else if(BAT_V_SENSE > (batteryParamsPtr->BatteryLowLevel+HYSTERESIS100mV) )
		{
			VIRT_BT_LOW = LU_FALSE;
		}
	}

	MOTOR_SUPPLY_RQST = LU_FALSE;
	/*If we receive a motor supply request...*/
    if(*(this->event) == MotorSupplyOnEvent)
    {
		MOTOR_SUPPLY_RQST = LU_TRUE;
	}	
	
}
/******************************************************/
/*EXIT 1 actions for NO_LV                 */
/******************************************************/
static lu_bool_t NO_LV_StateExitTo_DEEP_DISCHARGE(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if( isBatteryAboveDeepDischarge() != LU_TRUE )
    {
		/*Change present active state to NO_LV ready for next call*/
		*(this->presActiveState)=DEEP_DISCHARGE_StateFunc;

		/*CONTROL ACTIONS*/

		/*Publish charger is disconnected*/

		/*INDICATOR ACTIONS*/


		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 2 actions for NO_LV                 */
/******************************************************/
static lu_bool_t NO_LV_StateExitTo_OPERATE_SWITCHGEAR(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
    if(MOTOR_SUPPLY_RQST == LU_TRUE)
    {
		/*Change present active state to NO_LV ready for next call*/
		*(this->presActiveState)=OPERATE_SWITCHGEAR_StateFunc;

		/*CONTROL ACTIONS*/
		/*This event is from a CAN request and requires an acknowledge flag setting to be returned later*/
		this->eventAcknowledge = REPLY_STATUS_OKAY;

		/*INDICATOR ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 3 actions for NO_LV                 */
/******************************************************/
static lu_bool_t NO_LV_StateExitTo_INITIALISE(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if( is_LV_Present() == LU_TRUE )
    {
		/*Change present active state to BAT_TEST ready for next call*/
		*(this->presActiveState)=INITIALISE_StateFunc;

		/*CONTROL ACTIONS*/

		/*INDICATOR ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;	
}

/******************************************************/
/******************************************************/
/* BAT_TEST_ state function definitions               */
/******************************************************/
/******************************************************/
static void BAT_TEST_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
    /*Check if at entry and if so execute entry code */
	if(*(this->presActiveState) != *(this->prevActiveState))
    {
		BAT_TEST_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }	
    /*Execute any unconditional code */
    BAT_TEST_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(BAT_TEST_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/*ENTRY actions for BAT_TEST                          */
/******************************************************/
static void BAT_TEST_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	VIRT_BT_MGR_FSM_STATE = BAT_TEST_STATE;

	/*Check NVRam for a max duration override test time*/
	if (batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes > 0)
	{
		fsmTimerSet( &batMaxTestDurationTimer, (batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes * SECONDS_PER_MINUTE));
	}

	/*Publish test running*/
	VIRT_BT_TEST_IN_PROGRESS = FLAG_SET;
	/*INDICATOR ACTIONS*/
	VIRT_BATTERY_TEST_TIME = 0;
	VIRT_BATTERY_TEST_TOTAL_DISCHARGE = 0;

	/*Initialise test data structure and check test can run*/
	if( startBatteryTest(&batTestState) != BATT_TEST_TEST_RUNNING)
	{
		/*...if it can't run we must force a timeout*/
		fsmTimerSet( &batTestDurationTimer,FSM_TIMER_TIMED_OUT);
		/*Publish test complete*/
		VIRT_BT_TEST_IN_PROGRESS = FLAG_CLR;
	}
}
/******************************************************/
/*UNCONDITIONAL actions for BAT_TEST                  */
/******************************************************/
static void BAT_TEST_StateUncondFunc(fsm *this)
{
	/*Publish the battery voltage*/
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;

	/*Update the running test time total*/
	VIRT_BATTERY_TEST_TIME = *(this->presActiveStateSecsCounter);

	/*This test monitoring function is being called every second to update the test load profile...  */
	/*...monitor/store battery voltage, and battery drain current. It returns a stop code which tells*/
	/*us if the test is still running or stopped for an end reason which can be a fail,              */
	/*an abort(not a fail reason) or a pass. These stop codes are used by this FSM to direct exits   */
	/*from this state*/
	batteryTestEndReason = monitorBatteryTest(this, &batTestState);
}

/******************************************************/
/*EXIT 1 actions for BAT_TEST                         */
/******************************************************/
lu_bool_t BAT_TEST_StateExitTo_OPERATE_SWITCHGEAR(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
    /*If Test ends for this reason its an abort - we must supply motor power*/
    if(batteryTestEndReason == BATT_TEST_STOP_OPERATION)
    {
    	/*Change present active state*/
    	*(this->presActiveState)=OPERATE_SWITCHGEAR_StateFunc;

		/*CONTROL ACTIONS*/
		/*Switch off test load*/
		BAT_TEST_EN = CTRL_OFF;
		/* Reset the DAC to Zero */
		DAC_OUT_BT_TEST_LOAD = 0;

    	/*This event came from a CAN request and requires an acknowledge flag setting to be returned later*/
    	this->eventAcknowledge = REPLY_STATUS_OKAY;

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 2 actions for BAT_TEST                         */
/******************************************************/
lu_bool_t BAT_TEST_StateExitTo_NO_LV(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    /*If Test ends for this reason its an abort - don't use precious charge when we cant recharge*/
    if(batteryTestEndReason == BATT_TEST_STOP_LV_LOST )
    {
    	/*Change present active state*/
    	*(this->presActiveState)=NO_LV_StateFunc;

		/*CONTROL ACTIONS*/
		/*Switch off test load*/
		BAT_TEST_EN = CTRL_OFF;
		/* Reset the DAC to Zero */
		DAC_OUT_BT_TEST_LOAD = 0;

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}
/******************************************************/
/*EXIT 3 actions for BAT_TEST                         */
/******************************************************/
lu_bool_t BAT_TEST_StateExitTo_BAT_CHARGING_Test_End(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;	
	
    /*If Test is not running, return to the charging state */
    if (batteryTestEndReason != BATT_TEST_TEST_RUNNING)
    {
    	/*Change present active state*/
		*(this->presActiveState)=BAT_CHARGING_StateFunc;

		/*CONTROL ACTIONS*/
		/*Switch off test load*/
		BAT_TEST_EN = CTRL_OFF;
		/* Reset the DAC to Zero */
		DAC_OUT_BT_TEST_LOAD = 0;

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;

    }
    return retVal;
}

/******************************************************/
/******************************************************/
/* DEEP_DISCHARGE_ state function definitions     */
/******************************************************/
/******************************************************/
static void DEEP_DISCHARGE_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
    /*Check if at entry and if so execute entry code */
	if(*(this->presActiveState) != *(this->prevActiveState))
    {
		DEEP_DISCHARGE_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }	
    /*Execute any unconditional code */
    DEEP_DISCHARGE_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(DEEP_DISCHARGE_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/*ENTRY actions for DEEP_DISCHARGE                          */
/******************************************************/
static void DEEP_DISCHARGE_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	VIRT_BT_MGR_FSM_STATE = DEEP_DISCHARGE_STATE;

	fsmTimerSet(&batteryDisconnectTimer,TIME_TO_BATTERY_DISCONNECT);
}
/******************************************************/
/*UNCONDITIONAL actions for AT_DEEP_DISCHARGE         */
/******************************************************/
static void DEEP_DISCHARGE_StateUncondFunc(fsm *this)
{
	lu_int32_t iBatteryDrain;
	static lu_bool_t  WE_ARE_ALIVE_AFTER_DISCONNECT = LU_FALSE;

	/*Publish the battery voltage*/
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;
	/*Publish - we are beyond shutdown approach...we are about to shutdown*/
	BT_SHUTDOWN_APPROACHING = LU_FALSE;
	VIRT_BT_CH_SHUTDOWN = LU_TRUE;

	/*Every new entry into this state we need to clear the WE_ARE_ALIVE_AFTER_DISCONNECT flag*/
	if(*(this->presActiveStateSecsCounter) == 0)
	{
		WE_ARE_ALIVE_AFTER_DISCONNECT = LU_FALSE;
	}

	/*Read back the the current into the RTU. (in this state its supplied from battery)*/
	iBatteryDrain = MAIN_DC_I_SENSE;
	/*Publish RTU current as the battery drain current i.e. battery is supplying the RTU*/
	VIRT_BT_BATTERY_CURRENT = iBatteryDrain;
	
	/*Check to see if good battery has appeared*/	
	if(*(this->event) == TimerTickEvent)
    {
		/*Check if the disconnect timer has timed out*/
		if( fsmTimerGet(&batteryDisconnectTimer)==FSM_TIMER_TIMED_OUT )
		{
			if(WE_ARE_ALIVE_AFTER_DISCONNECT == LU_FALSE)
			{
				/**************************************************************************/
				/*THIS COULD BE THE END...OUR BATTERY IS IN DEEP DISCHARGE AND            */
				/*WE HAVE NO MEANS TO CHARGE IT BECAUSE WE HAVE NO  OR INSUFFICIENT LV.   */
				/*WE HAVE TO PROTECT THE BATTERY (due to its status no charging available)*/
				/*IF THERE IS NO LV THE BATTERY IS THE ONLY THING KEEPING THE RTU UP.     */
				/*WHEN WE TRIGGER THE DISCONNECT NOW WE WILL POWER DOWN THE ENTIRE SYSTEM */
				/*IF WE ARE HERE BECAUSE THE LV IS INSUFFICIENT TO DRIVE THE CHARGER IT IS*/
				/*POSSIBLE IT MIGHT BE ABLE TO KEEP THE SYSTEM ALIVE*/
				/**************************************************************************/
				BAT_DISCONNECT_UPDATE = LU_TRUE;
				/*Set for the case where LV is sufficient to keep system alive but cannot drive the charger*/
				WE_ARE_ALIVE_AFTER_DISCONNECT = LU_TRUE;/*OBVIOUSLY NOT SEEN IF WE ARE DEAD*/
				/*AND IT ALL WENT QUIET....or did it ?*/
			}
			else
			{
				/*Here because though we disconnected the battery the LV had sufficient power to keep us alive  */
				/*In this corner case we cannot allow the system to appear to be stuck in limbo...we have to    */
				/*keep checking battery status to see if anyone has replaced the battery or fixed the LV problem*/
				if( (*(this->presActiveStateSecsCounter)&0x7) < 4 )
				{
					/*Reconnect battery on step 0*/
					if( (*(this->presActiveStateSecsCounter)&0x7) == 0)
					{
						BAT_CONNECT_UPDATE = LU_TRUE;
					}
					/*On step one we do nothing here...we just allow the call to battery health status to asses battery*/
					/*if it shows a battery capable of driving the system it should pass the exit function on this step*/

					/*Disconnect if we reach step 2 without exiting for a good battery*/
					if( (*(this->presActiveStateSecsCounter)&0x7) == 2)
					{
						BAT_CONNECT_UPDATE = LU_TRUE;
					}
				}
			}
		}
	}
}

/******************************************************/
/*EXIT 1 actions for DEEP_DISCHARGE               */
/******************************************************/
static lu_bool_t DEEP_DISCHARGE_StateExitTo_INITIALISE(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if( is_LV_Present() == LU_TRUE )
    {
    	*(this->presActiveState)=INITIALISE_StateFunc;
		/*CONTROL ACTIONS*/

		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
    }
    return retVal;
}


/******************************************************/
/******************************************************/
/* BAT_NOT_PRESENT_ state function definitions           */
/******************************************************/
/******************************************************/
static void BAT_NOT_PRESENT_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
    /*Check if at entry and if so execute entry code */
	if(*(this->presActiveState) != *(this->prevActiveState))
    {
		BAT_NOT_PRESENT_StateEntryFunc(this);
        *(this->prevActiveState) = *(this->presActiveState);
    }	
    /*Execute any unconditional code */
    BAT_NOT_PRESENT_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(BAT_MAN_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(BAT_NOT_PRESENT_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/*ENTRY actions for BAT_NOT_PRESENT                      */
/******************************************************/
static void BAT_NOT_PRESENT_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish this FSMs present active state*/
	VIRT_BT_MGR_FSM_STATE = BAT_NOT_PRESENT_STATE;

	/*Set virtual point to indicate battery gone */
	VIRT_BT_DISCONNECTED = FLAG_SET;
	/*This flag is now irrelevant and is cleared*/
	VIRT_BT_LOW = FLAG_CLR;
}
/******************************************************/
/*UNCONDITIONAL actions for BAT_NOT_PRESENT              */
/******************************************************/
static void BAT_NOT_PRESENT_StateUncondFunc(fsm *this)
{
	PARAM_NOT_USED(this);
    /*Keep trying to connect battery to the RTU in case this is why its voltage has disappeared*/
	BAT_CONNECT_UPDATE = LU_TRUE;
	/*Publish the battery voltage*/
	VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;
}
/******************************************************/
/*EXIT 1 actions for BAT_NOT_PRESENT                     */
/******************************************************/
static lu_bool_t BAT_NOT_PRESENT_StateExitTo_INITIALISE(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

	/*If there is a battery with a sensor detected in the periodic check...*/
    if( offChargeIsBatteryPresent() == LU_TRUE)
	{
		/*...move on to the init state*/
		*(this->presActiveState)=INITIALISE_StateFunc;
		/*CONTROL ACTIONS*/
		/*Set virtual point to indicate battery present */
		VIRT_BT_DISCONNECTED = FLAG_CLR;
		/*Report to caller we have found an exit*/
		retVal = LU_TRUE;
	}

    return retVal;
}
/******************************************************/

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/* TIMER FUNCTION DEFINITIONS                                                                         */
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/******************************************************/
/******************************************************/
/* INTERNAL FIXED STATE MACHINE TIMERS                */
/******************************************************/
/******************************************************/

/***************************************************************/
/*Decrements all internal running timers every timer tick event*/
/*to this FSM                                                  */
/***************************************************************/
static void fsmTimersUpdate(void)
{
	lu_uint32_t fsmTimersIndx = 0;
	/*Keep looping until declared timers are done*/
	while( fsmTimers_p[fsmTimersIndx] != NULL_FSM_TIMER )
	{
		/*If the contents of the indexed pointer to timer have not reached 0...*/
		if( *fsmTimers_p[fsmTimersIndx] > 0 )
		{
			/*...then decrement the contents*/
			(*fsmTimers_p[fsmTimersIndx])--;
		}
		fsmTimersIndx++;
	}
}

static void fsmTimerSet( lu_int32_t* timerName, lu_int32_t timerValue)
{
	/*Store the timer value as the contents of the timer pointed to*/
	*timerName = timerValue;
}

static lu_int32_t fsmTimerGet( lu_int32_t* timerName )
{
	/*Store the timer value as the contents of the timer pointed to*/
	return  *timerName;
}

void fsmBatCANTestDurationAndCapacity( lu_int32_t timerValue, lu_int32_t capacity)
{
	canBatTestRqstTimeValue = timerValue;
	canBatTestRqstCapacityValue = capacity;
}


/*****************************************************************/
/* ADD MORE FUNCTION DEFINITIONS FOR ADDITIONAL STATES BELOW HERE*/
/* Remember each State must have an entry function, and          */
/* unconditional function a number of exit condition functions   */
/* and these placed in an array                                  */
/*****************************************************************/




/***********************************/
/*Add further support function here*/
/***********************************/

/*
 * Test for battery present/not present, this function is only valid whilst off charge.
 * 	return LU_TRUE if present else return LU_FALSE.
 */
static lu_bool_t offChargeIsBatteryPresent( void )
{
	lu_bool_t retVal = LU_FALSE;

    /*Evaluate the battery voltage against the present threshold */
	if(	BAT_V_SENSE > BATTERY_PRESENT )
	{
		retVal =  LU_TRUE;
	}

	return retVal;
}

static lu_bool_t isBatteryAboveDeepDischarge(void)
{
	lu_bool_t retVal = LU_FALSE;

    /*Evaluate battery voltage against the deep discharge threshold */
	if( BAT_V_SENSE > batteryParamsPtr->BatteryPackDeepDischargeVolts )
	{
		retVal =  LU_TRUE;
	}

	return retVal;
}

/******************************************************/
/******************************************************/


static lu_bool_t is_LvMotorDriveAvailable( void )
{
	lu_bool_t retValue = LU_TRUE;

//SRM
//	Find out if LV Motor Drive available
	return retValue;
}

/********************************************/
/*Called at entry to BATTERY TEST state     */
/********************************************/
static lu_int32_t startBatteryTest(BatteryTestStateStr *batTestStatePtr)
{
	lu_int32_t vBatteryStartTestNoLoad;
	lu_int32_t retValueEndReason;

	/*Reset all relevant battery test structure elements */
	batTestStatePtr->BatteryVoltage			  = 0;											/* Current battery voltage level 						*/
	batTestStatePtr->BatteryStartVoltage	  = 0;											/* Starting battery voltage level 						*/
	batTestStatePtr->BatteryEndVoltage		  = 0;											/* Finishing battery voltage level 						*/
	batTestStatePtr->BatteryDischargeRate	  = 0;											/* Discharge current via dummy load           			*/
	batTestStatePtr->BatteryTestTime		  = 0;											/* Time duration of current battery test				*/
	batTestStatePtr->BatteryTotalDrainCurrent = 0 ;											/* Running total of battery drain i.e. current * time	*/
	batTestStatePtr->BatteryTestThreshold	  = batteryParamsPtr->BatteryTestThresholdVolts;/* Voltage at which test fails					*/
	batTestStatePtr->BatteryTestStartTime	  = 0;											/* Time at which battery test was started				*/

	/*Initialise and publish the total discharge statistic for this test run*/
	VIRT_BATTERY_TEST_TOTAL_DISCHARGE = 0;

	/*Battery can not be fully charged once in this state*/
	VIRT_BT_FULLY_CHARGED = FLAG_CLR;

	/* Clear the last end reason */
	VIRT_BT_TEST_END_REASON = 0;

	/* KMG Clear this here for now, but maybe only should clear on a test pass */
	VIRT_BT_TEST_HARDWARE_FAULT = LU_FALSE;

	/*Read back start volts before load*/
	vBatteryStartTestNoLoad = BAT_V_SENSE;
	/*Output isolated battery volts on the filtered battery volts channel*/
	VIRT_BATTERY_VOLTAGE = vBatteryStartTestNoLoad;
	/*Store the battery voltage with no charger and before any test load applied*/
	batTestStatePtr->BatteryStartVoltage = vBatteryStartTestNoLoad;

	/**********************************************************/
	/* INITIALISE TEST DATA AND ALARMS FOR THIS NEW RUN */
	/**********************************************************/
	/*Reset low current alarm if that is what the last fail was caused by */

		SysAlarmSetLogEvent(LU_FALSE,
							SYS_ALARM_SEVERITY_INFO,
							SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
							SYSALC_BATTERY_CHARGER_BATT_TEST_LOW_CURRENT,
							0
					   	   );

	retValueEndReason                      = BATT_TEST_TEST_RUNNING;
	batTestStatePtr->BatteryTestEndReason  = BATT_TEST_TEST_RUNNING;
	batTestStatePtr->BatteryTestInProgress = LU_TRUE;
	batTestStatePtr->BatteryTestCancelled  = LU_FALSE;

	/*We are starting the test so publish */
	VIRT_BT_TEST_FAIL = LU_FALSE;
	VIRT_BT_TEST_PASS = LU_FALSE;

	/*Return test up and running or reason for ending*/
	return retValueEndReason;
}


/************************************************/
/*Called to find current load level every second*/
/************************************************/
static lu_uint16_t getRequiredCurrentLoadAtTime(lu_uint16_t presTime, const LnInterpTable1DU16U16Str *interpTable, lu_uint8_t numberOfInflections)
{
	lu_uint8_t inflectionIndex=0;;
    lu_uint16_t returnLoadCurrent=0;
    lu_int32_t interpLoadDelta=0;
    lu_int32_t interpTimeDelta=0;
    lu_int32_t presTimeDelta=0;
    lu_int32_t LoadCurrent=0;

    /*If present Time passed in is greater than the time of the last "Current Load vs time" inflection point*/
    if(presTime > interpTable[numberOfInflections-1].distributionX)
    {
		/*just return the last inflection point's load level*/
		returnLoadCurrent = interpTable[numberOfInflections-1].interpolateToX;
    }
    else
    {

		while(inflectionIndex <= numberOfInflections-1)
		{
			/*Find the "load_current vs time" profile inflection points our present time lies between*/
			/* present time into test profile                                                        */
			/*  |                                          time value of scanned inflection          */
			/*  |                                            |                                       */
			/*  V                                            V                                       */
			if(presTime >= interpTable[inflectionIndex].distributionX)
			{
				/*Interpolate a value for load current based on the inflection points that bound current time*/
				if(presTime == interpTable[inflectionIndex].distributionX)
				{
					/*No need to interpolate here - present time landed on an inflection point's time*/
					returnLoadCurrent = interpTable[inflectionIndex].interpolateToX;
				}
				else
				{
					/*Need to interpolate when present time does not land on an inflection point's time*/
					/*Get the change in load current between our interpolation points...can be negative or positive*/
					interpLoadDelta = interpTable[inflectionIndex].interpolateToX - interpTable[inflectionIndex+1].interpolateToX;
					/*Get the change in inflection point times between our interpolation points...will always be positive */
					interpTimeDelta = interpTable[inflectionIndex].distributionX - interpTable[inflectionIndex+1].distributionX;
					/*Get the change in "present time" from "inflection point 1 time" i.e. how far into this time delta we are...will always be positive*/
					presTimeDelta   = presTime - interpTable[inflectionIndex].distributionX;
					/*Calculate an interpolated load current...should always be positive*/
					LoadCurrent = interpTable[inflectionIndex].interpolateToX +
										interpLoadDelta * presTimeDelta/interpTimeDelta;
					/*return the calculated load current as an absolute unsigned 16 bit value...should always be positive */
					returnLoadCurrent = (lu_uint16_t)LoadCurrent;
				}
			}
			/*Scan to next "load current/time" profile inflection point*/
			inflectionIndex++;
		}
    }
    return returnLoadCurrent;
}

/******************************************************/
/*Called every timer tick whilst in BATTERY TEST state*/
/*Controls the test by updating the test load current */
/*every timer tick and monitoring the battery voltage  */
/*and drain current all the way through               */
static lu_uint8_t monitorBatteryTest(fsm *this, BatteryTestStateStr *batTestStatePtr )
{
	lu_int32_t  tBatteryMeasured;
	lu_int32_t  vBatteryUnderTest=0;
	lu_uint16_t iBatteryRqstDrain=0;
	lu_uint16_t iBatteryMeasDrain=0;
	lu_uint8_t  retValueEndReason = BATT_TEST_TEST_RUNNING;
	static lu_uint16_t noLoadCurrentCount, lowBatteryCurrentCount;


	/*****************************************************************************************/
	/*Test Monitoring - monitor the Battery's voltage level and Battery's current drain level*/
	/*****************************************************************************************/
	/*Read back battery volts under test*/
	vBatteryUnderTest = BAT_V_SENSE;
	/*Output isolated battery volts on the filtered battery volts channel*/
	VIRT_BATTERY_VOLTAGE = vBatteryUnderTest;

	/*Update the Total drain current during this test                          */
	/*Get the drain current at this point in time */
	batTestStatePtr->BatteryDischargeRate = BAT_TEST_I_SENSE;
	/*Record the drain current*/
	iBatteryMeasDrain = batTestStatePtr->BatteryDischargeRate;
	/*Read the battery's temperature*/
	tBatteryMeasured = BAT_TEMP;
	tBatteryMeasured /= MILLIDEGREES_TO_DEGREES;

	/*Evaluate potential test end reasons*/
	if( *(this->presActiveStateSecsCounter) == TEST_START_DELAY )
	{
		if(vBatteryUnderTest < batteryParamsPtr->BatteryMinStartVoltsForTestRqst)
		{
			/*We are just about to abort the test because the battery is required by the motor supply*/
			retValueEndReason = BATT_TEST_STOP_ISO_VOLTS_TOO_LOW;
			batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_ISO_VOLTS_TOO_LOW;
			batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
			batTestStatePtr->BatteryTestInProgress = LU_FALSE;
			batTestStatePtr->BatteryTestCancelled  = LU_TRUE;
		}
		else
		{
			/*If off load volts are ok switch on the load*/
			BAT_TEST_EN = CTRL_ON;
			/* Ensure the DAC is Zero at start of the test */
			DAC_OUT_BT_TEST_LOAD = 0;
			/* Zero the counters used for consecutive test fails */
			noLoadCurrentCount = 0;
			lowBatteryCurrentCount = 0;
		}
	}
	else if( *(this->presActiveStateSecsCounter) > TEST_START_DELAY )
	{
		/********************************************************************/
		/*Test Load Control- updates the Battery's Load Current every second*/
		/********************************************************************/
		/*We get here every second from the Battery Test state's unconditional call and we update the test load current*/
		/*  according to the "current load vs time" test profile. we do this to mimic load types*/
		switch (batteryParamsPtr->BatteryTestLoadType)
		{
			/*Choose the current load vs time test profile*/

			case BATT_LOAD_TYPE_10AMP_PTC_R:
				/*Use time in this state to look up the present test load current*/
				/* The test start delay needs to be taken from the SecCounter correctly */
				iBatteryRqstDrain = getRequiredCurrentLoadAtTime(((lu_uint16_t)*(this->presActiveStateSecsCounter) - (TEST_START_DELAY + 1)),
																 BattTestPtcHighTable1dU16U16,
																 MAX_BATT_TEST_HIGH_INFLECTION_POINTS );
				break;
			case BATT_LOAD_TYPE_2AMP_PTC:
			default:
				/*Use time in this state to look up the present test load current*/
				/* The test start delay needs to be taken from the SecCounter correctly */
				iBatteryRqstDrain = getRequiredCurrentLoadAtTime(((lu_uint16_t)*(this->presActiveStateSecsCounter) - (TEST_START_DELAY +1)),
																 BattTestLowTable1dU16U16,
																 MAX_BATT_TEST_LOW_INFLECTION_POINTS );
				break;
		}
		/*Set the actual battery drain current to the required drain current as looked up above */
		DAC_OUT_BT_TEST_LOAD = iBatteryRqstDrain;

		/*Calculate the drain current since last the measurement and add it to the total.*/
		/*For us:                timeDiff = 1 */
		/*Therefore:             current  = timeDiff * drainCurrent  i.e. current = drainCurrent */
		/*Calc current in mAsec: current  = current(in mA)           i.e. current = drainCurrent(already in mA)*/
		/*                                                           i.e. current = batTestStatePtr->BatteryDischargeRate/1000 */
		/*Add the number of mAseconds to the total discharge for battery test. */
		batTestStatePtr->BatteryTotalDrainCurrent += batTestStatePtr->BatteryDischargeRate;
		/*Update channel */
		VIRT_BATTERY_TEST_TOTAL_DISCHARGE = (batTestStatePtr->BatteryTotalDrainCurrent/SECONDS_PER_HOUR);

		/* Reset the accumulated fault counts if the battery current rises above the appropriate thresholds */
		if (iBatteryMeasDrain > NO_BAT_DETECT_CURRENT)
		{
			noLoadCurrentCount = 0;
		}

		if (iBatteryMeasDrain > (lu_int32_t)(batteryParamsPtr->BatteryTestCurrentMinimum))
		{
			lowBatteryCurrentCount = 0;
		}

		/* If the load Current is too high we have a problem */
		if( iBatteryMeasDrain > BATT_TEST_OVERLOAD_CURRENT )
		{
			/*Define the reason for failing the battery test*/
			retValueEndReason                      = BATT_TEST_STOP_OVERLOAD_CURRENT;
			batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_OVERLOAD_CURRENT;
			VIRT_BT_TEST_HARDWARE_FAULT = LU_TRUE;
			batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
			batTestStatePtr->BatteryTestInProgress = LU_FALSE;
			batTestStatePtr->BatteryTestCancelled  = LU_TRUE;
		}
		/* Allow 1 Second for the load current to stabilise before testing */
		else if((  *(this->presActiveStateSecsCounter) >  (TEST_START_DELAY+TIME_VALUE_1_SECS)) &&
				(  NO_BAT_DETECT_CURRENT > iBatteryMeasDrain ))
		{
			/* If there are consecutive low load current readings, end the test */
			if(++noLoadCurrentCount >= 3)
			{
				/*Define the reason for failing the battery test*/
				retValueEndReason                      = BATT_TEST_STOP_LOAD_NOT_CONNECTED;
				batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LOAD_NOT_CONNECTED;
				VIRT_BT_TEST_HARDWARE_FAULT = LU_TRUE;
				batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
				batTestStatePtr->BatteryTestInProgress = LU_FALSE;
				batTestStatePtr->BatteryTestCancelled  = LU_TRUE;
			}
		}
		else if( (  *(this->presActiveStateSecsCounter)                       >  (TEST_START_DELAY+TIME_VALUE_5_SECS) ) &&
			(  (lu_int32_t)(batteryParamsPtr->BatteryTestCurrentMinimum) >  iBatteryMeasDrain )    )
		{
			/* If there are consecutive low battery current readings, end the test */
			if(++lowBatteryCurrentCount >= 3)
			{
				/*Define the reason for failing the battery test*/
				retValueEndReason                      = BATT_TEST_STOP_LOW_CURRENT;
				batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LOW_CURRENT;
				batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
				batTestStatePtr->BatteryTestInProgress = LU_FALSE;
				batTestStatePtr->BatteryTestCancelled  = LU_TRUE;

				/*Log System Event */
				SysAlarmSetLogEvent(LU_TRUE,
									SYS_ALARM_SEVERITY_INFO,
									SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
									SYSALC_BATTERY_CHARGER_BATT_TEST_LOW_CURRENT,
									0 );
			}
		}
		/*We are just about to fail the test because we are below the test voltage threshold*/
		else if(vBatteryUnderTest < batteryParamsPtr->BatteryTestThresholdVolts)
		{
			/*Define the reason for failing the battery test*/
			retValueEndReason                      = BATT_TEST_STOP_LOW_VOLTAGE;
			batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LOW_VOLTAGE;
			batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
			batTestStatePtr->BatteryTestInProgress = LU_FALSE;
			batTestStatePtr->BatteryTestCancelled  = LU_TRUE;
		}
		/*We are just about to pass the test because we have reached the maximum discharge without error*/
		else if( (batTestStatePtr->BatteryTotalDrainCurrent) >
				  ((batteryParamsPtr->BatteryNomCapacity * SECONDS_PER_HOUR) * batteryParamsPtr->BatteryTestDischargeCapacity)/100 )
		{
			/*Define the reason for passing the battery test*/
			retValueEndReason                      = BATT_TEST_STOP_MAX_DISCHARGE;
			batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_MAX_DISCHARGE;
			batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
			batTestStatePtr->BatteryTestInProgress = LU_FALSE;
			batTestStatePtr->BatteryTestCancelled  = LU_FALSE;
		}
		/*We are just about to pass the test because we have reached a time without error*/
		else if(*(this->event) == TimerTickEvent)
		{
			/*if timed out we reached full test time without jumping out early for an error*/
			if(fsmTimerGet( &batTestDurationTimer)==FSM_TIMER_TIMED_OUT)
			{
				retValueEndReason                      = BATT_TEST_STOP_PASSED;
				batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_PASSED;
				batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
				batTestStatePtr->BatteryTestInProgress = LU_FALSE;
				batTestStatePtr->BatteryTestCancelled  = LU_FALSE;
			}

			/*Check NVRam for a max duration override test time*/
			if( (batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes > 0) &&
				fsmTimerGet( &batMaxTestDurationTimer) == FSM_TIMER_TIMED_OUT )
			{
				retValueEndReason                      = BATT_TEST_STOP_MAX_DURATION;
				batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_MAX_DURATION;
				batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
				batTestStatePtr->BatteryTestInProgress = LU_FALSE;
				batTestStatePtr->BatteryTestCancelled  = LU_FALSE;
			}
		}
	}

	if(*(this->event) == MotorSupplyOnEvent)
	{
		DEPLOY_ALT_LV_MOTOR_SUPPLY = LU_FALSE;

		/*We can use the battery to supply motor operation power*/
		MOTOR_SUPPLY_RQST = LU_TRUE;
		/*We are just about to abort the test because the battery is required by the motor supply*/
		retValueEndReason                      = BATT_TEST_STOP_OPERATION;
		batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_OPERATION;
		batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
		batTestStatePtr->BatteryTestInProgress = LU_FALSE;
		batTestStatePtr->BatteryTestCancelled  = LU_TRUE;

		}
	else if(*(this->event) == BatteryTestCancelEvent)
	{
		/*We are just about to abort the test because we have been requested to*/
		retValueEndReason                      = BATT_TEST_STOP_CANCELED;
		batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_CANCELED;
		batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
		batTestStatePtr->BatteryTestInProgress = LU_FALSE;
		batTestStatePtr->BatteryTestCancelled  = LU_TRUE;
	}
	/*We are just about to abort the test because we must have a LV supply*/
	else if( is_LV_Present() != LU_TRUE )
	{
		retValueEndReason                  	   = BATT_TEST_STOP_LV_LOST;
		batTestStatePtr->BatteryTestEndReason  = BATT_TEST_STOP_LV_LOST;
		batTestStatePtr->BatteryEndVoltage     = vBatteryUnderTest;
		batTestStatePtr->BatteryTestInProgress = LU_FALSE;
		batTestStatePtr->BatteryTestCancelled  = LU_TRUE;
	}

	/* Update the virtual points and channel information */
	/* Are we running ? */
	if(retValueEndReason == BATT_TEST_TEST_RUNNING)
	{
		VIRT_BT_TEST_IN_PROGRESS = FLAG_SET;
		VIRT_BT_TEST_PASS_OR_RUN = VIRT_BT_TEST_IN_PROGRESS;
		VIRT_BT_TEST_FAIL_OR_RUN = VIRT_BT_TEST_IN_PROGRESS;
	}
	else /* if not, why not */
	{
		VIRT_BT_TEST_IN_PROGRESS = FLAG_CLR;
		VIRT_BT_TEST_END_REASON = retValueEndReason;

		/* PASS Conditions */
		if ((retValueEndReason == BATT_TEST_STOP_MAX_DISCHARGE) ||
			(retValueEndReason == BATT_TEST_STOP_PASSED) ||
			(retValueEndReason == BATT_TEST_STOP_MAX_DURATION))
		{
			VIRT_BT_TEST_PASS = FLAG_SET;
			VIRT_BT_TEST_FAIL = FLAG_CLR;
		}
		/* FAIL Conditions */
		else if	((retValueEndReason == BATT_TEST_STOP_LOW_CURRENT) ||
			(retValueEndReason == BATT_TEST_STOP_LOAD_NOT_CONNECTED) ||
			(retValueEndReason == BATT_TEST_STOP_LOW_VOLTAGE) ||
			(retValueEndReason == BATT_TEST_STOP_OVERLOAD_CURRENT))
		{
			VIRT_BT_TEST_FAIL = FLAG_SET;
			VIRT_BT_TEST_PASS = FLAG_CLR;
		}
		/* ABORT Conditions */
		/*
		 * ABORT nothing should be updated
		*/

		/*Ensure the timer has been stopped */
		fsmTimerSet(&batTestDurationTimer,FSM_TIMER_TIMED_OUT);
	}

	/* Publish the end reason */
	if(VIRT_BT_TEST_FAIL == LU_TRUE || VIRT_BT_TEST_IN_PROGRESS == LU_TRUE)
	{
		VIRT_BT_TEST_FAIL_OR_RUN = LU_TRUE;
	}
	else
	{
		VIRT_BT_TEST_FAIL_OR_RUN = LU_FALSE;
	}
	if(VIRT_BT_TEST_PASS == LU_TRUE || VIRT_BT_TEST_IN_PROGRESS == LU_TRUE)
	{
		VIRT_BT_TEST_PASS_OR_RUN = LU_TRUE;
	}
	else
	{
		VIRT_BT_TEST_PASS_OR_RUN = LU_FALSE;
	}

	return retValueEndReason;
}

lu_bool_t setChargerTemperatureDisable(lu_bool_t tempStopCharge)
{
	chrgTemperatureDisable = tempStopCharge;

	return chrgTemperatureDisable;
}

void setBatteryTestSchedule(lu_int32_t Repeat,lu_int32_t Duration)
{
    /*Initial setup of the battery's test repeat time ...used to reload the actual test repeat timer*/
	batteryTestScheduleInSecs = Repeat * SECONDS_PER_HOUR;
	/*Don't forget to put the re-charger variable value into the actual timer right now.*/
	fsmTimerSet(&batTestRepeatTimer,(lu_int32_t)(batteryTestScheduleInSecs));
	/*Initial setup of the battery's test duration time ...used to reload the actual test duration timer*/
	/*this is used to reload the actual duration timer before executing the test*/
	batteryTestDurationInSecs = Duration;
}
