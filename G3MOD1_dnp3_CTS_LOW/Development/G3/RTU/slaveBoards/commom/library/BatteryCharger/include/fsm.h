/*****************************************************************************************************************************************
Description of this Finite State Machine (FSM) as in original template :
Finite State Machines are a collection of distinct "states" or "modes of operation". There can only ever be one active state at a time.
Transitioning from the active state to the next active state is determined by the exits defined in the active state in terms of events
or commands recieved and prevailing conditions such as flags and variables etc.
This FSM template caters for a number of system events such as a regular timer event or some CAN command events etc.
This FSM template caters for a state to execute code exclusively at entry to the state where it will ignore transitions back to itself.
This FSM template caters for the execution of code on every contiguous call to the same active state.
This FSM template caters for the testing of a number of possible exits where upon it will execute exit actions for the particular exit.
This FSM is always called via a function pointer that points to the present active state function.


FSM function pointer                                          <-- Called for specified system events and updated to point to a state 
      |                                                           function (becomming the active state) by a condition function when
	  |                                                           it tests for a validates the condition it is looking for.
	  |                                
	  |_____state 1 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 1 entry action code        <-- Called exclusively on first entry to state 1 function
	  |             |
	  |             |_______state 1 unconditional code       <-- Called for every event call to FSM whilst in state 1
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 1 condition 1 function     <--| All condition functions get called until one returns "true".
	  |             |_______state 1 condition 2 function     <--| If "true" it changes active state ie function pointed to by "FSM function pointer"
	  |             |                                        <--| to point to new active state and executes any exit actions. The next event to call
	  |             |_______state 1 condition m function     <--| "FSM function pointer" will call the new active state function.
	  |
	  |
	  |_____state 2 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 2 entry action code        <-- Called exclusively on first entry to state 2 function
	  |             |
	  |             |_______state 2 unconditional code       <-- Called for every event call to FSM whilst in state 2
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 2 condition 1 function
	  |             |_______state 2 condition 2 function
	  |             |
	  |             |_______state 2 condition m function
	  |
	  |_____state n function

The original template caters for 6 state functions each of which has 3 exit condition functions

******************************************************************************************************************************************/
#ifndef FSM_H
#define FSM_H
//typedef enum { false, true } bool;

/* Common FSM Timouts */
#define TIMEOUT_IN_60_SECS  60
#define TIMEOUT_IN_30_SECS  30
#define FSM_TIMER_TIMED_OUT 0

#define NULL_FSM_TIMER (lu_int32_t*)(0)

/*Add new event enum*/
typedef lu_uint8_t EventID;

/*Partially declare fsmTag struct here to satisfy typedefs*/
struct fsmTag;
/*State function pointer typedefs*/
typedef lu_bool_t (*fsmFunc)(EventID ev);
typedef void (*stateFunc)(struct fsmTag *fsm);
typedef void (*entryFunc)(struct fsmTag *fsm);
typedef void (*unconditionalFunc)(struct fsmTag *fsm);
typedef lu_bool_t (*exitConditionFuncs)(struct fsmTag *fsm);

/*State structure typedefs*/
typedef struct stateTag
{
    /*this state's function pointer*/
    stateFunc          stateCall;
    /*this state's entry action function pointer*/
    entryFunc          entryAction;
    /*this state's unconditional actions function pointer*/
    unconditionalFunc  uncondAction;
    /*this state's exit actions function pointer list*/    
    exitConditionFuncs *listOfExitConditionFuncs;

    
}state;

/*Finite State Machine struct typedef*/
typedef struct fsmTag
{
    EventID     *event;

    stateFunc    *presActiveState;
    stateFunc    *prevActiveState;
    state        **statesList;
    lu_uint32_t  *presActiveStateSecsCounter;
    REPLY_STATUS eventAcknowledge;
}fsm;

#endif
