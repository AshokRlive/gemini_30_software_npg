/*
 * NiMHChargerFSM.h
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */

#ifndef NIMHCHARGERFSM_H_
#define NIMHCHARGERFSM_H_

/*NiMHChargerFSM is a sub Finite State Machine (FSM) of the Battery Manager FSM.*/
/*It takes events defined for and passed into the Battery FSM to process further */
#include "BatManagerFSM.h"
#include "BatteryChargerTopLevel.h"

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocolEnum.h"
/*Include all the typedefs for data and function pointers to be used in an FSM*/
#include "fsm.h"

#define NiMH_CHECK_CHARGER_STATE     0
#define NiMH_FAST_CHARGE_STATE       1
#define NiMH_CHARGE_SUSPENDED_STATE  2
#define NiMH_MAINTAIN_CHARGE_STATE   3
#define NiMH_RECOVER_BATTERY_STATE   4

#define NIMH_CHARGER_MIN_V_LIM 30500     //31500
#define NIMH_CHARGER_MAX_V_LIM 31500     //32500
#define NIMH_CHARGING_VOLTS    31000     //32000

#define CHARGER_MAINTAIN_MIN 26000
#define BATTERY_PRESENT_THRESHOLD 5000
#define MAX_CHARGER_ABILITY 2300
#define TEN_MINUTES_COUNT_SECONDS 600
#define BATTERY_MAINTAIN_THRESHOLD 25800

#define FAST_DETECT_SLOPE 400
//#define FAST_DETECT_SLOPE 684

#define FAST_CHARGE_DETECT_THRESHOLD 7
#define MAX_FILTER_COUNT FAST_CHARGE_DETECT_THRESHOLD+2
#define FAST_CHARGE_THERMAL_CUTOFF 70000
#define CHARGE_THERMAL_CUTOFF 85000

/* To prevent fully charged being set early, there is a mask timer when entering the float state */
#define FULL_CHARGE_DELAY_TIME	300

/*The over all period for which we charge and rest*/
#define BATTERY_CHARGE_REST_AND_DETECT_REPEAT_PERIOD 18
/*The period for which we actually charge*/
#define BATTERY_CHARGE_PERIOD_END   3

/*Explicit initialisation requests for thermal signal filter*/
#define EXPLICIT_INITIALISE LU_TRUE
#define NO_INITIALISE LU_FALSE

/*Main event handler for this FSM*/
extern REPLY_STATUS CHRG_NICKEL_METAL_FSM_MAIN(EventID eventIn);
/*Access function to read battery analyser's evaluation of battery as fatal or not*/
extern lu_bool_t getNiMHChargingLevelFsmBatAnalysis(void);

/*Access to allow the Battery Manager FSM to check if the battery is missing*/
extern lu_bool_t isNiMHBatteryMissing(void);
/*Read access function*/
extern lu_bool_t getNiMHScheduledTestAcknowledge(void);
/*Write access function*/
extern void setNiMHScheduledTestAcknowledge(lu_bool_t SchedTestOk);
/*Read access function*/
extern lu_bool_t getNiMHBatteryPresenceIndicator(void);

/*Access to write the chargers voltage directly and record for others to access*/
extern void setNiMHChargerVolts(lu_int32_t vCharger);
#endif /* NIMHCHARGERFSM_H_ */
