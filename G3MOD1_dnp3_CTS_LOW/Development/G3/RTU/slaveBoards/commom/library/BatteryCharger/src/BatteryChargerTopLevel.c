/*
 * BatteryChargerTopLevel.c
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */
/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Battery Charger module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*******************************************************************************/
/* INCLUDES - Standard Library Modules Used                                    */
/*******************************************************************************/



/*******************************************************************************/
/* INCLUDES - Project Specific Modules Used                                    */
/*******************************************************************************/
#include "BatteryChargerTopLevel.h"
#include "BatteryToFsmInterface.h"
#include "LeadAcidChargerFSM.h"
#include "BatManagerFSM.h"
#include "IOManager.h"
#include "NVRAMDefOptPSM.h"
#include "BoardIOMap.h"
#include "LinearInterpolation.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmBatteryChargerEnum.h"

/*******************************************************************************/
/* LOCAL - Definitions And Macros                                              */
/*******************************************************************************/
#define LED_FLASH_PERIOD_500MS	500

/*******************************************************************************/
/* LOCAL - Typedefs and Structures                                             */
/*******************************************************************************/


/*******************************************************************************/
/* LOCAL - Prototypes Of Local Functions                                       */
/*******************************************************************************/
SB_ERROR BatteryChargerSelect(BatterySelectStr *);
SB_ERROR BatteryChargerOperate(BatteryOperateStr *);
SB_ERROR BatteryChargerCancel(BatteryCancelStr *);

SB_ERROR BatteryChargerConfig(ChargerConfigStr *);

lu_uint8_t BatteryDisconnectSelectHandler(BatterySelectStr *);
lu_uint8_t BatteryDisconnectOperateHandler(BatteryOperateStr *);
lu_uint8_t BatteryDisconnectCancelHandler(BatteryCancelStr *);

lu_uint8_t BatteryTestSelectHandler(BatterySelectStr *);
lu_uint8_t BatteryTestOperateHandler(BatteryOperateStr *);
lu_uint8_t BatteryTestCancelHandler(BatteryCancelStr *);

lu_uint8_t ChargerTestSelectHandler(BatterySelectStr *);
lu_uint8_t ChargerTestOperateHandler(BatteryOperateStr *);
lu_uint8_t ChargerTestCancelHandler(BatteryCancelStr *);

lu_uint8_t ChargerInhibitSelectHandler(BatterySelectStr *);
lu_uint8_t ChargerInhibitOperateHandler(BatteryOperateStr *);
lu_uint8_t ChargerInhibitCancelHandler(BatteryCancelStr *);

lu_uint8_t BatteryResetStats(BatteryResetStr *);

/*******************************************************************************/
/* EXPORTED - Variables                                                        */
/*******************************************************************************/
lu_bool_t 			SwEventParamsInProgress;
lu_bool_t			batteryTestInProgress;
lu_bool_t			chargerTestInProgress;
lu_bool_t           updateDataNVRAM;
BatteryTestStateStr batTestState;
NVRAMOptPSMStr    	*NVRAMOptPSMPtr;
lu_uint32_t batteryTestScheduleTime;

lu_int32_t RAW_DC_V_SENSE;
lu_int32_t BAT_CHARGER_I_SENSE;
lu_int32_t BAT_TEST_I_SENSE;
lu_int32_t BAT_V_SENSE;
lu_int32_t CHARGER_V_SENSE;
lu_int32_t MAIN_DC_I_SENSE;
lu_int32_t MOT_I_SENSE;
lu_int32_t BAT_CHARGER_EN;
lu_int32_t BAT_TEMP;
SB_ERROR    BAT_TEMP_SENSOR_ERROR;
lu_int32_t BAT_TO_MOT_CONNECT;
lu_int32_t LOCAL_LINE;
lu_int32_t REMOTE_LINE;
lu_int32_t VIRT_BT_DISCONNECTED;
lu_int32_t VIRT_CH_FAULT;
lu_int32_t VIRT_CH_ON;
lu_int32_t VIRT_CH_SYSTEM_RUNNING;
lu_bool_t BAT_CONNECT_UPDATE;
lu_bool_t BAT_DISCONNECT_UPDATE;
lu_int32_t ACCUMLATED_DISCHARGE_TIME;
lu_int32_t ACCUMULATED_CHARGE_CURRENT;
lu_int32_t ACCUMULATED_DISCHARGE_CURRENT;
lu_int32_t BAT_CHARGER_CUR_LIM_SET;
lu_int32_t BAT_CHARGER_VOLT_SET;
lu_int32_t BAT_TEST_EN;
lu_int32_t BT_SHUTDOWN_APPROACHING;
lu_int32_t DAC_OUT_BT_TEST_LOAD;
lu_int32_t VIRT_AC_OFF;
lu_int32_t VIRT_BATTERY_TEST_TIME;
lu_int32_t VIRT_BATTERY_TEST_TOTAL_DISCHARGE;
lu_int32_t VIRT_BATTERY_VOLTAGE;
lu_int32_t VIRT_BT_ACCUMMULATED_CHARGE_TIME;
lu_int32_t VIRT_BT_BATTERY_CURRENT;
lu_int32_t VIRT_BT_CH_INHIBITED;
lu_int32_t VIRT_BT_CH_SHUTDOWN;
lu_int32_t VIRT_BT_CHARGE_CYCLES;
lu_int32_t VIRT_BT_COMMS_FAIL;
lu_int32_t VIRT_BT_DATA_NVRAM_FAIL;
lu_int32_t VIRT_BT_ESTIMATED_CAPACITY;
lu_int32_t VIRT_BT_FAULT;
lu_int32_t VIRT_BT_FULLY_CHARGED;
lu_int32_t VIRT_BT_CHARGE_CYCLE_TIME;
lu_int32_t VIRT_BT_CHARGE_CYCLE_CHARGE;
lu_int32_t VIRT_BT_LOW;
lu_int32_t VIRT_BT_OVER_TEMPERATURE;
lu_int32_t VIRT_BT_TEMP_FAIL;
lu_int32_t VIRT_BT_TEST_FAIL;
lu_int32_t VIRT_BT_TEST_IN_PROGRESS;
lu_int32_t VIRT_BT_TYPE_LEAD_ACID;
lu_int32_t VIRT_BT_TYPE_NIHM;
lu_int32_t VIRT_CH_CURRENT_FAULT;
lu_int32_t VIRT_CH_VOLTAGE_FAULT;
lu_int32_t VIRT_DRAIN_CURRENT;
lu_int32_t VIRT_ON_ELAPSED_TIME;
lu_int32_t VIRT_BT_TEST_HARDWARE_FAULT;
lu_int32_t VIRT_BT_TEST_PASS;
lu_int32_t VIRT_BT_TEST_FAIL_OR_RUN;
lu_int32_t VIRT_BT_TEST_PASS_OR_RUN;
lu_int32_t VIRT_CHARGER_EN;
lu_int32_t VIRT_CHARGER_CON;
lu_int32_t VIRT_BT_TYPE_DEFAULT;
lu_int32_t VIRT_CH_SET_VOLTAGE;
lu_int32_t VIRT_CH_SET_CURRENT;
lu_int32_t VIRT_BT_MGR_FSM_STATE;
lu_int32_t VIRT_BT_CHGR_FSM_STATE;
lu_int32_t VIRT_BT_TEST_END_REASON;
lu_bool_t VIRT_NiMH_FAST_CHRG_COMPLETE;
/*******************************************************************************/
/* LOCAL - Global Variables                                                    */
/*******************************************************************************/
static NVRAMUserPSMStr *nvramAppUserBlkPtr;

lu_uint32_t batteryDebugFaultOffset;

lu_uint32_t batteryDisconnectSelectTime;
lu_uint32_t batteryDisconnectSelectPeriod;
lu_uint32_t batteryDisconnectOperateTime;
lu_uint32_t batteryDisconnectOperatePeriod;
lu_bool_t  	batteryDisconnectSelected;
lu_bool_t  	batteryDisconnectOperate;

lu_bool_t   batteryTestScheduleStart = LU_FALSE;

lu_uint32_t batteryTestSelectTime;
lu_uint32_t batteryTestSelectPeriod;
lu_uint32_t batteryTestOperateTime;
lu_uint32_t batteryTestOperatePeriod;
lu_bool_t  	batteryTestSelected;
lu_bool_t  	batteryTestOperate;

lu_uint32_t chargerTestSelectTime;
lu_uint32_t chargerTestSelectPeriod;
lu_uint32_t chargerTestOperateTime;
lu_uint32_t chargerTestOperatePeriod;
lu_bool_t  	chargerTestSelected;
lu_bool_t  	chargerTestOperate;

lu_uint32_t chargerInhibitSelectTime;
lu_uint32_t chargerInhibitSelectPeriod;
lu_uint32_t chargerInhibitOperateTime;
lu_uint32_t chargerInhibitOperatePeriod;
lu_bool_t  	chargerInhibitSelected;
lu_bool_t  	chargerInhibitOperate;

/*******************************************************************************/
/* List of supported CAN message                                               */
/*******************************************************************************/
static const filterTableStr ChargerModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Battery Charger API commands */
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_SELECT_C     	, LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C     	, LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_BAT_CH_RESET_C 		, LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_BATTERY_CHARGER_C 	, LU_FALSE , 0      }
};


/******************************************************************************/
/******************************************************************************/
/* Exported Functions                                                         */
/******************************************************************************/
/******************************************************************************/


/**********************************************************************************************************/
/*Battery Charger Initialise                                                                              */
/**********************************************************************************************************/
SB_ERROR BatteryChargerInit(void)
{
	SB_ERROR sbError=SB_ERROR_NONE;

	/* Read NVRAM to determine...*/
	sbError = NVRamIndenityGetMemoryPointer(NVRAM_APP_BLK_OPTS, (lu_uint8_t **)&NVRAMOptPSMPtr);

	/*...if current control charging board is fitted and if so...*/
	if (sbError != SB_ERROR_NONE)
	{
		/*...switch the option on*/
		NVRAMOptPSMPtr->optionBitField  |= NVRAMOPTPSMDEF_CHARGERCURRENTCONTROLFITTED_BM;
	}

	/*We are expecting a config message to set a couple of battery option...*/
	sbError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_USER_A, (lu_uint8_t **)&nvramAppUserBlkPtr);
	/*...for now and if the NVRAM containing them is inaccessible...*/
	if (sbError != SB_ERROR_NONE)
	{
		/*...initialise them with default values until config message received and...*/
		nvramAppUserBlkPtr->batteryTestPeriodHours = 0;
		nvramAppUserBlkPtr->batteryTestDurationSecs = 0;

		/*...call the function to indicate NVRAM should be updated with these values */
		sbError = NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );
	}

	/*If the NVRAM is accessible and if we have a non zero value testing will be scheduled */
	/*With zero as the repeat test time we stop all scheduled testing*/
	setBatteryTestSchedule(nvramAppUserBlkPtr->batteryTestPeriodHours,
	                       nvramAppUserBlkPtr->batteryTestDurationSecs);

	/*Set up the CAN messages filter*/
	sbError = CANFramingAddFilter( ChargerModulefilterTable,SU_TABLE_SIZE(ChargerModulefilterTable, filterTableStr) );

    /*Set up the data variables the FSM depends on*/
	BatManagerFsmInit();

	return sbError;
}


/**********************************************************************************************************/
/*Battery Charger tick is a regular 1sec timed input event to allow the FSM to process timers and changing*/
/*conditions                                                                                              */
/**********************************************************************************************************/
SB_ERROR BatteryChargerTick(void)
{
	SB_ERROR RetVal = SB_ERROR_NONE;

/***************************************************************************/
/*Send the FSM its regular timer tick event now  -  here every second      */
/***************************************************************************/
	SendTimerTickEventToFSM();

/***************************************************************************/
/*Update any new NVRAM updates                                             */
/***************************************************************************/
	/* If the NVRAM data block has been changed then call the API function to update the NVRAM */
	if ( updateDataNVRAM == LU_TRUE)
	{
		NVRamBatteryDataWriteUpdate( NVRAM_BATT_DATA_BLK_USER_A );
		updateDataNVRAM = LU_FALSE;
	}
	return RetVal;
}

void upDateInputConditions(void)
{
	/*Publish the elapsed time - records how long the RTU has been running since last powered up*/
	IOManagerSetValue(IO_ID_VIRT_ON_ELAPSED_TIME, STGetRunningTimeSecs());

	IOManagerGetCalibratedUnitsValue(IO_ID_36V_RAW_DC_V_SENSE,     &RAW_DC_V_SENSE);
	IOManagerGetCalibratedUnitsValue(IO_ID_BAT_CHARGER_I_SENSE,    &BAT_CHARGER_I_SENSE);
	IOManagerGetCalibratedUnitsValue(IO_ID_BAT_TEST_I_SENSE,       &BAT_TEST_I_SENSE);
	IOManagerGetCalibratedUnitsValue(IO_ID_BAT_V_SENSE,            &BAT_V_SENSE);
	IOManagerGetCalibratedUnitsValue(IO_ID_CHARGER_V_SENSE,        &CHARGER_V_SENSE);
	IOManagerGetCalibratedUnitsValue(IO_ID_MAIN_DC_I_SENSE,        &MAIN_DC_I_SENSE);
	IOManagerGetCalibratedUnitsValue(IO_ID_MOT_I_SENSE,            &MOT_I_SENSE);

	IOManagerGetValue(IO_ID_BAT_CHARGER_EN,         &BAT_CHARGER_EN);

	BAT_TEMP_SENSOR_ERROR = IOManagerGetValue(IO_ID_BAT_TEMP,      &BAT_TEMP);
	if (BAT_TEMP_SENSOR_ERROR != SB_ERROR_NONE)
	{
		/*Temperature sensor faulty*/
		VIRT_BT_TEMP_FAIL = FLAG_SET;
		VIRT_BT_COMMS_FAIL = FLAG_SET;
		/*Convert our threshold back to milli degrees and assign to BAT_TEMP*/
		BAT_TEMP = batteryParamsPtr->BatteryMaxTemp*MILLIDEGREES_TO_DEGREES;
	}
	else
	{
		/*Temperature sensor all working*/
		VIRT_BT_TEMP_FAIL = FLAG_CLR;
		VIRT_BT_COMMS_FAIL = FLAG_CLR;
	}

	IOManagerGetValue(IO_ID_BAT_TO_MOT_CONNECT,     &BAT_TO_MOT_CONNECT);
	IOManagerGetValue(IO_ID_LOCAL_LINE,             &LOCAL_LINE);
	IOManagerGetValue(IO_ID_REMOTE_LINE,            &REMOTE_LINE);
	IOManagerGetValue(IO_ID_VIRT_BT_DISCONNECTED,   &VIRT_BT_DISCONNECTED);
	IOManagerGetValue(IO_ID_VIRT_CH_FAULT,          &VIRT_CH_FAULT);
	IOManagerGetValue(IO_ID_VIRT_CH_ON,             &VIRT_CH_ON);
	IOManagerGetValue(IO_ID_VIRT_CH_SYSTEM_RUNNING, &VIRT_CH_SYSTEM_RUNNING);
}


void upDateOutputConditions(void)
{
	/**********************************/
	/*CHARGER LED UPDATE - RED & GREEN*/
	/**********************************/
	/* If we are in the charging state set the charger LED, otherwise turn it off */
	if(VIRT_CH_SYSTEM_RUNNING == LU_TRUE)
	{
		/* If there is a charger fault, set the charger LED RED */
		if(VIRT_CH_FAULT == LU_TRUE)
		{
			/* Charger Fault */
			IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN,LED_OFF);
			IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_RED,LED_ON);
		}
		/* Otherwise if the charger is ON, set the LED Green */
		else if(VIRT_CH_ON == LU_TRUE)
		{
			IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_RED,LED_OFF);
			IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN,LED_ON);
		}
	}
	else
	{
		/* Charger OFF */
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_GREEN,LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_CHRG_RED,LED_OFF);
	}

	/***************************************************/
	/*BATTERY LED UPDATE -RED GREEN ORANGE and/or FLASH*/
	/***************************************************/
	/*BATTERY LED UPDATE -Battery present and healthy  */
	if(VIRT_BT_TEST_IN_PROGRESS == LU_TRUE)
	{
		/*FLASHING GREEN*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_FLASH_PERIOD_500MS);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	}
	/*As long as no BatteryFault reported from charger we just need to be above shutdown approach volts or actually charging...*/
	/*...to go solid green*/
	else if(
			 ( VIRT_BT_FAULT == LU_FALSE)
			 &&
			 (
				( BAT_V_SENSE >= batteryParamsPtr->BatteryPackShutdownLevelVolts )
				||
				( ( VIRT_CH_ON == LU_TRUE )
				  &&
				  ( BAT_V_SENSE > batteryParamsPtr->BatteryLowLevel )
				)
			 )
		   )
	{
		/*SOLID GREEN*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_ON);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	}
	/*If the battery is not present OR...*/
	/*...a BatteryFault has been reported AND the charger is on OR ...*/
	/*...a BatteryFault has been reported AND the charger is off AND the battery voltage is above deep discharge...*/
	/*...we go solid red with */
	else if(
			 ( BAT_V_SENSE < BATTERY_PRESENT )
			 ||
	         (
	        	(VIRT_BT_FAULT == LU_TRUE)
	        	&&
		        (BAT_V_SENSE > batteryParamsPtr->BatteryPackDeepDischargeVolts)
		        &&
		        (VIRT_CH_ON == LU_FALSE )
		     )
		     ||
             (
            	(VIRT_BT_FAULT == LU_TRUE)
            	&&
            	(VIRT_CH_ON == LU_TRUE )
	         )
		   )
	{
		/*SOLID RED*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_ON);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);

	}
	/*If the battery is present but its voltage is less than deep discharge it doesn't matter if we have a BatteryFault...*/
	/*...reported because below deep discharge is a more critical condition to report*/
	else if(
			 (BAT_V_SENSE >= BATTERY_PRESENT)
			 &&
	         (BAT_V_SENSE <= batteryParamsPtr->BatteryPackDeepDischargeVolts)
	         &&
	         (VIRT_CH_ON == LU_FALSE )
	       )
	{
		/*FLASHING RED*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	    IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_FLASH_PERIOD_500MS);
	}
	/*BATTERY LED UPDATE -Battery less than healthy and not receiving charge current  */
	else if(
			 (BAT_V_SENSE < batteryParamsPtr->BatteryPackShutdownLevelVolts)
			 &&
			 (BAT_V_SENSE > batteryParamsPtr->BatteryLowLevel)
			 &&
			 (VIRT_BT_FAULT == LU_FALSE)
			 &&
			 (VIRT_CH_ON == LU_FALSE )
		   )
	{
		/*SOLID ORANGE*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_ON);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_ON);
	    IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	}
	/*BATTERY LED UPDATE -Battery low and not being charged  */
	else if(
			 (BAT_V_SENSE <= batteryParamsPtr->BatteryLowLevel)
			 &&
			 (VIRT_BT_FAULT == LU_FALSE)
		   )
	{
		/*FLASHING ORANGE - note when flashing both LEDs for flashing orange...*/
		/*...one must start with both red and green leds in the same state*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	    IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_FLASH_PERIOD_500MS);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_FLASH_PERIOD_500MS);
	}
	/* BATTERY LED UPDATE -Default               */
	else
	{
		/*ALL OFF...should never happen while the PSM is up and running*/
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	    IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, LED_OFF);
		IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, LED_OFF);
	}

	/********************************************/
	/*LV SUPPLY LED UPDATE -RED GREEN ORANGE    */
	/********************************************/
	/*LV SUPPLY LED UPDATE -We consider this as having no LV AC. The charger will not function */
	if(RAW_DC_V_SENSE < NO_LV_DETECT_THRESHOLD )
	{
		/*SOLID RED*/
		IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, LED_OFF);
		IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_RED, LED_ON);
		/*This FLAG maps easily to the LEDs*/
		VIRT_AC_OFF = FLAG_SET;
	}
	/*LV SUPPLY LED UPDATE -We consider this as having low LV AC which could limit the charger's output voltage */
	else if(RAW_DC_V_SENSE < LOW_LV_AC_LEVEL)
	{
		/*SOLID ORANGE*/
		IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, LED_ON);
		IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_RED, LED_ON);
		/*This FLAG maps easily to the LEDs*/
		VIRT_AC_OFF = FLAG_CLR;
	}
	/*LV SUPPLY LED UPDATE -We have full power available. The charger should function properly */
	else
	{
		/*SOLID GREEN*/
		IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, LED_ON);
		IOManagerSetValue(IO_ID_LED_CTRL_SUPPLY_OK_RED, LED_OFF);
		/*This FLAG maps easily to the LEDs*/
		VIRT_AC_OFF = FLAG_CLR;
	}

	/*Controls to connect and disconnect the battery from the RTU */
	if(BAT_CONNECT_UPDATE == LU_TRUE)
	{
		IOManagerSetPulse(IO_ID_BAT_CONNECT, CONNECT_PULSE_MS);
		BAT_CONNECT_UPDATE = LU_FALSE;
	}
	if(BAT_DISCONNECT_UPDATE == LU_TRUE)
	{
		IOManagerSetPulse(IO_ID_BAT_DISCONNECT,	DISCONNECT_PULSE_MS);
		BAT_DISCONNECT_UPDATE = LU_FALSE;
	}

	/********************************************/
	/*CONTROLS AND FLAGS                        */
	/********************************************/
	IOManagerSetValue(IO_ID_ACCUMLATED_DISCHARGE_TIME,			ACCUMLATED_DISCHARGE_TIME);
	IOManagerSetValue(IO_ID_ACCUMULATED_CHARGE_CURRENT,			ACCUMULATED_CHARGE_CURRENT);
	IOManagerSetValue(IO_ID_ACCUMULATED_DISCHARGE_CURRENT,		ACCUMULATED_DISCHARGE_CURRENT);
	IOManagerSetValue(IO_ID_BAT_CHARGER_CUR_LIM_SET,			BAT_CHARGER_CUR_LIM_SET);
	IOManagerSetValue(IO_ID_BAT_CHARGER_EN,						BAT_CHARGER_EN);
	IOManagerSetValue(IO_ID_BAT_CHARGER_VOLT_SET,				BAT_CHARGER_VOLT_SET);
	IOManagerSetValue(IO_ID_BAT_TEST_EN,						BAT_TEST_EN);
	IOManagerSetValue(IO_ID_BAT_TO_MOT_CONNECT,					BAT_TO_MOT_CONNECT);
	IOManagerSetValue(IO_ID_BT_SHUTDOWN_APPROACHING,			BT_SHUTDOWN_APPROACHING);
	IOManagerSetValue(IO_ID_DAC_OUT_BT_TEST_LOAD,				DAC_OUT_BT_TEST_LOAD);
	IOManagerSetValue(IO_ID_VIRT_AC_OFF,						VIRT_AC_OFF	);
	IOManagerSetValue(IO_ID_VIRT_BATTERY_TEST_TIME,				VIRT_BATTERY_TEST_TIME);
	IOManagerSetValue(IO_ID_VIRT_BATTERY_TEST_TOTAL_DISCHARGE,	VIRT_BATTERY_TEST_TOTAL_DISCHARGE);
	IOManagerSetValue(IO_ID_VIRT_BATTERY_VOLTAGE,				VIRT_BATTERY_VOLTAGE);
	IOManagerSetValue(IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME,	VIRT_BT_ACCUMMULATED_CHARGE_TIME);
	IOManagerSetValue(IO_ID_VIRT_BT_BATTERY_CURRENT,			VIRT_BT_BATTERY_CURRENT);
	IOManagerSetValue(IO_ID_VIRT_BT_CH_INHIBITED,				VIRT_BT_CH_INHIBITED);
	IOManagerSetValue(IO_ID_VIRT_BT_CH_SHUTDOWN,				VIRT_BT_CH_SHUTDOWN);
	IOManagerSetValue(IO_ID_VIRT_BT_CHARGE_CYCLES,				VIRT_BT_CHARGE_CYCLES);
	IOManagerSetValue(IO_ID_VIRT_BT_COMMS_FAIL,					VIRT_BT_COMMS_FAIL);
	IOManagerSetValue(IO_ID_VIRT_BT_DATA_NVRAM_FAIL,			VIRT_BT_DATA_NVRAM_FAIL);
	IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED,				VIRT_BT_DISCONNECTED);
	IOManagerSetValue(IO_ID_VIRT_BT_ESTIMATED_CAPACITY,			VIRT_BT_ESTIMATED_CAPACITY);
	IOManagerSetValue(IO_ID_VIRT_BT_FAULT,						VIRT_BT_FAULT);
	IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED,				VIRT_BT_FULLY_CHARGED);
	IOManagerSetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE,		VIRT_BT_CHARGE_CYCLE_CHARGE);
	IOManagerSetValue(IO_ID_VIRT_BT_LOW,						VIRT_BT_LOW);
	IOManagerSetValue(IO_ID_VIRT_BT_OVER_TEMPERATURE,			VIRT_BT_OVER_TEMPERATURE);
	IOManagerSetValue(IO_ID_VIRT_BT_TEMP_FAIL,					VIRT_BT_TEMP_FAIL);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL,					VIRT_BT_TEST_FAIL);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_IN_PROGRESS,			VIRT_BT_TEST_IN_PROGRESS);
	IOManagerSetValue(IO_ID_VIRT_BT_TYPE_LEAD_ACID,				VIRT_BT_TYPE_LEAD_ACID);
	IOManagerSetValue(IO_ID_VIRT_BT_TYPE_NIHM,					VIRT_BT_TYPE_NIHM);
	IOManagerSetValue(IO_ID_VIRT_CH_CURRENT_FAULT,				VIRT_CH_CURRENT_FAULT);
	IOManagerSetValue(IO_ID_VIRT_CH_FAULT,						VIRT_CH_FAULT);
	IOManagerSetValue(IO_ID_VIRT_CH_ON,							VIRT_CH_ON);
	IOManagerSetValue(IO_ID_VIRT_CH_SYSTEM_RUNNING,				VIRT_CH_SYSTEM_RUNNING);
	IOManagerSetValue(IO_ID_VIRT_CH_VOLTAGE_FAULT,				VIRT_CH_VOLTAGE_FAULT);
	IOManagerSetValue(IO_ID_VIRT_DRAIN_CURRENT,					VIRT_DRAIN_CURRENT);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_HARDWARE_FAULT,		VIRT_BT_TEST_HARDWARE_FAULT);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_PASS,					VIRT_BT_TEST_PASS);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_FAIL_OR_RUN,			VIRT_BT_TEST_FAIL_OR_RUN);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_PASS_OR_RUN,			VIRT_BT_TEST_PASS_OR_RUN);
	IOManagerSetValue(IO_ID_VIRT_CHARGER_EN,					VIRT_CHARGER_EN);
	IOManagerSetValue(IO_ID_VIRT_CHARGER_CON,					VIRT_CHARGER_CON);
	IOManagerSetValue(IO_ID_VIRT_BT_TYPE_DEFAULT,				VIRT_BT_TYPE_DEFAULT);
	IOManagerSetValue(IO_ID_VIRT_CH_SET_VOLTAGE,				VIRT_CH_SET_VOLTAGE);
	IOManagerSetValue(IO_ID_VIRT_CH_SET_CURRENT,				VIRT_CH_SET_CURRENT);
	IOManagerSetValue(IO_ID_VIRT_BT_MGR_FSM_STATE,				VIRT_BT_MGR_FSM_STATE);
	IOManagerSetValue(IO_ID_VIRT_BT_CHGR_FSM_STATE,			    VIRT_BT_CHGR_FSM_STATE);
	IOManagerSetValue(IO_ID_VIRT_BT_TEST_END_REASON,			VIRT_BT_TEST_END_REASON);
	IOManagerSetValue(IO_ID_VIRT_NiMH_FAST_CHRG_COMPLETE,       VIRT_NiMH_FAST_CHRG_COMPLETE);
}


/*******************************************************************************/
/*CAN Message type director for the Battery Charger                                 */
/*                                                                             */
/*                                                                             */
/*******************************************************************************/
SB_ERROR BatteryChargerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retVal;
	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{

		case MODULE_MSG_ID_CMD_BAT_CH_SELECT_C:
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatterySelectStr))
			{
				retVal = BatteryChargerSelect((BatterySelectStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_BAT_CH_OPERATE_C:
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatteryOperateStr))
			{
				retVal = BatteryChargerOperate((BatteryOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_BAT_CH_CANCEL_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatteryCancelStr))
			{

				retVal = BatteryChargerCancel((BatteryCancelStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_BAT_CH_RESET_C:
			/*! Message sanity check */
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BatteryResetStr))
			{
				retVal = BatteryResetStats((BatteryResetStr *)msgPtr->msgBufPtr);
			}
			break;
		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_BATTERY_CHARGER_C:
			/*! Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ChargerConfigStr))
			{
				retVal = BatteryChargerConfig((ChargerConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	default:
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retVal;
}


/*******************************************************************************/
/*                                                                             */
/*CAN MESSAGE COMMAND AND CONFIG TYPE HANDLING FUNCTIONS                       */
/*                                                                             */
/*******************************************************************************/

/*******************************************************************************/
/*CAN "SELECT" MESSAGE TYPE COMMAND HANDLER                                    */
/*******************************************************************************/

/*******************************************************************************/

/*******************************************************************************/
/*******************************************************************************/
SB_ERROR BatteryChargerSelect(BatterySelectStr *BatterySelect)
{
	ModReplyStr reply;

	/*Get channel from message*/
	reply.channel 	= BatterySelect->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	/*Check for a switch operation in progress. Return error if a switch operation in progress */
	if( SwEventParamsInProgress )
	{
		reply.status  = REPLY_STATUS_OPERATION_IN_PROGRESS;
	}
	else
	{
		switch(BatterySelect->channel)
		{
		case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
			reply.status  	= BatteryDisconnectSelectHandler(BatterySelect);
			break;
		case PSM_CH_BCHARGER_BATTERY_TEST:
			reply.status  	= BatteryTestSelectHandler(BatterySelect);
			break;
		case PSM_CH_BCHARGER_TEST:
			reply.status  	= ChargerTestSelectHandler(BatterySelect);
			break;
		case PSM_CH_BCHARGER_INHIBIT:
			reply.status  	= ChargerInhibitSelectHandler(BatterySelect);
			break;

		default:
			reply.status  	= REPLY_STATUS_ERROR;
			break;
		}
	}
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply );
}

/*******************************************************************************/
/*CAN "OPERATE" MESSAGE TYPE COMMAND HANDLER                                           */
/*******************************************************************************/
SB_ERROR BatteryChargerOperate(BatteryOperateStr *BatteryOperate)
{
	ModReplyStr reply;

	/*Get channel from message */
	reply.channel 	= BatteryOperate->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	/*Check for a switch operation in progress.Return error if a switch operation in progress*/
	if( SwEventParamsInProgress )
	{
		reply.status  	= REPLY_STATUS_OPERATION_IN_PROGRESS;
	}
	else
	{
		switch(BatteryOperate->channel)
		{
		case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
			reply.status  	= BatteryDisconnectOperateHandler(BatteryOperate);
			break;
		case PSM_CH_BCHARGER_BATTERY_TEST:
			reply.status  	= BatteryTestOperateHandler(BatteryOperate);
			break;
		case PSM_CH_BCHARGER_TEST:
			reply.status  	= ChargerTestOperateHandler(BatteryOperate);
			break;
		case PSM_CH_BCHARGER_INHIBIT:
			reply.status  	= ChargerInhibitOperateHandler(BatteryOperate);
			break;
		default:
			reply.status  	= REPLY_STATUS_ERROR;
			break;
		}
	}
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply );
}

/*******************************************************************************/
/*CAN "CANCEL" MESSAGE TYPE COMMAND HANDLER                                    */
/*******************************************************************************/
SB_ERROR BatteryChargerCancel(BatteryCancelStr *batteryCancel)
{
	ModReplyStr reply;

	/*Get channel from message */
	reply.channel 	= batteryCancel->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(batteryCancel->channel)
	{
	case PSM_CH_BCHARGER_BATTERY_DISCONNECT:
		reply.status  	= BatteryDisconnectCancelHandler(batteryCancel);
		break;
	case PSM_CH_BCHARGER_BATTERY_TEST:
		reply.status  	= BatteryTestCancelHandler(batteryCancel);
		break;
	case PSM_CH_BCHARGER_TEST:
		reply.status  	= ChargerTestCancelHandler(batteryCancel);
		break;
	case PSM_CH_BCHARGER_INHIBIT:
		reply.status  	= ChargerInhibitCancelHandler(batteryCancel);
		break;
	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_CANCEL_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply );
}

/*******************************************************************************/
/*CAN MESSAGE TYPE CONFIG HANDLER                                              */
/*******************************************************************************/
SB_ERROR BatteryChargerConfig(ChargerConfigStr *batteryConfigPtr)
{
	ModReplyStr reply;

	if( nvramAppUserBlkPtr->batteryTestPeriodHours  != batteryConfigPtr->batteryTestPeriodHours ||
		nvramAppUserBlkPtr->batteryTestDurationSecs != batteryConfigPtr->batteryTestDurationSecs   )
	{
		nvramAppUserBlkPtr->batteryTestPeriodHours = batteryConfigPtr->batteryTestPeriodHours;
		nvramAppUserBlkPtr->batteryTestDurationSecs = batteryConfigPtr->batteryTestDurationSecs;

		/* KMG */
		setBatteryTestSchedule(nvramAppUserBlkPtr->batteryTestPeriodHours,
		                       nvramAppUserBlkPtr->batteryTestDurationSecs);

		/* Call function to indicate NVRAM should be updated */
		NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );
	}
	reply.channel 	= 0;
	reply.status  	= REPLY_STATUS_OKAY;
	/* No battery charger configuration command has been defined so return okay */
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_BATTERY_CHARGER_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply );
}

/*******************************************************************************/
/*CAN "SELECT" SUPPORT FUNCTIONS                                               */
/*******************************************************************************/

/*******************************************************************************/
/*BatteryDisconnectSelectHandler                                                     */
/*******************************************************************************/
lu_uint8_t BatteryDisconnectSelectHandler(BatterySelectStr * batteryDisconnectSelect)
{
	lu_int32_t local,remote;
	lu_uint8_t RetVal;

	IOManagerGetValue(IO_ID_LOCAL_LINE, &local);
	IOManagerGetValue(IO_ID_REMOTE_LINE, &remote);

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_DISCON_SELOP_TIMEOUT,
						0 );

	/* Check that an operation isn't in progress already */
	if ( batteryDisconnectOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check for valid state of local/remote switch */
	else if ( (local ^ remote) == 0 )
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	else  if ( !(((batteryDisconnectSelect->local) && local ) || !((!batteryDisconnectSelect->local) && remote)) )
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( batteryDisconnectSelected == LU_TRUE)
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		batteryDisconnectSelectPeriod = (lu_uint32_t)(batteryDisconnectSelect->SelectTimeout) * MSEC;
		batteryDisconnectSelectTime = STGetTime();
		/* Set battery operation selected flag to indicate an operation is pending */
		batteryDisconnectSelected = LU_TRUE;
		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

/*******************************************************************************/
/*BatteryTestSelectHandler                                                     */
/*******************************************************************************/
lu_uint8_t BatteryTestSelectHandler(BatterySelectStr * batteryTestSelect)
{
	lu_uint8_t RetVal;

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_TEST_SELOP_TIMEOUT,
						0 );

	/*If we are not already selected...*/
    if ( batteryTestSelected == LU_FALSE)
	{
		/*Save the period after which the select expires */
		batteryTestSelectPeriod = (lu_uint32_t)(batteryTestSelect->SelectTimeout) * MSEC;
    	/*...start the select timer and...*/
    	batteryTestSelectTime = STGetTime();
    	/*...update the "selected" status*/
    	batteryTestSelected = LU_TRUE;
	}
    /*otherwise we just restart the selected timer*/
    else
    {
		/* Save the period after which the select expires */
		batteryTestSelectPeriod = (lu_uint32_t)(batteryTestSelect->SelectTimeout) * MSEC;
    	/*we got another select so just restart the timer*/
    	batteryTestSelectTime = STGetTime();
    }

	RetVal = REPLY_STATUS_OKAY;

	return RetVal;
}

/*******************************************************************************/
/*ChargerTestSelectHandler                                                     */
/*******************************************************************************/
lu_uint8_t ChargerTestSelectHandler(BatterySelectStr * chargerTestSelect)
{
	lu_uint8_t RetVal;

	/* Check that an operation isn't in progress already */
	if ( chargerTestOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( chargerTestSelected == LU_TRUE)
	{
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
		chargerTestOperate = LU_FALSE;
		chargerTestOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		chargerTestSelectPeriod = (lu_uint32_t)(chargerTestSelect->SelectTimeout) * MSEC;
		chargerTestSelectTime = STGetTime();
		/* Set battery operation selected flag to indicate an operation is pending */
		chargerTestSelected = LU_TRUE;
		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

/*******************************************************************************/
/*ChargerInhibitSelectHandler                                                  */
/*******************************************************************************/
lu_uint8_t ChargerInhibitSelectHandler(BatterySelectStr * chargerInhibitSelect)
{
	lu_uint8_t RetVal;

	/* Reset select timeout error */
	SysAlarmSetLogEvent(LU_FALSE,
						SYS_ALARM_SEVERITY_ERROR,
						SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
						SYSALC_BATTERY_CHARGER_INHIB_SELOP_TIMEOUT,
						0 );


	/* Check that an operation isn't in progress already */
	if ( chargerInhibitOperate == LU_TRUE)
	{
		/* Already operating so this is an error */
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that a select hasn't been received already */
	else if ( chargerInhibitSelected == LU_TRUE)
	{
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		/* Already selected so this is an error */
		RetVal = REPLY_STATUS_SELECT_ERROR;
	}
	else
	{
		/* Save the period after which the select expires */
		chargerInhibitSelectPeriod = (lu_uint32_t)(chargerInhibitSelect->SelectTimeout) * MSEC;
		chargerInhibitSelectTime = STGetTime();
		/* Set battery operation selected flag to indicate an operation is pending */
		chargerInhibitSelected = LU_TRUE;
		RetVal = REPLY_STATUS_OKAY;
	}
	return RetVal;
}

/*******************************************************************************/
/*CAN "OPERATE" SUPPORT FUNCTIONS                                              */
/*******************************************************************************/

/*******************************************************************************/
/*BatteryDisconnectOperateHandler                                                    */
/*******************************************************************************/
lu_uint8_t BatteryDisconnectOperateHandler(BatteryOperateStr * batteryDisconnectOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	lu_int32_t local,remote;
	time = STGetTime();

	/* Check that battery disconnect not operating already */
	if( batteryDisconnectOperate == LU_TRUE )
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that battery disconnect has been selected */
	else if ( batteryDisconnectSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( batteryDisconnectSelectTime , time );
		if ( elapsedTime >   batteryDisconnectSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			batteryDisconnectSelected = LU_FALSE;
			batteryDisconnectSelectTime = 0;
			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_DISCON_SELOP_TIMEOUT,
								0 );
			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Get state of local/remote switch */
			IOManagerGetValue(IO_ID_LOCAL_LINE, &local);
			IOManagerGetValue(IO_ID_REMOTE_LINE, &remote);
			/* Check for valid state of local/remote switch */
			if ( (local ^ remote) == 0 )
			{
				batteryDisconnectSelected = LU_FALSE;
				batteryDisconnectSelectTime = 0;
				batteryDisconnectOperate = LU_FALSE;
				batteryDisconnectOperateTime = 0;
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
			/* Check if local/remote switch is in the correct state */
			else if ( ((batteryDisconnectOperatePtr->local) && local ) || !((!batteryDisconnectOperatePtr->local) && remote) )
			{
				/* Start Battery Disconnection */
				if ( batteryDisconnectOperatePtr->operationDuration > 0 )
				{
					/* Save time of disconnection */
					batteryDisconnectOperatePeriod = (lu_uint32_t)(batteryDisconnectOperatePtr->operationDuration) * MSEC;
					batteryDisconnectOperateTime = time;
					/* Set operate flag */
					batteryDisconnectOperate = LU_TRUE;
					batteryDisconnectSelected = LU_FALSE;
				}

				/*Battery Disconnection Operation*/
				RetVal = IOManagerSetPulse(IO_ID_BAT_DISCONNECT, DISCONNECT_PULSE_MS);
				batteryStatePtr->BatteryConnected 	 = LU_FALSE;		/* Indicates if battery is connected	*/
				batteryStatePtr->BatteryDisConnected = LU_TRUE;			/* Indicates if battery is disconnected	*/
				/* Set virtual point to indicate battery is disconnected */
				IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, FLAG_SET);
				IOManagerSetValue(IO_ID_VIRT_BT_FULLY_CHARGED, FLAG_CLR);
				RetVal = REPLY_STATUS_OKAY;
			}
			else /* local/remote switch is not in the correct state so return an error */
			{
				batteryDisconnectSelected = LU_FALSE;
				batteryDisconnectSelectTime = 0;
				batteryDisconnectOperate = LU_FALSE;
				batteryDisconnectOperateTime = 0;
				RetVal = REPLY_STATUS_LOCAL_REMOTE_ERROR;
			}
		}
	}
	else  /* Battery has not been selected so return an error */
	{
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*******************************************************************************/
/*BatteryTestOperateHandler                                                    */
/*******************************************************************************/
lu_uint8_t BatteryTestOperateHandler(BatteryOperateStr * batteryTestOperatePtr)
{
	lu_uint8_t RetVal;

	RetVal = REPLY_STATUS_OKAY;

	lu_uint32_t time, elapsedTime;
	time = STGetTime();

	/* Check that battery test has been selected */
	if ( batteryTestSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( batteryTestSelectTime , time );
		if ( elapsedTime >   batteryTestSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			batteryTestSelected = LU_FALSE;
			batteryTestSelectTime = 0;
			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_TEST_SELOP_TIMEOUT,
								0 );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Try to kick off Battery Test */
			RetVal = SendBatteryTestEventToFSM(batteryTestOperatePtr);
		}
	}
	else  /* Battery has not been selected so return an error */
	{
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
		batteryTestOperate = LU_FALSE;
		batteryTestOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*******************************************************************************/
/*ChargerTestOperateHandler                                                    */
/*******************************************************************************/
lu_uint8_t ChargerTestOperateHandler(BatteryOperateStr * chargerTestOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	time = STGetTime();

	/* Check that battery test not operating already */
	if( chargerTestOperate == LU_TRUE )
	{
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that battery test has been selected */
	else if ( chargerTestSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( chargerTestSelectTime , time );
		if ( elapsedTime >   chargerTestSelectPeriod )
		{
			/* select time has been exceeded so cancel operation. */
			chargerTestSelected = LU_FALSE;
			chargerTestSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_TEST_OPERATE_TIMEOUT,
								0 );

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{
			/* select time has not been exceeded so operation allowed */
			/* Start charger Test */
			if ( chargerTestOperatePtr->operationDuration > 0 )
			{
				/* Save time for start of test */
				chargerTestOperatePeriod = (lu_uint32_t)(chargerTestOperatePtr->operationDuration) * MSEC;
				chargerTestOperateTime = time;
				/* Set operate flag */
				chargerTestOperate = LU_TRUE;
				chargerTestSelected = LU_FALSE;
			}

			/* Initiate Charger Test Operation to do the following:                */
			/* Leave the charger on but disconnect the charger from the battery.   */
			/* Measure the battery voltage.                                        */
			/* Set the virtual channel if the voltage is too low.                  */
			chargerTestInProgress = LU_TRUE;
			SendChargerTestEventToFSM(chargerTestOperatePtr);

			/*As there will be no cancel command needed cancel the battery test operating flags immediately*/
			chargerTestOperate = LU_FALSE;
			chargerTestOperateTime = 0;
			/* Cancel charger test operation selected flag */
			chargerTestSelected = LU_FALSE;
			chargerTestSelectTime = 0;
			RetVal = REPLY_STATUS_OKAY;
		}
	}
	else  /* charger has not been selected so return an error */
	{
		chargerTestSelected = LU_FALSE;
		chargerTestSelectTime = 0;
		chargerTestOperate = LU_FALSE;
		chargerTestOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}
	return RetVal;
}

/*******************************************************************************/
/*ChargerInhibitOperateHandler                                                 */
/*******************************************************************************/
lu_uint8_t ChargerInhibitOperateHandler(BatteryOperateStr * chargerInhibitOperatePtr)
{
	lu_uint8_t RetVal;
	lu_uint32_t time, elapsedTime;
	lu_int32_t local,remote;

	time = STGetTime();

	/* Check that ChargerInhibit not operating already */
	if( chargerInhibitOperate == LU_TRUE )
	{
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		RetVal = REPLY_STATUS_ALREADY_OPERATING_ERROR;
	}
	/* Check that charger inhibit has been selected */
	else if ( chargerInhibitSelected == LU_TRUE )
	{
		/* Check that select time has not been exceeded. */
		elapsedTime = STElapsedTime( chargerInhibitSelectTime , time );
		if ( elapsedTime >   chargerInhibitSelectPeriod   )
		{
			/* select time has been exceeded so cancel operation. */
			chargerInhibitSelected = LU_FALSE;
			chargerInhibitSelectTime = 0;

			/* Set select timeout error */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_INHIB_SELOP_TIMEOUT,
								0);

			RetVal = REPLY_STATUS_SELECT_ERROR;
		}
		else
		{	/* select time has not been exceeded so operation allowed */
			/* Get state of local/remote switch */
			IOManagerGetValue(IO_ID_LOCAL_LINE, &local);
			IOManagerGetValue(IO_ID_REMOTE_LINE, &remote);

				/* Start charger inhibit */
				if ( chargerInhibitOperatePtr->operationDuration > 0 )
				{
					/* Save time of disconnection */
					chargerInhibitOperatePeriod = (lu_uint32_t)(chargerInhibitOperatePtr->operationDuration) * MSEC;
					chargerInhibitOperateTime = time;
					/* Set operate flag */
					chargerInhibitOperate = LU_TRUE;
					chargerInhibitSelected = LU_FALSE;
				}

				/*Send Inhibit event to the charging state machine*/
				SendChargerInhibitEventToFSM(chargerInhibitOperatePtr);
				/* Set virtual point to indicate charger is inhibited */
				IOManagerSetValue(IO_ID_VIRT_BT_CH_INHIBITED, FLAG_SET);
				RetVal = REPLY_STATUS_OKAY;
		}
	}
	else  /* Has not been selected so return an error */
	{
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		RetVal = REPLY_STATUS_OPERATE_ERROR;
	}

	return RetVal;
}

/*******************************************************************************/
/*CAN "CANCEL" SUPPORT FUNCTIONS                                               */
/*******************************************************************************/

/*******************************************************************************/
/*BatteryDisconnectCancelHandler                                               */
/*******************************************************************************/
lu_uint8_t BatteryDisconnectCancelHandler(BatteryCancelStr * batteryDisconnectCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(batteryDisconnectCancel);

	/* Cancel operation */
	if ( (batteryDisconnectOperate == LU_TRUE) || (batteryDisconnectSelected == LU_TRUE) )
	{
		/*Reconnect the battery*/
		RetVal = IOManagerSetPulse(IO_ID_BAT_CONNECT, CONNECT_PULSE_MS);
		batteryStatePtr->BatteryConnected 			= LU_TRUE;
		batteryStatePtr->BatteryDisConnected 		= LU_FALSE;
		/* Set virtual point to indicate battery is connected */
		RetVal = IOManagerSetValue(IO_ID_VIRT_BT_DISCONNECTED, FLAG_CLR);
		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		/* Cancel battery disconnect operating flag */
		batteryDisconnectOperate = LU_FALSE;
		batteryDisconnectOperateTime = 0;
		/* Cancel battery disconnect operation selected flag */
		batteryDisconnectSelected = LU_FALSE;
		batteryDisconnectSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel battery disconnect operating flag */
	batteryDisconnectOperate = LU_FALSE;
	batteryDisconnectOperateTime = 0;
	/* Cancel battery disconnect operation selected flag */
	batteryDisconnectSelected = LU_FALSE;
	batteryDisconnectSelectTime = 0;

	return RetVal;
}

/*******************************************************************************/
/*BatteryTestCancelHandler                                                     */
/*******************************************************************************/
lu_uint8_t BatteryTestCancelHandler(BatteryCancelStr * batteryTestCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(batteryTestCancel);

	/* Cancel operation */
	if ( (batteryTestOperate == LU_TRUE) || (batteryTestSelected == LU_TRUE) )
	{
		/* Set flag in battery test structure to indicate a cancel has been received */
		batTestState.BatteryTestCancelled = LU_TRUE;
		RetVal = REPLY_STATUS_OKAY;
		SendBatteryTestCancelEventToFSM(batteryTestCancel);
	}
	else
	{
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	return RetVal;
}

/*******************************************************************************/
/*ChargerTestCancelHandler                                                     */
/*******************************************************************************/
lu_uint8_t ChargerTestCancelHandler(BatteryCancelStr * chargerTestCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(chargerTestCancel);

	/* Cancel operation */
	if ( (chargerTestOperate == LU_TRUE) || (chargerTestSelected == LU_TRUE) )
	{
		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		/* Cancel charger test operating flag */
		batteryTestOperate = LU_FALSE;
		batteryTestOperateTime = 0;
		/* Cancel charger test operation selected flag */
		batteryTestSelected = LU_FALSE;
		batteryTestSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/* Cancel charger test operating flag */
	chargerTestOperate = LU_FALSE;
	chargerTestOperateTime = 0;
	/* Cancel charger test operation selected flag */
	chargerTestSelected = LU_FALSE;
	chargerTestSelectTime = 0;
	/* Cancel charger test global flag */
	chargerTestInProgress = LU_TRUE;
	return RetVal;
}

/*******************************************************************************/
/*ChargerInhibitCancelHandler                                                  */
/*******************************************************************************/
lu_uint8_t ChargerInhibitCancelHandler(BatteryCancelStr * chargerInhibitCancel)
{
	lu_uint8_t RetVal;
	LU_UNUSED(chargerInhibitCancel);

	/* Cancel operation */
	if ( (chargerInhibitOperate == LU_TRUE) || (chargerInhibitSelected == LU_TRUE) )
	{
		RetVal = REPLY_STATUS_OKAY;
	}
	else
	{
		/* Cancel charger Inhibit operating flag */
		chargerInhibitOperate = LU_FALSE;
		chargerInhibitOperateTime = 0;
		/* Cancel charger test operation selected flag */
		chargerInhibitSelected = LU_FALSE;
		chargerInhibitSelectTime = 0;
		RetVal = REPLY_STATUS_CANCEL_ERROR;
	}

	/*Send cancel inhibit event to the battery charging state machine */
	SendChargerInhibitCancelEventToFSM(chargerInhibitCancel);
	/* Set virtual point to indicate charger is inhibited */
	IOManagerSetValue(IO_ID_VIRT_BT_CH_INHIBITED, FLAG_CLR);
	/* Cancel inhibit operating flag */
	chargerInhibitOperate = LU_FALSE;
	chargerInhibitOperateTime = 0;
	/* Cancel charger test operation selected flag */
	chargerInhibitSelected = LU_FALSE;
	chargerInhibitSelectTime = 0;
	return RetVal;
}

/*******************************************************************************/
/*BatteryResetStats                                                            */
/*******************************************************************************/
lu_uint8_t  BatteryResetStats(BatteryResetStr *batteryresetStr)
{
	lu_uint8_t RetVal = REPLY_STATUS_OKAY;
	ModReplyStr reply;
	LU_UNUSED(batteryresetStr);

	/*Initiate reseting the battery statistics held in NVRAM */
//SRM	if( SendBatteryResetStatsEventToFSM() )
//SRM	{
//SRM		reply.status  	= REPLY_STATUS_OKAY;
//SRM	}
//SRM	else
//SRM	{
		reply.status  	= REPLY_STATUS_ERROR;
//SRM	}
	reply.channel 	= 0;
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_BAT_CH_RESET_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply );
	return RetVal;
}

/*******************************************************************************/
/*updateBatteryCurrent                                                         */
/* Update the battery Current, battery drain current						   */
/* 	and the accumulated current												   */
/*******************************************************************************/
void updateBatteryCurrent(void)
{
	lu_int32_t iBatteryRTUDrain = 0;
	lu_int32_t iBatteryMotDrain = 0;
	lu_int32_t iBatteryTestDrain = 0;
	lu_int32_t iChargerMeasured = 0;
	lu_int32_t iBatteryDrain = 0;
	lu_int32_t iBatteryCurrent = 0;
	lu_int32_t ChargeTime = 0;
	lu_int32_t tempRange = 0;


	/*
	 * Measure the Battery Current, flowing out from the battery
	 * +ve => Battery discharging
	 * -ve => Battery charging
	 *
	 * if the Charger is On and Running then we are charging not draining the battery
	 * WHAT ABOUT CONNECTED ?
	 */
	if((VIRT_CH_ON == LU_TRUE) && (VIRT_CH_SYSTEM_RUNNING == LU_TRUE))
	{
		iChargerMeasured = BAT_CHARGER_I_SENSE;

		iBatteryDrain = 0;
		iBatteryCurrent = (0 - iChargerMeasured);

		if (iChargerMeasured > 0)
		{
			/* Accumulate the Total Charge Current,
			 * This is done every second whilst we are on Charge, this then is converted to mAh */
			batteryStatePtr->BatteryChgCycleTotalChargeCurrent += iChargerMeasured;
			/* Convert the Total Charge into mAh and publish to the Virtual point */
			VIRT_BT_CHARGE_CYCLE_CHARGE = batteryStatePtr->BatteryChgCycleTotalChargeCurrent / SECONDS_PER_HOUR;

			/* Update NVRAM storage */
			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds += iChargerMeasured;

			if(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds > SECONDS_PER_HOUR)
			{
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.mAmpSeconds -= SECONDS_PER_HOUR;
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours += 1;
			}

			// 2. Accumulate the charging time, increased every second (tick) and increase minutes every 60.
			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.seconds += 1;
			if( (batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.seconds) > SECONDS_PER_MINUTE )
			{
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.seconds -= SECONDS_PER_MINUTE;
				batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin += 1;
			}

			updateDataNVRAM = LU_TRUE;

			/* Update the Accumulated charge virtual points */
			ACCUMULATED_CHARGE_CURRENT = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours;
			VIRT_BT_ACCUMMULATED_CHARGE_TIME = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin;
		}

		/*
		 * The following code records the battery charging across temperature ranges,
		 * Added for completeness with previous battery charger
		 */
		{
			/* Add charging time to correct temperature band */

			if( BAT_TEMP < 0 )
			{
				tempRange = BAT_TEMP_RANGE_BELOW_0;
			}
			else if( (BAT_TEMP < 0) && (BAT_TEMP < 40000 ) )
			{
				tempRange = BAT_TEMP_RANGE_00_40;
			}
			else if( (BAT_TEMP < 40000) && (BAT_TEMP < 60000 ) )
			{
				tempRange = BAT_TEMP_RANGE_40_60;
			}
			else
			{
				tempRange = BAT_TEMP_RANGE_ABOVE_60;
			}

			batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.seconds += ChargeTime;

			/* Convert seconds to hours and seconds if needed */
			if( batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.seconds > SECONDS_PER_MINUTE )
			{
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.seconds -= SECONDS_PER_MINUTE;
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].time.totalMin   += 1;
			}

			/* Add the latest charge current to the correct temperature band */
			batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.mAmpSeconds	+=  batteryStatePtr->BatteryTotalChargeCurrent;

			/* Convert mA seconds to Amp hours and mA seconds if needed */
			if( batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.mAmpSeconds > SECONDS_PER_HOUR )
			{
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.mAmpSeconds -= SECONDS_PER_HOUR;
				batteryDataNVRamBlkPtr->charging[tempRange].currentData[CHARGING].current.total100mAmpHours += 1;
			}
		}
	}
	else /* We are not charging, so the battery is being drained */
	{
		/*
		 * Get all the current currents
		 * Possible Battery drain currents are
		 * RTU Current, Test Load Current, Motor supply Current
		 */

		/* If LV is missing then battery is supporting the RTU  */
		if( is_LV_Present() != LU_TRUE )
		{
			iBatteryRTUDrain = MAIN_DC_I_SENSE;
		}

		/* The motor is supplied by the battery */
		iBatteryMotDrain = MOT_I_SENSE;

		/* If battery test is in progress, the battery is supply the test current */
		if( VIRT_BT_TEST_IN_PROGRESS == LU_TRUE )
		{
			iBatteryTestDrain = BAT_TEST_I_SENSE;
		}

		iBatteryDrain = iBatteryRTUDrain + iBatteryMotDrain + iBatteryTestDrain;
		iBatteryCurrent = iBatteryDrain;

		/* Accumulate the Total Discharge Current,
		 * This is done every second whilst we are off charge, this then is converted to mAh and stored in NVRam */

		batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds += iBatteryDrain;
		if(batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds > SECONDS_PER_HOUR)
		{
			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.mAmpSeconds -= SECONDS_PER_HOUR;
			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours += 1;
		}

		batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.seconds += 1;
		if( (batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.seconds) > SECONDS_PER_MINUTE )
		{
			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.seconds -= SECONDS_PER_MINUTE;
			batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin += 1;
		}

		updateDataNVRAM = LU_TRUE;

		ACCUMULATED_DISCHARGE_CURRENT = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours;
		ACCUMLATED_DISCHARGE_TIME = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin;
	}

	/* Set the Virtual points to be updated */
	VIRT_BT_BATTERY_CURRENT = iBatteryCurrent;
	VIRT_DRAIN_CURRENT = iBatteryDrain;

	/* If the NVRAM data block has been changed then call the API function to update the NVRAM */
	if ( updateDataNVRAM == LU_TRUE)
	{
		NVRamBatteryDataWriteUpdate( NVRAM_BATT_DATA_BLK_USER_A );
		updateDataNVRAM = LU_FALSE;
	}
}

lu_bool_t is_LV_Present( void )
{
	lu_bool_t retVal = LU_FALSE;

    /*Evaluate it against the agreed threshold */
	if( RAW_DC_V_SENSE > LV_SUPPLY_THRESHOLD_VOLTAGE )
	{
		retVal =  LU_TRUE;
	}

	return retVal;
}

/************************ End of file ******************************************/


