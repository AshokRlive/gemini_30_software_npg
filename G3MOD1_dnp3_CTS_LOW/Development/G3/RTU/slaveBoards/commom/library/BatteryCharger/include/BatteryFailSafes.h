/*
 * BatteryFailSafes.h
 *
 *  Created on: 31 Jul 2018
 *      Author: gill_k
 */

#ifndef BATTERYFAILSAFES_H_
#define BATTERYFAILSAFES_H_

#include "NVRAMDef/NVRAMDefOptBat.h"

const struct NVRAMDefOptBatDef batteryOptsFailSafe =
{
	    2,                           /* Battery Chemistry */
	    1200,                  /* Battery Cell Nominal Volts */
	    13250,                /* Battery Cell Full Charge Volts */
	    7000,                    /* Battery Cell Max Capacity */
	    2000,                /* Battery Cell Charging Current */
	    13750,                  /* Battery Cell Charging Volts */
	    10,                        /* Battery Cell Leakage */
	    250,                    /* Battery Cell Float Charge */
	    0,                  /* Battery Cell Charging Method */
	    1,                        /* Battery Pack No Of Cols */
	    2,                        /* Battery Pack No Of Rows */
	    50,                  /* Battery Pack Max Charge Temp */
	    7200,                  /* Battery Pack Max Charge Time */
	    120,              /* Battery Pack Time Between Charge */
	    3600,                    /* Battery Pack Overide Time */
	    25500,                  /* Battery Pack Charge Trigger */
	    24000,                 /* Battery Pack Threshold Volts */
	    22000,                       /* Battery Pack Low Level Volts */
	    1,                    /* Battery Test Time Minutes */
	    300,                /* Battery Test Current Minimum (mA) */
	    7,             /* Battery Test Suspend Time Minutes */
	    240,    /* Battery Test Maximum Duration Time Minutes */
	    10,              /* Battery Test Discharge Capacity */
	    23000,                /* Battery Test Threshold Volts */
	    2,                       /* Battery Test Load Type (1 or 0xff = 2amp PTC 2=10amp PTR R) */
	    22500,            /* Battery Pack Shutdown Level Volts */
	    19000,            /* Battery Pack Deep Discharge Protection Level Volts */
};

#endif /* BATTERYFAILSAFES_H_ */
