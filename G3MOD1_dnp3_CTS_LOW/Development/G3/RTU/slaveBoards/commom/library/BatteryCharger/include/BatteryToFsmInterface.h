/*
 * BatteryToFsmInterface.h
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */

#ifndef BATTERYTOFSMINTERFACE_H_
#define BATTERYTOFSMINTERFACE_H_

#include "lu_types.h"
#include "errorCodes.h"
#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"
#include "NVRam.h"
#include "LinearInterpolation.h"

/*Should really have been placed in NVRAM*/
#define QUOTED_CHARGING_TEMPERATURE (20)	/* temperature at which NVM charging voltage is quoted */
#define TEMP_COMP_COEFFICIENT_LEAD_ACID  (-18)		/* Temperature compensation for charging in milli-volts per degree (3mV/Cell) */
#define VOLTS_AT_ZERO_DEGREES_LEAD_ACID  (2350)
#define TEMP_COMP_COEFFICIENT_NICKEL_METAL (0)			/* Temperature compensation for charging in milli-volts per degree */

extern NVRAMDefOptBatStr    *batteryOpts;
extern BatteryDataNVRAMBlkStr	*batteryDataNVRamBlkPtr;

extern lu_bool_t BatManagerFsmInit(void);

extern void SendTimerTickEventToFSM(void);

extern void SendMotorSupplyOnEventToFSM(void);

extern void  SendMotorSupplyCancelEventToFSM(void);

extern REPLY_STATUS  SendChargerInhibitEventToFSM(BatteryOperateStr * tstData);

extern REPLY_STATUS  SendChargerInhibitCancelEventToFSM(BatteryCancelStr * tstData);

extern REPLY_STATUS  SendBatteryTestEventToFSM(BatteryOperateStr * tstData);

extern REPLY_STATUS  SendBatteryTestCancelEventToFSM( BatteryCancelStr * tstData );

extern REPLY_STATUS  SendChargerTestEventToFSM(BatteryOperateStr * tstData);

extern REPLY_STATUS  SendBatteryResetStatsEventToFSM(BatteryResetStr *rstData);

#endif /* BATTERYTOFSMINTERFACE_H_ */

















