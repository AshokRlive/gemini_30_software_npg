/*
 * BaManagerFSM.h
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */

#ifndef BAMANAGERFSM_H_
#define BAMANAGERFSM_H_

/*****************************************************************************************************************************************
Description of this Finite State Machine (FSM) as in original template :
Finite State Machines are a collection of distinct "states" or "modes of operation". There can only ever be one active state at a time.
Transitioning from the active state to the next active state is determined by the exits defined in the active state in terms of events
or commands recieved and prevailing conditions such as flags and variables etc.
This FSM template caters for a number of system events such as a regular timer event or some CAN command events etc.
This FSM template caters for a state to execute code exclusively at entry to the state where it will ignore transitions back to itself.
This FSM template caters for the execution of code on every contiguous call to the same active state.
This FSM template caters for the testing of a number of possible exits where upon it will execute exit actions for the particular exit.
This FSM is always called via a function pointer that points to the present active state function.


FSM function pointer                                          <-- Called for specified system events and updated to point to a state
      |                                                           function (becomming the active state) by a condition function when
	  |                                                           it tests for a validates the condition it is looking for.
	  |
	  |_____state 1 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 1 entry action code        <-- Called exclusively on first entry to state 1 function
	  |             |
	  |             |_______state 1 unconditional code       <-- Called for every event call to FSM whilst in state 1
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 1 condition 1 function     <--| All condition functions get called until one returns "true".
	  |             |_______state 1 condition 2 function     <--| If "true" it changes active state ie function pointed to by "FSM function pointer"
	  |             |                                        <--| to point to new active state and executes any exit actions. The next event to call
	  |             |_______state 1 condition m function     <--| "FSM function pointer" will call the new active state function.
	  |
	  |
	  |_____state 2 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 2 entry action code        <-- Called exclusively on first entry to state 2 function
	  |             |
	  |             |_______state 2 unconditional code       <-- Called for every event call to FSM whilst in state 2
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 2 condition 1 function
	  |             |_______state 2 condition 2 function
	  |             |
	  |             |_______state 2 condition m function
	  |
	  |_____state n function

The original template caters for 6 state functions each of which has 3 exit condition functions

To create your a new statemachine you need to re-create this file  [newFileNameFSM.h]
and define your own event IDs below
******************************************************************************************************************************************/

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocolEnum.h"
/*Include all the typedefs for data and function pointers to be used in an FSM*/
#include "fsm.h"


/*Define event IDs for this statemachine*/
#define TimerTickEvent              (EventID)(0)
#define BatteryTestEvent            (EventID)(1)
#define BatteryTestCancelEvent	    (EventID)(2)
#define BatteryStatsResetEvent      (EventID)(3)
#define ChargeInhibitEvent          (EventID)(4)
#define ChargeInhibitCancelEvent    (EventID)(5)
#define ChargeTestEvent             (EventID)(6)
#define ChargeTestCancelEvent       (EventID)(7)
#define MotorSupplyOnEvent          (EventID)(8)
#define MotorSupplyCancelEvent      (EventID)(9)
#define BatteryDisconEvent          (EventID)(10)
#define BatteryDisconCancelEvent    (EventID)(11)
#define BatteryTestInhibEvent       (EventID)(12)
#define BatteryTestInhibCancelEvent (EventID)(13)
/*EVENT FROM MANAGER TO CHARGER LEVEL*/
#define GoToIdleEvent               (EventID)(14)
#define ScheduledTestEvent          (EventID)(15)
#define RecoverBatteryEvent         (EventID)(16)

#define INITIALISE_STATE           0
#define OPERATE_SWITCHGEAR_STATE   1
#define BAT_CHARGING_STATE         2
#define NO_LV_STATE                3
#define BAT_TEST_STATE             4
#define DEEP_DISCHARGE_STATE       5
#define BAT_NOT_PRESENT_STATE      6

#define TIME_VALUE_5_SECS 		5
#define TIME_VALUE_1_SECS 		1
/* PTC takes over 9A @ 20C, so allow up to 12A for colder operation */
#define BATT_TEST_OVERLOAD_CURRENT 12000

/*VOLTAGE MARGIN*/
#define vUNHEALTHY_MARGIN      1000
#define vWARNING_MARGIN        1000
#define vFULLY_CHARGED_MARGIN  500

#define HYSTERESIS100mV        100

/*CHARGER VOLTAGE SETTING TOLERANCE*/
#define CHRGR_SETTING_TOLERENCE_HI 500
#define CHRGR_VOLTAGE_SETTING_FAULT_OFFSET 500
#define CHRGR_SETTING_TOLERENCE_LO 1000
/*CHARGER CURRENT QUALIFYING THRESHOLD TO DETECT ERROR*/
#define CHRGR_CURRENT_LOW_LEVEL_THRESHOLD 10
/*Current level at which Bat test declares no battery connected*/
#define NO_BAT_DETECT_CURRENT 10

/*Re-check status timer while in BAT_CHARGING state...                      */
/*...in this state checking battery status interferes with the charging     */
/*process because we have to disconnect the charger to look at the battery's*/
/*stand alone voltage. For that reason we control the battery status updates */
//SRM
#define TRY_CHARGING_PERIODIC_BAT_STAT_TIMER_VAL 10 //60
#define BAT_NOT_PRESENT_PERIODIC_BAT_STAT_TIMER_VAL 5  //30
/*This is strictly backup to allow the PSM to disconnect from RTU if the MCM does not send a disconnect from RTU*/
#define TIME_TO_BATTERY_DISCONNECT 60
/*Both Scheduled and requested battery tests have a delay before testing begins*/
#define TEST_START_DELAY 5
#define TEST_BATTERY_IMPEDANCE_DELAY TEST_START_DELAY+30

/* Pulse length to switch motor supply */
#define MOTOR_PULSE_MS		30

#define INITIALISE_STABILISATION_TIME 5

#define MILLIDEGREES_TO_DEGREES 1000

/*Single point function call to drive this FSM with an event*/
extern REPLY_STATUS BAT_MANAGER_FSM_MAIN(EventID eventIn);

/*CAN requested Battery test timer for the FSM*/
extern void fsmBatCANTestDurationAndCapacity( lu_int32_t timerValue, lu_int32_t capacity);
/*CAN requested Motor Drive power supply test timer for the FSM*/
extern void fsmExtTimerSetSetup( lu_int16_t operationID, lu_int32_t timerValue);
extern void fsmExtTimerCancelSetup( lu_int16_t operationID);
extern void setBatteryTestSchedule(lu_int32_t Repeat,lu_int32_t Duration);

extern lu_bool_t setChargerTemperatureDisable(lu_bool_t tempStopCharge);

#endif /* BAMANAGERFSM_H_ */
