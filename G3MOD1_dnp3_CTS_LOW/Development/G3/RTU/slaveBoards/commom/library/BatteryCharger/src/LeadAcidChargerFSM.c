/*
 * LeadAcidChargerFSM.c
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */
/*****************************************************************************************************************************************
Description of this Finite State Machine (FSM) as in original template :
Finite State Machines are a collection of distinct "states" or "modes of operation". There can only ever be one active state at a time.
Transit from the active state to the next active state is determined by the exits defined in the active state in terms of events
or commands received and prevailing conditions such as flags and variables etc.
This FSM template caters for a number of system events such as a regular timer event or some CAN command events etc.
This FSM template caters for a state to execute code exclusively at entry to the state where it will ignore transitions back to itself.
This FSM template caters for the execution of code on every contiguous call to the same active state.
This FSM template caters for the testing of a number of possible exits where upon it will execute exit actions for the particular exit.
This FSM is always called via a function pointer that points to the present active state function.


FSM function pointer                                          <-- Called for specified system events and updated to point to a state
      |                                                           function (becoming the active state) by a condition function when
	  |                                                           it tests for a validates the condition it is looking for.
	  |
	  |_____state 1 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 1 entry action code        <-- Called exclusively on first entry to state 1 function
	  |             |
	  |             |_______state 1 unconditional code       <-- Called for every event call to FSM whilst in state 1
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 1 condition 1 function     <--| All condition functions get called until one returns "LU_TRUE".
	  |             |_______state 1 condition 2 function     <--| If "LU_TRUE" it changes active state ie function pointed to by "FSM function pointer"
	  |             |                                        <--| to point to new active state and executes any exit actions. The next event to call
	  |             |_______state 1 condition m function     <--| "FSM function pointer" will call the new active state function.
	  |
	  |
	  |_____state 2 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 2 entry action code        <-- Called exclusively on first entry to state 2 function
	  |             |
	  |             |_______state 2 unconditional code       <-- Called for every event call to FSM whilst in state 2
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 2 condition 1 function
	  |             |_______state 2 condition 2 function
	  |             |
	  |             |_______state 2 condition m function
	  |
	  |_____state n function

The original template caters for 6 state functions each of which has 3 exit condition functions

To create your a new state machine you need to
1)Re-create this file [newFileNameFSM.c]
2)Re-create its header file [newFileNameFSM.h]  and include the header here.
3)Re-create any timeout timers you will require
4)Re-create the timeout timers processing array with the names of your new timers.
5)Create any Global State exit functions that you require.
6)Create an array list of pointers to the Global State exit functions for processing them
7)For each state you require create a block of functions as below
7a)The StateFunc
7b)The StateEntryFunc
7c)The StateUncondFunc
7d)A StateExit function for each exit from the state
7e)An array of StateExit function pointers listing all the stateExit functions
8)Declare a state struct for each state and initialise it with the components created in the previous steps
9)Re-Create the array of pointers to state structures (allStates[]) and initialise with the address of all the states
10)Re-create the FSM struct for this FSM with its own name and initialise with the components created previously
11)Re-create the FSM_MAIN function with an appropriate name. This is the only function called from outside this file.
12)Write function definitions for all the files declared in the previous steps.
******************************************************************************************************************************************/

/*Sub state FSM of BatManagerFSM - This FSM runs only when the BatManagerFSM is in the CHARGE state. */
/*It receives no events of any kind, including the regular timer tick event, when the BatManagerFSM is*/
/*not in the CHARGE state.                                                                           */
/*It does use BatManagerFSM events and therefore includes the BatManagerFSM.h                        */
#include "LeadAcidChargerFSM.h"
#include "BatteryChargerTopLevel.h"
#include "BatteryToFsmInterface.h"
#include "BatManagerFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"

/*******************************************************/
/*Only one Event input structure for this state machine*/
/*******************************************************/
static EventID event;
/*******************************************************/

/***************************************************************************/
/*Declare any timeout timers for use in this FSM. These are decrement every*/
/*timer tick event fed into the FSM. This is expected to be a timed call to*/
/*this FSM e.g. every second. The programmer must take proper account of this*/
/***************************************************************************/
static lu_int32_t ConstCurrentPhaseTimer = 0;
static lu_int32_t batteryFullStabilisationTimer=0;

static lu_int32_t maxInitialChargeTimer=0;

/*Add all timers declared above here for processing and terminate with a NULL*/
static lu_int32_t *fsmTimers_p[]={&ConstCurrentPhaseTimer,
		                          &batteryFullStabilisationTimer,
		                          NULL_FSM_TIMER};

static void fsmTimersUpdate(void);
static void fsmTimerSet( lu_int32_t* timerName, lu_int32_t timerValue);
static lu_int32_t fsmTimerGet( lu_int32_t* timerName );

static lu_bool_t EXIT_FLAG_TO_INITIAL_CHARGE_STATE;
static lu_bool_t EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE;
static lu_bool_t EXIT_FLAG_FLOAT_TO_CHECK_CHARGER_STATE;

/*Storage for scheduled test acknowledge*/
static lu_bool_t SCHEDULED_TEST_ACCEPETED = LU_TRUE;
/*Read access function*/
lu_bool_t getLdAcidScheduledTestAcknowledge(void);

void setLdAcidScheduledTestAcknowledge(lu_bool_t SchedTestOk);

/*Storage for battery's health status*/
static lu_bool_t BatteryPresenceIndicator;
/*Read access function*/
lu_bool_t getLdAcidBatteryPresenceIndicator(void);
/*Write access function*/
void setLdAcidBatteryPresenceIndicator(lu_bool_t battStat);

/*Storage for the charger's health status*/
static chrgHlthStatus ChargerHealthStatus;
/*Read access function*/
chrgHlthStatus getLdAcidChargerHealthStatus(void);
/*Write access function*/
void setLdAcidChargerHealthStatus(chrgHlthStatus chrgStat);


/*Maintained here and accessible from Battery Manager FSM*/
static lu_int32_t vChargerSetting;
//lu_int32_t getChargerVolts(void);
//void       setChargerVolts(lu_int32_t vCharger);

/************************************************************************/
/*Access functions to battery charger level decisions                   */
/************************************************************************/
static lu_bool_t BATTERY_NOT_USABLE_FLAG = LU_FALSE;
//lu_bool_t getChargingLevelFsmBatAnalysis(void);
//void      invalidateBatFullyCharged(void);

/************************************************************************/
/*Only one Function to terminate exit searches in all states of this FSM*/
/************************************************************************/
static lu_bool_t stateExitSearchEnd(fsm *this);
/************************************************************************/

/****************************************/
/*Global exits processing function      */
/****************************************/
static lu_bool_t CHRG_LEAD_ACID_GLOBAL_Exit_Conditions(fsm *this);

/*****************************/
/*Global State exit functions*/
/*****************************/
static lu_bool_t CHRG_LEAD_ACID_GLOBAL_Exit_GoToIdleRqst(fsm *this);

/*   Add more global exits here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the global exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs CHRG_LEAD_ACID_GLOBAL_ExitFuncs[]={CHRG_LEAD_ACID_GLOBAL_Exit_GoToIdleRqst,
                                                      stateExitSearchEnd};

/*****************************************************************/
/*****************************************************************/
/*One of these blocks for each state in this Finite State Machine*/
/*****************************************************************/
/*****************************************************************/

/*******************************************************************/
/*    CHECK_CHARGER_ state function prototypes and initialisers*/
/*******************************************************************/
static void CHECK_CHARGER_StateFunc(fsm *this);
static void CHECK_CHARGER_StateEntryFunc(fsm *this);
static void CHECK_CHARGER_StateUncondFunc(fsm *this);

/*       One for each exit from the state*/
static lu_bool_t CHECK_CHARGER_StateExitTo_INITIAL_CHARGE(fsm *this);
static lu_bool_t CHECK_CHARGER_StateExitTo_CHECK_CHARGER(fsm *this);
static lu_bool_t CHECK_CHARGER_StateExitTo_CHARGE_SUSPENDED(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs CHECK_CHARGER_StateExitFuncs[]={CHECK_CHARGER_StateExitTo_CHARGE_SUSPENDED,
												   CHECK_CHARGER_StateExitTo_INITIAL_CHARGE,
												   CHECK_CHARGER_StateExitTo_CHECK_CHARGER,
                                                   stateExitSearchEnd};

/******************************************************************/
/*    INITIAL_CHARGE_ state function prototypes and initialisers*/
/******************************************************************/
static void INITIAL_CHARGE_StateFunc(fsm *this);
static void INITIAL_CHARGE_StateEntryFunc(fsm *this);
static void INITIAL_CHARGE_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t INITIAL_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this);
static lu_bool_t INITIAL_CHARGE_StateExitTo_FLOAT_CHARGE(fsm *this);
static lu_bool_t INITIAL_CHARGE_StateExitTo_FLOAT_CHARGE_BY_TIMEOUT(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs INITIAL_CHARGE_StateExitFuncs[]={INITIAL_CHARGE_StateExitTo_CHARGE_SUSPENDED,
													INITIAL_CHARGE_StateExitTo_FLOAT_CHARGE,
													INITIAL_CHARGE_StateExitTo_FLOAT_CHARGE_BY_TIMEOUT,
                                                    stateExitSearchEnd};

/*******************************************************************/
/*    CHARGE_SUSPENDED_ state function prototypes and initialisers*/
/*******************************************************************/
static void CHARGE_SUSPENDED_StateFunc(fsm *this);
static void CHARGE_SUSPENDED_StateEntryFunc(fsm *this);
static void CHARGE_SUSPENDED_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t CHARGE_SUSPENDED_StateExitTo_CHECK_CHARGER(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs CHARGE_SUSPENDED_StateExitFuncs[]={CHARGE_SUSPENDED_StateExitTo_CHECK_CHARGER,
                                                      stateExitSearchEnd};

/*******************************************************************/
/*    FLOAT_CHARGE_ state function prototypes and initialisers*/
/*******************************************************************/
static void FLOAT_CHARGE_StateFunc(fsm *this);
static void FLOAT_CHARGE_StateEntryFunc(fsm *this);
static void FLOAT_CHARGE_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t FLOAT_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this);
static lu_bool_t FLOAT_CHARGE_StateExitTo_CHECK_CHARGER(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs FLOAT_CHARGE_StateExitFuncs[]={FLOAT_CHARGE_StateExitTo_CHARGE_SUSPENDED,
												  FLOAT_CHARGE_StateExitTo_CHECK_CHARGER,
                                                  stateExitSearchEnd};






/*********      ADD MORE STATE BLOCKS HERE IF REQUIRED    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*****************************************************************/

/************************************************************/
/*InitiaLISE each of the states in this Finite State Machine*/
/************************************************************/
state CHECK_CHARGER={CHECK_CHARGER_StateFunc,CHECK_CHARGER_StateEntryFunc,CHECK_CHARGER_StateUncondFunc,CHECK_CHARGER_StateExitFuncs};
state INITIAL_CHARGE={INITIAL_CHARGE_StateFunc,INITIAL_CHARGE_StateEntryFunc,INITIAL_CHARGE_StateUncondFunc,INITIAL_CHARGE_StateExitFuncs};
state CHARGE_SUSPENDED={CHARGE_SUSPENDED_StateFunc,CHARGE_SUSPENDED_StateEntryFunc,CHARGE_SUSPENDED_StateUncondFunc,CHARGE_SUSPENDED_StateExitFuncs};
state FLOAT_CHARGE={FLOAT_CHARGE_StateFunc,FLOAT_CHARGE_StateEntryFunc,FLOAT_CHARGE_StateUncondFunc,FLOAT_CHARGE_StateExitFuncs};
/****    ADD MORE STATE INITIALISERS HERE IF REQUIRED       <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/************************************************************/

/***********************************************************************************************/
/*Initialise an array list of pointers to state for all the states in this finite state machine*/
/***********************************************************************************************/
static state* allStates[]={&CHECK_CHARGER,
					       &INITIAL_CHARGE,
					       &CHARGE_SUSPENDED,
					       &FLOAT_CHARGE
					       };           /* ADD MORE STATE NAMES TO THE INTIALISER HERE   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/***********************************************************************************************/

/*************************************************************************************************************/
/*Initialise present state counter...reset every state change, increments every second and does not roll over*/
/*************************************************************************************************************/
static lu_uint32_t presStateCounter=0;

/*****************************************************************/
/*Initialise a state machine structure with components from above*/
/*****************************************************************/
static fsm CHRG_LEAD_ACID_FSM = { &event,                /*event inputs*/
                                  &CHECK_CHARGER.stateCall,  /*presActiveState initialisation - should be the intended initialising state*/
                                  &CHARGE_SUSPENDED.stateCall, /*prevActiveState initialisation - can be any different state to presActive for initialisation*/
                                  &allStates[0],                  /*statesList initialisation*/
                                  &presStateCounter,
                                  0
                                  };
/*****************************************************************/


/**************************************************************************************************************************************/
/**************************************************************************************************************************************/
/*                                            FUNCTION DEFINITIONS                                                                    */
/**************************************************************************************************************************************/
/**************************************************************************************************************************************/
lu_int32_t set_vChargerTempCorrected(lu_int32_t vChargerNominalTemp,lu_int32_t tBatteryMeasured);



/**************************************************/
/*Call the finite state machine for each new event*/
/*This is the only call to the FSM from outside   */
/*except for access to some external group timers.*/
/*The return from this function allows specific   */
/*events (events other than timerTick) to return  */
/*an acknowledgement as to whether or not they    */
/*were acted on.                                  */
/**************************************************/
REPLY_STATUS CHRG_LEAD_ACID_FSM_MAIN(EventID eventIn)
{
    /*Add the new FSM event to the new FSM*/
	*(CHRG_LEAD_ACID_FSM.event) = eventIn;

	if(*(CHRG_LEAD_ACID_FSM.presActiveState) != *(CHRG_LEAD_ACID_FSM.prevActiveState) )
	{
		/*State has just changed so reset state seconds counter*/
		*(CHRG_LEAD_ACID_FSM.presActiveStateSecsCounter)=0;
	}

	/*Process all the fsm timers*/
	if(*(CHRG_LEAD_ACID_FSM.event) == TimerTickEvent)
	{
		/*Timer tick events are not specific CAN requests. They update conditions and times and so are always REPLY_STATUS_OKAY*/
		CHRG_LEAD_ACID_FSM.eventAcknowledge = REPLY_STATUS_OKAY;

		fsmTimersUpdate();

		/*Provided we won't roll over...*/
		if(*(CHRG_LEAD_ACID_FSM.presActiveStateSecsCounter) != 0xffffffff )
		{
			/*...update time in the present state*/
			*(CHRG_LEAD_ACID_FSM.presActiveStateSecsCounter)+=1;
		}
	}
	else
	{
		/*Events that require an acknowledge must assign this to be REPLY_STATUS_OKAY when they know*/
		/*that the event will be acted on in the unconditional or an exit state function*/
		CHRG_LEAD_ACID_FSM.eventAcknowledge = REPLY_STATUS_OPERATE_REJECT_ERROR;
	}

	/*...Call the present active state to check for exits*/
    (*CHRG_LEAD_ACID_FSM.presActiveState)( &CHRG_LEAD_ACID_FSM );

	return CHRG_LEAD_ACID_FSM.eventAcknowledge;
}
/*************************************************/


/*******************************************************************************************/
/*This function simply terminates the exit search for any list of state condition exits*****/
/*It must always be positioned at the end of the array of function pointers to exit condition*/
/*******************************************************************************************/
static lu_bool_t stateExitSearchEnd(fsm *this)
{
	PARAM_NOT_USED(this);
    return LU_TRUE;
}

/*******************************************/
/* Process global condition function exits */
/*******************************************/
static lu_bool_t CHRG_LEAD_ACID_GLOBAL_Exit_Conditions(fsm *this)
{
	lu_bool_t retValue=LU_FALSE;
	lu_uint8_t nextExitFunc=0;
    /*Execute each CHRG_LEAD_ACID_GLOBAL conditional exit function test in turn*/
    /*If valid exit found take it*/
    while(CHRG_LEAD_ACID_GLOBAL_ExitFuncs[nextExitFunc](this)==LU_FALSE)
    {
        nextExitFunc++;
    }
	/*Test to see if last exit function was just the search terminating function*/
	if(CHRG_LEAD_ACID_GLOBAL_ExitFuncs[nextExitFunc] != stateExitSearchEnd)
	{
        /*If here and not the exit search terminating function it must be a valid CHRG_LEAD_ACID_GLOBAL exit*/
		retValue=LU_TRUE;
	}
	return retValue;
}

/******************************************************************************************/
/* CHRG_LEAD_ACID_GLOBAL exit condition function - CHRG_LEAD_ACID_GLOBAL_Exit_NoUsableBat */
/******************************************************************************************/
static lu_bool_t CHRG_LEAD_ACID_GLOBAL_Exit_GoToIdleRqst(fsm *this)
{
	lu_bool_t retValue = LU_FALSE;
	/*This global event is gated to run every 30 seconds because it is invasive */
    if(*(this->event) == GoToIdleEvent)
    {
		/*For GoToIdle we suspend operation in the Battery Analyser State */
		*(this->presActiveState)=CHECK_CHARGER_StateFunc;
		/*Publish the current state number*/
		VIRT_BT_CHGR_FSM_STATE = CHECK_CHARGER_STATE;

		/*FLAG UP we are exiting the charging FSM */
		VIRT_CH_SYSTEM_RUNNING = FLAG_CLR;
		/*FLAGUP we are stopping charge current to the battery*/
		VIRT_CH_ON =  FLAG_CLR;
		/*We are never considered fully charged when disconnected from the charger*/
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		/*Disconnect the charger from the battery */
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*SWITCH THE CHARGER OFF*/
		BAT_CHARGER_EN = CTRL_OFF;
		/*Publish charger is off*/
		VIRT_CHARGER_EN = LU_FALSE;

		/* Reset charger current */
		if( batteryParamsPtr->BatteryNomCapacity/4 > MAX_CHARGER_ABILITY)
		{
			BAT_CHARGER_CUR_LIM_SET = MAX_CHARGER_ABILITY;
		}
		else
		{
			BAT_CHARGER_CUR_LIM_SET = batteryParamsPtr->BatteryNomCapacity/4;
		}
		/*Publish the charger's current limit setting*/
		VIRT_CH_SET_CURRENT = BAT_CHARGER_CUR_LIM_SET;

		/*Terminate the search for exit conditions*/
		retValue = LU_TRUE;
    }
	return retValue;
}

/******************************************************/
/******************************************************/
/******************************************************/
/* CHECK_CHARGER_ state function definitions          */
/******************************************************/
/******************************************************/
/******************************************************/
static void CHECK_CHARGER_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
	
    /*Check if at entry and if so execute entry code */   
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
		CHECK_CHARGER_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    CHECK_CHARGER_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_LEAD_ACID_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(CHECK_CHARGER_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*   CHECK_CHARGER_ entry function definition         */
/******************************************************/
/******************************************************/
static void CHECK_CHARGER_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = CHECK_CHARGER_STATE;

	SCHEDULED_TEST_ACCEPETED = LU_FALSE;
	/*DISCONNECT CHARGER FROM BATTERY*/
	BAT_TO_MOT_CONNECT = CTRL_OFF;
	/*Publish charger is disconnected from battery*/
	VIRT_CHARGER_CON = LU_FALSE;
	/*SWITCH ON CHARGER*/
	BAT_CHARGER_EN = CTRL_ON;
	/*Publish charger is on*/
	VIRT_CHARGER_EN = LU_TRUE;
	/*FLAG UP we are running the charging FSM */
	VIRT_CH_SYSTEM_RUNNING = FLAG_SET;
	/*FLAGUP we re not yet delivering charge current to the battery*/
	VIRT_CH_ON =  FLAG_CLR;
	/* Set the Fully charged flag to false */
	VIRT_BT_FULLY_CHARGED = LU_FALSE;
	/*Set the Battery charger current limit*/
	if( (batteryParamsPtr->BatteryNomCapacity/4) > MAX_CHARGER_ABILITY)
	{
		BAT_CHARGER_CUR_LIM_SET = MAX_CHARGER_ABILITY;
	}
	else
	{
		BAT_CHARGER_CUR_LIM_SET = batteryParamsPtr->BatteryNomCapacity/4;
	}
	/*Publish the charger's current limit setting*/
	VIRT_CH_SET_CURRENT = BAT_CHARGER_CUR_LIM_SET;
	/*SET CHARGER'S VOLTAGE TO MAX FOR THIS CHEMISTRY OF BATTERY*/
	setLdAcidChargerVolts(CHARGER_TEST_V);
	/*CLEAR TEMPORAL EXIT FLAG*/
	EXIT_FLAG_TO_INITIAL_CHARGE_STATE = LU_FALSE;
}
/******************************************************/
/*  CHECK_CHARGER_ unconditional function definition  */
/******************************************************/
static void CHECK_CHARGER_StateUncondFunc(fsm *this)
{
	lu_int32_t vChargerMeasured;
	static lu_int8_t ChargerSettingFaultCount = 0;

	if(	BAT_V_SENSE > BATTERY_PRESENT )
	{
		setLdAcidBatteryPresenceIndicator(LU_TRUE);
	}
	else
	{
		setLdAcidBatteryPresenceIndicator(LU_FALSE);
	}


	/*ALLOW SOME STABILISATION TIME FOR THE CHARGER AS JUST SWITCHED ON IN ENTRY FUNCTION*/
	if( *this->presActiveStateSecsCounter > 5)
	{
		/*READ BACK THE CHARGER'S VOLTAGE*/
		vChargerMeasured = CHARGER_V_SENSE;

		/*TEST THE CHARGER'S VOLTAGE AND IF OUT OF LIMITIS FLAG IT AND USE RED CHARGER LED*/
		if(vChargerMeasured < CHARGER_MIN_V_LIM || vChargerMeasured > CHARGER_MAX_V_LIM )
		{
			ChargerSettingFaultCount++;

			/* We allow the charger 5 attempts at starting before raising any faults */
			if( ChargerSettingFaultCount >= 5)
			{
				VIRT_CH_VOLTAGE_FAULT = FLAG_SET;
				VIRT_CH_FAULT = FLAG_SET;
				/* Reset the ChargerSettingFaultCounter */
				ChargerSettingFaultCount = 0;
			}

			EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE = LU_TRUE;
		}
		else
		{
			/* Reset the ChargerSettingFaultCounter */
			ChargerSettingFaultCount = 0;

			/*CLEAR THE CHARGER_VOLTAGE_SETTING_FAULT FLAGS*/
			VIRT_CH_VOLTAGE_FAULT = FLAG_CLR;
			VIRT_CH_FAULT = FLAG_CLR;

			/* We can only proceed to charging if the charger shows no fault */
			EXIT_FLAG_TO_INITIAL_CHARGE_STATE = LU_TRUE;
		}
	}
}

/******************************************************/
static lu_bool_t CHECK_CHARGER_StateExitTo_INITIAL_CHARGE(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	/*Charge phase status flags are determined in the unconditional function of this state*/
    	if( EXIT_FLAG_TO_INITIAL_CHARGE_STATE == LU_TRUE)
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=INITIAL_CHARGE_StateFunc;
			/*Execute actions for this transition*/

			/*Set the Battery charger current limit*/
			if( batteryParamsPtr->BatteryNomCapacity/4 > MAX_CHARGER_ABILITY)
			{
				BAT_CHARGER_CUR_LIM_SET = MAX_CHARGER_ABILITY;
			}
			else
			{
				BAT_CHARGER_CUR_LIM_SET = batteryParamsPtr->BatteryNomCapacity/4;
			}
			/*Publish the charger's current limit setting*/
			VIRT_CH_SET_CURRENT = BAT_CHARGER_CUR_LIM_SET;
			/*Set the Battery charger to an easy to achieve voltage for the charger...*/
			/*The INITIAL CHARGE state will correct this as it gets going*/
			set_vChargerTempCorrected(batteryParamsPtr->BatteryPackDeepDischargeVolts/batteryOpts->BatteryPackNoOfRows, BAT_TEMP);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}

/******************************************************/
static lu_bool_t CHECK_CHARGER_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	/*Charge phase status flags are determined in the unconditional function of this state*/
    	if( EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE == LU_TRUE )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState) = CHECK_CHARGER_StateFunc;
			/*Change the previous state to NULL, to force the State Entry */
			*(this->prevActiveState) = NULL;

			/* Reset the SecsCounter to 0 */
			*(this->presActiveStateSecsCounter) = 0;
			/*Execute actions for this transition*/

			/* Clear the flag */
			EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE = LU_FALSE;
			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }

    return retVal;
}
/******************************************************/
static lu_bool_t CHECK_CHARGER_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	if( BAT_TEMP/MILLIDEGREES_TO_DEGREES > batteryParamsPtr->BatteryMaxTemp)
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=CHARGE_SUSPENDED_StateFunc;

			/*DISCONNECT CHARGER FROM BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_OFF;
			/*Publish charger is disconnected from battery*/
			VIRT_CHARGER_CON = LU_FALSE;
			/*FLAGUP we are stopping delivering charge current to the battery*/
			VIRT_CH_ON =  FLAG_CLR;
			/*FLAG UP we are over temperature*/
			VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
			/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
			setChargerTemperatureDisable(LU_TRUE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}

/******************************************************/
/******************************************************/
/******************************************************/
/* INITIAL_CHARGE_ state function definitions         */
/******************************************************/
/******************************************************/
/******************************************************/
static void INITIAL_CHARGE_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;

    /*Check if at entry and if so execute entry code */  
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	INITIAL_CHARGE_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}	
    /*Execute any unconditional code */
    INITIAL_CHARGE_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_LEAD_ACID_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(INITIAL_CHARGE_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*  INITIAL_CHARGE_ entry function definition         */
/******************************************************/
/******************************************************/
static void INITIAL_CHARGE_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = INITIAL_CHARGE_STATE;

	/*Set Maximum initial charge time to the time taken to charge from empty to full*/
	maxInitialChargeTimer = ( (batteryParamsPtr->BatteryNomCapacity*USE_TWENTY_PERCENT_MARGING)/BAT_CHARGER_CUR_LIM_SET)*SECONDS_PER_HOUR;

	/*CONNECT CHARGER TO BATTERY*/
	BAT_TO_MOT_CONNECT = CTRL_ON;
	/*Publish charger is disconnected from battery*/
	VIRT_CHARGER_CON = LU_TRUE;
	/*FLAGUP we are now delivering charge current to the battery*/
	VIRT_CH_ON =  FLAG_SET;

}
/******************************************************/
/*  INITIAL_CHARGE_ unconditional function definition */
/******************************************************/
static void INITIAL_CHARGE_StateUncondFunc(fsm *this)
{
	static lu_uint32_t batteryFaultCountQualifier = 0;

	/*Give the charger a 5sec delay before adjusting to the correct & temp compensated charger voltage*/
	if(*this->presActiveStateSecsCounter > FIVE_SECONDS_IN_THIS_STATE)
	{
		/*Set the Battery charger to the calculated temperature corrected voltage*/
		set_vChargerTempCorrected(batteryParamsPtr->BatteryChargeVolts,BAT_TEMP);
	}

	/*******************************************************************************************************/
	/*                           CHARGER FAILURE DETECTION                                                 */
	/*******************************************************************************************************/
	/* If the charger current exceeds 120% of the set current limit, set the Charger current fault flag */
	if((VIRT_CHARGER_CON == LU_TRUE) && (BAT_CHARGER_I_SENSE > (USE_TWENTY_PERCENT_MARGING * BAT_CHARGER_CUR_LIM_SET)))
	{
		VIRT_CH_CURRENT_FAULT = FLAG_SET;
	}
	else
	{
		VIRT_CH_CURRENT_FAULT = FLAG_CLR;
	}

	/* If either charger voltage or charger current faults have been set, set charger fault */
	if((VIRT_CH_VOLTAGE_FAULT == FLAG_SET) || (VIRT_CH_CURRENT_FAULT == FLAG_SET))
	{
		VIRT_CH_FAULT = FLAG_SET;
	}
	else
	{
		VIRT_CH_FAULT = FLAG_CLR;
	}

	/*BATTERY CHARGING FAULT TEST*/
	if(BAT_V_SENSE > batteryParamsPtr->BatteryLowLevel )
	{
		/*Start again - set the end qualifying count out another 600 seconds*/
		batteryFaultCountQualifier = *this->presActiveStateSecsCounter + TEN_MINUTES_COUNT_SECONDS;
		/*Battery charging is ok*/
		VIRT_BT_FAULT = LU_FALSE;
	}
	else
	{
		/*if 600 consecutive seconds pass while on charge and battery volts remain below BATTERY_LOW level ...*/
		if( *this->presActiveStateSecsCounter > batteryFaultCountQualifier )
		{
			/*...that's a battery charging fault failure*/
			VIRT_BT_FAULT = LU_TRUE;
			batteryFaultCountQualifier = 0;
		}
	}
}
/******************************************************/
static lu_bool_t INITIAL_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	if( BAT_TEMP/MILLIDEGREES_TO_DEGREES > batteryParamsPtr->BatteryMaxTemp)
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=CHARGE_SUSPENDED_StateFunc;

			/*DISCONNECT CHARGER FROM BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_OFF;
			/*Publish charger is disconnected from battery*/
			VIRT_CHARGER_CON = LU_FALSE;
			/*FLAGUP we are stopping delivering charge current to the battery*/
			VIRT_CH_ON =  FLAG_CLR;
			/*FLAG UP we are over temperature*/
			VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
			/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
			setChargerTemperatureDisable(LU_TRUE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}
/******************************************************/
static lu_bool_t INITIAL_CHARGE_StateExitTo_FLOAT_CHARGE(fsm *this)
{
	PARAM_NOT_USED(this);
    lu_bool_t retVal = LU_FALSE;

	if( (BAT_CHARGER_I_SENSE < (batteryParamsPtr->BatteryNomCapacity/20)) &&
		(*this->presActiveStateSecsCounter > TEN_SECONDS_IN_THIS_STATE )      )
	{
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=FLOAT_CHARGE_StateFunc;

		/* Disconnect charger from the battery*/

		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;
	}

    return retVal;
}
/******************************************************/
static lu_bool_t INITIAL_CHARGE_StateExitTo_FLOAT_CHARGE_BY_TIMEOUT(fsm *this)
{
	PARAM_NOT_USED(this);
    lu_bool_t retVal = LU_FALSE;

    /*This time delay is dependent on battery capacity and charge rate*/
    if( (lu_int32_t)(*this->presActiveStateSecsCounter) > maxInitialChargeTimer )
    {
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=FLOAT_CHARGE_StateFunc;

		/* Disconnect charger from the battery*/


		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;

    }
    return retVal;
}

/******************************************************/
/******************************************************/
/******************************************************/
/* CHARGE_SUSPENDED_ state function definitions       */
/******************************************************/
/******************************************************/
/******************************************************/
static void CHARGE_SUSPENDED_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
	
	/*Check if at entry and if so execute entry code */  
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	CHARGE_SUSPENDED_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    CHARGE_SUSPENDED_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_LEAD_ACID_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(CHARGE_SUSPENDED_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*  CHARGE_SUSPENDED_ entry function definition       */
/******************************************************/
/******************************************************/
static void CHARGE_SUSPENDED_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = CHARGE_SUSPENDED_STATE;
}
/******************************************************/
/* CHARGE_SUSPENDED_ unconditional function definition */
/******************************************************/
static void CHARGE_SUSPENDED_StateUncondFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish battery current as 0 i.e. battery is being not being charged but also not being used*/
	VIRT_BT_BATTERY_CURRENT = 0;

}
/******************************************************/
static lu_bool_t CHARGE_SUSPENDED_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
    if(*(this->event) == TimerTickEvent)
    {
    	/*Compare monitored and normalised temperature with temperature charging threshold*/
		if( (BAT_TEMP/MILLIDEGREES_TO_DEGREES) < batteryParamsPtr->BatteryMaxTemp)
		{
			/*Change the active state ready for next call to this FSM*/
            *(this->presActiveState)=CHECK_CHARGER_StateFunc;
			/*Execute actions for this transition*/

			/*Publish charger temperature ok */
			VIRT_BT_OVER_TEMPERATURE = FLAG_CLR;

			/*Tell the BatteryManagerFSM you can restore charging due to temperature*/
			setChargerTemperatureDisable(LU_FALSE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}
/******************************************************/
/******************************************************/
/******************************************************/
/*    FLOAT_CHARGE_ state function definitions        */
/******************************************************/
/******************************************************/
/******************************************************/
static void FLOAT_CHARGE_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;

    /*Check if at entry and if so execute entry code */
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	FLOAT_CHARGE_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    FLOAT_CHARGE_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_LEAD_ACID_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(FLOAT_CHARGE_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*  FLOAT_CHARGE_ entry function definition           */
/******************************************************/
/******************************************************/
static void FLOAT_CHARGE_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = FLOAT_CHARGE_STATE;
	fsmTimerSet(&batteryFullStabilisationTimer, FULL_CHARGE_DELAY_TIME);

	setLdAcidScheduledTestAcknowledge(LU_FALSE);
}

/******************************************************/
/* FLOAT_CHARGE_ unconditional function definition */
/******************************************************/
static void FLOAT_CHARGE_StateUncondFunc(fsm *this)
{
	static lu_int32_t vBatApparentWithoutCharger=0;
	static lu_bool_t  ALLOW_MISSING_BATTERY_DETECTION_BY_VOLTAGE=LU_FALSE;
	static lu_int8_t ChargerVoltageFaultCount = 0;
	lu_int32_t vChargerSet = 0;
	lu_int32_t vChargerMeasured = 0;

	/*Set the Battery charger to the calculated temperature corrected Float Voltage*/
	vChargerSet = set_vChargerTempCorrected(batteryParamsPtr->BatteryCellFloatChargeVolts,BAT_TEMP);

    if(*(this->event) == TimerTickEvent)
    {
    	/*******************************************************************************************************/
		/*                           CHARGER FAILURE DETECTION                                                 */
		/*******************************************************************************************************/

    	/* 1. Voltage - Check that the charger is outputting a sensible voltage */
		/*READ BACK THE CHARGER'S VOLTAGE*/
		vChargerMeasured = CHARGER_V_SENSE;

		/*TEST THE CHARGER'S VOLTAGE If it is less than the Deep Discharge voltage something bad has happened */
		if(vChargerMeasured < batteryParamsPtr->BatteryPackDeepDischargeVolts)
		{
			ChargerVoltageFaultCount++;

			/* If after 5 attempts the charger voltage is still low, exit to the check charger state */
			if( ChargerVoltageFaultCount >= 5)
			{
				EXIT_FLAG_FLOAT_TO_CHECK_CHARGER_STATE = LU_TRUE;
			}
		}
		else
		{
			/* Reset the ChargerSettingFaultCounter */
			ChargerVoltageFaultCount = 0;
		}

		/* 2. Current - Check that the charger current does not exceed 120% of it setting */
		/* If the charger current exceeds 120% of the set current limit, set the Charger current fault flag */
		if((VIRT_CHARGER_CON == LU_TRUE) && (BAT_CHARGER_I_SENSE > (1.2 * BAT_CHARGER_CUR_LIM_SET)))
		{
			VIRT_CH_CURRENT_FAULT = FLAG_SET;
		}
		else
		{
			VIRT_CH_CURRENT_FAULT = FLAG_CLR;
		}

		/* If either charger voltage or charger current faults have been set, set charger fault */
		if((VIRT_CH_VOLTAGE_FAULT == FLAG_SET) || (VIRT_CH_CURRENT_FAULT == FLAG_SET))
		{
			VIRT_CH_FAULT = FLAG_SET;
		}
		else
		{
			VIRT_CH_FAULT = FLAG_CLR;
		}

    	/*******************************************************************************************************/
    	/*                           BATTERY FULL DETECTION                                                    */
    	/*******************************************************************************************************/
		/* Battery is only fully charged if,
		 * the charger is connected
		 * the charger is providing a valid voltage
		 * and the current is bellow a certain threshold 0.01 of the capacity */

		/* Allow for a stabilisation time once entering float */
		if(fsmTimerGet(&batteryFullStabilisationTimer) == FSM_TIMER_TIMED_OUT)
		{
			/* Ensure the charger was not disconected otherwise the current will be 0 */
			if((*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) > BATTERY_DETECT_BY_VOLTAGE_TEST_PERIOD)
			{
				if((BAT_TO_MOT_CONNECT == CTRL_ON) &&
					(vChargerMeasured > batteryParamsPtr->BatteryPackDeepDischargeVolts) &&
					(BAT_CHARGER_I_SENSE < batteryParamsPtr->BatteryNomCapacity*ONE_PERCENT_OF))
				{
					VIRT_BT_FULLY_CHARGED = LU_TRUE;
				}

				/* Include a little hysterisis to avoid the Fully charged signal bouncing */
				if(BAT_CHARGER_I_SENSE > batteryParamsPtr->BatteryNomCapacity*ONE_POINT_FOUR_PERCENT_OF)
				{
					VIRT_BT_FULLY_CHARGED = LU_FALSE;
				}
			}
		}

		/*******************************************************************************************************/
		/*                           BATTERY PRESENCE DETECTION                                                */
		/*******************************************************************************************************/
		/*If charging current is too low to say there is definitely a battery present enable voltage procedure*/
		if(BAT_CHARGER_I_SENSE < BATTERY_PRESENCE_DETECT_BY_CURRENT )
		{
			/*This can only become true outside of the active test steps which run from 0 t0 3 in the two LSBs*/
			if( (*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) >= BATTERY_DETECT_BY_VOLTAGE_TEST_PERIOD)
			{
				ALLOW_MISSING_BATTERY_DETECTION_BY_VOLTAGE = LU_TRUE;
			}
		}
		/*use a little hysteresis here*/
		else if(BAT_CHARGER_I_SENSE > BATTERY_PRESENCE_DETECT_BY_CURRENT_HYSTERISIS )
		{
			ALLOW_MISSING_BATTERY_DETECTION_BY_VOLTAGE = LU_FALSE;
			setLdAcidBatteryPresenceIndicator(LU_TRUE);
		}
		/*Do we need to detect battery presence by voltage...*/
		/*...this requires us to disconnect charger voltage...*/
		/*...to be able to see the battery voltage. This has...*/
		/*...to be done in a stepped sequence of calls to... */
		/*...this function*/
		if(ALLOW_MISSING_BATTERY_DETECTION_BY_VOLTAGE == LU_TRUE)
		{
			/*MULTI TIMER TICK EVENT SEQUENCE*/
			/*First few counts (i.e second intervals) of every repeat cycle of counts while in this state are used for test steps*/
			if( (*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) < BATTERY_DETECT_BY_VOLTAGE_TEST_PERIOD)
			{
				/*Step 3 - */
				if( (*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) == TIME_FOR_TEST_STEP3)
				{
					/*Opportunity to publish the actual battery voltage*/
					VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;
					/*Take voltage reading of charging battery*/
					vBatApparentWithoutCharger = BAT_V_SENSE;

					/*Re-connect the charger*/
					BAT_TO_MOT_CONNECT = CTRL_ON;
					/*Publish charger is connected to battery*/
					VIRT_CHARGER_CON = LU_TRUE;

					/*Evaluate whether or not we think the battery has gone missing*/
					if(vBatApparentWithoutCharger < BATTERY_PRESENT )
					{
						/*It looks like the battery has gone walkies...*/
						/*...publishing this means the Battery Manager FSM will*/
						/*force this FSM to Idle and places itself in to the Battery Not Present state */
						setLdAcidBatteryPresenceIndicator(LU_FALSE);
						VIRT_BT_FULLY_CHARGED = LU_FALSE;
					}
					else
					{
						setLdAcidBatteryPresenceIndicator(LU_TRUE);
					}
				}
				/*Step 2 - */
				if( (*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) == TIME_FOR_TEST_STEP2)
				{
					/*Not used - just waiting*/
					/*Opportunity to publish the actual battery voltage (i.e. no charger holding it up)*/
					VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;
				}
				/*Step 1 - */
				if((*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) == TIME_FOR_TEST_STEP1)
				{
					/*Not used - just waiting*/
					/*Opportunity to publish the actual battery voltage (i.e. no charger holding it up)*/
					VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;
				}
				/*Step 0 - */
				if( (*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) == TIME_FOR_TEST_STEP0)
				{
					/*Disconnect the charger*/
					BAT_TO_MOT_CONNECT = CTRL_OFF;
					/*Publish charger is disconnected from battery*/
					VIRT_CHARGER_CON = LU_FALSE;
				}
			}
		}
		/*******************************************************************************************************/
		/*******************************************************************************************************/
	}

    /*The Battery Manager has notified us by this event it wants to execute a scheduled battery test*/
    /*This is the only charger state to accept such a request*/
    /*By publishing we accept here, the battery manager will read this,put us to sleep, and execute the test */
    /*If we are unable to acknowledge this event the battery manager will send it again and again until we do*/
    if( (*(this->event) == ScheduledTestEvent) )
    {
    	/*If we have not yet switched to voltage detection for missing battery check...*/
    	if(ALLOW_MISSING_BATTERY_DETECTION_BY_VOLTAGE == LU_FALSE)
    	{
    		/*...its a straight forward case to acknowledge*/
    		setLdAcidScheduledTestAcknowledge(LU_TRUE);
    	}
    	/*If we have switched to voltage detection for the missing battery check...*/
    	else if( (*this->presActiveStateSecsCounter & BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD) >= BATTERY_DETECT_BY_VOLTAGE_TEST_PERIOD)
    	{
    		/*...it can only be acknowledged outside the active test steps sequence*/
    		setLdAcidScheduledTestAcknowledge(LU_TRUE);
    	}
    }

}
/******************************************************/
static lu_bool_t FLOAT_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
   lu_bool_t retVal = LU_FALSE;

	if(*(this->event) == TimerTickEvent)
	{
		if( BAT_TEMP/MILLIDEGREES_TO_DEGREES > batteryParamsPtr->BatteryMaxTemp)
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=CHARGE_SUSPENDED_StateFunc;

			/*DISCONNECT CHARGER FROM BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_OFF;
			/*Publish charger is disconnected from battery*/
			VIRT_CHARGER_CON = LU_FALSE;
			/*FLAGUP we are stopping delivering charge current to the battery*/
			VIRT_CH_ON =  FLAG_CLR;
			/*FLAG UP we are over temperature*/
			VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
			/* We are no longer charging so the battery should no longer be Full */
			VIRT_BT_FULLY_CHARGED = LU_FALSE;
			/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
			setChargerTemperatureDisable(LU_TRUE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
	}
	return retVal;
}

/******************************************************/
static lu_bool_t FLOAT_CHARGE_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	/*Charge phase status flags are determined in the unconditional function of this state*/
    	if( EXIT_FLAG_FLOAT_TO_CHECK_CHARGER_STATE == LU_TRUE )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState) = CHECK_CHARGER_StateFunc;

			/*Execute actions for this transition*/

			/* Clear the flag */
			EXIT_FLAG_FLOAT_TO_CHECK_CHARGER_STATE = LU_FALSE;

			/* We are no longer charging so the battery should no longer be Full */
			VIRT_BT_FULLY_CHARGED = LU_FALSE;

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }

    return retVal;
}

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/* TIMER FUNCTION DEFINITIONS                                                                         */
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/******************************************************/
/******************************************************/
/* INTERNAL FIXED STATE MACHINE TIMERS                */
/******************************************************/
/******************************************************/

/***************************************************************/
/*Decrements all internal running timers every timer tick event*/
/*to this FSM                                                  */
/***************************************************************/
static void fsmTimersUpdate(void)
{
	lu_uint32_t fsmTimersIndx = 0;
	/*Keep looping until declared timers are done*/
	while( fsmTimers_p[fsmTimersIndx] != NULL_FSM_TIMER )
	{
		/*If the contents of the indexed pointer to timer have not reached 0...*/
		if( *fsmTimers_p[fsmTimersIndx] > 0 )
		{
			/*...then decrement the contents*/
			(*fsmTimers_p[fsmTimersIndx])--;
		}
		fsmTimersIndx++;
	}
}

static void fsmTimerSet( lu_int32_t* timerName, lu_int32_t timerValue)
{
	    /*Store the timer value as the contents of the timer pointed to*/
		*timerName = timerValue;
}

static int fsmTimerGet( lu_int32_t* timerName )
{
	    /*Store the timer value as the contents of the timer pointed to*/
		return  *timerName;
}


/******************************************************/

/******************************************************/

/* ADD MORE FUNCTION DEFINITIONS FOR ADDITIONAL STATES BELOW HERE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/* Remember each State must have an entry function, and unconditional function a number of exit condition functions and these placed in an array*/


/*Add further support function here*/


/*********************************************************/
/*Takes the voltage required at nominal temperature      */
/*corrects this to the voltage for the actual temperature*/
/*and sets the charger to this corrected voltage         */
/*********************************************************/
lu_int32_t set_vChargerTempCorrected(lu_int32_t vChargerNominalTemp,lu_int32_t tBatteryMeasured)
{
    /*NVRAM STORED BATTERY ATTRIBUTES AND OTHER CONSTANTS TO BE READ IN...x100 for accuracy*/
	lu_int32_t vTempCoefficient    =  TEMP_COMP_COEFFICIENT_LEAD_ACID;
    lu_int32_t tempToCorrect = tBatteryMeasured;
    lu_int32_t MaxBatTempInMilliDegrees = batteryParamsPtr->BatteryMaxTemp*1000;

    /*Belt and braces - If the temperature is too high use the max temp instead*/
	if(tBatteryMeasured > MaxBatTempInMilliDegrees)
	{
		tempToCorrect = MaxBatTempInMilliDegrees;
	}

	/*Calculate a value for the temperature corrected Voltage*/
	vChargerSetting = (vChargerNominalTemp + (vTempCoefficient*(tempToCorrect - NOMINAL_BATTERY_TEMP))/1000 );

	/*Set the Battery charger to the calculated temperature corrected Float Voltage*/
	setLdAcidChargerVolts(vChargerSetting);
	return vChargerSetting;
}

lu_bool_t getLdAcidChargingLevelFsmBatAnalysis(void)
{
	return !BATTERY_NOT_USABLE_FLAG;
}

/*Read access function*/
lu_bool_t getLdAcidScheduledTestAcknowledge(void)
{
	/*WARNING - should only be called by the BAT MANAGR FSM when a scheduled test is waiting to start */
	/*If we can allow a scheduled test tidy up */
	if(SCHEDULED_TEST_ACCEPETED == LU_TRUE)
	{
		/*FLAG UP we will be exiting the charging FSM */
		VIRT_CH_SYSTEM_RUNNING = FLAG_CLR;
		/*FLAG UP we are stopping charge current to the battery*/
		VIRT_CH_ON =  FLAG_CLR;
		/*We are never considered fully charged when disconnected from the charger*/
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		/*Disconnect charger from the battery*/
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*SWITCH THE CHARGER OFF*/
		BAT_CHARGER_EN = CTRL_OFF;
		/*Publish charger is off*/
		VIRT_CHARGER_EN = LU_FALSE;
	}
	return SCHEDULED_TEST_ACCEPETED;

}
/*Write access function*/
void setLdAcidScheduledTestAcknowledge(lu_bool_t SchedTestOk)
{
	SCHEDULED_TEST_ACCEPETED = SchedTestOk;
}

/*Read access function*/
lu_bool_t getLdAcidBatteryPresenceIndicator(void)
{
	return BatteryPresenceIndicator;
}
/*Write access function*/
void setLdAcidBatteryPresenceIndicator(lu_bool_t battPres)
{
	BatteryPresenceIndicator = battPres;
}

/*Read access function*/
chrgHlthStatus getLdAcidChargerHealthStatus(void)
{
	return ChargerHealthStatus;
}
/*Write access function*/
void setLdAcidChargerHealthStatus(chrgHlthStatus chrgStat)
{
	ChargerHealthStatus = chrgStat;
}

/*Read access for Battery Manager FSM to determine the calculated temperature corrected voltage of the charger*/
lu_int32_t getLdAcidChargerVolts(void)
{
	return vChargerSetting;
}

void setLdAcidChargerVolts(lu_int32_t vChargerTarget)
{
	/*Update the charger's compliance voltage setting*/
	BAT_CHARGER_VOLT_SET = vChargerTarget;
	/*Publish the charger's compliance voltage setting*/
	VIRT_CH_SET_VOLTAGE = vChargerTarget;
}

