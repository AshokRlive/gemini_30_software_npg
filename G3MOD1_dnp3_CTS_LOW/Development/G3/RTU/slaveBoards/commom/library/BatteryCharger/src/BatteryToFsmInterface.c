/*
 * BatteryToFsmInterface.c
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */
/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Charger FSM module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 *******************************************************************************/

/*******************************************************************************/
/* INCLUDES - Standard Library Modules Used                                    */
/*******************************************************************************/

/*******************************************************************************/
/* INCLUDES - Project Specific Modules Used                                    */
/*******************************************************************************/

/******************************/
/* Testing and debug only     */
/******************************/


/*****************************/

#include "systemStatus.h"

#include "BatteryChargerTopLevel.h"
#include "BatteryToFsmInterface.h"
#include "BatManagerFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "systemTime.h"
#include "LinearInterpolation.h"
#include "NVRAMDef/NVRAMDefOptBat.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmBatteryChargerEnum.h"
#include "BatteryFailSafes.h"

/*******************************************************************************/
/* LOCAL - Definitions And Macros                                              */
/*******************************************************************************/

/*******************************************************************************/
/* LOCAL - Typedefs and Structures                                             */
/*******************************************************************************/

/********************************************************************************/
/* LOCAL - Prototypes Of Local Functions                                        */
/********************************************************************************/
#if 0
void BatManager2ndInit(void);
#endif

void SendTimerTickEventToFSM(void);
void SendMotorSupplyOnEventToFSM(void);
void  SendMotorSupplyCancelEventToFSM(void);
REPLY_STATUS  SendChargerInhibitEventToFSM(BatteryOperateStr * tstData);
REPLY_STATUS  SendChargerInhibitCancelEventToFSM(BatteryCancelStr * tstData);
REPLY_STATUS  SendBatteryTestEventToFSM(BatteryOperateStr * tstData);
REPLY_STATUS  SendBatteryTestCancelEventToFSM( BatteryCancelStr * tstData );
REPLY_STATUS  SendChargerTestEventToFSM(BatteryOperateStr * tstData);
REPLY_STATUS  SendBatteryResetStatsEventToFSM(BatteryResetStr *rstData);

REPLY_STATUS SendBatteryDisconCancelEventToFSM(BatteryCancelStr * tstData);
REPLY_STATUS SendBatteryDisconEventToFSM(BatteryOperateStr * tstData);
REPLY_STATUS SendBatTestInhibitCancelEventToFSM(BatTestInhibCancelStr * tstData);
REPLY_STATUS SendBatTestInhibitEventToFSM(BatTestInhibOperateStr * tstData);

/********************************************************************************/
/* EXPORTED - Variables                                                         */
/********************************************************************************/

NVRAMDefOptBatStr *batteryOpts;

BatteryDataNVRAMBlkStr	*batteryDataNVRamBlkPtr;
BatteryParamsStr  	batteryParams, *batteryParamsPtr;
BatteryStateStr	  	batteryState , *batteryStatePtr;
ChargerStateStr	  	chargerState , *chargerStatePtr;
BatteryTestStateStr testState    , *testStatePtr;

/********************************************************************************/
/* LOCAL - Global Variables                                                         */
/********************************************************************************/

static lu_float32_t	chargerVoltsCal;

static lu_uint8_t	battState;



/**********************/
/**********************/
/* Exported Functions */
/**********************/
/**********************/
lu_bool_t BatManagerFsmInit(void);

/***************************************************************************************/
/*Initialises the Data for the Battery Manager Finite State Machine (ie BatManagerFSM) */
/***************************************************************************************/
lu_bool_t BatManagerFsmInit(void)
{
	lu_bool_t sensorIsPresent = LU_FALSE;

	SB_ERROR 	sbError;
	batteryParamsPtr 	= &batteryParams ;
	batteryStatePtr 	= &batteryState ;
	chargerStatePtr 	= &chargerState ;
	testStatePtr 		= &testState ;
	battState			= BATT_HEALTHY;
	updateDataNVRAM		= LU_FALSE;
	chargerVoltsCal		= 1.0;

	/*First try to get batteryOpts pointer by looking in the eeprom battery ID memory cache...*/
	sbError = NVRamBatteryIndenityGetMemoryPointer(NVRAM_BATT_ID_BLK_OPTS, (lu_uint8_t**)&batteryOpts);
	if (sbError != SB_ERROR_NONE)
	{   /*...we got some kind of error there so now try looking in the eeprom battery data memory cache...*/
		sbError = NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK_OPTS, (lu_uint8_t**)&batteryOpts);
		if (sbError != SB_ERROR_NONE)
		{
			/* Set virtual point to indicate NVRAM failure */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_ID_NVRAM_FAIL, LU_TRUE);

			/* Major error - no factory info is available */
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER,
								SYSALC_BATTERY_CHARGER_TEMP_SENSOR_NVRAM,
								0 );
		}
		else
		{
			/* Reset virtual point to indicate NVRAM access is okay */
			sbError = IOManagerSetValue( IO_ID_VIRT_BT_ID_NVRAM_FAIL, LU_FALSE);

			/*...Success with finding battery opts in eeprom battery data memory cache*/
			sensorIsPresent = LU_TRUE;
		}
	}
	else
	{
		/*Success with finding battery opts in the eeprom battery ID memory cache*/
		sensorIsPresent = LU_TRUE;
	}

	/* If no sensor was detected we load fail safe battery options to allow safe charging,
	 * Set virtual point to indicate if using failsafe params */
	if(sensorIsPresent == LU_TRUE)
	{
		VIRT_BT_TYPE_DEFAULT = LU_FALSE;
	}
	else
	{
		batteryOpts = &batteryOptsFailSafe;
		VIRT_BT_TYPE_DEFAULT = LU_TRUE;
	}

	/*Fill local battery parameter structure from values in NVRAM ID Structure (or default values if no NVRAM found)*/
															  /*Indicates type of battery in use	e.g. Lead Acid, NiMH	*/
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryType 							= (BATT_TYPE)batteryOpts->BatteryChemistry;

															  /*The voltage to determine we are fully charged*/
//SRM - set permanently to 26.42V (i.e. 26420mV) pending new approach to this
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryFullChargeVolts				= 26420; //batteryOpts->BatteryCellFullChargeVolts* batteryOpts->BatteryPackNoOfRows;
															  /*Maximum capacity of each cell*/
	/*USED BY LEAD ACID AND NiMH CHARGER FSMs*/
	batteryParamsPtr->BatteryNomCapacity 					= batteryOpts->BatteryCellMaxCapacity * batteryOpts->BatteryPackNoOfCols * batteryOpts->BatteryPackNoOfRows;
															  /*Maximum temperature at which normal charging is allowed	*/
	/*USED BY LEAD ACID AND NiMH CHARGER FSMs*/
	batteryParamsPtr->BatteryMaxTemp						= batteryOpts->BatteryPackMaxChargeTemp;

    /*What a mess this all is...for now hard coded values that differ will need to be defined like this*/
	/*USED BY LEAD ACID CHARGER FSM*/
	if(batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		/*APPARENTLY NOT USED*/
		batteryParamsPtr->BatteryChargeVolts					= 31000;
	}
	else
	{
		batteryParamsPtr->BatteryChargeVolts					= 29400;//14700; // batteryOpts->BatteryCellChargingVolts * batteryOpts->BatteryPackNoOfRows;
	}

	/*Level below which an operation cannot be performed		*/
	/*USED BY LEAD ACID AND NiMH CHARGER FSMs*/
	batteryParamsPtr->BatteryLowLevel						= batteryOpts->BatteryPackLowLevel; /* Float charging level of battery */

	if(batteryParamsPtr->BatteryType == BATT_TYPE_LEAD_ACID)
	{
		/*USED BY LEAD ACID CHARGER FSM*/
		batteryParamsPtr->BatteryCellFloatChargeVolts			= 27300;//13650; // (batteryOpts->BatteryCellChargingVolts * FLOAT_VOLTAGE_PERCENTAGE)/100;
	}
															  /*Battery Test Current Minimum (mA)*/
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryTestCurrentMinimum				= batteryOpts->BatteryTestCurrentMinimum;
															  /*Battery Test Maximum Duration Time Minutes*/
	//if(batteryParamsPtr->BatteryNomCapacity < 11000 )
	//{
//SRM - set permanently to 6 hours pending new approach
		/*NOT USED BY CHARGER FSMs*/
	//	batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes	= 180; //batteryOpts->BatteryTestMaximumDurationTimeMinutes;
	//}
	//else
	//{
	batteryParamsPtr->BatteryTestMaximumDurationTimeMinutes	= 360; //batteryOpts->BatteryTestMaximumDurationTimeMinutes;
	//}
															/*Battery Test Discharge Capacity as whole number */
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryTestDischargeCapacity			= batteryOpts->BatteryTestDischargeCapacity;
															  /*Battery Test Threshold Volts */
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryTestThresholdVolts				= 23000; /* batteryOpts->BatteryTestThresholdVolts; */
															  /*Battery Test Load Type (1=2amp PTC 2=10amp PTR R) */
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryTestLoadType					= batteryOpts->BatteryTestLoadType;
															  /*Battery Pack Shutdown Level Volts */
	/*NOT USED BY CHARGER FSMs*/
	batteryParamsPtr->BatteryPackShutdownLevelVolts			= batteryOpts->BatteryPackShutdownLevelVolts;
															  /*Battery Pack Deep Discharge Protection Level Volts */
    /*What a mess this all is...for now hard coded values that differ will need to be defined like this*/
	/*USED BY LEAD ACID AND NiMH CHARGER FSMs*/
	if(batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
		//SRM - set permanently to 21V (i.e. 21000mV) pending new approach to this
		batteryParamsPtr->BatteryPackDeepDischargeVolts			= 21000;
	}
	else
	{
		//SRM - set permanently to 21V (i.e. 21000mV) pending new approach to this
	batteryParamsPtr->BatteryPackDeepDischargeVolts			= 21000;//batteryOpts->BatteryPackDeepDischargeVolts;
	}

	/*NOT USED BY CHARGER FSMs*/
	if(batteryParamsPtr->BatteryType == BATT_TYPE_NI_MH)
	{
	    /*The minimum battery voltage for a Requested battery test(scheduled tests require fully charged) */
        //SRM - not in NVRam yet
	    batteryParamsPtr->BatteryMinStartVoltsForTestRqst       = 24000;
	}
	else
	{
		/*The minimum battery voltage for a Requested battery test(scheduled tests require fully charged) */
	   //SRM - not in NVRam yet
		batteryParamsPtr->BatteryMinStartVoltsForTestRqst       = 24000;

	}

	sbError = NVRamBatteryDataGetMemoryPointer(NVRAM_BATT_DATA_BLK_USER_A, (lu_uint8_t    **)&batteryDataNVRamBlkPtr);

	if(sbError != SB_ERROR_NONE)
	{
		/* As a valid pointer was not returned the NVRAM data block must be initialised */
		memset( batteryDataNVRamBlkPtr,  0 , sizeof(BatteryDataNVRAMBlkStr) );		/* Fill the data block with zeroes							*/
		updateDataNVRAM					 		= LU_TRUE;							/* Set flag to indicate the NVRAM should be updated			*/
	}

	batteryDataNVRamBlkPtr->estimatedCapacity 	= batteryParamsPtr->BatteryNomCapacity;
	/* Load virtual IO_ID values from Battery Data NVRAM */
	ACCUMLATED_DISCHARGE_TIME = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].time.totalMin;
	ACCUMULATED_DISCHARGE_CURRENT = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[DISCHARGING].current.total100mAmpHours;
	ACCUMULATED_CHARGE_CURRENT = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].current.total100mAmpHours;
	VIRT_BT_ACCUMMULATED_CHARGE_TIME = batteryDataNVRamBlkPtr->charging[BAT_TEMP_RANGE_ALL].currentData[CHARGING].time.totalMin;
	VIRT_BT_CHARGE_CYCLES = batteryDataNVRamBlkPtr->chargeCycles;
	VIRT_BT_ESTIMATED_CAPACITY = batteryDataNVRamBlkPtr->estimatedCapacity;
	/* Read back NVRam to restore virtual point that indicates battery fault  */
	// batteryDataNVRamBlkPtr->batteryFault = LU_FALSE;
	VIRT_BT_FAULT = batteryDataNVRamBlkPtr->batteryFault;


	/* If we did not detect a sensor or the NVRAM failed reading reading the battery sensor, set the virtual point to indicate the error */
	if((sensorIsPresent == LU_FALSE) || (sbError != SB_ERROR_NONE))
	{
		VIRT_BT_DATA_NVRAM_FAIL = 1;
	}
	else
	{
		VIRT_BT_DATA_NVRAM_FAIL = 0;
	}

	return sensorIsPresent;
}

/******************************************************************************************************************************************/
/******************************************************************************************************************************************/
/* Executes the timer tick regular event for the Battery Manager FSM (BatteryManagerFSM.c). Also executes various support function        */
/******************************************************************************************************************************************/
/******************************************************************************************************************************************/


/**********************************************************/
/* Periodic tick event to FSM - main driver event for FSM */
/**********************************************************/
void SendTimerTickEventToFSM(void)
{
	/***************************************************************************/
	/*Get all the background conditions of the charger system for the FSM      */
	/***************************************************************************/
	upDateInputConditions();

	/*Send in the timertick event*/
	BAT_MANAGER_FSM_MAIN(TimerTickEvent);

	/* Measure the battery current, and update the accumulated charge/discharge */
	updateBatteryCurrent();

	/***************************************************************************/
	/*Set all the output indications from the FSM following the last timer tick*/
	/***************************************************************************/
	upDateOutputConditions();

}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/* As multiple sources can set the motor timer  */
/* it is important to ensure the timer with the */
/* farthest future timeout prevails and retains */
/* its operational ID so only its corresponding */
/* cancel event can kill it                     */
/************************************************/
void SendMotorSupplyOnEventToFSM(void)
{
	upDateInputConditions();
	/*Send in the motor supply on event*/
	BAT_MANAGER_FSM_MAIN(MotorSupplyOnEvent);
	upDateOutputConditions();
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
void SendMotorSupplyCancelEventToFSM(void)
{
	upDateInputConditions();
	/*Send in the Motor supply cancel event*/
	BAT_MANAGER_FSM_MAIN(MotorSupplyCancelEvent);
	upDateOutputConditions();
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatteryDisconEventToFSM(BatteryOperateStr * tstData)
{
	REPLY_STATUS retVal;

	/*Channel 3 ChargerInhibit*/
	if(tstData->channel == 0)
	{
		upDateInputConditions();
		/*Send in the ChargeInhibitEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(BatteryDisconEvent);
		upDateOutputConditions();
	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatteryDisconCancelEventToFSM(BatteryCancelStr * tstData)
{
	REPLY_STATUS retVal;

	/*Channel 3 is ChargerInhibit*/
	if(tstData->channel == 0)
	{
		upDateInputConditions();
		/*Send in the ChargeInhibitCancelEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(BatteryDisconCancelEvent);
		upDateOutputConditions();
	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatteryTestEventToFSM(BatteryOperateStr * tstData)
{
	REPLY_STATUS retVal;

	if(tstData->channel == 1)
	{
		upDateInputConditions();
		/*This event must delete its previously setup timer*/
//SRM CAN payload should contain capacity discharge
		fsmBatCANTestDurationAndCapacity(tstData->operationDuration,10 );
		/*Send in the BatteryTestEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN( BatteryTestEvent );
		upDateOutputConditions();

	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatteryTestCancelEventToFSM( BatteryCancelStr * tstData )
{
	REPLY_STATUS retVal;

	if(tstData->channel == 1)
	{
		/***************************************************************************/
		/*Get all the background conditions of the charger system for the FSM      */
		/***************************************************************************/
		upDateInputConditions();

		/*Send in the BatteryTestEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(BatteryTestCancelEvent);

		/***************************************************************************/
		/*Set all the output indications from the FSM following the last timer tick*/
		/***************************************************************************/
		upDateOutputConditions();

	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendChargerTestEventToFSM(BatteryOperateStr * tstData)
{
	REPLY_STATUS retVal;

	if(tstData->channel == 2)
	{
		upDateInputConditions();
		/*Send in the ChargeTestEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(ChargeTestEvent);
		upDateOutputConditions();
	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendChargerInhibitEventToFSM(BatteryOperateStr * tstData)
{
	REPLY_STATUS retVal;

	/*Channel 3 ChargerInhibit*/
	if(tstData->channel == 3)
	{
		upDateInputConditions();
		/*Send in the ChargeInhibitEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(ChargeInhibitEvent);
		upDateOutputConditions();
	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendChargerInhibitCancelEventToFSM(BatteryCancelStr * tstData)
{
	REPLY_STATUS retVal;

	/*Channel 3 is ChargerInhibit*/
	if(tstData->channel == 3)
	{
		upDateInputConditions();
		/*Send in the ChargeInhibitCancelEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(ChargeInhibitCancelEvent);
		upDateOutputConditions();

	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatTestInhibitEventToFSM(BatTestInhibOperateStr * tstData)
{
	REPLY_STATUS retVal;

	/*Channel 3 ChargerInhibit*/
	if(tstData->channel == 4)
	{
		/*Send in the BatteryTestInhibEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(BatteryTestInhibEvent);

	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatTestInhibitCancelEventToFSM(BatTestInhibCancelStr * tstData)
{
	REPLY_STATUS retVal;

	/*Channel 3 is ChargerInhibit*/
	if(tstData->channel == 4)
	{
		/*Send in the BatteryTestInhibCancelEvent event*/
		retVal = BAT_MANAGER_FSM_MAIN(BatteryTestInhibCancelEvent);

	}
	return retVal;
}

/************************************************/
/* Asynchronous event to FSM  - CAN bus command */
/************************************************/
REPLY_STATUS SendBatteryResetStatsEventToFSM(BatteryResetStr *rstData)
{
	REPLY_STATUS retVal = LU_FALSE;

	if(rstData->batType == 1)
	{
		/*This command event does not need to go to the FSM*/
		/*It simply clears down all the reportable battery stats*/
		retVal = LU_FALSE;
	}
	return retVal;
}
