/*
 * LeadAcidChargerFSM.h
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */

#ifndef LEADACIDCHARGERFSM_H_
#define LEADACIDCHARGERFSM_H_

/*LeadAcidChargerFSM is a sub Finite State Machine (FSM) of the Battery Manager FSM.*/
/*It takes events defined for and passed into the Battery FSM to process further */
#include "BatManagerFSM.h"
#include "BatteryChargerTopLevel.h"

#include "lu_types.h"
#include "errorCodes.h"
#include "ModuleProtocolEnum.h"
/*Include all the typedefs for data and function pointers to be used in an FSM*/
#include "fsm.h"

#define CHARGER_MIN_V_LIM 28500
#define CHARGER_TEST_V    29000
#define CHARGER_MAX_V_LIM 29500
#define MAX_CHARGER_ABILITY 2300
#define TEN_MINUTES_COUNT_SECONDS 600
#define NOMINAL_BATTERY_TEMP 20

#define CHECK_CHARGER_STATE    0
#define INITIAL_CHARGE_STATE   1
#define CHARGE_SUSPENDED_STATE 2
#define FLOAT_CHARGE_STATE     3

/*The over all testing time and none testing time for missing battery detect by voltage*/
#define BATTERY_DETECT_BY_VOLTAGE_REPEAT_PERIOD 0xf
/*The active test steps time within the overall testing time for missing battery detect by voltage*/
#define BATTERY_DETECT_BY_VOLTAGE_TEST_PERIOD   0x4
/*Test step times*/
#define TIME_FOR_TEST_STEP3 3
#define TIME_FOR_TEST_STEP2 2
#define TIME_FOR_TEST_STEP1 1
#define TIME_FOR_TEST_STEP0 0

#define FIVE_SECONDS_IN_THIS_STATE 5
#define TEN_SECONDS_IN_THIS_STATE 10
#define USE_TWENTY_PERCENT_MARGING 1.2
#define ONE_PERCENT_OF 0.01
#define ONE_POINT_FOUR_PERCENT_OF 0.014

#define BATTERY_PRESENCE_DETECT_BY_CURRENT 100
#define BATTERY_PRESENCE_DETECT_BY_CURRENT_HYSTERISIS 105

/* To prevent fully charged being set early, there is a mask timer when entering the float state */
#define FULL_CHARGE_DELAY_TIME	300

/*Main event handler for this FSM*/
extern REPLY_STATUS CHRG_LEAD_ACID_FSM_MAIN(EventID eventIn);
/*Access function to read battery analyser's evaluation of battery as fatal or not*/
extern lu_bool_t getLdAcidChargingLevelFsmBatAnalysis(void);
/*Access to allow the Battery Manager FSM to check if battery is fully charged or not*/
extern lu_bool_t is_LdAcidBatFullyCharged(void);
/*Read access function*/
extern lu_bool_t getLdAcidScheduledTestAcknowledge(void);
/*Write access function*/
extern void setScheduledTestAcknowledge(lu_bool_t SchedTestOk);
/*Read access function*/
extern lu_bool_t getLdAcidBatteryPresenceIndicator(void);
/*Write access function*/
extern void setLdAcidBatteryPresenceIndicator(lu_bool_t battStat);
/*Access to allow the Battery Manager FSM to invalidate the fully charged status*/
extern void invalidateBatFullyCharged(void);
/*Battery Managers access to the temperature corrected charger voltage setting*/
extern lu_int32_t getLdAcidChargerVolts(void);
/*Access to write the chargers voltage directly and record for others to access*/
extern void setLdAcidChargerVolts(lu_int32_t vCharger);
#endif /* LEADACIDCHARGERFSM_H_ */
