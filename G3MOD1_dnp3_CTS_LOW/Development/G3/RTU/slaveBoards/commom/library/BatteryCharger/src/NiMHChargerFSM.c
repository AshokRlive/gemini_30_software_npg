/*
 * NiMHChargerFSM.c
 *
 *  Created on: 13 Feb 2018
 *      Author: manning_s
 */
/*****************************************************************************************************************************************
Description of this Finite State Machine (FSM) as in original template :
Finite State Machines are a collection of distinct "states" or "modes of operation". There can only ever be one active state at a time.
Transit from the active state to the next active state is determined by the exits defined in the active state in terms of events
or commands received and prevailing conditions such as flags and variables etc.
This FSM template caters for a number of system events such as a regular timer event or some CAN command events etc.
This FSM template caters for a state to execute code exclusively at entry to the state where it will ignore transitions back to itself.
This FSM template caters for the execution of code on every contiguous call to the same active state.
This FSM template caters for the testing of a number of possible exits where upon it will execute exit actions for the particular exit.
This FSM is always called via a function pointer that points to the present active state function.


FSM function pointer                                          <-- Called for specified system events and updated to point to a state
      |                                                           function (becoming the active state) by a condition function when
	  |                                                           it tests for a validates the condition it is looking for.
	  |
	  |_____state 1 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 1 entry action code        <-- Called exclusively on first entry to state 1 function
	  |             |
	  |             |_______state 1 unconditional code       <-- Called for every event call to FSM whilst in state 1
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 1 condition 1 function     <--| All condition functions get called until one returns "LU_TRUE".
	  |             |_______state 1 condition 2 function     <--| If "LU_TRUE" it changes active state ie function pointed to by "FSM function pointer"
	  |             |                                        <--| to point to new active state and executes any exit actions. The next event to call
	  |             |_______state 1 condition m function     <--| "FSM function pointer" will call the new active state function.
	  |
	  |
	  |_____state 2 function                                 <-- state functions are always called via the FSM function pointer which
	  |             |                                            points to the presently active state.
	  |             |
	  |             |_______state 2 entry action code        <-- Called exclusively on first entry to state 2 function
	  |             |
	  |             |_______state 2 unconditional code       <-- Called for every event call to FSM whilst in state 2
	  |             |
	  |             |_______Check global exit conditions     <-- Call all global exit conditions
	  |             |
	  |             |_______state 2 condition 1 function
	  |             |_______state 2 condition 2 function
	  |             |
	  |             |_______state 2 condition m function
	  |
	  |_____state n function

The original template caters for 6 state functions each of which has 3 exit condition functions

To create your a new state machine you need to
1)Re-create this file [newFileNameFSM.c]
2)Re-create its header file [newFileNameFSM.h]  and include the header here.
3)Re-create any timeout timers you will require
4)Re-create the timeout timers processing array with the names of your new timers.
5)Create any Global State exit functions that you require.
6)Create an array list of pointers to the Global State exit functions for processing them
7)For each state you require create a block of functions as below
7a)The StateFunc
7b)The StateEntryFunc
7c)The StateUncondFunc
7d)A StateExit function for each exit from the state
7e)An array of StateExit function pointers listing all the stateExit functions
8)Declare a state struct for each state and initialise it with the components created in the previous steps
9)Re-Create the array of pointers to state structures (allStates[]) and initialise with the address of all the states
10)Re-create the FSM struct for this FSM with its own name and initialise with the components created previously
11)Re-create the FSM_MAIN function with an appropriate name. This is the only function called from outside this file.
12)Write function definitions for all the files declared in the previous steps.
******************************************************************************************************************************************/

/*Sub state FSM of BatManagerFSM - This FSM runs only when the BatManagerFSM is in the CHARGE state. */
/*It receives no events of any kind, including the regular timer tick event, when the BatManagerFSM is*/
/*not in the CHARGE state.                                                                           */
/*It does use BatManagerFSM events and therefore includes the BatManagerFSM.h                        */
#include "NiMHChargerFSM.h"
#include "BatteryChargerTopLevel.h"
#include "BatteryToFsmInterface.h"
#include "BatManagerFSM.h"
#include "IOManager.h"
#include "BoardIOMap.h"

/*******************************************************/
/*Only one Event input structure for this state machine*/
/*******************************************************/
static EventID event;
/*******************************************************/

/***************************************************************************/
/*Declare any timeout timers for use in this FSM. These are decrement every*/
/*timer tick event fed into the FSM. This is expected to be a timed call to*/
/*this FSM e.g. every second. The programmer must take proper account of this*/
/***************************************************************************/
static lu_int32_t ConstCurrentPhaseTimer = 0;
static lu_int32_t batteryFullStabilisationTimer=0;

static lu_int32_t maxInitialChargeTimer=0;
static lu_bool_t FAST_CHARGE_COMPLETE_THERMAL_SIGNAL = LU_FALSE;
static lu_bool_t BATTERY_RECOVERY_INPROGRESS = LU_FALSE;
/*Add all timers declared above here for processing and terminate with a NULL*/
static lu_int32_t *fsmTimers_p[]={&ConstCurrentPhaseTimer,
		                          &batteryFullStabilisationTimer,
		                          NULL_FSM_TIMER};

static void fsmTimersUpdate(void);
static void fsmTimerSet( lu_int32_t* timerName, lu_int32_t timerValue);
static lu_int32_t fsmTimerGet( lu_int32_t* timerName );

static lu_bool_t EXIT_FLAG_TO_CHARGE_STATE;
static lu_bool_t EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE;
static lu_bool_t EXIT_FLAG_MAINTAIN_TO_CHECK_CHARGER_STATE;
static lu_bool_t EXIT_FLAG_MAINTAIN_TO_FAST_CHARGE_STATE;

static lu_bool_t BATTERY_MISSING;
lu_bool_t isNiMHBatteryMissing(void);

/*Storage for scheduled test acknowledge*/
static lu_bool_t SCHEDULED_TEST_ACCEPETED = LU_TRUE;
/*Read access function*/
lu_bool_t getNiMHScheduledTestAcknowledge(void);

/*Storage for battery's health status*/
static lu_bool_t BatteryPresenceIndicator;
/*Read access function*/
lu_bool_t getNiMHBatteryPresenceIndicator(void);
/*Write access function*/
void setNiMHBatteryPresenceIndicator(lu_bool_t battStat);

/*Storage for the charger's health status*/
static chrgHlthStatus ChargerHealthStatus;
/*Read access function*/
chrgHlthStatus getNiMHChargerHealthStatus(void);
/*Write access function*/
void setNiMHChargerHealthStatus(chrgHlthStatus chrgStat);

/*Maintained here and accessible from Battery Manager FSM*/
void setNiMHChargerVolts(lu_int32_t vCharger);
void setNiMHChargerCurrent(lu_int32_t currentRqst);

/************************************************************************/
/*Access functions to battery charger level decisions                   */
/************************************************************************/
static lu_bool_t BATTERY_NOT_USABLE_FLAG = LU_FALSE;
lu_bool_t getNiMHChargingLevelFsmBatAnalysis(void);

/************************************************************************/
/*Only one Function to terminate exit searches in all states of this FSM*/
/************************************************************************/
static lu_bool_t stateExitSearchEnd(fsm *this);
/************************************************************************/

/****************************************/
/*Global exits processing function      */
/****************************************/
static lu_bool_t CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(fsm *this);

/*****************************/
/*Global State exit functions*/
/*****************************/
static lu_bool_t CHRG_NICKEL_METAL_GLOBAL_Exit_GoToIdleRqst(fsm *this);

/*   Add more global exits here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the global exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs CHRG_NICKEL_METAL_GLOBAL_ExitFuncs[]={CHRG_NICKEL_METAL_GLOBAL_Exit_GoToIdleRqst,
                                                      stateExitSearchEnd};

/*****************************************************************/
/*****************************************************************/
/*One of these blocks for each state in this Finite State Machine*/
/*****************************************************************/
/*****************************************************************/

/*******************************************************************/
/*    CHECK_CHARGER_ state function prototypes and initialisers*/
/*******************************************************************/
static void NiMH_CHECK_CHARGER_StateFunc(fsm *this);
static void NiMH_CHECK_CHARGER_StateEntryFunc(fsm *this);
static void NiMH_CHECK_CHARGER_StateUncondFunc(fsm *this);

/*       One for each exit from the state*/
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_RECOVER_BATTERY(fsm *this);
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_FAST_CHARGE(fsm *this);
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_CHECK_CHARGER(fsm *this);
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_CHARGE_SUSPENDED(fsm *this);
//static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_MAINTAIN_CHARGE(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs NiMH_CHECK_CHARGER_StateExitFuncs[]={NiMH_CHECK_CHARGER_StateExitTo_CHARGE_SUSPENDED,
                                                        NiMH_CHECK_CHARGER_StateExitTo_CHECK_CHARGER,
		                                                NiMH_CHECK_CHARGER_StateExitTo_RECOVER_BATTERY,
												        NiMH_CHECK_CHARGER_StateExitTo_FAST_CHARGE,
												        /*NiMH_CHECK_CHARGER_StateExitTo_MAINTAIN_CHARGE,*/
                                                        stateExitSearchEnd};

/******************************************************************/
/*    FAST_CHARGE_ state function prototypes and initialisers*/
/******************************************************************/
static void NiMH_FAST_CHARGE_StateFunc(fsm *this);
static void NiMH_FAST_CHARGE_StateEntryFunc(fsm *this);
static void NiMH_FAST_CHARGE_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this);
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_RECOVER_BATTERY(fsm *this);
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE(fsm *this);
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE_BY_FAST_THERMAL_CUT_OFF(fsm *this);
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE_BY_TIMEOUT(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs NiMH_FAST_CHARGE_StateExitFuncs[]={NiMH_FAST_CHARGE_StateExitTo_CHARGE_SUSPENDED,
		                                              NiMH_FAST_CHARGE_StateExitTo_RECOVER_BATTERY,
													  NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE,
													  NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE_BY_FAST_THERMAL_CUT_OFF,
													  NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE_BY_TIMEOUT,
                                                      stateExitSearchEnd};

/*******************************************************************/
/*    CHARGE_SUSPENDED_ state function prototypes and initialisers*/
/*******************************************************************/
static void NiMH_CHARGE_SUSPENDED_StateFunc(fsm *this);
static void NiMH_CHARGE_SUSPENDED_StateEntryFunc(fsm *this);
static void NiMH_CHARGE_SUSPENDED_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t NiMH_CHARGE_SUSPENDED_StateExitTo_CHECK_CHARGER(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs NiMH_CHARGE_SUSPENDED_StateExitFuncs[]={NiMH_CHARGE_SUSPENDED_StateExitTo_CHECK_CHARGER,
                                                      stateExitSearchEnd};

/*******************************************************************/
/*    MAINTAIN_CHARGE_ state function prototypes and initialisers*/
/*******************************************************************/
static void NiMH_MAINTAIN_CHARGE_StateFunc(fsm *this);
static void NiMH_MAINTAIN_CHARGE_StateEntryFunc(fsm *this);
static void NiMH_MAINTAIN_CHARGE_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t NiMH_MAINTAIN_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this);
static lu_bool_t NiMH_MAINTAIN_CHARGE_StateExitTo_RECOVER_BATTERY(fsm *this);
static lu_bool_t NiMH_MAINTAIN_CHARGE_StateExitTo_CHECK_CHARGER(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs NiMH_MAINTAIN_CHARGE_StateExitFuncs[]={NiMH_MAINTAIN_CHARGE_StateExitTo_CHARGE_SUSPENDED,
		                                                  NiMH_MAINTAIN_CHARGE_StateExitTo_RECOVER_BATTERY,
												          NiMH_MAINTAIN_CHARGE_StateExitTo_CHECK_CHARGER,
                                                          stateExitSearchEnd};


/*******************************************************************/
/*    RECOVER_BATTERY_ state function prototypes and initialisers*/
/*******************************************************************/
static void NiMH_RECOVER_BATTERY_StateFunc(fsm *this);
static void NiMH_RECOVER_BATTERY_StateEntryFunc(fsm *this);
static void NiMH_RECOVER_BATTERY_StateUncondFunc(fsm *this);
/*       One for each exit from the state*/
static lu_bool_t NiMH_RECOVER_BATTERY_StateExitTo_CHARGE_SUSPENDED(fsm *this);
static lu_bool_t NiMH_RECOVER_BATTERY_StateExitTo_CHECK_CHARGER(fsm *this);
/*   Add more exits for this state here if needed      <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*       One array listing all the exit functions in order to be executed and always terminated with stateExitSearchEnd*/
exitConditionFuncs NiMH_RECOVER_BATTERY_StateExitFuncs[]={NiMH_RECOVER_BATTERY_StateExitTo_CHARGE_SUSPENDED,
		                                                  NiMH_RECOVER_BATTERY_StateExitTo_CHECK_CHARGER,
                                                          stateExitSearchEnd};



/*********      ADD MORE STATE BLOCKS HERE IF REQUIRED    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/*****************************************************************/

/************************NiMH************************************/
/*InitiaLISE each of the states in this Finite State Machine*/
/************************************************************/
state NiMH_CHECK_CHARGER={NiMH_CHECK_CHARGER_StateFunc,NiMH_CHECK_CHARGER_StateEntryFunc,NiMH_CHECK_CHARGER_StateUncondFunc,NiMH_CHECK_CHARGER_StateExitFuncs};
state NiMH_FAST_CHARGE={NiMH_FAST_CHARGE_StateFunc,NiMH_FAST_CHARGE_StateEntryFunc,NiMH_FAST_CHARGE_StateUncondFunc,NiMH_FAST_CHARGE_StateExitFuncs};
state NiMH_CHARGE_SUSPENDED={NiMH_CHARGE_SUSPENDED_StateFunc,NiMH_CHARGE_SUSPENDED_StateEntryFunc,NiMH_CHARGE_SUSPENDED_StateUncondFunc,NiMH_CHARGE_SUSPENDED_StateExitFuncs};
state NiMH_MAINTAIN_CHARGE={NiMH_MAINTAIN_CHARGE_StateFunc,NiMH_MAINTAIN_CHARGE_StateEntryFunc,NiMH_MAINTAIN_CHARGE_StateUncondFunc,NiMH_MAINTAIN_CHARGE_StateExitFuncs};
state NiMH_RECOVER_BATTERY={NiMH_RECOVER_BATTERY_StateFunc,NiMH_RECOVER_BATTERY_StateEntryFunc,NiMH_RECOVER_BATTERY_StateUncondFunc,NiMH_RECOVER_BATTERY_StateExitFuncs};

/****    ADD MORE STATE INITIALISERS HERE IF REQUIRED       <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/************************************************************/

/***********************************************************************************************/
/*Initialise an array list of pointers to state for all the states in this finite state machine*/
/***********************************************************************************************/
static state* allStates[]={&NiMH_CHECK_CHARGER,
					       &NiMH_FAST_CHARGE,
					       &NiMH_CHARGE_SUSPENDED,
					       &NiMH_MAINTAIN_CHARGE,
					       &NiMH_RECOVER_BATTERY
					       };           /* ADD MORE STATE NAMES TO THE INTIALISER HERE   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/***********************************************************************************************/

/*************************************************************************************************************/
/*Initialise present state counter...reset every state change, increments every second and does not roll over*/
/*************************************************************************************************************/
static lu_uint32_t presStateCounter=0;

/*****************************************************************/
/*Initialise a state machine structure with components from above*/
/*****************************************************************/
static fsm CHRG_NICKEL_METAL_FSM = { &event,                /*event inputs*/
                                  &NiMH_CHECK_CHARGER.stateCall,  /*presActiveState initialisation - should be the intended initialising state*/
                                  &NiMH_CHARGE_SUSPENDED.stateCall, /*prevActiveState initialisation - can be any different state to presActive for initialisation*/
                                  &allStates[0],                  /*statesList initialisation*/
                                  &presStateCounter,
                                  0
                                  };
/*****************************************************************/


/**************************************************************************************************************************************/
/**************************************************************************************************************************************/
/*                                            FUNCTION DEFINITIONS                                                                    */
/**************************************************************************************************************************************/
/**************************************************************************************************************************************/
lu_bool_t NiMH_BatteryRecovered(fsm *this);
lu_bool_t thermalSlopeDetector(fsm *this, lu_bool_t initFilter);
lu_bool_t maintainChargeAndRestSequence(fsm *this);
lu_bool_t batteryPresenceDetection(fsm *this);
lu_bool_t chargerFailureDetect(fsm *this);
lu_bool_t isBatteryFullyCharged(fsm *this);

/**************************************************/
/*Call the finite state machine for each new event*/
/*This is the only call to the FSM from outside   */
/*except for access to some external group timers.*/
/*The return from this function allows specific   */
/*events (events other than timerTick) to return  */
/*an acknowledgement as to whether or not they    */
/*were acted on.                                  */
/**************************************************/
REPLY_STATUS CHRG_NICKEL_METAL_FSM_MAIN(EventID eventIn)
{
    /*Add the new FSM event to the new FSM*/
	*(CHRG_NICKEL_METAL_FSM.event) = eventIn;

	if(*(CHRG_NICKEL_METAL_FSM.presActiveState) != *(CHRG_NICKEL_METAL_FSM.prevActiveState) )
	{
		/*State has just changed so reset state seconds counter*/
		*(CHRG_NICKEL_METAL_FSM.presActiveStateSecsCounter)=0;
	}

	/*Process all the fsm timers*/
	if(*(CHRG_NICKEL_METAL_FSM.event) == TimerTickEvent)
	{
		/*Timer tick events are not specific CAN requests. They update conditions and times and so are always REPLY_STATUS_OKAY*/
		CHRG_NICKEL_METAL_FSM.eventAcknowledge = REPLY_STATUS_OKAY;

		fsmTimersUpdate();

		/*Provided we won't roll over...*/
		if(*(CHRG_NICKEL_METAL_FSM.presActiveStateSecsCounter) != 0xffffffff )
		{
			/*...update time in the present state*/
			*(CHRG_NICKEL_METAL_FSM.presActiveStateSecsCounter)+=1;
		}
	}
	else
	{
		/*Events that require an acknowledge must assign this to be REPLY_STATUS_OKAY when they know*/
		/*that the event will be acted on in the unconditional or an exit state function*/
		CHRG_NICKEL_METAL_FSM.eventAcknowledge = REPLY_STATUS_OPERATE_REJECT_ERROR;
	}

	/*...Call the present active state to check for exits*/
    (*CHRG_NICKEL_METAL_FSM.presActiveState)( &CHRG_NICKEL_METAL_FSM );

	return CHRG_NICKEL_METAL_FSM.eventAcknowledge;
}
/*************************************************/


/*******************************************************************************************/
/*This function simply terminates the exit search for any list of state condition exits*****/
/*It must always be positioned at the end of the array of function pointers to exit condition*/
/*******************************************************************************************/
static lu_bool_t stateExitSearchEnd(fsm *this)
{
	PARAM_NOT_USED(this);
    return LU_TRUE;
}

/*******************************************/
/* Process global condition function exits */
/*******************************************/
static lu_bool_t CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(fsm *this)
{
	lu_bool_t retValue=LU_FALSE;
	lu_uint8_t nextExitFunc=0;
    /*Execute each CHRG_NICKEL_METAL_GLOBAL conditional exit function test in turn*/
    /*If valid exit found take it*/
    while(CHRG_NICKEL_METAL_GLOBAL_ExitFuncs[nextExitFunc](this)==LU_FALSE)
    {
        nextExitFunc++;
    }
	/*Test to see if last exit function was just the search terminating function*/
	if(CHRG_NICKEL_METAL_GLOBAL_ExitFuncs[nextExitFunc] != stateExitSearchEnd)
	{
        /*If here and not the exit search terminating function it must be a valid CHRG_NICKEL_METAL_GLOBAL exit*/
		retValue=LU_TRUE;
	}
	return retValue;
}

/******************************************************************************************/
/* CHRG_NICKEL_METAL_GLOBAL exit condition function - CHRG_NICKEL_METAL_GLOBAL_Exit_NoUsableBat */
/******************************************************************************************/
static lu_bool_t CHRG_NICKEL_METAL_GLOBAL_Exit_GoToIdleRqst(fsm *this)
{
	lu_bool_t retValue = LU_FALSE;
	/*This global event is gated to run every 30 seconds because it is invasive */
    if(*(this->event) == GoToIdleEvent)
    {
		/*For GoToIdle we suspend operation in the Battery Analyser State */
		*(this->presActiveState)=NiMH_CHECK_CHARGER_StateFunc;
		/*Publish the current state number*/
		VIRT_BT_CHGR_FSM_STATE = NiMH_CHECK_CHARGER_STATE;

		/*FLAG UP we are exiting the charging FSM */
		VIRT_CH_SYSTEM_RUNNING = FLAG_CLR;
		/*FLAGUP we are stopping charge current to the battery*/
		VIRT_CH_ON =  FLAG_CLR;
		/*We are never considered fully charged when disconnected from the charger*/
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		/*Disconnect the charger from the battery */
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*SWITCH THE CHARGER OFF*/
		BAT_CHARGER_EN = CTRL_OFF;
		/*Publish charger is off*/
		VIRT_CHARGER_EN = LU_FALSE;
		/*Reset fast charge complete flag*/
		FAST_CHARGE_COMPLETE_THERMAL_SIGNAL = LU_FALSE;
	    /*Publish the fast charge complete flag*/
		VIRT_NiMH_FAST_CHRG_COMPLETE = FAST_CHARGE_COMPLETE_THERMAL_SIGNAL;

		/*Set charger current ready for when we allow this FSM to run again*/
		setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/4);

		/*Terminate the search for exit conditions*/
		retValue = LU_TRUE;
    }
	return retValue;
}

/******************************************************/
/******************************************************/
/******************************************************/
/* CHECK_CHARGER_ state function definitions          */
/******************************************************/
/******************************************************/
/******************************************************/
static void NiMH_CHECK_CHARGER_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
	
    /*Check if at entry and if so execute entry code */   
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
		NiMH_CHECK_CHARGER_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    NiMH_CHECK_CHARGER_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(NiMH_CHECK_CHARGER_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*   CHECK_CHARGER_ entry function definition         */
/******************************************************/
/******************************************************/
static void NiMH_CHECK_CHARGER_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = NiMH_CHECK_CHARGER_STATE;

	SCHEDULED_TEST_ACCEPETED = LU_FALSE;
	/*DISCONNECT CHARGER FROM BATTERY*/
	BAT_TO_MOT_CONNECT = CTRL_OFF;
	/*Publish charger is disconnected from battery*/
	VIRT_CHARGER_CON = LU_FALSE;
	/*SWITCH ON CHARGER*/
	BAT_CHARGER_EN = CTRL_ON;
	/*Publish charger is on*/
	VIRT_CHARGER_EN = LU_TRUE;
	/*FLAG UP we are running the charging FSM */
	VIRT_CH_SYSTEM_RUNNING = FLAG_SET;
	/*FLAGUP we re not yet delivering charge current to the battery*/
	VIRT_CH_ON =  FLAG_CLR;
	/* Set the Fully charged flag to false */
	VIRT_BT_FULLY_CHARGED = LU_FALSE;
	/*Set the Battery charger current limit*/
	setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/4);

	/*SET CHARGER'S VOLTAGE TO MAX FOR THIS CHEMISTRY OF BATTERY*/
	setNiMHChargerVolts(NIMH_CHARGING_VOLTS);
	/*CLEAR TEMPORAL EXIT FLAG*/
	EXIT_FLAG_TO_CHARGE_STATE = LU_FALSE;
}
/******************************************************/
/*  CHECK_CHARGER_ unconditional function definition  */
/******************************************************/
static void NiMH_CHECK_CHARGER_StateUncondFunc(fsm *this)
{
	lu_int32_t vChargerMeasured;
	static lu_int8_t ChargerSettingFaultCount = 0;

	if(	BAT_V_SENSE > BATTERY_PRESENT )
	{
		setNiMHBatteryPresenceIndicator(LU_TRUE);
	}
	else
	{
		setNiMHBatteryPresenceIndicator(LU_FALSE);
	}


	/*ALLOW SOME STABILISATION TIME FOR THE CHARGER AS JUST SWITCHED ON IN ENTRY FUNCTION*/
	if( *this->presActiveStateSecsCounter > 5)
	{
		/*READ BACK THE CHARGER'S VOLTAGE*/
		vChargerMeasured = CHARGER_V_SENSE;

		/*TEST THE CHARGER'S VOLTAGE AND IF OUT OF LIMITIS FLAG IT AND USE RED CHARGER LED*/
		if(vChargerMeasured < NIMH_CHARGER_MIN_V_LIM || vChargerMeasured > NIMH_CHARGER_MAX_V_LIM )
		{
			ChargerSettingFaultCount++;

			/* We allow the charger 5 attempts at starting before raising any faults */
			if( ChargerSettingFaultCount >= 5)
			{
				VIRT_CH_VOLTAGE_FAULT = FLAG_SET;
				VIRT_CH_FAULT = FLAG_SET;
				/* Reset the ChargerSettingFaultCounter */
				ChargerSettingFaultCount = 0;
			}

			EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE = LU_TRUE;
		}
		else
		{
			/* Reset the ChargerSettingFaultCounter */
			ChargerSettingFaultCount = 0;

			/*CLEAR THE CHARGER_VOLTAGE_SETTING_FAULT FLAGS*/
			VIRT_CH_VOLTAGE_FAULT = FLAG_CLR;
			VIRT_CH_FAULT = FLAG_CLR;

			/* We can only proceed to a charging state if the charger shows no fault */
			EXIT_FLAG_TO_CHARGE_STATE = LU_TRUE;
		}
	}
}

/******************************************************/
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_RECOVER_BATTERY(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if( (*(this->event) == RecoverBatteryEvent) ||
        (BATTERY_RECOVERY_INPROGRESS == LU_TRUE)   )
    {
    	BATTERY_RECOVERY_INPROGRESS = LU_TRUE;
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_RECOVER_BATTERY_StateFunc;
    }
    return retVal;
}
/******************************************************/
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_FAST_CHARGE(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	/*Charge phase status flags are determined in the unconditional function of this state*/
    	if(	( EXIT_FLAG_TO_CHARGE_STATE == LU_TRUE)     )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=NiMH_FAST_CHARGE_StateFunc;
			/*Execute actions for this transition*/

			/*Set the Battery charger current limit*/
			setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/4);

			/*Set the Battery charger to an easy to achieve voltage for the charger...*/
			/*The FAST CHARGE state will correct this as it gets going*/
			/*Set up the charger volts*/
			setNiMHChargerVolts(NIMH_CHARGING_VOLTS);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}

/******************************************************/
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	/*Charge phase status flags are determined in the unconditional function of this state*/
    	if( EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE == LU_TRUE )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState) = NiMH_CHECK_CHARGER_StateFunc;
			/*Change the previous state to NULL, to force the State Entry */
			*(this->prevActiveState) = NULL;

			/* Reset the SecsCounter to 0 */
			*(this->presActiveStateSecsCounter) = 0;
			/*Execute actions for this transition*/

			/* Clear the flag */
			EXIT_FLAG_CHECK_CHARGER_TO_CHECK_CHARGER_STATE = LU_FALSE;
			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }

    return retVal;
}
/******************************************************/
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	if( BAT_TEMP > CHARGE_THERMAL_CUTOFF)
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=NiMH_CHARGE_SUSPENDED_StateFunc;

			/*DISCONNECT CHARGER FROM BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_OFF;
			/*Publish charger is disconnected from battery*/
			VIRT_CHARGER_CON = LU_FALSE;
			/*FLAGUP we are stopping delivering charge current to the battery*/
			VIRT_CH_ON =  FLAG_CLR;
			/*FLAG UP we are over temperature*/
			VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
			/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
			setChargerTemperatureDisable(LU_TRUE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}

/******************************************************/
static lu_bool_t NiMH_CHECK_CHARGER_StateExitTo_MAINTAIN_CHARGE(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	if(	( EXIT_FLAG_TO_CHARGE_STATE == LU_TRUE) &&
    		( BAT_V_SENSE >= BATTERY_MAINTAIN_THRESHOLD )     )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=NiMH_MAINTAIN_CHARGE_StateFunc;

			/*Set the Battery charger current limit*/
			setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/20);

			/*Set the Battery charger to an easy to achieve voltage for the charger...*/
			/*The MAINTAIN CHARGE state will correct this as it gets going*/
			/*Set up the charger volts*/
			setNiMHChargerVolts(NIMH_CHARGING_VOLTS);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;

}

/******************************************************/
/******************************************************/
/******************************************************/
/* FAST_CHARGE_ state function definitions         */
/******************************************************/
/******************************************************/
/******************************************************/
static void NiMH_FAST_CHARGE_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;

    /*Check if at entry and if so execute entry code */  
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	NiMH_FAST_CHARGE_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}	
    /*Execute any unconditional code */
    NiMH_FAST_CHARGE_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(NiMH_FAST_CHARGE_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*  FAST_CHARGE_ entry function definition         */
/******************************************************/
/******************************************************/
static void NiMH_FAST_CHARGE_StateEntryFunc(fsm *this)
{
	/*Initialise the thermal signal filter*/
	FAST_CHARGE_COMPLETE_THERMAL_SIGNAL = thermalSlopeDetector(this, EXPLICIT_INITIALISE);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = NiMH_FAST_CHARGE_STATE;
	/*Reset fast charge complete flag*/
	FAST_CHARGE_COMPLETE_THERMAL_SIGNAL = LU_FALSE;
    /*Publish the fast charge complete flag*/
	VIRT_NiMH_FAST_CHRG_COMPLETE = FAST_CHARGE_COMPLETE_THERMAL_SIGNAL;


	/*Set Maximum initial charge time to the time taken to charge from empty to full*/
	maxInitialChargeTimer = ( (batteryParamsPtr->BatteryNomCapacity*1.2)/BAT_CHARGER_CUR_LIM_SET)*SECONDS_PER_HOUR;

	/*CONNECT CHARGER TO BATTERY*/
	BAT_TO_MOT_CONNECT = CTRL_ON;
	/*Publish charger is disconnected from battery*/
	VIRT_CHARGER_CON = LU_TRUE;
	/*FLAGUP we are now delivering charge current to the battery*/
	VIRT_CH_ON =  FLAG_SET;

}

/******************************************************/
/*  FAST_CHARGE_ unconditional function definition */
/******************************************************/
static void NiMH_FAST_CHARGE_StateUncondFunc(fsm *this)
{
	static lu_uint32_t batteryFaultCountQualifier = 0;

	/*******************************************************************************************************/
	/*                           NiMH THERMAL SIGNAL DETECTION FOR FAST CHARGING COMPLETE                  */
	/*******************************************************************************************************/
	FAST_CHARGE_COMPLETE_THERMAL_SIGNAL = thermalSlopeDetector(this, NO_INITIALISE);

	/*******************************************************************************************************/
	/*                           CHARGER FAILURE DETECTION                                                 */
	/*******************************************************************************************************/
	/* If the charger current exceeds 120% of the set current limit, set the Charger current fault flag */
	if((VIRT_CHARGER_CON == LU_TRUE) && (BAT_CHARGER_I_SENSE > (1.2 * BAT_CHARGER_CUR_LIM_SET)))
	{
		VIRT_CH_CURRENT_FAULT = FLAG_SET;
	}
	else
	{
		VIRT_CH_CURRENT_FAULT = FLAG_CLR;
	}

	/* If either charger voltage or charger current faults have been set, set charger fault */
	if((VIRT_CH_VOLTAGE_FAULT == FLAG_SET) || (VIRT_CH_CURRENT_FAULT == FLAG_SET))
	{
		VIRT_CH_FAULT = FLAG_SET;
	}
	else
	{
		VIRT_CH_FAULT = FLAG_CLR;
	}

	/*BATTERY CHARGING FAULT TEST*/
	if(BAT_V_SENSE > batteryParamsPtr->BatteryLowLevel )
	{
		/*Start again - set the end qualifying count out another 600 seconds*/
		batteryFaultCountQualifier = *this->presActiveStateSecsCounter + TEN_MINUTES_COUNT_SECONDS;
		/*Battery charging is ok*/
		VIRT_BT_FAULT = LU_FALSE;
	}
	else
	{
		/*if 600 consecutive seconds pass while on charge and battery volts remain below BATTERY_LOW level ...*/
		if( *this->presActiveStateSecsCounter > batteryFaultCountQualifier )
		{
			/*...that's a battery charging fault failure*/
			VIRT_BT_FAULT = LU_TRUE;
			batteryFaultCountQualifier = 0;
		}
	}
}
/******************************************************/
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	if( BAT_TEMP > CHARGE_THERMAL_CUTOFF )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=NiMH_CHARGE_SUSPENDED_StateFunc;

			/*DISCONNECT CHARGER FROM BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_OFF;
			/*Publish charger is disconnected from battery*/
			VIRT_CHARGER_CON = LU_FALSE;
			/*FLAGUP we are stopping delivering charge current to the battery*/
			VIRT_CH_ON =  FLAG_CLR;
			/*FLAG UP we are over temperature*/
			VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
			/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
			setChargerTemperatureDisable(LU_TRUE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}
/******************************************************/
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_RECOVER_BATTERY(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == RecoverBatteryEvent)
    {
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_RECOVER_BATTERY_StateFunc;
    }
    return retVal;
}



/******************************************************/
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE(fsm *this)
{
	PARAM_NOT_USED(this);
    lu_bool_t retVal = LU_FALSE;

	if( FAST_CHARGE_COMPLETE_THERMAL_SIGNAL == LU_TRUE )
	{
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_MAINTAIN_CHARGE_StateFunc;

		/*Set the Battery charger current limit*/
		setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/20);

		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;
	}

    return retVal;
}
/******************************************************/
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE_BY_TIMEOUT(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    /*This time delay is dependent on battery capacity and charge rate*/
    if( (lu_int32_t)(*this->presActiveStateSecsCounter) > maxInitialChargeTimer )
    {
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_MAINTAIN_CHARGE_StateFunc;

		/*Set the Battery charger current limit*/
		setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/20);


		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;

    }
    return retVal;
}
/******************************************************/
static lu_bool_t NiMH_FAST_CHARGE_StateExitTo_MAINTAIN_CHARGE_BY_FAST_THERMAL_CUT_OFF(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;

    /*This time delay is dependent on battery capacity and charge rate*/
    if( BAT_TEMP > FAST_CHARGE_THERMAL_CUTOFF )

    {
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_MAINTAIN_CHARGE_StateFunc;

		/*Set the Battery charger current limit*/
		setNiMHChargerCurrent(batteryParamsPtr->BatteryNomCapacity/20);


		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;

    }
    return retVal;
}




/******************************************************/
/******************************************************/
/******************************************************/
/* CHARGE_SUSPENDED_ state function definitions       */
/******************************************************/
/******************************************************/
/******************************************************/
static void NiMH_CHARGE_SUSPENDED_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;
	
	/*Check if at entry and if so execute entry code */  
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	NiMH_CHARGE_SUSPENDED_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    NiMH_CHARGE_SUSPENDED_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(NiMH_CHARGE_SUSPENDED_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*  CHARGE_SUSPENDED_ entry function definition       */
/******************************************************/
/******************************************************/
static void NiMH_CHARGE_SUSPENDED_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = NiMH_CHARGE_SUSPENDED_STATE;
}
/******************************************************/
/* CHARGE_SUSPENDED_ unconditional function definition */
/******************************************************/
static void NiMH_CHARGE_SUSPENDED_StateUncondFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish battery current as 0 i.e. battery is being not being charged but also not being used*/
	VIRT_BT_BATTERY_CURRENT = 0;

}
/******************************************************/
static lu_bool_t NiMH_CHARGE_SUSPENDED_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t retVal = LU_FALSE;
    if(*(this->event) == TimerTickEvent)
    {
    	/*Compare monitored and normalised temperature with temperature charging threshold*/
		if( (BAT_TEMP/MILLIDEGREES_TO_DEGREES) < batteryParamsPtr->BatteryMaxTemp)
		{
			/*Change the active state ready for next call to this FSM*/
            *(this->presActiveState)=NiMH_CHECK_CHARGER_StateFunc;
			/*Execute actions for this transition*/

			/*Publish charger temperature ok */
			VIRT_BT_OVER_TEMPERATURE = FLAG_CLR;

			/*Tell the BatteryManagerFSM you can restore charging due to temperature*/
			setChargerTemperatureDisable(LU_FALSE);

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }
    return retVal;
}
/******************************************************/
/******************************************************/
/******************************************************/
/*    MAINTAIN_CHARGE_ state function definitions        */
/******************************************************/
/******************************************************/
/******************************************************/
static void NiMH_MAINTAIN_CHARGE_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;

    /*Check if at entry and if so execute entry code */
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	NiMH_MAINTAIN_CHARGE_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    NiMH_MAINTAIN_CHARGE_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(NiMH_MAINTAIN_CHARGE_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}
/******************************************************/
/******************************************************/
/*  MAINTAIN_CHARGE_ entry function definition           */
/******************************************************/
/******************************************************/
static void NiMH_MAINTAIN_CHARGE_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Reset exit steering flag */
	EXIT_FLAG_MAINTAIN_TO_FAST_CHARGE_STATE = LU_FALSE;

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = NiMH_MAINTAIN_CHARGE_STATE;
	fsmTimerSet(&batteryFullStabilisationTimer, FULL_CHARGE_DELAY_TIME);

	setNiMHScheduledTestAcknowledge(LU_FALSE);
}

/******************************************************/
/* MAINTAIN_CHARGE_ unconditional function definition */
/******************************************************/
static void NiMH_MAINTAIN_CHARGE_StateUncondFunc(fsm *this)
{
	static lu_bool_t chargingActive;

    if(*(this->event) == TimerTickEvent)
    {
    	/*******************************************************************************************************/
		/*                           CHARGER FAILURE DETECTION                                                 */
		/*******************************************************************************************************/
    	chargerFailureDetect(this);
		/*******************************************************************************************************/
		/*                           BATTERY CHARGE RESTING                                                    */
		/*******************************************************************************************************/
		/*MULTI TIMER TICK EVENT SEQUENCE*/
		chargingActive = maintainChargeAndRestSequence(this);
		/*******************************************************************************************************/
		/*                           PRESENCE DETECTION                                                        */
		/*******************************************************************************************************/
		if( chargingActive == LU_FALSE)
		{
			batteryPresenceDetection(this);
		}
		/*******************************************************************************************************/
		/*                           BATTERY FULLY CHARGED                                                     */
		/*******************************************************************************************************/
		if(chargingActive == LU_TRUE)
		{
			isBatteryFullyCharged(this);
		}

	}

    /*The Battery Manager has notified us by this event it wants to execute a scheduled battery test*/
    /*This is the only charger state to accept such a request*/
    /*By publishing we accept here, the battery manager will read this,put us to sleep, and execute the test */
    /*If we are unable to acknowledge this event the battery manager will send it again and again until we do*/
    if( (*(this->event) == ScheduledTestEvent) )
    {
		if( (chargingActive == LU_TRUE) )
		{
			/*WE ARE IN THE CHARGING PART OF THE CHARGE CYCLE*/
            /*Only allow this test from a point where we are in the charging duty cycle*/
			setNiMHScheduledTestAcknowledge(LU_TRUE);
		}
    }
}
/******************************************************/
static lu_bool_t NiMH_MAINTAIN_CHARGE_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
   lu_bool_t retVal = LU_FALSE;

	if( BAT_TEMP > CHARGE_THERMAL_CUTOFF)
	{
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_CHARGE_SUSPENDED_StateFunc;

		/*DISCONNECT CHARGER FROM BATTERY*/
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*FLAGUP we are stopping delivering charge current to the battery*/
		VIRT_CH_ON =  FLAG_CLR;
		/*FLAG UP we are over temperature*/
		VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
		/* We are no longer charging so the battery should no longer be Full */
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
		setChargerTemperatureDisable(LU_TRUE);

		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;
	}

	return retVal;
}
/******************************************************/
static lu_bool_t NiMH_MAINTAIN_CHARGE_StateExitTo_RECOVER_BATTERY(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == RecoverBatteryEvent)
    {
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_RECOVER_BATTERY_StateFunc;
    }
    return retVal;
}

/******************************************************/
static lu_bool_t NiMH_MAINTAIN_CHARGE_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if(*(this->event) == TimerTickEvent)
    {
    	/*Charge phase status flags are determined in the unconditional function of this state*/
    	if( EXIT_FLAG_MAINTAIN_TO_CHECK_CHARGER_STATE == LU_TRUE )
		{
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState) = NiMH_CHECK_CHARGER_StateFunc;

			/*Execute actions for this transition*/

			/* Clear the flag */
			EXIT_FLAG_MAINTAIN_TO_CHECK_CHARGER_STATE = LU_FALSE;

			/* We are no longer charging so the battery should no longer be Full */
			VIRT_BT_FULLY_CHARGED = LU_FALSE;

			/*Terminate the search for exit conditions*/
			retVal = LU_TRUE;
		}
    }

    return retVal;
}


/******************************************************/
/******************************************************/
/******************************************************/
/*    RECOVER_BATTERY_ state function definitions        */
/******************************************************/
/******************************************************/
/******************************************************/
static void NiMH_RECOVER_BATTERY_StateFunc(fsm *this)
{
    lu_uint8_t nextExitFunc=0;

    /*Check if at entry and if so execute entry code */
    if(*(this->presActiveState) != *(this->prevActiveState) )
	{
    	NiMH_RECOVER_BATTERY_StateEntryFunc(this);
		*(this->prevActiveState) = *(this->presActiveState);
	}
    /*Execute any unconditional code */
    NiMH_RECOVER_BATTERY_StateUncondFunc(this);
	/*First check for any global exits then...*/
	if(CHRG_NICKEL_METAL_GLOBAL_Exit_Conditions(this)==LU_FALSE)
	{
		/*...if not see if there are any state dependent exits*/
		while(NiMH_RECOVER_BATTERY_StateExitFuncs[nextExitFunc](this)==LU_FALSE)
		{
			nextExitFunc++;
		}
	}
}

/******************************************************/
/******************************************************/
/*  RECOVER_BATTERY_ entry function definition           */
/******************************************************/
/******************************************************/
static void NiMH_RECOVER_BATTERY_StateEntryFunc(fsm *this)
{
	PARAM_NOT_USED(this);

	/*Publish the current state number*/
	VIRT_BT_CHGR_FSM_STATE = NiMH_RECOVER_BATTERY_STATE;
}

/******************************************************/
/* RECOVER_BATTERY_ unconditional function definition */
/******************************************************/
static void NiMH_RECOVER_BATTERY_StateUncondFunc(fsm *this)
{
	PARAM_NOT_USED(this);

}
/******************************************************/
static lu_bool_t NiMH_RECOVER_BATTERY_StateExitTo_CHARGE_SUSPENDED(fsm *this)
{
   lu_bool_t retVal = LU_FALSE;

	if( BAT_TEMP > CHARGE_THERMAL_CUTOFF)
	{
		/*Change the active state ready for next call to this FSM*/
		*(this->presActiveState)=NiMH_CHARGE_SUSPENDED_StateFunc;

		/*DISCONNECT CHARGER FROM BATTERY*/
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*FLAGUP we are stopping delivering charge current to the battery*/
		VIRT_CH_ON =  FLAG_CLR;
		/*FLAG UP we are over temperature*/
		VIRT_BT_OVER_TEMPERATURE = FLAG_SET;
		/* We are no longer charging so the battery should no longer be Full */
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		/*Tell the BatteryManagerFSM you had to switch off charging due to temperature*/
		setChargerTemperatureDisable(LU_TRUE);

		/*Terminate the search for exit conditions*/
		retVal = LU_TRUE;
	}

	return retVal;
}
/******************************************************/
static lu_bool_t NiMH_RECOVER_BATTERY_StateExitTo_CHECK_CHARGER(fsm *this)
{
    lu_bool_t   retVal = LU_FALSE;

    if( (*(this->event) == TimerTickEvent)   )
    {
    	/**/
    	if(NiMH_BatteryRecovered(this) == LU_TRUE)
    	{
    		/*So battery recovery steps are complete*/
			BATTERY_RECOVERY_INPROGRESS = LU_FALSE;
			/*Change the active state ready for next call to this FSM*/
			*(this->presActiveState)=NiMH_CHECK_CHARGER_StateFunc;
    	}
    }
    return retVal;
}

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/* TIMER FUNCTION DEFINITIONS                                                                         */
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/******************************************************/
/******************************************************/
/* INTERNAL FIXED STATE MACHINE TIMERS                */
/******************************************************/
/******************************************************/

/***************************************************************/
/*Decrements all internal running timers every timer tick event*/
/*to this FSM                                                  */
/***************************************************************/
static void fsmTimersUpdate(void)
{
	lu_uint32_t fsmTimersIndx = 0;
	/*Keep looping until declared timers are done*/
	while( fsmTimers_p[fsmTimersIndx] != NULL_FSM_TIMER )
	{
		/*If the contents of the indexed pointer to timer have not reached 0...*/
		if( *fsmTimers_p[fsmTimersIndx] > 0 )
		{
			/*...then decrement the contents*/
			(*fsmTimers_p[fsmTimersIndx])--;
		}
		fsmTimersIndx++;
	}
}

static void fsmTimerSet( lu_int32_t* timerName, lu_int32_t timerValue)
{
	    /*Store the timer value as the contents of the timer pointed to*/
		*timerName = timerValue;
}

static int fsmTimerGet( lu_int32_t* timerName )
{
	    /*Store the timer value as the contents of the timer pointed to*/
		return  *timerName;
}


/******************************************************/

/******************************************************/

/* ADD MORE FUNCTION DEFINITIONS FOR ADDITIONAL STATES BELOW HERE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/* Remember each State must have an entry function, and unconditional function a number of exit condition functions and these placed in an array*/


/*Add further support function here*/


lu_bool_t getNiMHChargingLevelFsmBatAnalysis(void)
{
	return !BATTERY_NOT_USABLE_FLAG;
}

/*Read access for the Battery Manager FSM to read whether the charger believes the Battery is missing*/
lu_bool_t isNiMHBatteryMissing(void)
{
	return BATTERY_MISSING;
}
/*Read access function*/
lu_bool_t getNiMHScheduledTestAcknowledge(void)
{
	/*WARNING - should only be called by the BAT MANAGR FSM when a scheduled test is waiting to start */
	/*If we can allow a scheduled test tidy up */
	if(SCHEDULED_TEST_ACCEPETED == LU_TRUE)
	{
		/*FLAG UP we will be exiting the charging FSM */
		VIRT_CH_SYSTEM_RUNNING = FLAG_CLR;
		/*FLAG UP we are stopping charge current to the battery*/
		VIRT_CH_ON =  FLAG_CLR;
		/*We are never considered fully charged when disconnected from the charger*/
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		/*Disconnect charger from the battery*/
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*SWITCH THE CHARGER OFF*/
		BAT_CHARGER_EN = CTRL_OFF;
		/*Publish charger is off*/
		VIRT_CHARGER_EN = LU_FALSE;
	}
	return SCHEDULED_TEST_ACCEPETED;

}
/*Write access function*/
void setNiMHScheduledTestAcknowledge(lu_bool_t SchedTestOk)
{
	SCHEDULED_TEST_ACCEPETED = SchedTestOk;
}


/*Read access function*/
lu_bool_t getNiMHBatteryPresenceIndicator(void)
{
	return BatteryPresenceIndicator;
}
/*Write access function*/
void setNiMHBatteryPresenceIndicator(lu_bool_t battPres)
{
	BatteryPresenceIndicator = battPres;
}

/*Read access function*/
chrgHlthStatus getNiMHChargerHealthStatus(void)
{
	return ChargerHealthStatus;
}
/*Write access function*/
void setNiMHChargerHealthStatus(chrgHlthStatus chrgStat)
{
	ChargerHealthStatus = chrgStat;
}

void setNiMHChargerVolts(lu_int32_t vChargerTarget)
{
	/*Update the charger's compliance voltage setting*/
	BAT_CHARGER_VOLT_SET = vChargerTarget;
	/*Publish the charger's compliance voltage setting*/
	VIRT_CH_SET_VOLTAGE = vChargerTarget;
}

void setNiMHChargerCurrent(lu_int32_t currentRqst)
{
	/*If requested current is above max value set it to max value*/
	if( currentRqst > MAX_CHARGER_ABILITY)
	{
		BAT_CHARGER_CUR_LIM_SET = MAX_CHARGER_ABILITY;
	}
	else
	{
		BAT_CHARGER_CUR_LIM_SET = currentRqst;
	}
	/*Publish the charger's current limit setting*/
	VIRT_CH_SET_CURRENT = BAT_CHARGER_CUR_LIM_SET;
}

/*This function is a single purpose function to carry out the process for recovering a NiMH battery when the chemicals*/
/*have dispersed. It is called from the NiMH Recovery State's unconditional function (i.e. every second while in that */
/*state. It carries out a time consuming process of 5 discharges and re-charges to recover the batteries lost capacity*/
/*due to sitting on a shelf.This process is interuptable by a switch gear operate request*/
/*It has been created here in its own function to tidy up unconditional function from where it is executed*/
lu_bool_t NiMH_BatteryRecovered(fsm *this)
{
	lu_bool_t retVal = LU_FALSE;
	static lu_int8_t chargCycleCount = 5;
	static lu_bool_t dischrgChargPhase = LU_FALSE;

	/************************************/
	/*If the recovery steps are complete*/
	/************************************/
	if(chargCycleCount == 0 && dischrgChargPhase == LU_TRUE)
	{
		/*Process steps to re-invigorate NiMH chemicals are complete*/
		retVal = LU_TRUE;
		/*Reset the static variables at completion*/
		chargCycleCount = 5;
		dischrgChargPhase = LU_FALSE;
	}
	/****************************************/
	/*If the recovery steps are NOT complete*/
	/****************************************/
	else
	{
		if(dischrgChargPhase == LU_TRUE)
		{
			/*******************/
			/*Tracking charging*/
			/*******************/

			/*Keep TestLoad off*/
			BAT_TEST_EN = CTRL_OFF;
			/*SET UP THE DRAIN CURRENT*/
			DAC_OUT_BT_TEST_LOAD = 0;

			/*Keep charger on*/
//			/*Set Maximum initial charge time to the time taken to charge from empty to full*/
//			maxInitialChargeTimer = ( (batteryParamsPtr->BatteryNomCapacity*1.2)/BAT_CHARGER_CUR_LIM_SET)*SECONDS_PER_HOUR;
			/*CONNECT CHARGER TO BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_ON;
			/*Publish charger is connected to battery*/
			VIRT_CHARGER_CON = LU_TRUE;
			/*FLAGUP we are now delivering charge current to the battery*/
			VIRT_CH_ON =  FLAG_SET;

			/*Detect charger complete*/
			if( thermalSlopeDetector(this,LU_FALSE) == LU_TRUE )
			{
				dischrgChargPhase = LU_FALSE;
				chargCycleCount--;
			}
		}
		else
		{
			/**********************/
			/*Tracking discharging*/
			/**********************/

			/*Keep charger off*/
//			/*Set Maximum initial charge time to the time taken to charge from empty to full*/
//			maxInitialChargeTimer = ( (batteryParamsPtr->BatteryNomCapacity*1.2)/BAT_CHARGER_CUR_LIM_SET)*SECONDS_PER_HOUR;
			/*DISCONNECT CHARGER FROM BATTERY*/
			BAT_TO_MOT_CONNECT = CTRL_OFF;
			/*Publish charger is disconnected from battery*/
			VIRT_CHARGER_CON = LU_FALSE;
			/*FLAGUP we are now delivering no charge current to the battery*/
			VIRT_CH_ON =  FLAG_CLR;

			/*Keep TestLoad on*/
			BAT_TEST_EN = CTRL_ON;
			/*SET UP THE DRAIN CURRENT*/
			DAC_OUT_BT_TEST_LOAD = 400;

			/*Detect discharge complete*/
			if(BAT_V_SENSE < batteryParamsPtr->BatteryPackDeepDischargeVolts )
			{
				dischrgChargPhase = LU_TRUE;
			}
		}
	}
	return retVal;
}

/*This filter function has been designed to work from the entry state and unconditional state function calls*/
/*of any given state and as synchronous only from timertick events into the FSM*/
/*It works across multiple calls with each call to it effectively a one second clock from which it derives a*/
/*30 second filter clock. As it works over many successive calls it is necessary to initialise it explicitly*/
/*with a call from the state's entry function (or some other single call start point) passing the initFilter*/
/*parameter set to true to initialise it. Calls from the unconditional state function (or some other regular*/
/*1second frequency calling point, which are expected to be normal clocking, should keep the initFilter     */
/*parameter set to false allowing the filter to do its thing*/
lu_bool_t thermalSlopeDetector(fsm *this, lu_bool_t initFilter)
{
	static lu_int8_t filterTickTime=30;
	static lu_int32_t batTempSamples[7];
	static lu_int32_t upDownCounter=0;
	static lu_bool_t fastChrgCompleteSig = LU_FALSE;

	if(*(this->event) == TimerTickEvent)
	{
		/*Thirty second interval counter for filter clock*/
		filterTickTime--;

		/*We have been requested to initialise the filter on this call*/
		if(initFilter == LU_TRUE)
		{
			/*We initialise the filter by saturating it with the present temperature value and ...*/
			batTempSamples[6] = BAT_TEMP;
			batTempSamples[5] = BAT_TEMP;
			batTempSamples[4] = BAT_TEMP;
			batTempSamples[3] = BAT_TEMP;
			batTempSamples[2] = BAT_TEMP;
			batTempSamples[1] = BAT_TEMP;
			batTempSamples[0] = BAT_TEMP;
			/*...setting the output signals to false*/
			fastChrgCompleteSig = LU_FALSE;
			VIRT_NiMH_FAST_CHRG_COMPLETE = LU_FALSE;
		}

		/*If on the thirty second filter clock edge then ...*/
		if(filterTickTime == 0)
		{
			/*...set up next filter clock edge and...*/
			filterTickTime = 30;
			/*...clock through Time domain samples:- */
			/*      batTempSamples[0] is the present sample  through to...*/
			/*      batTempSamples[6] 6th historic sample*/
			batTempSamples[6] = batTempSamples[5];
			batTempSamples[5] = batTempSamples[4];
			batTempSamples[4] = batTempSamples[3];
			batTempSamples[3] = batTempSamples[2];
			batTempSamples[2] = batTempSamples[1];
			batTempSamples[1] = batTempSamples[0];
			/*Simple low pass filter on raw data in to minimise LSB flicker*/
			batTempSamples[0] = (batTempSamples[0]+ BAT_TEMP)/2;

			/*Filtering analogue temperature samples for a positive rise of 5mDegrees/Second*/
			/*LSB flicker is very high compared with the minimum +ve slope we are detecting (at 125mDeg)*/
			/*We will use a little low pass filtering to attenuate any LSB flicker. This means reducing from 5 -> 3 mDeg/second*/
			/*So we are going to clock this filter every 30Seconds and look for our slope across four discrete time domain samples*/
			/*This means we are looking for a change of +360mDegrees for a slope of +3mDeg/S over 4 time samples with 30second interval ie 3*4*30*/
			if( batTempSamples[1]>(batTempSamples[4]+FAST_DETECT_SLOPE ) )
			{
				/*Set up counter limit*/
				if(upDownCounter < MAX_FILTER_COUNT )
				{
					upDownCounter++;
					/*Output trigger*/
					if(upDownCounter >= FAST_CHARGE_DETECT_THRESHOLD )
					{
						/*Set the TSF output signal (FAST_CHARGE_COMPLETE_THERMAL_SIGNAL)*/
						fastChrgCompleteSig = LU_TRUE;
						/*We will be leaving this state so reset the upDown counter now*/
						upDownCounter = 0;
					}
				}
			}
			else
			{
				/*Set down counter limit*/
				if(upDownCounter > 0)
				{
					upDownCounter--;
				}
			}

			/*Publish the fast charge complete flag*/
			VIRT_NiMH_FAST_CHRG_COMPLETE = fastChrgCompleteSig;
		}
	}
	return fastChrgCompleteSig;
}

/*This function is a single purpose to maintain the charge/rest cycle of the maintain charge state. It is called from*/
/*the maintain charge's unconditional state function (called every second while in that state) to enact the 3secs out*/
/*18secs charging duty cycle. It has been created as a function here to tidy up the unconditional function from where*/
/*it is executed*/
lu_bool_t maintainChargeAndRestSequence(fsm *this)
{
	lu_bool_t retValue = LU_FALSE;

	/*Take MOD 18 of presentActiveStateCounter to get our chargeRestCycleSteps repetitive sequence*/
	lu_int32_t chargeRestCycleSteps = *this->presActiveStateSecsCounter%BATTERY_CHARGE_REST_AND_DETECT_REPEAT_PERIOD;

	if( chargeRestCycleSteps <= BATTERY_CHARGE_PERIOD_END)
	{
		/*WE ARE IN THE CHARGING PART OF THE CHARGE CYCLE*/

		/*Keep the charger connected*/
		BAT_TO_MOT_CONNECT = CTRL_ON;
		/*Publish charger is connected to battery*/
		VIRT_CHARGER_CON = LU_TRUE;
		retValue = LU_TRUE;
	}
	else
	{
		/*WE ARE IN THE RESTING PART OF THE CHARGE CYCLE*/

		/*Keep the charger disconnected*/
		BAT_TO_MOT_CONNECT = CTRL_OFF;
		/*Publish charger is disconnected from battery*/
		VIRT_CHARGER_CON = LU_FALSE;
		/*Opportunity to publish the actual battery voltage*/
		VIRT_BATTERY_VOLTAGE = BAT_V_SENSE;
		retValue = LU_FALSE;

	}

	return retValue;
}

/*This function is a single purpose function to detect battery presence while in maintain charge state. It is called from*/
/*the maintain charge's unconditional state function (called every second while in that state) to ensure the battery     */
/*remains present. As NiMH have a charging/resting duty cycle its presence can simply be detected while charging is      */
/*inactive. This function has been created here to tidy up maintain charge's unconditional function*/
lu_bool_t batteryPresenceDetection(fsm *this)
{
	PARAM_NOT_USED(this);
	lu_bool_t retValue = LU_FALSE;
	lu_int32_t vBatApparentWithoutCharger;

	/*Take voltage reading of charging battery*/
	vBatApparentWithoutCharger = BAT_V_SENSE;
	/*Evaluate whether or not we think the battery has gone missing*/
	if(vBatApparentWithoutCharger < BATTERY_PRESENT )
	{
		/*It looks like the battery has gone walkies...*/
		/*...publishing this means the Battery Manager FSM will*/
		/*force this FSM to Idle and places itself in to the Battery Not Present state */
		setNiMHBatteryPresenceIndicator(LU_FALSE);
		VIRT_BT_FULLY_CHARGED = LU_FALSE;
		retValue = LU_FALSE;
	}
	else
	{
		setNiMHBatteryPresenceIndicator(LU_TRUE);
		retValue = LU_TRUE;
	}

	return retValue;
}

/*This function is a single purpose function to detect any charger problems while in maintain charge state. */
/*It is called from the maintain charge's unconditional state function (called every second while in that state) */
/*to flag up any problems with the charger. This function has been created here to tidy up maintain charge's */
/*unconditional function*/
lu_bool_t chargerFailureDetect(fsm *this)
{
	PARAM_NOT_USED(this);
	lu_bool_t retValue = LU_FALSE;
	lu_int32_t vChargerMeasured;
	static lu_int32_t ChargerVoltageFaultCount;

	/* 1. Voltage - Check that the charger is outputting a sensible voltage */
	/*READ BACK THE CHARGER'S VOLTAGE*/
	vChargerMeasured = CHARGER_V_SENSE;

	/*TEST THE CHARGER'S VOLTAGE If it is less than the Deep Discharge voltage something bad has happened */
	if(vChargerMeasured < batteryParamsPtr->BatteryPackDeepDischargeVolts)
	{
		ChargerVoltageFaultCount++;

		/* If after 5 attempts the charger voltage is still low, exit to the check charger state */
		if( ChargerVoltageFaultCount >= 5)
		{
			EXIT_FLAG_MAINTAIN_TO_CHECK_CHARGER_STATE = LU_TRUE;
		}
	}
	else
	{
		/* Reset the ChargerSettingFaultCounter */
		ChargerVoltageFaultCount = 0;
	}

	/* 2. Current - Check that the charger current does not exceed 120% of it setting */
	/* If the charger current exceeds 120% of the set current limit, set the Charger current fault flag */
	if((VIRT_CHARGER_CON == LU_TRUE) && (BAT_CHARGER_I_SENSE > (1.2 * BAT_CHARGER_CUR_LIM_SET)))
	{
		VIRT_CH_CURRENT_FAULT = FLAG_SET;
		retValue = LU_TRUE;
	}
	else
	{
		VIRT_CH_CURRENT_FAULT = FLAG_CLR;
		retValue = LU_FALSE;
	}

	/* If either charger voltage or charger current faults have been set, set charger fault */
	if((VIRT_CH_VOLTAGE_FAULT == FLAG_SET) || (VIRT_CH_CURRENT_FAULT == FLAG_SET))
	{
		VIRT_CH_FAULT = FLAG_SET;
		retValue = LU_TRUE;
	}
	else
	{
		VIRT_CH_FAULT = FLAG_CLR;
		retValue = LU_FALSE;
	}

	return retValue;
}

/*This function is a single purpose function to determine if the battery is fully charged while in maintain charge state.*/
/*It is called from the maintain charge's unconditional state function (called every second while in that state) reports */
/*back via flags whether the battery is fully charged or not*/
lu_bool_t isBatteryFullyCharged(fsm *this)
{
	lu_bool_t retValue = LU_FALSE;

#if 0
	/* Battery is only fully charged if,
	 * the charger is connected
	 * the charger is providing a valid voltage
	 * and the current is bellow a certain threshold 0.01 of the capacity */

	/* Allow for a stabilisation time once entering float */
	if(fsmTimerGet(&batteryFullStabilisationTimer) == FSM_TIMER_TIMED_OUT)
	{

		if((BAT_TO_MOT_CONNECT == CTRL_ON) &&
			(vChargerMeasured > batteryParamsPtr->BatteryPackDeepDischargeVolts) &&
			(BAT_CHARGER_I_SENSE < batteryParamsPtr->BatteryNomCapacity*0.01))
		{
			VIRT_BT_FULLY_CHARGED = LU_TRUE;
			retValue = LU_TRUE;
		}

		/* Include a little hysteresis to avoid the Fully charged signal bouncing */
		if(BAT_CHARGER_I_SENSE > batteryParamsPtr->BatteryNomCapacity*0.014)
		{
			VIRT_BT_FULLY_CHARGED = LU_FALSE;
			retValue = LU_FALSE;
		}
	}
#endif

	return retValue;
}
