/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Processor ADC module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_adc.h"

#include "IOManager.h"

#include "ProcessorGPIOMap.h"
#include "ProcessorADC.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR ProcADCInit(void)
{
	 SB_ERROR  			retError;
	 lu_uint32_t		adcRate;
	 ADC_TYPE_INT_OPT	adcTypIntOpt;

	 retError = SB_ERROR_NONE;

	 adcRate = 2000;
	 ADC_Init(LPC_ADC, adcRate);
	 ADC_IntConfig(LPC_ADC, ADC_ADGINTEN, DISABLE);

	 for (adcTypIntOpt = ADC_ADINTEN0; adcTypIntOpt <= ADC_ADINTEN7; adcTypIntOpt++)
	 {
		 ADC_IntConfig(LPC_ADC, adcTypIntOpt, DISABLE);
	 }

	 return retError;
}

SB_ERROR ProcADCSelectChannel(lu_uint8_t adcChan)
{
	SB_ERROR  			retError;

	retError = SB_ERROR_NONE;

	adcChan &= 0x07;

	/* Select channel */
	ADC_ChannelCmd(LPC_ADC, adcChan, ENABLE);

	return retError;
}

SB_ERROR ProcADCRead(lu_uint8_t adcChan, lu_uint16_t *retValuePtr)
{
	SB_ERROR  			retError;

	retError = SB_ERROR_NONE;

	/* Do the ADC conversion */
	ADC_StartCmd(LPC_ADC, ADC_START_NOW);

	/* Wait for conversion & get value */
	while (!(ADC_ChannelGetStatus(LPC_ADC, adcChan, ADC_DATA_DONE)));

	*retValuePtr = ADC_ChannelGetData(LPC_ADC, adcChan);

	ADC_ChannelCmd(LPC_ADC, adcChan, DISABLE);

	return retError;
}

SB_ERROR ProcADCConfigure(
		                  lu_uint8_t 		port,
	                      lu_uint8_t 		pin,
		                  GPIO_PIN_FUNC     func
		                 )
{
	SB_ERROR  			retError;
	lu_uint8_t  		portPinIdx;
	lu_uint8_t			pinDir;
	lu_uint8_t          pinFunc;
	PINSEL_CFG_Type 	pinCfg;
	lu_uint32_t	        pinShift;

	retError = SB_ERROR_INITIALIZED;

	pinDir = 0; /* Default to input */

	pinShift = 1;
	pinShift = pinShift << (pin & 0x1f);

	/* Now do the pin configuration */
	if (port < MAX_GPIO_PORTS && pin < MAX_GPIO_PORT_BITS)
	{
		portPinIdx = pin + (port << 5);
		if (portPinIdx < NUM_GPIO_PINPORT_IDX)
		{
			pinFunc = ProcGPIOCheckPeripheralPinPort(func, port, pin);

			if (pinFunc)
			{
				/* Set pin configuration */
				pinCfg.Funcnum   = pinFunc;
				pinCfg.Portnum   = port;
				pinCfg.Pinnum    = pin;
				pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
				pinCfg.Pinmode   = PINSEL_PINMODE_TRISTATE;

				/* Configure pin type */
				PINSEL_ConfigPin(&pinCfg);

				/* Set pin direction */
				//GPIO_SetDir(port, pinShift, pinDir);

				/* pin configuration was successful */
				retError = SB_ERROR_NONE;
			}
		}
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
