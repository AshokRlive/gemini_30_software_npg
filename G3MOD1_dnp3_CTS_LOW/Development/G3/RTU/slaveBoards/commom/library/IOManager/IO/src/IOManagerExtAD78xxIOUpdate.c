/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager ADE78xx - advanced ADC Update module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/11/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "systemTime.h"
#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerExtAD78xxIOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"
#include "ProcessorSPI.h"

#include "ADE78xx.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"

#include "peakDetector.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define ADE78XX_I2C_ADDR		(0x38)

/* ADC reset timeout in ms */
#define ADE78XX_RESET_TIMEOUT   (100)

/* Invalid channel used in the HSDCChMap variable */
#define INVALID_IO_CH 0xFFFFFFFF

/* 24ZP and 28ZP conversion constant */
#define ADE78XX_24ZP_SIGNUM     (0x08000000)
#define ADE78XX_24ZP_PAD        (0xFF000000)
#define ADE78XX_28ZP_SIGNUM     (0x00800000)
#define ADE78XX_28ZP_PAD        (0xF0000000)

/* Scaling factor multiplier */
#define ADE78XX_SF_MULTIPLIER   (1000000)

/* ADE78XX gain register divider (see datasheet) */
#define ADE78XX_GAIN_DIVIDER    (1 << 23)

/*!
 * \brief Convert a number from the 24ZP format to the standard 32bit format
 */
#define ADE78XX_24ZP_2_32(_val_)\
{\
    if(_val_ & ADE78XX_24ZP_SIGNUM)\
    {\
        /* Signed value. Pad the 8 MSBs with 1s */\
        _val_ |= ADE78XX_24ZP_PAD;\
    }\
}

/*!
 * \brief Convert a number from the 32bit standard format to the 24ZP format.
 *
 * The input value range is NOT checked
 */
#define ADE78XX_32_2_24ZP(_val_) (_val_ &= (~(ADE78XX_24ZP_PAD)))

/*!
 * \brief Convert a number from the 28ZP format to the standard 32bit format
 */
#define ADE78XX_28ZP_2_32(_val_)\
{\
    if(_val_ & ADE78XX_28ZP_SIGNUM)\
    {\
        /* Signed value. Pad the 8 MSBs with 1s */\
        _val_ |= ADE78XX_28ZP_PAD;\
    }\
}

/*!
 * \brief Convert a number from the 32bit standard format to the 28ZP format.
 *
 * The input value range is NOT checked
 */
#define ADE78XX_32_2_28ZP(_val_) (_val_ &= (~(ADE78XX_28ZP_PAD)))


/* Configuration */
#define ADE78XX_IGAIN -8385324
#define ADE78XX_VGAIN -8385324

/* Voltage channel with respect to current phase compensation */
#define ADE78XX_APHCAL_VALUE 0
#define ADE78XX_BPHCAL_VALUE 0
#define ADE78XX_CPHCAL_VALUE 0

/* HCLK: 0 (8 Mhz)
 * HSIZE: 1 (32 bit registers in 8-bit packages)
 * HGAP: 0 (no gap)
 * HXFER: 01 (only instantaneous values)
 * HSAPOL: 0 (SS active low)
 * Reserved: 00
 */
//#define ADE78XX_HSDC_CONFIG 	0x00	//ADE7878#ACPZ1407 FDM XB Mod!!!
#define ADE78XX_CONFIG_A		0x740

#define LAST_OP					0xE7FD
#define	LAST_OP_WRITE			0xCA
#define	LAST_OP_READ			0x35
#define VERSION_OF_THE_DIE		0xe707
#define DIE_VERSION_VALUE		0x8

#define HSDC_NUMBER_OF_SAMPLES 		8
#define NUMBER_OF_AVERAGED_SAMPLES 	20
#define NUMBER_OF_PEAK_SAMPLES 	20

#define GPDMA_CNTRL_RGST_VALUE		((GPDMA_DMACCxControl_I | \
									GPDMA_DMACCxControl_DI | \
									GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_BYTE) | \
									GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_BYTE) | \
									GPDMA_DMACCxControl_DBSize(GPDMA_BSIZE_4) | \
									GPDMA_DMACCxControl_SBSize(GPDMA_BSIZE_4) | \
									GPDMA_DMACCxControl_TransferSize(224)) & \
									GPDMA_DMACCxControl_BITMASK)

#define GPDMA_CNTRL_RGST_VALUE1		((GPDMA_DMACCxControl_I | \
									GPDMA_DMACCxControl_DI | \
									GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_BYTE) | \
									GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_BYTE) | \
									GPDMA_DMACCxControl_DBSize(GPDMA_BSIZE_4) | \
									GPDMA_DMACCxControl_SBSize(GPDMA_BSIZE_4) | \
									GPDMA_DMACCxControl_TransferSize(112)) & \
									GPDMA_DMACCxControl_BITMASK)

#define GPDMA_CNTRL_RGST_VALUE2		((GPDMA_DMACCxControl_I | \
									GPDMA_DMACCxControl_DI | \
									GPDMA_DMACCxControl_DWidth(GPDMA_WIDTH_BYTE) | \
									GPDMA_DMACCxControl_SWidth(GPDMA_WIDTH_BYTE) | \
									GPDMA_DMACCxControl_DBSize(GPDMA_BSIZE_4) | \
									GPDMA_DMACCxControl_SBSize(GPDMA_BSIZE_4) | \
									GPDMA_DMACCxControl_TransferSize(224)) & \
									GPDMA_DMACCxControl_BITMASK)

#define ADE78XX_CONFIG_VALUE		((ADE78XX_CONFIG_SWAP | \
									ADE78XX_CONFIG_HSDCEN | \
									ADE78XX_CONFIG_VTOIA(ADE78XX_CONFIG_VTOIA_A) | \
									ADE78XX_CONFIG_VTOIB(ADE78XX_CONFIG_VTOIB_B) | \
									ADE78XX_CONFIG_VTOIC(ADE78XX_CONFIG_VTOIC_C)) & \
									ADE78XX_CONFIG_BITMASK)

#define ADE78XX_HSDC_CFG_VALUE		((ADE78XX_HSDC_HSIZE | \
									ADE78XX_HSDC_HGAP | \
									ADE78XX_HSDC_HXFER(ADE78XX_HSDC_HXFER_VI)) & \
									ADE78XX_HSDC_BITMASK)


/* \brief Convert a 32-bit value from big endian to little endian */
#define BIG_2_LITTLE_ENDIAN(big, little) little = __builtin_bswap32(big)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*! SPI DMA buffer index */
typedef enum
{
    DMA_BUF_IDX_0 = 0,
    DMA_BUF_IDX_1    ,
    DMA_BUF_IDX_2    ,
    DMA_BUF_IDX_3    ,

    DMA_BUF_IDX_LAST
}DMA_BUF_IDX;

/*! IO SPI/I2C ADC (ADE78XX) Address structure */
typedef struct ADE78xxIoIDDef
{
	lu_int32_t        adcCfg1;
	lu_int32_t        adcCfg2;
	lu_int32_t        adcPm0;
	lu_int32_t        adcPm1;
	lu_int32_t        adcManualReset;
	lu_int32_t        adcIrq0;
	lu_int32_t        adcIrq1;
	lu_int32_t		  adcCs;

	IO_BUS_I2C        i2cBus;
	IO_BUS_SPI        spiBus;

}ADE78xxIoIDStr;

//#pragma pack(push)
//#pragma pack(1) //Note: versions.h disables packing

typedef struct ADE78xxRegisterValuesDef
{
	lu_int32_t registerValue[ADE78XX_HSDC_CH_LAST];
}ADE78xxRegisterValuesStr;

typedef struct ADE78xxSamplingPeriodDef
{
	ADE78xxRegisterValuesStr samplingPeriod[HSDC_NUMBER_OF_SAMPLES];
}ADE78xxSamplingPeriodStr;

typedef struct ADE78xxDMAChannelDef
{
	ADE78xxSamplingPeriodStr dmaChannel[DMA_BUF_IDX_LAST];
}ADE78xxDMAChannelStr;

//#pragma pack(pop)

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR configureCheckI2CADE78xx(lu_uint8_t i2cBus);
static SB_ERROR configureCheckHSDCADE78xx(lu_uint8_t spiBus);
static SB_ERROR configureCheckSPIADE78xx(lu_uint8_t spiBus);
static SB_ERROR configureADE78xx(void);
static lu_int32_t scaleFactor2GainRegister(lu_int32_t scaleFactor);
static SB_ERROR readSpiADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint32_t *valuePtr);
static SB_ERROR readSpiADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t *valuePtr);
static SB_ERROR readSpiADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint8_t *valuePtr);
static SB_ERROR writeSpiADE78xxRaw(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint8_t *buffer, lu_uint8_t length);
static SB_ERROR writeSpiADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint32_t value);
static SB_ERROR writeSpiADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t value);
static SB_ERROR writeSpiADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint8_t value);
static SB_ERROR readI2cADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint32_t *value);
static SB_ERROR readI2cADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint16_t *value);
static SB_ERROR readI2cADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint8_t *value);
static SB_ERROR writeI2cADE78xxRAW(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint8_t *buffer, lu_uint8_t length);
static SB_ERROR writeI2cADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint32_t value);
static SB_ERROR writeI2cADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint16_t value);
static SB_ERROR writeI2cADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint8_t value);

static SB_ERROR movingAverage(lu_uint32_t dmaIndex, lu_uint32_t sampleIndex, lu_uint8_t averagedDataLength);
lu_bool_t simplePeakDetectADE78xx(DMA_CH_0_PHASE phase, lu_uint32_t length, lu_int32_t value, lu_int32_t *retValuePtr);

void ADE78xxSPIDMAIntHandler(void);
void ADE78XXDecodePacket(IO_BUS_SPI sspBus, lu_uint8_t bufIdx);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static ADE78xxIoIDStr ade78xxIoIDPins[SPI_ADE78XX_CH_LAST];
static IO_BUS_I2C     ade78ccI2cBus;

/* HSDC DMA buffers. The buffers are located in the secondary RAM to not
 * interfere with the main processor memory
 */
lu_uint8_t HSDCDMABuffer[DMA_BUF_IDX_LAST][sizeof(ADE78xxSamplingPeriodStr)] __attribute__((section("ahbsram1")));

/* DMA controller linked list. The two buffers point to each
 * other in a "ping-pong" like configuration
 */
GPDMA_LLI_Type GPDMAlli[DMA_BUF_IDX_LAST] =
{
    /*Source Addr  Destination address                          Next LLI address                         GPDMA Control */
    {0x40030008, (lu_uint32_t)(&HSDCDMABuffer[DMA_BUF_IDX_0]), (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_1]), 0},
    {0x40030008, (lu_uint32_t)(&HSDCDMABuffer[DMA_BUF_IDX_1]), (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_2]), GPDMA_CNTRL_RGST_VALUE2},
    {0x40030008, (lu_uint32_t)(&HSDCDMABuffer[DMA_BUF_IDX_2]), (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_3]), GPDMA_CNTRL_RGST_VALUE2},
    {0x40030008, (lu_uint32_t)(&HSDCDMABuffer[DMA_BUF_IDX_3]), (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_0]), GPDMA_CNTRL_RGST_VALUE2}
};

/* Digital filter and peak detector buffers. One buffer per channel */
static filteredPeakDetectorDataStr peackDetector[ADE78XX_HSDC_CH_LAST];

/* Instantaneous data mapping */
static lu_uint32_t HSDCChMap[ADE78XX_HSDC_CH_LAST] =
{
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH
};

/* Peak data mapping */
static lu_uint32_t HSDCChMapPeak[DMA_CH_0_PHASE_LAST] =
{
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH,
    INVALID_IO_CH
};

static ADE78XXPacketDecode ADE78XXPacketDecodeFunc = NULL;

static lu_int32_t 	RawData[DMA_BUF_IDX_LAST][HSDC_NUMBER_OF_SAMPLES][ADE78XX_HSDC_CH_LAST];
static lu_int32_t 	Sum[DMA_CH_0_PHASE_LAST];
static lu_int32_t 	AveragedData[DMA_CH_0_PHASE_LAST][NUMBER_OF_AVERAGED_SAMPLES];
//static lu_int32_t neutral[20];
static lu_uint32_t 	MaxValue[DMA_CH_0_PHASE_LAST];
static lu_uint32_t 	SumLengthCounter[DMA_CH_0_PHASE_LAST];
static lu_int32_t 	Peak[DMA_CH_0_PHASE_LAST][20];

#ifdef TRACE_BUFFER
#define TRACE_BUFFER_SIZE 1024
static lu_uint32_t traceBufferDelay = 0;
static lu_uint32_t traceBufferIdx = 0;
lu_int32_t traceBufferR[TRACE_BUFFER_SIZE] __attribute__((section("ahbsram1")));
lu_int32_t traceBufferF[TRACE_BUFFER_SIZE] __attribute__((section("ahbsram1")));
#endif

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR IOManagerExtAD78xxIOInit(void)
{
    SB_ERROR	retError;
    lu_uint32_t i;

    retError = SB_ERROR_NONE;

    /* Initialise peak detector */
    for(i = 0; i < ADE78XX_HSDC_CH_LAST; i++)
    {
        filteredPeakDetectorDataStr *peackDetectorPtr;
        peackDetectorPtr = &peackDetector[i];

        filteredPeakDetectorFullReset(peackDetectorPtr);

        /* Use default3rd order IIR filter coefficients */
        peackDetectorPtr->IIRdata.xc[0] = XC1_3;
        peackDetectorPtr->IIRdata.xc[1] = XC2_3;
        peackDetectorPtr->IIRdata.xc[2] = XC3_3;
        peackDetectorPtr->IIRdata.xc[3] = XC4_3;
        peackDetectorPtr->IIRdata.yc[0] = YC1_3;
        peackDetectorPtr->IIRdata.yc[1] = YC2_3;
        peackDetectorPtr->IIRdata.yc[2] = YC3_3;
        peackDetectorPtr->IIRdata.yc[3] = 0;
    }

    ade78xxIoIDPins[SPI_ADE78XX_CH_A].adcCs = BOARD_IO_MAX_IDX;
    ade78xxIoIDPins[SPI_ADE78XX_CH_B].adcCs = BOARD_IO_MAX_IDX;

    /* Configure I2C pins & peripheral */
    configureCheckI2CADE78xx(IO_BUS_I2C_0);
    configureCheckI2CADE78xx(IO_BUS_I2C_1);
    configureCheckI2CADE78xx(IO_BUS_I2C_2);

    /* Configure HSDC SPI pins & peripheral */
//    configureCheckHSDCADE78xx(IO_BUS_SPI_SSPI_0);
    configureCheckHSDCADE78xx(IO_BUS_SPI_SSPI_1);

    /* Configure SPI pins & peripheral */
//    configureCheckSPIADE78xx(IO_BUS_SPI_SSPI_0);
//    configureCheckSPIADE78xx(IO_BUS_SPI_SSPI_1);

    /* Now configure ADE78xx and initialise the state */
    retError = configureADE78xx();

    return retError;
}

SB_ERROR IOManagerExtADE78xxIORegisterCallBack(ADE78XXPacketDecode callBackFunc)
{
	SB_ERROR retVal = SB_ERROR_PARAM;

	if (callBackFunc != NULL)
	{
		retVal = SB_ERROR_NONE;
		ADE78XXPacketDecodeFunc = callBackFunc;
	}

	return retVal;
}

SB_ERROR IOManagerExtAD78xxIOUpdate(void)
{
	SB_ERROR	        retError;
	lu_uint8_t 			i2cBus;
	lu_uint8_t          spiBus;
	lu_uint16_t			regAddr;
	lu_uint32_t			value;
	lu_uint32_t			ioID;
	lu_uint32_t         mode;
	SPI_ADE78XX_CH      spiChan;

	retError = SB_ERROR_NONE;

	/* Perform an IO update on all IO in the ioTable[] */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
	    if( ((BoardIOMap[ioID].ioDev == IO_DEV_I2C_ADE7854) ||
	    		(BoardIOMap[ioID].ioDev == IO_DEV_SPI_ADE7854))     &&
	        (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_INPUT)
	      )
	    {
            /* Is an I2C ADE78xx ADC analogue input*/
            regAddr  = BoardIOMap[ioID].ioAddr.i2cADE78xx.regLow;
            regAddr |= (BoardIOMap[ioID].ioAddr.i2cADE78xx.regHigh << 8);


            switch(BoardIOMap[ioID].ioDev)
            {
            case IO_DEV_I2C_ADE7854:
            	i2cBus   =  BoardIOMap[ioID].ioAddr.i2cADE78xx.busI2c;

            	/* Read I2C register for ADE78xx */
            	retError = readI2cADE78xxReg32Bit(regAddr, i2cBus, &value);
            	break;

            case IO_DEV_SPI_ADE7854:
            	spiChan = BoardIOMap[ioID].ioAddr.spiADE78xx.spiChan;
            	spiBus  = ade78xxIoIDPins[spiChan].spiBus;

            	/* Read I2C register for ADE78xx */
            	retError = readSpiADE78xxReg32Bit(regAddr, spiBus, ade78xxIoIDPins[spiChan].adcCs, &value);
            	break;

            default:
            	break;
            }


            if (retError == SB_ERROR_NONE)
            {
				/* Apply signum extension if necessary */
				mode = BoardIOMap[ioID].ioAddr.i2cADE78xx.mode;

				if((mode & GPIO_PM_ADC_REG_24ZP_PKA) ||
				   (mode & GPIO_PM_ADC_REG_24ZP_PKB) ||
				   (mode & GPIO_PM_ADC_REG_24ZP_PKC)
				  )
				{
					if ((value & (7 << 24)) == (7 << 24))
					{
						/* No Phase current */
						value = 0;

						/* Save RAW value */
						boardIOTable[ioID].rawValue = value;
						boardIOTable[ioID].value    = value;

						ioID++;

						boardIOTable[ioID].rawValue = value;
						boardIOTable[ioID].value    = value;

						ioID++;

						boardIOTable[ioID].rawValue = value;
						boardIOTable[ioID].value    = value;

						ioID++;
					}
					else if (value & (1 << 24) && (mode & GPIO_PM_ADC_REG_24ZP_PKA))
					{
						value &= 0xffffff;

						/* 24 bit value transmitted as a 32bit value
						 * with eight MSBs padded with 0s
						 */
						if(value & ADE78XX_24ZP_SIGNUM)
						{
							/* Signed value. Pad the 8 MSBs with 1s */
							value |= ADE78XX_24ZP_PAD;
						}

						/* Save RAW value */
						boardIOTable[ioID].rawValue = value;
						boardIOTable[ioID].value    = value;

						ioID += 2;
					}
					else if(value & (1 << 25) && (mode & GPIO_PM_ADC_REG_24ZP_PKB))
					{
						value &= 0xffffff;

						/* 24 bit value transmitted as a 32bit value
						 * with eight MSBs padded with 0s
						 */
						if(value & ADE78XX_24ZP_SIGNUM)
						{
							/* Signed value. Pad the 8 MSBs with 1s */
							value |= ADE78XX_24ZP_PAD;
						}

						ioID += 1;

						/* Save RAW value */
						boardIOTable[ioID].rawValue = value;
						boardIOTable[ioID].value    = value;

						ioID += 1;
					}
					else if(value & (1 << 26) && (mode & GPIO_PM_ADC_REG_24ZP_PKC))
					{
						value &= 0xffffff;

						/* 24 bit value transmitted as a 32bit value
						 * with eight MSBs padded with 0s
						 */
						if(value & ADE78XX_24ZP_SIGNUM)
						{
							/* Signed value. Pad the 8 MSBs with 1s */
							value |= ADE78XX_24ZP_PAD;
						}

						ioID += 2;

						/* Save RAW value */
						boardIOTable[ioID].rawValue = value;
						boardIOTable[ioID].value    = value;
					}
				}
				else
				{
					if(mode & GPIO_PM_ADC_REG_24ZP)
					{
						/* 24 bit value transmitted as a 32bit value
						 * with eight MSBs padded with 0s
						 */
						if(value & ADE78XX_24ZP_SIGNUM)
						{
							/* Signed value. Pad the 8 MSBs with 1s */
							value |= ADE78XX_24ZP_PAD;
						}
					}
					else if(value & ADE78XX_28ZP_SIGNUM)
					{
						/* 28 bit value transmitted as a 32bit value
						 * with four MSBs padded with 0s
						 */
						if(value & ADE78XX_28ZP_SIGNUM)
						{
							/* Signed value. Pad the 8 MSBs with 1s */
							value |= ADE78XX_28ZP_PAD;
						}
					}

					/* Save RAW value */
					boardIOTable[ioID].rawValue = value;

					/* Apply filter here... All the filtering
					 * already applied in the ADC/DSP
					 */
					boardIOTable[ioID].value  = value;
				}
			}
		}
    }

	return retError;
}

SB_ERROR IOManagerExtAD78xxWriteReg(ADE7878WriteRegStr *ADE7878WriteRegParam)
{
	SB_ERROR retError = SB_ERROR_PARAM;

	retError = writeI2cADE78xxRAW( ADE7878WriteRegParam->reg,
								   ade78ccI2cBus,
								   ADE7878WriteRegParam->data,
								   ADE7878WriteRegParam->length
							 	 );

	return CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
						MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_R,
						sizeof(SB_ERROR),
						(lu_uint8_t *)&retError
		   			  );
}

SB_ERROR IOManagerExtAD78xxReadReg(ADE7878ReadRegStr *ADE7878ReadRegParam)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	ADE7878ReadRspStr readReg = {0,0};

	if(ADE7878ReadRegParam->length == 1)
	{
		readReg.errorCode = readI2cADE78xxReg8Bit(ADE7878ReadRegParam->reg, ade78ccI2cBus, (lu_uint8_t*)&readReg.value);
	}

	if(ADE7878ReadRegParam->length == 2)
	{
		readReg.errorCode = readI2cADE78xxReg16Bit(ADE7878ReadRegParam->reg, ade78ccI2cBus, (lu_uint16_t*)&readReg.value);
	}

	if(ADE7878ReadRegParam->length == 4)
	{
		readReg.errorCode = readI2cADE78xxReg32Bit(ADE7878ReadRegParam->reg, ade78ccI2cBus, (lu_uint32_t*)&readReg.value);
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
							MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_R,
							sizeof(ADE7878ReadRspStr),
							(lu_uint8_t *)&readReg
	   				      );

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief SPI0 RX DMA ISR
 *
 *   \return None
 *
 ******************************************************************************
 */
void ADE78xxSPIDMAIntHandler(void)
{
    static lu_uint32_t error = 0;
    static lu_uint32_t idxCounter; //DMA_BUF_IDX_0;
    static lu_uint32_t counter;
//    static lu_uint32_t idxCounter1 = DMA_BUF_IDX_0;
    SB_ERROR retError;

    GPDMA_Channel_CFG_Type  GPDMACfg;


    /* Set start ISR pin */
//    GPIO_SetValue(PINSEL_PORT_0, 1 << PINSEL_PIN_10);

    /* check GPDMA interrupt on channel 0. Not necessary just one channel used
     * Check counter terminal status
     */
    if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
    {
        /* Clear terminate counter Interrupt pending */
        LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);

        ADE78XXDecodePacket(0, idxCounter);

		idxCounter++;

     if (idxCounter == (sizeof(DMA_BUF_IDX)))
     {
    	 /* Set start ISR pin */
//    	 GPIO_SetValue(PINSEL_PORT_0, 1 << PINSEL_PIN_10);
    	 idxCounter = 0;
    	  // Disable GPDMA channel 0
//          GPDMA_ChannelCmd(0, DISABLE);
//          NVIC_DisableIRQ (DMA_IRQn);

    	 GPDMA_Init();
         /* DMA Channel (0 highest priority) */
         GPDMACfg.ChannelNum = 0;
         /* Source memory - not used */
         GPDMACfg.SrcMemAddr = 0;
         /* Destination memory */
         GPDMACfg.DstMemAddr = (lu_uint32_t)(&HSDCDMABuffer[DMA_BUF_IDX_0]);
         /* Transfer size */
         GPDMACfg.TransferSize = sizeof(ADE78xxSamplingPeriodStr);
         /* Transfer width - not used */
         GPDMACfg.TransferWidth = 0;
         /* Transfer type */
         GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
         /* Source connection */
         GPDMACfg.SrcConn =  GPDMA_CONN_SSP1_Rx;
         /*  Destination connection - not used */
         GPDMACfg.DstConn = 0;
         /*  Linker List Item - unused */
         GPDMACfg.DMALLI = (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_1]);
    	 /* Setup channel with given parameter */
    	 GPDMA_Setup(&GPDMACfg);

    	 /* Enable Rx DMA */
    	 SSP_DMACmd(LPC_SSP1, SSP_DMA_RX, ENABLE);

    	 /* Enable GPDMA channel 0 */
    	 GPDMA_ChannelCmd(0, ENABLE);

    	 /* Clear start ISR pin */
//    	 GPIO_ClearValue(PINSEL_PORT_0, 1 << PINSEL_PIN_10);
     }

/*     if(counter==20)
     {

    	 // Disable GPDMA channel 0
         GPDMA_ChannelCmd(0, DISABLE);
         NVIC_DisableIRQ (DMA_IRQn);
     counter = 0;
     }*/
    }
     else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
     {
         /*  Check error terminal status */
         error++;
         /* Clear error counter Interrupt pending */
         LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
     }
}
/*!
 ******************************************************************************
 *   \brief Decode PIC packet
 *
 *   Detailed description
 *
 *   \param sspBus
 *   \param bufIdx
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
void ADE78XXDecodePacket(IO_BUS_SPI sspBus, lu_uint8_t bufIdx)
{
	static lu_uint8_t			length;
	static lu_uint8_t 			peakIndex;
	ModuleTimeStr	    		canTime;
	lu_uint32_t 				peakValue;
	SB_ERROR 					retError;

    ADE78xxSamplingPeriodStr 	*buffPtr;
    ADE78xxSamplingPeriodStr 	buff;
    lu_uint32_t i=0;
    lu_uint32_t y=0;
    lu_uint32_t z=0;

	canTime     = CANCGetTime();

	switch(sspBus)
	{
	case IO_BUS_SPI_SSPI_0:
		buffPtr = (ADE78xxSamplingPeriodStr *)&HSDCDMABuffer[bufIdx][0];
		break;

	case IO_BUS_SPI_SSPI_1:

		break;

	default:
		return;
		break;
	}

	/* Copy to working buffer to prevent next DMA corrupting, should ISR be delayed */
	memcpy((void *)&buff, buffPtr, sizeof(ADE78xxSamplingPeriodStr));
	buffPtr = &buff;

	/* HSDC data are in big-endian format.
	 * Covert them in little-endian format
	 */
	for (y = 0; y < HSDC_NUMBER_OF_SAMPLES; y++)
	{
		for (i = 0; i < ADE78XX_HSDC_CH_LAST; i++ )
		{
			BIG_2_LITTLE_ENDIAN( buffPtr->samplingPeriod[y].registerValue[i], RawData[bufIdx][y][i]);
		}
	}


	retError = movingAverage( bufIdx, HSDC_NUMBER_OF_SAMPLES, length);

	for (z=0; z<DMA_CH_0_PHASE_LAST; z++)
	{
		switch (z)
		{
		case DMA_CH_0_PHASE_A:
			if (simplePeakDetect(DMA_CH_0_PHASE_A, 10, AveragedData[DMA_CH_0_PHASE_A][length], &peakValue) == LU_TRUE)
			{
				Peak[DMA_CH_0_PHASE_A][peakIndex] = peakValue;
				IOManagerSetOnline(HSDCChMapPeak[z], LU_TRUE);
				IOManagerSetValue(HSDCChMapPeak[z], peakValue);
				/* Now call ISR routine callback to trigger on FPI threshold */
				if (ADE78XXPacketDecodeFunc != NULL)
				{
					ADE78XXPacketDecodeFunc(HSDCChMapPeak[z], canTime);
				}
			}
			break;

		case DMA_CH_0_PHASE_B:
			if (simplePeakDetect(DMA_CH_0_PHASE_B, 10, AveragedData[DMA_CH_0_PHASE_B][length], &peakValue) == LU_TRUE)
			{
				Peak[DMA_CH_0_PHASE_B][peakIndex] = peakValue;
				IOManagerSetOnline(HSDCChMapPeak[z], LU_TRUE);
				IOManagerSetValue(HSDCChMapPeak[z], peakValue);
				/* Now call ISR routine callback to trigger on FPI threshold */
				if (ADE78XXPacketDecodeFunc != NULL)
				{
					ADE78XXPacketDecodeFunc(HSDCChMapPeak[z], canTime);
				}
			}
			break;

		case DMA_CH_0_PHASE_C:
			if (simplePeakDetect(DMA_CH_0_PHASE_C, 10, AveragedData[DMA_CH_0_PHASE_C][length], &peakValue) == LU_TRUE)
			{
				Peak[DMA_CH_0_PHASE_C][peakIndex] = peakValue;
				IOManagerSetOnline(HSDCChMapPeak[z], LU_TRUE);
				IOManagerSetValue(HSDCChMapPeak[z], peakValue);
				/* Now call ISR routine callback to trigger on FPI threshold */
				if (ADE78XXPacketDecodeFunc != NULL)
				{
					ADE78XXPacketDecodeFunc(HSDCChMapPeak[z], canTime);
				}
			}
			break;

		case DMA_CH_0_PHASE_N:
			if (simplePeakDetect(DMA_CH_0_PHASE_N, 10, AveragedData[DMA_CH_0_PHASE_N][length], &peakValue) == LU_TRUE)
			{
				Peak[DMA_CH_0_PHASE_N][peakIndex] = peakValue;
				IOManagerSetOnline(HSDCChMapPeak[z], LU_TRUE);
				IOManagerSetValue(HSDCChMapPeak[z], peakValue);
				peakIndex++;
				if (peakIndex==NUMBER_OF_PEAK_SAMPLES) {peakIndex=0;}
				/* Now call ISR routine callback to trigger on FPI threshold */
				if (ADE78XXPacketDecodeFunc != NULL)
				{
					ADE78XXPacketDecodeFunc(HSDCChMapPeak[z], canTime);
				}
			}
			break;

		default :
			break;
		}
	}

	length++;
	if (length==NUMBER_OF_AVERAGED_SAMPLES) {length=0;}
/*
	static lu_int32_t 		peakIndex;
	lu_int32_t 				peakValue;

	if (simplePeakDetect(DMA_CH_0_PHASE_A, 10, AveragedData[DMA_CH_0_PHASE_A][length], &peakValue) == LU_TRUE)
	{
		Peak[DMA_CH_0_PHASE_A][peakIndex] = peakValue;
	}

	if (simplePeakDetect(DMA_CH_0_PHASE_B, 10, AveragedData[DMA_CH_0_PHASE_B][length], &peakValue) == LU_TRUE)
	{
		Peak[DMA_CH_0_PHASE_B][peakIndex] = peakValue;
	}

	if (simplePeakDetect(DMA_CH_0_PHASE_C, 10, AveragedData[DMA_CH_0_PHASE_C][length], &peakValue) == LU_TRUE)
	{
		Peak[DMA_CH_0_PHASE_C][peakIndex] = peakValue;
	}

	if (simplePeakDetect(DMA_CH_0_PHASE_N, 10, AveragedData[DMA_CH_0_PHASE_N][length], &peakValue) == LU_TRUE)
	{
		Peak[DMA_CH_0_PHASE_N][peakIndex] = peakValue;
		peakIndex++;
		if (peakIndex==NUMBER_OF_PEAK_SAMPLES) {peakIndex=0;}
	}
*/




/*
	else
	{
		IOManagerSetOnline(FEPICChMap[packetPtr->adcChannel][sspBus], LU_FALSE);
	}
*/
}

/*!
 ******************************************************************************
 *   \brief Moving Average
 *
 *   Detailed description
 *
 *   \param dmaIndex
 *   \param sampleIndex
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
SB_ERROR movingAverage(lu_uint32_t dmaIndex, lu_uint32_t sampleIndex, lu_uint8_t averagedDataLength)
{
	 /* Set start ISR pin */
	 GPIO_SetValue(PINSEL_PORT_0, 1 << PINSEL_PIN_10);

	static lu_uint8_t 		i;
	SB_ERROR 				retError = SB_ERROR_NONE;;

	for (i = 0; i < sampleIndex; i++)
	{
		Sum[DMA_CH_0_PHASE_A] = Sum[DMA_CH_0_PHASE_A] + RawData[dmaIndex][i][ADE78XX_HSDC_CH_VAWV];
		Sum[DMA_CH_0_PHASE_B] = Sum[DMA_CH_0_PHASE_B] + RawData[dmaIndex][i][ADE78XX_HSDC_CH_VBWV];
		Sum[DMA_CH_0_PHASE_C] = Sum[DMA_CH_0_PHASE_C] + RawData[dmaIndex][i][ADE78XX_HSDC_CH_VCWV];
	}
	i = 0;

	/* 8 point average */
	AveragedData[DMA_CH_0_PHASE_A][averagedDataLength] = (Sum[DMA_CH_0_PHASE_A] >> 3);
	AveragedData[DMA_CH_0_PHASE_B][averagedDataLength] = (Sum[DMA_CH_0_PHASE_B] >> 3);
	AveragedData[DMA_CH_0_PHASE_C][averagedDataLength] = (Sum[DMA_CH_0_PHASE_C] >> 3);
	/* Summation to generate the neutral current */
	AveragedData[DMA_CH_0_PHASE_N][averagedDataLength] = 	AveragedData[DMA_CH_0_PHASE_A][averagedDataLength]+ \
															AveragedData[DMA_CH_0_PHASE_B][averagedDataLength]+ \
															AveragedData[DMA_CH_0_PHASE_C][averagedDataLength];

	Sum[DMA_CH_0_PHASE_A] = 0;
	Sum[DMA_CH_0_PHASE_B] = 0;
	Sum[DMA_CH_0_PHASE_C] = 0;

	 /* Clear start ISR pin */
	 GPIO_ClearValue(PINSEL_PORT_0, 1 << PINSEL_PIN_10);
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Peak detect
 *
 *   Detailed description
 *
 *   \param dmaIndex
 *   \param sampleIndex
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
lu_bool_t simplePeakDetectADE78xx(DMA_CH_0_PHASE phase, lu_uint32_t length, lu_int32_t value, lu_int32_t *retValuePtr)
{
	value = abs(value);
	SumLengthCounter[phase]++;
	//Check all the sum values, apart from the last,
	//for a given length and locate the peak
	if (SumLengthCounter[phase] < length)
	{
	    //Compare value in the sum data array
	    //against a temporary value
	    if (value > MaxValue[phase])
		{
	    	MaxValue[phase] = value;
		}
	    return LU_FALSE;
	}

	else if (SumLengthCounter[phase] >= length)
	{
	    //Compare last value in the sum data array
	    //against a temporary value
	    if (value > MaxValue[phase])
		{
	    	MaxValue[phase] = value;
		}

	    SumLengthCounter[phase] = 0;
	    *retValuePtr = MaxValue[phase];
	    //reset temporary variable
	    MaxValue[phase] = 0;
	    return LU_TRUE;
	}
	else
	{
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Check for I2C ADE78xx ADC I2C device & configure I2C if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckI2CADE78xx(lu_uint8_t i2cBus)
{
	SB_ERROR				retVal;
	lu_uint32_t				ioID;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		/* Is Peripheral */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_GPIO_PERIPH:
			switch(BoardIOMap[ioID].ioDev)
			{
			/* Is an I2C Expander device */
			case IO_DEV_I2C_ADE7854:
				if (BoardIOMap[ioID].ioAddr.i2cExpander.busI2c == i2cBus)
				{
					/* Save details of I2C bus ADE78xx found on */
					ade78ccI2cBus = i2cBus;

					// find & configure I2C peripheral pins
					retVal = configureI2CPeripheral(i2cBus);
					ioID = BOARD_IO_MAX_IDX;
				}
				break;

			default:
				break;
			}
			break;

		default:
			break;

		}
	}

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Check for HSDC SPI ADE78xx ADC & configure SPI if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckHSDCADE78xx(lu_uint8_t spiBus)
{
    SB_ERROR                retVal;
    lu_uint32_t             ioID;
    SSP_CFG_Type            sspConfig;
    GPDMA_Channel_CFG_Type  GPDMACfg;
    SUIRQPriorityStr        priority;
    static lu_bool_t        initDMA = LU_FALSE;
    lu_uint32_t z=0;
    lu_uint32_t y=0;
//    lu_uint32_t DMAControl;
//    lu_uint32_t DMASrcAddr;
//    lu_uint32_t DMADstAddr;
//    lu_uint32_t DMANextLLI;

    retVal = SB_ERROR_INITIALIZED;

    for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
    {
        if( (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH) &&
            (BoardIOMap[ioID].ioDev   == IO_DEV_HSDC_ADE7854)   &&
            (BoardIOMap[ioID].ioAddr.spiAdc.busSspi == spiBus)
          )
        {
            /* Init SPI configuration */
            SSP_ConfigStructInit(&sspConfig);

            /* Override default values */
            sspConfig.CPHA        = SSP_CPHA_SECOND;
            sspConfig.CPOL        = SSP_CPOL_LO;
            sspConfig.Mode        = SSP_SLAVE_MODE;
            sspConfig.FrameFormat = SSP_FRAME_SPI;
            sspConfig.ClockRate   = 8000000;
//            sspConfig.Databit 	  = SSP_DATABIT_8;

            /* find & configure SPI peripheral pins */
            retVal = configureSPISspSlPeripheral(spiBus, &sspConfig);
            break;
        }
    }

    if(retVal == SB_ERROR_NONE)
    {
        /* Peripheral found and configured correctly.
         * Initialise DMA engine
         */

        /* Disable DMA Interrupt */
//        NVIC_DisableIRQ (DMA_IRQn);

    	for (z = 0; z< DMA_BUF_IDX_LAST; z++)
    	{
			for (y = 0; y < sizeof(ADE78xxSamplingPeriodStr); y++)
			{
				HSDCDMABuffer[z][y] = 0;
			}
    	}

        if (initDMA == LU_FALSE)
        {
        	// Initialise GPDMA controller
        	GPDMA_Init();
        	initDMA = LU_TRUE;
        }

//        GPDMALLI.DMACCLLI = (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_0]);

        /* DMA Channel (0 highest priority) */
        GPDMACfg.ChannelNum = 0;
        /* Source memory - not used */
        GPDMACfg.SrcMemAddr = 0;
        /* Destination memory */
        GPDMACfg.DstMemAddr = (lu_uint32_t)(&HSDCDMABuffer[DMA_BUF_IDX_0]);
        /* Transfer size */
        GPDMACfg.TransferSize = sizeof(ADE78xxSamplingPeriodStr);
        /* Transfer width - not used */
        GPDMACfg.TransferWidth = 0;
        /* Transfer type */
        GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
        /* Source connection */
        GPDMACfg.SrcConn = ( (spiBus == IO_BUS_SPI_SSPI_0) ? GPDMA_CONN_SSP0_Rx : GPDMA_CONN_SSP1_Rx );
        /*  Destination connection - not used */
        GPDMACfg.DstConn = 0;
        /*  Linker List Item - unused */
        GPDMACfg.DMALLI = (lu_uint32_t)(&GPDMAlli[DMA_BUF_IDX_1]);

        /* DMA Request Connection */
//        GPDMARqType.DMACSoftBReq = ((spiBus == IO_BUS_SPI_SSPI_0) ? GPDMA_CONN_SSP0_Rx : GPDMA_CONN_SSP1_Rx );

//        GPDMALLI.Control;
//        GPDMALLI.SrcAddr;
//        GPDMALLI.DstAddr;
//        GPDMALLI.NextLLI;

        /* Setup channel with given parameter */
        GPDMA_Setup(&GPDMACfg);

        /* Enable Rx DMA */
        SSP_DMACmd( (spiBus == IO_BUS_SPI_SSPI_0) ? LPC_SSP0 : LPC_SSP1,
                     SSP_DMA_RX,
                     ENABLE
                  );

        /* Enable interrupt for DMA. Set the highest priority
         * for the DMA Engine interrupt
         */
//        priority.group       = SU_IRQ_GROUP_0;
//        priority.subPriority = SU_IRQ_SUB_PRIORITY_0;
//        SUSetIRQPriority(DMA_IRQn, priority, LU_TRUE);
//        GPDMALLI.Control;
//        GPDMALLI.SrcAddr;
//        GPDMALLI.DstAddr;
//        GPDMALLI.NextLLI;

        /* Enable GPDMA channel 0 */
        GPDMA_ChannelCmd(0, ENABLE);
    }

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Check for SPI ADE78xx ADC & configure SPI if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckSPIADE78xx(lu_uint8_t spiBus)
{
    SB_ERROR                retVal;
    lu_uint32_t             ioID;
    lu_uint32_t             csIoID;
    SSP_CFG_Type            sspConfig;
    GPDMA_Channel_CFG_Type  GPDMACfg;
    SUIRQPriorityStr        priority;
    SPI_ADE78XX_CH          adcChan;

    retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		if( (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH) &&
			(BoardIOMap[ioID].ioDev   == IO_DEV_SPI_ADE7854)   &&
			(BoardIOMap[ioID].ioAddr.spiAdc.busSspi == spiBus)
		  )
		{
			csIoID 	= BoardIOMap[ioID].ioAddr.spiPeriphADE78xx.csIoID;
			adcChan = (BoardIOMap[csIoID].ioAddr.gpio.pinMode & GPIO_PM_ADC_CH_A) ?
						SPI_ADE78XX_CH_A : SPI_ADE78XX_CH_B;

			ade78xxIoIDPins[adcChan].adcCs           = csIoID;

			retVal = SB_ERROR_NONE;
		}
	}

	if(retVal == SB_ERROR_NONE)
	{
		/* Peripheral found and so.
		 * Initialise ...
		 */

		/* Init SPI configuration */
		SSP_ConfigStructInit(&sspConfig);

		/* Override default values */
		sspConfig.CPHA        = SSP_CPHA_SECOND;
		sspConfig.CPOL        = SSP_CPOL_LO;
		sspConfig.ClockRate   = 2500000;

		/* find & configure SPI peripheral pins */
		retVal = configureSPIParamPeripheral(spiBus, &sspConfig);
	}

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief configure ADE78xx ADC
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureADE78xx(void)
{
	SB_ERROR	        	retError;
	lu_uint32_t				ioID;
	lu_int32_t              retValS;
	lu_uint32_t             retValU;
	lu_uint16_t             retVal16U;
	lu_uint8_t             	retVal8U;
	lu_int32_t              param;
	lu_int16_t              param16U;
	lu_uint32_t             startTime;
	lu_int8_t				idx;
	SPI_ADE78XX_CH          adcChan;
	IO_BUS_SPI     			ade78ccSpiBus;
	lu_uint32_t 			adcCsIoID;

	retError = SB_ERROR_NONE;

	for (adcChan = SPI_ADE78XX_CH_A; adcChan < SPI_ADE78XX_CH_LAST; adcChan++)
	{
		ade78xxIoIDPins[adcChan].adcCfg1 			= BOARD_IO_MAX_IDX;
		ade78xxIoIDPins[adcChan].adcCfg2 			= BOARD_IO_MAX_IDX;
		ade78xxIoIDPins[adcChan].adcIrq0 			= BOARD_IO_MAX_IDX;
		ade78xxIoIDPins[adcChan].adcIrq1 			= BOARD_IO_MAX_IDX;
		ade78xxIoIDPins[adcChan].adcPm0  			= BOARD_IO_MAX_IDX;
		ade78xxIoIDPins[adcChan].adcPm1  			= BOARD_IO_MAX_IDX;
		ade78xxIoIDPins[adcChan].adcManualReset 	= BOARD_IO_MAX_IDX;
	}

	/* Find ioID's for pins required to control ADE78xx */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		/* Is GPIO */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_INPUT:
			adcChan = (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CH_A) ?
			        				SPI_ADE78XX_CH_A : SPI_ADE78XX_CH_B;

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CFG1)
			{
				ade78xxIoIDPins[adcChan].adcCfg1 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CFG2)
			{
				ade78xxIoIDPins[adcChan].adcCfg2 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_IRQ0)
			{
				ade78xxIoIDPins[adcChan].adcIrq0 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_IRQ1)
			{
				ade78xxIoIDPins[adcChan].adcIrq1 = ioID;
			}
			break;

		case IO_CLASS_DIGITAL_OUTPUT:
			adcChan = (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CH_A) ?
						        SPI_ADE78XX_CH_A : SPI_ADE78XX_CH_B;

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_RESET)
			{
				ade78xxIoIDPins[adcChan].adcManualReset = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_PM0)
			{
				ade78xxIoIDPins[adcChan].adcPm0 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_PM1)
			{
				ade78xxIoIDPins[adcChan].adcPm1 = ioID;
			}
			break;

		case IO_CLASS_ANALOG_INPUT:
		    if(BoardIOMap[ioID].ioDev == IO_DEV_HSDC_ADE7854)
		    {
		    	DMA_CH_0_PHASE ch;

		        /* Configure RAW WaveForm channel */
		        ch = BoardIOMap[ioID].ioAddr.hsdcDE78xx.hsdcChan;
		        if(ch < DMA_CH_0_PHASE_LAST)
		        {
		        	HSDCChMapPeak[ch] = ioID;
		        }
		    }
        break;

		default:
			break;
		}
	}

	for (adcChan = SPI_ADE78XX_CH_A; adcChan < SPI_ADE78XX_CH_LAST; adcChan++)
	{
		if ( ade78xxIoIDPins[adcChan].adcPm0 != BOARD_IO_MAX_IDX ||
			 ade78xxIoIDPins[adcChan].adcPm1 != BOARD_IO_MAX_IDX ||
			 ade78xxIoIDPins[adcChan].adcManualReset != BOARD_IO_MAX_IDX
		   )
		{
			/* Reset ADE78xx chip */
			IOManagerSetValue(ade78xxIoIDPins[adcChan].adcCs, 0);
			IOManagerSetValue(ade78xxIoIDPins[adcChan].adcManualReset, 1);
			STDelayTime(20);

			/* Release from reset */
			IOManagerSetValue(ade78xxIoIDPins[adcChan].adcManualReset, 0);

			/* Peripheral available. Start initialisation procedure */
			STDelayTime(30);

			/* At startup the ADC is in Power Mode PSM3 (Sleep Mode).
			 * Set the ADC Power Mode to PSM0(Normal Power Mode)
			 * (PM0 -> 1, PM1 -> 0)
			 */
			IOManagerSetValue(ade78xxIoIDPins[adcChan].adcPm0, 1);
			IOManagerSetValue(ade78xxIoIDPins[adcChan].adcPm1, 0);

			/* Wait 40 ms for the activation. At the end of the activation
			 * the IRQ1 is set to 0. If the IRQ is not set after 100ms
			 * generate an error
			 */
			startTime = STGetTime();
			do
			{
				IOManagerGetValue(ade78xxIoIDPins[adcChan].adcIrq1, &retValS);
				if(STElapsedTime(startTime, STGetTime()) > ADE78XX_RESET_TIMEOUT)
				{
					/* Timeout expired. Return an error */
					retError = SB_ERROR_AD78XX_RESET;
					break;
				}
			}
			while(retValS > 0);

			STDelayTime(40);

			if(retError == SB_ERROR_NONE)
			{
				/* Determine if I2C or SPI mode ? */
				if (ade78xxIoIDPins[adcChan].adcCs != BOARD_IO_MAX_IDX)
				{
					adcCsIoID = ade78xxIoIDPins[adcChan].adcCs;

					/* Setup in SPI mode - toggle CS pin 3 times */

					for (idx = 0; idx < 3; idx++)
					{
						IOManagerSetValue(ade78xxIoIDPins[adcChan].adcCs, 1);
						STDleayTimeUsec(10000);
						IOManagerSetValue(ade78xxIoIDPins[adcChan].adcCs, 0);
					}
					IOManagerSetValue(ade78xxIoIDPins[adcChan].adcCs, 1);

					/* Find which SPI bus */
					for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
					{
						switch(BoardIOMap[ioID].ioClass)
						{
						case IO_CLASS_GPIO_PERIPH:
							if (BoardIOMap[ioID].ioDev == IO_DEV_SPI_ADE7854)
							{
								if (BoardIOMap[ioID].ioAddr.spiPeriphADE78xx.csIoID == ade78xxIoIDPins[adcChan].adcCs)
								{
									ade78ccSpiBus = BoardIOMap[ioID].ioAddr.spiPeriphADE78xx.busSspi;
								}
							}
							break;

						default:
							break;
						}
					}

					ade78xxIoIDPins[adcChan].spiBus = ade78ccSpiBus;

					/* Reset all IRQs. Reset STATUS0 and STATUS1 registers*/
					retError = readSpiADE78xxReg32Bit(ADE78XX_STATUS0, ade78ccSpiBus, adcCsIoID, &retValU);
					retError = writeSpiADE78xxReg32Bit(ADE78XX_STATUS0, ade78ccSpiBus, adcCsIoID, retValU);
					retError = readSpiADE78xxReg32Bit(ADE78XX_STATUS1, ade78ccSpiBus, adcCsIoID, &retValU);
					retError = writeSpiADE78xxReg32Bit(ADE78XX_STATUS1, ade78ccSpiBus, adcCsIoID, retValU);

					/************************/
					/* Initialise Registers */
					/************************/

					/* Current Gain register */
					param = ADE78XX_IGAIN;
					ADE78XX_32_2_28ZP(param);
					//param = 0;
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_AIGAIN, ade78ccSpiBus, adcCsIoID, param);
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_BIGAIN, ade78ccSpiBus, adcCsIoID, param);
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_CIGAIN, ade78ccSpiBus, adcCsIoID, param);
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_NIGAIN, ade78ccSpiBus, adcCsIoID, param);

					/* Voltage Gain register */
					param = ADE78XX_VGAIN;
					ADE78XX_32_2_28ZP(param);
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_AVGAIN, ade78ccSpiBus, adcCsIoID, param);
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_BVGAIN, ade78ccSpiBus, adcCsIoID, param);
					retError =  writeSpiADE78xxReg32Bit(ADE78XX_CVGAIN, ade78ccSpiBus, adcCsIoID, param);

					/* Enable peak detection */
					writeSpiADE78xxReg8Bit(ADE78XX_MMODE, ade78ccSpiBus, adcCsIoID, (0x7 << 2)); // Enable PEAKSEL[0..2]
					//writeSpiADE78xxReg8Bit(ADE78XX_MMODE, ade78ccSpiBus, 0x02); // Enable PEAKSEL[0..2]

					writeSpiADE78xxReg8Bit(ADE78XX_PEAKCYC, ade78ccSpiBus, adcCsIoID, 8);

					retError = readSpiADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccSpiBus, adcCsIoID, &retVal16U);

					/* Write the last register 2 more times to flush the DSP internal pipeline */
					writeSpiADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccSpiBus, adcCsIoID, retVal16U);
					writeSpiADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccSpiBus, adcCsIoID, retVal16U);

					writeSpiADE78xxReg8Bit(ADE78XX_CONFIG_A,ade78ccSpiBus, adcCsIoID, 0x00);

					/* Enable memory protection */
					//writeSpiADE78xxReg8Bit(0xE7FE, ade78ccSpiBus, adcCsIoID, 0xAD);
					//writeSpiADE78xxReg8Bit(0xE7E3, ade78ccSpiBus, adcCsIoID, 0x80);

					/* RUN DSP */
					retError = writeSpiADE78xxReg16Bit(ADE78XX_RUN, ade78ccSpiBus, adcCsIoID, 0x0001);
				}
				else
				{
					/* Broken - only works with one I2C bus!!! */
					ade78xxIoIDPins[adcChan].i2cBus = ade78ccI2cBus;

					/* Reset all IRQs. Reset STATUS0 and STATUS1 registers*/
					retError = readI2cADE78xxReg32Bit(ADE78XX_STATUS0, ade78ccI2cBus, &retValU);
					retError = writeI2cADE78xxReg32Bit(ADE78XX_STATUS0, ade78ccI2cBus, retValU);
					retError = readI2cADE78xxReg32Bit(ADE78XX_STATUS1, ade78ccI2cBus, &retValU);

					/* If the last operation was a read, the LAST_OP
					register stores the value 0x35.*/
/*					retError = readI2cADE78xxReg8Bit(LAST_OP, ade78ccI2cBus, &retVal8U);
					if(retVal8U != LAST_OP_READ)
					{
						retError = SB_ERROR_I2C_FAIL;
					}
*/
					retError = writeI2cADE78xxReg32Bit(ADE78XX_STATUS1, ade78ccI2cBus, retValU);
					/* If the last operation was a write, the LAST_OP
					register stores the value 0xCA.*/
/*					retError = readI2cADE78xxReg8Bit(LAST_OP, ade78ccI2cBus, &retVal8U);
					if(retVal8U != LAST_OP_WRITE)
					{
						retError = SB_ERROR_I2C_FAIL;
					}
*/
					retError = readI2cADE78xxReg8Bit(VERSION_OF_THE_DIE, ade78ccI2cBus, &retVal8U);
					if(retVal8U != DIE_VERSION_VALUE)
					{
						retError = SB_ERROR_PARAM;
					}

					/************************/
					/* Initialise Registers */
					/************************/

//					param16U = ADE78XX_APHCAL_VALUE;
//					ADE78XX_32_2_28ZP(param16U);
					//param = 0;
					retError =  writeI2cADE78xxReg16Bit(ADE78XX_APHCAL, ade78ccI2cBus, ADE78XX_APHCAL_VALUE);

//					param16U = ADE78XX_BPHCAL_VALUE;
//					ADE78XX_32_2_28ZP(param16U);
					retError =  writeI2cADE78xxReg16Bit(ADE78XX_BPHCAL, ade78ccI2cBus, ADE78XX_BPHCAL_VALUE);

//					param16U = ADE78XX_CPHCAL_VALUE;
//					ADE78XX_32_2_28ZP(param16U);
					retError =  writeI2cADE78xxReg16Bit(ADE78XX_CPHCAL, ade78ccI2cBus, ADE78XX_CPHCAL_VALUE);

					/* Current Gain register */
					param = ADE78XX_IGAIN;
					ADE78XX_32_2_28ZP(param);
					//param = 0;
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_AIGAIN, ade78ccI2cBus, param);
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_BIGAIN, ade78ccI2cBus, param);
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_CIGAIN, ade78ccI2cBus, param);
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_NIGAIN, ade78ccI2cBus, param);

					/* Voltage Gain register */
					param = ADE78XX_VGAIN;
					ADE78XX_32_2_28ZP(param);
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_AVGAIN, ade78ccI2cBus, param);
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_BVGAIN, ade78ccI2cBus, param);
					retError =  writeI2cADE78xxReg32Bit(ADE78XX_CVGAIN, ade78ccI2cBus, param);

					/* Enable peak detection */
					writeI2cADE78xxReg8Bit(ADE78XX_MMODE, ade78ccI2cBus, (0x7 << 2)); // Enable PEAKSEL[0..2]
					//writeI2cADE78xxReg8Bit(ADE78XX_MMODE, ade78ccI2cBus, 0x02); // Enable PEAKSEL[0..2]

					writeI2cADE78xxReg8Bit(ADE78XX_PEAKCYC, ade78ccI2cBus, 8);


					/* Enable HSDC Interface */
					writeI2cADE78xxReg8Bit(ADE78XX_HSDC_CFG, ade78ccI2cBus, ADE78XX_HSDC_CFG_VALUE);
					retError = readI2cADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccI2cBus, &retVal16U);
					retVal16U |= ADE78XX_CONFIG_VALUE;
					writeI2cADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccI2cBus, retVal16U);

					/* Write the last register 2 more times to flush the DSP internal pipeline */
					writeI2cADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccI2cBus, retVal16U);
					writeI2cADE78xxReg16Bit(ADE78XX_CONFIG, ade78ccI2cBus, retVal16U);

					writeI2cADE78xxReg8Bit(ADE78XX_CONFIG_A,ade78ccI2cBus, 0x01);

					/* Enable memory protection */
					//writeI2cADE78xxReg8Bit(0xE7FE, ade78ccI2cBus, 0xAD);
					//writeI2cADE78xxReg8Bit(0xE7E3, ade78ccI2cBus, 0x80);

					/* RUN DSP */
					retError = writeI2cADE78xxReg16Bit(ADE78XX_RUN, ade78ccI2cBus, 0x0001);
				}
			}
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Convert a current/voltage scale factor to the ADC equivalent
 *   gain register
 *
 *   Detailed description
 *
 *   \param scaleFactor Scale factor multiplied by 1000000 (see ADE78XX_SF_MULTIPLIER)
 *
 *
 *   \return Gain Register value (in 28ZP format)
 *
 ******************************************************************************
 */
static lu_int32_t scaleFactor2GainRegister(lu_int32_t scaleFactor)
{
    lu_int64_t scaleFactorLong =  (lu_int64_t)(scaleFactor * (lu_int64_t)(ADE78XX_GAIN_DIVIDER));

    scaleFactorLong = scaleFactorLong - ((lu_int64_t)((lu_int64_t)(ADE78XX_GAIN_DIVIDER) * (lu_int64_t)(ADE78XX_SF_MULTIPLIER)));
    scaleFactor = scaleFactorLong/ADE78XX_SF_MULTIPLIER;

    return (ADE78XX_32_2_28ZP(scaleFactor));
}

/*!
 ******************************************************************************
 *   \brief Do an SSP read of 32bit register
 *
 *   Detailed description
 *
 *   \param regAddr
 *   \param spiBus
 *   \param csIoID
 *   \param *valuePtr
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readSpiADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint32_t *valuePtr)
{
	lu_uint8_t				txBuf[7];
	lu_uint8_t				rxBuf[7];
	lu_uint32_t				pinValue;
	lu_uint32_t				value;
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	//csIoID = BoardIOMap[ioID].ioAddr.spiAdc.csIoID;

	txBuf[0] = 0x01;
	txBuf[1] = (regAddr >> 8);
	txBuf[3] = (regAddr & 0xff);
	txBuf[3] = 0x00;

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 7;

	switch (spiBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	value  = rxBuf[6];
	value |= ((rxBuf[5] & 0x0f) << 8);
	value |= ((rxBuf[4] & 0x0f) << 16);
	value |= ((rxBuf[3] & 0x0f) << 24);

	/* Return the read value */
	*valuePtr = value;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SSP read of 16bit register
 *
 *   Detailed description
 *
 *   \param regAddr
 *   \param spiBus
 *   \param csIoID
 *   \param *valuePtr
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readSpiADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t *valuePtr)
{
	lu_uint8_t				txBuf[5];
	lu_uint8_t				rxBuf[5];
	lu_uint32_t				pinValue;
	lu_uint16_t				value;
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	//csIoID = BoardIOMap[ioID].ioAddr.spiAdc.csIoID;

	txBuf[0] = 0x01;
	txBuf[1] = (regAddr >> 8);
	txBuf[2] = (regAddr & 0xff);
	txBuf[3] = 0x00;

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 5;

	switch (spiBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	value  = rxBuf[4];
	value |= ((rxBuf[3] & 0x0f) << 8);

	/* Return the read value */
	*valuePtr = value;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SSP read of 8bit register
 *
 *   Detailed description
 *
 *   \param regAddr
 *   \param spiBus
 *   \param csIoID
 *   \param *valuePtr
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readSpiADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint8_t *valuePtr)
{
	lu_uint8_t				txBuf[4];
	lu_uint8_t				rxBuf[4];
	lu_uint32_t				pinValue;
	lu_uint8_t				value;
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	//csIoID = BoardIOMap[ioID].ioAddr.spiAdc.csIoID;

	txBuf[0] = 0x01;
	txBuf[1] = (regAddr >> 8);
	txBuf[2] = (regAddr & 0xff);
	txBuf[3] = 0x00;

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 4;

	switch (spiBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	value  = rxBuf[3];

	/* Return the read value */
	*valuePtr = value;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SSP read of 8bit register
 *
 *   Detailed description
 *
 *   \param regAddr
 *   \param spiBus
 *   \param csIoID
 *   \param *valuePtr
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeSpiADE78xxRaw(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint8_t *buffer, lu_uint8_t length)
{
	lu_uint8_t				txBuf[7];
	lu_uint8_t				rxBuf[7];
	lu_uint32_t				pinValue;
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint8_t				idx;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	//csIoID = BoardIOMap[ioID].ioAddr.spiAdc.csIoID;

	txBuf[0] = 0x0;
	txBuf[1] = (regAddr >> 8);
	txBuf[2] = (regAddr & 0xff);

	for (idx = 0; idx < length; idx++)
	{
		txBuf[idx + 3] = buffer[idx];
	}

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = (3 + length);

	switch (spiBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return retVal;
		break;
	}

	/* Check length */
	if (length == 0 || length > 4 || length == 3)
	{
		retVal = SB_ERROR_PARAM;
	}

	if (retVal == SB_ERROR_NONE)
	{
		/* Activate CS - low */
		pinValue = 0;
		IOManagerSetValue(csIoID, pinValue);

		xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

		/* deActivate CS - high */
		pinValue = 1;
		IOManagerSetValue(csIoID, pinValue);

		if (xferLen != xferConfig.length)
		{
			retVal = SB_ERROR_SPI_FAIL;
		}
	}

	return retVal;
}

inline SB_ERROR writeSpiADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint32_t value)
{
    lu_uint8_t tmpBuffer[sizeof(value)];

    /* Convert value form LSB to MSB */
    tmpBuffer[0] = (value >> 24) & 0xff;
    tmpBuffer[1] = (value >> 16) & 0xff;
    tmpBuffer[2] = (value >>  8) & 0xff;
    tmpBuffer[3] = value & 0xff;

    /* Write the value */
    return writeSpiADE78xxRaw(regAddr, spiBus, csIoID, tmpBuffer, sizeof(value));
}

inline SB_ERROR writeSpiADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t value)
{
    lu_uint8_t tmpBuffer[sizeof(value)];

    /* Convert value form LSB to MSB */
    tmpBuffer[0] = (value >>  8) & 0xff;
    tmpBuffer[1] = value & 0xff;

    /* Write the value */
    return writeSpiADE78xxRaw(regAddr, spiBus, csIoID, tmpBuffer, sizeof(value));
}

inline SB_ERROR writeSpiADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint8_t value)
{
    /* Write the value */
    return writeSpiADE78xxRaw(regAddr, spiBus, csIoID, &value, sizeof(value));
}

/*!
 ******************************************************************************
 *   \brief read I2C 32 bit register ADE78xx ADC
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint32_t *value)
{
	SB_ERROR	        	retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
	lu_uint8_t				txData[4];
	lu_uint8_t				rxData[4];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Register Addr MSB */
		 txData[0]						= (regAddr >> 8);
		 /* Register Addr LSB */
		 txData[1]						= (regAddr & 0xff);

		 txferCfg.sl_addr7bit     	    = ADE78XX_I2C_ADDR;
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 2;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 4;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
			 *value  = rxData[3];
			 *value |= (rxData[2] << 8);
			 *value |= (rxData[1] << 16);
			 *value |= (rxData[0] << 24);
		 }
	 }

	return retError;
}

/*!
 ******************************************************************************
 *   \brief read I2C 16 bit register ADE78xx ADC
 *
 *   \param regAddr Register address
 *   \param i2cBus I2C bus ID
 *   \param value buffer where the value is stored
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint16_t *value)
{
    SB_ERROR                retError;
    LPC_I2C_TypeDef         *i2cChanType;
    I2C_M_SETUP_Type        txferCfg;
    lu_uint8_t              txData[4];
    lu_uint8_t              rxData[4];

    retError = SB_ERROR_NONE;

    switch (i2cBus)
    {
    case IO_BUS_I2C_0:
        i2cChanType = LPC_I2C0;
        break;

    case IO_BUS_I2C_1:
        i2cChanType = LPC_I2C1;
        break;

    case IO_BUS_I2C_2:
        i2cChanType = LPC_I2C2;
        break;

    default:
        retError = SB_ERROR_PARAM;
        break;
    }

     if (retError == SB_ERROR_NONE)
     {
         /* Register Addr MS */
         txData[0]                      = (regAddr >> 8);
         /* Register Addr LSB */
         txData[1]                      = (regAddr & 0xff);

         txferCfg.sl_addr7bit           = ADE78XX_I2C_ADDR;
         txferCfg.tx_data               = txData;
         txferCfg.tx_length             = 2;
         txferCfg.rx_data               = rxData;
         txferCfg.rx_length             = 2;
         txferCfg.retransmissions_max   = 2;

         if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
         {
             retError = SB_ERROR_I2C_FAIL;
         }
         else
         {
             *value  = rxData[1];
             *value |= (rxData[0] << 8);
         }
     }

    return retError;
}

/*!
 ******************************************************************************
 *   \brief read I2C 8 bit register ADE78xx ADC
 *
 *   \param regAddr Register address
 *   \param i2cBus I2C bus ID
 *   \param value buffer where the value is stored
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint8_t *value)
{
    SB_ERROR                retError;
    LPC_I2C_TypeDef         *i2cChanType;
    I2C_M_SETUP_Type        txferCfg;
    lu_uint8_t              txData[4];
    lu_uint8_t              rxData[4];

    retError = SB_ERROR_NONE;

    switch (i2cBus)
    {
    case IO_BUS_I2C_0:
        i2cChanType = LPC_I2C0;
        break;

    case IO_BUS_I2C_1:
        i2cChanType = LPC_I2C1;
        break;

    case IO_BUS_I2C_2:
        i2cChanType = LPC_I2C2;
        break;

    default:
        retError = SB_ERROR_PARAM;
        break;
    }

     if (retError == SB_ERROR_NONE)
     {
         /* Register Addr MS */
         txData[0]                      = (regAddr >> 8);
         /* Register Addr LSB */
         txData[1]                      = (regAddr & 0xff);

         txferCfg.sl_addr7bit           = ADE78XX_I2C_ADDR;
         txferCfg.tx_data               = txData;
         txferCfg.tx_length             = 2;
         txferCfg.rx_data               = rxData;
         txferCfg.rx_length             = 1;
         txferCfg.retransmissions_max   = 2;

         if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
         {
             retError = SB_ERROR_I2C_FAIL;
         }
         else
         {
             *value = rxData[0];
         }
     }

    return retError;
}


/*!
 ******************************************************************************
 *   \brief write I2C register ADE78xx ADC
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeI2cADE78xxRAW(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint8_t *buffer, lu_uint8_t length)
{
	SB_ERROR	        	retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
	lu_uint8_t				idx;
	lu_uint8_t				txData[10];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	/* Check length */
	if (length == 0 || length > 4)
	{
		retError = SB_ERROR_PARAM;
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Register Addr MS */
		txData[0]						= (regAddr >> 8);
		/* Register Addr LSB */
		txData[1]						= (regAddr & 0xff);

		for (idx = 0; idx < length; idx++)
		{
			txData[idx + 2] = buffer[idx];
		}

		txferCfg.sl_addr7bit     	    = ADE78XX_I2C_ADDR;
		txferCfg.tx_data		  		= txData;
		txferCfg.tx_length       		= length + 2;
		txferCfg.rx_data		 	 	= NULL;
		txferCfg.rx_length       		= 0;
		txferCfg.retransmissions_max 	= 2;

		if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		{
			retError = SB_ERROR_I2C_FAIL;
		}
	}

	return retError;
}

inline SB_ERROR writeI2cADE78xxReg32Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint32_t value)
{
    lu_uint8_t tmpBuffer[sizeof(value)];

    /* Convert value form LSB to MSB */
    tmpBuffer[0] = (value >> 24) & 0xff;
    tmpBuffer[1] = (value >> 16) & 0xff;
    tmpBuffer[2] = (value >>  8) & 0xff;
    tmpBuffer[3] = value & 0xff;

    /* Write the value */
    return writeI2cADE78xxRAW(regAddr, i2cBus, tmpBuffer, sizeof(value));
}

inline SB_ERROR writeI2cADE78xxReg16Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint16_t value)
{
    lu_uint8_t tmpBuffer[sizeof(value)];

    /* Convert value form LSB to MSB */
    tmpBuffer[0] = (value >>  8) & 0xff;
    tmpBuffer[1] = value & 0xff;

    /* Write the value */
    return writeI2cADE78xxRAW(regAddr, i2cBus, tmpBuffer, sizeof(value));
}

inline SB_ERROR writeI2cADE78xxReg8Bit(lu_uint16_t regAddr, lu_uint8_t i2cBus, lu_uint8_t value)
{
    /* Write the value */
    return writeI2cADE78xxRAW(regAddr, i2cBus, &value, sizeof(value));
}

/*
 *********************** End of file ******************************************
 */
