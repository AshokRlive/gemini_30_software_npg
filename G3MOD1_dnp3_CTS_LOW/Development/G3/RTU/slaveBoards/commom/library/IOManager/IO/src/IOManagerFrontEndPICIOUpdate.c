/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [IOManager FE PIC Update]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IOManager Front End PIC phase current SPI ADC update module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/07/14      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_ssp.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "IOManager.h"
#include "IOManagerIO.h"

#include "ProcessorSPI.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"

#include "IOManagerFrontEndPICIOUpdate.h"

#include "crc16.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR configureCheckSPIFrontEndPIC(lu_uint8_t sspBus);
static SB_ERROR configureFrontEndPIC(void);

void FEPICSPIDMAIntHandler(void);
void frontEndPicDecodePacket(IO_BUS_SPI sspBus, lu_uint8_t bufIdx);
lu_bool_t simplePeakDetect(lu_uint32_t sspbus, lu_uint32_t length, lu_int16_t value, lu_int16_t *retValuePtr);
void sumErrorCorrection(lu_int16_t *value);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

typedef enum
{
    DMA_BUF_IDX_0 = 0,
    DMA_BUF_IDX_1    ,

    DMA_BUF_IDX_LAST
}DMA_BUF_IDX;


lu_int8_t ThreePhaseDataBuffer1[DMA_BUF_IDX_LAST][sizeof(FEPic2NXPPacketStr)] __attribute__((section("ahbsram1")));
lu_int8_t ThreePhaseDataBuffer2[DMA_BUF_IDX_LAST][sizeof(FEPic2NXPPacketStr)] __attribute__((section("ahbsram0")));

GPDMA_LLI_Type GPDMAlli1[DMA_BUF_IDX_LAST] =
{
    //Source Addr  Destination address                          Next LLI address                         GPDMA Control
	{0x40088008  , (lu_uint32_t)(ThreePhaseDataBuffer1[DMA_BUF_IDX_0]), (lu_uint32_t)(&GPDMAlli1[DMA_BUF_IDX_1]), 0x88009008},
    {0x40088008  , (lu_uint32_t)(ThreePhaseDataBuffer1[DMA_BUF_IDX_1]), (lu_uint32_t)(&GPDMAlli1[DMA_BUF_IDX_0]), 0x88009008}
};

GPDMA_LLI_Type GPDMAlli2[DMA_BUF_IDX_LAST] =
{
    //Source Addr  Destination address                          Next LLI address                         GPDMA Control
	{0x40030008  , (lu_uint32_t)(ThreePhaseDataBuffer2[DMA_BUF_IDX_0]), (lu_uint32_t)(&GPDMAlli2[DMA_BUF_IDX_1]), 0x88009008},
    {0x40030008  , (lu_uint32_t)(ThreePhaseDataBuffer2[DMA_BUF_IDX_1]), (lu_uint32_t)(&GPDMAlli2[DMA_BUF_IDX_0]), 0x88009008}
};

/* Phase currents ioID Map */
static lu_uint32_t FEPICChMap[FEPIC_ADC_CH_LAST][2] =
{
	{IO_ID_NA, IO_ID_NA},
	{IO_ID_NA, IO_ID_NA},
	{IO_ID_NA, IO_ID_NA},
	{IO_ID_NA, IO_ID_NA}
};

static FEPICPacketDecode fePicPacketDecodeFunc = NULL;

static lu_uint32_t MaxValue[IO_BUS_SPI_SPI];
static lu_uint32_t SumLengthCounter[IO_BUS_SPI_SPI];
static lu_int16_t phaseValueA;
static lu_int16_t phaseValueB;
static lu_int16_t phaseValueC;
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR IOManagerFrontEndPICIOInit(void)
{
	SB_ERROR	retError;

	 /* Configure SSP SPI pins & peripheral */
	configureCheckSPIFrontEndPIC(IO_BUS_SPI_SSPI_0);
	configureCheckSPIFrontEndPIC(IO_BUS_SPI_SSPI_1);

	/* Now configure Front End PIC SSP DMA and initialise the state */
	retError = configureFrontEndPIC();

	return retError;
}

SB_ERROR IOManagerFEPICIORegisterCallBack(FEPICPacketDecode callBackFunc)
{
	SB_ERROR retVal = SB_ERROR_PARAM;

	if (callBackFunc != NULL)
	{
		retVal = SB_ERROR_NONE;
		fePicPacketDecodeFunc = callBackFunc;
	}

	return retVal;
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief SPI RX DMA ISR
 *
 *   \return None
 *
 ******************************************************************************
 */
void FEPICSPIDMAIntHandler(void)
{
	static lu_uint32_t ssp0IdxCounter = DMA_BUF_IDX_0;
	static lu_uint32_t ssp1IdxCounter = DMA_BUF_IDX_0;

	/* SSP0 */
	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
	{
		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);

		frontEndPicDecodePacket(IO_BUS_SPI_SSPI_0, ssp0IdxCounter);
		ssp0IdxCounter = ssp0IdxCounter ? 0 : 1;
	}
	else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
	{
		/* Clear error counter Interrupt pending */
		LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
	}

	/* SSP1 */
	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(1))
	{
		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(1);

		frontEndPicDecodePacket(IO_BUS_SPI_SSPI_1, ssp1IdxCounter);
		ssp1IdxCounter = ssp1IdxCounter ? 0 : 1;
	}
	else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(1))
	{
	   /* Clear error counter Interrupt pending */
	   LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(1);
	}
}

/*!
 ******************************************************************************
 *   \brief Decode PIC packet
 *
 *   Detailed description
 *
 *   \param sspBus
 *   \param bufIdx
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
void frontEndPicDecodePacket(IO_BUS_SPI sspBus, lu_uint8_t bufIdx)
{
	FEPic2NXPPacketStr *packetPtr;
	FEPic2NXPPacketStr  packet;
	ModuleTimeStr	    canTime;
	lu_int16_t 			peakValue;

	canTime     = CANCGetTime();

	switch(sspBus)
	{
	case IO_BUS_SPI_SSPI_0:
		packetPtr = (FEPic2NXPPacketStr *)&ThreePhaseDataBuffer1[bufIdx][0];
		break;

	case IO_BUS_SPI_SSPI_1:
		packetPtr = (FEPic2NXPPacketStr *)&ThreePhaseDataBuffer2[bufIdx][0];
		break;

	default:
		return;
		break;
	}

	/* Copy to working buffer to prevent next DMA corrupting, should ISR be delayed */
	memcpy((void *)&packet, packetPtr, sizeof(FEPic2NXPPacketStr));
	packetPtr = &packet;

	if (packetPtr->adcChannel < FEPIC_ADC_CH_LAST)
	{
#ifdef NO_SPI_PACKET_CRC
		if (1)
#else
		crc16_calc16((lu_uint8_t *)packetPtr,
							 (sizeof(FEPic2NXPPacketStr) - sizeof(packetPtr->crc16))
							);

		if (packetPtr->crc16 == crc16)
#endif
		{
			switch (packetPtr->adcChannel)
			{
			case FEPIC_ADC_CH_SUM_PK:

				if ((simplePeakDetect(sspBus, 3, packetPtr->adcValue, &peakValue)) == LU_TRUE)
				{
					//sumErrorCorrection( &peakValue);
					IOManagerSetOnline(FEPICChMap[packetPtr->adcChannel][sspBus], LU_TRUE);
					IOManagerSetValue(FEPICChMap[packetPtr->adcChannel][sspBus], peakValue);
				}
				break;

			case FEPIC_ADC_CH_L1:
			case FEPIC_ADC_CH_L2:
			case FEPIC_ADC_CH_L3:
				IOManagerSetOnline(FEPICChMap[packetPtr->adcChannel][sspBus], LU_TRUE);
				IOManagerSetValue(FEPICChMap[packetPtr->adcChannel][sspBus], packetPtr->adcValue);
				break;

			case FEPIC_ADC_CH_SUM:
				IOManagerSetOnline(FEPICChMap[packetPtr->adcChannel][sspBus], LU_TRUE);
				IOManagerSetValue(FEPICChMap[packetPtr->adcChannel][sspBus], packetPtr->adcValue);
				break;

			default :
				break;
			}

			/* Now call ISR routine callback to trigger on FPI threshold */
			if (fePicPacketDecodeFunc != NULL)
			{
				fePicPacketDecodeFunc(FEPICChMap[packetPtr->adcChannel][sspBus], canTime);
			}
		}
		else
		{
			IOManagerSetOnline(FEPICChMap[packetPtr->adcChannel][sspBus], LU_FALSE);
		}


	}
}

/*!
 ******************************************************************************
 *   \brief Check for SPI Front End PIC & configure SPI if found
 *
 *   Detailed description
 *
 *   \param sspBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckSPIFrontEndPIC(lu_uint8_t sspBus)
{
    SB_ERROR                retVal;
    lu_uint32_t             ioID;
    SSP_CFG_Type            sspConfig;
    GPDMA_Channel_CFG_Type  GPDMACfg;
    lu_bool_t               foundFePic = LU_FALSE;
    static lu_bool_t        initDMA = LU_FALSE;

    retVal = SB_ERROR_INITIALIZED;

    for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
    {
        if( (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_INPUT) &&
            (BoardIOMap[ioID].ioDev   == IO_DEV_SPI_FEPIC)   &&
            (BoardIOMap[ioID].ioAddr.spiFEPIC.sspChan == sspBus)
          )
        {
            /* Init SPI configuration */
            SSP_ConfigStructInit(&sspConfig);

            /* Override default values */
            sspConfig.CPHA        = SSP_CPHA_SECOND;
            sspConfig.CPOL        = SSP_CPOL_LO;
            sspConfig.Mode        = SSP_SLAVE_MODE;
            sspConfig.FrameFormat = SSP_FRAME_SPI;
            sspConfig.ClockRate   = 4000000;

            /* find & configure SPI peripheral pins */
            retVal = configureSPISspSlPeripheral(sspBus, &sspConfig);

            foundFePic = LU_TRUE;
            break;
        }
    }

    if(retVal == SB_ERROR_NONE && foundFePic == LU_TRUE)
    {
        /* Peripheral found and configured correctly.
         * Initialise DMA engine
         */

        if (initDMA == LU_FALSE)
        {
        	/* Initialise GPDMA controller */
        	GPDMA_Init();

        	initDMA = LU_TRUE;
        }

        /* DMA Channel (0 highest priority) */
        GPDMACfg.ChannelNum = (sspBus == IO_BUS_SPI_SSPI_0) ? 0 : 1;
        /* Source memory - not used */
        GPDMACfg.SrcMemAddr = 0;
        /* Destination memory */
        GPDMACfg.DstMemAddr = (uint32_t) ((sspBus == IO_BUS_SPI_SSPI_0) ?
        		              &ThreePhaseDataBuffer1[DMA_BUF_IDX_0] : &ThreePhaseDataBuffer2[DMA_BUF_IDX_0]);
        /* Transfer size */
        GPDMACfg.TransferSize = sizeof(ThreePhaseDataBuffer1[DMA_BUF_IDX_0]);
        /* Transfer width - not used */
        GPDMACfg.TransferWidth = 0;
        /* Transfer type */
        GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
        /* Source connection */
        GPDMACfg.SrcConn = ( (sspBus == IO_BUS_SPI_SSPI_0) ? GPDMA_CONN_SSP0_Rx : GPDMA_CONN_SSP1_Rx );
        /*  Destination connection - not used */
        GPDMACfg.DstConn = 0;
        /*  Linker List Item - unused */
        GPDMACfg.DMALLI = (uint32_t)((sspBus == IO_BUS_SPI_SSPI_0) ?
        				  &GPDMAlli1[DMA_BUF_IDX_1] : &GPDMAlli2[DMA_BUF_IDX_1]);

        /* Setup channel with given parameter */
        GPDMA_Setup(&GPDMACfg);

        /* Enable Rx DMA */
        SSP_DMACmd( (sspBus == IO_BUS_SPI_SSPI_0) ? LPC_SSP0 : LPC_SSP1,
                     SSP_DMA_RX,
                     ENABLE
                  );

        /* Enable GPDMA channel n */
        GPDMA_ChannelCmd((sspBus == IO_BUS_SPI_SSPI_0) ? 0 : 1, ENABLE);
    }

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief configure Front end PIC
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureFrontEndPIC(void)
{
	SB_ERROR	        	retError;
	lu_uint32_t				ioID;
	lu_uint32_t             pic1ResetIoId = IO_ID_NA;
	lu_uint32_t             pic2ResetIoId = IO_ID_NA;

	retError = SB_ERROR_NONE;

	/* Find ioID's for pins required to control Front End PIC */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		/* Is GPIO */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_OUTPUT:
			if (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_FEPIC_RST1)
			{
				pic1ResetIoId = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_FEPIC_RST2)
			{
				pic2ResetIoId = ioID;
			}
			break;

		case IO_CLASS_ANALOG_INPUT:
		    if(BoardIOMap[ioID].ioDev == IO_DEV_SPI_FEPIC)
		    {
		        FEPIC_ADC_CH ch;

		        /* Configure map of ADC channels via ioID */
		        ch = BoardIOMap[ioID].ioAddr.spiFEPIC.adcChan;
		        if(ch < FEPIC_ADC_CH_LAST)
		        {
		            FEPICChMap[ch][BoardIOMap[ioID].ioAddr.spiFEPIC.sspChan] = ioID;
		        }
		    }
        break;

		default:
			break;
		}
	}

	/* Release Front End PIC's from Reset */
	IOManagerSetValue(pic1ResetIoId, 1);
	IOManagerSetValue(pic2ResetIoId, 1);

	return retError;
}

lu_bool_t simplePeakDetect(lu_uint32_t sspbus, lu_uint32_t length, lu_int16_t value, lu_int16_t *retValuePtr)
{
	SumLengthCounter[sspbus]++;
	//Check all the sum values, apart from the last,
	//for a given length and locate the peak
	if (SumLengthCounter[sspbus] < length)
	{
	    //Compare value in the sum data array
	    //against a temporary value
	    if (value > MaxValue[sspbus])
		{
	    	MaxValue[sspbus] = value;
		}
	    return LU_FALSE;
	}

	else if (SumLengthCounter[sspbus] >= length)
	{
	    //Compare last value in the sum data array
	    //against a temporary value
	    if (value > MaxValue[sspbus])
		{
	    	MaxValue[sspbus] = value;
		}

	    SumLengthCounter[sspbus] = 0;
	    *retValuePtr = MaxValue[sspbus];
	    //reset temporary variable
	    MaxValue[sspbus] = 0;
	    return LU_TRUE;
	}
}

void sumErrorCorrection(lu_int16_t *value)
{
	if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_350_A + PHASE_VALUE_350_A + PHASE_VALUE_350_A))
					<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
		{
			//DO NOT apply correction
		}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_400_A + PHASE_VALUE_400_A + PHASE_VALUE_400_A))
				<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_5_A);
	}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_450_A + PHASE_VALUE_450_A + PHASE_VALUE_450_A))
				<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_10_A);
	}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_500_A + PHASE_VALUE_500_A + PHASE_VALUE_500_A))
			<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_15_A);
	}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_550_A + PHASE_VALUE_550_A + PHASE_VALUE_550_A))
			<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction  for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_20_A);
	}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_600_A + PHASE_VALUE_600_A + PHASE_VALUE_600_A))
			<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction  for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_25_A);
	}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_650_A + PHASE_VALUE_650_A + PHASE_VALUE_650_A))
			<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction  for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_30_A);
	}

	else if (abs(((phaseValueA + phaseValueB + phaseValueC) - (PHASE_VALUE_700_A + PHASE_VALUE_700_A + PHASE_VALUE_700_A))
			<= (PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A + PHASE_TOLERANCE_VALUE_25_A)))
	{
		//Apply correction  for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_35_A);
	}

	else
	{
		//Apply correction for sum error
		*value = ((*value) - SUM_ERROR_CORRECTION_FACTOR_40_A);
	}

}



/*
 *********************** End of file ******************************************
 */
