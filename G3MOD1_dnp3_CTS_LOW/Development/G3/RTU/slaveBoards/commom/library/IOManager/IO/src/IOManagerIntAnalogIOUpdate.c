/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager Internal Analogue IO Update module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   22/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "systemTime.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorADC.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define INT_ADC_AVERAGE_ENABLE  1  /* Enable ADC avaeraging */

#define INT_ADC_MAX_CHAN		16
#define INT_ADC_MAX_SAMP        5

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

lu_uint8_t convertIoIDtoAdcChan(lu_uint32_t ioID, lu_uint8_t *retAdcChanPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint32_t	intAdcIoID = 0;
static lu_uint8_t   intAdcChan = 0;

static lu_uint32_t	intAdcIoIDStart = 0;
static lu_uint32_t	intAdcIoIDEnd = 0;

static lu_int32_t   intAdcAverage[INT_ADC_MAX_CHAN][INT_ADC_MAX_SAMP];
static lu_uint32_t  sampIdx[INT_ADC_MAX_CHAN] = {0,0,0,0,0,0,0,0};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR IOManagerIntAnalogIOInit(void)
{
	SB_ERROR			retError;
	lu_uint8_t			foundAnalog;

	retError = SB_ERROR_NONE;

	retError = ProcADCInit();

	/* Find first analog channel and select that channel */
	foundAnalog = 0;
	do
	{
		/* Is an Analogue input */
		if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT ||
			BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT_MUX	)
		{
			switch(BoardIOMap[intAdcIoID].ioDev)
			{
			case IO_DEV_PERIPH:
				if (convertIoIDtoAdcChan(intAdcIoID, &intAdcChan) == LU_TRUE)
				{
					if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT_MUX)
					{
						/* Select Mux Pin */
						IOManagerSetValue(BoardIOMap[intAdcIoID].ioAddr.adc.muxIoID, 1);
					}

					/* Select ADC channel for next conversion */
					ProcADCSelectChannel(intAdcChan);

					foundAnalog = 1;
				}
				break;

			default:
				break;
			}
		}
		if (!foundAnalog)
		{
			intAdcIoID++;
		}
		else
		{
			/* Found first analogue channel */
			intAdcIoIDStart = intAdcIoID;
		}
	} while (intAdcIoID < BOARD_IO_MAX_IDX && !foundAnalog);

	if (!foundAnalog)
	{
		/* Cannot find any ADC channels */
		intAdcIoID = BOARD_IO_MAX_IDX;
	}
	else
	{
		/* JF yuk this is a mess! (Need to tidyup!!) */
		foundAnalog = 0;
		do
		{
			switch (BoardIOMap[intAdcIoID].ioClass)
			{
			case IO_CLASS_ANALOG_INPUT:
			case IO_CLASS_ANALOG_INPUT_MUX:
				if (BoardIOMap[intAdcIoID].ioDev != IO_DEV_PERIPH)
				{
					foundAnalog = 1;
				}
				break;

			default:
				foundAnalog = 1;
				break;
			}
			if (!foundAnalog)
			{
				intAdcIoID++;
			}
			else
			{
				/* Found just pas last Analogue */
				intAdcIoIDEnd = --intAdcIoID;
				intAdcIoID    = intAdcIoIDStart;
			}
		} while (intAdcIoID < BOARD_IO_MAX_IDX && !foundAnalog);
	}

	return retError;
}

SB_ERROR IOManagerIntAnalogIOUpdate(void)
{
	SB_ERROR	        retError;
	lu_uint16_t			value;
	lu_uint8_t			foundAnalog;
	lu_uint32_t			idx, chanIdx;


	retError = SB_ERROR_NONE;

	if (intAdcIoID != BOARD_IO_MAX_IDX)
	{
		foundAnalog = 0;

		/* Start conversion & wait until complete */
		ProcADCRead(intAdcChan, &value);

		if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT_MUX)
		{
			/* De-select MUX pin */
			IOManagerSetValue(BoardIOMap[intAdcIoID].ioAddr.adc.muxIoID, 0);
		}

		/* If simulated, then use original value */
		if (boardIOTable[intAdcIoID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO)
		{
			value = boardIOTable[intAdcIoID].value;
		}

		/* Perform a single IoID update on all IO in the ioTable[] per scan */
		/* Save the raw value */
		boardIOTable[intAdcIoID].rawValue = value;

		/* Apply filter average over n samples here... */
#ifdef INT_ADC_AVERAGE_ENABLE
		chanIdx = intAdcChan;
		if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT_MUX)
		{
			chanIdx  += 8;
		}
		if (boardIOTable[intAdcIoID].InitAvgSamples == LU_TRUE)
		{
			boardIOTable[intAdcIoID].InitAvgSamples = LU_FALSE;

			for (idx = 0; idx < INT_ADC_MAX_SAMP; idx++)
			{
				intAdcAverage[chanIdx][idx] = value;
			}
		}

		/* Add sample to buffer */
		intAdcAverage[chanIdx][sampIdx[chanIdx]++] = value;
		if (sampIdx[chanIdx] >= INT_ADC_MAX_SAMP)
		{
			sampIdx[chanIdx] = 0;
		}

		/* Sum buffer values */
		boardIOTable[intAdcIoID].averageSumValue = 0;
		for (idx = 0; idx < INT_ADC_MAX_SAMP; idx++)
		{
			boardIOTable[intAdcIoID].averageSumValue += intAdcAverage[chanIdx][idx];
		}

		/* Calculate the average */
		boardIOTable[intAdcIoID].value            = (boardIOTable[intAdcIoID].averageSumValue / INT_ADC_MAX_SAMP);
#else
		boardIOTable[intAdcIoID].value  = boardIOTable[intAdcIoID].rawValue;
#endif
		/* Find the next channel & select that channel */
		do
		{
			/* Next ioID round-robin */
			intAdcIoID++;
			if (intAdcIoID > intAdcIoIDEnd)
			{
				intAdcIoID = intAdcIoIDStart;
			}

			/* Is an Analogue input */
			if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT ||
				BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT_MUX	)
			{
				switch(BoardIOMap[intAdcIoID].ioDev)
				{
				case IO_DEV_PERIPH:
					if (convertIoIDtoAdcChan(intAdcIoID, &intAdcChan) == LU_TRUE)
					{
						if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT_MUX)
						{
							/* Select Mux Pin */
							IOManagerSetValue(BoardIOMap[intAdcIoID].ioAddr.adc.muxIoID, 1);
						}

						/* Select ADC channel for next conversion */
						ProcADCSelectChannel(intAdcChan);

						foundAnalog = 1;
					}
					break;

				default:
					break;
				}
			}
		} while (!foundAnalog);
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_uint8_t convertIoIDtoAdcChan(lu_uint32_t ioID, lu_uint8_t *retAdcChanPtr)
{
	lu_uint8_t retVal;

	retVal = LU_TRUE;

	switch (BoardIOMap[ioID].ioAddr.gpio.periphFunc)
	{
	case FUNC_AD0_CH_0:
		*retAdcChanPtr = 0;
		break;

	case FUNC_AD0_CH_1:
		*retAdcChanPtr = 1;
		break;

	case FUNC_AD0_CH_2:
		*retAdcChanPtr = 2;
		break;

	case FUNC_AD0_CH_3:
		*retAdcChanPtr = 3;
		break;

	case FUNC_AD0_CH_4:
		*retAdcChanPtr = 4;
		break;

	case FUNC_AD0_CH_5:
		*retAdcChanPtr = 5;
		break;

	case FUNC_AD0_CH_6:
		*retAdcChanPtr = 6;
		break;

	case FUNC_AD0_CH_7:
		*retAdcChanPtr = 7;
		break;

	default:
		retVal = LU_FALSE;
		break;
	}

	return retVal;
}

/*
 *********************** End of file ******************************************
 */

