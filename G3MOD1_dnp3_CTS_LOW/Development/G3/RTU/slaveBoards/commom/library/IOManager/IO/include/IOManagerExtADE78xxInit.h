/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager ADE78xx - advanced ADC Update header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/04/15     venkat_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _IOMANAGEREXTADE78XXINIT_INCLUDED
#define _IOMANAGEREXTADE78XXINIT_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * \brief ADE78xx custom decode function.
 *
 * This function is called by the IOManagerExtADE78xxDMAIntHandler function.
 *
 * \param
 */
typedef void (*ADE78xxHSDCDecodeFunc)(ADE78XX_CH chip, PHASE_PEAK phase, lu_int32_t value, lu_uint32_t sampleTimeMs);


/*! IO SPI/I2C ADC (ADE78XX) Address structure */
typedef struct ADE78xxIoIDDef
{
	lu_int32_t        adcCfg1;
	lu_int32_t        adcCfg2;
	lu_int32_t        adcPm0;
	lu_int32_t        adcPm1;
	lu_int32_t        adcManualReset;
	lu_int32_t        adcIrq0;
	lu_int32_t        adcIrq1;
	lu_int32_t		  adcCs;
	IO_BUS_I2C        i2cBus;
	IO_BUS_SPI        spiBus;
}ADE78xxIoIDStr;


typedef struct CircularBufferDef
{
	lu_int32_t			buffer[2048];
	lu_uint16_t			write;
	lu_uint16_t 		read;
    lu_uint16_t 		size;
 }CircularBufferStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern ADE78xxIoIDStr ade78xxIOIDMap[];
//extern CircularBufferStr circularBfr;
/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
extern void CircularBufferInit(CircularBufferStr *cb, lu_uint32_t size);
extern lu_uint32_t CircularBufferDataLength(CircularBufferStr *cb);
extern SB_ERROR CircularBufferWrite(CircularBufferStr *cb, lu_uint32_t data);
extern SB_ERROR CircularBufferRead(CircularBufferStr *cb, lu_uint32_t *data);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxIOInit(void);

/*!
  ******************************************************************************
  *   \brief Register ADE78xx HSDC packet decoder
  *
  *   Detailed description
  *
  *   \param callBackFunc
  *
  *
  *   \return SB_ERROR
  *
  ******************************************************************************
  */
extern SB_ERROR IOManagerADE78xxHSDCDecodeRegisterCallBack(ADE78xxHSDCDecodeFunc callBackFunc);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxI2CReadReg(lu_uint8_t i2cBus, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t *valuePtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxI2CWriteReg(lu_uint8_t i2cBus, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t  value);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxSPIReadReg (lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t *valuePtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxSPIWriteReg(lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t  value);

#endif /* _IOMANAGEREXTAD78XXINIT_INCLUDED */

/*
 *********************** End of file ******************************************
 */
