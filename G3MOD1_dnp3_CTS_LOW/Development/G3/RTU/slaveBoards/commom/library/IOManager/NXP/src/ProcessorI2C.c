/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/10/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "errorCodes.h"
#include "IOManager.h"

#include "BoardIOMap.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR configureI2CPeripheral(lu_uint8_t i2CBus)
{
	lu_uint32_t				ioID;
	lu_uint8_t              pinFunc;
	lu_uint8_t              pinsConfigured;
	PINSEL_CFG_Type 	    pinCfg;
	LPC_I2C_TypeDef			*i2cChanType;
	lu_uint32_t				clockRate;

	pinsConfigured = 0x0;
	clockRate      = 100000;

	switch (i2CBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;

		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_I2C_0_SCL, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					/* High speed I2C */
					if (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_BUS_400KHZ)
					{
						clockRate      = 400000;
					}

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_I2C_0_SDA, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				if (pinsConfigured == 0x03)
				{
					/* Init I2C */
					I2C_Init((LPC_I2C_TypeDef *)i2cChanType, clockRate);

					/* Enable I2C operation */
					I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
				}
			}
		}
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;

		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_I2C_1_SCL, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					/* High speed I2C */
					if (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_BUS_400KHZ)
					{
						clockRate      = 400000;
					}

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_I2C_1_SDA, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				if (pinsConfigured == 0x03)
				{
					/* Init I2C */
					I2C_Init((LPC_I2C_TypeDef *)i2cChanType, clockRate);

					/* Enable I2C operation */
					I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
				}
			}
		}
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;

		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_I2C_2_SCL, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					/* High speed I2C */
					if (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_BUS_400KHZ)
					{
						clockRate      = 400000;
					}

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_I2C_2_SDA, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				if (pinsConfigured == 0x03)
				{
					/* Init I2C */
					I2C_Init((LPC_I2C_TypeDef *)i2cChanType, clockRate);

					/* Enable I2C operation */
					I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
				}
			}
		}
		break;

	default:
		return SB_ERROR_PARAM;
	}

	return SB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
