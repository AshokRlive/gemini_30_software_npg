/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager Front End PIC - SPI attached phase current ADC header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/10/14      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _IOMANAGERFRONTENDPICIOUPDATE_INCLUDED
#define _IOMANAGERFRONTENDPICIOUPDATE_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"

#include "FrontEndPIC.h"

#include"ModuleProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

//Sum sample rate per half cycle
#define SUM_SMPL_RATE_PER_HL_CL 32

//Reference phase threshold for error correction function
#define PHASE_VALUE_350_A 1300
#define PHASE_VALUE_400_A 1480
#define PHASE_VALUE_450_A 1680
#define PHASE_VALUE_500_A 1840
#define PHASE_VALUE_550_A 2040
#define PHASE_VALUE_600_A 2225
#define PHASE_VALUE_650_A 2410
#define PHASE_VALUE_700_A 2600
#define PHASE_VALUE_750_A 2780
// Sum error correction factor
#define SUM_ERROR_CORRECTION_FACTOR_5_A 0
#define SUM_ERROR_CORRECTION_FACTOR_10_A 0
#define SUM_ERROR_CORRECTION_FACTOR_15_A 8
#define SUM_ERROR_CORRECTION_FACTOR_20_A 10
#define SUM_ERROR_CORRECTION_FACTOR_25_A 13
#define SUM_ERROR_CORRECTION_FACTOR_30_A 27
#define SUM_ERROR_CORRECTION_FACTOR_35_A 60
#define SUM_ERROR_CORRECTION_FACTOR_40_A 80

//Tolerance value for sum error correction function
#define PHASE_TOLERANCE_VALUE_25_A 50

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * \brief Front End PIC custom decode function.
 *
 * This function is called by the FEPICSPIDMAIntHandler function.
 *
 * \param pktPtr Front End PIC data packet
 */
typedef void (*FEPICPacketDecode)(lu_uint32_t ioID, ModuleTimeStr canTime);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise Front End PIC SPI controller
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerFrontEndPICIOInit(void);

/*!
 ******************************************************************************
 *   \brief Register Front End PIC packet decoder
 *
 *   Detailed description
 *
 *   \param callBackFunc
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerFEPICIORegisterCallBack(FEPICPacketDecode callBackFunc);


#endif /* _IOMANAGERFRONTENDPICIOUPDATE_INCLUDED */

/*
 *********************** End of file ******************************************
 */
