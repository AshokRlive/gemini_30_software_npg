/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IOManager IO module header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _IOMANAGERIO_INCLUDED
#define _IOMANAGERIO_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define IOM_UPDATEFLAG_PULSE_REPEATED				(0x01)
#define IOM_UPDATEFLAG_PULSE_SINGLE					(0x02)
#define IOM_UPDATEFLAG_UPDATE_OUTPUT				(0x04)
#define IOM_UPDATEFLAG_INPUT_EVENT					(0x08)
#define IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO		(0x08)

#define IOM_UPDATEFLAG_PRE_OFFLINE          		(0x10)

#define IOM_UPDATEFLAG_SIMULATE_IO					(0x20) // Detach from physical IO

#define IOM_UPDATEFLAG_OFFLINE          			(0x40) // ioID is offline (Not available)
#define IOM_UPDATEFLAG_ALT_CFG          			(0x80) // Alternate device detected

#define IO_AI_EVENTRATE_MS		(1000)

#define IO_DI_DEBOUNCE_MS		(0)

#define IOM_SET_VALUE(ioID, value) \
	if (ioID < x) boardIOTable[ioID].value = boardIOTable[ioID].rawValue = value

/*! Utility macros to fill in ioChanDIMap */
#define IOM_IOCHAN_DI(ioChan, ioID) \
	{ioChan, ioID, {IO_DI_DEBOUNCE_MS, IO_DI_DEBOUNCE_MS}, LU_FALSE, 0, LU_FALSE }

/*! Utility macros to fill in ioChanDOMap */
#define IOM_IOCHAN_DO(ioChan, ioID) \
	{ioChan, ioID, LU_FALSE }

/*! Utility macros to fill in ioChanAOMap */
#define IOM_IOCHAN_AO(ioChan, ioID, calID) \
	{ioChan, ioID, calID, LU_FALSE}

/*! Utility macros to fill in ioChanAIMap */
#define IOM_IOCHAN_AI(ioChan, ioID, calID, evRate) \
	{ioChan, ioID, calID, evRate, 0, LU_FALSE, LU_FALSE }

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


LU_DISABLE_WARNING(-pedantic)
/* IO Channel Digital Input */
typedef struct IOMIOChanDIDef
{
	lu_uint32_t			ioChan;
	lu_uint32_t			ioID;
	IODebounceStr		db;
	lu_uint8_t			enable : 1;
	lu_uint8_t			extEquipInvert : 1;
	lu_uint8_t			eventEnable : 1;
}IOMIOChanDIStr;

/* IO Channel Digital Output */
typedef struct IOMIOChanDODef
{
	lu_uint32_t			ioChan;
	lu_uint32_t			ioID;
	lu_uint8_t			enable : 1;
}IOMIOChanDOStr;

/* IO Channel Analogue Output */
typedef struct IOMIOChanAODef
{
	lu_uint32_t			ioChan;
	lu_uint32_t			ioID;
	lu_uint8_t			calID;
	lu_uint8_t			enable : 1;
}IOMIOChanAOStr;

/* IO Channel Analogue Input */
typedef struct IOMIOChanAIDef
{
	lu_uint32_t			ioChan;
	lu_uint32_t			ioID;
	lu_uint8_t			calID;
	lu_uint16_t			eventRateMs;
	lu_uint16_t			eventRateCountMs;
	lu_uint8_t			enable : 1;
	lu_uint8_t			eventEnable:1;
}IOMIOChanAIStr;
LU_ENABLE_WARNING(-pedantic)


/* IO Table (Shadow copy of all IO) */
typedef struct IOTableDef
{
	lu_int32_t			value;
	lu_int32_t			rawValue;
	lu_int8_t			InitAvgSamples;
	lu_uint32_t         averageSumValue;
	lu_uint16_t			countHigh2Low;
	lu_uint16_t			countLow2High;
	lu_uint16_t			pulseMs;
	lu_uint32_t         pulseTimerCountMs;
	lu_uint8_t			updateFlags;
}IOTableStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


extern IOMIOChanDIStr ioChanDIMap[];
extern IOMIOChanDOStr ioChanDOMap[];
extern IOMIOChanAIStr ioChanAIMap[];
extern IOMIOChanAOStr ioChanAOMap[];

extern lu_bool_t ioManagerIOEventingEnable;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR  IOManagerIODisableSimulation(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOManagerIOInit(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerIOChanMapInit(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerIOGetModuleID(lu_uint8_t *deviceIDPtr, lu_uint32_t ioIDSel1, lu_uint32_t ioIDSel2, lu_uint32_t ioIDSel3, lu_uint32_t ioIDSel4);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerIOGetLocalRemoteStatus(IO_STAT_LOR *ioStatLORPtr, lu_uint32_t ioIDLocal, lu_uint32_t ioIDRemote);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOGetValue(lu_uint32_t ioID, lu_int32_t *retValuePtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOGetRawValue(lu_uint32_t ioID, lu_int32_t *retValuePtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOMSetValue(lu_uint32_t ioID, lu_int32_t value);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOMSetPulseGen(lu_uint32_t ioID, lu_uint16_t timeMs, lu_bool_t repeated);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOMChanAIGetValue(lu_uint32_t ioChan, lu_int32_t *retValue);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOMChanDIGetValue(lu_uint32_t ioChan, lu_int32_t *retValue, lu_int32_t *retRawValue);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOManagerIOSetSimIoID(lu_uint32_t ioID, lu_bool_t enableSimulation);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR  IOManagerIOSetSimAIChan(lu_uint32_t ioChan, lu_bool_t enableSimulation);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerIOSetSimDIChan(lu_uint32_t ioChan, lu_bool_t enableSimulation);

#endif /* _IOMANAGERIO_INCLUDED */

/*
 *********************** End of file ******************************************
 */
