/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/10/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_ssp.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_clkpwr.h"

#include "lpc17xx_spi.h"

#include "errorCodes.h"
#include "IOManager.h"

#include "BoardIOMap.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorSPI.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Configure SPI & SSP peripheral
 *
 *   Detailed description
 *
 *   \param sspBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureSPIPeripheral(lu_uint8_t spiBus, lu_uint32_t numDataBits)
{
	lu_uint32_t				ioID;
	lu_uint8_t              pinFunc;
	lu_uint8_t              pinsConfigured;
	PINSEL_CFG_Type 	    pinCfg;
	SSP_CFG_Type			sspConfig;
	SPI_CFG_Type            spiConfig;

	pinsConfigured = 0x0;

	switch (spiBus)
	{
	case IO_BUS_SPI_SSPI_0:
		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP0_SCK, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP0_MISO, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP0_MOSI, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x04;

				}

				if (pinsConfigured == 0x7)
				{
					/* Init SPI */
					SSP_ConfigStructInit(&sspConfig);

					switch (numDataBits)
					{
					case 4:
						sspConfig.Databit = SSP_DATABIT_4;
						break;

					case 5:
						sspConfig.Databit = SSP_DATABIT_5;
						break;

					case 6:
						sspConfig.Databit = SSP_DATABIT_6;
						break;

					case 7:
						sspConfig.Databit = SSP_DATABIT_7;
						break;

					case 8:
						sspConfig.Databit = SSP_DATABIT_8;
						break;

					case 9:
						sspConfig.Databit = SSP_DATABIT_9;
						break;

					case 10:
						sspConfig.Databit = SSP_DATABIT_10;
						break;

					case 11:
						sspConfig.Databit = SSP_DATABIT_11;
						break;

					case 12:
						sspConfig.Databit = SSP_DATABIT_12;
						break;

					case 13:
						sspConfig.Databit = SSP_DATABIT_13;
						break;

					case 14:
						sspConfig.Databit = SSP_DATABIT_14;
						break;

					default:
						sspConfig.Databit = SSP_DATABIT_8;
						break;
					}

					SSP_Init(LPC_SSP0, &sspConfig);

					SSP_Cmd(LPC_SSP0, ENABLE);

					break;
				}
			}
		}
		break;

	case IO_BUS_SPI_SSPI_1:
		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP1_SCK, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP1_MISO, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP1_MOSI, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x04;
				}

				if (pinsConfigured == 0x7)
				{
					/* Init SPI */
					SSP_ConfigStructInit(&sspConfig);

					switch (numDataBits)
					{
					case 4:
						sspConfig.Databit = SSP_DATABIT_4;
						break;

					case 5:
						sspConfig.Databit = SSP_DATABIT_5;
						break;

					case 6:
						sspConfig.Databit = SSP_DATABIT_6;
						break;

					case 7:
						sspConfig.Databit = SSP_DATABIT_7;
						break;

					case 8:
						sspConfig.Databit = SSP_DATABIT_8;
						break;

					case 9:
						sspConfig.Databit = SSP_DATABIT_9;
						break;

					case 10:
						sspConfig.Databit = SSP_DATABIT_10;
						break;

					case 11:
						sspConfig.Databit = SSP_DATABIT_11;
						break;

					case 12:
						sspConfig.Databit = SSP_DATABIT_12;
						break;

					case 13:
						sspConfig.Databit = SSP_DATABIT_13;
						break;

					case 14:
						sspConfig.Databit = SSP_DATABIT_14;
						break;

					default:
						sspConfig.Databit = SSP_DATABIT_8;
						break;
					}

					SSP_Init(LPC_SSP1, &sspConfig);

					SSP_Cmd(LPC_SSP1, ENABLE);

					break;
				}
			}
		}
		break;

	case IO_BUS_SPI_SPI:
		/* Legacy SPI Peripheral */

		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SPI_SCK, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SPI_MISO, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SPI_MOSI, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x04;

				}

				if (pinsConfigured == 0x7)
				{
					/* Init SPI */
					SPI_ConfigStructInit(&spiConfig);

					switch (numDataBits)
					{
					case 8:
						spiConfig.Databit = SPI_DATABIT_8;
						break;

					case 9:
						spiConfig.Databit = SPI_DATABIT_9;
						break;

					case 10:
						spiConfig.Databit = SPI_DATABIT_10;
						break;

					case 11:
						spiConfig.Databit = SPI_DATABIT_11;
						break;

					case 12:
						spiConfig.Databit = SPI_DATABIT_12;
						break;

					case 13:
						spiConfig.Databit = SPI_DATABIT_13;
						break;

					case 14:
						spiConfig.Databit = SPI_DATABIT_14;
						break;

					case 15:
						spiConfig.Databit = SPI_DATABIT_15;
						break;

					case 16:
						spiConfig.Databit = SPI_DATABIT_16;
						break;

					default:
						spiConfig.Databit = SPI_DATABIT_8;
						break;
					}

					SPI_Init(LPC_SPI, &spiConfig);
				}
			}
		}
		break;

	default:
		return SB_ERROR_PARAM;
	}

	return SB_ERROR_NONE;
}

SB_ERROR configureSPIParamPeripheral(lu_uint8_t spiBus, SSP_CFG_Type *sspConfig)
{
	lu_uint32_t				ioID;
	lu_uint8_t              pinFunc;
	lu_uint8_t              pinsConfigured;
	PINSEL_CFG_Type 	    pinCfg;

	pinsConfigured = 0x0;

	switch (spiBus)
	{
	case IO_BUS_SPI_SSPI_0:
		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP0_SCK, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP0_MISO, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP0_MOSI, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x04;

				}

				if (pinsConfigured == 0x7)
				{
					/* Init SPI */
					SSP_Init(LPC_SSP0, sspConfig);

					SSP_Cmd(LPC_SSP0, ENABLE);

					break;
				}
			}
		}
		break;

	case IO_BUS_SPI_SSPI_1:
		for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
		{
			if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
			{
				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP1_SCK, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x01;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP1_MISO, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x02;
				}

				pinFunc = ProcGPIOCheckPeripheralPinPort(FUNC_SSP1_MOSI, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				if (pinFunc)
				{
					/* Set pin configuration */
					pinCfg.Funcnum   = pinFunc;
					pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
					pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
					pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
					pinCfg.Pinmode   = 0;

					/* Configure pin type */
					PINSEL_ConfigPin(&pinCfg);

					pinsConfigured |= 0x04;
				}

				if (pinsConfigured == 0x7)
				{
					/* Init SPI */
					SSP_Init(LPC_SSP1, sspConfig);

					SSP_Cmd(LPC_SSP1, ENABLE);

					break;
				}
			}
		}
		break;

	default:
		return SB_ERROR_PARAM;
	}

	return SB_ERROR_NONE;
}

SB_ERROR configureSPISspSlPeripheral(lu_uint8_t sspBus, SSP_CFG_Type *sspConfig)
{
    lu_uint32_t             ioID;
    lu_uint8_t              pinFunc;
    PINSEL_CFG_Type         pinCfg;
    GPIO_PIN_FUNC           sck;
    GPIO_PIN_FUNC           mosi;
    GPIO_PIN_FUNC           miso;
    GPIO_PIN_FUNC           ss;
    lu_uint8_t              pinsConfigured = 0x00;

    /* Select pins */
    switch(sspBus)
    {
        case IO_BUS_SPI_SSPI_0:
            sck  = FUNC_SSP0_SCK;
            mosi = FUNC_SSP0_MOSI;
            miso = FUNC_SSP0_MISO;
            ss   = FUNC_SSP0_SEL;
        break;

        case IO_BUS_SPI_SSPI_1:
            sck  = FUNC_SSP1_SCK;
            mosi = FUNC_SSP1_MOSI;
            miso = FUNC_SSP1_MISO;
            ss   = FUNC_SSP1_SEL;
        break;

        case IO_BUS_SPI_SPI:
        	/* Legacy SPI peripheral */
        	return SB_ERROR_PARAM;
        	break;

        default: return SB_ERROR_PARAM;
    }

    for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
    {
        if (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH && BoardIOMap[ioID].ioDev == IO_DEV_PERIPH)
        {
            pinFunc = ProcGPIOCheckPeripheralPinPort(sck, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
            if (pinFunc)
            {
                /* Set pin configuration */
                pinCfg.Funcnum   = pinFunc;
                pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
                pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
                pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
                pinCfg.Pinmode   = 0;

                /* Configure pin type */
                PINSEL_ConfigPin(&pinCfg);

                pinsConfigured |= 0x01;
            }

            pinFunc = ProcGPIOCheckPeripheralPinPort(mosi, BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
            if (pinFunc)
            {
                /* Set pin configuration */
                pinCfg.Funcnum   = pinFunc;
                pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
                pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
                pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
                pinCfg.Pinmode   = 0;

                /* Configure pin type */
                PINSEL_ConfigPin(&pinCfg);

                pinsConfigured |= 0x02;
            }

            pinFunc = ProcGPIOCheckPeripheralPinPort(ss, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
            if (pinFunc)
            {
                /* Set pin configuration */
                pinCfg.Funcnum   = pinFunc;
                pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
                pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
                pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
                pinCfg.Pinmode   = 0;

                /* Configure pin type */
                PINSEL_ConfigPin(&pinCfg);

                pinsConfigured |= 0x04;
            }

            pinFunc = ProcGPIOCheckPeripheralPinPort(miso, BoardIOMap[ioID].ioAddr.gpio.port,  BoardIOMap[ioID].ioAddr.gpio.pin);
			if (pinFunc)
			{
				/* Set pin configuration */
				pinCfg.Funcnum   = pinFunc;
				pinCfg.Portnum   = BoardIOMap[ioID].ioAddr.gpio.port;
				pinCfg.Pinnum    = BoardIOMap[ioID].ioAddr.gpio.pin;
				pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
				pinCfg.Pinmode   = 0;

				/* Configure pin type */
				PINSEL_ConfigPin(&pinCfg);

				//pinsConfigured |= 0x01;
			}

            if (pinsConfigured == 0x7)
            {
                CLKPWR_SetPCLKDiv((sspBus == IO_BUS_SPI_SSPI_0) ? CLKPWR_PCLKSEL_SSP0 : CLKPWR_PCLKSEL_SSP1, CLKPWR_PCLKSEL_CCLK_DIV_1);
                /* All pins configured. Initialise peripheral */
                SSP_Init((sspBus == IO_BUS_SPI_SSPI_0) ? LPC_SSP0 : LPC_SSP1, sspConfig);

                SSP_Cmd((sspBus == IO_BUS_SPI_SSPI_0) ? LPC_SSP0 : LPC_SSP1, ENABLE);

                return SB_ERROR_NONE;
            }
        }
    }

    return SB_ERROR_SPI_FAIL;
}

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 lu_uint8_t convertIoIDtoSpiBus(lu_uint32_t ioID, lu_uint8_t *retSpiBusPtr)
 {
 	lu_uint8_t retVal;

 	retVal = LU_TRUE;


 	switch (BoardIOMap[ioID].ioAddr.spiDigiPot.busSspi)
 	{
 	case IO_BUS_SPI_SSPI_0:
 		*retSpiBusPtr = 0;
 		break;

 	case IO_BUS_SPI_SSPI_1:
 		*retSpiBusPtr = 1;
 		break;

 	case IO_BUS_SPI_SPI:
 		*retSpiBusPtr = 2; // Legacy SPI peripheral
 		break;

 	default:
 		retVal = LU_FALSE;
 		break;
 	}

 	return retVal;
 }

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
