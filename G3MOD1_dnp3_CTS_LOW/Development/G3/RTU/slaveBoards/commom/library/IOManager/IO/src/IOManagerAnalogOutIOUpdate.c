/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SPI Analogue Output DIGI Pot SPI IO Manager update module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/08/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_ssp.h"

#include "errorCodes.h"

#include "systemTime.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerAnalogOutIOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorSPI.h"
#include "ProcessorDAC.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"
#include "Calibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* Microchip digipot defines */
#define MCP4132_CMD_READ_DATA			(0x0c)
#define MCP4132_CMD_WRITE_DATA			(0x00)
#define MCP4132_CMD_INCREMENT			(0x04)
#define MCP4132_CMD_DECREMENT			(0x08)

#define MCP4132_ADDR_WIPER_0			(0x00)
#define MCP4132_ADDR_WIPER_1			(0x01)

/* Analog Devices AD84xx defines */
#define AD84xx_ADDR_RDAC_1				(0x00)
#define AD84xx_ADDR_RDAC_2				(0x01)
#define AD84xx_ADDR_RDAC_3				(0x02)
#define AD84xx_ADDR_RDAC_4				(0x03)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


SB_ERROR writeMicrochipDigiPot(lu_uint32_t ioID, lu_uint8_t sspBus, lu_uint32_t value);
SB_ERROR writeAD84xxDigiPot(lu_uint32_t ioID, lu_uint8_t sspBus, lu_uint32_t value);

SB_ERROR configureCheckDigiPotSPI(lu_uint8_t sspBus);
SB_ERROR configureCheckDAC(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR IOManagerAnalogOutIOInit(void)
{
	 SB_ERROR	retError;

	 retError = SB_ERROR_NONE;

	 /* Configure SPI pins & SSP peripheral */
	 configureCheckDigiPotSPI(IO_BUS_SPI_SSPI_0);
	 configureCheckDigiPotSPI(IO_BUS_SPI_SSPI_1);

	 /* Configure DAC */
	 configureCheckDAC();

	 return retError;
}

SB_ERROR IOManagerAnalogOutIOUpdate(void)
{
	SB_ERROR	        retError;
	lu_uint8_t          sspBus;
	lu_int32_t          value;
	lu_uint8_t          chanIdx;
	static lu_uint32_t	ioID = 0;

	retError = SB_ERROR_NONE;

	/* Perform a single IO update on all IO in the ioTable[] per scan */
	if (ioID < BOARD_IO_MAX_IDX)
	{
		/* Is an Analogue output */
		if (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_OUTPUT)
		{
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_PERIPH:
				/* Onchip DAC */
				if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_UPDATE_OUTPUT)
				{
					value = boardIOTable[ioID].value;

					/* Apply calibration factor if required */
					chanIdx = BoardIOMap[ioID].ioChan;
					if (chanIdx < IO_CHAN_AO_MAP_SIZE)
					{
						CalibrationConvertValue(ioChanAOMap[chanIdx].calID, value, &value);
					}

					if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					{
						/* Write to SPI Digi Pot */
						ProcDACSetValue(value);
					}

					/* Clear pending update */
					boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_UPDATE_OUTPUT;
				}
				break;

			case IO_DEV_SPI_DIGIPOT:
				/* Microchip digital pot */
				if (convertIoIDtoSpiBus(ioID, &sspBus) == LU_TRUE)
				{
					if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_UPDATE_OUTPUT)
					{
						value = boardIOTable[ioID].value;

						/* Apply calibration factor if required */
						chanIdx = BoardIOMap[ioID].ioChan;
						if (chanIdx < IO_CHAN_AO_MAP_SIZE)
						{
							CalibrationConvertValue(ioChanAOMap[chanIdx].calID, value, &value);
						}

						if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
						{
							/* Write to SPI Digi Pot */
							writeMicrochipDigiPot(ioID, sspBus, value);
						}

						/* Clear pending update */
						boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_UPDATE_OUTPUT;
					}
				}
				break;

			case IO_DEV_SPI_AD_DIGITAL_POT:
				/* Analog Device AD84xx Digital Pot */
				if (convertIoIDtoSpiBus(ioID, &sspBus) == LU_TRUE)
				{
					if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_UPDATE_OUTPUT)
					{
						value = boardIOTable[ioID].value;

						/* Apply calibration factor if required */
						chanIdx = BoardIOMap[ioID].ioChan;
						if (chanIdx < IO_CHAN_AO_MAP_SIZE)
						{
							CalibrationConvertValue(ioChanAOMap[chanIdx].calID, value, &value);
						}

						if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
						{
							/* Write to SPI Digi Pot */
							writeAD84xxDigiPot(ioID, sspBus, value);
						}

						/* Clear pending update */
						boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_UPDATE_OUTPUT;
					}
				}
				break;

			default:
				break;
			}
		}
		ioID++;
	}
	else
	{
		/* Reset to start for next scan */
		ioID = 0;
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*!
 ******************************************************************************
 *   \brief Do an SPI write to a Microchip digipot channel addressed
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeMicrochipDigiPot(lu_uint32_t ioID, lu_uint8_t sspBus, lu_uint32_t value)
{
	lu_uint8_t				csIoID;
	lu_uint8_t				txBuf[2];
	lu_uint8_t				rxBuf[3];
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint16_t             potValue;
	lu_uint32_t             pinValue;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	potValue = (value & 0x1ff);

	csIoID = BoardIOMap[ioID].ioAddr.spiDigiPot.csIoID;

	switch(BoardIOMap[ioID].ioAddr.spiDigiPot.chan)
	{
	case 0:
		txBuf[0] = MCP4132_ADDR_WIPER_0;
		break;

	case 1:
		txBuf[0] = MCP4132_ADDR_WIPER_1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	txBuf[0] |= MCP4132_CMD_WRITE_DATA;
	txBuf[0] |= (potValue >> 8);
	txBuf[1]  = (potValue & 0xff);

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 2;

	switch (sspBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SPI write to a AD84xx digipot channel addressed
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeAD84xxDigiPot(lu_uint32_t ioID, lu_uint8_t sspBus, lu_uint32_t value)
{
	lu_uint8_t				csIoID;
	lu_uint16_t				txBuf[2];
	lu_uint16_t				rxBuf[2];
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint16_t             potValue;
	lu_uint32_t             pinValue;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	potValue = (value & 0x1ff);

	csIoID = BoardIOMap[ioID].ioAddr.spiDigiPot.csIoID;

	switch(BoardIOMap[ioID].ioAddr.spiDigiPot.chan)
	{
	case 0:
		txBuf[0] = (AD84xx_ADDR_RDAC_1 << 8);
		break;

	case 1:
		txBuf[0] = (AD84xx_ADDR_RDAC_2 << 8);
		break;

	case 2:
		txBuf[0] = (AD84xx_ADDR_RDAC_3 << 8);
		break;

	case 3:
		txBuf[0] = (AD84xx_ADDR_RDAC_4 << 8);
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	txBuf[0] |= (potValue & 0xff);

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 2;

	switch (sspBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	sspTyp->CR0 &= 0xFFF0;
	sspTyp->CR0 |= SSP_DATABIT_10;

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SPI write to a digipot channel addressed
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckDigiPotSPI(lu_uint8_t sspBus)
{
	SB_ERROR				retVal;
	lu_uint32_t				ioID;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		 /* Is an Analogue output */
		 if (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_OUTPUT)
		 {
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_SPI_DIGIPOT:
				if (BoardIOMap[ioID].ioAddr.spiDigiPot.busSspi == sspBus)
				{
					// find & configure peripheral pins
					configureSPIPeripheral(sspBus, 8);
					ioID = BOARD_IO_MAX_IDX;
				}
				break;

			case IO_DEV_SPI_AD_DIGITAL_POT:
				if (BoardIOMap[ioID].ioAddr.spiDigiPot.busSspi == sspBus)
				{
					// find & configure peripheral pins
					configureSPIPeripheral(sspBus, 10);
					ioID = BOARD_IO_MAX_IDX;
				}
				break;

			default:
				break;
			}
		 }
	 }

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SPI write to a digipot channel addressed
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckDAC(void)
{
	SB_ERROR				retVal;
	lu_uint32_t				ioID;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		 /* Is an Analogue output */
		 if (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_OUTPUT)
		 {
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_PERIPH:
				retVal = ProcDACConfigure(BoardIOMap[ioID].ioAddr.gpio.port,
						                  BoardIOMap[ioID].ioAddr.gpio.pin,
						                  BoardIOMap[ioID].ioAddr.gpio.periphFunc);

				ProcDACInit();

				ioID = BOARD_IO_MAX_IDX;
				break;

			default:
				break;
			}
		 }
	 }

	return SB_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */
