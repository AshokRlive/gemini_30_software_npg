/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       SPI Analogue Input (mcpxxx) IO Manager update module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/08/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_ssp.h"
#include "lpc17xx_spi.h"

#include "errorCodes.h"

#include "systemTime.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerExtAnalogIOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorSPI.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define EXT_ADC_AVERAGE_ENABLE	1 /* Enable adc average */

#define EXT_ADC_MAX_CHAN		8
#define EXT_ADC_MAX_SAMP        5

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


SB_ERROR readSpiAdcValue(lu_uint32_t ioID, lu_uint8_t spiBus, lu_uint16_t *valuePtr);

SB_ERROR readLegacySpiAdcValue(lu_uint32_t ioID, lu_uint16_t *valuePtr);

SB_ERROR configureCheckAdcSPI(lu_uint8_t sspBus);



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static lu_int32_t   extAdcAverage[EXT_ADC_MAX_CHAN][EXT_ADC_MAX_SAMP];
static lu_uint32_t  sampIdx[EXT_ADC_MAX_CHAN] = {0,0,0,0,0,0,0,0};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR IOManagerExtAnalogIOInit(void)
{
	 SB_ERROR	retError;

	 retError = SB_ERROR_NONE;

	 /* Configure SPI pins & SSP peripheral */
	 configureCheckAdcSPI(IO_BUS_SPI_SSPI_0);
	 configureCheckAdcSPI(IO_BUS_SPI_SSPI_1);

	 /* Configure Legacy SPI peripheral */
	 configureCheckAdcSPI(IO_BUS_SPI_SPI);

	 return retError;
}

SB_ERROR IOManagerExtAnalogIOUpdate(void)
{
	SB_ERROR	        retError;
	lu_uint8_t          sspBus;
	lu_uint8_t			foundAnalog;
	lu_uint16_t			value;
	static lu_uint32_t	ioID = 0;
	lu_uint32_t			idx, chanIdx;

	retError = SB_ERROR_NONE;

	foundAnalog = 0;

	/* Perform a single IO update on all IO in the ioTable[] per scan */
	if (ioID >= BOARD_IO_MAX_IDX)
	{
		ioID = 0;
	}

	do
	{
		/* Is an Analogue input */
		if (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_INPUT)
		{
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_SPI_ADC:
				if (convertIoIDtoSpiBus(ioID, &sspBus) == LU_TRUE)
				{
					if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					{
						/* Read SPI ADC */
						readSpiAdcValue(ioID, sspBus, &value);

						boardIOTable[ioID].rawValue = value;

						/* Apply filter here ..... */
#ifdef EXT_ADC_AVERAGE_ENABLE
						chanIdx = (BoardIOMap[ioID].ioAddr.spiAdc.adcChan & 0x7);

						if (boardIOTable[ioID].InitAvgSamples == LU_TRUE)
						{
							boardIOTable[ioID].InitAvgSamples = LU_FALSE;

							for (idx = 0; idx < EXT_ADC_MAX_SAMP; idx++)
							{
								extAdcAverage[chanIdx][idx] = value;
							}
						}

						/* Add sample to buffer */
						extAdcAverage[chanIdx][sampIdx[chanIdx]++] = value;
						if (sampIdx[chanIdx] >= EXT_ADC_MAX_SAMP)
						{
							sampIdx[chanIdx] = 0;
						}

						/* Sum buffer values */
						boardIOTable[ioID].averageSumValue = 0;
						for (idx = 0; idx < EXT_ADC_MAX_SAMP; idx++)
						{
							boardIOTable[ioID].averageSumValue += extAdcAverage[chanIdx][idx];
						}

						/* Calculate the average */
						value            = (boardIOTable[ioID].averageSumValue / EXT_ADC_MAX_SAMP);
#endif
					}
					else
					{
						value = boardIOTable[ioID].value;
					}

					/* Set filtered value */
					boardIOTable[ioID].value  = value;

					foundAnalog = 1;
				}
				break;

			default:
				break;
			}
		}
		ioID++;
	} while (ioID < BOARD_IO_MAX_IDX && !foundAnalog);

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*!
 ******************************************************************************
 *   \brief Do an SSP read of ADC channel addressed
 *
 *   Detailed description
 *
 *   \param ioID
 *   \param spiBus
 *   \param *valuePtr
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readSpiAdcValue(lu_uint32_t ioID, lu_uint8_t spiBus, lu_uint16_t *valuePtr)
{
	lu_uint8_t				csIoID;
	lu_uint8_t				txBuf[3];
	lu_uint8_t				rxBuf[3];
	lu_uint16_t				adcValue;
	lu_uint32_t				xferLen;
	lu_uint32_t				pinValue;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	csIoID = BoardIOMap[ioID].ioAddr.spiAdc.csIoID;

	txBuf[0] = (((BoardIOMap[ioID].ioAddr.spiAdc.adcChan & 0x0f) >> 2) | 0x04);
	txBuf[1] = ((BoardIOMap[ioID].ioAddr.spiAdc.adcChan  & 0x0f) << 6);
	txBuf[2] = 0x00;

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 3;

	switch (spiBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	case 2:
		// Legacy SPI peripheral
		return readLegacySpiAdcValue(ioID, valuePtr);
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

    sspTyp->CR0 &= 0xFFF0;
	sspTyp->CR0 |= SSP_DATABIT_8;
	
	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	adcValue  = rxBuf[2];
	adcValue |= ((rxBuf[1] & 0x0f) << 8);

	/* Return the read value */
	*valuePtr = adcValue;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an Legacy SPI read of ADC channel addressed
 *
 *   Detailed description
 *
 *   \param ioID
 *   \param *valuePtr
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readLegacySpiAdcValue(lu_uint32_t ioID, lu_uint16_t *valuePtr)
{
	lu_uint8_t				csIoID;
	lu_uint8_t				txBuf[3];
	lu_uint8_t				rxBuf[3];
	lu_uint16_t				adcValue;
	lu_uint32_t				xferLen;
	lu_uint32_t				pinValue;
	SPI_DATA_SETUP_Type     xferConfig;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	csIoID = BoardIOMap[ioID].ioAddr.spiAdc.csIoID;

	txBuf[0] = (((BoardIOMap[ioID].ioAddr.spiAdc.adcChan & 0x0f) >> 2) | 0x04);
	txBuf[1] = ((BoardIOMap[ioID].ioAddr.spiAdc.adcChan  & 0x0f) << 6);
	txBuf[2] = 0x00;

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 3;

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	//sspTyp->CR0 &= 0xFFF0;
	//sspTyp->CR0 |= SSP_DATABIT_8;

	xferLen = SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	adcValue  = rxBuf[2];
	adcValue |= ((rxBuf[1] & 0x0f) << 8);

	/* Return the read value */
	*valuePtr = adcValue;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Do an SPI write to a digipot channel addressed
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR configureCheckAdcSPI(lu_uint8_t sspBus)
{
	SB_ERROR				retVal;
	lu_uint32_t				ioID;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		 /* Is an Analogue input */
		 if (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_INPUT)
		 {
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_SPI_ADC:
				if (BoardIOMap[ioID].ioAddr.spiAdc.busSspi == sspBus)
				{
					// find & configure peripheral pins with 8 bit data
					configureSPIPeripheral(sspBus, 8);
					ioID = BOARD_IO_MAX_IDX;
				}
				break;

			default:
				break;
			}
		 }
	 }

	return SB_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */
