/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/01/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IOManager.h"

#include "ProcessorGPIOMap.h"
#include "ProcessorDAC.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ProcDACInit(void)
{
	DAC_Init(LPC_DAC);

	ProcDACSetValue(512);

    return SB_ERROR_NONE;
}


SB_ERROR ProcDACConfigure(
		                  lu_uint8_t 		port,
	                      lu_uint8_t 		pin,
		                  GPIO_PIN_FUNC     func
		                 )
{
	SB_ERROR  		    retError;
	lu_uint8_t			pinDir;
	lu_uint8_t          pinFunc;
	PINSEL_CFG_Type 	pinCfg;
	lu_uint32_t	        pinShift;

	retError = SB_ERROR_INITIALIZED;

	pinDir = 0; /* Default to input */

	pinShift = 1;
	pinShift = pinShift << (pin & 0x1f);

	/* Now do the pin configuration */
	if (port < MAX_GPIO_PORTS && pin < MAX_GPIO_PORT_BITS)
	{
		pinFunc = ProcGPIOCheckPeripheralPinPort(func, port, pin);

		if (pinFunc)
		{
			/* Set pin configuration */
			pinCfg.Funcnum   = pinFunc;
			pinCfg.Portnum   = port;
			pinCfg.Pinnum    = pin;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
			pinCfg.Pinmode   = 0;

			/* Configure pin type */
			PINSEL_ConfigPin(&pinCfg);

			/* pin configuration was successful */
			retError = SB_ERROR_NONE;
		}
	}

	return retError;
}

SB_ERROR ProcDACSetValue(lu_uint32_t dacValue)
{
	DAC_UpdateValue(LPC_DAC, dacValue);

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
