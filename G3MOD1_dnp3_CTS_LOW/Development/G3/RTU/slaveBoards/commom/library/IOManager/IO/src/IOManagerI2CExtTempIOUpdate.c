/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/04/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerI2CExtTempIOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"

#include "systemTime.h"

#include "BoardIOMap.h"
#include "BoardIO.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_I2C_TMP_PER_BUS			3  /* Max supported devices on a I2C bus */

#define LM73_OFFLINE_DETECT_MS		10000 /* Db time for off / online detect */

#define LM73_ADDR					0x48 // was 0x90

#define LM73_IDENT_NATIONAL			0x190 // Identification reg

#define LM73_REG_MASK				0x07

#define LM73_REG_TEMPERATURE		0x00
#define LM73_REG_CONFIGURATION		0x01
#define LM73_REG_T_HIGH				0x02
#define LM73_REG_T_LOW				0x03
#define LM73_REG_CONTROL_STATUS		0x04
#define LM73_REG_IDENTIFICATION		0x07

#define LM73_CFG_FULL_POWER_DOWN	0x80
#define LM73_CFG_ALERT_DISABLE		0x20
#define LM73_CFG_ALERT_ACTIVE_HIGH	0x10
#define LM73_CFG_ALERT_RESET		0x08
#define LM73_CFG_ONE_SHOT			0x04

/* Control Status temperature resolution */
#define LM73_CONSTAT_RES_0_25       0x0
#define LM73_CONSTAT_RES_0_125      0x1
#define LM73_CONSTAT_RES_0_0625     0x2
#define LM73_CONSTAT_RES_0_03125    0x3

#define LM73_CONSTAT_RES_BITSHIFT	5

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*!
 *  IO Expander data data direction
 */
typedef struct I2cTempOfflineDbDef
{
	lu_uint8_t		addr; 	/* I2C Address */
	lu_uint32_t     dbTime;
}I2cTempOfflineDbStr;



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR configureCheckI2CTempLM73(lu_uint8_t i2cBus);
SB_ERROR configureAllI2CTempLM73(void);
SB_ERROR readI2cTempLM73(lu_uint8_t i2cBus, lu_uint8_t addr, lu_int32_t *valuePtr);
SB_ERROR readI2cTempLM73Reg(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t reg, lu_uint16_t *valuePtr);
SB_ERROR writeI2cTempLM73Reg8Bit(lu_uint8_t i2cBus,
		                         lu_uint8_t addr,
		                         lu_uint8_t reg,
		                         lu_uint8_t value);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint8_t				i2cBusDeviceCount[IO_BUS_I2C_2+1];
static I2cTempOfflineDbStr      i2cTempOffLineData[IO_BUS_I2C_2+1][MAX_I2C_TMP_PER_BUS + 1];

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR IOManagerI2CExtTempIOInit(void)
{
	 SB_ERROR	retError;

	 retError = SB_ERROR_NONE;

	 /* Configure I2C pins & peripheral */
	 configureCheckI2CTempLM73(IO_BUS_I2C_0);
	 configureCheckI2CTempLM73(IO_BUS_I2C_1);
	 configureCheckI2CTempLM73(IO_BUS_I2C_2);

	 /* Now configure I2C Temperature sensors */
	 retError = configureAllI2CTempLM73();

	 return retError;
}

SB_ERROR IOManagerI2CExtTempIOUpdate(void)
{
	 SB_ERROR	        retError;
	 lu_uint8_t			foundAnalog;
	 lu_int32_t 		value;
//	 lu_uint16_t        identifyPart;
	 ModuleTimeStr	    canTime;
	 lu_uint8_t		    i2cAddrIdx = 0;
	 lu_uint32_t        time;
	 lu_uint32_t        elapsedTime;
	 static lu_uint32_t	ioID = 0;

	 canTime     = CANCGetTime();

	 /*! Get the current time */
	 time = STGetTime();

	 retError = SB_ERROR_NONE;

	 foundAnalog = 0;

	 /* Perform a single IO update on all IO in the ioTable[] per scan */
	 if (ioID >= BOARD_IO_MAX_IDX)
	 {
		ioID = 0;
	 }

	 /* Perform an IO update on all IO in the ioTable[] */
	 do
	 {
		 switch(BoardIOMap[ioID].ioDev)
		 {
		 case IO_DEV_I2C_TEMP_LM73:
			 /* Is an I2C Temperature sensor */
			 if (BoardIOMap[ioID].ioClass == IO_CLASS_ANALOG_INPUT)
			 {
				if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
				{
					/* Set resolution to 0.125 deg c */
					retError = writeI2cTempLM73Reg8Bit(BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c,
													   BoardIOMap[ioID].ioAddr.i2cTempLM73.address,
													   LM73_REG_CONTROL_STATUS,
													   (LM73_CONSTAT_RES_0_125 << LM73_CONSTAT_RES_BITSHIFT));

					/* Read I2C Temperature Sensor */
					retError = readI2cTempLM73(BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c,
											   BoardIOMap[ioID].ioAddr.i2cTempLM73.address,
											   &value);

					/* Find addresss idx */
					for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_TMP_PER_BUS; i2cAddrIdx++)
					{
						if (i2cTempOffLineData[BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c][i2cAddrIdx].addr ==
						    BoardIOMap[ioID].ioAddr.i2cTempLM73.address
						)
						{
							break;
						}
					}

					if (retError == SB_ERROR_NONE)
					{
						if (BoardIOMap[ioID].ioAddr.i2cTempLM73.mode & GPIO_PM_OFFLINE_OVERSAMPLE)
						{
							if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PRE_OFFLINE)
							{
								/* Get time when state changes */
								i2cTempOffLineData[BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c][i2cAddrIdx].dbTime = STGetTime();
							}
							else
							{
								/* Clear offline after debounce period */
								elapsedTime = i2cTempOffLineData[BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c][i2cAddrIdx].dbTime;

								if(STElapsedTime(elapsedTime, time) > LM73_OFFLINE_DETECT_MS)
								{
									boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_OFFLINE;
								}
							}
							boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_PRE_OFFLINE;
						}
						else
						{
							boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_OFFLINE;
						}


						/* Convert to deg c */
						value = (lu_int16_t)value / 16;
						value = value * 125;

						boardIOTable[ioID].rawValue = value;

						/* Apply filter here... */
						boardIOTable[ioID].value  = boardIOTable[ioID].rawValue;
					}
					else
					{
						if (BoardIOMap[ioID].ioAddr.i2cTempLM73.mode & GPIO_PM_OFFLINE_OVERSAMPLE)
						{
							if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PRE_OFFLINE))
							{
								/* Get time when state changes */
								i2cTempOffLineData[BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c][i2cAddrIdx].dbTime = STGetTime();
							}
							else
							{
								/* Set offline after debounce period */
								elapsedTime = i2cTempOffLineData[BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c][i2cAddrIdx].dbTime;

								if(STElapsedTime(elapsedTime, time) > LM73_OFFLINE_DETECT_MS)
								{
									boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_OFFLINE;
								}
							}

							boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_PRE_OFFLINE;
						}
						else
						{
							boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_OFFLINE;
						}
					}

					foundAnalog = 1;
				}
			}
			break;


		 default:
			break;
		 }

		 ioID++;
	 } while (ioID < BOARD_IO_MAX_IDX && !foundAnalog);

	 return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


 /*!
  ******************************************************************************
  *   \brief Check for I2C Temperature sensors & configure I2C if found
  *
  *   Detailed description
  *
  *   \param i2cBus - Physical I2C bus to use
  *
  *
  *   \return SB_ERROR Error code
  *
  ******************************************************************************
  */
 SB_ERROR configureCheckI2CTempLM73(lu_uint8_t i2cBus)
 {
 	SB_ERROR				retVal;
 	lu_uint32_t				ioID;

 	retVal = SB_ERROR_INITIALIZED;

 	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
 	{
 		/* Is Digital IO */
 		switch(BoardIOMap[ioID].ioClass)
 		{
 		case IO_CLASS_ANALOG_INPUT:
 			switch(BoardIOMap[ioID].ioDev)
 			{
 			/* Is an I2C Expander device */
 			case IO_DEV_I2C_TEMP_LM73:
 				if (BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c == i2cBus)
 				{
 					// find & configure peripheral pins
 					retVal = configureI2CPeripheral(i2cBus);
 					ioID = BOARD_IO_MAX_IDX;
 				}
 				break;

 			default:
 				break;
 			}
 			break;

 		default:
 			break;

 		}
 	}

 	return SB_ERROR_NONE;
 }

/*!
  ******************************************************************************
  *   \brief Configure all I2C Temperature sensors
  *
  *   Detailed description
  *
  *   \param none
  *
  *
  *   \return SB_ERROR Error code
  *
  ******************************************************************************
  */
 SB_ERROR configureAllI2CTempLM73(void)
 {
	 SB_ERROR				retError;
	 lu_uint32_t			ioID;
	 lu_uint16_t			value;
	 lu_uint8_t				i2cBusIdx;
	 lu_uint8_t				i2cAddrIdx;

	 retError = SB_ERROR_NONE;

	 for (i2cBusIdx = 0; i2cBusIdx <= IO_BUS_I2C_2; i2cBusIdx++)
	 {
		 i2cBusDeviceCount[i2cBusIdx] = 0;

		 /* Default: Init I2C Exp IO Data Direction to inputs */
		 for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_TMP_PER_BUS; i2cAddrIdx++)
		 {
			 i2cTempOffLineData[i2cBusIdx][i2cAddrIdx].addr   = 0xff;
			 i2cTempOffLineData[i2cBusIdx][i2cAddrIdx].dbTime = STGetTime();
		 }
	 }

	 for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	 {
		/* Is Analogue IO */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_ANALOG_INPUT:
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_I2C_TEMP_LM73:
				i2cBusIdx = BoardIOMap[ioID].ioAddr.i2cTempLM73.busI2c;

				if (i2cBusIdx <= IO_BUS_I2C_2)
				{
					/* Initialise for temp device found on an I2C bus */
					if (i2cBusDeviceCount[i2cBusIdx] < MAX_I2C_TMP_PER_BUS)
					{
						i2cTempOffLineData[i2cBusIdx][i2cBusDeviceCount[i2cBusIdx]].addr = BoardIOMap[ioID].ioAddr.i2cTempLM73.address;
						i2cBusDeviceCount[i2cBusIdx]++;
					}

					// Do stuff here to configure the device...
					retError = readI2cTempLM73Reg(i2cBusIdx,
												  BoardIOMap[ioID].ioAddr.i2cTempLM73.address,
												  LM73_REG_IDENTIFICATION,
												  &value);

					if (retError == SB_ERROR_NONE && value == LM73_IDENT_NATIONAL)
					{
						/* Is National LM73 */
						boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_ALT_CFG;
					}
					else
					{
						/* Is Analog devices part */
						boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_ALT_CFG;
					}

					/* Re-enable sampling after startup */
					retError = writeI2cTempLM73Reg8Bit(i2cBusIdx,
													   BoardIOMap[ioID].ioAddr.i2cTempLM73.address,
													   LM73_REG_CONFIGURATION,
													   LM73_CFG_ALERT_DISABLE);

					if (retError != SB_ERROR_NONE)
					{
						/* ioID is offline / device not available */

						if (BoardIOMap[ioID].ioAddr.i2cTempLM73.mode & GPIO_PM_OFFLINE_OVERSAMPLE)
						{
							boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_PRE_OFFLINE;
						}
						else
						{
							boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_OFFLINE;
						}
					}
					/* Set bit mode ? */

				}
				break;

			default:
				break;
			}

			default:
				break;
		 }
	 }

	 return SB_ERROR_NONE;
}


/*!
 ******************************************************************************
 *   \brief Read LM73 register
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the LM73
 *   \param valuePtr - return value via pointer
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cTempLM73(lu_uint8_t i2cBus, lu_uint8_t addr, lu_int32_t *valuePtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_int16_t              value;
	lu_uint8_t				txData[3];
	lu_uint8_t				rxData[3];
	static lu_bool_t        failToggle = LU_FALSE;

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= LM73_REG_TEMPERATURE;

		 txferCfg.sl_addr7bit     	    = (LM73_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 failToggle = failToggle ? 0 : 1;

			 if (failToggle)
			 {
//				 GPIO_SetValue(2, 0);
			 }
			else
			{
//			 	GPIO_ClearValue(2, 0);
			}
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 value = rxData[1];
			 value |= (rxData[0] << 8);

			 /* Convert to Deg Centigrade */

			 //value = (lu_int16_t)value / 250;

			 //value = value / 128;

			 *valuePtr = value;
		 }
	 }

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Read LM73 register
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the LM73
 *   \param reg - register offset for LM73
 *   \param valuePtr - return value via pointer
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cTempLM73Reg(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t reg, lu_uint16_t *valuePtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint32_t              value;
	lu_uint8_t				txData[3];
	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= (reg & LM73_REG_MASK);

		 txferCfg.sl_addr7bit     	    = (LM73_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.retransmissions_max 	= 2;

		 switch (reg)
		 {
		 default:
		 case LM73_REG_TEMPERATURE:
		 case LM73_REG_T_HIGH:
		 case LM73_REG_T_LOW:
		 case LM73_REG_IDENTIFICATION:
			 txferCfg.rx_length       		= 2;
			 break;

		 case LM73_REG_CONFIGURATION:
		 case LM73_REG_CONTROL_STATUS:
			 txferCfg.rx_length       		= 1;
			 break;
		 }

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 switch (reg)
			 {
			 default:
			 case LM73_REG_TEMPERATURE:
			 case LM73_REG_T_HIGH:
			 case LM73_REG_T_LOW:
			 case LM73_REG_IDENTIFICATION:
				 value = rxData[1];
				 value |= (rxData[0] << 8);
				 break;

			 case LM73_REG_CONFIGURATION:
			 case LM73_REG_CONTROL_STATUS:
				 value = rxData[1];
				 break;
			 }

			 *valuePtr = value;
		 }
	 }

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Write LM73 register
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the LM73
 *   \param reg - register offset for LM73
 *   \param value - value to write to register
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeI2cTempLM73Reg8Bit(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t reg, lu_uint8_t value)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
//	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= (reg & LM73_REG_MASK);
		 txData[1] 						= value;

		 txferCfg.sl_addr7bit     	    = (LM73_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 2;
		 txferCfg.rx_data		 	 	= 0L;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }

	return retError;
}

/*
 *********************** End of file ******************************************
 */
