/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/04/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _IOMANAGERI2CEXTTEMPIOUPDATE_INCLUDED
#define _IOMANAGERI2CEXTTEMPIOUPDATE_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define IOMAN_UPDATE_I2CEXTMPIO_MS			200

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise the LM73 temperature sensor
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerI2CExtTempIOInit(void);

/*!
 ******************************************************************************
 *   \brief Scan update read the LM73 for IOManager
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerI2CExtTempIOUpdate(void);

#endif /* _IOMANAGERI2CEXTTEMPIOUPDATE_INCLUDED */

/*
 *********************** End of file ******************************************
 */
