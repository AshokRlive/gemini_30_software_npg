/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/10/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _PROCESSORSPI_INCLUDED
#define _PROCESSORSPI_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Configure SPI peripheral
 *
 *   Detailed description
 *
 *   \param sspBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
extern SB_ERROR configureSPIPeripheral(lu_uint8_t spiBus, lu_uint32_t numDataBits);

/*!
 ******************************************************************************
 *   \brief Configure SPI peripheral
 *
 *   Detailed description
 *
 *   \param sspBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
extern SB_ERROR configureSPIParamPeripheral(lu_uint8_t spiBus, SSP_CFG_Type *sspConfig);

/*!
 ******************************************************************************
 *   \brief Configure Slave SPI peripheral
 *
 *   \param sspBus SPI Bus address
 *   \param sspConfig SPI configuration
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
extern SB_ERROR configureSPISspSlPeripheral(lu_uint8_t sspBus, SSP_CFG_Type *sspConfig);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern lu_uint8_t convertIoIDtoSpiBus(lu_uint32_t ioID, lu_uint8_t *retSpiBusPtr);

#endif /* _PROCESSORSPI_INCLUDED */

/*
 *********************** End of file ******************************************
 */
