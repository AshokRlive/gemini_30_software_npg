/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "Calibration.h"
#include "IOManager.h"
#include "IOManagerIO.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorADC.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "IOManagerAnalogOutIOUpdate.h"
#include "IOManagerI2CIOUpdate.h"
#include "IOManagerI2CExtTempIOUpdate.h"
#include "IOManagerExtAnalogIOUpdate.h"

#include "IOManagerExtADE78xxInit.h"
#include "IOManagerFrontEndPICIOUpdate.h"
#include "IOManagerI2CExtAdcMCP342XOUpdate.h"

#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardIO.h"

#include "systemStatus.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR IOManagerPollAI(ModulePollChanIdStr *pollChanId);
SB_ERROR IOManagerPollDI(ModulePollChanIdStr *pollChanId);

SB_ERROR IOManagerReadIoID(ModuleReadIoIdStr *readIoIO);
SB_ERROR IOManagerWriteIoID(ModuleWriteIoIdStr *writeIoIO);
SB_ERROR IOManagerPulseIoID(ModulePulseIoIdStr *pulseIoIO);
SB_ERROR IOManagerFlashIoID(ModuleFlashIoIdStr *flashIoIO);

SB_ERROR IOManagerSetSimulationMode(ModuleSetSimStr *setSimPtr);
SB_ERROR IOManagerSetSimulationIoID(ModuleSimIoIdStr *setSimIoIDPtr);
SB_ERROR IOManagerSetSimulationAIChan(ModuleSimAIChanIdStr *setSimAIChanPtr);
SB_ERROR IOManagerSetSimulationDIChan(ModuleSimDIChanIdStr *setSimDIChanPtr);

SB_ERROR IOManagerConfigIoChanAI(ModuleCfgIoChanAIStr *cfgChanAI);
SB_ERROR IOManagerConfigIoChanDI(ModuleCfgIoChanDIStr *cfgChanDI);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported message */
static const filterTableStr IOManagerModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! IOManager commands */
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_C        , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_C     , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_PULSE_IOID_C     , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_FLASH_IOID_C     , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_SET_SIM_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_IOID_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_AI_CHAN_C    , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_DI_CHAN_C    , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_W_RAW_IOID_C     , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_DIAG_R_RAW_IOID_C     , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_C           , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_C           , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_MD_CMD, MODULE_MSG_ID_MD_CMD_STOP_EVENTING           , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_MD_CMD, MODULE_MSG_ID_MD_CMD_STOP_EVENTING           , LU_TRUE  , 0      },

    /* For FDM Testing only - To be removed after design testing */
//    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_C   , LU_FALSE , 1      },
//    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_C   , LU_FALSE , 0      },
//    {  MODULE_MSG_TYPE_IOMAN, MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_C    , LU_FALSE , 0      },
};

static lu_bool_t ioManagerSimulationEnabled = LU_FALSE;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR IOManagerInit(void)
{
    lu_uint16_t					ioMapIdx;
    SB_ERROR                    retError;

    retError = SB_ERROR_NONE;

    /* Check if IOMAP is valid */
	for (ioMapIdx = 0 ; ioMapIdx < BOARD_IO_MAX_IDX; ioMapIdx++)
	{
		if (BoardIOMap[ioMapIdx].ioID != ioMapIdx)
		{
			retError = SB_ERROR_INITIALIZED;
			break;
		}
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Initialise IO using IOMAP */
		for (ioMapIdx = 0 ; ioMapIdx < BOARD_IO_MAX_IDX; ioMapIdx++)
		{
			switch (BoardIOMap[ioMapIdx].ioClass)
			{
			case IO_CLASS_GPIO_PERIPH:
				break;

			case IO_CLASS_DIGITAL_INPUT:
			case IO_CLASS_DIGITAL_OUTPUT:
			case IO_CLASS_DIGITAL_INPUT_OUTPUT:
				switch (BoardIOMap[ioMapIdx].ioDev)
				{
				case IO_DEV_GPIO:
					/* configure Processor GPIO */
					retError = ProcGPIOConfigure(BoardIOMap[ioMapIdx].ioAddr.gpio.port,
												 BoardIOMap[ioMapIdx].ioAddr.gpio.pin,
												 BoardIOMap[ioMapIdx].ioClass,
												 BoardIOMap[ioMapIdx].ioAddr.gpio.pinMode);
					break;

				default:
					break;
				}
				break;

			case IO_CLASS_ANALOG_INPUT:
				switch (BoardIOMap[ioMapIdx].ioDev)
				{
				case IO_DEV_PERIPH:
					/* configure Processor ADC */
					retError = ProcADCConfigure(BoardIOMap[ioMapIdx].ioAddr.adc.port,
												BoardIOMap[ioMapIdx].ioAddr.adc.pin,
												BoardIOMap[ioMapIdx].ioAddr.adc.periphFunc);
					break;

				default:
					break;
				}
				break;

			case IO_CLASS_ANALOG_OUTPUT:
				break;

			default:
				break;
			}

			if (retError == SB_ERROR_INITIALIZED)
			{
				retError = SB_ERROR_INITIALIZED;
			}
		}
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Initialise/check Channel IO Map */
		retError = IOManagerIOChanMapInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Initialise the IOManager - with board specific IOMap */
		retError = IOManagerIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup ADC peripheral */
		retError = IOManagerIntAnalogIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup SPI Digi pots */
		retError = IOManagerAnalogOutIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup I2C Expansion IO */
		retError = IOManagerI2CIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup I2C Temperature Sensor */
		retError = IOManagerI2CExtTempIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup SPI ADC */
		retError = IOManagerExtAnalogIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup ADE78xx ADC */
		retError = IOManagerExtAD78xxIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup Front End PIC SPI controller for DMA */
		IOManagerFrontEndPICIOInit();
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Setup I2C ADC MCP342X */
		IOManagerI2CExtAdcMCP342XIOInit();
	}

	/* Trap error - Failed to initialise correctly ? */
	while (retError != SB_ERROR_NONE)
	{

	}

	return retError;
}

SB_ERROR IOManagerStartEventing(void)
{
	ioManagerIOEventingEnable = LU_TRUE;

	return SB_ERROR_NONE;
}

SB_ERROR IOManagerStopEventing(void)
{
	ioManagerIOEventingEnable = LU_FALSE;

	return SB_ERROR_NONE;
}

SB_ERROR IOManagerInitCANFilter(void)
{
	SB_ERROR                    retError;

	 /* Setup CAN Msg filter */
	retError = CANFramingAddFilter( IOManagerModulefilterTable,
									SU_TABLE_SIZE(IOManagerModulefilterTable, filterTableStr)
								  );
	return retError;
}

SB_ERROR IOManagerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
 	SB_ERROR	retError;

 	LU_UNUSED(time);

 	retError = SB_ERROR_CANC;

 	switch (msgPtr->messageType)
	{

	case MODULE_MSG_TYPE_IOMAN:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModulePollChanIdStr))
			{
				retError = IOManagerPollAI((ModulePollChanIdStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModulePollChanIdStr))
			{
				retError = IOManagerPollDI((ModulePollChanIdStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleReadIoIdStr))
			{
				retError = IOManagerReadIoID((ModuleReadIoIdStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleWriteIoIdStr))
			{
				/* Not a good idea to allow this, for factory test only!!! */
				retError = IOManagerWriteIoID((ModuleWriteIoIdStr *)msgPtr->msgBufPtr);
			}
			break;

		 case MODULE_MSG_ID_IOMAN_CMD_DIAG_PULSE_IOID_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModulePulseIoIdStr))
			{
				/* Not a good idea to allow this, for factory test only!!! */
				retError = IOManagerPulseIoID((ModulePulseIoIdStr *)msgPtr->msgBufPtr);
			}
			break;

		 case MODULE_MSG_ID_IOMAN_CMD_DIAG_FLASH_IOID_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModulePulseIoIdStr))
			{
				/* Not a good idea to allow this, for factory test only!!! */
				retError = IOManagerFlashIoID((ModuleFlashIoIdStr *)msgPtr->msgBufPtr);
			}
			break;

		 case MODULE_MSG_ID_IOMAN_CMD_DIAG_SET_SIM_C:
			 if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleSetSimStr))
			 {
				 /* Not a good idea to allow this, for factory test only!!! */
				 retError = IOManagerSetSimulationMode((ModuleSetSimStr *)msgPtr->msgBufPtr);
			 }
			 break;

		 case MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_IOID_C:
			 if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleSimIoIdStr) && ioManagerSimulationEnabled == LU_TRUE)
			 {
				 /* Not a good idea to allow this, for factory test only!!! */
				 retError = IOManagerSetSimulationIoID((ModuleSimIoIdStr *)msgPtr->msgBufPtr);
			 }
			 break;

		 case MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_AI_CHAN_C:
			 if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleSimAIChanIdStr) && ioManagerSimulationEnabled == LU_TRUE)
			 {
				 /* Not a good idea to allow this, for factory test only!!! */
				 retError = IOManagerSetSimulationAIChan((ModuleSimAIChanIdStr *)msgPtr->msgBufPtr);
			 }
			 break;

		 case MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_DI_CHAN_C:
			 if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleSimDIChanIdStr) && ioManagerSimulationEnabled == LU_TRUE)
			 {
				/* Not a good idea to allow this, for factory test only!!! */
				retError = IOManagerSetSimulationDIChan((ModuleSimDIChanIdStr *)msgPtr->msgBufPtr);
			 }
			 break;

		 case MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleCfgIoChanAIStr))
			{
				retError = IOManagerConfigIoChanAI((ModuleCfgIoChanAIStr *)msgPtr->msgBufPtr);
			}
			break;

		 case MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ModuleCfgIoChanDIStr))
			{
				retError = IOManagerConfigIoChanDI((ModuleCfgIoChanDIStr *)msgPtr->msgBufPtr);
			}
			break;
/*
		 case MODULE_MSG_ID_IOMAN_CMD_ADE7878_WRITE_REG_C:
		 	if (msgPtr->msgLen > (MODULE_MESSAGE_SIZE(ADE7878WriteRegStr)) - sizeof(lu_uint8_t*))
		 	{
		 		retError = IOManagerExtAD78xxWriteReg((ADE7878WriteRegStr *)msgPtr->msgBufPtr);
		 	}
		 	break;

		 case MODULE_MSG_ID_IOMAN_CMD_ADE7878_READ_REG_C:
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(ADE7878ReadRegStr))
			{
				retError = IOManagerExtAD78xxReadReg((ADE7878ReadRegStr *)msgPtr->msgBufPtr);
			}
			break;
*/
		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_MD_CMD:
		switch (msgPtr->messageID)
		{
			case MODULE_MSG_ID_MD_CMD_STOP_EVENTING:
				if (msgPtr->msgLen == 0)
				{
					retError = IOManagerStopEventing();

					SSSetBStatus(MODULE_BOARD_STATUS_WAIT_INIT);
				}
				break;

			default:
				retError = SB_ERROR_CANC_NOT_HANDLED;
				break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

 	return retError;
 }

SB_ERROR IOManagerSetOnline(lu_uint32_t ioID, lu_bool_t isOnline)
{
	SB_ERROR retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_INPUT:
		case IO_CLASS_ANALOG_INPUT:
			switch (BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_VIRTUAL:
			case IO_DEV_I2C_HUMIDITY:
				retError = SB_ERROR_NONE;

				if (isOnline == LU_TRUE)
				{
					boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_OFFLINE;
				}
				else
				{
					boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_OFFLINE;
				}
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
	}

	return retError;
}

SB_ERROR IOManagerGetValue(lu_uint32_t ioID, lu_int32_t *retValuePtr)
{
	SB_ERROR 			retError;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_INPUT:
		case IO_CLASS_DIGITAL_OUTPUT:
		case IO_CLASS_ANALOG_INPUT:
		case IO_CLASS_ANALOG_INPUT_MUX:
		case IO_CLASS_ANALOG_OUTPUT:
			switch (BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_GPIO:
			case IO_DEV_I2C_IO_EXPANDER:
			case IO_DEV_VIRTUAL:
			case IO_DEV_HSDC_ADE78XX:
			case IO_DEV_SPI_DIGIPOT:
			case IO_DEV_I2C_TEMP_LM73:
			case IO_DEV_PERIPH:
			case IO_DEV_SPI_FEPIC:
			case IO_DEV_I2C_HUMIDITY:
				return IOGetValue(ioID, retValuePtr);
				break;

			default:
				break;
			}
			break;

		case IO_CLASS_DIGITAL_INPUT_OUTPUT:
			switch (BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_GPIO:
				/* If not simulated, then use real IO operations */
				if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
				{
					/* Force the data direction to output */
					 retError = ProcGPIOConfigure(BoardIOMap[ioID].ioAddr.gpio.port,
												  BoardIOMap[ioID].ioAddr.gpio.pin,
												  IO_CLASS_DIGITAL_INPUT,
												  BoardIOMap[ioID].ioAddr.gpio.pinMode
												 );

					 *retValuePtr = ProcGPIORead(BoardIOMap[ioID].ioAddr.gpio.port, BoardIOMap[ioID].ioAddr.gpio.pin);
				}
				else
				{
					*retValuePtr = boardIOTable[ioID].value;
				}


				boardIOTable[ioID].rawValue = boardIOTable[ioID].value = *retValuePtr;
				return SB_ERROR_NONE;
				break;

			default:
				break;

			}
			break;

		default:
		 break;
		}
	}
	return SB_ERROR_PARAM;
}

SB_ERROR IOManagerGetCalibratedUnitsValue(lu_uint32_t ioID, lu_int32_t *retValuePtr)
{
	SB_ERROR 			retError;
	lu_int32_t          value;
	lu_uint8_t          chanIdx;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_ANALOG_INPUT:
		case IO_CLASS_ANALOG_INPUT_MUX:
			switch (BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_VIRTUAL:
			case IO_DEV_HSDC_ADE78XX:
			case IO_DEV_SPI_FEPIC:
			case IO_DEV_I2C_TEMP_LM73:
			case IO_DEV_PERIPH:
			case IO_DEV_SPI_ADC:
			case IO_DEV_I2C_ADC_MCP324X:

				retError =  IOGetValue(ioID, &value);

				if (retError == SB_ERROR_NONE)
				{
					chanIdx = BoardIOMap[ioID].ioChan;

					if (chanIdx < IO_CHAN_AI_MAP_SIZE)
					{
						/* convert value to calibrated value in scaled units */
						retError = CalibrationConvertValue(ioChanAIMap[chanIdx].calID,
														   value,
														   retValuePtr);
					}
				}
				else
				{
					/* No calibration has been found, just return raw value */
					*retValuePtr = value;
				}
				break;

			default:
				break;
			}
			break;

		default:
		 break;
		}
	}
	return retError;
}

 SB_ERROR IOManagerSetValue(lu_uint32_t ioID, lu_int32_t value)
 {
	 lu_uint8_t			digInvertValue;
	 lu_uint8_t			digValue;
	 SB_ERROR 			retError;

	 retError = SB_ERROR_PARAM;

	 if (ioID < BOARD_IO_MAX_IDX)
	 {
		 switch (BoardIOMap[ioID].ioClass)
		 {
		 case IO_CLASS_DIGITAL_OUTPUT:
			 /* Convert to digital value */
			 value = value ? 1 : 0;

			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_GPIO:
				 /* Update the boardIO table*/
				 retError = IOMSetValue(ioID, value);

				 if (retError == SB_ERROR_NONE)
				 {
					 /* Apply invert to physical output if required */
					 digInvertValue = value ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);

					 /* If not simulated, then use real IO operations */
					 if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					 {
						 /* Write the actual pin */
						 ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
									   BoardIOMap[ioID].ioAddr.gpio.pin,
									   (lu_uint8_t)digInvertValue);
					 }
				 }
				 break;

			 case IO_DEV_VIRTUAL:
			 case IO_DEV_I2C_IO_EXPANDER:
			 case IO_DEV_I2C_HUMIDITY:
				 /* Update the boardIO table*/
				 retError = IOMSetValue(ioID, value);
				 break;

			 default:
				 break;
			 }
			 break;

		 case IO_CLASS_DIGITAL_INPUT_OUTPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_GPIO:
				 /* Update the boardIO table*/
				 retError = IOMSetValue(ioID, value);

				 if (retError == SB_ERROR_NONE)
				 {
					 /* Apply invert to physical output if required */
					 digInvertValue = value ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);

					 /* If not simulated, then use real IO operations */
					 if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					 {
						 /* Write the actual pin */
						 ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
									   BoardIOMap[ioID].ioAddr.gpio.pin,
									   (lu_uint8_t)digInvertValue);

						 /* Force the data direction to output, but do not set the digital value! */
						 retError = ProcGPIOConfigure(BoardIOMap[ioID].ioAddr.gpio.port,
													  BoardIOMap[ioID].ioAddr.gpio.pin,
													  IO_CLASS_DIGITAL_OUTPUT_SET_DIR_ONLY,
													  BoardIOMap[ioID].ioAddr.gpio.pinMode
													 );
				     }
				 }
				 break;

			 default:
				 break;
			 }
			 break;

		 case IO_CLASS_DIGITAL_INPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_VIRTUAL:
				 /* Convert to digital value */
				 digValue = value ? 1 : 0;

				 /* Update the boardIO table*/
				 retError = IOMSetValue(ioID, digValue);
				 break;

			 case IO_DEV_I2C_IO_EXPANDER:
				 /* If simulated, then force input values */
				 if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO)
				 {
					 /* Convert to digital value */
					 digValue = value ? 1 : 0;

					 /* Update the boardIO table*/
					 retError = IOMSetValue(ioID, digValue);

					 /* Simulate an input event */
					 boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
				 }
				 break;

			 case IO_DEV_GPIO:
				 /* If simulated, then force input values */
				 if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO)
				 {
					 /* Convert to digital value */
					 digValue = value ? 1 : 0;

					 /* Update the boardIO table*/
					 retError = IOMSetValue(ioID, digValue);

					 /* Simulate an input event */
					 boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
				 }
				 break;

			 default:
				 break;
			 }
			 break;

		 case IO_CLASS_ANALOG_INPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_VIRTUAL:
			 case IO_DEV_I2C_HUMIDITY:
			 case IO_DEV_HSDC_ADE78XX:
			 case IO_DEV_SPI_FEPIC:
				 /* Update the boardIO table*/
				 retError = IOMSetValue(ioID, value);
				 break;

			 case IO_DEV_SPI_ADC:
			 case IO_DEV_I2C_TEMP_LM73:
			 case IO_DEV_PERIPH:
				 /* If simulated, then force input values */
				 if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO)
				 {
					 /* Update the boardIO table*/
					 retError = IOMSetValue(ioID, value);
				 }
				 break;

			 default:
				 break;
			 }
			 break;

		 case IO_CLASS_ANALOG_OUTPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_PERIPH:
			 case IO_DEV_VIRTUAL:
			 case IO_DEV_SPI_DIGIPOT:
			 case IO_DEV_SPI_AD_DIGITAL_POT:
				 /* Update the boardIO table*/
				 retError = IOMSetValue(ioID, value);
				 break;

			 default:
				 break;
			 }
			 break;

		 default:
			 break;
		 }
	 }
	 return retError;
}

SB_ERROR IOManagerForceOutputOff(lu_uint32_t ioID, lu_bool_t zeroValue)
{
	lu_uint8_t			digInvertValue;
	lu_int32_t          value;
	SB_ERROR 			retError;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_OUTPUT:
			if (zeroValue == LU_TRUE)
			{
				if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO))
				{
					/* Turn pin off - apply invert if required */
					boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO;

					switch(BoardIOMap[ioID].ioDev)
					{
					case IO_DEV_GPIO:
						 /* Apply invert to physical output if required */
						 digInvertValue = 0 ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);

						 /* Write the actual pin */
						 ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
									   BoardIOMap[ioID].ioAddr.gpio.pin,
									   (lu_uint8_t)digInvertValue);
						break;

					default:
						break;
					}
				}
			}
			else
			{
				if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO)
				{
					/* Restore original pin value */

					boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO;

					retError = IOManagerGetValue(ioID, &value);

					switch(BoardIOMap[ioID].ioDev)
					{
					case IO_DEV_GPIO:
						 digInvertValue = value ? 1 : 0;

						 /* Write the actual pin */
						 ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
									   BoardIOMap[ioID].ioAddr.gpio.pin,
									   (lu_uint8_t)digInvertValue);
						break;

					default:
						break;
					}
				}
			}
			retError = SB_ERROR_NONE;
			break;

		default:
			break;
		}
	}

	return retError;
}

SB_ERROR IOManagerSetPulse(lu_uint32_t ioID, lu_uint16_t timeMs)
{
	 SB_ERROR 			retError;
	 lu_int32_t			value;
	 lu_uint8_t			digValue;
	 lu_uint8_t			digInvertValue;

	 retError = SB_ERROR_PARAM;

	 if (ioID < BOARD_IO_MAX_IDX)
	 {
		 switch (BoardIOMap[ioID].ioClass)
		 {
		 case IO_CLASS_DIGITAL_OUTPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_VIRTUAL:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_FALSE);
				 break;

			 case IO_DEV_GPIO:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_FALSE);

				 if (retError == SB_ERROR_NONE)
				 {
					 /* Value is already toggled by call to IOMSetPulseGen() */
					 IOGetValue(ioID, &value);

					 /* Convert to digital value */
					 digValue = value ? 1 : 0;

					 /* Apply invert to physical output if required */
					 digInvertValue = digValue ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);

					 /* If not simulated, then use real IO operations */
					 if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					 {
						 /* Set initial value of pin */
						 ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
									   BoardIOMap[ioID].ioAddr.gpio.pin,
									   (lu_uint8_t)digInvertValue);
					 }
				 }
				 break;

			 case IO_DEV_I2C_IO_EXPANDER:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_FALSE);
				 break;

			 default:
				 break;
			 }
			 break;

		 case IO_CLASS_DIGITAL_INPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_VIRTUAL:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_FALSE);

				 /* Force a pending input event */
				 boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
				 break;

			 default:
				 break;
			 }
			 break;

		 default:
			 break;
		 }
	 }
	 return retError;
 }

 SB_ERROR IOManagerSetFlash(lu_uint32_t ioID, lu_uint16_t timeMs)
 {
	 SB_ERROR 		    retError;
	 lu_int32_t			value;
	 lu_uint8_t			digValue;
	 lu_uint8_t			digInvertValue;

	 retError = SB_ERROR_PARAM;

	 if (ioID < BOARD_IO_MAX_IDX)
	 {
		 switch (BoardIOMap[ioID].ioClass)
		 {
		 case IO_CLASS_DIGITAL_OUTPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_VIRTUAL:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_TRUE);
				 break;

			 case IO_DEV_GPIO:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_TRUE);

				 if (retError == SB_ERROR_NONE)
				 {
					 /* Value is already toggled by call to IOMSetPulseGen() */
					 IOGetValue(ioID, &value);

					 /* Convert to digital value */
					 digValue = value ? 1 : 0;

					 /* Apply invert to physical output if required */
					 digInvertValue = digValue ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);

					 /* If not simulated, then use real IO operations */
					 if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					 {
						 /* Set initial value of pin */
						 ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
									   BoardIOMap[ioID].ioAddr.gpio.pin,
									   (lu_uint8_t)digInvertValue);
					 }
				 }
				 break;

			 case IO_DEV_I2C_IO_EXPANDER:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_TRUE);
				 break;

			 default:
				 break;
			 }
			 break;

		 case IO_CLASS_DIGITAL_INPUT:
			 switch (BoardIOMap[ioID].ioDev )
			 {
			 case IO_DEV_VIRTUAL:
				 retError = IOMSetPulseGen(ioID, timeMs, LU_FALSE);

				 /* Force a pending input event */
				 boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
				 break;

			 default:
				 break;
			 }
			 break;

		default:
			 break;
		 }
	 }
	 return retError;
 }

SB_ERROR IOManagerDelayedToggle(lu_uint32_t ioID, lu_uint16_t timeMs)
{
	SB_ERROR 		    retError;
	lu_int32_t			value;
	lu_uint8_t			digValue;
	lu_uint8_t			digInvertValue;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		value = boardIOTable[ioID].value;

		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_OUTPUT:
			switch (BoardIOMap[ioID].ioDev )
			{
			case IO_DEV_VIRTUAL:
				/* Toggle state of current IO Pin */
				value = value ? 0 : 1;

				retError = IOMSetPulseGen(ioID, timeMs, LU_TRUE);

				/* Update the boardIO table*/
				retError = IOMSetValue(ioID, value);
				break;

			case IO_DEV_GPIO:
				/* Toggle state of current IO Pin */
				value = value ? 0 : 1;

				retError = IOMSetPulseGen(ioID, timeMs, LU_TRUE);

				/* Update the boardIO table*/
				retError = IOMSetValue(ioID, value);

				if (retError == SB_ERROR_NONE)
				{
					/* Value is already toggled by call to IOMSetPulseGen() */
					IOGetValue(ioID, &value);

					/* Convert to digital value */
					digValue = value ? 1 : 0;

					/* Apply invert to physical output if required */
					digInvertValue = digValue ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);

					/* If not simulated, then use real IO operations */
					if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
					{
						/* Set initial value of pin */
						ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
								   BoardIOMap[ioID].ioAddr.gpio.pin,
								   (lu_uint8_t)digInvertValue);
					}
				}
				break;

		 case IO_DEV_I2C_IO_EXPANDER:
			 /* Not supported yet! */
			 //retError = IOMSetPulseGen(ioID, timeMs, LU_TRUE);
			 break;

		 default:
			 break;
		 }
		 break;

		case IO_CLASS_DIGITAL_INPUT:
			switch (BoardIOMap[ioID].ioDev )
			{
			case IO_DEV_VIRTUAL:
				/* Toggle state of current IO Pin */
				value = value ? 0 : 1;

				retError = IOMSetPulseGen(ioID, timeMs, LU_FALSE);

				/* Update the boardIO table*/
				retError = IOMSetValue(ioID, value);

				/* Force a pending input event */
				boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
	}
	return retError;
}

 lu_uint8_t IOManagerCheckAnalgueChannel(lu_uint32_t ioID, lu_uint32_t channel)
 {
 	lu_uint8_t	ret;

 	ret = LU_FALSE;

	/* Check to see if ioID in IOMap[] has a channel ID that matches */
	if (ioID < BOARD_IO_MAX_IDX)
	{
		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_ANALOG_INPUT:
		case IO_CLASS_ANALOG_INPUT_MUX:
		case IO_CLASS_ANALOG_OUTPUT:
			if (BoardIOMap[ioID].ioChan == channel)
			{
				ret = LU_TRUE;
			}
			break;

		default:
			break;
		}
	}
 	return ret;
 }

 lu_uint8_t IOManagerCheckDigitalChannel(lu_uint32_t ioID, lu_uint32_t channel)
 {
 	lu_uint8_t	ret;

 	ret = LU_FALSE;

	/* Check to see if ioID in IOMap[] has a channel ID that matches */
	if (ioID < BOARD_IO_MAX_IDX)
	{
		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_INPUT:
		case IO_CLASS_DIGITAL_OUTPUT:
			if (BoardIOMap[ioID].ioChan == channel)
			{
				ret = LU_TRUE;
			}
			break;

		default:
			break;
		}
	}

 	return ret;
 }



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
 SB_ERROR IOManagerPollAI(ModulePollChanIdStr *pollChanId)
 {
 	lu_int32_t			value;
 	ModuleAIPollRspStr	retAI;
 	SB_ERROR 	        retError;

 	retAI.channel 		= pollChanId->channel;

 	/* Now read the value */
 	retError            = IOMChanAIGetValue(pollChanId->channel, &value);
 	retAI.value         = value;

 	retAI.simulated     = LU_FALSE;

	switch (retError)
	{
	case SB_ERROR_OFFLINE:
		retAI.isOnline = LU_FALSE;
		break;

	default:
		retAI.isOnline = LU_TRUE;
		break;
	}

 	/* convert value to calibrated value in scaled units */
 	if (retError == SB_ERROR_NONE)
 	{
		CalibrationConvertValue(ioChanAIMap[pollChanId->channel].calID,
								value,
								&retAI.value);
 	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
							MODULE_MSG_ID_IOMAN_CMD_POLL_AI_CHAN_R,
							sizeof(ModuleAIPollRspStr),
							(lu_uint8_t *)&retAI
						   );

 	return retError;
 }

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
  SB_ERROR IOManagerPollDI(ModulePollChanIdStr *pollChanId)
  {
  	lu_int32_t	         value;
  	lu_int32_t	         rawValue;
  	ModuleDIPollRspStr   retDI;
  	SB_ERROR 	         retError;

  	retDI.channel       = pollChanId->channel;

  	/* Now read the value */
  	retError            = IOMChanDIGetValue(pollChanId->channel, &value, &rawValue);
  	retDI.value         = value;
  	retDI.rawValue      = rawValue;

  	retDI.simulated     = LU_FALSE;

  	switch (retError)
  	{
  	case SB_ERROR_OFFLINE:
  		retDI.isOnline = LU_FALSE;
  		break;

  	default:
  		retDI.isOnline = LU_TRUE;
  		break;
  	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
							MODULE_MSG_ID_IOMAN_CMD_POLL_DI_CHAN_R,
							sizeof(ModuleDIPollRspStr),
							(lu_uint8_t *)&retDI
						   );


  	return retError;
  }

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerReadIoID(ModuleReadIoIdStr *readIoIO)
{
	SB_ERROR 	         retError;
	lu_int32_t			 retValue;
	ModuleReadIoIdRspStr retRsp;

	retError        = IOGetValue(readIoIO->ioID, &retValue);
	retRsp.value    = retValue;

	retError        = IOGetRawValue(readIoIO->ioID, &retValue);
	retRsp.rawValue = retValue;

	if (retError == SB_ERROR_NONE)
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
								MODULE_MSG_ID_IOMAN_CMD_DIAG_READ_IOID_R,
								sizeof(ModuleReadIoIdRspStr),
								(lu_uint8_t *)&retRsp
							   );
	}
	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerWriteIoID(ModuleWriteIoIdStr *writeIoIO)
{
	SB_ERROR 	         retError;
	lu_int32_t			 value;
	lu_uint32_t          ioID;

	value = writeIoIO->value;
	ioID  = writeIoIO->ioID;

	retError = IOManagerSetValue(ioID, value);

	if (retError == SB_ERROR_NONE)
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
								MODULE_MSG_ID_IOMAN_CMD_DIAG_WRITE_IOID_R,
								0,
								(lu_uint8_t *)0L
							   );
	}

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerPulseIoID(ModulePulseIoIdStr *pulseIoIO)
{
	SB_ERROR 	         retError;
	lu_int16_t			 timeMs;
	lu_uint32_t          ioID;

	timeMs = pulseIoIO->timeMs;
	ioID   = pulseIoIO->ioID;

	retError = IOManagerSetPulse(ioID, timeMs);

	if (retError == SB_ERROR_NONE)
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
								MODULE_MSG_ID_IOMAN_CMD_DIAG_PULSE_IOID_R,
								0,
								(lu_uint8_t *)0L
							   );
	}

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerFlashIoID(ModuleFlashIoIdStr *FlashIoIO)
{
	SB_ERROR 	         retError;
	lu_int16_t			 timeMs;
	lu_uint32_t          ioID;

	timeMs = FlashIoIO->timeMs;
	ioID   = FlashIoIO->ioID;

	retError = IOManagerSetFlash(ioID, timeMs);

	if (retError == SB_ERROR_NONE)
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
								MODULE_MSG_ID_IOMAN_CMD_DIAG_FLASH_IOID_R,
								0,
								(lu_uint8_t *)0L
							   );
	}

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerSetSimulationMode(ModuleSetSimStr *setSimPtr)
{
	ModStatusReplyStr    reply;
	SB_ERROR 	         retError;

	if(setSimPtr->enableSimulationIO)
	{
		ioManagerSimulationEnabled = LU_TRUE;

		/* Set alarm */
		SysAlarmSetLogEvent(LU_TRUE,
				            SYS_ALARM_SEVERITY_INFO,
				            SYS_ALARM_SUBSYSTEM_SYSTEM,
				            SYSALC_SYSTEM_IO_SIMULATION,
				            0
			               );
	}
	else
	{
		ioManagerSimulationEnabled = LU_FALSE;

		/* Now disable simulation mode for all ioID's */
		IOManagerIODisableSimulation();

		/* Clear alarm */
		SysAlarmSetLogEvent(LU_FALSE,
							SYS_ALARM_SEVERITY_INFO,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_IO_SIMULATION,
							0
						   );
	}

	reply.status = REPLY_STATUS_OKAY;

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
							MODULE_MSG_ID_IOMAN_CMD_DIAG_SET_SIM_R,
							sizeof(ModStatusReplyStr),
							(lu_uint8_t *)&reply
						   );

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerSetSimulationIoID(ModuleSimIoIdStr *setSimIoIDPtr)
{
	ModuleSimIoIdRspStr  reply;
	SB_ERROR 	         retError;

	retError = SB_ERROR_INITIALIZED;

	if(ioManagerSimulationEnabled)
	{
		retError = IOManagerIOSetSimIoID(setSimIoIDPtr->ioID, setSimIoIDPtr->enableSimulation);
	}

	reply.ioID   = setSimIoIDPtr->ioID;
	reply.status = REPLY_STATUS_OKAY;

	if (retError != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
			                MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_IOID_R,
							sizeof(ModuleSimIoIdRspStr),
							(lu_uint8_t *)&reply
						   );

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerSetSimulationAIChan(ModuleSimAIChanIdStr *setSimAIChanPtr)
{
	ModuleSimAIChanRspStr  reply;
	SB_ERROR 	           retError;

	retError = SB_ERROR_INITIALIZED;

	if(ioManagerSimulationEnabled)
	{
		retError = IOManagerIOSetSimAIChan(setSimAIChanPtr->ioChan, setSimAIChanPtr->enableSimulation);
	}

	reply.ioChan   = setSimAIChanPtr->ioChan;
	reply.status = REPLY_STATUS_OKAY;

	if (retError != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
			                MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_AI_CHAN_R,
							sizeof(ModuleSimAIChanRspStr),
							(lu_uint8_t *)&reply
						   );

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerSetSimulationDIChan(ModuleSimDIChanIdStr *setSimDIChanPtr)
{
	ModuleSimDIChanRspStr  reply;
	SB_ERROR 	           retError;

	retError = SB_ERROR_INITIALIZED;

	if(ioManagerSimulationEnabled)
	{
		retError = IOManagerIOSetSimDIChan(setSimDIChanPtr->ioChan, setSimDIChanPtr->enableSimulation);
	}

	reply.ioChan   = setSimDIChanPtr->ioChan;
	reply.status = REPLY_STATUS_OKAY;

	if (retError != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
			                MODULE_MSG_ID_IOMAN_CMD_DIAG_SIM_DI_CHAN_R,
							sizeof(ModuleSimDIChanRspStr),
							(lu_uint8_t *)&reply
						   );

	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerConfigIoChanAI(ModuleCfgIoChanAIStr *cfgChanAI)
{
	SB_ERROR 	         retError;
	lu_uint32_t			 ioID;
	ModReplyStr			 retRsp;

	retError = SB_ERROR_PARAM;

	retRsp.channel = cfgChanAI->channel;
	retRsp.status  = REPLY_STATUS_PARAM_ERROR;

	if (cfgChanAI->channel < IO_CHAN_AI_MAP_SIZE)
	{
		ioID = ioChanAIMap[cfgChanAI->channel].ioID;

		if (ioID < IO_ID_NA)
		{
			/* Should use an API function for this !!! */
			ioChanAIMap[cfgChanAI->channel].eventRateMs       = cfgChanAI->eventMs;
			ioChanAIMap[cfgChanAI->channel].eventRateCountMs  = 0;
			ioChanAIMap[cfgChanAI->channel].enable            = cfgChanAI->enable;
			ioChanAIMap[cfgChanAI->channel].eventEnable       = cfgChanAI->eventEnable;
			if (cfgChanAI->eventEnable)
			{
				ioChanAIMap[cfgChanAI->channel].enable        = cfgChanAI->eventEnable;

				/* Force first event to be generated */
				boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
			}
			retRsp.status = REPLY_STATUS_OKAY;
		}

		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
								MODULE_MSG_ID_IOMAN_CFG_IOCHAN_AI_R,
								sizeof(ModReplyStr),
								(lu_uint8_t *)&retRsp
							   );
	}
	return retError;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
SB_ERROR IOManagerConfigIoChanDI(ModuleCfgIoChanDIStr *cfgChanDI)
{
	SB_ERROR 	         retError;
	lu_uint32_t			 ioID;
	ModReplyStr			 retRsp;

	retError = SB_ERROR_PARAM;

	retRsp.channel = cfgChanDI->channel;
	retRsp.status  = REPLY_STATUS_PARAM_ERROR;

	if (cfgChanDI->channel < IO_CHAN_DI_MAP_SIZE)
	{
		ioID = ioChanDIMap[cfgChanDI->channel].ioID;

		if (ioID < IO_ID_NA)
		{
			/* Should use an API function for this !!! */
			ioChanDIMap[cfgChanDI->channel].db.high2LowMs       = cfgChanDI->dbHigh2LowMs;
			ioChanDIMap[cfgChanDI->channel].db.low2HighMs       = cfgChanDI->dbLow2HighMs;
			ioChanDIMap[cfgChanDI->channel].eventEnable         = cfgChanDI->eventEnable ? 1 : 0;
			ioChanDIMap[cfgChanDI->channel].enable              = cfgChanDI->enable ? 1 : 0;
			ioChanDIMap[cfgChanDI->channel].eventEnable         = cfgChanDI->eventEnable ? 1 : 0;
			if (cfgChanDI->eventEnable)
			{
				/* If eventing is enabled then the channel must also be enabled */
				ioChanDIMap[cfgChanDI->channel].enable          = cfgChanDI->eventEnable ? 1 : 0;

				/* Force first event to be generated */
				boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
			}
			retRsp.status = REPLY_STATUS_OKAY;
		}

		/* Allow invert for external equipment only! */
		if (ioID < IO_ID_NA)
		{
			if (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_EXT_EQUIP)
			{
				ioChanDIMap[cfgChanDI->channel].extEquipInvert  = cfgChanDI->extEquipInvert ? 1 : 0;
			}
		}

		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_IOMAN,
								MODULE_MSG_ID_IOMAN_CFG_IOCHAN_DI_R,
								sizeof(ModReplyStr),
								(lu_uint8_t *)&retRsp
							   );
	}
	return retError;
}
/*
 *********************** End of file ******************************************
 */
