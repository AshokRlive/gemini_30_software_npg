/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager ADE78xx - advanced ADC Init module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/03/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <math.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "stdlib.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_gpio.h"
#include "systemTime.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerExtADE78xxInit.h"
//#include "IOManagerExtADE78xxConfig.h"

#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"
#include "ProcessorSPI.h"

#include "ADE78xx.h"
#include "ADE78xxConfig.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/* HCLK: 0 (8 Mhz)
 * HSIZE: 1 (32 bit registers in 8-bit packages)
 * HGAP: 0 (no gap)
 * HXFER: 01 (only instantaneous values)
 * HSAPOL: 0 (SS active low)
 * Reserved: 00
 */
#define ADE78XX_HSDC_CONFIG_VALUE  0x0A
#define ADE78XX_IGAIN_VALUE        0xF881400
#define ADE78XX_VGAIN 			   -8348885

/* ADC reset timeout in ms */
#define ADE78XX_RESET_TIMEOUT   (100)

#define I2C_LOCK (1 << 1)

#define ADE78XX_HSDC_NUM_SAMPLES   4

/* \brief Convert a 32-bit value from big endian to little endian */
#define BIG_2_LITTLE_ENDIAN(big, little) little = __builtin_bswap32(big)

#define BL_ISR_TOGGLE_PORT							2
#define BL_ISR_TOGGLE_TRACE_0_PIN					(1 << 5)
#define BL_ISR_TOGGLE_TRACE_1_PIN					(1 << 4)

#define COEFFICIENT_50HZ	0x00007E6F
#define COEFFICIENT_100HZ	0x000079BC
//#define COEFFICIENT_50HZ	0x00003F36
//#define COEFFICIENT_100HZ	0x00003FCE
/*Coefficient values are divided by 2 in order to be represented in Q15 format.
 * This is required for the correct operation of the GetFrequencyBinMagnitude function*/
//#define COEFFICIENT_50HZ	0x205D55
//#define COEFFICIENT_100HZ	0x1F29B4

//#define COEFFICIENT_50HZ	0x1E2450
//#define COEFFICIENT_100HZ	0x1D0621

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*! SPI DMA buffer index */
typedef enum
{
    DMA_BUF_IDX_0 = 0,
    DMA_BUF_IDX_1    ,

    DMA_BUF_IDX_LAST
}DMA_BUF_IDX;

typedef enum
{
	HARMONIC_1ST = 0,
	HARMONIC_2ND	,

	HARMONIC_LAST
}HARMONIC;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void GetADE78xxIOIDParams(void);
SB_ERROR InitialiseADE78xx(void);
SB_ERROR ConfigureCheckI2CADE78xx(lu_uint8_t i2cBus);
SB_ERROR ConfigureCheckHSDCADE78xx(lu_uint8_t sspBus);
SB_ERROR ConfigureCheckSPIADE78xx(lu_uint8_t spiBus);
void IOManagerExtADE78xxDMAIntHandler(void);

void PeakSampleDetector(lu_int32_t *dataBuffer, lu_int32_t *peaksBuffer, lu_int32_t *peakValuePtr, lu_uint16_t *peaksCount);
void GetFreqeuncyBinMagnitude(HARMONIC harmonicNum, PHASE_PEAK phase, lu_uint32_t samplesPerCycle,
								lu_int64_t coefficient, lu_int32_t value, lu_int32_t *magnitudePtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

ADE78xxIoIDStr ade78xxIOIDMap[ADE78XX_CH_LAST];

/* HSDC DMA buffers. The buffers are located in the secondary RAM to not
 * interfere with the main processor memory
 */

lu_int32_t hsdcDMABufferA[DMA_BUF_IDX_LAST][ADE78XX_HSDC_NUM_SAMPLES][ADE78XX_HSDC_CH_LAST] __attribute__((section("ahbsram0"))) __attribute__ ((aligned (4)));

lu_int32_t hsdcDMABufferB[DMA_BUF_IDX_LAST][ADE78XX_HSDC_NUM_SAMPLES][ADE78XX_HSDC_CH_LAST] __attribute__((section("ahbsram1"))) __attribute__ ((aligned (4)));

/* DMA controller linked list. The two buffers point to each
 * other in a "ping-pong" like configuration
 */

GPDMA_LLI_Type gpdmaDMAlliA[DMA_BUF_IDX_LAST] =
{
    // Source Addr  Destination address                          Next LLI address                        GPDMA Control
    {0x40088008  , (lu_uint32_t)(hsdcDMABufferA[DMA_BUF_IDX_0]), (lu_uint32_t)(&gpdmaDMAlliA[DMA_BUF_IDX_1]), 0x88009070},
    {0x40088008  , (lu_uint32_t)(hsdcDMABufferA[DMA_BUF_IDX_1]), (lu_uint32_t)(&gpdmaDMAlliA[DMA_BUF_IDX_0]), 0x88009070}
};

GPDMA_LLI_Type gpdmaDMAlliB[DMA_BUF_IDX_LAST] =
{
    /* Source Addr  Destination address                          Next LLI address                        GPDMA Control */
    {0x40030008  , (lu_uint32_t)(hsdcDMABufferB[DMA_BUF_IDX_0]), (lu_uint32_t)(&gpdmaDMAlliB[DMA_BUF_IDX_1]), 0x88009070},
    {0x40030008  , (lu_uint32_t)(hsdcDMABufferB[DMA_BUF_IDX_1]), (lu_uint32_t)(&gpdmaDMAlliB[DMA_BUF_IDX_0]), 0x88009070}
};


/* Phase currents ioID Map */
static lu_uint32_t fpiCurrentChanMap[ADE78XX_CH_LAST][PHASE_PEAK_LAST];

lu_int32_t phaseDataBuffer[ADE78XX_CH_LAST][ADE78XX_HSDC_NUM_SAMPLES][PHASE_PEAK_LAST];

static lu_int32_t phaseSum[PHASE_PEAK_LAST];
static lu_int32_t maxValue[ADE78XX_CH_LAST][PHASE_PEAK_LAST];

static ADE78xxHSDCDecodeFunc ade78xxHSDCDecodeFunc = NULL;

static lu_int32_t harmonicMagnitude[HARMONIC_LAST][PHASE_PEAK_LAST][40];

//CircularBufferStr circularBfr;
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR __attribute__((optimize("O0"))) IOManagerExtAD78xxIOInit(void)
{
    SB_ERROR	 retError;
    lu_uint32_t	 ioID;
    lu_uint32_t  i;
    lu_uint32_t  y;
    lu_bool_t    foundAde78xx = LU_FALSE;

    retError  = SB_ERROR_NONE;

    for (i=0; i < PHASE_PEAK_LAST; i++)
    {
    	phaseSum[i] = 0;;
    }

    /* Initialize circular buffer */
//	for (i = 0; i < ADE78XX_CH_LAST; i++)
//	{
//		for (y = 0; y < PHASE_PEAK_LAST; y++)
//		{
			//CircularBufferInit(&circularBfr, 2048);
//		}
//	}

    for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
    {
    	switch(BoardIOMap[ioID].ioClass)
    	{
			case IO_CLASS_GPIO_PERIPH:
				switch(BoardIOMap[ioID].ioDev)
				{
					case IO_DEV_I2C_ADE78XX:
					case IO_DEV_HSDC_ADE78XX:
					case IO_DEV_SPI_ADE78XX:
						foundAde78xx = LU_TRUE;
						break;

					default:
						break;
				}
				break;

			default :
				break;
    	}
    }

    if (foundAde78xx == LU_TRUE)
    {
		/* Configure I2C pins & peripheral */
		ConfigureCheckI2CADE78xx(IO_BUS_I2C_0);
		ConfigureCheckI2CADE78xx(IO_BUS_I2C_1);
		ConfigureCheckI2CADE78xx(IO_BUS_I2C_2);

		GetADE78xxIOIDParams();

		/* Now configure ADE78xx and initialise the state */
		retError = InitialiseADE78xx();

		/* Configure HSDC SPI pins & peripheral */
		ConfigureCheckHSDCADE78xx(IO_BUS_SPI_SSPI_0);
		ConfigureCheckHSDCADE78xx(IO_BUS_SPI_SSPI_1);

		//IOManagerADE78xxConfigHSDCDMA(ade78xxIOIDMap[ADE78XX_CH_A].spiBus);
		//IOManagerADE78xxConfigHSDCDMA(ade78xxIOIDMap[ADE78XX_CH_B].spiBus);

		/* Configure SPI pins & peripheral */
		//ConfigureCheckSPIADE78xx(IO_BUS_SPI_SSPI_0);
		//ConfigureCheckSPIADE78xx(IO_BUS_SPI_SSPI_1);
    }
    return retError;
}

SB_ERROR IOManagerADE78xxHSDCDecodeRegisterCallBack(ADE78xxHSDCDecodeFunc callBackFunc)
{
	SB_ERROR retVal = SB_ERROR_PARAM;

	if (callBackFunc != NULL)
	{
		retVal = SB_ERROR_NONE;
		ade78xxHSDCDecodeFunc = callBackFunc;
	}

	return retVal;
}

SB_ERROR IOManagerExtAD78xxI2CReadReg(lu_uint8_t i2cBus, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t *valuePtr)
{
	SB_ERROR	        	retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
	lu_uint8_t				txData[4];
	lu_uint8_t				rxData[4];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Register Addr MSB */
		 txData[0]						= (regAddr >> 8);
		 /* Register Addr LSB */
		 txData[1]						= (regAddr & 0xff);

		 txferCfg.sl_addr7bit     	    = ADE78XX_I2C_ADDR;
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 2;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= size;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }

		 if(size == 1)
		 {
			 *valuePtr = rxData[0];
		 }

		 if(size == 2)
		 {
			 *valuePtr  = rxData[1];
			 *valuePtr |= (rxData[0] << 8);
		 }

		 if(size == 4)
		 {
			 *valuePtr  = rxData[3];
			 *valuePtr |= (rxData[2] << 8);
			 *valuePtr |= (rxData[1] << 16);
			 *valuePtr |= (rxData[0] << 24);
		 }
	 }

	return retError;
}

SB_ERROR IOManagerExtAD78xxI2CWriteReg(lu_uint8_t i2cBus, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t value)
{
	SB_ERROR	        	retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
	lu_uint8_t				txData[8];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	switch (size)
	{
		case 1:
			txData[2] = (value & 0xff);
			break;

		case 2:
			txData[2] = ((value >> 8) & 0xff);
			txData[3] = (value & 0xff);
			break;

		case 4:
			txData[2] = ((value >> 24) & 0xff);
			txData[3] = ((value >> 16) & 0xff);
			txData[4] = ((value >>  8) & 0xff);
			txData[5] = (value & 0xff);
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Register Addr MS */
		txData[0]						= (regAddr >> 8);
		/* Register Addr LSB */
		txData[1]						= (regAddr & 0xff);

		txferCfg.sl_addr7bit     	    = ADE78XX_I2C_ADDR;
		txferCfg.tx_data		  		= txData;
		txferCfg.tx_length       		= 2 + size;
		txferCfg.rx_data		 	 	= NULL;
		txferCfg.rx_length       		= 0;
		txferCfg.retransmissions_max 	= 2;

		if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		{
			retError = SB_ERROR_I2C_FAIL;
		}
	}

	return retError;
}

SB_ERROR IOManagerExtAD78xxSPIReadReg(lu_uint8_t spiBus, lu_uint32_t csIoID, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t *valuePtr)
{
	lu_uint8_t				txBuf[4];
	lu_uint8_t				rxBuf[7];
	lu_uint32_t				pinValue;
	lu_uint32_t				value = 0;
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	txBuf[0] = 0x01;
	txBuf[1] = (regAddr >> 8);
	txBuf[2] = (regAddr & 0xff);
	txBuf[3] = 0;

	xferConfig.tx_data = (lu_uint8_t*)&txBuf;
	xferConfig.rx_data = (lu_uint8_t*)&rxBuf;
	xferConfig.length  = size + 3;

	switch (spiBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	if(size == sizeof(lu_uint8_t))
	{
		value  = rxBuf[3];
	}

	if(size == sizeof(lu_uint16_t))
	{
		value  = rxBuf[4];
		value |= (rxBuf[3] << 8);
	}

	if(size == sizeof(lu_uint32_t))
 	{
		value  =  rxBuf[6];
		value |= (rxBuf[5] << 8);
		value |= (rxBuf[4] << 16);
		value |= (rxBuf[3] << 24);
	}

	/* Return the read value */
	*valuePtr = value;

	return SB_ERROR_NONE;
}

SB_ERROR IOManagerExtAD78xxSPIWriteReg(lu_uint8_t sspBus, lu_uint32_t csIoID, lu_uint16_t regAddr, lu_uint8_t size, lu_uint32_t value)
{
  	lu_uint8_t				txBuf[7];
  	lu_uint8_t				rxBuf[4];
  	lu_uint32_t				xferLen;
  	SSP_DATA_SETUP_Type		xferConfig;
  	LPC_SSP_TypeDef         *sspTyp;
  	lu_int32_t				pinValue;
  	SB_ERROR				retError;

  	retError = SB_ERROR_NONE;

  	txBuf[0] = 0x00;
   	txBuf[1] = ( regAddr  >>  8  );
   	txBuf[2] = ( regAddr  & 0xFF );

   	if(size == 1)
	{
		txBuf[3] = (value) & 0xFF;
	}

	if(size == 2)
	{
		txBuf[3] = (value >> 8) & 0xFF;
		txBuf[4] = value & 0xFF;
	}

	if(size == 4)
	{
		txBuf[3] = (value >> 24) & 0xFF;
		txBuf[4] = (value >> 16) & 0xFF;
		txBuf[5] = (value >> 8 ) & 0xFF;
		txBuf[6] = (value      ) & 0xFF;
	}

	xferConfig.tx_data  = (lu_uint8_t*)&txBuf;
  	xferConfig.rx_data  = (lu_uint8_t*)&rxBuf;
  	xferConfig.length   = size + 3;

  	switch (sspBus)
  	{
  	case 0:
  		sspTyp = LPC_SSP0;
  		break;

  	case 1:
  		sspTyp = LPC_SSP1;
  		break;

  	default:
  		retError = SB_ERROR_PARAM;
  		break;
  	}

	/* Activate CS - low */
	pinValue = 0;
	IOManagerSetValue(csIoID, pinValue);

  	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

  	/* deActivate CS - high */
	pinValue = 1;
	IOManagerSetValue(csIoID, pinValue);

  	if (xferLen != xferConfig.length)
  	{
  		retError =  SB_ERROR_SPI_FAIL;
  	}

  	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


void IOManagerExtADE78xxDMAIntHandler(void)
{
	lu_uint32_t        smpIdx;
	lu_uint32_t        phaseChan;

	 static volatile lu_uint8_t  dmaBufIdxA = DMA_BUF_IDX_0;
	 static volatile lu_uint8_t  dmaBufIdxB = DMA_BUF_IDX_0;

	 static volatile lu_uint32_t adeSampleTimeA;
	 static volatile lu_uint32_t adeSampleTimeB;

	/* check GPDMA interrupt on channel 0. Not necessary just one channel used
	 * Check counter terminal status
	 */
	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
	{
		GPIO_SetValue(BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_0_PIN);

		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);

		adeSampleTimeA++;

		/* HSDC data are in big-endian format.
		 * Covert them in little-endian format
		 */
		for(smpIdx = 0; smpIdx < ADE78XX_HSDC_NUM_SAMPLES; smpIdx++)
		{
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferA[dmaBufIdxA][smpIdx][ADE78XX_HSDC_CH_IAWV], phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_A]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferA[dmaBufIdxA][smpIdx][ADE78XX_HSDC_CH_IBWV], phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_B]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferA[dmaBufIdxA][smpIdx][ADE78XX_HSDC_CH_ICWV], phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_C]);

			phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_N] = phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_A] +
																	phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_B] +
																	phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_C];

//			CircularBufferWrite(&circularBfr, phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_A]);
			for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
			{
				phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan] = abs(phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan]);
				if (phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan] > maxValue[ADE78XX_CH_A][phaseChan])
				{
					maxValue[ADE78XX_CH_A][phaseChan] = phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan];
				}
			}
		}

		/* Increment buffer */
		dmaBufIdxA++;
		dmaBufIdxA &= (DMA_BUF_IDX_LAST - 1);

		for (phaseChan = 0; phaseChan < PHASE_PEAK_LAST; phaseChan++)
		{
			ade78xxHSDCDecodeFunc(ADE78XX_CH_A,
					              phaseChan,
					              maxValue[ADE78XX_CH_A][phaseChan],
							      adeSampleTimeA);

			maxValue[ADE78XX_CH_A][phaseChan] = 0;
			phaseSum[phaseChan] = 0;
		}

		GPIO_ClearValue(BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_0_PIN);
	}

	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(1))
	{
		GPIO_SetValue(BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN);

		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(1);

		adeSampleTimeB++;

		/* HSDC data are in big-endian format.
		 * Covert them in little-endian format
		 */
		for(smpIdx = 0; smpIdx < ADE78XX_HSDC_NUM_SAMPLES; smpIdx++)
		{
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferB[dmaBufIdxB][smpIdx][ADE78XX_HSDC_CH_IAWV], phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_A]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferB[dmaBufIdxB][smpIdx][ADE78XX_HSDC_CH_IBWV], phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_B]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferB[dmaBufIdxB][smpIdx][ADE78XX_HSDC_CH_ICWV], phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_C]);

			phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_N] = phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_A] +
																	phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_B] +
																	phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_C];

			for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
			{
				phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan] = abs(phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan]);
				if (phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan] > maxValue[ADE78XX_CH_B][phaseChan])
				{
					maxValue[ADE78XX_CH_B][phaseChan] = phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan];
				}
			}
		}

		/* Increment buffer */
		dmaBufIdxB++;
		dmaBufIdxB &= (DMA_BUF_IDX_LAST - 1);

		for (phaseChan = 0; phaseChan < PHASE_PEAK_LAST; phaseChan++)
		{
			ade78xxHSDCDecodeFunc(ADE78XX_CH_B,
								  phaseChan,
								  maxValue[ADE78XX_CH_B][phaseChan],
								  adeSampleTimeB);

			maxValue[ADE78XX_CH_B][phaseChan] = 0;
			phaseSum[phaseChan] = 0;
		}

		GPIO_ClearValue(BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN);
	}

	if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
	{
		 /* Clear error counter Interrupt pending */
		LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
	}

	if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(1))
	{
		 /* Clear error counter Interrupt pending */
		LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(1);
	}
}

void PeakSampleDetector(lu_int32_t *dataBuffer, lu_int32_t * peaksBuffer, lu_int32_t *peakValuePtr, lu_uint16_t *peaksCount)
{
	volatile lu_uint16_t i, j;

	peaksBuffer[*peaksCount] = 0;

	for(i = 0; i < ADE78XX_HSDC_NUM_SAMPLES; i++)
	{
		peaksBuffer[*peaksCount] += dataBuffer[i];
	}

	peaksBuffer[*peaksCount] = peaksBuffer[*peaksCount] >> 3;

	*peaksCount = *peaksCount + 1;

	if(*peaksCount == ADE78XX_HSDC_NUM_SAMPLES)
	{
		*peakValuePtr = 0;
		for(j = 0; j < ADE78XX_HSDC_NUM_SAMPLES; j++)
		{
			if(peaksBuffer[j] > *peakValuePtr)
			{
				*peakValuePtr  = peaksBuffer[j];
			}
		}
		GPIO_SetValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN);
		*peaksCount = 0;
		GPIO_ClearValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN);
	}
}

void CircularBufferInit(CircularBufferStr *cb, lu_uint32_t size)
{
	lu_uint16_t i=0;

	for (i=0; i<size; i++)
	{
		cb->buffer[i] = 0;
	}
	cb->size = size;
	cb->write = 0;
	cb->read = 0;
}

lu_uint32_t CircularBufferDataLength(CircularBufferStr *cb)
{
    return ((cb->write - cb->read) & ((cb->size)-1));
}

SB_ERROR CircularBufferWrite(CircularBufferStr *cb, lu_uint32_t data)
{
	SB_ERROR retError;

//    if (CircularBufferDataLength(cb) == ((cb->size)-1))
//    {
    	// Buffer full
//        return retError = SB_ERROR_PARAM;
//    }

    cb->buffer[cb->write] = data;
    cb->write = ((cb->write)+1) & ((cb->size)-1);
    return retError = SB_ERROR_NONE;
}


SB_ERROR CircularBufferRead(CircularBufferStr *cb, lu_uint32_t *data)
{
	SB_ERROR retError;

    if (CircularBufferDataLength(cb) == 0)
    {
    	// Buffer empty
        return SB_ERROR_PARAM;
    }

    *data = cb->buffer[cb->read];
    cb->read = (cb->read+1) & (cb->size-1);
    return retError = SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \ Goertzel algorithm
 *
 *   Provides the SQRT(magnitude) (real only) of a single frequency bin
 *
 *   \param 	samplesPerCycle		Number of samples per cycle (N)
 *	 			coefficient			2 * cos(2 * pi * (K / N) ), where K is the
 *	 								frequency bin (ie. 1 for 1st, 2 for 2nd, etc...)
 *	 			value				measured value
 *
 *   \return	magnitudePtr
 *
 ******************************************************************************
 */
void GetFreqeuncyBinMagnitude(HARMONIC harmonicNum, PHASE_PEAK phase, lu_uint32_t samplesPerCycle, lu_int64_t coefficient, lu_int32_t value, lu_int32_t *magnitudePtr)
{
	static lu_int64_t s_prev[HARMONIC_LAST][PHASE_PEAK_LAST];
	static lu_int64_t s_prev2[HARMONIC_LAST][PHASE_PEAK_LAST];
	static lu_uint32_t iteration[HARMONIC_LAST][PHASE_PEAK_LAST];

	lu_int64_t s = 0;
	lu_int64_t tmp = 0;

	s = ((lu_int64_t) value>>3) + ((coefficient * s_prev[harmonicNum][phase])>>14) - s_prev2[harmonicNum][phase];
	s_prev2[harmonicNum][phase] = s_prev[harmonicNum][phase];
	s_prev[harmonicNum][phase] = s;
    iteration[harmonicNum][phase]++;

    if(iteration[harmonicNum][phase] == samplesPerCycle)
    {
    	tmp = (s_prev[harmonicNum][phase] * s_prev[harmonicNum][phase]) +
    			(s_prev2[harmonicNum][phase] * s_prev2[harmonicNum][phase]) -
    			(((coefficient * s_prev[harmonicNum][phase])>>14) * s_prev2[harmonicNum][phase]);
    	tmp = tmp >> 3;
    	//tmp = (value * tmp) >> 15;
    	*magnitudePtr = (lu_int32_t) tmp;
    	s_prev[harmonicNum][phase] = 0;
    	s_prev2[harmonicNum][phase] = 0;
    	iteration[harmonicNum][phase] = 0;
    }
/*				for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
    			{
    				phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan] = abs(phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan]);
    				if (phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan] > maxValue[ADE78XX_CH_B][phaseChan])
    				{
    					maxValue[ADE78XX_CH_B][phaseChan] = phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan];
    				}
    			}

			phaseSum[PHASE_PEAK_A] += phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_A];
			phaseSum[PHASE_PEAK_B] += phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_B];
			phaseSum[PHASE_PEAK_C] += phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_C];
			phaseSum[PHASE_PEAK_N] += phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_N];

			phaseSum[phaseChan] = abs(phaseSum[phaseChan] >> 2);
	    	if ((lu_uint32_t)phaseSum[phaseChan] > maxValue[ADE78XX_CH_A][phaseChan])
	    	{
	    		maxValue[ADE78XX_CH_A][phaseChan] = phaseSum[phaseChan];
	    	}
	    	phaseSum[phaseChan] = 0;

		GetFreqeuncyBinMagnitude(HARMONIC_1ST, PHASE_PEAK_A, 40, COEFFICIENT_50HZ, (phaseSum[PHASE_PEAK_A] >> 2), &harmonicMagnitude[HARMONIC_1ST][PHASE_PEAK_A][indexHarmonic1]);
		GetFreqeuncyBinMagnitude(HARMONIC_2ND, PHASE_PEAK_A, 40, COEFFICIENT_100HZ, (phaseSum[PHASE_PEAK_A] >> 2), &harmonicMagnitude[HARMONIC_2ND][PHASE_PEAK_A][indexHarmonic1]);

		indexHarmonic++;
		if (indexHarmonic == 40)
			{
				indexHarmonic=0;
				indexHarmonic1++;
				if (indexHarmonic1==40) indexHarmonic1 = 0;
			}
*/

}

void GetADE78xxIOIDParams(void)
{
	ADE78XX_CH   adcChan;
	lu_uint32_t	 ioID;

	for (adcChan = ADE78XX_CH_A; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
		ade78xxIOIDMap[adcChan].adcCfg1 			= IO_ID_LAST;
		ade78xxIOIDMap[adcChan].adcCfg2 			= BOARD_IO_MAX_IDX;
		ade78xxIOIDMap[adcChan].adcIrq0 			= BOARD_IO_MAX_IDX;
		ade78xxIOIDMap[adcChan].adcIrq1 			= BOARD_IO_MAX_IDX;
		ade78xxIOIDMap[adcChan].adcPm0  			= BOARD_IO_MAX_IDX;
		ade78xxIOIDMap[adcChan].adcPm1  			= BOARD_IO_MAX_IDX;
		ade78xxIOIDMap[adcChan].adcManualReset 		= BOARD_IO_MAX_IDX;
		ade78xxIOIDMap[adcChan].adcCs 				= BOARD_IO_MAX_IDX;
	}

	/* Find ioID's for pins required to control ADE78xx */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		/* Is GPIO */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_INPUT:
			adcChan = (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CH_A) ?
			        				ADE78XX_CH_A : ADE78XX_CH_B;

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CFG1)
			{
				ade78xxIOIDMap[adcChan].adcCfg1 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CFG2)
			{
				ade78xxIOIDMap[adcChan].adcCfg2 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_IRQ0)
			{
				ade78xxIOIDMap[adcChan].adcIrq0 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_IRQ1)
			{
				ade78xxIOIDMap[adcChan].adcIrq1 = ioID;
			}
			break;

		case IO_CLASS_DIGITAL_OUTPUT:
			adcChan = (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CH_A) ?
						        ADE78XX_CH_A : ADE78XX_CH_B;

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_RESET)
			{
				ade78xxIOIDMap[adcChan].adcManualReset = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_PM0)
			{
				ade78xxIOIDMap[adcChan].adcPm0 = ioID;
			}

			if (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_PM1)
			{
				ade78xxIOIDMap[adcChan].adcPm1 = ioID;
			}
			break;

		case IO_CLASS_ANALOG_INPUT:
			if(BoardIOMap[ioID].ioDev == IO_DEV_HSDC_ADE78XX)
			{
				adcChan = BoardIOMap[ioID].ioAddr.hsdcDE78xx.hsdcChan;

				if(adcChan < ADE78XX_CH_LAST)
				{
					fpiCurrentChanMap[adcChan][BoardIOMap[ioID].ioAddr.hsdcDE78xx.phase] = ioID;
				}
			}
			break;

		default:
			break;
		}
	}
}

/*!
 ******************************************************************************
 *   \brief configure ADE78xx ADC
 *
 *   Detailed description
 *
 *   \param none
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR InitialiseADE78xx(void)
{
	SB_ERROR    retError;
	lu_int32_t  retValS;
	ADE78XX_CH  adcChan;
	lu_uint32_t adcCsIoID;
	lu_uint32_t regVal32bit;
	lu_uint8_t  idx;
	lu_uint8_t  regVal8bit;
	lu_uint16_t regVal16bit;

	/* Setting Direction of TRACE_0 Pin */
	GPIO_SetValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_0_PIN    );

	/* Setting Direction of TRACE_1 Pin */
	GPIO_SetValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN    );

	retError = SB_ERROR_INITIALIZED;

	for (adcChan = ADE78XX_CH_A; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
		if ( ade78xxIOIDMap[adcChan].adcPm0 != BOARD_IO_MAX_IDX ||
			 ade78xxIOIDMap[adcChan].adcPm1 != BOARD_IO_MAX_IDX ||
			 ade78xxIOIDMap[adcChan].adcManualReset != BOARD_IO_MAX_IDX
		   )
		{
			/* At startup the ADC is in Power Mode PSM3 (Sleep Mode).
			 * Set the ADC Power Mode to PSM0(Normal Power Mode)
			 * (PM0 -> 1, PM1 -> 0)
			 */
			IOManagerSetValue(ade78xxIOIDMap[adcChan].adcPm0, 1);
			IOManagerSetValue(ade78xxIOIDMap[adcChan].adcPm1, 0);

			IOManagerSetValue(ade78xxIOIDMap[adcChan].adcManualReset, 1);
			STDelayTime(50);
			/* Release from reset */
			IOManagerSetValue(ade78xxIOIDMap[adcChan].adcManualReset, 0);

			STDelayTime(10);

			retValS = ProcGPIORead( BoardIOMap[ade78xxIOIDMap[adcChan].adcIrq1].ioAddr.gpio.port,
									BoardIOMap[ade78xxIOIDMap[adcChan].adcIrq1].ioAddr.gpio.pin
								  );
			if(retValS == 0)
			{
				retError = SB_ERROR_NONE;
			}

			STDelayTime(10);

			if(retError == SB_ERROR_NONE)
			{
				/* Determine if I2C or SPI mode ? */
				if (ade78xxIOIDMap[adcChan].adcCs != BOARD_IO_MAX_IDX)
				{
					adcCsIoID = ade78xxIOIDMap[adcChan].adcCs;

					/* Setup in SPI mode - toggle CS pin 3 times */

					for (idx = 0; idx <= 4; idx++)
					{
						IOManagerSetValue(ade78xxIOIDMap[adcChan].adcCs, 1);
						STDleayTimeUsec(1000);
						IOManagerSetValue(ade78xxIOIDMap[adcChan].adcCs, 0);
					}

					IOManagerSetValue(ade78xxIOIDMap[adcChan].adcCs, 0);

					STDelayTime(100);

				}
				else
				{

					retError = IOManagerExtAD78xxI2CReadReg( ade78xxIOIDMap[adcChan].i2cBus,
															 ADE78XX_CONFIG2,
															 sizeof(lu_uint8_t),
															 (lu_uint32_t*)&regVal8bit
														   );
					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_CONFIG2,
															  sizeof(lu_uint8_t),
															  regVal8bit | I2C_LOCK
															);

					retError = IOManagerExtAD78xxI2CReadReg( ade78xxIOIDMap[adcChan].i2cBus,
															 ADE78XX_STATUS0,
															 sizeof(lu_uint32_t),
															 &regVal32bit
														   );
					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_STATUS0,
															  sizeof(lu_uint32_t),
															  regVal32bit
															);

					retError = IOManagerExtAD78xxI2CReadReg( ade78xxIOIDMap[adcChan].i2cBus,
															 ADE78XX_STATUS1,
															 sizeof(lu_uint32_t),
															 &regVal32bit
														   );

					/* Enable HSDC Interface */
					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_HSDC_CFG,
															  sizeof(lu_uint8_t),
															  ADE78XX_HSDC_CONFIG_VALUE
															);

					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_CONFIG_A,
															  sizeof(lu_uint8_t),
															  0x01
															);

					retError = IOManagerExtAD78xxI2CReadReg( ade78xxIOIDMap[adcChan].i2cBus,
															 ADE78XX_CONFIG,
															 sizeof(lu_uint16_t),
															 (lu_uint32_t*)&regVal16bit
														   );
					regVal16bit |= ADE78XX_CONFIG_HSDCEN;

					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_CONFIG,
															  sizeof(lu_uint16_t),
															  regVal16bit
															);
					/* Write the last register 2 more times to flush the DSP internal pipeline */
					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_CONFIG,
															  sizeof(lu_uint16_t),
															  regVal16bit
															);

					retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
															  ADE78XX_CONFIG,
															  sizeof(lu_uint16_t),
															  regVal16bit
															);

					STDelayTime(100);
				}
			}
		}
		else
		{
			retError = SB_ERROR_NONE;
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Check for I2C ADE78xx ADC I2C device & configure I2C if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR ConfigureCheckI2CADE78xx(lu_uint8_t i2cBus)
{
	SB_ERROR				retVal;
	lu_uint32_t				ioID;
	ADE78XX_CH				adcChan;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		/* Is Peripheral */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_GPIO_PERIPH:
			switch(BoardIOMap[ioID].ioDev)
			{
			/* Is an I2C Expander device */
			case IO_DEV_I2C_ADE78XX:

				adcChan = (BoardIOMap[ioID].ioAddr.i2cADE78xx.mode & GPIO_PM_ADC_CH_A) ?
										        	ADE78XX_CH_A : ADE78XX_CH_B;

				if (BoardIOMap[ioID].ioAddr.i2cADE78xx.busI2c == i2cBus)
				{
					/* Save details of I2C bus ADE78xx found on */
					ade78xxIOIDMap[adcChan].i2cBus = BoardIOMap[ioID].ioAddr.i2cADE78xx.busI2c;

					// find & configure I2C peripheral pins
					retVal = configureI2CPeripheral(i2cBus);
					ioID = BOARD_IO_MAX_IDX;
				}
				break;

			default:
				break;
			}
			break;

		default:
			break;

		}
	}

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Check for HSDC SPI ADE78xx ADC & configure SPI if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR ConfigureCheckHSDCADE78xx(lu_uint8_t sspBus)
{
	SB_ERROR                retVal;
	lu_uint32_t             ioID;
	ADE78XX_CH          	adcChan;
	SSP_CFG_Type            sspConfig;
	GPDMA_Channel_CFG_Type  GPDMACfg;
	lu_bool_t               foundADE78xx = LU_FALSE;
	static lu_bool_t        initDMA = LU_FALSE;

	retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		if( (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH) &&
			(BoardIOMap[ioID].ioDev   == IO_DEV_HSDC_ADE78XX)   &&
			(BoardIOMap[ioID].ioAddr.spiAdc.busSspi == sspBus)
		  )
		{
			adcChan = (BoardIOMap[ioID].ioAddr.hsdcDE78xx.mode & GPIO_PM_ADC_CH_A) ?
									ADE78XX_CH_A : ADE78XX_CH_B;

			ade78xxIOIDMap[adcChan].spiBus = BoardIOMap[ioID].ioAddr.hsdcDE78xx.hsdcChan;

			/* Init SPI configuration */
			SSP_ConfigStructInit(&sspConfig);

			/* Override default values */
			sspConfig.CPHA        = SSP_CPHA_SECOND;
			sspConfig.CPOL        = SSP_CPOL_LO;
			sspConfig.Databit     = SSP_DATABIT_8;
			sspConfig.Mode        = SSP_SLAVE_MODE;
			sspConfig.FrameFormat = SSP_FRAME_SPI;
			sspConfig.ClockRate   = 8000000;

			/* find & configure SPI peripheral pins */
			retVal = configureSPISspSlPeripheral(sspBus, &sspConfig);

			foundADE78xx = LU_TRUE;

			ioID = BOARD_IO_MAX_IDX;
			break;
		}
	}

	if(retVal == SB_ERROR_NONE && foundADE78xx == LU_TRUE)
	{
		/* Peripheral found and configured correctly.
		 * Initialise DMA engine
		 */

		if (initDMA == LU_FALSE)
		{
			/* Initialise GPDMA controller */
			GPDMA_Init();

			initDMA = LU_TRUE;
		}

		/* DMA Channel (0 highest priority) */
		GPDMACfg.ChannelNum = sspBus;
		/* Source memory - not used */
		GPDMACfg.SrcMemAddr = 0;
		/* Destination memory */
		GPDMACfg.DstMemAddr = (uint32_t) ((adcChan == ADE78XX_CH_A) ?
							  &hsdcDMABufferA[DMA_BUF_IDX_0] : &hsdcDMABufferB[DMA_BUF_IDX_0]);
		/* Transfer size */
		GPDMACfg.TransferSize = sizeof(hsdcDMABufferA[DMA_BUF_IDX_0]);
		/* Transfer width - not used */
		GPDMACfg.TransferWidth = 0;
		/* Transfer type */
		GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
		/* Source connection */
		GPDMACfg.SrcConn = ( (sspBus == IO_BUS_SPI_SSPI_0) ? GPDMA_CONN_SSP0_Rx : GPDMA_CONN_SSP1_Rx );
		/*  Destination connection - not used */
		GPDMACfg.DstConn = 0;
		/*  Link List Item - unused */
		GPDMACfg.DMALLI = (uint32_t)((adcChan == ADE78XX_CH_A) ?
						  &gpdmaDMAlliA[DMA_BUF_IDX_1] : &gpdmaDMAlliB[DMA_BUF_IDX_1]);

		/* Setup channel with given parameter */
		GPDMA_Setup(&GPDMACfg);

		/* Enable Rx DMA */
		SSP_DMACmd( (sspBus == IO_BUS_SPI_SSPI_0) ? LPC_SSP0 : LPC_SSP1,
					 SSP_DMA_RX,
					 ENABLE
				  );

		/* Enable GPDMA channel n */
		GPDMA_ChannelCmd((adcChan == ADE78XX_CH_A) ? 0 : 1, ENABLE);
	}

	return SB_ERROR_NONE;
}
/*!
 ******************************************************************************
 *   \brief Check for SPI ADE78xx ADC & configure SPI if found
 *
 *   Detailed description
 *
 *   \param i2cBus
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR ConfigureCheckSPIADE78xx(lu_uint8_t spiBus)
{
    SB_ERROR                retVal;
    lu_uint32_t             ioID;
    lu_uint32_t             csIoID;
    SSP_CFG_Type            sspConfig;
    ADE78XX_CH              adcChan;

    retVal = SB_ERROR_INITIALIZED;

	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		if( (BoardIOMap[ioID].ioClass == IO_CLASS_GPIO_PERIPH) &&
			(BoardIOMap[ioID].ioDev   == IO_DEV_SPI_ADE78XX)   &&
			(BoardIOMap[ioID].ioAddr.spiAdc.busSspi == spiBus)
		  )
		{
			csIoID 	= BoardIOMap[ioID].ioAddr.spiPeriphADE78xx.csIoID;
			adcChan = (BoardIOMap[csIoID].ioAddr.gpio.pinMode & GPIO_PM_ADC_CH_A) ?
						ADE78XX_CH_A : ADE78XX_CH_B;

			ade78xxIOIDMap[adcChan].spiBus = BoardIOMap[ioID].ioAddr.spiADE78xx.spiChan;
			ade78xxIOIDMap[adcChan].adcCs           = csIoID;

			retVal = SB_ERROR_NONE;
		}
	}

	if(retVal == SB_ERROR_NONE)
	{
		/* Peripheral found and so.
		 * Initialise ...
		 */

		/* Init SPI configuration */
		SSP_ConfigStructInit(&sspConfig);

		/* Override default values */
		sspConfig.CPHA        = SSP_CPHA_FIRST;
		sspConfig.CPOL        = SSP_CPOL_HI;
		sspConfig.Mode        = SSP_MASTER_MODE;
		sspConfig.FrameFormat = SSP_FRAME_SPI;
		sspConfig.ClockRate   = 2500000;

		/* find & configure SPI peripheral pins */
		retVal = configureSPIParamPeripheral(spiBus, &sspConfig);
	}

    return SB_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */
