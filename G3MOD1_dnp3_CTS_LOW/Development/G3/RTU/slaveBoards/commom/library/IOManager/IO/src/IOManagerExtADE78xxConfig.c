/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager ADE78xx - advanced ADC Update module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   02/04/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "stdlib.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "systemTime.h"
#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerExtADE78xxConfig.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"
#include "ProcessorSPI.h"

#include "ADE78xx.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
/*! SPI DMA buffer index */
typedef enum
{
    DMA_BUF_IDX_0 = 0,
    DMA_BUF_IDX_1    ,

    DMA_BUF_IDX_LAST
}DMA_BUF_IDX;

typedef enum
{
	PHASE_PEAK_A = 0,
	PHASE_PEAK_B = 1,
	PHASE_PEAK_C = 2,
	PHASE_PEAK_N = 3,

	PHASE_PEAK_LAST
}PEAK_CURRENTS;

#define HSDC_DATA_SET_COUNT		   8
#define PHASE_CURRENT_BUFFER_SIZE  8

/* \brief Convert a 32-bit value from big endian to little endian */
#define BIG_2_LITTLE_ENDIAN(big, little) little = __builtin_bswap32(big)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void IOManagerExtADE78xxDMAIntHandler(void);
void PeakSampleDetector(lu_int32_t *dataBuffer, lu_int32_t * peaksBuffer, lu_int32_t *peakValuePtr, lu_uint16_t *peaksCount);
void PeakCurrentsToIOManager(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_int32_t fpi_A_peakCurrent[PHASE_PEAK_LAST];
lu_int32_t fpi_B_peakCurrent[PHASE_PEAK_LAST];

lu_int32_t fpi_A_DataBuffer[DMA_BUF_IDX_LAST][HSDC_DATA_SET_COUNT][ADE78XX_HSDC_CH_LAST] __attribute__((section("ahbsram1"))) __attribute__ ((aligned (4)));
lu_int32_t fpi_B_DataBuffer[DMA_BUF_IDX_LAST][HSDC_DATA_SET_COUNT][ADE78XX_HSDC_CH_LAST] __attribute__((section("ahbsram0"))) __attribute__ ((aligned (4)));

/* DMA controller linked list. The two buffers point to each
 * other in a "ping-pong" like configuration
 */
GPDMA_LLI_Type fpi_B_DMAlli[DMA_BUF_IDX_LAST] =
{
    /* Source Addr  Destination address                          Next LLI address                        GPDMA Control */
    {0x40030008  , (lu_uint32_t)(fpi_B_DataBuffer[DMA_BUF_IDX_0]), (lu_uint32_t)(&fpi_B_DMAlli[DMA_BUF_IDX_1]), 0x880090E0},
    {0x40030008  , (lu_uint32_t)(fpi_B_DataBuffer[DMA_BUF_IDX_1]), (lu_uint32_t)(&fpi_B_DMAlli[DMA_BUF_IDX_0]), 0x880090E0}
};

GPDMA_LLI_Type fpi_A_DMAlli[DMA_BUF_IDX_LAST] =
{
    // Source Addr  Destination address                          Next LLI address                        GPDMA Control
    {0x40088008  , (lu_uint32_t)(fpi_A_DataBuffer[DMA_BUF_IDX_0]), (lu_uint32_t)(&fpi_A_DMAlli[DMA_BUF_IDX_1]), 0x880090E0},
    {0x40088008  , (lu_uint32_t)(fpi_A_DataBuffer[DMA_BUF_IDX_1]), (lu_uint32_t)(&fpi_A_DMAlli[DMA_BUF_IDX_0]), 0x880090E0}
};

lu_int32_t fpiA1DataBuffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t fpiA2DataBuffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t fpiA3DataBuffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t fpiASumDataBuffer[PHASE_CURRENT_BUFFER_SIZE];

lu_int32_t fpiB1DataBuffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t fpiB2DataBuffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t fpiB3DataBuffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t fpiBSumDataBuffer[PHASE_CURRENT_BUFFER_SIZE];

lu_int32_t peaksA1Buffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t peaksA2Buffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t peaksA3Buffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t peaksASumBuffer[PHASE_CURRENT_BUFFER_SIZE];

lu_int32_t peaksB1Buffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t peaksB2Buffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t peaksB3Buffer[PHASE_CURRENT_BUFFER_SIZE];
lu_int32_t peaksBSumBuffer[PHASE_CURRENT_BUFFER_SIZE];

lu_uint16_t peaksA1Count = 0, peaksA2Count = 0, peaksA3Count = 0, peaksANCount = 0 ;
lu_uint16_t peaksB1Count = 0, peaksB2Count = 0, peaksB3Count = 0, peaksBNCount = 0 ;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR IOManagerADE78xxConfigHSDCDMA(lu_uint8_t sspBus)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	GPDMA_Channel_CFG_Type  GPDMACfg;
	static lu_bool_t initDMA = LU_FALSE;

	if((sspBus == IO_BUS_SPI_SSPI_0) || (sspBus == IO_BUS_SPI_SSPI_1) )
	{
		/* Peripheral found and configured correctly.
		 * Initialise DMA engine
		 */
		if (initDMA == LU_FALSE)
		{
			/* Initialise GPDMA controller */
			GPDMA_Init();

			initDMA = LU_TRUE;
		}

		/* DMA Channel (0 highest priority) */
		GPDMACfg.ChannelNum = (sspBus == IO_BUS_SPI_SSPI_0) ? IO_BUS_SPI_SSPI_0 : IO_BUS_SPI_SSPI_1;
		/* Source memory - not used */
		GPDMACfg.SrcMemAddr = 0;
		/* Destination memory */
		GPDMACfg.DstMemAddr = (uint32_t) ((sspBus == IO_BUS_SPI_SSPI_0) ?
							  &fpi_A_DataBuffer[DMA_BUF_IDX_0] : &fpi_B_DataBuffer[DMA_BUF_IDX_0]);
		/* Transfer size */
		GPDMACfg.TransferSize = sizeof(fpi_A_DataBuffer[DMA_BUF_IDX_0]);
		/* Transfer width - not used */
		GPDMACfg.TransferWidth = 0;
		/* Transfer type */
		GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
		/* Source connection */
		GPDMACfg.SrcConn = ( (sspBus == IO_BUS_SPI_SSPI_0) ? GPDMA_CONN_SSP0_Rx : GPDMA_CONN_SSP1_Rx );
		/*  Destination connection - not used */
		GPDMACfg.DstConn = 0;
		/*  Linker List Item - unused */
		GPDMACfg.DMALLI = (uint32_t)((sspBus == IO_BUS_SPI_SSPI_0) ?
						  &fpi_A_DMAlli[DMA_BUF_IDX_1] : &fpi_B_DMAlli[DMA_BUF_IDX_1]);

		/* Setup channel with given parameter */
		GPDMA_Setup(&GPDMACfg);

		/* Enable Rx DMA */
		SSP_DMACmd( (sspBus == IO_BUS_SPI_SSPI_0) ? LPC_SSP0 : LPC_SSP1,
					 SSP_DMA_RX,
					 ENABLE
				  );

		/* Enable GPDMA channel n */
		GPDMA_ChannelCmd((sspBus == IO_BUS_SPI_SSPI_0) ? 0 : 1, ENABLE);

		retError = SB_ERROR_NONE;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
void IOManagerExtADE78xxDMAIntHandler(void)
{
    static lu_uint8_t fpiAidxCounter = DMA_BUF_IDX_0;
    static lu_uint8_t fpiBidxCounter = DMA_BUF_IDX_0;

    static lu_uint16_t i;

    /* check GPDMA interrupt on channel 0. Not necessary just one channel used
    * Check counter terminal status
    */
    if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
    {
        /* HSDC data are in big-endian format.
         * Covert them in little-endian format
         */
        for(i = 0; i < HSDC_DATA_SET_COUNT; i++)
        {
        	BIG_2_LITTLE_ENDIAN(fpi_A_DataBuffer[fpiAidxCounter][i][ADE78XX_HSDC_CH_IAWV], fpiA1DataBuffer[i]);
        	BIG_2_LITTLE_ENDIAN(fpi_A_DataBuffer[fpiAidxCounter][i][ADE78XX_HSDC_CH_IBWV], fpiA2DataBuffer[i]);
        	BIG_2_LITTLE_ENDIAN(fpi_A_DataBuffer[fpiAidxCounter][i][ADE78XX_HSDC_CH_ICWV], fpiA3DataBuffer[i]);

        	fpiASumDataBuffer[i] = fpiA1DataBuffer[i] +
        						   fpiA2DataBuffer[i] +
        						   fpiA3DataBuffer[i];

        	fpiA1DataBuffer[i]   = abs(fpiA1DataBuffer[i]);
        	fpiA2DataBuffer[i]   = abs(fpiA2DataBuffer[i]);
        	fpiA3DataBuffer[i]   = abs(fpiA3DataBuffer[i]);
        	fpiASumDataBuffer[i] = abs(fpiASumDataBuffer[i]);

        	fpiAidxCounter = ~fpiAidxCounter & 0x01;
        }

       	PeakSampleDetector(&fpiA1DataBuffer[0],   &peaksA1Buffer[0]   ,&fpi_A_peakCurrent[PHASE_PEAK_A], &peaksA1Count);
       	PeakSampleDetector(&fpiA2DataBuffer[0],   &peaksA2Buffer[0]   ,&fpi_A_peakCurrent[PHASE_PEAK_B], &peaksA2Count);
       	PeakSampleDetector(&fpiA3DataBuffer[0],   &peaksA3Buffer[0]   ,&fpi_A_peakCurrent[PHASE_PEAK_C], &peaksA3Count);
       	PeakSampleDetector(&fpiASumDataBuffer[0], &peaksASumBuffer[0] ,&fpi_A_peakCurrent[PHASE_PEAK_N], &peaksANCount);

        /* Clear terminate counter Interrupt pending */
        LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);
    }

    if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(1))
    {
    	/* HSDC data are in big-endian format.
		 * Covert them in little-endian format
		 */
		for(i = 0; i < HSDC_DATA_SET_COUNT; i++)
		{
			BIG_2_LITTLE_ENDIAN(fpi_B_DataBuffer[fpiAidxCounter][i][ADE78XX_HSDC_CH_IAWV], fpiB1DataBuffer[i]);
			BIG_2_LITTLE_ENDIAN(fpi_B_DataBuffer[fpiAidxCounter][i][ADE78XX_HSDC_CH_IBWV], fpiB2DataBuffer[i]);
			BIG_2_LITTLE_ENDIAN(fpi_B_DataBuffer[fpiAidxCounter][i][ADE78XX_HSDC_CH_ICWV], fpiB3DataBuffer[i]);

			fpiBSumDataBuffer[i] = fpiB1DataBuffer[i] +
								   fpiB2DataBuffer[i] +
								   fpiB3DataBuffer[i];

			fpiB1DataBuffer[i]   = abs(fpiB1DataBuffer[i]);
			fpiB2DataBuffer[i]   = abs(fpiB2DataBuffer[i]);
			fpiB3DataBuffer[i]   = abs(fpiB3DataBuffer[i]);
			fpiBSumDataBuffer[i] = abs(fpiBSumDataBuffer[i]);

			fpiBidxCounter = ~fpiBidxCounter & 0x01;
		}

		PeakSampleDetector(&fpiB1DataBuffer[0],   &peaksB1Buffer[0]   ,&fpi_B_peakCurrent[PHASE_PEAK_A], &peaksB1Count);
		PeakSampleDetector(&fpiB2DataBuffer[0],   &peaksB2Buffer[0]   ,&fpi_B_peakCurrent[PHASE_PEAK_B], &peaksB2Count);
		PeakSampleDetector(&fpiB3DataBuffer[0],   &peaksB3Buffer[0]   ,&fpi_B_peakCurrent[PHASE_PEAK_C], &peaksB3Count);
		PeakSampleDetector(&fpiBSumDataBuffer[0], &peaksBSumBuffer[0] ,&fpi_B_peakCurrent[PHASE_PEAK_N], &peaksBNCount);

		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(1);
    }

    if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
    {
         /* Clear error counter Interrupt pending */
    	LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
    }

    if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(1))
    {
         /* Clear error counter Interrupt pending */
    	LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(1);
    }

    //PeakCurrentsToIOManager();
}

void PeakSampleDetector(lu_int32_t *dataBuffer, lu_int32_t * peaksBuffer, lu_int32_t *peakValuePtr, lu_uint16_t *peaksCount)
{
	volatile lu_uint16_t i, j;

	peaksBuffer[*peaksCount] = 0;

	for(i = 0; i < PHASE_CURRENT_BUFFER_SIZE; i++)
	{
		if(dataBuffer[i] > peaksBuffer[*peaksCount])
		{
			peaksBuffer[*peaksCount] = dataBuffer[i];
		}
	}

	*peaksCount = *peaksCount + 1;

	if(*peaksCount == PHASE_CURRENT_BUFFER_SIZE)
	{
		*peakValuePtr = 0;
		for(j = 0; j < PHASE_CURRENT_BUFFER_SIZE; j++)
		{
			if(peaksBuffer[j] > *peakValuePtr)
			{
				*peakValuePtr  = peaksBuffer[j];
			}
		}
		*peaksCount = 0;
	}
}

/*
 *********************** End of file ******************************************
 */
