/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IOManager IO module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "ProcessorGPIOMap.h"

#include "systemTime.h"

#include "BoardIO.h"
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

lu_bool_t ioManagerIOEventingEnable = LU_FALSE;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR  IOManagerIODisableSimulation(void)
{
	lu_uint32_t	 		ioID;

	/* Initialise boardIOTable - */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_SIMULATE_IO;
	}

	return SB_ERROR_NONE;
}

SB_ERROR  IOManagerIOInit(void)
{
	lu_uint32_t	 		ioID;
	lu_uint32_t			value;

	/* Initialise boardIOTable - */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		boardIOTable[ioID].value = boardIOTable[ioID].rawValue = 0;
		boardIOTable[ioID].countHigh2Low     = 0;
		boardIOTable[ioID].countLow2High     = 0;
		boardIOTable[ioID].updateFlags       = 0;
		boardIOTable[ioID].pulseMs           = 0;
		boardIOTable[ioID].pulseTimerCountMs = 0;
		boardIOTable[ioID].InitAvgSamples    = LU_TRUE; /* Initialise for Filter */
		boardIOTable[ioID].averageSumValue   = 0;

		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_OUTPUT:
			/* set initialisation value for physical output pin */
			value = (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_LOW) ? 0 : 1;

			/* Invert value from the physical output value if required */
			value ^= (BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0;

			/* Set value in BoardIOTable */
			boardIOTable[ioID].value = boardIOTable[ioID].rawValue = value;
			break;

		case IO_CLASS_ANALOG_OUTPUT:
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_SPI_DIGIPOT:
				boardIOTable[ioID].value = boardIOTable[ioID].rawValue = 0x40; /* Default for Digipot @ power on */
				boardIOTable[ioID].updateFlags                        |= IOM_UPDATEFLAG_UPDATE_OUTPUT;
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
	}
	return SB_ERROR_NONE;
}

 SB_ERROR IOManagerIOChanMapInit(void)
 {
 	lu_uint32_t	channel;
 	lu_uint32_t ioID;
 	SB_ERROR    retError;

 	retError = SB_ERROR_NONE;

 	/* Runtime check for mapping of ioID <<==>> channel */

 	/* Analogue Input Channel */
 	for (channel = 0; channel < IO_CHAN_AI_MAP_SIZE; channel++)
 	{
 		if (channel != ioChanAIMap[channel].ioChan)
 		{
 			retError = SB_ERROR_INITIALIZED;
 			return retError;
 		}

 		ioID = ioChanAIMap[channel].ioID;
 		if (ioID != IO_ID_NA)
 		{
 			if (IOManagerCheckAnalgueChannel(ioID, channel) == LU_FALSE)
 			{
 				retError = SB_ERROR_INITIALIZED;
 				return retError;
 			}
 		}
 	}

#ifdef AN_OUT_IMPLEMENTED
 	/* Analogue Output Channel */
 	for (channel = 0; channel < IO_CHAN_AO_MAP_SIZE; channel++)
 	{
 		if (channel != ioChanAOMap[channel].ioChan)
 		{
 			retError = SB_ERROR_INITIALIZED;
 			return retError;
 		}
 		ioID = ioChanAOMap[channel].ioID;
 		if (ioID != IO_ID_NA)
 		{
 			if (IOManagerCheckAnalgueChannel(ioID, channel) == LU_FALSE)
 			{
 				retError = SB_ERROR_INITIALIZED;
 				return retError;
 			}
 		}
 	}
#endif

 	/* Digital Input Channel */
 	for (channel = 0; channel < IO_CHAN_DI_MAP_SIZE; channel++)
 	{
 		if (channel != ioChanDIMap[channel].ioChan)
 		{
 			retError = SB_ERROR_INITIALIZED;
 			return retError;
 		}

 		if (channel != ioChanDIMap[channel].ioChan)
 		{
 			retError = SB_ERROR_INITIALIZED;
 			return retError;
 		}

 		ioID = ioChanDIMap[channel].ioID;
 		if (ioID != IO_ID_NA)
 		{
 			if (IOManagerCheckDigitalChannel(ioID, channel) == LU_FALSE)
 			{
 				retError = SB_ERROR_INITIALIZED;
 				return retError;
 			}
 		}
 	}

 	/* Digital Output Channel */
 	for (channel = 0; channel < IO_CHAN_DO_MAP_SIZE; channel++)
 	{
 		ioID = ioChanDOMap[channel].ioID;
 		if (channel != ioChanDOMap[channel].ioChan)
 		{
 			retError = SB_ERROR_INITIALIZED;
 			return retError;
 		}

 		if (ioID != IO_ID_NA)
 		{
 			if (IOManagerCheckDigitalChannel(ioID, channel) == LU_FALSE)
 			{
 				retError = SB_ERROR_INITIALIZED;
 				return retError;
 			}
 		}
 	}

 	return retError;
 }

 SB_ERROR IOManagerIOGetModuleID(lu_uint8_t *deviceIDPtr, lu_uint32_t ioIDSel1, lu_uint32_t ioIDSel2, lu_uint32_t ioIDSel3, lu_uint32_t ioIDSel4)
 {
 	SB_ERROR	retError;
 	lu_uint32_t	sel1;
 	lu_uint32_t	sel2;
 	lu_uint32_t	sel3;
 	lu_uint32_t	sel4;
 	lu_uint8_t  bcdValue;

 	retError = SB_ERROR_NONE;
 	bcdValue = 0;

 	if (ioIDSel1 < BOARD_IO_MAX_IDX)
 	{
 		sel1 = ProcGPIORead(BoardIOMap[ioIDSel1].ioAddr.gpio.port, BoardIOMap[ioIDSel1].ioAddr.gpio.pin);
 	}
 	else
 	{
 		retError = SB_ERROR_PARAM;
 	}

 	if (ioIDSel2 < BOARD_IO_MAX_IDX)
 	{
 		sel2 = ProcGPIORead(BoardIOMap[ioIDSel2].ioAddr.gpio.port, BoardIOMap[ioIDSel2].ioAddr.gpio.pin);
 	}
 	else
 	{
 		retError = SB_ERROR_PARAM;
 	}

 	if (ioIDSel3 < BOARD_IO_MAX_IDX)
 	{
 		sel3 = ProcGPIORead(BoardIOMap[ioIDSel3].ioAddr.gpio.port, BoardIOMap[ioIDSel3].ioAddr.gpio.pin);
 	}
 	else
 	{
 		retError = SB_ERROR_PARAM;
 	}

 	if (ioIDSel4 < BOARD_IO_MAX_IDX)
 	{
 		sel4 = ProcGPIORead(BoardIOMap[ioIDSel4].ioAddr.gpio.port, BoardIOMap[ioIDSel4].ioAddr.gpio.pin);
 	}
 	else
 	{
 		retError = SB_ERROR_PARAM;
 	}

 	/* convert to bcd value */
 	if (retError == SB_ERROR_NONE)
 	{
 		bcdValue |= sel1;
 		bcdValue |= (sel2 << 1);
 		bcdValue |= (sel3 << 2);
 		bcdValue |= (sel4 << 3);
 		bcdValue ^= 0xf;
 	}

 	switch (bcdValue)
 	{
 	case 0x01:
 		*deviceIDPtr = MODULE_ID_0;
 		break;

 	case 0x02:
 		*deviceIDPtr = MODULE_ID_1;
 		break;

 	case 0x03:
 		*deviceIDPtr = MODULE_ID_2;
 		break;

 	case 0x04:
 		*deviceIDPtr = MODULE_ID_3;
 		break;

 	case 0x05:
 		*deviceIDPtr = MODULE_ID_4;
 		break;

 	case 0x06:
 		*deviceIDPtr = MODULE_ID_5;
 		break;

 	case 0x07:
 		*deviceIDPtr = MODULE_ID_6;
 		break;

 	default:
 		*deviceIDPtr = MODULE_ID_0;
 		if (retError == SB_ERROR_NONE)
 		{
 			retError = SB_ERROR_IOMAN_BOARD_DISABLE;
 		}
 		break;
 	}

 	return retError;
}

SB_ERROR IOManagerIOGetLocalRemoteStatus(IO_STAT_LOR *ioStatLORPtr, lu_uint32_t ioIDLocal, lu_uint32_t ioIDRemote)
{
	SB_ERROR		retError;
  	lu_uint32_t		local;
  	lu_uint32_t		remote;

  	retError = SB_ERROR_NONE;

  	if (ioIDLocal < BOARD_IO_MAX_IDX)
	{
  		local = ProcGPIORead(BoardIOMap[ioIDLocal].ioAddr.gpio.port, BoardIOMap[ioIDLocal].ioAddr.gpio.pin);
	}
  	else
	{
		retError = SB_ERROR_PARAM;
	}

  	if (retError == SB_ERROR_NONE)
  	{
  		if (ioIDRemote < BOARD_IO_MAX_IDX)
		{
  			remote = ProcGPIORead(BoardIOMap[ioIDRemote].ioAddr.gpio.port, BoardIOMap[ioIDRemote].ioAddr.gpio.pin);
		}
		else
		{
			retError = SB_ERROR_PARAM;
		}
  	}

  	*ioStatLORPtr = IO_STAT_LOR_INVALID;

  	/* Local / remote input are active low */
  	/* LR
  	 * 00 = Off
  	 * 01 = Local
  	 * 10 = Remote
  	 * 11 = Invalid (Local or Remote lines are not driven)
  	 */

  	if (retError == SB_ERROR_NONE)
  	{
  		if (local == 0 && remote == 0)
  		{
  			*ioStatLORPtr = IO_STAT_LOR_OFF;
  		}
  		else if (local == 0 && remote == 1)
  		{
  			*ioStatLORPtr = IO_STAT_LOR_LOCAL;
  		}
  		else if (local == 1 && remote == 0)
		{
  			*ioStatLORPtr = IO_STAT_LOR_REMOTE;
		}
  	}

  	return retError;
}

SB_ERROR  IOGetValue(lu_uint32_t ioID, lu_int32_t *retValuePtr)
{
	SB_ERROR 	retError;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		if (retValuePtr != NULL)
		{
			*retValuePtr = boardIOTable[ioID].value;

			if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE)
			{
				retError = SB_ERROR_OFFLINE;
			}
			else
			{
				retError = SB_ERROR_NONE;
			}
		}
	}
	return retError;
}

SB_ERROR  IOGetRawValue(lu_uint32_t ioID, lu_int32_t *retValuePtr)
{
	SB_ERROR 	retError;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		if (retValuePtr != NULL)
		{
			*retValuePtr = boardIOTable[ioID].rawValue;

			if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE)
			{
				retError = SB_ERROR_OFFLINE;
			}
			else
			{
				retError = SB_ERROR_NONE;
			}
		}
	}
	return retError;
}

SB_ERROR  IOMSetValue(lu_uint32_t ioID, lu_int32_t value)
{
	SB_ERROR 	retError;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{

		/* Clear any outstanding pulse generator */
		boardIOTable[ioID].pulseMs 			 = 0;
		boardIOTable[ioID].pulseTimerCountMs = 0;
		boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_REPEATED;
		boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_SINGLE;


		switch (BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_OUTPUT:
		case IO_CLASS_ANALOG_OUTPUT:
			/* For output the  rawValue is the same value */
			boardIOTable[ioID].value = value;
			boardIOTable[ioID].rawValue = value;

			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_SPI_DIGIPOT:
			case IO_DEV_SPI_AD_DIGITAL_POT:
			case IO_DEV_PERIPH:
				/* Notify IOManager that output value needs to change */
				boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_UPDATE_OUTPUT;
				break;
			default:
				break;
			}
			break;

		case IO_CLASS_DIGITAL_INPUT:
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_VIRTUAL:
				if (boardIOTable[ioID].value != value)
				{
					/* Force a pending input event */
					boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
				}
				boardIOTable[ioID].value = boardIOTable[ioID].rawValue = value;

				/* Event will be generated via IOManagerIORunUpdateGPIO() */
				break;

			default:
				break;
			}
			break;

		case IO_CLASS_ANALOG_INPUT:
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_VIRTUAL:
			case IO_DEV_I2C_HUMIDITY:
			case IO_DEV_HSDC_ADE78XX:
			case IO_DEV_SPI_FEPIC:
				boardIOTable[ioID].value = boardIOTable[ioID].rawValue   = value;

				/* Will need to change when analogue input is supported !!! */
				break;

			default:
				break;
			}
			break;


		default:
			break;
		}

		retError = SB_ERROR_NONE;
	}
	return retError;
}


SB_ERROR  IOMSetPulseGen(lu_uint32_t ioID, lu_uint16_t timeMs, lu_bool_t repeated)
{
	SB_ERROR 			retError;
	lu_int32_t			value;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		if (timeMs > 0)
		{
			if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_SINGLE)
			{
				retError = SB_EEROR_IOMAN_PULSE_STARTED;
			}
			else
			{
				retError = SB_ERROR_NONE;

				/* Toggle state of current IO Pin */
				value = boardIOTable[ioID].value;
				value = value ? 0 : 1;
				boardIOTable[ioID].value = boardIOTable[ioID].rawValue = value;

				boardIOTable[ioID].pulseMs 			 = timeMs;

				if (repeated)
				{
					boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_SINGLE;
					boardIOTable[ioID].updateFlags       |= IOM_UPDATEFLAG_PULSE_REPEATED;
					boardIOTable[ioID].pulseTimerCountMs  = STGetTime();
				}
				else
				{
					boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_REPEATED;
					boardIOTable[ioID].updateFlags       |= IOM_UPDATEFLAG_PULSE_SINGLE;
					boardIOTable[ioID].pulseTimerCountMs  = STGetTime();
				}
			}
		}
	}

	return retError;
}

SB_ERROR  IOMChanAIGetValue(lu_uint32_t ioChan, lu_int32_t *retValue)
{
	SB_ERROR 	retError;
	lu_uint32_t ioID;

	retError = SB_ERROR_PARAM;

	if (retValue != NULL)
	{
		if (ioChan < IO_CHAN_AI_MAP_SIZE)
		{
			ioID = ioChanAIMap[ioChan].ioID;

			if (ioID < BOARD_IO_MAX_IDX)
			{
				*retValue = boardIOTable[ioID].value;

				if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE)
				{
					retError = SB_ERROR_OFFLINE;
				}
				else
				{
					retError = SB_ERROR_NONE;
				}
			}
		}
	}
	return retError;
}

SB_ERROR  IOMChanDIGetValue(lu_uint32_t ioChan, lu_int32_t *retValue, lu_int32_t *retRawValue)
{
	SB_ERROR 	retError;
	lu_uint32_t ioID;

	retError = SB_ERROR_PARAM;

	if (retValue != NULL && retRawValue != NULL)
	{
		if (ioChan < IO_CHAN_DI_MAP_SIZE)
		{
			ioID = ioChanDIMap[ioChan].ioID;

			if (ioID < BOARD_IO_MAX_IDX)
			{
				*retValue    = boardIOTable[ioID].value;
				*retRawValue = boardIOTable[ioID].rawValue;

				if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE)
				{
					retError = SB_ERROR_OFFLINE;
				}
				else
				{
					retError = SB_ERROR_NONE;
				}
			}
		}
	}
	return retError;
}

SB_ERROR  IOManagerIOSetSimIoID(lu_uint32_t ioID, lu_bool_t enableSimulation)
{
	SB_ERROR 	retError;

	retError = SB_ERROR_PARAM;

	if (ioID < BOARD_IO_MAX_IDX)
	{
		if (enableSimulation)
		{
			boardIOTable[ioID].updateFlags       |= IOM_UPDATEFLAG_SIMULATE_IO;
		}
		else
		{
			boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_SIMULATE_IO;
		}

		retError = SB_ERROR_NONE;
	}

	return retError;
}

SB_ERROR IOManagerIOSetSimAIChan(lu_uint32_t ioChan, lu_bool_t enableSimulation)
{
	SB_ERROR 	retError;
	lu_uint32_t ioID;

	retError = SB_ERROR_PARAM;

	if (ioChan < IO_CHAN_AI_MAP_SIZE)
	{
		ioID = ioChanAIMap[ioChan].ioID;

		if (ioID < BOARD_IO_MAX_IDX)
		{
			if (enableSimulation)
			{
				boardIOTable[ioID].updateFlags       |= IOM_UPDATEFLAG_SIMULATE_IO;
			}
			else
			{
				boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_SIMULATE_IO;
			}

			retError = SB_ERROR_NONE;
		}
	}

	return retError;
}

SB_ERROR IOManagerIOSetSimDIChan(lu_uint32_t ioChan, lu_bool_t enableSimulation)
{
	SB_ERROR 	retError;
	lu_uint32_t ioID;

	retError = SB_ERROR_PARAM;

	if (ioChan < IO_CHAN_DI_MAP_SIZE)
	{
		ioID = ioChanDIMap[ioChan].ioID;

		if (ioID < BOARD_IO_MAX_IDX)
		{
			if (enableSimulation)
			{
				boardIOTable[ioID].updateFlags       |= IOM_UPDATEFLAG_SIMULATE_IO;
			}
			else
			{
				boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_SIMULATE_IO;
			}

			retError = SB_ERROR_NONE;
		}
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
