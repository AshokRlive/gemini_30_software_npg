/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   12/10/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "systemTime.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerI2CIOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"

#include "BoardIOMap.h"
#include "BoardIO.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_I2C_EXP_PER_BUS			3  /* Max supported devices on a I2C bus */

#define UPDATE_GPIO(port, pin) \
	port & (1L << pin) ? 1 : 0


#define PCA9555_ADDR					0x20

#define PCA9555_CMD_INPUT_PORT_0		0x00
#define PCA9555_CMD_INPUT_PORT_1		0x01
#define PCA9555_CMD_OUTPUT_PORT_0		0x02
#define PCA9555_CMD_OUTPUT_PORT_1		0x03
#define PCA9555_CMD_POLINV_PORT_0		0x04
#define PCA9555_CMD_POLINV_PORT_1		0x05
#define PCA9555_CMD_CONFIG_DDR_PORT_0	0x06
#define PCA9555_CMD_CONFIG_DDR_PORT_1	0x07

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*!
 *  IO Expander data data direction
 */
typedef struct I2cExpPinDDRDef
{
	lu_uint8_t		addr; 	/* I2C Address */
	lu_bool_t       isOnline;
	lu_uint16_t		ddr;
	lu_uint16_t		portOut;
	lu_uint16_t		portIn;
}I2cExpPinDDRStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR configureCheckI2CExpander(lu_uint8_t i2cBus);
SB_ERROR configureAllI2CExpanders(void);
SB_ERROR writeI2cDDR(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t value);
SB_ERROR readI2cDDR(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t *valuePtr);
SB_ERROR writeI2cPort(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t value);
SB_ERROR readI2cPort(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t *valuePtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint8_t			i2cBusDeviceCount[IO_BUS_I2C_2+1];
static I2cExpPinDDRStr      i2cPinData[IO_BUS_I2C_2+1][MAX_I2C_EXP_PER_BUS];


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR IOManagerI2CIOInit(void)
{
	 SB_ERROR	retError;

	 retError = SB_ERROR_NONE;

	 /* Configure I2C pins & peripheral */
	 configureCheckI2CExpander(IO_BUS_I2C_0);
	 configureCheckI2CExpander(IO_BUS_I2C_1);
	 configureCheckI2CExpander(IO_BUS_I2C_2);

	 /* Now configure IO pins direction and state */
	 retError = configureAllI2CExpanders();

	 return retError;
}

SB_ERROR IOManagerI2CIOUpdate(void)
{
	 SB_ERROR	        retError;
	 lu_uint32_t		ioID;
	 lu_uint32_t		ioChan;
	 lu_uint8_t			addressFound;
	 lu_uint8_t			i2cBusIdx;
	 lu_uint8_t			i2cAddrIdx;
	 lu_uint8_t			inputInvert;
	 lu_uint16_t		dbLow2HighMs;
	 lu_uint16_t		dbHigh2LowMs;
	 lu_int32_t			oldValue;
	 ModuleTimeStr	    canTime;
	 ModuleDEventStr    digEvent;
	 lu_uint8_t			digInvertValue;
	 lu_uint32_t         time;

	 time        = STGetTime();

	 canTime     = CANCGetTime();

	 retError = SB_ERROR_NONE;

	 /* Read I2C Expander inputs */
	 for (i2cBusIdx = 0; i2cBusIdx <= IO_BUS_I2C_2; i2cBusIdx++)
	 {
		 for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_EXP_PER_BUS; i2cAddrIdx++)
		 {
			 if (i2cPinData[i2cBusIdx][i2cAddrIdx].addr != 0xff)
			 {
				 retError = readI2cPort(i2cBusIdx,
						 	 	 	    i2cPinData[i2cBusIdx][i2cAddrIdx].addr,
						 	 	 	    &i2cPinData[i2cBusIdx][i2cAddrIdx].portIn
						 	 	 	   );

				 /* Record is port read is online or offline */
				 i2cPinData[i2cBusIdx][i2cAddrIdx].isOnline = (retError == SB_ERROR_NONE) ? LU_TRUE : LU_FALSE;
			 }
		 }
	 }


	 /* Perform an IO update on all IO in the ioTable[] */
	 for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	 {
		if (BoardIOMap[ioID].ioAddr.i2cExpander.mode > 0L)
		{
			digInvertValue = 0x20;
		}

		 switch(BoardIOMap[ioID].ioDev)
		 {
		 case IO_DEV_I2C_IO_EXPANDER:
			 /* Is an I2C IO Expander */

			 addressFound = 0;
			 for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_EXP_PER_BUS; i2cAddrIdx++)
			 {
				if (BoardIOMap[ioID].ioAddr.i2cExpander.address == i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].addr)
				{
					/* Match I2C bus address */
					addressFound = 1;
					break; //JF not sure ???
				}
			 }

			 if (!addressFound)
			 {
				continue;
			 }

			 if (BoardIOMap[ioID].ioClass == IO_CLASS_DIGITAL_INPUT)
			 {
				oldValue = boardIOTable[ioID].value;
				ioChan = BoardIOMap[ioID].ioChan;

				/* If not simulated, then use real IO operations */
				if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
				{
					/* Input Invert for Real GPIO (Not supported for virtual IO) */
					inputInvert =
						(BoardIOMap[ioID].ioAddr.i2cExpander.mode & GPIO_PM_INPUT_INVERT) ? 1 : 0;

					/* Update I2C Expander Input */
					boardIOTable[ioID].rawValue =
						UPDATE_GPIO(i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].portIn, \
								BoardIOMap[ioID].ioAddr.i2cExpander.ioPin);

					/* Apply Invert to raw from IOMap */
					boardIOTable[ioID].rawValue  ^= inputInvert;

					/* Set debounce limits from IOMap or IOChan */
					dbLow2HighMs = BoardIOMap[ioID].db.low2HighMs;
					dbHigh2LowMs = BoardIOMap[ioID].db.low2HighMs;
					if (ioChan != IO_CH_NA)
					{
						/* Apply invert for External equipment channel */
						inputInvert = (ioChanDIMap[ioChan].extEquipInvert) ? 1 : 0;
						boardIOTable[ioID].rawValue  ^= inputInvert;

						if (ioChanDIMap[ioChan].db.high2LowMs && ioChanDIMap[ioChan].db.low2HighMs)
						{
							dbLow2HighMs = ioChanDIMap[ioChan].db.high2LowMs;
							dbHigh2LowMs = ioChanDIMap[ioChan].db.low2HighMs;
						}
					}

					/* Debounce - override with channel db (The inverts are already applied) */
					if (dbLow2HighMs && dbHigh2LowMs)
					{
						if (boardIOTable[ioID].rawValue)
						{
							/* Low >>>>>>>> High */
							boardIOTable[ioID].countLow2High += IOMAN_UPDATE_I2CIO_MS;
							if (boardIOTable[ioID].countLow2High >= dbLow2HighMs)
							{
								boardIOTable[ioID].countLow2High = 0;
								boardIOTable[ioID].value = boardIOTable[ioID].rawValue;
							}
							boardIOTable[ioID].countHigh2Low = 0;
						}
						else
						{
							/* High >>>>>>> Low */
							boardIOTable[ioID].countHigh2Low += IOMAN_UPDATE_I2CIO_MS;
							if (boardIOTable[ioID].countHigh2Low >= dbHigh2LowMs)
							{
								boardIOTable[ioID].countHigh2Low = 0;
								boardIOTable[ioID].value = boardIOTable[ioID].rawValue;
							}
							boardIOTable[ioID].countLow2High = 0;
						}
					}
					else
					{
						boardIOTable[ioID].value = boardIOTable[ioID].rawValue;
					}

					if (oldValue != boardIOTable[ioID].value && ioChan != IO_CH_NA)
					{
						/* Change of state - Event pending */
						boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
					}
					else
					{
						if ((boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE))
						{
							/* Last reported state as OFFLINE */
							if (i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].isOnline == LU_TRUE)
							{
								/* Change of state (Now ONLINE)- Event pending */
								boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
							}
						}
						else
						{
							/* Last reported state as ONLINE */
							if (i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].isOnline == LU_FALSE)
							{
								/* Change of state (Now OFFLINE)- Event pending */
								boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
							}
						}

					}
				}

				/* Is enabled, so report event */
				if (ioManagerIOEventingEnable == LU_TRUE &&
					ioChanDIMap[ioChan].eventEnable &&
					(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_INPUT_EVENT))
				{

					/* Update online / offline status */
					if (i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].isOnline == LU_TRUE)
					{
						boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_OFFLINE;
					}
					else
					{
						boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_OFFLINE;
					}

					/* Is an event for an IO channel */
					/* Generate an event */
					digEvent.channel 	= BoardIOMap[ioID].ioChan;
					digEvent.time		= canTime;
					digEvent.value		= boardIOTable[ioID].value;
					digEvent.isOnline   = (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE) ? 0 : 1;
					digEvent.simulated  = (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO) ? 1 : 0;

					CANCSendMCM( MODULE_MSG_TYPE_EVENT,
								 MODULE_MSG_ID_EVENT_DIGITAL,
								 sizeof(ModuleDEventStr),
								 (lu_uint8_t *)&digEvent);

					/* Clear the event flag */
					boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_INPUT_EVENT;
				}
			}

			if (BoardIOMap[ioID].ioClass == IO_CLASS_DIGITAL_OUTPUT)
			{
				if (boardIOTable[ioID].pulseMs)
				{
					if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_SINGLE)
					{
						/* Single pulse generator is enabled */
						if (STElapsedTime(boardIOTable[ioID].pulseTimerCountMs , time) > boardIOTable[ioID].pulseMs)
						{
							/* Toggle current value */
							oldValue = boardIOTable[ioID].value;
							oldValue = oldValue ? 0 : 1;
							boardIOTable[ioID].value = boardIOTable[ioID].rawValue = oldValue;

							/* Clear single shot pulse */
							boardIOTable[ioID].pulseTimerCountMs = 0;
							boardIOTable[ioID].pulseMs           = 0;

							boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_SINGLE;
						}
					}

					if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_REPEATED)
					{
						/* Pulse repeated generator is enabled */

						if (STElapsedTime(boardIOTable[ioID].pulseTimerCountMs , time) > boardIOTable[ioID].pulseMs)
						{
							/* Toggle current value */
							oldValue = boardIOTable[ioID].value;
							oldValue = oldValue ? 0 : 1;
							boardIOTable[ioID].value = boardIOTable[ioID].rawValue = oldValue;

							/* Reset counter for next time */
							boardIOTable[ioID].pulseTimerCountMs = time;
						}
					}
				}

				/* Apply invert to physical output if required */
				digInvertValue = boardIOTable[ioID].value ? 1 : 0;

				/* Force output off */
				if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO)
				{
					digInvertValue = 0;
				}

				digInvertValue ^= (BoardIOMap[ioID].ioAddr.i2cExpander.mode  & GPIO_PM_OUTPUT_INVERT) ? 1 : 0;


				/* If not simulated, then use real IO operations */
				if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
				{
					/* Update i2cPinData table */
					if (digInvertValue)
					{
						/* Set output bit */
						i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].portOut |=
								(1 << BoardIOMap[ioID].ioAddr.i2cExpander.ioPin);
					}
					else
					{
						/* Clear output bit */
						i2cPinData[BoardIOMap[ioID].ioAddr.i2cExpander.busI2c][i2cAddrIdx].portOut &=
								~(1 << BoardIOMap[ioID].ioAddr.i2cExpander.ioPin);
					}
				}
			}
			break;


		 default:
			break;
		 }
	 }

	 /* Write I2C IO Expansion output */
	 for (i2cBusIdx = 0; i2cBusIdx <= IO_BUS_I2C_2; i2cBusIdx++)
	 {
		 for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_EXP_PER_BUS; i2cAddrIdx++)
		 {
			if (i2cPinData[i2cBusIdx][i2cAddrIdx].addr != 0xff)
			{
				/* Write Data & then DDR - (Required for safety to ensure DDR is OK even if chip resets!!!) */
				retError = writeI2cPort(i2cBusIdx,
						                i2cPinData[i2cBusIdx][i2cAddrIdx].addr,
						                i2cPinData[i2cBusIdx][i2cAddrIdx].portOut);

				/* Record is port Write is online or offline */
  			    i2cPinData[i2cBusIdx][i2cAddrIdx].isOnline = (retError == SB_ERROR_NONE) ? LU_TRUE : LU_FALSE;

				writeI2cDDR(i2cBusIdx,
				        i2cPinData[i2cBusIdx][i2cAddrIdx].addr,
						i2cPinData[i2cBusIdx][i2cAddrIdx].ddr);
			}
		 }
	 }
	 return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


 /*!
  ******************************************************************************
  *   \brief Check for I2C Expander I2C device & configure I2C if found
  *
  *   Detailed description
  *
  *   \param i2cBus
  *
  *
  *   \return SB_ERROR Error code
  *
  ******************************************************************************
  */
 SB_ERROR configureCheckI2CExpander(lu_uint8_t i2cBus)
 {
 	SB_ERROR				retVal;
 	lu_uint32_t				ioID;

 	retVal = SB_ERROR_INITIALIZED;

 	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
 	{
 		/* Is Digital IO */
 		switch(BoardIOMap[ioID].ioClass)
 		{
 		case IO_CLASS_DIGITAL_OUTPUT:
 		case IO_CLASS_DIGITAL_INPUT:
 			switch(BoardIOMap[ioID].ioDev)
 			{
 			/* Is an I2C Expander device */
 			case IO_DEV_I2C_IO_EXPANDER:
 				if (BoardIOMap[ioID].ioAddr.i2cExpander.busI2c == i2cBus)
 				{
 					// find & configure peripheral pins
 					retVal = configureI2CPeripheral(i2cBus);
 					ioID = BOARD_IO_MAX_IDX;
 				}
 				break;

 			default:
 				break;
 			}
 			break;

 		default:
 			break;

 		}
 	}

 	return SB_ERROR_NONE;
 }

/*!
  ******************************************************************************
  *   \brief Configure all I2C Expander pins
  *
  *   Detailed description
  *
  *   \param none
  *
  *
  *   \return SB_ERROR Error code
  *
  ******************************************************************************
  */
 SB_ERROR configureAllI2CExpanders(void)
 {
	 SB_ERROR				retError;
	 lu_uint32_t			ioID;
	 lu_uint8_t				addressFound;
	 lu_uint8_t				i2cBusIdx;
	 lu_uint8_t				i2cAddrIdx;

	 retError = SB_ERROR_NONE;

	 /* Initialise */
	 for (i2cBusIdx = 0; i2cBusIdx <= IO_BUS_I2C_2; i2cBusIdx++)
	 {
		 i2cBusDeviceCount[i2cBusIdx] = 0;

		 /* Default: Init I2C Exp IO Data Direction to inputs */
		 for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_EXP_PER_BUS; i2cAddrIdx++)
		 {
			 i2cPinData[i2cBusIdx][i2cAddrIdx].addr    = 0xff;
			 i2cPinData[i2cBusIdx][i2cAddrIdx].ddr     = 0xffff;
			 i2cPinData[i2cBusIdx][i2cAddrIdx].portOut = 0xffff;
		 }
	 }

	 for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	 {
		 /* Is Digital IO */
		switch(BoardIOMap[ioID].ioClass)
		{
		case IO_CLASS_DIGITAL_OUTPUT:
		case IO_CLASS_DIGITAL_INPUT:
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_I2C_IO_EXPANDER:
				i2cBusIdx = BoardIOMap[ioID].ioAddr.i2cExpander.busI2c;

				if (i2cBusIdx <= IO_BUS_I2C_2)
				{
					/* Initialise for first device found on an I2C bus */
					if (i2cBusDeviceCount[i2cBusIdx] == 0)
					{
						i2cPinData[i2cBusIdx][i2cBusDeviceCount[i2cBusIdx]].addr = BoardIOMap[ioID].ioAddr.i2cExpander.address;
						i2cBusDeviceCount[i2cBusIdx]++;
					}

					/* Scan pinData for I2C address */
					addressFound = 0;
					for (i2cAddrIdx = 0; i2cAddrIdx < i2cBusDeviceCount[i2cBusIdx]; i2cAddrIdx++)
					{
						if (i2cPinData[i2cBusIdx][i2cAddrIdx].addr == BoardIOMap[ioID].ioAddr.i2cExpander.address)
						{
							addressFound = 1;
						}
					}

					/* Add new address to pinData */
					if (!addressFound)
					{
						i2cBusDeviceCount[i2cBusIdx]++;

						if (i2cBusDeviceCount[i2cBusIdx] > MAX_I2C_EXP_PER_BUS)
						{
							retError = SB_ERROR_INITIALIZED;
							return retError;
						}

						i2cPinData[i2cBusIdx][(i2cBusDeviceCount[i2cBusIdx] - 1)].addr = BoardIOMap[ioID].ioAddr.i2cExpander.address;
					}

					if (BoardIOMap[ioID].ioClass == IO_CLASS_DIGITAL_OUTPUT)
					{
						/* Match address in IOMap with local pinData Array table */
						for (i2cAddrIdx = 0; i2cAddrIdx < i2cBusDeviceCount[i2cBusIdx]; i2cAddrIdx++)
						{
							if (BoardIOMap[ioID].ioAddr.i2cExpander.address == i2cPinData[i2cBusIdx][i2cAddrIdx].addr)
							{
								/* Have a match / Clear Data Direction bit */
								i2cPinData[i2cBusIdx][i2cAddrIdx].ddr &=
										~(0x0001 << BoardIOMap[ioID].ioAddr.i2cExpander.ioPin);

								/* Set default output pin state if low */
								if (BoardIOMap[ioID].ioAddr.i2cExpander.mode & GPIO_PM_OUTPUT_LOW)
								{
									i2cPinData[i2cBusIdx][i2cAddrIdx].portOut &=
											~(0x0001 << BoardIOMap[ioID].ioAddr.i2cExpander.ioPin);
								}
							}
						}
					}

				}
				break;

			default:
				break;
			}

			default:
				break;
		 }
	 }

	 /* Configure the Data Direction register & Write output on I2C Expander */
	 for (i2cBusIdx = 0; i2cBusIdx <= IO_BUS_I2C_2; i2cBusIdx++)
	 {
		 /* If devices present on this I2C bus... */
		 if (i2cBusDeviceCount[i2cBusIdx])
		 {
			 for (i2cAddrIdx = 0; i2cAddrIdx < MAX_I2C_EXP_PER_BUS; i2cAddrIdx++)
			 {
				 /* For each configured slave address on I2C bus */
				 if (i2cPinData[i2cBusIdx][i2cAddrIdx].addr != 0xff)
				 {
					 /* Write Data & then DDR */
					 writeI2cPort(i2cBusIdx,
							 i2cPinData[i2cBusIdx][i2cAddrIdx].addr,
							 i2cPinData[i2cBusIdx][i2cAddrIdx].portOut);

					 writeI2cDDR(i2cBusIdx,
							 i2cPinData[i2cBusIdx][i2cAddrIdx].addr,
							 i2cPinData[i2cBusIdx][i2cAddrIdx].ddr);
				 }
			 }
		 }
	 }
	 return retError;
}

/*!
 ******************************************************************************
 *   \brief
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeI2cDDR(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t value)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
//	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_CONFIG_DDR_PORT_0;
		 /* Data Value */
		 txData[1]						= (value & 0xff);
		 txData[2]						= (value >> 8);

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 3;
		 txferCfg.rx_data		 	 	= NULL;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }
	return retError;
}

/*!
 ******************************************************************************
 *   \brief
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cDDR(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t *valuePtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_CONFIG_DDR_PORT_0;

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 *valuePtr = *(lu_uint16_t *)&rxData[0];
		 }
	 }

	return retError;
}

/*!
 ******************************************************************************
 *   \brief
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeI2cPort(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t value)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
//	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_OUTPUT_PORT_0;
		 /* Data Value */
		 txData[1]						= (value & 0xff);
		 txData[2]						= (value >> 8);

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 3;
		 txferCfg.rx_data		 	 	= NULL;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }
	return retError;
}

/*!
 ******************************************************************************
 *   \brief
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cPort(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint16_t *valuePtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_INPUT_PORT_0;

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 *valuePtr = *(lu_uint16_t *)&rxData[0];
		 }
	 }

	return retError;
}

/*
 *********************** End of file ******************************************
 */
