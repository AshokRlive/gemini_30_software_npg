/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/04/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerI2CExtAdcMCP342XOUpdate.h"
#include "ProcessorGPIOMap.h"
#include "ProcessorI2C.h"

#include "BoardIOMap.h"
#include "BoardIO.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MCP342X_ADDR						0x68

#define MCP242X_CFG_REG_RDY					0x80

#define MCP242X_CFG_REG_ADC_CHAN_SHIFT 	 	5
#define MCP242X_CFG_REG_ADC_CHAN_MASK   	0x03

#define MCP242X_CFG_REG_CONVERT_CONT		0x10
#define MCP242X_CFG_REG_CONVERT_ONE_SHOT	0x00

#define MCP242X_CFG_REG_SAMP_3_75SPS		0x0c
#define MCP242X_CFG_REG_SAMP_15SPS			0x08
#define MCP242X_CFG_REG_SAMP_60SPS			0x04
#define MCP242X_CFG_REG_SAMP_240SPS			0x00

#define MCP242X_CFG_REG_PGA_X8				0x03
#define MCP242X_CFG_REG_PGA_X4				0x02
#define MCP242X_CFG_REG_PGA_X2				0x01
#define MCP242X_CFG_REG_PGA_X1				0x00

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

lu_uint8_t convertIoIDtoI2CAdcChan(lu_uint32_t ioID, lu_uint8_t *retAdcChanPtr);
SB_ERROR configureCheckI2CAdc(lu_uint8_t i2cBus);
SB_ERROR selectChannelI2cAdc(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t adcChan);
SB_ERROR readI2cAdc(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t adcChan, lu_int32_t *valuePtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint32_t	intAdcIoID = 0;
static lu_uint8_t   intAdcChan = 0;

static lu_uint32_t	intAdcIoIDStart = 0;
static lu_uint32_t	intAdcIoIDEnd = 0;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR IOManagerI2CExtAdcMCP342XIOInit(void)
{
	 SB_ERROR	retError;
	 lu_uint8_t	foundAnalog;


	 retError = SB_ERROR_NONE;

	 /* Configure I2C pins & peripheral */
	 configureCheckI2CAdc(IO_BUS_I2C_0);
	 configureCheckI2CAdc(IO_BUS_I2C_1);
	 configureCheckI2CAdc(IO_BUS_I2C_2);

	 /* Find first analog channel and select that channel */
	foundAnalog = 0;
	do
	{
		/* Is an Analogue input */
		if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT)
		{
			switch(BoardIOMap[intAdcIoID].ioDev)
			{
			case IO_DEV_I2C_ADC_MCP324X:
				if (convertIoIDtoI2CAdcChan(intAdcIoID, &intAdcChan) == LU_TRUE)
				{
					/* Select ADC channel for next conversion */
					retError = selectChannelI2cAdc(BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.busI2c,
											       BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.address,
											       intAdcChan);

					foundAnalog = 1;
				}
				break;

			default:
				break;
			}
		}
		if (!foundAnalog)
		{
			intAdcIoID++;
		}
		else
		{
			/* Found first analogue channel */
			intAdcIoIDStart = intAdcIoID;
		}
	} while (intAdcIoID < BOARD_IO_MAX_IDX && !foundAnalog);

	if (!foundAnalog)
	{
		/* Cannot find any ADC channels */
		intAdcIoID = BOARD_IO_MAX_IDX;
	}
	else
	{
		/* JF yuk this is a mess! (Need to tidyup!!) */
		foundAnalog = 0;
		do
		{
			switch (BoardIOMap[intAdcIoID].ioClass)
			{
			case IO_CLASS_ANALOG_INPUT:
				if (BoardIOMap[intAdcIoID].ioDev != IO_DEV_I2C_ADC_MCP324X)
				{
					foundAnalog = 1;
				}
				break;

			default:
				foundAnalog = 1;
				break;
			}
			if (!foundAnalog)
			{
				intAdcIoID++;
			}
			else
			{
				/* Found just pas last Analogue */
				intAdcIoIDEnd = --intAdcIoID;
				intAdcIoID    = intAdcIoIDStart;
			}
		} while (intAdcIoID < BOARD_IO_MAX_IDX && !foundAnalog);
	}

	return retError;
}

SB_ERROR IOManagerI2CExtAdcMCP342XIOUpdate(void)
{
	SB_ERROR	        retError;
	lu_uint8_t			foundAnalog;
	lu_int32_t 			value;
	ModuleTimeStr	    canTime;

	canTime     	= CANCGetTime();

	retError 	 	= SB_ERROR_NONE;

	foundAnalog 	= 0;

	/* Perform a single IO update on all IO in the ioTable[] per scan */

	if (intAdcIoID != BOARD_IO_MAX_IDX)
	{
		foundAnalog = 0;

		switch(BoardIOMap[intAdcIoID].ioDev)
		{
		case IO_DEV_I2C_ADC_MCP324X:
		 /* Is an I2C Temperature sensor */
		 if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT)
		 {
			if (!(boardIOTable[intAdcIoID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
			{
				/* Read I2C ADC */
				retError = readI2cAdc(BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.busI2c,
									  BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.address,
									  BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.adcChan,
									  &value);

				if (retError == SB_ERROR_NONE)
				{
					boardIOTable[intAdcIoID].updateFlags &= ~IOM_UPDATEFLAG_OFFLINE;

					boardIOTable[intAdcIoID].rawValue = value;

					/* Apply filter here... */
					boardIOTable[intAdcIoID].value  = boardIOTable[intAdcIoID].rawValue;
				}
				else
				{
					boardIOTable[intAdcIoID].updateFlags |= IOM_UPDATEFLAG_OFFLINE;
				}
			}

			/* Find the next channel & select that channel */
			do
			{
				/* Next ioID round-robin */
				intAdcIoID++;
				if (intAdcIoID > intAdcIoIDEnd)
				{
					intAdcIoID = intAdcIoIDStart;
				}

				/* Is an Analogue input */
				if (BoardIOMap[intAdcIoID].ioClass == IO_CLASS_ANALOG_INPUT)
				{
					switch(BoardIOMap[intAdcIoID].ioDev)
					{
					case IO_DEV_I2C_ADC_MCP324X:
						if (convertIoIDtoI2CAdcChan(intAdcIoID, &intAdcChan) == LU_TRUE)
						{
							/* Select ADC channel for next conversion */
							selectChannelI2cAdc(BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.busI2c,
											    BoardIOMap[intAdcIoID].ioAddr.i2cAdcMCP342x.address,
											    intAdcChan);

							foundAnalog = 1;
						}
						break;

					default:
						break;
					}
				}
			} while (!foundAnalog);
		}
		break;


		default:
		break;
		}
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t convertIoIDtoI2CAdcChan(lu_uint32_t ioID, lu_uint8_t *retAdcChanPtr)
{
	lu_uint8_t retVal;

	retVal = LU_TRUE;

	switch (BoardIOMap[ioID].ioAddr.i2cAdcMCP342x.adcChan)
	{
	case I2C_ADC_CH_1:
	case I2C_ADC_CH_2:
	case I2C_ADC_CH_3:
	case I2C_ADC_CH_4:
		*retAdcChanPtr = BoardIOMap[ioID].ioAddr.i2cAdcMCP342x.adcChan;
		break;

	default:
		retVal = LU_FALSE;
		break;
	}

	return retVal;
}

 /*!
  ******************************************************************************
  *   \brief Check for I2C ADC & configure I2C if found
  *
  *   Detailed description
  *
  *   \param i2cBus - Physical I2C bus to use
  *
  *
  *   \return SB_ERROR Error code
  *
  ******************************************************************************
  */
 SB_ERROR configureCheckI2CAdc(lu_uint8_t i2cBus)
 {
 	SB_ERROR				retVal;
 	lu_uint32_t				ioID;

 	retVal = SB_ERROR_INITIALIZED;

 	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
 	{
 		/* Is Digital IO */
 		switch(BoardIOMap[ioID].ioClass)
 		{
 		case IO_CLASS_ANALOG_INPUT:
 			switch(BoardIOMap[ioID].ioDev)
 			{
 			/* Is an I2C Expander device */
 			case IO_DEV_I2C_ADC_MCP324X:
 				if (BoardIOMap[ioID].ioAddr.i2cAdcMCP342x.busI2c == i2cBus)
 				{
 					// find & configure peripheral pins
 					retVal = configureI2CPeripheral(i2cBus);
 					ioID = BOARD_IO_MAX_IDX;
 				}
 				break;

 			default:
 				break;
 			}
 			break;

 		default:
 			break;

 		}
 	}

 	return SB_ERROR_NONE;
 }


/*!
 ******************************************************************************
 *   \brief setup I2C ADC
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the MCP342X
 *   \param adcChan
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR selectChannelI2cAdc(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t adcChan)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
//	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= MCP242X_CFG_REG_CONVERT_CONT | MCP242X_CFG_REG_SAMP_240SPS |
		 						          MCP242X_CFG_REG_PGA_X1 |
		 						          (adcChan & MCP242X_CFG_REG_ADC_CHAN_MASK) <<
		 						          MCP242X_CFG_REG_ADC_CHAN_SHIFT;

		 txferCfg.sl_addr7bit     	    = (MCP342X_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= 0L;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Read ADC MCP342X
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the MCP342X
 *   \param adcChan - the Adc Channel for MCP342X
 *   \param valuePtr - return value via pointer
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cAdc(lu_uint8_t i2cBus, lu_uint8_t addr, lu_uint8_t adcChan, lu_int32_t *valuePtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_int16_t              value;
	lu_uint8_t				txData[3];
	lu_uint8_t				rxData[5];
	static lu_bool_t        failToggle = LU_FALSE;

	retError = SB_ERROR_NONE;

	switch (i2cBus)
	{
	case IO_BUS_I2C_0:
		i2cChanType = LPC_I2C0;
		break;

	case IO_BUS_I2C_1:
		i2cChanType = LPC_I2C1;
		break;

	case IO_BUS_I2C_2:
		i2cChanType = LPC_I2C2;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= MCP242X_CFG_REG_CONVERT_CONT | MCP242X_CFG_REG_SAMP_240SPS |
						                          MCP242X_CFG_REG_PGA_X1 |
						                          (adcChan & MCP242X_CFG_REG_ADC_CHAN_MASK) <<
						                          MCP242X_CFG_REG_ADC_CHAN_SHIFT;

		 txferCfg.sl_addr7bit     	    = (MCP342X_ADDR | (addr & 0x07));
		 txferCfg.tx_data		  		= 0L;
		 txferCfg.tx_length       		= 0;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 failToggle = failToggle ? 0 : 1;

			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif

			 value = rxData[1];
			 value |= ((rxData[0] & 0xf) << 8);

			 *valuePtr = value;
		 }
	 }

	return retError;
}

/*
 *********************** End of file ******************************************
 */
