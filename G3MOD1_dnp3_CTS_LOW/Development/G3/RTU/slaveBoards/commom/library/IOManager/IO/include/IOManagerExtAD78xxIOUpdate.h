/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       IO Manager ADE78xx - advanced ADC Update header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   10/11/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _IOMANAGEREXTAD78XXIOUPDATE_INCLUDED
#define _IOMANAGEREXTAD78XXIOUPDATE_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define	IOMAN_UPDATE_EXT_AD78XX_AI_MS			5

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */
typedef enum
{
    DMA_CH_0_PHASE_A = 0,
    DMA_CH_0_PHASE_B     ,
    DMA_CH_0_PHASE_C     ,
    DMA_CH_0_PHASE_N     ,

    DMA_CH_0_PHASE_LAST
}DMA_CH_0_PHASE;

/*
typedef enum
{
    ADE78XX_HSDC_CH_IAWV = 0,
    ADE78XX_HSDC_CH_VAWV    ,
    ADE78XX_HSDC_CH_IBWV    ,
    ADE78XX_HSDC_CH_VBWV    ,
    ADE78XX_HSDC_CH_ICWV    ,
    ADE78XX_HSDC_CH_VCWV    ,
    ADE78XX_HSDC_CH_INWV    ,
    ADE78XX_HSDC_CH_AVA		,
    ADE78XX_HSDC_CH_BVA		,
    ADE78XX_HSDC_CH_CVA		,
    ADE78XX_HSDC_CH_AWATT	,
    ADE78XX_HSDC_CH_BWATT	,
    ADE78XX_HSDC_CH_CWATT	,
    ADE78XX_HSDC_CH_AVAR	,
    ADE78XX_HSDC_CH_BVAR	,
    ADE78XX_HSDC_CH_CVAR	,

    ADE78XX_HSDC_CH_LAST
}ADE78XX_HSDC_CH;
*/

/*!
 * \brief ADE78xx custom decode function.
 *
 * This function is called by the FEPICSPIDMAIntHandler function.
 *
 * \param pktPtr Front End PIC data packet
 */
typedef void (*ADE78XXPacketDecode)(lu_uint32_t ioID, ModuleTimeStr canTime);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxIOInit(void);

 /*!
  ******************************************************************************
  *   \brief Register ADE78xx packet decoder
  *
  *   Detailed description
  *
  *   \param callBackFunc
  *
  *
  *   \return SB_ERROR
  *
  ******************************************************************************
  */
 extern SB_ERROR IOManagerExtADE78xxIORegisterCallBack(ADE78XXPacketDecode callBackFunc);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxIOUpdate(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxWriteReg(ADE7878WriteRegStr *ADE7878WriteRegParam);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR IOManagerExtAD78xxReadReg(ADE7878ReadRegStr *ADE7878ReadRegParam);


#endif /* _IOMANAGEREXTAD78XXIOUPDATE_INCLUDED */

/*
 *********************** End of file ******************************************
 */
