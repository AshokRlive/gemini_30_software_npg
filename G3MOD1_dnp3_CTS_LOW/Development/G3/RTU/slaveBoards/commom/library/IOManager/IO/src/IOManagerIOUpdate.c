/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   14/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "systemTime.h"

#include "Calibration.h"
#include "IOManager.h"
#include "IOManagerIO.h"
#include "IOManagerIOUpdate.h"
#include "ProcessorGPIOMap.h"

#include "BoardIOChanMap.h"
#include "BoardIOMap.h"
#include "BoardIO.h"





/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define UPDATE_GPIO(port, pin) \
	port & (1L << pin) ? 1 : 0

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR IOManagerIORunUpdateGPIO(void)
{
	lu_uint32_t	 		ioID;
	lu_uint32_t			ioChan;
	lu_uint32_t			portRead[PROC_MAX_PORT+1];
	lu_uint8_t			portIdx;
	lu_uint8_t			inputInvert;
	lu_uint16_t			dbLow2HighMs;
	lu_uint16_t			dbHigh2LowMs;
	lu_int32_t			oldValue;
	ModuleTimeStr	    canTime;
	ModuleDEventStr     digEvent;
	lu_uint32_t         time;

	time        = STGetTime();

	canTime     = CANCGetTime();

	/* Read all GPIO Input */
	for (portIdx = 0 ; portIdx <= PROC_MAX_PORT; portIdx++)
	{
		portRead[portIdx] = ProcGPIOReadPort(portIdx);
	}

	/* Perform an IO update on all IO in the ioTable[] */
	for (ioID = 0; ioID < BOARD_IO_MAX_IDX; ioID++)
	{
		/* Is an GPIO input */
		if (BoardIOMap[ioID].ioClass == IO_CLASS_DIGITAL_INPUT)
		{
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_GPIO:
			case IO_DEV_VIRTUAL:

				oldValue = boardIOTable[ioID].value;
				ioChan = BoardIOMap[ioID].ioChan;

				/* If not simulated, then use real IO operations */
				if (!(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO))
				{
					/* Input Invert for Real GPIO (Not supported for virtual IO) */
					inputInvert =
						(BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_INPUT_INVERT) ? 1 : 0;

					if (BoardIOMap[ioID].ioDev == IO_DEV_GPIO)
					{
						/* Update GPIO Input */
						boardIOTable[ioID].rawValue =
							UPDATE_GPIO(portRead[BoardIOMap[ioID].ioAddr.gpio.port], \
									BoardIOMap[ioID].ioAddr.gpio.pin);

						/* Apply Invert to raw from IOMap */
						boardIOTable[ioID].rawValue  ^= inputInvert;

						/* Set debounce limits from IOMap or IOChan */
						dbLow2HighMs = BoardIOMap[ioID].db.low2HighMs;
						dbHigh2LowMs = BoardIOMap[ioID].db.low2HighMs;
						if (ioChan != IO_CH_NA)
						{
							/* Apply invert for External equipment channel */
							inputInvert = (ioChanDIMap[ioChan].extEquipInvert) ? 1 : 0;
							boardIOTable[ioID].rawValue  ^= inputInvert;

							if (ioChanDIMap[ioChan].db.high2LowMs && ioChanDIMap[ioChan].db.low2HighMs)
							{
								dbLow2HighMs = ioChanDIMap[ioChan].db.high2LowMs;
								dbHigh2LowMs = ioChanDIMap[ioChan].db.low2HighMs;
							}
						}

						/* Debounce - override with channel db (The inverts are already applied) */
						if (dbLow2HighMs && dbHigh2LowMs)
						{
							if (boardIOTable[ioID].rawValue)
							{
								/* Low >>>>>>>> High */
								boardIOTable[ioID].countLow2High += IOMAN_UPDATE_GPIO_MS;
								if (boardIOTable[ioID].countLow2High >= dbLow2HighMs)
								{
									boardIOTable[ioID].countLow2High = 0;
									boardIOTable[ioID].value = boardIOTable[ioID].rawValue;
								}
								boardIOTable[ioID].countHigh2Low = 0;
							}
							else
							{
								/* High >>>>>>> Low */
								boardIOTable[ioID].countHigh2Low += IOMAN_UPDATE_GPIO_MS;
								if (boardIOTable[ioID].countHigh2Low >= dbHigh2LowMs)
								{
									boardIOTable[ioID].countHigh2Low = 0;
									boardIOTable[ioID].value = boardIOTable[ioID].rawValue;
								}
								boardIOTable[ioID].countLow2High = 0;
							}
						}
						else
						{
							boardIOTable[ioID].value = boardIOTable[ioID].rawValue;
						}

					} /* End:- if (BoardIOMap[ioID].ioDev == IO_DEV_GPIO) */

					/* Probably only required for real IO ? */
					if (oldValue != boardIOTable[ioID].value && ioChan != IO_CH_NA)
					{
						/* Change of state - Event pending */
						boardIOTable[ioID].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
					}

#ifdef NOT_YET_USED
					if (BoardIOMap[ioID].ioDev == IO_DEV_VIRTUAL)
					{
						/* JF - TBA - allow support for pulse generation on virtual inputs */

						if (boardIOTable[ioID].pulseMs)
						{
							if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_SINGLE)
							{
								/* Single pulse generator is enabled */

								if (STElapsedTime(boardIOTable[ioID].pulseTimerCountMs , time) > boardIOTable[ioID].pulseMs)
								{
									/* Toggle current value */
									oldValue = boardIOTable[ioID].value;
									oldValue = oldValue ? 0 : 1;
									boardIOTable[ioID].value = boardIOTable[ioID].rawValue = oldValue;

									/* Force output off */
									if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO)
									{
										oldValue = 0 ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);
									}

									/* Clear single shot pulse */
									boardIOTable[ioID].pulseTimerCountMs = 0;
									boardIOTable[ioID].pulseMs           = 0;

									boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_SINGLE;
								}
							}

							if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_REPEATED)
							{
								/* Pulse repeated generator is enabled */

								if (STElapsedTime(boardIOTable[ioID].pulseTimerCountMs , time) > boardIOTable[ioID].pulseMs)
								{
									/* Toggle current value */
									oldValue = boardIOTable[ioID].value;
									oldValue = oldValue ? 0 : 1;
									boardIOTable[ioID].value = boardIOTable[ioID].rawValue = oldValue;

									/* Reset counter for next time */
									boardIOTable[ioID].pulseTimerCountMs = time;
								}
							}
						}
					} /* End:- if (BoardIOMap[ioID].ioDev == IO_DEV_VIRTUAL) */
#endif
				}

				/* Is enabled, so report event */
				if (ioManagerIOEventingEnable == LU_TRUE &&
				    ioChanDIMap[ioChan].eventEnable &&
					(boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_INPUT_EVENT))
				{
					/* Generate an event */
					digEvent.channel 	= BoardIOMap[ioID].ioChan;
					digEvent.time		= canTime;
					digEvent.value		= boardIOTable[ioID].value;
					digEvent.isOnline   = (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OFFLINE) ? 0 : 1;
					digEvent.simulated  = (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_SIMULATE_IO) ? 1 : 0;

					CANCSendMCM( MODULE_MSG_TYPE_EVENT,
								 MODULE_MSG_ID_EVENT_DIGITAL,
								 sizeof(ModuleDEventStr),
								 (lu_uint8_t *)&digEvent);

					/* Clear the event flag */
					boardIOTable[ioID].updateFlags &= ~IOM_UPDATEFLAG_INPUT_EVENT;
				}
				break;

			default:
				break;
			}
		} /* Is IO_CLASS_DIGITAL_INPUT */

		/* Is an GPIO output */
		if (BoardIOMap[ioID].ioClass == IO_CLASS_DIGITAL_OUTPUT)
		{
			switch(BoardIOMap[ioID].ioDev)
			{
			case IO_DEV_GPIO:
				if (boardIOTable[ioID].pulseMs)
				{
					if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_SINGLE)
					{
						/* Single pulse generator is enabled */

						if (STElapsedTime(boardIOTable[ioID].pulseTimerCountMs , time) > boardIOTable[ioID].pulseMs)
						{
							/* Toggle current value */
							oldValue = boardIOTable[ioID].value;
							oldValue = oldValue ? 0 : 1;
							boardIOTable[ioID].value = boardIOTable[ioID].rawValue = oldValue;

							/* Force output off */
							if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO)
							{
								oldValue = 0 ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);
							}

							/* Clear single shot pulse */
							boardIOTable[ioID].pulseTimerCountMs = 0;
							boardIOTable[ioID].pulseMs           = 0;

							/* Write the Pin */
							ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
										  BoardIOMap[ioID].ioAddr.gpio.pin,
										  (lu_uint8_t)oldValue);

							boardIOTable[ioID].updateFlags       &= ~IOM_UPDATEFLAG_PULSE_SINGLE;
						}
					}

					if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_PULSE_REPEATED)
					{
						/* Pulse repeated generator is enabled */

						if (STElapsedTime(boardIOTable[ioID].pulseTimerCountMs , time) > boardIOTable[ioID].pulseMs)
						{
							/* Toggle current value */
							oldValue = boardIOTable[ioID].value;
							oldValue = oldValue ? 0 : 1;
							boardIOTable[ioID].value = boardIOTable[ioID].rawValue = oldValue;

							/* Force output off */
							if (boardIOTable[ioID].updateFlags & IOM_UPDATEFLAG_OUTPUT_TEMP_VALUE_ZERO)
							{
								oldValue = 0 ^ ((BoardIOMap[ioID].ioAddr.gpio.pinMode & GPIO_PM_OUTPUT_INVERT) ? 1 : 0);
							}

							/* Reset counter for next time */
							boardIOTable[ioID].pulseTimerCountMs = time;

							/* Write the Pin */
							ProcGPIOWrite(BoardIOMap[ioID].ioAddr.gpio.port,
										  BoardIOMap[ioID].ioAddr.gpio.pin,
										  (lu_uint8_t)oldValue);
						}
					}
				}
				break;

			default:
				break;
			}
		} /* is IO_CLASS_DIGITAL_OUTPUT */

	} /* for()... ioID */

	return SB_ERROR_NONE;
}


SB_ERROR IOManagerIOAIEventUpdate(void)
{
    lu_uint8_t      chanIdx;
    ModuleTimeStr   canTime;
    ModuleAEventStr analogEvent;
    lu_uint32_t     ioIDidx;

    canTime = CANCGetTime();

    /* Scan through Analogue Input channels to generate events */
    for (chanIdx = 0; chanIdx < IO_CHAN_AI_MAP_SIZE; chanIdx++)
    {
        if (ioChanAIMap[chanIdx].eventRateMs)
        {
            ioIDidx = ioChanAIMap[chanIdx].ioID;
            ioChanAIMap[chanIdx].eventRateCountMs += IOMAN_UPDATE_INAI_EVENT_MS;

            if (ioChanAIMap[chanIdx].eventRateCountMs >= ioChanAIMap[chanIdx].eventRateMs)
            {
                ioChanAIMap[chanIdx].eventRateCountMs = 0;

                /* Change of state - Event pending */
                boardIOTable[ioIDidx].updateFlags |= IOM_UPDATEFLAG_INPUT_EVENT;
            }

            /* Generate events if enabled */
            if ( ioManagerIOEventingEnable == LU_TRUE &&
            	 ioChanAIMap[chanIdx].eventEnable &&
                 (boardIOTable[ioIDidx].updateFlags & IOM_UPDATEFLAG_INPUT_EVENT)
               )
            {
                /* Generate an event for an analog channel */
                analogEvent.channel     = chanIdx;
                analogEvent.time        = canTime;
                analogEvent.value       = boardIOTable[ioIDidx].value;
                analogEvent.isOnline    = (boardIOTable[ioIDidx].updateFlags & IOM_UPDATEFLAG_OFFLINE) ? 0 : 1;

                /* convert value to calibrated value in scaled units */
                CalibrationConvertValue(ioChanAIMap[chanIdx].calID,
                		                boardIOTable[ioIDidx].value,
                		                &analogEvent.value);

                /* Send CAN event message */
                CANCSendMCM( MODULE_MSG_TYPE_EVENT,
                MODULE_MSG_ID_EVENT_ANALOGUE,
                sizeof(ModuleAEventStr),
                (lu_uint8_t *)&analogEvent);

                /* Clear the event flag */
                boardIOTable[ioIDidx].updateFlags &= ~IOM_UPDATEFLAG_INPUT_EVENT;
            }
        }
    }
    return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
