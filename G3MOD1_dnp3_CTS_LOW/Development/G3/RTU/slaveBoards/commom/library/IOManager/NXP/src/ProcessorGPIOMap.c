/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Processor specific GPIO Map module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "IOManager.h"
#include "ProcessorGPIOMap.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* LPC176xx */
const ProcGPIOMapStr GpioMap[NUM_GPIO_PINPORT_IDX] =
{
	/* Port 0 */
/*  Pin             Port            En Func#1           Func#2           Func#3 */
	{PINSEL_PORT_0,	PINSEL_PIN_0 , 	1, {FUNC_CAN_1_RX,  FUNC_UART_3_TX,  FUNC_I2C_1_SDA}},
	{PINSEL_PORT_0,	PINSEL_PIN_1 , 	1, {FUNC_CAN_1_TX,  FUNC_UART_3_RX,  FUNC_I2C_1_SCL}},
	{PINSEL_PORT_0,	PINSEL_PIN_2 , 	1, {FUNC_UART_0_TX, FUNC_AD0_CH_7,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_3 , 	1, {FUNC_UART_0_RX, FUNC_AD0_CH_6,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_4 , 	1, {0,              FUNC_CAN_2_RX,   FUNC_TIM2_CAP0}},
	{PINSEL_PORT_0,	PINSEL_PIN_5 , 	1, {0,              FUNC_CAN_2_TX,   FUNC_TIM2_CAP1}},
	{PINSEL_PORT_0,	PINSEL_PIN_6 , 	1, {0,              FUNC_SSP1_SEL,   FUNC_TIM2_MAT0}},
	{PINSEL_PORT_0,	PINSEL_PIN_7 , 	1, {0,              FUNC_SSP1_SCK,   FUNC_TIM2_MAT1}},
	{PINSEL_PORT_0,	PINSEL_PIN_8 , 	1, {0,              FUNC_SSP1_MISO,  FUNC_TIM2_MAT2}},
	{PINSEL_PORT_0,	PINSEL_PIN_9 , 	1, {0,              FUNC_SSP1_MOSI,  FUNC_TIM2_MAT2}},
	{PINSEL_PORT_0,	PINSEL_PIN_10, 	1, {FUNC_UART_2_TX, FUNC_I2C_2_SDA,  FUNC_TIM3_MAT0}},
	{PINSEL_PORT_0,	PINSEL_PIN_11, 	1, {FUNC_UART_2_RX, FUNC_I2C_2_SCL,  FUNC_TIM3_MAT1}},
	{PINSEL_PORT_0,	PINSEL_PIN_12, 	0, {0,              0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_13, 	0, {0,              0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_14, 	0, {0,              0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_15, 	1, {FUNC_UART_1_TX, FUNC_SSP0_SCK,   FUNC_SPI_SCK}},
	{PINSEL_PORT_0,	PINSEL_PIN_16, 	1, {FUNC_UART_1_RX, FUNC_SSP0_SEL,   FUNC_SPI_SEL}},
	{PINSEL_PORT_0,	PINSEL_PIN_17, 	1, {FUNC_UART_1_CTS,FUNC_SSP0_MISO,  FUNC_SPI_MISO}},
	{PINSEL_PORT_0,	PINSEL_PIN_18, 	1, {FUNC_UART_1_DCD,FUNC_SSP0_MOSI,  FUNC_SPI_MOSI}},
	{PINSEL_PORT_0,	PINSEL_PIN_19, 	1, {FUNC_UART_1_DSR,0,               FUNC_I2C_1_SDA}},
	{PINSEL_PORT_0,	PINSEL_PIN_20, 	1, {FUNC_UART_1_DTR,0,               FUNC_I2C_1_SCL}},
	{PINSEL_PORT_0,	PINSEL_PIN_21, 	1, {FUNC_UART_1_RI, 0,               FUNC_CAN_1_RX}},
	{PINSEL_PORT_0,	PINSEL_PIN_22, 	1, {FUNC_UART_1_RTS,0,               FUNC_CAN_1_TX}},
	{PINSEL_PORT_0,	PINSEL_PIN_23, 	1, {FUNC_AD0_CH_0,  0,               FUNC_TIM3_CAP0}},
	{PINSEL_PORT_0,	PINSEL_PIN_24, 	1, {FUNC_AD0_CH_1,  0,               FUNC_TIM3_CAP1}},
	{PINSEL_PORT_0,	PINSEL_PIN_25, 	1, {FUNC_AD0_CH_2,  0,               FUNC_UART_3_TX}},
	{PINSEL_PORT_0,	PINSEL_PIN_26, 	1, {FUNC_AD0_CH_3,  FUNC_DAC_A,      FUNC_UART_3_RX}},
	{PINSEL_PORT_0,	PINSEL_PIN_27, 	1, {FUNC_I2C_0_SDA, 0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_28, 	1, {FUNC_I2C_0_SCL, 0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_29, 	1, {0,              0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_30, 	1, {0,              0,               0}},
	{PINSEL_PORT_0,	PINSEL_PIN_31, 	0, {0,              0,               0}},

	/* Port 1 */
/*  Pin             Port            En Func#1           Func#2           Func#3 */
	{PINSEL_PORT_1,	PINSEL_PIN_0 , 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_1 , 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_2 , 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_3 , 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_4 , 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_5 , 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_6 , 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_7 , 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_8 , 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_9 , 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_10, 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_11, 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_12, 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_13, 	0, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_14, 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_15, 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_16, 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_17, 	1, {0,              0,               0}},
	{PINSEL_PORT_1,	PINSEL_PIN_18, 	1, {0,              FUNC_PWM1_CH1,   FUNC_TIM1_CAP0}},
	{PINSEL_PORT_1,	PINSEL_PIN_19, 	1, {0,              0,               FUNC_TIM1_CAP1}},
	{PINSEL_PORT_1,	PINSEL_PIN_20, 	1, {0,              FUNC_PWM1_CH2,   FUNC_SSP0_SCK}},
	{PINSEL_PORT_1,	PINSEL_PIN_21, 	1, {0,              FUNC_PWM1_CH3,   FUNC_SSP0_SEL}},
	{PINSEL_PORT_1,	PINSEL_PIN_22, 	1, {0,              0,               FUNC_TIM1_MAT0}},
	{PINSEL_PORT_1,	PINSEL_PIN_23, 	1, {0,              FUNC_PWM1_CH4,   FUNC_SSP0_MISO}},
	{PINSEL_PORT_1,	PINSEL_PIN_24, 	1, {0,              FUNC_PWM1_CH5,   FUNC_SSP0_MOSI}},
	{PINSEL_PORT_1,	PINSEL_PIN_25, 	1, {0,              0,               FUNC_TIM1_MAT1}},
	{PINSEL_PORT_1,	PINSEL_PIN_26, 	1, {0,              FUNC_PWM1_CH6,   FUNC_TIM0_CAP0}},
	{PINSEL_PORT_1,	PINSEL_PIN_27, 	1, {FUNC_CLKOUT,    0,               FUNC_TIM0_CAP1}},
	{PINSEL_PORT_1,	PINSEL_PIN_28, 	1, {0,              0,               FUNC_TIM0_MAT0}},
	{PINSEL_PORT_1,	PINSEL_PIN_29, 	1, {0,              0,               FUNC_TIM0_MAT1}},
	{PINSEL_PORT_1,	PINSEL_PIN_30, 	1, {0,              0,               FUNC_AD0_CH_4}},
	{PINSEL_PORT_1,	PINSEL_PIN_31, 	1, {0,              FUNC_SSP1_SCK,   FUNC_AD0_CH_5}},

	/* Port 2 */
/*  Pin             Port            En Func#1           Func#2           Func#3 */
	{PINSEL_PORT_2,	PINSEL_PIN_0 , 	1, {FUNC_PWM1_CH1,  FUNC_UART_1_TX,  0}},
	{PINSEL_PORT_2,	PINSEL_PIN_1 , 	1, {FUNC_PWM1_CH2,  FUNC_UART_1_RX,  0}},
	{PINSEL_PORT_2,	PINSEL_PIN_2 , 	1, {FUNC_PWM1_CH3,  FUNC_UART_1_CTS, 0}},
	{PINSEL_PORT_2,	PINSEL_PIN_3 , 	1, {FUNC_PWM1_CH4,  FUNC_UART_1_DCD, 0}},
	{PINSEL_PORT_2,	PINSEL_PIN_4 , 	1, {FUNC_PWM1_CH5,  FUNC_UART_1_DSR, 0}},
	{PINSEL_PORT_2,	PINSEL_PIN_5 , 	1, {FUNC_PWM1_CH6,  FUNC_UART_1_DTR, 0}},
	{PINSEL_PORT_2,	PINSEL_PIN_6 , 	1, {0,              FUNC_UART_1_RI,  0}},
	{PINSEL_PORT_2,	PINSEL_PIN_7 , 	1, {FUNC_CAN_2_RX,  FUNC_UART_1_RTS, 0}},
	{PINSEL_PORT_2,	PINSEL_PIN_8 , 	1, {FUNC_CAN_2_TX,  FUNC_UART_2_TX,  0}},
	{PINSEL_PORT_2,	PINSEL_PIN_9 , 	1, {0,              FUNC_UART_2_RX,  0}},
	{PINSEL_PORT_2,	PINSEL_PIN_10, 	1, {FUNC_EINT0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_11, 	1, {FUNC_EINT1,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_12, 	1, {FUNC_EINT2,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_13, 	1, {FUNC_EINT3,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_14, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_15, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_16, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_17, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_18, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_19, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_20, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_21, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_22, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_23, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_24, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_25, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_26, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_27, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_28, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_29, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_30, 	0, {0,              0,               0}},
	{PINSEL_PORT_2,	PINSEL_PIN_31, 	0, {0,              0,               0}},

	/* Port 3 */
/*  Pin             Port            En Func#1           Func#2           Func#3 */
	{PINSEL_PORT_3,	PINSEL_PIN_0 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_1 , 	0, {0,  	        0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_2 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_3 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_4 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_5 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_6 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_7 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_8 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_9 , 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_10, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_11, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_12, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_13, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_14, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_15, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_16, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_17, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_18, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_19, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_20, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_21, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_22, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_23, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_24, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_25, 	1, {0,              FUNC_TIM0_MAT0,  FUNC_PWM1_CH2}},
	{PINSEL_PORT_3,	PINSEL_PIN_26, 	1, {0,              FUNC_TIM0_MAT1,  FUNC_PWM1_CH3}},
	{PINSEL_PORT_3,	PINSEL_PIN_27, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_28, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_29, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_30, 	0, {0,              0,               0}},
	{PINSEL_PORT_3,	PINSEL_PIN_31, 	0, {0,              0,               0}},

	/* Port 4 */
/*  Pin             Port            En Func#1          Func#2           Func#3 */
	{PINSEL_PORT_4,	PINSEL_PIN_0 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_1 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_2 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_3 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_4 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_5 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_6 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_7 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_8 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_9 , 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_10, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_11, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_12, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_13, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_14, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_15, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_16, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_17, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_18, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_19, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_20, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_21, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_22, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_23, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_24, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_25, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_26, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_27, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_28, 	1, {0,              FUNC_TIM2_MAT0,  FUNC_UART_3_TX}},
	{PINSEL_PORT_4,	PINSEL_PIN_29, 	1, {0,              FUNC_TIM2_MAT1,  FUNC_UART_3_RX}},
	{PINSEL_PORT_4,	PINSEL_PIN_30, 	0, {0,              0,               0}},
	{PINSEL_PORT_4,	PINSEL_PIN_31, 	0, {0,              0,               0}},
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ProcGPIOConfigure(
		lu_uint8_t 		port,
		lu_uint8_t 		pin,
		IO_CLASS 		ioClass,
		lu_uint32_t 	pinMode
		)
{
	SB_ERROR  			ret;
	lu_uint8_t  		portPinIdx;
	lu_uint8_t			pinDir;
	lu_uint8_t			pinValue;
	PINSEL_CFG_Type 	pinCfg;
	lu_uint32_t	        pinShift;


	ret = SB_ERROR_INITIALIZED;

	pinDir = 0; /* Default to input */
	pinValue = 1; /* Default to high */
	if (ioClass == IO_CLASS_DIGITAL_OUTPUT || ioClass == IO_CLASS_DIGITAL_OUTPUT_SET_DIR_ONLY)
	{
		pinDir = 1; /* Output direction */
		if (pinMode & GPIO_PM_OUTPUT_LOW)
		{
			pinValue = 0; /* Set pin low */
		}
	}

	/* Set pin configuration */
	pinCfg.Funcnum = 0;
	pinCfg.Portnum = port;
	pinCfg.Pinnum = pin;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_TRISTATE;

	if (pinMode & GPIO_PM_OPEN_DRAIN)
	{
		pinCfg.OpenDrain = PINSEL_PINMODE_OPENDRAIN;
	}

	if (pinMode & GPIO_PM_PULL_UP)
	{
		pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	}
	if (pinMode & GPIO_PM_PULL_DOWN)
	{
		pinCfg.Pinmode = PINSEL_PINMODE_PULLDOWN;
	}

	pinShift = 1;
	pinShift = pinShift << (pin & 0x1f);

	/* Now do the pin configuration */
	if (port < MAX_GPIO_PORTS && pin < MAX_GPIO_PORT_BITS)
	{
		portPinIdx = pin + (port << 5);
		if (portPinIdx < NUM_GPIO_PINPORT_IDX)
		{
			if (GpioMap[portPinIdx].enable)
			{
				if (ioClass == IO_CLASS_DIGITAL_OUTPUT)
				{
					/* Set pin value */
					if (pinValue)
					{
						GPIO_SetValue(port, pinShift);
					}
					else
					{
						GPIO_ClearValue(port, pinShift);
					}
				}

				/* Configure pin type */
				PINSEL_ConfigPin(&pinCfg);

				/* Set pin direction */
				GPIO_SetDir(port, pinShift, pinDir);

				/* pin configuration was successful */
				ret = SB_ERROR_NONE;
			}
		}
	}

	return ret;
}


lu_uint8_t ProcGPIOCheckPeripheralPinPort(GPIO_PIN_FUNC pinFunc, lu_int8_t port, lu_int8_t pin)
{
	lu_uint8_t  portPinIdx;
	lu_int32_t  funcIdx;
	lu_uint8_t  retFunc;

	retFunc = 0;

	if (port < MAX_GPIO_PORTS && pin < MAX_GPIO_PORT_BITS)
	{
		portPinIdx = pin + (port << 5);

		if (portPinIdx < NUM_GPIO_PINPORT_IDX)
		{
			for (funcIdx = 0; funcIdx < PROC_MAX_FUNC_IDX; funcIdx++)
			{
				if (GpioMap[portPinIdx].enable)
				{
					if (GpioMap[portPinIdx].pinFunc[funcIdx] == pinFunc)
					{
						retFunc = (funcIdx + 1);
						break;
					}
				}
			}
		}
	}

	return retFunc;
}

lu_uint32_t ProcGPIORead(lu_uint8_t port, lu_uint8_t pin)
{
	return ((GPIO_ReadValue(port) >> pin) & 0x00000001);
}

void ProcGPIOWrite(lu_uint8_t port, lu_uint8_t pin, lu_uint8_t value)
{
	lu_uint32_t	        pinShift;

	pinShift = 1;
	pinShift = pinShift << (pin & 0x1f);

	if (value)
	{
		GPIO_SetValue(port, pinShift);
	}
	else
	{
		GPIO_ClearValue(port, pinShift);
	}
}

lu_uint32_t ProcGPIOReadPort(lu_uint8_t port)
{
	return GPIO_ReadValue(port);
}

void ProcGPIOWritePort(lu_uint8_t port, lu_uint32_t value)
{
	GPIO_SetValue(port, value);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
