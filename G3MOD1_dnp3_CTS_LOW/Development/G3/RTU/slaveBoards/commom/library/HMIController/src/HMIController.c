/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "HMIController.h"
#include "BoardIO.h"
#include "BoardIOMap.h"

#include "DACSoundDriver.h"
#include "LCDDriver.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MIN_OPMODE_LR_PRESS_MS		100  /* Min time to select Mode LR */
#define MIN_OPMODE_OFF_PRESS_MS		1000 /* Min time to select Mode Off */

#define MAX_DISPLAY_ROW				4
#define MAX_DISPLAY_COL				20

#define DISPLAY_REFRESH_TIME_MS     (20 * 1000) /* Force a full screen update */
//#define DISPLAY_REFRESH_TIME_MS     100 /* Force a full screen update */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*! HMI Keypad IO reference to Map to idId used by IOManager */
typedef enum
{
	HMI_KP_IO_ID_SOFT_LEFT_1		= 0,
	HMI_KP_IO_ID_SOFT_LEFT_2		   ,
	HMI_KP_IO_ID_SOFT_RIGHT_1		   ,
	HMI_KP_IO_ID_SOFT_RIGHT_2		   ,
	HMI_KP_IO_ID_PB_NAV_UP		   	   ,
	HMI_KP_IO_ID_PB_NAV_DOWN		   ,
	HMI_KP_IO_ID_PB_NAV_LEFT		   ,
	HMI_KP_IO_ID_PB_NAV_RIGHT		   ,
	HMI_KP_IO_ID_PB_OK		   		   ,
	HMI_KP_IO_ID_PB_MENU		       ,
	HMI_KP_IO_ID_PB_ESC		           ,
	HMI_KP_IO_ID_PB_SW_ACTIVATE        ,
	HMI_KP_IO_ID_PB_SW_OP_MODE         ,
	HMI_KP_IO_ID_PB_SW_OPEN            ,
	HMI_KP_IO_ID_PB_SW_CLOSE           ,

	HMI_KP_IO_ID_LAST
} HMI_KP_IO_ID;


/*! HMI init structure */
typedef struct HMIControllerInitDef
{
	LcdDriverInitStr	lcdInitParams;
	lu_uint32_t			ioIDKeyPad[HMI_KP_IO_ID_LAST];
	lu_bool_t			keyPadCurrent[HMI_KP_IO_ID_LAST];
	lu_bool_t			keyPadPrevious[HMI_KP_IO_ID_LAST];
	lu_bool_t			hmiStarted;
	lu_bool_t			keyClickEnable;
	lu_bool_t			changeOLREnable;
	lu_uint32_t			buttonBitMask;
	lu_uint16_t			modeLRPressReleaseTimeMs;
	lu_uint16_t			modeOPressReleaseTimeMs;
	HMI_OLR_STATE       systemOLRState;
	lu_uint32_t			displaySyncTimerMs;
	lu_bool_t			displayUpdateNow;
	lu_bool_t			displayFullUpdate;
	lu_int8_t			lcdBufferSyncSrc[MAX_DISPLAY_ROW][MAX_DISPLAY_COL+1];  /* Scratch buffer */
	lu_int8_t			lcdBufferSyncDst[MAX_DISPLAY_ROW][MAX_DISPLAY_COL+1];  /* Screen buffer */
}HMIControllerInitStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR HMIDisplayOLR(HmiDisplayOLRStr *hmiDisplayOLRPtr);
SB_ERROR HMICmdPlaySound(HmiPlaySoundStr *hmiPlaySoundPtr);
SB_ERROR HMIGetDisplayInfo(void);
SB_ERROR HMIGetButtonState(void);
SB_ERROR HMIWriteLED(HmiLEDWriteStr *hmiLEDWritePtr);
SB_ERROR HMIDisplayWrite(HmiDisplayWriteStr *hmiDisplayWritePtr, lu_uint32_t msgLen);
SB_ERROR HMIDisplayClear(HmiDisplayClearStr *hmiDisplayClearPtr);
SB_ERROR HMIDisplaySync(HmiDisplaySyncStr *hmiDisplaySyncPtr);

SB_ERROR HMIConfig(HMIConfigStr *hmiConfigPtr);

static SB_ERROR HMIPlaySound(lu_uint32_t freq, lu_uint16_t durationMs);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static HMIControllerInitStr HMIConInitParams ;
static lu_uint16_t		    hmiSoundTimer = 0;

/*! List of supported CAN messages */
static const filterTableStr HMIControllerModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! HMI commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_C           , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_C            , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_C      , LU_FALSE , 0   	  },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_LED_WRITE_C             , LU_FALSE , 0   	  },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_C         , LU_FALSE , 0   	  },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_C         , LU_FALSE , 1   	  },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_C         , LU_FALSE , 0   	  },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_C          , LU_FALSE , 0   	  },

    /*! HMI Config */
	{  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_HMI_CONTROLLER_C     		  , LU_FALSE , 0   	  }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR HMIControllerInit(void)
{
	lu_uint32_t	idx,rowIdx,colIdx;
	SB_ERROR sbError;

	/* Init sound driver */
	DACSoundDriverInit(IO_ID_DAC_OUT);

	/* Set contrast control */
	IOManagerSetValue(IO_ID_LCD_VO, 0xff);

	/* Init LCD */
	LCDDriverInitStructure(&HMIConInitParams.lcdInitParams);

#ifdef LCD_DEBUG
	/* Turn on cursor */
	HMIConInitParams.lcdInitParams.lcdInitMode |= (LCD_INIT_MODE_CURSOR_ON | LCD_INIT_MODE_BLINK_ON);
#endif

	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_RW]  = IO_ID_LCD_RW;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_EN]  = IO_ID_LCD_EN;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_RS]  = IO_ID_LCD_RS;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB0] = IO_ID_LCD_DB0;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB1] = IO_ID_LCD_DB1;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB2] = IO_ID_LCD_DB2;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB3] = IO_ID_LCD_DB3;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB4] = IO_ID_LCD_DB4;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB5] = IO_ID_LCD_DB5;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB6] = IO_ID_LCD_DB6;
	HMIConInitParams.lcdInitParams.ioID[LCD_IO_ID_DB7] = IO_ID_LCD_DB7;

	LCDDriverInit(&HMIConInitParams.lcdInitParams);

	LCDPutString(&HMIConInitParams.lcdInitParams, "Lucy Electric- HMI", 0, 0);
	LCDPutString(&HMIConInitParams.lcdInitParams, "Waiting for START...", 0, 2);

	// Turn off amp
	//IOManagerSetValue(IO_ID_AMP_SHDN, 1);

	for (idx = 0; idx < HMI_KP_IO_ID_LAST; idx++)
	{
		HMIConInitParams.ioIDKeyPad[idx] = BOARD_IO_MAX_IDX;
		HMIConInitParams.keyPadCurrent[idx] = LU_FALSE;
		HMIConInitParams.keyPadPrevious[idx] = LU_FALSE;
	}

	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_SOFT_LEFT_1] 	 = IO_ID_PB_SOFT_LEFT_1;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_SOFT_LEFT_2] 	 = IO_ID_PB_SOFT_LEFT_2;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_SOFT_RIGHT_1]   = IO_ID_PB_SOFT_RIGHT_1;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_SOFT_RIGHT_2]   = IO_ID_PB_SOFT_RIGHT_2;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_NAV_UP] 	 = IO_ID_PB_NAV_UP;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_NAV_DOWN] 	 = IO_ID_PB_NAV_DOWN;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_NAV_LEFT] 	 = IO_ID_PB_NAV_LEFT;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_NAV_RIGHT]   = IO_ID_PB_NAV_RIGHT;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_OK] 		 = IO_ID_PB_OK;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_MENU] 		 = IO_ID_PB_MENU;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_ESC]         = IO_ID_PB_ESC;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_SW_ACTIVATE] = IO_ID_PB_SW_ACTIVATE;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_SW_OP_MODE]  = IO_ID_PB_SW_OP_MODE;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_SW_OPEN]     = IO_ID_PB_SW_OPEN;
	HMIConInitParams.ioIDKeyPad[HMI_KP_IO_ID_PB_SW_CLOSE]    = IO_ID_PB_SW_CLOSE;

	hmiSoundTimer = 0;

	/* Init config parameters */
	HMIConInitParams.keyClickEnable = LU_FALSE;
	HMIConInitParams.modeLRPressReleaseTimeMs = MIN_OPMODE_LR_PRESS_MS;
	HMIConInitParams.modeOPressReleaseTimeMs = MIN_OPMODE_OFF_PRESS_MS;
	HMIConInitParams.changeOLREnable = LU_FALSE;
	HMIConInitParams.systemOLRState = HMI_OLR_STATE_INVALID;
	HMIConInitParams.buttonBitMask = 0;
	HMIConInitParams.displaySyncTimerMs = 0;
	HMIConInitParams.displayUpdateNow  = LU_FALSE;
	HMIConInitParams.displayFullUpdate = LU_TRUE;

	/* HMI not ready until start message */
	HMIConInitParams.hmiStarted = LU_FALSE;

	/* Clear LCD buffers */
	for (rowIdx = 0; rowIdx < MAX_DISPLAY_ROW; rowIdx++)
	{
		HMIConInitParams.lcdBufferSyncSrc[rowIdx][MAX_DISPLAY_COL] = 0; /* Null terminate string */
		HMIConInitParams.lcdBufferSyncDst[rowIdx][MAX_DISPLAY_COL] = 0; /* Null terminate string */
		for (colIdx = 0; colIdx < MAX_DISPLAY_COL; colIdx++)
		{
			HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx] = 0;
			HMIConInitParams.lcdBufferSyncDst[rowIdx][colIdx] = 0;
		}
	}

	/* Setup CAN Msg filter */
	sbError = CANFramingAddFilter( HMIControllerModulefilterTable,
								   SU_TABLE_SIZE(HMIControllerModulefilterTable,
											     filterTableStr)
								 );

	return sbError;
}

SB_ERROR HMIControllerStart(void)
{
	LCDClearDisplay(&HMIConInitParams.lcdInitParams);

	/* Now allow button events & OLR mode control (if enabled) */
	HMIConInitParams.hmiStarted = LU_TRUE;

	HMIConInitParams.displaySyncTimerMs = DISPLAY_REFRESH_TIME_MS;

	return SB_ERROR_NONE;
}

SB_ERROR HMIConSoundTick(void)
{
	if (hmiSoundTimer > HMICON_SOUND_TICK_RATE_MS)
	{
		hmiSoundTimer -= HMICON_SOUND_TICK_RATE_MS;

		if (!hmiSoundTimer)
		{
			DACSoundStopSound();
		}
	}
	else
	{
		if (hmiSoundTimer)
		{
			hmiSoundTimer = 0;
			DACSoundStopSound();
		}
	}

	return SB_ERROR_NONE;
}

SB_ERROR HMIControllerTick(void)
{
	lu_int32_t	value;
	lu_bool_t   keypressed;
	lu_uint32_t	idx,rowIdx,colIdx;
	lu_bool_t  keyPressed;
	lu_int8_t chrStr[3];
	HmiButtonEventStr       buttonEvent;
	HmiReqOLRChangeEventStr reqOLREvent;
	static lu_uint8_t opMode = 0;
	static lu_uint16_t opModeTimer = 0;

	/* Scan keypad IO */
	for (idx = 0; idx < HMI_KP_IO_ID_LAST; idx++)
	{
		if (IOManagerGetValue(HMIConInitParams.ioIDKeyPad[idx], &value) == SB_ERROR_NONE)
		{
			HMIConInitParams.keyPadCurrent[idx] = value;
		}
	}

	/* Detect edge on key press */
	keyPressed = LU_FALSE;
	HMIConInitParams.buttonBitMask = 0;
	for (idx = 0; idx < HMI_KP_IO_ID_LAST; idx++)
	{
		switch (idx)
		{
		case HMI_KP_IO_ID_SOFT_LEFT_1:
		case HMI_KP_IO_ID_SOFT_LEFT_2:
		case HMI_KP_IO_ID_SOFT_RIGHT_1:
		case HMI_KP_IO_ID_SOFT_RIGHT_2:
		case HMI_KP_IO_ID_PB_NAV_UP:
		case HMI_KP_IO_ID_PB_NAV_DOWN:
		case HMI_KP_IO_ID_PB_NAV_LEFT:
		case HMI_KP_IO_ID_PB_NAV_RIGHT:
		case HMI_KP_IO_ID_PB_OK:
		case HMI_KP_IO_ID_PB_MENU:
		case HMI_KP_IO_ID_PB_ESC:
		case HMI_KP_IO_ID_PB_SW_ACTIVATE:
		case HMI_KP_IO_ID_PB_SW_CLOSE:
		case HMI_KP_IO_ID_PB_SW_OPEN:
			if (HMIConInitParams.keyPadCurrent[idx] == LU_TRUE)
			{
				HMIConInitParams.buttonBitMask |= (1 << idx);
			}
			break;

		default:
			break;
		}
		if (HMIConInitParams.keyPadCurrent[idx] != HMIConInitParams.keyPadPrevious[idx])
		{
			/* Key pressed */
			switch (idx)
			{
			case HMI_KP_IO_ID_SOFT_LEFT_1:
			case HMI_KP_IO_ID_SOFT_LEFT_2:
			case HMI_KP_IO_ID_SOFT_RIGHT_1:
			case HMI_KP_IO_ID_SOFT_RIGHT_2:
			case HMI_KP_IO_ID_PB_NAV_UP:
			case HMI_KP_IO_ID_PB_NAV_DOWN:
			case HMI_KP_IO_ID_PB_NAV_LEFT:
			case HMI_KP_IO_ID_PB_NAV_RIGHT:
			case HMI_KP_IO_ID_PB_OK:
			case HMI_KP_IO_ID_PB_MENU:
			case HMI_KP_IO_ID_PB_ESC:
			case HMI_KP_IO_ID_PB_SW_ACTIVATE:
			case HMI_KP_IO_ID_PB_SW_CLOSE:
			case HMI_KP_IO_ID_PB_SW_OPEN:
				keyPressed = LU_TRUE;
				break;

			default:
				break;
			}

			if (HMIConInitParams.keyPadCurrent[idx] == LU_TRUE)
			{
				/* kEY DOWN */
				switch (idx)
				{
				case HMI_KP_IO_ID_SOFT_LEFT_1:
				case HMI_KP_IO_ID_SOFT_LEFT_2:
				case HMI_KP_IO_ID_SOFT_RIGHT_1:
				case HMI_KP_IO_ID_SOFT_RIGHT_2:
				case HMI_KP_IO_ID_PB_NAV_UP:
				case HMI_KP_IO_ID_PB_NAV_DOWN:
				case HMI_KP_IO_ID_PB_NAV_LEFT:
				case HMI_KP_IO_ID_PB_NAV_RIGHT:
				case HMI_KP_IO_ID_PB_OK:
				case HMI_KP_IO_ID_PB_MENU:
				case HMI_KP_IO_ID_PB_ESC:
				case HMI_KP_IO_ID_PB_SW_ACTIVATE:
				case HMI_KP_IO_ID_PB_SW_CLOSE:
				case HMI_KP_IO_ID_PB_SW_OPEN:
					keyPressed = LU_TRUE;
					if (HMIConInitParams.keyClickEnable)
					{
						HMIPlaySound(50, 20);
					}
					break;

				case HMI_KP_IO_ID_PB_SW_OP_MODE:
					/* Reset MODE button timer */
					opModeTimer = 0;
					break;

				default:
					break;
				}
			}
		}
		/* Save previous key state */
		HMIConInitParams.keyPadPrevious[idx] = HMIConInitParams.keyPadCurrent[idx];
	}

	/* HMI button events */
	if (HMIConInitParams.hmiStarted)
	{
		if (keyPressed == LU_TRUE)
		{
			buttonEvent.state.buttonBitMask = HMIConInitParams.buttonBitMask;

			/* Generate CAN message */
			CANCSendMCM( MODULE_MSG_TYPE_HMI_EVENT,
					     MODULE_MSG_ID_HMI_EVENT_BUTTON,
						 sizeof(HmiButtonEventStr),
						 (lu_uint8_t *)&buttonEvent
					   );
		}
	}

	/* Read the Mode button */
	if (HMIConInitParams.changeOLREnable == LU_TRUE)
	{
		if (IOManagerGetValue(IO_ID_PB_SW_OP_MODE, &value) == SB_ERROR_NONE)
		{
			if (value)
			{
				if (opModeTimer != 0xffff)
				{
					opModeTimer += HMICON_TICK_RATE_MS;
					if (opModeTimer >= HMIConInitParams.modeOPressReleaseTimeMs)
					{
						opModeTimer = 0xffff;

						if (HMIConInitParams.keyClickEnable)
						{
							HMIPlaySound(50, 20);
						}

						/* request OFF */
						reqOLREvent.requestedHMIOLRState = HMI_OLR_STATE_OFF;

						/* Generate CAN message */
						CANCSendMCM( MODULE_MSG_TYPE_HMI_EVENT,
									 MODULE_MSG_ID_HMI_EVENT_REQ_OLR_CHANGE,
									 sizeof(HmiReqOLRChangeEventStr),
									 (lu_uint8_t *)&reqOLREvent
								   );
					}
				}
			}
			else
			{
				if (opModeTimer != 0xffff && opModeTimer >= HMIConInitParams.modeLRPressReleaseTimeMs)
				{
					/* request toggle Local - Remote */
					switch (HMIConInitParams.systemOLRState)
					{
					case HMI_OLR_STATE_OFF:
					case HMI_OLR_STATE_LOCAL:
					case HMI_OLR_STATE_REMOTE:

						if (HMIConInitParams.keyClickEnable)
						{
							HMIPlaySound(50, 10);
						}

						if (HMIConInitParams.systemOLRState == HMI_OLR_STATE_LOCAL)
						{
							reqOLREvent.requestedHMIOLRState = HMI_OLR_STATE_REMOTE;
						}
						else
						{
							reqOLREvent.requestedHMIOLRState = HMI_OLR_STATE_LOCAL;
						}

						/* Generate CAN message */
						CANCSendMCM( MODULE_MSG_TYPE_HMI_EVENT,
									 MODULE_MSG_ID_HMI_EVENT_REQ_OLR_CHANGE,
									 sizeof(HmiReqOLRChangeEventStr),
									 (lu_uint8_t *)&reqOLREvent
								   );
						break;

					default:
						break;
					}
				}

				/* Reset the timer */
				opModeTimer = 0;
			}
		}
	}

	/* Update OLR Mode LED's */
	switch (HMIConInitParams.systemOLRState)
	{
	case HMI_OLR_STATE_OFF:
		IOManagerSetValue(IO_ID_LED_OFF, 1);
		IOManagerSetValue(IO_ID_LED_LOCAL, 0);
		IOManagerSetValue(IO_ID_LED_REMOTE, 0);
		break;

	case HMI_OLR_STATE_LOCAL:
		IOManagerSetValue(IO_ID_LED_OFF, 0);
		IOManagerSetValue(IO_ID_LED_LOCAL, 1);
		IOManagerSetValue(IO_ID_LED_REMOTE, 0);
		break;

	case HMI_OLR_STATE_REMOTE:
		IOManagerSetValue(IO_ID_LED_OFF, 0);
		IOManagerSetValue(IO_ID_LED_LOCAL, 0);
		IOManagerSetValue(IO_ID_LED_REMOTE, 1);
		break;

	default:
		IOManagerSetValue(IO_ID_LED_OFF, 0);
		IOManagerSetValue(IO_ID_LED_LOCAL, 0);
		IOManagerSetValue(IO_ID_LED_REMOTE, 0);
		break;
	}

	/* Update LCD display timer */
	if (HMIConInitParams.displaySyncTimerMs)
	{
		if (HMIConInitParams.displaySyncTimerMs > HMICON_TICK_RATE_MS)
		{
			HMIConInitParams.displaySyncTimerMs -= HMICON_TICK_RATE_MS;
		}
		else
		{
			HMIConInitParams.displaySyncTimerMs = 0;
		}

		if (!HMIConInitParams.displaySyncTimerMs)
		{
			/* Set to update the display */
			HMIConInitParams.displayUpdateNow  = LU_TRUE;
			HMIConInitParams.displayFullUpdate = LU_TRUE;
		}
	}

	/* LCD screen update */
	if (HMIConInitParams.displayUpdateNow == LU_TRUE)
	{
		for (rowIdx = 0; rowIdx < MAX_DISPLAY_ROW; rowIdx++)
		{
			/* Update the display - Full screen write */
			if (HMIConInitParams.displayFullUpdate == LU_TRUE)
			{
				/* Full update of each line */
				LCDPutString(&HMIConInitParams.lcdInitParams, &HMIConInitParams.lcdBufferSyncSrc[rowIdx][0], 0, rowIdx);
			}

			for (colIdx = 0; colIdx < MAX_DISPLAY_COL; colIdx++)
			{
				/* Update the display - Update changes only */
				if (HMIConInitParams.displayFullUpdate == LU_FALSE)
				{
					/* only update what has changed */
					if (HMIConInitParams.lcdBufferSyncDst[rowIdx][colIdx] != HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx])
					{
						chrStr[0] = HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx];
						chrStr[1] = '\0';
						LCDPutString(&HMIConInitParams.lcdInitParams, chrStr, colIdx, rowIdx);
					}
				}

				/* Copy display buffers src --> dst */
				HMIConInitParams.lcdBufferSyncDst[rowIdx][colIdx] = HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx];
			}
		}

		/* clear flags & restart screen update timer */
		HMIConInitParams.displayUpdateNow  = LU_FALSE;
		HMIConInitParams.displayFullUpdate = LU_FALSE;

		HMIConInitParams.displaySyncTimerMs = DISPLAY_REFRESH_TIME_MS;
	}

	return SB_ERROR_NONE;
}


SB_ERROR HMIControllerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC;

	LU_UNUSED( time );

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_HMI_CONTROLLER_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(HMIConfigStr))
			{
				retError = HMIConfig((HMIConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(HmiDisplayOLRStr))
			{
				retError = HMIDisplayOLR((HmiDisplayOLRStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(HmiPlaySoundStr))
			{
				retError = HMICmdPlaySound((HmiPlaySoundStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_C:
			retError = HMIGetDisplayInfo();
			break;

		case MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_C:
			retError = HMIGetButtonState();
			break;

		case MODULE_MSG_ID_CMD_HMC_LED_WRITE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(HmiLEDWriteStr))
			{
				retError = HMIWriteLED((HmiLEDWriteStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_C:
			/* Message sanity check */
			if (msgPtr->msgLen > MODULE_MESSAGE_SIZE(HmiDisplayWriteStr))
			{
				retError = HMIDisplayWrite((HmiDisplayWriteStr *)msgPtr->msgBufPtr, msgPtr->msgLen);
			}
			break;

		case MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(HmiDisplayClearStr))
			{
				retError = HMIDisplayClear((HmiDisplayClearStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(HmiDisplaySyncStr))
			{
				retError = HMIDisplaySync((HmiDisplaySyncStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIDisplayOLR(HmiDisplayOLRStr *hmiDisplayOLRPtr)
{
	ModReplyStr reply;

	if (HMIConInitParams.hmiStarted)
	{
		switch (hmiDisplayOLRPtr->displayHMIOLRState)
		{
		case HMI_OLR_STATE_OFF:
		case HMI_OLR_STATE_LOCAL:
		case HMI_OLR_STATE_REMOTE:
			HMIConInitParams.systemOLRState = hmiDisplayOLRPtr->displayHMIOLRState;
			reply.status = REPLY_STATUS_OKAY;
			break;

		default:
			reply.status = REPLY_STATUS_PARAM_ERROR;
			break;
		}
	}
	else
	{
		reply.status =  REPLY_STATUS_NOT_ONLINE_ERROR;
	}

	reply.channel = 0; /* Not used */

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_HMC_DISPLAY_OLR_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMICmdPlaySound(HmiPlaySoundStr *hmiPlaySoundPtr)
{
	ModReplyStr reply;

	reply.status = REPLY_STATUS_OKAY;
	reply.channel = 0; /* Not used */

	HMIPlaySound(hmiPlaySoundPtr->frequencyHz, hmiPlaySoundPtr->durationMs);

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_HMC_PLAY_SOUND_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIGetDisplayInfo(void)
{
	HmiDisplayInfoReplyStr replyDisplayInfo;

	replyDisplayInfo.numColums = HMIConInitParams.lcdInitParams.lcdNumColumns;
	replyDisplayInfo.numRows   = HMIConInitParams.lcdInitParams.lcdNumRows;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_HMC_GET_DISPLAY_INFO_R,
						sizeof(HmiDisplayInfoReplyStr),
						(lu_uint8_t *)&replyDisplayInfo
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIGetButtonState(void)
{
	HmiButtonStateReplyStr replyButtonState;

	replyButtonState.state.buttonBitMask = HMIConInitParams.buttonBitMask;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_HMC_GET_BUTTON_STATE_R,
						sizeof(HmiButtonStateReplyStr),
						(lu_uint8_t *)&replyButtonState
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIWriteLED(HmiLEDWriteStr *hmiLEDWritePtr)
{
	ModReplyStr reply;

	if (hmiLEDWritePtr->ledBitMask & 0x01)
	{
		IOManagerSetValue(IO_ID_LED_SOFT_LEFT_1, 1);
	}
	else
	{
		IOManagerSetValue(IO_ID_LED_SOFT_LEFT_1, 0);
	}

	if (hmiLEDWritePtr->ledBitMask & 0x02)
	{
		IOManagerSetValue(IO_ID_LED_SOFT_LEFT_2, 1);
	}
	else
	{
		IOManagerSetValue(IO_ID_LED_SOFT_LEFT_2, 0);
	}

	if (hmiLEDWritePtr->ledBitMask & 0x04)
	{
		IOManagerSetValue(IO_ID_LED_SOFT_RIGHT_1, 1);
	}
	else
	{
		IOManagerSetValue(IO_ID_LED_SOFT_RIGHT_1, 0);
	}

	if (hmiLEDWritePtr->ledBitMask & 0x08)
	{
		IOManagerSetValue(IO_ID_LED_SOFT_RIGHT_2, 1);
	}
	else
	{
		IOManagerSetValue(IO_ID_LED_SOFT_RIGHT_2, 0);
	}

	reply.channel = 0; /* Not used */
	reply.status = REPLY_STATUS_OKAY;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_HMC_LED_WRITE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIDisplayWrite(HmiDisplayWriteStr *hmiDisplayWritePtr, lu_uint32_t msgLen)
{
	ModReplyStr reply;
	lu_uint32_t	rowIdx,colIdx,numChars;
	lu_uint8_t  *dataPtr;
	lu_uint32_t dataLen;

	reply.status = REPLY_STATUS_OKAY;
	reply.channel = 0; /* Not used */

	/* Calculate expected payload data length */
	dataLen = (msgLen - MODULE_MESSAGE_SIZE(HmiDisplayWriteStr));

	/* Check message length is correct */
	if (dataLen == hmiDisplayWritePtr->bufferLen)
	{
		HMIConInitParams.displaySyncTimerMs = DISPLAY_REFRESH_TIME_MS;

		numChars = hmiDisplayWritePtr->bufferLen;
		rowIdx   = hmiDisplayWritePtr->posRow;
		colIdx	 = hmiDisplayWritePtr->posColumn;
		dataPtr  = ((lu_uint8_t *)hmiDisplayWritePtr + sizeof(HmiDisplayWriteStr));

		while (numChars)
		{
			/* Copy data to buffer */
			HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx] = *dataPtr++;

			/* Increment col/row index pointers */
			colIdx++;
			if (colIdx >= HMIConInitParams.lcdInitParams.lcdNumColumns)
			{
				colIdx = 0;
				rowIdx++;
			}
			if (rowIdx >= HMIConInitParams.lcdInitParams.lcdNumRows)
			{
				rowIdx = 0;
			}
			numChars--;
		};

		/*! Send CAN reply message */
		return CANCSendMCM( MODULE_MSG_TYPE_CMD,
							MODULE_MSG_ID_CMD_HMC_DISPLAY_WRITE_R,
							sizeof(ModReplyStr),
							(lu_uint8_t *)&reply
						  );
	}
	else
	{
		/* Incorrectly coded message */
		return SB_ERROR_CANC;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIDisplayClear(HmiDisplayClearStr *hmiDisplayClearPtr)
{
	ModReplyStr reply;
	lu_uint32_t	rowIdx,colIdx,numChars;

	reply.status = REPLY_STATUS_OKAY;
	reply.channel = 0; /* Not used */

	HMIConInitParams.displaySyncTimerMs = DISPLAY_REFRESH_TIME_MS;

	if (hmiDisplayClearPtr->numChars == 0)
	{
		/* Clear the whole display */
		LCDClearDisplay(&HMIConInitParams.lcdInitParams);

		/* Now wipe out the display cache buffers */
		for (rowIdx = 0; rowIdx < MAX_DISPLAY_ROW; rowIdx++)
		{
			for (colIdx = 0; colIdx < MAX_DISPLAY_COL; colIdx++)
			{
				HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx] = 0;
				HMIConInitParams.lcdBufferSyncDst[rowIdx][colIdx] = 0;
			}
		}
	}
	else
	{
		numChars = hmiDisplayClearPtr->numChars;
		rowIdx   = hmiDisplayClearPtr->posRow;
		colIdx	 = hmiDisplayClearPtr->posColumn;

		while (numChars)
		{
			/* Clear buffer  with spaces */
			HMIConInitParams.lcdBufferSyncSrc[rowIdx][colIdx] = ' ';

			/* Increment col/row index pointers */
			colIdx++;
			if (colIdx >= HMIConInitParams.lcdInitParams.lcdNumColumns)
			{
				colIdx = 0;
				rowIdx++;
			}
			if (rowIdx >= HMIConInitParams.lcdInitParams.lcdNumRows)
			{
				rowIdx = 0;
			}
			numChars--;
		};
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
			MODULE_MSG_ID_CMD_HMC_DISPLAY_CLEAR_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIDisplaySync(HmiDisplaySyncStr *hmiDisplaySyncPtr)
{
	ModReplyStr reply;

	reply.status = REPLY_STATUS_OKAY;
	reply.channel = 0; /* Not used */

	if (hmiDisplaySyncPtr->forceFullUpdate)
	{
		HMIConInitParams.displayFullUpdate = LU_TRUE;
	}
	HMIConInitParams.displayUpdateNow = LU_TRUE;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_HMC_DISPLAY_SYNC_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR HMIConfig(HMIConfigStr *hmiConfigPtr)
{
	ModReplyStr reply;

	/* Set config parameters */
	HMIConInitParams.keyClickEnable = hmiConfigPtr->buttonClickEnable ? LU_TRUE : LU_FALSE;
	HMIConInitParams.changeOLREnable = hmiConfigPtr->reqOLREnable ? LU_TRUE : LU_FALSE;
	if ( hmiConfigPtr->timeModeOffMs      > MIN_OPMODE_OFF_PRESS_MS &&
	     hmiConfigPtr->timeModeToggleLRMs > MIN_OPMODE_LR_PRESS_MS
	   )
	{
		HMIConInitParams.modeLRPressReleaseTimeMs = hmiConfigPtr->timeModeToggleLRMs;
		HMIConInitParams.modeOPressReleaseTimeMs  = hmiConfigPtr->timeModeOffMs;
	}

	reply.channel = 0; /* Not used */
	reply.status = REPLY_STATUS_OKAY;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_HMI_CONTROLLER_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR HMIPlaySound(lu_uint32_t freq, lu_uint16_t durationMs)
{
	if (!hmiSoundTimer)
	{
		hmiSoundTimer = durationMs;
		DACSoundPlaySound(freq);
	}

	return SB_ERROR_NONE;
}


/*
 *********************** End of file ******************************************
 */
