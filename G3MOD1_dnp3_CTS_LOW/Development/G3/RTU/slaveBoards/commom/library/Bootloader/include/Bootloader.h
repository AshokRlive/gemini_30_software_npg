/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "CANProtocolCodec.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOOTLOADER_LED_FLASH_MS						200

#define BOOTLOADER_APP_START_TIMEOUT_MS				1000 // Timeout to auto start app

#define BOOTLOADER_WATCHDOG_KICK_MS					500

#define BOOTLOADER_MAGICSYNC						0x4afcc0de

#define PIN_SHIFT(pin)                             (1 << pin)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	BL_CMD_START_APP = 0x22,

	BL_CMD_LAST
}BL_CMD;

typedef struct BootloaderCommandDef
{
	lu_int32_t magicSync;
	lu_int32_t command;
	lu_int32_t Parameter;
}BootloaderCommandStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialisation of bootloader API
 *
 *   Initialisation of CAN messages for bootloader and board specific parameters.
 *
 *   \param None
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR BootloaderInit(void);

/*!
 ******************************************************************************
 *   \brief CAN Protocol decoder for bootloader
 *
 *   Handling all bootloader commands
 *
 *   \param msgPtr Pointer to CAN message
 *   \param time   Time from CAN controller
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR BootloaderCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief Initialisation of board specific bootloader parameters
 *
 *   Initialisation of board specific bootloader parameters such as starting address of
 *   user application etc,.
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR BoardBootloaderInit(void);

/*!
 ******************************************************************************
 *   \brief Calculate Module ID address
 *
 *   Get the Module ID Address
 *
 *   \param valSel1
 *   \param valSel2
 *   \param valSel3
 *   \param valSel4
 *
 *   \return MODULE_ID
 *
 ******************************************************************************
 */
extern MODULE_ID BootloaderCalcModuleID(lu_uint32_t valSel1, lu_uint32_t valSel2, lu_uint32_t valSel3, lu_uint32_t valSel4);

/*!
 ******************************************************************************
 *   \brief Save bootcommand to RAM
 *
 *   Save bootcommand to RAM
 *
 *   \param none
 *
 *   \return
 *
 ******************************************************************************
 */
extern void BootloaderStartCommand(void);

/*!
 ******************************************************************************
 *   \brief Check for boot command
 *
 *   Check for boot command
 *
 *   \param none
 *
 *   \return
 *
 ******************************************************************************
 */
extern void BootloaderCheckStartCommand(void);

/*!
 ******************************************************************************
 *   \brief Check if requested to start application
 *
 *   Is application start requested ?
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern lu_bool_t BootloaderGetStartApplicationNow(void);

/*!
 ******************************************************************************
 *   \brief Start Application now
 *
 *   Start the application
 *
 *   \param None
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR BootloaderStartApp(void);

#endif /* BOOTLOADER_H_*/

/*
 *********************** End of file ******************************************
 */

