/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef NXP17xxFLASHDRIVER_H_
#define NXP17xxFLASHDRIVER_H_

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "systemUtils.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define CCLK 						  48000 			/* 48,000 KHz for IAP call */
#define NUM_FLASH_SECTOR        	  30                /* What does this define mean (Name is ambigious) ??? */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */
/*******************************************************************************
 *   \brief Writing the given data to the specified flash address of LPC17xx
 *
 *	 Writing the number of data located in buffer into flash address.
 *
 *   \param flashAddress      The address where the data to be stored
 *   \param flashDataBuffer   Pointer to data buffer
 *   \param count        	  Number of bytes
 *
 *   \return Errorcode
 *
 ******************************************************************************
 */
extern SB_ERROR NXP17xxFlashWrite(lu_uint32_t flashAddress, lu_uint32_t *flashDataBuffer, lu_uint16_t count);

 /*******************************************************************************
  *   \brief Reading data from the specified flash address of LPC17xx to the given buffer
  *
  *	 Writing the number of data located in buffer into flash address.
  *
  *   \param flashAddress      The address where the data to be stored
  *   \param flashDataBuffer   Pointer to data buffer
  *   \param count        	   Number of bytes
  *
  *   \return Errorcode
  *
  ******************************************************************************
  */
extern SB_ERROR NXP17xxFlashRead(lu_uint32_t flashAddress, lu_uint32_t *flashDataBuffer, lu_uint16_t count);

/*******************************************************************************
  *   \brief Erasing specified size of data from flash memory
  *
  *	   Erasing the sector corresponding to the given flash address
  *
  *   \param flashAddress      The address where the data to be erased
  *
  *   \return Errorcode
  *
  ******************************************************************************
  */
extern SB_ERROR NXP17xxFlashErase(lu_uint32_t flashAddress);

#endif /* NXP17xxFLASHDRIVER_H_ */

/*
 *********************** End of file ******************************************
 */
