/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include "LPC17xx.h"

#include "lpc17xx_nvic.h"
#include "lpc17xx_can.h"
#include "string.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "lpc17xx_wdt.h"
#include "errorCodes.h"
#include "Bootloader.h"
#include "BoardBootloader.h"
#include "NXP17xxFlashDriver.h"
#include "BootNvRam.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define USER_APP_START_ADDR           BRD_BL_APP_FLASH_SECTOR
#define DATA_BLOCK256                 256

#define CAN_BASE(a)        ((LPC_CAN_TypeDef*)(a))

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef void (*USER_APP_MAIN)(void);

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR WriteFirmware(BootloaderFirmwareStr *dataToBeFlashed);
SB_ERROR ReadFirmware(lu_uint16_t *blockOffsetToRead);
SB_ERROR EraseFirmware(void);
SB_ERROR StartSlaveBoardApplication(void);
SB_ERROR GetFlashParams(void);
void setSP(lu_uint32_t address);
SB_ERROR WriteBootLoaderFirmware(lu_uint8_t *srcAddr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported message */
static const filterTableStr BootloaderModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */
	/*! Bootloader test API commands */
    {  MODULE_MSG_TYPE_BL, MODULE_MSG_ID_BL_WRITE_FIRMWARE_C     , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BL, MODULE_MSG_ID_BL_START_APP_C          , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BL, MODULE_MSG_ID_BL_READ_FIRMWARE_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BL, MODULE_MSG_ID_BL_GET_FLASH_PARAMS_C   , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BL, MODULE_MSG_ID_BL_ERASE_FIRMWARE_C     , LU_FALSE , 0      }
};

static USER_APP_MAIN UserApplication;

static lu_uint32_t maxBoardFlashSize;

static lu_bool_t startApplicationNow;

static lu_uint8_t flashDataBlock[DATA_BLOCK256] __attribute__ ((aligned (8)));

//BootloaderCommandStr BootloaderCommand __attribute__((io(0x20083c00)));
//lu_uint32_t          BootloaderCommandCRC __attribute__((io(0x20083c0c)));

BootloaderCommandStr *BootloaderCommandPtr = 0x20083c00;
lu_uint32_t          *BootloaderCommandCRCPtr = (0x20083c00 + sizeof(BootloaderCommandStr));

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR BootloaderInit(void)
{
	SB_ERROR retError;

	startApplicationNow = LU_FALSE;

	maxBoardFlashSize = BOARD_MAX_FLASH_SIZE_K * 1024;

	retError = BoardBootloaderInit();

	retError = CANFramingAddFilter( BootloaderModulefilterTable,
				                    SU_TABLE_SIZE(BootloaderModulefilterTable, filterTableStr)
	                              );
	return retError;
}

SB_ERROR BootloaderCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR retError;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retError = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_BL:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_BL_WRITE_FIRMWARE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BootloaderFirmwareStr))
			{
				retError = WriteFirmware((BootloaderFirmwareStr*)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_BL_READ_FIRMWARE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BootloaderFirmwareReadStr))
			{
				retError = ReadFirmware((lu_uint16_t*)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_BL_START_APP_C:
			/* Message sanity check */
			if (msgPtr->msgLen == 0)
			{
				retError = StartSlaveBoardApplication();
			}
			break;

		case MODULE_MSG_ID_BL_GET_FLASH_PARAMS_C:
			if (msgPtr->msgLen == 0)
			{
				retError = GetFlashParams();
			}
			break;
		case MODULE_MSG_ID_BL_ERASE_FIRMWARE_C:
			if (msgPtr->msgLen == 0)
			{
				retError = EraseFirmware();
			}
			break;
		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}

MODULE_ID BootloaderCalcModuleID(lu_uint32_t valSel1, lu_uint32_t valSel2, lu_uint32_t valSel3, lu_uint32_t valSel4)
{
 	lu_uint8_t  bcdValue;

 	bcdValue = 0;

 	/* convert to bcd value */
	bcdValue |= valSel1;
	bcdValue |= (valSel2 << 1);
	bcdValue |= (valSel3 << 2);
	bcdValue |= (valSel4 << 3);
	bcdValue ^= 0xf;

 	switch (bcdValue)
 	{
 	case 0x01:
 		return MODULE_ID_0;
 		break;

 	case 0x02:
 		return MODULE_ID_1;
 		break;

 	case 0x03:
 		return MODULE_ID_2;
 		break;

 	case 0x04:
 		return MODULE_ID_3;
 		break;

 	case 0x05:
 		return MODULE_ID_4;
 		break;

 	case 0x06:
 		return MODULE_ID_5;
 		break;

 	case 0x07:
 		return MODULE_ID_6;
 		break;

 	default:
 		return MODULE_ID_0;
 		break;
 	}
}

lu_bool_t BootloaderGetStartApplicationNow(void)
{
	return startApplicationNow;
}

void BootloaderStartCommand(void)
{
	// Write start command in RAM
	BootloaderCommandPtr->magicSync = BOOTLOADER_MAGICSYNC;
	BootloaderCommandPtr->command = BL_CMD_START_APP;
	BootloaderCommandPtr->Parameter = 0;

	crc32_calc32((lu_uint8_t * )BootloaderCommandPtr, sizeof(BootloaderCommandStr), BootloaderCommandCRCPtr);

	// Restart bootloader
	WDT_Init(WDT_CLKSRC_IRC, WDT_MODE_RESET);
	/* Start watchdog (1ms timeout) */
	WDT_Start(1000);
}

void BootloaderCheckStartCommand(void)
{
	lu_uint32_t crc32;

	crc32_calc32((lu_uint8_t * )BootloaderCommandPtr, sizeof(BootloaderCommandStr), &crc32);

	// Check for command command to RAM
	if (crc32 == *BootloaderCommandCRCPtr &&
		BootloaderCommandPtr->magicSync == BOOTLOADER_MAGICSYNC &&
		BootloaderCommandPtr->command == BL_CMD_START_APP)
	{
		// Erase command in RAM
		BootloaderCommandPtr->magicSync = 0;
		BootloaderCommandPtr->command = 0;
		BootloaderCommandPtr->Parameter = 0;
		*BootloaderCommandCRCPtr = 0;

		BootloaderStartApp();
	}
}

SB_ERROR BootloaderStartApp(void)
{
	startApplicationNow = LU_FALSE;

	/* Will need to check if valid application via a CRC after vector table ?? */
	if (*(lu_uint32_t *)USER_APP_START_ADDR == 0xffffffff)
	{
		return SB_ERROR_INITIALIZED;
	}
	else
	{
		/* Disable all interrupts set by bootloader */
		SU_CRITICAL_REGION_ENTER();

		/* De-initialise the Interrupt controller */
//		NVIC_DeInit();

		/* Disable CAN Receive interrupt */
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_RIE, DISABLE);

		/* Disable CAN Transmit Interrupt */
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_TIE1, DISABLE);
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_TIE2, DISABLE);
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_TIE3, DISABLE);

		/* Disable CAN Error Interrupt */
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_EIE, DISABLE); //CAN Error Warning
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_EPIE, DISABLE); //CAN Error Passive
//		CAN_IRQCmd(CAN_BASE(LPC_CAN1_BASE), CANINT_BEIE, DISABLE); //CAN Bus Error

		/* De-initialise CAN here */
//		CAN_DeInit(CAN_BASE(LPC_CAN1_BASE));

		/* Restoring the vector table address as user application address */
		SCB->VTOR = USER_APP_START_ADDR;

		/* Assigning the user application address to user application */
		UserApplication = (USER_APP_MAIN) (*((lu_uint32_t *) (USER_APP_START_ADDR + 4)));

		setSP((lu_uint32_t)USER_APP_START_ADDR);

		/* Start the slave board application */
		UserApplication();

		return SB_ERROR_NONE;
	}
}

/*!
 ******************************************************************************
 *   \brief Writing the firmware into flash
 *
 *	 This function will re-write the bootloader firmware and if the power should fail
 *	 it will be then edn!!
 *
 *   \param srcAddr pointer to firmware data to be programmed.
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR WriteBootLoaderFirmware(lu_uint8_t *srcAddr)
{
    SB_ERROR retError = SB_ERROR_NONE;
    lu_uint32_t flashAddress;

    flashAddress  = 0;

    do
    {
       /* Storing the incoming data block as a separate element
       as variation in memory bank can affect flash write operation */
       memcpy(flashDataBlock, srcAddr, DATA_BLOCK256);

       retError = NXP17xxFlashWrite( flashAddress,
    		                         (lu_uint32_t*)flashDataBlock,
    		                         DATA_BLOCK256
    		                       );

       if (retError != SB_ERROR_NONE)
       {
    	   flashAddress = USER_APP_START_ADDR;
       }

       srcAddr 		+= DATA_BLOCK256;
       flashAddress += DATA_BLOCK256;
    } while (flashAddress < USER_APP_START_ADDR);

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

void setSP(lu_uint32_t address)
{
	__asm("ldr	sp,[r0]");
}

/*!
 ******************************************************************************
 *   \brief Writing the firmware into flash
 *
 *	 This function write a 256 bytes of incoming firmware from CAN message into flash.
 *	 Depending upon the address range it will choose the sector and compare the written
 *	 bytes with data buffer as a process of verification.
 *
 *   \param dataToBeFlashed Pointer to the 256 bytes of firmware and number of block.
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR WriteFirmware(BootloaderFirmwareStr *dataToBeFlashed)
{
    SB_ERROR retError = SB_ERROR_NONE;
    ModStatusReplyStr statusReply;
    lu_uint32_t flashAddress;
    static lu_uint8_t flashDataBlock[DATA_BLOCK256] __attribute__ ((aligned (8)));

    flashAddress  = USER_APP_START_ADDR + ((dataToBeFlashed->blockOffset) * DATA_BLOCK256);

    if ((flashAddress < maxBoardFlashSize) && (flashAddress >= USER_APP_START_ADDR))
    {
       /* Storing the incoming data block as a separate element
       as variation in memory bank can affect flash write operation */
       memcpy(flashDataBlock, dataToBeFlashed->firmwareBlock256, DATA_BLOCK256);

       retError = NXP17xxFlashWrite( flashAddress,
    		                         (lu_uint32_t*)flashDataBlock,
    		                         DATA_BLOCK256
    		                       );
    }
    else
    {
    	retError = SB_ERROR_PARAM;
    }

	switch (retError)
	{
	case SB_ERROR_WRITE_FAIL:
	   statusReply.status = REPLY_STATUS_WRITE_ERROR;
	   break;

	case SB_ERROR_PARAM:
		statusReply.status = REPLY_STATUS_PARAM_ERROR;
		break;

	case SB_ERROR_NONE:
	   statusReply.status = REPLY_STATUS_OKAY;
	   break;

	default:
	   statusReply.status = REPLY_STATUS_ERROR;
	}

	retError = CANCSendMCM ( MODULE_MSG_TYPE_BL,
							MODULE_MSG_ID_BL_WRITE_FIRMWARE_R,
							sizeof(ModStatusReplyStr),
							(lu_uint8_t*)&statusReply
						   );
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Reading the firmware from memory
 *
 *	 This function reads 256 bytes of firmware from memory.
 *	 Depending upon the address range it will choose the sector and copy the data into buffer.
 *
 *   \param dataToBeFlashed Pointer to the 256 bytes of firmware and number of block.
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR ReadFirmware(lu_uint16_t *blockOffsetToRead)
{
    SB_ERROR retError = SB_ERROR_NONE;
    lu_uint32_t flashAddress;

    flashAddress  = USER_APP_START_ADDR + (*blockOffsetToRead * DATA_BLOCK256);

    if (flashAddress < maxBoardFlashSize)
    {
       retError = NXP17xxFlashRead( flashAddress,
    		                        (lu_uint32_t*)flashDataBlock,
    		                        DATA_BLOCK256
    		                      );

       retError = CANCSendMCM ( MODULE_MSG_TYPE_BL,
    		   	   	   	   	    MODULE_MSG_ID_BL_READ_FIRMWARE_R,
    		   	   	   	   	    sizeof(BootloaderFirmwareBlkStr),
    		   	   	   	   	    (lu_uint8_t*)flashDataBlock
    		    		   	   );
    }
    else
    {
    	retError = SB_ERROR_PARAM;
    }
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Erasing the firmware from memory
 *
 *   This function erase the first sector of user application in board.
 *
 *   \param none
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR EraseFirmware(void)
{
	SB_ERROR retError;
	ModStatusReplyStr statusReply;

	retError = NXP17xxFlashErase(USER_APP_START_ADDR);

	switch (retError)
		{
		case SB_ERROR_PARAM:
			statusReply.status = REPLY_STATUS_PARAM_ERROR;
			break;

		case SB_ERROR_NONE:
		   statusReply.status = REPLY_STATUS_OKAY;
		   break;

		default:
		   statusReply.status = REPLY_STATUS_ERROR;
		}

		retError = CANCSendMCM ( MODULE_MSG_TYPE_BL,
								MODULE_MSG_ID_BL_ERASE_FIRMWARE_R,
								sizeof(ModStatusReplyStr),
								(lu_uint8_t*)&statusReply
							   );
		return retError;

}

/*!
 ******************************************************************************
 *   \brief Starting of slave board application
 *
 *   Restoring the location of Interrupt vector table and loading the program counter
 *   by assigning the location of user application resides.
 *
 *   \param None
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR StartSlaveBoardApplication(void)
{
	SB_ERROR retError;
	ModStatusReplyStr modStatusReply;

	/* Check if application is available ?? */
	// modStatusReply.status = REPLY_STATUS_NO_APPLICATION;

	modStatusReply.status = REPLY_STATUS_OKAY;

	retError = CANCSendMCM ( MODULE_MSG_TYPE_BL,
			                 MODULE_MSG_ID_BL_START_APP_R,
							 sizeof(ModStatusReplyStr),
							 (lu_uint8_t*)&modStatusReply
						   );

	/* Signal start application */
	startApplicationNow = LU_TRUE;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Get Flash programming parameters
 *
 *
 *
 *   \param None
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR GetFlashParams(void)
{
	SB_ERROR retError;
	BootloaderFlashParamsReplyStr BootloaderFlashParams;

	BootloaderFlashParams.appFlashBaseAddress = USER_APP_START_ADDR;
	BootloaderFlashParams.maxAppFlashAddress  = maxBoardFlashSize;

	retError = CANCSendMCM ( MODULE_MSG_TYPE_BL,
							 MODULE_MSG_ID_BL_GET_FLASH_PARAMS_R,
							 sizeof(BootloaderFlashParamsReplyStr),
							 (lu_uint8_t*)&BootloaderFlashParams
						   );

	return SB_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */

