/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "BootNvRam.h"
#include "FactoryTestNVRAM.h"
#include "NVRAMDef.h"
#include "crc32.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR GetNVRamMemoryPtr(lu_uint8_t **memPtr,
		                   lu_uint8_t chan, lu_uint8_t addr,
		                   lu_uint16_t blockOffset, lu_uint16_t blockSize);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint8_t eepromMemoryCache[NVRAM_EEPROM_SIZE] __attribute__((section("ahbsram0")));



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BootNvRamGetApplicationInfoMemoryPtr(lu_uint8_t **memPtr)
{
	return GetNVRamMemoryPtr(memPtr, 1, 0x15, NVRAM_APP_BLK_OPTS_OFFSET, NVRAM_APP_BLK_OPTS_SIZE);
}

SB_ERROR BootNvRamGetApplicationOptsMemoryPtr(lu_uint8_t **memPtr)
{
	return GetNVRamMemoryPtr(memPtr, 1, 0x15, NVRAM_APP_BLK_OPTS_OFFSET, NVRAM_APP_BLK_OPTS_SIZE);
}

SB_ERROR BootNvRamGetIdentityInfoMemoryPtr(lu_uint8_t **memPtr)
{
	return GetNVRamMemoryPtr(memPtr, 1, 0x14, NVRAM_ID_BLK_INFO_OFFSET, NVRAM_ID_BLK_INFO_SIZE);
}

SB_ERROR BootNvRamGetIdentityOptsMemoryPtr(lu_uint8_t **memPtr)
{
	return GetNVRamMemoryPtr(memPtr, 1, 0x14, NVRAM_ID_BLK_OPTS_OFFSET, NVRAM_ID_BLK_OPTS_SIZE);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR GetNVRamMemoryPtr(lu_uint8_t **memPtr, lu_uint8_t chan, lu_uint8_t addr, lu_uint16_t blockOffset, lu_uint16_t blockSize)
{
	SB_ERROR retError;
	TestNVRAMReadBuffStr i2cNVRAMParams;
	lu_uint8_t *nvramBlkPtr;
	NVRAMBlkHeadStr *nvramBlkHeaderPtr;
	lu_uint32_t crc32;

	i2cNVRAMParams.addr.offset  = blockOffset;
	i2cNVRAMParams.addr.i2cChan = chan;
	i2cNVRAMParams.addr.addr    = addr;
	i2cNVRAMParams.size         = blockSize;

	retError = FactoryTestNVRAMReadBuffer(&i2cNVRAMParams, &eepromMemoryCache[0]);

	/* Return pointer to data */
	*memPtr = &eepromMemoryCache[0];

	if (retError == SB_ERROR_NONE)
	{
		/* check the CRC */
		nvramBlkPtr       = (lu_uint8_t *)&eepromMemoryCache[0];
		nvramBlkHeaderPtr = (NVRAMBlkHeadStr *)nvramBlkPtr;
		nvramBlkPtr      += sizeof(NVRAMBlkHeadStr);

		/* Set pointer to where data is... */
		*memPtr = nvramBlkPtr;

		crc32_calc32((lu_uint8_t * )nvramBlkPtr, (blockSize - sizeof(NVRAMBlkHeadStr)), &crc32);

		if (nvramBlkHeaderPtr->dataCrc32 == crc32)
		{
			retError = SB_ERROR_NONE;
		}
		else
		{
			retError = SB_ERROR_NVRAM_CRC;
		}
	}
	return retError;
}


/*
 *********************** End of file ******************************************
 */
