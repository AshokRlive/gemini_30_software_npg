/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/07/12      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"
#include "string.h"

#include "NXP17xxFlashDriver.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define SECTOR_SIZE_4K          	  0x1000
#define SECTOR_SIZE_32K         	  0x8000
#define CMD_SUCCESS             	  0
#define IAP_ADDRESS             	  0x1FFF1FF1
#define NO_ERROR_ON_FLASH_OPERATION   0
#define FILL_MEM_MAP(startAddress, sectorSize) {startAddress,(startAddress + sectorSize) - 1, sectorSize}

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
typedef struct NXP17xxFlashSectorMapDef
{
			lu_uint32_t startAddress;
			lu_uint32_t endAddress;
			lu_uint16_t sectorSize;
}NXP17xxFlashSectorMapStr;

/* IAP commands for LPC17xx (Refer the LPC17xx user manual) */
typedef enum
{
	PREPARE_SECTOR_FOR_WRITE = 50,
	COPY_RAM_TO_FLASH		 = 51,
	ERASE_SECTOR			 = 52,
	BLANK_CHECK_SECTOR		 = 53,
	READ_PART_ID			 = 54,
	READ_BOOT_VER			 = 55,
	COMPARE					 = 56,
	REINVOKE_ISP			 = 57
} IAP_Command_Code;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void PrepareFlashSector(lu_uint32_t startSector, lu_uint32_t endSector, lu_uint32_t cclk);
SB_ERROR WriteFlashSector(lu_uint32_t cclk, lu_uint32_t address, lu_uint32_t *flashDataBufffer, lu_uint32_t count);
void EraseFlashSector(lu_uint32_t startSector, lu_uint32_t endSector, lu_uint32_t cclk);
void IAPEntry(lu_uint32_t flashParamTable[],lu_uint32_t flashResultTable[]);
lu_uint16_t SelectEraseSectorByFlashAddress(lu_uint32_t flashAddress);
SB_ERROR FlashAddressToSector(lu_uint32_t flashAddress, lu_uint16_t *flashSectorPtr);
lu_bool_t FlashEraseCheck(lu_uint16_t flashSector);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Table of memory map of LPC1768 */
static const NXP17xxFlashSectorMapStr NXP17xxflashSectorMapTable[NUM_FLASH_SECTOR] =
{
		FILL_MEM_MAP (0x00000000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00001000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00002000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00003000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00004000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00005000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00006000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00007000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00008000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00009000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x0000A000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x0000B000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x0000C000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x0000D000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x0000E000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x0000F000, SECTOR_SIZE_4K),
		FILL_MEM_MAP (0x00010000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00018000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00020000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00028000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00030000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00038000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00040000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00048000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00050000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00058000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00060000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00068000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00070000, SECTOR_SIZE_32K),
		FILL_MEM_MAP (0x00078000, SECTOR_SIZE_32K)
};

lu_uint32_t flashParamTable  [5];
lu_uint32_t flashResultTable [5];
lu_uint16_t currentFlashSector;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR NXP17xxFlashWrite(lu_uint32_t flashAddress, lu_uint32_t *flashDataBuffer, lu_uint16_t count)
{
	SB_ERROR retError;
	lu_uint16_t sector;

	retError = SB_ERROR_NONE;

	sector = SelectEraseSectorByFlashAddress(flashAddress);
	PrepareFlashSector(sector, sector, CCLK);
	WriteFlashSector(CCLK, flashAddress,(lu_uint32_t*)flashDataBuffer, count);

	/* Comparing the written data and given block of data as a process of verification */
	if(memcmp((void *)flashDataBuffer, (void *)flashAddress, count))
	{
			retError = SB_ERROR_WRITE_FAIL;
	}

	return retError;
}

SB_ERROR NXP17xxFlashRead(lu_uint32_t flashAddress, lu_uint32_t *flashDataBuffer, lu_uint16_t count)
{
	SB_ERROR retError;

	if(flashDataBuffer != memcpy(flashDataBuffer, (lu_uint32_t*)flashAddress , count))
	{
		retError = SB_ERROR_PARAM;
	}

	return retError;
}

SB_ERROR NXP17xxFlashErase(lu_uint32_t flashAddress)
{
	SB_ERROR retError;
	lu_uint32_t currentSector;
	lu_bool_t result;

	retError = FlashAddressToSector(flashAddress, &currentFlashSector);

	if (retError == SB_ERROR_NONE)
	{
		currentSector = currentFlashSector;

		result = FlashEraseCheck((lu_uint8_t)currentSector);
		if(result == LU_FALSE)
		{
			PrepareFlashSector(currentSector,currentSector,CCLK);
			EraseFlashSector(currentSector,currentSector,CCLK);
		}

		if(FlashEraseCheck((lu_uint8_t)currentSector) != LU_TRUE)
		{
			retError =  SB_ERROR_WRITE_FAIL;
		}
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Selecting sector number by using flash address
 *
 *	 Selecting the corresponding sector number by using given flash address
 *	 and erasing of the sector if the flash address lies on the sector boundary.
 *
 *   \param flashAddress The location where the data to be written
 *
 *   \return currentSector The sector number for corresponding flash address.
 *
 ******************************************************************************
 */
lu_uint16_t SelectEraseSectorByFlashAddress(lu_uint32_t flashAddress)
{
	lu_uint16_t sectorCount, currentSector = 0;

	/* Selecting the sector number by using given flash address */
	for(sectorCount = 0; sectorCount < NUM_FLASH_SECTOR; sectorCount++)
	{
		if ((flashAddress >= NXP17xxflashSectorMapTable[sectorCount].startAddress) &&
			(flashAddress <= NXP17xxflashSectorMapTable[sectorCount].endAddress) )
		 {
			 currentSector = sectorCount;

			 /* Erase the sector if the given address is in sector boundary */
			 if(flashAddress == NXP17xxflashSectorMapTable[sectorCount].startAddress)
			 {
				 PrepareFlashSector(currentSector,currentSector,CCLK);
				 EraseFlashSector(currentSector,currentSector,CCLK);
			 }
			 break;
		 }
	}
	return currentSector;
}

/*!
 ******************************************************************************
 *   \brief Finding sector number corresponds to flash address
 *
 *   To find sector sector number corresponds to flash address and the sector number
 *   will be stored in a address.
 *
 *	 \param flashAddress
 *   \param flashSector Ptr Pointer to the sector
 *
 *   \return
 ******************************************************************************
 */
SB_ERROR FlashAddressToSector(lu_uint32_t flashAddress, lu_uint16_t *flashSectorPtr)
{
	lu_uint16_t sectorCount;
	SB_ERROR retError = SB_ERROR_PARAM;

	/* Selecting the sector number by using given flash address */
	for(sectorCount = 0; sectorCount < NUM_FLASH_SECTOR; sectorCount++)
	{
		if ((flashAddress >= NXP17xxflashSectorMapTable[sectorCount].startAddress) &&
			(flashAddress <= NXP17xxflashSectorMapTable[sectorCount].endAddress) )
		 {
			 *flashSectorPtr = sectorCount;
			 retError = SB_ERROR_NONE;
			 break;
		 }
	}
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Erase checking on flash sector
 *
 *   Checking of the given sector whether it is blank or not.
 *
 *	 \param flashSector
 *
 *   \return result
 ******************************************************************************
 */
lu_bool_t FlashEraseCheck(lu_uint16_t flashSector)
{
	lu_bool_t result = LU_TRUE;
	lu_uint32_t *flashAddressPtr;

	flashAddressPtr = (lu_uint32_t*)NXP17xxflashSectorMapTable[flashSector].startAddress;

	while((lu_uint32_t)flashAddressPtr <= NXP17xxflashSectorMapTable[flashSector].endAddress)
	{
		if(*flashAddressPtr != 0xffffffff)
		{
			result = LU_FALSE;
			break;
		}
		flashAddressPtr++;
	}
	return result;
}

/*!
 ******************************************************************************
 *   \brief Preparation of a sector in flash
 *
 *	 Preparation of a sector in flash, this function should be before any operation on
 *	 flash memory such as writing, reading and erasing
 *
 *   \param startSector starting number of sector
 *   \param endSector   ending number of sector
 *   \param cclk        Clock speed for preparation of sector
 *
 *   \return
 *
 ******************************************************************************
 */
void PrepareFlashSector(lu_uint32_t startSector, lu_uint32_t endSector, lu_uint32_t cclk)
{
	flashParamTable[0] = PREPARE_SECTOR_FOR_WRITE;
	flashParamTable[1] = startSector;
	flashParamTable[2] = endSector;
	flashParamTable[3] = cclk;
	IAPEntry(flashParamTable,flashResultTable);
}

/*!
  ******************************************************************************
  *   \brief Writing a sector by given data
  *
  *   Writing the given data into a particular flash sector.
  *
  *   \param cclk Clock speed for preparation of sector
  *   \param address Starting Address in the flash where data to be written
  *   \param flashDataBuffer Pointer to the data to be written
  *   \param count Number of bytes
  *
  *   \return Error code
  *
  ******************************************************************************
  */
SB_ERROR WriteFlashSector(lu_uint32_t cclk, lu_uint32_t address, lu_uint32_t *flashDataBuffer, lu_uint32_t count)
{
	SB_ERROR retError;

	flashParamTable[0] = COPY_RAM_TO_FLASH;
	flashParamTable[1] = address;
	flashParamTable[2] = (lu_uint32_t)flashDataBuffer;
	flashParamTable[3] = count;
	flashParamTable[4] = cclk;
	IAPEntry(flashParamTable,flashResultTable);

	if(flashResultTable[0] != NO_ERROR_ON_FLASH_OPERATION)
	{
	  retError = SB_ERROR_NONE;
	}

    return retError;
}

/*!
 ******************************************************************************
 *   \brief Erasing the flash sector
 *
 *   Erasing the number of given flash sector
 *
 *   \param startSector starting number of sector
 *   \param endSector   ending number of sector
 *   \param cclk        Clock speed for erasing of sector
 *   \return
 *
 ******************************************************************************
 */
void EraseFlashSector(lu_uint32_t startSector, lu_uint32_t endSector, lu_uint32_t cclk)
{
	 flashParamTable[0] = ERASE_SECTOR;
	 flashParamTable[1] = startSector;
	 flashParamTable[2] = endSector;
	 flashParamTable[3] = cclk;
	 IAPEntry(flashParamTable,flashResultTable);
}

/*!
 ******************************************************************************
 *   \brief IAP system call
 *
 *   IAP (In-chip Application Programming) is used to program the flash by user written code
 *   while another program is running.
 *
 *   \param flashParamTable  Array of Input parameters for IAP call
 *   \param flashResultTable Array of output parameters from IAP call
 *
 *   \return
 *
 ******************************************************************************
 */
void IAPEntry(lu_uint32_t flashParamTable[],lu_uint32_t flashResultTable[])
{
	void (*iap)(lu_uint32_t [],lu_uint32_t []);

	SU_CRITICAL_REGION_ENTER();

	iap = (void (*)(lu_uint32_t [],lu_uint32_t []))IAP_ADDRESS;
    iap(flashParamTable,flashResultTable);

    SU_CRITICAL_REGION_EXIT();
}

/*
 *********************** End of file ******************************************
 */
