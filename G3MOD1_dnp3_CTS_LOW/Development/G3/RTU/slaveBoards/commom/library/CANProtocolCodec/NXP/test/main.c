/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "system_LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "systemTime.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DEVICE 0x01
#define DEVICE_ID 0x03

#define SHORT_MESSAGE_LEN  8
#define LONG_MESSAGE_LEN   128

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR customDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANFramingMapStr CANMapping =
{
    GPIO_INVALID_PIN ,/* peripheralCAN     */
    GPIO_INVALID_PIN ,/* peripheralGPIO    */
    PINSEL_PORT_0    ,/* gpioPortBase      */
    PINSEL_PIN_1     ,/* CanTxPin          */
    GPIO_INVALID_PIN ,/* CanTxConfigurePin */
    PINSEL_PIN_0     ,/* CanRxPin          */
    GPIO_INVALID_PIN ,/* CanRxConfigurePin */
    LPC_CAN1_BASE    ,/* CanBase           */
    CAN_IRQn          /* CanInt            */
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR customDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
    /* Do nothing */
    return SB_ERROR_NONE;
}

int main(void)
{
    CANFramingMsgStr canMsg;
    SUIRQPriorityStr CANPriority;
    SUIRQPriorityStr TSynchPriority;
    SUIRQPriorityStr timePriority;
    lu_uint8_t bufferShort[SHORT_MESSAGE_LEN];
    lu_uint8_t bufferLong[LONG_MESSAGE_LEN];
    lu_uint32_t i;
    CANTimeStr canTime;

    CANPriority.group       = SU_IRQ_GROUP_0;
    CANPriority.subPriority = SU_IRQ_SUB_PRIORITY_0;

    TSynchPriority.group       = SU_IRQ_GROUP_7;
    TSynchPriority.subPriority = SU_IRQ_SUB_PRIORITY_3;

    timePriority.group       = SU_IRQ_GROUP_7;
    timePriority.subPriority = SU_IRQ_SUB_PRIORITY_0;

    SystemInit();

    STInit(timePriority);

    CANCodecInit(SystemCoreClock, 500000, DEVICE, DEVICE_ID, &CANMapping, CANPriority, TSynchPriority, customDecoder);

    /* Send short message */
    bufferShort[0] = 't';
    bufferShort[1] = 'e';
    bufferShort[2] = 's';
    bufferShort[3] = 't';
    bufferShort[4] = '!';
    bufferShort[5] = '!';
    bufferShort[6] = '!';
    bufferShort[7] = '!';

    canMsg.ID = 0;
    canMsg.messageType = 0x03;
    canMsg.messageID = 0x04;
    canMsg.deviceSrc = DEVICE;
    canMsg.deviceIDSrc = DEVICE_ID;
    canMsg.deviceDst = 0x8;
    canMsg.deviceIDDst = 0x07;
    canMsg.msgLen = SHORT_MESSAGE_LEN;
    canMsg.msgBufPtr = bufferShort;
    CANFramingSendMsg(&canMsg);

    /* Send long message */
    for(i = 0; i < LONG_MESSAGE_LEN; i++)
    {
        bufferLong[i] = 'a' + i;
    }

    canMsg.ID = 0;
    canMsg.messageType = 0x03;
    canMsg.messageID = 0x05;
    canMsg.deviceSrc = DEVICE;
    canMsg.deviceIDSrc = DEVICE_ID;
    canMsg.deviceDst = 0x8;
    canMsg.deviceIDDst = 0x07;
    canMsg.msgLen = LONG_MESSAGE_LEN;
    canMsg.msgBufPtr = bufferLong;

    CANFramingSendMsg(&canMsg);

    canMsg.ID = 0;
    canMsg.messageType = 0x05;
    canMsg.messageID = 0x07;
    canMsg.deviceSrc = DEVICE;
    canMsg.deviceIDSrc = DEVICE_ID;
    canMsg.deviceDst = 0x8;
    canMsg.deviceIDDst = 0x07;
    canMsg.msgLen = sizeof(canTime);
    canMsg.msgBufPtr = (lu_uint8_t*)&canTime;

    while(LU_TRUE)
    {
        //for(i = 0; i < 1000; i++);
        //CANFramingSendMsg(&canMsg);

        //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
        //GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_0);
        //CANCTestTimeSynch(GPIO_PORTA_BASE, GPIO_PIN_0);
        //CANCTestTimeSynch(0, 0);

        CANCDecode();

        /* Sleep a little */
        //for(i = 0; i < 10000; i++);


        //canTime = CANCGetTime();
        //CANFramingSendMsg(&canMsg);

    }

    return 0;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
