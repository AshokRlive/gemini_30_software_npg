/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DEVICE 0x01
#define DEVICE_ID 0x02

#define SHORT_MESSAGE_LEN  8
#define LONG_MESSAGE_LEN   128

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR customDecoder(CANFramingMsgStr *msgPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANFramingMapStr CANMapping =
{
    SYSCTL_PERIPH_CAN0 ,/* peripheralCAN     */
    SYSCTL_PERIPH_GPIOD,/* peripheralGPIO    */
    GPIO_PORTD_BASE    ,/* gpioPortBase      */
    GPIO_PIN_1         ,/* CanTxPin          */
    GPIO_PD1_CAN0TX    ,/* CanTxConfigurePin */
    GPIO_PIN_0         ,/* CanRxPin          */
    GPIO_PD0_CAN0RX    ,/* CanRxConfigurePin */
    CAN0_BASE          ,/* CanBase           */
    INT_CAN0            /* CanInt            */
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR customDecoder(CANFramingMsgStr *msgPtr)
{
    /* Echo message: change src and dst */
    msgPtr->deviceDst = msgPtr->deviceSrc;
    msgPtr->deviceIDDst = msgPtr->deviceIDSrc;
    msgPtr->deviceSrc = DEVICE;
    msgPtr->deviceIDSrc = DEVICE_ID;
    CANFramingSendMsg(msgPtr);

    return SB_ERROR_NONE;
}

int main(void)
{
    lu_uint32_t sysClock;
    CANFramingMsgStr canMsg;
    lu_uint8_t bufferShort[SHORT_MESSAGE_LEN];
    lu_uint8_t bufferLong[LONG_MESSAGE_LEN];
    lu_uint32_t i;
    CANTimeStr canTime;

    /* Set the clocking to run directly from the PLL at 50MHz */
    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_16MHZ);

    sysClock = SysCtlClockGet();

    IntMasterEnable(); //MG

    CANCodecInit(sysClock, 500000, DEVICE, DEVICE_ID, &CANMapping, customDecoder);

    /* Send short message */
    bufferShort[0] = 't';
    bufferShort[1] = 'e';
    bufferShort[2] = 's';
    bufferShort[3] = 't';
    bufferShort[4] = '!';
    bufferShort[5] = '!';
    bufferShort[6] = '!';
    bufferShort[7] = '!';

    canMsg.ID = 0;
    canMsg.messageType = 0x03;
    canMsg.messageID = 0x04;
    canMsg.deviceSrc = DEVICE;
    canMsg.deviceIDSrc = DEVICE_ID;
    canMsg.deviceDst = 0x8;
    canMsg.deviceIDDst = 0x07;
    canMsg.msgLen = SHORT_MESSAGE_LEN;
    canMsg.msgBufPtr = bufferShort;
    CANFramingSendMsg(&canMsg);

    /* Send long message */
    for(i = 0; i < LONG_MESSAGE_LEN; i++)
    {
        bufferLong[i] = 'a' + i;
    }

    canMsg.ID = 0;
    canMsg.messageType = 0x03;
    canMsg.messageID = 0x05;
    canMsg.deviceSrc = DEVICE;
    canMsg.deviceIDSrc = DEVICE_ID;
    canMsg.deviceDst = 0x8;
    canMsg.deviceIDDst = 0x07;
    canMsg.msgLen = LONG_MESSAGE_LEN;
    canMsg.msgBufPtr = bufferLong;

    CANFramingSendMsg(&canMsg);

    canMsg.ID = 0;
    canMsg.messageType = 0x05;
    canMsg.messageID = 0x07;
    canMsg.deviceSrc = DEVICE;
    canMsg.deviceIDSrc = DEVICE_ID;
    canMsg.deviceDst = 0x8;
    canMsg.deviceIDDst = 0x07;
    canMsg.msgLen = sizeof(canTime);
    canMsg.msgBufPtr = &canTime;

    while(LU_TRUE)
    {
        //for(i = 0; i < 1000; i++);
        //CANFramingSendMsg(&canMsg);

        //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
        //GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_0);
        //CANCTestTimeSynch(GPIO_PORTA_BASE, GPIO_PIN_0);

        CANCDecode();

        /* Sleep a little */
        for(i = 0; i < 10000; i++);


        canTime = CANCGetTime();
        CANFramingSendMsg(&canMsg);

    }

    return 0;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
