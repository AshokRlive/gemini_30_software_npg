/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Codec module public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_CODEC_INCLUDED
#define _CAN_CODEC_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "ModuleProtocol.h"
#include "CANProtocolFraming.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * \brief CAN Protocol custom decode function.
 *
 * This function is called by the CANCDecode function.
 *
 * \param msgPtr CAN message
 * \param time Module relative time
 */
typedef SB_ERROR (*CANCCustomDecode)(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize CAN Codec module
 *
 *   \param clock System clock
 *   \param bitRate CAN bit rate
 *   \param device Device type (It used for source field of the CAN header)
 *   \param deviceID Device address (It used for sourceID field of the
 *                   CAN header)
 *   \param pinMapPtr pointer to the pin map table
 *   \param CANIRQ CAN IRQ priority
 *   \param TsynchIRQ CAN Time Synch IRQ priority
 *   \param customDecoder callback used to forward a received message not
 *                        handled by the the Codec itself. This function is
 *                        executed outside the ISR.
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANCodecInit( lu_uint32_t       clock        ,
                              lu_uint32_t       bitRate      ,
                              lu_uint8_t        device       ,
                              lu_uint8_t        deviceID     ,
                              CANFramingMapStr *pinMapPtr    ,
                              SUIRQPriorityStr  CANIRQ       ,
                              SUIRQPriorityStr  TsynchIRQ    ,
                              CANCCustomDecode  customDecoder
                            );

/*!
 ******************************************************************************
 *   \brief Decode CAN messages
 *
 *   This should be periodically called in the main loop of the application to
 *   handle the received CAN messages. If a message is not handled by the
 *   codec itself, the message is forwarded to the registered callback
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANCDecode(void);


/*!
 ******************************************************************************
 *   \brief Get the CAN synchronised time
 *
 *   If the system is not synchronised an invalid time (CAN_TIME_INVALID) is
 *   returned
 *
 *   \return Synchronised time
 *
 ******************************************************************************
 */
extern ModuleTimeStr CANCGetTime(void);

/*!
 ******************************************************************************
 *  \brief Send a message to the MCM
 *
 *  \param messageType Message type
 *  \param messageID Message ID
 *  \param msgLen Message payload length
 *  \param msgBufPtr Message payload pointer
 *
 *  \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANCSendMCM( MODULE_MSG_TYPE messageType,
                             MODULE_MSG_ID   messageID  ,
                             lu_uint32_t     msgLen     ,
                             lu_uint8_t     *msgBufPtr
                            );


extern lu_uint8_t CANCGetDeviceID(void);

#endif /* _CAN_CODEC_INCLUDED */

/*
 *********************** End of file ******************************************
 */
