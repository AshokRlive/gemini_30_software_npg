/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Codec module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   26/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_timer.h"
#include "driverlib/timer.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"

#include "CANProtocol.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define CANC_MSG_POOL_SIZE_MASK 0x0F
#define CANC_MSG_POOL_SIZE      (CANC_MSG_POOL_SIZE_MASK + 1)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    CANC_TIME_STATUS_NO_SYNCH = 0,
    CANC_TIME_STATUS_IN_SYNCH    ,

    CANC_TIME_STATUS_LAST
}CANC_TIME_STATUS;

/*!
 * Control data for the tyme synch FSM
 */
typedef struct CANCTimeSynchDef
{
    CANC_TIME_STATUS status;

    /*! Number of tick in a usec */
    lu_uint32_t timerTickTous;
    /*! Number of tick in a 100 usec */
    lu_uint32_t timerTickTo100us;

    lu_uint8_t timeSlot;
}CANCTimeSynchStr;

/*!
 * Data structure for the internal message queue
 */
typedef struct CANCMsgPoolDef
{
    volatile lu_uint32_t readIndex;
    volatile lu_uint32_t writeIndex;
    CANFramingMsgStr *msgPtr[CANC_MSG_POOL_SIZE];
}CANCMsgPoolStr;

typedef struct CANCodecDef
{
    CANCTimeSynchStr timeSynch;
    CANCMsgPoolStr   msgPool;

    CANCCustomDecode customDecoder;
}CANCodecStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static void CANCNewMessage(CANFramingMsgStr *msgPtr);

static SB_ERROR CANCInitMsgPool(void);
static CANFramingMsgStr* CANCGetMsg(void);
static SB_ERROR CANCAddMsg(CANFramingMsgStr *msgPtr);

static SB_ERROR CANCTimeSynchInit(lu_uint32_t clock);
static SB_ERROR CANCTimeSynch(CANFramingMsgStr *msgPtr);

void CANCTSyncIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static CANCodecStr CANCodecData;

volatile lu_uint32_t debugThreshold = 0;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR CANCodecInit( lu_uint32_t       clock        ,
                       lu_uint32_t       bitRate      ,
                       lu_uint8_t        device       ,
                       lu_uint8_t        deviceID     ,
                       CANFramingMapStr *pinMapPtr    ,
                       CANCCustomDecode  customDecoder
                     )
{
    SB_ERROR ret = SB_ERROR_NONE;

    if ( (pinMapPtr    == NULL) ||
         (customDecoder== NULL)
       )
    {
        return SB_ERROR_PARAM;
    }

    /* Save custom decode function */
    CANCodecData.customDecoder = customDecoder;

    /* Initialize message pool */
    CANCInitMsgPool();

    /* Initialize Time synch subsystem */
    CANCTimeSynchInit(clock);

    /* Initialize CAN Framing module */
    ret = CANFramingInit( clock         ,
                          bitRate       ,
                          device        ,
                          deviceID      ,
                          pinMapPtr     ,
                          CANCNewMessage
                        );

    return ret;
}

SB_ERROR CANCDecode(void)
{
    CANFramingMsgStr *msgPtr;

    /* Get a message */
    msgPtr = CANCGetMsg();

    while(msgPtr != NULL)
    {
        CANCodecData.customDecoder(msgPtr);
        CANFramingReleaseMsg(msgPtr);
        msgPtr = CANCGetMsg();
    }

    return SB_ERROR_NONE;
}

CANTimeStr CANCGetTime(void)
{
    CANTimeStr time;
    register lu_uint32_t timeTmp;

    switch(CANCodecData.timeSynch.status)
    {
        case CANC_TIME_STATUS_IN_SYNCH:
            timeTmp = HWREG(TIMER0_BASE + TIMER_O_TAV);
            time.time = timeTmp / CANCodecData.timeSynch.timerTickTo100us;
            time.slot = CANCodecData.timeSynch.timeSlot;
        break;

        case CANC_TIME_STATUS_NO_SYNCH:
        default:
            /* Update slot and reset the timer */ //MG ATOMIC ??
            time.time = CAN_TIME_INVALID;
            time.slot = 0;
        break;
    }

    return time;
}

void CANCTestTimeSynch(lu_uint32_t base, lu_uint32_t pin)
{
#define DEBUG_THR 1000000

    register lu_uint32_t timeTmp;
    register lu_uint32_t output = 1;

    GPIOPinWrite(base, pin, output);

    while(LU_TRUE)
    {
        timeTmp = HWREG(TIMER0_BASE + TIMER_O_TAV);
        if(timeTmp > debugThreshold)
        {
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_0, output);
            output ^= 1;
            debugThreshold += DEBUG_THR;
        }
    }
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief
 *
 *   \param paramName description
 *
 *   \return None
 *
 ******************************************************************************
 */
void CANCNewMessage(CANFramingMsgStr *msgPtr)
{
    /* Only the time synch is managed directly into the ISR.
     * Everything else is saved in a local queue and managed out of the
     * ISR
     */
    if ( (msgPtr->messageType == CAN_MSG_TYPE_HPRIO)     &&
         (msgPtr->messageID   == CAN_MSG_ID_HPRIO_TSYNCH)
       )
    {
        /* Handle message */
        CANCTimeSynch(msgPtr);
        /* Release buffer */
        CANFramingReleaseMsg(msgPtr);
    }
    else
    {
        /* Save the message in the Application level message queue */
        CANCAddMsg(msgPtr);
    }

    return;
}

SB_ERROR CANCInitMsgPool(void)
{
    CANCodecData.msgPool.readIndex = 0;
    CANCodecData.msgPool.writeIndex = 0;

    return SB_ERROR_NONE;
}

CANFramingMsgStr* CANCGetMsg(void)
{
    CANFramingMsgStr *msgPtr;

    lu_uint32_t readIndex = CANCodecData.msgPool.readIndex;

    if (readIndex == CANCodecData.msgPool.writeIndex)
    {
        msgPtr = NULL;
    }
    else
    {
        readIndex = (readIndex + 1) & CANC_MSG_POOL_SIZE_MASK;
        msgPtr = CANCodecData.msgPool.msgPtr[readIndex];
        CANCodecData.msgPool.readIndex = readIndex;
    }

    return msgPtr;
}

SB_ERROR CANCAddMsg(CANFramingMsgStr *msgPtr)
{
    lu_uint32_t writeIndex;

    writeIndex = (CANCodecData.msgPool.writeIndex + 1) & CANC_MSG_POOL_SIZE_MASK;

    /* Enough space ? */
    if (writeIndex != CANCodecData.msgPool.readIndex)
    {
        /* Yes */
        CANCodecData.msgPool.msgPtr[writeIndex] = msgPtr;
        CANCodecData.msgPool.writeIndex = writeIndex;

        return SB_ERROR_NONE;
    }
    else
    {
        return SB_ERROR_CANC_QUEUE_FULL;
    }
}


SB_ERROR CANCTimeSynchInit(lu_uint32_t clock)
{
    /* Reset synchronisation */
    CANCodecData.timeSynch.status = CANC_TIME_STATUS_NO_SYNCH;

    /* Initialize conversion constants */
    CANCodecData.timeSynch.timerTickTous = clock / (CANC_SEC_2_USEC(1));
    CANCodecData.timeSynch.timerTickTo100us = CANCodecData.timeSynch.timerTickTous * 100;

    /* Enable Timer peripheral */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    /* Initialize Timer */
    TimerConfigure(TIMER0_BASE, TIMER_CFG_32_BIT_OS_UP);
    TimerLoadSet(TIMER0_BASE, TIMER_A, (CAN_TIME_SYNCH_MAX_DELAY_US * CANCodecData.timeSynch.timerTickTous));

    /* Enable Interrupt */
    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    return SB_ERROR_NONE;
}

SB_ERROR CANCTimeSynch(CANFramingMsgStr *msgPtr)
{
    CANTsynchStr *CANTsynchPtr;

    /* Message sanity check */
    if (msgPtr->msgLen == CAN_MESSAGE_SIZE(CANTsynchStr))
    {
        CANTsynchPtr = (CANTsynchStr*)(msgPtr->msgBufPtr);

        switch(CANCodecData.timeSynch.status)
        {
            case CANC_TIME_STATUS_NO_SYNCH:
                /* Start the timer */
                TimerEnable(TIMER0_BASE, TIMER_A);

                /* Save Time slot */
                CANCodecData.timeSynch.timeSlot = CANTsynchPtr->timeSlot;

                /* We are in synch */
                CANCodecData.timeSynch.status = CANC_TIME_STATUS_IN_SYNCH;
            break;

            case CANC_TIME_STATUS_IN_SYNCH:
                /* Update slot and reset the timer */ //MG ATOMIC ??
                HWREG(TIMER0_BASE + TIMER_O_TAV) = 0;
                CANCodecData.timeSynch.timeSlot = CANTsynchPtr->timeSlot;
                debugThreshold = 0;//MG
            break;

            default:
            break;
        }

        return SB_ERROR_NONE;
    }

    return SB_ERROR_CANC;
}

void CANCTSyncIntHandler(void)
{
    static lu_uint32_t errorCounter = 0;

    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    errorCounter++;

    CANCodecData.timeSynch.status = CANC_TIME_STATUS_NO_SYNCH;

    return;
}

/*
 *********************** End of file ******************************************
 */
