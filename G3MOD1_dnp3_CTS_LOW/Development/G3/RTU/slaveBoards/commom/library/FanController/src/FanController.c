/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Supply Controller module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lu_types.h"

#include "FanController.h"
#include "IOManager.h"
#include "BoardIOMap.h"

#include "ModuleProtocol.h"
#include "NVRam.h"
#include "NVRAMDef.h"

#include "systemStatus.h"
#include "DigitalOutController.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
SB_ERROR FanTestHandler(FanTestStr *);
SB_ERROR FanConfig(FanConfigStr *);
SB_ERROR FanController(void);
SB_ERROR FanTest(void);
lu_bool_t FanCheck(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

FanTestStr      fanTestParamStr;
lu_bool_t		fanTest  = LU_FALSE;
lu_bool_t		fanState = LU_FALSE;


static NVRAMUserPSMStr *nvramAppUserBlkPtr;
static lu_uint8_t fanAsExternalSupply = LU_FALSE;
static lu_uint8_t fanAsDigitalOutput = LU_FALSE;


/*! List of supported message */
static const filterTableStr FanControllerrModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Fan commands */
	{	MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_FAN_CH_TEST_C               , LU_FALSE , 0	},
    {  	MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_FAN_CONTROLLER_C            , LU_FALSE , 0   }

};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FanControllerInit(void)
{
	SB_ERROR RetVal;

	RetVal = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_USER_A, (lu_uint8_t **)&nvramAppUserBlkPtr);
	if (RetVal != SB_ERROR_NONE)
	{
		/*
		 * Initialise configuration table with default values until config
		 * message received.
		 */
		nvramAppUserBlkPtr->fanFitted              = DEFAULT_FAN_FITTED;
		nvramAppUserBlkPtr->fanSpeedSensorFitted   = DEFAULT_FAN_SPEED_SENSOR_FITTED;

		nvramAppUserBlkPtr->fanTempThreshold 		= DEFAULT_FAN_TEMP_THRESHOLD;
		nvramAppUserBlkPtr->fanTempHysteresis		= DEFAULT_FAN_TEMP_HYSTERESIS;
		nvramAppUserBlkPtr->fanFaultHysteresisMs	= DEFAULT_FAN_FAULT_HYSTERESIS_MS;

		/* Call function to indicate NVRAM should be updated */
		RetVal = NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );
	}

	fanAsDigitalOutput = DEFAULT_FAN_DIGITAL_OUTPUT;
	fanAsExternalSupply = DEFAULT_FAN_EXTERNAL_SUPPLY;

	/* Setup CAN Msg filter */
	return CANFramingAddFilter( FanControllerrModulefilterTable,
								SU_TABLE_SIZE(FanControllerrModulefilterTable,
											  filterTableStr)
							   );

}

SB_ERROR FanControlCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	LU_UNUSED(time);

	retError = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
		case MODULE_MSG_TYPE_CMD:
			switch (msgPtr->messageID)
			{
				case MODULE_MSG_ID_CMD_FAN_CH_TEST_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FanTestStr))
					{
						retError = FanTestHandler((FanTestStr *)msgPtr->msgBufPtr);
					}
					break;

				default:
					retError = SB_ERROR_CANC_NOT_HANDLED;
					break;
			}
		break;

		case MODULE_MSG_TYPE_CFG:
			switch (msgPtr->messageID)
			{
				case MODULE_MSG_ID_CFG_FAN_CONTROLLER_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FanConfigStr))
					{
						retError = FanConfig((FanConfigStr *)msgPtr->msgBufPtr);
					}
					break;

				default:
					retError = SB_ERROR_CANC_NOT_HANDLED;
					break;
			}
		break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}


SB_ERROR FanControllerTick(void)
{
	SB_ERROR retError = SB_ERROR_NONE;

	/* if fan not fitted then use fan output as either digital output or external LED supply */
	if (nvramAppUserBlkPtr->fanFitted == LU_FALSE) {
		if (fanAsExternalSupply == LU_TRUE) {
			/* If fan is not fitted, then output is used to mirror the power saving mode to drive an external light */
			if (SSGetPowerSaveMode()) {
				// if power saving mode active then force output to 0 V
				retError = IOManagerSetValue(IO_ID_FAN_ON, 0);

			} else {
				/* If RTU is not in power saver mode
				 * then fan output used to drive external LED (24 V)
				 * If user do "fan test", then force this output to 0 V */
				if (fanTest) {
					retError = FanTest();
				} else {
					retError = IOManagerSetValue(IO_ID_FAN_ON, 1);
				}

			}
		} else if (fanAsDigitalOutput == LU_TRUE) {
		    /* Lets Digital Output controller do job.
		     * For that enable digital out controller tick task. */
		    DigitalOutControllerTick();
		} else {
		    // Not selected any fan functionality. Do nothing.
		}
	} else if (fanTest) {
		/*
		 * If the fan test message has been received then turn the fan on
		 * for the duration specified in the message
		 */
		retError = FanTest();
	} else {
		/*
		 * Otherwise call the fan controller for normal fan operation.
		 * for the duration specified in the message
		 */
		retError = FanController();
	}
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FanController(void)
{
	static lu_uint32_t msCount = 0;
	lu_int32_t 	psuTemp;
	SB_ERROR 	retError,RetVal;
	lu_float32_t	psuTemperature = 0;


	if (nvramAppUserBlkPtr->fanFitted == LU_TRUE)
	{
		/*
		 * Get the RTU Ambient temperature and compare it against the Fan Temperature Threshold value.
		 * If the Ambient is greater than the threshold make sure the fan is on.
		 * If the Ambient is lower than the threshold make sure the fan is off.
		 * If the fan is on then check the fault conditions after the hysteresis time has expired.
		 */

		/* Read temperature sensor */
		retError = IOManagerGetValue(IO_ID_PSU_TEMP, &psuTemp );
		psuTemp /= TEMP_SENSOR_RESOLUTION;
		psuTemperature = psuTemp;

		/* Use default value for temperature if error reading sensor */
		if (retError != SB_ERROR_NONE)
		{
			psuTemperature = 28;	/**** Temporary code until valid value can be returned ********/
		}

		if ( fanState == FAN_OFF )
		{
			/* Check for temperature greater than threshold */
			if ( psuTemperature  > (nvramAppUserBlkPtr->fanTempThreshold + nvramAppUserBlkPtr->fanTempHysteresis) )
			{
				/* Turn on the Fan */
				retError = IOManagerSetValue(IO_ID_FAN_ON, 1);
				retError = IOManagerSetValue(IO_ID_VIRT_FAN_ON, 1);
				/* Turn on fan green LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_GREEN, 1);
				/* Turn off fan supply red LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 0);
				/* Update Fan state */
				fanState  = FAN_ON;
				msCount = 0;
			}
		}
		else if ( fanState == FAN_ON )
		{
			/* Check for temperature less than threshold */
			if ( psuTemperature < (nvramAppUserBlkPtr->fanTempThreshold - nvramAppUserBlkPtr->fanTempHysteresis) )
			{
				retError = IOManagerSetValue(IO_ID_FAN_ON, 0);
				retError = IOManagerSetValue(IO_ID_VIRT_FAN_ON, 0);
				/* Turn off fan green LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_GREEN, 0);
				/* Turn off fan supply red LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 0);
				/* Update Fan state */
				fanState = FAN_OFF;
			}
			else
			{
				/* If the fan is on then check for fan faults */
				if( FanCheck() == LU_TRUE )
				{
					fanState = FAN_FAULT;
				}
			}
		}

		/* If the fan is on then check for fan faults */
		else if (fanState == FAN_FAULT)
		{
			/* Wait for fan hysteresis time to expire before trying to reset the fan */
			if(msCount >=  nvramAppUserBlkPtr->fanFaultHysteresisMs)
			{
				/* Hysteresis time has expired so reset fan hysteresis period */
				msCount  = 0;
				/* Turn on the Fan */
				retError = IOManagerSetValue(IO_ID_FAN_ON, 1);
				retError = IOManagerSetValue(IO_ID_VIRT_FAN_ON, 1);
				/* Turn on fan green LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_GREEN, 1);
				/* Turn off fan supply red LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 0);

				// fanSpeedSensorFitted to be checked at a later date
				// Consider turning off the fan for a period if a fault detected
				//    using the fan test
				if (FanCheck() == LU_TRUE)
				{
					fanState = FAN_FAULT;
				}
				else
				{
					IOManagerSetValue(IO_ID_VIRT_FAN_FAULT, 0);
					fanState = FAN_ON;
				}
			}
			else
			{
				msCount += FAN_CON_TICK_MS;
			}
		}
	}
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t FanCheck(void)
{
	lu_int32_t 	fanPSGood;
	SB_ERROR 	retError,RetVal;

	retError = IOManagerGetValue(IO_ID_FAN_PGOOD, &fanPSGood);
	// fanSpeedSensorFitted to be checked at a later date
	// Consider turning off the fan for a period if a fault detected
	//    using the fan test
	if (!fanPSGood)
	{
		/* Indicate fan fault */
		IOManagerSetValue(IO_ID_VIRT_FAN_FAULT, 1);
		/* Turn fan off for for the hysteresis period */
		retError = IOManagerSetValue(IO_ID_FAN_ON, 0);
		retError = IOManagerSetValue(IO_ID_VIRT_FAN_ON, 0);
		/* Turn off fan green LED */
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_GREEN, 0);
		/* Turn on fan supply red LED */
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 1);
		fanState = FAN_FAULT;
		return LU_TRUE;

	}
	else
	{
		IOManagerSetValue(IO_ID_VIRT_FAN_FAULT, 0);
		return LU_FALSE;
	}

}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FanTest(void)
{
	static lu_uint32_t msCount= 0, msTest = 0;
	lu_int32_t 	fanPSGood;
	lu_int32_t      value;
	static lu_bool_t testStart = LU_FALSE;
	SB_ERROR 	retError, RetVal;
	lu_float32_t	psuTemperature = 0;

	if (fanAsDigitalOutput == LU_TRUE) {
		/* If Fan configure as digital LED output
		 * then "Fan test" is not applicable.
		 * Must be some error.
		 * User must do testing using PSM Digital Channel test*/
		return SB_ERROR_PARAM;
	}

	/* Set the test duration count */
	msTest  = fanTestParamStr.testDuration * 1000;
	if (testStart == LU_FALSE)
	{
		msCount = 0;
		testStart = LU_TRUE;
	}

	/* Reset the fan fault point */
	IOManagerSetValue(IO_ID_VIRT_FAN_FAULT, 0);
	/* Turn off fan supply red LED */
	RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 0);


	/* If the fan isn't already on then turn it on */
	if (fanState == LU_FALSE) {
		if (nvramAppUserBlkPtr->fanFitted == LU_TRUE) {
			retError = IOManagerSetValue(IO_ID_FAN_ON, 1);
		} else {
			/* for fan as external supply, turn value off to 0V */
			retError = IOManagerSetValue(IO_ID_FAN_ON, 0);
		}
		IOManagerSetValue(IO_ID_VIRT_FAN_ON, 1);

		fanState = LU_TRUE;
		/* Turn on fan green LED */
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_GREEN, 1);
		/* Turn off fan supply red LED */
		RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 0);
	}

	/* If the fan is on then check for fan faults */
	if (fanState == LU_TRUE) {
		if ((msCount >= nvramAppUserBlkPtr->fanFaultHysteresisMs)
				&& (nvramAppUserBlkPtr->fanFitted == LU_TRUE)) {
			retError = IOManagerGetValue(IO_ID_FAN_PGOOD, &fanPSGood);
			// fanSpeedSensorFitted to be checked at a later date
			// Consider turning off the fan for a period if a fault detected using the fan test
			if (!fanPSGood) {
				IOManagerSetValue(IO_ID_VIRT_FAN_FAULT, 1);
				/* Turn on fan supply red LED */
				RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_RED, 1);
			} else {
				IOManagerSetValue(IO_ID_VIRT_FAN_FAULT, 0);
			}
		}

		/* Check for test finished. Turn off the fan if it is. */
		if ((msCount += FAN_CON_TICK_MS) > msTest) {

			/* Turn off fan supply green LED */
			RetVal = IOManagerSetValue(IO_ID_LED_CTRL_FAN_GREEN, 0);
			fanTest = LU_FALSE;
			testStart = LU_FALSE;
			if (fanAsExternalSupply == LU_TRUE) {
				/* For Fan as external supply, set back to 24 V (default value) */
				retError = IOManagerSetValue(IO_ID_FAN_ON, 1);
			} else if (psuTemperature
					< (nvramAppUserBlkPtr->fanTempThreshold
							- nvramAppUserBlkPtr->fanTempHysteresis)) {
				/* For fan fitted, if temperature is below threshold
				 * then only set to 0 V otherwise let it be 24 V to avoid flickering */
				retError = IOManagerSetValue(IO_ID_FAN_ON, 0);
			}

			retError = IOManagerSetValue(IO_ID_VIRT_FAN_ON, 0);
			fanState = LU_FALSE;
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FanTestHandler(FanTestStr *fanTestStr)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= fanTestStr->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	fanTestParamStr.testDuration = fanTestStr->testDuration;

	/*
	 * Turn on the fan for the test duration. Will be run in fanController()
	 *  */
	fanTest = LU_TRUE;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_FAN_CH_TEST_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FanConfig(FanConfigStr *fanConfigPtr)
{
	ModReplyStr reply;
	SB_ERROR sbError;

	reply.channel 	= 0;
	reply.status  	= REPLY_STATUS_OKAY;

	/*
	 * Sanity check values. If any config values fail then use default values for all
	 */
	nvramAppUserBlkPtr->fanFitted 				= fanConfigPtr->fanFitted;

	if (fanConfigPtr->fanFitted == LU_TRUE )
	{
		if( ( fanConfigPtr->fanTempHysteresis < 2 ) || ( fanConfigPtr->fanTempHysteresis > 10) )
		{
			reply.status  = REPLY_STATUS_PARAM_ERROR;
		}
		else if( ( fanConfigPtr->fanFaultHysteresisMs < 500 ) || ( fanConfigPtr->fanFaultHysteresisMs > 20000) )
		{
			reply.status  = REPLY_STATUS_PARAM_ERROR;
		}
		else
		{
			/* Sanity checks passed so copy config values to local structure */
			nvramAppUserBlkPtr->fanSpeedSensorFitted	= fanConfigPtr->fanSpeedSensorFitted;
			nvramAppUserBlkPtr->fanTempThreshold 		= fanConfigPtr->fanTempThreshold;
			nvramAppUserBlkPtr->fanTempHysteresis 	    = fanConfigPtr->fanTempHysteresis;
			nvramAppUserBlkPtr->fanFaultHysteresisMs 	= fanConfigPtr->fanFaultHysteresisMs;
		}
	}

	fanAsDigitalOutput = fanConfigPtr->fanDigitalOutput;
	fanAsExternalSupply = fanConfigPtr->fanExternalSupply;

	/* Call function to indicate NVRAM should be updated */
	sbError = NVRamApplicationWriteUpdate( NVRAM_APP_BLK_USER_A );

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_FAN_CONTROLLER_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
