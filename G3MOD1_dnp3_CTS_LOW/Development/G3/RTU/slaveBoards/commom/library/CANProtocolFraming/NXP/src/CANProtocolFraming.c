/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   31/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <string.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolFraming.h"
#include "CANProtocol.h"
#include "ModuleProtocol.h"

#include "systemStatus.h"

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_can.h"

#include "lpc17xx_clkpwr.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* CAN LEDs */
#define CAN_LED_GREEN_PORT							1
#define CAN_LED_GREEN_PIN							(1 <<28)
#define CAN_LED_RED_PORT							1
#define CAN_LED_RED_PIN								(1 <<29)

/* Increment and wrap. Optimised version: works ONLY with multiple of 2 values */
#define CAN_INCREMENT_AND_WRAP(a, mask) ((a) = (((a)+ 1) & mask))

/* CAN controller buffer layout */

/* The buffer length MUST be a multiple of 2. We use the CAN_INCREMENT_AND_WRAP
 * optimised macro.16 buffers: 0 -15
 */
#define CAN_RX_BUF_MASK       (0x0F)
#define CAN_RX_BUF_LEN        (CAN_RX_BUF_MASK + 1)

#define CAN_TX_BUF_MASK       (0xFF)
#define CAN_TX_BUF_LEN        (CAN_TX_BUF_MASK + 1)

#define CAN_BASE(a)        ((LPC_CAN_TypeDef*)(a))

#define CAN_SHORT_MSG_SLOT    1
#define CAN_FRAGMENT_MSG_SLOT 2

/* Fragmentation Data */
#define CAN_FRAGMENT_HEADER_SIZE    (sizeof(CANFragmentHeaderStr))
#define CAN_FRAGMENT_PAYLOAD        (7)
#define CAN_FRAGMENT_ID_MASK        (0x3F) /* 6 bits */
#define CAN_FRAGMENT_ID             (0xFF)

/* Array size to track fragment numbers for duplicate rejection */
#define CAN_FRAGMENT_ID_MASK_BITS	32
#define CAN_FRAGMENT_ID_MASK_SIZE	((CAN_MAX_FRAGMENTS + CAN_FRAGMENT_ID_MASK_BITS) / CAN_FRAGMENT_ID_MASK_BITS)

/* Check if fragment id is set in mask */
#define CAN_FRAGMENT_ID_MASK_IS_NOT_SET(mask, id) 	( !(mask[(id >> 5) & CAN_FRAGMENT_ID_MASK] & (1 << (id & 0x1f)) ) )

/* Set fragment id in mask */
#define CAN_FRAGMENT_ID_MASK_SET(mask, id) 			( mask[(id >> 5) & CAN_FRAGMENT_ID_MASK] |= (1 << (id & 0x1f)) )

/* Get next fragment ID. Only one fragmented message at a time can be sent.
 * This doesn't need to be atomic
 */
#define CAN_FRAGMENT_NEXT_ID(a) (CAN_INCREMENT_AND_WRAP(a, CAN_FRAGMENT_ID_MASK))

#define UPDATE_STATS(a) ((CANFramingData.stats.a)++)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    CAN_FRAGMENTATION_STATUS_IDLE = 0,
    CAN_FRAGMENTATION_STATUS_START   ,
    CAN_FRAGMENTATION_STATUS_CONT    ,
    CAN_FRAGMENTATION_STATUS_END     ,

    CAN_FRAGMENTATION_STATUS_LAST
}CAN_FRAGMENTATION_STATUS;

/*!
 * CAN Framing statistics
 */
typedef struct CANFramingStatDef
{
    lu_uint32_t canError          ;

    lu_uint32_t	canBusError;
    lu_uint32_t canArbitrationError;
    lu_uint32_t canDataOverrun;
    lu_uint32_t canBusOff;
    lu_uint32_t canErrorPassive;
    lu_uint32_t canErrorBTR;

    /* TX */
    lu_uint32_t TXShortMessageReq ;
    lu_uint32_t TXLongMessageReq  ;
    lu_uint32_t TXLongMessage     ;
    lu_uint32_t TXTotal           ;
    lu_uint32_t TXError           ;
    lu_uint32_t TXFragmentBusy    ;
    lu_uint32_t TXFragmentError   ;
    lu_uint32_t TXTooLong         ;

    /* RX */
    lu_uint32_t RXMessageTotal    ;
    lu_uint32_t RXLongMessageTotal;
    lu_uint32_t RXMessageLost     ;
    lu_uint32_t RXFragmentError   ;

    lu_uint32_t icr;
    lu_uint32_t gsr;
    lu_uint32_t mod;
    lu_uint32_t btr;
    lu_uint32_t ier;

}CANFramingStatStr;

/*!
 * TX Fragmentation State Machine Control Data
 */
typedef struct CANTXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Fragment ID counter */
    lu_uint8_t counter;

    /*! Bytes already sent */
    lu_uint32_t sentData;

    /*! ObjectID (1-32) of the last sent packet */
    lu_int32_t objID;

    /* CAN message Object */
    CAN_MSG_Type CANMsgObject;

    /*! Total length of the buffer */
    lu_uint32_t bufferLen;
    /*! Local TX buffer */
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
}CANTXFragmentationStr;

/*!
 * RX Fragmentation State Machine Control Data
 */
typedef struct CANRXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Expected fragment ID */
    lu_uint8_t fragmentID;
    /*! received data */
    lu_uint32_t recvData;
    /*! Expected CAN header */
    lu_uint32_t expectedHeader;

    /* Count total numb fragments received */
    lu_uint32_t fragmentCount;
    /* Track fragments received to reject duplicates */
    lu_uint32_t fragmentIDMask[CAN_FRAGMENT_ID_MASK_SIZE];

    /*! G3 CAN message structure */
    CANFramingMsgStr  CANFramingMsg;
    /*! Local buffer */
    lu_uint8_t buffer[MODULE_MESSAGE_LENGTH];
}CANRXFragmentationStr;

/*!
 * RX short message pool
 */
typedef struct msgPoolDef
{
    /*! Buffer status */
    lu_bool_t        free;
    /*! G3 CAN message structure */
    CANFramingMsgStr msg;
    /*! Local Buffer */
    lu_uint8_t       msgBuf[CAN_MAX_PAYLOAD];
}msgPoolStr;

/*!
 * TX message pool
 */
typedef struct TXmsgPoolDef
{
    CAN_MSG_Type msgRingBuffer[CAN_TX_BUF_LEN];
    lu_uint32_t readIdx;
    lu_uint32_t writeIdx;
}TXmsgPoolStr;

/*!
 * Library control data
 */
typedef struct CANFramingDef
{
    /*! Library status */
    lu_bool_t   initialzied;
    /*! Device type */
    lu_uint8_t  device     ;
    /*! Device address */
    lu_uint8_t  deviceID   ;

    /*! Pins Map */
    CANFramingMapStr *pinMapPtr;

    /*! ID Mask */
    lu_uint32_t msgIDMask;

    /*! Receive Message Pool */
    msgPoolStr RXMsgPool[CAN_RX_BUF_LEN];

    /* Transmit message queue */
    TXmsgPoolStr TXMsgQueue;//MG

    /*! Receive buffer ID */
    lu_uint32_t rxIdx;

    /*! Fragmentation Control data (TX) */
    CANTXFragmentationStr TXFragment;

    /*! Fragmentation Control data (RX) */
    CANRXFragmentationStr RXFragment;

    /*! Forward data callback */
    CANFramingCB callBack   ;

    /*! Statistics */
    CANFramingStatStr stats;

    /* */
    lu_uint32_t busErrorTimerMs;

    lu_uint32_t canLedFlashTimerMs;

    lu_uint32_t btrReportTimerMs;
}CANFramingStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                          CAN_MSG_Type *CANMsgTxPtr,
                          lu_bool_t fragment
                        );

void initHeaderCANFMsg( CANFramingMsgStr *msgPtr,
                        CANHeaderStr *CANHeaderPtr
                      );

static SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr);
static SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID);
static SB_ERROR CANFramingFillFragment(void);
static SB_ERROR CANFramingRecvFragment(CAN_MSG_Type *CANMsgPtr);
static SB_ERROR CANFramingRecvFragmentRelease(CANFramingMsgStr *msgPtr);

static SB_ERROR CANFramingInitTXMsgPool(void);
static CAN_MSG_Type* CANFramingGetTXMsg(void);
static SB_ERROR CANFramingAddMsg(CAN_MSG_Type *msgPtr);

void CANFramingIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANFramingStr CANFramingData;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_uint32_t CANFramingGetRXMessageTotal(void)
{
	return CANFramingData.stats.RXMessageTotal;
}

lu_uint32_t CANFramingGetRXMessageLost(void)
{
	return CANFramingData.stats.RXMessageLost;
}

lu_uint32_t CANFramingGetcanError(void)
{
	return CANFramingData.stats.canError;
}

lu_uint32_t CANFramingGetICR(void)
{
	return CANFramingData.stats.icr;
}

lu_uint32_t CANFramingGetIER(void)
{
	return CANFramingData.stats.ier;
}

lu_uint32_t CANFramingGetGSR(void)
{
	return CANFramingData.stats.gsr;
}

lu_uint32_t CANFramingGetMOD(void)
{
	return CANFramingData.stats.mod;
}

lu_uint32_t CANFramingGetBTR(void)
{
	return CANFramingData.stats.btr;
}

lu_uint32_t CANFramingGetcanBusError(void)
{
	return CANFramingData.stats.canBusError;
}

lu_uint32_t CANFramingGetcanArbitrationError(void)
{
	return CANFramingData.stats.canArbitrationError;
}

lu_uint32_t CANFramingGetcanDataOverrun(void)
{
	return CANFramingData.stats.canDataOverrun;
}

lu_uint32_t CANFramingGetcanBusOff(void)
{
	return CANFramingData.stats.canBusOff;
}

lu_uint32_t CANFramingGetcanErrorPassive(void)
{
	return CANFramingData.stats.canErrorPassive;
}

lu_uint32_t CANFramingGetcanErrorBTR(void)
{
	return CANFramingData.stats.canErrorBTR;
}

lu_uint32_t CANFramingGetTXTotal(void)
{
	return CANFramingData.stats.TXTotal;
}

lu_uint32_t CANFramingGetRxLost(void)
{
	return CANFramingData.stats.RXMessageLost;
}

SB_ERROR CANFramingInit( lu_uint32_t       clock    ,
                         lu_uint32_t       bitRate  ,
		                 lu_uint8_t        device   ,
		                 lu_uint8_t        deviceID ,
		                 CANFramingMapStr *pinMapPtr,
		                 SUIRQPriorityStr  priority ,
		                 CANFramingCB      callBack
		               )
{
    lu_uint32_t i;
    msgPoolStr *msgPoolPtr;
    CANHeaderStr *CANHeaderPtr;
    PINSEL_CFG_Type pinCfg;
    LPC_CAN_TypeDef *canBase;

    LU_UNUSED(clock);

    CANFramingData.busErrorTimerMs = 0;

    /* Configure CAN LEDs */
    GPIO_SetDir(CAN_LED_GREEN_PORT, CAN_LED_GREEN_PIN, 1);

    GPIO_SetDir(CAN_LED_RED_PORT, CAN_LED_RED_PIN, 1);

    /* Parameter check */
    if ( (pinMapPtr == NULL) ||
         (callBack  == NULL)
       )
    {
        return SB_ERROR_PARAM;
    }

    CANFramingData.btrReportTimerMs = 500;

 //   if (CANFramingData.initialzied == LU_TRUE)
 //   {
 //       /* Module already initialized */
 //       return SB_ERROR_INITIALIZED;
 //   }

    SU_CRITICAL_REGION_ENTER()

    /* Module initialized */
    CANFramingData.initialzied = LU_TRUE;

    /* Save callback */
    CANFramingData.callBack = callBack;

    /* Save PIN map */
    CANFramingData.pinMapPtr = pinMapPtr;

    // JF Fix
 //   CAN_ModeConfig( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
 //                           CAN_RESET_MODE,
 //                         	ENABLE
 //                         );

    /* Save source ID */
    CANFramingData.device   = device  ;
    CANFramingData.deviceID = deviceID;

    /* Mask everything apart the destination */
    CANFramingData.msgIDMask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&CANFramingData.msgIDMask);
    CANHeaderPtr->deviceDst   = ((lu_uint8_t)(0x1F));
    CANHeaderPtr->deviceIDDst = ((lu_uint8_t)(0x07));

    /* Configure CAN Pins. */
    pinCfg.Funcnum = pinMapPtr->CanRxConfigurePin;
    pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
    pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
    pinCfg.Pinnum = pinMapPtr->CanRxPin;
    pinCfg.Portnum = pinMapPtr->gpioPortBase;
    PINSEL_ConfigPin(&pinCfg);

    pinCfg.Funcnum = pinMapPtr->CanTxConfigurePin;
    pinCfg.Pinnum = pinMapPtr->CanTxPin;
    PINSEL_ConfigPin(&pinCfg);

    /* Initialize CAN1 */
    CAN_Init(CAN_BASE(CANFramingData.pinMapPtr->CanBase), bitRate);

    /* Initialize RX message pool */
    for(i = 0; i < CAN_RX_BUF_LEN; i++)
    {
        msgPoolPtr = &CANFramingData.RXMsgPool[i];
        msgPoolPtr->free = LU_TRUE;
        msgPoolPtr->msg.ID = i;
        msgPoolPtr->msg.msgBufPtr = &msgPoolPtr->msgBuf[0];
    }

    /* Initialize TX message pool */
    CANFramingInitTXMsgPool();

    /* Enable Hardware Acceptance Filter */
    CAN_SetAFMode(LPC_CANAF, CAN_Normal);

    /* Initialize Rx index */
    CANFramingData.rxIdx = 0;

    /* Enable Receive Interrupt */
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_RIE, ENABLE);

    /* Enable Transmit Interrupt */
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_TIE1, ENABLE);
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_TIE2, ENABLE);
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_TIE3, ENABLE);

    /* Enable Error Interrupt */
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_EIE, ENABLE); //CAN Error Warning
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_EPIE, ENABLE); //CAN Error Passive
    CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_BEIE, ENABLE); //CAN Bus Error

    /* Set CAN Interrupt priority and enable interrupt */
    SUSetIRQPriority(pinMapPtr->CanInt, priority, LU_TRUE);

    SU_CRITICAL_REGION_EXIT()

    canBase = CAN_BASE(CANFramingData.pinMapPtr->CanBase);

    CANFramingData.stats.gsr = canBase->GSR;
	CANFramingData.stats.mod = canBase->MOD;
	CANFramingData.stats.btr = canBase->BTR;
	CANFramingData.stats.ier = canBase->IER;


    return SB_ERROR_NONE;
}

SB_ERROR CANFramingTick(void)
{
	LPC_CAN_TypeDef *canBase;

	canBase = CAN_BASE(CANFramingData.pinMapPtr->CanBase);

	/* CAN bus error recovery */
	SU_CRITICAL_REGION_ENTER()

	if (CANFramingData.busErrorTimerMs)
	{
		CANFramingData.busErrorTimerMs -= CAN_TICK_MS;

		if (!CANFramingData.busErrorTimerMs)
		{
			canBase->GSR = 0x0;

			// Restore baud rate ??

			GPIO_ClearValue(CAN_LED_RED_PORT, CAN_LED_RED_PIN);

			CAN_ModeConfig( canBase,
							CAN_OPERATING_MODE,
							ENABLE
						  );

			// Re-enable error interrupts
			CAN_IRQCmd(canBase, CANINT_EIE, ENABLE); //CAN Error Warning
			CAN_IRQCmd(canBase, CANINT_EPIE, ENABLE); //CAN Error Passive
			CAN_IRQCmd(canBase, CANINT_BEIE, ENABLE); //CAN Bus Error
		}
	}

	SU_CRITICAL_REGION_EXIT()

	if (CANFramingData.canLedFlashTimerMs)
	{
		CANFramingData.canLedFlashTimerMs -= CAN_TICK_MS;

		if (!CANFramingData.canLedFlashTimerMs)
		{
			/* Turn off Green CAN LED */
			GPIO_ClearValue(CAN_LED_GREEN_PORT, CAN_LED_GREEN_PIN);
		}
	}

	if (CANFramingData.btrReportTimerMs)
	{
		CANFramingData.btrReportTimerMs -= CAN_TICK_MS;

		if (!CANFramingData.btrReportTimerMs)
		{
//			_printf("[BTR] %x [PCONP] %x [IER] %x [MOD] %x\n\r", canBase->BTR, LPC_SC->PCONP, canBase->IER, canBase->MOD);
			CANFramingData.btrReportTimerMs = 500;
		}

		/* Restore baud rate */
		/*if (canBase->BTR != 0xcdc003)
		{
			SU_CRITICAL_REGION_ENTER()

			UPDATE_STATS(canErrorBTR);

			CAN_ModeConfig( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
			                        CAN_RESET_MODE,
			                      	ENABLE
			                      );

			canBase->BTR = 0xcdc003;

			CAN_ModeConfig( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
										CAN_OPERATING_MODE,
										ENABLE
									  );

			SU_CRITICAL_REGION_EXIT()
		}*/
	}

	return SB_ERROR_NONE;
}


SB_ERROR CANFramingPulseCANLed(void)
{
	CANFramingData.canLedFlashTimerMs = 50;

	if (!SSGetPowerSaveMode())
	{
		/* Turn on Green CAN LED */
		GPIO_SetValue(CAN_LED_GREEN_PORT, CAN_LED_GREEN_PIN);
	}

	/* Turn off Red CAN LED */
	GPIO_ClearValue(CAN_LED_RED_PORT, CAN_LED_RED_PIN);

	return SB_ERROR_NONE;
}

SB_ERROR CANFramingAddFilter( const filterTableStr *filterTable,
                              lu_uint32_t size
                            )
{
    lu_uint32_t           i;
    CANHeaderStr         *CANHeaderPtr;
    const filterTableStr *filterTablePtr;
    lu_uint32_t           id;
    CAN_ERROR             error;
    SB_ERROR              ret = SB_ERROR_NONE;

    if (CANFramingData.initialzied == LU_FALSE)
    {
        /* Module NOT initialized */
        return SB_ERROR_INITIALIZED;
    }

    if (filterTable == NULL)
    {
        return SB_ERROR_PARAM;
    }

    SU_CRITICAL_REGION_ENTER()

    /* Initialize ID */
    CANHeaderPtr = (CANHeaderStr*)(&id);

    /* Accept messages only from MCM 0 */
    CANHeaderPtr->deviceSrc   = MODULE_MCM ;
    CANHeaderPtr->deviceIDSrc = MODULE_ID_0;


    for(i = 0; i < size; i++)
    {
        filterTablePtr = &filterTable[i];

        /* Set message type/ID */
        CANHeaderPtr->messageType = filterTablePtr->messageType;
        CANHeaderPtr->messageID   = filterTablePtr->messageID  ;

        /* Set fragmentation bit */
        CANHeaderPtr->fragment = (filterTablePtr->fragmented == LU_FALSE)? 0:1;

        /* Set destination */
        if(filterTablePtr->broadcast == LU_FALSE)
        {
            CANHeaderPtr->deviceDst   = CANFramingData.device  ;
            CANHeaderPtr->deviceIDDst = CANFramingData.deviceID;
        }
        else
        {
            CANHeaderPtr->deviceDst   = MODULE_BRD   ;
            CANHeaderPtr->deviceIDDst = MODULE_ID_BRD;
        }

        /* Clear unused bit */
        id &= CAN_EXT_ID_MASK;

        /* Load ID */
        error =
             CAN_LoadExplicitEntry( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
                                    id                                         ,
                                    EXT_ID_FORMAT
                                  );
        if(error != CAN_OK)
        {
            ret = SB_ERROR_CANF;
            break;
        }
    }

    SU_CRITICAL_REGION_EXIT()

    return ret;
}

SB_ERROR CANFramingFilterEnable(lu_bool_t enable)
{
    if(enable == LU_TRUE)
    {
        /* Enable Hardware Acceptance Filter */
        CAN_SetAFMode(LPC_CANAF, CAN_Normal);
    }
    else
    {
        /* Disable Hardware Acceptance Filter */
        CAN_SetAFMode(LPC_CANAF, CAN_AccBP);
    }

    return SB_ERROR_NONE;
}

SB_ERROR CANFramingReleaseMsg(CANFramingMsgStr *msgPtr)
{
    SB_ERROR ret = SB_ERROR_NONE;

    if (msgPtr == NULL)
    {
        return SB_ERROR_PARAM;
    }

    if (msgPtr->ID != CAN_FRAGMENT_ID)
    {
        /* Release short message buffer */
        if (msgPtr->ID < CAN_RX_BUF_LEN)
        {
            CANFramingData.RXMsgPool[msgPtr->ID].free = LU_TRUE;
        }
    }
    else
    {
        /* Release fragmented message */
        ret = CANFramingRecvFragmentRelease(msgPtr);
    }

    return ret;
}

SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr)
{
    SB_ERROR ret;
    CAN_MSG_Type CANMsgTx;
    lu_uint32_t msgLen;
    lu_uint32_t i;

    if (msgPtr == NULL)
    {
        return SB_ERROR_PARAM;
    }

    msgLen = msgPtr->msgLen;
    if (msgLen > MODULE_MESSAGE_LENGTH)
    {
        UPDATE_STATS(TXTooLong);
        return SB_ERROR_CANF_MSG_TOO_LONG;
    }

    if (msgLen <= CAN_MAX_PAYLOAD)
    {
        /* Short message. Send immediately */
        initHeaderMsgObject(msgPtr, &CANMsgTx, LU_FALSE);
        CANMsgTx.len= msgPtr->msgLen;
        for(i = 0; i < msgPtr->msgLen; i++)//MG IMPROVE !!!!!!
        {
            if (i < 4)
            {
                CANMsgTx.dataA[i] = msgPtr->msgBufPtr[i];
            }
            else
            {
                CANMsgTx.dataB[i-4] = msgPtr->msgBufPtr[i];
            }
        }

        /* Send short message using slot 1 */
        if( CAN_SendMsgSlot( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
                             &CANMsgTx,
                             CAN_SHORT_MSG_SLOT
                           ) < 0)
        {
            /* hardware buffer full. Queue the message */
            ret = CANFramingAddMsg(&CANMsgTx);
            if(ret == SB_ERROR_NONE)
            {
                UPDATE_STATS(TXShortMessageReq);
                UPDATE_STATS(TXTotal);
            }
            else
            {
                UPDATE_STATS(TXError);
            }

            return ret;
        }
        else
        {
            UPDATE_STATS(TXShortMessageReq);

            return SB_ERROR_NONE;
        }
    }
    else
    {
        /* Long message. Start fragmentation */
        UPDATE_STATS(TXLongMessageReq);
        return CANFramingSendFragment(msgPtr);
    }

    return SB_ERROR_CANF;
}

void CANFramingEnterBusOff(void)
{
	LPC_CAN_TypeDef *canBase;

	canBase = CAN_BASE(CANFramingData.pinMapPtr->CanBase);

	canBase->MOD  = 1;
	canBase->GSR |= (255 << 24);
	canBase->MOD  = 0;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize CAN message using a G3 CAN Message
 *
 *   \param msgPtr G3 CAN Message used to initialize the CAN object
 *   \param CANMsgTxPtr CAN message to initialize
 *   \param fragment set to LU_TRUE if this is a fragmented message
 *
 *   \return None
 *
 ******************************************************************************
 */
inline void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                                 CAN_MSG_Type *CANMsgTxPtr,
                                 lu_bool_t fragment
                               )
{
    CANHeaderStr *CANHeaderPtr;

    CANMsgTxPtr->id = 0;
    CANMsgTxPtr->format = EXT_ID_FORMAT;
    CANMsgTxPtr->type = DATA_FRAME;
    CANHeaderPtr = (CANHeaderStr*)(&CANMsgTxPtr->id);
    CANHeaderPtr->deviceSrc   = msgPtr->deviceSrc;
    CANHeaderPtr->deviceIDSrc = msgPtr->deviceIDSrc;
    CANHeaderPtr->deviceDst   = msgPtr->deviceDst;
    CANHeaderPtr->deviceIDDst = msgPtr->deviceIDDst;
    CANHeaderPtr->messageType = msgPtr->messageType;
    CANHeaderPtr->messageID   = msgPtr->messageID;
    if(fragment == LU_TRUE)
    {
        CANHeaderPtr->fragment = 1;
    }
}

/*!
 ******************************************************************************
 *   \brief Initialize G3 CAN Framing Message using a CAN object
 *
 *   \param msgPtr CAN object used to initialize the G3 CAN Message
 *   \param MsgObjectTxPtr G3 CAN Message to initialize
 *
 *   \return None
 *
 ******************************************************************************
 */
inline void initHeaderCANFMsg( CANFramingMsgStr *msgPtr,
                               CANHeaderStr *CANHeaderPtr
                             )
{
    msgPtr->messageType = CANHeaderPtr->messageType;
    msgPtr->messageID   = CANHeaderPtr->messageID;
    msgPtr->deviceDst   = CANHeaderPtr->deviceDst;
    msgPtr->deviceIDDst = CANHeaderPtr->deviceIDDst;
    msgPtr->deviceSrc   = CANHeaderPtr->deviceSrc;
    msgPtr->deviceIDSrc = CANHeaderPtr->deviceIDSrc;
}

/*!
 ******************************************************************************
 *   \brief Start the transmission of a "long" message
 *
 *   Only one long message at a time can be transmitted. The first fragment
 *   is sent immediately. The successive fragments are automatically sent
 *   in the ISR when the transmission of the previous message is terminated.
 *
 *   \param msgPtr Object to send
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr)
{
    SB_ERROR    ret = SB_ERROR_NONE;

    switch(CANFramingData.TXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:

            /* Initialise control data */
            CANFramingData.TXFragment.objID = 0;
            CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_START;
            CANFramingData.TXFragment.sentData = 0;
            CANFramingData.TXFragment.counter = 0; // Reset fragment to zero for first fragment

            /* Save message buffer */
            CANFramingData.TXFragment.bufferLen = msgPtr->msgLen;
            memcpy( CANFramingData.TXFragment.buffer,
                    msgPtr->msgBufPtr,
                    msgPtr->msgLen
                  );

            /* Initialize common field of CAN MsgObj */
            initHeaderMsgObject(msgPtr, &CANFramingData.TXFragment.CANMsgObject, LU_TRUE);

            /* Initialize payload */
            ret = CANFramingFillFragment();

            if (ret == SB_ERROR_NONE)
            {
                /* Update state */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_CONT;

                /* Send message - Fragmented messages use a different slot */
                CANFramingData.TXFragment.objID =
                        CAN_SendMsgSlot( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
                                         &CANFramingData.TXFragment.CANMsgObject,
                                         CAN_FRAGMENT_MSG_SLOT
                                      );
                if(CANFramingData.TXFragment.objID < 0)
                {
                    /* Error */
                    UPDATE_STATS(TXError);
                    ret = SB_ERROR_CANF_HW_TX_QUEUE_FULL;
                    /* Reset FSM */
                    CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                }
                else
                {
                	UPDATE_STATS(TXTotal);
                }
            }
            else
            {
                UPDATE_STATS(TXFragmentError);
                /* Reset FSM */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
            }
        break;

        default:
            UPDATE_STATS(TXFragmentError);
            ret = SB_ERROR_BUSY;
        break;
    }


    return ret;
}

/*!
 ******************************************************************************
 *   \brief Continue the transmission of the "long" message
 *
 *   This function should be called directly from the ISR. A new fragment
 *   is sent only when a previous fragment has been successfully sent. The
 *   objID is used to identify the transmitted object
 *
 *   \param objID Id of the object that has just been transmitted (1-32)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragmentInt(lu_int32_t objID)
{
    SB_ERROR ret = SB_ERROR_NONE;

    switch(CANFramingData.TXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:
        case CAN_FRAGMENTATION_STATUS_START:
            /* Do nothing */
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            if (objID == CANFramingData.TXFragment.objID)
            {
                /* this interrupt is for us
                 * Initialize payload
                 */
                if(CANFramingFillFragment() == SB_ERROR_NONE)
                {
                    /* Send message */
                    CANFramingData.TXFragment.objID =
                            CAN_SendMsgSlot(CAN_BASE(CANFramingData.pinMapPtr->CanBase),
                                            &CANFramingData.TXFragment.CANMsgObject,
                                            CAN_FRAGMENT_MSG_SLOT
                                          );
                    if(CANFramingData.TXFragment.objID < 0)
                    {
                        /* Error */
                        UPDATE_STATS(TXError);//MG
                        /* Reset FSM */
                        CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                    }
                    else
                    {
                    	UPDATE_STATS(TXTotal);
                        ret = SB_ERROR_CANF_TX_FRAGMENT;
                    }
                }
                else
                {
                    UPDATE_STATS(TXFragmentError);
                    /* Reset FSM */
                    CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                }
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:
            /* Finished. Reset state machine */
            UPDATE_STATS(TXLongMessage);
            CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
        break;

        default:
            /* Do nothing */
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Prepare a new fragment to send
 *
 *   The buffer of the fragment CAN message is filled with new data.
 *   If necessary the State Machine status is also updated
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingFillFragment(void)
{
    lu_uint32_t bytesLeft;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    SB_ERROR ret = SB_ERROR_NONE;
    lu_uint32_t i;

    CANFragmentHeaderPtr =
        (CANFragmentHeaderStr *)&CANFramingData.TXFragment.CANMsgObject.dataA[0];

    /* Check left bytes and update state */
    bytesLeft = CANFramingData.TXFragment.bufferLen -
                CANFramingData.TXFragment.sentData   ;

    /* Set Fragment ID */
    CANFragmentHeaderPtr->id = CANFramingData.TXFragment.counter;

    /* update Counter */
    CAN_FRAGMENT_NEXT_ID(CANFramingData.TXFragment.counter);

    /* Set start/stop flags */
    switch(CANFramingData.TXFragment.state) //MG Reset State machine after a timeout
    {
        case CAN_FRAGMENTATION_STATUS_START:
            /* Sanity check */
            if (bytesLeft > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 0;
            }
            else
            {
                /* Error */
                UPDATE_STATS(TXFragmentError);
                ret = SB_ERROR_CANF_TX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            if (bytesLeft > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 1;
            }
            else
            {
                /* Last fragment */
                CANFragmentHeaderPtr->start = 0;
                CANFragmentHeaderPtr->stop  = 1;

                /* change state */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_END;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:

        break;

        default:
            UPDATE_STATS(TXFragmentError);
            ret = SB_ERROR_CANF_TX_FRAGMENT_ERROR;
        break;
    }

    if (ret == SB_ERROR_NONE)
    {
        /* Copy data to send into the buffer */
        bytesLeft = LU_MIN(bytesLeft, CAN_FRAGMENT_PAYLOAD);
        for(i = 0; i < bytesLeft; i++)
        {
            if (i < 3)
            {
                CANFramingData.TXFragment.CANMsgObject.dataA[i+1] =
                CANFramingData.TXFragment.buffer[CANFramingData.TXFragment.sentData+i];
            }
            else
            {
                CANFramingData.TXFragment.CANMsgObject.dataB[i-3] =
                CANFramingData.TXFragment.buffer[CANFramingData.TXFragment.sentData+i];

            }
        }

        /* Update sent bytes */
        CANFramingData.TXFragment.sentData += bytesLeft;

        /* Set payload length */
        CANFramingData.TXFragment.CANMsgObject.len = bytesLeft + CAN_FRAGMENT_HEADER_SIZE;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Manage the reception of a fragment
 *
 *   Only one "long" message at a time can be received. When a full message is
 *   correctly received the message is forwarded. The "long" message buffer
 *   should be released using the CANFramingReleaseMsg function.
 *   This function should be called with the ISR.
 *
 *   \param CANMsgPtr Message received.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingRecvFragment(CAN_MSG_Type *CANMsgPtr)
{
    lu_uint32_t msgLen;
    CANHeaderStr *CANHeaderPtr;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    lu_uint8_t i;
    lu_uint32_t idx;
    SB_ERROR ret = SB_ERROR_NONE;

    /* Decode fragmentation header */
    CANFragmentHeaderPtr = (CANFragmentHeaderStr*)(&CANMsgPtr->dataA[0]);

    /* Save message length */
    msgLen = CANMsgPtr->len - CAN_FRAGMENT_HEADER_SIZE;

    switch(CANFramingData.RXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:
            /* Check start/stop bits */
            if ( (CANFragmentHeaderPtr->start == 1) &&
                 (CANFragmentHeaderPtr->stop  == 0) &&
                 (CANFragmentHeaderPtr->id == 0)
               )
            {
                /* Valid first fragment
                 * Initialise control data
                 */
                CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_CONT;
                CANFramingData.RXFragment.fragmentID = CANFragmentHeaderPtr->id;
                CANFramingData.RXFragment.expectedHeader = CANMsgPtr->id;

                /* Reset mask to detect repeated fragments */
                for (idx = 0; idx < CAN_FRAGMENT_ID_MASK_SIZE; idx++)
				{
					CANFramingData.RXFragment.fragmentIDMask[idx] = 0L;
				}

                /* First fragment detected */
                CANFramingData.RXFragment.fragmentCount      = 1;
                CAN_FRAGMENT_ID_MASK_SET(CANFramingData.RXFragment.fragmentIDMask, 0);

                /* Set the next expect fragment ID */
                CAN_FRAGMENT_NEXT_ID(CANFramingData.RXFragment.fragmentID);

                /* Save buffer.
                 * CANFramingData.RXFragment.buffer[0] = CANMsgPtr->dataA[1];
                 * CANFramingData.RXFragment.buffer[1] = CANMsgPtr->dataA[2];
                 * CANFramingData.RXFragment.buffer[2] = CANMsgPtr->dataA[3];
                 * CANFramingData.RXFragment.buffer[3] = CANMsgPtr->dataB[0];
                 * CANFramingData.RXFragment.buffer[4] = CANMsgPtr->dataB[1];
                 * CANFramingData.RXFragment.buffer[5] = CANMsgPtr->dataB[2];
                 * CANFramingData.RXFragment.buffer[6] = CANMsgPtr->dataB[3];
                 */
                for(i = 0; i < msgLen; i++)
                {
                    if (i < 3)
                    {
                        CANFramingData.RXFragment.buffer[i] = CANMsgPtr->dataA[i+1];
                    }
                    else
                    {
                        CANFramingData.RXFragment.buffer[i] = CANMsgPtr->dataB[i-3];
                    }
                }
                CANFramingData.RXFragment.recvData = msgLen;
            }
            else
            {
                /* Wrong header */
                UPDATE_STATS(RXFragmentError);
                ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            /* Check start/stop bits */
            if (CANFragmentHeaderPtr->stop  == 1)
            {
                /* Valid header (continuation packet) */

            	/* Count total fragments received */
            	CANFramingData.RXFragment.fragmentCount++;

                /* Check msg header */
                if ( (CANFramingData.RXFragment.expectedHeader == CANMsgPtr->id) &&
                	 (CAN_FRAGMENT_ID_MASK_IS_NOT_SET(CANFramingData.RXFragment.fragmentIDMask,
                			                          CANFragmentHeaderPtr->id)) &&
                     (CANFramingData.RXFragment.recvData + msgLen <= MODULE_MESSAGE_LENGTH )
                   )
                {
                	CAN_FRAGMENT_ID_MASK_SET(CANFramingData.RXFragment.fragmentIDMask, CANFragmentHeaderPtr->id);

                    /* Fully validated header. Save buffer
                     * CANFramingData.RXFragment.buffer[recvData + 0] = CANMsgPtr->dataA[1];
                     * CANFramingData.RXFragment.buffer[recvData + 1] = CANMsgPtr->dataA[2];
                     * CANFramingData.RXFragment.buffer[recvData + 2] = CANMsgPtr->dataA[3];
                     * CANFramingData.RXFragment.buffer[recvData + 3] = CANMsgPtr->dataB[0];
                     * CANFramingData.RXFragment.buffer[recvData + 4] = CANMsgPtr->dataB[1];
                     * CANFramingData.RXFragment.buffer[recvData + 5] = CANMsgPtr->dataB[2];
                     * CANFramingData.RXFragment.buffer[recvData + 6] = CANMsgPtr->dataB[3];
                     */
                    for(i = 0; i < msgLen; i++)
                    {
                        if (i < 3)
                        {
                            CANFramingData.RXFragment.buffer[i+CANFramingData.RXFragment.recvData] = CANMsgPtr->dataA[i+1];
                        }
                        else
                        {
                            CANFramingData.RXFragment.buffer[i+CANFramingData.RXFragment.recvData] = CANMsgPtr->dataB[i-3];
                        }
                    }
                    CANFramingData.RXFragment.recvData += msgLen;

                    /* Last packet ? */
                    if(CANFragmentHeaderPtr->start == 0)
                    {
                    	if ((CANFramingData.RXFragment.fragmentCount - 1) ==
                    	     CANFragmentHeaderPtr->id)
                    	{
							/* Yes */
							CANHeaderPtr = (CANHeaderStr*)(&CANFramingData.RXFragment.expectedHeader);
							initHeaderCANFMsg(&CANFramingData.RXFragment.CANFramingMsg, CANHeaderPtr);

							/* Prepare data */
							CANFramingData.RXFragment.CANFramingMsg.ID = CAN_FRAGMENT_ID;
							CANFramingData.RXFragment.CANFramingMsg.msgLen = CANFramingData.RXFragment.recvData;
							CANFramingData.RXFragment.CANFramingMsg.msgBufPtr = CANFramingData.RXFragment.buffer;

							/* Update state */
							CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_END;

							/* forward data */
							if (CANFramingData.callBack(&CANFramingData.RXFragment.CANFramingMsg) ==
							    SB_ERROR_CANC_QUEUE_FULL)
							{
								/* Lost a message */
								UPDATE_STATS(RXMessageLost);
							}
							else
							{
								UPDATE_STATS(RXLongMessageTotal);
							}
                    	}
                    	else
                    	{
                    		/* Missing fragments - Reset State Machine */
							CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
							UPDATE_STATS(RXFragmentError);
							ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
                    	}
                    }
                    else
                    {
                        /* Set the next expect fragment ID */
                        CAN_FRAGMENT_NEXT_ID(CANFramingData.RXFragment.fragmentID);
                    }
                }
                else
                {
                    /* Wrong header - Reset State Machine */
                    CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                    UPDATE_STATS(RXFragmentError);
                    ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
                }
            }
            else
            {
                /* Wrong header - Reset State Machine */
                CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                UPDATE_STATS(RXFragmentError);
                ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:
            /* Waiting for release. Do nothing */
        case CAN_FRAGMENTATION_STATUS_START:
        case CAN_FRAGMENTATION_STATUS_LAST:
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Release a "long message" buffer
 *
 *   \param msgPtr Message to release.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingRecvFragmentRelease(CANFramingMsgStr *msgPtr)
{
    LU_UNUSED(msgPtr);

    if (CANFramingData.RXFragment.state == CAN_FRAGMENTATION_STATUS_END)
    {
        /* Reset the State machine to idle state */
        CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;

        return SB_ERROR_NONE;
    }
    else
    {
        /* Wrong state */
        UPDATE_STATS(RXFragmentError);
        return SB_ERROR_CANF_RX_FRAGMENT_ERROR;
    }
}


/*!
 ******************************************************************************
 *   \brief Initialize transmit message pool
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingInitTXMsgPool(void)
{
    CANFramingData.TXMsgQueue.readIdx  = 0;
    CANFramingData.TXMsgQueue.writeIdx = 0;

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Get a new message from the transmit message queue
 *
 *   \return Pointer to a new message. NULL if the queue is empty
 *
 ******************************************************************************
 */
CAN_MSG_Type *CANFramingGetTXMsg(void)
{
    CAN_MSG_Type *msgPtr;
    lu_uint32_t readIdx;

    /* Only this function updates the writeIndex. This function is called
     * only from one ISR. We don't need to disable interrupts
     */

    /* Enter Critical Region */
    //MG SU_CRITICAL_REGION_ENTER();

    /* Read current read position */
    readIdx = CANFramingData.TXMsgQueue.readIdx;

    if (readIdx == CANFramingData.TXMsgQueue.writeIdx)
    {
        /* No new messages.
         * Exit form critical region as soon as possible
         */
        //MG SU_CRITICAL_REGION_EXIT();
        msgPtr = NULL;
    }
    else
    {
        /* New messages available. Get a new message */
        readIdx = (readIdx + 1) & CAN_TX_BUF_MASK;
        /* Save the new read index */
        CANFramingData.TXMsgQueue.readIdx = readIdx;
        /* We can now exit from the critical region */
        //MG SU_CRITICAL_REGION_EXIT();

        msgPtr = &CANFramingData.TXMsgQueue.msgRingBuffer[readIdx];
    }

    return msgPtr;
}


/*!
 ******************************************************************************
 *   \brief Add a new message to the transmit message queue
 *
 *   \param  msgPtr Pointer to the message to add
 *
 *   \return error code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingAddMsg(CAN_MSG_Type *msgPtr)
{
    lu_uint32_t writeIndex;

    /* Enter Critical Region */
    SU_CRITICAL_REGION_ENTER();

    writeIndex = (CANFramingData.TXMsgQueue.writeIdx + 1) & CAN_TX_BUF_MASK;

    /* Enough space ? */
    if (writeIndex != CANFramingData.TXMsgQueue.readIdx)
    {
        /* Yes */
        CANFramingData.TXMsgQueue.writeIdx = writeIndex;

        CANFramingData.TXMsgQueue.msgRingBuffer[writeIndex] = *msgPtr;

        /* Exit from critical region as soon as possible */
        SU_CRITICAL_REGION_EXIT();

        return SB_ERROR_NONE;
    }
    else
    {
        SU_CRITICAL_REGION_EXIT();
        return SB_ERROR_CANC_QUEUE_FULL;
    }
}


/*!
 ******************************************************************************
 *   \brief CAN0 ISR
 *
 *   \return None
 *
 ******************************************************************************
 */
void CANFramingIntHandler(void)
{
    lu_uint32_t   intStatus;
    CAN_MSG_Type  CANMsg;
    msgPoolStr   *RXPoolMsgPtr;
    CANHeaderStr *CANHeaderPtr;
    CAN_MSG_Type *TXqueueMsg;
    LPC_CAN_TypeDef *canBase;

    canBase = CAN_BASE(CANFramingData.pinMapPtr->CanBase);

    /* Get interrupt status
     * Note that: Interrupt register CANICR will be reset after read.
     * So function "CAN_IntGetStatus" should be call only one time
     */
    intStatus = CAN_IntGetStatus(CAN_BASE(CANFramingData.pinMapPtr->CanBase));

    /* Check interrupt */
    if( intStatus &  CAN_ICR_RI)
    {
        /* Receive interrupt */
        CAN_ReceiveMsg(CAN_BASE(CANFramingData.pinMapPtr->CanBase), &CANMsg);

        /* The message is for us - Decode Header*/
        CANHeaderPtr = (CANHeaderStr*)(&CANMsg.id);

        if (CANHeaderPtr->fragment == LU_FALSE)
        {
            /* Get a pool message */
            RXPoolMsgPtr = &CANFramingData.RXMsgPool[CANFramingData.rxIdx];
            if (RXPoolMsgPtr->free == LU_TRUE)
            {
                /* fill CAN pool message */
                initHeaderCANFMsg(&RXPoolMsgPtr->msg, CANHeaderPtr);
                RXPoolMsgPtr->msg.msgLen = CANMsg.len;
                (*(lu_uint32_t*)(&RXPoolMsgPtr->msg.msgBufPtr[0])) = (*(lu_uint32_t*)(&CANMsg.dataA[0]));
                (*(lu_uint32_t*)(&RXPoolMsgPtr->msg.msgBufPtr[4])) = (*(lu_uint32_t*)(&CANMsg.dataB[0]));

                /* Use message pool slot */
                RXPoolMsgPtr->free = LU_FALSE;

                /* Forward message */
                if (CANFramingData.callBack(&RXPoolMsgPtr->msg) == SB_ERROR_CANC_QUEUE_FULL)
               	{
                	/* Release message pool */
                	RXPoolMsgPtr->free = LU_TRUE;

                	/* Lost a message */
                	UPDATE_STATS(RXMessageLost);
               	}
                else
                {
					/* Update Receive index */
					CAN_INCREMENT_AND_WRAP(CANFramingData.rxIdx, CAN_RX_BUF_MASK);

                	/* Update stats */
                	UPDATE_STATS(RXMessageTotal);
                }
            }
            else
            {
                /* Lost a message */
                UPDATE_STATS(RXMessageLost);
            }
        }
        else
        {
            /* Handle fragmented message */
            CANFramingRecvFragment(&CANMsg);
            /* Update stats */
            UPDATE_STATS(RXMessageTotal);
        }
    }
    else if (intStatus &  CAN_ICR_TI1)
    {
        /* Short messages slot */
        TXqueueMsg = CANFramingGetTXMsg();
        if(TXqueueMsg != NULL)
        {
            CAN_SendMsgSlot( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
                             TXqueueMsg,
                             CAN_SHORT_MSG_SLOT
                           );
            UPDATE_STATS(TXTotal);
        }
    }
    else if (intStatus &  CAN_ICR_TI2)
    {
        /* Fragmented messages slot */
        CANFramingSendFragmentInt(2);

    }
    else if (intStatus &  CAN_ICR_TI3)
    {
        /* Unused */
        UPDATE_STATS(canError);//MG
    }
    else
    {
        //MG Manage error
        UPDATE_STATS(canError);

        if (canBase->ICR & CAN_ICR_BEI)
        {
        	/* Bus Error */
        	UPDATE_STATS(canBusError);
        }

        if (canBase->ICR & CAN_ICR_ALI)
		{
			/* Arbitration error */
        	UPDATE_STATS(canArbitrationError);
		}

        if (canBase->ICR & CAN_ICR_DOI)
		{
			/* Data overrun */
        	UPDATE_STATS(canDataOverrun);
		}

        if (canBase->ICR & CAN_ICR_EPI)
		{
			/* Error Passive */
        	UPDATE_STATS(canErrorPassive);
		}

        if (canBase->GSR & CAN_GSR_BS)
        {
        	/* Bus Off */
        	UPDATE_STATS(canBusOff);
        }

		/* Turn off power and clock for CAN1 */
//		CLKPWR_ConfigPPWR(CLKPWR_PCONP_PCAN1, DISABLE);

//		CLKPWR_ConfigPPWR(CLKPWR_PCONP_PCAN1, ENABLE);

//		CAN_ModeConfig( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
//				        CAN_RESET_MODE,
//						ENABLE
//					  );

//		CAN_SetGsr (CAN_BASE(CANFramingData.pinMapPtr->CanBase), 0);

//       CAN_ModeConfig( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
//            		   CAN_OPERATING_MODE,
//              		   ENABLE
//               		 );

       // start timer
       // Disable error interrupts

        CAN_ModeConfig( CAN_BASE(CANFramingData.pinMapPtr->CanBase),
                        CAN_RESET_MODE,
                      	ENABLE
                      );

        CANFramingData.busErrorTimerMs = 50;

       	CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_EIE, DISABLE); //CAN Error Warning
		CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_EPIE, DISABLE); //CAN Error Passive
		CAN_IRQCmd(CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANINT_BEIE, DISABLE); //CAN Bus Error


        CANFramingData.stats.icr = intStatus;
        		//CAN_GetCTRLStatus (CAN_BASE(CANFramingData.pinMapPtr->CanBase), CANCTRL_INT_CAP);

        CANFramingData.stats.gsr = canBase->GSR;

        CANFramingData.stats.mod = canBase->MOD;

        CANFramingData.stats.btr = canBase->BTR;

        CANFramingData.stats.ier = canBase->IER;

        /* Set CAN Error LED */
        GPIO_SetValue(CAN_LED_RED_PORT, CAN_LED_RED_PIN);

    }
}


/*
 *********************** End of file ******************************************
 */
