/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CAN_FRAMING_INCLUDED
#define _CAN_FRAMING_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "systemUtils.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define CAN_TICK_MS			10

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	CAN_BUS_STATE_NORMAL = 0,
	CAN_BUS_STATE_BUS_OFF,
	CAN_BUS_STATE_RECOVERY,
	CAN_BUS_STATE_ERROR_PASSIVE,
	CAN_BUS_STATE_ARBITRATION_LOST,
	CAN_BUS_STATE_DATA_OVER_RUN
}CAN_BUS_STATE;


/*! Filter table structure */
typedef struct filterTableDef
{
    lu_uint8_t messageType;
    lu_uint8_t messageID  ;
    lu_bool_t  broadcast  ;
    lu_bool_t  fragmented ;
}filterTableStr;

/*!
 * Pin Map for CAN Controller
 */
typedef struct CANFramingMapDef
{
    lu_uint32_t peripheralCAN;
    lu_uint32_t peripheralGPIO;
    lu_uint32_t gpioPortBase;
    lu_uint32_t CanTxPin;
    lu_uint32_t CanTxConfigurePin;
    lu_uint32_t CanRxPin;
    lu_uint32_t CanRxConfigurePin;
    lu_uint32_t CanBase;
    lu_uint32_t CanInt;
}CANFramingMapStr;

/*!
 * Structure used to define a CAN message
 */
typedef struct CANFramingMsgDef
{
    /* Buffer ID. Used by the CAN Framing library */
    lu_uint8_t  ID         ;
    /* Message type */
    lu_uint8_t  messageType;
    /* Message ID */
    lu_uint8_t  messageID  ;
    /* Source address */
    lu_uint8_t  deviceIDSrc;
    /* Source device type */
    lu_uint8_t  deviceSrc  ;
    /* Destination address */
    lu_uint8_t  deviceIDDst;
    /* Destination device type */
    lu_uint8_t  deviceDst  ;
    /* Message length */
    lu_uint32_t  msgLen    ;
    /* Message buffer */
    lu_uint8_t *msgBufPtr  ;
}CANFramingMsgStr;

/*!
 * CAN Framing callback signature
 */
typedef SB_ERROR (*CANFramingCB)(CANFramingMsgStr *msgPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern CAN_BUS_STATE CANBusStatus;

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

extern lu_uint32_t CANFramingGetRXMessageTotal(void);
extern lu_uint32_t CANFramingGetRXMessageLost(void);
extern lu_uint32_t CANFramingGetcanError(void);

extern lu_uint32_t CANFramingGetICR(void);
extern lu_uint32_t CANFramingGetIER(void);
extern lu_uint32_t CANFramingGetGSR(void);
extern lu_uint32_t CANFramingGetMOD(void);
extern lu_uint32_t CANFramingGetBTR(void);

extern lu_uint32_t CANFramingGetcanBusError(void);
extern lu_uint32_t CANFramingGetcanArbitrationError(void);
extern lu_uint32_t CANFramingGetcanDataOverrun(void);
extern lu_uint32_t CANFramingGetcanBusOff(void);
extern lu_uint32_t CANFramingGetcanErrorPassive(void);
extern lu_uint32_t CANFramingGetcanErrorBTR(void);

extern lu_uint32_t CANFramingGetTXTotal(void);
extern lu_uint32_t CANFramingGetRxLost(void);

/*!
 ******************************************************************************
 *   \brief Initialize CAN Framing modules
 *
 *   \param clock System clock
 *   \param bitRate CAN bit rate
 *   \param device Device type (It used for source field of the CAN header)
 *   \param deviceID Device address (It used for sourceID field of the
 *                   CAN header)
 *   \param pinMapPtr pointer to the global CAN controller pins map
 *   \param priority CAN IRQ priority
 *   \param callBack callback used to forward a received message. When a message
 *                   is not needed anymore use the CANFramingReleaseMsg to
 *                   release the buffer
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingInit( lu_uint32_t       clock    ,
                                lu_uint32_t       bitRate  ,
		                        lu_uint8_t        device   ,
		                        lu_uint8_t        deviceID ,
		                        CANFramingMapStr *pinMapPtr,
		                        SUIRQPriorityStr  priority ,
		                        CANFramingCB      callBack
		                      );
/*!
 ******************************************************************************
 *   \brief Initialize CAN Framing timer
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingTick(void);

/*!
 ******************************************************************************
 *   \brief Pulse Green CAN LED
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingPulseCANLed(void);

/*!
 ******************************************************************************
 *   \brief Add a set of ID to the Hardware Acceptance Filter
 *
 *   The IDs specified in the table will be accepted by the filter
 *
 *   \param filterTable Table with the IDs
 *   \param size Table size (number of entries). The "SU_TABLE_SIZE" macro
 *               can be used to generate the table size
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingAddFilter( const filterTableStr *filterTable,
                                     lu_uint32_t size
                                   );

/*!
 ******************************************************************************
 *   \brief Enable/Disable Hardware Acceptance Filter
 *
 *   By default the Hardware Acceptance Filter is enabled
 *
 *   \param enable If LU_TRUE the filter is enabled. If LU_FALSE the
 *                 filter is disabled
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingFilterEnable(lu_bool_t enable);

/*!
 ******************************************************************************
 *   \brief Release an unused RX Buffer
 *
 *   When the application level layer has finished using an RX buffer, the
 *   buffer should be released using this function
 *
 *   \param msgPtr buffer to release
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingReleaseMsg(CANFramingMsgStr *msgPtr);

/*!
 ******************************************************************************
 *   \brief Send a message over the can protocol
 *
 *   If necessary the message is fragmented. Only one fragmented message at a
 *   time can be sent.
 *
 *   \param msgPtr Message to send
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr);

extern void CANFramingEnterBusOff(void);

#endif /* _CAN_FRAMING_INCLUDED */

/*
 *********************** End of file ******************************************
 */
