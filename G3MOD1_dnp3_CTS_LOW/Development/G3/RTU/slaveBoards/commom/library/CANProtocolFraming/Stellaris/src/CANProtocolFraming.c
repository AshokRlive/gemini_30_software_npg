/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>
#include <string.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolFraming.h"
#include "CANProtocol.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_can.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/can.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/* CAN controller buffer layout */
#define CAN_RX_BUF_FIRST            (0)
#define CAN_RX_BUF_LEN              (4)
#define CAN_RX_BROADCAST_BUF_LEN    (4)
#define CAN_RX_BUF_LEN_TOTAL        (CAN_RX_BUF_LEN + CAN_RX_BROADCAST_BUF_LEN)
#define CAN_TX_BUF_FIRST            (8)
#define CAN_TX_BUF_LEN              (10)

#define CAN_MAX_PAYLOAD    (8)

/* Fragmentation Data */
#define CAN_FRAGMENT_HEADER_SIZE    (sizeof(CANFragmentHeaderStr))
#define CAN_FRAGMENT_PAYLOAD        (7)
#define CAN_FRAGMENT_ID_MASK        (0x3F) //MG 6 bits
#define CAN_FRAGMENT_ID             (0xFF)

//MG atomic??
#define CAN_FRAGMENT_NEXT_ID(a) ((a) = (((a)+ 1) & CAN_FRAGMENT_ID_MASK))

#define UPDATE_STATS(a) ((CANFramingData.stats.a)++)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
    CAN_FRAGMENTATION_STATUS_IDLE = 0,
    CAN_FRAGMENTATION_STATUS_START   ,
    CAN_FRAGMENTATION_STATUS_CONT    ,
    CAN_FRAGMENTATION_STATUS_END     ,

    CAN_FRAGMENTATION_STATUS_LAST
}CAN_FRAGMENTATION_STATUS;

/*!
 * CAN Framing statistics
 */
typedef struct CANFramingStatDef
{
    lu_uint32_t canError          ;

    /* TX */
    lu_uint32_t TXShortMessageReq ;
    lu_uint32_t TXLongMessageReq  ;
    lu_uint32_t TXLongMessage     ;
    lu_uint32_t TXTotal           ;
    lu_uint32_t TXFragmentBusy    ;
    lu_uint32_t TXFragmentError   ;
    lu_uint32_t TXTooLong         ;

    /* RX */
    lu_uint32_t RXMessageTotal    ;
    lu_uint32_t RXLongMessageTotal;
    lu_uint32_t RXMessageLost     ;
    lu_uint32_t RXFragmentError   ;
}CANFramingStatStr;

/*!
 * CAN Fragment header.
 * First packet: start = 1, stop = 0
 * Intermediate packets: start = 1, stop = 1
 * Last packet: start = 0, stop = 1
 * Fragment ID is incremented for each packet
 */
typedef struct CANFragmentHeaderDef
{
    lu_uint8_t id    : 6;
    lu_uint8_t start : 1;
    lu_uint8_t stop  : 1;
}CANFragmentHeaderStr;

/*!
 * TX Fragmentation State Machine Control Data
 */
typedef struct CANTXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Fragment ID counter */
    lu_uint8_t counter;

    /*! Bytes already sent */
    lu_uint32_t sentData;

    /*! ObjectID (1-32) of the last sent packet */
    lu_uint32_t objID;
    /* CAN message Object */
    tCANMsgObject MsgObject;

    /*! Total length of the buffer */
    lu_uint32_t bufferLen;
    /*! Local TX buffer */
    lu_uint8_t buffer[CAN_MESSAGE_LENGTH];
}CANTXFragmentationStr;

/*!
 * RX Fragmentation State Machine Control Data
 */
typedef struct CANRXFragmentationDef
{
    /*! FSM state */
    CAN_FRAGMENTATION_STATUS state;
    /*! Expected fragment ID */
    lu_uint8_t fragmentID;
    /*! received data */
    lu_uint32_t recvData;
    /*! Expected CAN header */
    lu_uint32_t expectedHeader;

    /*! G3 CAN message structure */
    CANFramingMsgStr  CANFramingMsg;
    /*! Local buffer */
    lu_uint8_t buffer[CAN_MESSAGE_LENGTH];
}CANRXFragmentationStr;

/*!
 * RX short message pool
 */
typedef struct msgPoolDef
{
    /*! Buffer status */
    lu_bool_t        free;
    /*! G3 CAN message structure */
    CANFramingMsgStr msg;
    /*! Local Buffer */
    lu_uint8_t       msgBuf[CAN_MAX_PAYLOAD];
}msgPoolStr;

/*!
 * Library control data
 */
typedef struct CANFramingDef
{
    /*! Library status */
    lu_bool_t   initialzied;
    /*! Device type */
    lu_uint8_t  device     ;
    /*! Device address */
    lu_uint8_t  deviceID   ;

    /*! Pins Map */
    CANFramingMapStr *pinMapPtr;

    /*! Receive buffers mask */
    lu_uint32_t RXBufMask   ;
    /*! Transmit buffers mask */
    lu_uint32_t TXBufMask   ;

    /*! Receive Message Pool */
    msgPoolStr RXMsgPool[CAN_RX_BUF_LEN_TOTAL];

    /*! Transmit buffer ID */
    lu_uint32_t txIdx;

    /*! Fragmentation Control data (TX) */
    CANTXFragmentationStr TXFragment;

    /*! Fragmentation Control data (RX) */
    CANRXFragmentationStr RXFragment;

    /*! Forward data callback */
    CANFramingCB callBack   ;

    /*! Statistics */
    CANFramingStatStr stats;
}CANFramingStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                          tCANMsgObject *MsgObjectTxPtr,
                          lu_bool_t fragment
                        );

void initHeaderCANMsg( CANFramingMsgStr *msgPtr,
                       CANHeaderStr *CANHeaderPtr
                     );

static SB_ERROR CANFramingReceiveFIFO( lu_uint8_t device  , lu_uint8_t deviceID,
                                       lu_uint8_t firstObj, lu_uint8_t objNum  ,
                                       lu_uint32_t *mask
                                     );
static lu_uint32_t CANFramingSendObj(tCANMsgObject *MsgObjectTxPtr);


static SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr);
static SB_ERROR CANFramingSendFragmentInt(lu_uint32_t objID);
static SB_ERROR CANFramingFillFragment(lu_uint8_t *bufferPtr);
static SB_ERROR CANFramingRecvFragment(tCANMsgObject *CANObjPtr);
static SB_ERROR CANFramingRecvFragmentRelease(CANFramingMsgStr *msgPtr);

void CANFramingIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static CANFramingStr CANFramingData;



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR CANFramingInit( lu_uint32_t       clock    ,
                         lu_uint32_t       bitRate  ,
		                 lu_uint8_t        device   ,
		                 lu_uint8_t        deviceID ,
		                 CANFramingMapStr *pinMapPtr,
		                 CANFramingCB      callBack
		               )
{
    lu_uint32_t i;
    msgPoolStr *msgPoolPtr;

    /* Parameter check */
    if ( (pinMapPtr == NULL) ||
         (callBack  == NULL)
       )
    {
        return SB_ERROR_PARAM;
    }

    if (CANFramingData.initialzied == LU_TRUE)
    {
        /* Module already initialized */
        return SB_ERROR_INITIALIZED;
    }

    /* Module initialized */
    CANFramingData.initialzied = LU_TRUE;

    /* Save callback */
    CANFramingData.callBack = callBack;

    /* Save PIN map */
    CANFramingData.pinMapPtr = pinMapPtr;

    /* Save source ID */
    CANFramingData.device   = device  ;
    CANFramingData.deviceID = deviceID;

    /* Configure CAN 0 Pins. */
    SysCtlPeripheralEnable(pinMapPtr->peripheralGPIO);
    GPIOPinTypeCAN(pinMapPtr->gpioPortBase, pinMapPtr->CanRxPin | pinMapPtr->CanTxPin);
    if (pinMapPtr->CanRxConfigurePin != GPIO_INVALID_PIN)
    {
        GPIOPinConfigure(pinMapPtr->CanRxConfigurePin);
    }
    if (pinMapPtr->CanTxConfigurePin != GPIO_INVALID_PIN)
    {
        GPIOPinConfigure(pinMapPtr->CanTxConfigurePin);
    }

    /* Enable the CAN controller. */
    SysCtlPeripheralEnable(pinMapPtr->peripheralCAN);

    /* Reset the state of all the message object and the state of the CAN
     * module to a known state.
     */
    CANInit(pinMapPtr->CanBase);

    /* Configure the bit rate for the CAN device */
    CANBitRateSet(pinMapPtr->CanBase, clock, bitRate);

    /* Initialize RX messages FIFO */
    CANFramingData.RXBufMask = 0;
    CANFramingReceiveFIFO( device                   ,
                           deviceID                 ,
                           CAN_RX_BUF_FIRST         ,
                           CAN_RX_BUF_LEN           ,
                           &CANFramingData.RXBufMask
                         );
    /* Initialize Broadcast RX messages FIFO */
    CANFramingReceiveFIFO( CAN_BOARD_BRD                    ,
                           CAN_BOARD_ID_BRD                 ,
                           CAN_RX_BUF_FIRST + CAN_RX_BUF_LEN,
                           CAN_RX_BROADCAST_BUF_LEN         ,
                           &CANFramingData.RXBufMask
                         );

    /* Initialize RX message pool */
    for(i = 0; i < CAN_RX_BUF_LEN_TOTAL; i++)
    {
        msgPoolPtr = &CANFramingData.RXMsgPool[i];
        msgPoolPtr->free = LU_TRUE;
        msgPoolPtr->msg.ID = i;
        msgPoolPtr->msg.msgBufPtr = &msgPoolPtr->msgBuf[0];
    }

    /* Initialize Tx */
    CANFramingData.txIdx = 0;

    /* Initialize TX Mask */
    CANFramingData.TXBufMask = 0;
    for( i = CAN_TX_BUF_FIRST;
         i < CAN_TX_BUF_FIRST + CAN_TX_BUF_LEN;
         i++
       )
    {
        CANFramingData.TXBufMask |= (1 << i);
    }

    /* Initialize RX Fragmentation control data */
    CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;

    /* Take CAN0 device out of INIT state */
    CANEnable(pinMapPtr->CanBase);

    /* enable interrupts for CAN controller */
    CANIntEnable(pinMapPtr->CanBase, CAN_INT_MASTER | CAN_INT_ERROR);

    /* Enable interrupts for the CAN in the NVIC */
    IntEnable(pinMapPtr->CanInt);

    return SB_ERROR_NONE;
}


SB_ERROR CANFramingReleaseMsg(CANFramingMsgStr *msgPtr)
{
    SB_ERROR ret = SB_ERROR_NONE;

    if (msgPtr == NULL)
    {
        return SB_ERROR_PARAM;
    }

    if (msgPtr->ID != CAN_FRAGMENT_ID)
    {
        CANFramingData.RXMsgPool[msgPtr->ID].free = LU_TRUE;
    }
    else
    {
        /* Release fragmented message */
        ret = CANFramingRecvFragmentRelease(msgPtr);
    }

    return ret;
}

SB_ERROR CANFramingSendMsg(CANFramingMsgStr *msgPtr)
{
    tCANMsgObject MsgObjectTx;
    lu_uint8_t msgLen;

    if (msgPtr == NULL)
    {
        return SB_ERROR_PARAM;
    }

    msgLen = msgPtr->msgLen;
    if (msgLen > CAN_MESSAGE_LENGTH)
    {
        UPDATE_STATS(TXTooLong);
        return SB_ERROR_CANF_MSG_TOO_LONG;
    }

    if (msgLen <= CAN_MAX_PAYLOAD)
    {
        /* Short message. Send immediately */
        initHeaderMsgObject(msgPtr, &MsgObjectTx, LU_FALSE);

        MsgObjectTx.ulFlags = MSG_OBJ_TX_INT_ENABLE | MSG_OBJ_EXTENDED_ID;
        MsgObjectTx.ulMsgIDMask = 0;
        MsgObjectTx.pucMsgData = &msgPtr->msgBufPtr[0];
        MsgObjectTx.ulMsgLen = msgPtr->msgLen;

        CANFramingSendObj(&MsgObjectTx);

        UPDATE_STATS(TXShortMessageReq);

        return SB_ERROR_NONE;
    }
    else
    {
        /* Long message. Start fragmentation */
        UPDATE_STATS(TXLongMessageReq);
        return CANFramingSendFragment(msgPtr);
    }

    return SB_ERROR_CANF;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize CAN object using a G3 CAN Message
 *
 *   \param msgPtr G3 CAN Message used to initialize the CAN object
 *   \param MsgObjectTxPtr CAN object to initialize
 *   \param fragment set to LU_TRUE if this is a fragmented message
 *
 *   \return None
 *
 ******************************************************************************
 */
inline void initHeaderMsgObject( CANFramingMsgStr *msgPtr,
                                 tCANMsgObject *MsgObjectTxPtr,
                                 lu_bool_t fragment
                               )
{
    CANHeaderStr *CANHeaderPtr;

    MsgObjectTxPtr->ulMsgID = 0;
    CANHeaderPtr = (CANHeaderStr*)(&MsgObjectTxPtr->ulMsgID);
    CANHeaderPtr->deviceSrc   = msgPtr->deviceSrc;
    CANHeaderPtr->deviceIDSrc = msgPtr->deviceIDSrc;
    CANHeaderPtr->deviceDst   = msgPtr->deviceDst;
    CANHeaderPtr->deviceIDDst = msgPtr->deviceIDDst;
    CANHeaderPtr->messageType = msgPtr->messageType;
    CANHeaderPtr->messageID   = msgPtr->messageID;
    if(fragment == LU_TRUE)
    {
        CANHeaderPtr->fragment = 1;
    }
}

/*!
 ******************************************************************************
 *   \brief Initialize G3 CAN Message using a CAN object
 *
 *   \param msgPtr CAN object used to initialize the G3 CAN Message
 *   \param MsgObjectTxPtr G3 CAN Message to initialize
 *
 *   \return None
 *
 ******************************************************************************
 */
inline void initHeaderCANMsg( CANFramingMsgStr *msgPtr,
                              CANHeaderStr *CANHeaderPtr
                            )
{
    msgPtr->messageType = CANHeaderPtr->messageType;
    msgPtr->messageID   = CANHeaderPtr->messageID;
    msgPtr->deviceDst   = CANHeaderPtr->deviceDst;
    msgPtr->deviceIDDst = CANHeaderPtr->deviceIDDst;
    msgPtr->deviceSrc   = CANHeaderPtr->deviceSrc;
    msgPtr->deviceIDSrc = CANHeaderPtr->deviceIDSrc;
}

/*!
 ******************************************************************************
 *   \brief Initialize Receive FIFO
 *
 *   This function initialize both the internal data structure and the
 *   uprocessor controller
 *
 *   \param device Device type used to create the receive filter
 *   \param deviceID Device address used to create the receive filter
 *   \param firstObj First object to be used in the uprocessor
 *                   CAN controller (0-31)
 *   \param objNum Number of consecutive object to initialize
 *   \param mask update the receive FIFO mask
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingReceiveFIFO( lu_uint8_t device  , lu_uint8_t deviceID,
                                lu_uint8_t firstObj, lu_uint8_t objNum  ,
                                lu_uint32_t *mask
                              )
{
    tCANMsgObject msgObjectRx;
    CANHeaderStr *CANHeaderPtr;
    lu_uint32_t i;
    lu_uint8_t lastObj;

    /* Set the ID to receive anything sent to us */
    msgObjectRx.ulMsgID = 0;
    CANHeaderPtr = (CANHeaderStr*)(&msgObjectRx.ulMsgID);
    CANHeaderPtr->deviceDst   = device;
    CANHeaderPtr->deviceIDDst = deviceID;

    /* Mask everything apart the destination */
    msgObjectRx.ulMsgIDMask = 0;
    CANHeaderPtr = (CANHeaderStr*)(&msgObjectRx.ulMsgIDMask);
    CANHeaderPtr->deviceDst   = ((lu_uint8_t)(0x1F));
    CANHeaderPtr->deviceIDDst = ((lu_uint8_t)(0x07));

    /* Set flags */
    msgObjectRx.ulFlags = MSG_OBJ_RX_INT_ENABLE |
                          MSG_OBJ_EXTENDED_ID   |
                          MSG_OBJ_USE_EXT_FILTER|
                          MSG_OBJ_FIFO;

    /* Set maximum length  */
    msgObjectRx.ulMsgLen = CAN_MAX_PAYLOAD;

    /* Initialize CAN objects */
    lastObj = firstObj + objNum - 1;
    for(i = firstObj; i < lastObj; i++)
    {
        /* Set CAN Message */
        CANMessageSet( CANFramingData.pinMapPtr->CanBase,
                       i + 1                            ,
                       &msgObjectRx                     ,
                       MSG_OBJ_TYPE_RX
                     );

        /* Set Mask */
        *mask |= (1 << i);
    }

    msgObjectRx.ulFlags &= (~MSG_OBJ_FIFO);
    CANMessageSet( CANFramingData.pinMapPtr->CanBase,
                   i + 1                            ,
                   &msgObjectRx                     ,
                   MSG_OBJ_TYPE_RX
                 );
    /* Set Mask */
    *mask |= (1 << i);

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Send a CAN object
 *
 *   The ID of the CAN object to use is autoamtically incremented
 *
 *   \param MsgObjectTxPtr Object to send
 *
 *   \return CAN Object used for the transmission (1-32). 0 in case of error
 *
 ******************************************************************************
 */
lu_uint32_t CANFramingSendObj(tCANMsgObject *MsgObjectTxPtr)
{
    lu_uint32_t txIdx = 0;

    /* Save current Id and increment */
    txIdx = CANFramingData.txIdx + CAN_TX_BUF_FIRST + 1;
    CANFramingData.txIdx = (CANFramingData.txIdx + 1) %  CAN_TX_BUF_LEN;

    CANMessageSet( CANFramingData.pinMapPtr->CanBase,
                   txIdx                            ,
                   MsgObjectTxPtr                   ,
                   MSG_OBJ_TYPE_TX
                 );

    return txIdx;
}

/*!
 ******************************************************************************
 *   \brief Start the transmission of a "long" message
 *
 *   Only one long message at aa time can be transmitted. The first fragment
 *   is sent immediately. The successive fragments are automatically sent
 *   in the ISR when the transmission of the previous message is terminated.
 *
 *   \param msgPtr Object to send
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragment(CANFramingMsgStr *msgPtr)
{
    lu_uint8_t tmpBuffer[CAN_MAX_PAYLOAD];
    SB_ERROR ret = SB_ERROR_NONE;

    switch(CANFramingData.TXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:

            /* Initialize controll data */
            CANFramingData.TXFragment.objID = 0;
            CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_START;
            CANFramingData.TXFragment.sentData = 0;

            /* Save message buffer */
            CANFramingData.TXFragment.bufferLen = msgPtr->msgLen;
            memcpy( CANFramingData.TXFragment.buffer,
                    msgPtr->msgBufPtr,
                    msgPtr->msgLen
                  );

            /* Initialize common field of CAN MsgObj */
            initHeaderMsgObject(msgPtr, &CANFramingData.TXFragment.MsgObject, LU_TRUE);
            CANFramingData.TXFragment.MsgObject.ulFlags =
                                    MSG_OBJ_TX_INT_ENABLE | MSG_OBJ_EXTENDED_ID;
            CANFramingData.TXFragment.MsgObject.ulMsgIDMask = 0;

            /* Initialize payload */
            ret = CANFramingFillFragment(&tmpBuffer[0]);

            if (ret == SB_ERROR_NONE)
            {
                /* Update state */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_CONT;

                /* Send message */
                CANFramingData.TXFragment.objID =
                        CANFramingSendObj(&CANFramingData.TXFragment.MsgObject);
            }
        break;

        default:
            UPDATE_STATS(TXFragmentError);
            ret = SB_ERROR_BUSY;
        break;
    }


    return ret;
}

/*!
 ******************************************************************************
 *   \brief Continue the transmission of the "long" message
 *
 *   This function should be called directly from the ISR. A new fragment
 *   is sent only when a previous fragment has been successfully sent. The
 *   objID is used to identify the transmitted object
 *
 *   \param objID Id of the object that has just been transmitted (1-32)
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingSendFragmentInt(lu_uint32_t objID)
{
    lu_uint8_t tmpBuffer[CAN_MAX_PAYLOAD];
    SB_ERROR ret = SB_ERROR_NONE;

    switch(CANFramingData.TXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:
        case CAN_FRAGMENTATION_STATUS_START:
            /* Do nothing */
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            if (objID == CANFramingData.TXFragment.objID)
            {
                /* this interrupt is for us
                 * Initialize payload
                 */
                CANFramingFillFragment(&tmpBuffer[0]);

                /* Send message */
                CANFramingData.TXFragment.objID =
                        CANFramingSendObj(&CANFramingData.TXFragment.MsgObject);
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:
            /* Finished. Reset state machine */
            UPDATE_STATS(TXLongMessage);
            CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
        break;

        default:
            /* Do nothing */
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Prepare a new fragment to send
 *
 *   The buffer is filled with new data. If necessary the State Machine
 *   status is also updated
 *
 *   \param bufferPtr Buffer to fill with new data.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingFillFragment(lu_uint8_t *bufferPtr)
{
    lu_uint32_t bytesLeft;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    SB_ERROR ret = SB_ERROR_NONE;

    CANFragmentHeaderPtr = (CANFragmentHeaderStr *)bufferPtr;

    /* Check left bytes and update state */
    bytesLeft = CANFramingData.TXFragment.bufferLen -
                CANFramingData.TXFragment.sentData   ;

    /* Set Fragment ID */
    CANFragmentHeaderPtr->id = CANFramingData.TXFragment.counter;

    /* update Counter */
    CAN_FRAGMENT_NEXT_ID(CANFramingData.TXFragment.counter);

    /* Set start/stop flags */
    switch(CANFramingData.TXFragment.state) //MG Reset State machine after a timeout
    {
        case CAN_FRAGMENTATION_STATUS_START:
            /* Sanity check */
            if (bytesLeft > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 0;
            }
            else
            {
                /* Error */
                UPDATE_STATS(TXFragmentError);
                ret = SB_ERROR_CANF_TX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            if (bytesLeft > CAN_FRAGMENT_PAYLOAD)
            {
                CANFragmentHeaderPtr->start = 1;
                CANFragmentHeaderPtr->stop  = 1;
            }
            else
            {
                /* Last fragment */
                CANFragmentHeaderPtr->start = 0;
                CANFragmentHeaderPtr->stop  = 1;

                /* change state */
                CANFramingData.TXFragment.state = CAN_FRAGMENTATION_STATUS_END;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:

        break;

        default:
            UPDATE_STATS(TXFragmentError);
            ret = SB_ERROR_CANF_TX_FRAGMENT_ERROR;
        break;
    }

    if (ret == SB_ERROR_NONE)
    {
        /* Copy data to send into the buffer */
        bytesLeft = LU_MIN(bytesLeft, CAN_FRAGMENT_PAYLOAD);
        memcpy( &bufferPtr[1],
                &CANFramingData.TXFragment.buffer[CANFramingData.TXFragment.sentData],
                bytesLeft
              );

        /* Update sent bytes */
        CANFramingData.TXFragment.sentData += bytesLeft;

        /* Set payload pointer and length */
        CANFramingData.TXFragment.MsgObject.pucMsgData = bufferPtr;
        CANFramingData.TXFragment.MsgObject.ulMsgLen = bytesLeft + CAN_FRAGMENT_HEADER_SIZE;//MG
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Manage the reception of a fragment
 *
 *   Only one "long" message at a time can be received. When a full message is
 *   correctly received the message is forwarded. The "long" message buffer
 *   should be released using the CANFramingReleaseMsg function.
 *   This function should be called with the ISR.
 *
 *   \param CANObjPtr Message received.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingRecvFragment(tCANMsgObject *CANObjPtr)
{
    lu_uint32_t msgLen;
    CANHeaderStr *CANHeaderPtr;
    CANFragmentHeaderStr *CANFragmentHeaderPtr;
    SB_ERROR ret = SB_ERROR_NONE;

    /* Decode fragmentation header */
    CANFragmentHeaderPtr = (CANFragmentHeaderStr*)(&CANObjPtr->pucMsgData[0]);

    /* Save message length */
    msgLen = CANObjPtr->ulMsgLen - CAN_FRAGMENT_HEADER_SIZE;

    switch(CANFramingData.RXFragment.state)
    {
        case CAN_FRAGMENTATION_STATUS_IDLE:
            /* Check start/stop bits */
            if ( (CANFragmentHeaderPtr->start == 1) &&
                 (CANFragmentHeaderPtr->stop  == 0)
               )
            {
                /* Valid first fragment
                 * Initialize control data
                 */
                CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_CONT;
                CANFramingData.RXFragment.fragmentID = CANFragmentHeaderPtr->id;
                CANFramingData.RXFragment.expectedHeader = CANObjPtr->ulMsgID;

                /* Set the next expect fragment ID */
                CAN_FRAGMENT_NEXT_ID(CANFramingData.RXFragment.fragmentID);

                /* Save buffer */
                memcpy( CANFramingData.RXFragment.buffer,
                        &CANObjPtr->pucMsgData[1],
                        msgLen
                      );
                CANFramingData.RXFragment.recvData = msgLen;
            }
            else
            {
                /* Wrong header */
                UPDATE_STATS(RXFragmentError);
                ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_CONT:
            /* Check start/stop bits */
            if (CANFragmentHeaderPtr->stop  == 1)
            {
                /* Valid header (continuation packet) */

                /* Check msg header */
                if ( (CANFramingData.RXFragment.expectedHeader == CANObjPtr->ulMsgID) &&
                     (CANFramingData.RXFragment.fragmentID == CANFragmentHeaderPtr->id) &&
                     (CANFramingData.RXFragment.recvData + msgLen <= CAN_MESSAGE_LENGTH )
                   )
                {
                    /* Fully validated header. Save buffer */
                    memcpy( &CANFramingData.RXFragment.buffer[CANFramingData.RXFragment.recvData],
                            &CANObjPtr->pucMsgData[1],
                            msgLen
                          );
                    CANFramingData.RXFragment.recvData += msgLen;

                    /* Last packet ? */
                    if(CANFragmentHeaderPtr->start == 0)
                    {
                        /* Yes */
                        CANHeaderPtr = (CANHeaderStr*)(&CANFramingData.RXFragment.expectedHeader);
                        initHeaderCANMsg(&CANFramingData.RXFragment.CANFramingMsg, CANHeaderPtr);

                        /* Prepare data */
                        CANFramingData.RXFragment.CANFramingMsg.ID = CAN_FRAGMENT_ID;
                        CANFramingData.RXFragment.CANFramingMsg.msgLen = CANFramingData.RXFragment.recvData;
                        CANFramingData.RXFragment.CANFramingMsg.msgBufPtr = CANFramingData.RXFragment.buffer;

                        /* Update state */
                        CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_END;

                        UPDATE_STATS(RXLongMessageTotal);

                        /* forward data */
                        CANFramingData.callBack(&CANFramingData.RXFragment.CANFramingMsg);
                    }
                    else
                    {
                        /* Set the next expect fragment ID */
                        CAN_FRAGMENT_NEXT_ID(CANFramingData.RXFragment.fragmentID);
                    }
                }
                else
                {
                    /* Wrong header - Reset State Machine */
                    CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                    UPDATE_STATS(RXFragmentError);
                    ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
                }
            }
            else
            {
                /* Wrong header - Reset State Machine */
                CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;
                UPDATE_STATS(RXFragmentError);
                ret = SB_ERROR_CANF_RX_FRAGMENT_ERROR;
            }
        break;

        case CAN_FRAGMENTATION_STATUS_END:
            /* Waiting for release. Do nothing */
        case CAN_FRAGMENTATION_STATUS_START:
        case CAN_FRAGMENTATION_STATUS_LAST:
        break;
    }

    return ret;
}

/*!
 ******************************************************************************
 *   \brief Release a "long message" buffer
 *
 *   \param msgPtr Message to release.
 *
 *   \return Error Code
 *
 ******************************************************************************
 */
SB_ERROR CANFramingRecvFragmentRelease(CANFramingMsgStr *msgPtr)
{
    if (CANFramingData.RXFragment.state == CAN_FRAGMENTATION_STATUS_END)
    {
        /* Reset the State machine to idle state */
        CANFramingData.RXFragment.state = CAN_FRAGMENTATION_STATUS_IDLE;

        return SB_ERROR_NONE;
    }
    else
    {
        /* Wrong state */
        UPDATE_STATS(RXFragmentError);
        return SB_ERROR_CANF_RX_FRAGMENT_ERROR;
    }
}

/*!
 ******************************************************************************
 *   \brief CAN0 ISR
 *
 *   \return None
 *
 ******************************************************************************
 */
void CANFramingIntHandler(void)
{
    lu_uint32_t       ulStatus;
    lu_uint32_t       i;
    tCANMsgObject    CANMsgObject;
    CANHeaderStr     *CANHeaderPtr;
    msgPoolStr       *RXPoolMsgPtr;
    lu_uint8_t       tmpBuffer[CAN_MAX_PAYLOAD];

    /* Find the cause of the interrupt */
    ulStatus = CANIntStatus( CANFramingData.pinMapPtr->CanBase,
                             CAN_INT_STS_CAUSE
                           );

    if (ulStatus == CAN_INT_INTID_STATUS)
    {
        UPDATE_STATS(canError);
        /* Error interrupt: read controller status registers */
        ulStatus = CANStatusGet( CANFramingData.pinMapPtr->CanBase,
                                 CAN_STS_CONTROL
                               );

        /* MG - TODO - Check the error */
        CANIntClear(CANFramingData.pinMapPtr->CanBase, ulStatus);
    }
    else
    {
        /* Get objects with pending interrupts */
        ulStatus = CANIntStatus(CANFramingData.pinMapPtr->CanBase, CAN_INT_STS_OBJECT);

        /* Manage RX Objects */
        if(ulStatus & CANFramingData.RXBufMask)
        {
            for( i = CAN_RX_BUF_FIRST;
                 i < CAN_RX_BUF_FIRST + CAN_RX_BUF_LEN_TOTAL;
                 i++
               )
            {
                if (ulStatus & (1 << i))
                {
                    /* Pending interrupt */
                    RXPoolMsgPtr = &CANFramingData.RXMsgPool[i-CAN_RX_BUF_FIRST];

                    if (RXPoolMsgPtr->free == LU_TRUE)
                    {
                        RXPoolMsgPtr->free = LU_FALSE;

                        UPDATE_STATS(RXMessageTotal);

                        CANMsgObject.pucMsgData = &RXPoolMsgPtr->msgBuf[0];
                        CANMessageGet( CANFramingData.pinMapPtr->CanBase,
                                       i+1                              ,
                                       &CANMsgObject                    ,
                                       1
                                     );

                        /* Decode Header*/
                        CANHeaderPtr = (CANHeaderStr*)(&CANMsgObject.ulMsgID);

                        if (CANHeaderPtr->fragment == LU_FALSE)
                        {

                            initHeaderCANMsg(&RXPoolMsgPtr->msg, CANHeaderPtr);
                            RXPoolMsgPtr->msg.msgLen = CANMsgObject.ulMsgLen;

                            /* Forward message */
                            CANFramingData.callBack(&RXPoolMsgPtr->msg);
                        }
                        else
                        {
                            /* Handle fragmented message */
                            CANFramingRecvFragment(&CANMsgObject);
                            /* Release RX Buffer */
                            RXPoolMsgPtr->free = LU_TRUE;
                        }
                    }
                    else
                    {
                        /* Handle error. Discard message */
                        UPDATE_STATS(RXMessageLost);
                        CANMsgObject.pucMsgData = &tmpBuffer[0];
                        CANMessageGet( CANFramingData.pinMapPtr->CanBase,
                                       i+1                              ,
                                       &CANMsgObject                    ,
                                       1
                                     );
                    }
                }
            }
        }

        /* Manage TX Objects */
        if(ulStatus & CANFramingData.TXBufMask)
        {
            for( i = CAN_TX_BUF_FIRST;
                 i < CAN_TX_BUF_FIRST + CAN_TX_BUF_LEN;
                 i++
               )
            {
                if (ulStatus & (1 << i))
                {
                    /* Pending interrupt.
                     * Send the data to the Fragment FSM
                     * and clear the interrupt
                     */
                    CANFramingSendFragmentInt(i+1);
                    CANIntClear(CAN0_BASE, i+1);
                    UPDATE_STATS(TXTotal);
                }
            }
        }
    }
}


/*
 *********************** End of file ******************************************
 */
