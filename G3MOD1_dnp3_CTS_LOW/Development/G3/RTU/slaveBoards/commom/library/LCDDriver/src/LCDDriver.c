/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       LCD Driver (Hitachi type controller - HD44780U) module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/11/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "string.h"

#include "IOManager.h"
#include "BoardIOMap.h"

#include "systemTime.h"
#include "LCDDriver.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define LCD_INIT_RESET_DELAY_MS			    50
#define LCD_INIT_CMD_RESET_DELAY_MS			6

#define LCD_COMMAND_DELAY_MS				2
#define LCD_COMMAND_LONG_DELAY_MS			4

/* Instruction - Read Busy Flag & Address */
#define HD44780U_BUSY_FLAG					0x80
#define HD44780U_ADDRESS_COUNT_MASK			0x7f

/* Instruction - Clear Display */
#define HD44780U_INST_CLEAR_DISPLAY			0x01

/* Instruction - Return Home (Cursor) */
#define HD44780U_INST_RETURN_HOME			0x02

/* Instruction - Entry Mode */
#define HD44780U_INST_ENTRY_MODE_SET		0x04
#define HD44780U_ENTRY_MODE_INCREMENT		0x02
#define HD44780U_ENTRY_MODE_SHIFT_LEFT		0x01

/* Instruction - Display On/Off Control */
#define HD44780U_INST_DISPLAY_ONOFF			0x08
#define HD44780U_DISPLAY_ON					0x04
#define HD44780U_DISPLAY_CURSOR_ON			0x02
#define HD44780U_DISPLAY_CURSOR_FLASH		0x01

/* Instruction - Cursor or Display Shift */
#define HD44780U_INST_CURSOR_DISPLAY_SHIFT	0x10
#define HD44780U_DISPLAY_SHIFT				0x08
#define HD44780U_TEXT_SHIFT2RIGHT			0x04

/* Instruction - Function Set */
#define HD44780U_INST_FUNCTION_SET			0x20
#define HD44780U_8_BIT_MODE					0x10
#define HD44780U_2_LINES					0x08
#define HD44780U_5X10_FONT					0x04

/* Instruction - Set CG RAM Address */
#define HD44780U_INST_SET_CG_RAM_ADDRESS	0x40
#define HD44780U_CG_RAM_ADDRESS_MASK		0x3f

/* Instruction - Set DD RAM Address */
#define HD44780U_INST_SET_DD_RAM_ADDRESS	0x80
#define HD44780U_DD_RAM_ADDRESS_MASK		0x7f


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct LcdDriverStateDef
{
	lu_uint8_t		lcdCurrentAddressCount;
	lu_uint8_t		lcdColumn;
	lu_uint8_t		lcdRow;
}LcdDriverStateStr;

typedef enum
{
	LCD_RS_INSTRUCTION,
	LCD_RS_INSTRUCTION_INIT_4BIT,
	LCD_RS_DATA
} LCD_RS;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void convertBit2Byte(lu_uint8_t *dataPtr, lu_uint8_t bitNumber, lu_uint8_t bitValue);
SB_ERROR LCDWrite(LcdDriverInitStr *initPtr, lu_uint8_t data, LCD_RS rs);
SB_ERROR LCDRead(LcdDriverInitStr *initPtr, lu_uint8_t *dataPtr, LCD_RS rs);
SB_ERROR LCDCusorHome(LcdDriverInitStr *initPtr);
SB_ERROR LCDSetCursorTo(LcdDriverInitStr *initPtr, lu_uint8_t col, lu_uint8_t row);
SB_ERROR LCDSetDDRAMAddr(LcdDriverInitStr *initPtr, lu_uint8_t address);
SB_ERROR LCDPutc(LcdDriverInitStr *initPtr, lu_int8_t c);
SB_ERROR LCDIncrementAddressCount(LcdDriverInitStr *initPtr);
SB_ERROR LCDWaitBusy(LcdDriverInitStr *initPtr, lu_uint8_t *addressCountPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static LcdDriverStateStr lcdState;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


void LCDDriverInitStructure(LcdDriverInitStr *initPtr)
{
	lu_uint32_t		idx;

	/* Set default mode for 8-bit */
	initPtr->lcdInitMode = ( LCD_INIT_MODE_RW | 
							 LCD_INIT_MODE_ENABLE_BACKLIGHT | 
							 LCD_INIT_MODE_DISPLAY_ON | 
							 LCD_INIT_MODE_2_LINES
							);

	/* Init all ioID's */
	for (idx = 0; idx < LCD_IO_ID_LAST; idx++)
	{
		initPtr->ioID[idx] = BOARD_IO_MAX_IDX;
	}

	/* Set display layout size -> default to 20x4 display */
	initPtr->lcdNumRows    = 4;
	initPtr->lcdNumColumns = 20;

	/* Set DDRAM offset addresses -> default for 20x4 display */
	initPtr->lcdDDRamRowOffset[0] = 0x00;
	initPtr->lcdDDRamRowOffset[1] = 0x40;
	initPtr->lcdDDRamRowOffset[2] = 0x14;
	initPtr->lcdDDRamRowOffset[3] = 0x54;

	/* Set default timeouts */
	initPtr->lcdInitResetDelayMs   = LCD_INIT_RESET_DELAY_MS;
	initPtr->lcdInitCmdDelayMs     = LCD_INIT_CMD_RESET_DELAY_MS;

	initPtr->lcdCommandDelayMs     = LCD_COMMAND_DELAY_MS;
	initPtr->lcdCommandLongDelayMs = LCD_COMMAND_LONG_DELAY_MS; /* Used for Clear disp / return home*/
}

SB_ERROR LCDDriverInit(LcdDriverInitStr *initPtr)
{
	SB_ERROR		retError;
	lu_uint8_t		data;
	lu_uint8_t		functionSetData;
	
	retError = SB_ERROR_NONE;
	
	/* Default current lcd state */
	lcdState.lcdCurrentAddressCount = 0;
	lcdState.lcdColumn = 0;
	lcdState.lcdRow = 0;

	/* Backlight control */
	if (initPtr->lcdInitMode & LCD_INIT_MODE_ENABLE_BACKLIGHT)
	{
		if (initPtr->ioID[LCD_IO_ID_BACK_LIGHT] < BOARD_IO_MAX_IDX)
		{
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_BACK_LIGHT], 1);
		}
	}
	else
	{
		if (initPtr->ioID[LCD_IO_ID_BACK_LIGHT] < BOARD_IO_MAX_IDX)
		{
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_BACK_LIGHT], 0);
		}
	}

	/* Wait for display to initialise */
	STDelayTime(initPtr->lcdInitResetDelayMs);

	/* Define the function mode */
	functionSetData = HD44780U_INST_FUNCTION_SET;
	if (initPtr->lcdInitMode & LCD_INIT_MODE_4_BIT)
	{
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB4], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB5], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB6], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB7], 0L);
	}
	else
	{
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB0], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB1], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB2], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB3], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB4], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB5], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB6], 0L);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB7], 0L);

		functionSetData |= HD44780U_8_BIT_MODE;
	}
	if (initPtr->lcdInitMode & LCD_INIT_MODE_5X10_DOT_FONT)
	{
		functionSetData |= HD44780U_5X10_FONT;
	}
	if (initPtr->lcdInitMode & LCD_INIT_MODE_2_LINES)
	{
		functionSetData |= HD44780U_2_LINES;
	}

	/* Check 4/8 bit mode of operation */
	if (initPtr->lcdInitMode & LCD_INIT_MODE_4_BIT)
	{
		/* Set 4 Bit mode */

		/* 1st Attempt */
		LCDWrite(initPtr, 0x30, LCD_RS_INSTRUCTION_INIT_4BIT);
		STDelayTime(initPtr->lcdInitCmdDelayMs);

		/* 2nd Attempt */
		LCDWrite(initPtr, 0x30, LCD_RS_INSTRUCTION_INIT_4BIT);
		STDelayTime(initPtr->lcdInitCmdDelayMs);

		/* 3rd Attempt */
		LCDWrite(initPtr, 0x30, LCD_RS_INSTRUCTION_INIT_4BIT);
		STDelayTime(initPtr->lcdInitCmdDelayMs);
	}
	else
	{
		/* Set 8 Bit mode */

		/* 1st Attempt */
		LCDWrite(initPtr, 0x30, LCD_RS_INSTRUCTION);
		STDelayTime(initPtr->lcdInitCmdDelayMs);

		/* 2nd Attempt */
		LCDWrite(initPtr, 0x30, LCD_RS_INSTRUCTION);
		STDelayTime(initPtr->lcdCommandDelayMs);

		/* 3rd Attempt */
		LCDWrite(initPtr, 0x30, LCD_RS_INSTRUCTION);
	}

	/* Set the display mode - Function Set */
	LCDWrite(initPtr, functionSetData, LCD_RS_INSTRUCTION);

	/* Turn on the display */
	data = HD44780U_INST_DISPLAY_ONOFF;
	if (initPtr->lcdInitMode & LCD_INIT_MODE_DISPLAY_ON)
	{
		data |= HD44780U_DISPLAY_ON;
	}
	if (initPtr->lcdInitMode & LCD_INIT_MODE_CURSOR_ON)
	{
		data |= HD44780U_DISPLAY_CURSOR_ON;
	}
	if (initPtr->lcdInitMode & LCD_INIT_MODE_BLINK_ON)
	{
		data |= HD44780U_DISPLAY_CURSOR_FLASH;
	}
	LCDWrite(initPtr, data, LCD_RS_INSTRUCTION);

	/* Clear the display */
	LCDClearDisplay(initPtr);

	/* Set display entry mode */
	data = (HD44780U_INST_ENTRY_MODE_SET | HD44780U_ENTRY_MODE_INCREMENT);
	if (initPtr->lcdInitMode & LCD_INIT_MODE_TEXT_RIGHT2LEFT)
	{
		data |= HD44780U_ENTRY_MODE_SHIFT_LEFT;
	}
	LCDWrite(initPtr, data, LCD_RS_INSTRUCTION);

	//LCDPutString(initPtr, "Lucy Switchgear- HMI", 0, 0);

	return retError;
}

SB_ERROR LCDPutString(LcdDriverInitStr *initPtr, lu_int8_t *lineString, lu_uint8_t col, lu_uint8_t row)
{
	SB_ERROR		retError;
	lu_uint8_t		idx;
	lu_uint32_t		len;

	retError = SB_ERROR_PARAM;

	len = strlen(lineString);
	if (len > 0 && len <= (initPtr->lcdNumRows * initPtr->lcdNumColumns))
	{
		retError = LCDSetCursorTo(initPtr, col, row);
		if (retError == SB_ERROR_NONE)
		{
			for (idx = 0; idx < len; idx++)
			{
				LCDPutc(initPtr, *(lineString + idx));
			}
		}
	}
	return retError;
}

SB_ERROR LCDClearDisplay(LcdDriverInitStr *initPtr)
{
	SB_ERROR retError;

    retError = LCDWrite(initPtr, HD44780U_INST_CLEAR_DISPLAY, LCD_RS_INSTRUCTION);
	STDelayTime(initPtr->lcdCommandLongDelayMs);

	return retError;
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void convertBit2Byte(lu_uint8_t *dataPtr, lu_uint8_t bitNumber, lu_uint8_t bitValue)
{
	if (bitValue)
	{
		*dataPtr |= (1 << bitNumber);
	}
	else
	{
		*dataPtr &= ~(1 << bitNumber);
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDWrite(LcdDriverInitStr *initPtr, lu_uint8_t data, LCD_RS rs)
{
	SB_ERROR retError;
	lu_int32_t 	value;

	retError = SB_ERROR_NONE;

	/* Select Data or Instruction register */
	if (rs == LCD_RS_DATA)
	{
		/* Set RS High - Data register */
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_RS], 1);
	}
	else
	{
		/* Set RS Low - Instruction register */
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_RS], 0);
	}

	/* Set RW Low */
	if (initPtr->lcdInitMode & LCD_INIT_MODE_RW)
	{
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_RW], 0);
	}

	/* Set data direction as Output for DB0-DB3 / DB4-DB7 */
	if (initPtr->lcdInitMode & LCD_INIT_MODE_4_BIT)
	{
		/* 4 bit data mode */
		if (rs == LCD_RS_INSTRUCTION_INIT_4BIT)
		{
			/* High nibble */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB4], (lu_uint32_t)((data & 0x10) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB5], (lu_uint32_t)((data & 0x20) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB6], (lu_uint32_t)((data & 0x40) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB7], (lu_uint32_t)((data & 0x80) ? 1L : 0L));
		}
		else
		{
			/* High nibble */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB4], (lu_uint32_t)((data & 0x10) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB5], (lu_uint32_t)((data & 0x20) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB6], (lu_uint32_t)((data & 0x40) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB7], (lu_uint32_t)((data & 0x80) ? 1L : 0L));

			/* High edge on Enable */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
			STDleayTimeUsec(2);
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 1);
			STDleayTimeUsec(2);
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
			STDleayTimeUsec(100);

			/* Low nibble */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB0], (lu_uint32_t)((data & 0x01) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB1], (lu_uint32_t)((data & 0x02) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB2], (lu_uint32_t)((data & 0x04) ? 1L : 0L));
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB3], (lu_uint32_t)((data & 0x08) ? 1L : 0L));
		}
	}
	else
	{
		/* 8 bit data mode */
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB0], (lu_uint32_t)((data & 0x01) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB1], (lu_uint32_t)((data & 0x02) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB2], (lu_uint32_t)((data & 0x04) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB3], (lu_uint32_t)((data & 0x08) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB4], (lu_uint32_t)((data & 0x10) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB5], (lu_uint32_t)((data & 0x20) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB6], (lu_uint32_t)((data & 0x40) ? 1L : 0L));
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_DB7], (lu_uint32_t)((data & 0x80) ? 1L : 0L));
	}

	/* High edge on Enable */
	IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
	STDleayTimeUsec(2);
	IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 1);
	STDleayTimeUsec(2);
	IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
	STDleayTimeUsec(100);

	/* Restorre daata lines to inpputs */
	if (initPtr->lcdInitMode & LCD_INIT_MODE_4_BIT)
	{
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB4], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB5], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB6], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB7], &value);
	}
	else
	{
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB0], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB1], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB2], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB3], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB4], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB5], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB6], &value);
		IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB7], &value);
	}
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDRead(LcdDriverInitStr *initPtr, lu_uint8_t *dataPtr, LCD_RS rs)
{
	SB_ERROR 		retError;
	lu_int32_t 		value;
	lu_uint8_t      dataByte;

	retError = SB_ERROR_NONE;

	if (initPtr->lcdInitMode & LCD_INIT_MODE_RW)
	{
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_RW], 1);

		/* Select Data or Instruction register */
		if (rs == LCD_RS_DATA)
		{
			/* Set RS High - Data register */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_RS], 1);
		}
		else
		{
			/* Set RS Low - Instruction register */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_RS], 0);
		}

		/* High edge on Enable */
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
		STDleayTimeUsec(2);
		IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 1);
		STDleayTimeUsec(2);

		dataByte = 0;

		/* Set data direction as Input for DB0-DB3 / DB4-DB7 */
		if (initPtr->lcdInitMode & LCD_INIT_MODE_4_BIT)
		{
			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB4], &value);
			convertBit2Byte(&dataByte, 4, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB5], &value);
			convertBit2Byte(&dataByte, 5, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB6], &value);
			convertBit2Byte(&dataByte, 6, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB7], &value);
			convertBit2Byte(&dataByte, 7, value);

			/* High edge on Enable */
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
			STDleayTimeUsec(2);
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 1);
			STDleayTimeUsec(2);
			IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
			STDleayTimeUsec(100);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB4], &value);
			convertBit2Byte(&dataByte, 0, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB5], &value);
			convertBit2Byte(&dataByte, 1, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB6], &value);
			convertBit2Byte(&dataByte, 2, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB7], &value);
			convertBit2Byte(&dataByte, 3, value);
		}
		else
		{
			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB0], &value);
			convertBit2Byte(&dataByte, 0, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB1], &value);
			convertBit2Byte(&dataByte, 1, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB2], &value);
			convertBit2Byte(&dataByte, 2, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB3], &value);
			convertBit2Byte(&dataByte, 3, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB4], &value);
			convertBit2Byte(&dataByte, 4, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB5], &value);
			convertBit2Byte(&dataByte, 5, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB6], &value);
			convertBit2Byte(&dataByte, 6, value);

			IOManagerGetValue(initPtr->ioID[LCD_IO_ID_DB7], &value);
			convertBit2Byte(&dataByte, 7, value);
		}
		*dataPtr = dataByte;
	}

	IOManagerSetValue(initPtr->ioID[LCD_IO_ID_EN], 0);
	STDleayTimeUsec(100);
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDCusorHome(LcdDriverInitStr *initPtr)
{
	SB_ERROR retError;

	lcdState.lcdCurrentAddressCount = 0;

	retError = LCDWrite(initPtr, HD44780U_INST_RETURN_HOME, LCD_RS_INSTRUCTION);
	STDelayTime(initPtr->lcdCommandLongDelayMs);

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDSetCursorTo(LcdDriverInitStr *initPtr, lu_uint8_t col, lu_uint8_t row)
{
	SB_ERROR 	retError;
	lu_uint8_t 	dataAddr;

	if (col > initPtr->lcdNumColumns || row > initPtr->lcdNumColumns)
	{
		retError = SB_ERROR_PARAM;
	}
	else
	{
		dataAddr = (col + initPtr->lcdDDRamRowOffset[row]);
		retError = LCDSetDDRAMAddr(initPtr, dataAddr);
		if (retError == SB_ERROR_NONE)
		{
			lcdState.lcdCurrentAddressCount = dataAddr;
			lcdState.lcdColumn              = col;
			lcdState.lcdRow                 = row;
		}
	}
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDSetDDRAMAddr(LcdDriverInitStr *initPtr, lu_uint8_t address)
{
	SB_ERROR 	retError;
	lu_uint8_t 	data;

	data = (HD44780U_INST_SET_DD_RAM_ADDRESS + (address & HD44780U_DD_RAM_ADDRESS_MASK));
	retError = LCDWrite(initPtr, data, LCD_RS_INSTRUCTION);

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDPutc(LcdDriverInitStr *initPtr, lu_int8_t c)
{
	SB_ERROR 	retError;
	lu_uint8_t  addrCount;

	LCDWaitBusy(initPtr, &addrCount);
	retError = LCDWrite(initPtr, c, LCD_RS_DATA);

	retError = LCDIncrementAddressCount(initPtr);

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDIncrementAddressCount(LcdDriverInitStr *initPtr)
{
	SB_ERROR 	retError;
	lu_uint8_t 	addrCount;

	retError = SB_ERROR_NONE;

	/* Increment ro/col counter */
	if (lcdState.lcdColumn < (initPtr->lcdNumColumns - 1))
	{
		lcdState.lcdColumn++;
	}
	else
	{
		lcdState.lcdColumn = 0;
		if (lcdState.lcdRow < (initPtr->lcdNumRows - 1))
		{
			lcdState.lcdRow++;
		}
		else
		{
			lcdState.lcdRow = 0;
		}
	}
	/* Calculate what the next AddrCount should be */
	lcdState.lcdCurrentAddressCount = (lcdState.lcdColumn + initPtr->lcdDDRamRowOffset[lcdState.lcdRow]);

	/* Read current addrCount and then against current address count */
	LCDWaitBusy(initPtr, &addrCount);
	if (lcdState.lcdCurrentAddressCount != addrCount)
	{
		retError = LCDSetDDRAMAddr(initPtr, lcdState.lcdCurrentAddressCount);
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR LCDWaitBusy(LcdDriverInitStr *initPtr, lu_uint8_t *addressCountPtr)
{
	SB_ERROR 	retError;
	lu_uint8_t  data;
	lu_uint32_t counter;

	retError = SB_ERROR_NONE;

	counter = 0;
	do
	{
		retError = LCDRead(initPtr, &data, LCD_RS_INSTRUCTION);
		STDleayTimeUsec(100);
		counter++;
		if (counter > (LCD_COMMAND_LONG_DELAY_MS * 1000))
		{
			/* Operation timed out */
			retError = SB_ERROR_LCD_FAIL;
		}
	} while ((retError == SB_ERROR_NONE) && (data & HD44780U_BUSY_FLAG));

	if (retError == SB_ERROR_NONE)
	{
		/* Address counter is updated after this delay */
		STDleayTimeUsec(2);

		/* Get address counter */
		retError = LCDRead(initPtr, &data, LCD_RS_INSTRUCTION);
		*addressCountPtr = (data & HD44780U_ADDRESS_COUNT_MASK);
	}

	return retError;
}

/*
 *********************** End of file ******************************************
 */
