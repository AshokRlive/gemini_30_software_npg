/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Controller common definitions.
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   01/012/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _DUALSWITCHCOMMON_INCLUDED
#define _DUALSWITCHCOMMON_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_SWITCH_CHANNELS			4			/* Two channels per switch i.e. open & close */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/* Switch Control Parameters */
typedef struct SwConChanParamsDef
{
	lu_uint32_t ioIdRelayUp;
	lu_uint32_t ioIdRelayDown;
	lu_uint32_t ioIdRelayLink;
}SwConChanParamsStr;

typedef struct ioIdSwIndicationDef
{
	lu_uint32_t ioIdSwIndication[9];
}ioIdSwIndicationStr;

typedef struct PolarityFeedbackDef
{
	lu_uint32_t ioIDFeedback;
	lu_uint8_t  status;
}PolarityFeedbackStr;

typedef struct PolaritySelectorDef
{
	lu_uint8_t           polarityA;
	lu_uint32_t          ioIDSelectA;
	PolarityFeedbackStr  switch_A_Polarity;
	lu_bool_t		     hardwareMotorLimitEnableA;
	lu_bool_t			 actuatorLimitSwitchEnableA;

	lu_uint8_t           polarityB;
	lu_uint32_t          ioIDSelectB;
	PolarityFeedbackStr  switch_B_Polarity;
	lu_bool_t		     hardwareMotorLimitEnableB;
	lu_bool_t			 actuatorLimitSwitchEnableB;
}PolaritySelectorStr;

/* Switch Control Initialisation Parameters */
typedef struct SwConInitParamsDef
{
	SwConChanParamsStr SwitchConParams[MAX_SWITCH_CHANNELS];

	PolaritySelectorStr polaritySelect;

	lu_bool_t            doNotCheckSwitchPositionA;
	lu_bool_t            doNotCheckSwitchPositionB;
	
	/* Front Panel LEDs */
	lu_uint32_t ioIdLedFP_SW_A_CmdOpen;
	lu_uint32_t ioIdLedFP_SW_A_CmdClose;
	lu_uint32_t ioIdLedFP_SW_B_CmdOpen;
	lu_uint32_t ioIdLedFP_SW_B_CmdClose;

	lu_uint32_t ioIdLedFP_SW_A_MotorSupply;
	lu_uint32_t ioIdLedFP_SW_B_MotorSupply;
	/* LEDs */
	lu_uint32_t ioIdLedCtrlOkGreen;
	lu_uint32_t ioIdLedCtrlOkRed;

	/* Relay FB Inputs */
	lu_uint32_t ioIdRelayFb_SW_A_OpenUpDnClosed;
	lu_uint32_t ioIdRelayFb_SW_A_OpenLkClosed;
	lu_uint32_t ioIdRelayFb_SW_A_CloseUpDnClosed;
	lu_uint32_t ioIdRelayFb_SW_A_CloseLkClosed;

	lu_uint32_t ioIdRelayFb_SW_B_OpenUpDnClosed;
	lu_uint32_t ioIdRelayFb_SW_B_OpenLkClosed;
	lu_uint32_t ioIdRelayFb_SW_B_CloseUpDnClosed;
	lu_uint32_t ioIdRelayFb_SW_B_CloseLkClosed;

	lu_uint32_t ioIdRelayFb_SW_A_OutputUpDnClosed;
	lu_uint32_t ioIdRelayFb_SW_A_OutputLkClosed;
	lu_uint32_t ioIdRelayFb_SW_B_OutputUpDnClosed;
	lu_uint32_t ioIdRelayFb_SW_B_OutputLkClosed;


	lu_uint32_t ioIdVMotor_SW_A_FbOn;
	lu_uint32_t ioIdVMotor_SW_B_FbOn;

	/* Relay Control Outputs */
	lu_uint32_t ioIdRelayCtrlVMotor_A_A;
	lu_uint32_t ioIdRelayCtrlVMotor_A_B;
	lu_uint32_t ioIdRelayCtrlVMotor_B_A;
	lu_uint32_t ioIdRelayCtrlVMotor_B_B;

	lu_uint32_t ioIdBridgeMotor_A_AA;
	lu_uint32_t ioIdBridgeMotor_A_AB;
	lu_uint32_t ioIdBridgeMotor_A_BA;
	lu_uint32_t ioIdBridgeMotor_A_BB;

	lu_uint32_t ioIdBridgeMotor_B_AA;
	lu_uint32_t ioIdBridgeMotor_B_AB;
	lu_uint32_t ioIdBridgeMotor_B_BA;
	lu_uint32_t ioIdBridgeMotor_B_BB;

	lu_uint32_t ioIdHardwareActMotorLimit_A;
	lu_uint32_t ioIdHardwareActMotorLimit_B;

	lu_uint32_t ioIdMotorJogEnable_A;
	lu_uint32_t ioIdMotorJogEnable_B;

	lu_uint32_t ioId_SW_A_OpenedInput;
	lu_uint32_t ioId_SW_A_ClosedInput;

	lu_uint32_t ioId_SW_B_OpenedInput;
	lu_uint32_t ioId_SW_B_ClosedInput;

	lu_uint32_t ioId_SW_A_ActLimit_OpenedInput;
	lu_uint32_t ioId_SW_A_ActLimit_ClosedInput;

	lu_uint32_t ioId_SW_B_ActLimit_OpenedInput;
	lu_uint32_t ioId_SW_B_ActLimit_ClosedInput;

}SwConInitParamsStr;

/* User Event Parameters */
typedef struct SwEventParamsDef
{
	/*! Channel ID */
	lu_uint8_t		channel;

	/*! Time out  in seconds */
	lu_uint8_t		timeout;

	/*! Time out  in milliseconds */
	lu_uint16_t		preOpTime;

	/*! Local or remote operation indicator */
	lu_bool_t		local;

	/*! Status - okay or error */
	lu_uint8_t		status;

	/* Time message received */
	lu_uint32_t 	operateCmdreceiveTime; // Time for when CAN operate time received.
	lu_uint32_t 	selectReceiveTime;
	lu_uint32_t 	cancelReceiveTime;
	lu_uint32_t 	operateReceiveTime;  // Start time for actual operate countdown
	lu_uint32_t 	delayOpReceiveTime;
	lu_uint32_t 	preOpReceiveTime;
}SwEventParamsStr;

/* User defined state machine context info */
typedef struct SwitchUserContextDef
{
	lu_uint8_t activeChannel;
	lu_bool_t  ignoreSwitchPositionA;
	lu_bool_t  ignoreSwitchPositionB;
	SwConInitParamsStr swConInitParams;
}SwitchUserContextStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


#endif /* _SWITCHCOMMON_INCLUDED */

/*
 *********************** End of file ******************************************
 */
