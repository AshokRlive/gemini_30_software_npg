/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch FSM module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DualSwitchFSM.h"
#include "DualSwitchController.h"
#include "IOManager.h"
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "IOManagerIO.h"
#include "systemTime.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSwitchControllerEnum.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void 		MotorSupplyOff(struct DualSwitch *fsm);
lu_bool_t 	GetRelayFeedback(struct DualSwitchContext *fsm, struct SwConBinInputs *binInp);
SB_ERROR 	ReadBinaryInputs(struct DualSwitchContext *fsm, struct SwConBinInputs *binInp);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern lu_bool_t  				DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_NUM_MAX];

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
 

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/* SelectLocal & SelectRemote are almost identical - a common function is required here!!!
 * (Same is also true for operate / cancel / select timeout )
 * */

/*
 * Guard functions - called on event to check transition to new state is valid.
 */
 
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t SelectLocal(struct DualSwitchContext *fsm)
{
	SB_ERROR           retError;
	IO_STAT_LOR        locRem;
	lu_bool_t          localSelect;
	lu_uint8_t         binInhibit;
	lu_uint8_t         channelNo;
	lu_uint32_t        ioID;
	lu_int32_t         binIP;
	DualSwitchConfigStr *SwitchConfigurationPtr;

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &fsm->_owner->SwitchConfiguration[fsm->_owner->swEventParams.channel];

	/* Check for operation already in progress */
	if(fsm->_owner->switchStatus.activeChannel < MAX_SWITCH_CHANNELS)
	{
		/* Return CAN status to MODULE_MSG_ID_CMD_SWC_CH_SELECT_C for a SelectLocalEvent */
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;

		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			/* ERROR ? why is this set to false when it can also be set to REPLY_STATUS***, should be an enum! */
			fsm->_owner->errorCode = LU_FALSE;
		}
		
		/* Return false  to indicate a operation in progress */
		return LU_FALSE;
	}

	/*
	 * Check binary inhibit
	 */

	/* Get binary inhibit bit mask of channel numbers */
	binInhibit = SwitchConfigurationPtr->inhibitBI;
	
	/* Check maximum number of channels */
	for(channelNo = 0; channelNo < IO_CHAN_DI_MAP_SIZE; channelNo++)
	{
		/* Check for bit set in bit mask */
		if(binInhibit & (1 << channelNo))
		{
			/* Get Iod from channel number */
			ioID = ioChanDIMap[channelNo].ioID;
			
			/* Get value of binary input */
			retError = IOManagerGetValue(ioID, &binIP);

			/* Need to check retError is ok for getValue ??????? & if error return a different error code */
			/* Generate error code if set */
			if(binIP)
			{
				fsm->_owner->swEventParams.status = REPLY_STATUS_INHIBIT_ERROR;
				if(fsm->_owner->operationCommitted == LU_TRUE)
				{
					fsm->_owner->errorCode = REPLY_STATUS_INHIBIT_ERROR;
				}
				
				/* Return false  to indicate a operation in progress */
				return LU_FALSE;
			}
		}
	}

	/* Get select message local/remote */
	localSelect = fsm->_owner->swEventParams.local;

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	/* Return true if local otherwise return an error  */
	if((locRem == IO_STAT_LOR_LOCAL ) && localSelect)
	{
		/* Set active channel */
		fsm->_owner->switchStatus.activeChannel = fsm->_owner->swEventParams.channel;

		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_OKAY;
		}
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		
		/* Return false  to indicate a local/remote error */
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t SelectRemote(struct DualSwitchContext *fsm)
{
	SB_ERROR           retError;
	IO_STAT_LOR        locRem;
	lu_bool_t          remoteSelect;
	lu_uint8_t         binInhibit;
	lu_uint8_t         channelNo;
	lu_uint32_t        ioID;
	lu_int32_t         binIP;
	DualSwitchConfigStr *SwitchConfigurationPtr;

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &fsm->_owner->SwitchConfiguration[fsm->_owner->swEventParams.channel];

	/* Check for operation already in progress */
	if(fsm->_owner->switchStatus.activeChannel < MAX_SWITCH_CHANNELS)
	{
		/* Return CAN status to MODULE_MSG_ID_CMD_SWC_CH_SELECT_C for a SelectRemoteEvent */
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;

		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_SELECT_ERROR;
		}

		/* Return false  to indicate a operation in progress */
		return LU_FALSE;
	}

	/* Get binary inhibit bit mask of channel numbers */
	binInhibit = SwitchConfigurationPtr->inhibitBI;

	/* Check maximum number of channels */
	for(channelNo = 0; channelNo < IO_CHAN_DI_MAP_SIZE; channelNo++)
	{
		/* Check for bit set in bit mask */
		if(binInhibit & (1 << channelNo))
		{
			/* Get Iod from channel number */
			ioID = ioChanDIMap[channelNo].ioID;
			/* Get value of binary input */
			retError = IOManagerGetValue(ioID, &binIP);
			/* Generate error code if set */
			if(binIP)
			{
				fsm->_owner->swEventParams.status = REPLY_STATUS_INHIBIT_ERROR;
				if( fsm->_owner->operationCommitted == LU_TRUE)
				{
					fsm->_owner->errorCode = REPLY_STATUS_INHIBIT_ERROR;
				}
				
				/* Return false  to indicate a operation in progress */
				return LU_FALSE;
			}
		}
	}

	/* Get select message local/remote */
	remoteSelect = !(fsm->_owner->swEventParams.local);

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if((locRem == IO_STAT_LOR_REMOTE) && remoteSelect)
	{
		/* Set active channel */
		fsm->_owner->switchStatus.activeChannel = fsm->_owner->swEventParams.channel;

		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		/* Return false  to indicate a local/remote error */
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t OperateLocal(struct DualSwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   localOperate;

	/*
	 * Check if the the switch operate local is valid
	 */

	/* Get operate message local/remote */
	localOperate = fsm->_owner->swEventParams.local;

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if((locRem == IO_STAT_LOR_LOCAL) && localOperate)
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		return LU_TRUE;
	}
	else
	/* Return true switch operate local message has been received */
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t OperateRemote(struct DualSwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   remoteOperate;

	/*
	 * Check if the the switch operate remote is valid
	 */

	/* Get operate message local/remote */
	remoteOperate = !(fsm->_owner->swEventParams.local);

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if( (locRem == IO_STAT_LOR_REMOTE) && remoteOperate )
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		return LU_TRUE;
	}
	else
	/* Return true switch operate message has been received */
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if( fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t CancelLocal(struct DualSwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   localCancel;

	/*
	 * Check if the the switch cancel local is valid
	 */

	/* Get operate message local/remote */
	localCancel = fsm->_owner->swEventParams.local;

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if((locRem == IO_STAT_LOR_LOCAL) && localCancel)
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return true - switch cancel local message has been received */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_TRUE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t CancelRemote(struct DualSwitchContext *fsm)
{
	SB_ERROR    retError;
	IO_STAT_LOR locRem;
	lu_bool_t   remoteCancel;

	/*
	 * Check if the the switch cancel remote is valid
	 */

	/* Get operate message local/remote */
	remoteCancel = !(fsm->_owner->swEventParams.local);

	/* Get state of physical local/remote switch. */
	retError = boardIOGetLocalRemoteStatus(&locRem);

	if((locRem == IO_STAT_LOR_REMOTE) && remoteCancel)
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return true switch - cancel message has been received */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_LOCAL_REMOTE_ERROR;
		}
		return LU_TRUE;
	}
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Check for timeout from selected to operate and return LU_TRUE if timeout
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t SelectTimeout(struct DualSwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t selectTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();

	/* Get select start time */
	startTime 		= fsm->_owner->swEventParams.selectReceiveTime;
	selectTimeout 	= fsm->_owner->swEventParams.timeout;

	elapsedTime = STElapsedTime( startTime , time );

	if(elapsedTime > (selectTimeout * 1000))
	{
		fsm->_owner->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_SELECT_ERROR;
		}
		
		/* Return true to indicate a select timeout error */
		return LU_TRUE;
	}
	else
	{
		/* Set CAN reply response */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
		/* Return false to indicate timeout not exceeded */
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t DelayTimeout(struct DualSwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t delayTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();
	lu_uint32_t ioId;
	SB_ERROR    RetVal;

	/* Get select start time */
	startTime 		= fsm->_owner->swEventParams.operateCmdreceiveTime;
	delayTimeout 	= fsm->_owner->swEventParams.timeout;

	elapsedTime = STElapsedTime( startTime , time );

	/*
	 * If there is no delayed operation then go straight to pre-operating state.
	 * Otherwise wait for delayed operation to timeout.
	 */
	if((delayTimeout == 0) || ( elapsedTime > (delayTimeout * 1000)))
	{
		/* Stop LED flashing */
		ioId = fsm->_owner->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdOpen;
		RetVal = IOManagerSetValue(ioId, 0);
		ioId = fsm->_owner->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdClose;
		RetVal = IOManagerSetValue(ioId, 0);

		ioId = fsm->_owner->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdOpen;
		RetVal = IOManagerSetValue(ioId, 0);
		ioId = fsm->_owner->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdClose;
		RetVal = IOManagerSetValue(ioId, 0);

		/* Return TRUE  to indicate no delay or delay finished */
		return LU_TRUE;
	}
	else
	{
		/* Return true to indicate delay  not yet completed */
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t PreOpDelayTimeout(struct DualSwitchContext *fsm)
{
	lu_uint32_t startTime;
	lu_uint32_t preOpTimeout;
	lu_uint32_t elapsedTime;
	lu_uint32_t time = STGetTime();

	/* Get select start time */
	/* Note that receiveTime is NOT the Operate received time stamp at this point */
	startTime 		= fsm->_owner->swEventParams.preOpReceiveTime; /* modified by DelayTimeout() */
	preOpTimeout 	= fsm->_owner->swEventParams.preOpTime;

	elapsedTime 	= STElapsedTime( startTime , time );

	/*
	 * If there is no pre-op delay then go straight to operating state.
	 * Otherwise wait for delayed operation to timeout.
	 */
	if( (preOpTimeout == 0) || (elapsedTime > preOpTimeout) )
	{
		/* Return TRUE  to indicate no delay or delay finished */
		return LU_TRUE;
	}
	else
	{
		/* Return true to indicate delay  not yet completed */
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t OperationTimeout(struct DualSwitchContext *fsm)
{
	lu_uint32_t        opStartTime;
	lu_uint32_t        operationTimeout;
	lu_uint32_t        elapsedTime;
	lu_uint32_t        time = STGetTime();
	DualSwitchConfigStr *SwitchConfigurationPtr;

	switch(fsm->_owner->switchStatus.activeChannel)
	{
		// These should be enums and not defines, also should be in board specific code!!
		case DSM_CH_SWOUT_OPEN_SWITCH_A:
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			if(fsm->_owner->switchStatus.swConInitParams.doNotCheckSwitchPositionA &&
			   fsm->_owner->switchStatus.ignoreSwitchPositionA
			  )
			{
				/* Set CAN reply response */
				fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;

				/* Bypass operation timeout */
				return LU_FALSE;
			}
			break;
			
		case DSM_CH_SWOUT_OPEN_SWITCH_B:
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			if(fsm->_owner->switchStatus.swConInitParams.doNotCheckSwitchPositionB &&
			   fsm->_owner->switchStatus.ignoreSwitchPositionA
			  )
			{
				/* Set CAN reply response */
				fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;

				/* Bypass operation timeout */
				return LU_FALSE;
			}
			break;
			
		default:
			break;
	}

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &fsm->_owner->SwitchConfiguration[fsm->_owner->switchStatus.activeChannel];

	/* Get operate start time */
	opStartTime 		= fsm->_owner->swEventParams.operateReceiveTime;
	operationTimeout 	= (lu_uint32_t)SwitchConfigurationPtr->opTimeoutS;

	elapsedTime = STElapsedTime( opStartTime , time );

	if(elapsedTime > (operationTimeout * 1000))
	{
		/* Operate Timeout */
		fsm->_owner->swEventParams.status = REPLY_STATUS_OPERATE_ERROR;
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_OPERATE_ERROR;
		}
		/* Return false  to indicate a select timeout error */
		return LU_TRUE;
	}
	
	/* Set CAN reply response */
	fsm->_owner->swEventParams.status = REPLY_STATUS_OKAY;
	/* Return true to indicate timeout not exceeded */
	return LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t OverrunTimeout(struct DualSwitchContext *fsm)
{
	lu_uint32_t        startTime;
	lu_uint32_t        delayTimeout;
	lu_uint32_t        elapsedTime;
	lu_uint32_t        time = STGetTime();
	SB_ERROR           RetVal;
	DualSwitchConfigStr *SwitchConfigurationPtr;
	
	LU_UNUSED(fsm);

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &fsm->_owner->SwitchConfiguration[fsm->_owner->switchStatus.activeChannel];

	/* Get select start time */
	startTime 		= fsm->_owner->overrunStartTime;
	delayTimeout 	= (lu_uint32_t)SwitchConfigurationPtr->overrunMS;

	elapsedTime = STElapsedTime(startTime ,time);

	if(delayTimeout == 0)
	{
		/* Return TRUE  to indicate no delay */
		RetVal = LU_TRUE;
	}
	else if(elapsedTime > (delayTimeout))
	{
		/* Return TRUE  to indicate the delay has completed */
		RetVal = LU_TRUE;
	}
	else
	{
		/* Return true to indicate delay  not yet completed */
		RetVal = LU_FALSE;
	}

	return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t ErrorCode(struct DualSwitchContext *fsm)
{
	LU_UNUSED(fsm);
	
	/* Return true if there has been an error during the operation */
	if(fsm->_owner->errorCode != REPLY_STATUS_OKAY)
	{
		return LU_TRUE;
	}
	
	return LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t IsCheckPos(struct DualSwitchContext *fsm)
{
	/* Check if bypass switch position check */
	switch(fsm->_owner->switchStatus.activeChannel)
	{
		case DSM_CH_SWOUT_OPEN_SWITCH_A:
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			if(fsm->_owner->switchStatus.swConInitParams.doNotCheckSwitchPositionA)
			{
				return LU_FALSE;
			}
			break;
			
		case DSM_CH_SWOUT_OPEN_SWITCH_B:
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			if(fsm->_owner->switchStatus.swConInitParams.doNotCheckSwitchPositionB)
			{
				return LU_FALSE;
			}
			break;
			
		default:
			break;
	}
	
	return LU_TRUE;
}
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperationComplete(struct DualSwitchContext *fsm)
{
	SB_ERROR           RetVal;
	lu_int32_t         open_sw = 0;
	lu_int32_t         close_sw = 0;
	lu_uint32_t        ioId;
	lu_uint8_t         channel;
	lu_uint32_t        opStartTime;
	lu_uint32_t        operationTimeout;
	lu_uint32_t        elapsedTime;
	lu_uint32_t        time = STGetTime();
	DualSwitchConfigStr *SwitchConfigurationPtr;

	/* Is the operation in progress an open or close */
	channel = fsm->_owner->switchStatus.activeChannel;

	/* Check if bypass switch position check */
	switch(channel)
	{
		// These should be enums and not defines, also should be in board specific code!!
		case DSM_CH_SWOUT_OPEN_SWITCH_A:
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			if(fsm->_owner->switchStatus.swConInitParams.doNotCheckSwitchPositionA &&
			   fsm->_owner->switchStatus.ignoreSwitchPositionA
			  )
			{
				/* Bypass switch position check and wait until timeout */
				
				/* Set configuration pointer to correct channel */
				SwitchConfigurationPtr = &fsm->_owner->SwitchConfiguration[channel];

				/* Get operate start time */
				opStartTime 		= fsm->_owner->swEventParams.operateReceiveTime;
				operationTimeout 	= (lu_uint32_t)SwitchConfigurationPtr->opTimeoutS;

				elapsedTime = STElapsedTime( opStartTime , time );

				if(elapsedTime > (operationTimeout * 1000))
				{
					/* Return true  to indicate a operation complete */
					return LU_TRUE;
				}

				/* Return false  to indicate a operation not complete */
				return LU_FALSE;
			}
			break;
			
		case DSM_CH_SWOUT_OPEN_SWITCH_B:
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			if(fsm->_owner->switchStatus.swConInitParams.doNotCheckSwitchPositionB &&
			   fsm->_owner->switchStatus.ignoreSwitchPositionB
			  )
			{
				/* Bypass switch position check and wait until timeout */
				
				/* Set configuration pointer to correct channel */
				SwitchConfigurationPtr = &fsm->_owner->SwitchConfiguration[channel];

				/* Get operate start time */
				opStartTime 		= fsm->_owner->swEventParams.operateReceiveTime;
				operationTimeout 	= (lu_int32_t)SwitchConfigurationPtr->opTimeoutS;

				elapsedTime = STElapsedTime( opStartTime , time );

				if(elapsedTime > (operationTimeout * 1000))
				{
					/* Return true  to indicate a operation complete */
					return LU_TRUE;
				}

				/* Return false  to indicate a operation not complete */
				return LU_FALSE;
			}
			break;
			
		default:
			break;
	}
	

	/* Get state of inputs i.e. open or closed */
	switch(channel)
	{
	case DSM_CH_SWOUT_CLOSE_SWITCH_A:
	case DSM_CH_SWOUT_OPEN_SWITCH_A:
		if(fsm->_owner->switchStatus.swConInitParams.polaritySelect.polarityA ==
		   DUAL_SWITCH_POLARITY_DIRECT_DRIVE &&
		   (fsm->_owner->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableA == LU_TRUE ||
			(fsm->_owner->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableA == LU_FALSE &&
			 fsm->_owner->switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableA == LU_TRUE)))
		{
			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_A_ActLimit_OpenedInput;
			RetVal = IOManagerGetValue(ioId, &open_sw);

			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_A_ActLimit_ClosedInput;
			RetVal = IOManagerGetValue(ioId, &close_sw);
		}
		else
		{
			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_A_OpenedInput;
			RetVal = IOManagerGetValue(ioId, &open_sw);

			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_A_ClosedInput;
			RetVal = IOManagerGetValue(ioId, &close_sw);
		}
		break;

	case DSM_CH_SWOUT_CLOSE_SWITCH_B:
	case DSM_CH_SWOUT_OPEN_SWITCH_B:
		if(fsm->_owner->switchStatus.swConInitParams.polaritySelect.polarityB ==
		   DUAL_SWITCH_POLARITY_DIRECT_DRIVE &&
		   (fsm->_owner->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableB == LU_TRUE ||
			(fsm->_owner->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableB == LU_FALSE &&
		    fsm->_owner->switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableB == LU_TRUE)))
		{
			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_B_ActLimit_OpenedInput;
			RetVal = IOManagerGetValue(ioId, &open_sw);

			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_B_ActLimit_ClosedInput;
			RetVal = IOManagerGetValue(ioId, &close_sw);
		}
		else
		{
			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_B_OpenedInput;
			RetVal = IOManagerGetValue(ioId, &open_sw);

			ioId = fsm->_owner->switchStatus.swConInitParams.ioId_SW_B_ClosedInput;
			RetVal = IOManagerGetValue(ioId, &close_sw);
		}
		break;

	default:
		return LU_FALSE;
		break;
	}
	
	/* check state of switch against current operation */

	switch(channel)
	{
	case DSM_CH_SWOUT_CLOSE_SWITCH_A:
	case DSM_CH_SWOUT_CLOSE_SWITCH_B:
		if (close_sw)
		{
			/* Return true to indicate close operation is complete */
			return LU_TRUE;
		}
		break;

	case DSM_CH_SWOUT_OPEN_SWITCH_A:
	case DSM_CH_SWOUT_OPEN_SWITCH_B:
		if (open_sw)
		{
			/* Return true to indicate close operation is complete */
			return LU_TRUE;
		}
		break;

	default:
		return LU_FALSE;
		break;
	}

	/* Operation not completed so return false */
	return LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t CheckRelayError(struct DualSwitchContext *fsm)
{
	struct SwConBinInputs binInp;

	/* If any relay is not in its correct initial position then indicate an error */
	if(GetRelayFeedback(fsm, &binInp))
	{
		if(fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_RELAY_ERROR;
		}
		return LU_TRUE;
	}

	if(fsm->_owner->operationCommitted == LU_TRUE)
	{
		fsm->_owner->errorCode = REPLY_STATUS_OKAY;
	}

	return LU_FALSE;

}

/******************************************************************************
 * Action functions - called on entry to a state.
 ******************************************************************************/

 /*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationCommittedAction(struct DualSwitch *fsm)
{
	/*
	 * Set flag to indicate the operation is committed and
	 * can't be interrupted. An error code will be set instead
	 * for later reporting
	 */
	fsm->operationCommitted = LU_TRUE;

	fsm->swEventParams.preOpReceiveTime = STGetTime();
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationUnCommittedAction(struct DualSwitch *fsm)
{
	/*
	 * Set flag to indicate the operation is no longer committed
	 */
	fsm->operationCommitted = LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_InitAction(struct DualSwitch *fsm)
{
	/* Set active channel to indicate no operation active */
	fsm->switchStatus.activeChannel = MAX_SWITCH_CHANNELS;

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_OKAY;

	fsm->errorCode = REPLY_STATUS_OKAY;
	fsm->operationCommitted = LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_LocalSelectedAction(struct DualSwitch *fsm)
{
	/* Set channel activated */
	fsm->switchStatus.activeChannel = fsm->swEventParams.channel;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_RemoteSelectedAction(struct DualSwitch *fsm)
{
	/* Set channel activated */
	fsm->switchStatus.activeChannel = fsm->swEventParams.channel;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_SelectTimedOutAction(struct DualSwitch *fsm)
{
	/* Generate event to MCM */
	SysAlarmSetLogEvent(LU_TRUE,
						SYS_ALARM_SEVERITY_WARNING,
						SYS_ALARM_SUBSYSTEM_SWITCH_CONTROLLER,
						SYSALC_SWITCH_CONTROLLER_SELECT_TIMEOUT_ERR,
						0
					   );

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_TIMEOUT_ERROR;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_CancelledAction(struct DualSwitch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CANCEL_ERROR;

	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_CANCEL_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_ResetAction(struct DualSwitch *fsm)
{
	/* The operation is finished so reset the committed flag */
	fsm->operationCommitted = LU_FALSE;

	/* Set active channel to indicate no operation active */
	fsm->switchStatus.activeChannel = MAX_SWITCH_CHANNELS;

	/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
	MotorSupplyOff(fsm);

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_OKAY;
	fsm->errorCode = REPLY_STATUS_OKAY;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_PowerMotorAction(struct DualSwitch *fsm)
{
	SB_ERROR RetVal;
	lu_int32_t ioId;

	/* Switch on 24V Motor supply and turn on 24V Motor Supply LED */

	if( fsm->switchStatus.activeChannel == DSM_CH_SWOUT_OPEN_SWITCH_A   ||
		fsm->switchStatus.activeChannel == DSM_CH_SWOUT_CLOSE_SWITCH_A
	  )
	{
		if (fsm->switchStatus.swConInitParams.polaritySelect.polarityA == DUAL_SWITCH_POLARITY_DIRECT_DRIVE)
		{
			switch(fsm->switchStatus.activeChannel)
			{
			case DSM_CH_SWOUT_OPEN_SWITCH_A:
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_AA;
				RetVal = IOManagerSetValue(ioId, 1);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_AB;
				RetVal = IOManagerSetValue(ioId, 1);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_BA;
				RetVal = IOManagerSetValue(ioId, 0);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_BB;
				RetVal = IOManagerSetValue(ioId, 0);
				break;

			case DSM_CH_SWOUT_CLOSE_SWITCH_A:
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_AA;
				RetVal = IOManagerSetValue(ioId, 0);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_AB;
				RetVal = IOManagerSetValue(ioId, 0);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_BA;
				RetVal = IOManagerSetValue(ioId, 1);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_BB;
				RetVal = IOManagerSetValue(ioId, 1);
				break;

			default:
				break;
			}
		}
		else
		{
			/* Switch on 24V Motor supply and turn on 24V Motor Supply LED */
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_A;
			RetVal = IOManagerSetValue(ioId, 0);

			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_B;
			RetVal = IOManagerSetValue(ioId, 1);
		}

		ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_MotorSupply;
		RetVal = IOManagerSetValue(ioId, 1);
	}

	if( fsm->switchStatus.activeChannel == DSM_CH_SWOUT_OPEN_SWITCH_B   ||
		fsm->switchStatus.activeChannel == DSM_CH_SWOUT_CLOSE_SWITCH_B
	  )
	{
		if (fsm->switchStatus.swConInitParams.polaritySelect.polarityB == DUAL_SWITCH_POLARITY_DIRECT_DRIVE)
		{
			switch(fsm->switchStatus.activeChannel)
			{
			case DSM_CH_SWOUT_OPEN_SWITCH_B:
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_AA;
				RetVal = IOManagerSetValue(ioId, 1);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_AB;
				RetVal = IOManagerSetValue(ioId, 1);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_BA;
				RetVal = IOManagerSetValue(ioId, 0);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_BB;
				RetVal = IOManagerSetValue(ioId, 0);
				break;

			case DSM_CH_SWOUT_CLOSE_SWITCH_B:
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_AA;
				RetVal = IOManagerSetValue(ioId, 0);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_AB;
				RetVal = IOManagerSetValue(ioId, 0);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_BA;
				RetVal = IOManagerSetValue(ioId, 1);
				ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_BB;
				RetVal = IOManagerSetValue(ioId, 1);
				break;

			default:
				break;
			}
		}
		else
		{
			/* Switch on 24V Motor supply and turn on 24V Motor Supply LED */
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_A;
			RetVal = IOManagerSetValue(ioId, 0);

			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_B;
			RetVal = IOManagerSetValue(ioId, 1);
		}

		ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_MotorSupply;
		RetVal = IOManagerSetValue(ioId, 1);
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_StartMotorAction(struct DualSwitch *fsm)
{
	SB_ERROR           RetVal;
	lu_uint16_t        pulseLength;
	lu_int32_t         value = 0;
	lu_uint32_t        ioId;
	DualSwitchConfigStr *SwitchConfigurationPtr;

	/* Default relay error to true. Will be set to false when the feedback goes true */
// refs# 2660 disbable relay error!!
//	relayError = LU_TRUE;

	fsm->swEventParams.operateReceiveTime = STGetTime();

	/* Set configuration pointer to correct channel */
	SwitchConfigurationPtr = &fsm->SwitchConfiguration[fsm->switchStatus.activeChannel];

	fsm->switchStatus.ignoreSwitchPositionA = LU_FALSE;
	
	if(fsm->switchStatus.swConInitParams.doNotCheckSwitchPositionA)
	{
		/* Get state of inputs i.e. open or closed */

		switch(fsm->switchStatus.activeChannel)
		{
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			if(fsm->switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableA == LU_FALSE &&
			   fsm->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableA)
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_A_ActLimit_ClosedInput;
			}
			else
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_A_ClosedInput;
			}
			RetVal = IOManagerGetValue(ioId, &value);
			
			/* Check if already in correct position before operation */
			if (value)
			{
				fsm->switchStatus.ignoreSwitchPositionA = LU_TRUE;
			}
			break;

		case DSM_CH_SWOUT_OPEN_SWITCH_A:
			if(fsm->switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableA == LU_FALSE &&
			   fsm->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableA)
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_A_ActLimit_OpenedInput;
			}
			else
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_A_OpenedInput;
			}
			RetVal = IOManagerGetValue(ioId, &value);
		
			/* Check if already in correct position before operation */
			if (value)
			{
				fsm->switchStatus.ignoreSwitchPositionA = LU_TRUE;
			}
			break;

		default:
			break;
		}
	}

	fsm->switchStatus.ignoreSwitchPositionB = LU_FALSE;
	
	if(fsm->switchStatus.swConInitParams.doNotCheckSwitchPositionB)
	{
		switch(fsm->switchStatus.activeChannel)
		{
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			if(fsm->switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableB == LU_FALSE &&
			   fsm->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableB)
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_B_ActLimit_ClosedInput;
			}
			else
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_B_ClosedInput;
			}
			RetVal = IOManagerGetValue(ioId, &value);
		
			/* Check if already in correct position before operation */
			if (value)
			{
				fsm->switchStatus.ignoreSwitchPositionB = LU_TRUE;
			}
			break;

		case DSM_CH_SWOUT_OPEN_SWITCH_B:
			if(fsm->switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableB == LU_FALSE &&
			   fsm->switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableB)
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_B_ActLimit_OpenedInput;
			}
			else
			{
				ioId = fsm->switchStatus.swConInitParams.ioId_SW_B_OpenedInput;
			}
			RetVal = IOManagerGetValue(ioId, &value);
		
			/* Check if already in correct position before operation */
			if (value)
			{
				fsm->switchStatus.ignoreSwitchPositionB = LU_TRUE;
			}
			break;

		default:
			break;
		}
	}
	
	/*
	 * Turn Motor on to open or close the switch depending on the channel
	 * Turn on for length of pulse specified in Config command
	 */
	pulseLength = SwitchConfigurationPtr->pulseLengthMS;

	switch(fsm->switchStatus.activeChannel)
	{
		case DSM_CH_SWOUT_OPEN_SWITCH_A:
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			switch(fsm->switchStatus.swConInitParams.polaritySelect.polarityA)
			{
				case SWITCH_POLARITY_UP:
					ioId = fsm->switchStatus.swConInitParams.SwitchConParams[fsm->switchStatus.activeChannel].ioIdRelayUp;
					/* Open/Close specified relay for the specified pulse length */
					RetVal = IOManagerSetPulse(ioId, pulseLength);
					break;
					
				case SWITCH_POLARITY_DOWN:
					ioId = fsm->switchStatus.swConInitParams.SwitchConParams[fsm->switchStatus.activeChannel].ioIdRelayDown;
					/* Open/Close specified relay for the specified pulse length */
					RetVal = IOManagerSetPulse(ioId, pulseLength);
					break;
					
				case SWITCH_POLARITY_LINK:
					ioId = fsm->switchStatus.swConInitParams.SwitchConParams[fsm->switchStatus.activeChannel].ioIdRelayLink;
					/* Open/Close specified relay for the specified pulse length */
					RetVal = IOManagerSetPulse(ioId, pulseLength);
					break;
					
				case DUAL_SWITCH_POLARITY_DIRECT_DRIVE:
					// Turn on motor supply FET
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_A;
					RetVal = IOManagerSetValue(ioId, 1);
					break;

				default:
					if(fsm->operationCommitted == LU_TRUE)
					{
						fsm->errorCode = REPLY_STATUS_PARAM_ERROR;
					}
					break;
			}
			

			/* Switch on LED */
			if (fsm->switchStatus.activeChannel == DSM_CH_SWOUT_OPEN_SWITCH_A)
			{
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdOpen;
			}
			else if(fsm->switchStatus.activeChannel == DSM_CH_SWOUT_CLOSE_SWITCH_A)
			{
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdClose;
			}
	
			RetVal = IOManagerSetValue(ioId, 1);
			break;
		
		case DSM_CH_SWOUT_OPEN_SWITCH_B:
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			/* Determine which relay to pulse from the polarity specified in the config */
			switch(fsm->switchStatus.swConInitParams.polaritySelect.polarityB)
			{
				case SWITCH_POLARITY_UP:
					ioId = fsm->switchStatus.swConInitParams.SwitchConParams[fsm->switchStatus.activeChannel].ioIdRelayUp;
					/* Open/Close specified relay for the specified pulse length */
					RetVal = IOManagerSetPulse(ioId, pulseLength);
					break;
					
				case SWITCH_POLARITY_DOWN:
					ioId = fsm->switchStatus.swConInitParams.SwitchConParams[fsm->switchStatus.activeChannel].ioIdRelayDown;
					/* Open/Close specified relay for the specified pulse length */
					RetVal = IOManagerSetPulse(ioId, pulseLength);
					break;
					
				case SWITCH_POLARITY_LINK:
					ioId = fsm->switchStatus.swConInitParams.SwitchConParams[fsm->switchStatus.activeChannel].ioIdRelayLink;
					/* Open/Close specified relay for the specified pulse length */
					RetVal = IOManagerSetPulse(ioId, pulseLength);
					break;
					
				case DUAL_SWITCH_POLARITY_DIRECT_DRIVE:
					// Turn on motor supply FET
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_A;
					RetVal = IOManagerSetValue(ioId, 1);
					break;

				default:
					if(fsm->operationCommitted == LU_TRUE)
					{
						fsm->errorCode = REPLY_STATUS_PARAM_ERROR;
					}
					break;
			}

			/* Switch on LED */
			if(fsm->switchStatus.activeChannel == DSM_CH_SWOUT_OPEN_SWITCH_B)
			{
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdOpen;
			}
			else if(fsm->switchStatus.activeChannel == DSM_CH_SWOUT_CLOSE_SWITCH_B)
			{
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdClose;
			}
			
			RetVal = IOManagerSetValue(ioId, 1);
			break;
			
		default:
			break;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_ErrorAction(struct DualSwitch *fsm)
{
	/*
	 * Set CAN error status in reply to the error detected during operation
	 * to indicate to MCM what went wrong
	 */
	fsm->swEventParams.status = fsm->errorCode;

	/* Reset the error code so that on the next cancel command the okay reply is returned */
	fsm->errorCode = REPLY_STATUS_OKAY;

	/* The operation is finished so reset the committed flag */
	fsm->operationCommitted = LU_FALSE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_DelayedOperationAction(struct DualSwitch *fsm)
{
	SB_ERROR    RetVal;
	lu_uint32_t ioId;
	lu_uint8_t  channel;
	lu_uint32_t delayTimeout;

	channel         = fsm->switchStatus.activeChannel;
	delayTimeout 	= fsm->swEventParams.timeout;

	/*
	 * If delayed operation is required i.e. delayTimeout > 0
	 * then set the LED flashing.
	 * Otherwise do nothing.
	 */
	if(delayTimeout > 0)
	{
		/* Flash front panel LED to indicate delayed action */
		switch(channel)
		{
			case DSM_CH_SWOUT_OPEN_SWITCH_A:
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdOpen;
				RetVal = IOManagerSetFlash(ioId, DELAYED_ACTION_PULSE_MS);
				break;
			
			case DSM_CH_SWOUT_CLOSE_SWITCH_A:
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdClose;
				RetVal = IOManagerSetFlash(ioId, DELAYED_ACTION_PULSE_MS);
				break;
				
			case DSM_CH_SWOUT_OPEN_SWITCH_B:
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdOpen;
				RetVal = IOManagerSetFlash(ioId, DELAYED_ACTION_PULSE_MS);
				break;
				
			
			case DSM_CH_SWOUT_CLOSE_SWITCH_B:
				ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdClose;
				RetVal = IOManagerSetFlash(ioId, DELAYED_ACTION_PULSE_MS);
				break;
				
			default:
				break;
		}
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_RelayErrorAction(struct DualSwitch *fsm)
{
	LU_UNUSED(fsm);

	if((fsm->vMotorError == LU_TRUE) && (fsm->operationCommitted == LU_TRUE))
	{
		/* Set CAN error status in reply */
		fsm->swEventParams.status = REPLY_STATUS_VMOTOR_ERROR;
		fsm->errorCode = REPLY_STATUS_VMOTOR_ERROR;
	}
	else if((fsm->relayError == LU_TRUE) && (fsm->operationCommitted == LU_TRUE))
	{
		/* Set CAN error status in reply */
		fsm->swEventParams.status = REPLY_STATUS_RELAY_ERROR;
		fsm->errorCode = REPLY_STATUS_RELAY_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationCompleteAction(struct DualSwitch *fsm)
{
	/* Turn off motor power supply */
	MotorSupplyOff(fsm);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationOverrunAction(struct DualSwitch *fsm)
{
	fsm->overrunStartTime = STGetTime();
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationTimeoutAction(struct DualSwitch *fsm)
{
	// DO NOT SWITCH OFF MOTOR SUPPLY!!

	fsm->swEventParams.status = REPLY_STATUS_OPERATE_ERROR;
	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_OPERATE_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_SelectErrorAction(struct DualSwitch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_SELECT_ERROR;
	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_SELECT_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperateErrorAction(struct DualSwitch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_OPERATE_REJECT_ERROR;
	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_OPERATE_REJECT_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_CancelErrorAction(struct DualSwitch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CANCEL_ERROR;
	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_CANCEL_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperatingFeedbackAction(struct DualSwitch *fsm)
{
	SB_ERROR    retError;
	lu_uint32_t ioId;
	lu_int32_t  vMotorOnFb;
	lu_int32_t  relayClosedFb;

	/*
	 * Check that V Motor is closed and check that only the relay
	 * specified by the operation is closed. All others must be open.
	 */

	/* Get the V Motor ioID- */
	switch(fsm->switchStatus.activeChannel)
	{
		case DSM_CH_SWOUT_OPEN_SWITCH_A:
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			ioId = fsm->switchStatus.swConInitParams.ioIdVMotor_SW_A_FbOn;

			retError = IOManagerGetValue(ioId , &vMotorOnFb);
	// refs # 2660 disable vmotor relay error feedback
	//		if( (vMotorOnFb == LU_TRUE) && (vMotorError == LU_TRUE) )
	//		{
	//			vMotorError = LU_FALSE;
	//		}
	//		else if( (vMotorOnFb == LU_FALSE) && (vMotorError == FALSE) )
	//		{
	//			vMotorError = LU_TRUE;
	//		}
			break;
			
		case DSM_CH_SWOUT_OPEN_SWITCH_B:
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			ioId = fsm->switchStatus.swConInitParams.ioIdVMotor_SW_B_FbOn;

			retError = IOManagerGetValue(ioId , &vMotorOnFb);
	// refs # 2660 disable vmotor relay error feedback
	//		if( (vMotorOnFb == LU_TRUE) && (vMotorError == LU_TRUE) )
	//		{
	//			vMotorError = LU_FALSE;
	//		}
	//		else if( (vMotorOnFb == LU_FALSE) && (vMotorError == FALSE) )
	//		{
	//			vMotorError = LU_TRUE;
	//		}
			break;
			
		default:
			break;
	}

	/* Get the ioId of the relay specified by the operation */
	switch(fsm->switchStatus.activeChannel)
	{
		case DSM_CH_SWOUT_OPEN_SWITCH_A:
			/* Determine which relay to check from the polarity specified in the config */
			switch(fsm->switchStatus.swConInitParams.polaritySelect.polarityA)
			{
				case SWITCH_POLARITY_UP:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_A_OpenUpDnClosed;
					break;
					
				case SWITCH_POLARITY_DOWN:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_A_OpenUpDnClosed;
					break;
					
				case SWITCH_POLARITY_LINK:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_A_OpenLkClosed;
					break;
					
				default:
					if(fsm->operationCommitted == LU_TRUE)
					{
						fsm->errorCode = REPLY_STATUS_PARAM_ERROR;
					}
			}
			break;
		
		case DSM_CH_SWOUT_CLOSE_SWITCH_A:
			/* Determine which relay to check from the polarity specified in the config */
			switch(fsm->switchStatus.swConInitParams.polaritySelect.polarityA)
			{
				case SWITCH_POLARITY_UP:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_A_CloseUpDnClosed;
					break;
					
				case SWITCH_POLARITY_DOWN:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_A_CloseUpDnClosed;
					break;
					
				case SWITCH_POLARITY_LINK:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_A_CloseLkClosed;
					break;
					
				default:
					if(fsm->operationCommitted == LU_TRUE)
					{
						fsm->errorCode = REPLY_STATUS_PARAM_ERROR;
					}
			}
			break;
			
		case DSM_CH_SWOUT_OPEN_SWITCH_B:
			/* Determine which relay to check from the polarity specified in the config */
			switch(fsm->switchStatus.swConInitParams.polaritySelect.polarityB)
			{
				case SWITCH_POLARITY_UP:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_B_OpenUpDnClosed;
					break;
					
				case SWITCH_POLARITY_DOWN:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_B_OpenUpDnClosed;
					break;
					
				case SWITCH_POLARITY_LINK:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_B_OpenLkClosed;
					break;
					
				default:
					if(fsm->operationCommitted == LU_TRUE)
					{
						fsm->errorCode = REPLY_STATUS_PARAM_ERROR;
					}
			}
			break;
			
		case DSM_CH_SWOUT_CLOSE_SWITCH_B:
			/* Determine which relay to check from the polarity specified in the config */
			switch(fsm->switchStatus.swConInitParams.polaritySelect.polarityB)
			{
				case SWITCH_POLARITY_UP:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_B_CloseUpDnClosed;
					break;
					
				case SWITCH_POLARITY_DOWN:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_B_CloseUpDnClosed;
					break;
					
				case SWITCH_POLARITY_LINK:
					ioId = fsm->switchStatus.swConInitParams.ioIdRelayFb_SW_B_CloseLkClosed;
					break;
					
				default:
					if(fsm->operationCommitted == LU_TRUE)
					{
						fsm->errorCode = REPLY_STATUS_PARAM_ERROR;
					}
			}
			break;
			
		default:
			break;
	}
	
	retError = IOManagerGetValue(ioId, &relayClosedFb);
	if((relayClosedFb == LU_TRUE) && (fsm->relayError == LU_TRUE))
	{
		fsm->relayError = LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_ConfigErrorAction(struct DualSwitch *fsm)
{
	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_CONFIG_ERROR;
	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_CONFIG_ERROR;
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_CompleteErrorAction(struct DualSwitch *fsm)
{
	/* Turn off motor power supply */
	MotorSupplyOff(fsm);

	/* Set CAN error status in reply */
	fsm->swEventParams.status = REPLY_STATUS_COMPLETE_ERROR;
	if(fsm->operationCommitted == LU_TRUE)
	{
		fsm->errorCode = REPLY_STATUS_COMPLETE_ERROR;
	}
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void MotorSupplyOff(struct DualSwitch *fsm)
{
	SB_ERROR    RetVal;
	lu_uint32_t ioId;

	// FIXME ERROR only switch off the active channel!!!
	//
	if(DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_SWITCH_A]== LU_FALSE)
	{
		if (fsm->switchStatus.swConInitParams.polaritySelect.polarityA == DUAL_SWITCH_POLARITY_DIRECT_DRIVE)
		{
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_A;
			RetVal = IOManagerSetValue(ioId, 0);

			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_AA;
			RetVal = IOManagerSetValue(ioId, 0);
			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_AB;
			RetVal = IOManagerSetValue(ioId, 0);
			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_BA;
			RetVal = IOManagerSetValue(ioId, 0);
			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_A_BB;
			RetVal = IOManagerSetValue(ioId, 0);
		}
		else
		{

			/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_A;
			RetVal = IOManagerSetValue(ioId, 1);
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_B;
			RetVal = IOManagerSetValue(ioId, 0);

		}
		ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_MotorSupply;
		RetVal = IOManagerSetValue(ioId, 0);
	}
	// FIXME ERROR only switch off the active channel!!!
	//
	if(DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_SWITCH_B]== LU_FALSE)
	{
		if (fsm->switchStatus.swConInitParams.polaritySelect.polarityB == DUAL_SWITCH_POLARITY_DIRECT_DRIVE)
		{
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_A;
			RetVal = IOManagerSetValue(ioId, 0);

			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_AA;
			RetVal = IOManagerSetValue(ioId, 0);
			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_AB;
			RetVal = IOManagerSetValue(ioId, 0);
			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_BA;
			RetVal = IOManagerSetValue(ioId, 0);
			ioId = fsm->switchStatus.swConInitParams.ioIdBridgeMotor_B_BB;
			RetVal = IOManagerSetValue(ioId, 0);
		}
		else
		{
			/* Switch off 24V Motor supply and turn off 24V Motor Supply LED */
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_A;
			RetVal = IOManagerSetValue(ioId, 1);
			ioId = fsm->switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_B;
			RetVal = IOManagerSetValue(ioId, 0);
		}

		ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_MotorSupply;
		RetVal = IOManagerSetValue(ioId, 0);
	}
	// FIXME ERROR only switch off the active channel!!!
	//
	/* Open All relays */
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_A].ioIdRelayUp;
	RetVal = IOManagerSetValue(ioId, 1);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_A].ioIdRelayDown;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_A].ioIdRelayLink;
	RetVal = IOManagerSetValue(ioId, 0);

	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_A].ioIdRelayUp;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_A].ioIdRelayDown;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_A].ioIdRelayLink;
	RetVal = IOManagerSetValue(ioId, 0);

	// FIXME ERROR only switch off the active channel!!!
	//
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_B].ioIdRelayUp;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_B].ioIdRelayDown;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_B].ioIdRelayLink;
	RetVal = IOManagerSetValue(ioId, 0);

	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_B].ioIdRelayUp;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_B].ioIdRelayDown;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_B].ioIdRelayLink;
	RetVal = IOManagerSetValue(ioId, 0);

	// FIXME ERROR only switch off the active channel!!!
	//
	/* Turn off command LEDs */
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdOpen;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_A_CmdClose;
	RetVal = IOManagerSetValue(ioId, 0);

	// FIXME ERROR only switch off the active channel!!!
	//
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdOpen;
	RetVal = IOManagerSetValue(ioId, 0);
	ioId = fsm->switchStatus.swConInitParams.ioIdLedFP_SW_B_CmdClose;
	RetVal = IOManagerSetValue(ioId, 0);
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t GetRelayFeedback(struct DualSwitchContext *fsm, struct SwConBinInputs *binInp)
{
	SB_ERROR  retError;
	lu_bool_t retVal = LU_FALSE;

	/* Take snapshot of all binary inputs */
	retError = ReadBinaryInputs(fsm,binInp);
	if(retError != SB_ERROR_NONE)
	{
		/* Error detected in reading binary inputs */
		retVal = LU_TRUE;
		if( fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_IOMANAGER_ERROR;
		}
	}

	if( binInp->ioIdRelayFb_SW_A_OpenUpDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_A_OpenLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_A_CloseUpDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_A_CloseLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_B_OpenUpDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_B_OpenLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_B_CloseUpDnClosed )
	{
		retVal = LU_TRUE;
	}

	if( binInp->ioIdRelayFb_SW_B_CloseLkClosed )
	{
		retVal = LU_TRUE;
	}

	if( retVal == LU_TRUE )
	{
		if( fsm->_owner->operationCommitted == LU_TRUE)
		{
			fsm->_owner->errorCode = REPLY_STATUS_RELAY_ERROR;
		}
	}

	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR ReadBinaryInputs(struct DualSwitchContext *fsm, struct SwConBinInputs *binInp)
{
	SB_ERROR    retError = SB_ERROR_NONE;
	lu_uint32_t ioID;
	lu_int32_t  binIP;

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_A_OpenUpDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_A_OpenUpDnClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_A_OpenLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_A_OpenLkClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_A_CloseUpDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_A_CloseUpDnClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_A_CloseLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_A_CloseLkClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_B_OpenUpDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_B_OpenUpDnClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_B_OpenLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_B_OpenLkClosed		= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_B_CloseUpDnClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_B_CloseUpDnClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdRelayFb_SW_B_CloseLkClosed;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdRelayFb_SW_B_CloseLkClosed	= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdVMotor_SW_A_FbOn;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdVMotor_SW_A_FbOn				= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioIdVMotor_SW_B_FbOn;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioIdVMotor_SW_B_FbOn				= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioId_SW_A_OpenedInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioId_SW_A_OpenedInput				= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioId_SW_A_ClosedInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioId_SW_A_ClosedInput				= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioId_SW_B_OpenedInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioId_SW_B_OpenedInput				= binIP;
	}

	if( retError == SB_ERROR_NONE)
	{
		ioID = fsm->_owner->switchStatus.swConInitParams.ioId_SW_B_ClosedInput;
		retError = IOManagerGetValue(ioID, &binIP);
		binInp->ioId_SW_B_ClosedInput				= binIP;
	}

	return retError;
}

/*
 *********************** End of file ******************************************
 */
