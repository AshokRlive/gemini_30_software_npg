/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Controller library header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SWITCHCONTROLLER_INCLUDED
#define _SWITCHCONTROLLER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"
#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"
#include "IOManager.h"
#include "systemTime.h"
#include "DualSwitchCommon.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define SWITCH_CON_TICK_MS			100 		/* Schedule every n ms */

/* Default configuration values */
#define POLARITY  					0			/* Polarity of Binary input that inhibits the operation */
#define PULSELENGTH_MS  			5000		/* Length of operation pulse in milliseconds */
#define OPERATION_TIMEOUT_S			30			/* Operation timeout in seconds */
#define INHIBIT_BI  				((1<<SCM_CH_DINPUT_SPARE_INPUT_1) | (1<<SCM_CH_DINPUT_SPARE_INPUT_2))


typedef enum DualSwitchMotorNumer
{
	DSM_MOTOR_SUPPLY_SWITCH_A =0,
	DSM_MOTOR_SUPPLY_SWITCH_B,
	DSM_MOTOR_SUPPLY_NUM_MAX

}DualSwitchMotorNumberEnm;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 	FALSE indicate couldn't start.
 *   			True indicates start event sent to state machine.
 *
 ******************************************************************************
 */
extern lu_bool_t DualSwitchControlStart(void);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR DualSwitchControlInit(SwConInitParamsStr *initParamPtr);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR DualSwitchControlCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR DualSwitchControlTick(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitchFSMTick(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR DualSwitchPolarityConfigA(lu_uint8_t polarity,
		                                  lu_bool_t doNotCheckSwitchPosition,
		                                  lu_bool_t hardwareMotorLimitEnable,
		                                  lu_bool_t actuatorLimitSwitchEnable);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR DualSwitchPolarityConfigB(lu_uint8_t polarity,
		                                  lu_bool_t doNotCheckSwitchPosition,
		                                  lu_bool_t hardwareMotorLimitEnable,
		                                  lu_bool_t actuatorLimitSwitchEnable);

#endif /* _SWITCHCONTROLLER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
