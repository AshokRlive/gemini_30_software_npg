/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch Controller module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "DualSwitchController.h"
#include "DualSwitchFSM.h"
#include "../../SupplyController/MotorSupplyController/include/MotorSupplyController.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
 
SB_ERROR DualSwitchSelectHandler(DualSwitchSelectStr *);
SB_ERROR DualSwitchOperateHandler(DualSwitchOperateStr *);
SB_ERROR DualSwitchCancelHandler(DualSwitchCancelStr *);
SB_ERROR DualSwitchSynchHandler(DualSwitchSynchStr *);
SB_ERROR DualSwitchConfig(DualSwitchConfigStr *);
void 	 DualSwitchFSMInit(void);

SB_ERROR DualSwitchMotorSupplySelectHandler(SupplySelectStr *);
SB_ERROR DualSwitchMotorSupplyOperateHandler(SupplyOperateStr *);
SB_ERROR DualSwitchMotorSupplyCancelHandler(SupplyCancelStr *);
SB_ERROR DualSwitchMotorSupplyControlTick(void);
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_bool_t  				motorSelected[DSM_MOTOR_SUPPLY_NUM_MAX] = {0,0};
lu_bool_t  				DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_NUM_MAX]={0,0};

/*! State machine context */
static struct DualSwitch switchFsm;

/*! List of supported CAN messages */
static const filterTableStr DualSwitchControlModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Switch commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_SELECT_C             , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C            , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C             , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_SWC_CH_SYNCH_C              , LU_FALSE , 0   	  },
  	{  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_DUAL_SWITCH_C            	  , LU_FALSE , 1   	  },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_PSC_CH_SELECT_C             , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C            , LU_FALSE , 1      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C             , LU_FALSE , 0      }

};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void DualSwitchFSMTick(void)
{
	/*
	 * Periodic Tick to initiate events in state machine.
	 */
	DualSwitchContext_TimerTickEvent(&switchFsm._fsm);
}

lu_bool_t DualSwitchControlStart(void)
{
	lu_int8_t    swChan;
	lu_bool_t    retVal = LU_FALSE;

	/*
	 * Determine if any channel have been configured.
	 */
	for(swChan = 0; swChan < MAX_SWITCH_CHANNELS; swChan++)
	{
		if (switchFsm.SwitchConfiguration[swChan].enable == LU_TRUE)
		{
			/* If any channel hasn't been configured return false */
			retVal = LU_TRUE;
		}
	}

	/*
	 * If any channel have been configured then send event to state machine
	 */
	if(retVal == LU_TRUE )
	{
		/*
		 * Initiate start event in state machine.
		 */
		DualSwitchContext_StartEvent(&switchFsm._fsm);
	}

	return retVal;
}

SB_ERROR DualSwitchControlInit(SwConInitParamsStr *initParamPtr)
{
	SB_ERROR sbError;

	/* Setup CAN Msg filter */
	sbError = CANFramingAddFilter( DualSwitchControlModulefilterTable,
								   SU_TABLE_SIZE(DualSwitchControlModulefilterTable,
											  filterTableStr)
							     );

	/* Copy initialisation parameters to the context structure */
	switchFsm.switchStatus.swConInitParams = *initParamPtr;

	/* Initialise state-machine context */
	DualSwitchFSMInit();

	return sbError;
}

SB_ERROR DualSwitchControlCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	LU_UNUSED( time );
	
	retError = SB_ERROR_CANC;

	/* Force a timer tick in the state machine */
	DualSwitchFSMTick();

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_SWC_CH_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DualSwitchSelectStr))
			{
				retError = DualSwitchSelectHandler((DualSwitchSelectStr *)msgPtr->msgBufPtr);
			}
			break;
			
		case MODULE_MSG_ID_CMD_SWC_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DualSwitchOperateStr))
			{
				retError = DualSwitchOperateHandler((DualSwitchOperateStr *)msgPtr->msgBufPtr);
			}
			break;
			
		case MODULE_MSG_ID_CMD_SWC_CH_CANCEL_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DualSwitchCancelStr))
			{
				retError = DualSwitchCancelHandler((DualSwitchCancelStr *)msgPtr->msgBufPtr);
			}
			break;
			
		case MODULE_MSG_ID_CMD_SWC_CH_SYNCH_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DualSwitchSynchStr))
			{
				retError = DualSwitchSynchHandler((DualSwitchSynchStr *)msgPtr->msgBufPtr);
			}
			break;
		case MODULE_MSG_ID_CMD_PSC_CH_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplySelectStr))
			{
				retError = DualSwitchMotorSupplySelectHandler((SupplySelectStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_PSC_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplyOperateStr))
			{
				retError = DualSwitchMotorSupplyOperateHandler((SupplyOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_PSC_CH_CANCEL_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SupplyCancelStr))
			{
				retError = DualSwitchMotorSupplyCancelHandler((SupplyCancelStr *)msgPtr->msgBufPtr);
			}
			break;
		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;


	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_DUAL_SWITCH_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DualSwitchConfigStr))
			{
				retError = DualSwitchConfig((DualSwitchConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}


SB_ERROR DualSwitchControlTick(void)
{
	DualSwitchFSMTick();

	return SB_ERROR_NONE;
}

SB_ERROR DualSwitchPolarityConfigA(lu_uint8_t polarity,
		                           lu_bool_t doNotCheckSwitchPosition,
		                           lu_bool_t hardwareMotorLimitEnable,
		                           lu_bool_t actuatorLimitSwitchEnable)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	lu_uint32_t ioID;

	switch (polarity)
	{
	case DUAL_SWITCH_POLARITY_UP:
		retError = IOManagerSetValue(switchFsm.switchStatus.swConInitParams.polaritySelect.ioIDSelectA, 1);
		break;

	case DUAL_SWITCH_POLARITY_DOWN:
		retError = IOManagerSetValue(switchFsm.switchStatus.swConInitParams.polaritySelect.ioIDSelectA, 0);
		break;

	case DUAL_SWITCH_POLARITY_LINK:
		retError = SB_ERROR_NONE;
		switchFsm.switchStatus.swConInitParams.polaritySelect.polarityA = polarity;
		break;

	case DUAL_SWITCH_POLARITY_DIRECT_DRIVE:
		retError = SB_ERROR_NONE;
		switchFsm.switchStatus.swConInitParams.polaritySelect.polarityA = polarity;
		switchFsm.switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableA = hardwareMotorLimitEnable;
		switchFsm.switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableA = actuatorLimitSwitchEnable;

		ioID = switchFsm.switchStatus.swConInitParams.ioIdHardwareActMotorLimit_A;
		if(hardwareMotorLimitEnable == LU_TRUE)
		{
			retError = IOManagerSetValue(ioID, 1);

			ioID = switchFsm.switchStatus.swConInitParams.ioIdMotorJogEnable_A;
			retError = IOManagerSetValue(ioID, 0);
		}
		else
		{
			retError = IOManagerSetValue(ioID, 0);

			ioID = switchFsm.switchStatus.swConInitParams.ioIdMotorJogEnable_A;
			retError = IOManagerSetValue(ioID, 1);
		}
		break;

	default:
		break;
	}

	switchFsm.switchStatus.swConInitParams.doNotCheckSwitchPositionA = doNotCheckSwitchPosition;
	
	return retError;
}

SB_ERROR DualSwitchPolarityConfigB(lu_uint8_t polarity,
		                           lu_bool_t doNotCheckSwitchPosition,
		                           lu_bool_t hardwareMotorLimitEnable,
		                           lu_bool_t actuatorLimitSwitchEnable)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	lu_uint32_t ioID;

	switch (polarity)
	{
	case DUAL_SWITCH_POLARITY_UP:
		retError = IOManagerSetValue(switchFsm.switchStatus.swConInitParams.polaritySelect.ioIDSelectB, 1);
		break;

	case DUAL_SWITCH_POLARITY_DOWN:
		retError = IOManagerSetValue(switchFsm.switchStatus.swConInitParams.polaritySelect.ioIDSelectB, 0);
		break;

	case DUAL_SWITCH_POLARITY_LINK:
		retError = SB_ERROR_NONE;
		switchFsm.switchStatus.swConInitParams.polaritySelect.polarityB = polarity;
		break;

	case DUAL_SWITCH_POLARITY_DIRECT_DRIVE:
		retError = SB_ERROR_NONE;
		switchFsm.switchStatus.swConInitParams.polaritySelect.polarityB = polarity;
		switchFsm.switchStatus.swConInitParams.polaritySelect.hardwareMotorLimitEnableB = hardwareMotorLimitEnable;
		switchFsm.switchStatus.swConInitParams.polaritySelect.actuatorLimitSwitchEnableB = actuatorLimitSwitchEnable;

		ioID = switchFsm.switchStatus.swConInitParams.ioIdHardwareActMotorLimit_B;
		if(hardwareMotorLimitEnable == LU_TRUE)
		{
			retError = IOManagerSetValue(ioID, 1);

			ioID = switchFsm.switchStatus.swConInitParams.ioIdMotorJogEnable_B;
			retError = IOManagerSetValue(ioID, 0);
		}
		else
		{
			retError = IOManagerSetValue(ioID, 0);

			ioID = switchFsm.switchStatus.swConInitParams.ioIdMotorJogEnable_B;
			retError = IOManagerSetValue(ioID, 1);
		}
		break;

	default:
		break;
	}

	switchFsm.switchStatus.swConInitParams.doNotCheckSwitchPositionB = doNotCheckSwitchPosition;
	
	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitchFSMInit(void)
{
	lu_uint8_t swChan;

	switchFsm.overrunStartTime 		= 0;
	switchFsm.operationCommitted 	= LU_FALSE;
	switchFsm.errorCode 			= 0;
	switchFsm.vMotorError 			= LU_FALSE;
	switchFsm.relayError 			= LU_FALSE;

	/* Init FSM context */
	DualSwitchContext_Init(&switchFsm._fsm, &switchFsm);

	/* Enable state machine debug */
	setDebugFlag(&switchFsm._fsm, 1);

	/* Set default configuration parameters for switch channels */

	//*SwitchConfigurationPtr

	for (swChan = 0; swChan < MAX_SWITCH_CHANNELS; swChan++)
	{
		switchFsm.SwitchConfiguration[swChan].enable  			= LU_FALSE;
		switchFsm.SwitchConfiguration[swChan].inhibitBI  		= INHIBIT_BI;      /* Bit mask of channel numbers */
		switchFsm.SwitchConfiguration[swChan].pulseLengthMS  	= PULSELENGTH_MS;
		switchFsm.SwitchConfiguration[swChan].overrunMS			= 0;
		switchFsm.SwitchConfiguration[swChan].opTimeoutS		= OPERATION_TIMEOUT_S;
	}

	/* Initialise the active channel */
	switchFsm.switchStatus.activeChannel	= MAX_SWITCH_CHANNELS;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchSelectHandler(DualSwitchSelectStr *switchSelect)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 				= switchSelect->channel;
	switchFsm.swEventParams.local 					= switchSelect->local;
	switchFsm.swEventParams.timeout 				= switchSelect->SelectTimeout;
	switchFsm.swEventParams.selectReceiveTime		= STGetTime();

	reply.channel 			= switchSelect->channel;
	reply.status  			= REPLY_STATUS_OKAY;

	if(switchSelect->channel < MAX_SWITCH_CHANNELS)
	{
		/* Call function to send open select event to state machine */
		if( switchFsm.swEventParams.local )
		{
			DualSwitchContext_SelectLocalEvent(&switchFsm._fsm);
			reply.status = switchFsm.swEventParams.status;
		}
		else
		{
			DualSwitchContext_SelectRemoteEvent(&switchFsm._fsm);
			reply.status = switchFsm.swEventParams.status;
		}
	}
	else
	{
		reply.status  	= REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchOperateHandler(DualSwitchOperateStr *SwitchOperate)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 					= SwitchOperate->channel;
	switchFsm.swEventParams.local 						= SwitchOperate->local;
	switchFsm.swEventParams.timeout 					= SwitchOperate->SwitchDelay;
	switchFsm.swEventParams.preOpTime 					= SwitchOperate->SwitchPreOpDelay;
	switchFsm.swEventParams.operateCmdreceiveTime		= STGetTime();

	reply.channel 	= SwitchOperate->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	if(SwitchOperate->channel < MAX_SWITCH_CHANNELS)
	{
		/* Call function to send open operate event to state machine */
		DualSwitchContext_OperateEvent(&switchFsm._fsm);
		reply.status = switchFsm.swEventParams.status;
	}
	else
	{
		reply.status  	= REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchCancelHandler(DualSwitchCancelStr *switchCancel)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	switchFsm.swEventParams.channel 				= switchCancel->channel;
	switchFsm.swEventParams.local 					= switchCancel->local;
	switchFsm.swEventParams.timeout 				= 0;
	switchFsm.swEventParams.cancelReceiveTime		= STGetTime();

	reply.channel 			= switchCancel->channel;
	reply.status  			= REPLY_STATUS_OKAY;

	/*
	 * Get channel from message
	 */
	reply.channel 	= switchCancel->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	if(switchCancel->channel < MAX_SWITCH_CHANNELS)
	{
		/* Call function to send open cancel event to state machine */
		if( switchCancel->local )
			DualSwitchContext_CancelLocalEvent(&switchFsm._fsm);
		else
			DualSwitchContext_CancelRemoteEvent(&switchFsm._fsm);
		reply.status  	= switchFsm.swEventParams.status;
	}
	else
	{
		reply.status  	= REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_CANCEL_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchSynchHandler(DualSwitchSynchStr *switchSynch)
{
	ModReplyStr reply;
	/*
	 * Get channel from message
	 */
//	switchFsm.swEventParams.channel 		= switchSynch->channel;
//	switchFsm.swEventParams.local 			= 0;
//	switchFsm.swEventParams.timeout 		= 0;
//	switchFsm.swEventParams.receiveTime		= STGetTime();

	reply.channel 	= switchSynch->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_SWC_CH_SYNCH_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchConfig(DualSwitchConfigStr *switchConfigPtr)
{
	ModReplyStr reply;
	SB_ERROR retError = SB_ERROR_NONE;
	DualSwitchConfigStr *SwitchConfigurationPtr;

	reply.channel 	= switchConfigPtr->channel;

	/* Set the pointer to the channel configuration structure from the channel number */
	SwitchConfigurationPtr = &switchFsm.SwitchConfiguration[switchConfigPtr->channel];

	/* Fill the channel configuration structure from the message body */
	SwitchConfigurationPtr->channel             = switchConfigPtr->channel;
	SwitchConfigurationPtr->enable  		    = switchConfigPtr->enable;
	SwitchConfigurationPtr->inhibitBI  		    = switchConfigPtr->inhibitBI;
	SwitchConfigurationPtr->pulseLengthMS  	    = switchConfigPtr->pulseLengthMS;
	SwitchConfigurationPtr->overrunMS  		    = switchConfigPtr->overrunMS;
	SwitchConfigurationPtr->opTimeoutS  	    = switchConfigPtr->opTimeoutS;
	SwitchConfigurationPtr->motorOverDriveMS    = switchConfigPtr->motorOverDriveMS;
	SwitchConfigurationPtr->motorReverseDriveMS = switchConfigPtr->motorReverseDriveMS;

	if(retError == SB_ERROR_NONE)
	{
		reply.status  	= REPLY_STATUS_OKAY;
	}
	else
	{
		reply.status  	= REPLY_STATUS_IOMANAGER_ERROR;
	}

	/* Generate event for state machine */
	DualSwitchContext_ConfigEvent(&switchFsm._fsm);

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
					    MODULE_MSG_ID_CFG_DUAL_SWITCH_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchMotorSupplySelectHandler(SupplySelectStr *supplySelect)
{
	ModReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 	= supplySelect->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(supplySelect->channel)
	{
	case DSM_CH_SWOUT_OPEN_SWITCH_A:
	case DSM_CH_SWOUT_CLOSE_SWITCH_A:
		motorSelected[DSM_MOTOR_SUPPLY_SWITCH_A] = LU_TRUE;
		break;

	case DSM_CH_SWOUT_OPEN_SWITCH_B:
	case DSM_CH_SWOUT_CLOSE_SWITCH_B:
		motorSelected[DSM_MOTOR_SUPPLY_SWITCH_B] = LU_TRUE;
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_PSC_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************/
lu_uint8_t DualSwitchMotorSUpplyOnOff(DualSwitchMotorNumberEnm switchMotorNum,lu_uint8_t MotorCmd )
{
	lu_int32_t ioId;
	SB_ERROR RetVal;


	if(switchMotorNum == DSM_MOTOR_SUPPLY_SWITCH_A)
	{
		ioId = switchFsm.switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_A;
		/* Motor Suply ON*/
		if(MotorCmd == LU_TRUE)
		RetVal = IOManagerSetValue(ioId, 0);
		else
		RetVal = IOManagerSetValue(ioId, 1);

		ioId = switchFsm.switchStatus.swConInitParams.ioIdRelayCtrlVMotor_A_B;
		/* Motor Suply ON*/
		if(MotorCmd == LU_TRUE)
		RetVal = IOManagerSetValue(ioId, 1);
		else
		RetVal = IOManagerSetValue(ioId, 0);

		/*LED*/
		ioId = switchFsm.switchStatus.swConInitParams.ioIdLedFP_SW_A_MotorSupply;
		if(MotorCmd == LU_TRUE)
		RetVal = IOManagerSetValue(ioId, 1);
		else
		RetVal = IOManagerSetValue(ioId, 0);

	}else if (switchMotorNum == DSM_MOTOR_SUPPLY_SWITCH_B)
	{
		ioId = switchFsm.switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_A;
		/* Motor Suply ON*/
		if(MotorCmd == LU_TRUE)
		RetVal = IOManagerSetValue(ioId, 0);
		else
		RetVal = IOManagerSetValue(ioId, 1);

		ioId = switchFsm.switchStatus.swConInitParams.ioIdRelayCtrlVMotor_B_B;
		/* Motor Suply ON*/
		if(MotorCmd == LU_TRUE)
		RetVal = IOManagerSetValue(ioId, 1);
		else
		RetVal = IOManagerSetValue(ioId, 0);

		/*LED*/
		ioId = switchFsm.switchStatus.swConInitParams.ioIdLedFP_SW_B_MotorSupply;
		/* Motor Suply ON*/
		if(MotorCmd == LU_TRUE)
		RetVal = IOManagerSetValue(ioId, 1);
		else
		RetVal = IOManagerSetValue(ioId, 0);
	}else
	{
		RetVal = REPLY_STATUS_ERROR;
	}

	motorSelected[switchMotorNum] = 0;
		return RetVal;
}
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchMotorSupplyOperateHandler(SupplyOperateStr *supplyOperate)
{
	ModReplyStr reply;
	/*
	 * Get channel from message
	 */
	reply.channel 	= supplyOperate->channel;
	reply.status  	= REPLY_STATUS_OKAY;

	switch(supplyOperate->channel)
	{
	case DSM_CH_SWOUT_OPEN_SWITCH_A:
	case DSM_CH_SWOUT_CLOSE_SWITCH_A:
		if(motorSelected[DSM_MOTOR_SUPPLY_SWITCH_A] == LU_TRUE)
		{
			reply.status = DualSwitchMotorSUpplyOnOff(DSM_MOTOR_SUPPLY_SWITCH_A, LU_TRUE);
			DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_SWITCH_A] = LU_TRUE;
		}
	break;

	case DSM_CH_SWOUT_OPEN_SWITCH_B:
	case DSM_CH_SWOUT_CLOSE_SWITCH_B:
		if(motorSelected[DSM_MOTOR_SUPPLY_SWITCH_B] == LU_TRUE)
		{
			reply.status = DualSwitchMotorSUpplyOnOff(DSM_MOTOR_SUPPLY_SWITCH_B, LU_TRUE);
			DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_SWITCH_B] = LU_TRUE;
		}
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_PSC_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DualSwitchMotorSupplyCancelHandler(SupplyCancelStr *supplyCancel)
{
	SupplyCancelReplyStr reply;

	/*
	 * Get channel from message
	 */
	reply.channel 			= supplyCancel->channel;
	reply.status  			= REPLY_STATUS_OKAY;
	reply.peakCurrent		= 0;
	reply.operationID		= 0;

	switch(supplyCancel->channel)
	{
	case DSM_CH_SWOUT_OPEN_SWITCH_A:
	case DSM_CH_SWOUT_CLOSE_SWITCH_A:
		reply.status = DualSwitchMotorSUpplyOnOff(DSM_MOTOR_SUPPLY_SWITCH_A, LU_FALSE);
		DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_SWITCH_A] = LU_FALSE;
		break;

	case DSM_CH_SWOUT_OPEN_SWITCH_B:
	case DSM_CH_SWOUT_CLOSE_SWITCH_B:
		reply.status = DualSwitchMotorSUpplyOnOff(DSM_MOTOR_SUPPLY_SWITCH_B, LU_FALSE);
		DualSwitchMotorSupplyControllLogicCmdEn[DSM_MOTOR_SUPPLY_SWITCH_B] = LU_FALSE;
		break;

	default:
		reply.status  	= REPLY_STATUS_ERROR;
		break;
	}
	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_PSC_CH_CANCEL_R,
						sizeof(SupplyCancelReplyStr),
						(lu_uint8_t *)&reply
					  );
}


/*
 *********************** End of file ******************************************
 */
