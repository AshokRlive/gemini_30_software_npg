/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Switch FSM header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _DUALSWITCHFSM_INCLUDED
#define _DUALSWITCHFSM_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "debug_frmwrk.h"

/* Define tracing to use printf to serial port */
#define TRACE		_printf

#include "lu_types.h"
#include "DualSwitch_sm.h"
#include "DualSwitchCommon.h"

#include "ModuleProtocol.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define DELAYED_ACTION_PULSE_MS		250  /* Flash rate for delayed action */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

struct  DualSwitch
{
	SwEventParamsStr 		 swEventParams;
	SwitchUserContextStr 	 switchStatus;
	DualSwitchConfigStr      SwitchConfiguration[MAX_SWITCH_CHANNELS]; /* Configuration for switch Open/Close channels on */

	lu_uint16_t              errorCode;
	lu_bool_t 				 operationCommitted;
	lu_uint32_t				 overrunStartTime;
	lu_bool_t 	             vMotorError;
	lu_bool_t                relayError;

	struct DualSwitchContext 	_fsm;
};

struct SwConBinInputs
{
	/* Relay FB Inputs */
	lu_int32_t ioIdRelayFb_SW_A_OpenUpDnClosed;
	lu_int32_t ioIdRelayFb_SW_A_OpenLkClosed;
	lu_int32_t ioIdRelayFb_SW_A_CloseUpDnClosed;
	lu_int32_t ioIdRelayFb_SW_A_CloseLkClosed;

	lu_int32_t ioIdRelayFb_SW_B_OpenUpDnClosed;
	lu_int32_t ioIdRelayFb_SW_B_OpenLkClosed;
	lu_int32_t ioIdRelayFb_SW_B_CloseUpDnClosed;
	lu_int32_t ioIdRelayFb_SW_B_CloseLkClosed;

	lu_int32_t ioIdVMotor_SW_A_FbOn;
	lu_int32_t ioIdVMotor_SW_B_FbOn;

	/* Binary Inputs */
	lu_int32_t ioId_SW_A_OpenedInput;
	lu_int32_t ioId_SW_A_ClosedInput;

	/* Binary Inputs */
	lu_int32_t ioId_SW_B_OpenedInput;
	lu_int32_t ioId_SW_B_ClosedInput;

	ioIdSwIndicationStr ioId_SW_Indication;
};

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/* Guard functions */
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectLocal(struct DualSwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectRemote(struct DualSwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperateLocal(struct DualSwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperateRemote(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t CancelLocal(struct DualSwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t CancelRemote(struct DualSwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectTimeoutLocal(struct DualSwitchContext *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t SelectTimeout(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t DelayTimeout(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t PreOpDelayTimeout(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperationTimeout(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OverrunTimeout(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t IsCheckPos(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t OperationComplete(struct DualSwitchContext *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t ErrorCode(struct DualSwitchContext *fsm);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_bool_t  CheckRelayError(struct DualSwitchContext *fsm);




/********************************************************************************************
 *                              Action functions
 ********************************************************************************************/
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_OperationCommittedAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_OperationUnCommittedAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_ResetAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_InitAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_LocalSelectedAction(struct DualSwitch *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_RemoteSelectedAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_SelectTimedOutAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationTimeoutAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_CancelledAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_PowerMotorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_StartMotorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_DelayedOperationAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_OperationCompleteAction(struct DualSwitch *fsm);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void DualSwitch_OperationOverrunAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_SelectErrorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_OperateErrorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_CancelErrorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void DualSwitch_OperatingFeedbackAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_ErrorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_RelayErrorAction(struct DualSwitch *fsm);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_ConfigErrorAction(struct DualSwitch *fsm);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void DualSwitch_CompleteErrorAction(struct DualSwitch *fsm);


#endif /* _SWITCHFSM_INCLUDED */

/*
 *********************** End of file ******************************************
 */
