cmake_minimum_required(VERSION 2.8)

add_library( I2CHumiditySensor STATIC
             I2CHumiditySensor.c
           ) 

ADD_DEPENDENCIES(I2CHumiditySensor globalHeaders)

# Global include
include_directories (../../../../../../common/include/)

# Generated header include
include_directories(${CMAKE_G3_GENERATED_HEADER_INCLUDE}/)

# Slave Boards Global include
include_directories (../../../include/)

# System Utils
include_directories (../../systemUtils/include/)

# NXP drivers
include_directories (SYSTEM ../../../../thirdParty/LPC1700CMSIS/Core/CM3/DeviceSupport/NXP/LPC17xx/)
include_directories (SYSTEM ../../../../thirdParty/LPC1700CMSIS/Core/CM3/CoreSupport/)
include_directories (SYSTEM ../../../../thirdParty/LPC1700CMSIS/Drivers/include/)

# Linear Interpolation library include
include_directories (../../../../../../common/library/LinearInterpolation/include/)
include_directories (../../../../../../common/library/crc16/include/)

# Slave library include
include_directories (../../CANProtocolCodec/include/)
include_directories (../../CANProtocolFraming/include/)
include_directories (../../NVRam/include/)

# Other includes
include_directories (../../IOManager/include/)

# Module local include
include_directories (../include/)

# Slave Board specific includes
include_directories (${SLAVEBOARD_PATH}/IOManager/include/)

# Generate Doxygen documentation
gen_doxygen("I2CHumiditySensor" "")
