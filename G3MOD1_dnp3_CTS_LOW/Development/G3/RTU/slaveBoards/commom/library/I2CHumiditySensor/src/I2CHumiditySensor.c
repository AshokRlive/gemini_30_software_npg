/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       I2C Humidity Sensor module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "lu_types.h"

#include "I2CHumiditySensor.h"

#include "IOManager.h"

#include "crc16.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define AM2315_I2CADDR		0x5c
#define AM2315_READREG		0x03

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR readTempHumidity(lu_int32_t *tempPtr, lu_int32_t *humidPtr, I2CHumidityParmStr *paramsPtr);
SB_ERROR wakeupTempHumidity(I2CHumidityParmStr *paramsPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

I2CHumidityParmStr *i2cHumidityParmPtr;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR i2cHumiditySensorInit(I2CHumidityParmStr *paramsPtr)
{
	PINSEL_CFG_Type 	    pinCfg;

	i2cHumidityParmPtr = paramsPtr;

	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = PINSEL_PINMODE_NORMAL;

	/* SCL */
	pinCfg.Portnum     = paramsPtr->sclPort;
	pinCfg.Pinnum      = paramsPtr->sclPin;
	pinCfg.Funcnum     = paramsPtr->sclFunc;

	PINSEL_ConfigPin(&pinCfg);

	/* SDA */
	pinCfg.Portnum     = paramsPtr->sdaPort;
	pinCfg.Pinnum      = paramsPtr->sdaPin;
	pinCfg.Funcnum     = paramsPtr->sdaFunc;

	PINSEL_ConfigPin(&pinCfg);

	/* Init I2C */
	I2C_Init((LPC_I2C_TypeDef *)paramsPtr->i2cChan, 100000);

	/* Enable I2C operation */
	I2C_Cmd((LPC_I2C_TypeDef *)paramsPtr->i2cChan, ENABLE);

	wakeupTempHumidity(i2cHumidityParmPtr);

	return SB_ERROR_NONE;
}


SB_ERROR i2cHumiditySensorUpdate(void)
{
	lu_int32_t temperature;
	lu_int32_t humidity;
	static lu_int16_t offlineCount;
	static lu_int16_t offlinePeakCount = 0;

	if (readTempHumidity(&temperature, &humidity, i2cHumidityParmPtr) == SB_ERROR_NONE)
	{
		offlineCount = 0;

		IOManagerSetOnline(i2cHumidityParmPtr->tempIoID, LU_TRUE);
		IOManagerSetOnline(i2cHumidityParmPtr->HumidityIoIO, LU_TRUE);

		IOManagerSetValue(i2cHumidityParmPtr->tempIoID, temperature);
		IOManagerSetValue(i2cHumidityParmPtr->HumidityIoIO, humidity);
	}
	else
	{
		if (offlineCount > 16)
		{
			IOManagerSetOnline(i2cHumidityParmPtr->tempIoID, LU_FALSE);
			IOManagerSetOnline(i2cHumidityParmPtr->HumidityIoIO, LU_FALSE);

			/* Wakeup the sensor */
			wakeupTempHumidity(i2cHumidityParmPtr);
		}
		else
		{
			offlineCount++;
			if (offlineCount > offlinePeakCount)
			{
				offlinePeakCount = offlineCount;
			}
		}
	}

	return SB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief
 *
 *
 *
 *   \param
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR wakeupTempHumidity(I2CHumidityParmStr *paramsPtr)
{
	SB_ERROR            retError;
	lu_uint8_t 			txData[8];
	lu_uint8_t 			rxData[8];
	I2C_M_SETUP_Type	txferCfg;

	retError = SB_ERROR_NONE;

	txferCfg.sl_addr7bit     	    = AM2315_I2CADDR;
	txferCfg.tx_data		  		= txData;
	txferCfg.tx_length       		= 0;
	txferCfg.rx_data		 	 	= rxData;
	txferCfg.rx_length       		= 0;
	txferCfg.retransmissions_max 	= 0;

	if (I2C_MasterTransferData((LPC_I2C_TypeDef *)paramsPtr->i2cChan, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	{
		retError = SB_ERROR_I2C_FAIL;
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief
 *
 *
 *
 *   \param
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR readTempHumidity(lu_int32_t *tempPtr, lu_int32_t *humidPtr, I2CHumidityParmStr *paramsPtr)
{
	SB_ERROR            retError;
	lu_int32_t 			temperature;
	lu_int32_t 			humiditity;
	lu_uint8_t 			txData[8];
	lu_uint8_t 			rxData[8];
	lu_uint16_t         crc16Inp;
	lu_uint16_t         crc16Check;
	I2C_M_SETUP_Type	txferCfg;

	retError = SB_ERROR_NONE;

	txferCfg.sl_addr7bit     	    = AM2315_I2CADDR;
	txferCfg.tx_data		  		= txData;
	txferCfg.tx_length       		= 3;
	txferCfg.rx_data		 	 	= rxData;
	txferCfg.rx_length       		= 8;
	txferCfg.retransmissions_max 	= 0;

	txData[0] = AM2315_READREG;
	txData[1] = 0x0;
	txData[2] = 4;

	if (I2C_MasterTransferDataNew((LPC_I2C_TypeDef *)paramsPtr->i2cChan, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	{
		retError = SB_ERROR_I2C_FAIL;
	}
	else
	{
		if (rxData[0] != AM2315_READREG || rxData[1] != 4)
		{
			retError = SB_ERROR_I2C_FAIL;
		}
		else
		{
			crc16Check = crc16_modbus_calc16(&rxData[0], 6);

			humiditity  = rxData[2];
			humiditity *= 256;
			humiditity += rxData[3];
			humiditity /= 10;

			temperature  = rxData[4] & 0x7f;
			temperature *= 256;
			temperature += rxData[5];

			if (rxData[4] >> 7)
			{
				temperature = - temperature;
			}

			crc16Inp = *(lu_uint16_t *)&rxData[6];

			if(crc16Inp == crc16Check)
			{
				*humidPtr = humiditity;
				*tempPtr  = temperature;
			}
			else
			{
				retError = SB_ERROR_I2C_CRC_FAIL;
			}
		}
	}

	return retError;
}

/*
 *********************** End of file ******************************************
 */
