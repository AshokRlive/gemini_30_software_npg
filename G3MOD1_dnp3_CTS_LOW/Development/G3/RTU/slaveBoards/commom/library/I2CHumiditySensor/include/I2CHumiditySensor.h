/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       I2C Humidity Sensor module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/12      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _I2CHUMIDITYSENSOR_INCLUDED
#define _I2CHUMIDITYSENSOR_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include <string.h>
#include "lu_types.h"

#include "errorCodes.h"
#include "CANProtocolCodec.h"

#pragma pack(1)

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define I2CHUMIDITY_UPDATE_TICK_MS	500

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct I2CHumidityParmDef
{
	lu_uint16_t sclPort;
	lu_uint16_t sclPin;
	lu_uint8_t  sclFunc;

	lu_uint16_t sdaPort;
	lu_uint16_t sdaPin;
	lu_uint8_t  sdaFunc;

	lu_uint32_t i2cChan;

	lu_uint16_t tempIoID;
	lu_uint16_t HumidityIoIO;
} I2CHumidityParmStr;

#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief
 *
 *
 *
 *   \param void
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR i2cHumiditySensorInit(I2CHumidityParmStr *paramsPtr);

/*!
 ******************************************************************************
 *   \brief
 *
 *
 *
 *   \param void
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR i2cHumiditySensorUpdate(void);

#endif /* _I2CHUMIDITYSENSOR_INCLUDED */

/*
 *********************** End of file ******************************************
 */
