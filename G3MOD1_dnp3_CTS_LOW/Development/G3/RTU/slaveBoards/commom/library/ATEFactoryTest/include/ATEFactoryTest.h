/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       ATE Factory Test header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _ATEFACTORYTEST_INCLUDED
#define _ATEFACTORYTEST_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define ATE_BAUD_RATE				115200

#define ATE_MAX_CMD_LEN				100

#define ATE_MAX_PORTS				5

/* Start of command */
#define ATE_CMD_START_CHR			'>'

#define ATE_CMD_HELP				"??"

#define ATE_CMD_SYNC				"S?"
#define ATE_CMD_GET_ALL_INPUTS		"I?"
#define ATE_CMD_GET_ALL_DDR  		"D?"
#define ATE_CMD_SET_ALL_DDR  		"OD"
#define ATE_CMD_AND_ALL_OUTPUTS	    "OA"
#define ATE_CMD_OR_ALL_OUTPUTS	    "OO"

/* Command response */
#define ATE_CMD_RSP_OK				"OK"
#define ATE_CMD_RSP_ERROR			"ER"
#define ATE_CMD_RSP_ERROR_PARAM		"E?"
#define ATE_CMD_RSP_GET_ALL_INPUTS	"I="
#define ATE_CMD_RSP_GET_ALL_DDR   	"D="

/* Terminate command */
#define ATE_CMD_END					"\r\n"
#define ATE_CMD_END_CHR				0x0d

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	ATE_TEST_CMD_HELP            = 0,
	ATE_TEST_CMD_SYNC               ,
	ATE_TEST_CMD_GET_ALL_INPUTS,
	ATE_TEST_CMD_GET_ALL_DDR,
	ATE_TEST_CMD_SET_ALL_DDR,
	ATE_TEST_CMD_AND_ALL_OUTPUTS,
	ATE_TEST_CMD_OR_ALL_OUTPUTS,

	ATE_TEST_CMD_LAST
}ATE_TEST_CMD;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR ATEFactoryTestInit(void);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param none
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR ATEFactoryTestTick(void);

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param none
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern void BoardATEFactoryTestInit(void);

#endif /* _ATEFACTORYTEST_INCLUDED */

/*
 *********************** End of file ******************************************
 */
