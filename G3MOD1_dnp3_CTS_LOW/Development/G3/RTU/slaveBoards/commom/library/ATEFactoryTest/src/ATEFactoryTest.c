/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       ATE Factory Test module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "ctype.h"
#include "stdlib.h"
#include "string.h"

#include "lu_types.h"

#include "ATEFactoryTest.h"

#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "svnRevision.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void configureATEPins(void);
SB_ERROR sendATEUartString(lu_int8_t *stringPtr, lu_uint32_t *lenPtr);
SB_ERROR decodeATECommand(ATE_TEST_CMD ateCmd, lu_int8_t *params);

void cmdGetAllInputs(void);
void cmdGetAllDdr(void);
lu_bool_t isNotATEUart(lu_uint8_t port, lu_uint8_t pin);
void cmdSetAllDDR(lu_uint32_t ddrValue[]);
void cmdAndOrAllOutput(lu_uint32_t portTable[], lu_bool_t isAnd);

lu_uint8_t hexToNibbleBin(lu_int8_t chr);
lu_uint32_t hex8ToBin32(lu_int8_t *strPtr);
lu_int8_t *bin32ToHex8(lu_uint32_t bin32);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

lu_uint8_t hexTable[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* Have all IO pins DDR been initialised ? */
lu_bool_t                initialisedATE = LU_FALSE;
LPC_UART_TypeDef         *ateUart       = LPC_UART3;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ATEFactoryTestInit(void)
{
	SB_ERROR          retVal;
	UART_CFG_Type     uartCfg;
	PINSEL_CFG_Type   pinCfg;
	lu_uint32_t       len;

	retVal = SB_ERROR_NONE;

	/* Init serial port */
	UART_ConfigStructInit(&uartCfg);
	uartCfg.Baud_rate = ATE_BAUD_RATE;

	/* Configure pins */
	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;

	switch ((lu_uint32_t)ateUart)
	{
	case (lu_uint32_t)LPC_UART3:
		/* RX */
		pinCfg.Portnum     = PINSEL_PORT_4;
		pinCfg.Pinnum      = PINSEL_PIN_29;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* TX */
		pinCfg.Portnum     = PINSEL_PORT_4;
		pinCfg.Pinnum      = PINSEL_PIN_28;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);
		break;

	default:
		retVal = SB_ERROR_INITIALIZED;
		break;
	}

	if (retVal == SB_ERROR_NONE)
	{
		/* Initialise the UART */
		UART_Init(ateUart, &uartCfg);

		UART_TxCmd(ateUart, ENABLE);

		sendATEUartString(ATE_CMD_END, &len);
		sendATEUartString("ATEFactoryTest - svn:0x", &len);
		sendATEUartString(bin32ToHex8(_SVN_REVISION), &len);
		sendATEUartString(ATE_CMD_END, &len);
	}

	return retVal;
}


 SB_ERROR ATEFactoryTestTick(void)
 {
	 lu_uint32_t        rxLen;
	 lu_int32_t         len;
	 lu_int8_t          rxBuffer[10];
	 lu_int8_t          cmd[20];
	 ATE_TEST_CMD       ateCmd;
	 static lu_int8_t   lineBuffer[ATE_MAX_CMD_LEN + 2];
	 static lu_uint32_t lineIdx = 0;

	 /* Poll UART rx */
	 rxLen = UART_Receive(ateUart, (lu_uint8_t *)&rxBuffer[0], 1, NONE_BLOCKING);
	 if (rxLen)
	 {
		 if(rxBuffer[0] == ATE_CMD_START_CHR)
		 {
			lineIdx = 0;
		 }
		 else
		 {
			 /* Copy buffer */
			 lineBuffer[lineIdx++] = rxBuffer[0];

			 if(rxBuffer[0] == ATE_CMD_END_CHR)
			 {
				 /* Terminate string */
				 lineBuffer[--lineIdx] = '\0';

				 len = strlen(&lineBuffer[0]);

				 /* Parse for command */
				 if(len >= 2)
				 {
					 cmd[0] = lineBuffer[0];
					 cmd[1] = lineBuffer[1];
					 cmd[2] = '\0';

					 ateCmd = ATE_TEST_CMD_LAST;

					 if(!strcmp(cmd, ATE_CMD_HELP))
					 {
						 ateCmd = ATE_TEST_CMD_HELP;
					 }
					 else if(!strcmp(cmd, ATE_CMD_SYNC))
					 {
						 ateCmd = ATE_TEST_CMD_SYNC;
					 }
					 else if(!strcmp(cmd, ATE_CMD_GET_ALL_INPUTS))
					 {
						 ateCmd = ATE_TEST_CMD_GET_ALL_INPUTS;
					 }
					 else if(!strcmp(cmd, ATE_CMD_SET_ALL_DDR))
					 {
						 ateCmd = ATE_TEST_CMD_SET_ALL_DDR;
					 }
					 else if(!strcmp(cmd, ATE_CMD_AND_ALL_OUTPUTS))
					 {
						 ateCmd = ATE_TEST_CMD_AND_ALL_OUTPUTS;
					 }
					 else if(!strcmp(cmd, ATE_CMD_OR_ALL_OUTPUTS))
					 {
						 ateCmd = ATE_TEST_CMD_OR_ALL_OUTPUTS;
					 }
					 else if(!strcmp(cmd, ATE_CMD_GET_ALL_DDR))
					 {
						 ateCmd = ATE_TEST_CMD_GET_ALL_DDR;
					 }

					 /* Action command */
					 decodeATECommand(ateCmd, &lineBuffer[2]);

				 }
				 /* Reset for next command */
				 lineIdx = 0;
			 }

			 /* Check for line overflow */
			 if (lineIdx >= ATE_MAX_CMD_LEN)
			 {
				 lineIdx = 0;
			 }
		 }
	 }

	 return SB_ERROR_NONE;
 }

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

void configureATEPins(void)
{
	lu_uint32_t         port;
	PINSEL_CFG_Type 	pinCfg;
	lu_uint8_t 		    pin;
	lu_uint32_t	        pinShift;

	pinCfg.Funcnum = 0;

	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

	/* Configure all pins as inputs */
	for(port = 0; port < (PINSEL_PORT_4 + 1); port++)
	{
		pinCfg.Portnum = port;
		for(pin = 0; pin < 32; pin++)
		{
			pinCfg.Pinnum = pin;

			pinShift = 1;
			pinShift = pinShift << (pin & 0x1f);

			/* Only allow DDR change for port/pin not is not UART */
			if(isNotATEUart(port, pin))
			{
				PINSEL_ConfigPin(&pinCfg);
				GPIO_SetDir(port, pinShift, 0);
			}
		}
	}
}

SB_ERROR sendATEUartString(lu_int8_t *stringPtr, lu_uint32_t *lenPtr)
{
	lu_uint32_t sendLen;
	SB_ERROR    retVal;

	retVal = SB_ERROR_NONE;

	sendLen = strlen(stringPtr);
	if (sendLen > 0)
	{
		*lenPtr = UART_Send(ateUart, (lu_uint8_t *)stringPtr, sendLen, BLOCKING);
	}
	else
	{
		*lenPtr = 0;
	}

	return retVal;
}

SB_ERROR decodeATECommand(ATE_TEST_CMD ateCmd, lu_int8_t *params)
{
	lu_uint32_t len;
	lu_uint32_t paramLen;
	lu_uint32_t idx;
	lu_int8_t   hexBuff[10];
	lu_uint32_t portValues[ATE_MAX_PORTS];

	/* Enter ATE test mode */
	if(ateCmd != ATE_TEST_CMD_LAST)
	{
		if (!initialisedATE)
		{
			/* Board specific */
			BoardATEFactoryTestInit();

			/* Configure all pins as input */
			configureATEPins();

			initialisedATE = LU_TRUE;
		}
	}

	paramLen = strlen(params);

	switch(ateCmd)
	{
	case ATE_TEST_CMD_HELP:
		sendATEUartString(ATE_CMD_RSP_OK, &len);
		sendATEUartString(ATE_CMD_END, &len);
		break;

	case ATE_TEST_CMD_SYNC:
		sendATEUartString(ATE_CMD_RSP_OK, &len);
		sendATEUartString(ATE_CMD_END, &len);
		break;

	case ATE_TEST_CMD_GET_ALL_INPUTS:
		sendATEUartString(ATE_CMD_RSP_GET_ALL_INPUTS, &len);
		cmdGetAllInputs();
		sendATEUartString(ATE_CMD_END, &len);
		break;

	case ATE_TEST_CMD_GET_ALL_DDR:
		sendATEUartString(ATE_CMD_RSP_GET_ALL_DDR, &len);
		cmdGetAllDdr();
		sendATEUartString(ATE_CMD_END, &len);
		break;

	case ATE_TEST_CMD_SET_ALL_DDR:
		if(paramLen == (8 * ATE_MAX_PORTS ))
		{
			sendATEUartString(ATE_CMD_RSP_OK, &len);

			/* Convert each 32bit port value to binary */
			for(idx = 0; idx < ATE_MAX_PORTS; idx++)
			{
				strncpy(hexBuff, &params[idx * 8], 8);
				hexBuff[8] = '\0';

				portValues[idx] = hex8ToBin32(hexBuff);
			}

			cmdSetAllDDR(portValues);
		}
		else
		{
			/* Parameter error */
			sendATEUartString(ATE_CMD_RSP_ERROR_PARAM, &len);
		}

		sendATEUartString(ATE_CMD_END, &len);
		break;

	case ATE_TEST_CMD_AND_ALL_OUTPUTS:
		if(paramLen == (8 * ATE_MAX_PORTS ))
		{
			sendATEUartString(ATE_CMD_RSP_OK, &len);

			/* Convert each 32bit port value to binary */
			for(idx = 0; idx < ATE_MAX_PORTS; idx++)
			{
				strncpy(hexBuff, &params[idx * 8], 8);
				hexBuff[8] = '\0';

				portValues[idx] = hex8ToBin32(hexBuff);
			}

			cmdAndOrAllOutput(portValues, LU_TRUE);
		}
		else
		{
			/* Parameter error */
			sendATEUartString(ATE_CMD_RSP_ERROR_PARAM, &len);
		}

		sendATEUartString(ATE_CMD_END, &len);
		break;

	case ATE_TEST_CMD_OR_ALL_OUTPUTS:
		if(paramLen == (8 * ATE_MAX_PORTS ))
		{
			sendATEUartString(ATE_CMD_RSP_OK, &len);

			/* Convert each 32bit port value to binary */
			for(idx = 0; idx < ATE_MAX_PORTS; idx++)
			{
				strncpy(hexBuff, &params[idx * 8], 8);
				hexBuff[8] = '\0';

				portValues[idx] = hex8ToBin32(hexBuff);
			}

			cmdAndOrAllOutput(portValues, LU_FALSE);
		}
		else
		{
			/* Parameter error */
			sendATEUartString(ATE_CMD_RSP_ERROR_PARAM, &len);
		}

		sendATEUartString(ATE_CMD_END, &len);
		break;

	default:
		/* Unknown command */
		sendATEUartString(ATE_CMD_RSP_ERROR, &len);
		sendATEUartString(ATE_CMD_END, &len);
		break;
	}

	return SB_ERROR_NONE;
}

void cmdGetAllInputs(void)
{
	lu_uint32_t portIdx;
	lu_uint32_t portValue;
	lu_uint32_t len;
	lu_int8_t   *hexStr;

	for(portIdx = 0; portIdx < (PINSEL_PORT_4 + 1); portIdx++)
	{
		portValue = GPIO_ReadValue(portIdx);
		hexStr = bin32ToHex8(portValue);
		len = strlen(hexStr);
		sendATEUartString(hexStr, &len);
	}
}

void cmdGetAllDdr(void)
{
	lu_uint32_t portIdx;
	lu_uint32_t portDdrValue;
	lu_uint32_t len;
	lu_int8_t   *hexStr;

	for(portIdx = 0; portIdx < (PINSEL_PORT_4 + 1); portIdx++)
	{
		portDdrValue = GPIO_GetDir(portIdx);
		hexStr = bin32ToHex8(portDdrValue);
		len = strlen(hexStr);
		sendATEUartString(hexStr, &len);
	}
}

lu_bool_t isNotATEUart(lu_uint8_t port, lu_uint8_t pin)
{
	if (port == PINSEL_PORT_4 &&
		(pin == PINSEL_PIN_28 || pin == PINSEL_PIN_29)
	   )
	{
		return LU_FALSE;
	}
	else
	{
		return LU_TRUE;
	}
}

void cmdSetAllDDR(lu_uint32_t ddrTable[])
{
	lu_uint32_t         port;
	PINSEL_CFG_Type 	pinCfg;
	lu_uint8_t			pinDir;
	lu_uint8_t 		    pin;
	lu_uint32_t	        pinShift;

	pinCfg.Funcnum = 0;

	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

	for(port = 0; port < (PINSEL_PORT_4 + 1); port++)
	{
		pinCfg.Portnum = port;
		for(pin = 0; pin < 32; pin++)
		{
			pinCfg.Pinnum = pin;

			pinShift = 1;
			pinShift = pinShift << (pin & 0x1f);

			/* Only allow DDR change for port/pin not is board mask */
			if(isNotATEUart(port, pin))
			{
				pinDir = ((ddrTable[port] >> pin) & 0x1);

				GPIO_SetDir(port, pinShift, pinDir);
			}
		}
	}
}

void cmdAndOrAllOutput(lu_uint32_t portTable[], lu_bool_t isAnd)
{
	lu_uint32_t         port;
	lu_uint32_t         portVal;

	for(port = 0; port < (PINSEL_PORT_4 + 1); port++)
	{
		portVal  = GPIO_ReadValue(port);

		if(isAnd == LU_TRUE)
		{
			portVal &=  portTable[port];

		}
		else
		{
			portVal |= portTable[port];
		}

		GPIO_SetValue(port, portVal);

		portVal = ~portVal;
		GPIO_ClearValue(port, portVal);
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_uint8_t hexToNibbleBin(lu_int8_t chr)
{
	lu_uint8_t nibBin;

	nibBin = 0;

	chr = toupper(chr);

	if (isxdigit(chr))
	{
		nibBin = ((lu_uint8_t)chr - (lu_uint8_t)'0');
	}

	return nibBin;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint32_t hex8ToBin32(lu_int8_t *strPtr)
{
	lu_uint32_t len;
	lu_uint32_t retVal;
	lu_uint32_t idx;
	lu_uint32_t shift;

	retVal = 0;
	idx = 0;
	len    = strlen(strPtr);
	if (len > 0 && len <= 8)
	{
		for (idx = 0; idx < len; idx++)
		{
			shift = (28 - (4 * idx));

			retVal |= ((lu_uint32_t)hexToNibbleBin(strPtr[idx]) << shift);
		}
	}
	return retVal;
}

lu_int8_t *bin32ToHex8(lu_uint32_t bin32)
{
	lu_uint8_t nibBin;
	lu_uint32_t idx;
	lu_uint32_t shift;
	static lu_int8_t retStr[9];

	retStr[8] = '\0';

	for(idx = 0; idx < 8; idx++)
	{
		shift = (28 - (4 * idx));

		nibBin = (bin32 >> shift) & 0x0f;
		retStr[idx] = hexTable[nibBin];
	}

	return retStr;
}

/*
 *********************** End of file ******************************************
 */
