/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Status Manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   06/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "StatusManager.h"
#include "systemTime.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define SM_MAX_NO_THREADS			25

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

SMThreadContextStr   threadPool[SM_MAX_NO_THREADS];
lu_uint8_t			 threadIdx;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void StatusManagerRun(const SMThreadStr *threadList)
{
	lu_uint32_t		currentTime;
	lu_uint32_t     timeDiff;
	lu_uint32_t     timeReShedule;

	currentTime = STGetTime();

	/* Init thread pool context */
	threadIdx = 0;
	do
	{
		if (threadIdx < SM_MAX_NO_THREADS)
		{
			threadPool[threadIdx].nextCycle = 0;
			threadPool[threadIdx].nextTime = currentTime;
		}

		threadIdx++;
	} while (threadList[threadIdx].run != 0L);


	/* Begin Thread scheduler >>>>> */
	threadIdx = 0;
	while(LU_TRUE)
	{
		currentTime = STGetTime();

		/* Periodic timer scheduling */
		if (threadList[threadIdx].period)
		{
			timeDiff = STElapsedTime(threadPool[threadIdx].nextTime, currentTime);

			if (timeDiff > threadList[threadIdx].period)
			{
				/* Run the thread */
				if (threadList[threadIdx].run != 0L)
				{
					threadList[threadIdx].run();
				}

				/* Calc time period for next run */
				timeReShedule = (timeDiff - 1 - threadList[threadIdx].period);

				/* Update next period */
				threadPool[threadIdx].nextTime = (currentTime - timeReShedule);
			}
		}
		else if (threadList[threadIdx].cyclic)
		{
			/* Cyclic scheduling */
			if (threadPool[threadIdx].nextCycle == 0)
			{
				/* Run the thread */
				if (threadList[threadIdx].run != 0L)
				{
					threadList[threadIdx].run();
				}

				/* Update cyclic counter */
				threadPool[threadIdx].nextCycle = threadList[threadIdx].cyclic;
			}
			else
			{
				threadPool[threadIdx].nextCycle--;
			}
		} else
		{
			/* Always run this thread */
			if (threadList[threadIdx].run != 0L)
			{
				threadList[threadIdx].run();
			}
		}
		/* Increment to next thread - check for end of table */
		threadIdx++;
		if (threadList[threadIdx].run == 0L)
		{
			threadIdx = 0;
		}
	}
}

lu_uint8_t StatusManagerGetThreadIdx(void)
{
	return threadIdx;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
