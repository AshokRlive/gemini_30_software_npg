/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Status Manager library header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   06/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _STATUSMANAGER_INCLUDED
#define _STATUSMANAGER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * \brief Thread run function.
 *
 * This function is called by the Status Manager scheduler
 *
 */
typedef SB_ERROR (*SMThreadRun)(void);

typedef struct SMThreadContextDef
{
	lu_uint8_t			nextCycle;
	lu_uint32_t			nextTime;
} SMThreadContextStr;

typedef struct SMThreadDef
{
	SMThreadRun			run;
	lu_uint8_t			cyclic;
	lu_uint32_t			period;
} SMThreadStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void StatusManagerRun(const SMThreadStr *threadList);

/*!
 ******************************************************************************
 *   \brief Get current thread index
 *
 *   Get current thread index
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern lu_uint8_t StatusManagerGetThreadIdx(void);

#endif /* _STATUSMANAGER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
