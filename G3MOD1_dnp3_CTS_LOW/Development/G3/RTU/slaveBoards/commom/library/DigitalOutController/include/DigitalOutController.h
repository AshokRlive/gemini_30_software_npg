/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Output library header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/11/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _DIGITAL_OUT_CONTROLLER_INCLUDED
#define _DIGITAL_OUT_CONTROLLER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define DOC_IOCHAN(ioChan, IoIDOutReset, ioIDOutSet, ioIDInFeedBack, ioIDLED) \
	{ioChan, IoIDOutReset, ioIDOutSet, ioIDInFeedBack, ioIDLED, \
	 0, 0, 0, 0, 0, 0},

#define DOC_LAST \
	{0, IO_ID_NA, IO_ID_NA, IO_ID_NA, IO_ID_NA, \
	 0, 0, 0, 0, 0, 0}


#define DIG_OUT_CON_TICK_MS		20 // Must at at least equal to IO scan rate (I2C)


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

LU_DISABLE_WARNING(-pedantic)

typedef struct DigitalOutMapDef
{
	lu_uint32_t		ioChan;
	lu_uint32_t		ioIDOutReset;
	lu_uint32_t		ioIDOutSet;
	lu_uint32_t		ioIDInFeedBack;
	lu_uint32_t		ioIDLED;

	lu_uint32_t     operateTime;

	/* Channel config info */
	lu_uint16_t		pulseLengthMS;
	lu_uint8_t      enable : 1;
	lu_uint8_t      state : 4;
	lu_uint8_t      unused : 3;
	lu_uint16_t     selectCountDownMS;

	lu_uint32_t     operateCountDownMS;
}DigitalOutMapStr;

LU_ENABLE_WARNING(-pedantic)

/*! Digital Output Controller init structure */
typedef struct DigitalOutInitDef
{
	DigitalOutMapStr		*ioTablePtr; //(*ioTablePtr)[];
	lu_uint8_t              ioTableSize;
}DigitalOutInitStr;

/*!
 * Definition of Digital Output controller state machine
 */
typedef enum
{
	DIG_OUT_STATE_IDLE = 0					,
	DIG_OUT_STATE_SELECTED                  ,
	DIG_OUT_STATE_OPERATING_ON              ,
	DIG_OUT_STATE_OPERATING_OFF             ,
	DIG_OUT_STATE_OPERATING_PULSE           ,
	DIG_OUT_STATE_SELECT_TIMEOUT            ,
	DIG_OUT_STATE_OPERATE_ERROR             ,
	DIG_OUT_STATE_COMPLETED                 ,

	DIG_OUT_STATE_LAST
}DIG_OUT_STATE;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR DigitalOutControllerInit(DigitalOutInitStr *initPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR DigitalOutControllerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR DigitalOutControllerTick(void);

#endif /* _DIGITAL_OUT_CONTROLLER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
