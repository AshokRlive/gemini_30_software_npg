/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Digital Out Controller module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/11/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "string.h"

#include "IOManager.h"

#include "systemTime.h"
#include "DigitalOutController.h"

#include "ModuleProtocol.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */





/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR DigOutConfigureCmd(DigOutConfigStr *digOutConfigPtr);
SB_ERROR DigOutSelectCmd(DigOutSelectStr *digOutSelectPtr);
SB_ERROR DigOutOperateCmd(DigOutOperateStr *digOutOperatePtr);
SB_ERROR DigOutCancelCmd(DigOutCancelStr *digOutOCancelPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported CAN messages */
static const filterTableStr DigOutControlModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Digital Output commands */

	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_DOC_CH_SELECT_C             , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_DOC_CH_OPERATE_C            , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_DOC_CH_CANCEL_C             , LU_FALSE , 0      },
	{  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_DIGITAL_OUT_C        		  , LU_FALSE , 0   	  }
};

DigitalOutInitStr *digitalOutInitPtr = NULL;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



SB_ERROR DigitalOutControllerInit(DigitalOutInitStr *initPtr)
{
	SB_ERROR		retError;
	
	retError = SB_ERROR_PARAM;

	if (initPtr != NULL)
	{
		retError = SB_ERROR_NONE;

		/* Save pointer to Ditial Out channel params table */
		digitalOutInitPtr = initPtr;

		/* Setup CAN Msg filter */
		retError = CANFramingAddFilter( DigOutControlModulefilterTable,
										SU_TABLE_SIZE(DigOutControlModulefilterTable, filterTableStr)
									  );
	}

	return retError;
}


SB_ERROR DigitalOutControllerCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC;

	LU_UNUSED( time );

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_DOC_CH_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DigOutSelectStr))
			{
				DigOutSelectCmd((DigOutSelectStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_DOC_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DigOutOperateStr))
			{
				DigOutOperateCmd((DigOutOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CMD_DOC_CH_CANCEL_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DigOutCancelStr))
			{
				DigOutCancelCmd((DigOutCancelStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_DIGITAL_OUT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(DigOutConfigStr))
			{
				retError = DigOutConfigureCmd((DigOutConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}


	return retError;
}

SB_ERROR DigitalOutControllerTick(void)
{
	lu_uint8_t      chan;
	lu_int32_t      value;
	SB_ERROR        retError;
	lu_uint32_t		currentTime;

	currentTime = STGetTime();

	/* Update all channel state machine timers */
	for (chan = 0; chan < digitalOutInitPtr->ioTableSize; chan++)
	{
		if (digitalOutInitPtr->ioTablePtr[chan].enable)
		{
			switch (digitalOutInitPtr->ioTablePtr[chan].state)
			{
			case DIG_OUT_STATE_IDLE:
				/* Idle */
				break;

			case DIG_OUT_STATE_SELECTED:
				/* Selected */
				digitalOutInitPtr->ioTablePtr[chan].selectCountDownMS -=
						LU_MIN(digitalOutInitPtr->ioTablePtr[chan].selectCountDownMS,
							   DIG_OUT_CON_TICK_MS);

				if (!digitalOutInitPtr->ioTablePtr[chan].selectCountDownMS)
				{
					digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_SELECT_TIMEOUT;
				}
				/* If timeout expire then state goes back to select timeout */
				break;

			case DIG_OUT_STATE_OPERATING_ON:
				/* Operating */

				/* Verify operation was successful from input feedback pins */
				if( STElapsedTime( digitalOutInitPtr->ioTablePtr[chan].operateTime , currentTime) >
								    digitalOutInitPtr->ioTablePtr[chan].operateCountDownMS
				  )
				{
					retError = IOManagerGetValue(digitalOutInitPtr->ioTablePtr[chan].ioIDInFeedBack, &value);
					if (retError == SB_ERROR_NONE)
					{
						if (value == 1)
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_COMPLETED;
						}
						else
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATE_ERROR;
						}
					}
					else
					{
						if (retError == SB_ERROR_PARAM)
						{
							/* Input feedback not configured */
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_COMPLETED;
						}
						else
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATE_ERROR;
						}
					}
				}
				break;

			case DIG_OUT_STATE_OPERATING_OFF:
				/* Operating */

				/* Verify operation was successful from input feedback pins */
				if( STElapsedTime( digitalOutInitPtr->ioTablePtr[chan].operateTime , currentTime) >
								    digitalOutInitPtr->ioTablePtr[chan].operateCountDownMS
				  )
				{
					retError = IOManagerGetValue(digitalOutInitPtr->ioTablePtr[chan].ioIDInFeedBack, &value);
					if (retError == SB_ERROR_NONE)
					{
						if (value == 0)
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_COMPLETED;
						}
						else
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATE_ERROR;
						}
					}
					else
					{
						if (retError == SB_ERROR_PARAM)
						{
							/* Input feedback not configured */
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_COMPLETED;
						}
						else
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATE_ERROR;
						}
					}
				}
				break;

			case DIG_OUT_STATE_OPERATING_PULSE:
				if( STElapsedTime( digitalOutInitPtr->ioTablePtr[chan].operateTime , currentTime) >
				    digitalOutInitPtr->ioTablePtr[chan].operateCountDownMS
				  )
				{
					/* Check relay is off, after pulse (Unable to check during pulse) */
					retError = IOManagerGetValue(digitalOutInitPtr->ioTablePtr[chan].ioIDInFeedBack, &value);
					if (retError == SB_ERROR_NONE)
					{
						if (value == 0)
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_COMPLETED;
						}
						else
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATE_ERROR;
						}
					}
					else
					{
						if (retError == SB_ERROR_PARAM)
						{
							/* Input feedback not configured */
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_COMPLETED;
						}
						else
						{
							digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATE_ERROR;
						}
					}
				}
				break;

			case DIG_OUT_STATE_COMPLETED:
				/* Completed */
				break;

			case DIG_OUT_STATE_OPERATE_ERROR:
				break;

			default:
				break;
			}
		}
	}

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DigOutConfigureCmd(DigOutConfigStr *digOutConfigPtr)
{
	SB_ERROR retErr;
	lu_uint8_t chan;
	ModReplyStr reply;

	retErr = SB_ERROR_PARAM;

	chan = digOutConfigPtr->channel;
	reply.channel 	= digOutConfigPtr->channel;

	if (digOutConfigPtr->channel < digitalOutInitPtr->ioTableSize)
	{
		digitalOutInitPtr->ioTablePtr[chan].enable = digOutConfigPtr->outputEnabled;
		digitalOutInitPtr->ioTablePtr[chan].pulseLengthMS = digOutConfigPtr->pulseLengthMS;

		retErr = SB_ERROR_NONE;
	}

	reply.status = SB_ERROR_NONE;

	if (retErr != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_DIGITAL_OUT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DigOutSelectCmd(DigOutSelectStr *digOutSelectPtr)
{
	SB_ERROR retErr;
	lu_uint8_t chan;
	ModReplyStr reply;

	retErr = SB_ERROR_PARAM;
	chan = digOutSelectPtr->channel;
	reply.status = SB_ERROR_NONE;
	reply.channel = chan;

	if (chan < digitalOutInitPtr->ioTableSize)
	{
		if (digitalOutInitPtr->ioTablePtr[chan].enable)
		{
			switch (digitalOutInitPtr->ioTablePtr[chan].state)
			{
			case DIG_OUT_STATE_IDLE:
				/* Idle */
				digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_SELECTED;

				digitalOutInitPtr->ioTablePtr[chan].selectCountDownMS = digOutSelectPtr->SelectTimeoutMS;
				break;
			}
		}
		retErr = SB_ERROR_NONE;
	}

	if (retErr != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_DOC_CH_SELECT_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );


	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DigOutOperateCmd(DigOutOperateStr *digOutOperatePtr)
{
	SB_ERROR retErr;
	lu_uint8_t chan;
	ModReplyStr reply;
	lu_uint32_t outValue;
	lu_uint16_t pulseLengthMs;
	lu_uint32_t	currentTime;

	currentTime = STGetTime();

	retErr = SB_ERROR_PARAM;
	chan = digOutOperatePtr->channel;
	reply.status = SB_ERROR_NONE;
	reply.channel = chan;

	if (chan < digitalOutInitPtr->ioTableSize)
	{
		if (digitalOutInitPtr->ioTablePtr[chan].enable)
		{
			switch (digitalOutInitPtr->ioTablePtr[chan].state)
			{
			case DIG_OUT_STATE_IDLE:
				reply.status = REPLY_STATUS_OPERATE_ERROR;
				break;

			case DIG_OUT_STATE_SELECTED:
				/* Selected */

				/* Drive output pins to static state or pulsed (onOff ignored) */
				outValue = digOutOperatePtr->onOff ? 1 : 0;
				pulseLengthMs = digitalOutInitPtr->ioTablePtr[chan].pulseLengthMS;

				digitalOutInitPtr->ioTablePtr[chan].operateTime = currentTime;

				if (pulseLengthMs > 0)
				{
					digitalOutInitPtr->ioTablePtr[chan].operateCountDownMS = (pulseLengthMs + (DIG_OUT_CON_TICK_MS * 2));
					IOManagerSetPulse(digitalOutInitPtr->ioTablePtr[chan].ioIDOutSet, pulseLengthMs);

					IOManagerSetPulse(digitalOutInitPtr->ioTablePtr[chan].ioIDLED, pulseLengthMs);

					digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATING_PULSE;
				}
				else
				{
					digitalOutInitPtr->ioTablePtr[chan].operateCountDownMS = (DIG_OUT_CON_TICK_MS * 2);

					IOManagerSetValue(digitalOutInitPtr->ioTablePtr[chan].ioIDOutSet, outValue);

					IOManagerSetValue(digitalOutInitPtr->ioTablePtr[chan].ioIDLED, outValue);

					if (outValue)
					{
						digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATING_ON;
					}
					else
					{
						digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_OPERATING_OFF;
					}
				}
				break;

			case DIG_OUT_STATE_OPERATING_ON:
			case DIG_OUT_STATE_OPERATING_OFF:
			case DIG_OUT_STATE_OPERATING_PULSE:
				reply.status = REPLY_STATUS_ALREADY_OPERATING_ERROR;
				break;

			case DIG_OUT_STATE_SELECT_TIMEOUT:
				/* select timeout */
				reply.status = REPLY_STATUS_TIMEOUT_ERROR;
				break;

			default:
				reply.status = REPLY_STATUS_OPERATE_ERROR;
				break;
			}
		}
		retErr = SB_ERROR_NONE;
	}

	if (retErr != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_DOC_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return retErr;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR DigOutCancelCmd(DigOutCancelStr *digOutOCancelPtr)
{
	SB_ERROR retErr;
	lu_uint8_t chan;
	ModReplyStr reply;
	lu_uint32_t		currentTime;

	currentTime = STGetTime();

	retErr = SB_ERROR_PARAM;
	chan = digOutOCancelPtr->channel;
	reply.status = SB_ERROR_NONE;
	reply.channel = chan;

	if (chan < digitalOutInitPtr->ioTableSize)
	{
		if (digitalOutInitPtr->ioTablePtr[chan].enable)
		{
			switch (digitalOutInitPtr->ioTablePtr[chan].state)
			{
			case DIG_OUT_STATE_IDLE:
				break;

			case DIG_OUT_STATE_SELECTED:
				/* Selected */
				reply.status = REPLY_STATUS_SELECT_ERROR;
				digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_IDLE;
				break;

			case DIG_OUT_STATE_OPERATING_ON:
			case DIG_OUT_STATE_OPERATING_OFF:
			case DIG_OUT_STATE_OPERATING_PULSE:
				/* Operating */
				reply.status = REPLY_STATUS_OPERATION_IN_PROGRESS;
				break;

			case DIG_OUT_STATE_COMPLETED:
				reply.status = REPLY_STATUS_OKAY;
				digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_IDLE;
				/* Completed */
				break;

			case DIG_OUT_STATE_SELECT_TIMEOUT:
				/* select timeout */
				reply.status = REPLY_STATUS_SELECT_ERROR;

				digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_IDLE;
				break;

			case DIG_OUT_STATE_OPERATE_ERROR:
				reply.status = REPLY_STATUS_OPERATE_ERROR;
				digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_IDLE;
				break;

			default:
				reply.status = REPLY_STATUS_CANCEL_ERROR;
				digitalOutInitPtr->ioTablePtr[chan].state = DIG_OUT_STATE_IDLE;
				break;
			}

			/* Reset state machine & pulse timer */
			/* Should a cancel stop the operation ?? */
		}
		retErr = SB_ERROR_NONE;
	}

	if (retErr != SB_ERROR_NONE)
	{
		reply.status = REPLY_STATUS_ERROR;
	}

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
						MODULE_MSG_ID_CMD_DOC_CH_CANCEL_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return retErr;
}

/*
 *********************** End of file ******************************************
 */
