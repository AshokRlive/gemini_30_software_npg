/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [ADE78xx FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/07/14      fryers_j     Initial version. 
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"
#include "Calibration.h"
#include "BoardCalibration.h"
#include "BoardIOMap.h"
#include "BoardIO.h"
#include "systemTime.h"

#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"

#include "ADE78xx.h"
#include "ADE78xxConfig.h"

#include "ADE78xxFPI.h"

#include "IOManagerExtADE78xxInit.h"
#include "IOManagerExtADE78xxConfig.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
/* Configuration */
#define ADE78XX_IGAIN_VALUE 	 0xF881400
#define ADE78XX_VGAIN 			-8348885
#define ADE78XX_IRMSGAIN 		-8310000
#define ADE78XX_GAIN_REG_COUNT		4

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef struct ADE78xxFPIConfigParamsDef
{

	FPMConfigStr cfg;

	/* Define any other stuff required for timming FPI before trigger event ....*/

}ADE78xxFPIConfigParamsStr;


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void ADE78xxFPIStart(void);
SB_ERROR ADE78xxFPIConfig(FPMConfigStr *msgPtr);
SB_ERROR ADE78xxFPIOperate(FPIOperateStr *msgPtr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported CAN messages */
static const filterTableStr ADE78xxFPIModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! FPM commands */
	{  MODULE_MSG_TYPE_CMD, MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C            , LU_FALSE , 0      },

    /*! FPI Config */
    {  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_FPM_C                       , LU_FALSE , 1      }
};

static ADE78xxFPIConfigParamsStr fpmConfigParams[FPI_CONFIG_CH_LAST];


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR ADE78xxFPIInit(void)
{
	SB_ERROR retError;

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled = LU_TRUE;

	/* FPI config parameter for development purposes only */
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.minFaultDurationMs = 71;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.timedFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.InstantFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.minOverloadDurationMs = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.phaseFault.overloadCurrent = 1700;

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.earthFault.minFaultDurationMs = 50;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.earthFault.timedFaultCurrent = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.earthFault.InstantFaultCurrent = 100;

	fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.selfResetMs = 10500;

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled = LU_TRUE;

	/* FPI config parameter for development purposes only */

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.minFaultDurationMs = 71;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.timedFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.InstantFaultCurrent = 1700;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.minOverloadDurationMs = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.phaseFault.overloadCurrent = 1700;

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.earthFault.minFaultDurationMs = 50;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.earthFault.timedFaultCurrent = 100;
	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.earthFault.InstantFaultCurrent = 100;

	fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.selfResetMs = 10500;

	/* Now configure ADE78xx chips - using calibration info */
	ADE78xxFPIStart();

	/* Setup CAN Msg filter */
	retError = CANFramingAddFilter( ADE78xxFPIModulefilterTable,
								    SU_TABLE_SIZE(ADE78xxFPIModulefilterTable, filterTableStr)
								 );

	return retError;
}

SB_ERROR ADE78xxFPICANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC;

	LU_UNUSED( time );

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_FPM_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FPMConfigStr))
			{
				retError = ADE78xxFPIConfig((FPMConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	case MODULE_MSG_TYPE_CMD:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CMD_FPI_CH_OPERATE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(FPIOperateStr))
			{
				retError = ADE78xxFPIOperate((FPIOperateStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void ADE78xxFPIStart(void)
{
	SB_ERROR retError = SB_ERROR_PARAM;
	ADE78XX_CH  adcChan;
	ade78xxRegCalStr *fpiGain;
	CalElementStr calElement;

	/* FPI A Calibration of Gain Registers */
	CalibrationGetCalElement(CAL_ID_FPI1_PHASE_A_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_A].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	CalibrationGetCalElement(CAL_ID_FPI1_PHASE_B_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_A].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	CalibrationGetCalElement(CAL_ID_FPI1_PHASE_C_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_A].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	CalibrationGetCalElement(CAL_ID_FPI1_PHASE_N_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_A].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	/* FPI B Calibration of Gain Registers */
	CalibrationGetCalElement(CAL_ID_FPI2_PHASE_A_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_B].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	CalibrationGetCalElement(CAL_ID_FPI2_PHASE_B_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_B].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	CalibrationGetCalElement(CAL_ID_FPI2_PHASE_C_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_B].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);

	CalibrationGetCalElement(CAL_ID_FPI2_PHASE_N_GAIN, CAL_TYPE_ADE78XX_REG, &calElement);
	fpiGain = (ade78xxRegCalStr*)calElement.calDataPtr;
	retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[ADE78XX_CH_B].i2cBus,
											  fpiGain->reg,
											  sizeof(lu_uint32_t),
											  fpiGain->calValue
											);


	/* Configure ADE78xx chips using calibration data */
	/*
	for (adcChan = ADE78XX_CH_A; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
		retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
												  ADE78XX_AIGAIN,
												  sizeof(lu_uint32_t),
												  0xF85D0FF
												);

		retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
												  ADE78XX_BIGAIN,
												  sizeof(lu_uint32_t),
												  0xF85BFFF
												);

		retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
												  ADE78XX_CIGAIN,
												  sizeof(lu_uint32_t),
												  0xF85BFFF
												);

		retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
												  ADE78XX_NIGAIN,
												  sizeof(lu_uint32_t),
												  0xF85BFFF
												);
	}
	 */

	/* Start DSP */
	for (adcChan = ADE78XX_CH_A; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
		//retError = writeI2cADE78xxReg16Bit(ADE78XX_RUN, ade78xxIoIDPins[adcChan].i2cBus, 0x0001);
		retError = IOManagerExtAD78xxI2CWriteReg( ade78xxIOIDMap[adcChan].i2cBus,
												  ADE78XX_RUN,
												  sizeof(lu_uint16_t),
												  0x0001
												);
		STDelayTime(1000);
	}

	/* Enable interrupt handler for ADC sampling */
	/*
	for (adcChan = ADE78XX_CH_A; adcChan < ADE78XX_CH_LAST; adcChan++)
	{
			IOManagerADE78xxConfigHSDCDMA(ade78xxIOIDMap[adcChan].spiBus);
	}
	*/
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR ADE78xxFPIConfig(FPMConfigStr *msgPtr)
{
	SB_ERROR retError;
	ModReplyStr reply;

	retError = SB_ERROR_NONE;

	switch (msgPtr->fpiConfigChannel)
	{
	case FPI_CONFIG_CH_FPI1:
	case FPI_CONFIG_CH_FPI2:
		/* Save configuration + Enable FPI */
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.earthFault = msgPtr->earthFault;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.phaseFault = msgPtr->phaseFault;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.phaseFault.overloadCurrent = 20000;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.selfResetMs = msgPtr->selfResetMs;
		fpmConfigParams[msgPtr->fpiConfigChannel].cfg.enabled    = msgPtr->enabled;
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	reply.channel = msgPtr->fpiConfigChannel;
	reply.status  = retError == SB_ERROR_NONE ? REPLY_STATUS_OKAY : REPLY_STATUS_ERROR;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
			            MODULE_MSG_ID_CFG_FPM_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR ADE78xxFPIOperate(FPIOperateStr *msgPtr)
{
	SB_ERROR retError;
	ModReplyStr reply;

	retError = SB_ERROR_NONE;

	switch (msgPtr->channel)
	{
	case FPM_CH_FPI_1_TEST:
		/* Disable FPI and force all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled = LU_FALSE;

		retError = IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 1);

		retError = IOManagerSetValue(IO_ID_LED_FPI1, 1);
		retError = IOManagerSetValue(IO_ID_LED_EFI1, 1);

		retError = IOManagerSetValue(IO_ID_FPI1_TEST_ACTIVE, 1);
		break;

	case FPM_CH_FPI_1_RESET:
		/* Enable FPI and clear all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI1].cfg.enabled = LU_TRUE;

		retError = IOManagerSetValue(IO_ID_FPI1_COMMON_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_EARTH_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI1_INST_EARTH_FAULT, 0);

		retError = IOManagerSetValue(IO_ID_LED_FPI1, 0);
		retError = IOManagerSetValue(IO_ID_LED_EFI1, 0);

		retError = IOManagerSetValue(IO_ID_FPI1_TEST_ACTIVE, 0);
		break;

	case FPM_CH_FPI_2_TEST:
		/* Disable FPI and force all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled = LU_FALSE;

		retError = IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 1);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 1);

		retError = IOManagerSetValue(IO_ID_LED_FPI2, 1);
		retError = IOManagerSetValue(IO_ID_LED_EFI2, 1);

		retError = IOManagerSetValue(IO_ID_FPI2_TEST_ACTIVE, 1);
		break;

	case FPM_CH_FPI_2_RESET:
		/* Enable FPI and clear all FPI outputs */
		fpmConfigParams[FPI_CONFIG_CH_FPI2].cfg.enabled = LU_TRUE;

		retError = IOManagerSetValue(IO_ID_FPI2_COMMON_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_PHASE_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_EARTH_FAULT, 0);
		retError = IOManagerSetValue(IO_ID_FPI2_INST_EARTH_FAULT, 0);

		retError = IOManagerSetValue(IO_ID_LED_FPI2, 0);
		retError = IOManagerSetValue(IO_ID_LED_EFI2, 0);

		retError = IOManagerSetValue(IO_ID_FPI2_TEST_ACTIVE, 0);
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	reply.channel = msgPtr->channel;
	reply.status  = retError == SB_ERROR_NONE ? REPLY_STATUS_OKAY : REPLY_STATUS_ERROR;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CMD,
			            MODULE_MSG_ID_CMD_FPI_CH_OPERATE_R,
						sizeof(ModReplyStr),
						(lu_uint8_t *)&reply
					  );

	return retError;
}


/*
 *********************** End of file ******************************************
 */
