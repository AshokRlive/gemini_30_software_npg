/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test SPI ADE78xx ADC driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/10/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "lpc17xx_ssp.h"
#include "lpc17xx_spi.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_gpdma.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "systemTime.h"
#include "FactoryTestSPIADE78xx.h"
#include "ADE78xx.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define SELECT_SPI_TOGGLE_COUNT 4
#define SELECT_SPI_DELAY_MS     20

#define ADE78XX_CONFIG_A 0xE470

#define OVER_CURRENT_ENABLE (1 << 17)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*! SPI DMA buffer index */
typedef enum
{
    SSP_DMA_BUF_IDX_0 = 0,
    SSP_DMA_BUF_IDX_1    ,
    SSP_DMA_BUF_IDX_LAST
}DMA_BUF_IDX;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseSpiADE78xx (TestSpiADE78xxInitStr *ade78xxInitParamPtr);
SB_ERROR ADE78xxSPIReadReg (TestSpiADE78xxReadRegStr *ade78xxReadRegParamPtr);
SB_ERROR ADE78xxSPIWriteReg(TestSpiADE78xxWriteRegStr *ade78xxWriteRegParamPtr);

SB_ERROR FactoryTestADE78xxGPDMAInit(TestSSPDMAStr *sspDma);

void FactoryTestADE78xxOverCurrentHandler(void);
//void FactoryTestSPIADE78xxDMAIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
lu_uint8_t FPI_A_SumBuffer [SSP_DMA_BUF_IDX_LAST][sizeof(lu_uint32_t)];

lu_uint8_t readSumCommand[SSP_DMA_BUF_IDX_LAST][sizeof(lu_uint32_t)]= { {0x01, 0x43, 0xBF, 0x00},
		   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	    {0x01, 0x43, 0xBF, 0x00}
		 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	  };
lu_uint8_t sumValueBuffer[7];

GPDMA_LLI_Type fpiASumLLI[SSP_DMA_BUF_IDX_LAST] =
{
    //Source Addr  Destination address                          Next LLI address                         GPDMA Control
	{0x40030008  , (lu_uint32_t)(FPI_A_SumBuffer[SSP_DMA_BUF_IDX_0]), (lu_uint32_t)(&fpiASumLLI[SSP_DMA_BUF_IDX_1]), 0x88009007},
    {0x40030008  , (lu_uint32_t)(FPI_A_SumBuffer[SSP_DMA_BUF_IDX_1]), (lu_uint32_t)(&fpiASumLLI[SSP_DMA_BUF_IDX_0]), 0x88009007}
};

GPDMA_LLI_Type fpiASumCommandLLI[SSP_DMA_BUF_IDX_LAST] =
{
    //Source Addr  Destination address                          Next LLI address                         GPDMA Control
	{0x40030008 , (lu_uint32_t)(readSumCommand[SSP_DMA_BUF_IDX_0]), (lu_uint32_t)(&fpiASumCommandLLI[SSP_DMA_BUF_IDX_1]), 0x88009004},
    {0x40030008 , (lu_uint32_t)(readSumCommand[SSP_DMA_BUF_IDX_1]), (lu_uint32_t)(&fpiASumCommandLLI[SSP_DMA_BUF_IDX_0]), 0x88009004}
};


static lu_uint8_t  irq1Port,irq1Pin;
static lu_uint32_t irq1PinShift;

TestSpiADE78xxReadRegStr   ade78xxReadRegParam;
TestSpiADE78xxWriteRegStr  ade78xxWriteRegParam;
TestSSPDMAStr sspDma = {1, 1};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FactoryTestSPIADE78xxEnable(TestSpiADE78xxInitStr *ade78xxInitParamPtr)
{
	PINSEL_CFG_Type pinCfg;
	SSP_CFG_Type	sspConfig;
	SPI_CFG_Type	spiConfig;
	TestErrorRspStr retError = { SB_ERROR_NONE };

	/* Init SSP */
	SSP_ConfigStructInit(&sspConfig);

	sspConfig.CPHA        = SSP_CPHA_FIRST;
	sspConfig.CPOL        = SSP_CPOL_HI;
	sspConfig.Mode        = SSP_MASTER_MODE;
	sspConfig.FrameFormat = SSP_FRAME_SPI;
	sspConfig.ClockRate   = 2500000;

	switch(ade78xxInitParamPtr->sspBus)
	{
		case 0:
			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_1;
			pinCfg.Pinnum      = PINSEL_PIN_20;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			PINSEL_ConfigPin(&pinCfg);

			/* MISO */
			pinCfg.Portnum     = PINSEL_PORT_1;
			pinCfg.Pinnum      = PINSEL_PIN_23;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			PINSEL_ConfigPin(&pinCfg);

			/* MISI */
			pinCfg.Portnum     = PINSEL_PORT_1;
			pinCfg.Pinnum      = PINSEL_PIN_24;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			PINSEL_ConfigPin(&pinCfg);

			/* Init SPI */
			SSP_Init(LPC_SSP0, &sspConfig);

			SSP_Cmd(LPC_SSP0, ENABLE);
			break;

		case 1:
			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_7;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			PINSEL_ConfigPin(&pinCfg);

			/* MISO */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_8;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			PINSEL_ConfigPin(&pinCfg);

			/* MISI */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_9;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			PINSEL_ConfigPin(&pinCfg);

			/* Init SPI */
			SSP_Init(LPC_SSP1, &sspConfig);

			SSP_Cmd(LPC_SSP1, ENABLE);
			break;

		case 2:
			/* Legacy SPI peripheral */
			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_15;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			PINSEL_ConfigPin(&pinCfg);

			/* MISO */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_17;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			PINSEL_ConfigPin(&pinCfg);

			/* MISI */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_18;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			PINSEL_ConfigPin(&pinCfg);

			/* Init SPI */
			SPI_ConfigStructInit(&spiConfig);

			spiConfig.CPHA        = SPI_CPHA_FIRST;
			spiConfig.CPOL        = SPI_CPOL_HI;
			spiConfig.ClockRate   = 2500000;
			spiConfig.DataOrder   = SPI_DATA_MSB_FIRST;
			spiConfig.Databit     = SPI_DATABIT_8;
			spiConfig.Mode        = SPI_MASTER_MODE;

			SPI_Init(LPC_SPI, &spiConfig);
			break;

		default:
			retError.errorCode = SB_ERROR_PARAM;
			break;
	}

	retError.errorCode = initialiseSpiADE78xx(ade78xxInitParamPtr);

	/*
	irq1Port = ade78xxInitParamPtr->irq1Port;
	irq1Pin  = ade78xxInitParamPtr->irq1Pin;

	irq1PinShift = 1;
	irq1PinShift = (irq1PinShift << irq1Pin);

	pinCfg.Portnum     = irq1Port;
	pinCfg.Pinnum      = irq1Pin;
	pinCfg.Funcnum     = PINSEL_FUNC_0;
	pinCfg.Pinmode     = PINSEL_PINMODE_PULLUP;
	pinCfg.OpenDrain   = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&pinCfg);

	GPIO_SetDir(irq1Port, irq1PinShift, 0);
*/

	return CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
						MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_R,
						MODULE_MESSAGE_SIZE(TestErrorRspStr),
						(lu_uint8_t*)&retError
		               );

}

SB_ERROR FactoryTestSPIADE78xxReadReg(TestSpiADE78xxReadRegStr *ade78xxReadRegParamPtr)
{

	ADE78xxSPIReadReg(ade78xxReadRegParamPtr);

	return CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
 						MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_R,
					    sizeof(lu_uint32_t),
					    (lu_uint8_t*)&ade78xxReadRegParamPtr->value
           	   	   	  );

}

SB_ERROR FactoryTestSPIADE78xxWriteReg(TestSpiADE78xxWriteRegStr  *ade78xxWriteRegParamPtr)
{

	SB_ERROR retError;

	retError = ADE78xxSPIWriteReg(ade78xxWriteRegParamPtr);

	return CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
  						MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_R,
 					    sizeof(retError),
 					   (lu_uint8_t*)&retError
           	   	   	   );
}


void FactoryTestADE78xxOverCurrentHandler(void)
{

	lu_uint32_t status1Reg;

//	GPDMA_ChannelCmd(0, DISABLE);

	ade78xxReadRegParam.reg    = ADE78XX_STATUS1;
	ade78xxReadRegParam.length = sizeof(lu_uint32_t);
    ADE78xxSPIReadReg(&ade78xxReadRegParam);

    status1Reg = (lu_uint32_t)ade78xxReadRegParam.value;

	ade78xxReadRegParam.reg    = ADE78XX_PHSTATUS;
	ade78xxReadRegParam.length = sizeof(lu_uint32_t);
    ADE78xxSPIReadReg(&ade78xxReadRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_STATUS1;
	ade78xxReadRegParam.value &= ~(OVER_CURRENT_ENABLE);
	ade78xxWriteRegParam.value = ade78xxReadRegParam.value;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_PHSTATUS;
	ade78xxWriteRegParam.value  = 0;
	ade78xxWriteRegParam.length = sizeof(lu_uint16_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_STATUS1;
	ade78xxWriteRegParam.value  = (status1Reg | OVER_CURRENT_ENABLE);
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	GPIO_ClearInt(irq1Port, irq1PinShift);

//	GPDMA_ChannelCmd(0, ENABLE);
}

SB_ERROR FactoryTestADE78xxDMASoftTrigger(void)
{
	LPC_GPDMA->DMACSoftSReq |= 0x0C;

	return SB_ERROR_NONE;
}

/*
void FactoryTestSPIADE78xxDMAIntHandler(void)
{
	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
	{
		// Clear terminate counter Interrupt pending
	    LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);
	 }
	else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
	{
		// Clear error counter Interrupt pending
	    LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
	}

	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(1))
	{
		// Clear terminate counter Interrupt pending
	    LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(1);
	}
	else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(1))
	{
		// Clear error counter Interrupt pending
		LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(1);
	}
}
*/

SB_ERROR FactoryTestADE78xxGPDMAInit(TestSSPDMAStr *sspDma)
{
	GPDMA_Channel_CFG_Type GPDMACfg;

	if(!(LPC_GPDMA->DMACConfig & GPDMA_DMACConfig_E))
	{
		GPDMA_Init();
	}

	if(sspDma->sspBus == 1)
	{
		if(!(LPC_GPDMACH0->DMACCConfig & GPDMA_DMACCxConfig_E))
		{
			GPDMACfg.ChannelNum = 0;
			// Source memory - not used
			GPDMACfg.SrcMemAddr = (uint32_t)&readSumCommand[SSP_DMA_BUF_IDX_0];
			// Destination memory
			GPDMACfg.DstMemAddr = 0;
			// Transfer size
			GPDMACfg.TransferSize = sizeof(readSumCommand[SSP_DMA_BUF_IDX_0]);
			// Transfer width - not used
			GPDMACfg.TransferWidth = 0;
			// Transfer type
			GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
			// Source connection
			GPDMACfg.SrcConn = GPDMA_CONN_SSP1_Tx;
			// Destination connection - not used
			GPDMACfg.DstConn = 0;
			// Linker List Item
			GPDMACfg.DMALLI = (lu_uint32_t) &fpiASumCommandLLI;
			// Setup channel with given parameter
			GPDMA_Setup(&GPDMACfg);
		}

		if(!(LPC_GPDMACH1->DMACCConfig & GPDMA_DMACCxConfig_E))
		{
			GPDMACfg.ChannelNum = 1;
			// Source memory - not used
			GPDMACfg.SrcMemAddr = 0;
			// Destination memory
			GPDMACfg.DstMemAddr = (uint32_t) &FPI_A_SumBuffer[SSP_DMA_BUF_IDX_0];
			// Transfer size
			GPDMACfg.TransferSize = sizeof(readSumCommand[SSP_DMA_BUF_IDX_0])+ sizeof(FPI_A_SumBuffer[SSP_DMA_BUF_IDX_0]);
			// Transfer width - not used
			GPDMACfg.TransferWidth = 0;
			// Transfer type
			GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
			// Source connection
			GPDMACfg.SrcConn = GPDMA_CONN_SSP1_Rx;
			// Destination connection - not used
			GPDMACfg.DstConn = 0;
			// Linker List Item
			GPDMACfg.DMALLI = (lu_uint32_t) &fpiASumLLI;
			// Setup channel with given parameter
			GPDMA_Setup(&GPDMACfg);
		}

		LPC_GPDMA->DMACSoftSReq = 0x0C;
	}

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */

SB_ERROR ConfigureSPIADE78xx(  lu_uint8_t   sspBus,
							lu_uint8_t   csPort,
		  					lu_uint8_t   csPin,
							lu_int32_t   gainRegAI,
							lu_int32_t   gainRegBI,
							lu_int32_t   gainRegCI
						 )
{
	SB_ERROR    retError;
	FunctionalState irq1PinIntStatus;
	lu_uint32_t csPinShift;

	ade78xxReadRegParam.sspBus = sspBus;
	ade78xxReadRegParam.csPort = csPort;
	ade78xxReadRegParam.csPin  = csPin;

	ade78xxWriteRegParam.sspBus = sspBus;
	ade78xxWriteRegParam.csPort = csPort;
	ade78xxWriteRegParam.csPin  = csPin;

	/* Reset all IRQs. Reset STATUS0 and STATUS1 registers */
	ade78xxReadRegParam.reg    = ADE78XX_STATUS0;
	ade78xxReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxSPIReadReg(&ade78xxReadRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_STATUS0;
	ade78xxWriteRegParam.value  = ade78xxReadRegParam.value;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxReadRegParam.reg    = ADE78XX_STATUS1;
	ade78xxReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxSPIReadReg(&ade78xxReadRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_STATUS1;
	ade78xxWriteRegParam.value  = ade78xxReadRegParam.value;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	/* Initialise Registers */
	/* Current Gain register */
	ade78xxWriteRegParam.reg    = ADE78XX_AIGAIN;
	ade78xxWriteRegParam.value  = gainRegAI;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_BIGAIN;
	ade78xxWriteRegParam.value  = gainRegBI;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_CIGAIN;
	ade78xxWriteRegParam.value  = gainRegCI;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_OILVL;
	ade78xxWriteRegParam.value  = 88400;
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxReadRegParam.reg    = ADE78XX_MASK1;
	ade78xxReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxSPIReadReg(&ade78xxReadRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_MASK1;
	/* Applying Over current Indication */
	ade78xxWriteRegParam.value  = ade78xxReadRegParam.value | (OVER_CURRENT_ENABLE);
	ade78xxWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxReadRegParam.reg    = ADE78XX_CONFIG;
	ade78xxReadRegParam.length = sizeof(lu_uint16_t);
	retError = ADE78xxSPIReadReg(&ade78xxReadRegParam);

	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, ADE78XX_CONFIG, sizeof(lu_uint16_t), retVal16U);
	ade78xxWriteRegParam.reg    = ADE78XX_CONFIG;
	ade78xxWriteRegParam.value  = ade78xxReadRegParam.value;
	ade78xxWriteRegParam.length = sizeof(lu_uint16_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	/* Write the last register 2 more times to flush the DSP internal pipeline */
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	ade78xxWriteRegParam.reg    = ADE78XX_CONFIG_A;
	ade78xxWriteRegParam.value  = 0x00000000;
	ade78xxWriteRegParam.length = sizeof(lu_uint8_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	/* Enable memory protection */
	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, 0xE7FE, sizeof(lu_uint8_t), 0x000000AD);
	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, 0xE7E3, sizeof(lu_uint8_t), 0x00000080);

	/* RUN DSP */
	ade78xxWriteRegParam.reg    = ADE78XX_RUN;
	ade78xxWriteRegParam.value  = 0x00000001;
	ade78xxWriteRegParam.length = sizeof(lu_uint16_t);
	ADE78xxSPIWriteReg(&ade78xxWriteRegParam);

	//Enabling IRQ1 as an input and enabling GPIO interrupt for over current
	/*
	GPIO_ClearInt(irq1Port, irq1PinShift);
	GPIO_IntCmd(irq1Port, irq1PinShift , 1);

	irq1PinIntStatus = GPIO_GetIntStatus(irq1Port, irq1Pin, 1);
*/
/*
	csPinShift = 1;
	csPinShift = csPinShift << csPin;
	GPIO_ClearValue(csPort, csPinShift);

	FactoryTestADE78xxGPDMAInit(&sspDma);
	//SSP_DMACmd (LPC_SSP1, SSP_DMA_TX, ENABLE);
	SSP_DMACmd (LPC_SSP1, SSP_DMA_RX, ENABLE);
	//GPDMA_ChannelCmd(0, ENABLE);
	GPDMA_ChannelCmd(1, ENABLE);
*/

	return retError;
}

SB_ERROR ADE78xxSPIWriteReg(TestSpiADE78xxWriteRegStr *ade78xxWriteRegParamPtr)
{
  	lu_uint8_t				csPort;
  	lu_uint8_t				csPin;
  	lu_uint32_t	            csPinShift;
  	lu_uint8_t				txBuf[7];
  	lu_uint8_t				rxBuf[4];
  	lu_uint32_t				xferLen;
  	SSP_DATA_SETUP_Type		xferConfig;
  	LPC_SSP_TypeDef         *sspTyp;
  	SB_ERROR				retError;

  	retError = SB_ERROR_NONE;

  	csPort = ade78xxWriteRegParamPtr->csPort;
  	csPin  = ade78xxWriteRegParamPtr->csPin;

  	csPinShift = 1;
  	csPinShift = csPinShift << (csPin);

  	txBuf[0] = 0x00;
   	txBuf[1] = ( ade78xxWriteRegParamPtr->reg  >>  8  );
   	txBuf[2] = ( ade78xxWriteRegParamPtr->reg  & 0xFF );

   	if(ade78xxWriteRegParamPtr->length == 1)
	{
		txBuf[3] = (ade78xxWriteRegParamPtr->value) & 0xFF;
	}

	if(ade78xxWriteRegParamPtr->length == 2)
	{
		txBuf[3] = (ade78xxWriteRegParamPtr->value >> 8) & 0xFF;
		txBuf[4] = ade78xxWriteRegParamPtr->value & 0xFF;
	}

	if(ade78xxWriteRegParamPtr->length == 4)
	{
		txBuf[3] = (ade78xxWriteRegParamPtr->value >> 24) & 0xFF;
		txBuf[4] = (ade78xxWriteRegParamPtr->value >> 16) & 0xFF;
		txBuf[5] = (ade78xxWriteRegParamPtr->value >> 8 ) & 0xFF;
		txBuf[6] = (ade78xxWriteRegParamPtr->value      ) & 0xFF;
	}

	xferConfig.tx_data  = (lu_uint8_t*)&txBuf;
  	xferConfig.rx_data  = (lu_uint8_t*)&rxBuf;
  	xferConfig.length   = ade78xxWriteRegParamPtr->length + 3;

  	switch (ade78xxWriteRegParamPtr->sspBus)
  	{
  	case 0:
  		sspTyp = LPC_SSP0;
  		break;

  	case 1:
  		sspTyp = LPC_SSP1;
  		break;

  	default:
  		retError = SB_ERROR_PARAM;
  		break;
  	}

  	/* Activate CS - low */
  	GPIO_ClearValue(csPort, csPinShift);

  	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

  	/* deActivate CS - high */
  	GPIO_SetValue(csPort, csPinShift);

  	if (xferLen != xferConfig.length)
  	{
  		retError =  SB_ERROR_SPI_FAIL;
  	}

  	return retError;
}

SB_ERROR ADE78xxSPIReadReg(TestSpiADE78xxReadRegStr *ade78xxReadRegParamPtr)
{
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	lu_uint32_t	            csPinShift;
	lu_uint8_t				txBuf[4];
	lu_uint8_t				rxBuf[7];
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint32_t             regValue = 0;
	SB_ERROR				retError;

	retError = SB_ERROR_PARAM;

	csPort = ade78xxReadRegParamPtr->csPort;
	csPin  = ade78xxReadRegParamPtr->csPin;

	csPinShift = 1;
	csPinShift = csPinShift << (csPin);

	txBuf[0] = 0x01;
	txBuf[1] = ( ade78xxReadRegParamPtr->reg  >>  8  );
	txBuf[2] = ( ade78xxReadRegParamPtr->reg  & 0xFF );
	txBuf[3] = 0;

	xferConfig.tx_data  = (lu_uint8_t*)&txBuf;
	xferConfig.rx_data  = (lu_uint8_t*)&rxBuf;
	xferConfig.length   = ade78xxReadRegParamPtr->length + 3;

	switch (ade78xxReadRegParamPtr->sspBus)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	// Activate CS - low
	GPIO_ClearValue(csPort, csPinShift);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	// deActivate CS - high
	GPIO_SetValue(csPort, csPinShift);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	if(ade78xxReadRegParamPtr->length == 1)
	{
		regValue  = rxBuf[3];
		ade78xxReadRegParamPtr->value = regValue;
		retError = SB_ERROR_NONE;
	}

	else if(ade78xxReadRegParamPtr->length == 2)
	{
		regValue  = rxBuf[4];
		regValue |= (rxBuf[3] << 8);
		ade78xxReadRegParamPtr->value = regValue;
		retError = SB_ERROR_NONE;
	}

	else if(ade78xxReadRegParamPtr->length == 4)
	{
		regValue  = rxBuf[6];
		regValue |= (rxBuf[5] << 8);
		regValue |= (rxBuf[4] << 16);
		regValue |= (rxBuf[3] << 24);
		ade78xxReadRegParamPtr->value = regValue;
		retError = SB_ERROR_NONE;
	}
	else
	{
		retError = SB_ERROR_PARAM;
	}

	return retError;
}

SB_ERROR initialiseSpiADE78xx(TestSpiADE78xxInitStr *ade78xxInitParamPtr)
{
	PINSEL_CFG_Type pinCfg;
	lu_uint32_t     csPinShift, resetPinShift, pm0Shift, pm1Shift, irq1PinShift, irq1Status;
	lu_uint8_t      resetPort, resetPin;
	lu_uint8_t      pm0Port, pm0Pin;
	lu_uint8_t      pm1Port, pm1Pin;
	lu_uint8_t      irq1Port, irq1Pin;
	lu_uint8_t 		i;
	SB_ERROR 		retError = SB_ERROR_INITIALIZED;

	/* Configuring PM0 pin*/
	pm0Port  = ade78xxInitParamPtr->pinMap.pm0.port;
	pm0Pin   = ade78xxInitParamPtr->pinMap.pm0.pin;
	pm0Shift = 1;
	pm0Shift = (pm0Shift << pm0Pin);

	/* Setting Direction of PM0 Pin*/
	GPIO_SetValue( pm0Port, pm0Shift    );
	GPIO_SetDir  ( pm0Port, pm0Shift, 1 );

	/* Configuring PM1 pin*/
	pm1Port  = ade78xxInitParamPtr->pinMap.pm1.port;
	pm1Pin   = ade78xxInitParamPtr->pinMap.pm1.pin;
	pm1Shift = 1;
	pm1Shift = (pm1Shift << pm1Pin);

	/* Setting Direction of PM1 Pin*/
	GPIO_SetValue  ( pm1Port, pm1Shift    );
	GPIO_SetDir    ( pm1Port, pm1Shift, 1 );

	/* Configuring RESET pin*/
	resetPort = ade78xxInitParamPtr->pinMap.reset.port;
	resetPin  = ade78xxInitParamPtr->pinMap.reset.pin;
	resetPinShift = 1;
	resetPinShift = (resetPinShift << resetPin);

	pinCfg.Portnum = resetPort;
	pinCfg.Pinnum  = resetPort;
	pinCfg.Funcnum = PINSEL_FUNC_0;
	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;
	PINSEL_ConfigPin(&pinCfg);

	/* Setting Direction of RESET Pin*/
	GPIO_SetValue(resetPort, resetPinShift);
	GPIO_SetDir(resetPort,   resetPinShift, 1);

	/* Configuring CS pin*/
	pinCfg.Portnum = ade78xxInitParamPtr->csPort;
	pinCfg.Pinnum  = ade78xxInitParamPtr->csPin;
	pinCfg.Funcnum = PINSEL_FUNC_0;
	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;
	PINSEL_ConfigPin(&pinCfg);
	csPinShift = 1;
	csPinShift = csPinShift << (ade78xxInitParamPtr->csPin);

	/* Setting Direction of CS Pin*/
	GPIO_ClearValue(ade78xxInitParamPtr->csPort, csPinShift);
	GPIO_SetDir(ade78xxInitParamPtr->csPort, csPinShift, 1);

	irq1Port = ade78xxInitParamPtr->pinMap.irq1.port;
	irq1Pin  = ade78xxInitParamPtr->pinMap.irq1.pin;
	irq1PinShift = 1;
	irq1PinShift = (irq1PinShift << (irq1Pin));

	/* Initialising ADE78xx chip
	 * Power Mode Selection of ADE78xx
	 * Setting bits for PSM 0
	 */
	GPIO_SetValue  ( pm0Port, pm0Shift    );
	GPIO_ClearValue( pm1Port, pm1Shift    );

	/* Reset ADE78xx */
	GPIO_ClearValue(resetPort, resetPinShift);
	STDelayTime(SELECT_SPI_DELAY_MS);
	GPIO_SetValue(resetPort, resetPinShift);

	/* Wait for setting power mode*/
	STDelayTime(100);

	/* Check status of ADE78xx - Ready to run */
	irq1Status = GPIO_ReadValue(ade78xxInitParamPtr->pinMap.irq1.port);

	if(((irq1Status & irq1PinShift) >> (irq1Pin)) == 0)
	{
		STDelayTime(SELECT_SPI_DELAY_MS);

		/* Toggling CS pin 3 times for selecting SPI interface to be active */
		for(i = 0; i <= SELECT_SPI_TOGGLE_COUNT; i++)
		{
			GPIO_SetValue(ade78xxInitParamPtr->csPort, csPinShift);
			STDleayTimeUsec(10000);
			GPIO_ClearValue(ade78xxInitParamPtr->csPort, csPinShift);
		}

		retError = SB_ERROR_NONE;
	}

	/* Set CS pin high */
	GPIO_SetValue(ade78xxInitParamPtr->csPort, csPinShift);

	return retError;
}

/*
 *********************** End of file ******************************************
 */
