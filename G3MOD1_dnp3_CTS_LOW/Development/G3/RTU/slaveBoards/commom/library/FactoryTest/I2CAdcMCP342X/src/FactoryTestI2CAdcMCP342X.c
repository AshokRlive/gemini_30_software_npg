/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C ADC MCP342X driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "BoardIOAPII2C.h"
#include "FactoryTestI2CAdcMCP342X.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MCP342X_ADDR						0x68

#define MCP242X_CFG_REG_RDY					0x80

#define MCP242X_CFG_REG_ADC_CHAN_SHIFT 	 	5
#define MCP242X_CFG_REG_ADC_CHAN_MASK   	0x03

#define MCP242X_CFG_REG_CONVERT_CONT		0x10
#define MCP242X_CFG_REG_CONVERT_ONE_SHOT	0x00

#define MCP242X_CFG_REG_SAMP_3_75SPS		0x0c
#define MCP242X_CFG_REG_SAMP_15SPS			0x08
#define MCP242X_CFG_REG_SAMP_60SPS			0x04
#define MCP242X_CFG_REG_SAMP_240SPS			0x00

#define MCP242X_CFG_REG_PGA_X8				0x03
#define MCP242X_CFG_REG_PGA_X4				0x02
#define MCP242X_CFG_REG_PGA_X2				0x01
#define MCP242X_CFG_REG_PGA_X1				0x00

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR readI2cAdcValue(TestI2CAdcMCP342XReadStr *i2cAdcParamsPtr, lu_uint16_t *adcValuePtr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestI2CAdcMCP342XRead(TestI2CAdcMCP342XReadStr *i2cAdcParamsPtr)
{
	SB_ERROR                    retError;
	lu_uint16_t			        adcValue;
	TestI2CAdcMCP342XReadRspStr resp;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseBoardIOAPII2C(i2cAdcParamsPtr->i2cChan, 100000);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readI2cAdcValue(i2cAdcParamsPtr, &adcValue);

		resp.adcVal = adcValue;
	}

	/* deInit the I2C peripheral */
	deInitialiseBoardIOAPII2C(i2cAdcParamsPtr->i2cChan);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_R,
							sizeof(TestI2CAdcMCP342XReadRspStr),
							(lu_uint8_t *)&resp
						   );

	return SB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
 SB_ERROR readI2cAdcValue(TestI2CAdcMCP342XReadStr *i2cAdcParamsPtr, lu_uint16_t *adcValuePtr)
 {
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
	 lu_uint8_t					txData[3];
	 lu_uint8_t					rxData[5];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2CChannelBoardIOAPII2C(i2cAdcParamsPtr->i2cChan, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* Configuration */
		 txData[0]						= MCP242X_CFG_REG_CONVERT_CONT | MCP242X_CFG_REG_SAMP_240SPS |
				                          MCP242X_CFG_REG_PGA_X1 |
				                          (i2cAdcParamsPtr->adcChan & MCP242X_CFG_REG_ADC_CHAN_MASK) <<
				                          MCP242X_CFG_REG_ADC_CHAN_SHIFT;

		 txferCfg.sl_addr7bit     	    = (MCP342X_ADDR | (i2cAdcParamsPtr->addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 *adcValuePtr = 0;

			 *adcValuePtr |= rxData[1];
			 *adcValuePtr |= (rxData[0] & 0xf) << 8;

		 }
	 }
	 return retError;
 }


/*
 *********************** End of file ******************************************
 */
