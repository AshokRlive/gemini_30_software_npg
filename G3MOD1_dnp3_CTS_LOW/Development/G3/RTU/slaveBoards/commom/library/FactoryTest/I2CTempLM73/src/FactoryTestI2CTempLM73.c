/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C IO Expansion driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "FactoryTestI2CTempLM73.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define LM73_ADDR					0x48 // was 0x90

#define LM73_IDENT_NATIONAL			0x190 // Identification reg

#define LM73_REG_MASK				0x07

#define LM73_REG_TEMPERATURE		0x00
#define LM73_REG_CONFIGURATION		0x01
#define LM73_REG_T_HIGH				0x02
#define LM73_REG_T_LOW				0x03
#define LM73_REG_CONTROL_STATUS		0x04
#define LM73_REG_IDENTIFICATION		0x07

#define LM73_CFG_FULL_POWER_DOWN	0x80
#define LM73_CFG_ALERT_DISABLE		0x20
#define LM73_CFG_ALERT_ACTIVE_HIGH	0x10
#define LM73_CFG_ALERT_RESET		0x08
#define LM73_CFG_ONE_SHOT			0x04

/* Control Status temperature resolution */
#define LM73_CONSTAT_RES_0_25       0x0
#define LM73_CONSTAT_RES_0_125      0x1
#define LM73_CONSTAT_RES_0_0625     0x2
#define LM73_CONSTAT_RES_0_03125    0x3

#define LM73_CONSTAT_RES_BITSHIFT	5

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseI2cTempLM73(TestI2cTempLM73ReadStr  *i2cTempReadParamsPtr);
SB_ERROR readI2cI2cTempLM73(TestI2cTempLM73ReadStr *i2cTempReadParamsPtr, lu_int32_t *valuePtr);
SB_ERROR readI2cTempLM73Regs(TestI2cTempLM73ReadRegStr *i2cTempReadRegParamsPtr, lu_int16_t *valuePtr);

SB_ERROR writeI2cTempLM73Reg8Bit(TestI2cTempLM73WriteRegStr *i2cTempLM73WriteRegParamsPtr);

LPC_I2C_TypeDef* getI2cI2cTempLM73Channel(TestI2cTempLM73ReadStr *i2cTempReadParamsPtr,  SB_ERROR *retVal);
SB_ERROR deInitialiseI2cI2cTempLM73(TestI2cTempLM73ReadStr  *i2cTempReadParamsPtr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestI2cTempLM73Read(TestI2cTempLM73ReadStr *i2cTempReadParamsPtr)
{
	SB_ERROR               retError;
	lu_int32_t			   tempValue = 0;
	TestI2cTempLM73WriteRegStr i2cWriteRegParams;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cTempLM73(i2cTempReadParamsPtr);

	i2cWriteRegParams.addr					= i2cTempReadParamsPtr->addr;
	i2cWriteRegParams.i2cChan				= i2cTempReadParamsPtr->i2cChan;
	i2cWriteRegParams.reg                   = LM73_REG_CONTROL_STATUS;
	i2cWriteRegParams.value                 = (LM73_CONSTAT_RES_0_125 << LM73_CONSTAT_RES_BITSHIFT);

	retError = writeI2cTempLM73Reg8Bit(&i2cWriteRegParams);

	/* Do the I2C transfer on the selected bus */
	retError = readI2cI2cTempLM73(i2cTempReadParamsPtr, &tempValue);

	tempValue = (lu_int16_t)tempValue / 16;
	tempValue = tempValue * 125;

	/* deInit the I2C peripheral */
	deInitialiseI2cI2cTempLM73(i2cTempReadParamsPtr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_R,
							sizeof(TestI2cTempLM73ReadRspStr),
							(lu_uint8_t *)&tempValue
						   );

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2cTempLM73ReadReg(TestI2cTempLM73ReadRegStr *i2cTempReadRegParamsPtr)
{
	SB_ERROR               retError;
	lu_int16_t			   tempValue;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cTempLM73(&i2cTempReadRegParamsPtr->params);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readI2cTempLM73Regs(i2cTempReadRegParamsPtr, &tempValue);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cI2cTempLM73(&i2cTempReadRegParamsPtr->params);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_R,
							sizeof(TestI2cTempLM73ReadRspStr),
							(lu_uint8_t *)&tempValue
						   );

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2cTempLM73WriteReg(TestI2cTempLM73WriteRegStr *i2cTempWriteRegParamsPtr)
{
	SB_ERROR               retError, errorCode;
	TestI2cTempLM73ReadStr i2cTempReadParamsPtr;


	i2cTempReadParamsPtr.i2cChan = i2cTempWriteRegParamsPtr->i2cChan;
	i2cTempReadParamsPtr.addr    = i2cTempWriteRegParamsPtr->addr;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cTempLM73(&i2cTempReadParamsPtr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		errorCode = writeI2cTempLM73Reg8Bit(i2cTempWriteRegParamsPtr);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cI2cTempLM73(&i2cTempReadParamsPtr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_R,
							sizeof(TestErrorRspStr),
							(lu_uint8_t *)&errorCode
						   );

	return SB_ERROR_NONE;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR initialiseI2cTempLM73(TestI2cTempLM73ReadStr  *i2cTempReadParamsPtr)
{
 	PINSEL_CFG_Type 	    pinCfg;
 	SB_ERROR                retVal;
 	LPC_I2C_TypeDef			*i2cChanType;

 	retVal = SB_ERROR_NONE;

 	pinCfg.Pinmode     = 0;
 	pinCfg.OpenDrain   = 0;

 	switch (i2cTempReadParamsPtr->i2cChan)
 	{
 	case 0:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_28;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_27;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	case 1:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_20;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_19;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

	case 2:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_11;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;
 		pinCfg.OpenDrain   = PINSEL_PINMODE_NORMAL;
 		pinCfg.Pinmode   = 0;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_10;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;
 		pinCfg.OpenDrain   = PINSEL_PINMODE_NORMAL;
 		pinCfg.Pinmode   = 0;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	default:
 		retVal = SB_ERROR_PARAM;
 		break;
 	}

 	if (retVal == SB_ERROR_NONE)
 	{
 		i2cChanType = getI2cI2cTempLM73Channel(i2cTempReadParamsPtr, &retVal);
 	}

    if (retVal == SB_ERROR_NONE)
    {
    	/* Init I2C */
		I2C_Init((LPC_I2C_TypeDef *)i2cChanType, 100000);

		/* Enable I2C operation */
		I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
    }
 	return retVal;
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readI2cI2cTempLM73(TestI2cTempLM73ReadStr *i2cTempReadParamsPtr, lu_int32_t *valuePtr)
{
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
	 lu_int16_t                 value;
//	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];
	 lu_uint8_t					rxData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2cI2cTempLM73Channel(i2cTempReadParamsPtr, &retError);

	 /* Command */
	 txData[0]						= LM73_REG_TEMPERATURE;

	 txferCfg.sl_addr7bit     	    = (LM73_ADDR | (i2cTempReadParamsPtr->addr & 0x07));
	 txferCfg.tx_data		  		= txData;
	 txferCfg.tx_length       		= 1;
	 txferCfg.rx_data		 	 	= rxData;
	 txferCfg.rx_length       		= 2;
	 txferCfg.retransmissions_max 	= 2;

	 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	 {
		 retError = SB_ERROR_I2C_FAIL;
	 }
	 else
	 {
#if 0
		 /* Must wait for transfer to complete!! */
		 for (count = 0; count < 10000; count++)
		 {
			 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
				 break;
		 }
#endif

		 value = rxData[1];
		 value |= (rxData[0] << 8);

		 *valuePtr = value;

		 //*valuePtr = *(lu_uint16_t *)&rxData[0];
	 }

	 return retError;
}

/*!
 ******************************************************************************
 *   \brief Read LM73 register
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the LM73
 *   \param reg - register offset for LM73
 *   \param valuePtr - return value via pointer
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR readI2cTempLM73Regs(TestI2cTempLM73ReadRegStr *i2cTempReadRegParamsPtr, lu_int16_t *valuePtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_int16_t              value;
	lu_uint8_t				txData[3];
	lu_uint8_t				rxData[3];

	retError = SB_ERROR_NONE;

	i2cChanType = getI2cI2cTempLM73Channel(&i2cTempReadRegParamsPtr->params, &retError);

	/* Command */
	txData[0]						= (i2cTempReadRegParamsPtr->reg & LM73_REG_MASK);

	txferCfg.sl_addr7bit     	    = (LM73_ADDR | (i2cTempReadRegParamsPtr->params.addr & 0x07));
	txferCfg.tx_data		  		= txData;
	txferCfg.tx_length       		= 1;
	txferCfg.rx_data		 	 	= rxData;
	txferCfg.retransmissions_max 	= 2;

	switch (i2cTempReadRegParamsPtr->reg)
	{
	default:
	case LM73_REG_TEMPERATURE:
	case LM73_REG_T_HIGH:
	case LM73_REG_T_LOW:
	case LM73_REG_IDENTIFICATION:
		txferCfg.rx_length       		= 2;
	 break;

	case LM73_REG_CONFIGURATION:
	case LM73_REG_CONTROL_STATUS:
		txferCfg.rx_length       		= 1;
		break;
	}

	if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
	{
		retError = SB_ERROR_I2C_FAIL;
	}
	else
	{
	#if 0
	 /* Must wait for transfer to complete!! */
	 for (count = 0; count < 10000; count++)
	 {
		 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
			 break;
	 }
	#endif

	 switch (i2cTempReadRegParamsPtr->reg)
	 {
	 default:
	 case LM73_REG_TEMPERATURE:
	 case LM73_REG_T_HIGH:
	 case LM73_REG_T_LOW:
	 case LM73_REG_IDENTIFICATION:
		 value = rxData[1];
		 value |= (rxData[0] << 8);
		 break;

	 case LM73_REG_CONFIGURATION:
	 case LM73_REG_CONTROL_STATUS:
		 value = rxData[1];
		 break;
	 }

	 *valuePtr = value;
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Write LM73 register
 *
 *   Detailed description
 *
 *   \param i2cbus - the physical i2c bus to use
 *   \param addr - the I2C address for the LM73
 *   \param reg - register offset for LM73
 *   \param value - value to write to register
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeI2cTempLM73Reg8Bit(TestI2cTempLM73WriteRegStr *i2cTempLM73WriteRegParamsPtr)
{
	SB_ERROR				retError;
	LPC_I2C_TypeDef			*i2cChanType;
	I2C_M_SETUP_Type		txferCfg;
//	lu_uint32_t				count;
	lu_uint8_t				txData[3];
//	lu_uint8_t				rxData[3];
	TestI2cTempLM73ReadStr i2cTempReadRegParams;

	i2cTempReadRegParams.i2cChan = i2cTempLM73WriteRegParamsPtr->i2cChan;
	i2cTempReadRegParams.addr    = i2cTempLM73WriteRegParamsPtr->addr;

	retError = SB_ERROR_NONE;

	i2cChanType = getI2cI2cTempLM73Channel(&i2cTempReadRegParams, &retError);

	if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= (i2cTempLM73WriteRegParamsPtr->reg & LM73_REG_MASK);
		 txData[1] 						= i2cTempLM73WriteRegParamsPtr->value;

		 txferCfg.sl_addr7bit     	    = (LM73_ADDR | (i2cTempLM73WriteRegParamsPtr->addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 2;
		 txferCfg.rx_data		 	 	= 0L;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }

	return retError;
}

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 LPC_I2C_TypeDef* getI2cI2cTempLM73Channel(TestI2cTempLM73ReadStr *i2cTempReadParamsPtr,  SB_ERROR *retVal)
 {
 	 LPC_I2C_TypeDef			*i2cChanType;

     *retVal = SB_ERROR_NONE;

 	 switch (i2cTempReadParamsPtr->i2cChan)
	 {
	 case 0:
		i2cChanType = LPC_I2C0;
		break;

	 case 1:
		i2cChanType = LPC_I2C1;
		break;

	 case 2:
		i2cChanType = LPC_I2C2;
		break;

	 default:
		*retVal = SB_ERROR_PARAM;
		break;
     }

 	 return i2cChanType;
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseI2cI2cTempLM73(TestI2cTempLM73ReadStr  *i2cTempReadParamsPtr)
{
	SB_ERROR               	retVal;
	LPC_I2C_TypeDef			*i2cChanType;

	retVal = SB_ERROR_NONE;

	i2cChanType = getI2cI2cTempLM73Channel(i2cTempReadParamsPtr, &retVal);

	if (retVal == SB_ERROR_NONE)
	{
		I2C_DeInit(i2cChanType);
	}

	return retVal;
}

/*
 *********************** End of file ******************************************
 */
