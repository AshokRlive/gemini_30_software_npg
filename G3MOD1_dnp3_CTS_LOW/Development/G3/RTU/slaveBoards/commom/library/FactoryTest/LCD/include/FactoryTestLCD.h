/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       LCD Driver library header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/11/11      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _LCDDRIVER_INCLUDED
#define _LCDDRIVER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/* Max supported display type */
#define LCD_MAX_ROWS						4
#define LCD_MAX_COLS						80

/* LcdDriverInitStr - lcdInitMode bit defines */
#define LCD_INIT_MODE_8_BIT					0x00000000L
#define LCD_INIT_MODE_4_BIT					0x00000001L
#define LCD_INIT_MODE_RW					0x00000002L
#define LCD_INIT_MODE_ENABLE_BACKLIGHT		0x00000004L
#define LCD_INIT_MODE_5X10_DOT_FONT			0x00000008L
#define LCD_INIT_MODE_DISPLAY_ON			0x00000010L
#define LCD_INIT_MODE_CURSOR_ON				0x00000020L
#define LCD_INIT_MODE_BLINK_ON				0x00000040L
#define LCD_INIT_MODE_TEXT_RIGHT2LEFT		0x00000080L
#define LCD_INIT_MODE_2_LINES				0x00000100L


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! LCD IO reference to Map to idId used by IOManager */
typedef enum
{
	LCD_IO_ID_RW		= 0,
	LCD_IO_ID_EN		   ,
	LCD_IO_ID_RS		   ,
	LCD_IO_ID_DB0		   ,
	LCD_IO_ID_DB1		   ,
	LCD_IO_ID_DB2		   ,
	LCD_IO_ID_DB3		   ,
	LCD_IO_ID_DB4		   ,
	LCD_IO_ID_DB5		   ,
	LCD_IO_ID_DB6		   ,
	LCD_IO_ID_DB7		   ,
	LCD_IO_ID_BACK_LIGHT   ,

	LCD_IO_ID_LAST
} LCD_IO_ID;

typedef struct LcdDriverPinPortDef
{
	lu_int16_t		pin;
	lu_int16_t		port;
}LcdDriverPinPortStr;

/*! LCD Driver init structure */
typedef struct LcdDriverInitDef
{
	lu_uint32_t				lcdInitMode;
	LcdDriverPinPortStr		ioID[LCD_IO_ID_LAST];
	lu_uint8_t				lcdNumRows;
	lu_uint8_t				lcdNumColumns;
	lu_uint8_t      		lcdDDRamRowOffset[LCD_MAX_ROWS];
	lu_uint32_t     		lcdInitResetDelayMs;
	lu_uint32_t     		lcdInitCmdDelayMs;
	lu_uint32_t     		lcdCommandDelayMs;
	lu_uint32_t				lcdCommandLongDelayMs;
}LcdDriverInitStr;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern void FactoryTestLCDInitStructure(LcdDriverInitStr *initPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FactoryTestLCDInit(LcdDriverInitStr *initPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FactoryTestLCDPutString(LcdDriverInitStr *initPtr, lu_int8_t *lineString, lu_uint8_t col, lu_uint8_t row);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FactoryTestLCDClearDisplay(LcdDriverInitStr *initPtr);

#endif /* _LCDDRIVER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
