/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*       \brief  Interprocess Communication Module: implementation
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/05/14      venkat_s     Initial version.
 *
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/
#include <string.h>
#include "lpc17xx_ssp.h"
#include "lpc17xx_spi.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_nvic.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_clkpwr.h"
#include "lpc17xx_gpio.h"
#include "debug_frmwrk.h"

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/
#include "FactoryTestSSPDMA.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"
#include "FrontEndPIC.h"
#include "crc16.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/
/*! SPI DMA buffer index */
typedef enum
{
    DMA_BUF_IDX_0 = 0,
    DMA_BUF_IDX_1    ,

    DMA_BUF_IDX_LAST
}DMA_BUF_IDX;

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/

lu_int8_t ThreePhaseDataBuffer1[DMA_BUF_IDX_LAST][sizeof(FEPic2NXPPacketStr)] __attribute__((section("ahbsram1")));
lu_int8_t ThreePhaseDataBuffer2[DMA_BUF_IDX_LAST][sizeof(FEPic2NXPPacketStr)] __attribute__((section("ahbsram0")));

GPDMA_LLI_Type GPDMAlli1[DMA_BUF_IDX_LAST] =
{
    //Source Addr  Destination address                          Next LLI address                         GPDMA Control
	{0x40088008  , (lu_uint32_t)(ThreePhaseDataBuffer1[DMA_BUF_IDX_0]), (lu_uint32_t)(&GPDMAlli1[DMA_BUF_IDX_1]), 0x88009008},
    {0x40088008  , (lu_uint32_t)(ThreePhaseDataBuffer1[DMA_BUF_IDX_1]), (lu_uint32_t)(&GPDMAlli1[DMA_BUF_IDX_0]), 0x88009008}
};

GPDMA_LLI_Type GPDMAlli2[DMA_BUF_IDX_LAST] =
{
    //Source Addr  Destination address                          Next LLI address                         GPDMA Control
	{0x40030008  , (lu_uint32_t)(ThreePhaseDataBuffer2[DMA_BUF_IDX_0]), (lu_uint32_t)(&GPDMAlli2[DMA_BUF_IDX_1]), 0x88009008},
    {0x40030008  , (lu_uint32_t)(ThreePhaseDataBuffer2[DMA_BUF_IDX_1]), (lu_uint32_t)(&GPDMAlli2[DMA_BUF_IDX_0]), 0x88009008}
};

volatile lu_uint16_t fePic1ADC[FEPIC_ADC_CH_LAST];
volatile lu_uint16_t fePic2ADC[FEPIC_ADC_CH_LAST];

/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/

void FactoryTestSSPDMAIntHandler(void);
void FactotyTestClearDMABuffer(lu_int8_t *bufferPtr);
SB_ERROR FactoryTestGPDMAInit(TestSSPDMAStr *sspDma);
SB_ERROR FactoryTestSSPInit(TestSSPDMAStr *sspDma);
void frontEndPicDecodePacket(lu_uint16_t sspBus, lu_uint8_t bufIdx);

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FactoryTestSSPDMAEnable(TestSSPDMAStr *sspDma)
{

	TestErrorRspStr retError;

	retError.errorCode = SB_ERROR_NONE;

	switch(sspDma->sspBus)
	{
	case 0:
			if(sspDma->enable)
			{
				FactoryTestSSPInit(sspDma);
				FactoryTestGPDMAInit(sspDma);
				SSP_DMACmd (LPC_SSP0, SSP_DMA_RX, ENABLE);
				GPDMA_ChannelCmd(0, ENABLE);
				GPIO_SetValue(PINSEL_PORT_2, 1 << PINSEL_PIN_1);
			}
			else
			{
				SSP_DMACmd (LPC_SSP0, SSP_DMA_RX, DISABLE);
				GPDMA_ChannelCmd(0, DISABLE);
				GPIO_ClearValue(PINSEL_PORT_2, 1 << PINSEL_PIN_1);
				FactotyTestClearDMABuffer(&ThreePhaseDataBuffer1[0][0]);
			}
		break;

	case 1:
			if(sspDma->enable)
			{
				FactoryTestSSPInit(sspDma);
				FactoryTestGPDMAInit(sspDma);
				SSP_DMACmd (LPC_SSP1, SSP_DMA_RX, ENABLE);
				GPDMA_ChannelCmd(1, ENABLE);
				GPIO_SetValue(PINSEL_PORT_3, 1 << PINSEL_PIN_26);
			}
			else
			{
				SSP_DMACmd (LPC_SSP1, SSP_DMA_RX, DISABLE);
				GPDMA_ChannelCmd(1, DISABLE);
				GPIO_ClearValue(PINSEL_PORT_3, 1 << PINSEL_PIN_26);
				FactotyTestClearDMABuffer(&ThreePhaseDataBuffer2[0][0]);
			}
			break;
	default:
			retError.errorCode = SB_ERROR_PARAM;
		break;
	}

	return CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
					    MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_R,
	           	        sizeof(TestErrorRspStr),
	           	        (lu_uint8_t*)&retError
			  	  	  );
}

SB_ERROR FactoryTestReadFEPChannel(TestFEPReadStr *FEPReadParam)
{
	TestFEPDataStr readADCValue;

	readADCValue.errorCode = SB_ERROR_NONE;

	switch(FEPReadParam->sspBus)
	{
		case 0:
		{
			switch(FEPReadParam->adcChan)
			{
			case FEPIC_ADC_CH_L1:
			case FEPIC_ADC_CH_L2:
			case FEPIC_ADC_CH_L3:
			case FEPIC_ADC_CH_SUM:
			case FEPIC_ADC_CH_SUM_PK:
				readADCValue.value = fePic1ADC[FEPReadParam->adcChan];
				break;

			default:
				readADCValue.errorCode = SB_ERROR_PARAM;
				break;
			}
		}
		break;

		case 1:
		{
			switch(FEPReadParam->adcChan)
			{
			case FEPIC_ADC_CH_L1:
			case FEPIC_ADC_CH_L2:
			case FEPIC_ADC_CH_L3:
			case FEPIC_ADC_CH_SUM:
			case FEPIC_ADC_CH_SUM_PK:
				readADCValue.value = fePic2ADC[FEPReadParam->adcChan];
				break;

			default:
				readADCValue.errorCode = SB_ERROR_PARAM;
				break;
			}
		}
		break;

	default:
		readADCValue.errorCode = SB_ERROR_PARAM;
		break;
	}

	return CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
						MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_R,
		           	    sizeof(TestFEPDataStr),
		           	    (lu_uint8_t*)&readADCValue
				  	  );
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
  ******************************************************************************
  *   \brief Decode FEPic Packets
  *
  *   Decoding Packets from FEPics
  *
  *   \param
  *
  *   \return void
  *
  ******************************************************************************
  */
void frontEndPicDecodePacket(lu_uint16_t sspBus, lu_uint8_t bufIdx)
{
	FEPic2NXPPacketStr *packetPtr;
	FEPic2NXPPacketStr  packet;

	switch(sspBus)
	{
	case 0:
		packetPtr = (FEPic2NXPPacketStr *)&ThreePhaseDataBuffer1[bufIdx][0];
		break;

	case 1:
		packetPtr = (FEPic2NXPPacketStr *)&ThreePhaseDataBuffer2[bufIdx][0];
		break;

	default:
		return;
		break;
	} // End of Switch(sspBus)

	// Copy to working buffer to prevent next DMA corrupting, should ISR be delayed
	memcpy((void *)&packet, packetPtr, sizeof(FEPic2NXPPacketStr));
	packetPtr = &packet;

	if (packetPtr->adcChannel < FEPIC_ADC_CH_LAST)
	{
		crc16_calc16((lu_uint8_t *)packetPtr,
					 (sizeof(FEPic2NXPPacketStr) - sizeof(packetPtr->crc16))
					);

		if (packetPtr->crc16 == crc16)
		{
			switch(sspBus)
			{
				case 0:
				{
					switch(packet.adcChannel)
					{
					case FEPIC_ADC_CH_L1:
					case FEPIC_ADC_CH_L2:
					case FEPIC_ADC_CH_L3:
					case FEPIC_ADC_CH_SUM:
					case FEPIC_ADC_CH_SUM_PK:
						fePic1ADC[packet.adcChannel] = packet.adcValue;
						break;

					 default:
						 break;
					}
				}
				break;

				case 1:
				{
					switch(packet.adcChannel)
					{
					case FEPIC_ADC_CH_L1:
					case FEPIC_ADC_CH_L2:
					case FEPIC_ADC_CH_L3:
					case FEPIC_ADC_CH_SUM:
					case FEPIC_ADC_CH_SUM_PK:
						fePic2ADC[packet.adcChannel] = packet.adcValue;
						break;

					 default:
						 break;
				   }
					break;
				}
				default:
					break;
		  }
	  }
    }
}



/*!
  ******************************************************************************
  *   \brief SSP DMA Handler
  *
  *   Clears the corresponding  GPDMA interrupt and cll back the ISR function.
  *
  *   \param void
  *
  *   \return void
  *
  ******************************************************************************
  */
void FactoryTestSSPDMAIntHandler(void)
{
	static lu_uint32_t ssp0IdxCounter = DMA_BUF_IDX_0;
	static lu_uint32_t ssp1IdxCounter = DMA_BUF_IDX_0;

	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
	{
		/* Clear terminate counter Interrupt pending */
	    LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);

	    frontEndPicDecodePacket(0, ssp0IdxCounter);
	    ssp0IdxCounter = ssp0IdxCounter ? 0 : 1;
	 }
	else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
	{
		/* Clear error counter Interrupt pending */
	    LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
	}

	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(1))
	{
	    /* Clear terminate counter Interrupt pending */
	    LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(1);

	    frontEndPicDecodePacket(1, ssp1IdxCounter);
	    ssp1IdxCounter = ssp1IdxCounter ? 0 : 1;
	}
	else if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(1))
	{
	   /* Clear error counter Interrupt pending */
	   LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(1);
	}
}

/*!
  ******************************************************************************
  *   \brief Clearing DMA Buffer
  *
  *   To clear GPDMA Channel buffer
  *
  *   \param bufferPtr
  *
  *   \return void
  *
  ******************************************************************************
  */

void FactotyTestClearDMABuffer(lu_int8_t *bufferPtr)
{
	memset((void*)bufferPtr, 0, (DMA_BUF_IDX_LAST * sizeof(FEPic2NXPPacketStr)));
}

/*!
  ******************************************************************************
  *   \brief GPDMA Initialisation
  *
  *   To initialise the GPDMA Channel according to the given SSP bus.
  *
  *   \param TestSSPDMAStr SSP bus and its state (Enable / Disable)
  *
  *   \return Slave Board Error Code
  *
  ******************************************************************************
  */
SB_ERROR FactoryTestGPDMAInit(TestSSPDMAStr *sspDma)
{
	GPDMA_Channel_CFG_Type GPDMACfg;

	if(!(LPC_GPDMA->DMACConfig & GPDMA_DMACConfig_E))
	{
		GPDMA_Init();
	}

	if(sspDma->sspBus == 0)
	{
		if(!(LPC_GPDMACH0->DMACCConfig & GPDMA_DMACCxConfig_E))
		{
			GPDMACfg.ChannelNum = 0;
			// Source memory - not used
			GPDMACfg.SrcMemAddr = 0;
			// Destination memory
			GPDMACfg.DstMemAddr = (uint32_t) &ThreePhaseDataBuffer1[DMA_BUF_IDX_0];
			// Transfer size
			GPDMACfg.TransferSize = sizeof(ThreePhaseDataBuffer1[DMA_BUF_IDX_0]);
			// Transfer width - not used
			GPDMACfg.TransferWidth = 0;
			// Transfer type
			GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
			// Source connection
			GPDMACfg.SrcConn = GPDMA_CONN_SSP0_Rx;
			// Destination connection - not used
			GPDMACfg.DstConn = 0;
			// Linker List Item
			GPDMACfg.DMALLI = (lu_uint32_t)(&GPDMAlli1);
			// Setup channel with given parameter
			GPDMA_Setup(&GPDMACfg);
		}
	}

	if(sspDma->sspBus == 1)
	{
		if(!(LPC_GPDMACH1->DMACCConfig & GPDMA_DMACCxConfig_E))
		{
			GPDMACfg.ChannelNum = 1;
			// Source memory - not used
			GPDMACfg.SrcMemAddr = 0;
			// Destination memory
			GPDMACfg.DstMemAddr = (uint32_t) &ThreePhaseDataBuffer2[DMA_BUF_IDX_0];
			// Transfer size
			GPDMACfg.TransferSize = sizeof(ThreePhaseDataBuffer2[DMA_BUF_IDX_0]);
			// Transfer width - not used
			GPDMACfg.TransferWidth = 0;
			// Transfer type
			GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
			// Source connection
			GPDMACfg.SrcConn = GPDMA_CONN_SSP1_Rx;
			// Destination connection - not used
			GPDMACfg.DstConn = 0;
			// Linker List Item
			GPDMACfg.DMALLI = (lu_uint32_t)(&GPDMAlli2);
			// Setup channel with given parameter
			GPDMA_Setup(&GPDMACfg);
		}
	}

	return SB_ERROR_NONE;
}

/*!
  ******************************************************************************
  *   \brief Initialisation of SSP Buses
  *
  *   To initialise SSP bus and configure the corresponding pins.
  *
  *   \param TestSSPDMAStr SSP bus and its state (Enable / Disable)
  *
  *   \return Slave Board Error Code
  *
  ******************************************************************************
  */
SB_ERROR FactoryTestSSPInit(TestSSPDMAStr *sspDma)
{
	PINSEL_CFG_Type 	    pinCfg;
	SSP_CFG_Type			sspConfig;
	SB_ERROR                retVal;

	retVal = SB_ERROR_NONE;

	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;

	SSP_ConfigStructInit(&sspConfig);
	sspConfig.CPHA        = SSP_CPHA_SECOND;
	sspConfig.CPOL        = SSP_CPOL_LO;
	sspConfig.ClockRate   = 4000000;
	sspConfig.Databit     = SSP_DATABIT_8;
	sspConfig.Mode        = SSP_SLAVE_MODE;
	sspConfig.FrameFormat = SSP_FRAME_SPI;

	switch (sspDma->sspBus)
	{
	case 0:
		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_20;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* SS */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_21;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_23;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* MISI */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_24;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		CLKPWR_SetPCLKDiv(CLKPWR_PCLKSEL_SSP0, CLKPWR_PCLKSEL_CCLK_DIV_1);
		/* Init SPI */
		SSP_Init(LPC_SSP0, &sspConfig);

		SSP_Cmd(LPC_SSP0, ENABLE);
		break;

	case 1:
		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_7;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* SS */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_6;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_8;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* MOSI */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_9;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		CLKPWR_SetPCLKDiv(CLKPWR_PCLKSEL_SSP1, CLKPWR_PCLKSEL_CCLK_DIV_1);
		/* Init SPI */
		SSP_Init(LPC_SSP1, &sspConfig);

		SSP_Cmd(LPC_SSP1, ENABLE);
		break;


	default:
		retVal = SB_ERROR_PARAM;
		break;
	}

	return retVal;
}
/*
*********************** End of file *******************************************
*/
