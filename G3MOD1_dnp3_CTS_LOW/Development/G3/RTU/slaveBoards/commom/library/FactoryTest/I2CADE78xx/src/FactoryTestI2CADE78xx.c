/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C ADE78xx ADC driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   30/10/14      venkat_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include "stdlib.h"

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_spi.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_gpdma.h"
#include "lpc17xx_clkpwr.h"

//#include "IOManager.h"

#include "systemTime.h"
#include "ADE78xx.h"
#include "ADE78xxConfig.h"
#include "FactoryTestI2CADE78xx.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ADE78XX_I2C_ADDR		(0x38)
#define SELECT_I2C_DELAY_MS      50

#define ADE78XX_CONFIG_A 0xE470
#define OVER_CURRENT_ENABLE (1 << 17)

#define I2C_LOCK (1 << 1)

#define ADE78XX_HSDC_CONFIG 0x0A

#define PHASE_CURRENT_BUFFER_SIZE  8
#define HSDC_DATA_SET_COUNT		   10
#define ADE78XX_HSDC_NUM_SAMPLES   4

/* \brief Convert a 32-bit value from big endian to little endian */
#define BIG_2_LITTLE_ENDIAN(big, little) little = __builtin_bswap32(big)

#define BL_ISR_TOGGLE_PORT							2
#define BL_ISR_TOGGLE_TRACE_0_PIN					(1 << 5)
#define BL_ISR_TOGGLE_TRACE_1_PIN					(1 << 4)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */
/*! SPI DMA buffer index */
typedef enum
{
    DMA_BUF_IDX_0 = 0,
    DMA_BUF_IDX_1    ,

    DMA_BUF_IDX_LAST
}DMA_BUF_IDX;

typedef enum
{
	PHASE_PEAK_A = 0,
	PHASE_PEAK_B = 1,
	PHASE_PEAK_C = 2,
	PHASE_PEAK_N = 3,

	PHASE_PEAK_LAST
}PEAK_CURRENTS;
/*
typedef enum
{
    ADE78XX_HSDC_CH_IAWV = 0,
    ADE78XX_HSDC_CH_VAWV    ,
    ADE78XX_HSDC_CH_IBWV    ,
    ADE78XX_HSDC_CH_VBWV    ,
    ADE78XX_HSDC_CH_ICWV    ,
    ADE78XX_HSDC_CH_VCWV    ,
    ADE78XX_HSDC_CH_INWV    ,

    ADE78XX_HSDC_CH_LAST
}ADE78XX_HSDC_CH;
*/
typedef enum
{
	ADE78XX_CH_A		= 0,
	ADE78XX_CH_B           ,

	ADE78XX_CH_LAST
}ADE78XX_CH;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseADE78xxI2cMode(TestI2cADE78xxInitStr *ade78xxInitParamPtr);
SB_ERROR ADE78xxI2CReadReg(TestI2cADE78xxReadRegStr *ade78xxI2cReadRegParamPtr);
SB_ERROR ADE78xxI2CWriteReg(TestI2cADE78xxWriteRegStr *ade78xxI2cWriteRegParamPtr);


SB_ERROR initialiseI2c(lu_uint8_t i2cChan);
SB_ERROR initialiseHSDC(lu_uint8_t sspBus);

LPC_I2C_TypeDef* getI2cADE78xxChannel(lu_uint8_t i2cChan,  SB_ERROR *retVal);
SB_ERROR deInitialiseI2cADE78xx(lu_uint8_t  i2cChan);

void PeakSampleDetector(lu_int32_t *dataBuffer, lu_int32_t *peaksBuffer, lu_int32_t *peakValuePtr, lu_uint16_t *peaksCount);
void PeakCurrentsToIOManager(void);

void FactoryTestI2CADE78xxDMAIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

TestI2cADE78xxReadRegStr   ade78xxI2cReadRegParam;
TestI2cADE78xxWriteRegStr  ade78xxI2cWriteRegParam;

/* HSDC DMA buffers. The buffers are located in the secondary RAM to not
 * interfere with the main processor memory
 */

lu_int32_t hsdcDMABufferA[DMA_BUF_IDX_LAST][ADE78XX_HSDC_NUM_SAMPLES][ADE78XX_HSDC_CH_LAST] __attribute__((section("ahbsram0"))) __attribute__ ((aligned (4)));

lu_int32_t hsdcDMABufferB[DMA_BUF_IDX_LAST][ADE78XX_HSDC_NUM_SAMPLES][ADE78XX_HSDC_CH_LAST] __attribute__((section("ahbsram1"))) __attribute__ ((aligned (4)));

/* DMA controller linked list. The two buffers point to each
 * other in a "ping-pong" like configuration
 */

GPDMA_LLI_Type gpdmaDMAlliA[DMA_BUF_IDX_LAST] =
{
    // Source Addr  Destination address                          Next LLI address                        GPDMA Control
    {0x40088008  , (lu_uint32_t)(hsdcDMABufferA[DMA_BUF_IDX_0]), (lu_uint32_t)(&gpdmaDMAlliA[DMA_BUF_IDX_1]), 0x88009070},
    {0x40088008  , (lu_uint32_t)(hsdcDMABufferA[DMA_BUF_IDX_1]), (lu_uint32_t)(&gpdmaDMAlliA[DMA_BUF_IDX_0]), 0x88009070}
};

GPDMA_LLI_Type gpdmaDMAlliB[DMA_BUF_IDX_LAST] =
{
    /* Source Addr  Destination address                          Next LLI address                        GPDMA Control */
    {0x40030008  , (lu_uint32_t)(hsdcDMABufferB[DMA_BUF_IDX_0]), (lu_uint32_t)(&gpdmaDMAlliB[DMA_BUF_IDX_1]), 0x88009070},
    {0x40030008  , (lu_uint32_t)(hsdcDMABufferB[DMA_BUF_IDX_1]), (lu_uint32_t)(&gpdmaDMAlliB[DMA_BUF_IDX_0]), 0x88009070}
};

/* Phase currents ioID Map */
static lu_uint32_t fpiCurrentChanMap[ADE78XX_CH_LAST][PHASE_PEAK_LAST];

lu_int32_t phaseDataBuffer[ADE78XX_CH_LAST][ADE78XX_HSDC_NUM_SAMPLES][PHASE_PEAK_LAST];

static lu_int32_t phaseSum[PHASE_PEAK_LAST];
static lu_uint32_t fpi_peakCurrent[ADE78XX_CH_LAST][PHASE_PEAK_LAST];

static lu_int32_t maxValue[ADE78XX_CH_LAST][PHASE_PEAK_LAST];
static lu_uint32_t PeakDetectCounter[ADE78XX_CH_LAST][PHASE_PEAK_LAST];

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */
SB_ERROR FactoryTestI2CADE78xxEnable(TestI2cADE78xxInitStr *ade78xxInitParamPtr)
{
	SB_ERROR retError = SB_ERROR_NONE;

	retError = initialiseI2c(ade78xxInitParamPtr->i2cChan);

	initialiseADE78xxI2cMode(ade78xxInitParamPtr);

	initialiseHSDC(ade78xxInitParamPtr->sspBus);

	/*

	FactoryTestI2CADE78xxConfigure(ade78xxInitParamPtr->i2cChan,
						  	  	   ade78xxInitParamPtr->sspBus,
								   ade78xxInitParamPtr->csPort,
								   ade78xxInitParamPtr->csPin,
								   0xFF809372,
								   0xFF809372,
								   0xFF809372
	   				   	   	   	  );

	ade78xxI2cWriteRegParam.reg    = ADE78XX_RUN;
	ade78xxI2cWriteRegParam.value  = 0x00000001;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint16_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	*/

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			  			    MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_R,
							MODULE_MESSAGE_SIZE(SB_ERROR),
							(lu_uint8_t*)&retError
						  );

	return retError;
}

SB_ERROR FactoryTestI2CADE78xxReadPeak(TestI2cADE78xxReadPeakStr  *ade78xxReadPeakParamPtr)
{
	TestI2cADE78xxReadPeakRespStr peak;

	peak.fpiChan   = ade78xxReadPeakParamPtr->fpiChan;
	peak.peakChan  = ade78xxReadPeakParamPtr->peakChan;
	peak.peakValue = 0xFFFFFFFF;

	switch(ade78xxReadPeakParamPtr->fpiChan)
	{
		case 0:
			switch(ade78xxReadPeakParamPtr->peakChan)
			{
				case PHASE_PEAK_A:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_A][PHASE_PEAK_A];
					break;

				case PHASE_PEAK_B:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_A][PHASE_PEAK_B];
					break;

				case PHASE_PEAK_C:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_A][PHASE_PEAK_C];
					break;

				case PHASE_PEAK_N:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_A][PHASE_PEAK_N];
					break;

				default:
					break;
			}
			break;

		case 1:
			switch(ade78xxReadPeakParamPtr->peakChan)
			{
				case PHASE_PEAK_A:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_B][PHASE_PEAK_A];
					break;

				case PHASE_PEAK_B:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_B][PHASE_PEAK_B];
					break;

				case PHASE_PEAK_C:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_B][PHASE_PEAK_C];
					break;

				case PHASE_PEAK_N:
					peak.peakValue = fpi_peakCurrent[ADE78XX_CH_B][PHASE_PEAK_N];
					break;

				default:
					break;
			}
			break;

		default:
			break;
	}

	return CANCSendMCM( MODULE_MSG_TYPE_BLTST_1,
						MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_R,
						MODULE_MESSAGE_SIZE(TestI2cADE78xxReadPeakRespStr),
						(lu_uint8_t*)&peak
					  );
}

void FactoryTestI2CADE78xxDMAIntHandler(void)
{
	lu_uint32_t        smpIdx;
	lu_uint32_t        phaseChan;

	static lu_uint8_t  dmaBufIdxA = DMA_BUF_IDX_0;
	static lu_uint8_t  dmaBufIdxB = DMA_BUF_IDX_0;

	/* check GPDMA interrupt on channel 0. Not necessary just one channel used
	 * Check counter terminal status
	 */
	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(0))
	{
		/* HSDC data are in big-endian format.
		 * Covert them in little-endian format
		 */
		for(smpIdx = 0; smpIdx < ADE78XX_HSDC_NUM_SAMPLES; smpIdx++)
		{
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferA[dmaBufIdxA][smpIdx][ADE78XX_HSDC_CH_IAWV], phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_A]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferA[dmaBufIdxA][smpIdx][ADE78XX_HSDC_CH_IBWV], phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_B]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferA[dmaBufIdxA][smpIdx][ADE78XX_HSDC_CH_ICWV], phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_C]);

			phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_N] =   phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_A] +
																	phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_B] +
																	phaseDataBuffer[ADE78XX_CH_A][smpIdx][PHASE_PEAK_C];

			for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
			{
				phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan] = abs(phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan]);
				if (phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan] > maxValue[ADE78XX_CH_A][phaseChan])
				{
					maxValue[ADE78XX_CH_A][phaseChan] = phaseDataBuffer[ADE78XX_CH_A][smpIdx][phaseChan];
				}
			}
		}

		/* Increment buffer */
		dmaBufIdxA++;
		dmaBufIdxA &= (DMA_BUF_IDX_LAST - 1);

		//Peak detect
		for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
		{
	    	PeakDetectCounter[ADE78XX_CH_A][phaseChan]++;

	    	// Half Cycle length reached
			if (PeakDetectCounter[ADE78XX_CH_A][phaseChan] >= 20)
			{
				fpi_peakCurrent[ADE78XX_CH_A][phaseChan] = maxValue[ADE78XX_CH_A][phaseChan];
				maxValue[ADE78XX_CH_A][phaseChan] = 0;
				PeakDetectCounter[ADE78XX_CH_A][phaseChan] = 0;
			}
	    }

		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(0);
	}

	if (LPC_GPDMA->DMACIntTCStat & GPDMA_DMACIntTCStat_Ch(1))
	{
		/* HSDC data are in big-endian format.
		 * Covert them in little-endian format
		 */
		for(smpIdx = 0; smpIdx < ADE78XX_HSDC_NUM_SAMPLES; smpIdx++)
		{
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferB[dmaBufIdxB][smpIdx][ADE78XX_HSDC_CH_IAWV], phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_A]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferB[dmaBufIdxB][smpIdx][ADE78XX_HSDC_CH_IBWV], phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_B]);
			BIG_2_LITTLE_ENDIAN(hsdcDMABufferB[dmaBufIdxB][smpIdx][ADE78XX_HSDC_CH_ICWV], phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_C]);

			phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_N] =   phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_A] +
																	phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_B] +
																	phaseDataBuffer[ADE78XX_CH_B][smpIdx][PHASE_PEAK_C];

			for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
			{
				phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan] = abs(phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan]);
				if (phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan] > maxValue[ADE78XX_CH_B][phaseChan])
				{
					maxValue[ADE78XX_CH_B][phaseChan] = phaseDataBuffer[ADE78XX_CH_B][smpIdx][phaseChan];
				}
			}
		}

		/* Increment buffer */
		dmaBufIdxB++;
		dmaBufIdxB &= (DMA_BUF_IDX_LAST - 1);

		//Peak detect
		for (phaseChan = 0; phaseChan< PHASE_PEAK_LAST; phaseChan++)
		{
	    	PeakDetectCounter[ADE78XX_CH_B][phaseChan]++;

	    	// Half Cycle length reached
			if (PeakDetectCounter[ADE78XX_CH_B][phaseChan] >= 20)
			{
				fpi_peakCurrent[ADE78XX_CH_B][phaseChan] = maxValue[ADE78XX_CH_B][phaseChan];
				maxValue[ADE78XX_CH_B][phaseChan] = 0;
				PeakDetectCounter[ADE78XX_CH_B][phaseChan] = 0;
			}
	    }

		/* Clear terminate counter Interrupt pending */
		LPC_GPDMA->DMACIntTCClear = GPDMA_DMACIntTCClear_Ch(1);
	}

	if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(0))
	{
		 // Clear error counter Interrupt pending
		LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(0);
	}

	if (LPC_GPDMA->DMACIntErrStat & GPDMA_DMACIntTCClear_Ch(1))
	{
		 // Clear error counter Interrupt pending
		LPC_GPDMA->DMACIntErrClr = GPDMA_DMACIntErrClr_Ch(1);
	}
}

void PeakSampleDetector(lu_int32_t *dataBuffer, lu_int32_t * peaksBuffer, lu_int32_t *peakValuePtr, lu_uint16_t *peaksCount)
{
	volatile lu_uint16_t i, j;

	peaksBuffer[*peaksCount] = 0;

	for(i = 0; i < HSDC_DATA_SET_COUNT; i++)
	{
		/*
		if(dataBuffer[i] > peaksBuffer[*peaksCount])
		{
			peaksBuffer[*peaksCount] = dataBuffer[i];
		}
		*/
		peaksBuffer[*peaksCount] += dataBuffer[i];
	}

	peaksBuffer[*peaksCount] = peaksBuffer[*peaksCount] / 10;
	*peaksCount = *peaksCount + 1;

	if(*peaksCount == PHASE_CURRENT_BUFFER_SIZE)
	{
		*peakValuePtr = 0;
		for(j = 0; j < PHASE_CURRENT_BUFFER_SIZE; j++)
		{
			if(peaksBuffer[j] > *peakValuePtr)
			{
				*peakValuePtr  = peaksBuffer[j];
			}
		}
		GPIO_SetValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN);
		*peaksCount = 0;
		GPIO_ClearValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN);
	}
}

SB_ERROR FactoryTestI2CADE78xxConfigure(lu_uint8_t   i2cChan,
							   	   	    lu_uint8_t   sspBus,
										lu_uint8_t   csPort,
										lu_uint8_t   csPin,
									    lu_int32_t   gainRegAI,
									    lu_int32_t   gainRegBI,
									    lu_int32_t   gainRegCI
						              )
{
	SB_ERROR    retError;

	ade78xxI2cReadRegParam.i2cChan = i2cChan;

	ade78xxI2cWriteRegParam.i2cChan = i2cChan;

	//retError = readI2cADE78xxReg8Bit(ADE78XX_CONFIG2, ade78xxIoIDPins[adcChan].i2cBus, &regVal8bit);
	ade78xxI2cReadRegParam.reg    = ADE78XX_CONFIG2;
	ade78xxI2cReadRegParam.length = sizeof(lu_uint8_t);
	retError = ADE78xxI2CReadReg(&ade78xxI2cReadRegParam);

    //retError = writeI2cADE78xxReg8Bit(ADE78XX_CONFIG2, ade78xxIoIDPins[adcChan].i2cBus, regVal8bit | I2C_LOCK);
	ade78xxI2cWriteRegParam.reg    = ADE78XX_CONFIG2;
	ade78xxI2cWriteRegParam.value  = (ade78xxI2cReadRegParam.value | I2C_LOCK);
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint8_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	//retError = readI2cADE78xxReg8Bit(ADE78XX_CONFIG2, ade78xxIoIDPins[adcChan].i2cBus, &regVal8bit);

	/* Reset all IRQs. Reset STATUS0 and STATUS1 registers */
	ade78xxI2cReadRegParam.reg    = ADE78XX_STATUS0;
	ade78xxI2cReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxI2CReadReg(&ade78xxI2cReadRegParam);

	ade78xxI2cWriteRegParam.reg    = ADE78XX_STATUS0;
	ade78xxI2cWriteRegParam.value  = ade78xxI2cReadRegParam.value;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	ade78xxI2cReadRegParam.reg    = ADE78XX_STATUS1;
	ade78xxI2cReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxI2CReadReg(&ade78xxI2cReadRegParam);

	ade78xxI2cWriteRegParam.reg    = ADE78XX_STATUS1;
	ade78xxI2cWriteRegParam.value  = ade78xxI2cReadRegParam.value;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	/* Initialise Registers  */
	/* Current Gain register */
	ade78xxI2cWriteRegParam.reg    = ADE78XX_AIGAIN;
	ade78xxI2cWriteRegParam.value  = gainRegAI;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);


	ade78xxI2cReadRegParam.reg    = ADE78XX_AIGAIN;
	ade78xxI2cReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxI2CReadReg(&ade78xxI2cReadRegParam);


	ade78xxI2cWriteRegParam.reg    = ADE78XX_BIGAIN;
	ade78xxI2cWriteRegParam.value  = gainRegBI;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	ade78xxI2cWriteRegParam.reg    = ADE78XX_CIGAIN;
	ade78xxI2cWriteRegParam.value  = gainRegCI;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	ade78xxI2cWriteRegParam.reg    = ADE78XX_NIGAIN;
	ade78xxI2cWriteRegParam.value  = gainRegAI;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	/*
	ade78xxI2cWriteRegParam.reg    = ADE78XX_OILVL;
	ade78xxI2cWriteRegParam.value  = 88400;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	ade78xxI2cReadRegParam.reg    = ADE78XX_MASK1;
	ade78xxI2cReadRegParam.length = sizeof(lu_uint32_t);
	retError = ADE78xxI2CReadReg(&ade78xxI2cReadRegParam);

	ade78xxI2cWriteRegParam.reg    = ADE78XX_MASK1;
	// Applying Over current Indication
	ade78xxI2cWriteRegParam.value  = ade78xxI2cReadRegParam.value | (OVER_CURRENT_ENABLE);
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint32_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);
	*/

	//Enable HSDC Interface

	//writeI2cADE78xxReg8Bit(ADE78XX_HSDC_CFG, ade78xxIoIDPins[adcChan].i2cBus, ADE78XX_HSDC_CONFIG);
	ade78xxI2cWriteRegParam.reg    = ADE78XX_HSDC_CFG;
	ade78xxI2cWriteRegParam.value  = ADE78XX_HSDC_CONFIG;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint8_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	ade78xxI2cWriteRegParam.reg    = ADE78XX_CONFIG_A;
	ade78xxI2cWriteRegParam.value  = 0x01;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint8_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	//retError = readI2cADE78xxReg16Bit(ADE78XX_CONFIG, ade78xxIoIDPins[adcChan].i2cBus, &retVal16U);
	ade78xxI2cReadRegParam.reg    = ADE78XX_CONFIG;
	ade78xxI2cReadRegParam.length = sizeof(lu_uint16_t);
	retError = ADE78xxI2CReadReg(&ade78xxI2cReadRegParam);

	ade78xxI2cReadRegParam.value |= ADE78XX_CONFIG_HSDCEN;

	//writeI2cADE78xxReg16Bit(ADE78XX_CONFIG, ade78xxIoIDPins[adcChan].i2cBus, retVal16U);
	ade78xxI2cWriteRegParam.reg    = ADE78XX_CONFIG;
	ade78xxI2cWriteRegParam.value  = ade78xxI2cReadRegParam.value;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint16_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	/* Write the last register 2 more times to flush the DSP internal pipeline */
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);

	/* Enable memory protection */
	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, 0xE7FE, sizeof(lu_uint8_t), 0x000000AD);
	//FPMV2ADE78xxSPIWriteReg(module, moduleId, sspBus, csPort, csPin, 0xE7E3, sizeof(lu_uint8_t), 0x00000080);

	/* RUN DSP */
	/*
	ade78xxI2cWriteRegParam.reg    = ADE78XX_RUN;
	ade78xxI2cWriteRegParam.value  = 0x00000001;
	ade78xxI2cWriteRegParam.length = sizeof(lu_uint16_t);
	ADE78xxI2CWriteReg(&ade78xxI2cWriteRegParam);
	*/

	STDelayTime(100);

	return retError;
}

SB_ERROR FactoryTestI2CADE78xxWriteReg(TestI2cADE78xxWriteRegStr *ade78xxI2cWriteRegParamPtr)
{
	SB_ERROR               retError;

	retError = ADE78xxI2CWriteReg(ade78xxI2cWriteRegParamPtr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_R,
							MODULE_MESSAGE_SIZE(SB_ERROR),
							(lu_uint8_t*)&retError
						   );

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2CADE78xxReadReg(TestI2cADE78xxReadRegStr *ade78xxI2cReadRegParamPtr)
{
	SB_ERROR               		retError;

	retError = ADE78xxI2CReadReg(ade78xxI2cReadRegParamPtr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_R,
							sizeof(lu_uint32_t),
							(lu_uint8_t*)&ade78xxI2cReadRegParamPtr->value
						  );

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
SB_ERROR initialiseADE78xxI2cMode(TestI2cADE78xxInitStr *ade78xxInitParamPtr)
{
	PINSEL_CFG_Type pinCfg;
	lu_uint32_t     csPinShift, resetPinShift, pm0Shift, pm1Shift, irq1PinShift, irq1Status;
	lu_uint8_t      resetPort, resetPin;
	lu_uint8_t      pm0Port, pm0Pin;
	lu_uint8_t      pm1Port, pm1Pin;
	lu_uint8_t      irq1Port, irq1Pin;
	SB_ERROR 		retError = SB_ERROR_INITIALIZED;

	/* Setting Direction of TRACE_0 Pin */
	GPIO_SetValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_0_PIN    );
	GPIO_SetDir  ( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_0_PIN, 1 );

	/* Setting Direction of TRACE_1 Pin */
	GPIO_SetValue( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN    );
	GPIO_SetDir  ( BL_ISR_TOGGLE_PORT, BL_ISR_TOGGLE_TRACE_1_PIN, 1 );

	/* Configuring PM0 pin */
	pm0Port  = ade78xxInitParamPtr->pinMap.pm0.port;
	pm0Pin   = ade78xxInitParamPtr->pinMap.pm0.pin;
	pm0Shift = 1;
	pm0Shift = (pm0Shift << pm0Pin);

	/* Setting Direction of PM0 Pin */
	GPIO_SetValue( pm0Port, pm0Shift    );
	GPIO_SetDir  ( pm0Port, pm0Shift, 1 );

	/* Configuring PM1 pin */
	pm1Port  = ade78xxInitParamPtr->pinMap.pm1.port;
	pm1Pin   = ade78xxInitParamPtr->pinMap.pm1.pin;
	pm1Shift = 1;
	pm1Shift = (pm1Shift << pm1Pin);

	/* Setting Direction of PM1 Pin */
	GPIO_SetValue  ( pm1Port, pm1Shift    );
	GPIO_SetDir    ( pm1Port, pm1Shift, 1 );

	/* Configuring RESET pin */
	resetPort = ade78xxInitParamPtr->pinMap.reset.port;
	resetPin  = ade78xxInitParamPtr->pinMap.reset.pin;
	resetPinShift = 1;
	resetPinShift = (resetPinShift << resetPin);

	pinCfg.Portnum = resetPort;
	pinCfg.Pinnum  = resetPort;
	pinCfg.Funcnum = PINSEL_FUNC_0;
	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;
	PINSEL_ConfigPin(&pinCfg);

	/* Setting Direction of RESET Pin */
	GPIO_SetValue(resetPort, resetPinShift);
	GPIO_SetDir(resetPort,   resetPinShift, 1);

	/* Configuring CS pin*/
	//pinCfg.Portnum = ade78xxInitParamPtr->csPort;
	//pinCfg.Pinnum  = ade78xxInitParamPtr->csPin;
	//pinCfg.Funcnum = PINSEL_FUNC_0;
	//pinCfg.Pinmode     = 0;
	//pinCfg.OpenDrain   = 0;
	//PINSEL_ConfigPin(&pinCfg);
	//csPinShift = 1;
	//csPinShift = csPinShift << (ade78xxInitParamPtr->csPin);

	/* Setting Direction of CS Pin*/
	//GPIO_ClearValue(ade78xxInitParamPtr->csPort, csPinShift);
	//GPIO_SetDir(ade78xxInitParamPtr->csPort, csPinShift, 1);

	irq1Port = ade78xxInitParamPtr->pinMap.irq1.port;
	irq1Pin  = ade78xxInitParamPtr->pinMap.irq1.pin;
	irq1PinShift = 1;
	irq1PinShift = (irq1PinShift << (irq1Pin));

	/* Initialising ADE78xx chip
	 * Power Mode Selection of ADE78xx
	 * Setting bits for PSM 0
	 */
	GPIO_SetValue  ( pm0Port, pm0Shift    );
	GPIO_ClearValue( pm1Port, pm1Shift    );

	/* To Choose I2C mode SS/HSA needs to be HIGH */
	//GPIO_ClearValue(ade78xxInitParamPtr->csPort, csPinShift);

	/* Reset ADE78xx */
	GPIO_ClearValue(resetPort, resetPinShift);
	STDelayTime(SELECT_I2C_DELAY_MS);
	GPIO_SetValue(resetPort, resetPinShift);

	/* Wait for setting power mode */
	STDelayTime(100);

	/* Check status of ADE78xx - Ready to run */
	irq1Status = GPIO_ReadValue(ade78xxInitParamPtr->pinMap.irq1.port);

	if(((irq1Status & irq1PinShift) >> (irq1Pin)) == 0)
	{
		retError = SB_ERROR_NONE;
	}

	/* Set CS pin high */
	//GPIO_SetValue(ade78xxInitParamPtr->csPort, csPinShift);

	return retError;
}

SB_ERROR initialiseHSDC(lu_uint8_t sspBus)
{
	SB_ERROR 				retError = SB_ERROR_PARAM;
	ADE78XX_CH				adcChan;
	PINSEL_CFG_Type 		pinCfg;
	SSP_CFG_Type			sspConfig;
	SPI_CFG_Type			spiConfig;
	GPDMA_Channel_CFG_Type  GPDMACfg;
	lu_bool_t 				foundADE78xx = LU_FALSE;
	static lu_bool_t 		initDMA = LU_FALSE;

	/* Init SSP */
	SSP_ConfigStructInit(&sspConfig);

	sspConfig.CPHA        = SSP_CPHA_SECOND;
	sspConfig.CPOL        = SSP_CPOL_LO;
	sspConfig.Mode        = SSP_SLAVE_MODE;
	sspConfig.Databit     = SSP_DATABIT_8;
	sspConfig.FrameFormat = SSP_FRAME_SPI;
	sspConfig.ClockRate   = 8000000;

	switch(sspBus)
	{
		case 0:
			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_15;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* SSEL */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_16;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* MISO */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_17;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* MOSI */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_18;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			CLKPWR_SetPCLKDiv((sspBus == 0) ? CLKPWR_PCLKSEL_SSP0 : CLKPWR_PCLKSEL_SSP1, CLKPWR_PCLKSEL_CCLK_DIV_1);

			/* Init SPI */
			SSP_Init(LPC_SSP0, &sspConfig);

			SSP_Cmd(LPC_SSP0, ENABLE);

			foundADE78xx = LU_TRUE;
			retError = SB_ERROR_NONE;
			break;

		case 1:
			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_7;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_6;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* MISO */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_8;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* MOSI */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_9;
			pinCfg.Funcnum     = PINSEL_FUNC_2;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			CLKPWR_SetPCLKDiv((sspBus == 0) ? CLKPWR_PCLKSEL_SSP0 : CLKPWR_PCLKSEL_SSP1, CLKPWR_PCLKSEL_CCLK_DIV_1);
			/* Init SPI */
			SSP_Init(LPC_SSP1, &sspConfig);

			SSP_Cmd(LPC_SSP1, ENABLE);

			retError = SB_ERROR_NONE;
			foundADE78xx = LU_TRUE;
			break;

		case 2:
			/* Legacy SPI peripheral */
			/* SCK */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_15;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* MISO */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_17;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* MISI */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_18;
			pinCfg.Funcnum     = PINSEL_FUNC_3;
			pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
			pinCfg.Pinmode   = 0;
			PINSEL_ConfigPin(&pinCfg);

			/* Init SPI */
			SPI_ConfigStructInit(&spiConfig);

			spiConfig.CPHA        = SPI_CPHA_FIRST;
			spiConfig.CPOL        = SPI_CPOL_HI;
			spiConfig.ClockRate   = 8000000;
			spiConfig.DataOrder   = SPI_DATA_MSB_FIRST;
			spiConfig.Databit     = SPI_DATABIT_8;
			spiConfig.Mode        = SPI_SLAVE_MODE;

			SPI_Init(LPC_SPI, &spiConfig);

			retError = SB_ERROR_NONE;
			foundADE78xx = LU_TRUE;
			break;

		default:
			retError = SB_ERROR_PARAM;
			break;
	}

	if(retError == SB_ERROR_NONE && foundADE78xx == LU_TRUE)
	{
		/* Peripheral found and configured correctly.
		 * Initialise DMA engine
		 */
		if (initDMA == LU_FALSE)
		{
			/* Initialise GPDMA controller */
			GPDMA_Init();

			initDMA = LU_TRUE;
		}

		/* DMA Channel (0 highest priority) */
		GPDMACfg.ChannelNum =  (sspBus == 0) ? 0 : 1;
		/* Source memory - not used */
		GPDMACfg.SrcMemAddr = 0;
		/* Destination memory */
		GPDMACfg.DstMemAddr = (uint32_t) ((sspBus == 0) ?
							  &hsdcDMABufferA[DMA_BUF_IDX_0] : &hsdcDMABufferB[DMA_BUF_IDX_0]);
		/* Transfer size */
		GPDMACfg.TransferSize = sizeof(hsdcDMABufferA[DMA_BUF_IDX_0]);
		/* Transfer width - not used */
		GPDMACfg.TransferWidth = 0;
		/* Transfer type */
		GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
		/* Source connection */
		GPDMACfg.SrcConn = ( (sspBus == 0) ? GPDMA_CONN_SSP0_Rx : GPDMA_CONN_SSP1_Rx );
		/*  Destination connection - not used */
		GPDMACfg.DstConn = 0;
		/*  Link List Item - unused */
		GPDMACfg.DMALLI = (uint32_t)((sspBus == 0) ?
						  &gpdmaDMAlliA[DMA_BUF_IDX_1] : &gpdmaDMAlliB[DMA_BUF_IDX_1]);

		/* Setup channel with given parameter */
		GPDMA_Setup(&GPDMACfg);

		/* Enable Rx DMA */
		SSP_DMACmd( (sspBus == 0) ? LPC_SSP0 : LPC_SSP1,
					 SSP_DMA_RX,
					 ENABLE
				  );

		/* Enable GPDMA channel n */
		GPDMA_ChannelCmd((sspBus == 0) ? 0 : 1, ENABLE);
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR initialiseI2c(lu_uint8_t i2cChan)
{
 	PINSEL_CFG_Type 	    pinCfg;
 	SB_ERROR                retVal;
 	LPC_I2C_TypeDef			*i2cChanType;
 	lu_uint32_t 			clockRate = 400000;

 	retVal = SB_ERROR_NONE;

 	pinCfg.Pinmode     = 0;
 	pinCfg.OpenDrain   = PINSEL_PINMODE_NORMAL;

 	switch (i2cChan)
 	{
 	case 0:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_28;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_27;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	case 1:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_20;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_19;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

	case 2:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_11;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_10;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	default:
 		retVal = SB_ERROR_PARAM;
 		break;
 	}

 	if (retVal == SB_ERROR_NONE)
 	{
 		i2cChanType = getI2cADE78xxChannel(i2cChan, &retVal);
 	}

    if (retVal == SB_ERROR_NONE)
    {
    	/* Init I2C */
		I2C_Init((LPC_I2C_TypeDef *)i2cChanType, clockRate);

		/* Enable I2C operation */
		I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
    }
 	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR ADE78xxI2CWriteReg(TestI2cADE78xxWriteRegStr *ade78xxI2cWriteRegParamPtr)
{
	SB_ERROR                	retError;
	LPC_I2C_TypeDef				*i2cChanType;
	I2C_M_SETUP_Type			txferCfg;
	lu_uint8_t					txData[8];

	retError = SB_ERROR_NONE;

	i2cChanType = getI2cADE78xxChannel(ade78xxI2cWriteRegParamPtr->i2cChan, &retError);

	switch(ade78xxI2cWriteRegParamPtr->length)
	{
	case 1:
		txData[2] = (ade78xxI2cWriteRegParamPtr->value & 0xff);
		break;

	case 2:
		txData[2] = ((ade78xxI2cWriteRegParamPtr->value >> 8) & 0xff);
		txData[3] = ( ade78xxI2cWriteRegParamPtr->value       & 0xff);
		break;

	case 4:
		txData[2] = ((ade78xxI2cWriteRegParamPtr->value >> 24) & 0xff);
		txData[3] = ((ade78xxI2cWriteRegParamPtr->value >> 16) & 0xff);
		txData[4] = ((ade78xxI2cWriteRegParamPtr->value >>  8) & 0xff);
		txData[5] = ( ade78xxI2cWriteRegParamPtr->value        & 0xff);
		break;

	default:
		retError = SB_ERROR_PARAM;
		break;
	}

	if (retError == SB_ERROR_NONE)
	{
		/* Addr Value */
		txData[0]						= (ade78xxI2cWriteRegParamPtr->reg >> 8);
		txData[1]						= (ade78xxI2cWriteRegParamPtr->reg & 0xff);

		txferCfg.sl_addr7bit     	    = ADE78XX_I2C_ADDR;
		txferCfg.tx_data		  		= txData;
		txferCfg.tx_length       		= 2 + ade78xxI2cWriteRegParamPtr->length;
		txferCfg.rx_data		 	 	= NULL;
		txferCfg.rx_length       		= 0;
		txferCfg.retransmissions_max 	= 2;

		if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		{
			retError = SB_ERROR_I2C_FAIL;
		}
	}
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR ADE78xxI2CReadReg(TestI2cADE78xxReadRegStr *ade78xxI2cReadRegParamPtr)
{
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
	 lu_uint8_t					txData[4];
	 lu_uint8_t					rxData[4];
	 lu_uint32_t             	regValue = 0;

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2cADE78xxChannel(ade78xxI2cReadRegParamPtr->i2cChan, &retError);

	 switch(ade78xxI2cReadRegParamPtr->length)
	 {
	 case 1:
	 case 2:
	 case 4:
		 break;

	 default:
		 retError = SB_ERROR_PARAM;
		 break;
	 }

	 if (retError == SB_ERROR_NONE)
	 {
		 /* Addr Value */
		 txData[0]						= (ade78xxI2cReadRegParamPtr->reg >> 8);
		 txData[1]						= (ade78xxI2cReadRegParamPtr->reg & 0xff);

		 txferCfg.sl_addr7bit     	    = ADE78XX_I2C_ADDR;
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 2;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= ade78xxI2cReadRegParamPtr->length;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
			 switch(ade78xxI2cReadRegParamPtr->length)
			 {
			 case 1:
				 regValue  = rxData[0];
				 ade78xxI2cReadRegParamPtr->value = regValue;
				 break;

			 case 2:
				 regValue  = rxData[1];
				 regValue |= (rxData[0] << 8);
				 ade78xxI2cReadRegParamPtr->value = regValue;
				 break;

			 case 4:
				 regValue  =  rxData[3];
				 regValue |= (rxData[2] << 8);
				 regValue |= (rxData[1] << 16);
				 regValue |= (rxData[0] << 24);
				 ade78xxI2cReadRegParamPtr->value = regValue;
				 break;

			 default:
				 retError = SB_ERROR_PARAM;
				 break;
			 }
		 }
	 }
	 return retError;
}


 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 LPC_I2C_TypeDef* getI2cADE78xxChannel(lu_uint8_t i2cChan,  SB_ERROR *retVal)
 {
 	 LPC_I2C_TypeDef			*i2cChanType;

     *retVal = SB_ERROR_NONE;

 	 switch (i2cChan)
	 {
	 case 0:
		i2cChanType = LPC_I2C0;
		break;

	 case 1:
		i2cChanType = LPC_I2C1;
		break;

	 case 2:
		i2cChanType = LPC_I2C2;
		break;

	 default:
		*retVal = SB_ERROR_PARAM;
		break;
     }

 	 return i2cChanType;
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseI2cADE78xx(lu_uint8_t  i2cChan)
{
	SB_ERROR               	retVal;
	LPC_I2C_TypeDef			*i2cChanType;

	retVal = SB_ERROR_NONE;

	i2cChanType = getI2cADE78xxChannel(i2cChan, &retVal);

	if (retVal == SB_ERROR_NONE)
	{
		I2C_DeInit(i2cChanType);
	}

	return retVal;
}

/*
 *********************** End of file ******************************************
 */
