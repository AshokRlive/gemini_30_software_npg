/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test SPI Digi Pot module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_ssp.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"


#include "FactoryTest.h"
#include "FactoryTestSPIDigiPot.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MCP4132_CMD_READ_DATA			(0x0c)
#define MCP4132_CMD_WRITE_DATA			(0x00)
#define MCP4132_CMD_INCREMENT			(0x04)
#define MCP4132_CMD_DECREMENT			(0x08)

#define MCP4132_ADDR_WIPER_0			(0x00)
#define MCP4132_ADDR_WIPER_1			(0x01)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseSpiDigiPot(TestSPIDigiPotAddrStr  *spiDigiPotParamsPtr);
SB_ERROR getSpiDigiPotCsPinPort( TestSPIDigiPotAddrStr  *spiDigiPotParamsPtr,
		                         lu_uint8_t				 *csPortPtr,
		                         lu_uint8_t				 *csPinPtr
		                       );
SB_ERROR writeSpiDigiPot(TestSPIDigiPotWriteStr  *spiDigiPotParamsPtr);
SB_ERROR readSpiDigiPot(TestSPIDigiPotReadStr  *spiDigiPotParamsPtr, lu_uint8_t *readValPtr);
SB_ERROR deInitialiseSpiDigiPot(TestSPIDigiPotAddrStr  *spiDigiPotParamsPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR FactoryTestSPIDigiPotWrite(TestSPIDigiPotWriteStr *spiDigiPotParamsPtr)
{
	SB_ERROR                	retError;

	/* Init the SPI peripheral & configure IO pins */
	retError = initialiseSpiDigiPot(&spiDigiPotParamsPtr->addr);

	/* Do the SPI transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = writeSpiDigiPot(spiDigiPotParamsPtr);
	}

	/* deInit the SPI peripheral */
	deInitialiseSpiDigiPot(&spiDigiPotParamsPtr->addr);

	return retError;
}

SB_ERROR FactoryTestSPIDigiPotRead(TestSPIDigiPotReadStr *spiDigiPotParamsPtr)
{
	SB_ERROR                	retError;
	lu_uint8_t					readValue;

	/* Init the SPI peripheral & configure IO pins */
	retError = initialiseSpiDigiPot(&spiDigiPotParamsPtr->addr);

	/* Do the SPI transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readSpiDigiPot(spiDigiPotParamsPtr, &readValue);
	}

	/* deInit the SPI peripheral */
	deInitialiseSpiDigiPot(&spiDigiPotParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_R,
							MODULE_MESSAGE_SIZE(TestSPIDigiPotReadRspStr),
							(lu_uint8_t *)&readValue
						   );

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR initialiseSpiDigiPot(TestSPIDigiPotAddrStr  *spiDigiPotParamsPtr)
{
	PINSEL_CFG_Type 	    pinCfg;
	SSP_CFG_Type			sspConfig;
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	SB_ERROR                retVal;
	lu_uint32_t	            pinShift;

	retVal = SB_ERROR_NONE;

	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;

	switch (spiDigiPotParamsPtr->sspChan)
	{
	case 0:
		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_20;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* CS */
		retVal = getSpiDigiPotCsPinPort(spiDigiPotParamsPtr, &csPort, &csPin);
		if (retVal != SB_ERROR_NONE)
		{
			break;
		}

		pinShift = 1;
		pinShift = pinShift << (csPin & 0x1f);

		pinCfg.Portnum     = csPort;
		pinCfg.Pinnum      = csPin;
		pinCfg.Funcnum     = PINSEL_FUNC_0;

		PINSEL_ConfigPin(&pinCfg);
		GPIO_SetValue(csPort, pinShift);
		GPIO_SetDir(csPort, pinShift, 1);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_23;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* MISI */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_24;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* Init SPI */
		SSP_ConfigStructInit(&sspConfig);

		SSP_Init(LPC_SSP0, &sspConfig);

		SSP_Cmd(LPC_SSP0, ENABLE);
		break;

	case 1:
		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_7;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* CS */
		retVal = getSpiDigiPotCsPinPort(spiDigiPotParamsPtr, &csPort, &csPin);
		if (retVal != SB_ERROR_NONE)
		{
			break;
		}

		pinShift = 1;
		pinShift = pinShift << (csPin & 0x1f);

		pinCfg.Portnum     = csPort;
		pinCfg.Pinnum      = csPin;
		pinCfg.Funcnum     = PINSEL_FUNC_0;

		PINSEL_ConfigPin(&pinCfg);
		GPIO_SetValue(csPort, pinShift);
		GPIO_SetDir(csPort, pinShift, 1);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_8;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* MISI */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_9;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* Init SPI */
		SSP_ConfigStructInit(&sspConfig);

		SSP_Init(LPC_SSP1, &sspConfig);

		SSP_Cmd(LPC_SSP1, ENABLE);
		break;

	default:
		retVal = SB_ERROR_PARAM;
		break;
	}

	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Get the CS port/port for a given Digi Pot address
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams  Pointer to SPI Digi Pot address
 *   \param *csPort Returns CS port
 *   \param *csPin Returns CS pin
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR getSpiDigiPotCsPinPort( TestSPIDigiPotAddrStr  *spiDigiPotParamsPtr,
		                         lu_uint8_t				 *csPortPtr,
		                         lu_uint8_t				 *csPinPtr
		                       )
{
	switch (spiDigiPotParamsPtr->csChan)
	{
	case 0:
		*csPortPtr = PINSEL_PORT_0;
		*csPinPtr  = PINSEL_PIN_6;
		return SB_ERROR_NONE;
		break;

	case 1:
		*csPortPtr = PINSEL_PORT_0;
		*csPinPtr  = PINSEL_PIN_5;
		return SB_ERROR_NONE;
		break;

	case 2:
		*csPortPtr = PINSEL_PORT_1;
		*csPinPtr  = PINSEL_PIN_21;
		return SB_ERROR_NONE;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}
}

/*!
 ******************************************************************************
 *   \brief Do an SPI write to a digipot channel addressed
 *
 *   Detailed description
 *
 *   \param *spiDigiPotParams
 *
 *
 *   \return SB_ERROR Error code
 *
 ******************************************************************************
 */
SB_ERROR writeSpiDigiPot(TestSPIDigiPotWriteStr  *spiDigiPotParamsPtr)
{
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	lu_uint32_t	            csPinShift;
	lu_uint8_t				txBuf[2];
	lu_uint8_t				rxBuf[3];
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint16_t             potValue;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	potValue = (spiDigiPotParamsPtr->potVal & 0x1ff);

	retVal = getSpiDigiPotCsPinPort(&spiDigiPotParamsPtr->addr, &csPort, &csPin);
	if (retVal != SB_ERROR_NONE)
	{
		return retVal;
	}
	csPinShift = 1;
	csPinShift = csPinShift << (csPin & 0x1f);

	switch(spiDigiPotParamsPtr->addr.potChan)
	{
	case 0:
		txBuf[0] = MCP4132_ADDR_WIPER_0;
		break;

	case 1:
		txBuf[0] = MCP4132_ADDR_WIPER_1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	txBuf[0] |= MCP4132_CMD_WRITE_DATA;
	txBuf[0] |= (potValue >> 8);
	txBuf[1]  = (potValue & 0xff);

	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 2;

	switch (spiDigiPotParamsPtr->addr.sspChan)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	GPIO_ClearValue(csPort, csPinShift);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	GPIO_SetValue(csPort, csPinShift);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readSpiDigiPot(TestSPIDigiPotReadStr  *spiDigiPotParamsPtr, lu_uint8_t *readValPtr)
{
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	lu_uint32_t	            csPinShift;
	lu_uint8_t				txBuf[2];
	lu_uint8_t				rxBuf[3];
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint16_t             potValue;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;

	retVal = getSpiDigiPotCsPinPort(&spiDigiPotParamsPtr->addr, &csPort, &csPin);
	if (retVal != SB_ERROR_NONE)
	{
		return retVal;
	}
	csPinShift = 1;
	csPinShift = csPinShift << (csPin & 0x1f);


	switch(spiDigiPotParamsPtr->addr.potChan)
	{
	case 0:
		txBuf[0] = MCP4132_ADDR_WIPER_0;
		break;

	case 1:
		txBuf[0] = MCP4132_ADDR_WIPER_1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	txBuf[0] |= MCP4132_CMD_READ_DATA;
	xferConfig.tx_data = txBuf;
	xferConfig.rx_data = rxBuf;
	xferConfig.length  = 2;

	switch (spiDigiPotParamsPtr->addr.sspChan)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	GPIO_ClearValue(csPort, csPinShift);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	GPIO_SetValue(csPort, csPinShift);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	potValue  = (rxBuf[0] & 0x01);
	potValue  = potValue << 8;
	potValue |= rxBuf[1];

	/* Return the read value */
	*readValPtr = potValue;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseSpiDigiPot(TestSPIDigiPotAddrStr  *spiDigiPotParamsPtr)
{
	switch (spiDigiPotParamsPtr->sspChan)
	{
	case 0:
		SSP_Cmd(LPC_SSP0, DISABLE);
		return SB_ERROR_NONE;
		break;

	case 1:
			SSP_Cmd(LPC_SSP1, DISABLE);
			return SB_ERROR_NONE;
			break;

	default:
		return SB_ERROR_PARAM;
		break;
	}
}

/*
 *********************** End of file ******************************************
 */
