/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C IO Expansion driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "LPC17xx.h"

#include "errorCodes.h"

#include "BoardIOAPII2C.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR initialiseBoardIOAPII2C(lu_uint8_t	  i2cChan, lu_uint32_t clockRate)
{
 	PINSEL_CFG_Type 	    pinCfg;
 	SB_ERROR                retVal;
 	LPC_I2C_TypeDef			*i2cChanType;

 	retVal = SB_ERROR_NONE;

 	pinCfg.Pinmode     = 0;
 	pinCfg.OpenDrain   = 0;

 	switch (i2cChan)
 	{
 	case 0:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_28;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_27;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	case 1:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_20;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_19;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

	case 2:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_11;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_10;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	default:
 		retVal = SB_ERROR_PARAM;
 		break;
 	}

 	if (retVal == SB_ERROR_NONE)
 	{
 		i2cChanType = getI2CChannelBoardIOAPII2C(i2cChan, &retVal);
 	}

    if (retVal == SB_ERROR_NONE)
    {
    	/* Init I2C */
		I2C_Init((LPC_I2C_TypeDef *)i2cChanType, clockRate);

		/* Enable I2C operation */
		I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
    }

 	return retVal;
}

/*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 LPC_I2C_TypeDef* getI2CChannelBoardIOAPII2C(lu_uint8_t	  i2cChan,  SB_ERROR *retVal)
 {
 	 LPC_I2C_TypeDef			*i2cChanType;

     *retVal = SB_ERROR_NONE;

 	 switch (i2cChan)
	 {
	 case 0:
		i2cChanType = LPC_I2C0;
		break;

	 case 1:
		i2cChanType = LPC_I2C1;
		break;

	 case 2:
		i2cChanType = LPC_I2C2;
		break;

	 default:
		*retVal = SB_ERROR_PARAM;
		break;
     }

 	 return i2cChanType;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseBoardIOAPII2C(lu_uint8_t	  i2cChan)
{
	SB_ERROR               	retVal;
	LPC_I2C_TypeDef			*i2cChanType;

	retVal = SB_ERROR_NONE;

	i2cChanType = getI2CChannelBoardIOAPII2C(i2cChan, &retVal);

	if (retVal == SB_ERROR_NONE)
	{
		I2C_DeInit(i2cChanType);
	}

	return retVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
