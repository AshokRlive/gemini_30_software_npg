/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test SPI ADC module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_ssp.h"
#include "lpc17xx_spi.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"


#include "FactoryTest.h"
#include "FactoryTestSpiAdc.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseSpiAdc(TestSpiAdcAddrStr  *spiAdcParamsPtr);
SB_ERROR getSpiAdcCsPinPort( TestSpiAdcAddrStr       *spiAdcParamsPtr,
		                    lu_uint8_t				 *csPortPtr,
		                    lu_uint8_t				 *csPinPtr
		                  );
SB_ERROR readSpiAdc(TestSpiAdcReadStr  *spiAdcParamsPtr, lu_uint16_t *readValPtr);
SB_ERROR readLegacySpiAdc(TestSpiAdcReadStr  *spiAdcParamsPtr, lu_uint16_t *readValPtr);
SB_ERROR getSpiAdcCsPinPort( TestSpiAdcAddrStr  *spiAdcParamsPtr,
		                     lu_uint8_t			 *csPortPtr,
		                     lu_uint8_t			 *csPinPtr
		                   );
SB_ERROR deInitialiseSpiAdc(TestSpiAdcAddrStr  *spiAdcParamsPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestSpiAdcRead(TestSpiAdcReadStr *spiAdcParamsPtr)
{
	SB_ERROR                	retError;
	lu_uint16_t					readValue;

   	/* Init the SPI peripheral & configure IO pins */
    retError = initialiseSpiAdc(&spiAdcParamsPtr->addr);

	/* Do the SPI transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readSpiAdc(spiAdcParamsPtr, &readValue);
	}

	/* deInit the SPI peripheral */
	deInitialiseSpiAdc(&spiAdcParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_SPI_ADC_READ_R,
							MODULE_MESSAGE_SIZE(TestSpiAdcReadRspStr),
							(lu_uint8_t*)&readValue
						   );

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR initialiseSpiAdc(TestSpiAdcAddrStr  *spiAdcParamsPtr)
{
	PINSEL_CFG_Type 	    pinCfg;
	SSP_CFG_Type			sspConfig;
	SPI_CFG_Type			spiConfig;
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	lu_uint32_t 			csPinShift;
	SB_ERROR                retVal;

	retVal = SB_ERROR_NONE;

	pinCfg.Pinmode     = 0;
	pinCfg.OpenDrain   = 0;

	csPort = spiAdcParamsPtr->csPort;
	csPin  = spiAdcParamsPtr->csPin;

	csPinShift = 1;
	csPinShift = csPinShift << (csPin & 0x1f);

	switch (spiAdcParamsPtr->spiChan)
	{
	case 0:
		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_20;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* Chip select */
		pinCfg.Portnum     = csPort;
		pinCfg.Pinnum      = csPin;
		pinCfg.Funcnum     = PINSEL_FUNC_0;

		csPinShift = 1;
		csPinShift = csPinShift << (csPin & 0x1f);

		PINSEL_ConfigPin(&pinCfg);
		GPIO_SetValue(csPort, csPinShift);
		GPIO_SetDir(csPort, csPinShift, 1);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_23;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* MISI */
		pinCfg.Portnum     = PINSEL_PORT_1;
		pinCfg.Pinnum      = PINSEL_PIN_24;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* Init SPI */

		SSP_ConfigStructInit(&sspConfig);

		SSP_Init(LPC_SSP0, &sspConfig);

		SSP_Cmd(LPC_SSP0, ENABLE);
		break;

	case 1:
		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_7;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* Chip select */
		pinCfg.Portnum     = csPort;
		pinCfg.Pinnum      = csPin;
		pinCfg.Funcnum     = PINSEL_FUNC_0;

		csPinShift = 1;
		csPinShift = csPinShift << (csPin & 0x1f);

		PINSEL_ConfigPin(&pinCfg);
		GPIO_SetValue(csPort, csPinShift);
		GPIO_SetDir(csPort, csPinShift, 1);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_8;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* MISI */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_9;
		pinCfg.Funcnum     = PINSEL_FUNC_2;

		PINSEL_ConfigPin(&pinCfg);

		/* Init SPI */
		SSP_ConfigStructInit(&sspConfig);
		SSP_Init(LPC_SSP1, &sspConfig);

		SSP_Cmd(LPC_SSP1, ENABLE);
		break;

	case 2:
		/* Legacy SPI peripheral */

		/* SCK */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_15;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* Chip select */
		pinCfg.Portnum     = csPort;
		pinCfg.Pinnum      = csPin;
		pinCfg.Funcnum     = PINSEL_FUNC_0;

		csPinShift = 1;
		csPinShift = csPinShift << (csPin & 0x1f);

		PINSEL_ConfigPin(&pinCfg);
		GPIO_SetValue(csPort, csPinShift);
		GPIO_SetDir(csPort, csPinShift, 1);

		/* MISO */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_17;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* MISI */
		pinCfg.Portnum     = PINSEL_PORT_0;
		pinCfg.Pinnum      = PINSEL_PIN_18;
		pinCfg.Funcnum     = PINSEL_FUNC_3;

		PINSEL_ConfigPin(&pinCfg);

		/* Init SPI */
		SPI_ConfigStructInit(&spiConfig);
		SPI_Init(LPC_SPI, &spiConfig);
		break;

	default:
		retVal = SB_ERROR_PARAM;
		break;
	}

	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Read ADC using SPI
 *
 *   Detailed description
 *
 *   \param *spiAdcParamsPtr
 *   \param *readValPtr
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readSpiAdc(TestSpiAdcReadStr  *spiAdcParamsPtr, lu_uint16_t *readValPtr)
{
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	lu_uint32_t	            csPinShift;
	lu_uint8_t				txBuf[3];
	lu_uint8_t				rxBuf[3];
	lu_uint32_t				xferLen;
	SSP_DATA_SETUP_Type		xferConfig;
	LPC_SSP_TypeDef         *sspTyp;
	lu_uint16_t             adcValue;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;
	csPort = spiAdcParamsPtr->addr.csPort;
	csPin  = spiAdcParamsPtr->addr.csPin;

	csPinShift = 1;
	csPinShift = csPinShift << (csPin & 0x1f);

	txBuf[0] = (((spiAdcParamsPtr->addr.adcChan & 0x0f) >> 2)| 0x04);
	txBuf[1] = ((spiAdcParamsPtr->addr.adcChan & 0x0f) << 6);
	txBuf[2] = 0x00;

	xferConfig.tx_data  = (lu_uint8_t*)&txBuf;
	xferConfig.rx_data  = (lu_uint8_t*)&rxBuf;
	xferConfig.length   = 3;

	switch (spiAdcParamsPtr->addr.spiChan)
	{
	case 0:
		sspTyp = LPC_SSP0;
		break;

	case 1:
		sspTyp = LPC_SSP1;
		break;

	case 2:
		/* Legacy SPI peripheral */
		return readLegacySpiAdc(spiAdcParamsPtr, readValPtr);
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}

	/* Activate CS - low */
	GPIO_ClearValue(csPort, csPinShift);

	xferLen = SSP_ReadWrite(sspTyp, &xferConfig, SSP_TRANSFER_POLLING);

	/* deActivate CS - high */
	GPIO_SetValue(csPort, csPinShift);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	adcValue  = rxBuf[2];
	adcValue |= ((rxBuf[1] & 0x0f) << 8);

	/* Return the read value */
	*readValPtr = adcValue;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Read ADC using SPI
 *
 *   Detailed description
 *
 *   \param *spiAdcParamsPtr
 *   \param *readValPtr
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readLegacySpiAdc(TestSpiAdcReadStr  *spiAdcParamsPtr, lu_uint16_t *readValPtr)
{
	lu_uint8_t				csPort;
	lu_uint8_t				csPin;
	lu_uint32_t	            csPinShift;
	lu_uint8_t				txBuf[3];
	lu_uint8_t				rxBuf[3];
	lu_uint32_t				xferLen;
	SPI_DATA_SETUP_Type		xferConfig;
	lu_uint16_t             adcValue;
	SB_ERROR				retVal;

	retVal = SB_ERROR_NONE;
	csPort = spiAdcParamsPtr->addr.csPort;
	csPin  = spiAdcParamsPtr->addr.csPin;

	csPinShift = 1;
	csPinShift = csPinShift << (csPin & 0x1f);

	txBuf[0] = (((spiAdcParamsPtr->addr.adcChan & 0x0f) >> 2)| 0x04);
	txBuf[1] = ((spiAdcParamsPtr->addr.adcChan & 0x0f) << 6);
	txBuf[2] = 0x00;

	xferConfig.tx_data  = (lu_uint8_t*)&txBuf;
	xferConfig.rx_data  = (lu_uint8_t*)&rxBuf;
	xferConfig.length   = 3;

	/* Activate CS - low */
	GPIO_ClearValue(csPort, csPinShift);

	xferLen = SPI_ReadWrite(LPC_SPI, &xferConfig, SPI_TRANSFER_POLLING);

	/* deActivate CS - high */
	GPIO_SetValue(csPort, csPinShift);

	if (xferLen != xferConfig.length)
	{
		return SB_ERROR_SPI_FAIL;
	}

	adcValue  = rxBuf[2];
	adcValue |= ((rxBuf[1] & 0x0f) << 8);

	/* Return the read value */
	*readValPtr = adcValue;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseSpiAdc(TestSpiAdcAddrStr  *spiAdcParamsPtr)
{
	switch (spiAdcParamsPtr->spiChan)
	{
	case 0:
		SSP_Cmd(LPC_SSP0, DISABLE);
		return SB_ERROR_NONE;
		break;

	case 1:
		SSP_Cmd(LPC_SSP1, DISABLE);
		return SB_ERROR_NONE;
		break;

	case 2:
		/* Legacy SPI peripheral */
		return SB_ERROR_NONE;
		break;

	default:
		return SB_ERROR_PARAM;
		break;
	}
}

/*
 *********************** End of file ******************************************
 */
