/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "FactoryTest.h"
#include "FactoryTestGPIO.h"
#include "FactoryTestSPIDigiPot.h"
#include "FactoryTestSPIADDigitalPot.h"
#include "FactoryTestADC.h"
#include "FactoryTestNVRAM.h"
#include "FactoryTestSpiAdc.h"
#include "FactoryTestI2CExp.h"
#include "FactoryTestI2CADE78xx.h"
#include "FactoryTestI2CTempLM73.h"
#include "FactoryTestSPIADE78xx.h"
#include "FactoryTestSSPDMA.h"
#include "FactoryTestDAC.h"
#include "ds30PicProgram.h"
#include "FactoryTestI2CAdcMCP342X.h"
#include "FactoryTestI2CHumidity.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! List of supported message */
static const filterTableStr FactoryTestModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Bootloader test API commands */
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_GPIO_READ_PIN_C           , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_C          , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_GPIO_READ_PORT_C          , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_C         , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_ADC_READ_C                , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_SPI_DIGIPOT_WRITE_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_C    , LU_FALSE , 0      },

    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_NVRAM_READ_C              , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_NVRAM_WRITE_C             , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_SPI_ADC_READ_C            , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_EXP_READ_PORT_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PORT_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_C        , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITE_C     , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITEV2_C   , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_DAC_WRITE_C               , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C       , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C       , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C      , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST,   MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_C          , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C        , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_C     , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_C     , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_MEM_READ_C              , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_MEM_WRITE_C             , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_C , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_LCD_WRITE_C             , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_LCD_WRITE_C             , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_LCD_CMD_C               , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_LCD_CMD_C               , LU_FALSE , 1      },
    {  MODULE_MSG_TYPE_BLTST_1, MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_C     , LU_FALSE , 0      },


    {  MODULE_MSG_TYPE_BL, MODULE_MSG_ID_BL_WRITE_PIC_FIRMWARE_C            , LU_FALSE , 1       },

};

lu_uint8_t readBuffer[400] __attribute__ ((aligned (8))) __attribute__((section("ahbsram0")));

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestInit(void)
{
	/*! Check to see if we allowed to accept factory test commands ?? */
	return CANFramingAddFilter( FactoryTestModulefilterTable,
				                SU_TABLE_SIZE(FactoryTestModulefilterTable, filterTableStr)
	                          );
}


SB_ERROR FactoryTestCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	     retVal;
	SB_ERROR	     retError;
	lu_uint8_t       *buffPtr;
	lu_uint16_t      size;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
		case MODULE_MSG_TYPE_BL:
			switch (msgPtr->messageID)
			{
			case MODULE_MSG_ID_BL_WRITE_PIC_FIRMWARE_C:
				/* Message sanity check */
				if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(BootloaderPICFirmwareStr))
				{
					retVal = ds30PicProgramBlock((BootloaderPICFirmwareStr *)msgPtr->msgBufPtr);
				}
				break;

			default:
				retVal = SB_ERROR_CANC_NOT_HANDLED;
				break;
			}
			break;

		case MODULE_MSG_TYPE_BLTST:
			switch (msgPtr->messageID)
			{
				case MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestGPIOPinPortStr))
					{
						retVal = FactoryTestGPIOGetPinMode((TestGPIOPinPortStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestGPIOModeStr))
					{
						retVal = FactoryTestGPIOSetPinMode((TestGPIOModeStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_GPIO_READ_PIN_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestGPIOPinPortStr))
					{
						retVal = FactoryTestGPIOReadPin((TestGPIOPinPortStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestGPIOWriteStr))
					{
						retVal = FactoryTestGPIOWritePin((TestGPIOWriteStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_GPIO_READ_PORT_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestGPIOPortStr))
					{
						retVal = FactoryTestGPIOReadPort((TestGPIOPortStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestGPIOPortWriteStr))
					{
						retVal = FactoryTestGPIOWritePort((TestGPIOPortWriteStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_ADC_READ_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestADCPinChanStr))
					{
						retVal = FactoryTestADCRead((TestADCPinChanStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_SPI_DIGIPOT_WRITE_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSPIDigiPotWriteStr))
					{
						/*
						 * Microchip Digipot
						 */
						retVal = FactoryTestSPIDigiPotWrite((TestSPIDigiPotWriteStr*)msgPtr->msgBufPtr);

						/* Send CAN reply message */
						retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
												MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITE_R,
												MODULE_MESSAGE_SIZE(TestSPIDigiPotWriteRspStr),
												(lu_uint8_t *)&retVal
											   );
					}
					break;

				case MODULE_MSG_ID_BLTST_SPI_DIGIPOT_READ_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSPIDigiPotReadStr))
					{
						/*
						 * Microchip Digipot
						 */
						retVal = FactoryTestSPIDigiPotRead((TestSPIDigiPotReadStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_ADC_MCP342X_READ_C:
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2CAdcMCP342XReadStr))
					{
						retVal = FactoryTestI2CAdcMCP342XRead((TestI2CAdcMCP342XReadRspStr *)msgPtr->msgBufPtr);
					}
					break;


				case MODULE_MSG_ID_BLTST_DAC_WRITE_C:
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestDACStr))
					{
						retVal = FactoryTestDACWrite((TestDACStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_NVRAM_READ_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestNVRAMReadStr))
					{
						retVal = FactoryTestNVRAMRead((TestNVRAMReadStr *)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_NVRAM_WRITE_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestNVRAMWriteStr))
					{
						retVal = FactoryTestNVRAMWrite((TestNVRAMWriteStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_SPI_ADC_READ_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSpiAdcReadStr))
					{
						retVal = FactoryTestSpiAdcRead((TestSpiAdcReadStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cExpDDRReadStr))
					{
						retVal = FactoryTestI2cExpReadDDR((TestI2cExpDDRReadStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cExpDDRWriteStr))
					{
						retVal = FactoryTestI2cExpWriteDDR((TestI2cExpDDRWriteStr*)msgPtr->msgBufPtr, LU_TRUE);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cExpPinReadStr))
					{
						retVal = FactoryTestI2cExpReadPin((TestI2cExpPinReadStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cExpPinWriteStr))
					{
						retVal = FactoryTestI2cExpWritePin((TestI2cExpPinWriteStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_EXP_READ_PORT_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cExpPortReadStr))
					{
						retVal = FactoryTestI2cExpReadPort((TestI2cExpPortReadStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PORT_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cExpPortWriteStr))
					{
						retVal = FactoryTestI2cExpWritePort((TestI2cExpPortWriteStr*)msgPtr->msgBufPtr);
					}
					break;


				case MODULE_MSG_ID_BLTST_I2C_ADE78XX_INIT_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cADE78xxInitStr))
					{
						retVal = FactoryTestI2CADE78xxEnable((TestI2cADE78xxInitStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cADE78xxWriteRegStr))
					{
						retVal = FactoryTestI2CADE78xxWriteReg((TestI2cADE78xxWriteRegStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_C:
				/* Message sanity check */
				if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cADE78xxReadRegStr))
				{
					retVal = FactoryTestI2CADE78xxReadReg((TestI2cADE78xxReadRegStr*)msgPtr->msgBufPtr);
				}
				break;

				case MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITE_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSPIDigiPotWriteStr))
					{
						retVal = FactoryTestSPIADDigitalPotWrite((TestSPIDigiPotWriteStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_SPI_AD_DIGPOT_WRITEV2_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSPIDigiPotWriteV2Str))
					{
						retVal = FactoryTestSPIADDigitalPotWriteV2((TestSPIDigiPotWriteV2Str*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_TEMP_READ_DEG_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cTempLM73ReadStr))
					{
						retVal = FactoryTestI2cTempLM73Read((TestI2cTempLM73ReadStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_TEMP_WRITE_REG_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cTempLM73WriteRegStr))
					{
						retVal = FactoryTestI2cTempLM73WriteReg((TestI2cTempLM73WriteRegStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_I2C_TEMP_READ_REG_C:
					/* Message sanity check */
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cTempLM73ReadRegStr))
					{
						retVal = FactoryTestI2cTempLM73ReadReg((TestI2cTempLM73ReadRegStr*)msgPtr->msgBufPtr);
					}
					break;

				case MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_C:
					if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestNVRAMReadBuffStr))
					{
						retVal = FactoryTestNVRAMReadBuffer((TestNVRAMReadBuffStr*)msgPtr->msgBufPtr, &readBuffer[0]);

						/* Send CAN reply message */
						retVal   = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
												MODULE_MSG_ID_BLTST_NVRAM_READ_BUFFER_R,
												((TestNVRAMReadBuffStr*)msgPtr->msgBufPtr)->size,
												&readBuffer[0]
											   );
					}
					break;

				case MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_C:
					if (msgPtr->msgLen > MODULE_MESSAGE_SIZE(TestNVRAMReadStr))
					{
						buffPtr  = msgPtr->msgBufPtr;
						buffPtr += sizeof(TestNVRAMReadStr);
						size     = msgPtr->msgLen - MODULE_MESSAGE_SIZE(TestNVRAMReadStr);

						retVal = FactoryTestNVRAMWriteBuffer((TestNVRAMReadStr*)msgPtr->msgBufPtr, buffPtr, size);

						/* Send CAN reply message */
						retVal   = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
												MODULE_MSG_ID_BLTST_NVRAM_WRITE_BUFFER_R,
												MODULE_MESSAGE_SIZE(SB_ERROR),
												&retVal
											   );
					}
					break;

				default:
					retVal = SB_ERROR_CANC_NOT_HANDLED;
					break;
				}
			break;

		case MODULE_MSG_TYPE_BLTST_1:
			switch (msgPtr->messageID)
				{
					case MODULE_MSG_ID_BLTST_1_SSP_DMA_FEP_INIT_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSSPDMAStr))
						{
							retVal = FactoryTestSSPDMAEnable((TestSSPDMAStr*)msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_SSP_FEP_READ_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestFEPReadStr))
						{
							retVal = FactoryTestReadFEPChannel((TestFEPReadStr*)msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_ADE78xx_ENABLE_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSpiADE78xxInitStr))
						{
							retVal = FactoryTestSPIADE78xxEnable((TestSpiADE78xxInitStr*)msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_ADE78xx_READ_REG_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSpiADE78xxReadRegStr))
						{
							retVal = FactoryTestSPIADE78xxReadReg((TestSpiADE78xxReadRegStr*)msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_ADE78xx_WRITE_REG_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestSpiADE78xxWriteRegStr))
						{
							retVal = FactoryTestSPIADE78xxWriteReg((TestSpiADE78xxWriteRegStr*)msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_I2C_ADE78xx_READ_PEAK_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cADE78xxReadPeakStr))
						{
							retVal = FactoryTestI2CADE78xxReadPeak((TestI2cADE78xxReadPeakStr*)msgPtr->msgBufPtr);
						}
						break;

					case MODULE_MSG_ID_BLTST_1_MEM_READ_C:
						break;

					case MODULE_MSG_ID_BLTST_1_MEM_WRITE_C:
						break;

					case MODULE_MSG_ID_BLTST_1_LCD_WRITE_C:
						break;

					case MODULE_MSG_ID_BLTST_1_LCD_CMD_C:
						break;

					case MODULE_MSG_ID_BLTST_1_I2C_HUMIDITY_READ_C:
						/* Message sanity check */
						if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(TestI2cHumidityReadStr))
						{
							retVal = FactoryTestI2CHumidityRead((TestI2cHumidityReadStr*)msgPtr->msgBufPtr);
						}
						break;

					default:
						retVal = SB_ERROR_CANC_NOT_HANDLED;
						break;
				}
		break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
	}


	return retVal;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
