/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test NVRAM driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "string.h"

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "FactoryTestNVRAM.h"

#include "systemTime.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_PAGE_WRITE					(16)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseI2cNVRAM(TestNVRAMAddrStr  *i2cNVRAMParamsPtr);
SB_ERROR writeI2cNVRAM(TestNVRAMWriteStr *i2cNVRAMParamsPtr);
SB_ERROR writeI2cPageNVRAM(TestNVRAMReadStr *i2cNVRAMParamsPtr, lu_uint8_t *buffPtr);
SB_ERROR readI2cNVRAM(TestNVRAMReadStr *i2cNVRAMParamsPtr, lu_uint8_t *retValue);
LPC_I2C_TypeDef * getI2CChannel(TestNVRAMAddrStr *i2cNVRAMParamsPtr, SB_ERROR *retVal);
SB_ERROR deInitialiseI2cNVRAM(TestNVRAMAddrStr  *i2cNVRAMParamsPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestNVRAMWrite(TestNVRAMWriteStr *i2cNVRAMParamsPtr)
{
	SB_ERROR               retError;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cNVRAM(&i2cNVRAMParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = writeI2cNVRAM(i2cNVRAMParamsPtr);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cNVRAM(&i2cNVRAMParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_ADE78XX_WRITE_R,
							MODULE_MESSAGE_SIZE(TestErrorRspStr),
							&retError
						   );

	return SB_ERROR_NONE;
}

 SB_ERROR FactoryTestNVRAMRead(TestNVRAMReadStr *i2cNVRAMParamsPtr)
 {
	 TestNVRAMReadRspStr	nvramReadValue;
	 SB_ERROR               retError;

	 /* Init the I2C peripheral & configure IO pins */
	 retError = initialiseI2cNVRAM(&i2cNVRAMParamsPtr->addr);

	 /* Do the I2C transfer on the selected bus */
	 /* Do the I2C transfer on the selected bus */
	 if (retError == SB_ERROR_NONE)
	 {
	 	retError = readI2cNVRAM(i2cNVRAMParamsPtr, (lu_uint8_t *)&nvramReadValue);
	 }

	 /* deInit the I2C peripheral */
	 deInitialiseI2cNVRAM(&i2cNVRAMParamsPtr->addr);

	 /* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_ADE78XX_READ_R,
							MODULE_MESSAGE_SIZE(TestNVRAMReadRspStr),
							(lu_uint8_t *)&nvramReadValue
						   );
	 return SB_ERROR_NONE;
 }

SB_ERROR FactoryTestNVRAMWriteBuffer(TestNVRAMReadStr *i2cNVRAMParamsPtr, lu_uint8_t *bufPtr, lu_uint16_t size)
{
	SB_ERROR              retError;
	lu_uint16_t           count;
	lu_uint8_t            *writeDataPtr;
	TestNVRAMWriteStr	  i2cNVRAMWriteParams;
	TestNVRAMReadStr      i2cNVRAMPageWriteParams;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cNVRAM(&i2cNVRAMParamsPtr->addr);

	if (retError == SB_ERROR_NONE)
	{
		/* Copy to aligned buffer (incase this is required ?) */
		//memcpy(&eepromMemoryCache[0], bufPtr, size);

		count                        = size;
		writeDataPtr                 = bufPtr;
		i2cNVRAMWriteParams.addr     = i2cNVRAMParamsPtr->addr;
		i2cNVRAMPageWriteParams.addr = i2cNVRAMWriteParams.addr;

		while (count && retError == SB_ERROR_NONE)
		{
			if ((i2cNVRAMWriteParams.addr.offset & 0x0f) == 0 && count >= MAX_PAGE_WRITE)
			{
				/* Aligned, so Write a page */
				i2cNVRAMPageWriteParams.addr.offset = i2cNVRAMWriteParams.addr.offset;
				retError = writeI2cPageNVRAM(&i2cNVRAMPageWriteParams, writeDataPtr);

				count        				      -= MAX_PAGE_WRITE;
				writeDataPtr                      += MAX_PAGE_WRITE;
				i2cNVRAMWriteParams.addr.offset  += MAX_PAGE_WRITE;
			}
			else
			{
				/* Not aligned, so write bytes */
				i2cNVRAMWriteParams.value = *writeDataPtr;

				retError = writeI2cNVRAM(&i2cNVRAMWriteParams);

				count--;
				writeDataPtr++;
				i2cNVRAMWriteParams.addr.offset++;
			}
		}
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cNVRAM(&i2cNVRAMParamsPtr->addr);

	return retError;
}

 SB_ERROR FactoryTestNVRAMReadBuffer(TestNVRAMReadBuffStr *testNVRAMReadBuffPtr, lu_uint8_t *bufPtr)
 {
	 lu_uint8_t				nvramReadValue;
	 SB_ERROR               retError;
	 lu_uint16_t            count;
	 lu_uint8_t            *readDataPtr;
	 TestNVRAMReadStr       i2cNVRAMParams;
	 lu_uint16_t            size;

	 i2cNVRAMParams.addr = testNVRAMReadBuffPtr->addr;
	 size                = testNVRAMReadBuffPtr->size;

	 /* Init the I2C peripheral & configure IO pins */
	 retError = initialiseI2cNVRAM(&testNVRAMReadBuffPtr->addr);

	 if (retError == SB_ERROR_NONE)
	 {
		 count       = size;
		 readDataPtr = bufPtr;

		 while (count)
		 {
			retError = readI2cNVRAM(&i2cNVRAMParams, (lu_uint8_t *)&nvramReadValue);
			if (retError != SB_ERROR_NONE)
			{
				break;
			}
			else
			{
				*readDataPtr = nvramReadValue;
			}

			count--;
			readDataPtr++;
			i2cNVRAMParams.addr.offset++;
		 }
	 }

	 /* deInit the I2C peripheral */
	 deInitialiseI2cNVRAM(&testNVRAMReadBuffPtr->addr);

	 return retError;
 }

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 SB_ERROR initialiseI2cNVRAM(TestNVRAMAddrStr  *i2cNVRAMParamsPtr)
 {
 	PINSEL_CFG_Type 	    pinCfg;
 	SB_ERROR                retVal;
 	LPC_I2C_TypeDef			*i2cChanType;


 	retVal = SB_ERROR_NONE;

 	pinCfg.Pinmode     = 0;
 	pinCfg.OpenDrain   = 0;

 	switch (i2cNVRAMParamsPtr->i2cChan)
 	{
 	case 0:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_28;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_27;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	case 1:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_20;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_19;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

	case 2:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_11;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_10;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	default:
 		retVal = SB_ERROR_PARAM;
 		break;
 	}

 	if (retVal == SB_ERROR_NONE)
 	{
 		i2cChanType = getI2CChannel(i2cNVRAMParamsPtr, &retVal);
 	}

    if (retVal == SB_ERROR_NONE)
    {
    	/* Init I2C */
		I2C_Init((LPC_I2C_TypeDef *)i2cChanType, 100000);

		/* Enable I2C operation */
		I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
    }
 	return retVal;
 }

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 SB_ERROR writeI2cNVRAM(TestNVRAMWriteStr *i2cNVRAMParamsPtr)
 {
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
	 lu_uint32_t				slaveAddrOffset;
	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2CChannel(&i2cNVRAMParamsPtr->addr, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* AT24C08 type EEPROM */

		 /* Slave address + offset bits */
		 slaveAddrOffset  = ((i2cNVRAMParamsPtr->addr.addr << 2) & 0xfc);
		 slaveAddrOffset |= ((i2cNVRAMParamsPtr->addr.offset >> 8)) & 0x03;

		 /* Word Address */
		 txData[0]						= (i2cNVRAMParamsPtr->addr.offset & 0xff);
		 /* Data Value */
		 txData[1]						= i2cNVRAMParamsPtr->value;

		 txferCfg.sl_addr7bit     	    = slaveAddrOffset;
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 2;
		 txferCfg.rx_data		 	 	= NULL;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
			 /* Now do Acknowledge polling to wait for write to complete - should be no more than 5ms */
			 STDelayTime(6);

#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }

	 return retError;
 }

 /*!
   ******************************************************************************
   *   \brief Brief description
   *
   *   Detailed description
   *
   *   \param parameterName Description
   *
   *
   *   \return
   *
   ******************************************************************************
   */
  SB_ERROR writeI2cPageNVRAM(TestNVRAMReadStr *i2cNVRAMParamsPtr, lu_uint8_t *buffPtr)
  {
 	 SB_ERROR                	retError;
 	 LPC_I2C_TypeDef			*i2cChanType;
 	 I2C_M_SETUP_Type			txferCfg;
 	 lu_uint32_t				slaveAddrOffset;
 	 lu_uint32_t				count;
 	 lu_uint8_t			        txData[MAX_PAGE_WRITE + 3];

 	 retError = SB_ERROR_NONE;

 	 i2cChanType = getI2CChannel(&i2cNVRAMParamsPtr->addr, &retError);
 	 if (retError == SB_ERROR_NONE)
 	 {
 		 /* AT24C08 type EEPROM */

 		 /* Slave address + offset bits */
 		 slaveAddrOffset  = ((i2cNVRAMParamsPtr->addr.addr << 2) & 0xfc);
 		 slaveAddrOffset |= ((i2cNVRAMParamsPtr->addr.offset >> 8)) & 0x03;

 		 /* Word Address */
 		 txData[0]						= (i2cNVRAMParamsPtr->addr.offset & 0xff);

 		/* Data Value */
 	 	memcpy(&txData[1], buffPtr, MAX_PAGE_WRITE);


 		 txferCfg.sl_addr7bit     	    = slaveAddrOffset;
 		 txferCfg.tx_data		  		= txData;
 		 txferCfg.tx_length       		= (MAX_PAGE_WRITE + 1);;
 		 txferCfg.rx_data		 	 	= NULL;
 		 txferCfg.rx_length       		= 0;
 		 txferCfg.retransmissions_max 	= 2;

 		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
 		 {
 			 retError = SB_ERROR_I2C_FAIL;
 		 }
 		 else
 		 {
 			 /* Now do Acknowledge polling to wait for write to complete - should be no more than 5ms */
 			 STDelayTime(6);

 #if 0
 			 /* Must wait for transfer to complete!! */
 			 for (count = 0; count < 10000; count++)
 			 {
 				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
 					 break;
 			 }
 #endif
 		 }
 	 }

 	 return retError;
  }

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 SB_ERROR readI2cNVRAM(TestNVRAMReadStr *i2cNVRAMParamsPtr, lu_uint8_t *retValue)
 {
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
	 lu_uint32_t				slaveAddrOffset;
	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];
	 lu_uint8_t					rxData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2CChannel(&i2cNVRAMParamsPtr->addr, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* AT24C08 type EEPROM */

		 /* Slave address + offset bits */
		 slaveAddrOffset  = ((i2cNVRAMParamsPtr->addr.addr << 2) & 0xfc);
		 slaveAddrOffset |= ((i2cNVRAMParamsPtr->addr.offset >> 8) & 0x03);

		 /* Word Address */
		 txData[0]						= (i2cNVRAMParamsPtr->addr.offset & 0xff);

		 txferCfg.sl_addr7bit     	    = slaveAddrOffset;
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 1;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
			 STDelayTime(6);
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 *retValue = rxData[0];
		 }
	 }

	 return retError;
 }

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
LPC_I2C_TypeDef* getI2CChannel(TestNVRAMAddrStr *i2cNVRAMParamsPtr,  SB_ERROR *retVal)
{
 	 LPC_I2C_TypeDef			*i2cChanType;

     *retVal = SB_ERROR_NONE;

 	 switch (i2cNVRAMParamsPtr->i2cChan)
	 {
	 case 0:
		i2cChanType = LPC_I2C0;
		break;

	 case 1:
		i2cChanType = LPC_I2C1;
		break;

	 case 2:
		i2cChanType = LPC_I2C2;
		break;

	 default:
		*retVal = SB_ERROR_PARAM;
		break;
     }

 	 return i2cChanType;
  }

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseI2cNVRAM(TestNVRAMAddrStr  *i2cNVRAMParamsPtr)
{
	SB_ERROR                	retVal;
	LPC_I2C_TypeDef			*i2cChanType;

	retVal = SB_ERROR_NONE;

	i2cChanType = getI2CChannel(i2cNVRAMParamsPtr, &retVal);

	if (retVal == SB_ERROR_NONE)
	{
	  I2C_DeInit(i2cChanType);
	}

	return retVal;
}


/*
 *********************** End of file ******************************************
 */
