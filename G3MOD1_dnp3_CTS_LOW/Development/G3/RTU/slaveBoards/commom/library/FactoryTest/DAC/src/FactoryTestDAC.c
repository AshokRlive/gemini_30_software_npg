/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test DAC
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_adc.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_dac.h"

#include "FactoryTest.h"
#include "FactoryTestDAC.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void configDacPin(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR FactoryTestDACWrite(TestDACStr *dacPtr)
{
	TestErrorRspStr     rsp;


	/* Setup and configure pin */
	configDacPin();

	DAC_Init(LPC_DAC);

	DAC_UpdateValue(LPC_DAC, dacPtr->value);
	
	rsp.errorCode = SB_ERROR_NONE;
	
	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_DAC_WRITE_R,
						sizeof(TestErrorRspStr),
						(lu_uint8_t *)&rsp
					   );
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
void configDacPin(void)
{
	PINSEL_CFG_Type 	pinCfg;

	pinCfg.Funcnum   = PINSEL_FUNC_2;
	pinCfg.Portnum   = PINSEL_PORT_0;
	pinCfg.Pinnum    = PINSEL_PIN_26;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	pinCfg.Pinmode   = 0;

	PINSEL_ConfigPin(&pinCfg);
}

/*
 *********************** End of file ******************************************
 */
