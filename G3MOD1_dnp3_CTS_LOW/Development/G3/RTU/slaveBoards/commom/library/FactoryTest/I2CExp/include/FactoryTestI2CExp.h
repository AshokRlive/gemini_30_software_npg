/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C IO Expander driver header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _FACTORYTESTI2CEXP_INCLUDED
#define _FACTORYTESTI2CEXP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "CANProtocolCodec.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FactoryTestI2cExpWritePin(TestI2cExpPinWriteStr *i2cExpParamsPtr);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR FactoryTestI2cExpReadPin(TestI2cExpPinReadStr *i2cExpParamsPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FactoryTestI2cExpWritePort(TestI2cExpPortWriteStr *i2cExpParamsPtr);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR FactoryTestI2cExpReadPort(TestI2cExpPortReadStr *i2cExpParamsPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FactoryTestI2cExpWriteDDR(TestI2cExpDDRWriteStr *i2cExpParamsPtr, lu_bool_t sendRsp);

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
extern SB_ERROR FactoryTestI2cExpReadDDR(TestI2cExpDDRReadStr *i2cExpParamsPtr);

#endif /* _FACTORYTESTI2CEXP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
