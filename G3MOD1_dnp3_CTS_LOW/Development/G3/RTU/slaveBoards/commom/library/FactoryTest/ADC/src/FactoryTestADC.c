/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test SPI Digi Pot module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   23/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_adc.h"
#include "lpc17xx_pinsel.h"

#include "FactoryTest.h"
#include "FactoryTestADC.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void configAdcPin(lu_uint8_t chan);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


SB_ERROR FactoryTestADCRead(TestADCPinChanStr *adcPinChanPtr)
{
	TestADCReadStr		adcRead;
	lu_uint32_t			adcRate;
	lu_uint8_t			adcChan;
	ADC_TYPE_INT_OPT	adcTypIntOpt;
	lu_uint32_t			count;

	switch (adcPinChanPtr->chan)
	{
	case 0:
		adcTypIntOpt = ADC_ADINTEN0;
		break;

	case 1:
		adcTypIntOpt = ADC_ADINTEN1;
		break;

	case 2:
		adcTypIntOpt = ADC_ADINTEN2;
		break;

	case 3:
		adcTypIntOpt = ADC_ADINTEN3;
		break;

	case 4:
		adcTypIntOpt = ADC_ADINTEN4;
		break;

	case 5:
		adcTypIntOpt = ADC_ADINTEN5;
		break;

	case 6:
		adcTypIntOpt = ADC_ADINTEN6;
		break;

	case 7:
		adcTypIntOpt = ADC_ADINTEN7;
		break;

	default:
		return SB_ERROR_PARAM;
	}

	adcChan = (adcPinChanPtr->chan & 0x07);

	/* Setup and configure pin */
	configAdcPin(adcChan);

	/* Setup ADC rate - default to 2khz */
	adcRate = 2;
	if (adcPinChanPtr->rateKhz <= 2 && adcPinChanPtr->rateKhz != 0)
	{
		adcRate = (1000 * adcPinChanPtr->rateKhz);
	}
	ADC_Init(LPC_ADC, adcRate);

	ADC_IntConfig(LPC_ADC, adcTypIntOpt, DISABLE);

	/* Select channel */
	ADC_ChannelCmd(LPC_ADC, adcChan, ENABLE);

	/* Delay required for sample & hold */
	for (count = 0; count < 10000; count++)
	{
	}

	/* Do the ADC conversion */
	ADC_StartCmd(LPC_ADC, ADC_START_NOW);

	/* Wait for conversion & get value */
	while (!(ADC_ChannelGetStatus(LPC_ADC, adcChan, ADC_DATA_DONE)));

	adcRead.value = ADC_ChannelGetData(LPC_ADC, adcChan);

	/* Don't disable for now, other IOManager will break */

	ADC_ChannelCmd(LPC_ADC, adcChan, DISABLE);

//	ADC_DeInit(LPC_ADC);

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_ADC_READ_R,
						sizeof(TestADCReadStr),
						(lu_uint8_t *)&adcRead
					   );
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
void configAdcPin(lu_uint8_t chan)
{
	PINSEL_CFG_Type 	pinCfg;

	switch ((chan & 0x07))
	{
	case 0:
		pinCfg.Portnum   = PINSEL_PORT_0;
		pinCfg.Pinnum    = 23;
		pinCfg.Funcnum   = 1;
		break;

	case 1:
		pinCfg.Portnum   = PINSEL_PORT_0;
		pinCfg.Pinnum    = 24;
		pinCfg.Funcnum   = 1;
		break;

	case 2:
		pinCfg.Portnum   = PINSEL_PORT_0;
		pinCfg.Pinnum    = 25;
		pinCfg.Funcnum   = 1;
		break;

	case 3:
		pinCfg.Portnum   = PINSEL_PORT_0;
		pinCfg.Pinnum    = 26;
		pinCfg.Funcnum   = 1;
		break;

	case 4:
		pinCfg.Portnum   = PINSEL_PORT_1;
		pinCfg.Pinnum    = 30;
		pinCfg.Funcnum   = 3;
		break;

	case 5:
		pinCfg.Portnum   = PINSEL_PORT_1;
		pinCfg.Pinnum    = 31;
		pinCfg.Funcnum   = 3;
		break;

	case 6:
		pinCfg.Portnum   = PINSEL_PORT_0;
		pinCfg.Pinnum    = 3;
		pinCfg.Funcnum   = 2;
		break;

	case 7:
		pinCfg.Portnum   = PINSEL_PORT_0;
		pinCfg.Pinnum    = 2;
		pinCfg.Funcnum   = 2;
		break;
	}


	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	//pinCfg.Pinmode   = 0;
	pinCfg.Pinmode   = PINSEL_PINMODE_TRISTATE;

	PINSEL_ConfigPin(&pinCfg);
}

/*
 *********************** End of file ******************************************
 */
