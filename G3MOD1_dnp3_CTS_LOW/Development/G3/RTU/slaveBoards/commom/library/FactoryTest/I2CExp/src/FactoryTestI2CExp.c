/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test I2C IO Expansion driver module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   05/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "FactoryTestI2CExp.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define PCA9555_ADDR					0x20

#define PCA9555_CMD_INPUT_PORT_0		0x00
#define PCA9555_CMD_INPUT_PORT_1		0x01
#define PCA9555_CMD_OUTPUT_PORT_0		0x02
#define PCA9555_CMD_OUTPUT_PORT_1		0x03
#define PCA9555_CMD_POLINV_PORT_0		0x04
#define PCA9555_CMD_POLINV_PORT_1		0x05
#define PCA9555_CMD_CONFIG_DDR_PORT_0	0x06
#define PCA9555_CMD_CONFIG_DDR_PORT_1	0x07

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR initialiseI2cExp(TestI2cExpAddrStr  *i2cExpParamsPtr);
SB_ERROR writeI2cExpPort(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t portValue);
SB_ERROR readI2cExpPort(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t *portValue);
SB_ERROR writeI2cExpDDR(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t ddrValue);
SB_ERROR readI2cExpDDR(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t *ddrValue);
LPC_I2C_TypeDef* getI2cExpChannel(TestI2cExpAddrStr *i2cExpParamsPtr,  SB_ERROR *retVal);
SB_ERROR deInitialiseI2cExp(TestI2cExpAddrStr  *i2cExpParamsPtr);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestI2cExpWritePin(TestI2cExpPinWriteStr *i2cExpParamsPtr)
{
	SB_ERROR               retError;
	lu_uint16_t			   portValue;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readI2cExpPort(&i2cExpParamsPtr->addr, &portValue);

		if (i2cExpParamsPtr->value)
		{
			/* Set pin bit */
			portValue |= ( 0x01 << (i2cExpParamsPtr->pin & 0x0f));
		}
		else
		{
			/* Clear pin bit */
			portValue &= ~( 0x01 << (i2cExpParamsPtr->pin & 0x0f));
		}

		retError = writeI2cExpPort(&i2cExpParamsPtr->addr, portValue);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PIN_R,
							0,
							0L
						   );

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2cExpReadPin(TestI2cExpPinReadStr *i2cExpParamsPtr)
{
	SB_ERROR               retError;
	lu_uint16_t			   portValue;
	lu_uint8_t			   pinValue;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readI2cExpPort(&i2cExpParamsPtr->addr, &portValue);

		pinValue = (portValue >> i2cExpParamsPtr->pin);
		pinValue &= 0x01;
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_EXP_READ_PIN_R,
							sizeof(TestI2cExpPinReadRspStr),
							(lu_uint8_t *)&pinValue
						   );

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2cExpWritePort(TestI2cExpPortWriteStr *i2cExpParamsPtr)
{
	SB_ERROR               retError;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = writeI2cExpPort(&i2cExpParamsPtr->addr, i2cExpParamsPtr->value);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_PORT_R,
							0,
							0L
						   );

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2cExpReadPort(TestI2cExpPortReadStr *i2cExpParamsPtr)
{
	SB_ERROR               retError;
	lu_uint16_t			   portValue;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readI2cExpPort(&i2cExpParamsPtr->addr, &portValue);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_EXP_READ_PORT_R,
							sizeof(TestI2cExpPortReadRspStr),
							(lu_uint8_t *)&portValue
						   );

	return SB_ERROR_NONE;
}


SB_ERROR FactoryTestI2cExpWriteDDR(TestI2cExpDDRWriteStr *i2cExpParamsPtr, lu_bool_t sendRsp)
{
	SB_ERROR               retError;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = writeI2cExpDDR(&i2cExpParamsPtr->addr, i2cExpParamsPtr->value);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cExp(&i2cExpParamsPtr->addr);

	if (sendRsp)
	{
		/* Send CAN reply message */
		retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
								MODULE_MSG_ID_BLTST_I2C_EXP_WRITE_DDR_R,
								0,
								0L
							   );
	}

	return SB_ERROR_NONE;
}

SB_ERROR FactoryTestI2cExpReadDDR(TestI2cExpDDRReadStr *i2cExpParamsPtr)
{
	SB_ERROR               retError;
	lu_uint16_t			   ddrValue;

	/* Init the I2C peripheral & configure IO pins */
	retError = initialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Do the I2C transfer on the selected bus */
	if (retError == SB_ERROR_NONE)
	{
		retError = readI2cExpDDR(&i2cExpParamsPtr->addr, &ddrValue);
	}

	/* deInit the I2C peripheral */
	deInitialiseI2cExp(&i2cExpParamsPtr->addr);

	/* Send CAN reply message */
	retError = CANCSendMCM( MODULE_MSG_TYPE_BLTST,
							MODULE_MSG_ID_BLTST_I2C_EXP_READ_DDR_R,
							sizeof(TestI2cExpDDRReadRspStr),
							(lu_uint8_t*)&ddrValue
						   );

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR initialiseI2cExp(TestI2cExpAddrStr  *i2cExpParamsPtr)
{
 	PINSEL_CFG_Type 	    pinCfg;
 	SB_ERROR                retVal;
 	LPC_I2C_TypeDef			*i2cChanType;

 	retVal = SB_ERROR_NONE;

 	pinCfg.Pinmode     = 0;
 	pinCfg.OpenDrain   = 0;

 	switch (i2cExpParamsPtr->i2cChan)
 	{
 	case 0:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_28;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_27;
 		pinCfg.Funcnum     = PINSEL_FUNC_1;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	case 1:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_20;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_19;
 		pinCfg.Funcnum     = PINSEL_FUNC_3;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

	case 2:
 		/* SCL */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_11;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);

 		/* SDA */
 		pinCfg.Portnum     = PINSEL_PORT_0;
 		pinCfg.Pinnum      = PINSEL_PIN_10;
 		pinCfg.Funcnum     = PINSEL_FUNC_2;

 		PINSEL_ConfigPin(&pinCfg);
 		break;

 	default:
 		retVal = SB_ERROR_PARAM;
 		break;
 	}

 	if (retVal == SB_ERROR_NONE)
 	{
 		i2cChanType = getI2cExpChannel(i2cExpParamsPtr, &retVal);
 	}

    if (retVal == SB_ERROR_NONE)
    {
    	/* Init I2C */
		I2C_Init((LPC_I2C_TypeDef *)i2cChanType, 100000);

		/* Enable I2C operation */
		I2C_Cmd((LPC_I2C_TypeDef *)i2cChanType, ENABLE);
    }
 	return retVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
 SB_ERROR writeI2cExpPort(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t portValue)
 {
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
//	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2cExpChannel(i2cExpParamsPtr, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_OUTPUT_PORT_0;
		 /* Data Value */
		 txData[1]						= (portValue & 0xff);
		 txData[2]						= (portValue >> 8);

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (i2cExpParamsPtr->addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 3;
		 txferCfg.rx_data		 	 	= NULL;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }
	 return retError;
 }

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readI2cExpPort(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t *portValue)
{
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
//	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];
	 lu_uint8_t					rxData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2cExpChannel(i2cExpParamsPtr, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_INPUT_PORT_0;

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (i2cExpParamsPtr->addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 *portValue = *(lu_uint16_t *)&rxData[0];
		 }
	 }
	 return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR writeI2cExpDDR(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t ddrValue)
{
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
//	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2cExpChannel(i2cExpParamsPtr, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_CONFIG_DDR_PORT_0;
		 /* Data Value */
		 txData[1]						= (ddrValue & 0xff);
		 txData[2]						= (ddrValue >> 8);

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (i2cExpParamsPtr->addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 3;
		 txferCfg.rx_data		 	 	= NULL;
		 txferCfg.rx_length       		= 0;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
		 }
	 }
	 return retError;
 }

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR readI2cExpDDR(TestI2cExpAddrStr *i2cExpParamsPtr, lu_uint16_t *ddrValue)
{
	 SB_ERROR                	retError;
	 LPC_I2C_TypeDef			*i2cChanType;
	 I2C_M_SETUP_Type			txferCfg;
//	 lu_uint32_t				count;
	 lu_uint8_t					txData[3];
	 lu_uint8_t					rxData[3];

	 retError = SB_ERROR_NONE;

	 i2cChanType = getI2cExpChannel(i2cExpParamsPtr, &retError);
	 if (retError == SB_ERROR_NONE)
	 {
		 /* Command */
		 txData[0]						= PCA9555_CMD_CONFIG_DDR_PORT_0;

		 txferCfg.sl_addr7bit     	    = (PCA9555_ADDR | (i2cExpParamsPtr->addr & 0x07));
		 txferCfg.tx_data		  		= txData;
		 txferCfg.tx_length       		= 1;
		 txferCfg.rx_data		 	 	= rxData;
		 txferCfg.rx_length       		= 2;
		 txferCfg.retransmissions_max 	= 2;

		 if (I2C_MasterTransferData(i2cChanType, &txferCfg, I2C_TRANSFER_POLLING) == ERROR)
		 {
			 retError = SB_ERROR_I2C_FAIL;
		 }
		 else
		 {
#if 0
			 /* Must wait for transfer to complete!! */
			 for (count = 0; count < 10000; count++)
			 {
				 if (I2C_MasterTransferComplete(i2cChanType) == TRUE)
					 break;
			 }
#endif
			 *ddrValue = *(lu_uint16_t *)&rxData[0];
		 }
	 }
	 return retError;
}

 /*!
  ******************************************************************************
  *   \brief Brief description
  *
  *   Detailed description
  *
  *   \param parameterName Description
  *
  *
  *   \return
  *
  ******************************************************************************
  */
 LPC_I2C_TypeDef* getI2cExpChannel(TestI2cExpAddrStr *i2cExpParamsPtr,  SB_ERROR *retVal)
 {
 	 LPC_I2C_TypeDef			*i2cChanType;

     *retVal = SB_ERROR_NONE;

 	 switch (i2cExpParamsPtr->i2cChan)
	 {
	 case 0:
		i2cChanType = LPC_I2C0;
		break;

	 case 1:
		i2cChanType = LPC_I2C1;
		break;

	 case 2:
		i2cChanType = LPC_I2C2;
		break;

	 default:
		*retVal = SB_ERROR_PARAM;
		break;
     }

 	 return i2cChanType;
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR deInitialiseI2cExp(TestI2cExpAddrStr  *i2cExpParamsPtr)
{
	SB_ERROR               	retVal;
	LPC_I2C_TypeDef			*i2cChanType;

	retVal = SB_ERROR_NONE;

	i2cChanType = getI2cExpChannel(i2cExpParamsPtr, &retVal);

	if (retVal == SB_ERROR_NONE)
	{
		I2C_DeInit(i2cChanType);
	}

	return retVal;
}

/*
 *********************** End of file ******************************************
 */
