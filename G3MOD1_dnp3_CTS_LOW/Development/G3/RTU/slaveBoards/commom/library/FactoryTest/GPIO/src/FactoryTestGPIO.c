/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Factory Test GPIO API module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "FactoryTest.h"
#include "FactoryTestGPIO.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

lu_uint8_t gpioGetPinPortFunc(lu_uint8_t port, lu_uint8_t pin);
lu_uint8_t gpioGetPinPortMode(lu_uint8_t port, lu_uint8_t pin);
lu_uint8_t gpioGetPinPortOpenDrain(lu_uint8_t port, lu_uint8_t pin);
lu_uint8_t gpioGetPinPortDir(lu_uint8_t port, lu_uint8_t pin);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR FactoryTestGPIOGetPinMode(TestGPIOPinPortStr *pinPortPtr)
{
	TestGPIOModeStr     gpioMode;

	gpioMode.port     = pinPortPtr->port;
	gpioMode.pin      = pinPortPtr->pin;

	/* Some work required here, since NXP lib does not allow read of pin mode ?? */
	gpioMode.pinFunc   = gpioGetPinPortFunc(gpioMode.port, gpioMode.pin);
	gpioMode.pinMode   = gpioGetPinPortMode(gpioMode.port, gpioMode.pin);
	gpioMode.openDrain = gpioGetPinPortOpenDrain(gpioMode.port, gpioMode.pin);

	gpioMode.dir       = gpioGetPinPortDir(gpioMode.port, gpioMode.pin);

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
						MODULE_MSG_ID_BLTST_GPIO_GET_PIN_MODE_R,
						sizeof(TestGPIOModeStr),
						(lu_uint8_t *)&gpioMode
					   );
}


SB_ERROR FactoryTestGPIOSetPinMode(TestGPIOModeStr *gpioModePtr)
{
	PINSEL_CFG_Type 	pinCfg;
	lu_uint32_t	        pinShift;

	pinShift = 1;
	pinShift = pinShift << (gpioModePtr->pin & 0x1f);

	pinCfg.Portnum   = gpioModePtr->port;
	pinCfg.Pinnum    = gpioModePtr->pin;
	pinCfg.Funcnum   = gpioModePtr->pinFunc;
	pinCfg.Pinmode   = gpioModePtr->pinMode;
	pinCfg.OpenDrain = gpioModePtr->openDrain;

	PINSEL_ConfigPin(&pinCfg);

	/* Set pin direction - 0=input;1=output */
	GPIO_SetDir(gpioModePtr->port, pinShift, gpioModePtr->dir);

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_GPIO_SET_PIN_MODE_R,
			            0,
	                    NULL
	                   );
}

SB_ERROR FactoryTestGPIOReadPin(TestGPIOPinPortStr *gpioPinPortPtr)
{
	TestGPIOReadStr     gpioReadValue;

	gpioReadValue.value = (lu_uint8_t)((FIO_ReadValue(gpioPinPortPtr->port) >> gpioPinPortPtr->pin) & 0x00000001);

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_GPIO_READ_PIN_R,
	                    sizeof(TestGPIOReadStr),
	                    (lu_uint8_t *)&gpioReadValue
	                   );
}

SB_ERROR FactoryTestGPIOWritePin(TestGPIOWriteStr *gpioWritePtr)
{
	lu_uint32_t	        pinShift;

	pinShift = 1;
	pinShift = pinShift << (gpioWritePtr->pin & 0x1f);

	if (gpioWritePtr->value)
	{
		GPIO_SetValue(gpioWritePtr->port, pinShift);
	}
	else
	{
		GPIO_ClearValue(gpioWritePtr->port, pinShift);
	}

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_GPIO_WRITE_PIN_R,
			            0,
	                    NULL
	                   );
}

SB_ERROR FactoryTestGPIOReadPort(TestGPIOPortStr *gpioPortPtr)
{
	TestGPIOPortReadStr     gpioReadValue;

	gpioReadValue.value = (lu_uint32_t)FIO_ReadValue(gpioPortPtr->port);

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_GPIO_READ_PORT_R,
	                    sizeof(TestGPIOPortReadStr),
	                    (lu_uint8_t *)&gpioReadValue
	                   );
}

SB_ERROR FactoryTestGPIOWritePort(TestGPIOPortWriteStr *gpioPortWritePtr)
{
	FIO_SetValue(gpioPortWritePtr->port, gpioPortWritePtr->value);

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BLTST,
			            MODULE_MSG_ID_BLTST_GPIO_WRITE_PORT_R,
			            0,
	                    NULL
	                   );
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_uint8_t gpioGetPinPortFunc(lu_uint8_t port, lu_uint8_t pin)
{
	lu_uint32_t *pinConPtr;
	lu_uint32_t pinSelIdx;
	lu_uint32_t pinNumSel;
	lu_uint8_t  pinFunc;

	pinNumSel = pin;
	pinSelIdx = (2 * port);
	if (pinNumSel >= 16)
	{
		pinNumSel -= 16;
		pinSelIdx++;
	}

	pinConPtr = (lu_uint32_t *)&LPC_PINCON->PINSEL0;

	pinFunc = (*(lu_uint32_t *)(pinConPtr + pinSelIdx) >> (2 * pinNumSel)) & 0x03;

	return pinFunc;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t gpioGetPinPortMode(lu_uint8_t port, lu_uint8_t pin)
{
	lu_uint32_t *pinConPtr;
	lu_uint32_t pinModedx;
	lu_uint32_t pinNumSel;
	lu_uint8_t  pinMode;

	pinNumSel = pin;
	pinModedx = (2 * port);
	if (pinNumSel >= 16)
	{
		pinNumSel -= 16;
		pinModedx++;
	}

	pinConPtr = (lu_uint32_t *)&LPC_PINCON->PINMODE0;

	pinMode = (*(lu_uint32_t *)(pinConPtr + pinModedx) >> (2 * pinNumSel)) & 0x03;

	return pinMode;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t gpioGetPinPortOpenDrain(lu_uint8_t port, lu_uint8_t pin)
{
	lu_uint32_t *pinConPtr;
	lu_uint8_t  pinOpenDrain;

	pinConPtr = (lu_uint32_t *)&LPC_PINCON->PINMODE_OD0;

	pinOpenDrain = (*(lu_uint32_t *)(pinConPtr + port) >> pin) & 0x01;

	return pinOpenDrain;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint8_t gpioGetPinPortDir(lu_uint8_t port, lu_uint8_t pin)
{
	lu_uint32_t	dirBits;
	lu_uint32_t	pinShift;
	lu_uint8_t  retDir;

	pinShift = (pin & 0x1f);

	dirBits = GPIO_GetDir(port);
	retDir = (dirBits >> pinShift) & 0x01;

	return  retDir;
}

/*
 *********************** End of file ******************************************
 */
