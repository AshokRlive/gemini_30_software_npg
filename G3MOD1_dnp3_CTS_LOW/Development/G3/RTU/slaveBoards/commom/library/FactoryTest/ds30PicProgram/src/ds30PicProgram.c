/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [PIC ds30loader]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   25/07/14      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "systemTime.h"
#include "ds30loader.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "CANProtocolCodec.h"
#include "ModuleProtocol.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DS30_PIC_PROG_BL_PLACEMENT	(DS30_BOOTLOADER_PLACEMENT / DS30_PIC16F1788_PAGESIZEW) // 8

#define DS30_PIC_PROG_BL_PAGESZ_ROW (DS30_PIC16F1788_PAGESIZEW / DS30_PIC16F1788_ROWSIZEW) // 1

#define DS30_PIC_PROG_BL_START 		(DS30_PIC16F1788_MAX_FLASH - \\
		                             (DS30_PIC_PROG_BL_PLACEMENT  * \\
		                              DS30_PIC_PROG_BL_PAGESZ_ROW * \\
		                              DS30_PIC16F1788_ROWSIZEW \\
		                             ) \\
		                            ) // 0x3f00

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

lu_uint8_t CalcChk(lu_uint8_t *bufferPtr, lu_uint8_t length);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint8_t txBuffer[64 + sizeof(Ds30CmdHeaderStr) + 1];

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR ds30PicProgramBlock(BootloaderPICFirmwareStr *message)
{
	ModStatusReplyStr reply;
	UART_CFG_Type     uartCfg;
	LPC_UART_TypeDef  *lpcUart;
	PINSEL_CFG_Type   pinCfg;
	lu_uint32_t       picResetPort;
	lu_uint32_t       picResetPin;
	lu_uint32_t	      resetPinShift;
	lu_uint32_t       rxLen;
	lu_uint8_t        rxBuffer[10];
	Ds30CmdHeaderStr  *ds30HeaderPtr;

	reply.status = SB_ERROR_NONE;

	if (message->blockOffset == 0)
	{
		/* Init serial port */
		UART_ConfigStructInit(&uartCfg);

		uartCfg.Baud_rate = DS30_BAUD_RATE;

		pinCfg.Pinmode     = 0;
		pinCfg.OpenDrain   = 0;

		switch(message->uartChannel)
		{
		case 0:
			lpcUart = LPC_UART0;

			/* RX */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_2;
			pinCfg.Funcnum     = PINSEL_FUNC_2;

			PINSEL_ConfigPin(&pinCfg);

			/* TX */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_3;
			pinCfg.Funcnum     = PINSEL_FUNC_2;

			PINSEL_ConfigPin(&pinCfg);

			/* Reset */
			picResetPort = PINSEL_PORT_3;
			picResetPin  = PINSEL_PIN_26;

			pinCfg.Portnum     = picResetPort;
			pinCfg.Pinnum      = picResetPin;
			pinCfg.Funcnum = 0;

			resetPinShift = 1;
			resetPinShift = resetPinShift << (picResetPin & 0x1f);

			/* Reset PIC */
			GPIO_ClearValue(picResetPort, resetPinShift);

			PINSEL_ConfigPin(&pinCfg);
			break;

		case 2:
			lpcUart = LPC_UART2;

			/* RX */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_10;
			pinCfg.Funcnum     = PINSEL_FUNC_1;

			PINSEL_ConfigPin(&pinCfg);

			/* TX */
			pinCfg.Portnum     = PINSEL_PORT_0;
			pinCfg.Pinnum      = PINSEL_PIN_11;
			pinCfg.Funcnum     = PINSEL_FUNC_1;

			PINSEL_ConfigPin(&pinCfg);

			/* Reset */
			picResetPort = PINSEL_PORT_2;
			picResetPin  = PINSEL_PIN_1;

			pinCfg.Portnum     = picResetPort;
			pinCfg.Pinnum      = picResetPin;
			pinCfg.Funcnum = 0;

			resetPinShift = 1;
			resetPinShift = resetPinShift << (picResetPin & 0x1f);

			/* Reset PIC */
			GPIO_ClearValue(picResetPort, resetPinShift);

			PINSEL_ConfigPin(&pinCfg);
			break;

		default:
			reply.status = SB_ERROR_PARAM;
			break;
		}

		if(reply.status == SB_ERROR_NONE)
		{
			UART_Init(lpcUart, &uartCfg);

			STDelayTime(5);

			/* Release PIC from Reset */
			GPIO_SetValue(picResetPort, resetPinShift);

			STDelayTime(100);

			/* Send hello */
			UART_SendByte(lpcUart, DS30_CMD_HELLO);

			STDelayTime(20);

			rxLen = UART_Receive(lpcUart, &rxBuffer[0], 4, NONE_BLOCKING);

			if (rxLen != 4 && rxBuffer[3] != DS30_REP_OK)
			{
				reply.status = SB_ERROR_TIMEOUT;
			}
			else
			{
				/* Check device ID */

				reply.status = SB_ERROR_NONE;
			}
		}
	}

	if (reply.status == SB_ERROR_NONE)
	{
		ds30HeaderPtr           = &txBuffer[0];
		ds30HeaderPtr->address 	= (message->blockOffset * 64);
		ds30HeaderPtr->command	= DS30_CMD_WRITE;
		ds30HeaderPtr->length   = 64 + 1;  // Data plus checksum

		/* Copy to transmit buffer */
		memcpy(&txBuffer[sizeof(Ds30CmdHeaderStr)], message->firmwareBlock, 64);

		/*  Calculate checksum */
		txBuffer[sizeof(Ds30CmdHeaderStr) + 64] =
				CalcChk(&txBuffer[0], (sizeof(Ds30CmdHeaderStr) + 64));

		/* Send block */
		UART_Send(lpcUart, &txBuffer[0], (sizeof(Ds30CmdHeaderStr) + 64 + 1), NONE_BLOCKING);

		STDelayTime(20);

		rxLen = UART_Receive(lpcUart, &rxBuffer[0], 1, NONE_BLOCKING);

		if(rxBuffer[0] != DS30_REP_OK)
		{
			reply.status = SB_ERROR_WRITE_FAIL;
		}
	}

	/* Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_BL,
						MODULE_MSG_ID_BL_WRITE_PIC_FIRMWARE_R,
						sizeof(ModStatusReplyStr),
						(lu_uint8_t *)&reply
					   );
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Calculate check byte for ds30loader
 *
 *   Detailed description
 *
 *   \param *bufferPtr
 *   \param length
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
lu_uint8_t CalcChk(lu_uint8_t *bufferPtr, lu_uint8_t length)
{
	lu_uint8_t i;
	lu_uint8_t chk;

	chk = 0;
	for(i = 0; i < length; i++)
	{
		chk = chk - *bufferPtr++;
	}

	return chk;
}

/*
 *********************** End of file ******************************************
 */
