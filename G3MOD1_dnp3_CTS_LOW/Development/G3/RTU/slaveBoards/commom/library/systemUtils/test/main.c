/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief CAN Protocol Framing module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "system_LPC17xx.h"
#include "systemUtils.h"
#include "systemTime.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define DEVICE 0x01
#define DEVICE_ID 0x02

#define SHORT_MESSAGE_LEN  8
#define LONG_MESSAGE_LEN   128

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


int main(void)
{
    lu_uint32_t time1, time2, timeE, i;
    SUIRQPriorityStr priority;
    SystemInit();
    SUInitIRQPriority();

    priority.group = SU_IRQ_GROUP_7;
    priority.subPriority = SU_IRQ_SUB_PRIORITY_0;
    STInit(priority);

    SU_CRITICAL_REGION_ENTER();
    SU_CRITICAL_REGION_EXIT();

    i = 0;
    SU_ATOMIC(i++);

    while(LU_TRUE)
    {
        time1 = STGetTime();
        for(i = 0; i < 10000; i++);
        time2 = STGetTime();

        timeE = STElapsedTime(time1, time2);
    }

    return 0;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
