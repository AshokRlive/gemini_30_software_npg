/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System time module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "systemStatus.h"
#include "systemUtils.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */




/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static MODULE_BOARD_STATUS SSStatus = MODULE_BOARD_STATUS_INVALID;
static MODULE_BOARD_ERROR  SSError  = 0x00;

static lu_uint32_t         SSerialNumber                = 0xFFFFFFFF;
static lu_uint32_t         SSupplierId                  = 0x0;
static lu_uint32_t         SSfeatureRevisionMajor;
static lu_uint32_t         SSfeatureRevisionMinor;

static lu_uint32_t         SSappSystemAPIMajor;
static lu_uint32_t         SSappSystemAPIMinor;
static lu_uint32_t         SSappSoftwareVersionMajor;
static lu_uint32_t         SSappSoftwareVersionMinor;
static lu_uint32_t         SSappSoftwareVersionPatch;
static lu_uint8_t          SSappSoftwareReleaseType;
static lu_uint32_t		   SSappSvnRevision;

static lu_uint32_t         SSbootSystemAPIMajor;
static lu_uint32_t         SSbootSystemAPIMinor;
static lu_uint32_t         SSbootSoftwareVersionMajor;
static lu_uint32_t         SSbootSoftwareVersionMinor;
static lu_uint32_t         SSbootSoftwareVersionPatch;
static lu_uint8_t          SSbootSoftwareReleaseType;
static lu_uint32_t		   SSbootSvnRevision;

static lu_bool_t           SSpowerSaveMode = LU_FALSE;
static lu_bool_t           SSbootloaderMode = LU_FALSE;
static lu_bool_t           SSfactoryMode = LU_FALSE;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_bool_t SSGetPowerSaveMode(void)
{
    return SSpowerSaveMode;
}

void SSSetPowerSaveMode(lu_bool_t powerSaveMode)
{
    SU_ATOMIC(SSpowerSaveMode = powerSaveMode);
}

lu_bool_t SSGetBootloaderMode(void)
{
    return SSbootloaderMode;
}

void SSSetBootloaderMode(lu_bool_t bootloaderMode)
{
    SU_ATOMIC(SSbootloaderMode = bootloaderMode);
}

lu_bool_t SSGetFactoryMode(void)
{
    return SSfactoryMode;
}

void SSSetFactoryMode(lu_bool_t factoryMode)
{
    SU_ATOMIC(SSfactoryMode = factoryMode);
}

MODULE_BOARD_STATUS SSGetBStatus(void)
{
    return SSStatus;
}

void SSSetBStatus(MODULE_BOARD_STATUS status)
{
    SU_ATOMIC(SSStatus = status);
}

MODULE_BOARD_ERROR SSGetBError(void)
{
    return SSError;
}
void SSSetBError (MODULE_BOARD_ERROR type, lu_bool_t val)
{
    if(val == LU_TRUE)
    {
        /* Set the type bit */
        SU_ATOMIC(SSError |= type);
    }
    else
    {
        /* Reset the type bit */
        SU_ATOMIC(SSError &= (~type));
    }
}

void SSGetFullStatus(MODULE_BOARD_STATUS *status, MODULE_BOARD_ERROR *error, lu_uint32_t *serialNumber, lu_uint32_t *supplierId)
{
    if( (status == NULL) || (error == NULL) )
    {
        return;
    }

    SU_ATOMIC(*status = SSStatus; *error = SSError; *serialNumber = SSerialNumber; *supplierId = SSupplierId;);
}

lu_uint32_t SSGetSerial(void)
{
    return SSerialNumber;
}

void SSSetSerial(lu_uint32_t serialNumber)
{
	SSerialNumber = serialNumber;
}

lu_uint32_t SSGetSupplierId(void)
{
    return SSupplierId;
}

void SSSetSupplierId(lu_uint32_t supplierId)
{
    SSupplierId = supplierId;
}

void SSSetSupplierIdStr(lu_char_t *supplierIdPtr)
{
	lu_char_t supplier[3];

	supplier[0] = *supplierIdPtr++;
	supplier[1] = *supplierIdPtr++;
	supplier[2] = '\0';

    SSupplierId = atoi(supplier);
}

lu_uint32_t SSGetAppSystemAPIMajor(void)
{
    return SSappSystemAPIMajor;
}

void SSSetAppSystemAPIMajor(lu_uint32_t systemAPIMajor)
{
    SSappSystemAPIMajor = systemAPIMajor;
}

lu_uint32_t SSGetAppSystemAPIMinor(void)
{
    return SSappSystemAPIMinor;
}

void SSSetAppSystemAPIMinor(lu_uint32_t systemAPIMinor)
{
    SSappSystemAPIMinor = systemAPIMinor;
}

lu_uint32_t SSGetAppSvnRevision(void)
{
	return SSappSvnRevision;
}

void SSSetAppSvnRevision(lu_uint32_t svnRevision)
{
	SSappSvnRevision = svnRevision;
}

lu_uint32_t SSGetBootSvnRevision(void)
{
	return SSbootSvnRevision;
}

void SSSetBootSvnRevision(lu_uint32_t svnRevision)
{
	SSbootSvnRevision = svnRevision;
}

lu_uint32_t SSGetBootSystemAPIMajor(void)
{
	return SSbootSystemAPIMajor;
}

void SSSetBootSystemAPIMajor(lu_uint32_t systemAPIMajor)
{
	SSbootSystemAPIMajor = systemAPIMajor;
}

lu_uint32_t SSGetBootSystemAPIMinor(void)
{
	return SSbootSystemAPIMinor;
}

void SSSetBootSystemAPIMinor(lu_uint32_t systemAPIMinor)
{
	SSbootSystemAPIMinor = systemAPIMinor;
}


lu_uint32_t SSGetFeatureRevisionMajor(void)
{
    return SSfeatureRevisionMajor;
}

void SSSetFeatureRevisionMajor(lu_uint32_t featureRevisionMajor)
{
    SSfeatureRevisionMajor = featureRevisionMajor;
}

lu_uint32_t SSGetFeatureRevisionMinor(void)
{
    return SSfeatureRevisionMinor;
}

void SSSetFeatureRevisionMinor(lu_uint32_t featureRevisionMinor)
{
    SSfeatureRevisionMinor = featureRevisionMinor;
}

lu_uint32_t SSGetAppSoftwareVersionMajor(void)
{
    return SSappSoftwareVersionMajor;
}

void SSSetAppSoftwareVersionMajor(lu_uint32_t softwareVersionMajor)
{
	SSappSoftwareVersionMajor = softwareVersionMajor;
}

lu_uint32_t SSGetAppSoftwareVersionMinor(void)
{
    return SSappSoftwareVersionMinor;
}

void SSSetAppSoftwareVersionMinor(lu_uint32_t softwareVersionMinor)
{
	SSappSoftwareVersionMinor = softwareVersionMinor;
}

lu_uint32_t SSGetAppSoftwareVersionPatch(void)
{
    return SSappSoftwareVersionPatch;
}

void SSSetAppSoftwareVersionPatch(lu_uint32_t softwareVersionPatch)
{
	SSappSoftwareVersionPatch = softwareVersionPatch;
}

lu_uint8_t SSGetAppSoftwareReleaseType(void)
{
    return SSappSoftwareReleaseType;
}

void SSSetAppSoftwareReleaseType(lu_uint8_t softwareReleaseType)
{
	SSappSoftwareReleaseType = softwareReleaseType;
}



lu_uint32_t SSGetBootSoftwareVersionMajor(void)
{
	return SSbootSoftwareVersionMajor;
}

void SSSetBootSoftwareVersionMajor(lu_uint32_t softwareVersionMajor)
{
	SSbootSoftwareVersionMajor = softwareVersionMajor;
}

lu_uint32_t SSGetBootSoftwareVersionMinor(void)
{
	return SSbootSoftwareVersionMinor;
}

void SSSetBootSoftwareVersionMinor(lu_uint32_t softwareVersionMinor)
{
	SSbootSoftwareVersionMinor = softwareVersionMinor;
}

lu_uint32_t SSGetBootSoftwareVersionPatch(void)
{
	return SSbootSoftwareVersionPatch;
}

void SSSetBootSoftwareVersionPatch(lu_uint32_t softwareVersionPatch)
{
	SSbootSoftwareVersionPatch = softwareVersionPatch;
}

lu_uint8_t SSGetBootSoftwareReleaseType(void)
{
	return SSbootSoftwareReleaseType;
}

void SSSetBootSoftwareReleaseType(lu_uint8_t softwareReleaseType)
{
	SSbootSoftwareReleaseType = softwareReleaseType;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */



/*
 *********************** End of file ******************************************
 */
