/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SYSTEM_UTILS_INCLUDED
#define _SYSTEM_UTILS_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules UsedPriorityGroup
 ******************************************************************************
 */

#include "LPC17xx.h"
#include "core_cm3.h"
#include "lu_types.h"
#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*!
 * \brief Return a table size
 *
 * \param table Table you want calculate the size
 * \param type Table entry type
 */
#define SU_TABLE_SIZE(table, type) (sizeof(table) / sizeof(type))

/*!
 * \brief Enter a critical section.
 *
 * This macro disables the interrupt of the system. Never return from a
 * critical section. Nested calls are supported.
 */
#define SU_CRITICAL_REGION_ENTER()\
{\
    __disable_irq();\
    SUCriticalRegionNesting++;\
}

/*!
 * \brief Exit from a critical section.
 *
 * This macro enables the interrupt of the system only if the nesting counter
 * is zero
 */
#define SU_CRITICAL_REGION_EXIT()\
{\
    if(SUCriticalRegionNesting > 0)\
    {\
        if(--SUCriticalRegionNesting == 0)\
            __enable_irq();\
    }\
}

/*!
 * \brief Action "a" is executed in an atomically (interrupt disabled).
 *
 * The codec executed should never return or sleep.
 *
 */
#define SU_ATOMIC(a)\
{\
    SU_CRITICAL_REGION_ENTER();\
    a;\
    SU_CRITICAL_REGION_EXIT();\
}

/*!
 * Supported IRQ priority groups. SU_IRQ_GROUP_0 is the highest priority group.
 * An IRQ from an higher group can preempt an IRQ from a lower group.
 */
typedef enum
{
    SU_IRQ_GROUP_0 = 0,
    SU_IRQ_GROUP_1 = 1,
    SU_IRQ_GROUP_2 = 2,
    SU_IRQ_GROUP_3 = 3,
    SU_IRQ_GROUP_4 = 4,
    SU_IRQ_GROUP_5 = 5,
    SU_IRQ_GROUP_6 = 6,
    SU_IRQ_GROUP_7 = 7
}SU_IRQ_GROUP;

/*!
 * Supported sub-priority within a group. SU_IRQ_SUB_PRIORITY_0 is the
 * highest priority. An IRQ from an higher priority can NOT preempt an IRQ
 * from a lower priority. The priority is used only to statically identify the
 * order of execution when more than one IRQ is ready at the same time.
 */
typedef enum
{
    SU_IRQ_SUB_PRIORITY_0 = 0,
    SU_IRQ_SUB_PRIORITY_1 = 1,
    SU_IRQ_SUB_PRIORITY_2 = 2,
    SU_IRQ_SUB_PRIORITY_3 = 3
}SU_IRQ_SUB_PRIORITY;

/*!
 * Structure used to identify an IRQ priority
 */
typedef struct SUIRQPriorityDef
{
    SU_IRQ_GROUP        group      ;
    SU_IRQ_SUB_PRIORITY subPriority;
}SUIRQPriorityStr;

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * Global variable to keep track of the critical region nesting. Never
 * manipulate this variable explicitly.
 */
extern lu_uint32_t SUCriticalRegionNesting;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize the NVIC priority bits
 *
 *   The NXP NVIC (Nested Vectored Interrupt Controller) supports 32 priorities
 *   where 0 is the higher priority. The priority bits are split in 8 groups
 *   with 4 subpriorities each.
 *   IRQ from different groups can be preempted.
 *   IRQ in the same group can not be preempted.
 *   See the LPC17xx User Manual for more information about the NVIC features.
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SUInitIRQPriority(void);

/*!
 ******************************************************************************
 *   \brief Set IRQ priority
 *
 *   \param IRQn IQ number
 *   \param priority IRQ priority
 *   \param enable If TRUE the IRQ is enabled
 *
 *   \return None
 *
 ******************************************************************************
 */
extern void SUSetIRQPriority( IRQn_Type IRQn           ,
                              SUIRQPriorityStr priority,
                              lu_bool_t enable
                            );

#endif /* _SYSTEM_UTILS_INCLUDED */

/*
 *********************** End of file ******************************************
 */
