/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System alarm header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SYSTEM_ALRAM_INCLUDED
#define _SYSTEM_ALRAM_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"

#include "SysAlarmCodeEnum.h"

#include "CANProtocolCodec.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! System Alarm event structure */
typedef struct SysAlarmEventDef
{
	lu_uint8_t 	state; /* SYS_ALARM_STATE_* */
	lu_uint16_t alarmSubSysCode; /* SYS_ALARM_SUBSYSTEM_* + (SYSALC_* << MAX_SYS_ALARM_BITSHIFT) */
	lu_uint16_t param;
} SysAlarmEventStr;

/*! System Alarm queue structure */
typedef struct SysAlarmEventsDef
{
	SysAlarmEventStr event[MAX_SYS_ALARM];
    lu_uint32_t      writeIndex;
} SysAlarmEventsStr;

/*! System Alarm buffer structure */
typedef struct SysAlarmEventBuffDef
{
	SysAlarmEventsStr  critical;
	SysAlarmEventsStr  error;
	SysAlarmEventsStr  warning;
	SysAlarmEventsStr  info;
} SysAlarmEventBuffStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialise System alarm module CAN Decoder
 *
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR SysAlarmInitCanDecoder(void);

/*!
 ******************************************************************************
 *   \brief Initialise System alarm module events
 *
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR SysAlarmInitEvents(void);

/*!
 ******************************************************************************
 *   \brief CAN Decoder for System alarm module
 *
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR SysAlarmCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief Log an alarm event
 *
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */

extern SB_ERROR SysAlarmSetLogEvent(lu_bool_t           set,
                                    SYS_ALARM_SEVERITY  severity,
                                    SYS_ALARM_SUBSYSTEM subSystem,
                                    lu_uint16_t         alarmCode,
                                    lu_uint16_t		 param
                                   );

#endif /* _SYSTEM_ALRAM_INCLUDED */

/*
 *********************** End of file ******************************************
 */
