/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System alarm module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "systemUtils.h"
#include "systemStatus.h"

#include "systemAlarm.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void ClearAlarmEvents(void);

SB_ERROR SysAlarmCommandGetFirstEvent(SysAlSeverityStr *sysAlSeverityPtr);
SB_ERROR SysAlarmCommandGetNextEvent(SysAlSeverityStr *sysAlSeverityPtr);

void UpdateSystemStatus(void);

SB_ERROR SysAlarmCommandClearEvents(void);

SB_ERROR AddAlarmEvent(SysAlarmEventsStr   *alarmsEventsPtr,
		               lu_bool_t           set,
		               SYS_ALARM_SUBSYSTEM subSystem,
		               lu_uint16_t         alarmCode,
		               lu_uint16_t		   param
		              );

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


SysAlarmEventBuffStr sysAlarmEventBuff __attribute__((section("ahbsram0")));

lu_uint32_t sysAlarmReadIndex[SYS_ALARM_SEVERITY_LAST];


/*! List of supported message */
static const filterTableStr SysAlarmModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Bootloader test API commands */
	{  MODULE_MSG_TYPE_SYSALARM, MODULE_MSG_ID_SYSALARM_CLEAR_EVENTS_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_SYSALARM, MODULE_MSG_ID_SYSALARM_GET_FIRST_EVENT_C   , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_SYSALARM, MODULE_MSG_ID_SYSALARM_GET_NEXT_EVENT_C    , LU_FALSE , 0      }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR SysAlarmInitCanDecoder(void)
{
	/* To add calibration commands to CAN filter */
	return CANFramingAddFilter( SysAlarmModulefilterTable,
								SU_TABLE_SIZE(SysAlarmModulefilterTable, filterTableStr)
							  );
}

SB_ERROR SysAlarmInitEvents(void)
{
	ClearAlarmEvents();

	UpdateSystemStatus();

	return SB_ERROR_NONE;
}

SB_ERROR SysAlarmCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retVal;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_SYSALARM:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_SYSALARM_CLEAR_EVENTS_C:
			/* Message sanity check */
			if (msgPtr->msgLen == 0)
			{
				retVal = SysAlarmCommandClearEvents();
			}
			break;

		case MODULE_MSG_ID_SYSALARM_GET_FIRST_EVENT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == sizeof(SysAlSeverityStr))
			{
				retVal = SysAlarmCommandGetFirstEvent((SysAlSeverityStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_SYSALARM_GET_NEXT_EVENT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == sizeof(SysAlSeverityStr))
			{
				retVal = SysAlarmCommandGetNextEvent((SysAlSeverityStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retVal;
}


SB_ERROR SysAlarmSetLogEvent(lu_bool_t           set,
		                     SYS_ALARM_SEVERITY  severity,
		                     SYS_ALARM_SUBSYSTEM subSystem,
		                     lu_uint16_t         alarmCode,
		                     lu_uint16_t		 param
		                     )
{
	SB_ERROR retError;

	retError = SB_ERROR_PARAM;

	SU_CRITICAL_REGION_ENTER()

	switch (severity)
	{
	case SYS_ALARM_SEVERITY_CRITICAL:
		retError = AddAlarmEvent(&sysAlarmEventBuff.critical, set, subSystem, alarmCode, param);
		break;

	case SYS_ALARM_SEVERITY_ERROR:
		retError = AddAlarmEvent(&sysAlarmEventBuff.error, set, subSystem, alarmCode, param);
		break;

	case SYS_ALARM_SEVERITY_WARNING:
		retError = AddAlarmEvent(&sysAlarmEventBuff.warning, set, subSystem, alarmCode, param);
		break;

	case SYS_ALARM_SEVERITY_INFO:
		retError = AddAlarmEvent(&sysAlarmEventBuff.info, set, subSystem, alarmCode, param);
		break;

	default:
		break;
	}

	/* update global alarm status */
	if (retError == SB_ERROR_NONE)
	{
		UpdateSystemStatus();
	}

	SU_CRITICAL_REGION_EXIT()

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Update system status
 *
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
void ClearAlarmEvents(void)
{
	int idx;

	/* reset Alarm buffer pointers */
	sysAlarmEventBuff.critical.writeIndex = 0;
	sysAlarmEventBuff.error.writeIndex = 0;
	sysAlarmEventBuff.warning.writeIndex = 0;
	sysAlarmEventBuff.info.writeIndex = 0;

	for (idx = 0; idx < SYS_ALARM_SEVERITY_LAST; idx++)
	{
		sysAlarmReadIndex[idx] = 0;
	}
}

/*!
 ******************************************************************************
 *   \brief Update system status
 *
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
void UpdateSystemStatus(void)
{
	lu_uint32_t idx;
	lu_bool_t   setAlarm;

	/* set/clear Critical alarm */
	setAlarm = LU_FALSE;
	for (idx = 0; idx < sysAlarmEventBuff.critical.writeIndex; idx++)
	{
		if (sysAlarmEventBuff.critical.event[idx].state & SYS_ALARM_STATE_ACTIVE)
		{
			setAlarm = LU_TRUE;
			break;
		}
	}
	SSSetBError(MODULE_BOARD_ERROR_ALARM_CRITICAL, setAlarm);

	/* set/clear Error alarm */
	setAlarm = LU_FALSE;
	for (idx = 0; idx < sysAlarmEventBuff.error.writeIndex; idx++)
	{
		if (sysAlarmEventBuff.error.event[idx].state & SYS_ALARM_STATE_ACTIVE)
		{
			setAlarm = LU_TRUE;
			break;
		}
	}
	SSSetBError(MODULE_BOARD_ERROR_ALARM_ERROR, setAlarm);

	/* set/clear Warning alarm */
	setAlarm = LU_FALSE;
	for (idx = 0; idx < sysAlarmEventBuff.warning.writeIndex; idx++)
	{
		if (sysAlarmEventBuff.warning.event[idx].state & SYS_ALARM_STATE_ACTIVE)
		{
			setAlarm = LU_TRUE;
			break;
		}
	}
	SSSetBError(MODULE_BOARD_ERROR_ALARM_WARNING, setAlarm);

	/* set/clear Info alarm */
	setAlarm = LU_FALSE;
	for (idx = 0; idx < sysAlarmEventBuff.info.writeIndex; idx++)
	{
		if (sysAlarmEventBuff.info.event[idx].state & SYS_ALARM_STATE_ACTIVE)
		{
			setAlarm = LU_TRUE;
			break;
		}
	}
	SSSetBError(MODULE_BOARD_ERROR_ALARM_INFO, setAlarm);
}

/*!
 ******************************************************************************
 *   \brief Clear all system alarms
 *
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
SB_ERROR SysAlarmCommandClearEvents(void)
{
	SB_ERROR retError;

	SU_CRITICAL_REGION_ENTER()

	ClearAlarmEvents();

	UpdateSystemStatus();

	SU_CRITICAL_REGION_EXIT()

	/* Send CAN message reply */
	retError = CANCSendMCM( MODULE_MSG_TYPE_SYSALARM,
							MODULE_MSG_ID_SYSALARM_CLEAR_EVENTS_R,
							0,
							0L
						  );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Clear all system alarms
 *
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
SB_ERROR SysAlarmCommandGetFirstEvent(SysAlSeverityStr *sysAlSeverityPtr)
{
	SB_ERROR retError;
	SysAlEventStatStr eventStat;
	lu_uint32_t idx;

	retError = SB_ERROR_PARAM;

	eventStat.status     = REPLY_STATUS_NO_EVENTS;
	eventStat.parameter  = 0;
	eventStat.state      = 0;
	eventStat.subSystem  = 0;
	eventStat.alarmCode  = 0;

	if (sysAlSeverityPtr->severity < SYS_ALARM_SEVERITY_LAST)
	{
		idx = sysAlarmReadIndex[sysAlSeverityPtr->severity] = 0;

		switch(sysAlSeverityPtr->severity)
		{
		case SYS_ALARM_SEVERITY_CRITICAL:
			if (sysAlarmEventBuff.critical.writeIndex > 0)
			{
				eventStat.status    = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.critical.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.critical.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.critical.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.critical.event[idx].param;
			}
			break;

		case SYS_ALARM_SEVERITY_ERROR:
			if (sysAlarmEventBuff.error.writeIndex > 0)
			{
				eventStat.status = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.error.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.error.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.error.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.error.event[idx].param;
			}
			break;

		case SYS_ALARM_SEVERITY_WARNING:
			if (sysAlarmEventBuff.warning.writeIndex > 0)
			{
				eventStat.status = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.warning.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.warning.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.warning.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.warning.event[idx].param;
			}
			break;

		case SYS_ALARM_SEVERITY_INFO:
			if (sysAlarmEventBuff.info.writeIndex > 0)
			{
				eventStat.status = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.info.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.info.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.info.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.info.event[idx].param;
			}
			break;
		}
	}

	/* Send CAN message reply */
	retError = CANCSendMCM( MODULE_MSG_TYPE_SYSALARM,
							MODULE_MSG_ID_SYSALARM_GET_FIRST_EVENT_R,
							sizeof(SysAlEventStatStr),
							(lu_uint8_t *)&eventStat
						  );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Clear all system alarms
 *
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
SB_ERROR SysAlarmCommandGetNextEvent(SysAlSeverityStr *sysAlSeverityPtr)
{
	SB_ERROR retError;
	SysAlEventStatStr eventStat;
	lu_uint32_t idx;

	retError = SB_ERROR_PARAM;

	eventStat.status     = REPLY_STATUS_NO_EVENTS;
	eventStat.parameter  = 0;
	eventStat.state      = 0;
	eventStat.subSystem  = 0;
	eventStat.alarmCode  = 0;

	if (sysAlSeverityPtr->severity < SYS_ALARM_SEVERITY_LAST)
	{
		/* Increment to next event */
		idx = ++sysAlarmReadIndex[sysAlSeverityPtr->severity];

		switch(sysAlSeverityPtr->severity)
		{
		case SYS_ALARM_SEVERITY_CRITICAL:
			if (sysAlarmEventBuff.critical.writeIndex < MAX_SYS_ALARM &&
				idx < sysAlarmEventBuff.critical.writeIndex
			   )
			{
				eventStat.status    = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.critical.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.critical.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.critical.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.critical.event[idx].param;
			}
			break;

		case SYS_ALARM_SEVERITY_ERROR:
			if (sysAlarmEventBuff.error.writeIndex < MAX_SYS_ALARM &&
		        idx < sysAlarmEventBuff.error.writeIndex)
			{
				eventStat.status = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.error.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.error.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.error.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.error.event[idx].param;
			}
			break;

		case SYS_ALARM_SEVERITY_WARNING:
			if (sysAlarmEventBuff.warning.writeIndex < MAX_SYS_ALARM &&
				idx < sysAlarmEventBuff.warning.writeIndex
			    )
			{
				eventStat.status = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.warning.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.warning.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.warning.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.warning.event[idx].param;
			}
			break;

		case SYS_ALARM_SEVERITY_INFO:
			if (sysAlarmEventBuff.info.writeIndex < MAX_SYS_ALARM &&
				idx < sysAlarmEventBuff.info.writeIndex
			   )
			{
				eventStat.status = REPLY_STATUS_OKAY;
				eventStat.alarmCode = (sysAlarmEventBuff.info.event[idx].alarmSubSysCode >> MAX_SYS_ALARM_BITSHIFT);
				eventStat.subSystem = (sysAlarmEventBuff.info.event[idx].alarmSubSysCode & 0xff);
				eventStat.state     = sysAlarmEventBuff.info.event[idx].state;
				eventStat.parameter = sysAlarmEventBuff.info.event[idx].param;
			}
			break;
		}
	}

	/* Send CAN message reply */
	retError = CANCSendMCM( MODULE_MSG_TYPE_SYSALARM,
							MODULE_MSG_ID_SYSALARM_GET_NEXT_EVENT_R,
							sizeof(SysAlEventStatStr),
							(lu_uint8_t *)&eventStat
						  );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Add event to queue
 *
 *
 *
 *   \return SB_ERROR
 *
 ******************************************************************************
 */
SB_ERROR AddAlarmEvent(SysAlarmEventsStr   *alarmsEventsPtr,
		               lu_bool_t           set,
		               SYS_ALARM_SUBSYSTEM subSystem,
		               lu_uint16_t         alarmCode,
		               lu_uint16_t		   param
		              )
{
	SB_ERROR    retError;
	lu_uint16_t alarmSubSysCode;
	lu_bool_t   found;
	lu_uint32_t idx;

	found = LU_FALSE;
	retError = SB_ERROR_NONE;

	alarmSubSysCode  = subSystem;
	alarmSubSysCode |= (alarmCode << MAX_SYS_ALARM_BITSHIFT);

	/* Search for existing event in queue */
	for (idx = 0; idx < alarmsEventsPtr->writeIndex; idx++)
	{

		if (alarmsEventsPtr->event[idx].alarmSubSysCode == alarmSubSysCode)
		{
			found = LU_TRUE;
			break;
		}
	}

	/* Find a spare slot in the queue */
	if (found == LU_FALSE &&
		alarmsEventsPtr->writeIndex < MAX_SYS_ALARM &&
		set == LU_TRUE)
	{
		idx = alarmsEventsPtr->writeIndex;

		found = LU_TRUE;

		/* Increment write pointer */
		alarmsEventsPtr->writeIndex++;
	}

	/* If slot found update alarm code */
	if (found == LU_TRUE)
	{
		/* Update alarm state */
		alarmsEventsPtr->event[idx].alarmSubSysCode = alarmSubSysCode;
		alarmsEventsPtr->event[idx].param           = param;

		if (set)
		{
			alarmsEventsPtr->event[idx].state     |= SYS_ALARM_STATE_ACTIVE;

			if (alarmsEventsPtr->event[idx].state & SYS_ALARM_STATE_INACTIVE_LATCHED)
			{
				alarmsEventsPtr->event[idx].state |= SYS_ALARM_STATE_REACTIVATE_LATCHED;
			}
		}
		else
		{
			alarmsEventsPtr->event[idx].state     &= ~SYS_ALARM_STATE_ACTIVE;

			alarmsEventsPtr->event[idx].state     |= SYS_ALARM_STATE_INACTIVE_LATCHED;
		}
	}
	return retError;
}

/*
 *********************** End of file ******************************************
 */
