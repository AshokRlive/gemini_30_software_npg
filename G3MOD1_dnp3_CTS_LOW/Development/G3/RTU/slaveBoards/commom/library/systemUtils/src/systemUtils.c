/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System time module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "systemUtils.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*!
 * IRQ Priority grouping: 8 groups, 4 subpriprities.
 * See LPC17xx User Manual
 */
#define SU_IRQ_PRIO_G8_SG4   0x04

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */




/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

lu_uint32_t SUCriticalRegionNesting = 0;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


void SUInitIRQPriority(void)
{
    /* Set priority grouping */
    NVIC_SetPriorityGrouping(SU_IRQ_PRIO_G8_SG4);
}


void SUSetIRQPriority( IRQn_Type IRQn           ,
                       SUIRQPriorityStr priority,
                       lu_bool_t enable
                     )
{
    lu_uint32_t encodedPriority;

    /* Encode priority */
    encodedPriority = NVIC_EncodePriority( SU_IRQ_PRIO_G8_SG4,
                                           priority.group    ,
                                           priority.subPriority
                                         );
    /* Set priority */
    NVIC_SetPriority(IRQn, encodedPriority);

    /* Enable interrupt */
    if (enable == LU_TRUE)
    {
        NVIC_EnableIRQ(IRQn);
    }
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
