/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System time module implementation file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


#include "lpc17xx_systick.h"
#include "debug_frmwrk.h"
#include "systemTime.h"
#include "systemUtils.h"

//#include "StatusManager.h"

#include "lpc17xx_wdt.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*! System tick time base (in ms) */
#define ST_TIME_BASE_MS 1 /* ms */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*!
 * System timer status data
 */
typedef struct STDataDef
{
    lu_bool_t initialized;

    lu_uint32_t time;
}STDataStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

inline static lu_uint32_t STElapsedTimeTick(lu_uint32_t timeStartTick, lu_uint32_t timeEndTick);
void STSysTickIntHandler(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*!
 * System time global data
 */
static volatile STDataStr STData =
{
    LU_FALSE, /* initialized */
    0         /* time        */
};

static volatile lu_uint32_t STRunningTimeSecs = 0;

static volatile lu_uint32_t STTickUint16Secs = 0;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR STInit(SUIRQPriorityStr priority)
{
    if (STData.initialized == LU_FALSE)
    {
        STData.initialized = LU_TRUE;

        /* Initialize System Tick with 1ms time interval */
        SYSTICK_InternalInit(ST_TIME_BASE_MS);
        /* Set IRQ priority and enable System Tick priority  */
        SUSetIRQPriority(SysTick_IRQn, priority, LU_FALSE);
        /* Enable System Tick interrupt */
        SYSTICK_IntCmd(ENABLE);
        /* Enable System Tick Counter */
        SYSTICK_Cmd(ENABLE);
    }

    return SB_ERROR_NONE;
}


lu_uint32_t STGetTime(void)
{
    return STData.time;
}

lu_uint32_t STGetRunningTimeSecs(void)
{
    return STRunningTimeSecs;
}

lu_uint32_t STGetTickUint16Secs(void)
{
    return STTickUint16Secs;
}


void STDelayTime(lu_uint32_t timeMs)
{
	lu_uint32_t startTime;
	lu_uint32_t elapsedTime;
	lu_uint32_t currentTime;

	startTime = STGetTime();	/*! Record the tick number */

	/* Loop until time has expired */
	do
	{
		currentTime = STGetTime();

		elapsedTime = STElapsedTime( startTime , currentTime );
	} while (elapsedTime < timeMs);
}

void STDleayTimeUsec(lu_uint32_t timeUsec)
{
	lu_uint32_t	timeTick;
	lu_uint32_t	startTimeTick;
	lu_uint32_t elapsedTimeTick;
	lu_uint32_t currentTimeTick;

	startTimeTick = SYSTICK_GetCurrentValue();	/*! Record the tick number */
	timeTick      = ((SystemCoreClock / 1000000) * timeUsec);

	/* Loop until time has expired */
	do
	{
		currentTimeTick = SYSTICK_GetCurrentValue();

		elapsedTimeTick = STElapsedTimeTick( startTimeTick , currentTimeTick );
	} while (elapsedTimeTick < timeTick);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   Time resolution is SysClk . Maximum elapsed time 24 bits
 *
 *   \return Tiem in us
 *
 ******************************************************************************
 */
inline static lu_uint32_t STElapsedTimeTick(lu_uint32_t timeStartTick, lu_uint32_t timeEndTick)
{
    if (timeEndTick > timeStartTick)
    {
        return (timeEndTick - timeStartTick);
    }
    else
    {
        return ( (ST_TIME_TICK_OVERFLOW - timeStartTick + timeEndTick) + 1);
    }
}

/*!
 ******************************************************************************
 *   \brief System timer IRQ handler
 *
 *   The internal timer is incremented.
 *
 *   \return None
 *
 ******************************************************************************
 */
 void STSysTickIntHandler(void)
 {
	 static lu_uint16_t secCount = 1000;
//	 lu_uint8_t index;

     /* Clear System Tick counter flag */
     SYSTICK_ClearCounterFlag();

     SU_CRITICAL_REGION_ENTER()

     /* Increment internal time counter */
     STData.time++;

     /* Count seconds */
     secCount--;
     if (!secCount)
     {
    	 secCount = 1000;
    	 STRunningTimeSecs++;

    	 STTickUint16Secs++;

    	 if(STTickUint16Secs > 32767)
    	 {
    		 STTickUint16Secs = 0;
    	 }

 //   	 index = StatusManagerGetThreadIdx();
/*
    	 _printf("Task[%d]", index);
    	 _printf("Can RX[%d] Can Err[%d] ICR[%x] GSR[%x] MOD[%x] BTR[%x]\n",
    			 CANFramingGetRXMessageTotal(),
    			 CANFramingGetcanError(),
    			 CANFramingGetICR(),
    			 CANFramingGetGSR(),
    			 CANFramingGetMOD(),
    			 CANFramingGetBTR()
    			 );
*/
     }

     SU_CRITICAL_REGION_EXIT()
     return;
 }

/*
 *********************** End of file ******************************************
 */
