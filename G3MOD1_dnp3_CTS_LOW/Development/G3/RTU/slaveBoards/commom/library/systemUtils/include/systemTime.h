/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief System time public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   03/06/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _SYSTEM_TIME_INCLUDED
#define _SYSTEM_TIME_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "errorCodes.h"
#include "systemUtils.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define ST_TIME_OVERFLOW        (0xFFFFFFFF)

#define ST_TIME_TICK_OVERFLOW   (0x00FFFFFF) // 24 bit tick counter

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialize System time module
 *
 *   Time resolution is 1ms
 *
 *   \param priority Timer IRQ priority
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR STInit(SUIRQPriorityStr priority);

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   The time resolution is 1ms
 *
 *   \return Tiem in ms
 *
 ******************************************************************************
 */
extern lu_uint32_t STGetTime(void);

/*!
 ******************************************************************************
 *   \brief Return running time
 *
 *   The time resolution is 1Sec
 *
 *   \return time in Secs
 *
 ******************************************************************************
 */
extern lu_uint32_t STGetRunningTimeSecs(void);

/*!
 ******************************************************************************
 *   \brief Return running time
 *
 *   The time resolution is 1Sec 0 - 32767
 *
 *   \return time in Secs
 *
 ******************************************************************************
 */
extern lu_uint32_t STGetTickUint16Secs(void);

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   The time resolution is 1us
 *
 *   \return Tiem in ms
 *
 ******************************************************************************
 */
extern void STDleayTimeUsec(lu_uint32_t timeUsec);

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   Time resolution is 1ms. Maximum elapsed time ~49.7 days
 *
 *   \return Tiem in ms
 *
 ******************************************************************************
 */
inline static lu_uint32_t STElapsedTime(lu_uint32_t timeStart, lu_uint32_t timeEnd);
inline static lu_uint32_t STElapsedTime(lu_uint32_t timeStart, lu_uint32_t timeEnd)
{
    if (timeEnd > timeStart)
    {
        return (timeEnd - timeStart);
    }
    else
    {
        return ( (ST_TIME_OVERFLOW - timeStart + timeEnd) + 1);
    }
}

/*!
 ******************************************************************************
 *   \brief Delay for time in ms
 *
 *   Time resolution is 1ms. Maximum elapsed time ~49.7 days
 *
 *   \return
 *
 ******************************************************************************
 */
extern void STDelayTime(lu_uint32_t time);


#endif /* _SYSTEM_TIME_INCLUDED */

/*
 *********************** End of file ******************************************
 */
