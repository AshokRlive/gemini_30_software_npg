/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/04/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "FPI.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialises a single FPI channel
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FPIChanInit(FPICfgAndParamsStr *paramsPtr)
{
	/* For development purposes only */
	/* Clear all phase parameters */
	FPIChanReset(paramsPtr);

	/* Set configuration parameters */
	paramsPtr->cfg.enabled = LU_FALSE;

	paramsPtr->cfg.minFaultDurationMs = 50;
	paramsPtr->cfg.minInrushDurationMs = 100;
	paramsPtr->cfg.instantFaultCurrent = 10000;
	paramsPtr->cfg.inrushCurrent = 12000;
	paramsPtr->cfg.timedFaultCurrent = 10000;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Resets a single FPI channel
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FPIChanReset(FPICfgAndParamsStr *paramsPtr)
{
	paramsPtr->faultParams.Alarm = LU_FALSE;
	paramsPtr->faultParams.InstantAlarm = LU_FALSE;
	paramsPtr->faultParams.FaultDurationMs = 0;
	paramsPtr->faultParams.InitTimeofAlarmMs = 0;
	paramsPtr->faultParams.InitTimeSinceInstantAlarmMs = 0;
	paramsPtr->faultParams.InitialTimeofFaultMs = 0;
	paramsPtr->faultParams.InstantFaultEventDetected = 0;
	paramsPtr->faultParams.FaultEventDetected = 0;
	paramsPtr->faultParams.TimeSinceAlarmMs = 0;
	paramsPtr->faultParams.TimeSinceInstantAlarmMs = 0;
	paramsPtr->faultParams.InrushDurationMs = 0;
	paramsPtr->faultParams.InitTimeofInrushMs= 0;
	paramsPtr->faultParams.InrushDetected = 0;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Test a single FPI channel
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FPIChanTest(FPICfgAndParamsStr *paramsPtr)
{
	paramsPtr->faultParams.Alarm = LU_TRUE;
	paramsPtr->faultParams.InstantAlarm = LU_TRUE;
	paramsPtr->faultParams.FaultDurationMs = 0;
	paramsPtr->faultParams.InitTimeofAlarmMs = 0;
	paramsPtr->faultParams.InitTimeSinceInstantAlarmMs = 0;
	paramsPtr->faultParams.InitialTimeofFaultMs = 0;
	paramsPtr->faultParams.InstantFaultEventDetected = 0;
	paramsPtr->faultParams.FaultEventDetected = 0;
	paramsPtr->faultParams.TimeSinceAlarmMs = 0;
	paramsPtr->faultParams.TimeSinceInstantAlarmMs = 0;
	paramsPtr->faultParams.InrushDurationMs = 0;
	paramsPtr->faultParams.InitTimeofInrushMs= 0;
	paramsPtr->faultParams.InrushDetected = 0;

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief FPI Phase Fault Detection
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FPIPhaseFaultDetection(FPICfgAndParamsStr 	*paramsPtr,
							lu_uint32_t 		peakValue,
							lu_uint32_t			timeMs)
{
	if (paramsPtr->cfg.enabled == LU_TRUE)
	{
		//Reset the fault function if the phase peakValue has fallen below the
		//overcurrent threshold once the counter has been triggered at least once
		if ((paramsPtr->faultParams.FaultEventDetected >= 1) &&
			 (peakValue < paramsPtr->cfg.timedFaultCurrent))
		{
			//Reset overcurrent timers
			paramsPtr->faultParams.FaultEventDetected   = 0;
			paramsPtr->faultParams.InitialTimeofFaultMs = 0;
			paramsPtr->faultParams.FaultDurationMs      = 0;
		}

		//Reset inrush timer if the phase current value
		//has fallen below the inrush threshold level
		if ((paramsPtr->faultParams.InrushDetected >= 1) &&
				(peakValue < paramsPtr->cfg.inrushCurrent))
		{
			//reset overload timer
			paramsPtr->faultParams.InrushDurationMs = 0;
			paramsPtr->faultParams.InitTimeofInrushMs= 0;
			paramsPtr->faultParams.InrushDetected = 0;
		}

		// If an instant overcurrent has not been detected previously
		// check the current phase value
		if ((paramsPtr->faultParams.InstantAlarm == LU_FALSE) &&
				(paramsPtr->cfg.instantFaultCurrent > 0))
		{
			if (peakValue >= paramsPtr->cfg.instantFaultCurrent)
			{
				paramsPtr->faultParams.InstantFaultEventDetected++;
			}
		}

		// Disable the fault function if it has found a fault
		// The function is reset remotely or through a timer
		if ((paramsPtr->faultParams.Alarm == LU_FALSE) &&
				(paramsPtr->cfg.timedFaultCurrent > 0))
		{
			//Check for motor and transformer inrush(/overload) current
			if (peakValue >= paramsPtr->cfg.inrushCurrent)
			{
				paramsPtr->faultParams.InrushDetected++;

				//first inrush event detected, get initial time
				if(paramsPtr->faultParams.InrushDetected == 1)
				{
					paramsPtr->faultParams.InitTimeofInrushMs = timeMs;
				}

				paramsPtr->faultParams.InrushDurationMs = timeMs;
				paramsPtr->faultParams.InrushDurationMs = FPIElapsedTime(
								paramsPtr->faultParams.InitTimeofInrushMs,
								paramsPtr->faultParams.InrushDurationMs);
			}

			//Is the phase peakValue greater than the fault
			//threshold level and below the inrush current threshold
			if ((peakValue >= paramsPtr->cfg.timedFaultCurrent) &&
				 (peakValue <  paramsPtr->cfg.inrushCurrent))
			{
				paramsPtr->faultParams.FaultEventDetected++;
				if (paramsPtr->faultParams.FaultEventDetected == 1)
				{
					paramsPtr->faultParams.InitialTimeofFaultMs = timeMs;
				}

				paramsPtr->faultParams.FaultDurationMs = timeMs;
				paramsPtr->faultParams.FaultDurationMs = FPIElapsedTime(
							paramsPtr->faultParams.InitialTimeofFaultMs,
								paramsPtr->faultParams.FaultDurationMs);

			}
		}//disable overcurrent detect

		// Have the inrush inhibition condition been exceeded
		if (paramsPtr->faultParams.InrushDurationMs >=
				paramsPtr->cfg.minInrushDurationMs)
		{
			//Alarm for overcurrent event
			paramsPtr->faultParams.InitTimeofAlarmMs = timeMs;

			//Reset timer variable
			paramsPtr->faultParams.InrushDurationMs  = 0;
			paramsPtr->faultParams.InrushDetected = 0;
			paramsPtr->faultParams.InitTimeofInrushMs = 0;
			paramsPtr->faultParams.FaultDurationMs  = 0;
			paramsPtr->faultParams.InitialTimeofFaultMs = 0;
			paramsPtr->faultParams.FaultEventDetected = 0;
			//Set alarm
			paramsPtr->faultParams.Alarm = LU_TRUE;
		}//inrushDurationMs

		//Are the Overcurrent fault alarm condition met
		if (paramsPtr->faultParams.FaultDurationMs >=
				paramsPtr->cfg.minFaultDurationMs)
		{
			//Alarm for overcurrent event
			//Load global fault timer
			paramsPtr->faultParams.InitTimeofAlarmMs = timeMs;
			//Reset timer variable
			paramsPtr->faultParams.FaultDurationMs  = 0;
			paramsPtr->faultParams.InitialTimeofFaultMs = 0;
			paramsPtr->faultParams.FaultEventDetected = 0;
			//Set alarm
			paramsPtr->faultParams.Alarm = LU_TRUE;
		}//FaultDurationMs

		//instant overcurrent warning
		if (paramsPtr->faultParams.InstantFaultEventDetected == 1)
		{
			paramsPtr->faultParams.InitTimeSinceInstantAlarmMs = timeMs;
			paramsPtr->faultParams.InstantFaultEventDetected = 0;
			//Set instantaneous alarm
			paramsPtr->faultParams.InstantAlarm = LU_TRUE;
		}//InstantFaultEventDetected > 0

		//Reset overcurrent alarm
		if (paramsPtr->faultParams.Alarm == LU_TRUE)
		{
			paramsPtr->faultParams.TimeSinceAlarmMs = timeMs;
			paramsPtr->faultParams.TimeSinceAlarmMs = FPIElapsedTime(
							paramsPtr->faultParams.InitTimeofAlarmMs,
							paramsPtr->faultParams.TimeSinceAlarmMs);
		}

		//Reset instant overcurrent alarm
		if (paramsPtr->faultParams.InstantAlarm == LU_TRUE)
		{
			paramsPtr->faultParams.TimeSinceInstantAlarmMs = timeMs;
			paramsPtr->faultParams.TimeSinceInstantAlarmMs = FPIElapsedTime(
							paramsPtr->faultParams.InitTimeSinceInstantAlarmMs,
							paramsPtr->faultParams.TimeSinceInstantAlarmMs);
		}
	}
}

/*!
 ******************************************************************************
 *   \brief FPI Current Absence or Presence
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FPICurrentAbsence(FPICfgAndParamsStr 	*paramsPtr,
						lu_uint32_t 		peakValue,
						lu_uint32_t			timeMs)
{
	if (paramsPtr->cfg.enabled == LU_TRUE)
	{
		// Reset the Presence Indication variables if the peakValue has fallen below the
		// presence threshold
		// Current Presence and Absence Indications are cleared from within this function only
		// i.e. Their is no selfResetAbsence(/Presence) function
		if (peakValue < paramsPtr->cfg.timedPresenceCurrent)
		{
			// Reset Presence timers
			paramsPtr->absenceParams.CurrentPresenceDetected = 0;
			paramsPtr->absenceParams.InitialTimeofPresenceMs = 0;
			paramsPtr->absenceParams.PresenceDurationMs      = 0;
			// Clear Current Presence Indication
			paramsPtr->absenceParams.CurrentPresenceIndication = LU_FALSE;
		}

		// Reset the Absence Indication variables if the peakValue has increased above the
		// Absence threshold
		if (peakValue > paramsPtr->cfg.timedAbsenceCurrent)
		{
			// Reset Current Absence timers
			paramsPtr->absenceParams.CurrentAbsenceDetected = 0;
			paramsPtr->absenceParams.InitialTimeofAbsenceMs = 0;
			paramsPtr->absenceParams.AbsenceDurationMs      = 0;
			// Clear Current Absence Indication
			paramsPtr->absenceParams.CurrentAbsenceIndication = LU_FALSE;
		}

		// Disable the Current Presence Indication if the threshold has been set to Zero
		if ((paramsPtr->absenceParams.CurrentPresenceIndication == LU_FALSE) &&
				(paramsPtr->cfg.timedPresenceCurrent > 0))
		{
			//Is the peakValue greater than the timedPresenceCurrent
			if (peakValue >= paramsPtr->cfg.timedPresenceCurrent)
			{
				paramsPtr->absenceParams.CurrentPresenceDetected++;
				if (paramsPtr->absenceParams.CurrentPresenceDetected == 1)
				{
					paramsPtr->absenceParams.InitialTimeofPresenceMs = timeMs;
				}

				paramsPtr->absenceParams.PresenceDurationMs = timeMs;
				paramsPtr->absenceParams.PresenceDurationMs = FPIElapsedTime(
							paramsPtr->absenceParams.InitialTimeofPresenceMs,
								paramsPtr->absenceParams.PresenceDurationMs);
			}

			//Have the Current Presence Indication timing condition been met
			if (paramsPtr->absenceParams.PresenceDurationMs >=
					paramsPtr->cfg.minPresenceDurationMs)
			{
				//Indication of Current Presence event
				//Reset timer variable
				paramsPtr->absenceParams.PresenceDurationMs  	= 0;
				paramsPtr->absenceParams.InitialTimeofPresenceMs = 0;
				paramsPtr->absenceParams.CurrentPresenceDetected = 0;
				//Set Current Presence Indication
				paramsPtr->absenceParams.CurrentPresenceIndication = LU_TRUE;
				//Clear Current Absence Indication
				paramsPtr->absenceParams.CurrentAbsenceIndication = LU_FALSE;
				//Reset Current Absence timers
				paramsPtr->absenceParams.CurrentAbsenceDetected = 0;
				paramsPtr->absenceParams.InitialTimeofAbsenceMs = 0;
				paramsPtr->absenceParams.AbsenceDurationMs      = 0;
			}//PresenceDurationMs
		}//disable Current Presence Indication

		// Disable the Current Absence Indication if the threshold has been set to Zero
		if ((paramsPtr->absenceParams.CurrentAbsenceIndication == LU_FALSE) &&
				(paramsPtr->cfg.timedAbsenceCurrent > 0))
		{
			//Is the peakValue less than the timedAbsenceCurrent
			if (peakValue <= paramsPtr->cfg.timedAbsenceCurrent)
			{
				paramsPtr->absenceParams.CurrentAbsenceDetected++;
				if (paramsPtr->absenceParams.CurrentAbsenceDetected == 1)
				{
					paramsPtr->absenceParams.InitialTimeofAbsenceMs = timeMs;
				}

				paramsPtr->absenceParams.AbsenceDurationMs = timeMs;
				paramsPtr->absenceParams.AbsenceDurationMs = FPIElapsedTime(
							paramsPtr->absenceParams.InitialTimeofAbsenceMs,
								paramsPtr->absenceParams.AbsenceDurationMs);
			}

			//Have the Current Absence Indication timing condition been met
			if (paramsPtr->absenceParams.AbsenceDurationMs >=
					paramsPtr->cfg.minAbsenceDurationMs)
			{
				//Indication of Current Absence event
				//Reset timer variable
				paramsPtr->absenceParams.AbsenceDurationMs  = 0;
				paramsPtr->absenceParams.InitialTimeofAbsenceMs = 0;
				paramsPtr->absenceParams.CurrentAbsenceDetected = 0;
				//Set Current Absence Indication
				paramsPtr->absenceParams.CurrentAbsenceIndication = LU_TRUE;
				//Clear Current Presence Indication
				paramsPtr->absenceParams.CurrentPresenceIndication = LU_FALSE;
				//Reset Presence timers
				paramsPtr->absenceParams.CurrentPresenceDetected = 0;
				paramsPtr->absenceParams.InitialTimeofPresenceMs = 0;
				paramsPtr->absenceParams.PresenceDurationMs      = 0;
			}//PresenceDurationMs
		}//disable Current Absence Indication
	}
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
