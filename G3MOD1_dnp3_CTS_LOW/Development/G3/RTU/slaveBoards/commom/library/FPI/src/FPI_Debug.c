/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/04/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "FPI_Debug.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */
#define ST_TIME_OVERFLOW        (0xFFFFFFFF)

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
static CircularBufferStr circularBfr[8];
/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Initialises the circular buffer
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void CircularBufferInit(CircularBufferStr *cb, lu_uint32_t size)
{
	cb->size = size;
	cb->write = 0;
	cb->read = 0;
}

/*!
 ******************************************************************************
 *   \brief Returns the Circular Buffer Data Length
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_uint32_t CircularBufferDataLength(CircularBufferStr *cb)
{
    return ((cb->write - cb->read) & ((cb->size)-1));
}

/*!
 ******************************************************************************
 *   \brief Circular Buffer Write
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR CircularBufferWrite(CircularBufferStr *cb, lu_uint32_t data)
{
	SB_ERROR retError;

    if (CircularBufferDataLength(cb) == ((cb->size)-1))
    {
    	// Buffer full
        return retError = SB_ERROR_PARAM;
    }

    cb->buffer[cb->write] = data;
    cb->write = ((cb->write)+1) & ((cb->size)-1);
    return retError = SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Circular Buffer Read
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR CircularBufferRead(CircularBufferStr *cb, lu_uint32_t *data)
{
	SB_ERROR retError;

    if (CircularBufferDataLength(cb) == 0)
    {
    	// Buffer empty
        return SB_ERROR_PARAM;
    }

    *data = cb->buffer[cb->read];
    cb->read = (cb->read+1) & (cb->size-1);
    return retError = SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Initialises a single FPI channel
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
FPICfgAndParamsStr FPIChanInit( FPICfgAndParamsStr params)
{
	/* For development purposes only */
	/* Clear all phase parameters */
	params.phaseFaultParams.Alarm = LU_FALSE;
	params.phaseFaultParams.InstantAlarm = LU_FALSE;
	params.phaseFaultParams.FaultDurationMs = 0;
	params.phaseFaultParams.InitTimeofAlarmMs = 0;
	params.phaseFaultParams.InitTimeSinceInstantAlarmMs = 0;
	params.phaseFaultParams.InitialTimeofFaultMs = 0;
	params.phaseFaultParams.InstantFaultEventDetected = 0;
	params.phaseFaultParams.FaultEventDetected = 0;
	params.phaseFaultParams.TimeSinceAlarmMs = 0;
	params.phaseFaultParams.TimeSinceInstantAlarmMs = 0;
	/* Clear all earth parameters */
	params.earthFaultParams.Alarm = LU_FALSE;
	params.earthFaultParams.InstantAlarm = LU_FALSE;
	params.earthFaultParams.FaultDurationMs = 0;
	params.earthFaultParams.InitTimeofAlarmMs = 0;
	params.earthFaultParams.InitTimeSinceInstantAlarmMs = 0;
	params.earthFaultParams.InitialTimeofFaultMs = 0;
	params.earthFaultParams.InstantFaultEventDetected = 0;
	params.earthFaultParams.FaultEventDetected = 0;
	params.earthFaultParams.TimeSinceAlarmMs = 0;
	params.earthFaultParams.TimeSinceInstantAlarmMs = 0;
	/* Set configuration parameters */
	params.cfg.enabled = 1;
	params.cfg.fpiConfigChannel = 1;
	params.cfg.selfResetMs = 10000;
	params.cfg.phaseFault.faultCurrent = 1600;
	params.cfg.phaseFault.minFaultTimeMs = 70;
	params.cfg.earthFault.faultCurrent = 100;
	params.cfg.earthFault.minFaultTimeMs = 50;

	return params;
}

/*!
 ******************************************************************************
 *   \brief Resets a single FPI channel
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
FPICfgAndParamsStr FPIChanReset( FPICfgAndParamsStr params)
{
	params.phaseFaultParams.Alarm = LU_FALSE;
	params.phaseFaultParams.InstantAlarm = LU_FALSE;
	params.phaseFaultParams.FaultDurationMs = 0;
	params.phaseFaultParams.InitTimeofAlarmMs = 0;
	params.phaseFaultParams.InitTimeSinceInstantAlarmMs = 0;
	params.phaseFaultParams.InitialTimeofFaultMs = 0;
	params.phaseFaultParams.InstantFaultEventDetected = 0;
	params.phaseFaultParams.FaultEventDetected = 0;
	params.phaseFaultParams.TimeSinceAlarmMs = 0;
	params.phaseFaultParams.TimeSinceInstantAlarmMs = 0;

	params.earthFaultParams.Alarm = LU_FALSE;
	params.earthFaultParams.InstantAlarm = LU_FALSE;
	params.earthFaultParams.FaultDurationMs = 0;
	params.earthFaultParams.InitTimeofAlarmMs = 0;
	params.earthFaultParams.InitTimeSinceInstantAlarmMs = 0;
	params.earthFaultParams.InitialTimeofFaultMs = 0;
	params.earthFaultParams.InstantFaultEventDetected = 0;
	params.earthFaultParams.FaultEventDetected = 0;
	params.earthFaultParams.TimeSinceAlarmMs = 0;
	params.earthFaultParams.TimeSinceInstantAlarmMs = 0;

	return params;
}

/*!
 ******************************************************************************
 *   \brief FPI Phase Fault Detection
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
FPICfgAndParamsStr FPIPhaseFaultDetection(
							lu_uint32_t 		*peakValue,
							lu_uint32_t			*timeMs,
							FPICfgAndParamsStr 	params)
{
	//Reset the fault function if the phase peakValue has fallen below the
	//overcurrent threshold once the counter has been triggered at least once
	if ((params.phaseFaultParams.InitialTimeofFaultMs >= 1) && \
			((*peakValue) < params.cfg.phaseFault.faultCurrent))
	{
		//Reset overcurrent timers
		params.phaseFaultParams.FaultEventDetected = 0;
		params.phaseFaultParams.InitialTimeofFaultMs = 0;
		params.phaseFaultParams.FaultDurationMs = 0;
	}

	// If an instant overcurrent has not been detected previously
	// check the current phase value
	if ( params.phaseFaultParams.InitTimeSinceInstantAlarmMs == 0)
	{
		if ((*peakValue) >= params.cfg.phaseFault.faultCurrent)
		{
			params.phaseFaultParams.InstantFaultEventDetected++;
		}
	}

	// Disable the fault function if it has found a fault
	// The function is reset remotely or through a timer
	if ( params.phaseFaultParams.InitTimeofAlarmMs == 0)
	{
		//Is the phase peakValue greater than the low set fault
		//threshold level
		if ((*peakValue) >= params.cfg.phaseFault.faultCurrent)
		{
			params.phaseFaultParams.FaultEventDetected++;
			if (params.phaseFaultParams.FaultEventDetected == 1)
			{
				params.phaseFaultParams.InitialTimeofFaultMs = (*timeMs);
//				printf("%d\n", 55);
//				printf("%d\n", params.phaseFaultParams.InitialTimeofFaultMs);
			}

			params.phaseFaultParams.FaultDurationMs = (*timeMs);
		    if (params.phaseFaultParams.FaultDurationMs > \
		    		params.phaseFaultParams.InitialTimeofFaultMs)
		    {
		    	params.phaseFaultParams.FaultDurationMs = \
		    		(params.phaseFaultParams.FaultDurationMs - \
		        	params.phaseFaultParams.InitialTimeofFaultMs);
		    }
		    else
		    {
		    	params.phaseFaultParams.FaultDurationMs = \
		    		((ST_TIME_OVERFLOW - \
		    		params.phaseFaultParams.InitialTimeofFaultMs + \
		        	params.phaseFaultParams.FaultDurationMs) + 1);
		    }
		}
	}//disable overcurrent detect

	//Are the Overcurrent fault alarm condition met
	if (params.phaseFaultParams.FaultDurationMs >= \
			params.cfg.phaseFault.minFaultTimeMs)
	{
		//Alarm for overcurrent event
		//Load global fault timer
		params.phaseFaultParams.InitTimeofAlarmMs = (*timeMs);
		//Reset timer variable
		params.phaseFaultParams.FaultDurationMs  = 0;
		params.phaseFaultParams.InitialTimeofFaultMs = 0;
		params.phaseFaultParams.FaultEventDetected = 0;
		//Set alarm
		params.phaseFaultParams.Alarm = LU_TRUE;
	}//FaultDurationMs

	//instant overcurrent warning
	if (params.phaseFaultParams.InstantFaultEventDetected == 1)
	{
		params.phaseFaultParams.InitTimeSinceInstantAlarmMs = (*timeMs);
		params.phaseFaultParams.InstantFaultEventDetected = 0;
		//Set instantaneous alarm
		params.phaseFaultParams.InstantAlarm = LU_TRUE;
	}//InstantFaultEventDetected > 0

	//Reset overcurrent alarm
	if( params.phaseFaultParams.InitTimeofAlarmMs > 0)
	{
		params.phaseFaultParams.TimeSinceAlarmMs = (*timeMs);
	    if (params.phaseFaultParams.TimeSinceAlarmMs > \
	    		params.phaseFaultParams.InitTimeofAlarmMs)
	    {
	    	params.phaseFaultParams.TimeSinceAlarmMs = \
	    		(params.phaseFaultParams.TimeSinceAlarmMs  - \
	    		params.phaseFaultParams.InitTimeofAlarmMs);
	    }

	    else
	    {
	    	params.phaseFaultParams.TimeSinceAlarmMs = \
	    		((ST_TIME_OVERFLOW - \
	    		params.phaseFaultParams.InitTimeofAlarmMs + \
	    		params.phaseFaultParams.TimeSinceAlarmMs) + 1);
	    }

		if (params.phaseFaultParams.TimeSinceAlarmMs >= \
				params.cfg.selfResetMs)
		{
			//Clear alarms
			params.phaseFaultParams.Alarm = LU_FALSE;
			//Reset self timer variables
			params.phaseFaultParams.TimeSinceAlarmMs = 0;
			params.phaseFaultParams.InitTimeofAlarmMs = 0;
			params.phaseFaultParams.FaultEventDetected = 0;
		}
	}

	//Reset instant overcurrent alarm
	if( params.phaseFaultParams.InitTimeSinceInstantAlarmMs > 0)
	{
		params.phaseFaultParams.TimeSinceInstantAlarmMs = (*timeMs);
	    if (params.phaseFaultParams.TimeSinceInstantAlarmMs > \
	    		params.phaseFaultParams.InitTimeSinceInstantAlarmMs)
	    {
	    	params.phaseFaultParams.TimeSinceInstantAlarmMs = \
	    		(params.phaseFaultParams.TimeSinceInstantAlarmMs - \
	    		params.phaseFaultParams.InitTimeSinceInstantAlarmMs);
	    }

	    else
	    {
	    	params.phaseFaultParams.TimeSinceInstantAlarmMs = \
	    		((ST_TIME_OVERFLOW - \
	    		params.phaseFaultParams.InitTimeSinceInstantAlarmMs + \
	    		params.phaseFaultParams.TimeSinceInstantAlarmMs) + 1);
	    }

		if (params.phaseFaultParams.TimeSinceInstantAlarmMs >= \
				params.cfg.selfResetMs)
		{
			//Clear alarms
			params.phaseFaultParams.InstantAlarm = LU_FALSE;
			//Reset self timer variables
			params.phaseFaultParams.TimeSinceInstantAlarmMs = 0;
			params.phaseFaultParams.InitTimeSinceInstantAlarmMs = 0;
		}
	}

	return params;
}

/*!
 ******************************************************************************
 *   \brief FPI Earth Fault Detection
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
FPICfgAndParamsStr FPIEarthFaultDetection(
							lu_uint32_t 		*peakValue,
							lu_uint32_t			*timeMs,
							FPICfgAndParamsStr 	params)

{
	//Reset the low set fault function if the phase peakValue has fallen below the
	//Earth threshold  level once the counter has been triggered at least once
	if ((params.earthFaultParams.InitialTimeofFaultMs >= 1) && \
			((*peakValue) < params.cfg.earthFault.faultCurrent))
	{
		//Reset earth fault timer
		params.earthFaultParams.FaultEventDetected = 0;
		params.earthFaultParams.InitialTimeofFaultMs = 0;
		params.earthFaultParams.FaultDurationMs = 0;
	}

	// If an instant earth fault has not been detected previously
	// check the current phase value
	if ( params.earthFaultParams.InitTimeSinceInstantAlarmMs == 0)
	{
		if ((*peakValue) >= params.cfg.earthFault.faultCurrent)
		{
			params.earthFaultParams.InstantFaultEventDetected++;
		}
	}
	// Disable the fault function if it has found a fault
	// The function is reset remotely or through a timer
	if ( params.earthFaultParams.InitTimeofAlarmMs == 0)
	{
		//Is the neutral peakValue greater than the Earth fault
		//threshold level
		if ((*peakValue) >= params.cfg.earthFault.faultCurrent)
		{
			params.earthFaultParams.FaultEventDetected++;
			if (params.earthFaultParams.FaultEventDetected == 1)
			{
				params.earthFaultParams.InitialTimeofFaultMs = (*timeMs);
			}

			params.earthFaultParams.FaultDurationMs = (*timeMs);
		    if (params.earthFaultParams.FaultDurationMs > \
		    		params.earthFaultParams.InitialTimeofFaultMs)
		    {
		    	params.earthFaultParams.FaultDurationMs = \
		    		(params.earthFaultParams.FaultDurationMs - \
		        	params.earthFaultParams.InitialTimeofFaultMs);
		    }
		    else
		    {
		    	params.earthFaultParams.FaultDurationMs = \
		    		((ST_TIME_OVERFLOW - \
		    		params.earthFaultParams.InitialTimeofFaultMs + \
		        	params.earthFaultParams.FaultDurationMs) + 1);
		    }
		}
	}//disable earth detect

	//Are the earth fault alarm condition met
	if (params.earthFaultParams.FaultDurationMs >= \
			params.cfg.earthFault.minFaultTimeMs)
	{
		//Alarm for earth fault event by setting the fault timer
		params.earthFaultParams.InitTimeofAlarmMs = (*timeMs);
		//Reset timer variable
		params.earthFaultParams.FaultDurationMs = 0;
		params.earthFaultParams.InitialTimeofFaultMs = 0;
		params.earthFaultParams.FaultEventDetected = 0;
		//Set alarm
		params.earthFaultParams.Alarm = LU_TRUE;
	}//FaultDurationMs

	//instant earth fault warning
	if (params.earthFaultParams.InstantFaultEventDetected == 1)
	{
		params.earthFaultParams.InitTimeSinceInstantAlarmMs = (*timeMs);
		params.earthFaultParams.InstantFaultEventDetected = 0;
		//Set instantaneous alarm
		params.earthFaultParams.InstantAlarm = LU_TRUE;
	}//InstantFaultEventDetected > 0

	//Reset earth fault
	if( params.earthFaultParams.InitTimeofAlarmMs > 0)
	{
		params.earthFaultParams.TimeSinceAlarmMs = (*timeMs);
	    if (params.earthFaultParams.TimeSinceAlarmMs > \
	    		params.earthFaultParams.InitTimeofAlarmMs)
	    {
	    	params.earthFaultParams.TimeSinceAlarmMs  = \
	    		(params.earthFaultParams.TimeSinceAlarmMs - \
	    		params.earthFaultParams.InitTimeofAlarmMs);
	    }

	    else
	    {
	    	params.earthFaultParams.TimeSinceAlarmMs = \
	    		((ST_TIME_OVERFLOW - \
	    		params.earthFaultParams.InitTimeofAlarmMs + \
	    		params.earthFaultParams.TimeSinceAlarmMs) + 1);
	    }

		if (params.earthFaultParams.TimeSinceAlarmMs >= \
				params.cfg.selfResetMs)
		{
			//Clear alarms
			params.earthFaultParams.Alarm = LU_FALSE;
			//Reset self timer variables
			params.earthFaultParams.TimeSinceAlarmMs = 0;
			params.earthFaultParams.InitTimeofAlarmMs = 0;
			params.earthFaultParams.FaultEventDetected = 0;
		}
	}

	//Reset instant earth fault
	if( params.earthFaultParams.InitTimeSinceInstantAlarmMs > 0)
	{
		params.earthFaultParams.TimeSinceInstantAlarmMs = (*timeMs);
	    if (params.earthFaultParams.TimeSinceInstantAlarmMs > \
	    		params.earthFaultParams.InitTimeSinceInstantAlarmMs)
	    {
	    	params.earthFaultParams.TimeSinceInstantAlarmMs  = \
	    		(params.earthFaultParams.TimeSinceInstantAlarmMs - \
	    		params.earthFaultParams.InitTimeSinceInstantAlarmMs);
	    }

	    else
	    {
	    	params.earthFaultParams.TimeSinceInstantAlarmMs = \
	    		((ST_TIME_OVERFLOW - \
	    		params.earthFaultParams.InitTimeSinceInstantAlarmMs + \
	    		params.earthFaultParams.TimeSinceInstantAlarmMs) + 1);
	    }

		if (params.earthFaultParams.TimeSinceInstantAlarmMs >= \
				params.cfg.selfResetMs)
		{
			//Clear alarms
			params.earthFaultParams.InstantAlarm = LU_FALSE;
			//Reset self timer variables
			params.earthFaultParams.TimeSinceInstantAlarmMs = 0;
			params.earthFaultParams.InitTimeSinceInstantAlarmMs = 0;
		}
	}

	return params;
}
/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
