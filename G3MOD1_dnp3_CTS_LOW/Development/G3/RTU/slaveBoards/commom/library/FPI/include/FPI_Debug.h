/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.h 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/04/15      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _FPI_DEDUB_INCLUDED
#define _FPI_DEDUB_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
//#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

typedef struct CircularBufferDef
{
	lu_int32_t			buffer[256];
	lu_uint32_t			write;
	lu_uint32_t 		read;
    lu_uint32_t 		size;
 }CircularBufferStr;

/*FPI Fault Parameters*/
typedef struct FPIFaultParamsDef
{
	/* Fault duration */
	lu_uint32_t InitialTimeofFaultMs;

	/* Fault duration */
	lu_uint32_t FaultDurationMs;

	/* Flag for Instant event */
	lu_uint32_t	InstantFaultEventDetected;

	/* Flag for event */
	lu_uint32_t	FaultEventDetected;

	/* Initial time at which Instant Alarm triggered */
	lu_uint32_t InitTimeSinceInstantAlarmMs;

	/* Initial time at which Alarm triggered */
	lu_uint32_t InitTimeofAlarmMs;

	/* Duration since Alarm triggered */
	lu_uint32_t TimeSinceAlarmMs;

	/* Duration since Instant Alarm triggered */
	lu_uint32_t TimeSinceInstantAlarmMs;

	/* Alarm */
	lu_bool_t 	Alarm;

	/* Instant Alarm */
	lu_bool_t 	InstantAlarm;

	/* FPI Error Code */
//	SB_ERROR 	retError;
}FPIFaultParamsStr;

typedef struct FaultDef
{
	lu_uint16_t     minFaultTimeMs;
	lu_uint32_t     faultCurrent;  // x1 scaled units
}FaultStr;

typedef struct FPIConfigDef
{
	/* Channel Number */
	lu_uint8_t   	fpiConfigChannel; // FPI_CONFIG_CH

	lu_bool_t	 	enabled;

	FaultStr  		phaseFault;

	FaultStr 		earthFault;

	lu_uint32_t 	selfResetMs;
}FPIConfigStr;

typedef struct FPICfgAndParamsDef
{
	FPIConfigStr 		cfg;

	FPIFaultParamsStr 	phaseFaultParams;

	FPIFaultParamsStr 	earthFaultParams;
}FPICfgAndParamsStr;



/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern FPICfgAndParamsStr FPIChanInit( FPICfgAndParamsStr params);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern FPICfgAndParamsStr FPIChanReset( FPICfgAndParamsStr params);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern FPICfgAndParamsStr FPIPhaseFaultDetection(
								lu_uint32_t 			*peakValue,
								lu_uint32_t				*timeMs,
								FPICfgAndParamsStr 		params);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern FPICfgAndParamsStr FPIEarthFaultDetection(
								lu_uint32_t 			*peakValue,
								lu_uint32_t				*timeMs,
								FPICfgAndParamsStr	 	params);

#endif /* _FPI_INCLUDED */

/*
 *********************** End of file ******************************************
 */
