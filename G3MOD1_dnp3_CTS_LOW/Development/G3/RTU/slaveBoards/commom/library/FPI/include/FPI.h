/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPI]
 *
 *    FILE NAME:
 *               $Id: c_template.h 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/04/15      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _FPI_INCLUDED
#define _FPI_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "errorCodes.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */
#define FPI_TIME_OVERFLOW        (0xFFFFFFFF)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* FPI Fault Parameters */
typedef struct FPIFaultParamsDef
{
	/* Fault duration */
	lu_uint32_t InitialTimeofFaultMs;

	/* Fault duration */
	lu_uint32_t FaultDurationMs;

	/* Inrush duration */
	lu_uint32_t InrushDurationMs;

	/* Flag for Instant event */
	lu_uint32_t	InstantFaultEventDetected;

	/* Flag for event */
	lu_uint32_t	FaultEventDetected;

	/* Inrush Flag */
	lu_uint32_t InrushDetected;

	/* Initial time at which Instant Alarm triggered */
	lu_uint32_t InitTimeSinceInstantAlarmMs;

	/* Initial time at which Alarm triggered */
	lu_uint32_t InitTimeofAlarmMs;

	/* Initial time at which Inrush event started */
	lu_uint32_t InitTimeofInrushMs;

	/* Duration since Alarm triggered */
	lu_uint32_t TimeSinceAlarmMs;

	/* Duration since Instant Alarm triggered */
	lu_uint32_t TimeSinceInstantAlarmMs;

	/* Alarm */
	lu_bool_t 	Alarm;

	/* Instant Alarm */
	lu_bool_t 	InstantAlarm;
}FPIFaultParamsStr;

/* FPI Current Presence and Absence Parameters */
typedef struct FPICurrentAbsenceParamsDef
{
	/* Initial time at which the first Presence threshold crossing took place, in milliseconds*/
	lu_uint32_t 	InitialTimeofPresenceMs;

	/* Duration of the Current above Presence threshold*/
	lu_uint32_t 	PresenceDurationMs;

	/* Counter for Detected Current Presence events (half cycle update rate) */
	lu_uint32_t		CurrentPresenceDetected;

	/* Current Presence Indication */
	lu_bool_t		CurrentPresenceIndication;

	/* Initial time at which the first Absence threshold crossing took place, in milliseconds*/
	lu_uint32_t		InitialTimeofAbsenceMs;

	/* Duration of the Current above Absence threshold*/
	lu_uint32_t		AbsenceDurationMs;

	/* Counter for Detected Current Absence events (half cycle update rate) */
	lu_uint32_t		CurrentAbsenceDetected;

	/* Current Absence Indication */
	lu_bool_t		CurrentAbsenceIndication;
}FPICurrentAbsenceParamsStr;

typedef struct FPIConfigDef
{
	lu_bool_t	 	enabled;
	lu_uint16_t     minFaultDurationMs;
	lu_uint16_t		minInrushDurationMs;
	lu_uint32_t		instantFaultCurrent;
	lu_uint32_t		inrushCurrent;
	lu_uint32_t     timedFaultCurrent;  // x1 scaled units

	lu_uint32_t		timedPresenceCurrent;
	lu_uint32_t		minPresenceDurationMs;

	lu_uint32_t		timedAbsenceCurrent;
	lu_uint32_t		minAbsenceDurationMs;
}FPIConfigStr;

typedef struct FPICfgAndParamsDef
{
	FPIConfigStr 				cfg;

	FPIFaultParamsStr 			faultParams;

	FPICurrentAbsenceParamsStr 	absenceParams;
}FPICfgAndParamsStr;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FPIChanInit(FPICfgAndParamsStr *paramsPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FPIChanReset( FPICfgAndParamsStr *paramsPtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR FPIChanTest(FPICfgAndParamsStr *paramsPtr);
/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void FPIPhaseFaultDetection(FPICfgAndParamsStr 	*paramsPtr,
		                           lu_uint32_t 			peakValue,
								   lu_uint32_t			timeMs);

/*!
 ******************************************************************************
 *   \brief FPI Current Absence or Presence
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FPICurrentAbsence(FPICfgAndParamsStr 	*paramsPtr,
						lu_uint32_t 		peakValue,
						lu_uint32_t			timeMs);

/*!
 ******************************************************************************
 *   \brief Return system time
 *
 *   Time resolution is 1ms. Maximum elapsed time ~49.7 days
 *
 *   \return Tiem in ms
 *
 ******************************************************************************
 */
inline static lu_uint32_t FPIElapsedTime(lu_uint32_t timeStart, lu_uint32_t timeEnd);
inline static lu_uint32_t FPIElapsedTime(lu_uint32_t timeStart, lu_uint32_t timeEnd)
{
    if (timeEnd > timeStart)
    {
        return (timeEnd - timeStart);
    }
    else
    {
        return ( (FPI_TIME_OVERFLOW - timeStart + timeEnd) + 1);
    }
}

#endif /* _FPI_INCLUDED */

/*
 *********************** End of file ******************************************
 */
