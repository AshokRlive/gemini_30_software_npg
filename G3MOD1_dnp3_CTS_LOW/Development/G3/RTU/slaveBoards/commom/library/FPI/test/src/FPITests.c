/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [FPI Tests]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/04/15     saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "FPITests.h"
#include "FPI_Debug.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	FPI_CH_0_PHASE_A = 0,
	FPI_CH_0_PHASE_B = 1,
	FPI_CH_0_PHASE_C = 2,
	FPI_CH_0_NEUTRAL = 3,
	FPI_CH_1_PHASE_A = 4,
	FPI_CH_1_PHASE_B = 5,
	FPI_CH_1_PHASE_C = 6,
	FPI_CH_1_NEUTRAL = 7,

	FPI_CH_LAST
}FPI_CH;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
lu_bool_t FpiTestAssertEqualInt(lu_uint32_t expectedValue, lu_uint32_t actualValue);
void FpiChanInitParamsCleared(void);
void FpiChanInitCfgSet(void);
void InstantPhaseFaultAlarm(void);
void InitialTimeofFaultMs(void);
void FaultDurationMs(void);
void FaultDetectedAndReset(void);
void FaultDetectedResetandRetriggered(void);
void FaultDetectedTimerOverflowOperational(void);
void AlarmReset(void);
void InstantAlarmReset(void);
void InstantAlarmResetPhaseB(void);
void EarthParamsCleared(void);
void EarthCfgSet(void);
void InstantEarthFaultAlarm(void);
void InitialTimeofEarthFault(void);
void EarthFaultDetectThenReset(void);
void EarthFaultDetectResetThenRetrigger(void);
void EarthFaultAlarmReset(void);
void EarthFaultInstantAlarmReset(void);
void PhaseAndEarthFaultAlarm(void);


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
//FILE *filePtr;
//static FPIConfigAndParamsStr fpiConfigAndParams[FPI_CH_LAST];
/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

int main()
{

	FpiChanInitParamsCleared();
	FpiChanInitCfgSet();
	InstantPhaseFaultAlarm();
	InitialTimeofFaultMs();
	FaultDurationMs();
	FaultDetectedAndReset();
	FaultDetectedResetandRetriggered();
	FaultDetectedTimerOverflowOperational();
	AlarmReset();
	InstantAlarmReset();
	InstantAlarmResetPhaseB();
	EarthParamsCleared();
	EarthCfgSet();
	InstantEarthFaultAlarm();
	InitialTimeofEarthFault();
	EarthFaultDetectThenReset();
	EarthFaultDetectResetThenRetrigger();
	EarthFaultAlarmReset();
	EarthFaultInstantAlarmReset();
	PhaseAndEarthFaultAlarm();


	return 0;
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Check
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t FpiTestAssertEqualInt(lu_uint32_t expectedValue, lu_uint32_t actualValue)
{
	if (expectedValue == actualValue)
	{
		printf("-- Pass --\n");
		return LU_TRUE;
	}

	else
	{
		printf("-- Fail --\n");
		return LU_FALSE;
	}
}

/*!
 ******************************************************************************
 *   \brief Check FPIChanInit clears phase and earth parameters
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FpiChanInitParamsCleared(void)
{
	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	printf("\n");
	printf("FpiChanInitParamsCleared\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.FaultDurationMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InitialTimeofFaultMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InitTimeSinceInstantAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InitTimeofAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InstantFaultEventDetected);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.FaultEventDetected);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.TimeSinceAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.TimeSinceInstantAlarmMs);

	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.FaultDurationMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.InitialTimeofFaultMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.InitTimeSinceInstantAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.InitTimeofAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.InstantFaultEventDetected);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.FaultEventDetected);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.TimeSinceAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].earthFaultParams.TimeSinceInstantAlarmMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIChanInit sets configuration values
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FpiChanInitCfgSet(void)
{
	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	printf("FpiChanInitCfgSet\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.enabled);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.fpiConfigChannel);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.selfResetMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.phaseFault.faultCurrent);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.phaseFault.minFaultTimeMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.earthFault.faultCurrent);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].cfg.earthFault.minFaultTimeMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection detects Instant Fault
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void InstantPhaseFaultAlarm(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 1;
	lu_int32_t 		peakValue[1] = {1610};
	lu_uint32_t 	timeMs[1] = {10};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("InstantPhaseFaultAlarm\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InstantAlarm);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection loads InitialTimeofFaultMs
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void InitialTimeofFaultMs(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 1;
	lu_int32_t 		peakValue[1] = {1610};
	lu_uint32_t 	timeMs[] = {10};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("InitialTimeofFaultMs\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InitialTimeofFaultMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection loads FaultDurationMs
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FaultDurationMs(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 2;
	lu_int32_t 		peakValue[2] = {1610, 1610};
	lu_uint32_t 	timeMs[2] = {10, 20};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("FaultDurationMs\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.FaultDurationMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection detects an fault and then
 *   resets after the fault has cleared.
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FaultDetectedAndReset(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 3;
	lu_int32_t 		peakValue[3] = {1610, 1610, 1590};
	lu_uint32_t 	timeMs[3] = {10, 20, 30};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("FaultDetectedAndReset\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.FaultDurationMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection detects a fault, resets and
 *   re-triggers again.
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FaultDetectedResetandRetriggered(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 10;
	lu_int32_t 		peakValue[10] = {1610, 1590, 1610, 1610, 1610, 1610, 1610, 1610, 1610, 1610};
	lu_uint32_t 	timeMs[10] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("FaultDetectedResetandRetriggered\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.FaultDurationMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection timer overflow operates.
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void FaultDetectedTimerOverflowOperational(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 4;
	lu_int32_t 		peakValue[4] = {1610, 1610, 1610, 1610};
	lu_uint32_t 	timeMs[4] = { 4294967275, 4294967285, 4294967295, 9};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("FaultDetectedTimerOverflowOperational\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.FaultDurationMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection Alarm resets after cfg.selfreset
 *   threshold crosses
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void AlarmReset(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 10;
	lu_int32_t 		peakValue[10] = {1610, 1610, 1610, 1610, 1610, 1610, 1610, 1610, 1590, 1590};

	lu_uint32_t 	timeMs[10] = { 10, 20, 30, 40, 50, 60, 70, 80, 10070, 10080};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("AlarmReset\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.TimeSinceAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.Alarm);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection instant Alarm resets after cfg.selfreset
 *   threshold crosses
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void InstantAlarmReset(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 3;
	lu_int32_t 		peakValue[3] = {1610, 1590, 1590};

	lu_uint32_t 	timeMs[3] = { 10, 10010, 10020};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_A]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_A] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_A]);
	}

	printf("InstantAlarmReset\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.TimeSinceInstantAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.InstantAlarm);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIPhaseFaultDetection instant Alarm resets after cfg.selfreset
 *   threshold crosses
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void InstantAlarmResetPhaseB(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 3;
	lu_int32_t 		peakValue[3] = {1610, 1610, 1610};

	lu_uint32_t 	timeMs[3] = { 10, 20, 30};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_PHASE_B] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_PHASE_B]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_PHASE_B] = FPIPhaseFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_PHASE_B]);
	}

	printf("InstantAlarmResetPhaseB\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_B].phaseFaultParams.TimeSinceInstantAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_B].phaseFaultParams.InstantAlarm);
	printf("\n");
}
/*!
 ******************************************************************************
 *   \brief Check FPIChanInit clears phase and earth parameters
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void EarthParamsCleared(void)
{
	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	printf("\n");
	printf("Earth - ParamsCleared\n");

	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.FaultDurationMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitialTimeofFaultMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitTimeSinceInstantAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitTimeofAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InstantFaultEventDetected);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.FaultEventDetected);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.TimeSinceAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.TimeSinceInstantAlarmMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIChanInit sets configuration values
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void EarthCfgSet(void)
{
	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	printf("Earth - CfgSet\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.enabled);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.fpiConfigChannel);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.selfResetMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.phaseFault.faultCurrent);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.phaseFault.minFaultTimeMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.earthFault.faultCurrent);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].cfg.earthFault.minFaultTimeMs);
	printf("\n");
}
/*!
 ******************************************************************************
 *   \brief Check FPIEarthFaultDetection detects Instant Fault
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void InstantEarthFaultAlarm(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 1;
	lu_int32_t 		peakValue[1] = { 110};
	lu_uint32_t 	timeMs[1] = { 10};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIEarthFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_NEUTRAL]);
	}

	printf("Earth - InstantFaultAlarm\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InstantAlarm);
	printf("\n");
}
/*!
 ******************************************************************************
 *   \brief Check FPIEarthFaultDetection detects InitialTimeofFaultMs
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void InitialTimeofEarthFault(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 1;
	lu_int32_t 		peakValue[1] = { 110};
	lu_uint32_t 	timeMs[1] = { 10};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIEarthFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_NEUTRAL]);
	}

	printf("Earth - InitialTimeofEarthFault\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitialTimeofFaultMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIEarthFaultDetection detects a fault and then resets
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void EarthFaultDetectThenReset(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 2;
	lu_int32_t 		peakValue[2] = { 110, 90};
	lu_uint32_t 	timeMs[2] = { 10, 20};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIEarthFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_NEUTRAL]);
	}

	printf("Earth - FaultDetectThenReset\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.FaultDurationMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIEarthFaultDetection detects an instant fault, resets and
 *   re-triggers on a full fault again.
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void EarthFaultDetectResetThenRetrigger(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 8;
	lu_int32_t 		peakValue[8] = { 110, 90, 110, 110, 110, 110, 110, 110};
	lu_uint32_t 	timeMs[8] = { 10, 20, 30, 40, 50, 60, 70, 80};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIEarthFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_NEUTRAL]);
	}

	printf("Earth - FaultDetectResetThenRetrigger\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.FaultDurationMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIEarthFaultDetection detects a fault and resets after
 *   cfg.selfreset threshold crosses
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void EarthFaultAlarmReset(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 8;
	lu_int32_t 		peakValue[8] = { 110, 110, 110, 110, 110, 110, 90, 90};
	lu_uint32_t 	timeMs[8] = { 10, 20, 30, 40, 50, 60, 10060, 10070};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIEarthFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_NEUTRAL]);
	}

	printf("Earth - FaultAlarmReset\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.TimeSinceAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitTimeofAlarmMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check FPIEarthFaultDetection detects an instant fault and resets after
 *   cfg.selfreset threshold crosses
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void EarthFaultInstantAlarmReset(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		lengthOfData = 2;
	lu_int32_t 		peakValue[2] = { 110, 90};
	lu_uint32_t 	timeMs[2] = { 10, 10010};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIChanInit( fpiConfigAndParams[FPI_CH_0_NEUTRAL]);

	for(i = 0; i < lengthOfData; i++)
	{
		fpiConfigAndParams[FPI_CH_0_NEUTRAL] = FPIEarthFaultDetection(
													&peakValue[i],
													&timeMs[i],
													fpiConfigAndParams[FPI_CH_0_NEUTRAL]);
	}

	printf("Earth - InstantAlarmReset\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InstantAlarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.TimeSinceInstantAlarmMs);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitTimeSinceInstantAlarmMs);
	printf("\n");
}

/*!
 ******************************************************************************
 *   \brief Check Phase and earth fault alarms trigger
 *
 *   Detailed description
 *
 *   \param
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void PhaseAndEarthFaultAlarm(void)
{
	lu_uint8_t 		i = 0;
	lu_uint8_t 		y = 0;
	lu_uint8_t 		lengthOfData = 8;
	lu_int32_t 		peakPhaseValue[8] = { 1610, 1610, 1610, 1610, 1610, 1610, 1610, 1610};
	lu_int32_t 		peakEarthValue[8] = { 110, 110, 110, 110, 110, 110, 90, 90};
	lu_uint32_t 	timeMs[8] = { 10, 20, 30, 40, 50, 60, 70, 80};

	FPICfgAndParamsStr fpiConfigAndParams[FPI_CH_LAST];

	for (y = 0; y < FPI_CH_LAST; y++)
	{
		fpiConfigAndParams[y] = FPIChanInit( fpiConfigAndParams[y]);
	}
	y=0;

	for (y = 0; y < FPI_CH_LAST; y++)
	{
		for(i = 0; i < lengthOfData; i++)
		{
			if((y==FPI_CH_0_NEUTRAL) || (y==FPI_CH_1_NEUTRAL))
			{
				fpiConfigAndParams[y] = FPIEarthFaultDetection(
															&peakEarthValue[i],
															&timeMs[i],
															fpiConfigAndParams[y]);
			}
			else
			{
				fpiConfigAndParams[y] = FPIPhaseFaultDetection(
															&peakPhaseValue[i],
															&timeMs[i],
															fpiConfigAndParams[y]);
			}
		}
	}

	printf("PhaseAndEarthFaultAlarm\n");
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_PHASE_A].phaseFaultParams.Alarm);
	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.Alarm);
//	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.TimeSinceInstantAlarmMs);
//	printf("%u\n", fpiConfigAndParams[FPI_CH_0_NEUTRAL].earthFaultParams.InitTimeSinceInstantAlarmMs);
	printf("\n");
}
/*
 *********************** End of file ******************************************
 */
