/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/12      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CALIBRATION_INCLUDED
#define _CALIBRATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include <string.h>
#include "lu_types.h"

#include "errorCodes.h"
#include "CANProtocolCodec.h"

#pragma pack(1)

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define CALID_ELEMENT(calID) \
	{calID, 0xff, 0, 0L},

#define CALID_ELEMENT_LAST() \
	{0xff, 0xff, 0, 0L}

#define CAL_ID_NA					(CAL_ID_LAST)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Type of calibrated value */
typedef enum
{
	CAL_TYPE_1DU16S16  = 0,
	CAL_TYPE_1DU16U16     ,
	CAL_TYPE_1DU16U32	  ,
	CAL_TYPE_1DU16F32	  ,
	CAL_TYPE_1DU32U32	  ,
	CAL_TYPE_1DU16U16_CT_TEMP_COMP,
	CAL_TYPE_1DU16U16_CT_TEMP_COMP_SUM,
	CAL_TYPE_1DU16U16_DSM_E_CT_COMP,
	CAL_TYPE_ADE78XX_REG,
	CAL_TYPE_LAST
}CAL_TYPE;


/* Calibration ID to Calibration Data Mapping */
typedef struct CalElementDef
{
	lu_uint8_t    calID;
	lu_int8_t     calType;
	lu_int16_t    calDatalength;
	lu_uint8_t    *calDataPtr;
} CalElementStr;

/* Calibration Data format in memory */
typedef struct CalDataHeaderdef
{
	lu_uint8_t calId;
	lu_uint8_t type;
	lu_int16_t length;
} CalDataHeaderStr;

#pragma pack()

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
  *   \brief Initialisation of Calibration table and CAN decoder
 *
  *   Initialise the calibration table for specific slave board and CAN decoder
  *   for calibration messages.
  *
  *   \param void
  *
  *   \return Error code
  *
  ******************************************************************************
  */
extern SB_ERROR CalibrationInit(void);

/*!
  ******************************************************************************
  *   \brief Initialise the calibration table and keep the address of next calibration
  *    element to be appended
  *
 *   Detailed description
 *
  *   \param calDataPtr ,size
 *
  *   \return Error code
 *
  ******************************************************************************
  */

extern SB_ERROR CalibrationInitCalDataPtr(lu_uint8_t *calDataPtr, lu_uint16_t size, lu_uint16_t maxCalId);

/*!
  ******************************************************************************
  *   \brief Initialise the CAN Protocol Decoder for calibration commands
 *
  *   Detailed description
  *
  *   \param msgPtr ,time
  *
  *   \return Error code
  *
 ******************************************************************************
 */

extern SB_ERROR CalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

/*!
 ******************************************************************************
 *   \brief To erase all calibration elements
 *
 *   Erasing of all calibration elements and data located in NV RAM
 *
 *   \param void
 *
 *
 *   \return Error code
 *
 ******************************************************************************
 */

extern SB_ERROR CalibrationEraseAllElements(void);

/*!
  ******************************************************************************
  *   \brief To get calibrated value from corresponding algorithms by using calibration ID's type
  *
  *   To get calibrated value from corresponding algorithm by using calibrations ID's type via
  *   stored table of location of data for particular ID. (boardCalTable).
  *   And storing the response with error flag.
  *
  *   \param calId - Calibration ID
  *   \param inValue - Raw value to be calibrated
  *   \param *outValuePtr - pointer to converted value
  *
  *   \return Error code - SB_ERROR
  *
  ******************************************************************************
 */
extern SB_ERROR CalibrationConvertValue(lu_uint8_t calId, lu_int32_t inValue, lu_int32_t *outValuePtr);

/*!
  ******************************************************************************
  *   \brief To get calibration data table  by using calibration ID and type
  *
  *   To get calibrated value from stored table of location of data for particular ID (boardCalTable).
  *
  *   \param calId       - Calibration ID
  *   \param calType     - Calibration Type
  *   \param *calDataPtr - pointer to calibration data
  *
  *   \return Error code - SB_ERROR
  *
  ******************************************************************************
 */
extern SB_ERROR CalibrationGetCalElement(lu_uint8_t calId, lu_uint8_t calType, CalElementStr *calElement);

/*!
 ******************************************************************************
 *   \brief Writing the calibration element structure to memory (Flash or NV Ram)
 *
 *	 Writing the calibration element structure to memory in the form of
 *	 ID,Type, Length (number of bytes), the actual data need for calibration.
 *	 Note: No address of data is stored.
 *
 *   After writing calibration elements in to memory, this function will update
 *	 the board specific calibration table as well.
 *
 *   \param calElement - Calibration Element in the form of structure CalElementStr
 *
 *   \return Error code
 *
 ******************************************************************************
 */
extern SB_ERROR CalibrationAddElement(lu_uint8_t    calID,
								      lu_uint8_t    calType,
								      lu_uint16_t   calDatalength,
								      lu_uint8_t    *calDataPtr
								     );

extern SB_ERROR CalibrationInterChangeColumns(lu_uint8_t calID,
									   lu_uint32_t minInput,
									   lu_uint32_t maxInput,
									   lu_uint32_t startOutput,
									   lu_uint32_t endOutput
									  );


#endif /* _CALIBRATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
