/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       CT Temperature compensation for ADC values
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/07/14      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CTTempCompensation.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define TEMP_COMPENSATION_PARAM  0.000027

#define TCP                      TEMP_COMPENSATION_PARAM

/* Temperature Compensation Scale Offset Parameter*/
#define TCOSP                    25

/* Temperature Compensation Scale Parameter*/
#define TCSP                     0.5

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

lu_uint32_t CTTempCompensationBySlopeTable( lu_uint64_t measuredCurrent,
										    lu_int32_t  temperature,
										    lu_int32_t compensationFactor
										  )
{
	 //measuredCurrent = ((measuredCurrent << 14) - ((measuredCurrent << 12) + (measuredCurrent << 11) + ((measuredCurrent <<  8) - (measuredCurrent << 4 )    )));
	 //measuredCurrent = (measuredCurrent >> 13);

	if(temperature >= -20000 && temperature <= -10000)
	{
		measuredCurrent = measuredCurrent + (measuredCurrent * (4 * compensationFactor * 0.0001));
	}

	else if(temperature >= -10000 && temperature <= 0)
	{
  		measuredCurrent = measuredCurrent + (measuredCurrent * (3 * compensationFactor * 0.0001));
  	}

	else if(temperature >= 0 && temperature <= 10000)
 	{
 		measuredCurrent = measuredCurrent + (measuredCurrent * (2 * compensationFactor * 0.0001));
 	}

	else if(temperature >= 10000 && temperature <= 20000)
	{
		measuredCurrent = measuredCurrent + (measuredCurrent * (1 * compensationFactor * 0.0001));
	}

	else if(temperature >= 20000 && temperature <= 30000)
	{
		measuredCurrent = measuredCurrent;
	}

	else if(temperature >= 30000 && temperature <= 40000)
	{
		measuredCurrent = measuredCurrent - (measuredCurrent * (1 * compensationFactor  * 0.0001));
	}

	else if(temperature >= 40000 && temperature <= 50000)
	{
		measuredCurrent = measuredCurrent - (measuredCurrent * ( 2 * compensationFactor * 0.0001));
	}

	else if(temperature >= 50000 && temperature <= 60000)
	{
		measuredCurrent = measuredCurrent - (measuredCurrent * ( 3 * compensationFactor * 0.0001));
	}

	else if(temperature >= 60000 && temperature <= 70000)
	{
		measuredCurrent = measuredCurrent - (measuredCurrent * ( 4 * compensationFactor * 0.0001));
	}

	else
	{
		measuredCurrent = measuredCurrent;
	}

	return measuredCurrent;
}

 lu_uint32_t CTTempCompensation(lu_uint32_t measuredCurrent, lu_int32_t temperature)
 {
 	lu_int64_t actualCurrent, dt , TCS;

 	measuredCurrent = measuredCurrent / 10;
 	temperature     = temperature   / 1000;

 	dt = (TCOSP - temperature);

    TCS = ((((100 * TCSP) - 10)) / 10000) * 100000000;

    /*!**************************************************************************************|
    |																                         |
    |  actualCurrent = ( measuredCurrent * ( 1 + (TCS * dt) ) ) * ( 1 + ( dt * dt * TCP));   |
    |																                         |
    |****************************************************************************************/

 	actualCurrent =  ( measuredCurrent                            ) +
 					 ( measuredCurrent  * dt * dt * TCP           ) +
 					 ( TCS * dt * measuredCurrent                 ) / 100000000 +
 					 ( TCS * dt * measuredCurrent * dt * dt * TCP ) / 100000000 ;

 	return actualCurrent * 10;
 }


 lu_uint32_t CTLinearTempCompensation(lu_uint32_t measuredCurrent, lu_int32_t temperature)
 {
 	lu_int64_t actualCurrent;

 	temperature     = ((temperature / 1000) - 25);

    /*!**************************************************************************************|
    |																                         |
    |  actualCurrent = ( measuredCurrent - (measuredCurrent * 0.0035 * temperature)); 	     |
    |			y	 =      c			 + (					   a *      x  	   )		 |
    |****************************************************************************************/
 	//The input current needs to be compensated ~3.5% for every 10C temperature increase
 	//Offset point 'c' is set at 25C

 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*38*temperature));

 		return actualCurrent / 10000;
 }


 lu_uint32_t CTTempCompensationSum(lu_uint32_t measuredCurrent, lu_int32_t temperature)
 {
 	lu_int64_t actualCurrent = 0;

    /*!**************************************************************************************|
    |																                         |
    |  actualCurrent = ( measuredCurrent - (measuredCurrent * 0.0035 * temperature)); 	     |
    |			y	 =      c			 + (					   a *      x  	   )		 |
    |****************************************************************************************/

 	// Apply  linear compensation up too 0C
 	if (temperature <= 0)
 	{
 	 	//Offset point 'c' is set at 25C
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) + (measuredCurrent*144*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply  linear compensation from 0.125C too 20C
 	else if (temperature <= 20000)
 	{
 	 	//Offset point 'c' is set at 25C
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) + (measuredCurrent*120*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply linear compensation below from 20.125C too 30C
 	else if (temperature <= 30000)
 	{
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*192*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply linear compensation below from 30.125C too 40C
 	else if (temperature <= 40000)
 	{
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*195*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply  linear compensation below from 40.125C too 50C
 	else if (temperature <= 50000)
 	{
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*220*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply linear compensation below from 50.125C too 55C
 	else if (temperature <= 55000)
 	{
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*225*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply linear compensation below from 55.125C too 60C
 	else if (temperature <= 60000)
 	{
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*225*temperature));

 		return actualCurrent / 10000;
 	}

 	// Apply linear compensation below from 60.125C too 65C
 	else if (temperature <= 65000)
 	{
 		temperature     = ((temperature / 1000) - 25);
 		//Add compensation
 		actualCurrent = (( measuredCurrent*10000) - (measuredCurrent*225*temperature));

 		return actualCurrent / 10000;
 	}

 	return actualCurrent / 10000;
 }



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */

