/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Linear interpolation tests public header file
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/07/14      venkat_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _CTTEMPCOMPENSATION_INCLUDED
#define _CTTEMPCOMPENSATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
  ******************************************************************************
  *   \brief Adjusting values by slope
  *
  *   \param
  *   \param
  *
  *   \return
  *
  ******************************************************************************
  */
lu_uint32_t CTTempCompensationBySlopeTable( lu_uint64_t measuredCurrent,
										    lu_int32_t  temperature,
										    lu_int32_t compensationFactor
										  );

/*!
  ******************************************************************************
  *   \brief Temperature Compensation for Current Transformers
  *
  *   \param
  *   \param
  *
  *   \return
  *
  ******************************************************************************
  */
lu_uint32_t CTTempCompensation(lu_uint32_t actualCurrent, lu_int32_t temperature);

/*!
  ******************************************************************************
  *   \brief Temperature Compensation for Current Transformers
  *
  *   \param
  *   \param
  *
  *   \return
  *
  ******************************************************************************
  */
lu_uint32_t CTLinearTempCompensation(lu_uint32_t measuredCurrent, lu_int32_t temperature);

/*!
  ******************************************************************************
  *   \brief Temperature Compensation for Current Transformers
  *
  *   \param
  *   \param
  *
  *   \return
  *
  ******************************************************************************
  */
lu_uint32_t CTTempCompensationSum(lu_uint32_t measuredCurrent, lu_int32_t temperature);


#endif /* _CTTEMPCOMPENSATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
