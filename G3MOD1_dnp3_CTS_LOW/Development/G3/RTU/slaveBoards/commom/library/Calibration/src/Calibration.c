/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/12      saravanan_v    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "Calibration.h"
#include "LinearInterpolation.h"
#include "CTTempCompensation.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define CAL_ID_NONE -1

#define MAX_TEMP_COMP_SUM  10

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*!
 *  Calibration data parameters
 */
typedef struct calDataParamDef
{
		lu_uint8_t   *calDataStartPtr;
		lu_uint8_t   *calDataNextPtr;
		lu_uint16_t   calDataSize;
		lu_uint16_t   calDataMaxSize;
		lu_int16_t    lastCalId;

} calDataParamStr;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR CalibrationGetCalTableSize(lu_uint8_t  calType, lu_uint16_t calDatalength, lu_uint8_t *tableSize);
SB_ERROR CalibrationReadElement(CalTstCGetDataStr *calIDPtr);
SB_ERROR CalDataCheck(lu_int8_t calID, CalDataHeaderStr *calElementPtr);

void CalTestConvertValue(CalTstConvValueStr *CalTstConvValuePtr);
void CalTestAddElement(lu_uint16_t msgLen, CalDataHeaderStr *calDataPtr);
void CalTestEraseElements(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static calDataParamStr calDataParam = {0L, 0L, 0, 0, -1};


/*! List of supported message */
static filterTableStr CalibrationModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Bootloader test API commands */
	{  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_CONVERT_VALUE_C      , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_ERASE_ELEMENTS_C     , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_ADD_ELEMENT_C        , LU_FALSE , 0      },
    {  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_ADD_ELEMENT_C        , LU_FALSE , 1      },
	{  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_READ_ELEMENT_C       , LU_FALSE , 0      },
};

static const LnInterpTable1DS32U32Str TempCompSum1dS32U32[MAX_TEMP_COMP_SUM] =
{
		 //TempCompSum1dS32U32 10 Points


		 //Input Temperature value ,       Outputvalue(0.1 ma offset - physical chan units)

		{-10000,							0},
		{0,									0},
		{10000,								0},
		{20000,								0},
		{30000,								0},
		{40000,								0},
		{45000,								0},
		{50000,								0},
		{550000,							0},
		{600000,							0}
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR CalibrationInit(void)
{
	/* To Initialise Board specific calibration table */
	BoardCalibrationInit();

	/* To add calibration commands to CAN filter */
	return CANFramingAddFilter( CalibrationModulefilterTable,
				                SU_TABLE_SIZE(CalibrationModulefilterTable, filterTableStr)
	                          );
}

SB_ERROR CalibrationInitCalDataPtr(lu_uint8_t *calDataPtr, lu_uint16_t size, lu_uint16_t maxCalId)
{
	lu_uint16_t calID;
	CalDataHeaderStr *calElementPtr;
	SB_ERROR retValue;

	retValue = SB_ERROR_NONE;

	/* Initialisation of calibration data parameters */
	calDataParam.calDataStartPtr  = calDataPtr;
	calDataParam.calDataNextPtr   = calDataPtr;
	calDataParam.calDataMaxSize   = size;
	calDataParam.lastCalId        = CAL_ID_NONE;

    /* Check for valid Parameters */
    if( (calDataPtr != NULL) && (size > 0) )
    {
    	/* To traverse by calibration ID and storing the location of Next data to be written on NV RAM or Flash */
		for(calID = 0; calID < maxCalId; calID++)
		{
			/* Set current Calibration element for next available */
			calElementPtr = (CalDataHeaderStr *)calDataParam.calDataNextPtr;

			if (CalDataCheck(calID, calElementPtr) == SB_ERROR_NONE)
			{
				/* To check sequence order of calibration elements */
				if (calElementPtr->calId == calID)
				{
					 /* Filling of Board specific calibration table */
					 boardCalTable[calID].calType        = calElementPtr->type;
					 boardCalTable[calID].calDatalength  = calElementPtr->length;
					 boardCalTable[calID].calDataPtr     = (lu_uint8_t *)calElementPtr;
					 boardCalTable[calID].calDataPtr    += sizeof(CalDataHeaderStr);

					 /* Updating calibration data parameters */
					 calDataParam.calDataNextPtr   = (lu_uint8_t *)calElementPtr;
					 calDataParam.calDataNextPtr  += sizeof(CalDataHeaderStr);
					 calDataParam.calDataNextPtr  += calElementPtr->length;
					 calDataParam.lastCalId        = calElementPtr->calId;
				}
				else
				{
					 /* If the calibration IDs are not in sequential order, break the search */
					 retValue = SB_ERROR_CAL_SEQUENCE;
					 break;
				}
		   }
		   else
		   {
				/* If the calibration table is empty */
				retValue = SB_ERROR_INITIALIZED;
				break;
		   }
		}
	}
    else
    {
    	retValue = SB_ERROR_PARAM;
    }

    return retValue;
}


SB_ERROR CalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retVal;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CALTST:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CALTST_CONVERT_VALUE_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(CalTstConvValueStr))
			{
				CalTestConvertValue((CalTstConvValueStr *)msgPtr->msgBufPtr);
			}
			break;

		case MODULE_MSG_ID_CALTST_ADD_ELEMENT_C:
			/* Message sanity check */
			if (msgPtr->msgLen > MODULE_MESSAGE_SIZE(CalDataHeaderStr))
			{
				CalTestAddElement(msgPtr->msgLen, (CalDataHeaderStr *)msgPtr->msgBufPtr);
			}
	        break;

		case MODULE_MSG_ID_CALTST_ERASE_ELEMENTS_C:
			/* Message sanity check */
			if (msgPtr->msgLen == 0)
			{
				CalTestEraseElements();
			}
			break;

		case MODULE_MSG_ID_CALTST_READ_ELEMENT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(CalTstCGetDataStr))
			{
				CalibrationReadElement((CalTstCGetDataStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retVal;
}

SB_ERROR CalibrationEraseAllElements(void)
{
	lu_uint32_t calId;
	SB_ERROR retValue = SB_ERROR_INITIALIZED;

	if (calDataParam.calDataStartPtr != NULL)
	{
		/* To fill calibration table with zeros */
		memset((void *)calDataParam.calDataStartPtr , 0, calDataParam.calDataMaxSize);

		/* To keep start and next address equal to keep calibration table empty */
		calDataParam.calDataNextPtr = calDataParam.calDataStartPtr;
		calDataParam.calDataSize    = 0;

		/* Initialise with -1, because the calibration ID starts from 0 */
		calDataParam.lastCalId  = CAL_ID_NONE;

		/* Erase calibration table */
		for (calId = 0; calId < MAX_CAL_ID; calId++)
		{
			boardCalTable[calId].calDataPtr    = 0L;
			boardCalTable[calId].calType       = CAL_TYPE_LAST;
			boardCalTable[calId].calDatalength = 0;
		}

		retValue = SB_ERROR_NONE;
	}

	return retValue;
}

SB_ERROR CalibrationConvertValue(lu_uint8_t calId, lu_int32_t inValue, lu_int32_t *outValuePtr)
{
	lu_int16_t calDataLength;
	lu_uint8_t tableSize;
	lu_uint8_t *calDataPtr;
	lu_uint8_t calDataType;
	lu_int32_t temperature;
	lu_uint32_t interpolatedValue;
	lu_int16_t compensationFactor;
	SB_ERROR retError;

	retError = SB_ERROR_NONE;

	if (calDataParam.calDataStartPtr != NULL)
	{
		if (calId < MAX_CAL_ID)
		{
			calDataPtr    = boardCalTable[calId].calDataPtr;
			calDataType   = boardCalTable[calId].calType;
			calDataLength = boardCalTable[calId].calDatalength;

			if (calDataPtr != NULL)
			{
				/* To check the calibration type and choose appropriate algorithm  */
				switch(calDataType)
				{
				 case CAL_TYPE_1DU16S16:
					 tableSize = (calDataLength / sizeof(LnInterpTable1DU16S16Str));

					 *outValuePtr = lnInterp1DU16S16((LnInterpTable1DU16S16Str *)calDataPtr,
													 tableSize,
													 inValue);
					 break;

				 case CAL_TYPE_1DU16U16:
					 tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));

					 *outValuePtr = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)calDataPtr,
													 tableSize,
													 inValue);
					 break;

				 case CAL_TYPE_1DU16U16_DSM_E_CT_COMP:
					 tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));

					 *outValuePtr = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)calDataPtr,
					  								 tableSize,
					 								 inValue);

					 BoardCalibrationGetTemperature(&temperature);

					 calDataPtr    = boardCalTable[calId + 1].calDataPtr;
					 calDataType   = boardCalTable[calId + 1].calType;
					 calDataLength = boardCalTable[calId + 1].calDatalength;

					 tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));

					 compensationFactor = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)calDataPtr,
					 					  			 	   tableSize,
					 					  			       (lu_uint16_t)*outValuePtr);

					 *outValuePtr = (lu_int32_t)CTTempCompensationBySlopeTable( (lu_uint64_t)*outValuePtr,
							 	 	 	 	 	 	 	 	 	 	 	 	 	temperature,
							 	 	 	 	 	 	 	 	 	 	 	 	 	compensationFactor
					 							   	   	   	   	  	  	  	  );

					 break;

				 case CAL_TYPE_1DU16U16_CT_TEMP_COMP:
					 tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));

					 interpolatedValue = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)calDataPtr,
							 	 	 	 	 	 	      tableSize,
							 	 					      inValue);

					 BoardCalibrationGetTemperature(&temperature);
					 *outValuePtr = CTLinearTempCompensation(interpolatedValue, temperature);

					 break;

				  case CAL_TYPE_1DU16U16_CT_TEMP_COMP_SUM:
					 tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));

					 interpolatedValue = lnInterp1DU16U16((LnInterpTable1DU16U16Str *)calDataPtr,
														  tableSize,
														  inValue);

					 BoardCalibrationGetTemperature(&temperature);

					 *outValuePtr = CTTempCompensationSum(interpolatedValue, temperature);
					 *outValuePtr = interpolatedValue;

					 break;

				 /* If there no calibration type present, set the response to false */
				  case CAL_TYPE_ADE78XX_REG:
					  /* This type is used to get calDataPtr of ADE78xx Reg */
				  default:
					 retError = SB_ERROR_PARAM;
					 break;
				}
			}
		}
		else
		{
			retError = SB_ERROR_PARAM;
		}
	}
	else
	{
		retError = SB_ERROR_INITIALIZED;
	}

	return retError;
}

SB_ERROR CalibrationGetCalElement(lu_uint8_t calId, lu_uint8_t calType, CalElementStr *calElement)
{
	SB_ERROR retError = SB_ERROR_PARAM;

	if(calType == boardCalTable[calId].calType)
	{
		*calElement = boardCalTable[calId];
		retError = SB_ERROR_NONE;
	}

	return retError;
}

SB_ERROR CalibrationAddElement(lu_uint8_t    calID,
							   lu_uint8_t    calType,
							   lu_uint16_t   calDatalength,
							   lu_uint8_t    *calDataPtr
	                          )
{
	SB_ERROR retValue = SB_ERROR_NONE;
	CalDataHeaderStr  *calElementPtr;
	lu_uint16_t       totalCalSize;

	/* To check memory for calibration initialised or not */
	if (calDataParam.calDataStartPtr != NULL)
	{
		/* Checking the enough space is available to add this calibration element */
		totalCalSize  = calDatalength;
		totalCalSize += sizeof(CalDataHeaderStr);

		if ((calDataParam.calDataMaxSize - calDataParam.calDataSize) >= totalCalSize)
		{
			if(calDatalength > 0)
			{
				/* Checking for calibration ID is in the sequential order */
				if(calID == calDataParam.lastCalId + 1 )
				{
					/* Copying new element header to memory location and incrementing by number bytes written*/
					calElementPtr = (CalDataHeaderStr *)calDataParam.calDataNextPtr;

					calElementPtr->calId  = calID;
					calElementPtr->type   = calType;
					calElementPtr->length = calDatalength;

					calDataParam.calDataNextPtr += sizeof(CalDataHeaderStr);

					/* Copying new element's data in to memory location */
					memcpy(calDataParam.calDataNextPtr, (void *)calDataPtr, calDatalength);

					/* Updating the board specific calibration table after adding new element*/
					boardCalTable[calID].calType       = calType;
					boardCalTable[calID].calDatalength = calDatalength;
					boardCalTable[calID].calDataPtr    = calDataParam.calDataNextPtr;

					/* Updating the calibration data parameters after adding new element */
					calDataParam.lastCalId       = calID;
					calDataParam.calDataSize    += (sizeof(CalDataHeaderStr) + calDatalength);
					calDataParam.calDataNextPtr += calDatalength;
				}
				else
				{
					retValue = SB_ERROR_CAL_SEQUENCE;
				}
			}
			else
			{
				retValue = SB_ERROR_CAL_LENGTH;
			}
		}
		else
		{
			retValue = SB_ERROR_CAL_EXCEED;
		}
	}
	else
	{
		retValue = SB_ERROR_PARAM;
	}
	return retValue;
}

SB_ERROR CalibrationInterChangeColumns(lu_uint8_t calID,
									   lu_uint32_t minInput,
									   lu_uint32_t maxInput,
									   lu_uint32_t startOutput,
									   lu_uint32_t endOutput
									  )
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint8_t calElementIndex;
	lu_uint8_t calTableSize;
	lu_uint16_t calValueTemp;
	LnInterpTable1DU16U16Str *calData1DU16U16Ptr;
	lu_bool_t rangeOK = LU_TRUE;
	lu_bool_t ascOrderOK = LU_TRUE;

	if (calID < MAX_CAL_ID)
	{
		if(calID == boardCalTable[calID].calID)
		{
			retError= CalibrationGetCalTableSize(boardCalTable[calID].calType,
												 boardCalTable[calID].calDatalength,
												 &calTableSize
												);
			if(retError == SB_ERROR_NONE)
			{
				switch(boardCalTable[calID].calType)
				{
					case CAL_TYPE_1DU16U16:
						calData1DU16U16Ptr = (LnInterpTable1DU16U16Str*)boardCalTable[calID].calDataPtr;
						break;
					default:
						break;
				}

				if(calData1DU16U16Ptr != NULL)
				{
					for(calElementIndex = 0; calElementIndex < calTableSize; calElementIndex++)
					{
						if(calData1DU16U16Ptr[calElementIndex].interpolateToX >= minInput    &&
						   calData1DU16U16Ptr[calElementIndex].interpolateToX <= maxInput    &&
						   calData1DU16U16Ptr[calElementIndex].distributionX  >= startOutput &&
						   calData1DU16U16Ptr[calElementIndex].distributionX  <= endOutput   &&
						   rangeOK == LU_TRUE
						  )
						{
							rangeOK = LU_TRUE;
						}
						else
						{
							rangeOK = LU_FALSE;
							break;
						}
					}

					if(rangeOK == LU_TRUE)
					{
						for(calElementIndex = 0; calElementIndex < (calTableSize -1); calElementIndex++)
						{
							if((calData1DU16U16Ptr[calElementIndex].interpolateToX <= calData1DU16U16Ptr[calElementIndex + 1].interpolateToX) &&
							   ascOrderOK == LU_TRUE
							  )
							{
								ascOrderOK = LU_TRUE;
							}
							else
							{
								ascOrderOK = LU_FALSE;
								break;
							}
						}
					}

					if(rangeOK    == LU_TRUE &&
					   ascOrderOK == LU_TRUE
					  )
					{
						for(calElementIndex = 0; calElementIndex < calTableSize; calElementIndex++)
						{
							calValueTemp                                       = calData1DU16U16Ptr[calElementIndex].distributionX;
							calData1DU16U16Ptr[calElementIndex].distributionX  = calData1DU16U16Ptr[calElementIndex].interpolateToX;
							calData1DU16U16Ptr[calElementIndex].interpolateToX = calValueTemp;
						}
					}
					else
					{
						retError = SB_ERROR_CAL_EXCEED;
					}
				}
			}
		}
	}

	return retError;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

SB_ERROR CalibrationGetCalTableSize(lu_uint8_t  calType,
								    lu_uint16_t calDataLength,
								    lu_uint8_t  *tableSize
								   )
{
	SB_ERROR retError = SB_ERROR_NONE;

	switch(calType)
	{
	 case CAL_TYPE_1DU16S16:
		 *tableSize = (calDataLength / sizeof(LnInterpTable1DU16S16Str));
		 break;

	 case CAL_TYPE_1DU16U16:
		 *tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));
		 break;

	 case CAL_TYPE_1DU16U16_DSM_E_CT_COMP:
		 *tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));
		 break;

	 case CAL_TYPE_1DU16U16_CT_TEMP_COMP:
		 *tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));
		 break;

	  case CAL_TYPE_1DU16U16_CT_TEMP_COMP_SUM:
		 *tableSize = (calDataLength / sizeof(LnInterpTable1DU16U16Str));
		 break;

	 /* If there no calibration type present, set the response to false */
	  case CAL_TYPE_ADE78XX_REG:
		  /* This type is used to get calDataPtr of ADE78xx Reg */
	  default:
		 retError = SB_ERROR_PARAM;
		 break;
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief To Read Clibration Element
 *
 *   Reading of a stored calibration element from slave board.
 *
 *   \param calIDPtr Pointer to the calibration ID
 *
 *   \return Error code
 *
 ******************************************************************************
 */
SB_ERROR CalibrationReadElement(CalTstCGetDataStr *calIDPtr)
{
	SB_ERROR retError = SB_ERROR_NONE;
	lu_uint8_t idCount = 0;
	CalDataHeaderStr *calHeaderPtr;
	lu_uint8_t *calDataPtr;
	lu_uint16_t calElementLength = 0;

	/* Assigning the location of calibration elements to the header and data pointers */
	calHeaderPtr = (CalDataHeaderStr *)calDataParam.calDataStartPtr;
	calDataPtr   = (lu_uint8_t *)calDataParam.calDataStartPtr;

	/* Searching for calibration ID */
	while(idCount <= calIDPtr->calID)
	{
		if(calHeaderPtr->calId == calIDPtr->calID)
		{
			/* Assigning length of the element */
			calElementLength  = sizeof(CalDataHeaderStr) + calHeaderPtr->length;
			break;
		}

		calDataPtr       += sizeof(CalDataHeaderStr) + calHeaderPtr->length;
		calHeaderPtr      = (CalDataHeaderStr *)calDataPtr;
		idCount++;
	}

	retError = CANCSendMCM( MODULE_MSG_TYPE_CALTST,
							MODULE_MSG_ID_CALTST_READ_ELEMENT_R,
							calElementLength,
							(lu_uint8_t*)calDataPtr
						  );
	return retError;
}

/*!
 ******************************************************************************
 *   \brief Getting the address of particular calibration value by using it's ID
 *
 *   Detailed description
 *
 *   \param calId  - calibration ID
 *   \param calElementPtr - Pointer to a location where calibration data is located
 *
 *   \return Pointer to a calibration data
 *
 ******************************************************************************
 */
SB_ERROR CalDataCheck(lu_int8_t calID, CalDataHeaderStr *calElementPtr)
{
	lu_uint16_t totalCalSize;
	SB_ERROR retError;

	retError = SB_ERROR_CAL_NODATA;

	/* Checking for given calibration ID's presence in the memory */
	if (calID == calElementPtr->calId)
	{
		if (calElementPtr->length > 0)
		{
			totalCalSize  = calElementPtr->length;
			totalCalSize += sizeof(CalDataHeaderStr);

			/* Check calibration data is within memory bounds */
			if ((calDataParam.calDataMaxSize - calDataParam.calDataSize) >= totalCalSize)
			{
				retError = SB_ERROR_NONE;
			}
		}
	}

	return retError;
}

void CalTestConvertValue(CalTstConvValueStr *CalTstConvValuePtr)
{
	SB_ERROR retError;
	CalTstConvValueRspStr CalTstConvValueRsp;

	CalTstConvValueRsp.calValue = 0;
	CalTstConvValueRsp.status   = LU_FALSE;

	retError = CalibrationConvertValue(CalTstConvValuePtr->calID,
			                          CalTstConvValuePtr->inputValue,
			                          &CalTstConvValueRsp.calValue);

	if (retError == SB_ERROR_NONE)
	{
		CalTstConvValueRsp.status = LU_TRUE;
	}

	retError   = CANCSendMCM(MODULE_MSG_TYPE_CALTST,
					 		 MODULE_MSG_ID_CALTST_CONVERT_VALUE_R,
							 sizeof(CalTstConvValueRspStr),
							 (lu_uint8_t*)&CalTstConvValueRsp
						    );
}

void CalTestAddElement(lu_uint16_t msgLen, CalDataHeaderStr *calDataPtr)
{
	lu_uint8_t   *calElementDataPtr;
	CalTstAddElementRspStr CalTstAddElementRsp;
	SB_ERROR     retError;

	calElementDataPtr 		= (lu_uint8_t *)calDataPtr;
	calElementDataPtr      += sizeof(CalDataHeaderStr);

	CalTstAddElementRsp.calID  = calDataPtr->calId;
	CalTstAddElementRsp.status = LU_FALSE;

	if (msgLen == (MODULE_MESSAGE_SIZE(CalDataHeaderStr) + calDataPtr->length))
	{
		retError = CalibrationAddElement(calDataPtr->calId,
				                         calDataPtr->type,
				                         calDataPtr->length,
				                         calElementDataPtr);

		if (retError == SB_ERROR_NONE)
		{
			CalTstAddElementRsp.status = LU_TRUE;
		}

		retError = CANCSendMCM(MODULE_MSG_TYPE_CALTST,
					           MODULE_MSG_ID_CALTST_ADD_ELEMENT_R,
							   sizeof(CalTstAddElementRspStr),
							   (lu_uint8_t*)&CalTstAddElementRsp
							  );
	}
}

void CalTestEraseElements(void)
{
	SB_ERROR retVal;
	CalTstEraseRspStr CalTstEraseRsp;

	CalTstEraseRsp.status = LU_FALSE;

	retVal   = CalibrationEraseAllElements();
	if (retVal == SB_ERROR_NONE)
	{
		CalTstEraseRsp.status = LU_TRUE;
	}

	retVal   = CANCSendMCM( MODULE_MSG_TYPE_CALTST,
							MODULE_MSG_ID_CALTST_ERASE_ELEMENTS_R,
							sizeof(CalTstEraseRspStr),
							(lu_uint8_t*)&CalTstEraseRsp
						  );
}

/*
 *********************** End of file ******************************************
 */
