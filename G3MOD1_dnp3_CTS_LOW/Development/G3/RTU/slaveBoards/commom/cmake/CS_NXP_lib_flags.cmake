# Common C FLAGS
SET (CMAKE_C_COMMON_FLAGS "-Dsourcerygxx -fmessage-length=0 -fdiagnostics-show-option -mthumb -mcpu=cortex-m3 -ffunction-sections -fdata-sections -fno-common -g3 -Wall -mlittle-endian")

# Common Linker FLAGS
SET (CMAKE_COMMON_EXE_LINKER_FLAGS "-Wl,--gc-sections -Xlinker -Map=${PROJECT_NAME}.map")

# Debug FLAGS
SET(CMAKE_C_FLAGS_DEBUG "-O0 -DBUILD=Debug ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")

# Release FLAGS
SET(CMAKE_C_FLAGS_RELEASE "-O2 -DBUILD=Release ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")



