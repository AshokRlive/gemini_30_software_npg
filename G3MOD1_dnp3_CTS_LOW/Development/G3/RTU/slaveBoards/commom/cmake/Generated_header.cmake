# Set the path of the header files generated from XMLs, svn ,etc
set(CMAKE_G3_GENERATED_HEADER_BUILD_PATH "${CMAKE_G3_PATH_ROOT}/RTU/slaveBoards/commom/includeGenerated/")
set(CMAKE_G3_GENERATED_HEADER_INCLUDE 
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/SysAlarm/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/ModuleProtocol/"
    "${CMAKE_G3_GENERATED_HEADER_BUILD_PATH}/NVRAMDef/"
    )
add_subdirectory(${CMAKE_G3_GENERATED_HEADER_BUILD_PATH} commom/includeGenerated/)
# Generated header 
include_directories(${CMAKE_G3_GENERATED_HEADER_INCLUDE}/)