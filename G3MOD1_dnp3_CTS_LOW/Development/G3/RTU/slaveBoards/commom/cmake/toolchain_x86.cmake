# this one is important
SET(CMAKE_SYSTEM_NAME Generic)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)


# specify the cross compiler
# Get host name
SITE_NAME(HOST_NAME)
# Host is jparkerlinux or autolinux
SET(CMAKE_C_X86_COMPILER   /usr/bin/gcc)
SET(CMAKE_CXX_X86_COMPILER /usr/bin/g++)
SET(CMAKE_SIZE /usr/bin/size)


# where is the target environment 
# SET(CMAKE_FIND_ROOT_PATH  /opt/eldk-2007-01-19/ppc_74xx /home/alex/eldk-ppc74xx-inst)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

