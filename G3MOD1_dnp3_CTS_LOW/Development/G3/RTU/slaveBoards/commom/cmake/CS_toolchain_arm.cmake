# this one is important
SET(CMAKE_SYSTEM_NAME Generic)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 1)


# specify the cross compiler
if("$ENV{G3_SLAVE_CROSS_COMPILER}" STREQUAL "")
# Get host name
SITE_NAME(HOST_NAME)
# Host is jparkerlinux
if(${HOST_NAME} STREQUAL "jparker") 
SET(CMAKE_C_COMPILER   /usr/local/toolchains/sourcery/sourceryg++-2013.05-53-arm-none-eabi/bin//arm-none-eabi-gcc)
SET(CMAKE_CXX_COMPILER /usr/local/toolchains/sourcery/sourceryg++-2013.05-53-arm-none-eabi/bin/arm-none-eabi-g++)
SET(CMAKE_SIZE /usr/local/toolchains/sourcery/sourceryg++-2013.05-53-arm-none-eabi/bin/arm-none-eabi-size)

# Host is autolinux
else()
SET(CMAKE_C_COMPILER   /usr/local/Sourcery_G++-2011.02-02-arm-none-eabi/bin/arm-none-eabi-gcc)
SET(CMAKE_CXX_COMPILER /usr/local/Sourcery_G++-2011.02-02-arm-none-eabi/bin/arm-none-eabi-g++)
SET(CMAKE_SIZE /usr/local/Sourcery_G++-2011.02-02-arm-none-eabi/bin/arm-none-eabi-size)
endif()

else()
message(STATUS  "G3_SLAVE_CROSS_COMPILER: $ENV{G3_SLAVE_CROSS_COMPILER}")
SET(CMAKE_C_COMPILER   $ENV{G3_SLAVE_CROSS_COMPILER}/bin/arm-none-eabi-gcc)
SET(CMAKE_CXX_COMPILER $ENV{G3_SLAVE_CROSS_COMPILER}/bin/arm-none-eabi-g++)
SET(CMAKE_SIZE         $ENV{G3_SLAVE_CROSS_COMPILER}/bin/arm-none-eabi-size)
SET(CMAKE_INCLUDE_PATH $ENV{G3_SLAVE_CROSS_COMPILER}/../include/smc/C)
endif()



# where is the target environment 
# SET(CMAKE_FIND_ROOT_PATH  /opt/eldk-2007-01-19/ppc_74xx /home/alex/eldk-ppc74xx-inst)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

