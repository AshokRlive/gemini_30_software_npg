#
# Find the smc State Machine Compiler includes and smc.jar
#
# This module defines
# SMC_C_INCLUDE_DIR, where to find statemap.h.
# SMC_JAR, where to find the smc.jar file.
# SMC_FOUND, If false, do not try to use SMC.

#  also defined, but not for general use are
#

SET(SMC_FOUND "NO")

FIND_PATH(SMC_C_INCLUDE_DIR statemap.h /usr/local/include/smc/C /usr/include/smc/C)

FIND_PATH(SMC_JAR Smc.jar /usr/local/bin)

Message ("msg: ${SMC_C_INCLUDE_DIR} SMC_JAR:${SMC_JAR}")

IF(SMC_C_INCLUDE_DIR)
  IF(SMC_JAR)
    SET(SMC_FOUND "YES")
  ELSE (SMC_JAR)
    MESSAGE(SEND_ERROR "Could not find Smc.jar compiler ?")
  ENDIF(SMC_JAR)
ELSE(SMC_C_INCLUDE_DIR)
  MESSAGE(SEND_ERROR "Could not find C/statemap.h ?")
ENDIF(SMC_C_INCLUDE_DIR)

macro(testerMacro)
	MESSAGE("Test ????????")
endmacro(testerMacro)

# runSMC Statemachine compiler
macro (runSMC smPath smFile cFile hFile)
	# set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/")
	#FIND_PACKAGE(SMC REQUIRED)

	MESSAGE("SMC cmd:  ${SMC_JAR} ${smPath}/${smFile} ${smPath}/${cFile} ${smPath}/${hFile}"  )
	
	ADD_CUSTOM_COMMAND( TARGET ${EXE_NAME} POST_BUILD
	    #OUTPUT "${smPath}/${cFile}" "${smPath}/${hFile}" 
	    COMMAND cd ${smPath} && java -jar ${SMC_JAR}/Smc.jar -g -c ${smPath}/${smFile} 
	    DEPENDS "${smPath}/${smFile}"
	    )
	
	clean_file("${smPath}/${cFile}") 
	clean_file("${smPath}/${cFile}") 
	
	
endmacro (runSMC)