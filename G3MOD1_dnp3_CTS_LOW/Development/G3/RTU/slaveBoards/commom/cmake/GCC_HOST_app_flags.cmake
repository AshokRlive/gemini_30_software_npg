# Common C FLAGS
SET (CMAKE_C_COMMON_FLAGS "-fmessage-length=0 -fdiagnostics-show-option -g3 -std=c99 -pedantic -Wall -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wno-long-long")

# Common Linker FLAGS
SET (CMAKE_COMMON_EXE_LINKER_FLAGS "-Map=${PROJECT_NAME}.map")

# Debug FLAGS
SET(CMAKE_C_FLAGS_DEBUG "-O0 ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_DEBUG  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")

# Release FLAGS
SET(CMAKE_C_FLAGS_RELEASE "-O2 ${CMAKE_C_COMMON_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE  "${CMAKE_COMMON_EXE_LINKER_FLAGS}")



