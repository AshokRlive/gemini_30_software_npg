# Set the name for the executable
SET (EXE_NAME ${PROJECT_NAME}.axf)

# Set the name for the firmware binary file
SET (BIN_NAME ${PROJECT_NAME}.bin)

# Set the name for the firmware hex file
SET (HEX_NAME ${PROJECT_NAME}.hex)

# Set the name of the MAP file
SET (MAP_NAME ${PROJECT_NAME}.map)

# Add a file to the list of file removed in the clean command.
macro (clean_file file)
    SET (CLEAN_FILE_LIST "${CLEAN_FILE_LIST};${file}")
    set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES "${CLEAN_FILE_LIST}")
endmacro (clean_file)

# Clean map file
clean_file(${MAP_NAME})

# Create binary file from executable file. Also print size of the executable
macro (generate_bin)
    add_custom_command( TARGET ${EXE_NAME} POST_BUILD
                        COMMAND ${CMAKE_OBJCOPY} -O binary ${EXE_NAME} ${BIN_NAME} &&
                                ${CMAKE_SIZE} ${EXE_NAME} &&
                                G3SlaveBinCRC -FP=${BIN_NAME} &&
                                ${CMAKE_OBJCOPY} -I binary -O ihex ${BIN_NAME} ${HEX_NAME} 
                      )
    clean_file(${BIN_NAME} ${HEX_NAME})
endmacro (generate_bin)

macro (set_cpu_type cpu)
    SET (CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DPART_${cpu}")
    SET (CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -DPART_${cpu}")
endmacro (set_cpu_type)

macro (set_dbg_serial_port port)
    SET (CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DUSED_UART_DEBUG_PORT=${port}")
    SET (CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -DUSED_UART_DEBUG_PORT=${port}")
endmacro (set_dbg_serial_port)

# Add a linker script to the linker. Use only one file at a time. Call the macro multiple times to add multple linker scripts
macro (add_linker_script script)
    SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -T ${script}")
    SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -T ${script}")
endmacro (add_linker_script)

