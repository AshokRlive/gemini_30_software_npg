/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_0,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_1,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_CAN2_RX,               PINSEL_PORT_0,  PINSEL_PIN_4,  \
			FUNC_CAN_2_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN2_TX,               PINSEL_PORT_0,  PINSEL_PIN_5,  \
			FUNC_CAN_2_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_0,  PINSEL_PIN_2,  \
			FUNC_UART_0_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_0,  PINSEL_PIN_3,  \
			FUNC_UART_0_TX, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/* (Some Virt for NXP dev board) */
	/* (But will be physical on PSM) */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_LOCAL_LINE,               IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   GPIO_PM_PULL_UP)
    IOM_GPIO_INPUT( IO_ID_REMOTE_LINE,               IO_CH_NA, \
            PINSEL_PORT_1,  PINSEL_PIN_29,   GPIO_PM_PULL_UP)

	IOM_VIRT_DI( IO_ID_AUTO_CONFIG_IN, 			 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_AUTO_CONFIG_OUT,			 	IO_CH_NA)

	IOM_VIRT_DI( IO_ID_FACTORY, 			     	IO_CH_NA)

	IOM_GPIO_INPUT( IO_ID_AC_SUPPLY_ON,             PSM_CH_DINPUT_AC_OFF, \
			PINSEL_PORT_1,  PINSEL_PIN_23,   GPIO_PM_PULL_UP)

	IOM_VIRT_DO( IO_ID_FAN_ON, 		        	 	IO_CH_NA)
	IOM_VIRT_DI( IO_ID_FAN_PGOOD, 	     		 	IO_CH_NA)
    IOM_GPIO_INPUT( IO_ID_FAN_FAULT,             PSM_CH_DINPUT_FAN_FAULT, \
            PINSEL_PORT_1,  PINSEL_PIN_24,   GPIO_PM_PULL_UP)
	IOM_VIRT_DI( IO_ID_FAN_SPEED_S, 			 	IO_CH_NA)

	IOM_VIRT_DO( IO_ID_MOT_PS_ON, 		       	 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_MOT_PS_OFF, 		       	 	IO_CH_NA)

	IOM_GPIO_OUTPUT( IO_ID_BAT_CONNECT,             IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_5,   GPIO_PM_OUTPUT_HIGH)
	IOM_GPIO_OUTPUT( IO_ID_BAT_DISCONNECT,          IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_6,   GPIO_PM_OUTPUT_HIGH)
	IOM_GPIO_OUTPUT(IO_ID_BAT_CHARGER_EN,           IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_3,   GPIO_PM_OUTPUT_HIGH)

	IOM_VIRT_DO( IO_ID_PS_LATCH_EN, 	       	 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_24V_PS_ON, 		       	 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_24V_PS_OFF, 		       	 	IO_CH_NA)

	IOM_VIRT_DO( IO_ID_LOGIC_PS_ON, 	       	 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LOGIC_PS_OFF, 		   	 	IO_CH_NA)

	IOM_VIRT_DO( IO_ID_HWDOG_1HZ_KICK, 		   	 	IO_CH_NA)

	IOM_VIRT_DO( IO_ID_LED_CTRL_OK_GREEN, 			IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_OK_RED, 		   	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_CAN_GREEN, 		  	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_CAN_RED, 		   	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_EXTINGUISH, 		IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_SUPPLY_OK_GREEN,	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_SUPPLY_OK_RED, 		IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_LOGIC_GREEN, 		IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_LOGIC_RED, 		   	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_MOTOR_GREEN, 		IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_MOTOR_RED, 		 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_BAT_CHRG_GREEN, 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_BAT_CHRG_RED, 	    IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_BAT_HEALTH_GREEN, 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_BAT_HEALTH_RED, 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_FAN_GREEN,    	 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_LED_CTRL_FAN_RED, 	  	 	IO_CH_NA)

	IOM_PERIPH_AIN( IO_ID_MOT_I_SENSE, 				PSM_CH_AINPUT_MS_CURRENT, \
	            PINSEL_PORT_0,  PINSEL_PIN_25,  FUNC_AD0_CH_2)

	IOM_PERIPH_AIN( IO_ID_MOT_V_SENSE, 				PSM_CH_AINPUT_MS_VOLTAGE, \
				PINSEL_PORT_0,  PINSEL_PIN_23,  FUNC_AD0_CH_0)
	IOM_PERIPH_AIN( IO_ID_BAT_V_SENSE,    			PSM_CH_AINPUT_BT_VOLTAGE, \
			PINSEL_PORT_0,  PINSEL_PIN_24,  FUNC_AD0_CH_1)

	IOM_VIRT_AI( IO_ID_BAT_CHARGER_I_SENSE, 	    PSM_CH_AINPUT_CH_CHARGING_CURRENT)
	IOM_VIRT_AI( IO_ID_MAIN_DC_I_SENSE, 			IO_CH_NA)
	IOM_VIRT_AI( IO_ID_36V_RAW_DC_V_SENSE, 			IO_CH_NA)
	IOM_VIRT_AI( IO_ID_COMMS_1_V_SENSE, 				PSM_CH_AINPUT_AUX_VOLTAGE)
	IOM_VIRT_AI( IO_ID_LOGIC_V_SENSE, 				PSM_CH_AINPUT_LS_VOLTAGE)
	IOM_VIRT_AI( IO_ID_LOGIC_I_SENSE, 				IO_CH_NA)

	IOM_VIRT_DO( IO_ID_DIGITAL_POT_CS0, 	  	 	IO_CH_NA)
	IOM_VIRT_DO( IO_ID_DIGITAL_POT_CS1, 	  	 	IO_CH_NA)

	IOM_VIRT_DO( IO_ID_APPRAM_WEN, 	  	          	IO_CH_NA)

	/* I2C temp sensor */
	IOM_VIRT_AI( IO_ID_BAT_TEMP, 			 	    PSM_CH_AINPUT_TS_BATTERY_TEMPERATURE)
	IOM_VIRT_AI( IO_ID_PSU_TEMP, 					PSM_CH_AINPUT_TS_INTERNAL_TEMPERATURE)

	IOM_VIRT_AI(IO_ID_VIRT_CH_VOLTAGE, IO_CH_NA)

	IOM_VIRT_DO(IO_ID_MOT_PS_EN, 	  	          	IO_CH_NA)
	IOM_VIRT_DO(IO_ID_COMMS_1_PRESENT, 	  	        IO_CH_NA)
	IOM_VIRT_DO(IO_ID_COMMS_2_PRESENT, 	  	        IO_CH_NA)
	IOM_VIRT_DO(IO_ID_COMMS_1_EN, 	  	            IO_CH_NA)


	/* I2C IO Expanders */

	/* SPI Digi pot devices */
	IOM_VIRT_DO(IO_ID_BAT_CHARGER_CUR_LIM_SET,      IO_CH_NA)
	IOM_VIRT_DO(IO_ID_BAT_CHARGER_VOLT_SET,       	IO_CH_NA)
	IOM_VIRT_DO(IO_ID_MOT_CUR_LIM,       			IO_CH_NA)

	/* SPI attached ADC */

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IOM_VIRT_DI( IO_ID_VIRT_LS_OVERVOLTAGE,             PSM_CH_DINPUT_LS_OVERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_LS_UNDERVOLTAGE,            PSM_CH_DINPUT_LS_UNDERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_LS_SELECT_OPERATE_TIMEOUT,  PSM_CH_DINPUT_LS_SELECT_OPERATE_TIMEOUT)
	IOM_VIRT_DI( IO_ID_VIRT_MS_SELECT_OPERATE_TIMEOUT,  PSM_CH_DINPUT_MS_SELECT_OPERATE_TIMEOUT)
	IOM_VIRT_DI( IO_ID_VIRT_MS_MOTOR_ON_TIMEOUT,        PSM_CH_DINPUT_MS_MOTOR_ON_TIMEOUT)
	IOM_VIRT_DI( IO_ID_VIRT_MS_RELAY_ON,                PSM_CH_DINPUT_MS_RELAY_ON)
	IOM_VIRT_DI( IO_ID_VIRT_MS_OVERVOLTAGE,             PSM_CH_DINPUT_MS_OVERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_MS_UNDERVOLTAGE,            PSM_CH_DINPUT_MS_UNDERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_MS_OVERCURRENT,             PSM_CH_DINPUT_MS_OVERCURRENT)
	IOM_VIRT_DI( IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT,      PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT)
	IOM_VIRT_DI( IO_ID_VIRT_AUX_OVERVOLTAGE,            PSM_CH_DINPUT_AUX_OVERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_AUX_UNDERVOLTAGE,           PSM_CH_DINPUT_AUX_UNDERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_AC_SELECT_OPERATE_TIMEOUT,  PSM_CH_DINPUT_AC_SELECT_OPERATE_TIMEOUT)
	IOM_VIRT_DI( IO_ID_VIRT_AC_OFF,                     IO_CH_NA)
	IOM_VIRT_DI( IO_ID_VIRT_AC_OVERVOLTAGE,             PSM_CH_DINPUT_AC_OVERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_AC_UNDERVOLTAGE,            PSM_CH_DINPUT_AC_UNDERVOLTAGE)
	IOM_VIRT_DI( IO_ID_VIRT_CH_FAULT,                   PSM_CH_DINPUT_CH_FAULT)
	IOM_VIRT_DI( IO_ID_VIRT_CH_ON,                      PSM_CH_DINPUT_CH_ON)
	IOM_VIRT_DI( IO_ID_VIRT_BT_COMMS_FAIL,              PSM_CH_DINPUT_BT_COMMS_FAIL)
	IOM_VIRT_DI( IO_ID_VIRT_BT_DISCONNECTED,            PSM_CH_DINPUT_BT_DISCONNECTED)
	IOM_VIRT_DI( IO_ID_VIRT_BT_OVER_TEMPERATURE,        PSM_CH_DINPUT_BT_OVER_TEMPERATURE)
	IOM_VIRT_DI( IO_ID_VIRT_CH_SYSTEM_RUNNING,       PSM_CH_DINPUT_BT_CHARGING_OVERRIDE)
	IOM_VIRT_DI( IO_ID_VIRT_BT_TEST_FAIL,               PSM_CH_DINPUT_BT_TEST_FAIL)
	IOM_VIRT_DI( IO_ID_VIRT_BT_LOW,                     PSM_CH_DINPUT_BT_LOW)
	IOM_VIRT_DI( IO_ID_VIRT_BT_FAULT,                   PSM_CH_DINPUT_BT_FAULT)

	IOM_VIRT_DI( IO_ID_VIRT_FAN_ON, 	     		 	PSM_CH_DINPUT_FAN_ON)
	IOM_VIRT_DI( IO_ID_VIRT_FAN_FAULT, 	     		 	IO_CH_NA)

	IOM_VIRT_AI( IO_ID_VIRT_AC_VOLTAGE, 	     	 	PSM_CH_AINPUT_AC_VOLTAGE)
	IOM_VIRT_AI( IO_ID_VIRT_AC_LOWER_TRIP_LEVEL,   	 	PSM_CH_AINPUT_AC_LOWER_TRIP_LEVEL)
	IOM_VIRT_AI( IO_ID_VIRT_AC_UPPER_TRIP_LEVEL,   	 	PSM_CH_AINPUT_AC_UPPER_TRIP_LEVEL)

	IOM_VIRT_AI( IO_ID_VIRT_MS_PEAK_HOLD_CURRENT,  	 	PSM_CH_AINPUT_MS_PEAK_HOLD_CURRENT)
	IOM_VIRT_AI( IO_ID_VIRT_DRAIN_CURRENT,   	    	PSM_CH_AINPUT_DRAIN_CURRENT)
	IOM_VIRT_AI( IO_ID_VIRT_ON_ELAPSED_TIME,     	 	PSM_CH_AINPUT_ON_ELAPSED_TIME)

	/* End of table marker */
	IOM_LAST
};




/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
