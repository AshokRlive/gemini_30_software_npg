/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"


/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(PSM_CH_DINPUT_LS_OVERVOLTAGE, 			IO_ID_VIRT_LS_OVERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_LS_UNDERVOLTAGE, 			IO_ID_VIRT_LS_UNDERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_LS_SELECT_OPERATE_TIMEOUT, 	IO_ID_VIRT_LS_SELECT_OPERATE_TIMEOUT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_SELECT_OPERATE_TIMEOUT, 	IO_ID_VIRT_MS_SELECT_OPERATE_TIMEOUT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_MOTOR_ON_TIMEOUT, 		IO_ID_VIRT_MS_MOTOR_ON_TIMEOUT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_RELAY_ON, 				IO_ID_VIRT_MS_RELAY_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_OVERVOLTAGE, 			IO_ID_VIRT_MS_OVERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_UNDERVOLTAGE,            IO_ID_VIRT_MS_UNDERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_OVERCURRENT, 			IO_ID_VIRT_MS_OVERCURRENT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT, 		IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AUX_OVERVOLTAGE,            IO_ID_VIRT_AUX_OVERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AUX_UNDERVOLTAGE,           IO_ID_VIRT_AUX_UNDERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AC_SELECT_OPERATE_TIMEOUT, 	IO_ID_VIRT_AC_SELECT_OPERATE_TIMEOUT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AC_OFF, 					IO_ID_AC_SUPPLY_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AC_OVERVOLTAGE, 			IO_ID_VIRT_AC_OVERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AC_UNDERVOLTAGE, 			IO_ID_VIRT_AC_UNDERVOLTAGE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_FAULT, 					IO_ID_VIRT_CH_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_ON, 						IO_ID_VIRT_CH_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_COMMS_FAIL, 				IO_ID_VIRT_BT_COMMS_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_DISCONNECTED, 			IO_ID_VIRT_BT_DISCONNECTED),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_OVER_TEMPERATURE, 		IO_ID_VIRT_BT_OVER_TEMPERATURE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_CHARGING_OVERRIDE, 		IO_ID_VIRT_CH_SYSTEM_RUNNING),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_FAIL, 				IO_ID_VIRT_BT_TEST_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_LOW, 					IO_ID_VIRT_BT_LOW),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_FAULT, 					IO_ID_VIRT_BT_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_FAN_ON, 					IO_ID_VIRT_FAN_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_FAN_FAULT, 					IO_ID_FAN_FAULT),

	IOM_IOCHAN_DI(0, 						                IO_ID_NA)
};

IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),

	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};

IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AI(PSM_CH_AINPUT_LS_VOLTAGE, 				IO_ID_LOGIC_V_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_LS_CURRENT, 				IO_ID_NA, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_MS_VOLTAGE, 				IO_ID_MOT_V_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_MS_CURRENT, 				IO_ID_MOT_I_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_MS_PEAK_HOLD_CURRENT, 		IO_ID_VIRT_MS_PEAK_HOLD_CURRENT, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_AC_VOLTAGE, 				IO_ID_VIRT_AC_VOLTAGE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_AC_LOWER_TRIP_LEVEL, 		IO_ID_VIRT_AC_LOWER_TRIP_LEVEL, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_AC_UPPER_TRIP_LEVEL, 		IO_ID_VIRT_AC_UPPER_TRIP_LEVEL, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_AUX_VOLTAGE,                IO_ID_COMMS_1_V_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_CH_VOLTAGE, 				IO_ID_NA, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_CH_CHARGING_CURRENT, 		IO_ID_BAT_CHARGER_I_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_VOLTAGE, 				IO_ID_BAT_V_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_DRAIN_CURRENT, 				IO_ID_VIRT_DRAIN_CURRENT, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_ON_ELAPSED_TIME, 			IO_ID_VIRT_ON_ELAPSED_TIME, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_TS_INTERNAL_TEMPERATURE, 	IO_ID_PSU_TEMP, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_TS_BATTERY_TEMPERATURE, 	IO_ID_BAT_TEMP, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_36V_RAW_DC, 	            IO_ID_36V_RAW_DC_V_SENSE, CAL_ID_NA, 0),
	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, CAL_ID_NA, 0)
};

IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(0, 										IO_ID_NA, CAL_ID_NA),

	IOM_IOCHAN_AO(0, 										IO_ID_NA, CAL_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
