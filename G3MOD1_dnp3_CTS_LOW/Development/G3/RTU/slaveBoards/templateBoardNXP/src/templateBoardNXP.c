/*! \file
 ******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
 *       \brief
*
*       Detailed description of this template module
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/12      saravanan_v    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
 ******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/


/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/
#include <string.h>

#include "system_LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "debug_frmwrk.h"
#include "systemTime.h"
#include "CANProtocol.h"
#include "CANProtocolFraming.h"
#include "CANProtocolCodec.h"

#include "systemStatus.h"

/* Board specific includes */
#include "CANProtocolDecoder.h"
#include "BoardStatusManager.h"

#include "IOManager.h"

#include "BoardCalibration.h"
#include "Calibration.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

#define DEVICE    MODULE_PSM
#define DEVICE_ID MODULE_ID_0

#define SERIAL    0x11111111



/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/



/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

static CANFramingMapStr CANMapping =
{
    GPIO_INVALID_PIN ,/* peripheralCAN     */
    GPIO_INVALID_PIN ,/* peripheralGPIO    */
    PINSEL_PORT_0    ,/* gpioPortBase      */
    PINSEL_PIN_1     ,/* CanTxPin          */
    PINSEL_FUNC_1    ,/* CanTxConfigurePin */
    PINSEL_PIN_0     ,/* CanRxPin          */
    PINSEL_FUNC_1    ,/* CanRxConfigurePin */
    LPC_CAN1_BASE    ,/* CanBase           */
    CAN_IRQn          /* CanInt            */
};

/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/


/*!
*******************************************************************************
* EXPORTED:
*   \name
*******************************************************************************
* @{
*/

int main(void)
{
    SUIRQPriorityStr CANPriority;
    SUIRQPriorityStr TSynchPriority;
    SUIRQPriorityStr timePriority;

    /* System specific initialization (set PLL and CPU speed) */

    SystemInit();

    /* ********************************************************************************
     * Very Very important to identify vector table on user application area
     *
     * */
    //SCB->VTOR = 0x00020000;
    /* ***************************************************************************** */

    /* Enable debug serial port (COM0) */
    debug_frmwrk_init();

    /* Initialize IRQ priorites */
    CANPriority.group = SU_IRQ_GROUP_0;
    CANPriority.group = SU_IRQ_SUB_PRIORITY_0;
    TSynchPriority.group = SU_IRQ_GROUP_7;
    TSynchPriority.group = SU_IRQ_SUB_PRIORITY_3;
    timePriority.group = SU_IRQ_GROUP_7;
    timePriority.group = SU_IRQ_SUB_PRIORITY_0;

    /* Initialize IRQ priority */
    SUInitIRQPriority();

    /* Initialize System Time */
    STInit(timePriority);

    /* Set board serial number */
    SSSetSerial(SERIAL);
    SSSetSupplierId(0);

    /* Initialise system specific IOManager - GPIO,etc IOMap / IO Channel map */
    IOManagerInit();

    /* Initialize CAN Codec */
    CANCodecInit( SystemCoreClock,
                  CAN_BUS_BAUDRATE,
                  DEVICE,
                  DEVICE_ID,
                  &CANMapping,
                  CANPriority,
                  TSynchPriority,
                  BoardCANProtocolDecoder
                );

    /* Initialise CAN filter for IOManager */
    IOManagerInitCANFilter();

    /* Initialise Calibration */
    CalibrationInit();

    /* Call scheduler executive */
    BoardStatusManagerExecutive();

    return 0;
}

/*!
*******************************************************************************
* @}
*/


/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/


/*!
*******************************************************************************
* @}
*/


/*
*********************** End of file *******************************************
*/
