/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/05/12      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARD_CALIBRATION_INCLUDED
#define _BOARD_CALIBRATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Calibration.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_CAL_ID 		    CAL_ID_LAST
#define MAX_CAL_DATA_SIZE   0x200
#define MAX_SENSORS         3

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! Calibration ID */
typedef enum
{
	CAL_ID_I_MEASURE       = 0,
	CAL_ID_TEMP_MEASURE       ,
	CAL_ID_NEW_ELEMENT        ,

	CAL_ID_LAST
} CAL_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */
extern CalElementStr boardCalTable[MAX_CAL_ID + 1];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief To initialise and create calibration table according to the board specifications
 *
 *    Implementation of calibration table and maintains it's parameter such as starting address,
 *    address of next data to be written.
 *
 *   \param void
 *
 *   \return Error code
 *
 ******************************************************************************
 */

SB_ERROR BoardCalibrationInit(void);

/*!
 ******************************************************************************
 *   \brief Writing the calibration element structure to memory (Flash or NV Ram)
 *
 *	 Writing the calibration element structure to memory in the form of
 *	 ID,Type, Length (number of bytes), the actual data need for calibration.
 *	 Note: No address of data is stored.
 *
 *   \param address, calibration elements table, calibration elements size
 *
 *   \return Error code
 *
 ******************************************************************************
 */
//extern SB_ERROR CalibrationAddElement(CalElementStr *calElement);


#endif /* _BOARD_CALIBRATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
