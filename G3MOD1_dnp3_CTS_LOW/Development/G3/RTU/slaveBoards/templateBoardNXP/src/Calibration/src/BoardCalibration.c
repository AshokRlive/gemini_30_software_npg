/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   24/05/12      saravanan_v     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <string.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "LinearInterpolation.h"
#include "Calibration.h"
#include "BoardCalibration.h"

#include "IOManager.h"
#include "BoardIO.h"
#include "BoardIOMap.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_1DU16S16_TEMP   2

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_int8_t nvRamData[MAX_CAL_DATA_SIZE];

static const LnInterpTable1DU16S16Str Temp1dU16S16[MAX_1DU16S16_TEMP] =
{
		/*
		 * Input value ,       Output value
		 */
		{216,                  -40},
		{1496,                 125}
};

// This table is for testing purpose only
static const LnInterpTable1DU16S16Str Current1dU16S16[2] =
{
	    { 1000, 5000 },
		{ 2000, 6000 }
};


CalElementStr boardCalTable[MAX_CAL_ID + 1] =
{
	CALID_ELEMENT(CAL_ID_I_MEASURE)
	CALID_ELEMENT(CAL_ID_TEMP_MEASURE)
	CALID_ELEMENT(CAL_ID_NEW_ELEMENT)

	/* End Table Marker */
	CALID_ELEMENT_LAST()
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardCalibrationInit(void)
{
	SB_ERROR      retError;

	memset(&nvRamData[0], 0 , sizeof(nvRamData));

	/* To check the sequence of calibration ID and boundary */
	retError = CalibrationInitCalDataPtr(&nvRamData[0], sizeof(nvRamData), MAX_CAL_ID);

	if (retError != SB_ERROR_NONE)
	{
		/* To erase all the calibration data*/
		CalibrationEraseAllElements();

		CalibrationAddElement(CAL_ID_I_MEASURE,
							  CAL_TYPE_1DU16S16,
							  sizeof(Temp1dU16S16),
							  (lu_uint8_t *)&Temp1dU16S16[0]
							 );

		CalibrationAddElement(CAL_ID_TEMP_MEASURE,
							  CAL_TYPE_1DU16U16,
							  sizeof(Current1dU16S16),
							  (lu_uint8_t)&Current1dU16S16[0]
							 );
	}

	/* To check the sequence of calibration ID and boundary */
    CalibrationInitCalDataPtr(&nvRamData[0], sizeof(nvRamData), MAX_CAL_ID);

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
 *********************** End of file ******************************************
 */
