/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME: 
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION: 
*		\brief  Gemini v2.5 Over Voltage Protected PSU
*
*       Code for PIC18f1220 that monitores the incomming mains
*	voltage and shuts PSU down if outsie of valid range
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY	 
*
*   Date        EAR/Reason      Name        Details
*   ---------------------------------------------------------------------------
*   20/01/10    V25            mudie_i     Initial version.
*   16/08/10    V25.01         fryers_j	   Change 220V low limit from 199V to 190V
*   28/06/11    V25.02 beta    ousby_c   Change high limit from 255V to 265V & ignore dips of less than 3 seconds
*   20/07/11    V25.03 beta    ousby_c   Reduce start-up time to use ~12 cycles to make decision
*
*******************************************************************************
*/

#ifdef CAK
/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/
#include <stdio.h>
#include <p18f1220.h>


/*
*******************************************************************************
* Configuration Bits
*******************************************************************************
*/
/* Before #define as ON \ OFF is a special keyword */
#pragma config OSC = INTIO2, FSCM = OFF, IESO = OFF    	// CONFIG1H 
#pragma config PWRT = ON, BOR = ON, BORV = 42          	// CONFIG2L
#pragma config WDT = ON, WDTPS = 8                     	// CONFIG2H
#pragma config MCLRE = ON			   	// CONFIG3H
#pragma config STVR = ON, LVP = OFF,  DEBUG = OFF	// CONFIG4L
#pragma config CP0 = OFF, CP1 = OFF			// CONFIG5L
#pragma config CPB = OFF, CPD = OFF                    	// CONFIG5H
#pragma config WRT0 = ON, WRT1 = OFF               	// CONFIG6L
#pragma config WRTB = ON, WRTC = OFF, WRTD = OFF       	// CONFIG6H
#pragma config EBTR0 = OFF, EBTR1 = OFF           	// CONFIG7L
#pragma config EBTRB = OFF                             	// CONFIG7H


/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/
#define PIC18F1220 

#define ON  1
#define OFF 0

#define LED_RED      PORTBbits.RB1
#define LED_GREEN    PORTBbits.RB2
#define LED_YELLOW   LED_RED=LED_GREEN
#define TRIAC_OUTPUT PORTAbits.RA7
#define TRIAC_ENABLE PORTBbits.RB0

#define VOLTAGE_220_HIGH_LIMIT_AD   887  /* ~265V */ 
#define VOLTAGE_220_LOW_LIMIT_AD    633  /* 190V */   
#define VOLTAGE_110_HIGH_LIMIT_AD   412  /* 120V */
#define VOLTAGE_110_LOW_LIMIT_AD    319  /* 100V */							
#define HISTERESIS_VOLTAGE_AD         9  /* 3v (Approx 1 Volts p-p on AC Side for each 3 Counts) */
#define HISTERESIS_TIME_500US      50  /* 0.250 Seconds */
#define SURGE_SETTLE_TIME_500US    1000  /* 0.5 Seconds */
#define LED_FLASH_PERIOD_500US	   1000  /* 0.5 Seconds */
#define UNDER_DIP_PERIOD_500US	   6000  /* 3.0 Second */


/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/
typedef enum{
	VOLTAGE_UNDETERMINDED,
	VOLTAGE_110,
	VOLTAGE_220
}NOMINAL_VOLTAGE_OPTIONS;

typedef enum{
	VOLTAGE_UNDER,
	VOLTAGE_UNDER_WAITING,
	VOLTAGE_OVER,
	VOLTAGE_NOMINAL
}VOLTAGE_STATUS_OPTIONS;

typedef struct{
	NOMINAL_VOLTAGE_OPTIONS nominal_voltage;
	VOLTAGE_STATUS_OPTIONS  voltage_status;
	VOLTAGE_STATUS_OPTIONS  previous_voltage_status;
	unsigned int voltage_high_limit;
	unsigned int voltage_low_limit;
}ProgFlags;

typedef struct{
	unsigned int histerisis_timer;
	unsigned int output_settle_timer;
	unsigned int under_dip_test;
	unsigned char led_flash;
}Timers;


/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/
ProgFlags myProgFlags;
Timers    myTimers;

unsigned int ad_result,
			 sample_result,
			 sample_counter,
			 previous_sample_start,
			 sample_start,
			 max_ad_results[3];


/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/
void Update_Outputs (void);
void InterruptHandlerHigh (void);
void Initialise_Pic_Hardware (void);








/*!
*******************************************************************************
* Function Name:
*	main()
*
* Description:
*	\brief   main function. 
*
*******************************************************************************
*/
void main(void)
{
unsigned int voltage_level=0;

	Initialise_Pic_Hardware();

	LED_RED=0;
	LED_GREEN=0;
	TRIAC_OUTPUT=0;
	TRIAC_ENABLE=0;

	previous_sample_start=0;
	sample_start=0;
	sample_counter=0;

	max_ad_results[0]=0;
	max_ad_results[1]=0;
	max_ad_results[2]=0;

	myProgFlags.nominal_voltage=VOLTAGE_UNDETERMINDED;
	myProgFlags.voltage_status=VOLTAGE_UNDER;
	myProgFlags.previous_voltage_status=VOLTAGE_UNDER;

	myTimers.histerisis_timer=HISTERESIS_TIME_500US;
	myTimers.output_settle_timer=0;
	myTimers.led_flash=LED_FLASH_PERIOD_500US;
	myTimers.under_dip_test=UNDER_DIP_PERIOD_500US;

	while (1)
	{
		ClrWdt();	/* Watchdog Timeout = 32mS */
		
		/* If 110v or 220v has not yet been determined */
		if ((myProgFlags.nominal_voltage==VOLTAGE_UNDETERMINDED) && (myTimers.histerisis_timer==0))
		{
			/* If a sample sequence is compete, calculate voltage level */
			if (sample_start==0 && previous_sample_start==1)
			{
				voltage_level=(max_ad_results[0]+max_ad_results[1]+max_ad_results[2])/3;
				max_ad_results[0]=0;
				max_ad_results[1]=0;
				max_ad_results[2]=0;
				previous_sample_start=sample_start;
			}
			
			if (sample_start==1 && previous_sample_start==0)
				previous_sample_start=sample_start;

			/* Check if sample falls between 110V limits */
			if (voltage_level>VOLTAGE_110_LOW_LIMIT_AD && voltage_level<VOLTAGE_110_HIGH_LIMIT_AD) 
			{
				myProgFlags.nominal_voltage=VOLTAGE_110;
				myProgFlags.voltage_high_limit=VOLTAGE_110_HIGH_LIMIT_AD;
				myProgFlags.voltage_low_limit=VOLTAGE_110_LOW_LIMIT_AD;
			}
			/* Check if sample falls between 220V limits */
			if (voltage_level>VOLTAGE_220_LOW_LIMIT_AD && voltage_level<VOLTAGE_220_HIGH_LIMIT_AD) 
			{
				myProgFlags.nominal_voltage=VOLTAGE_220;
				myProgFlags.voltage_high_limit=VOLTAGE_220_HIGH_LIMIT_AD;
				myProgFlags.voltage_low_limit=VOLTAGE_220_LOW_LIMIT_AD;
			}
		}
		
		if (myProgFlags.nominal_voltage!=VOLTAGE_UNDETERMINDED)
		{
			/* If a sample sequence is compete, calculate voltage level */
			if (sample_start==0 && previous_sample_start==1)
			{
				voltage_level=(max_ad_results[0]+max_ad_results[1]+max_ad_results[2])/3;
				max_ad_results[0]=0;
				max_ad_results[1]=0;
				max_ad_results[2]=0;

				/* Check sample against limits - */
		   		/* Voltage must be nominal for Histeresis time before output re-enabled */
				/* Settle time must be at 0 before sample is checked, thus ignoring any initial power on surge that drags down the input voltage */
				if (myTimers.output_settle_timer==0)
				{
					if (voltage_level>myProgFlags.voltage_high_limit) 
					{
						myProgFlags.voltage_status=VOLTAGE_OVER;
						myTimers.histerisis_timer=HISTERESIS_TIME_500US;
					}
					else if (myProgFlags.voltage_status!=VOLTAGE_NOMINAL && voltage_level>(myProgFlags.voltage_high_limit-HISTERESIS_VOLTAGE_AD))
					{		
						myProgFlags.voltage_status=VOLTAGE_OVER;
						myTimers.histerisis_timer=HISTERESIS_TIME_500US;
					}
	
					else if (voltage_level<myProgFlags.voltage_low_limit && myProgFlags.voltage_status==VOLTAGE_UNDER_WAITING && (myTimers.under_dip_test==0))
					{
						myProgFlags.voltage_status=VOLTAGE_UNDER;
					//	myTimers.under_dip_test=UNDER_DIP_PERIOD_500US;
					}

					else if (voltage_level<myProgFlags.voltage_low_limit) 
					{
						if (myProgFlags.voltage_status!=VOLTAGE_UNDER_WAITING)
						{
							myProgFlags.voltage_status=VOLTAGE_UNDER_WAITING;
							myTimers.under_dip_test=UNDER_DIP_PERIOD_500US;
						}
					}

					else if (myProgFlags.voltage_status==VOLTAGE_UNDER && voltage_level<(myProgFlags.voltage_low_limit+HISTERESIS_VOLTAGE_AD))
					{		
						myProgFlags.voltage_status=VOLTAGE_UNDER;
						myTimers.histerisis_timer=HISTERESIS_TIME_500US;
					}
					else if (myTimers.histerisis_timer==0)
					{
						myProgFlags.voltage_status=VOLTAGE_NOMINAL;	
					}
					
					if (myProgFlags.previous_voltage_status!=myProgFlags.voltage_status)
					{
						if (myProgFlags.voltage_status==VOLTAGE_NOMINAL)
							myTimers.output_settle_timer=SURGE_SETTLE_TIME_500US;
			
						myProgFlags.previous_voltage_status=myProgFlags.voltage_status;
					}
	
					Update_Outputs();
				
				}

				previous_sample_start=sample_start;
			}
		
			if (sample_start==1 && previous_sample_start==0)
				previous_sample_start=sample_start;

		} /* end of if */

	}  /* end of while(1) */

}	/* end of main() */



/*!
*******************************************************************************
* Function Name:
*	Update_Outputs()
*
* Description:
*	\brief   Controls the PIC outputs. 
*
*	Controls the PIC outputs for Triac and LED
*        
*******************************************************************************
*/
void Update_Outputs (void)
{
	switch (myProgFlags.voltage_status)
	{
		case VOLTAGE_UNDER:
			TRIAC_OUTPUT=OFF;
			TRIAC_ENABLE=OFF;
			LED_YELLOW=ON;		/* YELLOW = RED and GREEN LED ON TOGETHER */
			break;
		case VOLTAGE_OVER:
			TRIAC_OUTPUT=OFF;
			TRIAC_ENABLE=OFF;
			LED_GREEN=OFF;
			LED_RED=ON;
			break;
		case VOLTAGE_NOMINAL:
			TRIAC_ENABLE=ON;
			TRIAC_OUTPUT=ON;
			LED_GREEN=ON;
			LED_RED=OFF;
			break;
	}

}

/*!
*******************************************************************************
* Function Name:
*	InterruptVectorHigh()
*
* Description:
*	\brief   High Priority Interrupt Vector 
*
*******************************************************************************
*/
#pragma code InterruptVectorHigh = 0x08
void InterruptVectorHigh (void)
{
  _asm
    goto InterruptHandlerHigh  //jump to interrupt routine
  _endasm
}

/*!
*******************************************************************************
* Function Name:
*	InterruptHandlerHigh()
*
* Description:
*	\brief   High Priority Interrupt Routine
*
*	Provides 500us timer and AD sample control 
*
*******************************************************************************
*/
#pragma code
#pragma interrupt InterruptHandlerHigh
void InterruptHandlerHigh(void)
{
unsigned char i;

	TMR0L=0xFF-117;				// Reload Value - 500uS (1:4 Pre-Scale)
	INTCONbits.T0IF = 0;		// Clear interrupt flag, ready for next

	myTimers.histerisis_timer-=(myTimers.histerisis_timer>0);
	myTimers.output_settle_timer-=(myTimers.output_settle_timer>0);
	myTimers.led_flash-=(myTimers.led_flash>0);
	myTimers.under_dip_test-=(myTimers.under_dip_test>0);

	
	/* If operating voltage is undetermined, flash RED LED */
	if (myTimers.led_flash==0)
	{
		myTimers.led_flash=LED_FLASH_PERIOD_500US;
		if (myProgFlags.nominal_voltage==VOLTAGE_UNDETERMINDED)
		{
			LED_GREEN=0;
			LED_RED=1-LED_RED;
		}
	}
		
	/* Sample AD */
	ADCON0bits.GO_DONE=1;
	while (ADCON0bits.GO_DONE);
	ad_result=((int)ADRESH<<8 | (int)ADRESL);

	/* Store the 3 highest samples this cycle */
	if (sample_start)
	{
		if (ad_result>max_ad_results[0])
		{
			max_ad_results[2]=max_ad_results[1];
			max_ad_results[1]=max_ad_results[0];
			max_ad_results[0]=ad_result;
		}
		else if (ad_result>max_ad_results[1])
		{
			max_ad_results[2]=max_ad_results[1];
			max_ad_results[1]=ad_result;
		}
		else if (ad_result>max_ad_results[2])
		{
			max_ad_results[1]=ad_result;
		}
	}

	/* Decide if over or under 50v (Start \ End Sample Process) */
	if ((!sample_start) && (ad_result>150) && (!previous_sample_start))
	{
		sample_start=1;
	}

	if ((ad_result<150) && (sample_start)) sample_start=0;

}


/*!
*******************************************************************************
* Function Name:
*	Initialise_Pic_Hardware()
*
* Description:
*	\brief   Initialised PIC periphials
*
*******************************************************************************
*/
void Initialise_Pic_Hardware (void)
{

	OSCCON=0b01100000;			// OSC = 4Mhz

	TRISA = 0x7F;				// Port A Direction Register <RA7:RA0> OIIIIIII
	TRISB = 0xF8;				// Port B Direction Register <RB7:RB0> IIIIIOOO
		
	ADCON0 = 0b01000001;			// Vref, AN0, AD On
	ADCON1 = 0b01111110; 			// AN0 Analog, Rest Digital IO
	ADCON2 = 0b10100001;			// Right Justify, 8Tad, FOsc\8

	T0CONbits.T08BIT=1;			// Timer0 Set to 8 bits
	T0CONbits.T0CS=0;			// Timer0 increments on instruction clock
	T0CONbits.PSA=0;			// PreScaler Assigned to Timer0
	T0CONbits.T0PS0=1;			// PreScaler set to 1:4
	T0CONbits.T0PS1=0;
	T0CONbits.T0PS2=0;
	T0CONbits.TMR0ON=1;			// Enable Timer0
	INTCONbits.T0IE=1;			// Enable interrupt on Timer0 overflow
	RCONbits.IPEN=0;			// Disable Interrupt Priorities
	INTCONbits.GIE=1;			// Global interrupt enable

}

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

#endif
