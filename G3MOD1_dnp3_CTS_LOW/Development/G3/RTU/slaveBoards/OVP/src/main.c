/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: c_template.c 1469 2012-06-20 20:51:28Z fryers_j $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development/G3/common/template/c_template.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision: 1469 $: (Revision of last commit)
 *               $Author: fryers_j $: (Author of last commit)
 *       \date   $Date: 2012-06-20 21:51:28 +0100 (Wed, 20 Jun 2012) $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define ON  1
#define OFF 0

//#define LED_RED      PORTBbits.RB1
//#define LED_GREEN    PORTBbits.RB2
//#define LED_YELLOW   LED_RED=LED_GREEN
//#define TRIAC_OUTPUT PORTAbits.RA7
//#define TRIAC_ENABLE PORTBbits.RB0

#define VOLTAGE_220_HIGH_LIMIT_AD   887  /* ~265V */
#define VOLTAGE_220_LOW_LIMIT_AD    633  /* 190V */
#define VOLTAGE_110_HIGH_LIMIT_AD   412  /* 120V */
#define VOLTAGE_110_LOW_LIMIT_AD    319  /* 100V */
#define HISTERESIS_VOLTAGE_AD         9  /* 3v (Approx 1 Volts p-p on AC Side for each 3 Counts) */
#define HISTERESIS_TIME_500US      50  /* 0.250 Seconds */
#define SURGE_SETTLE_TIME_500US    1000  /* 0.5 Seconds */
#define LED_FLASH_PERIOD_500US	   1000  /* 0.5 Seconds */
#define UNDER_DIP_PERIOD_500US	   6000  /* 3.0 Second */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	VOLTAGE_UNDETERMINDED,
	VOLTAGE_110,
	VOLTAGE_220
}NOMINAL_VOLTAGE_OPTIONS;

typedef enum
{
	VOLTAGE_UNDER,
	VOLTAGE_UNDER_WAITING,
	VOLTAGE_OVER,
	VOLTAGE_NOMINAL
}VOLTAGE_STATUS_OPTIONS;

typedef struct
{
	NOMINAL_VOLTAGE_OPTIONS nominal_voltage;
	VOLTAGE_STATUS_OPTIONS  voltage_status;
	VOLTAGE_STATUS_OPTIONS  previous_voltage_status;
	lu_uint16_t voltage_high_limit;
	lu_uint16_t voltage_low_limit;
}ProgFlags;

typedef struct
{
	lu_uint16_t histerisis_timer;
	lu_uint16_t output_settle_timer;
	lu_uint16_t under_dip_test;
	lu_uint8_t  led_flash;
}Timers;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void Update_Outputs (void);
void InterruptHandlerHigh (void);
void Initialise_Pic_Hardware (void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

ProgFlags myProgFlags;
Timers    myTimers;

lu_uint16_t ad_result;
lu_uint16_t	sample_result;
lu_uint16_t	sample_counter;
lu_uint16_t	previous_sample_start;
lu_uint16_t	sample_start;
lu_uint16_t	max_ad_results[3];

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
void main(void)
{
	lu_uint16_t voltage_level=0;

	//Initialise_Pic_Hardware();

//	LED_RED=0;
//	LED_GREEN=0;
//	TRIAC_OUTPUT=0;
//	TRIAC_ENABLE=0;

	previous_sample_start=0;
	sample_start=0;
	sample_counter=0;

	max_ad_results[0]=0;
	max_ad_results[1]=0;
	max_ad_results[2]=0;

	myProgFlags.nominal_voltage=VOLTAGE_UNDETERMINDED;
	myProgFlags.voltage_status=VOLTAGE_UNDER;
	myProgFlags.previous_voltage_status=VOLTAGE_UNDER;

	myTimers.histerisis_timer=HISTERESIS_TIME_500US;
	myTimers.output_settle_timer=0;
	myTimers.led_flash=LED_FLASH_PERIOD_500US;
	myTimers.under_dip_test=UNDER_DIP_PERIOD_500US;

	while (1)
	{
		//ClrWdt();	/* Watchdog Timeout = 32mS */

		/* If 110v or 220v has not yet been determined */
		if ((myProgFlags.nominal_voltage == VOLTAGE_UNDETERMINDED) &&
			(myTimers.histerisis_timer == 0)
		   )
		{
			/* If a sample sequence is compete, calculate voltage level */
			if (sample_start == 0 && previous_sample_start == 1)
			{
				voltage_level = (max_ad_results[0] +
						        max_ad_results[1] +
						        max_ad_results[2]) / 3;
				max_ad_results[0] = 0;
				max_ad_results[1] = 0;
				max_ad_results[2] = 0;
				previous_sample_start = sample_start;
			}

			if (sample_start==1 && previous_sample_start==0)
				previous_sample_start = sample_start;

			/* Check if sample falls between 110V limits */
			if (voltage_level > VOLTAGE_110_LOW_LIMIT_AD &&
				voltage_level < VOLTAGE_110_HIGH_LIMIT_AD
			   )
			{
				myProgFlags.nominal_voltage = VOLTAGE_110;
				myProgFlags.voltage_high_limit = VOLTAGE_110_HIGH_LIMIT_AD;
				myProgFlags.voltage_low_limit = VOLTAGE_110_LOW_LIMIT_AD;
			}

			/* Check if sample falls between 220V limits */
			if (voltage_level > VOLTAGE_220_LOW_LIMIT_AD &&
				voltage_level < VOLTAGE_220_HIGH_LIMIT_AD)
			{
				myProgFlags.nominal_voltage = VOLTAGE_220;
				myProgFlags.voltage_high_limit = VOLTAGE_220_HIGH_LIMIT_AD;
				myProgFlags.voltage_low_limit = VOLTAGE_220_LOW_LIMIT_AD;
			}
		}

		if (myProgFlags.nominal_voltage != VOLTAGE_UNDETERMINDED)
		{
			/* If a sample sequence is compete, calculate voltage level */
			if (sample_start == 0 && previous_sample_start == 1)
			{
				voltage_level = (max_ad_results[0] +
						         max_ad_results[1] +
						         max_ad_results[2]) / 3;
				max_ad_results[0] = 0;
				max_ad_results[1] = 0;
				max_ad_results[2] = 0;

				/* Check sample against limits - */
		   		/* Voltage must be nominal for Histeresis time before output re-enabled */
				/* Settle time must be at 0 before sample is checked, thus ignoring any initial power on surge that drags down the input voltage */
				if (myTimers.output_settle_timer == 0)
				{
					if (voltage_level > myProgFlags.voltage_high_limit)
					{
						myProgFlags.voltage_status = VOLTAGE_OVER;
						myTimers.histerisis_timer = HISTERESIS_TIME_500US;
					}
					else if (myProgFlags.voltage_status != VOLTAGE_NOMINAL &&
							voltage_level >
					        (myProgFlags.voltage_high_limit - HISTERESIS_VOLTAGE_AD)
					        )
					{
						myProgFlags.voltage_status = VOLTAGE_OVER;
						myTimers.histerisis_timer = HISTERESIS_TIME_500US;
					}
					else if (voltage_level < myProgFlags.voltage_low_limit &&
							 myProgFlags.voltage_status == VOLTAGE_UNDER_WAITING &&
							 (myTimers.under_dip_test == 0)
							)
					{
						myProgFlags.voltage_status = VOLTAGE_UNDER;
					//	myTimers.under_dip_test=UNDER_DIP_PERIOD_500US;
					}
					else if (voltage_level<myProgFlags.voltage_low_limit)
					{
						if (myProgFlags.voltage_status != VOLTAGE_UNDER_WAITING)
						{
							myProgFlags.voltage_status = VOLTAGE_UNDER_WAITING;
							myTimers.under_dip_test = UNDER_DIP_PERIOD_500US;
						}
					}
					else if (myProgFlags.voltage_status == VOLTAGE_UNDER &&
							 voltage_level <
							 (myProgFlags.voltage_low_limit+HISTERESIS_VOLTAGE_AD)
                            )
					{
						myProgFlags.voltage_status = VOLTAGE_UNDER;
						myTimers.histerisis_timer = HISTERESIS_TIME_500US;
					}
					else if (myTimers.histerisis_timer == 0)
					{
						myProgFlags.voltage_status = VOLTAGE_NOMINAL;
					}

					if (myProgFlags.previous_voltage_status != myProgFlags.voltage_status)
					{
						if (myProgFlags.voltage_status == VOLTAGE_NOMINAL)
							myTimers.output_settle_timer = SURGE_SETTLE_TIME_500US;

						myProgFlags.previous_voltage_status = myProgFlags.voltage_status;
					}

					Update_Outputs();

				}

				previous_sample_start = sample_start;
			}

			if (sample_start == 1 &&
				previous_sample_start == 0
			    )
			{
				previous_sample_start = sample_start;
			}

		} /* end of if */

	}  /* end of while(1) */

}	/* end of main() */


/*!
*******************************************************************************
* Function Name:
*	Update_Outputs()
*
* Description:
*	\brief   Controls the PIC outputs.
*
*	Controls the PIC outputs for Triac and LED
*
*******************************************************************************
*/
void Update_Outputs (void)
{
	switch (myProgFlags.voltage_status)
	{
		case VOLTAGE_UNDER:
//			TRIAC_OUTPUT=OFF;
//			TRIAC_ENABLE=OFF;
//			LED_YELLOW=ON;		/* YELLOW = RED and GREEN LED ON TOGETHER */
			break;

		case VOLTAGE_OVER:
//			TRIAC_OUTPUT=OFF;
//			TRIAC_ENABLE=OFF;
//			LED_GREEN=OFF;
//			LED_RED=ON;
			break;

		case VOLTAGE_NOMINAL:
//			TRIAC_ENABLE=ON;
//			TRIAC_OUTPUT=ON;
//			LED_GREEN=ON;
//			LED_RED=OFF;
			break;

		default:
			break;
	}

}

/*
 *********************** End of file ******************************************
 */
