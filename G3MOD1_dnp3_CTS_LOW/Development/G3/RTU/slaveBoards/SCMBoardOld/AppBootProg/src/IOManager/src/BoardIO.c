/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BoardIO.c 2070 2012-11-26 21:33:34Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/SCMBoardOld/AppBootProg/src/IOManager/src/BoardIO.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO table (shadow copy of IO)
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"
#include "ModuleProtocol.h"

/* Local includes */
#include "BoardIO.h"
#include "BoardIOMap.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

IOTableStr	boardIOTable[IO_ID_LAST];


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR boardIOGetModuleID(lu_uint8_t *deviceIDPtr)
{
	SB_ERROR 	retError;

	retError = IOManagerIOGetModuleID(deviceIDPtr, IO_ID_MOD_SEL1, IO_ID_MOD_SEL2, IO_ID_MOD_SEL3, IO_ID_MOD_SEL4);

	return retError;
}

SB_ERROR boardIOGetLocalRemoteStatus(IO_STAT_LOR *ioStatLORPtr)
{
	SB_ERROR 	retError;

	retError = IOManagerIOGetLocalRemoteStatus(ioStatLORPtr, IO_ID_LOCAL_LINE, IO_ID_REMOTE_LINE);

	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
