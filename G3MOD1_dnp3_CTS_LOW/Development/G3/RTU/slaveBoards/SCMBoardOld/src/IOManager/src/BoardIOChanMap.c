/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"


/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(SCM_CH_DINPUT_SWITCH_OPEN, 				IO_ID_SWITCH_OPEN_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_SWITCH_CLOSED, 				IO_ID_SWITCH_CLOSED_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_SWITCH_EARTHED, 			IO_ID_SWITCH_EARTHED_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_INSULTATION_LOW, 			IO_ID_INSULTATION_LOW_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_ACTUATOR_DISABLED, 			IO_ID_ACTUATOR_DISABLED_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_EXTERNAL_FPI, 		    	IO_ID_EXTERNAL_FPI_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_SPARE_INPUT_1, 			    IO_ID_SPARE1_INPUT),
	IOM_IOCHAN_DI(SCM_CH_DINPUT_SPARE_INPUT_2, 			    IO_ID_SPARE2_INPUT),

	IOM_IOCHAN_DI(0, 						                IO_ID_NA)
};

IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),

	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};

#ifdef SCM_V_XXE
IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AI(SCM_CH_AINPUT_TEMPERATURE, 				IO_ID_TEMP_MEASURE, \
			      CAL_ID_NA,		   	        IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(SCM_CH_AINPUT_CT, 						IO_ID_I_MEASURE, \
			      CAL_ID_I_MEASURE,    			IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(SCM_CH_AINPUT_ON_ELAPSED_TIME, 						IO_ID_VIRT_ON_ELAPSED_TIME, \
			      CAL_ID_NA,    			IO_AI_EVENTRATE_MS),
				  

	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, \
			      CAL_ID_NA,    0)
};
#else
IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AI(SCM_CH_AINPUT_TEMPERATURE, 				IO_ID_TEMP_MEASURE, \
			      CAL_ID_TEMP_MEASURE,		   	IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(SCM_CH_AINPUT_CT, 						IO_ID_I_MEASURE, \
				  CAL_ID_I_MEASURE,    			IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(SCM_CH_AINPUT_ON_ELAPSED_TIME, 						IO_ID_VIRT_ON_ELAPSED_TIME, \
			      CAL_ID_NA,    			IO_AI_EVENTRATE_MS),
				  
	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, \
			      CAL_ID_NA,    0)
};
#endif

IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA),

	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
