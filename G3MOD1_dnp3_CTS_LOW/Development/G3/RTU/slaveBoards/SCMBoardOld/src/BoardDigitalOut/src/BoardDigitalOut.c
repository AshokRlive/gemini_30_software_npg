/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"
#include "BoardIO.h"
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"

#include "BoardDigitalOut.h"
#include "DigitalOutController.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



static DigitalOutMapStr  BoardDigitalOutTable[IOM_CH_DOUT_LAST + 1] =
{
	/* ioChan        ioIDOutReset       ioIDOutSet                       ioIDInFeedBack                     ioIDLED */
//	DOC_IOCHAN(SCM_CH_DOUT_1, IO_ID_NA, IO_ID_RELAY_OUTPUT_1_LK_COMMAND, IO_ID_RELAY_FB_OUTPUT_1_LK_CLOSED, IO_ID_LED_FP_OUTPUT_1)
//	DOC_IOCHAN(SCM_CH_DOUT_2, IO_ID_NA, IO_ID_RELAY_OUTPUT_2_LK_COMMAND, IO_ID_RELAY_FB_OUTPUT_2_LK_CLOSED, IO_ID_LED_FP_OUTPUT_2)

	DOC_IOCHAN(SCM_CH_DOUT_1, IO_ID_NA, IO_ID_RELAY_OUTPUT_1_LK_COMMAND, IO_ID_NA,                          IO_ID_LED_FP_OUTPUT_1)
	DOC_IOCHAN(SCM_CH_DOUT_2, IO_ID_NA, IO_ID_RELAY_OUTPUT_2_LK_COMMAND, IO_ID_NA,                          IO_ID_LED_FP_OUTPUT_2)

	/* End of table marker */
	DOC_LAST
};


static DigitalOutInitStr digitalOutInitParams =
{
	&BoardDigitalOutTable[0], SCM_CH_DOUT_LAST
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void boardDigitalOutInit(void)
{
    /* Initialise Digital Output Controller */
    DigitalOutControllerInit(&digitalOutInitParams);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
