/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [slave board]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Slave board CAN protocol decoder module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   08/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolDecoder.h"

#include "Calibration.h"
#include "IOManager.h"
#include "SwitchController.h"
#include "BoardStatusManager.h"

#include "BoardCalibration.h"
#include "NVRam.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC_NOT_HANDLED;

	/* Call board specific decode functions */

	/* IOManager CAN protocol decoder */
	retError = IOManagerCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	/* Calibration CAN Protocol Decoder */
	retError = CalibrationCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	/* Switch Controller CAN protocol decoder */
	retError = SwitchControlCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	/* Aux Switch Controller CAN protocol decoder */

	/* SCM CAN protocol decoder */
	retError = SCMCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	/* Digital Out Controller CAN Protocol Decoder */
	retError = DigitalOutControllerCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = BoardCalibrationCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

	retError = NVRAMCANProtocolDecoder(msgPtr, time);
	if (retError != SB_ERROR_CANC_NOT_HANDLED)
	{
		return retError;
	}

    /* Do nothing */
	return retError;
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
