/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "ModuleProtocol.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "NVRam.h"
#include "IOManager.h"
#include "IOManagerIOUpdate.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "IOManagerI2CExtTempIOUpdate.h"
#include "IOManagerI2CIOUpdate.h"
#include "BoardIO.h"
#include "BoardIOMap.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmStatusManagerEnum.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

#include "Calibration.h"
#include "systemStatus.h"
#include "systemTime.h"

#include "DualSwitchController.h"

#include "DigitalOutController.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR SCMConfig(SCMMK2ConfigStr *scmConfigPtr);
SB_ERROR SCMPeriodicManager(void);
SB_ERROR SCMPowerSaveMode(lu_bool_t powerSaveMode);
void CheckIDSwitchChange(void);

void SCMHealthMonitor(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				 0, 				0 				          }, // Run all the time

	{CANFramingTick, 			 0, 				CAN_TICK_MS		          }, // Run every 10ms

	{IOManagerIORunUpdateGPIO, 	 0, 				IOMAN_UPDATE_GPIO_MS      }, // run every 10ms
	{IOManagerIntAnalogIOUpdate, 0, 				IOMAN_UPDATE_INAI_MS      }, // run every 15 ms
	{IOManagerI2CIOUpdate,       0, 				IOMAN_UPDATE_I2CIO_MS     }, // run every 40 ms
	{IOManagerIOAIEventUpdate,   0,                 IOMAN_UPDATE_INAI_EVENT_MS}, //run every 50 ms
	{IOManagerI2CExtTempIOUpdate,0, 				IOMAN_UPDATE_I2CEXTMPIO_MS}, // run every 100 ms

	{NVRamUpdateTick, 		     0, 				NVRAM_UPDATE_TICK_MS 	  }, // run every 100ms

	{DualSwitchControlTick, 	 0, 				SWITCH_CON_TICK_MS 	      }, // run every 100ms

	{DigitalOutControllerTick, 	 0, 				DIG_OUT_CON_TICK_MS 	  }, // run every

	{SCMPeriodicManager,         0,                 200                       },  // run every 200ms

	{(SMThreadRun)0L, 	    	 0, 				0   			          }  // end of table
};

/*! List of supported CAN messages */
static const filterTableStr SCMModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! SCM commands */
	{  MODULE_MSG_TYPE_CFG, MODULE_MSG_ID_CFG_SCM_MK2_C            	  , LU_FALSE , 0   	  }

};

SwConInitParamsStr swConInitParams;


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{
	SB_ERROR sbError;


	/* Setup CAN Msg filter */
	sbError = CANFramingAddFilter( SCMModulefilterTable,
								   SU_TABLE_SIZE(SCMModulefilterTable,
								   filterTableStr)
								 );


	swConInitParams.polaritySelect.ioIDSelectA                     = IO_ID_NA;
	swConInitParams.polaritySelect.ioIDSelectB                     = IO_ID_NA;

	swConInitParams.polaritySelect.switch_A_Polarity.ioIDFeedback = IO_ID_NA;
	swConInitParams.polaritySelect.switch_B_Polarity.ioIDFeedback = IO_ID_NA;

	/* Relay Commands */
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_A].ioIdRelayDown	= IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_A].ioIdRelayUp	    = IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_A].ioIdRelayLink	= IO_ID_NA;

	swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_A].ioIdRelayDown	= IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_A].ioIdRelayUp	= IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_A].ioIdRelayLink	= IO_ID_NA;

	swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_B].ioIdRelayDown	= IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_B].ioIdRelayUp	    = IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_OPEN_SWITCH_B].ioIdRelayLink	= IO_ID_NA;

	swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_B].ioIdRelayDown  = IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_B].ioIdRelayUp	= IO_ID_NA;
	swConInitParams.SwitchConParams[DSM_CH_SWOUT_CLOSE_SWITCH_B].ioIdRelayLink  = IO_ID_NA;

	swConInitParams.ioIdBridgeMotor_A_AA = IO_ID_BRIDGE_A_24V_A;
	swConInitParams.ioIdBridgeMotor_A_AB = IO_ID_BRIDGE_A_24V_B;
	swConInitParams.ioIdBridgeMotor_A_BA = IO_ID_BRIDGE_B_24V_A;
	swConInitParams.ioIdBridgeMotor_A_BB = IO_ID_BRIDGE_B_24V_B;

	swConInitParams.ioIdBridgeMotor_B_AA = IO_ID_NA;
	swConInitParams.ioIdBridgeMotor_B_AB = IO_ID_NA;
	swConInitParams.ioIdBridgeMotor_B_BA = IO_ID_NA;
	swConInitParams.ioIdBridgeMotor_B_BB = IO_ID_NA;

	swConInitParams.ioIdHardwareActMotorLimit_A = IO_ID_SWITCHGEAR_CE;
	swConInitParams.ioIdHardwareActMotorLimit_B = IO_ID_NA;

	swConInitParams.ioIdMotorJogEnable_A = IO_ID_OPEN_CLOSE_MOTOR_JOG_EN;
	swConInitParams.ioIdMotorJogEnable_B = IO_ID_NA;

	swConInitParams.ioIdLedFP_SW_A_CmdOpen 			= IO_ID_LED_SW_OUTPUT_SWITCH_OPENING;
	swConInitParams.ioIdLedFP_SW_A_CmdClose 		= IO_ID_LED_SW_OUTPUT_SWITCH_CLOSING;

	swConInitParams.ioIdLedFP_SW_B_CmdOpen 			= IO_ID_NA;
	swConInitParams.ioIdLedFP_SW_B_CmdClose 		= IO_ID_NA;

	/* Front panel LEDs */
	swConInitParams.ioIdLedFP_SW_A_MotorSupply		= IO_ID_LED_SW_MOTOR_OUTPUT;
	swConInitParams.ioIdLedFP_SW_B_MotorSupply		= IO_ID_NA;

	swConInitParams.ioIdRelayCtrlVMotor_A_A			= IO_ID_VMOTOR_MOSFET_EN;
	swConInitParams.ioIdRelayCtrlVMotor_A_B			= IO_ID_NA;

	swConInitParams.ioIdRelayCtrlVMotor_B_A			= IO_ID_NA;
	swConInitParams.ioIdRelayCtrlVMotor_B_B			= IO_ID_NA;

	/* Relay feed-back */
	swConInitParams.ioIdRelayFb_SW_A_OpenUpDnClosed	 = IO_ID_NA;
	swConInitParams.ioIdRelayFb_SW_A_OpenLkClosed	 = IO_ID_NA;

	swConInitParams.ioIdRelayFb_SW_A_CloseUpDnClosed = IO_ID_NA;
	swConInitParams.ioIdRelayFb_SW_A_CloseLkClosed	 = IO_ID_NA;

	swConInitParams.ioIdRelayFb_SW_A_OutputUpDnClosed = IO_ID_NA;
	swConInitParams.ioIdRelayFb_SW_A_OutputLkClosed	  = IO_ID_NA;

	swConInitParams.ioIdRelayFb_SW_B_OpenUpDnClosed	 = IO_ID_NA;
	swConInitParams.ioIdRelayFb_SW_B_OpenLkClosed	 = IO_ID_NA;

	swConInitParams.ioIdRelayFb_SW_B_CloseUpDnClosed = IO_ID_NA;
	swConInitParams.ioIdRelayFb_SW_B_CloseLkClosed	 = IO_ID_NA;

	swConInitParams.ioIdRelayFb_SW_B_OutputUpDnClosed = IO_ID_NA;
	swConInitParams.ioIdRelayFb_SW_B_OutputLkClosed	  = IO_ID_NA;

	swConInitParams.ioIdVMotor_SW_A_FbOn			= IO_ID_NA;
	swConInitParams.ioIdVMotor_SW_B_FbOn			= IO_ID_NA;

	swConInitParams.ioId_SW_A_OpenedInput = IO_ID_SW_OPEN_INPUT;
	swConInitParams.ioId_SW_A_ClosedInput = IO_ID_SW_CLOSED_INPUT;

	swConInitParams.ioId_SW_A_ActLimit_OpenedInput = IO_ID_SW_INDICATION_6_ACT_LIM_OPEN;
	swConInitParams.ioId_SW_A_ActLimit_ClosedInput = IO_ID_SW_INDICATION_7_ACT_LIM_CLOSE;

	swConInitParams.ioId_SW_B_ActLimit_OpenedInput = IO_ID_NA;
	swConInitParams.ioId_SW_B_ActLimit_ClosedInput = IO_ID_NA;

	swConInitParams.ioId_SW_B_OpenedInput = IO_ID_NA;
	swConInitParams.ioId_SW_B_ClosedInput = IO_ID_NA;

	/* Init Switch Controller */
	DualSwitchControlInit( &swConInitParams);

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}

SB_ERROR SCMCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retError;

	retError = SB_ERROR_CANC;

	LU_UNUSED( time );

	/* Force a timer tick in the state machine */
	DualSwitchFSMTick();

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CFG:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CFG_SCM_MK2_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(SCMMK2ConfigStr))
			{
				retError = SCMConfig((SCMMK2ConfigStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retError = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;


	default:
		retError = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retError;

}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

SB_ERROR SCMConfig(SCMMK2ConfigStr *scmConfigPtr)
{
	SB_ERROR retError;
	ModStatusReplyStr modStatusReply;

	if (scmConfigPtr->greenLEDPolarity)
	{
		/* OPEN == GREEN Polarity */
		retError = IOManagerSetValue(IO_ID_COL2_RED, 0);
		retError = IOManagerSetValue(IO_ID_COL1_GREEN, 1);
	}
	else
	{
		/* OPEN == RED Polarity */
		retError = IOManagerSetValue(IO_ID_COL1_GREEN, 0);
		retError = IOManagerSetValue(IO_ID_COL2_RED, 1);
	}

	retError = DualSwitchPolarityConfigA(DUAL_SWITCH_POLARITY_DIRECT_DRIVE,
			                             scmConfigPtr->doNotCheckSwitchPosition,
			                             scmConfigPtr->hardwareMotorLimitEnable,
			                             scmConfigPtr->actuatorLimitSwitchEnable);

	modStatusReply.status  	= REPLY_STATUS_OKAY;

	/*! Send CAN reply message */
	return CANCSendMCM( MODULE_MSG_TYPE_CFG,
						MODULE_MSG_ID_CFG_SCM_MK2_R,
						sizeof(ModStatusReplyStr),
						(lu_uint8_t *)&modStatusReply
					  );
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR SCMPowerSaveMode(lu_bool_t powerSaveMode)
{
	SB_ERROR retError;
	MODULE_BOARD_ERROR  moduleError;
	static MODULE_BOARD_ERROR oldModuleError = MODULE_BOARD_ERROR_TSYNCH;

	/* Turn off All LED's under software control */
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_OK_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_OK_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_CAN_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_CAN_RED, powerSaveMode);

	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INPUT_SWITCH_OPEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INPUT_SWITCH_CLOSED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INDICATION_1, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INDICATION_2, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INDICATION_3, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INDICATION_4, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INDICATION_5, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_INDICATION_6, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_MOTOR_OUTPUT, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_SW_OUTPUT_1, powerSaveMode);

	/* Extinguish LED's under hardware control / gated by software */
	retError = IOManagerSetValue(IO_ID_LED_CTRL_EXTINGUISH, powerSaveMode ? 1L : 0L);

	/* Update status LED's */
	moduleError = SSGetBError();

	if (oldModuleError != moduleError)
	{
		oldModuleError = moduleError;

		if (moduleError & MODULE_BOARD_ERROR_ALARM_CRITICAL)
		{
			/* Critical - Fast Flash Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 250);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_ERROR)
		{
			/* Error - Slow Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 1000);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_WARNING)
		{
			/* Warning - Fast Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 250);
		} else
		{
			/* Normal - Slow Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 1000);
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return none
 *
 ******************************************************************************
 */
void CheckIDSwitchChange(void)
{
	lu_uint8_t idSwitchNew;
	static lu_uint8_t idSwitch = 10;

	/* Read the ID Switch */
	boardIOGetModuleID(&idSwitchNew);

	if (idSwitch != idSwitchNew)
	{
		idSwitch = idSwitchNew;

		/* Compare ID switch with the ID the CAN driver is using */
		if (idSwitch != CANCGetDeviceID())
		{
			/* Force card to go offline */
			SSSetBStatus(MODULE_BOARD_STATUS_ERROR);

			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_CRITICAL,
								SYS_ALARM_SUBSYSTEM_SYSTEM,
								SYSALC_SYSTEM_ID_SWITCH,
								(lu_uint16_t)idSwitchNew
								);
		}
		else
		{
			SysAlarmSetLogEvent(LU_FALSE,
								SYS_ALARM_SEVERITY_CRITICAL,
								SYS_ALARM_SUBSYSTEM_SYSTEM,
								SYSALC_SYSTEM_ID_SWITCH,
								0
								);
		}
	}
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */

SB_ERROR SCMPeriodicManager(void)
{
	lu_int32_t value;
	SB_ERROR RetVal;
	lu_bool_t SwitchControlStartStatus;
	lu_bool_t powerSaveMode;

	/* Set virtual elapsed time value */
	RetVal = IOManagerSetValue(IO_ID_VIRT_ON_ELAPSED_TIME, STGetRunningTimeSecs());
	
	/* Check if ID switch mis-match */
	CheckIDSwitchChange();

	/* Get state of hardware and write to LEDs  */

	/* Get state of hardware and write to LEDs  */
	RetVal = IOManagerGetValue(swConInitParams.ioId_SW_A_OpenedInput, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INPUT_SWITCH_OPEN, value);

	RetVal = IOManagerGetValue(swConInitParams.ioId_SW_A_ClosedInput, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INPUT_SWITCH_CLOSED, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_1, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_1, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_2, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_2, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_3, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_3, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_4, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_4, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_5, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_5, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_6_ACT_LIM_OPEN, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_6, value);

	RetVal = IOManagerGetValue(IO_ID_SW_INDICATION_7_ACT_LIM_CLOSE, &value);
	RetVal = IOManagerSetValue(IO_ID_LED_SW_INDICATION_7, value);


    /* Update module status - temporary workaround */
    if(SSGetBStatus() == MODULE_BOARD_STATUS_STARTING)
    {
    	/* Allow the IOManager to report events */
		IOManagerStartEventing();

		/* Start SCM subsystem */
		SwitchControlStartStatus = DualSwitchControlStart();
		if( SwitchControlStartStatus == LU_TRUE)
		{
			/* SCM has started so change status to on-line */
			// SSSetBStatus(MODULE_BOARD_STATUS_ON_LINE);
		}

        SSSetBStatus(MODULE_BOARD_STATUS_ON_LINE);
    }

    /* Power saving mode check - LED off */
	powerSaveMode = SSGetPowerSaveMode();
	SCMPowerSaveMode(powerSaveMode);

	/*
	 * Monitor the handshake and system healthy signals
	 * from the hardware watchdog PIC
	 */
    SCMHealthMonitor();

    return RetVal;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void SCMHealthMonitor(void)
{
	SB_ERROR 	retError;

	static	lu_bool_t initialised 		 = LU_FALSE;
	static	lu_int32_t lastHandshake 	 = 0;
	static	lu_int32_t lastSystemHealthy = 0;
	static 	lu_int32_t lastHandshakeTime = 0;
	static	lu_bool_t handshakeAlarmSet	 = LU_FALSE;
	static	lu_bool_t sysHealthyAlarmSet = LU_FALSE;

	lu_int32_t 	wdhandshake;
	lu_int32_t 	sysHealthy;
	lu_int32_t 	time;
	lu_int32_t 	elapsedTime;

	/*
	 * Get current time
	 */
	time = STGetTime();

	/*
	 * If this is the first execution of the function then
	 * initialise the times and outputs
	 */
	if( initialised == LU_FALSE )
	{
		/* Initialisation - Wait for handshake to go low */
		retError = IOManagerGetValue(IO_ID_WD_HANDSHAKE, &wdhandshake);
		retError = IOManagerGetValue(IO_ID_SYS_HEALTHY, &sysHealthy);
//		if( wdhandshake == 0 )
		{
			/* Start kicking the PIC watch dog every second */
			retError = IOManagerSetFlash(IO_ID_HWDOG_1HZ_KICK, ONE_HZ_KICK_PERIOD);
			lastHandshake = 0;
			lastSystemHealthy = sysHealthy;

			/* Initialise the timer for the handshake */
			lastHandshakeTime = time;

			initialised = LU_TRUE;
			return;
		}
	}

	/*
	 * Get time since last handshake
	 */
	elapsedTime = STElapsedTime( lastHandshakeTime , time );

	/*
	 * Read the state of the handshake signal from the PIC
	 */
	retError = IOManagerGetValue(IO_ID_WD_HANDSHAKE, &wdhandshake);

	/*
	 * Check for a change of state in the handshake signal
	 */
	if ( lastHandshake != wdhandshake )
	{
		/*
		 * A change of state has been detected so reset timer and handshake state
		 */
		lastHandshake = wdhandshake;
		lastHandshakeTime = time;
		/* Clear alarm if already set */
		if( handshakeAlarmSet == LU_TRUE )
		{
			SysAlarmSetLogEvent(LU_FALSE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
								SYSALC_STATUS_SYSTEM_WD_HANDSHAKE,
								0
							   );
			handshakeAlarmSet = LU_FALSE;
		}
	}
	else if( elapsedTime > PIC_HANDSHAKE_ERROR_PERIOD )
	{
		/*
		 * A change of state of the handshake signal has not been received
		 * so set the alarm if it hasn't been set already
		 */
		if( handshakeAlarmSet == LU_FALSE )
		{
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
								SYSALC_STATUS_SYSTEM_WD_HANDSHAKE,
								0
							   );
			handshakeAlarmSet = LU_TRUE;
		}

	}

	/*
	 * Read the state of the system healthy signal from the PIC
	 */
	retError = IOManagerGetValue(IO_ID_SYS_HEALTHY, &sysHealthy);

	/*
	 * Set the alarm if the system healthy signal goes low
	 * Clear the alarm if it goes high
	 */
	if( sysHealthy != lastSystemHealthy )
	{
		lastSystemHealthy = sysHealthy;
		if( sysHealthy == 0 )
		{
			/* Set the alarm if not already set*/
			if( sysHealthyAlarmSet == LU_FALSE )
			{
				SysAlarmSetLogEvent(LU_TRUE,
									SYS_ALARM_SEVERITY_CRITICAL,
									SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
									SYSALC_STATUS_SYSTEM_HEALTH,
									0
								   );
				sysHealthyAlarmSet = LU_TRUE;
			}
		}
		else
		{
			/* Clear the alarm if already set */
			if( sysHealthyAlarmSet == LU_TRUE )
			{
				SysAlarmSetLogEvent(LU_FALSE,
									SYS_ALARM_SEVERITY_CRITICAL,
									SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
									SYSALC_STATUS_SYSTEM_HEALTH,
									0
								   );
				sysHealthyAlarmSet = LU_FALSE;
			}
		}
	}

}


/*
 *********************** End of file ******************************************
 */
