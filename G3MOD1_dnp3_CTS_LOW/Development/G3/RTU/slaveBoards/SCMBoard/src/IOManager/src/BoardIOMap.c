/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{

/*********************************************************************************************************/
						/* NEW SCM IOMap */
/*********************************************************************************************************/

/********************************** CAN TX and RX Pins ***************************************************/

	IOM_GPIO_PERIPH(IO_ID_CAN1_RX, PINSEL_PORT_0, PINSEL_PIN_21, FUNC_CAN_1_RX, 0)

	IOM_GPIO_PERIPH(IO_ID_CAN1_TX, PINSEL_PORT_0, PINSEL_PIN_22, FUNC_CAN_1_TX, 0)

/*********************************************************************************************************/

/********************************** UART-3/Debug Port ****************************************************/

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX, PINSEL_PORT_4, PINSEL_PIN_28, FUNC_UART_3_TX, 0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX, PINSEL_PORT_4, PINSEL_PIN_29,	FUNC_UART_3_TX, 0)

/*********************************************************************************************************/

/********************************** System Health Monitor ************************************************/

	IOM_GPIO_INPUT(IO_ID_SYS_HEALTHY,     IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_18, 0)

	IOM_GPIO_INPUT(IO_ID_WD_HANDSHAKE,    IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_19, 0)

	IOM_GPIO_OUTPUT(IO_ID_HWDOG_1HZ_KICK, IO_CH_NA,	PINSEL_PORT_2, PINSEL_PIN_12, 0)

/*********************************************************************************************************/

/********************************** Module Status LEDs ***************************************************/

	IOM_GPIO_OUTPUT(IO_ID_LED_CTRL_OK_GREEN,    IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_9, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT(IO_ID_LED_CTRL_OK_RED,      IO_CH_NA, PINSEL_PORT_0, PINSEL_PIN_3, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN,  IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_28, 0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED,    IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_29, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH,	IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_15,  GPIO_PM_OUTPUT_LOW)

/*********************************************************************************************************/

/************************************ Module ID Switch ***************************************************/

	IOM_GPIO_INPUT( IO_ID_MOD_SEL1, IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_0, (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT( IO_ID_MOD_SEL2, IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_1, (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT( IO_ID_MOD_SEL3, IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_4, (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT( IO_ID_MOD_SEL4,	IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_8, (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

/*********************************************************************************************************/

/********************************** Front Panel LED I2C/Power ********************************************/

	IOM_GPIO_PERIPH(IO_ID_LED_SCL, PINSEL_PORT_0,  PINSEL_PIN_28, FUNC_I2C_0_SCL, 	0)

	IOM_GPIO_PERIPH(IO_ID_LED_SDA, PINSEL_PORT_0,  PINSEL_PIN_27, FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_INPUT(IO_ID_F_P_I2C_P_E_CHECK,	IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_17, 0)

	IOM_GPIO_INPUT(IO_ID_LED_PWR_FAULT,     IO_CH_NA, PINSEL_PORT_0, PINSEL_PIN_4,  GPIO_PM_OUTPUT_HIGH)

	IOM_GPIO_OUTPUT(IO_ID_IP_LED_PWR_EN,    IO_CH_NA, PINSEL_PORT_0, PINSEL_PIN_5,  0)

/*********************************************************************************************************/

/********************************** Local/Remote Status **************************************************/

	IOM_GPIO_INPUT(IO_ID_LOCAL_LINE,  IO_CH_NA, PINSEL_PORT_1,  PINSEL_PIN_20, 0)

	IOM_GPIO_INPUT(IO_ID_REMOTE_LINE, IO_CH_NA, PINSEL_PORT_0,  PINSEL_PIN_10, 0)

/*********************************************************************************************************/

/********************************** Switch-A Motor Control Signals ***************************************/

	IOM_GPIO_OUTPUT(IO_ID_VMOTOR_MOSFET_EN,          IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_27, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT(IO_ID_SWITCHGEAR_CE,             IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_31, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT(IO_ID_OPEN_CLOSE_MOTOR_JOG_EN,   IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_30, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT(IO_ID_BRIDGE_A_24V_A,    	     IO_CH_NA, PINSEL_PORT_0, PINSEL_PIN_11, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT(IO_ID_BRIDGE_A_24V_B,            IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_26, GPIO_PM_OUTPUT_HIGH)

	IOM_GPIO_OUTPUT(IO_ID_BRIDGE_B_24V_A,            IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_23, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT(IO_ID_BRIDGE_B_24V_B,            IO_CH_NA, PINSEL_PORT_3, PINSEL_PIN_26, GPIO_PM_OUTPUT_HIGH)

	IOM_GPIO_INPUT(IO_ID_CLOSE_CMD_EN,               IO_CH_NA, PINSEL_PORT_2, PINSEL_PIN_9,   GPIO_PM_OUTPUT_HIGH)

	IOM_GPIO_INPUT(IO_ID_OPEN_CMD_EN,                IO_CH_NA, PINSEL_PORT_2, PINSEL_PIN_8,   GPIO_PM_OUTPUT_HIGH)

	IOM_GPIO_INPUT(IO_ID_RELAY_FB_BRIDGE_A_24V_B_0V, IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_21,   GPIO_PM_OUTPUT_HIGH)

	IOM_GPIO_INPUT(IO_ID_RELAY_FB_BRIDGE_B_24V_A_0V, IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_22,   GPIO_PM_OUTPUT_HIGH)

/*********************************************************************************************************/

/********************************** AUX Output Signals ***************************************************/

	IOM_GPIO_OUTPUT(IO_ID_AUX_OP_1_LK_CMD,         IO_CH_NA, PINSEL_PORT_2, PINSEL_PIN_0, GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_INPUT(IO_ID_RELAY_FB_OP_1_LK_CLOSED,  IO_CH_NA, PINSEL_PORT_2, PINSEL_PIN_2, GPIO_PM_OUTPUT_HIGH)

/*********************************************************************************************************/

/********************************** Switch-A Inputs ******************************************************/

	IOM_GPIO_INPUT(IO_ID_SW_OPEN_INPUT,   SCM_MK2_CH_DINPUT_SWITCH_OPEN,  PINSEL_PORT_0,  PINSEL_PIN_15, (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_CLOSED_INPUT, SCM_MK2_CH_DINPUT_SWITCH_CLOSED,PINSEL_PORT_0,  PINSEL_PIN_16, (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_1, SCM_MK2_CH_DINPUT_INPUT_1,      PINSEL_PORT_2,  PINSEL_PIN_1,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_2,	SCM_MK2_CH_DINPUT_INPUT_2,	    PINSEL_PORT_2,  PINSEL_PIN_3,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_3,	SCM_MK2_CH_DINPUT_INPUT_3, 	    PINSEL_PORT_2,  PINSEL_PIN_4,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_4, SCM_MK2_CH_DINPUT_INPUT_4,      PINSEL_PORT_2,  PINSEL_PIN_6,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_5, SCM_MK2_CH_DINPUT_INPUT_5,      PINSEL_PORT_2,  PINSEL_PIN_7,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_6_ACT_LIM_OPEN, SCM_MK2_CH_DINPUT_INPUT_6_ACT_LIM_OPEN,      PINSEL_PORT_2,  PINSEL_PIN_8,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_INPUT(IO_ID_SW_INDICATION_7_ACT_LIM_CLOSE, SCM_MK2_CH_DINPUT_INPUT_7_ACT_LIM_CLOSE,      PINSEL_PORT_2,  PINSEL_PIN_9,  (GPIO_PM_EXT_EQUIP | GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))


/*********************************************************************************************************/

/********************************** ADC - Analogue Measurements ******************************************/

	IOM_PERIPH_AIN( IO_ID_VDD_SUPPLY_MEASURE, SCM_MK2_CH_AINPUT_VDD_SUPPLY_MEASURE, PINSEL_PORT_0,  PINSEL_PIN_23,  FUNC_AD0_CH_0)

	IOM_PERIPH_AIN( IO_ID_VLOGIC_MEASURE,     SCM_MK2_CH_AINPUT_VLOGIC_MEASURE,     PINSEL_PORT_0,  PINSEL_PIN_24,  FUNC_AD0_CH_1)

	IOM_PERIPH_AIN( IO_ID_VMOTOR_MEASURE,     SCM_MK2_CH_AINPUT_VMOTOR_MEASURE,     PINSEL_PORT_0,  PINSEL_PIN_25,  FUNC_AD0_CH_2)

	IOM_PERIPH_AIN( IO_ID_VREF_MEASURE,       SCM_MK2_CH_VREF_MEASURE,              PINSEL_PORT_0,  PINSEL_PIN_26,  FUNC_AD0_CH_3)

/*********************************************************************************************************/

/********************************** Switch Status LEDs *************************************************/

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INPUT_SWITCH_OPEN,     IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_7, GPIO_PM_OUTPUT_LOW)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INPUT_SWITCH_CLOSED,   IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_6, GPIO_PM_OUTPUT_LOW)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_OUTPUT_SWITCH_OPENING, IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_0, GPIO_PM_OUTPUT_LOW)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_OUTPUT_SWITCH_CLOSING, IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_1, GPIO_PM_OUTPUT_LOW)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_1,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_1, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_2,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_2, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_3,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_3, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_4,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_4, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_5,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_5, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_6,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_0_0, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_INDICATION_7, 	     IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_4, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_MOTOR_OUTPUT,          IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_2, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_LED_SW_OUTPUT_1, 	         IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_3, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT( IO_ID_COL1_GREEN, 		             IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_5, 0)

	IOM_I2CEXP_OUTPUT( IO_ID_COL2_RED, 		                 IO_CH_NA, IO_BUS_I2C_0, 0x00, I2C_EXP_IOPIN_1_6, GPIO_PM_OUTPUT_LOW)

	IOM_I2CEXP_OUTPUT( IO_ID_F_P_I2C_P_E_1_CHECK, 		     IO_CH_NA, IO_BUS_I2C_0, 0x05, I2C_EXP_IOPIN_1_7, 0)


/*********************************************************************************************************/

/********************************** Temperature Sensor ***************************************************/

	IOM_I2CTEMP_AIN( IO_ID_TEMP_MEASURE, SCM_MK2_CH_AINPUT_TEMPERATURE, IO_BUS_I2C_0, 0x01, 0)

	IOM_GPIO_INPUT(IO_ID_TEMP_SEN_ALERT, IO_CH_NA, PINSEL_PORT_1, PINSEL_PIN_16,  0)

/*********************************************************************************************************/

/********************************** ID/Application NVRAM I2C ********************************************/

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK, PINSEL_PORT_0, PINSEL_PIN_20, FUNC_I2C_1_SCL, GPIO_PM_BUS_400KHZ)

	IOM_GPIO_PERIPH(IO_ID_I2C1_SDA, PINSEL_PORT_0, PINSEL_PIN_19,FUNC_I2C_1_SDA, 0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, IO_CH_NA, PINSEL_PORT_0,  PINSEL_PIN_18, 0)

	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM, IO_BUS_I2C_1, 0x14, IO_ID_LAST)

	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM, IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)

/********************************************************************************************************/

/********************************** ID/Application NVRAM I2C ********************************************/

	IOM_VIRT_AI( IO_ID_VIRT_ON_ELAPSED_TIME,     	 	SCM_MK2_CH_AINPUT_ON_ELAPSED_TIME)

/********************************************************************************************************/


	/* End of table marker */
	IOM_LAST
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
