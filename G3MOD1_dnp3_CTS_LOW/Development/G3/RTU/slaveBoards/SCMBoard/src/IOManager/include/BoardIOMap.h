/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX (IO_ID_LAST)

// NOTE - Comment out until F board released.
//#define SCM_V_XXE		1

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Board Specific IO ID's */
typedef enum
{
/*********************************************************************************************************/
						/* NEW SCM IOMap */
/*********************************************************************************************************/

/********************************** CAN TX and RX Pins ***************************************************/

	IO_ID_CAN1_RX,

	IO_ID_CAN1_TX,

/*********************************************************************************************************/

/********************************** UART-3/Debug Port ****************************************************/

	IO_ID_UART_DEBUG_TX,

	IO_ID_UART_DEBUG_RX,

/*********************************************************************************************************/

/********************************** System Health Monitor ************************************************/

	IO_ID_SYS_HEALTHY,

	IO_ID_WD_HANDSHAKE,

	IO_ID_HWDOG_1HZ_KICK,

/*********************************************************************************************************/

/********************************** Module Status LEDs ***************************************************/

	IO_ID_LED_CTRL_OK_GREEN,

	IO_ID_LED_CTRL_OK_RED,

	IO_ID_LED_CTRL_CAN_GREEN,

	IO_ID_LED_CTRL_CAN_RED,

	IO_ID_LED_CTRL_EXTINGUISH,

/*********************************************************************************************************/

/************************************ Module ID Switch ***************************************************/

	 IO_ID_MOD_SEL1,

	 IO_ID_MOD_SEL2,

	 IO_ID_MOD_SEL3,

	 IO_ID_MOD_SEL4,

/*********************************************************************************************************/

/********************************** Front Panel LED I2C/Power ********************************************/

	IO_ID_LED_SCL,

	IO_ID_LED_SDA,

	IO_ID_F_P_I2C_P_E_CHECK,

	IO_ID_LED_PWR_FAULT,

	IO_ID_IP_LED_PWR_EN,

/*********************************************************************************************************/

/********************************** Local/Remote Status **************************************************/

	IO_ID_LOCAL_LINE,

	IO_ID_REMOTE_LINE,

/*********************************************************************************************************/

/********************************** Switch-A Motor Control Signals ***************************************/

	IO_ID_VMOTOR_MOSFET_EN,

	IO_ID_SWITCHGEAR_CE,

	IO_ID_OPEN_CLOSE_MOTOR_JOG_EN,

	IO_ID_BRIDGE_A_24V_A,

	IO_ID_BRIDGE_A_24V_B,

	IO_ID_BRIDGE_B_24V_A,

	IO_ID_BRIDGE_B_24V_B,

	IO_ID_CLOSE_CMD_EN,

	IO_ID_OPEN_CMD_EN,

	IO_ID_RELAY_FB_BRIDGE_A_24V_B_0V,

	IO_ID_RELAY_FB_BRIDGE_B_24V_A_0V,

/*********************************************************************************************************/

/********************************** AUX Output Signals ***************************************************/

	IO_ID_AUX_OP_1_LK_CMD,

	IO_ID_RELAY_FB_OP_1_LK_CLOSED,

/*********************************************************************************************************/

/********************************** Switch-A Inputs ******************************************************/

	IO_ID_SW_OPEN_INPUT,

	IO_ID_SW_CLOSED_INPUT,

	IO_ID_SW_INDICATION_1,

	IO_ID_SW_INDICATION_2,

	IO_ID_SW_INDICATION_3,

	IO_ID_SW_INDICATION_4,

	IO_ID_SW_INDICATION_5,

	IO_ID_SW_INDICATION_6_ACT_LIM_OPEN,

	IO_ID_SW_INDICATION_7_ACT_LIM_CLOSE,

/*********************************************************************************************************/

/********************************** ADC - Analogue Measurements ******************************************/

	 IO_ID_VDD_SUPPLY_MEASURE,

	 IO_ID_VLOGIC_MEASURE,

	 IO_ID_VMOTOR_MEASURE,

	 IO_ID_VREF_MEASURE,

/*********************************************************************************************************/

/********************************** Switch-A Status LEDs *************************************************/

	IO_ID_LED_SW_INPUT_SWITCH_OPEN,

	IO_ID_LED_SW_INPUT_SWITCH_CLOSED,

	IO_ID_LED_SW_OUTPUT_SWITCH_OPENING,

	IO_ID_LED_SW_OUTPUT_SWITCH_CLOSING,

	IO_ID_LED_SW_INDICATION_1,

	IO_ID_LED_SW_INDICATION_2,

	IO_ID_LED_SW_INDICATION_3,

	IO_ID_LED_SW_INDICATION_4,

	IO_ID_LED_SW_INDICATION_5,

	IO_ID_LED_SW_INDICATION_6,

	IO_ID_LED_SW_INDICATION_7,

	IO_ID_LED_SW_MOTOR_OUTPUT,

	IO_ID_LED_SW_OUTPUT_1,

	IO_ID_COL1_GREEN,

	IO_ID_COL2_RED,

	IO_ID_F_P_I2C_P_E_1_CHECK,


/*********************************************************************************************************/

/********************************** Temperature Sensor ***************************************************/

	IO_ID_TEMP_MEASURE,

	IO_ID_TEMP_SEN_ALERT,

/*********************************************************************************************************/

/********************************** ID/Application NVRAM I2C ********************************************/

	IO_ID_I2C1_CLK,

	IO_ID_I2C1_SDA,

	IO_ID_APPRAM_WEN,

	IO_ID_IDENTITY_NVRAM,

	IO_ID_APPLICATION_NVRAM,

/********************************************************************************************************/

/********************************** ID/Application NVRAM I2C ********************************************/

	IO_ID_VIRT_ON_ELAPSED_TIME,

/********************************************************************************************************/


	/* End of table marker */
   IO_ID_LAST
} IO_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
