/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"


/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_SWITCH_OPEN, 			IO_ID_SW_OPEN_INPUT),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_SWITCH_CLOSED, 			IO_ID_SW_CLOSED_INPUT),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_1, 				IO_ID_SW_INDICATION_1),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_2, 				IO_ID_SW_INDICATION_2),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_3, 				IO_ID_SW_INDICATION_3),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_4, 		    	IO_ID_SW_INDICATION_4),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_5, 				IO_ID_SW_INDICATION_5),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_6_ACT_LIM_OPEN, 	IO_ID_SW_INDICATION_6_ACT_LIM_OPEN),
	IOM_IOCHAN_DI(SCM_MK2_CH_DINPUT_INPUT_7_ACT_LIM_CLOSE, 	IO_ID_SW_INDICATION_7_ACT_LIM_CLOSE),


	IOM_IOCHAN_DI(0, 						            IO_ID_NA)
};

IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),

	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};


IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AI(SCM_MK2_CH_AINPUT_ON_ELAPSED_TIME,    IO_ID_VIRT_ON_ELAPSED_TIME, \
			      CAL_ID_NA,    			IO_AI_EVENTRATE_MS),
    IOM_IOCHAN_AI(SCM_MK2_CH_AINPUT_TEMPERATURE, 	    IO_ID_TEMP_MEASURE, \
   			      CAL_ID_NA,		   	    IO_AI_EVENTRATE_MS),
   	IOM_IOCHAN_AI(SCM_MK2_CH_AINPUT_VDD_SUPPLY_MEASURE, IO_ID_VDD_SUPPLY_MEASURE, \
   				  CAL_ID_VDD_SUPPLY_VOLTAGE,		   	    IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(SCM_MK2_CH_AINPUT_VLOGIC_MEASURE, 	IO_ID_VLOGIC_MEASURE, \
				  CAL_ID_VLOGIC_VOLTAGE,		   	    IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(SCM_MK2_CH_AINPUT_VMOTOR_MEASURE, 	IO_ID_VMOTOR_MEASURE, \
				  CAL_ID_VMOTOR_VOLTAGE,		   	    IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(SCM_MK2_CH_VREF_MEASURE, 				IO_ID_VREF_MEASURE, \
				  CAL_ID_VREF_VOLTAGE,		   	    IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, \
			      CAL_ID_NA,    0)
};

IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA),

	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
			      CAL_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
