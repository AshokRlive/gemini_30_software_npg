/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "systemStatus.h"
#include "systemTime.h"

#include "NVRam.h"
#include "NVRAMDef.h"
#include "IOManager.h"
#include "IOManagerIOUpdate.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "IOManagerAnalogOutIOUpdate.h"
#include "BoardIO.h"
#include "BoardIOMap.h"

#include "HMIController.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR HMIPeriodicManager(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				 0, 				0 				          }, // Run all the time

	{CANFramingTick, 			 0, 				CAN_TICK_MS		          }, // Run every 10ms

	{IOManagerIORunUpdateGPIO, 	 0, 				IOMAN_UPDATE_GPIO_MS      }, // run every 10ms
	{IOManagerIntAnalogIOUpdate, 0, 				IOMAN_UPDATE_INAI_MS      }, // run every 15 ms
	{IOManagerAnalogOutIOUpdate, 0, 				IOMAN_UPDATE_AO_MS        },
	{IOManagerIOAIEventUpdate,   0,                 IOMAN_UPDATE_INAI_EVENT_MS}, //run every 50 ms

	{NVRamUpdateTick, 		     0, 				NVRAM_UPDATE_TICK_MS 	  }, // run every 100ms

	{HMIControllerTick,   		 0,                 HMICON_TICK_RATE_MS       }, //run every 50 ms
	{HMIConSoundTick,   		 0,                 HMICON_SOUND_TICK_RATE_MS }, //run every 10 ms

	{HMIPeriodicManager,         0,                 200                       }, // run every 1000ms

	{(SMThreadRun)0L, 	    	 0, 				0   			          }  // end of table
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{
	SB_ERROR       RetVal;
	NVRAMOptHMIStr *nvramOptHMIBlkPtr;
	NVRAMInfoStr   *nvramInfoBlkPtr;
	lu_uint8_t     displayType;
	lu_uint8_t     contrastDigipotSet;

	/* Initialise the HMI */
	HMIControllerInit();

	displayType        = DISPLAY_TYPE_UNKNOWN;
	contrastDigipotSet = 0xff;

	/* Read NVRAM to determine contrast control setting */
	RetVal = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_OPTS, (lu_uint8_t **)&nvramOptHMIBlkPtr);
	if (RetVal != SB_ERROR_NONE)
	{
		RetVal = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_OPTS, (lu_uint8_t **)&nvramOptHMIBlkPtr);

		if (RetVal == SB_ERROR_NONE)
		{
			displayType        = nvramOptHMIBlkPtr->displayType;
			contrastDigipotSet = nvramOptHMIBlkPtr->contrastDigipotSet;
		}
		else
		{
			/* Read Board Identity NVRAM */
			RetVal = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_INFO, (lu_uint8_t **)&nvramInfoBlkPtr);

			if (RetVal != SB_ERROR_NONE)
			{
				/* Read Backup Board Identity from APP NVRAM */
				RetVal = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_INFO, (lu_uint8_t **)&nvramInfoBlkPtr);
			}

			/* Work-around for failed to program option block memory with programming tool! */
			if (RetVal == SB_ERROR_NONE)
			{
				if (!strcmp(nvramInfoBlkPtr->assemblyNo, "AUT0000037") &&
				    !strcmp(nvramInfoBlkPtr->assemblyRev, "03")
				    )
				{
					displayType        = DISPLAY_TYPE_ALT_SUP_POWER_TIP_2KDSLC_2004D_GSW_WT;
					contrastDigipotSet = 0x79;
				}
			}
		}

		switch (displayType)
		{
		case DISPLAY_TYPE_ALT_SUP_POWER_TIP_2KDSLC_2004D_GSW_WT:
		default:
			/* Set contrast control */
			IOManagerSetValue(IO_ID_LCD_VO, contrastDigipotSet);
			break;

		case DISPLAY_TYPE_UNKNOWN:
			break;
		}
	}
	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR HMIPeriodicManager(void)
{
	SB_ERROR retError;
	MODULE_BOARD_ERROR  moduleError;
	static MODULE_BOARD_ERROR oldModuleError = MODULE_BOARD_ERROR_TSYNCH;

	/* Set virtual elapsed time value */
	retError = IOManagerSetValue(IO_ID_VIRT_ON_ELAPSED_TIME, STGetRunningTimeSecs());
	
    if(SSGetBStatus() == MODULE_BOARD_STATUS_STARTING)
    {
    	/* Allow the IOManager to report events */
		IOManagerStartEventing();

		/* Start HMI subsystem */
		HMIControllerStart();

        SSSetBStatus(MODULE_BOARD_STATUS_ON_LINE);
    }

    /* Update status LED's */
	moduleError = SSGetBError();

	if (oldModuleError != moduleError)
	{
		oldModuleError = moduleError;

		if (moduleError & MODULE_BOARD_ERROR_ALARM_CRITICAL)
		{
			/* Critical - Fast Flash Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 250);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_ERROR)
		{
			/* Error - Slow Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 1000);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_WARNING)
		{
			/* Warning - Fast Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 250);
		} else
		{
			/* Normal - Slow Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 1000);
		}
	}

    return SB_ERROR_NONE;
}

/*
 *********************** End of file ******************************************
 */
