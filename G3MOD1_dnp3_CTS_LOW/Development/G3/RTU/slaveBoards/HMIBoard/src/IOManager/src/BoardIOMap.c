/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

#define HMI_V2	1

#ifdef HMI_V2
/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_TX, 	0)

	IOM_GPIO_PERIPH(IO_ID_DIGITAL_POT_SCK,		 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDO,		 PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDI,		 PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_DATA, 		     PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,   0)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
					PINSEL_PORT_0,  PINSEL_PIN_18,  0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_LEFT_1, 		IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_7,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_LEFT_2, 		IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_6,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_RIGHT_1, 		IO_CH_NA, \
			PINSEL_PORT_3,  PINSEL_PIN_26 ,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_RIGHT_2, 		IO_CH_NA, \
			PINSEL_PORT_3,  PINSEL_PIN_25,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_LOCAL, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_17,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_REMOTE, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_15,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_OFF, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_16,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LCD_EN, 			 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_15,  GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_INOUT( IO_ID_LCD_DB0, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_16,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB1, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_17,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB2, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_18,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB3, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_19,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB4, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_20,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB5, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_21,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB6, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_22,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB7, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_23,  0)
	IOM_GPIO_OUTPUT( IO_ID_LCD_RW, 			 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_24,  0)
	IOM_GPIO_OUTPUT( IO_ID_LCD_RS, 			 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_25,  0)

	IOM_GPIO_OUTPUT( IO_ID_PB_COMMON, 				IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_4,  0)

	IOM_VIRT_DI(IO_ID_PB_SOFT_LEFT_1, 			IO_CH_NA)
	IOM_VIRT_DI(IO_ID_PB_SOFT_LEFT_2, 			IO_CH_NA)
	IOM_VIRT_DI(IO_ID_PB_SOFT_RIGHT_1, 			IO_CH_NA)
	IOM_VIRT_DI(IO_ID_PB_SOFT_RIGHT_2, 			IO_CH_NA)

	IOM_GPIO_INPUT( IO_ID_PB_NAV_UP,     			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_14,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_DOWN, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_0, GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_LEFT, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_1,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_RIGHT, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_10,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_OK, 					IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_4, GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_MENU, 					IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_8,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_ESC, 					IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_5,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_OPEN, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_4,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_CLOSE, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_3,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_ACTIVATE, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_9,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_OP_MODE, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_2,  GPIO_PM_PULL_DOWN)

	IOM_PERIPH_AIN( IO_ID_TEMP_MEASURE, 			HMI_CH_AINPUT_TEMP_SENSOR, \
			PINSEL_PORT_0,  PINSEL_PIN_24,  FUNC_AD0_CH_1)
	IOM_PERIPH_AIN( IO_ID_5V_MEAS, 					HMI_CH_AINPUT_POWER_SUPPLY_5V, \
			PINSEL_PORT_1,  PINSEL_PIN_30,  FUNC_AD0_CH_4)
	IOM_PERIPH_AIN( IO_ID_3_3V_MEAS, 				HMI_CH_AINPUT_POWER_SUPPLY_3_3V, \
			PINSEL_PORT_1,  PINSEL_PIN_31,  FUNC_AD0_CH_5)

	IOM_PERIPH_AOUT( IO_ID_DAC_OUT, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_26,  FUNC_DAC_A)

	IOM_GPIO_OUTPUT( IO_ID_DIGITAL_POT_SHDN, 	  	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_5,   0)
	IOM_GPIO_OUTPUT( IO_ID_DIGITAL_POT_CS1, 	  	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_6,   0)

	IOM_GPIO_OUTPUT( IO_ID_AMP_SHDN, 	  			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_9,   GPIO_PM_OUTPUT_LOW)

	/* I2C temp sensor */

	/* I2C IO Expanders */

	/* SPI Digi pot devices */
	IOM_DIGPOT_OUT(IO_ID_LCD_VO, IO_CH_NA, \
			IO_BUS_SPI_SSPI_1, 0, IO_ID_DIGITAL_POT_CS1)

	/* ADE7854 ADC I2C + SPI attached */


	/* SPI attached ADC (MCP3204) */

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IOM_VIRT_AI( IO_ID_VIRT_ON_ELAPSED_TIME,     	 	HMI_CH_AINPUT_ON_ELAPSED_TIME)


	/* End of table marker */
	IOM_LAST
};

#else
/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_TX, 	0)

	IOM_GPIO_PERIPH(IO_ID_DIGITAL_POT_SCK,		 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDO,		 PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDI,		 PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_DATA, 		     PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,   0)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
					PINSEL_PORT_0,  PINSEL_PIN_18,  0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_LEFT_1, 		IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_7,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_LEFT_2, 		IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_6,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_RIGHT_1, 		IO_CH_NA, \
			PINSEL_PORT_3,  PINSEL_PIN_26 ,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_SOFT_RIGHT_2, 		IO_CH_NA, \
			PINSEL_PORT_3,  PINSEL_PIN_25,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_LOCAL, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_17,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_REMOTE, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_15,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_OFF, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_16,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LCD_EN, 			 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_15,  GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_INOUT( IO_ID_LCD_DB0, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_16,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB1, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_17,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB2, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_18,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB3, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_19,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB4, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_20,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB5, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_21,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB6, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_22,  0)
	IOM_GPIO_INOUT( IO_ID_LCD_DB7, 			     	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_23,  0)
	IOM_GPIO_OUTPUT( IO_ID_LCD_RW, 			 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_24,  0)
	IOM_GPIO_OUTPUT( IO_ID_LCD_RS, 			 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_25,  0)

	IOM_GPIO_OUTPUT( IO_ID_PB_COMMON, 				IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_3,  0)

	IOM_GPIO_INPUT( IO_ID_PB_SOFT_LEFT_1, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_5,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SOFT_LEFT_2, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_4,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SOFT_RIGHT_1, 			IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_2,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SOFT_RIGHT_2, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_0,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_UP,     			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_9,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_DOWN, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_14, GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_LEFT, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_4,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_NAV_RIGHT, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_8,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_OK, 					IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_10, GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_MENU, 					IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_1,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_ESC, 					IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_3,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_OPEN, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_2,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_CLOSE, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_1,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_ACTIVATE, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_4,  GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_PB_SW_OP_MODE, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_0,  GPIO_PM_PULL_DOWN)

	IOM_PERIPH_AIN( IO_ID_TEMP_MEASURE, 			HMI_CH_AINPUT_TEMP_SENSOR, \
			PINSEL_PORT_0,  PINSEL_PIN_24,  FUNC_AD0_CH_1)
	IOM_PERIPH_AIN( IO_ID_5V_MEAS, 					HMI_CH_AINPUT_POWER_SUPPLY_5V, \
			PINSEL_PORT_1,  PINSEL_PIN_30,  FUNC_AD0_CH_4)
	IOM_PERIPH_AIN( IO_ID_3_3V_MEAS, 				HMI_CH_AINPUT_POWER_SUPPLY_3_3V, \
			PINSEL_PORT_1,  PINSEL_PIN_31,  FUNC_AD0_CH_5)

	IOM_PERIPH_AOUT( IO_ID_DAC_OUT, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_26,  FUNC_DAC_A)

	IOM_GPIO_OUTPUT( IO_ID_DIGITAL_POT_SHDN, 	  	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_5,   0)
	IOM_GPIO_OUTPUT( IO_ID_DIGITAL_POT_CS1, 	  	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_6,   0)

	IOM_GPIO_OUTPUT( IO_ID_AMP_SHDN, 	  			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_9,   GPIO_PM_OUTPUT_LOW)

	/* I2C temp sensor */

	/* I2C IO Expanders */

	/* SPI Digi pot devices */
	IOM_DIGPOT_OUT(IO_ID_LCD_VO, IO_CH_NA, \
			IO_BUS_SPI_SSPI_1, 0, IO_ID_DIGITAL_POT_CS1)

	/* ADE7854 ADC I2C + SPI attached */


	/* SPI attached ADC (MCP3204) */

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IOM_VIRT_AI( IO_ID_VIRT_ON_ELAPSED_TIME,     	 	HMI_CH_AINPUT_ON_ELAPSED_TIME)


	/* End of table marker */
	IOM_LAST
};

#endif


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
