/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX			(IO_ID_LAST)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IO_ID_CAN1_RX	                       =0 ,
	IO_ID_CAN1_TX	                          ,

	IO_ID_UART_DEBUG_TX	                      ,
	IO_ID_UART_DEBUG_RX	                      ,

	IO_ID_DIGITAL_POT_SCK                     ,
	IO_IO_DIGITAL_POT_SDO                     ,
	IO_IO_DIGITAL_POT_SDI                     ,

	IO_ID_I2C1_CLK                            ,
	IO_ID_I2C1_DATA                           ,

	/*********************************/
	/* Physically connected IO Pins  */
	/* (Some Virt for NXP dev board) */
	/* (But will be physical on PSM) */
	/*********************************/
	IO_ID_FACTORY                             ,

	IO_ID_HWDOG_1HZ_KICK                      ,

	IO_ID_APPRAM_WEN                          ,

	IO_ID_LED_CTRL_OK_GREEN                   ,
	IO_ID_LED_CTRL_OK_RED                     ,
	IO_ID_LED_CTRL_CAN_GREEN                  ,
	IO_ID_LED_CTRL_CAN_RED                    ,
	IO_ID_LED_CTRL_EXTINGUISH                 ,

	IO_ID_LED_SOFT_LEFT_1                     ,
	IO_ID_LED_SOFT_LEFT_2                     ,
	IO_ID_LED_SOFT_RIGHT_1                    ,
	IO_ID_LED_SOFT_RIGHT_2                    ,
	IO_ID_LED_LOCAL                           ,
	IO_ID_LED_REMOTE                          ,
	IO_ID_LED_OFF                             ,

	IO_ID_LCD_EN							  ,
	IO_ID_LCD_DB0							  ,
	IO_ID_LCD_DB1							  ,
	IO_ID_LCD_DB2							  ,
	IO_ID_LCD_DB3							  ,
	IO_ID_LCD_DB4							  ,
	IO_ID_LCD_DB5							  ,
	IO_ID_LCD_DB6							  ,
	IO_ID_LCD_DB7							  ,
	IO_ID_LCD_RW							  ,
	IO_ID_LCD_RS							  ,

	IO_ID_PB_COMMON							  ,

	IO_ID_PB_SOFT_LEFT_1                      ,
	IO_ID_PB_SOFT_LEFT_2                      ,
	IO_ID_PB_SOFT_RIGHT_1                     ,
	IO_ID_PB_SOFT_RIGHT_2                     ,
	IO_ID_PB_NAV_UP                           ,
	IO_ID_PB_NAV_DOWN                         ,
	IO_ID_PB_NAV_LEFT						  ,
	IO_ID_PB_NAV_RIGHT						  ,
	IO_ID_PB_OK                               ,
	IO_ID_PB_MENU                             ,
	IO_ID_PB_ESC                              ,
	IO_ID_PB_SW_OPEN                          ,
	IO_ID_PB_SW_CLOSE   			    	  ,
	IO_ID_PB_SW_ACTIVATE					  ,
	IO_ID_PB_SW_OP_MODE						  ,

	IO_ID_TEMP_MEASURE                        ,
	IO_ID_5V_MEAS                             ,
	IO_ID_3_3V_MEAS                           ,

	IO_ID_DAC_OUT							  ,

	IO_ID_DIGITAL_POT_SHDN                    ,
	IO_ID_DIGITAL_POT_CS1                     ,

	IO_ID_AMP_SHDN                            ,

	/* I2C Expanders */


	/* SPI Digi pots */
	IO_ID_LCD_VO                              ,

	/* ADE7854 ADC I2C + SPI attached */


	/* SPI ADC */


	/* NOVRAM */
	IO_ID_APPLICATION_NVRAM                   ,
	IO_ID_IDENTITY_NVRAM                      ,

	/******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IO_ID_VIRT_ON_ELAPSED_TIME                ,
	
	IO_ID_LAST
} IO_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
