/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"
#include "BoardBootloader.h"

#include "lpc17xx_pinsel.h"

#include "FactoryTestSPIDigiPot.h"
#include "FactoryTest.h"
#include "FactoryTestLCD.h"

#include "ATEFactoryTest.h"

#include "NVRAMDef.h"
#include "BootNvRam.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

LcdDriverInitStr lcdInit;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				    period(ms)        */
	{CANCDecode, 				 0, 					0 				      }, // Run all the time
	{ATEFactoryTestTick, 		 0, 					0			          }, // Run all the time

	{CANFramingTick, 			 0, 				    CAN_TICK_MS		      }, // Run every 10ms

	{BoardBootloaderTick,        0,                     BRD_BL_TICK_MS        },

	{(SMThreadRun)0L, 	    	 0, 					0   			      }  // end of table
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardBootloaderSetLcdPtr(LcdDriverInitStr *setLcdInitPtr);

void BoardStatusManagerExecutive(void)
{
	SB_ERROR retVal;
	TestSPIDigiPotWriteStr digiPotWriteParams;
	NVRAMOptHMIStr 	       *nvramOptHMIBlkPtr;
	NVRAMInfoStr           *nvramInfoBlkPtr;
	lu_uint8_t             displayType;
	lu_uint8_t             contrastDigipotSet;

	displayType        = DISPLAY_TYPE_UNKNOWN;
	contrastDigipotSet = 0xff;

	/* Enable ATE factory test cmd api */
	ATEFactoryTestInit();
	
	/* Setup params for contrast digipot */
	digiPotWriteParams.addr.sspChan = 1;
	digiPotWriteParams.addr.potChan = 0;
	digiPotWriteParams.addr.csChan  = 0;
	digiPotWriteParams.potVal       = contrastDigipotSet;

	/* Detect Display type and set default contrast value */
	retVal = BootNvRamGetIdentityOptsMemoryPtr((lu_uint8_t **)&nvramOptHMIBlkPtr);

	if (retVal != SB_ERROR_NONE)
	{
		/* Detect Display type and set default contrast value */
		retVal = BootNvRamGetApplicationOptsMemoryPtr((lu_uint8_t **)&nvramOptHMIBlkPtr);
	}

	if (retVal == SB_ERROR_NONE)
	{
		displayType        = nvramOptHMIBlkPtr->displayType;
		contrastDigipotSet = nvramOptHMIBlkPtr->contrastDigipotSet;
	}
	else
	{
		/* Read Board Identity NVRAM */
		retVal = BootNvRamGetIdentityInfoMemoryPtr((lu_uint8_t **)&nvramInfoBlkPtr);

		if (retVal != SB_ERROR_NONE)
		{
			retVal = BootNvRamGetApplicationInfoMemoryPtr((lu_uint8_t **)&nvramInfoBlkPtr);
		}

		/* Work-around for failed to program option block memory with programming tool! */
		if (retVal == SB_ERROR_NONE)
		{
			if (!strcmp(nvramInfoBlkPtr->assemblyNo, "AUT0000037") &&
				!strcmp(nvramInfoBlkPtr->assemblyRev, "03")
				)
			{
				displayType        = DISPLAY_TYPE_ALT_SUP_POWER_TIP_2KDSLC_2004D_GSW_WT;
				contrastDigipotSet = 0x79;
			}
		}
	}

	switch (displayType)
	{
	case DISPLAY_TYPE_ALT_SUP_POWER_TIP_2KDSLC_2004D_GSW_WT:
	default:
		digiPotWriteParams.potVal = contrastDigipotSet;
		break;

	case DISPLAY_TYPE_UNKNOWN:
		break;
	}

	retVal = FactoryTestSPIDigiPotWrite(&digiPotWriteParams);

	FactoryTestLCDInitStructure(&lcdInit);

	lcdInit.ioID[LCD_IO_ID_RW].port  = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_RW].pin   = PINSEL_PIN_24;

	lcdInit.ioID[LCD_IO_ID_EN].port  = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_EN].pin   = PINSEL_PIN_15;

	lcdInit.ioID[LCD_IO_ID_RS].port  = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_RS].pin   = PINSEL_PIN_25;

	lcdInit.ioID[LCD_IO_ID_DB0].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB0].pin  = PINSEL_PIN_16;

	lcdInit.ioID[LCD_IO_ID_DB1].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB1].pin  = PINSEL_PIN_17;

	lcdInit.ioID[LCD_IO_ID_DB2].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB2].pin  = PINSEL_PIN_18;

	lcdInit.ioID[LCD_IO_ID_DB3].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB3].pin  = PINSEL_PIN_19;

	lcdInit.ioID[LCD_IO_ID_DB4].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB4].pin  = PINSEL_PIN_20;

	lcdInit.ioID[LCD_IO_ID_DB5].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB5].pin  = PINSEL_PIN_21;

	lcdInit.ioID[LCD_IO_ID_DB6].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB6].pin  = PINSEL_PIN_22;

	lcdInit.ioID[LCD_IO_ID_DB7].port = PINSEL_PORT_1;
	lcdInit.ioID[LCD_IO_ID_DB7].pin  = PINSEL_PIN_23;

	lcdInit.lcdInitMode |= (LCD_INIT_MODE_CURSOR_ON | LCD_INIT_MODE_BLINK_ON);

	FactoryTestLCDInit(&lcdInit);

	FactoryTestLCDPutString(&lcdInit, "Initialising........", 0, 0);
	FactoryTestLCDPutString(&lcdInit, "....................", 0, 1);
	FactoryTestLCDPutString(&lcdInit, "....................", 0, 2);
	FactoryTestLCDPutString(&lcdInit, "...................", 0, 3);

	BoardBootloaderSetLcdPtr(&lcdInit);

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
