/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"

#include "lu_types.h"

#include "systemTime.h"
#include "systemStatus.h"
#include "Bootloader.h"
#include "BoardBootloader.h"
#include "ModuleProtocol.h"
#include "FactoryTest.h"
#include "FactoryTestLCD.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define STR_UP   "UP"
#define STR_DN   "DN"
#define STR_LT   "LT"
#define STR_RT   "RT"
#define STR_OK   "OK"
#define STR_MENU "MENU"

#define STR_ESC  "ESC"
#define STR_OPEN  "OP"
#define STR_CLOSE "CL"
#define STR_MODE  "MODE"
#define STR_ACT   "ACT"

#define BL_PSM_PORT_FACTORY							0
#define BL_PSM_PIN_FACTORY							(1 <<1)

#define BL_HMI_PORT_LED_CTRL_EXTINGUISH				0
#define BL_HMI_PIN_LED_CTRL_EXTINGUISH				(1 << 0)

#define BL_HMI_PORT_LED_CTRL_OK_GREEN				1
#define BL_HMI_PIN_LED_CTRL_OK_GREEN				(1 << 26)

#define BL_HMI_PORT_LED_CTRL_OK_RED					1
#define BL_HMI_PIN_LED_CTRL_OK_RED					(1 << 27)

#define BL_HMI_PORT_LED_CTRL_CAN_GREEN				1
#define BL_HMI_PIN_LED_CTRL_CAN_GREEN				(1 << 28)

#define BL_HMI_PORT_LED_CTRL_CAN_RED				1
#define BL_HMI_PIN_LED_CTRL_CAN_RED					(1 << 29)


#define BL_HMI_PORT_PB_COMMON						1
#define BL_HMI_PIN_PB_COMMON 						(1 << 4)

#define BL_HMI_PORT_PB_NAV_UP 						1
#define BL_HMI_PIN_PB_NAV_UP 						(1 << 14)
#define BL_HMI_PINCFG_PB_NAV_UP 					(14)

#define BL_HMI_PORT_PB_NAV_DOWN						2
#define BL_HMI_PIN_PB_NAV_DOWN 						(1 << 0)
#define BL_HMI_PINCFG_PB_NAV_DOWN 					(0)

#define BL_HMI_PORT_PB_NAV_LEFT 					2
#define BL_HMI_PIN_PB_NAV_LEFT 						(1 << 1)
#define BL_HMI_PINCFG_PB_NAV_LEFT 					(1)

#define BL_HMI_PORT_PB_NAV_RIGHT 					1
#define BL_HMI_PIN_PB_NAV_RIGHT 					(1 << 10)
#define BL_HMI_PINCFG_PB_NAV_RIGHT 					(10)

#define BL_HMI_PORT_PB_OK 							0
#define BL_HMI_PIN_PB_OK 							(1 << 4)
#define BL_HMI_PINCFG_PB_OK 						(4)

#define BL_HMI_PORT_PB_MENU 						1
#define BL_HMI_PIN_PB_MENU 							(1 << 8)
#define BL_HMI_PINCFG_PB_MENU 						(8)

#define BL_HMI_PORT_PB_ESC 							2
#define BL_HMI_PIN_PB_ESC 							(1 << 5)
#define BL_HMI_PINCFG_PB_ESC 						(5)

#define BL_HMI_PORT_PB_SW_OPEN 						2
#define BL_HMI_PIN_PB_SW_OPEN 						(1 << 4)
#define BL_HMI_PINCFG_PB_SW_OPEN 					(4)

#define BL_HMI_PORT_PB_SW_CLOSE 					2
#define BL_HMI_PIN_PB_SW_CLOSE 						(1 << 3)
#define BL_HMI_PINCFG_PB_SW_CLOSE 					(3)

#define BL_HMI_PORT_PB_SW_ACTIVATE 					1
#define BL_HMI_PIN_PB_SW_ACTIVATE 					(1 << 9)
#define BL_HMI_PINCFG_PB_SW_ACTIVATE 				(9)

#define BL_HMI_PORT_PB_SW_OP_MODE 					2
#define BL_HMI_PIN_PB_SW_OP_MODE 					(1 << 2)
#define BL_HMI_PINCFG_PB_SW_OP_MODE 				(2)


#define BL_REQ_APP_START_TIMEOUT_MS					100 // Timeout to start app

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

void BoardBootloaderTestKeyPad(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

static lu_uint32_t startAppTimeout;
static lu_uint32_t autoStartAppTimeout;
static lu_uint32_t ledFlashTimeout;
static lu_bool_t   autoStartEnable = LU_FALSE;
static lu_bool_t   factoryMode = LU_FALSE;
static lu_bool_t   bootloaderEnable = LU_TRUE;

LcdDriverInitStr *lcdInitPtr = NULL;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardBootloaderSetLcdPtr(LcdDriverInitStr *setLcdInitPtr)
{
	lcdInitPtr = setLcdInitPtr;
}

void BoardBootloaderAutoAppStart(void)
{
	autoStartEnable     = LU_TRUE;
}

SB_ERROR BoardBootloaderPOST(void)
{
	lu_uint32_t         portValue;
	PINSEL_CFG_Type 	pinCfg;

	pinCfg.Funcnum = 0;
	pinCfg.Portnum = BL_PSM_PORT_FACTORY;
	pinCfg.Pinnum = BL_PSM_PIN_FACTORY;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_PULLUP;

	PINSEL_ConfigPin(&pinCfg);

	/* POST test is success... */
	GPIO_SetValue(BL_HMI_PORT_LED_CTRL_OK_GREEN, BL_HMI_PIN_LED_CTRL_OK_GREEN);

	/* Sample factory input pin here */
	portValue = GPIO_ReadValue(BL_PSM_PORT_FACTORY);
	if (!(portValue & BL_PSM_PIN_FACTORY))
	{
		autoStartAppTimeout = 0;
		autoStartEnable = LU_FALSE;
		factoryMode = LU_TRUE;
	}

	return SB_ERROR_NONE;
}

SB_ERROR BoardBootloaderTick(void)
{
	static lu_bool_t  ledState;

	if (bootloaderEnable == LU_TRUE)
	{
		if (BootloaderGetStartApplicationNow())
		{
			/* Count-up until ready to start App, allow time to send CAN message reply */
			autoStartAppTimeout = 0;
			startAppTimeout += BRD_BL_TICK_MS;
		}
		else
		{
			/* Count-up to autostart, to start charging battery!! */
			autoStartAppTimeout += BRD_BL_TICK_MS;
		}

		/* Start Application if timeout reached */
		if (startAppTimeout >= BL_REQ_APP_START_TIMEOUT_MS ||
			(autoStartEnable && autoStartAppTimeout >= BOOTLOADER_APP_START_TIMEOUT_MS))
		{
			/* Clear display before running main App */
			FactoryTestLCDClearDisplay(lcdInitPtr);

			/* Init for next trigger should there be no application available ?*/
			autoStartAppTimeout = 0;
			startAppTimeout = 0;
			autoStartEnable = LU_FALSE;

			/* Clear Green / Set red before launch app */
			GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_EXTINGUISH, BL_HMI_PIN_LED_CTRL_EXTINGUISH);
			GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_OK_GREEN, BL_HMI_PIN_LED_CTRL_OK_GREEN);
			GPIO_SetValue(BL_HMI_PORT_LED_CTRL_OK_RED, BL_HMI_PIN_LED_CTRL_OK_RED);

			/* Attempt to run Application */
			BootloaderStartCommand();

			/* Should never get here!! */
		}

		/* Check if forced to stay in bootloader */
		if (SSGetBootloaderMode() == LU_TRUE)
		{
			autoStartAppTimeout = 0;
			autoStartEnable = LU_FALSE;

			if (SSGetFactoryMode() == LU_TRUE)
			{
				factoryMode = LU_TRUE;
			}
		}

		ledFlashTimeout += BRD_BL_TICK_MS;

		/* Flash OK LED Red <-> Green while in bootloader */
		if (ledFlashTimeout >= BOOTLOADER_LED_FLASH_MS)
		{
			ledFlashTimeout = 0;

			/* Do not allow LED control when in factory test mode */
			if (factoryMode == LU_FALSE)
			{
				if (ledState)
				{
					/* Turn on RED */
					ledState = LU_FALSE;
					GPIO_SetValue(BL_HMI_PORT_LED_CTRL_EXTINGUISH, BL_HMI_PIN_LED_CTRL_EXTINGUISH);
					GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_OK_RED, BL_HMI_PIN_LED_CTRL_OK_RED);
					GPIO_SetValue(BL_HMI_PORT_LED_CTRL_OK_GREEN, BL_HMI_PIN_LED_CTRL_OK_GREEN);
				}
				else
				{
					/* Turn on Green */
					ledState = LU_TRUE;
					GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_EXTINGUISH, BL_HMI_PIN_LED_CTRL_EXTINGUISH);
					GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_OK_GREEN, BL_HMI_PIN_LED_CTRL_OK_GREEN);
					GPIO_SetValue(BL_HMI_PORT_LED_CTRL_OK_RED, BL_HMI_PIN_LED_CTRL_OK_RED);
				}

				BoardBootloaderTestKeyPad();
			}
		}
	}

	return SB_ERROR_NONE;
}

SB_ERROR BoardBootloaderDisable(void)
{
	bootloaderEnable = LU_FALSE;

	/* Disable CAN */

	/* Re-init all DDR / func code to gpio inputs, except UART3 */

	return SB_ERROR_NONE;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Board bootloader initialisation
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR BoardBootloaderInit(void)
{
	PINSEL_CFG_Type 		pinCfg;

	startAppTimeout     = 0;
	autoStartAppTimeout = 0;

	/* Configure GPIO pins */
	/* Set LED status... */

	/* Set as output */
	GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_EXTINGUISH, BL_HMI_PIN_LED_CTRL_EXTINGUISH);
	GPIO_SetDir(BL_HMI_PORT_LED_CTRL_EXTINGUISH, BL_HMI_PIN_LED_CTRL_EXTINGUISH, 1);

	/* Set as output */
	GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_OK_GREEN, BL_HMI_PIN_LED_CTRL_OK_GREEN);
	GPIO_SetDir(BL_HMI_PORT_LED_CTRL_OK_GREEN, BL_HMI_PIN_LED_CTRL_OK_GREEN, 1);

	/* Set as output */
	GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_OK_RED, BL_HMI_PIN_LED_CTRL_OK_RED);
	GPIO_SetDir(BL_HMI_PORT_LED_CTRL_OK_RED, BL_HMI_PIN_LED_CTRL_OK_RED, 1);

	/* Set as output */
	GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_CAN_GREEN, BL_HMI_PIN_LED_CTRL_CAN_GREEN);
	GPIO_SetDir(BL_HMI_PORT_LED_CTRL_CAN_GREEN, BL_HMI_PIN_LED_CTRL_CAN_GREEN, 1);

	/* Set as output */
	GPIO_ClearValue(BL_HMI_PORT_LED_CTRL_CAN_RED, BL_HMI_PIN_LED_CTRL_CAN_RED);
	GPIO_SetDir(BL_HMI_PORT_LED_CTRL_CAN_RED, BL_HMI_PIN_LED_CTRL_CAN_RED, 1);


	/* Set as output */
	GPIO_SetValue(BL_HMI_PORT_PB_COMMON, BL_HMI_PIN_PB_COMMON);
	GPIO_SetDir(BL_HMI_PORT_PB_COMMON, BL_HMI_PIN_PB_COMMON, 1);

	/* Set pin configuration */
	pinCfg.Funcnum = 0;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Pinmode = PINSEL_PINMODE_PULLDOWN;

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_NAV_UP;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_NAV_UP;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_NAV_DOWN;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_NAV_DOWN;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_NAV_LEFT;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_NAV_LEFT;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_NAV_RIGHT;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_NAV_RIGHT;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_OK;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_OK;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_MENU;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_MENU;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_ESC;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_ESC;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_SW_OPEN;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_SW_OPEN;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_SW_CLOSE;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_SW_CLOSE;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_SW_ACTIVATE;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_SW_ACTIVATE;
	PINSEL_ConfigPin(&pinCfg);

	/* Enable pull-down */
	pinCfg.Portnum = BL_HMI_PORT_PB_SW_OP_MODE;
	pinCfg.Pinnum  = BL_HMI_PINCFG_PB_SW_OP_MODE;
	PINSEL_ConfigPin(&pinCfg);

	return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Board bootloader keypad test
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void BoardBootloaderTestKeyPad(void)
{
	lu_char_t     line1[21];
	lu_char_t     line2[21];
	lu_uint32_t   portValue;
	static lu_uint16_t counter = 0;

	counter++;

	if (!(counter % 2))
	{
		strcpy(line1, "....................");
		strcpy(line2, line1);

		//	....................
		//	UP DN LT RT OK MENU
		//	ESC OP CL MODE ACT

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_NAV_UP);
		if (portValue & BL_HMI_PIN_PB_NAV_UP)
		{
			memcpy(&line1[0], STR_UP, strlen(STR_UP));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_NAV_DOWN);
		if (portValue & BL_HMI_PIN_PB_NAV_DOWN)
		{
			memcpy(&line1[3], STR_DN, strlen(STR_DN));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_NAV_LEFT);
		if (portValue & BL_HMI_PIN_PB_NAV_LEFT)
		{
			memcpy(&line1[6], STR_LT, strlen(STR_LT));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_NAV_RIGHT);
		if (portValue & BL_HMI_PIN_PB_NAV_RIGHT)
		{
			memcpy(&line1[9], STR_RT, strlen(STR_RT));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_OK);
		if (portValue & BL_HMI_PIN_PB_OK)
		{
			memcpy(&line1[12], STR_OK, strlen(STR_OK));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_MENU);
		if (portValue & BL_HMI_PIN_PB_MENU)
		{
			memcpy(&line1[15], STR_MENU, strlen(STR_MENU));
		}


		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_ESC);
		if (portValue & BL_HMI_PIN_PB_ESC)
		{
			memcpy(&line2[0], STR_ESC, strlen(STR_ESC));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_SW_OPEN);
		if (portValue & BL_HMI_PIN_PB_SW_OPEN)
		{
			memcpy(&line2[4], STR_OPEN, strlen(STR_OPEN));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_SW_CLOSE);
		if (portValue & BL_HMI_PIN_PB_SW_CLOSE)
		{
			memcpy(&line2[7], STR_CLOSE, strlen(STR_CLOSE));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_SW_OP_MODE);
		if (portValue & BL_HMI_PIN_PB_SW_OP_MODE)
		{
			memcpy(&line2[10], STR_MODE, strlen(STR_MODE));
		}

		portValue = GPIO_ReadValue(BL_HMI_PORT_PB_SW_ACTIVATE);
		if (portValue & BL_HMI_PIN_PB_SW_ACTIVATE)
		{
			memcpy(&line2[15], STR_ACT, strlen(STR_ACT));
		}

		FactoryTestLCDPutString(lcdInitPtr, line1, 0, 1);
		FactoryTestLCDPutString(lcdInitPtr, line2, 0, 2);
		FactoryTestLCDPutString(lcdInitPtr, "...................", 0, 3);

	}
}

/*
 *********************** End of file ******************************************
 */
