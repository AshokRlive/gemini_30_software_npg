/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*		\brief  Initialisation Module
*
*
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY
*
*   Date          EAR/Reason      Name            Details
*   ---------------------------------------------------------------------------
*   27/03/2014    FPM Module      Steve Walde    Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/


/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/
#include "fdm.h"
#include "globalVariables.h"


/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/

/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/



/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/

/*!
*******************************************************************************
* EXPORTED:
*   \name Public Functions
*******************************************************************************
* @{
*/


/*!
*******************************************************************************
* Function Name:
*	Initialise()
*
* Description:
*	\brief   Initialise.
*
*
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	\param na.
*
* Return Value:
*   \return T
*
* Example Usage:
*   \code
*
*   \endcode
*
* See Also:
*   \sa na
*
*******************************************************************************
*/
void Initialise(void)
{

	/*
	 * PIC GPIO Port assignments
	 *
	 * Port A	TRIS 0b11111111
	 * Bit 0	Analogue 	Input	L1 CT Analogue
	 * Bit 1	Analogue 	Input	L2 CT Analogue
	 * Bit 2	Analogue 	Input	L3 CT Analogue
	 * Bit 3	Analogue 	Input	Sum Analogue
	 * Bit 4	Digital		Input	Bootloader
	 * Bit 5	Digital		Input
	 * Bit 6	Digital		Output  CLKOUT
	 * Bit 7	Digital		Input
	 *
	 * Port B       TRIS 0b11111111
	 * Bit 0	Digital		Input   L1 Zero Crossing
	 * Bit 1	Digital		Input   L2 Zero Crossing
	 * Bit 2	Digital		Input   L3 Zero Crossing
	 * Bit 3	Digital		Ouput	l1 Interrupt/Scope trigger
	 * Bit 4	Digital		Ouput	L2 Interrupt/Scope trigger
	 * Bit 5	Digital		Input	L3 Interrupt/Scope trigger
	 * Bit 6	Digital		Ouput	Sum Interrupt/Scope trigger
	 * Bit 7	Digital		Input
	 *
	 * Port C	TRIS 0b11111111
	 * Bit 0	Digital		Input
	 * Bit 1	Digital		Input
	 * Bit 2	Digital		Ouput   SS
	 * Bit 3	Digital		Output	MSSP SCK
	 * Bit 4	Digital		Input	MSSP SDI
	 * Bit 5	Digital		Output  MSSP SDO
	 * Bit 6	Digital		Output  UART Tx
	 * Bit 7	Digital		Input   UART Rx
	 *
	 * Port E	TRIS 0b11111111
	 * Bit 0	Digital		Input
	 * Bit 1	Digital		Input
	 * Bit 2	Digital		Input
	 * Bit 3	Digital		Input   MCLR
	 */


        /*
	 * Initialise Oscillator - 32 mHz with 4 X PLL enabled
	 */
        OSCCONbits.IRCF     = 0x0F;   // Internal Oscilator 16 MHz
        OSCCONbits.SCS      = 0x00;   // Clk determined by FOSC<2:0>
        OSCTUNE             = 0x00;   // factory cal. frequency
        OSCCONbits.SPLLEN   = 0x1;    // Software PLL enable bit -
                                      // ignored as PLLEN set in config


        /*
	 * Set all ports to zero before setting the TRIS registers.
	 */
	PORTA = PORTB = PORTC = PORTE   = 0;
        LATA  = LATB  = LATC            = 0;

        // Set all GPIO as input by default
	TRISA = 0b11111111;
	TRISB = 0b11111111;
	TRISC = 0b11111111;
	TRISE = 0b11111111;

        /*
         * Analog select pins - default to all digital I/O
         */
        ANSELA = 0b00000000;
        ANSELB = 0b00000000;
        ANSELC = 0b00000000;

        /*
         * Set individual channels for analogue input
         */
        TRISA0 = 0x01;      // RA0 L1 CT Analgue Input
        TRISA1 = 0x01;      // RA1 L2 CT Analgue Input
        TRISA2 = 0x01;      // RA2 L3 CT Analgue Input
        TRISA3 = 0x01;      // RA3 Sum Analgue Input
        TRISA6 = 0x00;      // RA6 CLKOUT

        /*
         * Set individual channel for Slave select
         */
        TRISC2 = 0x00;      // Slave select output

        /*
         * Set individual channel for bootloader input
         */
        TRISA5 = 0x01;      // Bootloader indicator


        /*
         * Set individual channel for MSSP
         */
        SS = 1;             // SS is active low so set high to initialise

        /*
         * Set individual channel select for analogue input
         */
        ANSELA = 0x0F;      // RA0-3 Analogue inputs


        /*
         * Set individual channels for digital I/O
         */
        TRISB0 = 0x01;      // RB0 L1 Zero crossing digital input
        TRISB1 = 0x01;      // RB0 L2 Zero crossing digital input
        TRISB2 = 0x01;      // RB0 L3 Zero crossing digital input

        TRISB3 = 0x00;      // RB3 L1 Scope trigger digital output
        TRISB4 = 0x00;      // RB4 L2 Scope trigger digital output
        TRISB5 = 0x00;      // RB5 L3 Scope trigger digital output


        /*
         * Set individual channels for MSSP and UART
         */
        TRISC0 = 0x0;       // RB6 Sum Scope trigger digital output
        TRISC3 = 0x0;       // MSSP SCK
        TRISC4 = 0x1;       // MSSP SDI
        TRISC5 = 0x0;       // MSSP SDO
        TRISC6 = 0x0;       // UART Tx
        TRISC7 = 0x1;       // UART Rx


        /*
	 * Initialise Timer 1 as a free running timer
	 */
        TMR1L               = 0x00;
        TMR1H               = 0x00;
        T1GCON              = 0x00;

        T1CONbits.TMR1CS    = 0x00; // Timer1 clock source is instruction clock (FOSC/4)

        T1CONbits.T1CKPS    = 0x03; // Timer 1 prescale 1:8
        T1CONbits.T1OSCEN   = 0x00; // Timer 1 dedicated osc. disabled
        T1CONbits.nT1SYNC   = 0x00; // Timer 1 asynch. clock synched with system clock
        T1CONbits.TMR1ON    = 0x01; // Timer 1 enabled

        /*
         * Initialise Timer 2 to interrupt periodically
         * Note : Used in conjunction with TIMER2_PERIOD
         * to set the interrupt period
         */
//        T2Con - Timer 2 control register bits
//        bit 6-3 T2OUTPS<3:0>: Timer2 Output Postscaler Select bits
//            1111 = 1:16 Postscaler
//            1110 = 1:15 Postscaler
//            1101 = 1:14 Postscaler
//            1100 = 1:13 Postscaler
//            1011 = 1:12 Postscaler
//            1010 = 1:11 Postscaler
//            1001 = 1:10 Postscaler
//            1000 = 1:9 Postscaler
//            0111 = 1:8 Postscaler
//            0110 = 1:7 Postscaler
//            0101 = 1:6 Postscaler
//            0100 = 1:5 Postscaler
//            0011 = 1:4 Postscaler
//            0010 = 1:3 Postscaler
//            0001 = 1:2 Postscaler
//            0000 = 1:1 Postscaler
//        bit 2 TMR2ON: Timer2 On bit
//            1 = Timer2 is on
//            0 = Timer2 is off
//        bit 1-0 T2CKPS<1:0>: Timer2 Clock Prescale Select bits
//            11 = Prescaler is 64
//            10 = Prescaler is 16
//            01 = Prescaler is 4
//            00 = Prescaler is 1

        T2CONbits.T2CKPS    = TIMER2_PRESCALE;  // Prescale of 4
        T2CONbits.T2OUTPS   = TIMER2_POSTSCALE; // Postscale of 1:8
        T2CONbits.TMR2ON    = 0x0;              // Timer 2 off
        TMR2                = 0x00;             // Set timer 2 count to zero
        PR2                 = 0x00;             // Timer 2 module period set to zero

        /*
	 * Initialise ADC Module for for 12 bit channels
	 */
        ADCON0              = 0x0;  // Channel AD0 selected
        ADCON0bits.ADON     = 0x1;  // ADC enabled in code
        ADCON0bits.ADRMD    = 0x0;  // ADC Result mode 12 bit

        ADCON1              = 0x0;
#ifndef FOSC32MHZ
        ADCON1bits.ADCS     = 0x05; // ADC conversion clock Fosc/16
#else
        ADCON1bits.ADCS     = 0x02; // ADC conversion clock Fosc/32
#endif
        ADCON1bits.ADFM     = 0x1;  // ADC result format 2s complement
        ADCON1bits.ADNREF   = 0x0;  // ADC negative V ref connected to Vss
        ADCON1bits.ADPREF   = 0x00; // ADC positive V ref connected to Vdd

        ADCON2              = 0x0F; // Auto conversion disabled
                                    // ADC Negative ref selected by ADNREF

        /*
	 * Initialise all three Capture/compare modules - one per phase
	 * Initialise Capture/compare modules to measure the frequency
         * of the zero crossing detector inputs and generate a software
         * interrupt when comparison made.
	 */
        CCP1CON             = 0x00;
        CCP1CONbits.CCP1M   = 0x0A; // Compare mode : generate software interrupt

        CCP2CON             = 0x00;
        CCP2CONbits.CCP2M   = 0x0A; // Compare mode : generate software interrupt

        CCP3CON             = 0x00;
        CCP3CONbits.CCP3M   = 0x0A; // Compare mode : generate software interrupt

        /*
         * Initialise interrupt on change registers
         */
        IOCAF = IOCBF = IOCCF = IOCEF = 0x00;
        IOCAP = IOCBP = IOCCP = IOCEP = 0x00;
        IOCAN = IOCBN = IOCCN = IOCEN = 0x00;

        /*
         * Set zero crosing detectors to interrupt on both edges
         */
        IOCBPbits.IOCBP0    = 0x1;     // L1 zero crossing detector +ve edge

        IOCBPbits.IOCBP1    = 0x1;     // L2 zero crossing detector +ve edge
        IOCBPbits.IOCBP2    = 0x1;     // L3 zero crossing detector +ve edge

        IOCBNbits.IOCBN0    = 0x1;     // L1 zero crossing detector -ve edge

        IOCBNbits.IOCBN1    = 0x1;     // L2 zero crossing detector -ve edge
        IOCBNbits.IOCBN2    = 0x1;     // L3 zero crossing detector -ve edge

        /*
         * Initialise Synchonous Serial Port registers
         */
        SSPSTAT             = 0x0;
        SSPSTATbits.SMP     = 0x0;      // Input data sampled at middle of output time
        SSPSTATbits.CKE     = 0x0;      //
        SSPCON1             = 0x00;
        SSPCON1bits.SSPEN   = 0x1;      // Enables serial port and configures SCK, SDO, SDI & SS
        SSPCON1bits.CKP     = 0x0;      // idle state for clock is a low level
        SSPCON1bits.SSPM    = 0x00;     // Clock speed Fosc/4

        SSPCON2             = 0x00;
        SSPCON2bits.GCEN    = 0x0;      // General call address disabled
        SSPCON2bits.SEN     = 0x0;      // Start condition idle

        SSPCON3             = 0x00;

        SSPMSK              = 0x00;

        SSPADD              = 0x00;
        SSPADDbits.ADD      = 0x00;     // Baud rate divider clock is 4/Fosc

        SSPBUF              = 0x00;

        /*
         * Ensure all interrupts are cleared before enabling
         */
        PIR1 = PIR2 = PIR4 = 0x00;  // Clear all interrupt flags

        /*
	 * Initialise peripheral interrupt enable registers
	 */
        PIE1bits.TMR1GIE    = 0x0;  // Disable timer 1 gate interrupt
        PIE1bits.ADIE       = 0x0;  // Enable ADC interrupt in code
        PIE1bits.RCIE       = 0x0;  // Disable USART Rx interrupt
        PIE1bits.TXIE       = 0x0;  // Disable USART Tx interrupt
        PIE1bits.SSP1IE     = 0x0;  // Disable synchronous serial port interrupt
        PIE1bits.CCP1IE     = 0x0;  // Enable CCP1 interrupt in code
        PIE1bits.TMR2IE     = 0x0;  // Disable timer2 interrupt
        PIE1bits.TMR1IE     = 0x0;  // Disable timer 1 overflow interrupt

        PIE2bits.OSFIE      = 0x0;  // Disable Osc fail interrupt
        PIE2bits.C1IE       = 0x0;  // Disable comparator 1 interrupt
        PIE2bits.C2IE       = 0x0;  // Disable comparator 2 interrupt
        PIE2bits.C3IE       = 0x0;  // Disable comparator 3 interrupt
        PIE2bits.EEIE       = 0x0;  // Disable EEPROM write interrupt
        PIE2bits.BCL1IE     = 0x0;  // Disable MSSP bus collision interrupt
        PIE2bits.C4IE       = 0x0;  // Disable comparator 3 interrupt
        PIE2bits.CCP2IE     = 0x0;  // Enable CCP2 interrupt in code

        PIE4bits.PSMC4TIE   = 0x0;  // Disable PSMC4 time base interrupt
        PIE4bits.PMSC3TIE   = 0x0;  // Disable PSMC3 time base interrupt
        PIE4bits.PSMC2TIE   = 0x0;  // Disable PSMC2 time base interrupt
        PIE4bits.PSMC1TIE   = 0x0;  // Disable PSMC1 time base interrupt
        PIE4bits.PSMC4SIE   = 0x0;  // Disable PSMC4 auto shutdown interrupt
        PIE4bits.PSMC3SIE   = 0x0;  // Disable PSMC3 auto shutdown interrupt
        PIE4bits.PSMC2SIE   = 0x0;  // Disable PSMC2 auto shutdown interrupt
        PIE4bits.PSMC1SIE   = 0x0;  // Disable PSMC1 auto shutdown interrupt

        PIE3bits.CCP3IE     = 0x0;  // Enable CCP3 interrupt in code
        PIE1bits.TMR2IE     = 0x0;  // Enable TMR2 interrupt in code

        PIR1 = PIR2 = PIR3 = PIR4  = 0x00; // Clear all peripheral interrupts

        /*
	 * Initialise Interrupt module for compare interrupts
         * and zero crossing detector inputs
	 */
        INTCONbits.PEIE     = 1;    // Enable peripheral active interrupts
        INTCONbits.TMR0IE   = 0;    // Disable Timer 0 interrupt
        INTCONbits.INTE     = 0;    // Disable INT external interrupt
        INTCONbits.IOCIE    = 1;    // Enable interrupt on change
	INTCONbits.GIE      = 1;    // Enable all interrupts.


        /*
         * Load Timer 2 with period for SUM conversion interrupts
         */
        PR2             = TIMER2_PERIOD;        // Timer 2 module period set

        /*
         * Enable timer 2 interrupt
         */
        PIE1bits.TMR2IE = 0x1;

        T2CONbits.TMR2ON = 0x1;                 // Turn timer 2 on

        /* Initialise global variable */
        ADCL1ConvRequired       = LU_FALSE;
        ADCL2ConvRequired       = LU_FALSE;
        ADCL3ConvRequired       = LU_FALSE;
        ADCSumConvRequired      = LU_FALSE;
        ADCSumPkConvRequired    = LU_FALSE;
        ADCResultReady          = LU_FALSE;
        PhaseDataAbsent         = LU_FALSE;
        ZeroPhaseDataSend       = 0;

}


/*!
*******************************************************************************
* @}
*/

/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/



/*!
*******************************************************************************
* @}
*/


/*
*********************** End of file *******************************************
*/
