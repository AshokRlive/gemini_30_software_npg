/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME: 
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION: 
*		\brief  ISR handler Module
*
*       
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY	 
*
*   Date          EAR/Reason      Name            Details
*   ---------------------------------------------------------------------------
*   27/03/2014    FPM Module      Steve Walde    Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/


/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/
#include "lu_types.h"
#include "FrontEndPIC.h"

#include "fdm.h"
#include "globalVariables.h"

/*
*******************************************************************************
* LOCAL - Private Definitions
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Types
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Constants
*******************************************************************************
*/

/*
*******************************************************************************
* LOCAL - Private Variables
*******************************************************************************
*/
static unsigned char debug;

volatile lu_bool_t   L1ArmedFlag;
volatile lu_bool_t   L2ArmedFlag;
volatile lu_bool_t   L3ArmedFlag;

volatile lu_bool_t   L1PreviousValidFlag;
volatile lu_bool_t   L2PreviousValidFlag;
volatile lu_bool_t   L3PreviousValidFlag;

volatile lu_uint16_t L1TimerValue;
volatile lu_uint16_t L2TimerValue;
volatile lu_uint16_t L3TimerValue;

volatile lu_uint16_t L1Period;
volatile lu_uint16_t L2Period;
volatile lu_uint16_t L3Period;

volatile lu_uint16_t L1PreviousPeriod;
volatile lu_uint16_t L2PreviousPeriod;
volatile lu_uint16_t L3PreviousPeriod;

volatile lu_uint16_t L1Time;
volatile lu_uint16_t L1PreviousTime;
volatile lu_uint16_t L1DelayTime;

volatile lu_uint16_t L2Time;
volatile lu_uint16_t L2PreviousTime;
volatile lu_uint16_t L2DelayTime;

volatile lu_uint16_t L3Time;
volatile lu_uint16_t L3PreviousTime;
volatile lu_uint16_t L3DelayTime;

/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/


/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/


/*!
*******************************************************************************
* EXPORTED:
*   \name Public Functions
*******************************************************************************
* @{
*/



/*!
*******************************************************************************
* Function Name:
*	isr()
*
* Description:
*	\brief   . 
*
*	
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	\param na.
* 
* Return Value:
*   \return T  
*
* Example Usage: 
*   \code 
*    
*   \endcode 
*
* See Also:
*   \sa na          
*
*******************************************************************************
*/

void interrupt Isr(void)
{
    lu_uint8_t timerLow, timerHigh;
    /*
     * Check each of the interrupt flags and loop until all
     * have been cleared.
     */

    /*
     * Stop timer to read the timer one high and low values
     * to avoid roll over problems
     */
    T1CONbits.TMR1ON    = 0x00;
    timerLow            = TMR1L;
    timerHigh           = TMR1H;
    T1CONbits.TMR1ON    = 0x01;
    
  while(1)
  {
    if( INTCONbits.IOCIF )      // Change of state interrupt
    {
        /*
         * L1,L2,L3 zero crossing detector change detected
         * so read timer 1, add the period and write to the
         * capture/compare unit
         */
        if( L1_INTERRUPT_FLAG )       // L1 Zero crossing
        {
            /*
             * Clear interrupt flag
             */
            L1_INTERRUPT_FLAG = 0x0;   // Reset the interrupt flag

            /* Combine timer 1 bytes into a single value */
            L1TimerValue = (timerLow & 0xff) | ((timerHigh & 0xff)<<8);
            L1Time = L1TimerValue;

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output high */
             L1_SCOPE_TRIGGER = 0x1;
#endif
            
             /*
              * Set flag to show compare interrupt is valid
              */
             L1ArmedFlag = LU_TRUE;

            /*
             * If first time don't do any more as previous time won't be valid
             */
            if( L1PreviousValidFlag  == LU_FALSE )
            {
                L1PreviousValidFlag  = LU_TRUE;
            }
            /*
             * If second time only calculate period as previous period won't be valid
             */
            else if(L1PreviousPeriod == 0)
            {
                L1Period = L1Time - L1PreviousTime;
            }
            else
            {
                 /* Measure the half period of the sine wave and save */
                L1Period = L1Time - L1PreviousTime;

                /*
                 *  Add the half period/2 to determine the peak and then subtract
                 *  the time taken to start the conversion
                 */
                L1DelayTime  = ( (L1PreviousPeriod >> 1) -  ADC_SETUP_TIME);

                L1TimerValue += L1DelayTime;

                /*
                 *  Write value to CCP2
                 */
                CCPR1L = L1TimerValue & 0xff;
                CCPR1H = ( L1TimerValue >> 8 ) & 0xff;

                /* Enable compare interrupt */
                PIE1bits.CCP1IE     = 0x1;

            }

            /* Record new time and period as previous time and period */
            L1PreviousTime = L1Time;
            L1PreviousPeriod = L1Period;

#ifdef L1_SYNCH
            /*
             * Synchronise timer 2 interrupt
             */
            PIE1bits.TMR2IE = 0x0;

            T2CONbits.TMR2ON = 0x0;      // Turn timer 2 off

            /*
             * Load Timer 2 with period
             */
            PR2             = TIMER2_PERIOD - TIMER2_OFFSET;        // Timer 2 module period set

            /*
             * Enable timer 2 interrupt
             */
            PIE1bits.TMR2IE = 0x1;

            T2CONbits.TMR2ON = 0x1;                 // Turn timer 2 on
#endif

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
             L1_SCOPE_TRIGGER = 0x0;
#endif

        }
        else if( L2_INTERRUPT_FLAG )       // L2 Zero crossing
        {
            /*
             * Clear interrupt flag
             */
            L2_INTERRUPT_FLAG = 0x0;

            /* Combine timer 1 bytes into a single value */
            L2TimerValue = (timerLow & 0xff) | ((timerHigh & 0xff)<<8);
            L2Time = L2TimerValue;

#ifdef DEBUG_MODE_ISR
           /* Set scope trigger output high */
           L2_SCOPE_TRIGGER = 0x1;
#endif

           /*
            * Set flag to show compare interrupt is valid
            */
            L2ArmedFlag = LU_TRUE;

            /*
             * If first time don't do any more as previous time won't be valid
             */
            if( L2PreviousValidFlag  == LU_FALSE )
            {
                L2PreviousValidFlag  = LU_TRUE;
            }
            /*
             * If second time only calculate period as previous period won't be valid
             */
            else if(L2PreviousPeriod == 0)
            {
                L2Period = L2Time - L2PreviousTime;
            }
            else
            {
                /* Measure the half period of the sine wave and save */
                L2Period = L2Time - L2PreviousTime;

                /*
                 *  Add the half period/2 to determine the peak and then subtract
                 *  the time taken to start the conversion
                 */
                L2DelayTime  = ( (L2PreviousPeriod >> 1) -  ADC_SETUP_TIME);

                L2TimerValue += L2DelayTime;

                /*
                 *  Write value to CCP2
                 */
                CCPR2L = L2TimerValue & 0xff;
                CCPR2H = ( L2TimerValue >> 8 ) & 0xff;

                /* Enable compare interrupt */
                PIE2bits.CCP2IE     = 0x1;
                
           }
            
            /* Record new time and period as previous time and period */
            L2PreviousTime = L2Time;
            L2PreviousPeriod = L2Period;

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
             L2_SCOPE_TRIGGER = 0x0;
#endif

            
        }
        else if( L3_INTERRUPT_FLAG )       // L3 Zero crossing
        {
            /*
             * Clear interrupt flag
             */
            L3_INTERRUPT_FLAG = 0x0;

            /* Combine timer 1 bytes into a single value */
            L3TimerValue = (timerLow & 0xff) | ((timerHigh & 0xff)<<8);
            L3Time = L3TimerValue;

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output high */
             L3_SCOPE_TRIGGER = 0x1;
#endif
           /*
            * Set flag to show compare interrupt is valid
            */
            L3ArmedFlag = LU_TRUE;

            /*
             * If first time don't do any more as previous time won't be valid
             */
            if( L3PreviousValidFlag  == LU_FALSE )
            {
                L3PreviousValidFlag  = LU_TRUE;
            }
            /*
             * If second time only calculate period as previous period won't be valid
             */
            else if(L3PreviousPeriod == 0)
            {
                L3Period = L3Time - L3PreviousTime;
            }
            else
            {
                /* Measure the half period of the sine wave and save */
                L3Period = L3Time - L3PreviousTime;

                /*
                 *  Add the half period/2 to determine the peak and then subtract
                 *  the time taken to start the conversion
                 */
                L3DelayTime  = ( (L3PreviousPeriod >> 1) -  ADC_SETUP_TIME);

                L3TimerValue += L3DelayTime;

                /*
                 *  Write value to CCP3
                 */
                CCPR3L = L3TimerValue & 0xff;
                CCPR3H = ( L3TimerValue >> 8 ) & 0xff;

                /* Enable compare interrupt */
                PIE3bits.CCP3IE     = 0x1;

            }

            /* Record new time and period as previous time and period */
            L3PreviousTime = L3Time;
            L3PreviousPeriod = L3Period;

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
            L3_SCOPE_TRIGGER = 0x0;
#endif

        }

    }

    else if( PIR1bits.CCP1IF )      // Compare 1
    {
        /*
         * Clear the interupt flag
         */
        PIR1bits.CCP1IF = 0x0;

        if( L1ArmedFlag == LU_TRUE )
        {
#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output high */
             L1_SCOPE_TRIGGER = 0x1;
#endif
             
            /* Disable compare interrupt */
            PIE1bits.CCP1IE     = 0x0;

            /*
             * Set flag to indicate that a conversion is required
             */

            ADCL1ConvRequired  = LU_TRUE;
            L1ArmedFlag = LU_FALSE;
            
#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
             L1_SCOPE_TRIGGER = 0x0;
#endif
        }
    }
    else if( PIR2bits.CCP2IF )      // Compare 2
    {
        /*
         * Clear the interupt flag
         */
        PIR2bits.CCP2IF = 0x0;
        if( L2ArmedFlag == LU_TRUE)
        {
#ifdef DEBUG_MODE_ISR
        /* Set scope trigger output high */
         L2_SCOPE_TRIGGER = 0x1;
#endif

            /* Disable compare interrupt */
            PIE2bits.CCP2IE     = 0x0;

            /*
             * Set flag to indicate that a conversion is required
             * The conversion will be started in main when ADC is free
             */
            ADCL2ConvRequired  = LU_TRUE;
            L2ArmedFlag = LU_FALSE;

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
             L2_SCOPE_TRIGGER = 0x0;
#endif
        }
    }
    else if( PIR3bits.CCP3IF )      // Compare 3
    {
        /*
         * Clear the interupt flag
         */
        PIR3bits.CCP3IF = 0x0;

        /* Disable compare interrupt */
        PIE3bits.CCP3IE     = 0x0;

        if( L3ArmedFlag == LU_TRUE)
        {
#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output high */
             L3_SCOPE_TRIGGER = 0x1;
#endif

            /*
             * Set flag to indicate that a conversion is required
             */
            ADCL3ConvRequired  = LU_TRUE;

            L3ArmedFlag = LU_FALSE;
#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
             L3_SCOPE_TRIGGER = 0x0;
#endif
        }
    }
    else if( INTCONbits.INTF )
    {
        /*
         * Not used but clear the interupt flag if
         * a spurious interrupt received
         */
        INTCONbits.INTF = 0x0;

    }
    else if( PIR1bits.TMR2IF )      // Timer 2 interrupt
    {
        /*
         * Clear the interupt flag
         */
        PIR1bits.TMR2IF = 0x0;

#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output high */
             SUM_SCOPE_TRIGGER = 0x1;
#endif


        ADCSumConvRequired  = LU_TRUE;

        /*
         * Disable timer 2 interrupt
         */
        PIE1bits.TMR2IE = 0x0;

        T2CONbits.TMR2ON = 0x0;      // Turn timer 2 off

        /*
         * Load Timer 2 with period
         */
        PR2             = TIMER2_PERIOD;        // Timer 2 module period set

        /*
         * Enable timer 2 interrupt
         */
        PIE1bits.TMR2IE = 0x1;

        T2CONbits.TMR2ON = 0x1;                 // Turn timer 2 on


#ifdef DEBUG_MODE_ISR
            /* Set scope trigger output low */
             SUM_SCOPE_TRIGGER = 0x0;
#endif

    }
    else if( PIR1bits.ADIF )      // ADC completion
    {
         /*
         * Clear the interupt flag
         */
        PIR1bits.ADIF = 0x0;

        /*
         *  Disable ADC interrupt 
         */
        PIE1bits.ADIE = 0x0;

        /*
         * ADC conversion finished so set flag
         */
        ADCResultReady = LU_TRUE;

    }
    else
    {
        break;
    }

  }

}

/*
*********************** End of file *******************************************
*/