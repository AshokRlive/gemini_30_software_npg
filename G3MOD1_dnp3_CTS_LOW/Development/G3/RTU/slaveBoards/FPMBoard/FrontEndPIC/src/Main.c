/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: $
 *               $HeadURL: $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision:  $: (Revision of last commit)
 *               $Author:  $: (Author of last commit)
 *       \date   $Date: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/07/14      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <pic16f1788.h>
#include <stdlib.h>
#include <xc.h>


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */
#include "pragma.h"
#include "lu_types.h"
#include "FrontEndPIC.h"
#include "adc.h"
#include "initialise.h"
#include "spi.h"
#include "isr_han.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

volatile lu_bool_t ADCL1ConvRequired;
volatile lu_bool_t ADCL2ConvRequired;
volatile lu_bool_t ADCL3ConvRequired;
volatile lu_bool_t ADCSumConvRequired;
volatile lu_bool_t ADCSumPkConvRequired;
volatile lu_bool_t ADCResultReady;
volatile lu_bool_t PhaseDataAbsent;
volatile lu_uint8_t ZeroPhaseDataSend;

/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*
*******************************************************************************
* LOCAL - Function Prototypes
*******************************************************************************
*/

/*!
*******************************************************************************
* EXPORTED:
*   \name Public Functions
*******************************************************************************
* @{
*/

/*!
*******************************************************************************
* @}
*/

/*!
*******************************************************************************
* LOCAL:
*   \name Private Functions
*******************************************************************************
* @{
*/

/*!
*******************************************************************************
* Function Name:
*	main()
*
* Description: Front end for analogue measurement of phases and sum
*	\brief   .
*
*
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	\param na.
*
* Return Value:
*   \return T
*
* Example Usage:
*   \code
*
*   \endcode
*
* See Also:
*   \sa na
*
*******************************************************************************
*/
void main(void)
{
    static lu_uint8_t   chan;
    lu_bool_t           doADC = LU_FALSE;
    lu_int16_t          value;


    /*
     * Call the initialisation function
     */
    Initialise();

    /*
     * Start of never ending loop
     */
    while(1)
    {
        /*
         * Kick the watchdog on every iteration
         */
        CLRWDT();

        /*
         * Check that no conversion is taking place by checking that
         * the ADC GoDone bit and the doADC flag is not set
         */
        if( AdcIdle() && (doADC == LU_FALSE) )
        {
            /*
             * Disable all interrupts
             */
            INTCONbits.GIE = 0;

            /*
             * Check for a conversion required.
             * These flags are set in the compare or Timer2 ISR
             */
            if( ADCL1ConvRequired == LU_TRUE )
            {
                /*Set the channel to be converted and the start flag */
                chan = FEPIC_ADC_CH_L1;
                doADC = LU_TRUE;
                ADCL1ConvRequired = LU_FALSE;
            }
            else if( ADCL2ConvRequired == LU_TRUE )
            {
                /*Set the channel to be converted and the start flag */
                chan = FEPIC_ADC_CH_L2;
                doADC = LU_TRUE;
                ADCL2ConvRequired = LU_FALSE;
            }
            else if( ADCL3ConvRequired == LU_TRUE )
            {
                /*Set the channel to be converted and the start flag */
                chan = FEPIC_ADC_CH_L3;
                doADC = LU_TRUE;
                ADCL3ConvRequired = LU_FALSE;
            }
            else if( ADCSumConvRequired == LU_TRUE )
            {
                /*Set the channel to be converted and the start flag */
                chan = FEPIC_ADC_CH_SUM;
                doADC = LU_TRUE;
                ADCSumConvRequired = LU_FALSE;
            }
            else if( ADCSumPkConvRequired == LU_TRUE )
            {
                /*Set the channel to be converted and the start flag */
                chan = FEPIC_ADC_CH_SUM_PK;
                doADC = LU_TRUE;
                ADCSumPkConvRequired = LU_FALSE;
            }

            /*Start the ADC convertion and reset the flag */
            if( doADC == LU_TRUE )
            {
                AdcStart(chan);
            }
        }

        /*
         * Enable all interrupts
         */
        INTCONbits.GIE = 1;

        /*
         * If the conversion is complete read the value
         * and send it to the SPI
         */
        if ( AdcConvertComplete( chan ) )
        {
            value = AdcRead(chan);
            SpiSendPacket(chan, value);
            doADC = LU_FALSE;

            /*
             * If no zero crossings have been received for one cycle
             * then send zero values for each phase in turn
             */
            if( PhaseDataAbsent == LU_TRUE)
            {
                if( ZeroPhaseDataSend < FEPIC_ADC_CH_LAST )
                {
                    chan = ZeroPhaseDataSend;
                    value = 0;

                    SpiSendPacket(chan, value);

                    ZeroPhaseDataSend += 1;

                    if (ZeroPhaseDataSend == FEPIC_ADC_CH_SUM)
                    {
                    	ZeroPhaseDataSend = FEPIC_ADC_CH_SUM_PK;
                    }
                }
            }
        }
    }   /* Finish of never ending loop */
}


/****************************************************************************
 ***************** Local Functions ******************************************
 ****************************************************************************/




/*
 *********************** End of file ******************************************
 */
