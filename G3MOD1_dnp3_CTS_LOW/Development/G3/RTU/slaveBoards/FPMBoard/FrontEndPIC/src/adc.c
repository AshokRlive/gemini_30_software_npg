/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id:  $
 *               $HeadURL:  $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $ $: (Revision of last commit)
 *               $Author:  $: (Author of last commit)
 *       \date   $Date:  $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "fdm.h"
#include "globalVariables.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
void        wait(unsigned int);
void        phaseReset(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */
static lu_uint8_t  zeroCrossingCounter = 0;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *      Return true if the ADC is not busy else return false
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t AdcConvertComplete( lu_uint8_t chan )
{
    lu_uint8_t channelConverted;
    lu_bool_t errorFlag = LU_FALSE;

     /* Read the channel that was converted */
    channelConverted = ADCON0bits.CHS;

    if(  (ADCON0bits.GO_nDONE == 0x0 ) && ( ADCResultReady == LU_TRUE ) )
    {
        if(channelConverted != chan )
        {
            errorFlag = LU_TRUE;
        }

        ADCResultReady = LU_FALSE;
        return( LU_TRUE );
    }
    else
    {
        return( LU_FALSE);
    }
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *      Return true if the ADC is not busy else return false
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_bool_t AdcIdle( void )
{
    if( ADCON0bits.GO_nDONE == 0x0)
    {
    return( LU_TRUE );
    }
    else
    {
        return( LU_FALSE);
    }
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *      Return true if the ADC is not busy else return false
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */

void   AdcStart(lu_uint8_t chan)
{
   /*
    * Start conversion
    */
    if (chan == FEPIC_ADC_CH_SUM_PK)
    {
        chan = FEPIC_ADC_CH_SUM;
    }
    
    ADCON0bits.CHS      = chan;     /* Select channel AN0 */
    wait(ADC_DELAY);
    ADCON0bits.GO_nDONE = 0x1;      /* Start conversion */
    PIE1bits.ADIE       = 0x1;      /* Enable ADC interrupt */

#ifdef DEBUG_MODE_ADC
    switch(chan)
    {
        case FEPIC_ADC_CH_L1:
            L1_SCOPE_TRIGGER = 0x1;
        break;
	case FEPIC_ADC_CH_L2:
            L2_SCOPE_TRIGGER = 0x1;
        break;
	case FEPIC_ADC_CH_L3:
            L3_SCOPE_TRIGGER = 0x1;
        break;
	case FEPIC_ADC_CH_SUM:
            SUM_SCOPE_TRIGGER = 0x1;
        break;
    }
#endif
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *      Return true if the ADC is not busy else return false
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
lu_int16_t AdcRead(lu_uint8_t chan)
{
    lu_int16_t analogueResult;
    lu_uint8_t channelConverted;
    lu_bool_t errorFlag = LU_FALSE;

    /*
     * Read the two 8 bit result registers and combine
     * into one 16 bit signed twos complement number
     */
    analogueResult = ((ADRESH & 0xff)<<8) | (ADRESL & 0xff);

    /*
     * Read the channel that was converted
     */
    channelConverted = ADCON0bits.CHS;

    if(channelConverted != chan )
    {
        errorFlag = LU_TRUE;
    }

    switch(channelConverted)
    {
        case FEPIC_ADC_CH_L1:
            ADCL1ConvRequired = LU_FALSE;
            /* reset the sum counter */
            phaseReset();
#ifdef DEBUG_MODE_ADC
            L1_SCOPE_TRIGGER = 0x0;
#endif
            ADCSumPkConvRequired = LU_TRUE;
        break;

	case FEPIC_ADC_CH_L2:
            ADCL2ConvRequired = LU_FALSE;
            /* reset the sum counter */
            phaseReset();
#ifdef DEBUG_MODE_ADC
            L2_SCOPE_TRIGGER = 0x0;
#endif
            ADCSumPkConvRequired = LU_TRUE;
        break;

	case FEPIC_ADC_CH_L3:
            ADCL3ConvRequired = LU_FALSE;
            phaseReset();
#ifdef DEBUG_MODE_ADC
            L3_SCOPE_TRIGGER = 0x0;
#endif
            ADCSumPkConvRequired = LU_TRUE;
        break;

        case FEPIC_ADC_CH_SUM_PK:
             ADCSumPkConvRequired = LU_FALSE;
            break;

	case FEPIC_ADC_CH_SUM:
            ADCSumConvRequired = LU_FALSE;

#ifdef NO_NEGATIVE_SUM
            /*
             *  Zero the analogue result if the sum value is negative
             */
            if( analogueResult < 0 )
            {
                analogueResult = 0;
            }
#endif
            /*
             *  Zero the analogue result if the sum counter exceeds
             *  the max value, otherwise increment the counter
             */
            if(zeroCrossingCounter++ > MAX_SUM_PACKET_COUNT)
            {
                zeroCrossingCounter = 0;
                PhaseDataAbsent     = LU_TRUE;
                ZeroPhaseDataSend   = FEPIC_ADC_CH_L1;
            }

            /*
             *  Zero the analogue result if no phase data
             */
            if( PhaseDataAbsent == LU_TRUE )
            {
                analogueResult = 0;
            }

#ifdef DEBUG_MODE_ADC
            SUM_SCOPE_TRIGGER = 0x0;
#endif
        break;
    }


    return( analogueResult );

}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
*******************************************************************************
* Function Name:
*       wait()
*
* Description:
*	Delays for for increments of approx. 10ms
*
*
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	Delay time in units of ~100us
*
* Return Value:
*   \return T
*
* Example Usage:
*   \code
*
*   \endcode
*
* See Also:
*   \sa na
*
*******************************************************************************
*/
void wait(unsigned int time)
{

    lu_uint16_t i,j;
    for(i=0;i<time;i++)
    {
        for(j=0;j<WAIT_DELAY;j++);
    }

}

/*!
*******************************************************************************
* Function Name:
*       wait()
*
* Description:
*	Delays for for increments of approx. 10ms
*
*
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	Delay time in units of ~100us
*
* Return Value:
*   \return T
*
* Example Usage:
*   \code
*
*   \endcode
*
* See Also:
*   \sa na
*
*******************************************************************************
*/
void phaseReset(void)
{

    /*
     * Reset variables if a zero crossing is detected
     */
    zeroCrossingCounter     = 0;
    PhaseDataAbsent         = LU_FALSE;
    ZeroPhaseDataSend       = FEPIC_ADC_CH_LAST;

}

/*
 *********************** End of file ******************************************
 */
