/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id:  $
 *               $HeadURL:  $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $ $: (Revision of last commit)
 *               $Author:  $: (Author of last commit)
 *       \date   $Date:  $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "FrontEndPIC.h"
#include "crc16.h"
#include "fdm.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */
lu_uint8_t spiSendByte(lu_uint8_t);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */





/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

/*!
*******************************************************************************
* Function Name:
*       SpiSendPacket()
*
* Description:
*	Sends a data packet to the SPI
*
*
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	Pointer to the buffer containing the packet to be sent
*       Pointer to the buffer where the packet returned is written
*
*
* Return Value:
*   \return T
*
* Example Usage:
*   \code
*
*   \endcode
*
* See Also:
*   \sa na
*
*******************************************************************************
*/
void  SpiSendPacket(lu_uint8_t chan, lu_int16_t value)
{
    static lu_uint8_t   packetCount = 0;
    FEPic2NXPPacketStr packet;
    lu_uint8_t rxData[sizeof(packet)];
    lu_uint8_t *txDataPtr;
    lu_uint8_t *rxDataPtr;
    lu_uint8_t i;

    /* Create pointers to transmit buffer and receive buffers */
    txDataPtr = (lu_uint8_t *)&packet;
    rxDataPtr = (lu_uint8_t *)rxData;

#ifdef DEBUG_MODE_SPI
    switch(chan)
    {
        case FEPIC_ADC_CH_L1:
            L1_SCOPE_TRIGGER = 0x1;
        break;
	case FEPIC_ADC_CH_L2:
            L2_SCOPE_TRIGGER = 0x1;
        break;
	case FEPIC_ADC_CH_L3:
            L3_SCOPE_TRIGGER = 0x1;
        break;
	case FEPIC_ADC_CH_SUM:
            SUM_SCOPE_TRIGGER = 0x1;
        break;
    }
#endif
    
    /* Fill packet structure with data */
    packet.packetCount = packetCount++;
    packet.adcChannel  = chan;
    packet.adcValue    = value;

    /* Calculate the crc for the packet */
#ifndef NO_SPI_PACKET_CRC
    crc16_calc16(txDataPtr,  (sizeof(packet) - sizeof(crc16) ));
    packet.crc16 = crc16;
#else
    packet.crc16 = 0;
#endif

     /* Set chip select to slave (active low) */
    SS = 0x0;

    /* Send the packet over the SPI */
    for( i=0 ; i<(sizeof(packet)) ; i++)
    {
        *rxDataPtr = spiSendByte(*txDataPtr);
        txDataPtr++;
        rxDataPtr++;
    }

    /* Reset chip select to slave (active low) */
    SS = 0x1;

#ifdef DEBUG_MODE_SPI
    switch(chan)
    {
        case FEPIC_ADC_CH_L1:
            L1_SCOPE_TRIGGER = 0x0;
        break;
	case FEPIC_ADC_CH_L2:
            L2_SCOPE_TRIGGER = 0x0;
        break;
	case FEPIC_ADC_CH_L3:
            L3_SCOPE_TRIGGER = 0x0;
        break;
	case FEPIC_ADC_CH_SUM:
            SUM_SCOPE_TRIGGER = 0x0;
        break;
    }
#endif

}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */
/*!
*******************************************************************************
* Function Name:
*       SpiSendByte
*
* Description:
*	Sends a byte to the SPI
*
*
*
* Variables:
*       Local / external variables modified...
*
* Parameters:
*	Byte to be sent
*
* Return Value:
*       The value of the response byte
*
* Example Usage:
*   \code
*
*   \endcode
*
* See Also:
*   \sa na
*
*******************************************************************************
*/
lu_uint8_t spiSendByte(lu_uint8_t value)
{
    lu_uint8_t dummy;

    /* Send char to to buffer */
    SSPBUF = value;

    /* check the buffer full flag */
    while( SSPSTATbits.BF != 0x1 );

    /* Read the buffer and return the value */
    dummy = SSPBUF;

    return dummy;
}



/*
 *********************** End of file ******************************************
 */
