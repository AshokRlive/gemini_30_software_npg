/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [pragma.h]
 *
 *    FILE NAME:
 *               $Id:  $
 *               $HeadURL:  $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Configures the PIC
 *
 *    CURRENT REVISION
 *
 *               $Revision:  $: (Revision of last commit)
 *               $Author: $: (Author of last commit)
 *       \date   $Date:  $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   16/07/14      walde_s     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef _PRAGMA_INCLUDED
#define	_PRAGMA_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "fdm.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */

/*
 * Set all configuration registers explicitly
 */

#pragma config FCMEN 	= 0	// Fail-Safe Clock Monitor Enable bit
                                //    1 = Fail-Safe Clock Monitor and internal/external switchover are both enabled.
                                //    0 = Fail-Safe Clock Monitor is disabled
#pragma config IESO 	= 0	// Internal External Switchover bit
                                //    1 = Internal/External Switchover mode is enabled
                                //    0 = Internal/External Switchover mode is disabled
#pragma config CLKOUTEN = ON    // If FOSC configuration bits are set to LP, XT, HS modes:
                                //    This bit is ignored, CLKOUT function is disabled. Oscillator function on the CLKOUT pin.
                                // All other FOSC modes:
                                //    1 = CLKOUT function is disabled. I/O function on the CLKOUT pin.
                                //    0 = CLKOUT function is enabled on the CLKOUT pin
#ifdef WATCHDOG_DISABLED
#pragma config BOREN 	= 0	// BOREN<1:0>: Brown-out Reset Enable bits
#else
#pragma config BOREN 	= ON	// Brown-out Reset enabled and controlled by software (SBOREN is enabled)
#endif
                                //    11 = BOR enabled
                                //    10 = BOR enabled during operation and disabled in Sleep
                                //    01 = BOR controlled by SBOREN bit of the BORCON register
                                //    00 = BOR disabled
#pragma config CPD 	= 1	// Data Code Protection bit(1)
                                //    1 = Data memory code protection is disabled
                                //    0 = Data memory code protection is enabled
#pragma config CP	= 1	// Code Protection bit
                                //    1 = Program memory code protection is disabled
                                //    0 = Program memory code protection is enabled
#pragma config MCLRE 	= 1	// MCLR pin enabled; RE3 input pin disabled
                                //    MCLR/VPP Pin Function Select bit
                                //    If LVP bit = 1:
                                //    This bit is ignored.
                                //    If LVP bit = 0:
                                //    1 = MCLR/VPP pin function is MCLR; Weak pull-up enabled.
                                //    0 = MCLR/VPP pin function is digital input; MCLR internally disabled; Weak pull-up under control of
                                //    WPUE3 bit.
#pragma config PWRTE 	= 1	// Power-up Timer Enable bit
                                //    1 = PWRT disabled
                                //    0 = PWRT enabled
#ifdef WATCHDOG_DISABLED
#pragma config WDTE 	= 0	// Watchdog Timer Enable bit
#else
#pragma config WDTE 	= ON	// Watchdog Timer Enabled
#endif
                                //    11 =WDT enabled
                                //    10 =WDT enabled while running and disabled in Sleep
                                //    01 =WDT controlled by the SWDTEN bit in the WDTCON register
                                //    00 =WDT disabled
#pragma config FOSC = INTOSC	// Oscillator Selection bits
                                //    111 = ECH: External Clock, High-Power mode (4-20 MHz): device clock supplied to CLKIN pin
                                //    110 = ECM: External Clock, Medium-Power mode (0.5-4 MHz): device clock supplied to CLKIN pin
                                //    101 = ECL: External Clock, Low-Power mode (0-0.5 MHz): device clock supplied to CLKIN pin
                                //    100 = INTOSC oscillator: I/O function on CLKIN pin
                                //    011 = EXTRC oscillator: External RC circuit connected to CLKIN pin
                                //    010 = HS oscillator: High-speed crystal/resonator connected between OSC1 and OSC2 pins
                                //    001 = XT oscillator: Crystal/resonator connected between OSC1 and OSC2 pins
                                //    000 =LP oscillator: Low-power crystal connected between OSC1 and OSC2 pins
#pragma config LVP 	= 0	// Low-Voltage Programming Enable bit(1)
                                //    1 = Low-voltage programming enabled
                                //    0 = High-voltage on MCLR must be used for programming
#pragma config LPBOR    = 1     // Low-Power BOR Enable bit
                                //    1 = Low-Power Brown-out Reset is disabled
                                //    0 = Low-Power Brown-out Reset is enabled
#pragma config BORV 	= 0	// Brown-out Reset Voltage Selection bit
                                //    1 = Brown-out Reset voltage (VBOR), low trip point selected
                                //    0 = Brown-out Reset voltage (VBOR), high trip point selected

#pragma config STVREN 	= 1	// Stack Overflow/Underflow Reset Enable bit
                                //    1 = Stack Overflow or Underflow will cause a Reset
                                //    0 = Stack Overflow or Underflow will not cause a Reset

#pragma config PLLEN   = ON     // PLL Enable bit
                                //    1 = 4xPLL enabled
                                //    0 = 4xPLL disabled

#pragma config VCAPEN   = 1     // Voltage Regulator Capacitor Enable bit(2)
                                //    1 = VCAP functionality is disabled on RA6
                                //    0 = VCAP functionality is enabled on RA6
#pragma config WRT 	= 0x3	// Flash Memory Self-Write Protection bits
                                //    8 kW Flash memory:
                                //    11 = Write protection off
                                //    10 = 000h to 1FFh write-protected, 200h to 3FFFh may be modified by EECON control
                                //    01 = 000h to 1FFFh write-protected, 2000h to 3FFFh may be modified by EECON control
                                //    00 = 000h to 3FFFh write-protected, no addresses may be modified by EECON control

#endif /* _PRAGMA_INCLUDED */

/*
 *********************** End of file ******************************************
 */

