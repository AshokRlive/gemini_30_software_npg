/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Common]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief common definitions for FPM Pic front end
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   21/07/14      walde_s    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */

#ifndef _FDM_INCLUDE
#define	_FDM_INCLUDE

/*
*******************************************************************************
* INCLUDES - Standard Library
*******************************************************************************
*/
#include <pic16f1788.h>
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

/*
*******************************************************************************
* INCLUDES - Project Specific
*******************************************************************************
*/
#include "lu_types.h"
#include "FrontEndPIC.h"

/*
*******************************************************************************
* EXPORTED - Public Definitions
*******************************************************************************
*/

//#define WATCHDOG_DISABLED 	LU_TRUE		// TRUE if debugging
#define FOSC32MHZ                // Define if Fosc is 32 MHz
//#define NO_SPI_PACKET_CRC
#define L1_SYNCH
//#define DEBUG_MODE_ADC         // Define to output scope triggers in adc
//#define DEBUG_MODE_ISR         // Define to output scope triggers in isr
#define DEBUG_MODE_SPI         // Define to output scope triggers in spi

#define MAX_SUM_PACKET_COUNT 64  // Number of sum counts with no zero crossing
                             //  detector interrupts allowed
#define NO_NEGATIVE_SUM         // Don't transmit negative sum values

#define _XTAL_FREQ          4000000

//        T2Con - Timer 2 control register bits
//        -------------------------------------
//        bit 6-3 T2OUTPS<3:0>: Timer2 Output Postscaler Select bits
//            1111 = 1:16 Postscaler
//            1110 = 1:15 Postscaler
//            1101 = 1:14 Postscaler
//            1100 = 1:13 Postscaler
//            1011 = 1:12 Postscaler
//            1010 = 1:11 Postscaler
//            1001 = 1:10 Postscaler
//            1000 = 1:9 Postscaler
//            0111 = 1:8 Postscaler
//            0110 = 1:7 Postscaler
//            0101 = 1:6 Postscaler
//            0100 = 1:5 Postscaler
//            0011 = 1:4 Postscaler
//            0010 = 1:3 Postscaler
//            0001 = 1:2 Postscaler
//            0000 = 1:1 Postscaler
//        bit 2 TMR2ON: Timer2 On bit
//            1 = Timer2 is on
//            0 = Timer2 is off
//        bit 1-0 T2CKPS<1:0>: Timer2 Clock Prescale Select bits
//            11 = Prescaler is 64
//            10 = Prescaler is 16
//            01 = Prescaler is 4
//            00 = Prescaler is 1
#define TIMER2_PERIOD       0x98            // Value loaded to Timer 2
#define TIMER2_OFFSET       0x58            // Set to 0x58 for 16 per half cycle
#define TIMER2_PRESCALE     0x01            // Prescale of 4
#define TIMER2_POSTSCALE    0x03            // Postscale of 1:4

#define ADC_SETUP_TIME      0               // Time delay to account for ADC

/*
 * I/O Definitions
 */
#define L1_CURRENT          PORTAbits.RA0
#define L2_CURRENT          PORTAbits.RA1
#define L3_CURRENT          PORTAbits.RA2
#define SUM_CURRENT         PORTAbits.RA3

#define BOOTLOADER          PORTAbits.RA5

#define SS                  PORTCbits.RC2   // Used as output for chip select

#define L1_ZERO_CROSSING    PORTBbits.RB0
#define L2_ZERO_CROSSING    PORTBbits.RB1
#define L3_ZERO_CROSSING    PORTBbits.RB2

#define L1_SCOPE_TRIGGER    PORTBbits.RB3
#define L2_SCOPE_TRIGGER    PORTBbits.RB4
#define L3_SCOPE_TRIGGER    PORTBbits.RB5
#define SUM_SCOPE_TRIGGER   PORTCbits.RC0

#define L1_INTERRUPT_FLAG   IOCBFbits.IOCBF0
#define L2_INTERRUPT_FLAG   IOCBFbits.IOCBF1
#define L3_INTERRUPT_FLAG   IOCBFbits.IOCBF2

#define SCK                 PORTCbits.RC3
#define SDI                 PORTCbits.RC4
#define SDO                 PORTCbits.RC5

#define TX                  PORTCbits.RC6
#define RX                  PORTCbits.RC7

#define PACKET_SIZE         4               // Size of packets in bytes

#define ADC_DELAY           1               // delay between setting channel
                                            //  and starting comversion (usec)
#define WAIT_DELAY          1               // gives a delay of ~10 usec

/*
*******************************************************************************
* EXPORTED - Public Types
*******************************************************************************
*/

/*
*******************************************************************************
* EXPORTED - Public Constants
*******************************************************************************
*/

/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

/*
*******************************************************************************
* EXPORTED - Functions
*******************************************************************************
*/

#endif	/* _FDM_INCLUDE */

/*
*********************** End of file *******************************************
*/









