/*! \file
*******************************************************************************
*       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
*                       - Automation Dept.
*******************************************************************************
*    FILE NAME:
*               $Id$
*               $HeadURL$
*
*    DESCRIPTION:
*		\brief  External variable definitions
*
*
*
*    CURRENT REVISION
*
*               $Rev:: $: (Revision of last commit)
*               $Author:: $: (Author of last commit)
*       \date   $Date:: $: (Date of last commit)
*******************************************************************************
*/

/*
*******************************************************************************
*    REVISION HISTORY
*
*   Date          EAR/Reason      Name            Details
*   ---------------------------------------------------------------------------
*   02/05/2014    FPM Module      Steve Walde    Initial version.
*
*******************************************************************************
*/

/*
*******************************************************************************
*   Copyright
*       This Document is the property of Lucy Switchgear Ltd.
*       It must not be reproduced, in whole or in part, or otherwise
*       disclosed without prior consent in writing from
*       Lucy Switchgear Ltd.
*
*******************************************************************************
*/

#ifndef _GLOBALVARIABLES_INCLUDE
#define	_GLOBALVARIABLES_INCLUDE


/*
*******************************************************************************
* EXPORTED - Public Variables
*******************************************************************************
*/

extern volatile lu_bool_t   ADCL1ConvRequired;
extern volatile lu_bool_t   ADCL2ConvRequired;
extern volatile lu_bool_t   ADCL3ConvRequired;
extern volatile lu_bool_t   ADCSumConvRequired;
extern volatile lu_bool_t                                                                                                                                                                                  ADCSumPkConvRequired;

extern volatile lu_bool_t   ADCResultReady;

extern volatile lu_bool_t   PhaseDataAbsent;
extern volatile lu_uint8_t  ZeroPhaseDataSend;

#endif	/* _GLOBALVARIABLES_INCLUDE */

/*
*********************** End of file *******************************************
*/
