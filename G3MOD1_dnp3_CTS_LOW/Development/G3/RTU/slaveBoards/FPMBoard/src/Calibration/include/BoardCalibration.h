/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARD_CALIBRATION_INCLUDED
#define _BOARD_CALIBRATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Calibration.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_CAL_ID 		    CAL_ID_LAST


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! Calibration ID - Calibration Elements */
typedef enum
{
	CAL_ID_5V_MEAS       = 0,
	CAL_ID_3_3V_MEAS        ,
	CAL_ID_AINPUT_A1        ,
	CAL_ID_AINPUT_A2        ,
	CAL_ID_AINPUT_A3        ,
	CAL_ID_3_3V_ISO_MEAS    ,
	CAL_ID_24V_ISO_MEAS     ,

	CAL_ID_FPI1_PHASE_A     ,
	CAL_ID_FPI1_PHASE_B     ,
	CAL_ID_FPI1_PHASE_C     ,
	CAL_ID_FPI1_PHASE_N     ,

	CAL_ID_FPI2_PHASE_A     ,
	CAL_ID_FPI2_PHASE_B     ,
	CAL_ID_FPI2_PHASE_C     ,
	CAL_ID_FPI2_PHASE_N     ,

	CAL_ID_LAST
} CAL_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern CalElementStr boardCalTable[MAX_CAL_ID + 1];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationInit(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationGetTemperature(lu_int32_t *tempValuePtr);


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);


#endif /* _BOARD_CALIBRATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
