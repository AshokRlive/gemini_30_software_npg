/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX			(IO_ID_LAST)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IO_ID_CAN1_RX	                       =0 ,
	IO_ID_CAN1_TX	                          ,

	IO_ID_UART_DEBUG_TX	                      ,
	IO_ID_UART_DEBUG_RX	                      ,

	IO_ID_PIC1_UART_RX                        ,
	IO_ID_PIC1_UART_TX                        ,

	IO_ID_PIC2_UART_RX                        ,
	IO_ID_PIC2_UART_TX                        ,

	IO_ID_PIC1_SS							  ,
	IO_ID_PIC1_SCK							  ,
	IO_ID_PIC1_SDI							  ,
	IO_ID_PIC1_SDO							  ,

	IO_ID_PIC2_SS							  ,
	IO_ID_PIC2_SCK							  ,
	IO_ID_PIC2_SDI							  ,
	IO_ID_PIC2_SDO							  ,

	IO_ID_I2C_SCL                             ,
	IO_ID_I2C_SDA                             ,

	IO_ID_I2C1_CLK                            ,
	IO_ID_I2C1_SDA                            ,

	IO_ID_ADC_CLK                             ,
	IO_ID_ADC_DIN                             ,
	IO_ID_ADC_DOUT                            ,

	/*********************************/
	/* Physically connected IO Pins  */
	/* (Some Virt for NXP dev board) */
	/* (But will be physical on PSM) */
	/*********************************/

	IO_ID_FACTORY                             ,

	IO_ID_HWDOG_1HZ_KICK                      ,

	IO_ID_APPRAM_WEN                          ,

	IO_ID_LED_CTRL_OK_GREEN                   ,
	IO_ID_LED_CTRL_OK_RED                     ,
	IO_ID_LED_CTRL_CAN_GREEN                  ,
	IO_ID_LED_CTRL_CAN_RED                    ,
	IO_ID_LED_CTRL_EXTINGUISH                 ,

	IO_ID_LED_FPI1                            ,
	IO_ID_LED_EFI1                            ,
	IO_ID_LED_FPI2                            ,
	IO_ID_LED_EFI2                            ,

	IO_ID_MOD_SEL1							  ,
	IO_ID_MOD_SEL2							  ,
	IO_ID_MOD_SEL3							  ,
	IO_ID_MOD_SEL4							  ,

	IO_ID_ADC_CS                              ,

	IO_ID_PIC1_BL_IND                         ,
	IO_ID_PIC1_MEMCLR                         ,
	IO_ID_PIC2_BL_IND                         ,
	IO_ID_PIC2_MEMCLR                         ,

	IO_ID_5V_MEAS                             ,
	IO_ID_3_3V_MEAS                           ,

	/* SPI ADC */
	IO_ID_AINPUT_A1                           ,
	IO_ID_AINPUT_A2                           ,
	IO_ID_AINPUT_3_3V_ISO_MEAS                ,
	IO_ID_AINPUT_24V_ISO_MEAS                 ,
	IO_ID_AINPUT_A3                           ,

	IO_ID_TEMPERATURE                         ,

	/* I2C Expanders */

	IO_ID_FPI1_PHASE_A_CURRENT                ,
	IO_ID_FPI1_PHASE_B_CURRENT                ,
	IO_ID_FPI1_PHASE_C_CURRENT                ,
	IO_ID_FPI1_PHASE_N_CURRENT                ,
	IO_ID_FPI1_PHASE_N_CURRENT_DETECT		  ,
	IO_ID_FPI2_PHASE_A_CURRENT                ,
	IO_ID_FPI2_PHASE_B_CURRENT                ,
	IO_ID_FPI2_PHASE_C_CURRENT                ,
	IO_ID_FPI2_PHASE_N_CURRENT                ,
	IO_ID_FPI2_PHASE_N_CURRENT_DETECT		  ,

	IO_ID_SYS_HEALTHY                         ,
	IO_ID_WD_HANDSHAKE                        ,

	IO_ID_APPLICATION_NVRAM                   ,
	IO_ID_IDENTITY_NVRAM                      ,

	/******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IO_ID_FPI1_COMMON_FAULT                   ,
	IO_ID_FPI1_PHASE_FAULT                    ,
	IO_ID_FPI1_INST_PHASE_FAULT               ,
	IO_ID_FPI1_EARTH_FAULT                    ,
	IO_ID_FPI1_INST_EARTH_FAULT               ,
	IO_ID_FPI2_COMMON_FAULT                   ,
	IO_ID_FPI2_PHASE_FAULT                    ,
	IO_ID_FPI2_INST_PHASE_FAULT               ,
	IO_ID_FPI2_EARTH_FAULT                    ,
	IO_ID_FPI2_INST_EARTH_FAULT               ,

	IO_ID_FPI1_TEST_ACTIVE                    ,
	IO_ID_FPI2_TEST_ACTIVE                    ,

	IO_ID_LAST
} IO_ID;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
