/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "FrontEndPIC.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_RX, 	0)

	IOM_GPIO_PERIPH(IO_ID_PIC1_UART_RX,          PINSEL_PORT_0,  PINSEL_PIN_10,  \
			FUNC_UART_2_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_PIC1_UART_TX,          PINSEL_PORT_0,  PINSEL_PIN_11,  \
			FUNC_UART_2_RX, 	0)

	IOM_GPIO_PERIPH(IO_ID_PIC2_UART_RX,          PINSEL_PORT_0,  PINSEL_PIN_2,  \
			FUNC_UART_0_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_PIC2_UART_TX,          PINSEL_PORT_0,  PINSEL_PIN_3,  \
			FUNC_UART_0_RX, 	0)

	IOM_GPIO_PERIPH(IO_ID_PIC1_SS,	 	 	 	 PINSEL_PORT_1,  PINSEL_PIN_21,   \
			FUNC_SSP0_SEL, 		0)
	IOM_GPIO_PERIPH(IO_ID_PIC1_SCK,	 	 	 	 PINSEL_PORT_1,  PINSEL_PIN_20,   \
			FUNC_SSP0_SCK, 		0)
	IOM_GPIO_PERIPH(IO_ID_PIC1_SDI,		     	 PINSEL_PORT_1,  PINSEL_PIN_23,   \
			FUNC_SSP0_MISO, 	0)
	IOM_GPIO_PERIPH(IO_ID_PIC1_SDO,		     	 PINSEL_PORT_1,  PINSEL_PIN_24,   \
			FUNC_SSP0_MOSI, 	0)

	IOM_GPIO_PERIPH(IO_ID_PIC2_SS,	 	 	 	 PINSEL_PORT_0,  PINSEL_PIN_6,   \
			FUNC_SSP1_SEL, 		0)
	IOM_GPIO_PERIPH(IO_ID_PIC2_SCK,	 	 	 	 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		0)
	IOM_GPIO_PERIPH(IO_ID_PIC2_SDI,		     	 PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	0)
	IOM_GPIO_PERIPH(IO_ID_PIC2_SDO,		     	 PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	0)


	IOM_GPIO_PERIPH(IO_ID_I2C_SCL,				 PINSEL_PORT_0,  PINSEL_PIN_28,  \
			FUNC_I2C_0_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_27,  \
			FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_ADC_CLK,			 	 PINSEL_PORT_0,  PINSEL_PIN_15,  \
			FUNC_SPI_SCK, 		0)
	IOM_GPIO_PERIPH(IO_ID_ADC_DIN,			     PINSEL_PORT_0,  PINSEL_PIN_18,  \
			FUNC_SPI_MOSI, 	0)
	IOM_GPIO_PERIPH(IO_ID_ADC_DOUT,		     	 PINSEL_PORT_0,  PINSEL_PIN_17,  \
			FUNC_SPI_MISO, 	0)


	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,   		GPIO_PM_PULL_UP)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
					PINSEL_PORT_2,  PINSEL_PIN_8,  0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   		0)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    		GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_FPI1, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_22,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_EFI1, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_25,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_FPI2, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_18,    		GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_EFI2, 	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_19,    		GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_INPUT( IO_ID_MOD_SEL1, 			   	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_0,   	(GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL2, 			   	IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_1,   (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL3, 			   	IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_4,   (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))
	IOM_GPIO_INPUT( IO_ID_MOD_SEL4, 			   	IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_8,   (GPIO_PM_PULL_UP | GPIO_PM_INPUT_INVERT))

	IOM_GPIO_OUTPUT( IO_ID_ADC_CS, 					IO_CH_NA, \
					PINSEL_PORT_0,  PINSEL_PIN_16,   0)

	IOM_GPIO_OUTPUT( IO_ID_PIC1_BL_IND, 					IO_CH_NA, \
						PINSEL_PORT_2,  PINSEL_PIN_0,   0)
	IOM_GPIO_OUTPUT( IO_ID_PIC1_MEMCLR, 					IO_CH_NA, \
						PINSEL_PORT_2,  PINSEL_PIN_1,   (GPIO_PM_OUTPUT_LOW | GPIO_PM_FEPIC_RST1))
	IOM_GPIO_OUTPUT( IO_ID_PIC2_BL_IND, 					IO_CH_NA, \
						PINSEL_PORT_3,  PINSEL_PIN_25,   0)
	IOM_GPIO_OUTPUT( IO_ID_PIC2_MEMCLR, 					IO_CH_NA, \
						PINSEL_PORT_3,  PINSEL_PIN_26,   (GPIO_PM_OUTPUT_LOW | GPIO_PM_FEPIC_RST2))

	IOM_PERIPH_AIN( IO_ID_5V_MEAS, 		FPM_CH_AINPUT_5V_MEAS, \
				PINSEL_PORT_1,  PINSEL_PIN_30,  FUNC_AD0_CH_4)
	IOM_PERIPH_AIN( IO_ID_3_3V_MEAS,    		FPM_CH_AINPUT_3_3V_MEAS, \
			PINSEL_PORT_1,  PINSEL_PIN_31,  FUNC_AD0_CH_5)

	/* SPI attached ADC (MCP3204/MCP3208) */
	IOM_SPI_AIN(IO_ID_AINPUT_A1, 				FPM_CH_AINPUT_A1, \
					IO_BUS_SPI_SPI, SPI_ADC_CH_SE_0, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_A2, 				FPM_CH_AINPUT_A2, \
					IO_BUS_SPI_SPI, SPI_ADC_CH_SE_1, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_3_3V_ISO_MEAS, 				FPM_CH_AINPUT_3_3V_ISO_MEAS, \
					IO_BUS_SPI_SPI, SPI_ADC_CH_SE_2, IO_ID_ADC_CS)
	IOM_SPI_AIN(IO_ID_AINPUT_24V_ISO_MEAS, 				FPM_CH_AINPUT_24V_ISO_MEAS, \
					IO_BUS_SPI_SPI, SPI_ADC_CH_SE_3, IO_ID_ADC_CS)

	IOM_VIRT_AI(IO_ID_AINPUT_A3, 				FPM_CH_AINPUT_A3)

	IOM_I2CTEMP_AIN(IO_ID_TEMPERATURE, 			    FPM_CH_AINPUT_TEMPERATURE, \
					IO_BUS_I2C_0, 	0x01, 0)

	IOM_SPI_FEPIC_AIN(IO_ID_FPI1_PHASE_A_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_A_CURRENT, \
					IO_BUS_SPI_SSPI_0, FEPIC_ADC_CH_L1)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI1_PHASE_B_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_B_CURRENT, \
					IO_BUS_SPI_SSPI_0, FEPIC_ADC_CH_L2)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI1_PHASE_C_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_C_CURRENT, \
					IO_BUS_SPI_SSPI_0, FEPIC_ADC_CH_L3)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI1_PHASE_N_CURRENT, FPM_CH_AINPUT_FPI1_PHASE_N_CURRENT, \
					IO_BUS_SPI_SSPI_0, FEPIC_ADC_CH_SUM_PK)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI1_PHASE_N_CURRENT_DETECT, IO_CH_NA, \
					IO_BUS_SPI_SSPI_0, FEPIC_ADC_CH_SUM)

	IOM_SPI_FEPIC_AIN(IO_ID_FPI2_PHASE_A_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_A_CURRENT, \
					IO_BUS_SPI_SSPI_1, FEPIC_ADC_CH_L1)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI2_PHASE_B_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_B_CURRENT, \
					IO_BUS_SPI_SSPI_1, FEPIC_ADC_CH_L2)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI2_PHASE_C_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_C_CURRENT, \
					IO_BUS_SPI_SSPI_1, FEPIC_ADC_CH_L3)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI2_PHASE_N_CURRENT, FPM_CH_AINPUT_FPI2_PHASE_N_CURRENT, \
					IO_BUS_SPI_SSPI_1, FEPIC_ADC_CH_SUM_PK)
	IOM_SPI_FEPIC_AIN(IO_ID_FPI2_PHASE_N_CURRENT_DETECT, IO_CH_NA, \
					IO_BUS_SPI_SSPI_1, FEPIC_ADC_CH_SUM)

	/* I2C IO Expanders */

	IOM_GPIO_INPUT( IO_ID_SYS_HEALTHY, 		   	    IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_11,   0)
	IOM_GPIO_INPUT( IO_ID_WD_HANDSHAKE, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_13,   0)

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	IOM_VIRT_DI(IO_ID_FPI1_COMMON_FAULT,    	FPM_CH_DINPUT_FPI1_COMMON_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_PHASE_FAULT,         FPM_CH_DINPUT_FPI1_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_INST_PHASE_FAULT,    FPM_CH_DINPUT_FPI1_INST_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_EARTH_FAULT,    		FPM_CH_DINPUT_FPI1_EARTH_FAULT)
	IOM_VIRT_DI(IO_ID_FPI1_INST_EARTH_FAULT,    FPM_CH_DINPUT_FPI1_INST_EARTH_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_COMMON_FAULT,    	FPM_CH_DINPUT_FPI2_COMMON_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_PHASE_FAULT,         FPM_CH_DINPUT_FPI2_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_INST_PHASE_FAULT,    FPM_CH_DINPUT_FPI2_INST_PHASE_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_EARTH_FAULT,    		FPM_CH_DINPUT_FPI2_EARTH_FAULT)
	IOM_VIRT_DI(IO_ID_FPI2_INST_EARTH_FAULT,    FPM_CH_DINPUT_FPI2_INST_EARTH_FAULT)

	IOM_VIRT_DI(IO_ID_FPI1_TEST_ACTIVE,    		FPM_CH_DINPUT_FPI1_TEST_ACTIVE)
	IOM_VIRT_DI(IO_ID_FPI2_TEST_ACTIVE,    		FPM_CH_DINPUT_FPI2_TEST_ACTIVE)

	/* End of table marker */
	IOM_LAST
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
