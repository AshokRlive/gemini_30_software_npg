/*************************************************************************
 *
 *   Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2011
 *
 *    File name   : FlashHitexLPC1850_NOR.c
 *    Description : FlashLoader For Hitex LPC1850 evaluation board NOR flash
 *
 *    History :
 *    History :
 *    1. Date        : November 21, 2008
 *       Author      : Stanimir Bonev
 *       Description : new style flash loader for Toshiba TOPAS 910 evaluation
 *                     board NOR Flash
 *    2. Date        : October 14, 2011
 *       Author      : Atanas Uzunov
 *       Description : ported for Hitex LPC4350 board NOR flash SST39VF3201B
 *    3. Date        : November 28, 2011
 *       Author      : Stanimir Bonev
 *       Description : ported for Hitex LPC1850 board NOR flash SST39VF3201B/SST39VF3201
 *    4. Date        : November 28, 2011
 *       Author      : Stanimir Bonev
 *       Description : Remove CFI query (unreliable data), The flashloader
 *                     should support SST39VF1601,SST39VF1602,SST39VF1601C,SST39VF1602C
 *                     SST39VF3201,SST39VF3202,SST39VF3201B,SST39VF3202B
 *                     SST39VF3201C,SST39VF3202C
 *
 *
 *    $Revision: 51034 $
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <yfuns.h>

#include "flash_loader.h" // The flash loader framework API declarations.
#include "flash_loader_extra.h"

// Flash-specific definitions
// These encompass all the commands and status checks the program makes.
#define MULT                0x00001

/*
** define flash command codes D0 - D15 Chip1,  D16 - D32 Chip2
*/
#define FLASH_A_SEQ_ADD1    0x5555
#define FLASH_A_SEQ_ADD2    0x2AAA

#define FLASH_RESET         (0x00F0 * MULT)

#define FLASH_CODE1         (0x00AA * MULT)
#define FLASH_CODE2         (0x0055 * MULT)

#define ID_IN_CODE          (0x0090 * MULT)

#define UNLOCK_CODE         (0x0070 * MULT)

#define UNLOCK_BYPASS       (0x0020 * MULT)
#define LOCK_BYPASS1        (0x0090 * MULT)
#define LOCK_BYPASS2        (0x0000 * MULT)

#define CFG_REG_CODE        (0x00C0 * MULT)
#define WRITE_CODE          (0x00A0 * MULT)
#define ERASE_SECTOR_CODE1  (0x0080 * MULT)

#define FLASH_SW_ID         (0x0090 * MULT)
#define SST_ID              (0x00BF * MULT)

#define QUERY_CFI           (0x0098 * MULT)
#define BIT_TOGGLE          ((1<<6) * MULT)

#pragma pack(1)

typedef struct
{
  unsigned short BlockNumb;
  unsigned short BlockSize;
} SectorInfo_t, *pSectorInfo_t;

typedef struct
{
  uint16_t dev_id;
  uint32_t size;
  uint16_t erase_cmd2;
} dev_entry_t;

#pragma pack()

#define MB  (1024UL*1024UL)
#define KB  (1024UL)


const dev_entry_t DevBootTable[] =
{
  {0x234B, 2*MB, 0x30},	// SST39VF1601
  {0x234A, 2*MB, 0x30},	// SST39VF1602
  {0x234F, 2*MB, 0x50},	// SST39VF1601C
  {0x234E, 2*MB, 0x50},	// SST39VF1602C
  {0x235B, 4*MB, 0x30},	// SST39VF3201
  {0x235A, 4*MB, 0x30},	// SST39VF3202
  {0x235D, 4*MB, 0x50},	// SST39VF3201B
  {0x235C, 4*MB, 0x50},	// SST39VF3202B
  {0x235F, 4*MB, 0x50},	// SST39VF3201C
  {0x235E, 4*MB, 0x50},	// SST39VF3202C
};

typedef uint16_t flashunit;

/** default settings **/

/** external functions **/

/** external data **/

/** internal functions **/
static uint32_t FlashPrepare(void);
static uint32_t Query (char * Buffer);
static uint32_t FlashEraseSector(uint32_t SectorAddr);
static uint32_t FlashWriteUnit(uint32_t LoadAddr, flashunit Data);
static void FlashCleanup(void);

void cSpyMessageLog(char* msg)
{
    int messageLogFile = -1;
    messageLogFile = __open("$DEBUG_LOG$", _LLIO_CREAT | _LLIO_TRUNC | _LLIO_WRONLY);
     __write(messageLogFile, (unsigned char *)msg, strlen(msg));
     __close(messageLogFile);
}    
void cSpyMessageBox(char* msg)
{
    int log_fd;
    log_fd = __open("$MESSAGE_BOX$", _LLIO_CREAT | _LLIO_TRUNC | _LLIO_WRONLY);
    __write(log_fd, (unsigned char *)msg, strlen(msg));
    __close(log_fd);
}

static char message[80];

/** public data **/
/** private data **/
static volatile uint32_t flashBase;
static const dev_entry_t * pdev;

/** public functions **/

/*************************************************************************
 * Function Name: FlashInit
 * Parameters: Flash Base Address
 *
 * Return:  0 - Init Successful
 *          1 - Init Fail
 * Description: Init flash and build layout.
 *
 *************************************************************************/
#if USE_ARGC_ARGV
uint32_t FlashInit(void *base_of_flash, uint32_t image_size,
                   uint32_t link_address, uint32_t flags,
                   int argc, char const *argv[])
#else
uint32_t FlashInit(void *base_of_flash, uint32_t image_size,
                   uint32_t link_address, uint32_t flags)
#endif /* USE_ARGC_ARGV */
{
//    sprintf(message, "%s", "FlashInit");
//    cSpyMessageBox(message);

  /* Init flash */
  flashBase = (uint32_t)base_of_flash;
  FlashPrepare();
  /* Build NOR flash layout */
  return Query((char*)theFlashParams.buffer);
}

/*************************************************************************
 * Function Name: FlashWrite
 * Parameters: block base address, offet in block, data size, ram buffer
 *             pointer
 * Return:  0 - Write Successful
 *          1 - Write Fail
 * Description. Writes data in to NOR
 *************************************************************************/
uint32_t FlashWrite(void *block_start,
                    uint32_t offset_into_block,
                    uint32_t count,
                    char const *buffer)
{
uint32_t LoadAddr = (uint32_t)block_start + offset_into_block;
flashunit * pData = (flashunit *)buffer;
  do
  {
    if(RESULT_OK != FlashWriteUnit(LoadAddr, *pData))
    {
      return(RESULT_ERROR);
    }
    ++pData;
    LoadAddr += sizeof(flashunit);
    count -= sizeof(flashunit);
  }
  while(count >= sizeof(flashunit));

  return(RESULT_OK);
}

/*************************************************************************
 * Function Name: FlashErase
 * Parameters:  Block Address, Block Size
 *
 * Return: 0
 *
 * Description: Erase block
 *************************************************************************/
uint32_t FlashErase(void *block_start,
                    uint32_t block_size)
{
  return FlashEraseSector((uint32_t)block_start);
}

/** private functions **/

static uint32_t FlashPrepare(void)
{
  // Flash reset
  *(volatile flashunit *)flashBase = (flashunit)FLASH_RESET;
  for(volatile int i = 20; i; i--);
  // Read Array
  volatile flashunit Dummy = *(flashunit *)flashBase;
  return(RESULT_OK) ;
}

static void FlashCleanup(void)
{
  // Flash reset
  *(volatile flashunit *)flashBase = (flashunit)FLASH_RESET;
  for(volatile int i = 20; i; i--);
  // Read Array
  volatile flashunit Dummy = *(flashunit *)flashBase;
}

static uint32_t Query (char * Buffer)
{
uint16_t dev_id;

  pdev = NULL;
  /* Get device ID */
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = FLASH_CODE1;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD2 ) = FLASH_CODE2;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = FLASH_SW_ID;
  if(*((volatile flashunit *)flashBase + 0x0) != SST_ID)
  {
    FlashCleanup();
    return(RESULT_ERROR);
  }
  dev_id = *((volatile flashunit *)flashBase + 0x01);
  FlashCleanup();

  for(int i = 0; i < sizeof(DevBootTable)/sizeof(DevBootTable[0]); i++)
  {
    if(DevBootTable[i].dev_id == dev_id)
    {
      pdev = &DevBootTable[i];
      break;
    }
  }

  if(NULL == pdev)
  {
    return(RESULT_ERROR);
  }

  sprintf(Buffer,"%d 0x%X, ",pdev->size/(4*KB),4*KB); // 2KWord by 16 bits
  return(OVERRIDE_LAYOUT);
}

static uint32_t FlashWriteUnit(uint32_t LoadAddr, flashunit Data)
{
  if(!pdev)
  {
    return(RESULT_ERROR);
  }
  /* enter programming code sequence */
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = FLASH_CODE1;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD2 ) = FLASH_CODE2;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = WRITE_CODE;
  *( ( volatile flashunit * )LoadAddr ) = Data ;
  /* use the toggle function for bussy check */
  while((*(volatile flashunit*)LoadAddr ^ *(volatile flashunit *)LoadAddr) & BIT_TOGGLE);
  return( RESULT_OK ) ;
}

static uint32_t FlashEraseSector(uint32_t SectorAddr)
{

  if(!pdev)
  {
    return(RESULT_ERROR);
  }

  /* enter sector erase sequence codes */
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = FLASH_CODE1;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD2 ) = FLASH_CODE2;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = ERASE_SECTOR_CODE1;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD1 ) = FLASH_CODE1;
  *( ( volatile flashunit * )flashBase + FLASH_A_SEQ_ADD2 ) = FLASH_CODE2;
  *(   volatile flashunit * )SectorAddr                     = pdev->erase_cmd2;

  /* use the toggle function for bussy check */
  while((*(volatile flashunit*)SectorAddr ^ *(volatile flashunit *)SectorAddr) & BIT_TOGGLE);
  return( RESULT_OK ) ;
}
