
DESCRIPTION
===========

Flashloader for LPC18xx SPIFI flash memories.

CONFIGURATION
=============

The project contains configuration:

- Debug
Builds a flashloader for LPC18xx SPIFI flash memories
Output file: FlashLPC18xx_SPIFI.out
Macro file: FlashLPC18xx_SPIFI.mac

USAGE
=====
The flash loader assumes that the application code is linked on address 0x00000000.
