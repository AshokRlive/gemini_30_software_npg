/*************************************************************************
 *
 *   Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2007
 *
 *    File name   : uart.c
 *    Description : uart module functions
 *
 *    History :
 *    1. Dat�        : 14.11.2007 �.
 *       Author      : Stoyan Choynev
 *       Description : initial version
 *
 *    $Revision: 50467 $
 **************************************************************************/
#ifdef DEBUG
/** include files **/

#if __CORE__ < 7
#include <NXP\iolpc2378.h>
#define PERIPHERALS_ON_MASK 0x00c8179E
#else
#include <NXP\iolpc1766.h>
#define PERIPHERALS_ON_MASK 0x042887DE
#endif
#include "uart.h"
#include <stdio.h>

/** local definitions **/
#define PCUART0 3

/** public functions **/
/*************************************************************************
 * Function Name: UartInit
 * Parameters: None
 *
 * Return: None
 *
 * Description: Initialization of the UART module.
 *
 *************************************************************************/

void UartInit ( Lpc2103_Uart * pUart )
{
  PCLKSEL0_bit.PCLK_UART0 = 1;
  PINSEL0_bit.P0_2 = 1;         //Uart TX function select
  PINSEL0_bit.P0_3 = 1;         //Uart RX function select
  PCONP = (PCONP & PERIPHERALS_ON_MASK | (1<<PCUART0));        //Enable UART0 module

  pUart->Lcr_bit.WLS = 3;       //Serlect 8-Bits
  pUart->Lcr_bit.SBS = 0;       //One Stop Bit
  pUart->Lcr_bit.PE = 0;        //No parity

  pUart->Lcr_bit.DLAB = 1;      //Devisor Latch Access ebabled
  pUart->Dll = 4;              //Select 115200 bps
  pUart->Dlm = 0;               //
  pUart->Lcr_bit.DLAB = 0;      //Clear Devisor Latch Access bit

  pUart->Fdr = 216;

  pUart->Fcr = (1<<FIFO_EN);
  pUart->Ter = (1<<TX_EN);
}
/*************************************************************************
 * Function Name: write
 * Parameters: puts one Byte in the Uart Tx Buffer
 *
 * Return:
 *
 * Description:
 *
 *************************************************************************/

size_t __write(int Handle, const unsigned char * Buf, size_t Bufsize)
{
size_t nChars = 0;
Lpc2103_Uart * pUart = UART0_BASE;

for (/*Empty */; Bufsize > 0; --Bufsize)
  {
    while( !pUart->Lsr_bit.THRE );  //Wait
    pUart->Thr = * Buf++;
    ++nChars;
  }
  return nChars;
}
#endif /*DEBUG*/
