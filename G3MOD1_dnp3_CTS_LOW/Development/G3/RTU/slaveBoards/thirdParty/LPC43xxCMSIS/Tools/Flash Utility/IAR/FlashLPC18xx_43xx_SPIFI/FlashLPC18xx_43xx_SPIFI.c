/*************************************************************************
 *
 *   Used with ICCARM and AARM.
 *
 *    (c) Copyright IAR Systems 2012
 *
 *    File name   : FlashLPC18xx_SPIFI.c
 *    Description : FlashLoader For Hitex LPC1850 evaluation board SPI flash
 *
 *    History :
 *    1. Date        : November 29, 2011
 *       Author      : Stanimir Bonev
 *       Description : new style SPIFI flash loader for Hitex LPC1850 board
 *
 *    $Revision: 50311 $
 **************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "flash_loader.h" // The flash loader framework API declarations.
#include "flash_loader_extra.h"
#include "spifi_rom_api.h"

typedef uint16_t flashunit;

/** default settings **/

/** external functions **/

/** external data **/

/** internal functions **/

/** public data **/
/** private data **/
static volatile uint32_t flashBase;
static SPIFIobj Obj;

/** public functions **/
/*************************************************************************
 * Function Name: FlashInit
 * Parameters: Flash Base Address
 *
 * Return:  0 - Init Successful
 *          1 - Init Fail
 * Description: Init flash and build layout.
 *
 *************************************************************************/

#if USE_ARGC_ARGV
uint32_t FlashInit(void *base_of_flash, uint32_t image_size,
                   uint32_t link_address, uint32_t flags,
                   int argc, char const *argv[])
#else
uint32_t FlashInit(void *base_of_flash, uint32_t image_size,
                   uint32_t link_address, uint32_t flags)
#endif /* USE_ARGC_ARGV */
{
int status;
uc erase_shift;
  flashBase =  (uint32_t)base_of_flash;
  status = spifi_init(&Obj, 3, S_RCVCLK | S_FULLCLK, 12);
  if(status)
  {
    return(RESULT_ERROR);
  }
  Obj.base = flashBase;

  /* Build device layout */
  for(int i = 3; i >= 0; i--)
  {
    // Take lagest eraseble unit
    if ((erase_shift = Obj.erase_shifts[i])) break;
  }

  sprintf((char*)theFlashParams.buffer,"%d 0x%X",Obj.memSize >> erase_shift,1ul << erase_shift);

  return(OVERRIDE_LAYOUT);
}

/*************************************************************************
 * Function Name: FlashWrite
 * Parameters: block base address, offet in block, data size, ram buffer
 *             pointer
 * Return:  0 - Write Successful
 *          1 - Write Fail
 * Description. Writes data in to NOR
 *************************************************************************/
uint32_t FlashWrite(void *block_start,
                    uint32_t offset_into_block,
                    uint32_t count,
                    char const *buffer)
{
  int status;
  SPIFIopers opers;
  opers.dest = (char *)((uint32_t)block_start+offset_into_block);
  opers.length = count;
  opers.scratch = NULL;
  opers.protect = 0;
  opers.options = 0;
  status = spifi_program(&Obj, (char *)buffer, &opers);
	return (!status?RESULT_OK:RESULT_ERROR);
}

/*************************************************************************
 * Function Name: FlashErase
 * Parameters:  Block Address, Block Size
 *
 * Return: 0
 *
 * Description: Erase block
 *************************************************************************/
uint32_t FlashErase(void *block_start,
                    uint32_t block_size)
{
  int status;
  SPIFIopers opers;
  opers.dest = (char *)((uint32_t)block_start);
  opers.length = block_size;
  opers.scratch = NULL;
  opers.protect = 0;
  opers.options = 0;
  status = spifi_erase(&Obj, &opers);
	return (!status?RESULT_OK:RESULT_ERROR);
}

/** private functions **/