/***********************************************************************/
/*  This file is part of the uVision/ARM development tools             */
/*  Copyright (c) 2003-2010 Keil Software. All rights reserved.        */
/***********************************************************************/
/*                                                                     */
/*  FlashDev.C:  Device Description for NXP LPC11xx/13xx/LPC17xx Flash */
/*                                                                     */
/***********************************************************************/

#include "..\FlashOS.H"        // FlashOS Structures

struct FlashDevice const FlashDevice  =  {
   FLASH_DRV_VERS,             // Driver Version, do not modify!
#ifdef BANK_A
   "LPC1857A BankA IAP Flash", // Device Name 
#else
   "LPC1857A BankB IAP Flash", // Device Name 
#endif
//   "LPC1857A aaaaaaaaaaaaaaa", // Device Name 
   ONCHIP,                     // Device Type
#ifdef BANK_A
   0x1A000000UL,               // Device Start Address
#else
   0x1B000000UL,               // Device Start Address
#endif
   0x00080000UL,               // Device Size (512kB)
   1024,                       // Programming Page Size
   0,                          // Reserved, must be 0
   0xFF,                       // Initial Content of Erased Memory
   5000,                       // Program Page Timeout 5000 mSec
   5000,                       // Erase Sector Timeout 5000 mSec

// Specify Size and Address of Sectors
   0x002000, 0x000000UL,       // Sector Size  8kB (8 Sectors)
   0x010000, 0x010000UL,       // Sector Size 64kB (7 Sectors) 
   SECTOR_END
};
