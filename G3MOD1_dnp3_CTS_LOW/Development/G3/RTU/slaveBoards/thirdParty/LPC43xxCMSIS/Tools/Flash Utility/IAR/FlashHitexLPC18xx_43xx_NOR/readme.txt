
DESCRIPTION
===========

Flashloader for Hitex LPC1850 evaluation board.

CONFIGURATION
=============

The project contains configuration:

- Debug
Builds a flashloader for LPC1850 evaluation board
Output file: FlashHitexLPC1850_NOR.out
Macro file: FlashHitexLPC1850_NOR.mac

USAGE
=====
The flash loader assumes that the application code is linked on address 0x00000000.
Supported devices : SST39VF1601,SST39VF1602,SST39VF1601C,SST39VF1602C,SST39VF3201,
SST39VF3202,SST39VF3201B,SST39VF3202B, SST39VF3201C,SST39VF3202C