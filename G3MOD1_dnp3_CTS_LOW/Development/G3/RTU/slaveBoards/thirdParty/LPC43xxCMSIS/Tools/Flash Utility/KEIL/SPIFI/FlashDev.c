/***********************************************************************/
/*  This file is part of the ARM Toolchain package                     */
/*  Copyright (c) 2010 Keil - An ARM Company. All rights reserved.     */
/***********************************************************************/
/*                                                                     */
/*  FlashDev.C:  Device Description for serial quad SPI flash devices, */
/*               used on LPC1850/LPC4350 boards.                       */
/*                                                                     */
/*  There are two memory areas for the SPIFI code:                     */
/*  - 0x14000000 to 0x17FFFFFF:    64MB                                */
/*  - 0x80000000 to 0x87FFFFFF:   128MB                                */
/*                                                                     */
/*  Remark: These are the maximum areas allocated to the SPIFI.        */ 
/*  In practice, the usable space is limited to the size of the        */
/*  the connected device.                                              */
/*  If the program counter is in the higher memory area, debugging     */
/*  with JTAG/SWD is not possible.                                     */
/*  The same data appears in the first area and the first half of      */
/*  the second area.                                                   */ 
/*                                                                     */
/***********************************************************************/

#include "../FlashOS.H"        // FlashOS Structures
#include "spifi_rom_api.h"


struct FlashDevice const FlashDevice = {
   FLASH_DRV_VERS,             // Driver Version, do not modify!
   //"SPI-Flash_LPC18xx@0x80000000",  // Device Name 
   "SPI-Flash_LPC18xx@0x14000000",  // Device Name 
   EXTSPI,                     // Device Type
   //0x80000000,                 // Device Start Address
   0x14000000,                 // Device Start Address
   0x08000000,                 // Device Size is 64MByte max
   PROG_SIZE,                  // Programming Page Size
   0,                          // Reserved, must be 0
   0xFF,                       // Initial Content of Erased Memory
   500,                        // Program Page Timeout 100 mSec
   5000,                       // Erase Sector Timeout 3000 mSec

// Specify Size and Address of Sectors
   1024*256, 0,         		// all sectors are 256 KB
   SECTOR_END
};
