/***********************************************************************/
/*  This file is part of the ARM Toolchain package                     */
/*  Copyright KEIL ELEKTRONIK GmbH 2003 - 2006                         */
/***********************************************************************/
/*                                                                     */
/*  FlashDev.C: Adapted by NXP                                         */
/*              Device Description for SST39VF3201/3201B (16-bit Bus)  */
/*              connected to CS0 of LPC1800/LPC4300 on the Hitex board.*/
/*              This applies to Rev A2/A3/A4 boards.                   */
/*                                                                     */
/***********************************************************************/

#include "..\FlashOS.H"        // FlashOS Structures


struct FlashDevice const FlashDevice  =  {
   FLASH_DRV_VERS,                        // Driver Version, do not modify!
   "SST39VF3201x @ LPC1850-Hitex",        // Device Name
   EXT16BIT,                              // Device Type
   0x1C000000,                            // Device Start Address
   0x00400000,                            // Device Size in Bytes (4MB)
   1024,                                  // Programming Page Size
   0,                                     // Reserved, must be 0
   0xFF,                                  // Initial Content of Erased Memory
   50,                                    // Program Page Timeout 50 mSec
   100,                                   // Erase Sector Timeout 100 mSec

// Specify the sector sizes and the relative start address of the first sector
   0x001000, 0x000000,         // Sector Size 4kB (1024 Sectors)
   SECTOR_END
};
