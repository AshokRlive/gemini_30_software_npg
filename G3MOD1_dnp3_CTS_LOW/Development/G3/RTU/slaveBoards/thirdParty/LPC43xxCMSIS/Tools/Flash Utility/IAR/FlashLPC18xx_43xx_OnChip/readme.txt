DESCRIPTION
===========

Flashloader for the LPC18xx and LPC43xx.

CONFIGURATION
=============

The project contains configuration:

- FlashNXPLPC18xx_RAM64K
Builds a flashloader for the LPC18xx and LPC43xx
Output file: FlashNXPLPC18xx_RAM64K.out
Macro file: FlashNXPLPC18xx.mac

USAGE
=====
The flash loader assumes that the application code is linked on address 0x1A000000.
