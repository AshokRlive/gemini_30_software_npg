/***********************************************************************/
/*                                                                     */
/***********************************************************************/
/*                                                                     */
/*  FlashDev.C:  Device Description for Sharp LHF00L28 (16-bit, 16MBit)*/
/*               on the LPC1800/LPC4300 Eval Board v2.3                */
/*                                                                     */
/***********************************************************************/

#include "..\FlashOS.H"        // FlashOS Structures


struct FlashDevice const FlashDevice  =  {
   FLASH_DRV_VERS,             // Driver Version, do not modify!
   "LHF00L28 @ LPC4350-NXP Evalboard v2.3",           // Device Name 
   EXT16BIT,                   // Device Type
   0x1C000000,                 // Device Start Address
   0x0200000,                  // Device Size in Bytes (16MBit =2MByte)
   1024,                       // Programming Page Size
   0,                          // Reserved, must be 0
   0xFF,                       // Initial Content of Erased Memory
   100,                        // Program Page Timeout 100 mSec
   4000,                       // Erase Sector Timeout 4000 mSec

// Specify Size and Address of Sectors
   0x20000, 0x00000,		      //  sector size 128Kb = 64KWords, 15 sectors
   0x10000, 0x1E0000,			  //  sector size  64Kb = 32KWords, 1 sector
   0x2000,  0x1F0000,			  //  sector size   8Kb =  4KWords, 8 sectors
   SECTOR_END
};
