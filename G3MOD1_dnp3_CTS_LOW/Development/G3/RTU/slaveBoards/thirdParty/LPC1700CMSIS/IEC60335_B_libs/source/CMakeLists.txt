cmake_minimum_required(VERSION 2.8)

# Override default flags
include (../../../../commom/cmake/CS_NXP_lib_flags.cmake)

add_library( iec60335blib STATIC
             IEC60335_B_ClockTest.c
             IEC60335_B_CPUregTests.c
             IEC60335_B_FLASHTest.c
             IEC60335_B_Interrupts.c
             IEC60335_B_ProgramCounterTest.c
             IEC60335_B_RAMTests.c
             IEC60335_B_SecureDataStorage.c
             IEC60335_B_CPUregTestBIST_GNU.asm
             IEC60335_B_CPUregTestPOST_GNU.asm
           ) 

include_directories (../include/)
include_directories (../../Drivers/include/)
include_directories (../../Core/CM3/DeviceSupport/NXP/LPC17xx/)
include_directories (../../Core/CM3/CoreSupport/)

# Generate Doxygen documentation
gen_doxygen("iec60335blib" "")
