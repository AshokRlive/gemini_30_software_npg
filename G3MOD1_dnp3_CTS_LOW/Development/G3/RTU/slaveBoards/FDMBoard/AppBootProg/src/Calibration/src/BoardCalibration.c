/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BoardCalibration.c 2068 2012-11-26 21:12:23Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/FDMBoard/AppBootProg/src/Calibration/src/BoardCalibration.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "systemStatus.h"
#include "errorCodes.h"
#include "CANProtocolCodec.h"

#include "LinearInterpolation.h"
#include "Calibration.h"
#include "BoardCalibration.h"

#include "NVRam.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR BoardCalTestNvramSelect(CalTstNvramSelStr *calTstNvramPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


CalElementStr boardCalTable[MAX_CAL_ID + 1] =
{
	CALID_ELEMENT(CAL_ID_NOT_USED)

	/* End Table Marker */
	CALID_ELEMENT_LAST()
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardCalibrationInit(void)
{
	SB_ERROR      retError;
	lu_uint8_t    *calDataPtr;
	lu_uint16_t   calDataSize;

	return SB_ERROR_NONE;
}



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
