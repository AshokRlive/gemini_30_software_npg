/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BoardBootloader.h 4111 2013-11-12 10:48:22Z saravanan_v $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/FDMBoard/AppBootProg/src/BoardBootloader/include/BoardBootloader.h $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board bootloader module specific
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDBOOTLOADER_INCLUDED
#define _BOARDBOOTLOADER_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BRD_BL_APP_FLASH_SECTOR     0x18000
#define BOARD_MAX_FLASH_SIZE_K      512 // NXP LPC1768

#define BRD_BL_TICK_MS				50 // ms Tick rate

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Enable application Auto start
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern void BoardBootloaderAutoAppStart(void);

/*!
 ******************************************************************************
 *   \brief Board bootloader POST test
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return
 *
 ******************************************************************************
 */
extern SB_ERROR BoardBootloaderPOST(void);

/*!
 ******************************************************************************
 *   \brief Board Bootloader tick
 *
 *   Detailed description
 *
 *   \param none
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
extern SB_ERROR BoardBootloaderTick(void);

#endif /* _BOARDBOOTLOADER_INCLUDED */

/*
 *********************** End of file ******************************************
 */
