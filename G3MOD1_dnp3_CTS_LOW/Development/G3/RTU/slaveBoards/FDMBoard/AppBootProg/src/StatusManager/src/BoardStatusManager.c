/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BoardStatusManager.c 4625 2014-04-07 11:29:41Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/FDMBoard/AppBootProg/src/StatusManager/src/BoardStatusManager.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "NVRam.h"
#include "IOManager.h"
#include "IOManagerIOUpdate.h"

#include "BoardIO.h"
#include "BoardIOMap.h"

#include "systemStatus.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define PSM_PERIOD_MS			200

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

static SB_ERROR FDMPeriodicManager(void);

extern SB_ERROR WriteBootLoaderFirmware(lu_uint8_t *srcAddr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				 0, 				0 				          }, // Run all the time

	{CANFramingTick, 			 0, 				CAN_TICK_MS		          }, // Run every 10ms

	{IOManagerIORunUpdateGPIO, 	 0, 				IOMAN_UPDATE_GPIO_MS      }, // run every 10ms

	{FDMPeriodicManager,         0,                 PSM_PERIOD_MS             }, // run every 1000ms

	{(SMThreadRun)0L, 	    	 0, 				0   			          }  // end of table
};


lu_uint32_t countDownToProgamMs = 2000;
lu_uint8_t  *bootloaderSrcAddr = 0L;

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */


void BoardStatusSetBootloaderSrc(lu_uint8_t *addr, SlaveBinImageHeaderStr *slaveBinImageHeaderPtr)
{
	SB_ERROR retError;

	SlaveBinImageHeaderStr *bootloaderBinImageHeaderPtr;
	SlaveBinImageHeaderStr *bootloaderSrcBinImageHeaderPtr;

	/* Adjust pointer to point to image headers */
	bootloaderBinImageHeaderPtr    = (SlaveBinImageHeaderStr *)(0L + SLAVE_IMAGE_BASE_OFFSET);
	bootloaderSrcBinImageHeaderPtr = (SlaveBinImageHeaderStr *)(addr + SLAVE_IMAGE_BASE_OFFSET);

	/* Check if correct image type has been appended */
	if (slaveBinImageHeaderPtr->moduleType == bootloaderSrcBinImageHeaderPtr->moduleType &&
		bootloaderSrcBinImageHeaderPtr->imageType == SLAVE_IMAGE_TYPE_BOOT_LOADER
	   )
	{
		if (bootloaderBinImageHeaderPtr->postHeaderCRC32 != bootloaderSrcBinImageHeaderPtr->postHeaderCRC32 ||
			bootloaderBinImageHeaderPtr->headerCRC32     != bootloaderSrcBinImageHeaderPtr->headerCRC32 ||
			bootloaderBinImageHeaderPtr->preHeaderCRC32  != bootloaderSrcBinImageHeaderPtr->preHeaderCRC32
		   )
		{
			bootloaderSrcAddr = addr;

			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 100);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 300);
		}
		else
		{
			/* Set module status */
			SSSetBStatus(MODULE_BOARD_STATUS_APP_BOOTL_OK);
		}
	}
}

void BoardStatusManagerExecutive(void)
{
	/* Start kicking the PIC watch dog every second */
	IOManagerSetFlash(IO_ID_HWDOG_1HZ_KICK, 500);

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR FDMPeriodicManager(void)
{
	SB_ERROR retError;


	if (SSGetBStatus() == MODULE_BOARD_STATUS_APP_BOOTL_INIT)
	{
		if (bootloaderSrcAddr != 0L)
		{
			/* Set module status */
			SSSetBStatus(MODULE_BOARD_STATUS_APP_BOOTL_PROG);
		}
	}

	if (countDownToProgamMs)
	{
		if (countDownToProgamMs >= PSM_PERIOD_MS)
		{
			countDownToProgamMs -= PSM_PERIOD_MS;
		}
		else
		{
			countDownToProgamMs = 0;
		}

		if (!countDownToProgamMs)
		{
			if (bootloaderSrcAddr != 0L)
			{
				if (WriteBootLoaderFirmware(bootloaderSrcAddr) == SB_ERROR_NONE)
				{
					/* Set module status */
					SSSetBStatus(MODULE_BOARD_STATUS_APP_BOOTL_OK);

					retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 1);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
				}
				else
				{
					/* Set module status */
					SSSetBStatus(MODULE_BOARD_STATUS_APP_BOOTL_FAIL);

					retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 300);
					retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
				}
			}
			else
			{
				retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
				retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 300);
				retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			}
		}
	}

    return SB_ERROR_NONE;
}




/*
 *********************** End of file ******************************************
 */
