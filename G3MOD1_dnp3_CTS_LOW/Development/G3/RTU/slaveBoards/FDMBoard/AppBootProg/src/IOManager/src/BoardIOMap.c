/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: BoardIOMap.c 3665 2013-08-09 09:49:26Z fryers_j $
 *               $HeadURL: http://svn-general-svc.wlucynet.com:18080/svn/gemini_30_software/branches/NPG/G3MOD1_dnp3_CTS_LOW/Development/G3/RTU/slaveBoards/FDMBoard/AppBootProg/src/IOManager/src/BoardIOMap.c $
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_TX, 	0)

	IOM_GPIO_PERIPH(IO_ID_DIGITAL_POT_SCK,		 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDO,		 PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDI,		 PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	0)

	IOM_GPIO_PERIPH(IO_ID_BAT_SCL,				 PINSEL_PORT_0,  PINSEL_PIN_28,  \
			FUNC_I2C_0_SCL, 	0)
	IOM_GPIO_PERIPH(IO_ID_BAT_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_27,  \
			FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C2_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_11,  \
			FUNC_I2C_2_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C2_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_10,  \
			FUNC_I2C_2_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_UART_OVP_TX,         PINSEL_PORT_2,  PINSEL_PIN_0,  \
			FUNC_UART_1_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_OVP_RX,         PINSEL_PORT_2,  PINSEL_PIN_1,  \
			FUNC_UART_1_TX, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/

	IOM_GPIO_INPUT( IO_ID_MOD_SEL1, 			   	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_0,    GPIO_PM_PULL_UP)
	IOM_GPIO_INPUT( IO_ID_MOD_SEL2, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_1,    GPIO_PM_PULL_UP)
	IOM_GPIO_INPUT( IO_ID_MOD_SEL3, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_4,    GPIO_PM_PULL_UP)
	IOM_GPIO_INPUT( IO_ID_MOD_SEL4, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_8,    GPIO_PM_PULL_UP)

	IOM_GPIO_INPUT( IO_ID_LOCAL_LINE,               IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_17,   0)
	IOM_GPIO_INPUT( IO_ID_REMOTE_LINE, 			 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_15,   0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_18,  0)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
				PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/


	/* End of table marker */
	IOM_LAST
};




/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
