/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX			(IO_ID_LAST)

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IO_ID_CAN1_RX	                       =0 ,
	IO_ID_CAN1_TX	                          ,
	IO_ID_CAN2_RX                             ,
	IO_ID_CAN2_TX                             ,

	IO_ID_UART_DEBUG_TX	                      ,
	IO_ID_UART_DEBUG_RX	                      ,

	IO_ID_ADC2_CLK							  ,
	IO_IO_ADC2_DIN							  ,
	IO_IO_ADC2_DOUT							  ,

	IO_ID_ADC_HSA_SEL                         ,
	IO_ID_ADC_HSCLK_SCK                       ,
	IO_IO_ADC_HSD_SDI                         ,
	IO_IO_ADC_HSD_SDO                         ,

	IO_ID_ADC_I2C_CLK						  ,
	IO_ID_ADC_I2C_SDA                         ,

	IO_ID_I2C1_CLK                            ,
	IO_ID_I2C1_DATA                           ,

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IO_ID_LOCAL_LINE                          ,
	IO_ID_REMOTE_LINE                         ,

	IO_ID_AUTO_CONFIG_IN                      ,
	IO_ID_AUTO_CONFIG_OUT                     ,

	IO_ID_FACTORY                             ,

	IO_ID_HWDOG_1HZ_KICK                      ,

	IO_ID_APPRAM_WEN                          ,

	IO_ID_LED_CTRL_OK_GREEN                   ,
	IO_ID_LED_CTRL_OK_RED                     ,
	IO_ID_LED_CTRL_CAN_GREEN                  ,
	IO_ID_LED_CTRL_CAN_RED                    ,
	IO_ID_LED_CTRL_EXTINGUISH                 ,

	IO_ID_LED_FPI_RED						  ,
	IO_ID_LED_FPI_GREEN						  ,
	IO_ID_LED_VOL1_GREEN					  ,
	IO_ID_LED_VOL1_RED						  ,
	IO_ID_LED_VOL2_GREEN					  ,
	IO_ID_LED_VOL2_RED						  ,

	IO_ID_MOD_SEL1							  ,
	IO_ID_MOD_SEL2							  ,
	IO_ID_MOD_SEL3							  ,
	IO_ID_MOD_SEL4							  ,

	IO_ID_ADC2_CS							  ,

	IO_ID_VS_RANGE_SEL                        ,

	IO_ID_ADC_CFG1							  ,
	IO_ID_ADC_CFG2							  ,
	IO_ID_ADC_PM0							  ,
	IO_ID_ADC_PM1							  ,
	IO_ID_ADC_MANUAL_RESET     				  ,
	IO_ID_ADC_IRQ0							  ,
	IO_ID_ADC_IRQ1							  ,

	IO_ID_TEMP_MEASURE                        ,
	IO_ID_3_3V_MEAS                           ,
	IO_ID_5V_MEAS                             ,

	/* I2C Expanders */


	/* SPI Digi pots */

	/* ADE7854 ADC I2C + SPI attached */
	IO_ID_I2C_ADE7854						  ,
	IO_IO_SPI_ADE7854						  ,

	/* SPI ADC */
	IO_IO_3_3V_ISO_MEAS						  ,
//	IO_IO_VTP								  ,
//	IO_IO_VSP								  ,
//	IO_IO_VRP								  ,

	/* NOVRAM */
	IO_ID_APPLICATION_NVRAM                   ,
	IO_ID_IDENTITY_NVRAM                      ,

	/******************************/
	/* Virtual IO ID's go here... */
	/******************************/
//	IO_IO_VAP								  ,
//	IO_IO_VBP								  ,
//	IO_IO_VCP								  ,
//	IO_IO_IA								  ,
//	IO_IO_IB								  ,
//	IO_IO_IC								  ,
//	IO_IO_IN                                  ,

//	IO_IO_IA_RMS							  ,
//	IO_IO_IB_RMS							  ,
//	IO_IO_IC_RMS							  ,
//	IO_IO_VA_RMS							  ,
//	IO_IO_VB_RMS							  ,
//	IO_IO_VC_RMS							  ,

	IO_IO_IPEAKA                              ,//62
	IO_IO_IPEAKB                              ,
	IO_IO_IPEAKC                              ,
	IO_IO_ISUM                                ,

	IO_IO_TEST_00                             ,
	IO_IO_TEST_01                             ,

	IO_ID_LAST
} IO_ID;

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
