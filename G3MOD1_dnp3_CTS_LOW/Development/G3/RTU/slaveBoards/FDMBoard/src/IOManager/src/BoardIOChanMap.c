/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"


/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(0,						 				IO_ID_NA),

	IOM_IOCHAN_DI(0, 						                IO_ID_NA)
};

IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),

	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};

IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AI(FDM_CH_AINPUT_TEMP_SENSOR, 				IO_ID_TEMP_MEASURE, CAL_ID_NA, IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FDM_CH_AINPUT_POWER_SUPPLY_3_3V, 			IO_ID_3_3V_MEAS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FDM_CH_AINPUT_POWER_SUPPLY_5V, 			IO_ID_5V_MEAS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FDM_CH_AINPUT_POWER_SUPPLY_ISO_3_3V, 		IO_IO_3_3V_ISO_MEAS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_A_HP_VOLTAGE, 		IO_IO_VAP, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_B_HP_VOLTAGE, 		IO_IO_VBP, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_C_HP_VOLTAGE, 		IO_IO_VCP, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_A_HP_CURRENT, 		IO_IO_IA, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_B_HP_CURRENT, 		IO_IO_IB, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_C_HP_CURRENT, 		IO_IO_IC, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_N_HP_CURRENT,         IO_IO_IN, CAL_ID_NA, IO_AI_EVENTRATE_MS),

//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_A_HP_VOLTAGE_RMS, 	IO_IO_VA_RMS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_B_HP_VOLTAGE_RMS, 	IO_IO_VB_RMS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_C_HP_VOLTAGE_RMS, 	IO_IO_VC_RMS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_A_HP_CURRENT_RMS, 	IO_IO_IA_RMS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_B_HP_CURRENT_RMS, 	IO_IO_IB_RMS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_C_HP_CURRENT_RMS, 	IO_IO_IC_RMS, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_A_LP_VOLTAGE, 		IO_IO_VRP, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_B_LP_VOLTAGE, 		IO_IO_VSP, CAL_ID_NA, IO_AI_EVENTRATE_MS),
//	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_C_LP_VOLTAGE, 		IO_IO_VTP, CAL_ID_NA, IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_A_PEAK_CURRENT, 		IO_IO_IPEAKA, CAL_ID_NA, IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_B_PEAK_CURRENT, 		IO_IO_IPEAKB, CAL_ID_NA, IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_C_PEAK_CURRENT, 		IO_IO_IPEAKC, CAL_ID_NA, IO_AI_EVENTRATE_MS),

	IOM_IOCHAN_AI(FDM_CH_AINPUT_PHASE_N_CURRENT, 			IO_IO_ISUM, CAL_ID_NA, IO_AI_EVENTRATE_MS),




	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, CAL_ID_NA, 0)
};

IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(0, 										CAL_ID_NA, IO_ID_NA),

	IOM_IOCHAN_AO(0, 										CAL_ID_NA, IO_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
