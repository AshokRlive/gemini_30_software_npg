/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ADE78xx.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"




/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

// Define if using Version 2 of FDM board (XB) !!!
#define FDM_V2		1

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_CAN2_RX,               PINSEL_PORT_2,  PINSEL_PIN_7,   \
			FUNC_CAN_2_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN2_TX,               PINSEL_PORT_2,  PINSEL_PIN_8,   \
			FUNC_CAN_2_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_TX, 	0)

	IOM_GPIO_PERIPH(IO_ID_ADC2_CLK,			 	 PINSEL_PORT_1,  PINSEL_PIN_20,  \
			FUNC_SSP0_SCK, 		0)
	IOM_GPIO_PERIPH(IO_IO_ADC2_DIN,			     PINSEL_PORT_1,  PINSEL_PIN_24,  \
			FUNC_SSP0_MOSI, 	0)
	IOM_GPIO_PERIPH(IO_IO_ADC2_DOUT,		     PINSEL_PORT_1,  PINSEL_PIN_23,  \
			FUNC_SSP0_MISO, 	0)

	IOM_GPIO_PERIPH(IO_ID_ADC_HSA_SEL,		     PINSEL_PORT_0,  PINSEL_PIN_6,   \
			FUNC_SSP1_SEL, 	GPIO_PM_ADC_CH_A)
	IOM_GPIO_PERIPH(IO_ID_ADC_HSCLK_SCK,	 	 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		GPIO_PM_ADC_CH_A)
	IOM_GPIO_PERIPH(IO_IO_ADC_HSD_SDI,		     PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	GPIO_PM_ADC_CH_A)
	IOM_GPIO_PERIPH(IO_IO_ADC_HSD_SDO,		     PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	GPIO_PM_ADC_CH_A)


	IOM_GPIO_PERIPH(IO_ID_ADC_I2C_CLK,			 PINSEL_PORT_0,  PINSEL_PIN_28,  \
			FUNC_I2C_0_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_ADC_I2C_SDA,			 PINSEL_PORT_0,  PINSEL_PIN_27,  \
			FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_DATA, 		     PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_LOCAL_LINE,               IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_17,   0)
	IOM_GPIO_INPUT( IO_ID_REMOTE_LINE, 			 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_15,   0)

	IOM_GPIO_INPUT( IO_ID_AUTO_CONFIG_IN, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_9,    0)
	IOM_GPIO_OUTPUT( IO_ID_AUTO_CONFIG_OUT,			IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_16,   0)

	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,    GPIO_PM_PULL_UP)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
					PINSEL_PORT_0,  PINSEL_PIN_18,  0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    GPIO_PM_OUTPUT_LOW)


	IOM_GPIO_OUTPUT( IO_ID_LED_FPI_RED, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_25,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_FPI_GREEN, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_22,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_VOL1_GREEN, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_18,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_VOL1_RED, 			IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_19,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_VOL2_GREEN, 			IO_CH_NA, \
			PINSEL_PORT_3,  PINSEL_PIN_26,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_VOL2_RED, 			IO_CH_NA, \
			PINSEL_PORT_3,  PINSEL_PIN_25,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_INPUT( IO_ID_MOD_SEL1, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_0,    GPIO_PM_PULL_UP)
	IOM_GPIO_INPUT( IO_ID_MOD_SEL2, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_1,    GPIO_PM_PULL_UP)
	IOM_GPIO_INPUT( IO_ID_MOD_SEL3, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_4,    GPIO_PM_PULL_UP)
	IOM_GPIO_INPUT( IO_ID_MOD_SEL4, 			   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_8,    GPIO_PM_PULL_UP)

	IOM_GPIO_OUTPUT( IO_ID_ADC2_CS, 				IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_21,   0)

	IOM_GPIO_OUTPUT( IO_ID_VS_RANGE_SEL, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_3,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_INPUT( IO_ID_ADC_CFG1, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_4,   (GPIO_PM_ADC_CFG1 | GPIO_PM_ADC_CH_A))
	IOM_GPIO_INPUT( IO_ID_ADC_CFG2, 				IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_5,   (GPIO_PM_ADC_CFG2 | GPIO_PM_ADC_CH_A))

#ifdef FDM_V2
	IOM_GPIO_OUTPUT( IO_ID_ADC_PM0, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_0,   (GPIO_PM_ADC_PM0 | GPIO_PM_ADC_CH_A))
	IOM_GPIO_OUTPUT( IO_ID_ADC_PM1, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_1,   (GPIO_PM_ADC_PM1 | GPIO_PM_ADC_CH_A))
#else
	IOM_GPIO_OUTPUT( IO_ID_ADC_PM0, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_0,   (GPIO_PM_ADC_PM0 | GPIO_PM_OUTPUT_INVERT))
	IOM_GPIO_OUTPUT( IO_ID_ADC_PM1, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_1,   (GPIO_PM_ADC_PM1 | GPIO_PM_OUTPUT_INVERT))
#endif
	IOM_GPIO_OUTPUT( IO_ID_ADC_MANUAL_RESET,		IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_2,   (GPIO_PM_ADC_RESET | GPIO_PM_OUTPUT_INVERT | GPIO_PM_ADC_CH_A))
	IOM_GPIO_INPUT( IO_ID_ADC_IRQ0, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_11,  (GPIO_PM_ADC_IRQ0 | GPIO_PM_ADC_CH_A))
	IOM_GPIO_INPUT( IO_ID_ADC_IRQ1, 				IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_13,  (GPIO_PM_ADC_IRQ1 | GPIO_PM_ADC_CH_A))


	IOM_PERIPH_AIN( IO_ID_TEMP_MEASURE, 			FDM_CH_AINPUT_TEMP_SENSOR, \
			PINSEL_PORT_0,  PINSEL_PIN_23,  FUNC_AD0_CH_0)
	IOM_PERIPH_AIN( IO_ID_3_3V_MEAS,				FDM_CH_AINPUT_POWER_SUPPLY_3_3V, \
			PINSEL_PORT_1,  PINSEL_PIN_31,  FUNC_AD0_CH_5)
	IOM_PERIPH_AIN( IO_ID_5V_MEAS,					FDM_CH_AINPUT_POWER_SUPPLY_5V, \
			PINSEL_PORT_1,  PINSEL_PIN_30,  FUNC_AD0_CH_4)

	/* I2C temp sensor */

	/* I2C IO Expanders */


	/* SPI Digi pot devices */

	/* ADE7854 ADC I2C + HSDC or SPI attached */
	IOM_I2C_ADE7854(IO_ID_I2C_ADE7854, IO_BUS_I2C_0)
	IOM_HSDC_ADE7854(IO_IO_SPI_ADE7854, IO_BUS_SPI_SSPI_1)

//	IOM_SPI_ADE7854(IO_IO_SPI_ADE7854, IO_BUS_SPI_SSPI_1, IO_ID_ADC_HSA_SEL)

	/* SPI attached ADC (MCP3204) */
	IOM_SPI_AIN(IO_IO_3_3V_ISO_MEAS, 				FDM_CH_AINPUT_POWER_SUPPLY_ISO_3_3V, \
			IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_0, IO_ID_ADC2_CS)
//	IOM_SPI_AIN(IO_IO_VTP,           				FDM_CH_AINPUT_PHASE_C_LP_VOLTAGE, \
			IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_1, IO_ID_ADC2_CS)
//	IOM_SPI_AIN(IO_IO_VSP,           				FDM_CH_AINPUT_PHASE_B_LP_VOLTAGE, \
			IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_2, IO_ID_ADC2_CS)
//	IOM_SPI_AIN(IO_IO_VRP,           				FDM_CH_AINPUT_PHASE_A_LP_VOLTAGE, \
			IO_BUS_SPI_SSPI_0, SPI_ADC_CH_SE_3, IO_ID_ADC2_CS)

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

	IOM_ADE78XX_SPI_AIN(IO_IO_IPEAKA,					FDM_CH_AINPUT_PHASE_A_PEAK_CURRENT, \
			SPI_ADE78XX_CH_A, ADE78XX_IPEAK, GPIO_PM_ADC_REG_24ZP_PKA)

	IOM_ADE78XX_SPI_AIN(IO_IO_IPEAKB,					FDM_CH_AINPUT_PHASE_B_PEAK_CURRENT, \
			SPI_ADE78XX_CH_A, ADE78XX_IPEAK, GPIO_PM_ADC_REG_24ZP_PKB)

	IOM_ADE78XX_SPI_AIN(IO_IO_IPEAKC,					FDM_CH_AINPUT_PHASE_C_PEAK_CURRENT, \
			SPI_ADE78XX_CH_A, ADE78XX_IPEAK, GPIO_PM_ADC_REG_24ZP_PKC)

	IOM_ADE78XX_SPI_AIN(IO_IO_ISUM,						FDM_CH_AINPUT_PHASE_N_CURRENT, \
			SPI_ADE78XX_CH_A, ADE78XX_ISUM, GPIO_PM_ADC_REG_24ZP)

	/******************************/
	/* Debug IO                   */
	/******************************/
    IOM_GPIO_OUTPUT( IO_IO_TEST_00,                 IO_CH_NA, \
                PINSEL_PORT_0,  PINSEL_PIN_10,   0)
    IOM_GPIO_OUTPUT( IO_IO_TEST_01,                 IO_CH_NA, \
                PINSEL_PORT_0,  PINSEL_PIN_11,   0)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/

	/* End of table marker */
	IOM_LAST
};




/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
