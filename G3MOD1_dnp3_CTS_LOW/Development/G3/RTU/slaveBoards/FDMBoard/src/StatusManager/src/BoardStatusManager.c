/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "systemStatus.h"

#include "NVRam.h"
#include "IOManager.h"
#include "IOManagerIOUpdate.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "IOManagerExtAnalogIOUpdate.h"
#include "IOManagerExtAD78xxIOUpdate.h"
#include "BoardIO.h"





/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/* Temporary thread used for testing */
static SB_ERROR FDMPeriodicManager(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				 0, 				0 				      }, // Run all the time

	{CANFramingTick, 			 0, 				CAN_TICK_MS		          }, // Run every 10ms

	{IOManagerIORunUpdateGPIO, 	 0, 				IOMAN_UPDATE_GPIO_MS  },
	{IOManagerIntAnalogIOUpdate, 0, 				IOMAN_UPDATE_INAI_MS  },
	{IOManagerExtAnalogIOUpdate, 0, 				IOMAN_UPDATE_EXT_AI_MS},
	{IOManagerExtAD78xxIOUpdate, 0, 				IOMAN_UPDATE_EXT_AD78XX_AI_MS},
	{IOManagerIOAIEventUpdate,   0,                 IOMAN_UPDATE_INAI_EVENT_MS},

	{NVRamUpdateTick, 		     0, 				NVRAM_UPDATE_TICK_MS 	  },

	{FDMPeriodicManager      ,   0,                 200                   },

	{(SMThreadRun)0L, 	    	 0, 				0   			      }  // end of table
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{
	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

SB_ERROR FDMPeriodicManager(void)
{
    if(SSGetBStatus() == MODULE_BOARD_STATUS_STARTING)
    {
    	/* Allow the IOManager to report events */
    	IOManagerStartEventing();

    	/* Start FDM subsystem */

        SSSetBStatus(MODULE_BOARD_STATUS_ON_LINE);
    }

    return SB_ERROR_NONE;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
