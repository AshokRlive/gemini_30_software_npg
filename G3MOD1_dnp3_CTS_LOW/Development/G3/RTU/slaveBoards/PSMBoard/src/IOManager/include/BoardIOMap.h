/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO MAP/Board Manager include header
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARDIOMAP_INCLUDED
#define _BOARDIOMAP_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "IOManager.h"

/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define BOARD_IO_MAX_IDX			(IO_ID_LAST)


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/* Board Specific IO ID's */
typedef enum
{
	/* processor GPIO */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IO_ID_CAN1_RX	                       =0 ,
	IO_ID_CAN1_TX	                          ,

	IO_ID_UART_DEBUG_TX	                      ,
	IO_ID_UART_DEBUG_RX	                      ,

	IO_ID_DIGITAL_POT_SCK                     ,
	IO_IO_DIGITAL_POT_SDO                     ,
	IO_IO_DIGITAL_POT_SDI                     ,

	IO_ID_BAT_SCL                             ,
	IO_ID_BAT_SDA                             ,

	IO_ID_I2C1_CLK                            ,
	IO_ID_I2C1_SDA                            ,

	IO_ID_I2C2_CLK                            ,
	IO_ID_I2C2_SDA                            ,

	IO_ID_UART_OVP_TX					      ,
	IO_ID_UART_OVP_RX						  ,


	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IO_ID_LOCAL_LINE                          ,
	IO_ID_REMOTE_LINE                         ,

	IO_ID_AUTO_CONFIG_IN                      ,
	IO_ID_AUTO_CONFIG_OUT                     ,

	IO_ID_FACTORY                             ,

	IO_ID_AC_SUPPLY_ON                        ,
	IO_ID_OVP_PIC_UP_STATUS                   ,

	IO_ID_FAN_ON                              ,
	IO_ID_FAN_PGOOD                           ,
	IO_ID_FAN_SPEED_S                         ,

	IO_ID_MOT_PS_ON                           ,
	IO_ID_MOT_PS_OFF                          ,

	// New PSM (V2)
	IO_ID_MOT_PS_EN                           ,

	IO_ID_BAT_TO_MOT_CONNECT                  ,

	IO_ID_BAT_CONNECT                         ,
	IO_ID_BAT_DISCONNECT                      ,

	IO_ID_BAT_CHARGER_EN                      ,

	IO_ID_24V_AUX_EN                          ,
	IO_ID_24V_PS_EN                           ,
	IO_ID_24V_AUX_ST                          ,
	IO_ID_24V_AUX_PWR_GOOD                    ,

	IO_ID_LOGIC_PS_ON                         ,
	IO_ID_LOGIC_PS_OFF                        ,
	IO_ID_LOGIC_PS_PWR_GOOD                   ,
	IO_ID_LOGIC_PS_EN                         ,

	IO_ID_COMMS_1_PRESENT                     ,
	IO_ID_COMMS_2_PRESENT                     ,
	IO_ID_COMMS_1_EN                          ,
	IO_ID_COMMS_2_EN                          ,
	IO_ID_ANALOG_SUP_SEL                      ,

	// New PSM (V2)
	IO_ID_HWDOG_1HZ_KICK                      ,

	IO_ID_LED_CTRL_OK_GREEN                   ,
	IO_ID_LED_CTRL_OK_RED                     ,
	IO_ID_LED_CTRL_CAN_GREEN                  ,
	IO_ID_LED_CTRL_CAN_RED                    ,
	IO_ID_LED_CTRL_EXTINGUISH                 ,
	IO_ID_LED_CTRL_SUPPLY_OK_GREEN            ,
	IO_ID_LED_CTRL_SUPPLY_OK_RED              ,
	IO_ID_LED_CTRL_LOGIC_GREEN                ,
	IO_ID_LED_CTRL_LOGIC_RED                  ,
	IO_ID_LED_CTRL_MOTOR_GREEN                ,
	IO_ID_LED_CTRL_MOTOR_RED                  ,
	IO_ID_LED_CTRL_BAT_CHRG_GREEN             ,
	IO_ID_LED_CTRL_BAT_CHRG_RED               ,
	IO_ID_LED_CTRL_BAT_HEALTH_GREEN           ,
	IO_ID_LED_CTRL_BAT_HEALTH_RED             ,
	IO_ID_LED_CTRL_FAN_GREEN                  ,
	IO_ID_LED_CTRL_FAN_RED                    ,

	// New for PSM V2 (ISO DC 1 on PSM feature 0.2)
	IO_ID_LED_CTRL_COMMS_GREEN                ,
	IO_ID_LED_CTRL_COMMS_RED                  ,

	// changed for PSM feature 0.2 (ISO DC 2)
	IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN      ,
	IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED        ,

	// Additional for PSM feature 0.2
	IO_ID_LED_CTRL_24VAUX_GREEN               ,
	IO_ID_LED_CTRL_24VAUX_RED                 ,

	IO_ID_MOT_I_SENSE                         ,
	IO_ID_MOT_V_SENSE                         ,
	IO_ID_BAT_V_SENSE                         ,
	IO_ID_BAT_CHARGER_I_SENSE                 ,
	IO_ID_MAIN_DC_I_SENSE                     ,
	IO_ID_CHARGER_V_SENSE                     ,

	// New PSM (V2)
	IO_ID_LOGIC_I_SENSE                       ,
	IO_ID_36V_RAW_DC_V_SENSE                  ,
	IO_ID_COMMS_1_V_SENSE                     , // psm (v2)To become IO_ID_COMMS_1_V_SENSE
	IO_ID_COMMS_2_V_SENSE                     ,

	IO_ID_LOGIC_PS_VSENSE                     ,
	IO_ID_VHH_VSENSE                          ,
	IO_ID_VDD_VSENSE                          ,

	// New for PSM feature 0.2
	IO_ID_24V_AUX_V_SENSE                     ,

	IO_ID_BAT_TEST_I_SENSE                    ,



	IO_ID_DIGITAL_POT_SHDN                    ,
	IO_ID_DIGITAL_POT_CS0                     ,
	IO_ID_DIGITAL_POT_CS1                     ,

	IO_ID_ADC_AI8_CS                          ,
	IO_ID_BAT_TEST_EN                         ,

	IO_ID_APPRAM_WEN                          ,
	IO_ID_BAT_DATA_WEN                        ,
	IO_ID_BAT_ID_WEN						  ,

	IO_ID_BAT_TEMP							  ,
	IO_ID_PSU_TEMP							  ,

	/* I2C Expanders */

	/* SPI Digi pots */
	IO_ID_BAT_CHARGER_CUR_LIM_SET             ,
	IO_ID_BAT_CHARGER_VOLT_SET                ,

	/* Additional for PSM feature 0.2 */
	IO_ID_DAC_OUT_BT_TEST_LOAD                ,

	/* SPI ADC */
	IO_ID_SYS_HEALTHY                         ,
	IO_ID_WD_HANDSHAKE                        ,

	IO_ID_BATTERY_ID_NVRAM                    ,
	IO_ID_BATTERY_DATA_NVRAM                  ,
	IO_ID_APPLICATION_NVRAM                   ,
	IO_ID_IDENTITY_NVRAM                      ,

	/******************************/
	/* Virtual IO ID's go here... */
	/******************************/
	IO_ID_VIRT_MS_RELAY_ON                    ,
	IO_ID_VIRT_MS_OVERCURRENT                 ,
	IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT          ,
	IO_ID_VIRT_AC_OFF                         ,
	IO_ID_VIRT_CH_FAULT                       ,
	IO_ID_VIRT_CH_ON                          ,
	IO_ID_VIRT_BT_COMMS_FAIL                  ,
	IO_ID_VIRT_BT_DISCONNECTED                ,
	IO_ID_VIRT_BT_OVER_TEMPERATURE            ,
	IO_ID_VIRT_CH_SYSTEM_RUNNING           ,
	IO_ID_VIRT_BT_TEST_FAIL                   ,
	IO_ID_VIRT_BT_LOW                         ,
	IO_ID_VIRT_BT_FAULT                       ,

	IO_ID_VIRT_FAN_ON                         ,
	IO_ID_VIRT_FAN_FAULT                      ,

	IO_ID_VIRT_MS_PEAK_HOLD_CURRENT           ,
	IO_ID_VIRT_DRAIN_CURRENT                  ,
	IO_ID_VIRT_ON_ELAPSED_TIME                ,

	IO_ID_VIRT_BT_TYPE_NIHM                   ,
	IO_ID_VIRT_BT_TYPE_LEAD_ACID              ,
	IO_ID_ACCUMULATED_CHARGE_CURRENT          ,
	IO_ID_ACCUMLATED_DISCHARGE_TIME           ,
	IO_ID_ACCUMULATED_DISCHARGE_CURRENT       ,

	IO_ID_VIRT_AUX_FAULT                      ,
	IO_ID_VIRT_AUX_ON                         ,
	IO_ID_VIRT_COMMS2_FAULT                   ,
	IO_ID_VIRT_COMMS2_ON                      ,
	IO_ID_VIRT_COMMS1_FAULT                   ,
	IO_ID_VIRT_COMMS1_ON                      ,
	IO_ID_VIRT_COMMS1_INSTALLED               ,
	IO_ID_VIRT_COMMS2_INSTALLED               ,

	IO_ID_VIRT_BT_CHARGE_CYCLES               ,
	IO_ID_VIRT_BT_ESTIMATED_CAPACITY          ,
	IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME    ,
	
	IO_ID_VIRT_BT_BATTERY_CURRENT             ,

	IO_ID_VIRT_CH_TEST_FAIL                   ,
	IO_ID_VIRT_CH_VOLTAGE_FAULT               ,
	IO_ID_VIRT_CH_CURRENT_FAULT               ,
	IO_ID_VIRT_BT_VOLTAGE_FAULT               ,
	IO_ID_VIRT_BT_TEMP_FAIL                   ,
	IO_ID_VIRT_BT_ID_NVRAM_FAIL               ,
	IO_ID_VIRT_BT_DATA_NVRAM_FAIL             ,

	IO_ID_VIRT_BT_CH_SHUTDOWN                 ,

	IO_ID_VIRT_BT_CH_INHIBITED                ,

	IO_ID_EXTERNAL_HUMIDITY                   ,
	IO_ID_EXTERNAL_TEMPERATURE                ,

	IO_ID_VIRT_BT_TEST_IN_PROGRESS         ,

	IO_ID_VIRT_MS_OVERCURRENT_LIMIT           ,

	IO_ID_BT_SHUTDOWN_APPROACHING             ,

	IO_ID_VIRT_BATTERY_TEST_TIME              ,
	IO_ID_VIRT_BATTERY_TEST_TOTAL_DISCHARGE   ,

	IO_ID_VIRT_BT_FULLY_CHARGED               ,

	IO_ID_VIRT_BT_CHARGE_CYCLE_TIME           ,
	IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE         ,
	IO_ID_VIRT_BATTERY_VOLTAGE				  ,

//	IO_ID_VIRT_ON_ELAPSED_TIME_UINT16         ,

	//JP Additional diagnostics
	IO_ID_VIRT_BT_TEST_HARDWARE_FAULT		,
	IO_ID_VIRT_BT_TEST_PASS					,
	IO_ID_VIRT_BT_TEST_FAIL_OR_RUN			,
	IO_ID_VIRT_BT_TEST_PASS_OR_RUN			,
	IO_ID_VIRT_CHARGER_EN					,
	IO_ID_VIRT_CHARGER_CON					,
	IO_ID_VIRT_BT_TYPE_DEFAULT				,
	IO_ID_VIRT_CH_SET_VOLTAGE				,
	IO_ID_VIRT_CH_SET_CURRENT				,
	IO_ID_VIRT_BT_MGR_FSM_STATE				,
	IO_ID_VIRT_BT_CHGR_FSM_STATE			,
	IO_ID_VIRT_BT_TEST_END_REASON			,
    IO_ID_VIRT_NiMH_FAST_CHRG_COMPLETE      ,

	// Use Fan Output as a Digital Out
	IO_ID_FAN_DOUT                          ,

	IO_ID_LAST
} IO_ID;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1];

/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */



#endif /* _BOARDIOMAP_INCLUDED */

/*
 *********************** End of file ******************************************
 */
