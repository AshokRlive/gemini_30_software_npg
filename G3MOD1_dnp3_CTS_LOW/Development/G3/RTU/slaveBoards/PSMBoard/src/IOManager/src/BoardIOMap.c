/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board Specific IO MAP/IO Manager Module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   15/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_pinsel.h"

#include "ModuleProtocol.h"
#include "IOManager.h"

/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"



/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */



/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/******************************************************************************/
/* LOCAL - Global Variables                                                   */
/******************************************************************************/

/******************************************************************************/
/*This constant array is an ordered list of all the IO used by the PSM        */
/*The elements of this array are of type IOMapStr as shown below:-            */
/*                                                                            */
/*typedef struct IOMapDef                                                     */
/*{                                                                           */
/*	lu_uint16_t	     ioID;                                                    */
/*	lu_uint32_t		 ioChan;                                                  */
/*	IO_CLASS         ioClass;                                                 */
/*	IO_DEV           ioDev;                                                   */
/*	IOAddressStr     ioAddr;                                                  */
/*	IODebounceStr	 db;                                                      */
/*}IOMapStr;                                                                  */
/*                                                                            */
/*All functions within this software use the IOManager functions to send and  */
/*receive data from the hardware/peripherals of the system.                   */
/*The IOManager uses this IOMapStr structure to know how to access various io */
/*To help with the initialisation of this array an number of initialising     */
/*macros have been written to populate the IOMapStr elements. There is one    */
/*macro per type of IO device                                                 */
/*The initialisation of the IOMapStr elements is straight forward apart from  */
/*the structure member called ioAddr which is of type IOAdressStr which is a  */
/*union of structures that represent the differing needs of the various types */
/*of IO devices                                                               */
/*The initialising macros assist the programmer to initialise the IOMapStr    */
/*elements correctly by passing through the variable parameters of a device   */
/*type and fixing constant parameters of a device type into an initialising   */
/*string                                                                      */
/******************************************************************************/


/*! Board specific Pin IO Map */
const IOMapStr BoardIOMap[BOARD_IO_MAX_IDX + 1] =
{
	/* Must be defined as same order as the IO_ID Enum!!! */

	/* Local processor GPIO map */

	/**************************************/
	/* Processor Peripheral function pins */
	/**************************************/
	IOM_GPIO_PERIPH(IO_ID_CAN1_RX,               PINSEL_PORT_0,  PINSEL_PIN_21,  \
			FUNC_CAN_1_RX, 		0)
	IOM_GPIO_PERIPH(IO_ID_CAN1_TX,               PINSEL_PORT_0,  PINSEL_PIN_22,  \
			FUNC_CAN_1_TX, 		0)

	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_TX,         PINSEL_PORT_4,  PINSEL_PIN_28,  \
			FUNC_UART_3_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_DEBUG_RX,         PINSEL_PORT_4,  PINSEL_PIN_29,  \
			FUNC_UART_3_TX, 	0)

	IOM_GPIO_PERIPH(IO_ID_DIGITAL_POT_SCK,		 PINSEL_PORT_0,  PINSEL_PIN_7,   \
			FUNC_SSP1_SCK, 		0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDO,		 PINSEL_PORT_0,  PINSEL_PIN_8,   \
			FUNC_SSP1_MISO, 	0)
	IOM_GPIO_PERIPH(IO_IO_DIGITAL_POT_SDI,		 PINSEL_PORT_0,  PINSEL_PIN_9,   \
			FUNC_SSP1_MOSI, 	0)

	IOM_GPIO_PERIPH(IO_ID_BAT_SCL,				 PINSEL_PORT_0,  PINSEL_PIN_28,  \
			FUNC_I2C_0_SCL, 	0)
	IOM_GPIO_PERIPH(IO_ID_BAT_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_27,  \
			FUNC_I2C_0_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C1_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_20,  \
			FUNC_I2C_1_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C1_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_19,  \
			FUNC_I2C_1_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_I2C2_CLK,				 PINSEL_PORT_0,  PINSEL_PIN_11,  \
			FUNC_I2C_2_SCL, 	GPIO_PM_BUS_400KHZ)
	IOM_GPIO_PERIPH(IO_ID_I2C2_SDA,				 PINSEL_PORT_0,  PINSEL_PIN_10,  \
			FUNC_I2C_2_SDA, 	0)

	IOM_GPIO_PERIPH(IO_ID_UART_OVP_TX,         PINSEL_PORT_2,  PINSEL_PIN_0,  \
			FUNC_UART_1_TX, 	0)
	IOM_GPIO_PERIPH(IO_ID_UART_OVP_RX,         PINSEL_PORT_2,  PINSEL_PIN_1,  \
			FUNC_UART_1_TX, 	0)

	/*********************************/
	/* Physically connected IO Pins  */
	/*********************************/
	IOM_GPIO_INPUT( IO_ID_LOCAL_LINE,               IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_17,   0)
	IOM_GPIO_INPUT( IO_ID_REMOTE_LINE, 			 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_15,   0)

	IOM_GPIO_INPUT( IO_ID_AUTO_CONFIG_IN, 			IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_9,    0)
	IOM_GPIO_OUTPUT( IO_ID_AUTO_CONFIG_OUT,			IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_16,   0)

	IOM_GPIO_INPUT( IO_ID_FACTORY, 			     	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_1,    GPIO_PM_PULL_UP)

	IOM_GPIO_INPUT( IO_ID_AC_SUPPLY_ON,             IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_10,   0)

	IOM_GPIO_INPUT( IO_ID_OVP_PIC_UP_STATUS,        IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_9,    0)

	IOM_GPIO_OUTPUT( IO_ID_FAN_ON, 		        	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_16,    GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_INPUT( IO_ID_FAN_PGOOD, 	     		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_14,    0)

	IOM_GPIO_INPUT( IO_ID_FAN_SPEED_S, 			 	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_15,    0)

	IOM_GPIO_OUTPUT( IO_ID_MOT_PS_ON, 		       	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_4,    GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_MOT_PS_OFF, 		       	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_8,    GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_INPUT( IO_ID_MOT_PS_EN, 	     		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_1,    0)

	IOM_GPIO_OUTPUT( IO_ID_BAT_TO_MOT_CONNECT,      IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_0,    0)

	IOM_GPIO_OUTPUT( IO_ID_BAT_CONNECT,             IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_4,    GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_BAT_DISCONNECT,          IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_17,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT(IO_ID_BAT_CHARGER_EN,           IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_2,    GPIO_PM_OUTPUT_LOW)


	IOM_GPIO_OUTPUT( IO_ID_24V_AUX_EN, 	     		IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_21,    0)

	IOM_GPIO_OUTPUT( IO_ID_24V_PS_EN, 	     		IO_CH_NA, \
				PINSEL_PORT_2,  PINSEL_PIN_11,    0)

	IOM_GPIO_INPUT( IO_ID_24V_AUX_ST, 	     		IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_20,    0)
	IOM_GPIO_INPUT( IO_ID_24V_AUX_PWR_GOOD,    		IO_CH_NA, \
					PINSEL_PORT_1,  PINSEL_PIN_22,    0)

	IOM_GPIO_OUTPUT( IO_ID_LOGIC_PS_ON, 	       	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_4,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LOGIC_PS_OFF, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_3,    GPIO_PM_OUTPUT_LOW)

	IOM_VIRT_DI( IO_ID_LOGIC_PS_PWR_GOOD,    	IO_CH_NA)

	IOM_GPIO_INPUT( IO_ID_LOGIC_PS_EN,    			IO_CH_NA, \
					PINSEL_PORT_2,  PINSEL_PIN_5,    0)

	IOM_GPIO_INPUT( IO_ID_COMMS_1_PRESENT, 			 IO_CH_NA, \
				PINSEL_PORT_2,  PINSEL_PIN_8,    GPIO_PM_PULL_DOWN)
	IOM_GPIO_INPUT( IO_ID_COMMS_2_PRESENT, 			 IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_24,   GPIO_PM_PULL_DOWN)

	IOM_GPIO_OUTPUT( IO_ID_COMMS_1_EN, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_7,    (GPIO_PM_OUTPUT_INVERT | GPIO_PM_OUTPUT_LOW))
	IOM_GPIO_OUTPUT( IO_ID_COMMS_2_EN, 		   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_25,   (GPIO_PM_OUTPUT_INVERT | GPIO_PM_OUTPUT_LOW))

	IOM_VIRT_DO( IO_ID_ANALOG_SUP_SEL, 		   	IO_CH_NA)

	IOM_GPIO_OUTPUT( IO_ID_HWDOG_1HZ_KICK, 		   	IO_CH_NA, \
			PINSEL_PORT_2,  PINSEL_PIN_12,   0)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_26,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_OK_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_27,   GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_GREEN, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_28,   0)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_CAN_RED, 		IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_29,   GPIO_PM_OUTPUT_LOW)

	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_EXTINGUISH, 	IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_0,    GPIO_PM_OUTPUT_LOW)

	/* I2C IO Expanders */
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, 			IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_0, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_SUPPLY_OK_RED, 			IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_1, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_LOGIC_GREEN, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_2, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_LOGIC_RED, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_3, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_MOTOR_GREEN, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_4, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_MOTOR_RED, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_5, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_BAT_CHRG_GREEN, 			IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_6, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_BAT_CHRG_RED, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_0_7, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, 			IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_1_0, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_BAT_HEALTH_RED, 			IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_1_1, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_FAN_GREEN, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_1_2, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_FAN_RED, 					IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_1_3, GPIO_PM_OUTPUT_INVERT)

	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_COMMS_GREEN, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
				I2C_EXP_IOPIN_1_4, GPIO_PM_OUTPUT_INVERT)
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_COMMS_RED, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_1_5, GPIO_PM_OUTPUT_INVERT)

	//			was IO_ID_LED_CTRL_24VAUX_GREEN on AUTO124 XM
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
				I2C_EXP_IOPIN_1_6, GPIO_PM_OUTPUT_INVERT)

	//	was     IO_ID_LED_CTRL_24VAUX_RED on AUTO124 XM
	IOM_I2CEXP_OUTPUT(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, 				IO_CH_NA, IO_BUS_I2C_2, 0x00,
			I2C_EXP_IOPIN_1_7, GPIO_PM_OUTPUT_INVERT)

	// New for AUTO450 Issue: 1
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_24VAUX_GREEN,        IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_30,    GPIO_PM_OUTPUT_LOW)
	IOM_GPIO_OUTPUT( IO_ID_LED_CTRL_24VAUX_RED,          IO_CH_NA, \
			PINSEL_PORT_0,  PINSEL_PIN_29,   GPIO_PM_OUTPUT_LOW)


	IOM_SPI_AIN(IO_ID_MOT_I_SENSE, 				PSM_CH_AINPUT_MS_CURRENT, \
									IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_4, IO_ID_ADC_AI8_CS)

	IOM_SPI_AIN(IO_ID_MOT_V_SENSE, 				PSM_CH_AINPUT_MS_VOLTAGE, \
									IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_7, IO_ID_ADC_AI8_CS)

	IOM_SPI_AIN(IO_ID_BAT_V_SENSE, 				PSM_CH_AINPUT_BT_VOLTAGE, \
									IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_6, IO_ID_ADC_AI8_CS)

	IOM_SPI_AIN(IO_ID_BAT_CHARGER_I_SENSE, 				PSM_CH_AINPUT_CH_CHARGING_CURRENT, \
									IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_5, IO_ID_ADC_AI8_CS)

	IOM_SPI_AIN(IO_ID_MAIN_DC_I_SENSE, 				PSM_CH_AINPUT_MAIN_DC_CURRENT, \
								IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_2, IO_ID_ADC_AI8_CS)

	IOM_SPI_AIN(IO_ID_CHARGER_V_SENSE, 				PSM_CH_AINPUT_CH_VOLTAGE, \
									IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_0, IO_ID_ADC_AI8_CS)

	IOM_SPI_AIN(IO_ID_LOGIC_I_SENSE, 				PSM_CH_AINPUT_LS_CURRENT, \
							IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_1, IO_ID_ADC_AI8_CS)

	IOM_PERIPH_AIN( IO_ID_36V_RAW_DC_V_SENSE, 		PSM_CH_AINPUT_36V_RAW_DC, \
						PINSEL_PORT_0,  PINSEL_PIN_24,  FUNC_AD0_CH_1)

	IOM_PERIPH_AIN( IO_ID_COMMS_1_V_SENSE, 			PSM_CH_AINPUT_COMMS_1_VOLTAGE, \
				PINSEL_PORT_1,  PINSEL_PIN_31,   FUNC_AD0_CH_5)

	IOM_PERIPH_AIN( IO_ID_COMMS_2_V_SENSE, 			PSM_CH_AINPUT_COMMS_2_AUX_VOLTAGE, \
					PINSEL_PORT_1,  PINSEL_PIN_30,  FUNC_AD0_CH_4)


	IOM_PERIPH_AIN( IO_ID_LOGIC_PS_VSENSE, 		PSM_CH_AINPUT_LOGIC_PS_VSENSE, \
						PINSEL_PORT_0,  PINSEL_PIN_23,  FUNC_AD0_CH_0)

	IOM_PERIPH_AIN( IO_ID_VHH_VSENSE, 			PSM_CH_AINPUT_VHH_VSENSE, \
					PINSEL_PORT_0,  PINSEL_PIN_2,  	 FUNC_AD0_CH_7)

	IOM_PERIPH_AIN( IO_ID_VDD_VSENSE, 		PSM_CH_AINPUT_VDD_VSENSE, \
					PINSEL_PORT_0,  PINSEL_PIN_3,    FUNC_AD0_CH_6)

	// New for PSM feature 0.2
	IOM_PERIPH_AIN( IO_ID_24V_AUX_V_SENSE, 			PSM_CH_AINPUT_AUX_VOLTAGE, \
					PINSEL_PORT_0,  PINSEL_PIN_25,  FUNC_AD0_CH_2)

	IOM_SPI_AIN(IO_ID_BAT_TEST_I_SENSE, 				PSM_CH_AINPUT_BAT_TEST_I_SENSE, \
						IO_BUS_SPI_SSPI_1, SPI_ADC_CH_SE_3, IO_ID_ADC_AI8_CS)



	IOM_VIRT_DO( IO_ID_DIGITAL_POT_SHDN, 	  	IO_CH_NA)

	IOM_GPIO_OUTPUT( IO_ID_DIGITAL_POT_CS0, 	  	IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_6,   0)
	IOM_VIRT_DO( IO_ID_DIGITAL_POT_CS1,             IO_CH_NA)


	IOM_GPIO_OUTPUT( IO_ID_ADC_AI8_CS, 	  	        IO_CH_NA, \
					PINSEL_PORT_0,  PINSEL_PIN_5,  0)
	IOM_GPIO_OUTPUT( IO_ID_BAT_TEST_EN, 	  	        IO_CH_NA, \
						PINSEL_PORT_2,  PINSEL_PIN_6,  GPIO_PM_OUTPUT_LOW)


	IOM_GPIO_OUTPUT( IO_ID_APPRAM_WEN, 	  	        IO_CH_NA, \
				PINSEL_PORT_0,  PINSEL_PIN_18,  0)

	IOM_GPIO_OUTPUT( IO_ID_BAT_DATA_WEN, 	  	    IO_CH_NA, \
				PINSEL_PORT_3,  PINSEL_PIN_26,  0)

	IOM_GPIO_OUTPUT( IO_ID_BAT_ID_WEN, 	  	    IO_CH_NA, \
				PINSEL_PORT_3,  PINSEL_PIN_25,  0)
	/* I2C temp sensor */
	IOM_I2CTEMP_AIN(IO_ID_BAT_TEMP, 			    PSM_CH_AINPUT_TS_BATTERY_TEMPERATURE, \
				IO_BUS_I2C_0, 	0x01, GPIO_PM_OFFLINE_OVERSAMPLE)
	IOM_I2CTEMP_AIN(IO_ID_PSU_TEMP, 			    PSM_CH_AINPUT_TS_INTERNAL_TEMPERATURE, \
				IO_BUS_I2C_2, 	0x01, 0)

	/* SPI Digi pot devices */
	IOM_ADPOT_OUT(IO_ID_BAT_CHARGER_CUR_LIM_SET, PSM_CH_AOUTPUT_BAT_CHARGER_CUR_LIM_SET, IO_BUS_SPI_SSPI_1,
			1, IO_ID_DIGITAL_POT_CS0)
	IOM_ADPOT_OUT(IO_ID_BAT_CHARGER_VOLT_SET, PSM_CH_AOUTPUT_BAT_CHARGER_VOLT_SET, IO_BUS_SPI_SSPI_1,
			0, IO_ID_DIGITAL_POT_CS0)

	/* Additional for PSM feature 0.2 */
	IOM_PERIPH_AOUT(IO_ID_DAC_OUT_BT_TEST_LOAD, 	PSM_CH_AOUTPUT_BAT_TEST_CURRENT_SET, \
			PINSEL_PORT_0, PINSEL_PIN_26, FUNC_DAC_A)

	IOM_GPIO_INPUT( IO_ID_SYS_HEALTHY, 		   	    IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_18,   0)
	IOM_GPIO_INPUT( IO_ID_WD_HANDSHAKE, 		   	IO_CH_NA, \
			PINSEL_PORT_1,  PINSEL_PIN_19,   0)

	/* I2C NVRAM */
	IOM_PERIPH_NVRAM(IO_ID_BATTERY_ID_NVRAM,    IO_BUS_I2C_0, 0x14, IO_ID_LAST)
	IOM_PERIPH_NVRAM(IO_ID_BATTERY_DATA_NVRAM,  IO_BUS_I2C_0, 0x15, IO_ID_BAT_DATA_WEN)
	IOM_PERIPH_NVRAM(IO_ID_APPLICATION_NVRAM,   IO_BUS_I2C_1, 0x15, IO_ID_APPRAM_WEN)
	IOM_PERIPH_NVRAM(IO_ID_IDENTITY_NVRAM,      IO_BUS_I2C_1, 0x14, IO_ID_LAST)

    /******************************/
	/* Virtual IO ID's go here... */
	/******************************/
	IOM_VIRT_DI( IO_ID_VIRT_MS_RELAY_ON,                PSM_CH_DINPUT_MS_RELAY_ON)
	IOM_VIRT_DI( IO_ID_VIRT_MS_OVERCURRENT,             PSM_CH_DINPUT_MS_OVERCURRENT)
	IOM_VIRT_DI( IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT,      PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT)
	IOM_VIRT_DI( IO_ID_VIRT_AC_OFF,                     PSM_CH_DINPUT_AC_OFF)
	IOM_VIRT_DI( IO_ID_VIRT_CH_FAULT,                   PSM_CH_DINPUT_CH_FAULT)
	IOM_VIRT_DI( IO_ID_VIRT_CH_ON,                      PSM_CH_DINPUT_CH_ON)
	IOM_VIRT_DI( IO_ID_VIRT_BT_COMMS_FAIL,              PSM_CH_DINPUT_BT_COMMS_FAIL)
	IOM_VIRT_DI( IO_ID_VIRT_BT_DISCONNECTED,            PSM_CH_DINPUT_BT_DISCONNECTED)
	IOM_VIRT_DI( IO_ID_VIRT_BT_OVER_TEMPERATURE,        PSM_CH_DINPUT_BT_OVER_TEMPERATURE)
	IOM_VIRT_DI( IO_ID_VIRT_CH_SYSTEM_RUNNING,       	PSM_CH_DINPUT_CH_SYSTEM_RUNNING)
	IOM_VIRT_DI( IO_ID_VIRT_BT_TEST_FAIL,               PSM_CH_DINPUT_BT_TEST_FAIL)
	IOM_VIRT_DI( IO_ID_VIRT_BT_LOW,                     PSM_CH_DINPUT_BT_LOW)
	IOM_VIRT_DI( IO_ID_VIRT_BT_FAULT,                   PSM_CH_DINPUT_BT_FAULT)

	IOM_VIRT_DI( IO_ID_VIRT_FAN_ON, 	     		 	PSM_CH_DINPUT_FAN_ON)
	IOM_VIRT_DI( IO_ID_VIRT_FAN_FAULT, 	     		 	PSM_CH_DINPUT_FAN_FAULT)

	IOM_VIRT_AI( IO_ID_VIRT_MS_PEAK_HOLD_CURRENT,  	 	PSM_CH_AINPUT_MS_PEAK_HOLD_CURRENT)
	IOM_VIRT_AI( IO_ID_VIRT_DRAIN_CURRENT,   	    	PSM_CH_AINPUT_DRAIN_CURRENT)
	IOM_VIRT_AI( IO_ID_VIRT_ON_ELAPSED_TIME,     	 	PSM_CH_AINPUT_ON_ELAPSED_TIME)

	IOM_VIRT_DI( IO_ID_VIRT_BT_TYPE_NIHM,   			PSM_CH_DINPUT_BT_TYPE_NIHM)
	IOM_VIRT_DI( IO_ID_VIRT_BT_TYPE_LEAD_ACID,   		PSM_CH_DINPUT_BT_TYPE_LEAD_ACID)

//	IOM_VIRT_AI( IO_ID_ACCUMULATED_CHARGE_CURRENT,   	PSM_CH_AINPUT_ACCUMULATED_CHARGE_CURRENT)
//	IOM_VIRT_AI( IO_ID_ACCUMLATED_DISCHARGE_TIME,   	PSM_CH_AINPUT_ACCUMLATED_DISCHARGE_TIME)
//	IOM_VIRT_AI( IO_ID_ACCUMULATED_DISCHARGE_CURRENT,  	PSM_CH_AINPUT_ACCUMULATED_DISCHARGE_CURRENT)
	IOM_VIRT_AI( IO_ID_ACCUMULATED_CHARGE_CURRENT,   	IO_CH_NA)
	IOM_VIRT_AI( IO_ID_ACCUMLATED_DISCHARGE_TIME,   	IO_CH_NA)
	IOM_VIRT_AI( IO_ID_ACCUMULATED_DISCHARGE_CURRENT,  	IO_CH_NA)

	IOM_VIRT_DI(IO_ID_VIRT_AUX_FAULT, 					PSM_CH_DINPUT_AUX_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_AUX_ON, 						PSM_CH_DINPUT_AUX_ON)
	IOM_VIRT_DI(IO_ID_VIRT_COMMS2_FAULT, 				PSM_CH_DINPUT_COMMS2_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_COMMS2_ON, 					PSM_CH_DINPUT_COMMS2_ON)
	IOM_VIRT_DI(IO_ID_VIRT_COMMS1_FAULT, 				PSM_CH_DINPUT_COMMS1_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_COMMS1_ON, 					PSM_CH_DINPUT_COMMS1_ON)
	IOM_VIRT_DI(IO_ID_VIRT_COMMS1_INSTALLED,			PSM_CH_DINPUT_COMMS1_INSTALLED)
	IOM_VIRT_DI(IO_ID_VIRT_COMMS2_INSTALLED, 			PSM_CH_DINPUT_COMMS2_INSTALLED)

//	IOM_VIRT_AI( IO_ID_VIRT_BT_CHARGE_CYCLES,  			PSM_CH_AINPUT_BT_CHARGE_CYCLES)
//	IOM_VIRT_AI( IO_ID_VIRT_BT_ESTIMATED_CAPACITY,  	PSM_CH_AINPUT_BT_ESTIMATED_CAPACITY)
//	IOM_VIRT_AI( IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME,PSM_CH_AINPUT_BT_ACCUMMULATED_CHARGE_TIME)
	IOM_VIRT_AI( IO_ID_VIRT_BT_CHARGE_CYCLES,  			IO_CH_NA)
	IOM_VIRT_AI( IO_ID_VIRT_BT_ESTIMATED_CAPACITY,  	IO_CH_NA)
	IOM_VIRT_AI( IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME,IO_CH_NA)
	
	IOM_VIRT_AI( IO_ID_VIRT_BT_BATTERY_CURRENT,         PSM_CH_AINPUT_BATTERY_CURRENT)

	IOM_VIRT_DI(IO_ID_VIRT_CH_TEST_FAIL,				PSM_CH_DINPUT_CH_TEST_FAIL)
	IOM_VIRT_DI(IO_ID_VIRT_CH_VOLTAGE_FAULT,			PSM_CH_DINPUT_CH_VOLTAGE_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_CH_CURRENT_FAULT,			PSM_CH_DINPUT_CH_CURRENT_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_BT_VOLTAGE_FAULT,			PSM_CH_DINPUT_BT_VOLTAGE_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_BT_TEMP_FAIL,				PSM_CH_DINPUT_BT_TEMP_FAIL)
	IOM_VIRT_DI(IO_ID_VIRT_BT_ID_NVRAM_FAIL,			PSM_CH_DINPUT_BT_ID_NVRAM_FAIL)
	IOM_VIRT_DI(IO_ID_VIRT_BT_DATA_NVRAM_FAIL,			PSM_CH_DINPUT_BT_DATA_NVRAM_FAIL)

	IOM_VIRT_DI(IO_ID_VIRT_BT_CH_SHUTDOWN,              PSM_CH_DINPUT_BT_CH_SHUTDOWN)

	IOM_VIRT_DI(IO_ID_VIRT_BT_CH_INHIBITED,             PSM_CH_DINPUT_BT_CH_INHIBITED)


	IOM_VIRT_AI( IO_ID_EXTERNAL_HUMIDITY,               PSM_CH_AINPUT_EXTERNAL_HUMIDITY)
	IOM_VIRT_AI( IO_ID_EXTERNAL_TEMPERATURE,            PSM_CH_AINPUT_EXTERNAL_TEMPERATURE)

	IOM_VIRT_DI( IO_ID_VIRT_BT_TEST_IN_PROGRESS,     PSM_CH_DINPUT_BT_TEST_IN_PROGRESS)

	IOM_VIRT_DI( IO_ID_VIRT_MS_OVERCURRENT_LIMIT,       PSM_CH_DINPUT_MS_OVERCURRENT_LIMIT)

	IOM_VIRT_DI( IO_ID_BT_SHUTDOWN_APPROACHING,			PSM_CH_DINPUT_BT_SHUTDOWN_APPROACHING)

	IOM_VIRT_AI(IO_ID_VIRT_BATTERY_TEST_TIME,            PSM_CH_AINPUT_BATTERY_TEST_TIME)
	IOM_VIRT_AI(IO_ID_VIRT_BATTERY_TEST_TOTAL_DISCHARGE, PSM_CH_AINPUT_BATTERY_TEST_TOTAL_DISCHARGE)

	IOM_VIRT_DI(IO_ID_VIRT_BT_FULLY_CHARGED,             PSM_CH_DINPUT_BT_FULLY_CHARGED)

	IOM_VIRT_AI(IO_ID_VIRT_BT_CHARGE_CYCLE_TIME,         PSM_CH_AINPUT_BT_CHARGE_CYCLE_TIME)
	IOM_VIRT_AI(IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE,       PSM_CH_AINPUT_BT_CHARGE_CYCLE_CHARGE)

	IOM_VIRT_AI(IO_ID_VIRT_BATTERY_VOLTAGE,       		 PSM_CH_AINPUT_BT_VOLTAGE_FILTERED)
//	IOM_VIRT_AI(IO_ID_VIRT_BATTERY_VOLTAGE,       		 IO_CH_NA)

	IOM_VIRT_DI(IO_ID_VIRT_BT_TEST_HARDWARE_FAULT,		 PSM_CH_DINPUT_BT_TEST_HARDWARE_FAULT)
	IOM_VIRT_DI(IO_ID_VIRT_BT_TEST_PASS,				 PSM_CH_DINPUT_BT_TEST_PASS)
	IOM_VIRT_DI(IO_ID_VIRT_BT_TEST_FAIL_OR_RUN,			 PSM_CH_DINPUT_BT_TEST_FAIL_OR_RUN)
	IOM_VIRT_DI(IO_ID_VIRT_BT_TEST_PASS_OR_RUN,			 PSM_CH_DINPUT_BT_TEST_PASS_OR_RUN)
	IOM_VIRT_DI(IO_ID_VIRT_CHARGER_EN,					 PSM_CH_DINPUT_CHARGER_EN)
	IOM_VIRT_DI(IO_ID_VIRT_CHARGER_CON,					 PSM_CH_DINPUT_CHARGER_CON)
	IOM_VIRT_DI(IO_ID_VIRT_BT_TYPE_DEFAULT,				 PSM_CH_DINPUT_BT_TYPE_DEFAULT)
	
	IOM_VIRT_AI(IO_ID_VIRT_CH_SET_VOLTAGE,       	 	 PSM_CH_AINPUT_CH_SET_VOLTAGE)
	IOM_VIRT_AI(IO_ID_VIRT_CH_SET_CURRENT,       	 	 PSM_CH_AINPUT_CH_SET_CURRENT)
	IOM_VIRT_AI(IO_ID_VIRT_BT_MGR_FSM_STATE,       	 	 PSM_CH_AINPUT_BT_MGR_FSM_STATE)
	IOM_VIRT_AI(IO_ID_VIRT_BT_CHGR_FSM_STATE,       	 PSM_CH_AINPUT_BT_CHGR_FSM_STATE)
	IOM_VIRT_AI(IO_ID_VIRT_BT_TEST_END_REASON,       	 PSM_CH_AINPUT_BT_TEST_END_REASON)
	
	IOM_VIRT_DI(IO_ID_VIRT_NiMH_FAST_CHRG_COMPLETE,		 PSM_CH_DINPUT_NiMH_FAST_CHRG_COMPLETE)
//	IOM_VIRT_AI(IO_ID_VIRT_ON_ELAPSED_TIME_UINT16,       PSM_CH_AINPUT_ON_ELAPSED_TIME_UINT16)

	IOM_GPIO_OUTPUT( IO_ID_FAN_DOUT,	        	IO_CH_NA, \
				PINSEL_PORT_1,  PINSEL_PIN_16,    GPIO_PM_OUTPUT_LOW)

	/* End of table marker */
	IOM_LAST
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
