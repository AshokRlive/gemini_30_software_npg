/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific IO Channel map
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   11/07/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "errorCodes.h"
#include "IOManager.h"
#include "IOManagerIO.h"


/* Local includes */
#include "BoardIOMap.h"
#include "BoardIOChanMap.h"
#include "BoardCalibration.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */





/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */





/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

IOMIOChanDIStr ioChanDIMap[IO_CHAN_DI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_RELAY_ON, 				IO_ID_VIRT_MS_RELAY_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_OVERCURRENT, 			IO_ID_VIRT_MS_OVERCURRENT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_LOCAL_REMOTE_FAULT, 		IO_ID_VIRT_MS_LOCAL_REMOTE_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AC_OFF, 					IO_ID_VIRT_AC_OFF),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_FAULT, 					IO_ID_VIRT_CH_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_ON, 						IO_ID_VIRT_CH_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_COMMS_FAIL, 				IO_ID_VIRT_BT_COMMS_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_DISCONNECTED, 			IO_ID_VIRT_BT_DISCONNECTED),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_OVER_TEMPERATURE, 		IO_ID_VIRT_BT_OVER_TEMPERATURE),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_SYSTEM_RUNNING, 		IO_ID_VIRT_CH_SYSTEM_RUNNING),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_FAIL, 				IO_ID_VIRT_BT_TEST_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_LOW, 					IO_ID_VIRT_BT_LOW),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_FAULT, 					IO_ID_VIRT_BT_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_FAN_ON, 					IO_ID_VIRT_FAN_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_FAN_FAULT, 					IO_ID_VIRT_FAN_FAULT),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TYPE_NIHM, 					IO_ID_VIRT_BT_TYPE_NIHM),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TYPE_LEAD_ACID, 				IO_ID_VIRT_BT_TYPE_LEAD_ACID),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_COMMS2_FAULT, 				    IO_ID_VIRT_COMMS2_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_COMMS2_ON, 					    IO_ID_VIRT_COMMS2_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_COMMS1_FAULT, 					IO_ID_VIRT_COMMS1_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_COMMS1_ON, 						IO_ID_VIRT_COMMS1_ON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_COMMS1_INSTALLED, 				IO_ID_VIRT_COMMS1_INSTALLED),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_COMMS2_INSTALLED, 				IO_ID_VIRT_COMMS2_INSTALLED),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_TEST_FAIL,					IO_ID_VIRT_CH_TEST_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_VOLTAGE_FAULT,				IO_ID_VIRT_CH_VOLTAGE_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CH_CURRENT_FAULT,				IO_ID_VIRT_CH_CURRENT_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_VOLTAGE_FAULT,				IO_ID_VIRT_BT_VOLTAGE_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEMP_FAIL,					IO_ID_VIRT_BT_TEMP_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_ID_NVRAM_FAIL,				IO_ID_VIRT_BT_ID_NVRAM_FAIL),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_DATA_NVRAM_FAIL,				IO_ID_VIRT_BT_DATA_NVRAM_FAIL),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_CH_SHUTDOWN,	             	IO_ID_VIRT_BT_CH_SHUTDOWN),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_CH_INHIBITED,	            IO_ID_VIRT_BT_CH_INHIBITED),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_IN_PROGRESS,	        IO_ID_VIRT_BT_TEST_IN_PROGRESS),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_MS_OVERCURRENT_LIMIT, 			IO_ID_VIRT_MS_OVERCURRENT_LIMIT),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_SHUTDOWN_APPROACHING,        IO_ID_BT_SHUTDOWN_APPROACHING),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_FULLY_CHARGED,               IO_ID_VIRT_BT_FULLY_CHARGED),

	IOM_IOCHAN_DI(PSM_CH_DINPUT_AUX_FAULT, 				        IO_ID_VIRT_AUX_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_AUX_ON, 					    IO_ID_VIRT_AUX_ON),

	//JP - Added diagnostics
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_HARDWARE_FAULT, 		IO_ID_VIRT_BT_TEST_HARDWARE_FAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_PASS, 					IO_ID_VIRT_BT_TEST_PASS),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_FAIL_OR_RUN, 			IO_ID_VIRT_BT_TEST_FAIL_OR_RUN),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TEST_PASS_OR_RUN, 			IO_ID_VIRT_BT_TEST_PASS_OR_RUN),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CHARGER_EN, 					IO_ID_VIRT_CHARGER_EN),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_CHARGER_CON, 					IO_ID_VIRT_CHARGER_CON),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_BT_TYPE_DEFAULT, 				IO_ID_VIRT_BT_TYPE_DEFAULT),
	IOM_IOCHAN_DI(PSM_CH_DINPUT_NiMH_FAST_CHRG_COMPLETE, 	    IO_ID_VIRT_NiMH_FAST_CHRG_COMPLETE),

	IOM_IOCHAN_DI(0, 						                    IO_ID_NA)
};

IOMIOChanDOStr ioChanDOMap[IO_CHAN_DO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_DO(0, 						IO_ID_NA),

	IOM_IOCHAN_DO(0, 						IO_ID_NA)
};

IOMIOChanAIStr ioChanAIMap[IO_CHAN_AI_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */

	IOM_IOCHAN_AI(PSM_CH_AINPUT_MAIN_DC_CURRENT, 			IO_ID_MAIN_DC_I_SENSE, \
				  CAL_ID_DC_I_SENSE, 				IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_LS_CURRENT, 				IO_ID_LOGIC_I_SENSE, \
			      CAL_ID_LOGIC_CURRENT, 				0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_MS_VOLTAGE, 				IO_ID_MOT_V_SENSE, \
				  CAL_ID_MOTOR_VOLTS, 				IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_MS_CURRENT, 				IO_ID_MOT_I_SENSE, \
			      CAL_ID_MOT_I_SENSE, 				IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_MS_PEAK_HOLD_CURRENT, 		IO_ID_VIRT_MS_PEAK_HOLD_CURRENT, \
			      CAL_ID_NA, 					    0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_COMMS_2_AUX_VOLTAGE,        IO_ID_COMMS_2_V_SENSE, \
			      CAL_ID_COMMS_2_AUX_VOLTAGE, 		IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_CH_VOLTAGE, 				IO_ID_CHARGER_V_SENSE,
				  CAL_ID_CHARGER_VOLTAGE, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_CH_CHARGING_CURRENT, 		IO_ID_BAT_CHARGER_I_SENSE, \
			      CAL_ID_CHARGER_CURRENT, 			IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_VOLTAGE, 				IO_ID_BAT_V_SENSE, \
				  CAL_ID_BATTERY_VOLTAGE, 			IO_AI_EVENTRATE_MS),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_DRAIN_CURRENT, 				IO_ID_VIRT_DRAIN_CURRENT, \
			      CAL_ID_NA, 						0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_ON_ELAPSED_TIME, 			IO_ID_VIRT_ON_ELAPSED_TIME, \
			      CAL_ID_NA, 						0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_TS_INTERNAL_TEMPERATURE, 	IO_ID_PSU_TEMP, \
			      CAL_ID_NA, 						0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_TS_BATTERY_TEMPERATURE, 	IO_ID_BAT_TEMP, \
			      CAL_ID_NA, 						0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_36V_RAW_DC, 				IO_ID_36V_RAW_DC_V_SENSE, \
			      CAL_ID_36V_RAW_VOLTAGE, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_COMMS_1_VOLTAGE, 			IO_ID_COMMS_1_V_SENSE, \
				  CAL_ID_COMMS_1_VOLTAGE, 			0),

//	IOM_IOCHAN_AI(PSM_CH_AINPUT_ACCUMULATED_CHARGE_CURRENT, 			IO_ID_ACCUMULATED_CHARGE_CURRENT, \
//				  CAL_ID_NA, 			0),
//	IOM_IOCHAN_AI(PSM_CH_AINPUT_ACCUMLATED_DISCHARGE_TIME, 			IO_ID_ACCUMLATED_DISCHARGE_TIME, \
//				  CAL_ID_NA, 			0),
//	IOM_IOCHAN_AI(PSM_CH_AINPUT_ACCUMULATED_DISCHARGE_CURRENT, 			IO_ID_ACCUMULATED_DISCHARGE_CURRENT, \
//				  CAL_ID_NA, 			0),
//	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_CHARGE_CYCLES, 					IO_ID_VIRT_BT_CHARGE_CYCLES, \
//					  CAL_ID_NA, 			0),
//	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_ESTIMATED_CAPACITY, 					IO_ID_VIRT_BT_ESTIMATED_CAPACITY, \
//					  CAL_ID_NA, 			0),
//	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_ACCUMMULATED_CHARGE_TIME, 			IO_ID_VIRT_BT_ACCUMMULATED_CHARGE_TIME, \
						  CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_LOGIC_PS_VSENSE, 			IO_ID_LOGIC_PS_VSENSE, \
						  CAL_ID_LOGIC_PS_VSENSE, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_VHH_VSENSE, 			IO_ID_VHH_VSENSE, \
						  CAL_ID_VHH_VSENSE, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_VDD_VSENSE, 			IO_ID_VDD_VSENSE, \
							  CAL_ID_VDD_VSENSE, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_BAT_TEST_I_SENSE, 			IO_ID_BAT_TEST_I_SENSE, \
							  CAL_ID_BAT_TEST_I_SENSE, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_EXTERNAL_HUMIDITY, 			IO_ID_EXTERNAL_HUMIDITY, \
								CAL_ID_NA, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_EXTERNAL_TEMPERATURE, 		IO_ID_EXTERNAL_TEMPERATURE, \
								CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_BATTERY_CURRENT, 		    IO_ID_VIRT_BT_BATTERY_CURRENT, \
								CAL_ID_NA, 			0),


	IOM_IOCHAN_AI(PSM_CH_AINPUT_BATTERY_TEST_TIME,            IO_ID_VIRT_BATTERY_TEST_TIME, \
			CAL_ID_NA, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_BATTERY_TEST_TOTAL_DISCHARGE, IO_ID_VIRT_BATTERY_TEST_TOTAL_DISCHARGE, \
			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_CHARGE_CYCLE_TIME,       IO_ID_VIRT_BT_CHARGE_CYCLE_TIME, \
			CAL_ID_NA, 			0),
	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_CHARGE_CYCLE_CHARGE,     IO_ID_VIRT_BT_CHARGE_CYCLE_CHARGE, \
			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_AUX_VOLTAGE,     IO_ID_24V_AUX_V_SENSE, \
			CAL_ID_24V_AUX_VOLTAGE, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_VOLTAGE_FILTERED,     IO_ID_VIRT_BATTERY_VOLTAGE, \
			CAL_ID_NA, 			0),

	// JP Additional diagnostics
	IOM_IOCHAN_AI(PSM_CH_AINPUT_CH_SET_VOLTAGE,     IO_ID_VIRT_CH_SET_VOLTAGE, \
			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_CH_SET_CURRENT,     IO_ID_VIRT_CH_SET_CURRENT, \
			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_MGR_FSM_STATE,     IO_ID_VIRT_BT_MGR_FSM_STATE, \
			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_CHGR_FSM_STATE,     IO_ID_VIRT_BT_CHGR_FSM_STATE, \
			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(PSM_CH_AINPUT_BT_TEST_END_REASON,     IO_ID_VIRT_BT_TEST_END_REASON, \
			CAL_ID_NA, 			0),

//	IOM_IOCHAN_AI(PSM_CH_AINPUT_ON_ELAPSED_TIME_UINT16,		IO_ID_VIRT_ON_ELAPSED_TIME_UINT16, \
//			CAL_ID_NA, 			0),

	IOM_IOCHAN_AI(0, 		                 				IO_ID_NA, \
			      CAL_ID_NA, 						0)
};

IOMIOChanAOStr ioChanAOMap[IO_CHAN_AO_MAP_SIZE + 1] =
{
	/* Must be in same order as channel Enum!!! */
	IOM_IOCHAN_AO(PSM_CH_AOUTPUT_BAT_CHARGER_CUR_LIM_SET,	IO_ID_BAT_CHARGER_CUR_LIM_SET, \
			      CAL_ID_BAT_CHARGER_CUR_LIM_SET),

	IOM_IOCHAN_AO(PSM_CH_AOUTPUT_BAT_CHARGER_VOLT_SET, 		IO_ID_BAT_CHARGER_VOLT_SET, \
			      CAL_ID_BAT_CHARGER_VOLT_SET),

	IOM_IOCHAN_AO(PSM_CH_AOUTPUT_BAT_TEST_CURRENT_SET, 		IO_ID_DAC_OUT_BT_TEST_LOAD, \
				  CAL_ID_BAT_TEST_CURRENT_SET),

	IOM_IOCHAN_AO(0, 										IO_ID_NA, \
				  CAL_ID_NA)
};


/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */




/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */




/*
 *********************** End of file ******************************************
 */
