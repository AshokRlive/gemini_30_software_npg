/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Board specific status manager module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   13/06/11      fryers_j     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "CANProtocolCodec.h"
#include "CANProtocolFraming.h"
#include "ModuleProtocol.h"
#include "StatusManager.h"
#include "BoardStatusManager.h"

#include "NVRam.h"
#include "IOManager.h"
#include "IOManagerIOUpdate.h"
#include "IOManagerIntAnalogIOUpdate.h"
#include "IOManagerExtAnalogIOUpdate.h"
#include "IOManagerI2CIOUpdate.h"
#include "IOManagerI2CExtTempIOUpdate.h"
#include "IOManagerAnalogOutIOUpdate.h"
#include "BoardIO.h"
#include "BoardIOMap.h"

#include "systemAlarm.h"
#include "SysAlarm/SysAlarmStatusManagerEnum.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

#include "BatteryChargerTopLevel.h"
#include "SupplyController.h"
#include "FanController.h"
#include "I2CHumiditySensor.h"
#include "BoardI2CHumiditySensor.h"

#include "systemStatus.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define PSM_PERIOD_TICK_MS		200

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */

typedef enum
{
	BT_HEALTH_STATE_NORMAL,
	BT_HEALTH_STATE_BATTERY_TEST,
	BT_HEALTH_STATE_SHUTDOWN,
	BT_HEALTH_STATE_BATTERY_LOW,

	BT_HEALTH_STATE_LAST
}BT_HEALTH_STATE;

/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


SB_ERROR PSMPowerSaveMode(lu_bool_t powerSaveMode);
static SB_ERROR PSMPeriodicManager(void);

void PSMHealthMonitor(void);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */


static const SMThreadStr boardThreadList[] =
{
	/* run						 cyclic				period(ms)        */
	{CANCDecode, 				 0, 				0 				          }, // Run all the time

	{CANFramingTick, 			 0, 				CAN_TICK_MS		          }, // Run every 10ms

	{IOManagerIORunUpdateGPIO, 	 0, 				IOMAN_UPDATE_GPIO_MS      }, // run every 10ms
	{IOManagerIntAnalogIOUpdate, 0, 				IOMAN_UPDATE_INAI_MS      }, // run every 15 ms
	{IOManagerExtAnalogIOUpdate, 0, 				15						  }, //IOMAN_UPDATE_EXT_AI_MS
	{IOManagerI2CIOUpdate,       0, 				IOMAN_UPDATE_I2CIO_MS     }, // run every 40 ms
	{IOManagerI2CExtTempIOUpdate,0, 				IOMAN_UPDATE_I2CEXTMPIO_MS}, // run every 100 ms
	{IOManagerAnalogOutIOUpdate, 0, 				IOMAN_UPDATE_AO_MS        }, // run every 15 ms
	{IOManagerIOAIEventUpdate,   0,                 IOMAN_UPDATE_INAI_EVENT_MS}, // run every 50 ms

	{NVRamUpdateTick, 		     0, 				NVRAM_UPDATE_TICK_MS 	  }, // run every 100ms

	{BatteryChargerTick, 		 0, 				BATT_CH_TICK_MS 	      }, // run every 1000ms

	{SupplyControlTick, 		 0, 				SUPPLY_CON_TICK_MS 	      }, // run every 1000ms

	{FanControllerTick, 		 0, 				FAN_CON_TICK_MS 	      }, // run every 1000ms

	{i2cHumiditySensorUpdate,    0,                 I2CHUMIDITY_UPDATE_TICK_MS}, // run every 2000ms

	{PSMPeriodicManager,         0,                 PSM_PERIOD_TICK_MS        }, // run every 1000ms

	{(SMThreadRun)0L, 	    	 0, 				0   			          }  // end of table
};



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

void BoardStatusManagerExecutive(void)
{
	BatteryChargerInit();
	FanControllerInit();
	SupplyControlInit();

	boardI2cHumiditySensorInit();

	/* Call status manager - Never return!! */
	StatusManagerRun(&boardThreadList[0]);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR PSMPowerSaveMode(lu_bool_t powerSaveMode)
{
	SB_ERROR retError;
	MODULE_BOARD_ERROR  moduleError;
	static MODULE_BOARD_ERROR oldModuleError = MODULE_BOARD_ERROR_TSYNCH;

	/* Turn off All LED's under software control */
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_OK_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_OK_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_CAN_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_CAN_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_SUPPLY_OK_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_SUPPLY_OK_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_LOGIC_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_LOGIC_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_MOTOR_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_MOTOR_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_BAT_CHRG_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_BAT_CHRG_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_BAT_HEALTH_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_FAN_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_FAN_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_COMMS_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_COMMS_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_DC_IOS_2_24VAUX_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_DC_ISO_2_24VAUX_RED, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_24VAUX_GREEN, powerSaveMode);
	retError = IOManagerForceOutputOff(IO_ID_LED_CTRL_24VAUX_RED, powerSaveMode);
	
	/* Extinguish LED's under hardware control / gated by software */
	retError = IOManagerSetValue(IO_ID_LED_CTRL_EXTINGUISH, powerSaveMode ? 1L : 0L);

	/* Update status LED's */
	moduleError = SSGetBError();

	if (oldModuleError != moduleError)
	{
		oldModuleError = moduleError;

		if (moduleError & MODULE_BOARD_ERROR_ALARM_CRITICAL)
		{
			/* Critical - Fast Flash Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 250);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_ERROR)
		{
			/* Error - Slow Red flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_GREEN, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_RED, 1000);
		} else if (moduleError & MODULE_BOARD_ERROR_ALARM_WARNING)
		{
			/* Warning - Fast Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 250);
		} else
		{
			/* Normal - Slow Green flash */
			retError = IOManagerSetValue(IO_ID_LED_CTRL_OK_RED, 0);
			retError = IOManagerSetFlash(IO_ID_LED_CTRL_OK_GREEN, 1000);
		}
	}

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR PSMPeriodicManager(void)
{
	lu_bool_t   powerSaveMode;
	SB_ERROR    sbError;
	lu_int32_t  btDisconnected;
	lu_int32_t  btShutdownApproaching;
	lu_int32_t  btTestInProgress;
	lu_int32_t  btLow;
	lu_int32_t  chgOn;
	lu_int32_t  value;
	lu_int32_t  tdiffSecs;
	BT_HEALTH_STATE btHealthState;

	static lu_uint32_t sampleTimeMs = 0;
	static BT_HEALTH_STATE oldBtHealthState = BT_HEALTH_STATE_LAST;
	static lu_uint32_t prevTime;
	static lu_bool_t firstTime = LU_TRUE;

	sampleTimeMs += PSM_PERIOD_TICK_MS;

	sbError = IOManagerGetValue(IO_ID_VIRT_BT_DISCONNECTED, &btDisconnected);
	sbError = IOManagerGetValue(IO_ID_BT_SHUTDOWN_APPROACHING, &btShutdownApproaching);
	sbError = IOManagerGetValue(IO_ID_VIRT_BT_LOW, &btLow);
	sbError = IOManagerGetValue(IO_ID_VIRT_BT_TEST_IN_PROGRESS, &btTestInProgress);
	sbError = IOManagerGetValue(IO_ID_VIRT_CH_ON, &chgOn);

	if (chgOn)
	{
		if( firstTime == LU_TRUE)
		{
			prevTime = STGetTime();
			firstTime = LU_FALSE;
		}
		else
		{
			/* Update battery charge running time */
			sbError = IOManagerGetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_TIME, &value);
			tdiffSecs = (STElapsedTime(prevTime, STGetTime()) / MSEC);

			if (tdiffSecs > 0)
			{
				value += (STElapsedTime(prevTime, STGetTime()) / MSEC);
				sbError = IOManagerSetValue(IO_ID_VIRT_BT_CHARGE_CYCLE_TIME, value);
				prevTime = STGetTime();
			}
		}
	}
	else
	{
		firstTime = LU_FALSE;
	}

	if (sampleTimeMs > 1000)
	{
		sampleTimeMs = 0;
// Moved to BatteryChargerTopLevel.c:upDateOutputConditions()
#if 0
		if (btTestInProgress)
		{
			/* Update battery test running time */
			sbError = IOManagerGetValue(IO_ID_VIRT_BATTERY_TEST_TIME, &value);
			value++;
			sbError = IOManagerSetValue(IO_ID_VIRT_BATTERY_TEST_TIME, value);

			btHealthState = BT_HEALTH_STATE_BATTERY_TEST;
		}
		else if (btDisconnected || btLow)
		{
			btHealthState = BT_HEALTH_STATE_BATTERY_LOW;
		}
		else if (btShutdownApproaching)
		{
			btHealthState = BT_HEALTH_STATE_SHUTDOWN;
		}
		else
		{
			btHealthState = BT_HEALTH_STATE_NORMAL;
		}

		if (btHealthState != oldBtHealthState)
		{
			oldBtHealthState = btHealthState;

			switch(btHealthState)
			{
			case BT_HEALTH_STATE_BATTERY_TEST:
				/* Flashing green battery health */
				sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, 0);
				sbError = IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, 250);
				break;

			case BT_HEALTH_STATE_SHUTDOWN:
				/* Flashing red battery health */
				sbError = IOManagerSetFlash(IO_ID_LED_CTRL_BAT_HEALTH_RED, 250);
				sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, 0);
				break;

			case BT_HEALTH_STATE_BATTERY_LOW:
				/* red battery health */
				sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, 1);
				sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, 0);
				break;

			default:
			case BT_HEALTH_STATE_NORMAL:
				/* green battery health */
				sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_RED, 0);
				sbError = IOManagerSetValue(IO_ID_LED_CTRL_BAT_HEALTH_GREEN, 1);
				break;
			}


		}
#endif

	}

	/* Put PSM board online */
    if(SSGetBStatus() == MODULE_BOARD_STATUS_STARTING)
    {
    	/* Allow the IOManager to report events */
		IOManagerStartEventing();

		/* Start PSM subsystem */

        SSSetBStatus(MODULE_BOARD_STATUS_ON_LINE);
    }

    /* Power saving mode check - LED off */
    powerSaveMode = SSGetPowerSaveMode();
    PSMPowerSaveMode(powerSaveMode);

	/*
	 * Monitor the handshake and system healthy signals
	 * from the hardware watchdog PIC
	 */
    PSMHealthMonitor();

    return SB_ERROR_NONE;
}


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
void PSMHealthMonitor(void)
{
	SB_ERROR 	retError;
	static	lu_bool_t initialised 		 = LU_FALSE;
	static	lu_int32_t lastHandshake 	 = 0;
	static	lu_int32_t lastSystemHealthy = 0;
	static 	lu_int32_t lastHandshakeTime = 0;
	static	lu_bool_t handshakeAlarmSet	 = LU_FALSE;
	static	lu_bool_t sysHealthyAlarmSet = LU_FALSE;

	lu_int32_t 	wdhandshake;
	lu_int32_t 	sysHealthy;
	lu_int32_t 	time;
	lu_int32_t 	elapsedTime;

	/*
	 * Get current time
	 */
	time = STGetTime();

	/*
	 * If this is the first execution of the function then
	 * initialise the times and outputs
	 */
	if( initialised == LU_FALSE )
	{
		/* Initialisation - Wait for handshake to go low */
		retError = IOManagerGetValue(IO_ID_WD_HANDSHAKE, &wdhandshake);
		retError = IOManagerGetValue(IO_ID_SYS_HEALTHY, &sysHealthy);
//		if( wdhandshake == 0 )
		{
			/* Start kicking the PIC watch dog every second */
			retError = IOManagerSetFlash(IO_ID_HWDOG_1HZ_KICK, ONE_HZ_KICK_PERIOD);
			lastHandshake = 0;
			lastSystemHealthy = sysHealthy;

			/* Initialise the timer for the handshake */
			lastHandshakeTime = time;

			initialised = LU_TRUE;
			return;
		}
	}

	/*
	 * Get time since last handshake
	 */
	elapsedTime = STElapsedTime( lastHandshakeTime , time );

	/*
	 * Read the state of the handshake signal from the PIC
	 */
	retError = IOManagerGetValue(IO_ID_WD_HANDSHAKE, &wdhandshake);

	/*
	 * Check for a change of state in the handshake signal
	 */
	if ( lastHandshake != wdhandshake )
	{
		/*
		 * A change of state has been detected so reset timer and handshake state
		 */
		lastHandshake = wdhandshake;
		lastHandshakeTime = time;
		/* Clear alarm if already set */
		if( handshakeAlarmSet == LU_TRUE )
		{
			SysAlarmSetLogEvent(LU_FALSE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
								SYSALC_STATUS_SYSTEM_WD_HANDSHAKE,
								0
							   );
			handshakeAlarmSet = LU_FALSE;
		}
	}
	else if( elapsedTime > PIC_HANDSHAKE_ERROR_PERIOD )
	{
		/*
		 * A change of state of the handshake signal has not been received
		 * so set the alarm if it hasn't been set already
		 */
		if( handshakeAlarmSet == LU_FALSE )
		{
			SysAlarmSetLogEvent(LU_TRUE,
								SYS_ALARM_SEVERITY_ERROR,
								SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
								SYSALC_STATUS_SYSTEM_WD_HANDSHAKE,
								0
							   );
			handshakeAlarmSet = LU_TRUE;
		}

	}

	/*
	 * Read the state of the system healthy signal from the PIC
	 */
	retError = IOManagerGetValue(IO_ID_SYS_HEALTHY, &sysHealthy);

	/*
	 * Set the alarm if the system healthy signal goes low
	 * Clear the alarm if it goes high
	 */
	if( sysHealthy != lastSystemHealthy )
	{
		lastSystemHealthy = sysHealthy;
		if( sysHealthy == 0 )
		{
			/* Set the alarm if not already set*/
			if( sysHealthyAlarmSet == LU_FALSE )
			{
				SysAlarmSetLogEvent(LU_TRUE,
									SYS_ALARM_SEVERITY_CRITICAL,
									SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
									SYSALC_STATUS_SYSTEM_HEALTH,
									0
								   );
				sysHealthyAlarmSet = LU_TRUE;
			}
		}
		else
		{
			/* Clear the alarm if already set */
			if( sysHealthyAlarmSet == LU_TRUE )
			{
				SysAlarmSetLogEvent(LU_FALSE,
									SYS_ALARM_SEVERITY_CRITICAL,
									SYS_ALARM_SUBSYSTEM_STATUS_MANAGER,
									SYSALC_STATUS_SYSTEM_HEALTH,
									0
								   );
				sysHealthyAlarmSet = LU_FALSE;
			}
		}
	}

}


/*
 *********************** End of file ******************************************
 */
