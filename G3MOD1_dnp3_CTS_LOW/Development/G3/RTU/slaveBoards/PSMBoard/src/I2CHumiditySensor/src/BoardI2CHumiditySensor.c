/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       I2C Humidity Sensor module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   07/05/12      fryers_j    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */

#include <stdlib.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lpc17xx_i2c.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "lu_types.h"

#include "I2CHumiditySensor.h"

#include "IOManager.h"

#include "BoardIOMap.h"

/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */



/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR boardI2cHumiditySensorInit(void)
{
	 static I2CHumidityParmStr params;

	 params.HumidityIoIO = IO_ID_EXTERNAL_HUMIDITY;
	 params.tempIoID     = IO_ID_EXTERNAL_TEMPERATURE;

	 params.i2cChan      = LPC_I2C0;
	 params.sclPort      = PINSEL_PORT_0;
	 params.sclPin 		 = PINSEL_PIN_28;
	 params.sclFunc      = PINSEL_FUNC_1;

	 params.sdaPort      = PINSEL_PORT_0;
	 params.sdaPin       = PINSEL_PIN_27;
	 params.sdaFunc      = PINSEL_FUNC_1;

	 return i2cHumiditySensorInit(&params);
}


/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

/*!
 ******************************************************************************
 *   \brief
 *
 *
 *
 *   \param
 *
 *   \return Error code
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
