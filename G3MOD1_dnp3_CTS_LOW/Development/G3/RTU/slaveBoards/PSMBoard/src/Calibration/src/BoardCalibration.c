/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"

#include "systemStatus.h"
#include "systemAlarm.h"
#include "SysAlarm/SysAlarmSystemEnum.h"

#include "errorCodes.h"
#include "CANProtocolCodec.h"

#include "LinearInterpolation.h"
#include "Calibration.h"
#include "BoardCalibration.h"
#include "BoardIOMap.h"

#include "NVRam.h"


/*
 ******************************************************************************
 * LOCAL - Definitions And Macros
 ******************************************************************************
 */

#define MAX_CAL_ID_MOT_I_SENSE_1DU16U16				9
#define MAX_CAL_ID_BAT_CHARGER_CUR_LIM_SET_1DU16U16	5
#define MAX_CAL_ID_BAT_CHARGER_VOLT_SET_1DU16U16	16
#define MAX_CAL_ID_CHARGER_VOLTAGE_1DU16U16			5
#define MAX_CAL_ID_LOGIC_CURRENT_1DU16U16			7
#define MAX_CAL_ID_MOTOR_VOLTS_1DU16U16				4
#define MAX_CAL_ID_CHARGER_CURRENT_1DU16U16			10
#define MAX_CAL_ID_BATTERY_VOLTAGE_1DU16U16			9
#define MAX_CAL_ID_36V_RAW_VOLTAGE_1DU16U16			5
#define MAX_CAL_ID_DC_I_SENSE_1DU16U16				7
#define MAX_CAL_ID_COMMS_2_AUX_VOLTAGE_1DU16U16		6
#define MAX_CAL_ID_COMMS_1_VOLTAGE_1DU16U16			6

#define MAX_CAL_ID_LOGIC_PS_VSENSE					7
#define MAX_CAL_ID_VHH_VSENSE						3
#define MAX_CAL_ID_VDD_VSENSE						3
#define MAX_CAL_ID_BAT_TEST_I_SENSE					3

#define MAX_CAL_ID_BAT_TEST_CURR_SET 				2
#define MAX_CAL_ID_24V_AUX_VOLTS 					2

/*
 ******************************************************************************
 * LOCAL - Typedefs and Structures
 ******************************************************************************
 */


/*
 *******************************************************************************
 * LOCAL - Prototypes Of Local Functions
 *******************************************************************************
 */

SB_ERROR BoardCalTestNvramSelect(CalTstNvramSelStr *calTstNvramPtr);

/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


/*
 ******************************************************************************
 * LOCAL - Global Variables
 ******************************************************************************
 */

/* 0 Motor current */
static const LnInterpTable1DU16U16Str CalIdMotISenseTableU16U16[MAX_CAL_ID_MOT_I_SENSE_1DU16U16] =
{
		/* CAL_ID_MOT_I_SENSE.csv  9 Points*/

		/*
		 * Input value ,       Output value (ma)
		 */
		{54,					0},
		{61,					300},
		{80,					500},
		{103,					700},
		{141,					1000},
		{276,					2000},
		{685,					5000},
		{802,					5900},
		{4095,					24910}
};

/* 1 Battery charger current limit set */
static const LnInterpTable1DU16U16Str CalIdBatChargerCurLim_SetTable1dU16U16[MAX_CAL_ID_BAT_CHARGER_CUR_LIM_SET_1DU16U16] =
{
  		/* CAL_ID_BAT_CHARGER_CUR_LIM_SET.csv 5 Points*/

		/*
	   	 * Input value(mA) ,       Output value(DigiPot)
 		 */
   		{ 100,						255},
   		{ 640,						213},
   		{ 1300,						116},
   		{ 1670,						19},
   		{ 1800,						0}

};

/* 2 Battery charger voltage limit set */
static const LnInterpTable1DU16U16Str CalIdBatChargerVoltSetTable1dU16U16[MAX_CAL_ID_BAT_CHARGER_VOLT_SET_1DU16U16] =
{
		/* CAL_ID_BATTERY_VOLTAGE.csv 5 Points */

		/*
		 * Input value(mv) ,       Outputvalue(DigiPot)
		 */
		{15094,255},
		{15461,245},
		{20225,132},
		{21875,105},
		{22424,96},
		{23249,84},
		{23616,78},
		{24623,66},
		{25143,60},
		{26151,48},
		{26670,42},
		{27861,30},
		{28441,24},
		{29541,12},
		{29541,6},
		{29510,0}

};

/* 3 Charger voltage */
static const LnInterpTable1DU16U16Str CalIdChargerVoltageTable1dU16U16[MAX_CAL_ID_CHARGER_VOLTAGE_1DU16U16] =
{
		/* CAL_ID_CHARGER_VOLTAGE.csv 5 Points */
		/*
		 * Input ADC value ,       Output value(mV)
		 */
		{2,0},
		{1491,19126},
		{2016,25876},
		{2294,29510},
		{4095,52000}
};

/* 4 Logic current */
static const LnInterpTable1DU16U16Str CalIdLogicCurrentTable1dU16U16[MAX_CAL_ID_LOGIC_CURRENT_1DU16U16] =
{
		/* CAL_ID_LOGIC_CURRENT.csv 7 Points */

		/*
		 * Input ADC value ,       Output value(mA)
		 */
		{108,0},
		{140,150},
		{179,300},
		{223,450},
		{268,600},
		{484,1300},
		{4095,4760}

};

/* 5 Motor Volts */
static const LnInterpTable1DU16U16Str CalIdMotorVoltsTable1dU16U16[MAX_CAL_ID_MOTOR_VOLTS_1DU16U16] =
{
		/* CAL_ID_MOTOR_VOLTS.csv 7 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,0},
		{1842,24060},
		{2762,36130},
		{4095,52800}

};

/* 6 Charger Current */
static const LnInterpTable1DU16U16Str CalIdChargerCurrentTable1dU16U16[MAX_CAL_ID_CHARGER_CURRENT_1DU16U16] =
{
		/* CAL_ID_CHARGER_CURRENT.csv 7 Points */

		/*
		 * Input ADC value ,       Outputvalue(ma)
		 */
		{12,0},
		{150,100},
		{441,250},
		{735,400},
		{1026,550},
		{1318,700},
		{1417,750},
		{1708,900},
		{1864,980},
		{4095,1040}

};

/* 7 Battery Voltage */
static const LnInterpTable1DU16U16Str CalIdBatteryVoltageTable1dU16U16[MAX_CAL_ID_BATTERY_VOLTAGE_1DU16U16] =
{
		/* CAL_ID_BATTERY_VOLTAGE.csv 5 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{20,529},
		{377,4867},
		{744,9542},
		{1111,14308},
		{1538,19746},
		{1902,24451},
		{2162,27843},
		{2407,30990},
		{4095,57900}

};

/* 8 36V Raw DC Voltage */
static const LnInterpTable1DU16U16Str CalId36VRawVoltageTable1dU16U16[MAX_CAL_ID_36V_RAW_VOLTAGE_1DU16U16] =
{
		/* CAL_ID_36V_RAW_VOLTAGE.csv 5 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,0},
		{2306,30000},
		{2519,33000},
		{2762,36130},
		{4095,52800}

};

/* 9 Main DC I Sense */
static const LnInterpTable1DU16U16Str CalIdDcISenseTable1dU16U16[MAX_CAL_ID_DC_I_SENSE_1DU16U16] =
{
		/* CAL_ID_DC_I_SENSE.csv 8 Points*/

		/*
		 * Input ADC value ,       Output value(ma)
		 */
		{24,70},
		{358,570},
		{690,1070},
		{968,1570},
		{1261,2070},
		{1882,3070},
		{4095,4760}
};

/* 10 Comms2/Aux voltage */
static const LnInterpTable1DU16U16Str CalIdComms2_AuxVoltageTable1dU16U16[MAX_CAL_ID_COMMS_2_AUX_VOLTAGE_1DU16U16] =
{
		/* CAL_ID_COMMS_2_AUX_VOLTAGE.csv 5 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,32000},
		{620,26000},
		{720,24000},
		{2323,14000},
		{2665,12000},
		{4095,0}

};

/* 11 Comms 1 Voltage */
static const LnInterpTable1DU16U16Str CalIdComms1Voltage1dU16U16[MAX_CAL_ID_COMMS_1_VOLTAGE_1DU16U16] =
{
		/* CAL_ID_COMMS_1_VOLTAGE.csv 6 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,32000},
		{620,26000},
		{720,24000},
		{2323,14000},
		{2665,12000},
		{4095,0}
};

/* 12 Logic PS V Sense */
static const LnInterpTable1DU16U16Str CalIdLogicPsVSense1dU16U16[MAX_CAL_ID_LOGIC_PS_VSENSE] =
{
		/* CAL_ID_LOGIC_PS_VSENSE.csv 7 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,0},
		{1842,24060},
		{2010,26200},
		{2306,30000},
		{2519,33000},
		{2762,36130},
		{4095,52300}

};

/* 13 VHH V Sense */
static const LnInterpTable1DU16U16Str CalIdVHHVSense1dU16U16[MAX_CAL_ID_VHH_VSENSE] =
{
		/* CAL_ID_VHH_VSENSE.csv 3 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,							0},
		{2047,						3300},
		{4095,						6600}

};

/* 14 VDD V Sense */
static const LnInterpTable1DU16U16Str CalIdVDDVSense1dU16U16[MAX_CAL_ID_VDD_VSENSE] =
{
		/* CAL_ID_VDD_VSENSE.csv 3 Points */

		/*
		 * Input ADC value ,       Outputvalue(mv)
		 */
		{0,							0},
		{2047,						3300},
		{4095,						6600}

};

/* 15 Bat Test I Sense */
static const LnInterpTable1DU16U16Str CalIdBatTestISense1dU16U16[MAX_CAL_ID_BAT_TEST_I_SENSE] =
{
		/* CAL_ID_BAT_TEST_I_SENSE.csv 2 Points */

		/*
		 * Input ADC value ,       Outputvalue(ma)
		 */
		{0,							0},
		{2047,						3300},
		{4095,						6600}
};

/* 16 Bat Test Current set */
static const LnInterpTable1DU16U16Str CalIdBatTestCur1dU16U16[MAX_CAL_ID_BAT_TEST_CURR_SET] =
{
		/* CAL_ID_BAT_TEST_CURRENT_SET - 2 points */
		/*
		 * Input value(mA) ,       Output value(DigiPot)
		 */
		{0,		                       0},
		{13000,						1023}

};

/* 17 24v aux voltage */
static const LnInterpTable1DU16U16Str CalId24VAuxVoltageTable1dU16U16[MAX_CAL_ID_24V_AUX_VOLTS] =
{
		/* CAL_ID_24V_AUX_VOLTAGE - 2 points */

		/*
		 * Input ADC value ,       Outputvalue(ma)
		 */
		{ 0,						0},
		{ 4095,						52800}

};

CalElementStr boardCalTable[MAX_CAL_ID + 1] =
{
	CALID_ELEMENT(CAL_ID_MOT_I_SENSE)
	CALID_ELEMENT(CAL_ID_BAT_CHARGER_CUR_LIM_SET)
	CALID_ELEMENT(CAL_ID_BAT_CHARGER_VOLT_SET)
	CALID_ELEMENT(CAL_ID_CHARGER_VOLTAGE)
	CALID_ELEMENT(CAL_ID_LOGIC_CURRENT)
	CALID_ELEMENT(CAL_ID_MOTOR_VOLTS)
	CALID_ELEMENT(CAL_ID_CHARGER_CURRENT)
	CALID_ELEMENT(CAL_ID_BATTERY_VOLTAGE)
	CALID_ELEMENT(CAL_ID_36V_RAW_VOLTAGE)
	CALID_ELEMENT(CAL_ID_DC_I_SENSE)
	CALID_ELEMENT(CAL_ID_COMMS_2_AUX_VOLTAGE)
	CALID_ELEMENT(CAL_ID_COMMS_1_VOLTAGE)

	CALID_ELEMENT(CAL_ID_LOGIC_PS_VSENSE)
	CALID_ELEMENT(CAL_ID_VHH_VSENSE)
	CALID_ELEMENT(CAL_ID_VDD_VSENSE)
	CALID_ELEMENT(CAL_ID_BAT_TEST_I_SENSE)

	CALID_ELEMENT(CAL_ID_BAT_TEST_CURRENT_SET)
	CALID_ELEMENT(CAL_ID_24V_AUX_VOLTAGE)

	/* End Table Marker */
	CALID_ELEMENT_LAST()
};


/*! List of supported message */
static const filterTableStr BoardCalibrationModulefilterTable[] =
{
    /* messageType            messageID                                 broadcast fragment */

	/*! Bootloader test API commands */
	{  MODULE_MSG_TYPE_CALTST, MODULE_MSG_ID_CALTST_NVRAM_SELECT_C      , LU_FALSE , 0      }
};

/*
 ******************************************************************************
 * Exported Functions
 ******************************************************************************
 */

SB_ERROR BoardCalibrationInit(void)
{
	SB_ERROR      retError;
	lu_uint8_t    *calDataPtr;
	lu_uint16_t   calDataSize;
	lu_bool_t	  primaryFail;
	lu_bool_t     secondaryFail;
	lu_uint16_t   maxCalId;

	/* To add calibration commands to CAN filter */
	CANFramingAddFilter( BoardCalibrationModulefilterTable,
						 SU_TABLE_SIZE(BoardCalibrationModulefilterTable, filterTableStr)
					   );

	primaryFail   = LU_FALSE;
	secondaryFail = LU_FALSE;

	maxCalId = MAX_CAL_ID;

	if (SSGetFeatureRevisionMajor() == 0 &&
	    SSGetFeatureRevisionMinor() == 1)
	{
		// PSM Feature revision before 0.2 does not have dynamic battery test load or 24v aux analogue
		maxCalId = (CAL_ID_BAT_TEST_I_SENSE + 1);
	}

	// Get NVRAM data pointer
	calDataSize = NVRAM_ID_BLK_CAL_SIZE;
	retError = NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_CAL, &calDataPtr);
	if (retError != SB_ERROR_NONE)
	{
		primaryFail   = LU_TRUE;
	}
	else
	{
		/* To check the sequence of calibration ID and boundary */
		retError = CalibrationInitCalDataPtr(calDataPtr, calDataSize, maxCalId);
		if (retError != SB_ERROR_NONE)
		{
			primaryFail   = LU_TRUE;
		}
	}

	if (primaryFail == LU_TRUE)
	{
		calDataSize = NVRAM_APP_BLK_CAL_SIZE;
		retError = NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_CAL, &calDataPtr);
		if (retError != SB_ERROR_NONE)
		{
			secondaryFail   = LU_TRUE;
		}
		else
		{
			/* To check the sequence of calibration ID and boundary */
			retError = CalibrationInitCalDataPtr(calDataPtr, calDataSize, maxCalId);
			if (retError != SB_ERROR_NONE)
			{
				secondaryFail   = LU_TRUE;
			}
		}
	}

	if (secondaryFail == LU_TRUE)
	{
		SysAlarmSetLogEvent(LU_TRUE,
									SYS_ALARM_SEVERITY_WARNING,
									SYS_ALARM_SUBSYSTEM_SYSTEM,
									SYSALC_SYSTEM_FACTORY_CAL,
									0
								   );
	}

	if (primaryFail == LU_TRUE)
	{
		/* Using fixed calibration - since all calibration is corrupted */
		SysAlarmSetLogEvent(LU_TRUE,
							SYS_ALARM_SEVERITY_ERROR,
							SYS_ALARM_SUBSYSTEM_SYSTEM,
							SYSALC_SYSTEM_PRIMARY_FACTORY_CAL,
							0
						   );
	}

	if (primaryFail == LU_TRUE && secondaryFail == LU_TRUE)
	{
		/* Using fixed calibration - since all calibration is corrupted */
		retError = CalibrationInitCalDataPtr(calDataPtr, calDataSize, MAX_CAL_ID);

		/* To erase all the calibration data*/
		CalibrationEraseAllElements();

		/* Note you must add in same order as defined in CAL_ID enum!! */
		retError = CalibrationAddElement(CAL_ID_MOT_I_SENSE,
							 		     CAL_TYPE_1DU16U16,
									     sizeof(CalIdMotISenseTableU16U16),
									     (lu_uint8_t *)CalIdMotISenseTableU16U16);

		retError = CalibrationAddElement(CAL_ID_BAT_CHARGER_CUR_LIM_SET,
									     CAL_TYPE_1DU16U16,
										 sizeof(CalIdBatChargerCurLim_SetTable1dU16U16),
										 (lu_uint8_t *)CalIdBatChargerCurLim_SetTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_BAT_CHARGER_VOLT_SET,
									 	 CAL_TYPE_1DU16U16,
										 sizeof(CalIdBatChargerVoltSetTable1dU16U16),
										 (lu_uint8_t *)CalIdBatChargerVoltSetTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_CHARGER_VOLTAGE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdChargerVoltageTable1dU16U16),
										 (lu_uint8_t *)CalIdChargerVoltageTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_LOGIC_CURRENT,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdLogicCurrentTable1dU16U16),
										 (lu_uint8_t *)CalIdLogicCurrentTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_MOTOR_VOLTS,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdMotorVoltsTable1dU16U16),
										 (lu_uint8_t *)CalIdMotorVoltsTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_CHARGER_CURRENT,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdChargerCurrentTable1dU16U16),
										 (lu_uint8_t *)CalIdChargerCurrentTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_BATTERY_VOLTAGE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdBatteryVoltageTable1dU16U16),
										 (lu_uint8_t *)CalIdBatteryVoltageTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_36V_RAW_VOLTAGE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalId36VRawVoltageTable1dU16U16),
										 (lu_uint8_t *)CalId36VRawVoltageTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_DC_I_SENSE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdDcISenseTable1dU16U16),
										 (lu_uint8_t *)CalIdDcISenseTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_COMMS_2_AUX_VOLTAGE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdComms2_AuxVoltageTable1dU16U16),
										 (lu_uint8_t *)CalIdComms2_AuxVoltageTable1dU16U16);

		retError = CalibrationAddElement(CAL_ID_COMMS_1_VOLTAGE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdComms1Voltage1dU16U16),
										 (lu_uint8_t *)CalIdComms1Voltage1dU16U16);


		retError = CalibrationAddElement(CAL_ID_LOGIC_PS_VSENSE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdLogicPsVSense1dU16U16),
										 (lu_uint8_t *)CalIdLogicPsVSense1dU16U16);

		retError = CalibrationAddElement(CAL_ID_VHH_VSENSE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdVHHVSense1dU16U16),
										 (lu_uint8_t *)CalIdVHHVSense1dU16U16);

		retError = CalibrationAddElement(CAL_ID_VDD_VSENSE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdVDDVSense1dU16U16),
										 (lu_uint8_t *)CalIdVDDVSense1dU16U16);

		retError = CalibrationAddElement(CAL_ID_BAT_TEST_I_SENSE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdBatTestISense1dU16U16),
										 (lu_uint8_t *)CalIdBatTestISense1dU16U16);

		retError = CalibrationAddElement(CAL_ID_BAT_TEST_CURRENT_SET,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalIdBatTestCur1dU16U16),
										 (lu_uint8_t *)CalIdBatTestCur1dU16U16);

		retError = CalibrationAddElement(CAL_ID_24V_AUX_VOLTAGE,
										 CAL_TYPE_1DU16U16,
										 sizeof(CalId24VAuxVoltageTable1dU16U16),
										 (lu_uint8_t *)CalId24VAuxVoltageTable1dU16U16);

	}

	CalibrationInterChangeColumns(CAL_ID_BAT_CHARGER_CUR_LIM_SET, 0, 3000, 0, 255 );

	/* To check the sequence of calibration ID and boundary */
	CalibrationInitCalDataPtr(calDataPtr, calDataSize, maxCalId);

	return SB_ERROR_NONE;
}

SB_ERROR BoardCalibrationGetTemperature(lu_int32_t *tempValuePtr)
{
	return IOManagerGetValue(IO_ID_PSU_TEMP, tempValuePtr);
}

SB_ERROR BoardCalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time)
{
	SB_ERROR	retVal;

	LU_UNUSED(msgPtr);
	LU_UNUSED(time);

	retVal = SB_ERROR_CANC;

	switch (msgPtr->messageType)
	{
	case MODULE_MSG_TYPE_CALTST:
		switch (msgPtr->messageID)
		{
		case MODULE_MSG_ID_CALTST_NVRAM_SELECT_C:
			/* Message sanity check */
			if (msgPtr->msgLen == MODULE_MESSAGE_SIZE(CalTstNvramSelStr))
			{
				BoardCalTestNvramSelect((CalTstNvramSelStr *)msgPtr->msgBufPtr);
			}
			break;

		default:
			retVal = SB_ERROR_CANC_NOT_HANDLED;
			break;
		}
		break;

	default:
		retVal = SB_ERROR_CANC_NOT_HANDLED;
		break;
	}

	return retVal;
}

/*
 ******************************************************************************
 * Local Functions
 ******************************************************************************
 */

SB_ERROR BoardCalTestNvramSelect(CalTstNvramSelStr *calTstNvramPtr)
{
	SB_ERROR 	   retError;
	lu_uint8_t    *calDataPtr;

	retError = SB_ERROR_PARAM;

	switch (calTstNvramPtr->nvramType)
	{
	case NVRAM_TYPE_IDENTITY:
		NVRamIndenityGetMemoryPointer(NVRAM_ID_BLK_CAL, &calDataPtr);
		retError = CalibrationInitCalDataPtr(calDataPtr, NVRAM_ID_BLK_CAL_SIZE, MAX_CAL_ID);
		break;

	case NVRAM_TYPE_APPLICATION:
		NVRamApplicationGetMemoryPointer(NVRAM_APP_BLK_CAL, &calDataPtr);
		retError = CalibrationInitCalDataPtr(calDataPtr, NVRAM_APP_BLK_CAL_SIZE, MAX_CAL_ID);
		break;

	default:
		break;
	}

	retError = CANCSendMCM( MODULE_MSG_TYPE_CALTST,
			                MODULE_MSG_ID_CALTST_NVRAM_SELECT_R,
								1,
								(lu_uint8_t*)&retError
							  );

	return retError;
}

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */


/*
 *********************** End of file ******************************************
 */
