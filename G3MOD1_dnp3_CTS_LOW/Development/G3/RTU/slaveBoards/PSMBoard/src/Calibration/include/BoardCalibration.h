/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id$
 *               $HeadURL$
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Rev:: $: (Revision of last commit)
 *               $Author:: $: (Author of last commit)
 *       \date   $Date:: $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   17/05/11      galli_m     Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef _BOARD_CALIBRATION_INCLUDED
#define _BOARD_CALIBRATION_INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */

#include "lu_types.h"
#include "Calibration.h"


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

#define MAX_CAL_ID 		    CAL_ID_LAST


/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */

/*! Calibration ID - Calibration Elements */
typedef enum
{
	CAL_ID_MOT_I_SENSE       = 0,
	CAL_ID_BAT_CHARGER_CUR_LIM_SET,
	CAL_ID_BAT_CHARGER_VOLT_SET,
	CAL_ID_CHARGER_VOLTAGE,
	CAL_ID_LOGIC_CURRENT,
	CAL_ID_MOTOR_VOLTS,
	CAL_ID_CHARGER_CURRENT,
	CAL_ID_BATTERY_VOLTAGE,
	CAL_ID_36V_RAW_VOLTAGE,
	CAL_ID_DC_I_SENSE,
	CAL_ID_COMMS_2_AUX_VOLTAGE,
	CAL_ID_COMMS_1_VOLTAGE,

	CAL_ID_LOGIC_PS_VSENSE,
	CAL_ID_VHH_VSENSE,
	CAL_ID_VDD_VSENSE,
	CAL_ID_BAT_TEST_I_SENSE,

	CAL_ID_BAT_TEST_CURRENT_SET,
	CAL_ID_24V_AUX_VOLTAGE,

	CAL_ID_LAST
} CAL_ID;


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */

extern CalElementStr boardCalTable[MAX_CAL_ID + 1];


/*
 ******************************************************************************
 * EXPORTED - Functions
 ******************************************************************************
 */


/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description 
 *
 *
 *   \return 
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationInit(void);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationGetTemperature(lu_int32_t *tempValuePtr);

/*!
 ******************************************************************************
 *   \brief Brief description
 *
 *   Detailed description
 *
 *   \param parameterName Description
 *
 *
 *   \return
 *
 ******************************************************************************
 */
SB_ERROR BoardCalibrationCANProtocolDecoder(CANFramingMsgStr *msgPtr, lu_uint32_t time);

#endif /* _BOARD_CALIBRATION_INCLUDED */

/*
 *********************** End of file ******************************************
 */
