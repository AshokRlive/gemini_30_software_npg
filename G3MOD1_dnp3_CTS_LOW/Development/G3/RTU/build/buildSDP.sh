#!/bin/bash

# Environment variables
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
INSTALL_DIR=${CURRENT_DIR}/installed
INSTALL_DIR_MCM=$INSTALL_DIR/MCM
INSTALL_DIR_SLAVE=$INSTALL_DIR/Slave
INSTALL_DIR_UTILITY=$INSTALL_DIR/Uitlity
BUILD_TYPE=Release


# Function to check the return code, exit if there is an error.
checkErr(){
RESULT=$?
if [ $RESULT -ne 0 ]; then
	echo "The building process failed with error:$RESULT"
	exit $RESULT
fi
}

# Build MCM
bash $CURRENT_DIR/../MCM/build/buildMCMPackage.sh $INSTALL_DIR_MCM $BUILD_TYPE
checkErr

# Build Slaves
bash $CURRENT_DIR/../slaveBoards/build/buildAll.sh $INSTALL_DIR_SLAVE $BUILD_TYPE
checkErr

echo "Building SDP utility..."
mvn -f $CURRENT_DIR/../../BuildTools/SDPUtility/pom.xml -q package -DDIST_DIR=$INSTALL_DIR_UTILITY -DskipTests=true
checkErr

# Get svn revision.
svnRevision="$(svnversion $CURRENT_DIR/../../../)"
checkErr

echo "Building SDP..."

# Creating G3 Software Distribution Package for bootloader"
SLAVE_FILES=""
for file in $INSTALL_DIR_SLAVE/*AppBootProgPlusBL.bin; do
SLAVE_FILES="$SLAVE_FILES -m=${file}"
done
java -jar $INSTALL_DIR_UTILITY/SDPUtility.jar -t="$INSTALL_DIR/SDP_Bootloader.zip" -legacy -r=$svnRevision -m="$INSTALL_DIR_MCM/MCMBoard.zip" $SLAVE_FILES
checkErr

# Creating G3 Software Distribution Package
SLAVE_FILES=""
for file in $INSTALL_DIR_SLAVE/*BoardNXP.bin; do
SLAVE_FILES="$SLAVE_FILES -m=${file}"
done
java -jar $INSTALL_DIR_UTILITY/SDPUtility.jar -t="$INSTALL_DIR/SDP.zip" -legacy -r=$svnRevision -m="$INSTALL_DIR_MCM/MCMBoard.zip" $SLAVE_FILES
checkErr

# Creating Combined G3 Software Distribution Package
SLAVE_FILES=""
for file in $INSTALL_DIR_SLAVE/*.bin; do
SLAVE_FILES="$SLAVE_FILES -m=${file}"
done
java -jar $INSTALL_DIR_UTILITY/SDPUtility.jar -t="$INSTALL_DIR/SDP_Combined.zip" -r=$svnRevision -m="$INSTALL_DIR_MCM/MCMBoard.zip" $SLAVE_FILES
checkErr


# Patching SDP to support additional features 
echo "Patching SDP..."
java -jar $INSTALL_DIR_UTILITY/SDPUtility.jar -legacy -t="$INSTALL_DIR/SDP_Bootloader.zip"  -f=PSMAppBootProgPlusBL.bin=0.1
checkErr

java -jar $INSTALL_DIR_UTILITY/SDPUtility.jar -legacy -t="$INSTALL_DIR/SDP.zip"  -f=PSMBoardNXP.bin=0.1
checkErr

java -jar $INSTALL_DIR_UTILITY/SDPUtility.jar -t "$INSTALL_DIR/SDP_Combined.zip"  -f=PSMBoardNXP.bin=0.1 -f=PSMAppBootProgPlusBL.bin=0.1
checkErr

echo "Done"
