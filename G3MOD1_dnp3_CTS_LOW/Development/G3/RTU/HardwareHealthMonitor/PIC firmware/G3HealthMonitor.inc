;/*! \file
; ******************************************************************************
; *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
; *                       - Automation Dept.
; ******************************************************************************
; *    PROJECT:
; *       G3 Health Monitor
; *
; *    FILE NAME:
; *               
; *               G3HealthMonitor.inc
; *
; *    DESCRIPTION:
; *       \brief
; *
; *       Watchdog/Health Monitor include file
; *
; *    CURRENT REVISION
; *
; *               $Rev:: $: (Revision of last commit)
; *               $Author:: $: (Author of last commit)
; *       \date   $Date:: $: (Date of last commit)
; *
; *
; *    CREATION
; *
; *   Date          Name        Details
; *   --------------------------------------------------------------------------
; *   18/07/13      walde_s     Initial version.
; *
; ******************************************************************************
; *   COPYRIGHT
; *       This Document is the property of Lucy Switchgear Ltd.
; *       It must not be reproduced, in whole or in part, or otherwise
; *       disclosed without prior consent in writing from
; *       Lucy Switchgear Ltd.
; ******************************************************************************
; */

; Define each of the GPIO pins
SYS_HEALTHY             EQU H'0000' ; O/P Bit 0
PGC                     EQU H'0001' ; I/P Bit 1
HWDOG_1HZ_KICK          EQU H'0002' ; I/P Bit 2
; GP3 is used for MCLR
WD_HANDSHAKE            EQU H'0004' ; O/P Bit 4
RESET_CONTROL           EQU H'0005' ; O/P Bit 5

DIR_REG                 EQU H'000E' ; Bits 0,4 & 5 are O/P
                                    ; Bits 1,2 & 3 are I/P

TIMER2_SETUP            EQU H'000A' ; Postscaler of 2,prescale of 16
TIMER2_PERIOD           EQU H'00FF' ; Timer 2 period setting

SYS_HEALTHY_TIME        EQU H'000A' ; Timeout time for system healthy
SYS_RESET_TIME          EQU H'000A' ; Timeout time for system reset
SYS_RESET_PULSE         EQU H'000A' ; Timeout for reset pulse
SYS_RESET_DELAY         EQU H'0002' ; Timeout for reset delay
SYS_RESET_OFF           EQU H'0058' ; Timeout time for system reset off
INTS_PER_SEC            EQU H'00F6' ; Number of timer 2 interrupts per sec.
SHORT_PULSE_CNT         EQU H'000A' ; Number of short pulses to reset flag
KICK_ACK_TIME           EQU H'003E' ; Time between handshakes to indicate
                                    ;  the reset has been acknowledged

WTCHDG_SET              EQU H'0000' ; Bit 0 of watchdog_set
; Possible states for the state machine
INITIAL                 EQU H'0000'
NORMAL                  EQU H'0001'
NOT_HEALTHY             EQU H'0002'
RESTART_ON              EQU H'0003'
RESTART_OFF             EQU H'0004'
RESTART_DELAY           EQU H'0005'

; End of file



