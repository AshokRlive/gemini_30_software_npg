/*! \file
 ******************************************************************************
 *       \author         Lucy Switchgear Ltd: http://www.lucyswitchgear.co.uk
 *                       - Automation Dept.
 ******************************************************************************
 *    PROJECT:
 *       G3 [Module Name]
 *
 *    FILE NAME:
 *               $Id: GlobalDefinitions.h 29 Aug 2013 pueyos_a $
 *               $HeadURL: http://autosql:81/svn/gemini_30_software/trunk/Development//home/pueyos_a/workspace/G3Trunk/G3/RTU/common/include/GlobalDefinitions.h $
 *
 *    FILE TYPE: c++ header
 *
 *    DESCRIPTION:
 *       \brief
 *
 *       Detailed description of this template module
 *
 *    CURRENT REVISION
 *
 *               $Revision$: (Revision of last commit)
 *               $Author: pueyos_a $: (Author of last commit)
 *       \date   $Date: 29 Aug 2013 $: (Date of last commit)
 *
 *
 *    CREATION
 *
 *   Date          Name        Details
 *   --------------------------------------------------------------------------
 *   29/06/13      pueyos_a    Initial version.
 *
 ******************************************************************************
 *   COPYRIGHT
 *       This Document is the property of Lucy Switchgear Ltd.
 *       It must not be reproduced, in whole or in part, or otherwise
 *       disclosed without prior consent in writing from
 *       Lucy Switchgear Ltd.
 ******************************************************************************
 */


#ifndef GLOBALDEFINITIONS_H__INCLUDED
#define GLOBALDEFINITIONS_H__INCLUDED

/*
 ******************************************************************************
 * INCLUDES - Standard Library Modules Used
 ******************************************************************************
 */
#include <syslog.h>

/*
 ******************************************************************************
 * INCLUDES - Project Specific Modules Used
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Definitions And Macros
 ******************************************************************************
 */

/**
 * Name of the socket used for common Console
 */
#define CONSOLESOCKETNAME "/tmp/MCMConsoleSocket"

/**
 * Global Definition of current board used for compilation
 */
enum MCMBOARD_TYPE
{
    MCMBOARD_TYPE_DEV,  //Development board
    MCMBOARD_TYPE_XE,   //MCM hardware revision XE and backwards
    MCMBOARD_TYPE_XF,   //MCM hardware revision XF onwards
    MCMBOARD_TYPE_XH,   //MCM hardware revision XH onwards - Added SPI FRAM
    MCMBOARD_TYPE_LAST
};
#define MCMBOARD_CURRENT MCMBOARD_TYPE_XH

/*
 * Application mapping to syslog facilities
 */
enum MCM_LOG_FACILITY
{
    MCM_LOG_FACILITY_MCMMONITOR     = LOG_LOCAL0,
    MCM_LOG_FACILITY_MCMAPPLICATION = LOG_LOCAL1,
    MCM_LOG_FACILITY_MCMUPDATER     = LOG_LOCAL2,
    MCM_LOG_FACILITY_LAST
};

/*
 ******************************************************************************
 * EXPORTED - Typedefs and Structures
 ******************************************************************************
 */


/*
 ******************************************************************************
 * EXPORTED - Variables
 ******************************************************************************
 */


#endif /* GLOBALDEFINITIONS_H__INCLUDED */

/*
 *********************** End of file ******************************************
 */
