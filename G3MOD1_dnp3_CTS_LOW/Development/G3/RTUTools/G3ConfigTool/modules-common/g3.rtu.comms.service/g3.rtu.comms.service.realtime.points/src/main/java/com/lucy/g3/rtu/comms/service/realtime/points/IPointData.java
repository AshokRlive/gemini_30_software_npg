/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.util.Date;

import com.lucy.g3.common.bean.ObservableBean2;

/**
 * A <code>PointData</code> stores the real-time information of a virtual point
 * received from RTU.
 */
public interface IPointData extends ObservableBean2 {

  String PROPERTY_TYPE = "type";
  String PROPERTY_GROUP_ID = "groupIDString";
  String PROPERTY_DESCRIPTIOIN = "description";

  // ================== Boundary Properties ===================
  String PROPERTY_VALUE = "value";
  String PROPERTY_FORMATTED_VALUE = "formattedValue";
  String PROPERTY_VALUE_CHANGED = "ValueChanged";


  /** Supported point status.*/
  enum PointStatus {
    VALID("Valid"), ONLINE("Online"), OVERFLOW("Overflow"), FILTER("Filter"), CHATTER("Chatter");
    
    PointStatus(String description) {
      this.description = description;
    }
    
    public String getDescription() {
      return description;
    }
    
    private final String description;
  }


  // ================== Properties Accessors ===================

  Object getType();

  int getId();

  int getGroup();

  String getGroupIDString();

  String getDescription();

  String getUnit();

  String getScale();

  /**
   * Clear all real-time information.
   */
  void clear();

  // ================== Real-time Information Accessors===================
  /**
   * Checks if the point value has recently changed. This flag can be reset by
   * calling {@linkplain #resetValueChanged()}. Bean getter method.
   *
   * @return <code>true</code> if the value has changed, <code>false</code>
   *         otherwise.
   * @see #resetValueChanged()
   */
  boolean isValueChanged();

  /**
   * Bean getter method. Gets point state "online".
   */
  Boolean isOnline();

  /**
   * Bean getter method.Gets point state "valid".
   */
  Boolean isValid();

  Boolean isOverflowed();

  Boolean isFiltered();

  Boolean isChatter();

  /**
   * Bean getter method.Gets the point value.
   *
   * @return point value number.
   */
  Number getValue();
  
  String getFormattedValue();

  /**
   * Gets the value of current value.
   *
   * @return a label string. null if no label configured.
   */
  String getLabel();

  /**
   * Gets the real-time status in text style.
   *
   * @return the point status in formatted text. E.g. "Online | Valid ".
   */
  String getStatusText();

  /**
   * Gets the time when the point status is changed.
   */
  Date getTimestamp();

  /**
   * Retrieve a kind of status of this point data.
   *
   * @param status
   *          the enum of status to be retrieved.
   * @return <li><code>Boolean.True</code> if the status flat is set to true;
   *         <li>
   *         <code>Boolean.False</code> if the status flat is set to false;
   *         <li><code>null</code> if the status is never set.
   */
  Boolean getStatus(PointStatus status);

  /**
   * Checks if the specific status has recently changed. This flag can be reset
   * by calling {@linkplain #resetValueChanged()}.
   *
   * @param status
   *          the enum of the status to be checked.
   * @return <code>true</code> if the status has changed, <code>false</code>
   *         otherwise.
   */
  Boolean isStatusChanged(PointStatus status);

  /**
   * Gets the content of this point data for printing.
   *
   * @return the content String that consists of status and value
   */
  String print();

  String getGroupName();

  String getMappingModuleName();

  // ================== Real-time Status Mutators ===================
  /**
   * Clears the value changed state and set it to false.
   */
  void resetValueChanged();

  /**
   * Replaces the content of this data object with a new value and status code.
   * The new status code will be decoded into state flags, such as "valid" and
   * "online", according to <em>G3Protocol</em>.
   *
   * @param newValue
   *          the point value received from RTU
   * @param newStatus
   *          the real-time status code received from RTU.
   */
  void injectData(Number value, byte statusCode);

  void injectTimeStamp(long sec, long nsec);

  boolean isFrozen();
}
