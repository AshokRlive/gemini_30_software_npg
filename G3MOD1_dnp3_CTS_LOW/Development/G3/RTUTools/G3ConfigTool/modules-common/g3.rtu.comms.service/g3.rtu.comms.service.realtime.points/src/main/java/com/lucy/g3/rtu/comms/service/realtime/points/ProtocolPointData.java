/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.beans.PropertyChangeListener;
import java.util.Date;


/**
 * <code>ProtocolPointData</code> holds the real-time data of a protocol point.
 * <p>
 * Since G3 RTU doen't supply API for retrieving the value of protocol point, we
 * implement <code>ProtocolPointData</code> as a wrapper of a virtual point
 * data.
 * </p>
 */
public class ProtocolPointData implements IPointData {

  private final long id;
  private final String type;
  private final String description;
  private final String protocolName;

  private final IPointData vpointData;// Wrapped vpoint data


  /**
   * Construct a protocol point data by wrapping a given point data.
   *
   * @param type
   *          protocol point type description
   * @param id
   *          protocol point id
   */
  public ProtocolPointData(String protocolName, String type, long id, String description, IPointData vpointData) {
    this.type = type;
    this.id = id;
    this.description = description;
    this.protocolName = protocolName;
    this.vpointData = vpointData;
  }

  public long getProtocolPointID() {
    return id;
  }

  public String getProtocolPointType() {
    return type;
  }

  public String getProtocolName() {
    return protocolName;
  }

  @Override
  public String getDescription() {
    return description;
  }

  // ================== Delegate Methods ===================

  public String getMappedPointDescription() {
    return vpointData.getDescription();
  }

  @Override
  public Object getType() {
    return vpointData.getType();
  }

  @Override
  public int getId() {
    return vpointData.getId();
  }

  @Override
  public int getGroup() {
    return vpointData.getGroup();
  }

  @Override
  public String getGroupIDString() {
    return vpointData.getGroupIDString();
  }

  @Override
  public String getUnit() {
    return vpointData.getUnit();
  }

  @Override
  public String getScale() {
    return vpointData.getScale();
  }

  @Override
  public void clear() {
    vpointData.clear();
  }

  @Override
  public boolean isValueChanged() {
    return vpointData.isValueChanged();
  }

  @Override
  public Boolean isOnline() {
    return vpointData.isOnline();
  }

  @Override
  public Boolean isValid() {
    return vpointData.isValid();
  }

  @Override
  public Number getValue() {
    return vpointData.getValue();
  }

  @Override
  public String getFormattedValue() {
    return vpointData.getFormattedValue();
  }

  @Override
  public String getStatusText() {
    return vpointData.getStatusText();
  }

  @Override
  public Date getTimestamp() {
    return vpointData.getTimestamp();
  }

  @Override
  public Boolean getStatus(PointStatus status) {
    return vpointData.getStatus(status);
  }

  @Override
  public Boolean isStatusChanged(PointStatus status) {
    return vpointData.isStatusChanged(status);
  }

  @Override
  public String print() {
    return vpointData.print();
  }

  @Override
  public String getGroupName() {
    return vpointData.getGroupName();
  }

  @Override
  public String getMappingModuleName() {
    return vpointData.getMappingModuleName();
  }

  @Override
  public void resetValueChanged() {
    vpointData.resetValueChanged();
  }

  @Override
  public void injectTimeStamp(long sec, long nsec) {
    vpointData.injectTimeStamp(sec, nsec);
  }

  @Override
  public void injectData(Number value, byte statusCode) {
    vpointData.injectData(value, statusCode);
  }

  @Override
  public String toString() {
    return "[ProtocolPointID=" + id
        + "," + vpointData + "]";
  }

  @Override
  public Boolean isOverflowed() {
    return vpointData.isOverflowed();
  }

  @Override
  public Boolean isFiltered() {
    return vpointData.isFiltered();
  }

  @Override
  public Boolean isChatter() {
    return vpointData.isChatter();
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    vpointData.addPropertyChangeListener(listener);
  }

  @Override
  public void addPropertyChangeListener(String propertyName,
      PropertyChangeListener listener) {
    vpointData.addPropertyChangeListener(propertyName, listener);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener listener) {
    vpointData.removePropertyChangeListener(listener);
  }

  @Override
  public void removePropertyChangeListener(String propertyName,
      PropertyChangeListener listener) {
    vpointData.removePropertyChangeListener(propertyName, listener);
  }

  @Override
  public PropertyChangeListener[] getPropertyChangeListeners() {
    return vpointData.getPropertyChangeListeners();
  }

  @Override
  public PropertyChangeListener[] getPropertyChangeListeners(
      String propertyName) {
    return vpointData.getPropertyChangeListeners(propertyName);
  }

  @Override
  public String getLabel() {
    return vpointData.getLabel();
  }

  @Override
  public boolean isFrozen() {
    return vpointData.isFrozen();
  }

}
