/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

class PL_Reply_DPointsPolling extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_DPointPolling;
  
  private final List<IPointData> updateList;


  PL_Reply_DPointsPolling(List<IPointData> updateList) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.updateList = new ArrayList<IPointData>(updateList);
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    long sec = 0;// time stamp seconds
    long nsec = 0;// //time stamp nanoseconds

    ByteBuffer buf = createByteBuffer(payload);

    int pointNum = payload.length / PAYLOAD_SIZE;
    for (int i = 0; i < pointNum; i++) {
      /* Read time stamp */
      sec = NumUtils.convert2Unsigned(buf.getInt());
      nsec = NumUtils.convert2Unsigned(buf.getInt());

      short group = (buf.getShort());
      short id = (buf.getShort());
      byte status = buf.get();
      Number value = NumUtils.getBitsValue(status, 4, 3);

      // Update the point data
      ArrayList<IPointData> foundPoints = FindUtil.findPoint(updateList, group, id);
      for (IPointData vpData:foundPoints) {
          vpData.injectData(value, status);
          vpData.injectTimeStamp(sec, nsec);
          //updateList.remove(vpData);// Remove the updated point
      }
    }

    /* Clear the point data that have not been updated */
    // if(selectionList.size() > 0){
    // log.warn("Points data not received(Amount:"+selectionList.size()+")");
    // for(PointData pdata : selectionList){
    // pdata.clear();
    // }
    // }
  }
}

