/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.nio.ByteBuffer;
import java.util.Collection;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

class PL_Send_ChannelPolling extends RequestPayload  {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_ChannelPolling;
  private final Collection<ChannelData> chList;


  PL_Send_ChannelPolling(Collection<ChannelData> chList) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.chList = chList;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE * chList.size());
    for (ChannelData chData : chList) {
      buf.put(new byte[] { chData.moduleType, chData.moduleID, chData.channelType, chData.channelID });
    }
    return buf.array();
  }
}
