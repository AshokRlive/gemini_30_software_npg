/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * Utility for searching.
 */
class FindUtil {
  public static ArrayList<IPointData> findPoint(List<IPointData> pointList, int group, int id) {
    //TODO Improve this using searchin algorithm
    ArrayList<IPointData> found = new ArrayList<>();
    for (IPointData vp : pointList) {
      if (vp.getGroup() == group && vp.getId() == id) {
        found.add(vp);
      }
    }
    
    if (found.isEmpty()) {
      Logger logger = Logger.getLogger(PL_Reply_APointsPolling.class);
      logger.error("Point is not found in update list:[" + group + "," + id + "].");
    }
    return found;
  }
}

