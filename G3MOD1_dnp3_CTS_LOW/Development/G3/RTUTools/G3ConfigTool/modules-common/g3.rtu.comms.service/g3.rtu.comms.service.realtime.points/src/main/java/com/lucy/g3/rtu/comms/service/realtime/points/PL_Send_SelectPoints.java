/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

class PL_Send_SelectPoints extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_SelectPoint;
  private final IPointData[] pointList;
  final G3Protocol.PollingMode mode;


  PL_Send_SelectPoints(IPointData[] pointList, G3Protocol.PollingMode mode) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.mode =mode;
    this.pointList = pointList;
  }

  @Override
  public byte[] toBytes() {
    int pointNum = pointList == null ? 0 : pointList.length;

    ByteBuffer buf = createByteBuffer(pointNum * PAYLOAD_SIZE);

    for (short i = 0; i < pointNum; i++) {
      if (pointList[i] != null) {
        buf.putShort((short) pointList[i].getGroup());// group
        buf.putShort((short) pointList[i].getId());// id
      } else {
        buf.putShort((short) 0);// group
        buf.putShort((short) 0);// id
      }
    }

    return buf.array();
  }
}
