/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetChannel;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetPointAnalogue;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetPointCounter;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetPointDigital;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_PollPointAnalogue;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_PollPointCounter;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_PollPointDigital;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SetPointAList;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SetPointCList;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SetPointDList;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetChannel;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetPointAnalogue;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetPointCounter;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetPointDigital;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_PollPointAnalogue;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_PollPointCounter;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_PollPointDigital;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Diagnostic;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_PointData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.IG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.PollingMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * A subsystem for retrieving and storing all RTU points' value and status for
 * real-time monitoring.
 */
public class RTUPoints extends AbstractCommsModule {

  private Logger log = Logger.getLogger(RTUPoints.class);

  /**
   * Read-only property that indicates whether or not the RTU points are loaded
   * for real-time monitoring.
   */
  public static final String PROPERTY_POINTS_LOADED = "pointsLoaded";

  /* All real-time virtual points */
  private final ArrayList<VirtualPointData> vpointsData;

  /* All real-time protocol points */
  private final ArrayList<ProtocolPointData> ppointsData;

  /* Selected analogue virtual points for polling */
  private ArrayList<IPointData> selectedAPoints;

  /* Selected digital virtual points for polling */
  private ArrayList<IPointData> selectedDPoints;

  /* Selected digital virtual points for polling */
  private ArrayList<IPointData> selectedCPoints;

  /* Observers list */
  private final ArrayList<RTUPointsListChangeObserver> observers;

  private boolean pointsLoaded = false;


  public RTUPoints(IClient client) {
    super(client);

    ppointsData = new ArrayList<ProtocolPointData>();
    vpointsData = new ArrayList<VirtualPointData>();
    selectedAPoints = new ArrayList<IPointData>();
    selectedDPoints = new ArrayList<IPointData>();
    selectedCPoints = new ArrayList<IPointData>();
    observers = new ArrayList<RTUPointsListChangeObserver>();
  }

  public boolean isPointsLoaded() {
    return pointsLoaded;
  }

  private void setPointsLoaded(boolean newValue) {
    Object oldValue = isPointsLoaded();
    this.pointsLoaded = newValue;
    firePropertyChange(PROPERTY_POINTS_LOADED, oldValue, newValue);
  }

  /**
   * Update the real-time virtual point data list.
   */
  public void updatePointDataList(Collection<VirtualPointData> newPointsData) {
    vpointsData.clear();
    if (newPointsData != null) {
      vpointsData.addAll(newPointsData);
    }

    // Update state
    setPointsLoaded(newPointsData != null);

    // Update observers
    ArrayList<VirtualPointData> newList = new ArrayList<VirtualPointData>(vpointsData);
    for (RTUPointsListChangeObserver ob : observers) {
      ob.realtimeVirtualPointListUpdated(newList);
    }
  }

  /**
   * Update the real-time protocol point data list.
   */
  public void updateProtocolPointDataList(Collection<ProtocolPointData> newPointsData) {
    ppointsData.clear();
    if (newPointsData != null) {
      ppointsData.addAll(newPointsData);
    }

    // Update observers
    ArrayList<ProtocolPointData> newList = new ArrayList<ProtocolPointData>(ppointsData);
    for (RTUPointsListChangeObserver ob : observers) {
      ob.realtimeProtocolPointListUpdated(newList);
    }
  }

  public Collection<VirtualPointData> getAllVritualPointData() {
    return new ArrayList<VirtualPointData>(vpointsData);
  }

  public Collection<ProtocolPointData> getAllProtocolPointData() {
    return new ArrayList<ProtocolPointData>(ppointsData);
  }

  public void addObserver(RTUPointsListChangeObserver ob) {
    if (ob != null) {
      observers.add(ob);
    }
  }

  public void removeObserver(RTUPointsListChangeObserver ob) {
    observers.remove(ob);
  }

  /**
   * Sends command to RTU to set the selected points for polling.
   */
  public void cmdSelectPoints(IPointData[] pointList, PollingMode mode)
      throws IOException, SerializationException {

    PL_Send_SelectPoints send = new PL_Send_SelectPoints(pointList, mode);
    
    send( CTMsgType_PointData                    ,
          convertModeToSelectionMsgID(send.mode),
          _CTMsgID_R_AckNack                     ,
        send,
        null);

    // Save currently selected points
    if (mode == PollingMode.DIGITAL) {
      selectedDPoints.clear();
      if (pointList != null) {
        selectedDPoints.addAll(Arrays.asList(pointList));
      }
      
    } else if (mode == PollingMode.ANALOG) {
      selectedAPoints.clear();
      if (pointList != null) {
        selectedAPoints.addAll(Arrays.asList(pointList));
      }
      
    } else if (mode == PollingMode.COUNTER) {
      selectedCPoints.clear();
      if (pointList != null) {
        selectedCPoints.addAll(Arrays.asList(pointList));
      }
      
    } else {
      log.error("Unsupported mode: " + mode);
    }
  }

  /**
   * Send command to RTU to clear the previously selected polling points.
   */
  public void cmdClearSelectedPoints(PollingMode mode)
      throws IOException, SerializationException {

    cmdSelectPoints(null, mode);
  }

  /**
   * Reads the status of the selected polling points from RTU.
   */
  public void cmdPollPointStatus(PollingMode mode, boolean includeTimeStamp)
      throws SerializationException, IOException {
    short msgID_send ; 
    short msgID_reply; 
    IG3MsgPayload reply;
    
    if (mode == PollingMode.ANALOG) {
      reply = new PL_Reply_APointsPolling(selectedAPoints);
      msgID_send = _CTMsgID_C_PollPointAnalogue;      
      msgID_reply = _CTMsgID_R_PollPointAnalogue;
      
    } else if (mode == PollingMode.DIGITAL) {
      reply = new PL_Reply_DPointsPolling(selectedDPoints);
      msgID_send  = _CTMsgID_C_PollPointDigital;
      msgID_reply = _CTMsgID_R_PollPointDigital;
      
    } else if (mode == PollingMode.COUNTER) {
      reply = new PL_Reply_CPointsPolling(selectedCPoints);
      msgID_send  = _CTMsgID_C_PollPointCounter;     
      msgID_reply = _CTMsgID_R_PollPointCounter;    
      
    } else {
      log.error("Unsupported polling mode:" + mode);
      return;
    }
    
    send(CTMsgType_PointData ,
         msgID_send  ,
         msgID_reply ,
        null,
        reply);

  }

  /**
   * Reads the status of the given points from RTU.
   */
  public void cmdGetPointStatus(List<IPointData> pointList, PollingMode mode)
      throws SerializationException, IOException {

    short msgType ; 
    short msgID_send ; 
    short msgID_reply; 
    IG3MsgPayload reply;
    
    IPointData[] pointArray = pointList.toArray(new IPointData[pointList.size()]);
    PL_Send_SelectPoints send = new PL_Send_SelectPoints(pointArray, mode);

    if (mode == PollingMode.ANALOG) {
      reply = new PL_Reply_APointsPolling(pointList);
      msgType = CTMsgType_Diagnostic;
      msgID_send = _CTMsgID_C_GetPointAnalogue;
      msgID_reply = _CTMsgID_R_GetPointAnalogue;
      
    } else if (mode == PollingMode.DIGITAL) {
      reply = new PL_Reply_DPointsPolling(pointList);
      msgType = CTMsgType_Diagnostic;
      msgID_send = _CTMsgID_C_GetPointDigital;
      msgID_reply = _CTMsgID_R_GetPointDigital;
      
      
    } else if (mode == PollingMode.COUNTER) {
      reply = new PL_Reply_CPointsPolling(pointList);
      msgType = CTMsgType_Diagnostic;
      msgID_send = _CTMsgID_C_GetPointCounter;
      msgID_reply = _CTMsgID_R_GetPointCounter;
      
    } else {
      log.error("Unsupported polling mode:" + mode);
      return;
    }
    
    send(msgType,
        msgID_send,
        msgID_reply,
        send,
        reply);
  }


  private static short convertModeToSelectionMsgID(PollingMode m) {
    switch (m) {
    case ANALOG:
      return _CTMsgID_C_SetPointAList;
    case COUNTER:
      return _CTMsgID_C_SetPointCList;
    case DIGITAL:
      return _CTMsgID_C_SetPointDList;
    default:
      throw new UnsupportedOperationException("Unsupported mode: " + m);
    }
  }

  /**
   * Polling the value of a list channels.
   */
  public void cmdPollingChannelValue(Collection<ChannelData> chList)
      throws SerializationException, IOException {

    ArrayList<ChannelData> localChList = new ArrayList<ChannelData>(chList);
    localChList.removeAll(Collections.singleton(null));

    PL_Send_ChannelPolling send = new PL_Send_ChannelPolling(localChList);
    PL_Reply_ChannelPolling reply = new PL_Reply_ChannelPolling(localChList);
    send(CTMsgType_Diagnostic,
        _CTMsgID_C_GetChannel,
        _CTMsgID_R_GetChannel,        
        send,
        reply);
  }


  /**
   * An asynchronous update interface for receiving notifications about
   * RTUPointsListChange information as the RTUPointsListChange is constructed.
   */
  public interface RTUPointsListChangeObserver {

    void realtimeVirtualPointListUpdated(ArrayList<VirtualPointData> newVirtualPoints);

    void realtimeProtocolPointListUpdated(ArrayList<ProtocolPointData> newProtocolPoints);
  }
}
