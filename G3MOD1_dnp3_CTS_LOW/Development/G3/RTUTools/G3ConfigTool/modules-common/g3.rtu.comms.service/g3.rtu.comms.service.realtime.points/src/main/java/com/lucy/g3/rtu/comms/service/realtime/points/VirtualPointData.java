/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import com.lucy.g3.common.bean.Bean;
import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;

/**
 * Implementation of the interface {@linkplain IPointData}.
 */
public abstract class VirtualPointData extends Bean implements IPointData {

  // ================== Properties ==============================
  private final Object type;
  private final int group;
  private final int id;
  private final String groupName;
  private final String mappingModuleName;
  private final String groupIDString;
  private final String description;
  private final String unit;
  private final String scale;

  // ================== Real-time Information ===================
  /** The point value which is read from RTU. */
  private Number value;
  private String formattedValue;

  private String label;
  
  private Date timestamp;

  /** All boolean status. e.g. valid,online,etc */
  private final Boolean[] status = new Boolean[PointStatus.values().length];

  /** Indicate if value is changed recently. */
  private boolean isValueChanged = false;

  /** Indicate if a status is changed recently. */
  private final boolean[] isStatusChanged = new boolean[PointStatus.values().length];

  private HashMap<Double, String> labelSet;
  
  private final IPointValueFormatter valueFormatter;

  public VirtualPointData(IPointValueFormatter valueFormatter, Object type, String groupName,
      String moduleName, int group, int id,
      String description, String unit, String scale) {
    this.valueFormatter = valueFormatter;
    this.type = type;
    this.group = group;
    this.id = id;
    this.description = description;
    this.groupIDString = String.format("[%d,%d]", group, id);
    this.unit = unit;
    this.scale = scale;
    this.groupName = groupName;
    this.mappingModuleName = moduleName;
  }

  // ================== Properties Accessors ===================

  @Override
  public Object getType() {
    return type;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public int getGroup() {
    return group;
  }

  @Override
  public String getGroupIDString() {
    return groupIDString;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String getUnit() {
    return unit;
  }

  @Override
  public String getScale() {
    return scale;
  }

  /**
   * Clear all real-time information.
   */
  @Override
  public void clear() {
    value = null;
    label = null;
    for (int i = 0; i < status.length; i++) {
      status[i] = null;
    }

    resetValueChanged();

  }

  // ================== Real-time Information Accessors===================

  @Override
  public Number getValue() {
    return value;
  }

  
  @Override
  public String getFormattedValue() {
    return formattedValue;
  }

  
  private void setFormattedValue(String formattedValue) {
    Object oldValue = this.formattedValue;
    this.formattedValue = formattedValue;
    firePropertyChange(PROPERTY_FORMATTED_VALUE, oldValue, formattedValue);
  }

  @Override
  public String getLabel() {
    return label;
  }

  private void setLabel(String label) {
    this.label = label;
  }
  
  @Override
  public boolean isValueChanged() {
    return isValueChanged;
  }

  @Override
  public Boolean isOnline() {
    return status[PointStatus.ONLINE.ordinal()];
  }

  @Override
  public Boolean isValid() {
    return status[PointStatus.VALID.ordinal()];
  }
  
  @Override
  public Boolean isOverflowed() {
    return status[PointStatus.OVERFLOW.ordinal()];
  }

  @Override
  public Boolean isFiltered() {
    return status[PointStatus.FILTER.ordinal()];
  }

  @Override
  public Boolean isChatter() {
    return status[PointStatus.CHATTER.ordinal()];
  }

  @Override
  public String getStatusText() {
    StringBuffer statusDisplay = new StringBuffer();
    PointStatus[] allStatus = PointStatus.values();

    for (int i = 0; i < allStatus.length; i++) {
      if (status[i] == Boolean.TRUE) {
        statusDisplay.append(allStatus[i].name());
        statusDisplay.append("|");
      }
    }

    return statusDisplay.toString();
  }

  @Override
  public Boolean getStatus(PointStatus status) {
    if (status == null) {
      return null;
    }

    return this.status[status.ordinal()];
  }
  
  protected void setStatus(PointStatus statusEnum, Boolean statusValue) {
    if (statusEnum == null) {
      return;
    }
    
    this.status[statusEnum.ordinal()] = statusValue;
  }

  @Override
  public Date getTimestamp() {
    return isValid() == Boolean.TRUE ? timestamp : null;
  }

  @Override
  public Boolean isStatusChanged(PointStatus status) {
    if (status == null) {
      return false;
    }

    return isStatusChanged[status.ordinal()];
  }
  
  protected abstract PointStatus[] getStatusEnums(); 

  @Override
  public String print() {
    StringBuffer sb = new StringBuffer();
    PointStatus[] statusEnums = getStatusEnums();
    
    sb.append(getType());
    sb.append(getGroupIDString());
    
    if(statusEnums != null) {
      sb.append(",");
      for (int i = 0; i < statusEnums.length; i++) {
        if(statusEnums[i] != null) {
        sb.append(" ");
        sb.append(statusEnums[i].getDescription());
        sb.append(":");
        sb.append(getStatus(statusEnums[i]));
        }
      }
    }

    sb.append(" Value:" + getValue());

    return sb.toString();
  }

  @Override
  public void injectTimeStamp(long sec, long nsec) {
    if (sec == 0 && nsec == 0) {
      timestamp = null;
    } else {
      timestamp = RTUTimeDecoder.decode(sec * 1000 + nsec / 1000000);
    }
  }

  protected abstract void injectStatus(byte statusCode);
  
  @Override
  public void injectData(Number newValue, byte statusCode) {
    Boolean[] oldStatus = Arrays.copyOf(status, status.length);

    injectStatus(statusCode);
    
    // Set statusChanged flags
    for (int i = 0; i < oldStatus.length; i++) {
      if (oldStatus[i] != null && oldStatus[i] != status[i]) {
        isStatusChanged[i] = true;
      }
    }

    setValue(newValue);
  }

  private void setValue(Number value) {
    Object oldValue = getValue();
    this.value = value;

    // Update label
    String newLabel = null;
    if (labelSet != null && value != null && isValid() == Boolean.TRUE) {
      newLabel = labelSet.get(value.doubleValue());
    } 
    setLabel(newLabel);
    
    // Update value string
    setFormattedValue(generateFormattedValue(value));
    
    firePropertyChange(PROPERTY_VALUE, oldValue, value);
    
    

    // Update valueChanged flag
    if (isValid() == Boolean.TRUE
        && oldValue != null
        && value != null
        && !value.equals(oldValue)) {
      setValueChanged(true);
    }
  }

  private String generateFormattedValue(Number value) {
    if (isValid() != Boolean.TRUE)
      return "";
    
    if (isOnline() != Boolean.TRUE)
      return "?";

    String label = getLabel();
    if (label != null && !label.trim().isEmpty())
      return label;

    return valueFormatter.format(this,value, false);
  }

  private void setValueChanged(boolean newValue) {
    Object oldValue = isValueChanged();
    this.isValueChanged = newValue;

    firePropertyChange(PROPERTY_VALUE_CHANGED, oldValue, newValue);
  }

  @Override
  public String getGroupName() {
    return groupName;
  }

  @Override
  public String getMappingModuleName() {
    return mappingModuleName;
  }

  @Override
  public String toString() {
    return "[group=" + group + ", id=" + id + "]";
  }

  // ================== Real-time Status Mutators ===================
  @Override
  public void resetValueChanged() {
    for (int i = 0; i < isStatusChanged.length; i++) {
      isStatusChanged[i] = false;
    }
    setValueChanged(false);
  }

  public HashMap<Double, String> getLabelSet() {
    return labelSet;
  }

  public void setLabelSet(HashMap<Double, String> labelSet) {
    this.labelSet = labelSet;
  }

//  @Override
//  public int hashCode() {
//    final int prime = 31;
//    int result = 1;
//    result = prime * result + group;
//    result = prime * result + id;
//    return result;
//  }
//
//  @Override
//  public boolean equals(Object obj) {
//    if (this == obj) {
//      return true;
//    }
//    if (obj == null) {
//      return false;
//    }
//    if (getClass() != obj.getClass()) {
//      return false;
//    }
//    VirtualPointData other = (VirtualPointData) obj;
//    if (group != other.group) {
//      return false;
//    }
//    if (id != other.id) {
//      return false;
//    }
//    return true;
//  }
  
  static public final class AIPointData extends VirtualPointData {
    public AIPointData(IPointValueFormatter valueFormatter, Object type,
        String groupName, String moduleName,
        int group, int id, String description, String unit, String scale) {
      super(valueFormatter, type, 
          groupName, moduleName, group, id, description, unit, scale);
    }

    @Override
    protected PointStatus[] getStatusEnums() {
      return new PointStatus[]{PointStatus.VALID, PointStatus.ONLINE, PointStatus.FILTER,PointStatus.OVERFLOW};
    }

    @Override
    protected void injectStatus(byte statusCode) {
      setStatus(PointStatus.VALID, (NumUtils.getBitsValue(statusCode, 0, 0) == 1));
      if (isValid()) {
        setStatus(PointStatus.ONLINE,NumUtils.getBitsValue(statusCode, 1, 1) == 1);
        setStatus(PointStatus.FILTER,NumUtils.getBitsValue(statusCode, 2, 2) == 1);
        setStatus(PointStatus.OVERFLOW,NumUtils.getBitsValue(statusCode, 3, 3) == 1);
      } else {
        setStatus(PointStatus.ONLINE,null);
        setStatus(PointStatus.FILTER,null);
        setStatus(PointStatus.OVERFLOW,null);
      }      
    }

    @Override
    public boolean isFrozen() {
      return false;
    }
  }
  
  static public final class DIPointData extends VirtualPointData {

    public DIPointData(IPointValueFormatter valueFormatter, Object type, String groupName, String moduleName,
        int group, int id, String description, String unit, String scale) {
      super(valueFormatter, type, groupName, moduleName, group, id, description, unit, scale);
    }

    @Override
    protected PointStatus[] getStatusEnums() {
      return new PointStatus[]{PointStatus.VALID, PointStatus.ONLINE, PointStatus.CHATTER};
    }

    @Override
    protected void injectStatus(byte statusCode) {
      setStatus(PointStatus.VALID, NumUtils.getBitsValue(statusCode, 0, 0) == 1);
      if (isValid()) {
        setStatus(PointStatus.ONLINE, NumUtils.getBitsValue(statusCode, 1, 1) == 1);
        setStatus(PointStatus.CHATTER,NumUtils.getBitsValue(statusCode, 2, 2) == 1);
      } else {
        setStatus(PointStatus.ONLINE,null);
        setStatus(PointStatus.CHATTER,null);
      }      
    }

    @Override
    public boolean isFrozen() {
      return false;
    }
    
  }
  
  static public final class CounterPointData extends VirtualPointData {
    private final boolean frozen;
    
    public CounterPointData(IPointValueFormatter valueFormatter, Object type, String groupName,
        String moduleName, int group, int id, String description, String unit, String scale, boolean frozen) {
      super(valueFormatter, type, groupName, moduleName, group, id, description, unit, scale);
      this.frozen = frozen;
    }

    @Override
    protected PointStatus[] getStatusEnums() {
      return new PointStatus[]{PointStatus.VALID, PointStatus.ONLINE, PointStatus.OVERFLOW};
    }

    @Override
    protected void injectStatus(byte statusCode) {
      setStatus(PointStatus.VALID, NumUtils.getBitsValue(statusCode, 0, 0) == 1);
      if (isValid()) {
        setStatus(PointStatus.ONLINE ,  NumUtils.getBitsValue(statusCode, 1, 1) == 1);
        setStatus(PointStatus.OVERFLOW, NumUtils.getBitsValue(statusCode, 2, 2) == 1);
      } else {
        setStatus(PointStatus.ONLINE, null);
        setStatus(PointStatus.OVERFLOW, null);
      }      
    }

    @Override
    public boolean isFrozen() {
      return frozen;
    }
  }

}
