/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import java.nio.ByteBuffer;
import java.util.Collection;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

class PL_Reply_ChannelPolling extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ChannelPolling;
  private final Collection<ChannelData> chList;


  PL_Reply_ChannelPolling(Collection<ChannelData> chList) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.chList = chList;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    byte mType, mId, chType, chID, flags;
    ByteBuffer buf = createByteBuffer(payload);
    for (int i = 0, chNum = payload.length / PAYLOAD_SIZE; i < chNum; i++) {
      mType = buf.get();
      mId = buf.get();
      chType = buf.get();
      chID = buf.get();
      flags = buf.get();

      // Find and update one channel
      boolean foundCh = false;
      for (ChannelData chData : chList) {
        if (chData.channelID == chID
            && chData.channelType == chType
            && chData.moduleType == mType
            && chData.moduleID == mId) {
          if (chType == G3Protocol.ANALOGUE_INPUT_CHANNEL) {
            chData.setValue(buf.getFloat());
          } else if (chType == G3Protocol.DIGITAL_INPUT_CHANNEL) {
            chData.setValue(buf.getInt());
          }

          chData.parseFlags(flags);

          foundCh = true;
          chList.remove(chData);
          break;
        }
      }

      if (!foundCh) {
        log.error(String.format("Channel not found. " +
            "moudleType = %d moudleID = %d channelType = %d channelID = %d",
            mType, mId, chType, chID));
      }
    }

    // Mark non-updated channels invalid
    for (ChannelData chdata : chList) {
      chdata.parseFlags((byte) 0);
    }
  }
}