/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.points;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;

/**
 * The value and status of a channel.
 */
public class ChannelData {
  // GEN-BEGIN: turn off checkstyle
  public final byte moduleType;
  public final byte moduleID;
  /**
   * {@code G3ConfigProtocol.ANALOGUE_INPUT_CHANNEL} or
   * {@code G3ConfigProtocol.DIGITAL_INPUT_CHANNEL}
   */
  public final byte channelType;
  public final byte channelID;
  // GEN-END: turn on checkstyle

  private final String description;
  private final String unit;
  private Number value;
  private boolean valid = false;
  private boolean online = false;
  private boolean alarm  = false;
  private boolean restart= false;
  private boolean outOfRange= false;
  private boolean filtered = false;
  private boolean simulated = false;
  


  public ChannelData(byte moduleType, byte moduleID, byte channelType, byte channelID,
      String description, String unit) {
    if(channelType != G3Protocol.ANALOGUE_INPUT_CHANNEL
        && channelType != G3Protocol.DIGITAL_INPUT_CHANNEL)
        throw new IllegalArgumentException("Invalid channel type:" + channelType);

    this.moduleType = moduleType;
    this.moduleID = moduleID;
    this.channelType = channelType;
    this.channelID = channelID;
    this.description = description;
    this.unit = unit;
  }

  public void setValue(Number value) {
    this.value = value;
  }

  public Number getValue() {
    return valid ? value : null;
  }

  public boolean isValid() {
    return valid;
  }

  public boolean isOnline() {
    return online;
  }

  public boolean isAlarm() {
    return alarm;
  }

  public boolean isRestart() {
    return restart;
  }

  public boolean isOutOfRange() {
    return outOfRange;
  }

  public boolean isFiltered() {
    return filtered;
  }

  public boolean isSimulated() {
    return simulated;
  }

  private void setValid(boolean valid) {
    this.valid = valid;
  }

  private void setOnline(boolean online) {
    this.online = online;
  }

  private void setAlarm(boolean alarm) {
    this.alarm = alarm;
  }

  private void setRestart(boolean restart) {
    this.restart = restart;
  }

  private void setOutOfRange(boolean outOfRange) {
    this.outOfRange = outOfRange;
  }

  private void setFiltered(boolean filtered) {
    this.filtered = filtered;
  }

  private void setSimulated(boolean simulated) {
    this.simulated = simulated;
  }

  public String getUnit() {
    return unit;
  }

  public String getTypeString() {
    return channelType == G3Protocol.ANALOGUE_INPUT_CHANNEL ? "Analogue" : "Digital";
  }

  public String getDescription() {
    return description;
  }

  public String getValueString() {
    if (value == null) {
      return "";
    }

    if (value instanceof Float) {
      return String.format("%f", value);
    } else {
      return value.toString();
    }
  }

  public void clearValue() {
    value = null;
    valid = false;
  }

  @Override
  public String toString() {
    return "ChannelData [moduleType=" + moduleType + ", moduleID=" + moduleID
        + ", channelType=" + channelType + ", channelID=" + channelID + ", value="
        + getValueString() + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + channelID;
    result = prime * result + channelType;
    result = prime * result + moduleID;
    result = prime * result + moduleType;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ChannelData other = (ChannelData) obj;
    if (channelID != other.channelID) {
      return false;
    }
    if (channelType != other.channelType) {
      return false;
    }
    if (moduleID != other.moduleID) {
      return false;
    }
    if (moduleType != other.moduleType) {
      return false;
    }
    return true;
  }

  public void parseFlags(byte flags) {
    setValid(NumUtils.getBitsValue(flags, 0, 0)== 1);
    setOnline(NumUtils.getBitsValue(flags, 1, 1)== 1);
    setAlarm(NumUtils.getBitsValue(flags, 2, 2)== 1);
    setRestart(NumUtils.getBitsValue(flags, 3, 3)== 1);
    setOutOfRange(NumUtils.getBitsValue(flags, 4, 4)== 1);
    setFiltered(NumUtils.getBitsValue(flags, 5, 5)== 1);
    setSimulated(NumUtils.getBitsValue(flags, 6, 6)== 1);
  }

}
