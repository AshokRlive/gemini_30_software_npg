/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_ModuleList.
 */
class PL_Reply_ModuleList extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ModuleInfo;

  private final ArrayList<ModuleInfo> moduleList = new ArrayList<ModuleInfo>();


  PL_Reply_ModuleList() {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
  }

  public ArrayList<ModuleInfo> getModuleList() {
    return moduleList;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    
    moduleList.clear();

    int moduleNum = payload.length / PAYLOAD_SIZE;
    ByteBuffer buf = createByteBuffer(payload);

    byte[] mbuf = new byte[PAYLOAD_SIZE];
    for (int i = 0; i < moduleNum; i++) {
      buf.get(mbuf);
      ModuleInfo info = ModuleInfo.parseBytes(mbuf);
      if (info != null) {
        moduleList.add(info);
      }
    }
  }
}