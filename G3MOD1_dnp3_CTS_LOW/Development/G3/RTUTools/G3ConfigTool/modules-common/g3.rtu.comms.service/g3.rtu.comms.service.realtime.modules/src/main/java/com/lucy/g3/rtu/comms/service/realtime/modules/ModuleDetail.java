/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import static com.lucy.g3.rtu.comms.protocol.ISerializable.Buffer.createByteBuffer;

import java.nio.ByteBuffer;

import com.jgoodies.common.bean.Bean;
import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This bean stores all module detail status.
 */
public class ModuleDetail extends Bean {

  public static final String PROPERTY_RUNNINGTIMESECS = "runningTimeSecs";
  public static final String PROPERTY_SVNREVISION_BL = "svnRevisionBL";
  public static final String PROPERTY_SVNREVISION_APP = "svnRevisionApp";
  public static final String PROPERTY_ARCH = "arch";

  //GEN-BEGIN: turn off checkstyle
  public final MODULE_ID moduleID;
  public final MODULE moduleType;
  //GEN-END: turn on checkstyle

  // ====== Module Details ======
  private long svnRevisionBL;
  private long svnRevisionApp;
  private long arch;
  private long runningTimeSecs;


  public ModuleDetail(MODULE moduleType, MODULE_ID moduleID) {
    this.moduleID = moduleID;
    this.moduleType = moduleType;
  }

  public long getSvnRevisionBL() {
    return svnRevisionBL;
  }

  private void setSvnRevisionBL(long newVlaue) {
    Object oldValue = getSvnRevisionBL();
    this.svnRevisionBL = newVlaue;
    firePropertyChange(PROPERTY_SVNREVISION_BL, oldValue, newVlaue);
  }

  public long getSvnRevisionApp() {
    return svnRevisionApp;
  }

  private void setSvnRevisionApp(long newVlaue) {
    Object oldValue = getSvnRevisionApp();
    this.svnRevisionApp = newVlaue;
    firePropertyChange(PROPERTY_SVNREVISION_APP, oldValue, newVlaue);
  }

  public long getArch() {
    return arch;
  }

  private void setArch(long newVlaue) {
    Object oldValue = getArch();
    this.arch = newVlaue;
    firePropertyChange(PROPERTY_ARCH, oldValue, newVlaue);
  }

  public long getRunningTimeSecs() {
    return runningTimeSecs;
  }

  private void setRunningTimeSecs(long newVlaue) {
    Object oldValue = getRunningTimeSecs();
    this.runningTimeSecs = newVlaue;
    firePropertyChange(PROPERTY_RUNNINGTIMESECS, oldValue, newVlaue);
  }

  public void parseBytes(byte[] moduleDetailBytes) {
    assert moduleDetailBytes.length == G3Protocol.PLSize_R_ModuleDetail;

    ByteBuffer buf = createByteBuffer(moduleDetailBytes);

    // Running time
    long svnRevisionBL = NumUtils.convert2Unsigned(buf.getInt());// 4 bytes
    long svnRevisionApp = NumUtils.convert2Unsigned(buf.getInt());// 4 bytes
    long arch = NumUtils.convert2Unsigned(buf.getInt());// 4 bytes
    long runningTimeSecs = NumUtils.convert2Unsigned(buf.getInt());// 4 bytes

    setRunningTimeSecs(runningTimeSecs);
    setSvnRevisionBL(svnRevisionBL);
    setSvnRevisionApp(svnRevisionApp);
    setArch(arch);
  }

}
