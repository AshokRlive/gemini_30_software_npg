/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.rtu.comms.service.realtime.modules;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Inherit from ModuleDetail since their payload structure are exactly the
 * same.
 */
public final class PL_Send_CANStat extends PL_Send_ModuleDetail {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_ModuleCANStat;


  public PL_Send_CANStat(MODULE type, MODULE_ID id) {
    super(type, id);
  }
}