/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import static com.lucy.g3.rtu.comms.protocol.ISerializable.Buffer.createByteBuffer;

import java.nio.ByteBuffer;
import java.util.Arrays;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;

/**
 * CAN Statistics data structure.
 */
public class CANStatistics {

  private static final String[] FIELD_NAMES = {
      "CAN error ",
      "CAN bus error",
      "CAN arbitration error",
      "CAN data overrun",
      "CAN Bus OFF",
      "CAN error passive",
      "CAN transmitted packets count",
      "CAN received packets count",
      "CAN Rx lost packets count",
  };

  private long[] fieldsValue = new long[FIELD_NAMES.length];
  private boolean[] fieldsValid = new boolean[FIELD_NAMES.length];


  public String[] getFieldNames() {
    return Arrays.copyOf(FIELD_NAMES, FIELD_NAMES.length);
  }

  public long[] getFieldsValue() {
    return Arrays.copyOf(fieldsValue, fieldsValue.length);
  }

  public boolean[] getFieldsValid() {
    return Arrays.copyOf(fieldsValid, fieldsValid.length);
  }

  public static CANStatistics parseBytes(byte[] data) {
    assert data != null;
    assert data.length == G3Protocol.PLSize_R_ModuleCANStat;

    CANStatistics canStat = new CANStatistics();
    ByteBuffer buf = createByteBuffer(data);
    short validMask = buf.getShort();

    for (int i = 0; i < canStat.fieldsValue.length; i++) {
      canStat.fieldsValue[i] = NumUtils.convert2Unsigned(buf.getInt());// 4 bytes
      canStat.fieldsValid[i] = NumUtils.getBitsValue(validMask, i, i) == 1;
    }
    return canStat;
  }

}
