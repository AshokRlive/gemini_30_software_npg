/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import java.nio.ByteBuffer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The Class PL_Send_ModuleDetail.
 */
class PL_Send_ModuleDetail extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_ModuleDetail;
  private final MODULE type;
  private final MODULE_ID id;


  PL_Send_ModuleDetail(MODULE type, MODULE_ID id) {
    super(PAYLOAD_SIZE);
    this.type = Preconditions.checkNotNull(type, "type is null");
    this.id = Preconditions.checkNotNull(id, "id is null");
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);

    buf.put((byte) type.getValue());
    buf.put((byte) id.getValue());

    return buf.array();
  }
}