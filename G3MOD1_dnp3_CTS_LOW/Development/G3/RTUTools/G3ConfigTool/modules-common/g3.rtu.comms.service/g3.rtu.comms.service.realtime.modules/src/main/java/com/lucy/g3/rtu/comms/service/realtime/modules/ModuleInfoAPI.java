/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import java.io.IOException;
import java.util.List;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This interface defines the commands to get module information.
 */
public interface ModuleInfoAPI extends ICommsAPI {

  /**
   * Reads the module list and status from RTU and save to local module list
   * model.
   */
  List<ModuleInfo> cmdGetModueInfo()
      throws IOException, SerializationException;

  /**
   * Reads the detail of a module from RTU and store into the given ModuleDetail
   * object.
   */
  void cmdGetModueDetail(ModuleDetail mDetail)
      throws IOException, SerializationException;

  /**
   * Reads the alarms of a module from RTU.
   */
  ModuleAlarm[] cmdGetModueAlarm(MODULE type, MODULE_ID id)
      throws IOException, SerializationException;

  /**
   * Reads modules CAN statistics from RTU.
   */
  CANStatistics[] cmdGetCANStat(MODULE type, MODULE_ID id)
      throws IOException, SerializationException;

  /**
   * Acknowledge one module's alarms.
   *
   */
  void cmdAckModueAlarm(MODULE type, MODULE_ID id) throws IOException,
      SerializationException;
}
