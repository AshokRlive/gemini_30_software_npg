/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_BOARD_ERROR;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;

public class ModuleInfoTest {

  private String[] readOnlyProperties = {
      ModuleInfo.PROPERTY_VERSION,
      ModuleInfo.PROPERTY_FEATUREVERSION,
      ModuleInfo.PROPERTY_SYSTEMAPI,
      ModuleInfo.PROPERTY_BOOTAPI,
      ModuleInfo.PROPERTY_BOOTVERSION,
      ModuleInfo.PROPERTY_SERIALNO,
      ModuleInfo.PROPERTY_INTERNALSTATE,
      ModuleInfo.PROPERTY_STATE_OK,
      ModuleInfo.PROPERTY_STATE_CONFIGURED,
      ModuleInfo.PROPERTY_STATE_PRESENT,
      ModuleInfo.PROPERTY_STATE_DETECTED,
      ModuleInfo.PROPERTY_STATE_REGISTERED,
      ModuleInfo.PROPERTY_STATE_DISABLED,
      ModuleInfo.PROPERTY_FSM_STATE,
  };


  @Test
  public void testConstruct() {
    MODULE type = MODULE.MODULE_SCM;
    MODULE_ID id = MODULE_ID.MODULE_ID_0;
    ModuleInfo m = new ModuleInfo(type, id);

    assertEquals(type, m.getModuleType());
    assertEquals(id, m.getModuleID());
  }

  @Test
  public void testSetErrorMask() {

    ModuleInfo fixture = new ModuleInfo(MODULE.MODULE_FDM, MODULE_ID.MODULE_ID_0);

    // Set "time synch" bit
    fixture.setErrorMask((byte) (MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_TSYNCH.getValue()));
    assertFalse(fixture.isTimeSync());

    fixture.setErrorMask((byte) (0 & (byte) (MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_TSYNCH.getValue())));
    assertTrue(fixture.isTimeSync());

    // Set "critical" bit
    fixture.setErrorMask((byte) (MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_CRITICAL.getValue()));
    assertTrue(fixture.hasError());
    assertEquals(1, fixture.getErrorFlags().size());
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_CRITICAL));

    // Set "error" bit
    fixture.setErrorMask((byte) (MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_ERROR.getValue()));
    assertTrue(fixture.hasError());
    assertEquals(1, fixture.getErrorFlags().size());
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_ERROR));

    // Set "info" bit
    fixture.setErrorMask((byte) (MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_INFO.getValue()));
    assertTrue(fixture.hasError());
    assertEquals(1, fixture.getErrorFlags().size());
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_INFO));

    // Set "warn" bit
    fixture.setErrorMask((byte) (MODULE_BOARD_ERROR.MODULE_BOARD_ERROR_ALARM_WARNING.getValue()));
    assertTrue(fixture.hasError());
    assertEquals(1, fixture.getErrorFlags().size());
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_WARNING));

    // Set all bit
    fixture.setErrorMask((byte) 0xFF);
    assertTrue(fixture.hasError());
    assertEquals(4, fixture.getErrorFlags().size());
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_WARNING));
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_INFO));
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_ERROR));
    assertTrue(fixture.getErrorFlags().contains(SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_CRITICAL));

  }

  @Test
  public void testReadOnlyProperties() {
    ModuleInfo bean = new ModuleInfo(MODULE.MODULE_BRD, MODULE_ID.MODULE_ID_0);
    BeanTestUtil.testReadOnlyProperties(bean, readOnlyProperties);
  }

  @Test
  public void testEquals() {
    ModuleInfo o1 = new ModuleInfo(MODULE.MODULE_FDM, MODULE_ID.MODULE_ID_0);
    ModuleInfo o11 = new ModuleInfo(MODULE.MODULE_FDM, MODULE_ID.MODULE_ID_0);
    ModuleInfo o2 = new ModuleInfo(MODULE.MODULE_FDM, MODULE_ID.MODULE_ID_1);
    ModuleInfo o3 = new ModuleInfo(MODULE.MODULE_SCM, MODULE_ID.MODULE_ID_0);

    assertTrue(o1.equals(o11));
    assertTrue(o11.equals(o1));
    assertTrue(o1.hashCode() == o1.hashCode());
    assertTrue(o1.hashCode() == o11.hashCode());

    assertFalse(o1.equals(null));

    assertFalse(o1.equals(o2));
    assertFalse(o2.equals(o1));
    assertFalse(o1.equals(o2));
    assertFalse(o2.equals(o1));

    assertFalse(o1.equals(o3));
    assertFalse(o3.equals(o1));
    assertFalse(o1.equals(o3));
    assertFalse(o3.equals(o1));

    assertFalse(o1.hashCode() == o2.hashCode());
    assertFalse(o1.hashCode() == o3.hashCode());
  }

  
  @Test
  public void testSerialDeocder() {
    int serial = 0X30000062;
    assertEquals(0x62, ModuleInfo.decodeModuleSerialNo(serial));
    assertEquals(0x18, ModuleInfo.decodeSupplierID(serial));
  }
}
