/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_STATE;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SUBSYSTEM;

public class ModuleAlarmTest {

  @Test
  public void testConstructor() {
    SYS_ALARM_STATE[] states = SYS_ALARM_STATE.values();
    SYS_ALARM_SUBSYSTEM[] subsystems = SYS_ALARM_SUBSYSTEM.values();

    final SYS_ALARM_SEVERITY severity = SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_ERROR;
    final long alarmcode = 0L;
    final int parameter = 2;

    for (SYS_ALARM_STATE state : states) {
      for (SYS_ALARM_SUBSYSTEM subsys : subsystems) {
        ModuleAlarm alarm = new ModuleAlarm(MODULE.MODULE_FDM, MODULE_ID.MODULE_ID_0, severity,
            (short) state.getValue(), (short) subsys.getValue(), alarmcode, parameter);
        assertEquals(state, alarm.getStateEnum());
        assertEquals(subsys, alarm.getSubSystemEnum());
        assertNotNull("alarmCodeEnum is decoded to " + alarm.getAlarmCodeEnum()
            + " when state:" + state + " subsystem:" + subsys + " alarmcode" + alarmcode,
            alarm.getAlarmCodeEnum());

        assertEquals(alarm.getAlarmCode(), alarmcode);
        assertEquals(alarm.getParameter(), parameter);
      }
    }
  }

}
