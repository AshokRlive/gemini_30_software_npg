/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import java.nio.ByteBuffer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;

/**
 * The Class PL_Reply_ModuleAlarms.
 */
class PL_Reply_ModuleAlarms extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ModuleAlarm;
  private final MODULE type;
  private final MODULE_ID id;

  private ModuleAlarm[] alarms = null;


  PL_Reply_ModuleAlarms(MODULE type, MODULE_ID id) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.type = Preconditions.checkNotNull(type, "type is null");
    this.id = Preconditions.checkNotNull(id, "id is null");
  }

  public ModuleAlarm[] getAlarms() {
    return alarms;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    int alarmNum = payload.length / PAYLOAD_SIZE;
    ByteBuffer buf = createByteBuffer(payload);

    short state;
    short subSystem;
    long alarmCode;
    int parameter;
    SYS_ALARM_SEVERITY severity;

    alarms = new ModuleAlarm[alarmNum];
    for (int i = 0; i < alarms.length; i++) {
      state = NumUtils.convert2Unsigned(buf.get());
      subSystem = NumUtils.convert2Unsigned(buf.get());
      alarmCode = NumUtils.convert2Unsigned(buf.getInt());
      severity = SYS_ALARM_SEVERITY.forValue(NumUtils.convert2Unsigned(buf.get()));
      parameter = NumUtils.convert2Unsigned(buf.getShort());
      alarms[i] = new ModuleAlarm(type, id, severity, state, subSystem, alarmCode, parameter);
    }
  }
}