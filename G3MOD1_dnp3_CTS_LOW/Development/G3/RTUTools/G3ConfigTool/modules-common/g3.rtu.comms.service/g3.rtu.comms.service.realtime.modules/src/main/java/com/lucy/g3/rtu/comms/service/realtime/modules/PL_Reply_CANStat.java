/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_CANStat.
 */
class PL_Reply_CANStat extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ModuleCANStat;

  private CANStatistics[] canStat;


  PL_Reply_CANStat() {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
  }

  public CANStatistics[] getCanStat() {
    return canStat;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    int num = payload.length / PAYLOAD_SIZE;
    canStat = new CANStatistics[num];

    ByteBuffer buf = createByteBuffer(payload);
    byte[] ibuf = new byte[PAYLOAD_SIZE];
    for (int i = 0; i < num; i++) {
      buf.get(ibuf);
      canStat[i] = CANStatistics.parseBytes(ibuf);
    }
  }

}