/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ModuleAlarm;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ModuleCANStats;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ModuleClearAlarm;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ModuleDetail;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ModuleInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_ModuleAlarm;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_ModuleCANStats;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_ModuleDetail;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_ModuleInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Module;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * A subsystem for retrieving and storing RTU module info.
 */
public class RTUModules extends AbstractCommsModule implements ModuleInfoAPI {

  public static final String PROPERTY_ALARMED = "alarmed";

  private Logger log = Logger.getLogger(RTUModules.class);

  private final ArrayListModel<ModuleInfo> modulesInfoList;

  private final PropertyChangeListener minfoPCL;

  private boolean alarmed;


  public RTUModules(IClient client) {
    super(client);
    this.modulesInfoList = new ArrayListModel<ModuleInfo>();
    this.minfoPCL = new ModuleInfoPCL();
  }

  public boolean isAlarmed() {
    return alarmed;
  }

  private void setAlarmed(boolean newValue) {
    Object oldValue = isAlarmed();
    this.alarmed = newValue;
    firePropertyChange(PROPERTY_ALARMED, oldValue, newValue);
  }

  @SuppressWarnings("unchecked")
  public ListModel<ModuleInfo> getModulesListModel() {
    return modulesInfoList;
  }

  public ModuleInfo[] getAllModuleInfo() {
    return modulesInfoList.toArray(new ModuleInfo[modulesInfoList.size()]);
  }

  public List<ModuleInfo> getAllModuleInfoList() {
    return new ArrayList<ModuleInfo>(modulesInfoList);
  }

  private synchronized void clearModuleList() {
    for (ModuleInfo minfo : modulesInfoList) {
      minfo.removePropertyChangeListener(minfoPCL);
      minfo.setOutOfSynch(true);
    }

    modulesInfoList.clear();
  }

  private synchronized void updateModuleList(List<ModuleInfo> mlist) {
    boolean alarmed = false;

    // Clear module list if it is in disconnect state.
    if (getState() == null 
        || getState() == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UNKNOWN) {
      clearModuleList();
      return;
    }

    ArrayList<ModuleInfo> notUpdatedModules = new ArrayList<ModuleInfo>(modulesInfoList);

    ModuleInfo updatedModule;
    if (mlist != null) {
      for (ModuleInfo m : mlist) {
        if (m == null) {
          continue;
        }

        updatedModule = getModuleInfo(m.getModuleType(), m.getModuleID());
        if (updatedModule != null) {
          updatedModule.copyFrom(m);
          notUpdatedModules.remove(updatedModule);

          if (updatedModule.hasError()) {
            alarmed = true;
          }
        } else {
          log.error("Module not found:" + m);
        }
      }
    }

    // Clean all module that are not updated.
    for (ModuleInfo m : notUpdatedModules) {
      m.removePropertyChangeListener(minfoPCL);
      m.setOutOfSynch(true);
    }

    modulesInfoList.removeAll(notUpdatedModules);

    // Fire event to notify GUI to repaint
    for (int i = 0; i < modulesInfoList.size(); i++) {
      modulesInfoList.fireContentsChanged(i);
    }

    // Set alarm flag
    setAlarmed(alarmed);
  }


  /**
   * Gets a moduleInfo instance from existed moduleInfoList by specifying module
   * type and id. If the module info doen't exist, simply create and return a
   * new one.
   *
   * @param type
   *          the module type's value for enum <code>MODULE</code>.
   * @param id
   *          the module ID's value of for enum <code>MODULE_ID</code>.
   * @return the found moduleInfo in moduleInfo list. <code>Null</code> if id or
   *         value cannot be converted to Enum.
   */
  private synchronized ModuleInfo getModuleInfo(MODULE type, MODULE_ID id) {
    if (getState() == null || getState() == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UNKNOWN) {
      return null;
    }

    if (type == null) {
      log.error("Cannot access module info. Unrecongized module type value: " + type);
      return null;
    }

    if (id == null) {
      log.error("Cannot access module info. Unrecongized module id value: " + id);
      return null;
    }

    /* Search in current list */
    ModuleInfo minfo = null;
    for (ModuleInfo m : modulesInfoList) {
      if (m.getModuleType() == type
          && m.getModuleID() == id) {
        minfo = m;
        break;
      }
    }

    /* Module info not found. Create new one */
    if (minfo == null) {
      minfo = new ModuleInfo(type, id);
      modulesInfoList.add(minfo);
      minfo.addPropertyChangeListener(minfoPCL);
    }

    return minfo;
  }

  private void printModuleList(List<ModuleInfo> modules) {
    if (!log.isDebugEnabled()) {
      return;
    }

    if (modules == null || modules.size() == 0) {
      log.debug("No modules");
      return;
    }

    StringBuffer sb = new StringBuffer();
    for (ModuleInfo m : modules) {
      if (m == null) {
        continue;
      }

      sb.append("\n\n---------- " + m.getModuleType() + " " + m.getModuleID() + "-------------------\n");
      sb.append("Serial:" + m.getSerialNo());
      sb.append(" Version:" + m.getVersion());
      sb.append("\nStatus:" + m.getInternalState());
      sb.append("\nErrors:" + m.getErrorText());
      sb.append("\nActive:" + m.isOk());
      sb.append("\nOnline:" + m.isDetected());
      sb.append("\nRegistered:" + m.isRegistered());
    }
    log.debug(sb.toString());
  }

  @Override
  public List<ModuleInfo> cmdGetModueInfo()
      throws IOException, SerializationException {

    PL_Reply_ModuleList reply = new PL_Reply_ModuleList();
    send(CTMsgType_Module    ,
         _CTMsgID_C_ModuleInfo,
         _CTMsgID_R_ModuleInfo,
        null,
        reply);

    ArrayList<ModuleInfo> mlist = reply.getModuleList();
    updateModuleList(mlist);
    printModuleList(mlist);

    return mlist;
  }
  

  @Override
  public ModuleAlarm[] cmdGetModueAlarm(MODULE type, MODULE_ID id)
      throws IOException, SerializationException {

    PL_Send_ModuleAlarms send = new PL_Send_ModuleAlarms(type, id);
    PL_Reply_ModuleAlarms reply = new PL_Reply_ModuleAlarms(type, id);

    send(CTMsgType_Module     ,
        _CTMsgID_C_ModuleAlarm,
        _CTMsgID_R_ModuleAlarm,
       send,
       reply);
    
    return reply.getAlarms();
  }

  @Override
  public void cmdAckModueAlarm(MODULE type, MODULE_ID id)
      throws IOException, SerializationException {

    PL_Send_ModuleAlarms send = new PL_Send_ModuleAlarms(type, id);

    send(CTMsgType_Module,          
         _CTMsgID_C_ModuleClearAlarm,
         _CTMsgID_R_AckNack        ,
        send,
        null);
  }

  @Override
  public void cmdGetModueDetail(ModuleDetail mDetail)
      throws IOException, SerializationException {

    PL_Send_ModuleDetail send = new PL_Send_ModuleDetail(mDetail.moduleType, mDetail.moduleID);
    PL_Reply_ModuleDetail reply = new PL_Reply_ModuleDetail(mDetail);

    send(CTMsgType_Module      , 
         _CTMsgID_C_ModuleDetail, 
         _CTMsgID_R_ModuleDetail, 
        send,
        reply);
  }

  @Override
  public CANStatistics[] cmdGetCANStat(MODULE type, MODULE_ID id)
      throws IOException, SerializationException {

    PL_Send_CANStat send = new PL_Send_CANStat(type, id);
    PL_Reply_CANStat reply = new PL_Reply_CANStat();

    send(CTMsgType_Module        ,
         _CTMsgID_C_ModuleCANStats,
         _CTMsgID_R_ModuleCANStats,
        send,
        reply);

    return reply.getCanStat();
  }

  
  private class ModuleInfoPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      int index = modulesInfoList.indexOf(evt.getSource());
      assert index >= 0;
      if (index >= 0) {
        // Notify GUI view to repaint
        modulesInfoList.fireContentsChanged(index);
      }
    }
  }


  public static void clearAll(RTUModules rtumodules) {
    if (rtumodules != null) {
      rtumodules.clearModuleList();
      rtumodules.setAlarmed(false);
    }
  }

}
