/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmAuxSwitchControllerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmBatteryChargerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmCalibrationEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmDigitalOutControllerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmFanControllerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmHmiControllerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmIoManagerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmLcdDriverEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmMCMEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmNvramEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmSerialProtocolEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmStatusManagerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmSupplyControllerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmSwitchControllerEnum;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmSystemEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_STATE;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SUBSYSTEM;

/**
 * This class stores all internal error information of a slave module.
 */
public class ModuleAlarm {

  /**
   * The format string used for printing out alarm context.
   * Parameters: severity, subsystem, alarm code, state.
   */
  public static final String ALARM_PRINT_FORMAT = "%-10s %-50s %-100s %s"; 

  private static Logger log = Logger.getLogger(ModuleAlarm.class);

  private final SYS_ALARM_SEVERITY severity;

  /* Bit fields decode with SYS_ALARM_STATE.*/
  private final short state;
  private final SYS_ALARM_STATE stateEnum;

  /* SYS_ALARM_SUBSYSTEM_.*/
  private final short subSystem;
  private final SYS_ALARM_SUBSYSTEM subSystemEnum;

  /** Subsystem specific alarm codes e.g. SYSALC_SYSTEM_* */
  private final long alarmCode;
  private final Enum<?> alarmCodeEnum;

  /* Additional debug info */
  private final int parameter;

  private final MODULE type;
  private final MODULE_ID id;


  /**
   * Construct instance from raw alarm values.
   */
  public ModuleAlarm(MODULE type, MODULE_ID id,
      SYS_ALARM_SEVERITY severity,
      short state,
      short subSystem,
      long alarmCode,
      int parameter) {

    super();
    this.type = Preconditions.checkNotNull(type, "type is null");
    this.id = Preconditions.checkNotNull(id, "id is null");

    this.severity = severity;
    this.state = state;
    this.subSystem = subSystem;
    this.alarmCode = alarmCode;
    this.parameter = parameter;

    // Decode state
    stateEnum = SYS_ALARM_STATE.forValue(NumUtils.convert2Unsigned(state));
    if (stateEnum == null) {
      log.warn(String.format("Alarm State value:%d cannot be decoded ", state));
    }

    // Decode sub system
    subSystemEnum = SYS_ALARM_SUBSYSTEM.forValue(subSystem);
    if (subSystemEnum == null) {
      log.warn(String.format("Alarm Sub System value:%d cannot be decoded ", subSystem));
    }

    // Decode alarm code enum
    if (subSystemEnum != null) {
      switch (subSystemEnum) {
      case SYS_ALARM_SUBSYSTEM_AUX_SWITCH_CONTROLLER:
        alarmCodeEnum = SysAlarmAuxSwitchControllerEnum.SYSALC_AUX_SWITCH_CONTROL.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_CALIBRATION:
        alarmCodeEnum = SysAlarmCalibrationEnum.SYSALC_CALIBRATION.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_DIGITAL_OUT_CON:
        alarmCodeEnum = SysAlarmDigitalOutControllerEnum.SYSALC_DIGITAL_OUT_CONTROL.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_FAN_CONTROLLER:
        alarmCodeEnum = SysAlarmFanControllerEnum.SYSALC_FAN_CONTROL.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_HMI_CONTROLLER:
        alarmCodeEnum = SysAlarmHmiControllerEnum.SYSALC_HMI_CONTROL.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_IOMANAGER:
        alarmCodeEnum = SysAlarmIoManagerEnum.SYSALC_IOMAN.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_LCD_DRIVER:
        alarmCodeEnum = SysAlarmLcdDriverEnum.SYSALC_LCD_DRIVER.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_NVRAM:
        alarmCodeEnum = SysAlarmNvramEnum.SYSALC_NVRAM.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_SERIAL_PROTOCOL:
        alarmCodeEnum = SysAlarmSerialProtocolEnum.SYSALC_SERIAL_PROTOCOL.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_STATUS_MANAGER:
        alarmCodeEnum = SysAlarmStatusManagerEnum.SYSALC_STATUS_MANAGER.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_SUPPLY_CONTROLLER:
        alarmCodeEnum = SysAlarmSupplyControllerEnum.SYSALC_SUPPLY_CONTROL.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_SWITCH_CONTROLLER:
        alarmCodeEnum = SysAlarmSwitchControllerEnum.SYSALC_SWITCH_CONTROLLER.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_SYSTEM:
        alarmCodeEnum = SysAlarmSystemEnum.SYSALC_SYSTEM.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_BATTERY_CHARGER:
        alarmCodeEnum = SysAlarmBatteryChargerEnum.SYSALC_BATTERY_CHARGER.forValue((int) alarmCode);
        break;

      case SYS_ALARM_SUBSYSTEM_MCM:
        alarmCodeEnum = SysAlarmMCMEnum.SYSALC_MCM.forValue((int) alarmCode);
        break;

      default:
        alarmCodeEnum = null;
      }

    } else {
      alarmCodeEnum = null;
    }

    if (alarmCodeEnum == null) {
      log.warn(String.format("Alarm Code value:%d for subsystem\"%s\" cannot be decoded", alarmCode, subSystemEnum));
    }
  }

  public MODULE getType() {
    return type;
  }

  public MODULE_ID getId() {
    return id;
  }

  public SYS_ALARM_STATE getStateEnum() {
    return stateEnum;
  }

  public SYS_ALARM_SUBSYSTEM getSubSystemEnum() {
    return subSystemEnum;
  }

  public Enum<?> getAlarmCodeEnum() {
    return alarmCodeEnum;
  }

  public String getState() {
    return stateEnum == null ? String.valueOf(state) : stateEnum.getDescription();
  }

  public boolean isActive() {
    return stateEnum == SYS_ALARM_STATE.SYS_ALARM_STATE_ACTIVE
        || stateEnum == SYS_ALARM_STATE.SYS_ALARM_STATE_REACTIVATE_LATCHED;
  }

  public String getSubSystem() {
    return subSystemEnum == null ? String.valueOf(subSystem) : subSystemEnum.getDescription();
  }

  public String getAlarmMessage() {
    return alarmCodeEnum == null ? "No Message" : alarmCodeEnum.toString();
  }

  public long getAlarmCode() {
    return alarmCode;
  }

  public int getParameter() {
    return parameter;
  }

  public SYS_ALARM_SEVERITY getSeverity() {
    return severity;
  }

  /**
   * Converts this alarm to formatted text.
   */
  public String getPrintText() {
    return String.format(ALARM_PRINT_FORMAT, getSeverity(), getSubSystem(), getAlarmMessage(), getState());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (alarmCode ^ (alarmCode >>> 32));
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result
        + ((severity == null) ? 0 : severity.hashCode());
    result = prime * result + subSystem;
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ModuleAlarm other = (ModuleAlarm) obj;
    if (alarmCode != other.alarmCode) {
      return false;
    }
    if (id != other.id) {
      return false;
    }
    if (severity != other.severity) {
      return false;
    }
    if (subSystem != other.subSystem) {
      return false;
    }
    if (type != other.type) {
      return false;
    }
    return true;
  }

}
