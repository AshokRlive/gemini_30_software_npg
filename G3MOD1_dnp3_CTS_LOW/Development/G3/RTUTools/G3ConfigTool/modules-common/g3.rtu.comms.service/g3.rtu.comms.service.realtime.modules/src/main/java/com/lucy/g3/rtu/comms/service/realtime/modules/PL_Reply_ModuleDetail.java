/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.modules;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_ModuleDetail.
 */
class PL_Reply_ModuleDetail extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ModuleDetail;

  private final ModuleDetail mDetail;


  PL_Reply_ModuleDetail(ModuleDetail mDetail) {
    super(PAYLOAD_SIZE);
    this.mDetail = mDetail;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    
    if (mDetail != null) {
      mDetail.parseBytes(payload);
    }
  }
}