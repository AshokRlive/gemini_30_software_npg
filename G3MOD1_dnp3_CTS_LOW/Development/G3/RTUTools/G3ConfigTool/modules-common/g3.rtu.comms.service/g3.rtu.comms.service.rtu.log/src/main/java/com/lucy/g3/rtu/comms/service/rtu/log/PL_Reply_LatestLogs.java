/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.rtu.comms.service.rtu.log;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * The Class PL_Reply_LatestLogs.
 */
class PL_Reply_LatestLogs extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_LogEntry;
  private final CTH_RUNNINGAPP mode;
  private ArrayList<LogEntry> logs;


  public PL_Reply_LatestLogs(CTH_RUNNINGAPP mode) {
    super(PayloadType.RAW, PAYLOAD_SIZE);
    this.mode = mode;
  }

  public ArrayList<LogEntry> getLogs() {
    return logs;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    logs = new ArrayList<LogEntry>();

    ByteBuffer buf = createByteBuffer(payload);
    do {
      long secs = NumUtils.convert2Unsigned(buf.getInt());
      long millSecs = NumUtils.convert2Unsigned(buf.getInt());
      short subSystem = NumUtils.convert2Unsigned(buf.get());
      short logLevel = NumUtils.convert2Unsigned(buf.get());
      int logLen = NumUtils.convert2Unsigned(buf.getShort());

      byte[] content = new byte[logLen];
      buf.get(content, buf.arrayOffset(), logLen);

      String log = new String(content);
      LogEntry entry = new LogEntry(secs, millSecs,
          LogEntry.logLevelValue2Description(logLevel, mode),
          LogEntry.subSystemValue2Description(subSystem, mode), log);

      logs.add(0, entry);
    } while (buf.remaining() >= PAYLOAD_SIZE);
  }
}