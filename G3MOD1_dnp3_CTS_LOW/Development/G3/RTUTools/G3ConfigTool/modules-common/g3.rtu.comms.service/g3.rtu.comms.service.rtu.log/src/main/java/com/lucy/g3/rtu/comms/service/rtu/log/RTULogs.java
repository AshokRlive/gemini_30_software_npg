/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.log;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetLastLog;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetLogFileName;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetLogLevel;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SetLogLevel;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetLastLog;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetLogFileName;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetLogLevel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Log;

import java.io.IOException;
import java.util.ArrayList;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * A subsystem for retrieving RTU logs/events.
 */
public class RTULogs extends AbstractCommsModule implements RTULogsAPI {

  public RTULogs(IClient client) {
    super(client);
  }

  /**
   * Gets the current log levels of RTU.
   */
  @Override
  public String[][] cmdGetLogLevel() throws IOException, SerializationException {

    PL_Reply_LogLevels reply = new PL_Reply_LogLevels(getState());
    send( CTMsgType_Log        ,  
          _CTMsgID_C_GetLogLevel,  
          _CTMsgID_R_GetLogLevel,  
        null,
        reply);

    return reply.getLoglevels();
  }
  

  /**
   * Sets the current log level of RTU.
   */
  @Override
  public void cmdSetLogLevel(String subSystem, String logLevel)
      throws IOException, SerializationException {
    PL_Send_LogLevels send = new PL_Send_LogLevels(getState(), subSystem, logLevel);
    
    send( CTMsgType_Log        ,  
        _CTMsgID_C_SetLogLevel,  
        _CTMsgID_R_AckNack,  
      send,
      null);
  }


  /**
   * Reads the latest logs of RTU.
   */
  @Override
  public ArrayList<LogEntry> cmdGetLatestLogs()
      throws IOException, SerializationException {

    PL_Reply_LatestLogs reply = new PL_Reply_LatestLogs(getState());
    
    send( CTMsgType_Log        ,
        _CTMsgID_C_GetLastLog,
        _CTMsgID_R_GetLastLog,
      null,
      reply);

    return reply.getLogs();
  }
  
  /**
   * Get the log file name of RTU.
   */
  @Override
  public String cmdGetLogFileName() throws IOException, SerializationException {

    PL_Reply_LogFileName reply = new PL_Reply_LogFileName();
    
    send( CTMsgType_Log        ,
        _CTMsgID_C_GetLogFileName,
        _CTMsgID_R_GetLogFileName,
      null,
      reply);

    return reply.getRTULogFileName();
  }

}
