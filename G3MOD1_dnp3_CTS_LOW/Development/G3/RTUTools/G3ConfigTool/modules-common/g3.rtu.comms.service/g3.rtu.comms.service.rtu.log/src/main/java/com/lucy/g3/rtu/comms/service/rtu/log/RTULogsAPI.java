/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.log;

import java.io.IOException;
import java.util.ArrayList;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;

/**
 * This interface defines the commands for login into RTU.
 */
public interface RTULogsAPI extends ICommsAPI {

  String[][] cmdGetLogLevel() throws IOException, SerializationException;

  void cmdSetLogLevel(String subSystem, String logLevel) throws IOException, SerializationException;

  ArrayList<LogEntry> cmdGetLatestLogs() throws IOException, SerializationException;

  String cmdGetLogFileName() throws IOException, SerializationException;
 
}
