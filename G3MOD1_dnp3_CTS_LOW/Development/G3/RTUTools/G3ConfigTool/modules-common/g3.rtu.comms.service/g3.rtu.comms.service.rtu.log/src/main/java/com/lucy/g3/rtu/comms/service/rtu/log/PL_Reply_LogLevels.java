/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.log;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * The Class PL_Reply_LogLevels.
 */
class PL_Reply_LogLevels extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_LogLevel;
  private String[][] loglevels;
  private final CTH_RUNNINGAPP mode;


  PL_Reply_LogLevels(CTH_RUNNINGAPP mode) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.mode = mode;
  }

  public String[][] getLoglevels() {
    return loglevels;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    ByteBuffer buf = createByteBuffer(payload);
    int logLevelNum = payload.length / PAYLOAD_SIZE;
    loglevels = new String[logLevelNum][2];

    for (int i = 0; i < loglevels.length; i++) {
      Short sysValue = NumUtils.convert2Unsigned(buf.get());
      Short levelValue = NumUtils.convert2Unsigned(buf.get());
      loglevels[i][0] = LogEntry.subSystemValue2Description(sysValue, mode);
      loglevels[i][1] = LogEntry.logLevelValue2Description(levelValue, mode);
    }
  }
}
