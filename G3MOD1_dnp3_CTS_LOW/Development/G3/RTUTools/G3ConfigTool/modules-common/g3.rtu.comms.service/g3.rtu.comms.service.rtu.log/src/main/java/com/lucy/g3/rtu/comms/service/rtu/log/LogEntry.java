/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.log;

import java.util.Date;

import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * RTU System log entry received from RTU.
 */
public class LogEntry {

  private final long seconds;

  private final long milliSecs;

  private final String logLevelID;

  private final String subSystemID;

  private final String logMsg;

  private Date timestamp;


  public LogEntry(long seconds, long milliSecs, String logLevelID, String subSystemID, String logMessage) {
    this.seconds = seconds;
    this.milliSecs = milliSecs;
    this.logMsg = logMessage;
    this.logLevelID = logLevelID;
    this.subSystemID = subSystemID;
  }

  public Date getTimestamp() {
    if (timestamp == null) {
      long totalMilliSecs = seconds * 1000 + milliSecs;
      timestamp = RTUTimeDecoder.decode(totalMilliSecs);
    }

    return timestamp;
  }

  public long getSeconds() {
    return seconds;
  }

  public long getMilliSecs() {
    return milliSecs;
  }

  public String getLogLevelID() {
    return logLevelID;
  }

  public String getSubSystemID() {
    return subSystemID;
  }

  public String getLogContent() {
    return logMsg;
  }

  public String getFormattedLogText() {
    StringBuilder builder = new StringBuilder(100);

    builder.append(RTUTimeDecoder.formatEventLog(getTimestamp()));

    if (logLevelID != null) {
      builder.append(String.format(" [%-8s] ", logLevelID));
    }
    if (subSystemID != null) {
      builder.append(String.format(" [%-20s] ", subSystemID));
    }
    builder.append(logMsg);
    return builder.toString();

  }

  public static final String logLevelValue2Description(short loglevelValue, CTH_RUNNINGAPP state) {
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL level =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL.forValue(loglevelValue);
      if (level != null) {
        return level.getDescription();
      } else {
        return String.valueOf(loglevelValue);
      }

    } else {
      com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL level =
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.forValue(loglevelValue);
      if (level != null) {
        return level.getDescription();
      } else {
        return String.valueOf(loglevelValue);
      }
    }
  }

  public static final String subSystemValue2Description(short subsystemValue, CTH_RUNNINGAPP state) {
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID subsys =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID.forValue(subsystemValue);
      if (subsys != null) {
        return subsys.getDescription();
      } else {
        return String.valueOf(subsystemValue);
      }

    } else {
      com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID subsys =
          com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID.forValue(subsystemValue);
      if (subsys != null) {
        return subsys.getDescription();
      } else {
        return String.valueOf(subsystemValue);
      }
    }

  }

  public static final Byte logLevelDescription2Value(String loglevelDescription, CTH_RUNNINGAPP state) {
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL.values();
      for (int i = 0; i < levels.length; i++) {
        if (levels[i].getDescription().equals(loglevelDescription)) {
          return (byte) levels[i].getValue();
        }
      }
      return null;

    } else {
      com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.values();
      for (int i = 0; i < levels.length; i++) {
        if (levels[i].getDescription().equals(loglevelDescription)) {
          return (byte) levels[i].getValue();
        }
      }
      return null;
    }
  }

  /**
   * Gets list of description of all available G3 log levels.
   */
  public static final String[] getAllLogLevels(CTH_RUNNINGAPP state) {
    String[] levelItems = new String[0];
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL.values();
      levelItems = new String[levels.length];
      for (int i = 0; i < levels.length; i++) {
        levelItems[i] = levels[i].getDescription();
      }
    } else if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.values();
      levelItems = new String[levels.length];
      for (int i = 0; i < levels.length; i++) {
        levelItems[i] = levels[i].getDescription();
      }
    }
    return levelItems;
  }

  /**
   * Gets list of description of all available G3 sub systems.
   *
   * @param state
   *          the state
   * @return the all subsystems
   * @deprecated bad design.
   */
  @Deprecated
  public static final String[] getAllSubsystems(CTH_RUNNINGAPP state) {
    String[] subsytemItems = new String[0];
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID[] subsystems =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID.values();
      subsytemItems = new String[subsystems.length];
      for (int i = 0; i < subsystems.length; i++) {
        subsytemItems[i] = subsystems[i].getDescription();
      }
    } else if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID[] subsystems =
          com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID.values();
      subsytemItems = new String[subsystems.length];
      for (int i = 0; i < subsystems.length; i++) {
        subsytemItems[i] = subsystems[i].getDescription();
      }
    }
    return subsytemItems;
  }

  public static final Byte subSystemDescription2Value(String subsystemDescription, CTH_RUNNINGAPP state) {
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID[] subsystems =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID.values();
      for (int i = 0; i < subsystems.length; i++) {
        if (subsystems[i].getDescription().equals(subsystemDescription)) {
          return (byte) subsystems[i].getValue();
        }
      }
      return null;

    } else {
      com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID[] subsystems =
          com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID.values();
      for (int i = 0; i < subsystems.length; i++) {
        if (subsystems[i].getDescription().equals(subsystemDescription)) {
          return (byte) subsystems[i].getValue();
        }
      }
      return null;
    }
  }

}
