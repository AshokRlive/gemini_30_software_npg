/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service;

import java.io.IOException;

import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.client.Session;
import com.lucy.g3.rtu.comms.datalink.DataLink;
import com.lucy.g3.rtu.comms.datalink.http.HttpDataLink;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.realtime.modules.RTUModules;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUControl;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUTestAPI;
import com.lucy.g3.rtu.comms.service.rtu.control.RestartAPI;
import com.lucy.g3.rtu.comms.service.rtu.events.RTUEvents;
import com.lucy.g3.rtu.comms.service.rtu.events.RTUEventsAPI;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogs;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogsAPI;
import com.lucy.g3.rtu.comms.service.rtu.login.RTULogin;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.FactoryResetAPI;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.FirmwareUpgrade;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * Facade class for accessing G3 comms service.
 */
public class RTUCommsService implements G3Protocol,
RTULoginExtAPI, RTUTestAPI, FactoryResetAPI, RestartAPI, CommissioningAPI{

//@formatter:off
 // ====== Communication Modules ======
 private final IClient client;
 private final RTULogin          login;
 private final RTUInfo           rtuInfo;
 private final RTUPoints         rtuPoints;
 private final RTUModules        rtuModules;
 private final RTUControl        rtuControl;
 private final RTUStatus         rtuStatus;
 private final FileTransfer      fileTransfer;
// private final FileTransferService  fileTransferService;
 private final RTULogs           rtuLogs;
 private final RTUEvents         rtuEvents;
 private final FirmwareUpgrade   fwUpgrade;
 // @formatter:on
  public RTUCommsService(String rtuIPAddress) {
    this.client = new G3RTUClient(createDataLink(rtuIPAddress));

    // Initialise real-time components
    login = new RTULogin(client);
    rtuInfo = new RTUInfo(client);
    rtuModules = new RTUModules(client);
    rtuPoints = new RTUPoints(client);
    rtuStatus = new RTUStatus(client);
    fileTransfer = new FileTransfer(client);
    // fileTransferService = new FileTransferService(fileTransfer);
    rtuLogs = new RTULogs(client);
    rtuEvents = new RTUEvents(client);
    rtuControl = new RTUControl(client, rtuStatus);

    fwUpgrade = new FirmwareUpgrade(client, rtuStatus, rtuModules, rtuControl);

  }
  

  private DataLink createDataLink(String hostAddress) {
//    String enableZMQ = System.getProperty(SYSTEM_PROPERTY_ZMQ_ENABLED);
//    if (enableZMQ != null && "true".equalsIgnoreCase(enableZMQ)) {
//      return new ZMQDataLink(hostAddress);
//    } else {
//      return new HttpDataLink(hostAddress);
//    }
      return new HttpDataLink(hostAddress);
  }
  
  public void clearAll() {
    RTUInfo.clearAll(rtuInfo);
    RTUModules.clearAll(rtuModules);
    RTUStatus.clearAll(rtuStatus);
  }

  public void clearForCommsLost() {
    // Do not clear since these info are not read back when connection recoveres
    RTUInfo.clearAll(rtuInfo);
    RTUModules.clearAll(rtuModules);
    RTUStatus.clearAll(rtuStatus);
  }

  public CTH_RUNNINGAPP getRTURunningMode() {
    return (CTH_RUNNINGAPP) client.getSession().getData(Session.SESSSION_DATA_RTU_RUNNING_MODE);
  }

  public IClient getClient() {
    return client;
  }

  @Override
  public DataLink getLinkLayer() {
    return client.getDataLink();
  }

  public RTUInfo getRtuInfo() {
    return rtuInfo;
  }

  public RTUPoints getRtuPoints() {
    return rtuPoints;
  }

  public RTUModules getRtuModules() {
    return rtuModules;
  }

  public RTUStatus getRtuStatus() {
    return rtuStatus;
  }

  public FileTransfer getFileTransfer() {
    return fileTransfer;
  }

  // public FileTransferService getFileTransferService() {
  // return fileTransferService;
  // }

  public RTULogsAPI getRtuLogs() {
    return rtuLogs;
  }

  public RTUEventsAPI getRtuEvents() {
    return rtuEvents;
  }

  public RTULogin getLogin() {
    return login;
  }

  public RTUControl getRtuControl() {
    return rtuControl;
  }

  public FirmwareUpgrade getFwUpgrade() {
    return fwUpgrade;
  }

  public void setHost(String address) {
    client.getDataLink().setHost(address);
  }

  public String getHost() {
    return client.getDataLink().getHost();
  }

  public void setConnectTimeout(int timeoutMs) {
    client.getDataLink().setTimeout(timeoutMs);
  }

  @Override
  public CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException {
    return getRtuStatus().cmdCheckAlive();
  }

  @Override
  public boolean cmdCheckConfigExist() {
    try {
      getRtuInfo().cmdGetConfigInfo();
      return getRtuInfo().getConfigExist() == Boolean.TRUE;
    } catch (Exception e) {
      return false; // Sending command failed
    }
  }

  @Override
  public G3ConfigAPI cmdGetConfigAPI() throws IOException, SerializationException {
    return getRtuInfo().cmdGetConfigAPI();
  }

  @Override
  public LoginResult cmdLogin(String userName, char[] password) throws IOException, SerializationException {
    return getLogin().cmdLogin(userName, password);
  }

  @Override
  public OLR_STATE cmdGetOLR() throws IOException, SerializationException {
    return getRtuStatus().cmdGetOLR();
  }

  @Override
  public void cmdEraseConfig() throws IOException, SerializationException {
    getRtuControl().cmdEraseConfig();
  }

  @Override
  public void cmdActivateConfig() throws IOException, SerializationException {
    getRtuControl().cmdActivateConfig();
  }

  @Override
  public void cmdRestoreConfig() throws IOException, SerializationException {
    getRtuControl().cmdRestoreConfig();
  }

  @Override
  public void cmdRegisterModules() throws IOException, SerializationException {
    getRtuControl().cmdRegisterModules();
  }

  @Override
  public void cmdEraseCommissioningCache() throws IOException, SerializationException {
    getRtuControl().cmdEraseCommissioningCache();

  }

  @Override
  public void cmdRestart(RestartMode mode, boolean forced) throws IOException, SerializationException {
    getRtuControl().cmdRestart(mode, forced);

  }

  @Override
  public void cmdFactoryReset() throws IOException, SerializationException {
    getFwUpgrade().cmdFactoryReset();

  }

  @Override
  public void cmdOperateSwitch(short logicID, SWITCH_OPERATION openClose, LocalRemoteCode localRemote)
      throws IOException, SerializationException {
    getRtuControl().cmdOperateSwitch(logicID, openClose, localRemote);
  }

  @Override
  public void cmdOperateLogic(short logicID, short period) throws IOException, SerializationException {
    getRtuControl().cmdOperateLogic(logicID, period);
  }

  @Override
  public void cmdSetOLR(OLR_STATE state) throws IOException, SerializationException {
    getRtuControl().cmdSetOLR(state);
  }

}
