/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service;

import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * Help class for Waiting RTU Restart.
 */
public class RTURestartSupport {

  /**
   * The approximate time cost required by RTU to restart application.
   */
  static final int RTU_APP_EXIT_TIME_COST = 10000; // ms

  private Logger log = Logger.getLogger(RTURestartSupport.class);
  
  private int timeoutSecs;
  private CTH_RUNNINGAPP state;
  
  private final IWaitingRestartInvoker invoker;
  private final int rtuRestartTimeSecs;
  
  RTURestartSupport(IWaitingRestartInvoker invoker, int rtuRestartTimeSecs, int timeoutSecs) {
    super();
    if(timeoutSecs <= 0)
      throw new IllegalArgumentException("timeout must > 0");
    
    if(invoker == null)
      throw new NullPointerException("invoker must not be null");
    this.invoker = invoker;
    this.timeoutSecs =  timeoutSecs;
    this.rtuRestartTimeSecs = rtuRestartTimeSecs;
  }

  private boolean ping() {
    state = invoker.checkRTUState();
    return state != null;
  }

  private void checkTimeout(long start) throws TimeoutException {
    long elapsedTime = System.nanoTime() - start;
    double seconds = elapsedTime / 1000000000.0;

    if (seconds > timeoutSecs) {
      throw new TimeoutException("RTU restart timeout!");
    }
  }

  /** Waiting RTU to restart */
  public final CTH_RUNNINGAPP waitingRestart() throws InterruptedException, TimeoutException {
    if(rtuRestartTimeSecs > 0)
    Thread.sleep(rtuRestartTimeSecs);// Give RTU time to exit and restart

    // Checking if restart is completed
    long start = System.nanoTime();
    while (!invoker.isCancelled()) {
      if (ping())
        break;

      checkTimeout(start);
      Thread.sleep(1000);
    }

    return invoker.isCancelled() ? null : state;
  }

  /**
   * Waiting the RTU back from restart. A exception will be thrown if it is
   * timeout.
   *
   * @param invoker
   *          the invoker task
   * @param timeoutSecs
   *          timeout of waiting (seconds)
   * @return the state after restarting, non-null.
   */
  public static final CTH_RUNNINGAPP waitingRestart(IWaitingRestartInvoker invoker, final int timeoutSecs)
      throws TimeoutException, InterruptedException {
    return new RTURestartSupport(invoker, RTU_APP_EXIT_TIME_COST, timeoutSecs).waitingRestart();
  }
  
  
  public interface IWaitingRestartInvoker {
    
    boolean isCancelled();
    
    CTH_RUNNINGAPP checkRTUState();
  }
}
