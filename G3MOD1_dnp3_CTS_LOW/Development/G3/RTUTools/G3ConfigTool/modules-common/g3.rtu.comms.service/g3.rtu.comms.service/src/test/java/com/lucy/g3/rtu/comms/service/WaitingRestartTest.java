/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service;

import java.util.concurrent.TimeoutException;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.comms.service.RTURestartSupport;
import com.lucy.g3.rtu.comms.service.RTURestartSupport.IWaitingRestartInvoker;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;


public class WaitingRestartTest implements IWaitingRestartInvoker {
  private int rtuRestartTimeSecs;
  
  private long elapsedTime;
  private long startTime;
  
  @Before
  public void init() {
    elapsedTime = 0;
    rtuRestartTimeSecs = 0;
    
  }
  
  @Test(expected =TimeoutException.class)
  public void testWaitRestartTimeout() throws TimeoutException, InterruptedException {
    rtuRestartTimeSecs = 3;
    startTime = System.nanoTime();
    new RTURestartSupport(this, 0, 1).waitingRestart();
  }
  
  @Test
  public void testWaitRestart() throws TimeoutException, InterruptedException {
    rtuRestartTimeSecs = 1;
    startTime = System.nanoTime();
    new RTURestartSupport(this, 0, rtuRestartTimeSecs).waitingRestart();
  }

  @Override
  public boolean isCancelled() {
    return false;
  }
  
  @Override
  public CTH_RUNNINGAPP checkRTUState() {
    elapsedTime = System.nanoTime() - startTime;
 
    if(elapsedTime / 1000000000.0 >= rtuRestartTimeSecs)
      return CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP;
    
    return null;
  }
}

