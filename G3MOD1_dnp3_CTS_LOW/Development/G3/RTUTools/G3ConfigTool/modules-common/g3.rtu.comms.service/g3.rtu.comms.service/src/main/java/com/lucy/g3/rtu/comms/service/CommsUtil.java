/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service;

import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfoAPI;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;
import com.lucy.g3.rtu.comms.service.rtu.control.ConfigurationAPI;
import com.lucy.g3.rtu.comms.service.rtu.events.RTUEventsAPI;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogsAPI;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.FactoryResetAPI;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeAPI;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;


/**
 * Providing convenient methods to access {@code RTUCommsService}. 
 */
public class CommsUtil {
  private CommsUtil(){}
  
  public static NetInfo getRTUEth0Info(RTUCommsService comms) {
    return comms.getRtuInfo().getNetInfo(ETHERNET_PORT.ETHERNET_PORT_CONTROL);
  }
  
  public static NetInfo[] getRTUEthInfo(RTUCommsService comms) {
    return comms.getRtuInfo().getNetInfoArray();
  }

  public static FileTransfer getFileTransfer(RTUCommsService comms){
    return comms.getFileTransfer();
  }
  
  public static CommissioningAPI getCommissionAPI(RTUCommsService comms) {
    return comms;
  }
  
  public static ConfigurationAPI getConfigurationAPI(RTUCommsService comms) {
    return comms.getRtuControl();
  }
  
  public static FactoryResetAPI getFactoryResetAPI(RTUCommsService comms) {
    return comms;
  }

  public static UpgradeAPI getUpgradeAPI(RTUCommsService comms) {
    return comms.getFwUpgrade();
  }

  public static RTULoginExtAPI getLoginCommand(RTUCommsService comms) {
    return comms;
  }

  public static RTUEventsAPI getRTUEventsAPI(RTUCommsService comms) {
    return comms.getRtuEvents();
  }

  public static RTULogsAPI getRTULogsAPI(RTUCommsService comms) {
    return comms.getRtuLogs();
  }

  public static ModuleInfoAPI getModuleInfoAPI(RTUCommsService comms) {
    return comms.getRtuModules();
  }
  
  public static RTUPoints getRTUPointsAPI(RTUCommsService comms) {
    return comms.getRtuPoints();
  }
}

