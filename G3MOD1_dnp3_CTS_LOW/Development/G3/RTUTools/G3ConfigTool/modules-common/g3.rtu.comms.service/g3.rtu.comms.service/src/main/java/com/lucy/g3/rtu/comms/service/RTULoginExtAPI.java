/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service;

import java.io.IOException;

import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.datalink.DataLink;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.realtime.status.HeartBeatAPI;
import com.lucy.g3.rtu.comms.service.rtu.login.RTULoginAPI;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
/**
 * Extended RTULoginAPI.
 */
public interface RTULoginExtAPI extends RTULoginAPI, HeartBeatAPI{

  G3ConfigAPI cmdGetConfigAPI() throws IOException, SerializationException;

  boolean cmdCheckConfigExist() throws IOException, SerializationException;
  
  OLR_STATE cmdGetOLR() throws IOException, SerializationException;
  
  DataLink getLinkLayer();
}

