/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_LASTRESET;

/**
 * The Class PL_Reply_RTUInfo.
 */
class PL_Reply_RTUInfo extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_RTUInfo;

  private static final int ROOTFS_SIZE = 200;
  private static final int KERNEL_REVISION_SIZE = 200;
  private static final int MCM_REVISION_SIZE = 200;

  public String cpuSerial; // 8 bytes
  public String mcmVersion; // 200 bytes
  public String kernelVersion; // 200 bytes
  public String rootfsVersion; // 200 bytes
  public String resetCause; // 1 byte
  public String siteName; // 1+ bytes


  PL_Reply_RTUInfo() {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    ByteBuffer buf = createByteBuffer(payload);

    /* Decode cpuSerial, 8 bytes */
    byte[] cpuserial = new byte[8];
    buf.get(cpuserial);
    /* Reverse bytes array cause it comes from little endian system */
    byte temp;
    for (int i = 0; i < cpuserial.length / 2; i++) {
      temp = cpuserial[i];
      cpuserial[i] = cpuserial[cpuserial.length - 1 - i];
      cpuserial[cpuserial.length - 1 - i] = temp;
    }
    cpuSerial = (NumUtils.byteToHex(cpuserial)).toUpperCase();

    /* Decode mcmVersion,200 bytes */
    byte[] mcmVersionBytes = new byte[MCM_REVISION_SIZE];
    buf.get(mcmVersionBytes);
    mcmVersion = bytesToString(mcmVersionBytes);

    /* Decode kernelVersion,200 bytes */
    byte[] kernelVersionBytes = new byte[KERNEL_REVISION_SIZE];
    buf.get(kernelVersionBytes);
    kernelVersion = bytesToStringASCII(kernelVersionBytes);

    /* Decode rootfsVersion, 100 bytes */
    byte[] rootfsVersionBytes = new byte[ROOTFS_SIZE];
    buf.get(rootfsVersionBytes);
    rootfsVersion = bytesToString(rootfsVersionBytes);

    /* Decode resetCause,1byte */
    CTH_LASTRESET resetCasue = CTH_LASTRESET.forValue(NumUtils.convert2Unsigned(buf.get()));
    if (resetCasue == null) {
      resetCasue = CTH_LASTRESET.CTH_LASTRESET_UNKNOWN;
    }
    resetCause = ("Last time reset cause:" + resetCasue.getDescription());

    /* Decode siteName, 1+ bytes */
    byte[] siteNameBytes = new byte[buf.remaining()];
    buf.get(siteNameBytes);
    siteName = bytesToStringASCII(siteNameBytes);
    /* Convert the site name if it is encoded as HEX string */
    if (siteName.startsWith("0x") || siteName.startsWith("0X")) {
      try {
        siteName = SecurityUtils.decodeHexToString(siteName, G3Protocol.RTU_NAME_CHARSET);
      } catch (UnsupportedEncodingException e1) {
        siteName = new String(siteNameBytes).trim();
      }
    }
  }
}