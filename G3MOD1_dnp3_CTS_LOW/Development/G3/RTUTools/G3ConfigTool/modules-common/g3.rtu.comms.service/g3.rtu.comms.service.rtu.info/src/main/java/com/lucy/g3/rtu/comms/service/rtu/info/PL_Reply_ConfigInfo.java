/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_ConfigInfo.
 */
class PL_Reply_ConfigInfo extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ConfigInfo;

  public String configModifyDate; // 30 bytes
  public BasicVersion configFileVersion; // 8 bytes
  public Long configFileCRC; // 4 bytes
  public Boolean configExist; // 1 bytes
  public Boolean configValid; // 1 bytes
  public String configDesc; 


  PL_Reply_ConfigInfo() {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    ByteBuffer buf = createByteBuffer(payload);

    /* Decode configModifyDate, 30 bytes */
    byte[] mdate = new byte[30];
    buf.get(mdate);
    configModifyDate = (bytesToString(mdate).trim());

    /* Decode configFileVersion, 8 bytes */
    long configFileMajor = NumUtils.convert2Unsigned(buf.getInt()); // major
    long configFileMinor = NumUtils.convert2Unsigned(buf.getInt()); // minor
    configFileVersion = new BasicVersion(configFileMajor, configFileMinor);

    /* Decode CRC */
    configFileCRC = NumUtils.convert2Unsigned(buf.getInt());

    /* Decode Status */
    byte status = buf.get();
    configExist = NumUtils.getBitsValue(status, 0, 0) == 1;
    configValid = NumUtils.getBitsValue(status, 1, 1) == 1;

    /* Reserved*/
    byte[]  reserver = new byte[21];
    buf.get(reserver);
    
    /* Description*/
    if(buf.hasRemaining()) {
      byte[] descBytes = new byte[buf.remaining()];
      buf.get(descBytes);
      configDesc = bytesToString(descBytes).trim();
    }
  }
}