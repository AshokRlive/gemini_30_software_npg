/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetConfigInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetNetInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetRTUInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetSDPVersion;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetSysAPI;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetConfigInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetNetInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetRTUInfo;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetSDPVersion;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetSysAPI;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_RTUInfo;

import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * The comms module for retrieving and storing all RTU information.
 */
public class RTUInfo extends AbstractCommsModule {

  // @formatter:off
  public static final String PROPERTY_SDP_VERSION       = "sdpVersion";
  public static final String PROPERTY_SITENAME          = "siteName";
  public static final String PROPERTY_KERNELVERSION     = "kernelVersion";
  public static final String PROPERTY_ROOTFSVERSION     = "rootfsVersion";
  public static final String PROPERTY_MCMVERSION        = "mcmVersion";
  public static final String PROPERTY_CPU_SERIAL        = "cpuSerial";
  public static final String PROPERTY_LAST_RESET_CAUSE  = "resetCause";
  public static final String PROPERTY_CONFIG_API        = "configAPI";
  public static final String PROPERTY_NETWORK_IP0       = "ip0";
  public static final String PROPERTY_NETWORK_IP1       = "ip1";
  public static final String PROPERTY_CONFIG_STATUS     = "configStatusStr";
  // @formatter:on

  private static String NO_RTU_CONNECTED_TEXT = "No Device";

  private static String DEFAULT_RTU_NAME = "Gemini3 RTU";

  private Logger log = Logger.getLogger(RTUInfo.class);

  private String siteName = NO_RTU_CONNECTED_TEXT;

  private String cpuSerial;

  private String resetCause;

  private String kernelVersion;

  private String rootfsVersion;

  private String mcmVersion;

  private String sdpVersion;

  private NetInfo[] netInfoArray;
  private String ip0;
  private String ip1;

  private G3ConfigAPI configAPI;

  // ====== ConfigInfo ======
  private BasicVersion configFileVersion;
  private String configModifyDate;
  private Long configCRC;
  private Boolean configExist;
  private Boolean configValid;

  /*
   * The HTML text that represents the current configuration version or a
   * message says config does not exist.
   */
  private String configStatusStr;


  public RTUInfo(IClient client) {
    super(client);

    RTUInfo.clearAll(this);
  }

  public Boolean getConfigExist() {
    return configExist;
  }

  private void setConfigExist(Boolean configExist) {
    // Object oldValue = getConfigExist();
    this.configExist = configExist;
    // firePropertyChange(PROPERTY_CONFIG_EXIST,oldValue,configExist);
  }

  public Boolean getConfigValid() {
    return configValid;
  }

  private void setConfigValid(Boolean configValid) {
    // Object oldValue = getConfigValid();
    this.configValid = configValid;
    // firePropertyChange(PROPERTY_CONFIG_VALID,oldValue,configValid);
  }

  public Long getConfigCRC() {
    return configCRC;
  }

  private void setConfigCRC(Long configCRC) {
    // Object oldValue = getConfigCRC();
    this.configCRC = configCRC;
    // firePropertyChange(PROPERTY_CONFIG_CRC,oldValue,configCRC);
  }

  public String getConfigModifyDate() {
    return configModifyDate;
  }

  private void setConfigModifyDate(String configModifyDate) {
    // Object oldValue = getConfigModifyDate();
    this.configModifyDate = configModifyDate;
    // firePropertyChange(PROPERTY_CONFIG_MODIFY_DATE,oldValue,configModifyDate);
  }

  public String getConfigStatusStr() {
    return configStatusStr;
  }

  private void setConfigStatusStr(String configStatusStr) {
    Object oldValue = getConfigStatusStr();
    this.configStatusStr = configStatusStr;
    firePropertyChange(PROPERTY_CONFIG_STATUS, oldValue, configStatusStr);
  }

  public String getSiteName() {
    return siteName;
  }

  private void setSiteName(String name) {
    if (name == null || name.trim().isEmpty()) {
      name = DEFAULT_RTU_NAME;
    }

    Object oldValue = getSiteName();
    siteName = name;
    firePropertyChange(PROPERTY_SITENAME, oldValue, name);
  }

  public String getKernelVersion() {
    return kernelVersion;
  }

  private void setKernelVersion(String kernelVersion) {
    Object oldValue = getKernelVersion();
    this.kernelVersion = kernelVersion;
    firePropertyChange(PROPERTY_KERNELVERSION, oldValue, kernelVersion);
  }

  public String getRootfsVersion() {
    return rootfsVersion;
  }

  private void setRootfsVersion(String rootfsVersion) {
    Object oldValue = getRootfsVersion();
    this.rootfsVersion = rootfsVersion;
    firePropertyChange("rootfsVersion", oldValue, rootfsVersion);
  }

  public String getSdpVersion() {
    return sdpVersion;
  }

  public void setSdpVersion(String sdpVersion) {
    Object oldValue = this.sdpVersion;
    this.sdpVersion = sdpVersion;
    firePropertyChange(PROPERTY_SDP_VERSION, oldValue, sdpVersion);
  }

  public BasicVersion getConfigFileVersion() {
    return configFileVersion;
  }

  private void setConfigFileVersion(BasicVersion configFileVersion) {
    // Object oldValue = getConfigFileVersion();
    this.configFileVersion = configFileVersion;
    // firePropertyChange(PROPERTY_CONFIGFILE_VERSION,oldValue,configFileVersion);
  }

  public String getMcmVersion() {
    return mcmVersion;
  }

  private void setMcmVersion(String mcmVersion) {
    Object oldValue = getMcmVersion();
    this.mcmVersion = mcmVersion;
    firePropertyChange("mcmVersion", oldValue, mcmVersion);
  }

  public NetInfo[] getNetInfoArray() {
    return netInfoArray == null ? null : Arrays.copyOf(netInfoArray, netInfoArray.length);
  }

  public NetInfo getNetInfo(ETHERNET_PORT portEnum) {
    if (portEnum != null
        && netInfoArray != null
        && portEnum.getValue() < netInfoArray.length
        && portEnum.getValue() >= 0) {
      return netInfoArray[portEnum.getValue()];
    }

    return null;
  }

  private void setNetInfoArray(NetInfo[] netInfoArray) {
    if (netInfoArray != null && netInfoArray.length > 0) {
      this.netInfoArray = Arrays.copyOf(netInfoArray, netInfoArray.length);
      if (netInfoArray[0] != null) {
        setIp0(netInfoArray[0].getIp());
      }
      if (netInfoArray.length > 1 && netInfoArray[1] != null) {
        setIp1(netInfoArray[1].getIp());
      }
    } else {
      this.netInfoArray = new NetInfo[0];
      setIp0(TEXT_NA);
      setIp1(TEXT_NA);
    }
  }

  public String getIp0() {
    return ip0;
  }

  private void setIp0(String newValue) {
    Object oldValue = getIp0();
    this.ip0 = newValue;
    firePropertyChange(PROPERTY_NETWORK_IP0, oldValue, newValue);
  }

  public String getIp1() {
    return ip1;
  }

  private void setIp1(String newValue) {
    Object oldValue = getIp1();
    this.ip1 = newValue;
    firePropertyChange(PROPERTY_NETWORK_IP1, oldValue, newValue);
  }

  public String getCpuSerial() {
    return cpuSerial;
  }

  private void setCpuSerial(String cpuSerial) {
    Object oldValue = getCpuSerial();
    this.cpuSerial = cpuSerial;
    firePropertyChange(PROPERTY_CPU_SERIAL, oldValue, cpuSerial);
  }

  public String getResetCause() {
    return resetCause;
  }

  private void setResetCause(String resetCause) {
    Object oldValue = getResetCause();
    this.resetCause = resetCause;
    firePropertyChange(PROPERTY_LAST_RESET_CAUSE, oldValue, resetCause);
  }

  @Override
  public String toString() {
    return "RTUInfo [siteName=" + siteName + ", kernelVersion=" + kernelVersion + ", rootfsVersion="
        + rootfsVersion + ", configFileVersion=" + configFileVersion + "]";
  }

  public G3ConfigAPI getConfigAPI() {
    return configAPI;
  }

  private void setConfigAPI(G3ConfigAPI newG3ConfigAPI) {
    Object oldValue = getConfigAPI();
    this.configAPI = newG3ConfigAPI;
    firePropertyChange(PROPERTY_CONFIG_API, oldValue, newG3ConfigAPI);
  }

  /**
   * Reads the CONFIG API from RTU.
   * @throws SerializationException 
   */
  public G3ConfigAPI cmdGetConfigAPI() throws IOException, SerializationException {

    PL_Reply_ConfigAPI reply = new PL_Reply_ConfigAPI();

    send(CTMsgType_RTUInfo,
        _CTMsgID_C_GetSysAPI,
        _CTMsgID_R_GetSysAPI,
        null,
        reply);

    setConfigAPI(reply.getApi());

    return reply.getApi();
  }


  /**
   * Reads RTU information from RTU.
   */
  public void cmdGetRTUInfo() throws IOException, SerializationException {
    PL_Reply_RTUInfo reply = new PL_Reply_RTUInfo();
    
    send(CTMsgType_RTUInfo,
        _CTMsgID_C_GetRTUInfo,
        _CTMsgID_R_GetRTUInfo,
        null,
        reply);
    

    setRootfsVersion(reply.rootfsVersion);
    setCpuSerial(reply.cpuSerial);
    setResetCause(reply.resetCause);
    setSiteName(reply.siteName);
    setMcmVersion(reply.mcmVersion);
    setKernelVersion(reply.kernelVersion);
  }
  

  /**
   * Reads SDP version from RTU.
   */
  public void cmdGetSDPVersion() throws IOException, SerializationException {
    PL_Reply_SDPVersion reply = new PL_Reply_SDPVersion();
    
    send(CTMsgType_RTUInfo,
        _CTMsgID_C_GetSDPVersion,
        _CTMsgID_R_GetSDPVersion,
        null,
        reply);
    
    setSdpVersion(reply.getSdpVersion());
  }

  
  /**
   * Reads RTU information from RTU.
   */
  public void cmdGetConfigInfo() throws IOException, SerializationException {
    PL_Reply_ConfigInfo reply = new PL_Reply_ConfigInfo();
    send(CTMsgType_RTUInfo,
        _CTMsgID_C_GetConfigInfo,
        _CTMsgID_R_GetConfigInfo,
        null,
        reply);
    
    setConfigModifyDate(reply.configModifyDate);
    setConfigFileVersion(reply.configFileVersion);
    setConfigCRC(reply.configFileCRC);
    setConfigExist(reply.configExist);
    setConfigValid(reply.configValid);

    // Update config status text
    StringBuilder sb = new StringBuilder();
    sb.append("<html>");
    if (reply.configExist == Boolean.FALSE) {
      sb.append("<font color = \"red\">No Configuration</font>");// #FF8C00
    } else if (reply.configValid == Boolean.FALSE) {
      sb.append("<font color = \"red\">Invalid Configuration</font>");
    } else if (reply.configFileVersion != null) {
      sb.append("Version " + configFileVersion.toString());
      // Append description
      if(!Strings.isBlank(reply.configDesc)) {
        sb.append(" - ");
        sb.append(reply.configDesc);
      }
    } else {
      sb.append(TEXT_NA);
    }
    sb.append("</html>");

    setConfigStatusStr(sb.toString());
  }
  
  /**
   * Reads network information from RTU.
   */
  public NetInfo[] cmdGetNetInfo() throws IOException, SerializationException {

    PL_Reply_NetInfo reply = new PL_Reply_NetInfo();
    send(CTMsgType_RTUInfo,
        _CTMsgID_C_GetNetInfo,
        _CTMsgID_R_GetNetInfo,
        null,
        reply);

    printNetInfo(reply.getNetInfo());
    setNetInfoArray(reply.getNetInfo());
    return reply.getNetInfo();
  }
  

  private void printNetInfo(NetInfo[] netInfo) {
    if (!log.isDebugEnabled()) {
      return;
    }
    StringBuffer info = new StringBuffer();

    for (int i = 0; i < netInfo.length; i++) {
      info.append("-----------NetInfo [" + i + "] -------------");
      info.append("IP:" + netInfo[i].getIp());
      info.append("Mask:" + netInfo[i].getMask());
      info.append("Gateway:" + netInfo[i].getGateway());
      info.append("Broadcast" + netInfo[i].getBroadcast());
      info.append("DNS1:" + netInfo[i].getDns1());
      info.append("DNS2:" + netInfo[i].getDns2());
      info.append("Mac:" + netInfo[i].getMac());
      info.append("Device ID:" + netInfo[i].getDeviceID());
      info.append("Enabled:" + netInfo[i].isEnabled());
      info.append("DHCP:" + netInfo[i].isDhcp());
      info.append("Net ID:" + netInfo[i].getNetID());
    }
    log.debug(info.toString());
  }

  public static void clearAll(RTUInfo info) {
    if (info == null) {
      return;
    }

    // Clear RTU Info
    info.setSiteName(NO_RTU_CONNECTED_TEXT);
    info.setMcmVersion(TEXT_NA);
    info.setRootfsVersion(TEXT_NA);
    info.setKernelVersion(TEXT_NA);
    info.setCpuSerial(TEXT_NA);
    info.setResetCause(TEXT_NA);
    info.setSdpVersion(TEXT_NA);
    info.setConfigAPI(null);

    // Clear Net Info
    info.setNetInfoArray(null);

    // Clear Config Info
    info.setConfigFileVersion(null);
    info.setConfigModifyDate(TEXT_NA);
    info.setConfigExist(null);
    info.setConfigValid(null);
    info.setConfigCRC(null);
    info.setConfigStatusStr(TEXT_NA);
  }

}
