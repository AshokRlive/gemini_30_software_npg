/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_NetInfo.
 */
class PL_Reply_NetInfo extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_NetInfo;

  private NetInfo[] netInfo;


  PL_Reply_NetInfo() {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
  }

  public NetInfo[] getNetInfo() {
    return netInfo;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    int netNum = payload.length / PAYLOAD_SIZE;
    netInfo = new NetInfo[netNum];

    ByteBuffer buf = createByteBuffer(payload);
    byte[] ibuf = new byte[PAYLOAD_SIZE];
    for (int i = 0; i < netNum; i++) {
      buf.get(ibuf);
      netInfo[i] = NetInfo.parseBytes(ibuf);
    }
  }
}
