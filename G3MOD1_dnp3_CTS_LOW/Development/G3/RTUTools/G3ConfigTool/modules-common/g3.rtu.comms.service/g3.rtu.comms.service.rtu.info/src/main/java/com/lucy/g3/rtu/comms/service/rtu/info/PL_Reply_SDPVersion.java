/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_SDPVersion.
 */
class PL_Reply_SDPVersion extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_SDPVersion;
  private String sdpVersion;


  PL_Reply_SDPVersion() {
    super(PayloadType.RAW, PAYLOAD_SIZE);
  }

  public String getSdpVersion() {
    return sdpVersion;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    sdpVersion = new String(payload);
    sdpVersion = sdpVersion.trim();
  }
}