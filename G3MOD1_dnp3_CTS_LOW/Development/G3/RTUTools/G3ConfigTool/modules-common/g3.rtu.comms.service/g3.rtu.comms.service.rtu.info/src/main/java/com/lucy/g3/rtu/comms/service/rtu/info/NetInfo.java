/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import static com.lucy.g3.rtu.comms.protocol.ISerializable.Buffer.createByteBuffer;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;


/**
 * JavaBeans Class for storing the network information of RTU.
 */
public final class NetInfo {

  private static final String IP_FORMAT = "%s.%s.%s.%s";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_IP = "ip";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_MASK = "mask";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_GATEWAY = "gateway";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_BROADCAST = "broadcast";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_DNS1 = "dns1";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_DNS2 = "dns2";

  /** Simple read-only property {@value} . Value Type: <code>byte</code>. */
  public static final String PROPERTY_DEVICEID = "deviceID";

  /** Simple read-only property {@value} . Value Type: <code>Boolean</code>. */
  public static final String PROPERTY_ENABLED = "enabled";

  /** Simple read-only property {@value} . Value Type: <code>Boolean</code>. */
  public static final String PROPERTY_DHCP = "dhcp";

  /** Simple read-only property {@value} . Value Type: <code>String</code>. */
  public static final String PROPERTY_MAC = "mac";

  /** Simple read-only property {@value} .Value Type: <code>String</code>. */
  public static final String PROPERTY_NET_ID = "netID";

  /** The size of net ID in netInfo message. */
  private static final int NET_ID_SIZE = 100;

  private String ip;

  private String mask;

  private String gateway;

  private String broadcast;

  private String dns1;

  private String dns2;

  private String mac;

  private byte deviceID;

  private boolean enabled;

  private boolean dhcp;

  private String netID;


  public String getIp() {
    return ip;
  }

  protected void setIp(String ip) {
    // Object oldValue = getIp();
    this.ip = ip;
    // firePropertyChange("ip",oldValue,ip);
  }

  public String getMask() {
    return mask;
  }

  protected void setMask(String mask) {
    // Object oldValue = getMask();
    this.mask = mask;
    // firePropertyChange("mask",oldValue,mask);
  }

  public String getGateway() {
    return gateway;
  }

  protected void setGateway(String gateway) {
    // Object oldValue = getGateway();
    this.gateway = gateway;
    // firePropertyChange("gateway",oldValue,gateway);
  }

  public String getMac() {
    return mac;
  }

  protected void setMac(String mac) {
    // Object oldValue = getMac();
    this.mac = mac;
    // firePropertyChange("mac",oldValue,mac);
  }

  public String getNetID() {
    return netID;
  }

  protected void setNetID(String netID) {
    // Object oldValue = getNetID();
    this.netID = netID;
    // firePropertyChange("netID",oldValue,netID);
  }

  public String getBroadcast() {
    return broadcast;
  }

  protected void setBroadcast(String broadcast) {
    this.broadcast = broadcast;
  }

  public String getDns1() {
    return dns1;
  }

  protected void setDns1(String dns1) {
    this.dns1 = dns1;
  }

  public String getDns2() {
    return dns2;
  }

  protected void setDns2(String dns2) {
    this.dns2 = dns2;
  }

  public byte getDeviceID() {
    return deviceID;
  }

  protected void setDeviceID(byte deviceID) {
    this.deviceID = deviceID;
  }

  public boolean isEnabled() {
    return enabled;
  }

  protected void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isDhcp() {
    return dhcp;
  }

  protected void setDhcp(boolean dhcp) {
    this.dhcp = dhcp;
  }

  public static NetInfo parseBytes(byte[] netInfoBytes) {
    NetInfo netInfo = new NetInfo();

    ByteBuffer buf = createByteBuffer(netInfoBytes);
    /* IP Address */
    String address;
    int b0 = NumUtils.convert2Unsigned(buf.get());
    int b1 = NumUtils.convert2Unsigned(buf.get());
    int b2 = NumUtils.convert2Unsigned(buf.get());
    int b3 = NumUtils.convert2Unsigned(buf.get());
    address = String.format(IP_FORMAT, b0, b1, b2, b3);
    netInfo.setIp(address);

    /* IP Mask */
    b0 = NumUtils.convert2Unsigned(buf.get());
    b1 = NumUtils.convert2Unsigned(buf.get());
    b2 = NumUtils.convert2Unsigned(buf.get());
    b3 = NumUtils.convert2Unsigned(buf.get());
    address = String.format(IP_FORMAT, b0, b1, b2, b3);
    netInfo.setMask(address);

    /* Gateway */
    b0 = NumUtils.convert2Unsigned(buf.get());
    b1 = NumUtils.convert2Unsigned(buf.get());
    b2 = NumUtils.convert2Unsigned(buf.get());
    b3 = NumUtils.convert2Unsigned(buf.get());
    address = String.format(IP_FORMAT, b0, b1, b2, b3);
    netInfo.setGateway(address);

    /* Broadcast */
    b0 = NumUtils.convert2Unsigned(buf.get());
    b1 = NumUtils.convert2Unsigned(buf.get());
    b2 = NumUtils.convert2Unsigned(buf.get());
    b3 = NumUtils.convert2Unsigned(buf.get());
    address = String.format(IP_FORMAT, b0, b1, b2, b3);
    netInfo.setBroadcast(address);

    /* DNS1 */
    b0 = NumUtils.convert2Unsigned(buf.get());
    b1 = NumUtils.convert2Unsigned(buf.get());
    b2 = NumUtils.convert2Unsigned(buf.get());
    b3 = NumUtils.convert2Unsigned(buf.get());
    address = String.format(IP_FORMAT, b0, b1, b2, b3);
    netInfo.setDns1(address);

    /* DNS2 */
    b0 = NumUtils.convert2Unsigned(buf.get());
    b1 = NumUtils.convert2Unsigned(buf.get());
    b2 = NumUtils.convert2Unsigned(buf.get());
    b3 = NumUtils.convert2Unsigned(buf.get());
    address = String.format(IP_FORMAT, b0, b1, b2, b3);
    netInfo.setDns2(address);

    /* MAC Address. 6 bytes */
    String mac = String.format("%s-%s-%s-%s-%s-%s",
        NumUtils.byteToHex(buf.get()),
        NumUtils.byteToHex(buf.get()),
        NumUtils.byteToHex(buf.get()),
        NumUtils.byteToHex(buf.get()),
        NumUtils.byteToHex(buf.get()),
        NumUtils.byteToHex(buf.get()));

    netInfo.setMac(mac);

    /* Ethernet device ID */
    byte deviceID = buf.get();
    netInfo.setDeviceID(deviceID);

    /* Ethernet device Configuration */
    byte deviceConfig = buf.get();
    boolean enabled = NumUtils.getBitsValue(deviceConfig, 0, 0) == 1;
    boolean dhcp = NumUtils.getBitsValue(deviceConfig, 1, 1) == 1;
    netInfo.setEnabled(enabled);
    /* TODO Hack to set port to enabled */
    netInfo.setEnabled(true);
    /* End hack */
    netInfo.setDhcp(dhcp);

    /* Net ID */
    byte[] netIDBytes = new byte[NET_ID_SIZE];
    buf.get(netIDBytes, buf.arrayOffset(), NET_ID_SIZE);
    String netID;
    try {
      netID = new String(netIDBytes, G3Protocol.CHAR_SET);
    } catch (UnsupportedEncodingException e) {
      netID = new String(netIDBytes);
    }
    netInfo.setNetID(netID.trim());

    return netInfo;
  }
}
