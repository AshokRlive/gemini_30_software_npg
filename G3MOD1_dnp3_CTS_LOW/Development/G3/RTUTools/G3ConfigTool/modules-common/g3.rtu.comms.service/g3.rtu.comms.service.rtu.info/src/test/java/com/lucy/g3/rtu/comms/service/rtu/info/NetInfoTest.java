/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import org.junit.Test;

import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class NetInfoTest {

  private String[] readOnlyProperties = {
      NetInfo.PROPERTY_GATEWAY,
      NetInfo.PROPERTY_MAC,
      NetInfo.PROPERTY_IP,
      NetInfo.PROPERTY_MASK,
      NetInfo.PROPERTY_NET_ID,
      NetInfo.PROPERTY_BROADCAST,
      NetInfo.PROPERTY_DHCP,
      NetInfo.PROPERTY_DNS1,
      NetInfo.PROPERTY_DNS2,
      NetInfo.PROPERTY_DEVICEID,
      NetInfo.PROPERTY_ENABLED,
  };


  @Test
  public void testReadOnlyProperty() throws Exception {
    BeanTestUtil.testReadOnlySimpleProperties(new NetInfo(), readOnlyProperties);
  }

}
