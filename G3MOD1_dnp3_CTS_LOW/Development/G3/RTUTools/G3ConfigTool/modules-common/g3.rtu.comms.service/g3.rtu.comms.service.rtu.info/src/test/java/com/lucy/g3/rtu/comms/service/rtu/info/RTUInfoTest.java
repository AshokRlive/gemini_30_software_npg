/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.info;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;

import javax.swing.JLabel;

import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiTask;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.beans.BeanAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.AbstractValueModel;
import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.datalink.DummyDataLink;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class RTUInfoTest {

  private IClient client = new G3RTUClient(new DummyDataLink());

  private RTUInfo rtuInfo;
  private PresentationModel<RTUInfo> pm;
  private BeanAdapter<RTUInfo> adapter;

  private String[] properties = {
      RTUInfo.PROPERTY_SITENAME,
      RTUInfo.PROPERTY_KERNELVERSION,
      RTUInfo.PROPERTY_ROOTFSVERSION,
      RTUInfo.PROPERTY_CONFIG_STATUS,
      RTUInfo.PROPERTY_MCMVERSION,
      RTUInfo.PROPERTY_CPU_SERIAL,
      RTUInfo.PROPERTY_LAST_RESET_CAUSE,
      RTUInfo.PROPERTY_CONFIG_API,
      RTUInfo.PROPERTY_NETWORK_IP0,
      RTUInfo.PROPERTY_NETWORK_IP1
  };


  @Before
  public void setUp() throws Exception {
    rtuInfo = new RTUInfo(client);
    pm = new PresentationModel<RTUInfo>(rtuInfo);
    adapter = new BeanAdapter<RTUInfo>(rtuInfo);
  }

  @After
  public void tearDown() throws Exception {
    rtuInfo = null;
    adapter.release();
    adapter = null;
    pm.release();
    pm = null;
  }

  @Test
  public void testReadBeanProperties() {
    for (String propertie : properties) {
      adapter.getValueModel(propertie).getValue();

    }
  }

  @Test
  public void testReadOnlyBeanProperty0() {
    for (String propertie : properties) {
      try {
        adapter.getValueModel(propertie).setValue(null);
        fail("Exception expected when trying to write a readonly property: " + propertie);
      } catch (Exception e) {
      }
    }
  }

  @Test
  public void testBindReadonlyProperty() {
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {

        JLabel label = new JLabel();
        Object site = rtuInfo.getSiteName();
        // This connect way accepts read only property
        PropertyConnector connector = PropertyConnector.connect(label,
            "text", rtuInfo, RTUInfo.PROPERTY_SITENAME);
        connector.updateProperty1();
        assertEquals(site, label.getText());

        // Change of label should not affect bean
        label.setText("New site name");
        assertNotSame("New site name", rtuInfo.getSiteName());
        assertEquals(site, rtuInfo.getSiteName());
      }
    });
  }

  @Test
  public void testBindReadonlyProperty2() {
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {

        JLabel label = new JLabel();
        AbstractValueModel vm = pm.getModel(RTUInfo.PROPERTY_SITENAME);

        Object site = vm.getValue();
        PropertyConnector.connectAndUpdate(vm, label, "text");
        assertEquals(site, label.getText());

        // Change of label will try to sync rtuBean, it will cause Exception
        // cause rtuBean is read-only.
        try {
          label.setText("New site name");
          assertNotSame("New site name", rtuInfo.getSiteName());
          assertEquals(site, rtuInfo.getSiteName());
          fail("Exception expected");
        } catch (Exception e) {
        }

      }
    });
  }

  @Test
  public void testReadOnlyProperties() {
    BeanTestUtil.testReadOnlyProperties(rtuInfo, properties);
  }

}
