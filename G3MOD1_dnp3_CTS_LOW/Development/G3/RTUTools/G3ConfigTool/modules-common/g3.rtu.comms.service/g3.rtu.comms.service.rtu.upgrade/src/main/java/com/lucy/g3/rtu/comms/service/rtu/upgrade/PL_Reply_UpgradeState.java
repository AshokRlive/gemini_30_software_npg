/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.InvalidPayloadException;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.UPGRADE_STATE;

/**
 * The Class PL_Reply_UpgradeState.
 */
class PL_Reply_UpgradeState extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_UpgradeState;
  private UpgradeStatus status;


  PL_Reply_UpgradeState() {
    super(PAYLOAD_SIZE);
  }

  public UpgradeStatus getState() {
    return status;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    
    ByteBuffer buf = createByteBuffer(payload);
    byte[] status = new byte[3];
    status[0] = buf.get(); // state
    status[1] = buf.get(); // progress
    
    /* Convert state value to enum. */
    UPGRADE_STATE stateEnum = UPGRADE_STATE.forValue(NumUtils.convert2Unsigned(status[0]));
    if (stateEnum == null) {
      throw new InvalidPayloadException("Unrecognized UPGRADE_STATE enum value: " + status[0]);
    }
    
    this.status = new UpgradeStatus(stateEnum, status[1]);
  }
}