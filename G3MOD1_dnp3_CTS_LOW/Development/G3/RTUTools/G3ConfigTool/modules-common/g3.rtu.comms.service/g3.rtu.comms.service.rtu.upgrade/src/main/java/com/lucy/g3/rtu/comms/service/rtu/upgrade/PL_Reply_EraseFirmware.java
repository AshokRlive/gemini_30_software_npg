/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


/**
 * The Class PL_Reply_EraseFirmware.
 */
class PL_Reply_EraseFirmware extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_ModuleRef;
  private ArrayList<ModuleRef> erasedModules;


  PL_Reply_EraseFirmware() {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    erasedModules = new ArrayList<ModuleRef>();
  }

  public ArrayList<ModuleRef> getErasedModules() {
    return erasedModules;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    ByteBuffer buf = createByteBuffer(payload);
    int moduleNum = payload.length / PAYLOAD_SIZE;
    MODULE type;
    MODULE_ID id;
    long serial;
    for (int i = 0; i < moduleNum; i++) {
      serial = NumUtils.convert2Unsigned(buf.getInt());
      type = MODULE.forValue(NumUtils.convert2Unsigned(buf.get()));// MODULE
      id = MODULE_ID.forValue(NumUtils.convert2Unsigned(buf.get()));// MODULE_ID
      if (type != null && id != null) {
        erasedModules.add(new ModuleRef(type, id, serial));
      }
    }
  }
}