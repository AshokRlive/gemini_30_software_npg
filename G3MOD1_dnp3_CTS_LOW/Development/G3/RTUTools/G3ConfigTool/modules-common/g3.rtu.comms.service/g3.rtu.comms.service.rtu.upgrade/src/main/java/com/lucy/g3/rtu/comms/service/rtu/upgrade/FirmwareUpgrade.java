/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_EraseFirmware;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FactoryReset;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetUpgradeState;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SDPVersion;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_UpgradeModule;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_EraseFirmware;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetUpgradeState;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_RTUControl;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Upgrade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.RTUModules;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUControl;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


public class FirmwareUpgrade extends AbstractCommsModule implements UpgradeAPI{
  private final RTUModules rtuModules;
  private final RTUControl rtuControl;
  private final RTUStatus rtuStatus;
  
  /**
   * @param client
   */
  public FirmwareUpgrade(IClient client, RTUStatus rtuStatus, RTUModules rtuModules,RTUControl rtuControl) {
    super(client);
    this.rtuModules = rtuModules;
    this.rtuControl = rtuControl;
    this.rtuStatus = rtuStatus;
  }
  
  @Override
  public Collection<ModuleInfo> cmdGetUpgradeableModules() throws IOException,
      SerializationException {
    List<ModuleInfo> allModules = rtuModules.cmdGetModueInfo();
    ArrayList<ModuleInfo> upgradableModules = new ArrayList<ModuleInfo>();

    for (ModuleInfo m : allModules) {
      if (m != null && m.isPresent()) {
        upgradableModules.add(m);
      }
    }

    return upgradableModules;
  }

  @Override
  public boolean cmdCheckUpgMode() throws IOException, SerializationException {
    CTH_RUNNINGAPP runningapp = cmdCheckAlive();
    return runningapp == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER;
  }

  @Override
  public void cmdEnterUpgradeMode() throws IOException, SerializationException {
    rtuControl.cmdRestart(RestartMode.ENTER_UPGRADE, true);
  }

  @Override
  public void cmdExitUpgradeMode() throws IOException, SerializationException {
    rtuControl.cmdRestart(RestartMode.EXIT_UPGRADE, true);
  }

  @Override
  public void cmdSendSDPVersion(String version) throws IOException,
      SerializationException {
    PL_Send_SDPVersion send = new PL_Send_SDPVersion(version);
    send(CTMsgType_Upgrade    ,
         _CTMsgID_C_SDPVersion,
         _CTMsgID_R_AckNack  ,
         send,
         null);
  }

  /**
   * Sends a command to start upgrading a module. Firmware file has to be
   * transferred before sending this command.
   */
  @Override
  public void cmdUpgradeModule(MODULE type, MODULE_ID id, int serial)
      throws IOException, SerializationException {

    PL_Send_UpgradeModule send = new PL_Send_UpgradeModule(type, id, serial);
    
    send(CTMsgType_Upgrade      ,
         _CTMsgID_C_UpgradeModule,
         _CTMsgID_R_AckNack     ,
      send,
      null);
  }

  /**
   * Sends a command to erase the selected modules.
   */
  @Override
  public ArrayList<ModuleRef> cmdEraseModules(List<ModuleRef> targets)
      throws IOException, SerializationException {

    PL_Send_EraseFirmware send = new PL_Send_EraseFirmware(targets);
    PL_Reply_EraseFirmware reply = new PL_Reply_EraseFirmware();
    
    send(CTMsgType_Upgrade        ,
         _CTMsgID_C_EraseFirmware ,
         _CTMsgID_R_EraseFirmware ,
      send,
      reply);
    
    return reply.getErasedModules();
  }

  /**
   * Gets current upgrade status of RTU.
   */
  @Override
  public UpgradeStatus cmdGetUpradeStatus() throws IOException, SerializationException {

    PL_Reply_UpgradeState reply = new PL_Reply_UpgradeState();
    send(CTMsgType_Upgrade         ,   
         _CTMsgID_C_GetUpgradeState,   
         _CTMsgID_R_GetUpgradeState,   
        null,
        reply);
    
    return reply.getState();
  }
  
  public void cmdFactoryReset() throws IOException, SerializationException {
    send(CTMsgType_RTUControl   ,
         _CTMsgID_C_FactoryReset ,
         _CTMsgID_R_AckNack      ,
        null,
        null);
  }

  @Override
  public CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException {
    return rtuStatus.cmdCheckAlive();
  }

  @Override
  public OLR_STATE cmdGetOLR() throws IOException, SerializationException {
    return rtuStatus.cmdGetOLR();
  }

  public void warmRestart() throws IOException, SerializationException {
    rtuControl.cmdRestart(RestartMode.RESTART, false);
  }

}

