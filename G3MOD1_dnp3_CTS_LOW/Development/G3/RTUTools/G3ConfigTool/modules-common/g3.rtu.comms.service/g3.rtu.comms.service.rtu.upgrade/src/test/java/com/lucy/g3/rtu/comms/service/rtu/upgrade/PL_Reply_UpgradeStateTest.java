/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.comms.protocol.exceptions.InvalidPayloadException;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.PL_Reply_UpgradeState;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeStatus;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.UPGRADE_STATE;

public class PL_Reply_UpgradeStateTest {

  private PL_Reply_UpgradeState fixture;
  @Before
  public void setUp() throws Exception {
    fixture = new PL_Reply_UpgradeState();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParse() throws SerializationException  {
    UPGRADE_STATE[] enums = UPGRADE_STATE.values();

     byte[] rawBytes;
    for (UPGRADE_STATE state : enums) {
      rawBytes = new byte[]{(byte) state.getValue(), (byte) state.getValue()};
      fixture.parseBytes(rawBytes);
      UpgradeStatus status = fixture.getState();
          
      assertEquals("Failed to parse state value:" + state.getValue(), state, status.state);
    }
  }

  @Test(expected = InvalidPayloadException.class)
  public void testParseInvalid() throws SerializationException  {
    byte[] rawBytes = new byte[]{(byte) 100, (byte) 100};
    fixture.parseBytes(rawBytes);
  }
}
