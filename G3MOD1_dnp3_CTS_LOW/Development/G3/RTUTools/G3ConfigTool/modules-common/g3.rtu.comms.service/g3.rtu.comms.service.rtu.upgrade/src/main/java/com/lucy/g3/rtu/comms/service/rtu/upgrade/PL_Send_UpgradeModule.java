/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The Class PL_Send_UpgradeModule.
 */
class PL_Send_UpgradeModule extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_ModuleRef;
  private final MODULE type;
  private final MODULE_ID id;
  private final int serial;


  PL_Send_UpgradeModule(MODULE type, MODULE_ID id, int serial) {
    super(PAYLOAD_SIZE);
    this.type = type;
    this.id = id;
    this.serial = serial;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);
    buf.putInt(serial);
    buf.put((byte) type.getValue());
    buf.put((byte) id.getValue());
    return buf.array();
  }
}