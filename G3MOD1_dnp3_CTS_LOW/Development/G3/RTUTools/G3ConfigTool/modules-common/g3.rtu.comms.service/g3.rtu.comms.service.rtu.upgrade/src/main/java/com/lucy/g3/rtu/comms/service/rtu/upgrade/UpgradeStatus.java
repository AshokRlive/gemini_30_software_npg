/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.UPGRADE_STATE;

/**
 * Wraps the upgrading state of G3RTU.
 */
public final class UpgradeStatus {

  public final UPGRADE_STATE state;
  public final byte progress; // 0 -100


  UpgradeStatus(UPGRADE_STATE state, byte progressValue) {
    this.state = state;
    this.progress = progressValue;
  }

  public boolean hasError() {
    return state != UPGRADE_STATE.UPGRADE_STATE_COMPLETED
        && state != UPGRADE_STATE.UPGRADE_STATE_IDLE
        && state != UPGRADE_STATE.UPGRADE_STATE_UPGRADING;
  }

  public UPGRADE_STATE getStateEnum() {
    return state;
  }

  public boolean isUpgrading() {
    return state == UPGRADE_STATE.UPGRADE_STATE_UPGRADING;
  }

  public boolean isFinshed() {
    return state == UPGRADE_STATE.UPGRADE_STATE_COMPLETED
        || state == UPGRADE_STATE.UPGRADE_STATE_IDLE;
  }
}
