/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

/**
 * The Class PL_Send_EraseFirmware.
 */
class PL_Send_EraseFirmware extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_ModuleRef;
  private final List<ModuleRef> targets;


  PL_Send_EraseFirmware(List<ModuleRef> targets) {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
    this.targets = targets;
    targets.removeAll(Collections.singleton(null)); // Remove null elements
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE * targets.size());
    for (ModuleRef ref : targets) {
      buf.putInt((int) ref.serialNo);
      buf.put((byte) ref.type.getValue());
      buf.put((byte) ref.id.getValue());
    }

    return buf.array();
  }
}