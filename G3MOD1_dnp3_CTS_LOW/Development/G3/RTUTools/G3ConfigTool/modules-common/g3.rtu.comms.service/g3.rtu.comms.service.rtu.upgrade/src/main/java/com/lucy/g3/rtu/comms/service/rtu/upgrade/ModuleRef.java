/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Reference information of a G3 module.
 */
public class ModuleRef {

  public final MODULE type;
  public final MODULE_ID id;
  public final long serialNo;


  public ModuleRef(MODULE type, MODULE_ID id, long serialNo) {
    this.type = type;
    this.id = id;
    this.serialNo = serialNo;
  }

   @Override
   public String toString() {
     return String.format("%s %s", type.getDescription(), id.getDescription());
   }

}
