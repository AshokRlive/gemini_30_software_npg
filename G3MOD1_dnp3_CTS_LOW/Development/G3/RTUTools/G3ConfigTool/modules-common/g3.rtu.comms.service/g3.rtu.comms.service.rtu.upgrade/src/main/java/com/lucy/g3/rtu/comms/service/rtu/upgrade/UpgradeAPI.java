/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This interface defines the command for upgrading.
 */
public interface UpgradeAPI extends ICommsAPI{

  CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException;

  
  UpgradeStatus cmdGetUpradeStatus() throws IOException, SerializationException;

  /**
   * Sends a command to start upgrading a module. Firmware file has to be
   * transferred before sending this command.
   */
  void cmdUpgradeModule(MODULE type, MODULE_ID id, int serial)
      throws IOException, SerializationException;

  /**
   * Sends a command to erase the selected modules.
   */
  ArrayList<ModuleRef> cmdEraseModules(List<ModuleRef> targets)
      throws IOException, SerializationException;

  /**
   * Sends a command to get all modules that ready for upgrade.
   *
   */
  Collection<ModuleInfo> cmdGetUpgradeableModules()
      throws IOException, SerializationException;

  boolean cmdCheckUpgMode() throws IOException, SerializationException;

  void cmdEnterUpgradeMode() throws IOException, SerializationException;

  void cmdExitUpgradeMode() throws IOException, SerializationException;

  void cmdSendSDPVersion(String version) throws IOException, SerializationException;

  OLR_STATE cmdGetOLR() throws IOException, SerializationException;
}
