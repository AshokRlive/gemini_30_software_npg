/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.rtu.comms.service.rtu.upgrade;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

/**
 * The Class PL_Send_SDPVersion.
 */
class PL_Send_SDPVersion extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_SDPVersion;
  private final String version;


  public PL_Send_SDPVersion(String version) {
    super(PayloadType.RAW, PAYLOAD_SIZE);
    this.version = version;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);
    buf.put(version.getBytes());
    return buf.array();
  }
}
