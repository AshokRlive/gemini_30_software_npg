/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.events;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_BeginEventLog;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ClearEventLog;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetEventLog;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetEventLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Log;

import java.io.IOException;
import java.util.Collection;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;


/**
 * The Class RTUEvents.
 */
public class RTUEvents extends AbstractCommsModule implements RTUEventsAPI{

  public RTUEvents(IClient client) {
    super(client);
  }

  @Override
  public Collection<EventEntry> cmdGetLatestEvents()
      throws IOException, SerializationException {

    PL_Reply_LatestEvents reply = new PL_Reply_LatestEvents();
    
    send( CTMsgType_Log        ,
          _CTMsgID_C_GetEventLog,
          _CTMsgID_R_GetEventLog,
        null,
        reply);

    return reply.getEvents();
  }

 
  @Override
  public void cmdEraseEvents() throws IOException, SerializationException {
    send(CTMsgType_Log           ,   
         _CTMsgID_C_ClearEventLog,   
         _CTMsgID_R_AckNack      ,   
        null,
        null);
  }

  
 
  @Override
  public void cmdResetEvent() throws IOException, SerializationException {
    send(CTMsgType_Log           ,   
        _CTMsgID_C_BeginEventLog,   
        _CTMsgID_R_AckNack      ,   
       null,
       null);
  }

}

