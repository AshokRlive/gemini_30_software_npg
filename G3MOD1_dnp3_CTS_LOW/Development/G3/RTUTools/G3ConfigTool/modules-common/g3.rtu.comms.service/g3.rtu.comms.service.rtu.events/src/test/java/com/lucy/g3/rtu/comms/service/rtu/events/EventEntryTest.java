/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.events;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.service.rtu.events.EventEntry;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_CLASS;
import com.lucy.g3.xml.gen.common.EventTypeDef.EVENT_TYPE_S104;

/**
 * The Class EventEntryTest.
 */
public class EventEntryTest {

  private static final int POINT_ID = -123;
  private static final long POINT_ID_UNSIGNED = NumUtils.convert2Unsigned(POINT_ID);
  private static final float NORMALISED_VALUE = 9.87654321F;
  private static final int SCALED_VALUE = -987;

  private final static ByteOrder ORDER = ByteOrder.LITTLE_ENDIAN;


  @Before
  public void setUp() throws Exception {
    Logger.getLogger(EventEntry.class).setLevel(Level.DEBUG);

  }

  private byte[] createS104RawEvent(boolean isScaled) {
    ByteBuffer buf = ByteBuffer.allocate(G3Protocol.PLSize_R_EventEntry);
    buf.order(ORDER);

    // Header
    buf.putInt(0); // seconds
    buf.putInt(0); // nanoseconds
    buf.put((byte) EVENT_CLASS.EVENT_CLASS_S104_POINT.getValue()); // event
                                                                   // class

    // Payload
    if (isScaled) {
      buf.put((byte) EVENT_TYPE_S104.EVENT_TYPE_S104_ANALOGUE_SCALED.getValue()); // Point
                                                                                  // Type
    }
    else {
      buf.put((byte) EVENT_TYPE_S104.EVENT_TYPE_S104_ANALOGUE_NORMAL.getValue()); // Point
                                                                                  // Type
    }
    buf.putInt(POINT_ID);// Point ID
    if (isScaled) {
      buf.putShort((short) SCALED_VALUE); // Point Value
      buf.putShort((short) 0xFFFF); // Point Value
    }
    else {
      buf.putFloat(NORMALISED_VALUE); // Point Value
    }
    buf.put((byte) 0xFF); // status
    return buf.array();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDecodeS104Normalised() {
    EventEntry event = EventEntry.parseBytes(createS104RawEvent(false), ORDER);
    assertNotNull(event);

    assertEquals(EVENT_TYPE_S104.EVENT_TYPE_S104_ANALOGUE_NORMAL, event.getEventType());
    assertEquals(EVENT_CLASS.EVENT_CLASS_S104_POINT, event.getEventClass());
    assertTrue(NORMALISED_VALUE == event.getPointValue().floatValue());
    assertTrue(POINT_ID_UNSIGNED == event.getPointID());

    print(event);
  }

  @Test
  public void testDecodeS104Scaled() {
    EventEntry event = EventEntry.parseBytes(createS104RawEvent(true), ORDER);
    assertNotNull(event);

    assertEquals(EVENT_TYPE_S104.EVENT_TYPE_S104_ANALOGUE_SCALED, event.getEventType());
    assertEquals(EVENT_CLASS.EVENT_CLASS_S104_POINT, event.getEventClass());
    assertTrue(SCALED_VALUE == event.getPointValue().intValue());
    assertTrue(POINT_ID_UNSIGNED == event.getPointID());

    print(event);
  }

  private void print(EventEntry event) {
    System.out.println("============");
    System.out.println("date       : " + event.getFormattedEventText());
    System.out.println("event type : " + event.getEventType());
    System.out.println("status     : " + event.getStatus());
    System.out.println("event class: " + event.getEventClass());
    System.out.println("point id   : " + event.getPointID());
    System.out.println("point value: " + event.getPointValue());
  }

}
