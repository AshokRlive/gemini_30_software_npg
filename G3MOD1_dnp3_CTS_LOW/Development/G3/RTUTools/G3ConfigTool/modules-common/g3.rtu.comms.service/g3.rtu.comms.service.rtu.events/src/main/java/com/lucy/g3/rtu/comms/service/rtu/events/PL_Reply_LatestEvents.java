/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.events;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * The Class PL_Reply_LatestEvents.
 */
class PL_Reply_LatestEvents extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_EventEntry;
  private ArrayList<EventEntry> events = new ArrayList<EventEntry>();


  public PL_Reply_LatestEvents() {
    super(PayloadType.FRAGMENTED, PAYLOAD_SIZE);
  }

  public ArrayList<EventEntry> getEvents() {
    return events;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    ByteBuffer buf = createByteBuffer(payload);
    do {
      byte[] entryBytes = new byte[PAYLOAD_SIZE];
      buf.get(entryBytes);
      EventEntry entry = EventEntry.parseBytes(entryBytes, G3Protocol.RTU_BYTE_ORDER);
      if (entry != null) {
        events.add(0, entry);
      } else {
        log.error("Fail to decode event entry: " + Arrays.toString(entryBytes));
      }

    } while (buf.remaining() >= PAYLOAD_SIZE);
  }
}