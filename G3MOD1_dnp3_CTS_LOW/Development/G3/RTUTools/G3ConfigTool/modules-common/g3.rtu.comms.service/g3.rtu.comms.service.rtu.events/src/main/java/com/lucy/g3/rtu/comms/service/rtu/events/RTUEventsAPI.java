/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.events;

import java.io.IOException;
import java.util.Collection;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;

/**
 * The Interface RTUEventAPI.
 */
public interface RTUEventsAPI extends ICommsAPI {

  /**
   * Reads the latest events of RTU.
   */
  Collection<EventEntry> cmdGetLatestEvents() throws IOException, SerializationException;

  /**
   * Sends command to erase all events.
   */
  void cmdResetEvent() throws IOException, SerializationException;

  /**
   * Resets the offset of event reading to read from the beginning of event
   * list.
   */
  void cmdEraseEvents() throws IOException, SerializationException;
}
