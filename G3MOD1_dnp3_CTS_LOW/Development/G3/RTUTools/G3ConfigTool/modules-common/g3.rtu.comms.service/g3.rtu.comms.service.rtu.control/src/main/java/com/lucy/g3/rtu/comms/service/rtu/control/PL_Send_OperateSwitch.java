/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

  
class PL_Send_OperateSwitch extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_SwitchTest;
  private final short logicID;
  private final SWITCH_OPERATION openClose;
  private final LocalRemoteCode localRemote;


  PL_Send_OperateSwitch(short logicID,
      SWITCH_OPERATION openClose,
      LocalRemoteCode localRemote) {
    super(PAYLOAD_SIZE);
    this.logicID = logicID;
    this.openClose = openClose;
    this.localRemote = localRemote;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);

    buf.putShort(logicID);
    byte opCode = 0x0;
    opCode = (byte) (opCode | (openClose.getValue() << 0));
    opCode = (byte) (opCode | (localRemote.getValue() << 1));
    buf.put(opCode);

    return buf.array();
  }
}
