/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * This interface defines the APIs for restarting RTU.
 */
public interface RestartAPI extends ICommsAPI {

  CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException;

  void cmdRestart(RestartMode mode, boolean force) throws IOException, SerializationException;

}
