/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * This interface defines the APIs for RTU testing.
 */
public interface RTUTestAPI extends ICommsAPI {

  void cmdOperateSwitch(short logicID, SWITCH_OPERATION openClose, LocalRemoteCode localRemote)
      throws IOException, SerializationException;

  void cmdOperateLogic(short logicID, short period)
      throws IOException, SerializationException;

  OLR_STATE cmdGetOLR() throws IOException, SerializationException;

  /**
   * Change the off-local-remote state of RTU.
   */
  void cmdSetOLR(OLR_STATE state) throws IOException, SerializationException;

}
