/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;

/**
 * This interface specifies the APIs to ease/active/restore the
 * configuration of RTU.
 */
public interface ConfigurationAPI extends ICommsAPI {

  /**
   * Sends command to RTU to erase configuration.
   */
  void cmdEraseConfig() throws IOException, SerializationException;

  void cmdActivateConfig() throws IOException, SerializationException;

  void cmdRestoreConfig() throws IOException, SerializationException;
}
