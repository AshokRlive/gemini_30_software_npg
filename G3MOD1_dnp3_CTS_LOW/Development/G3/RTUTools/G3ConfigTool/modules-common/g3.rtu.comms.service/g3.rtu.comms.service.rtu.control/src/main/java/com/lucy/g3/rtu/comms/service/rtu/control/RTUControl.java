/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_ActivateConfig;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_EraseCommissioningCache;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_EraseConfig;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Forced_Reset;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Forced_Restart;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Forced_Shutdown;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_OperateCLogic;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_OperateSwitch;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Register;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Reset;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Restart;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_RestoreConfig;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_Shutdown;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_UpgradeModeOff;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_UpgradeModeOn;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_RTUControl;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Testing;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Upgrade;

import java.io.IOException;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.IG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;


/**
 *
 */
public class RTUControl extends AbstractCommsModule implements RTUControlAPI{
  
  private final RTUStatus rtuStatus;
  
  /**
   * @param client
   */
  public RTUControl(IClient client, RTUStatus rtuStatus) {
    super(client);
    this.rtuStatus = rtuStatus;
  }

  public OLR_STATE cmdGetOLR() throws IOException, SerializationException {
    return rtuStatus.cmdGetOLR();
  }

  public void cmdSetOLR(OLR_STATE state) throws IOException,
      SerializationException {
    rtuStatus.cmdSetOLR(state);
  }
  
  @Override
  public void cmdActivateConfig() throws IOException, SerializationException {
    send(CTMsgType_RTUControl     ,     
         _CTMsgID_C_ActivateConfig,     
         _CTMsgID_R_AckNack       ,     
       null,
       null);
  }

  
  @Override
  public void cmdRestoreConfig() throws IOException, SerializationException {
    send(CTMsgType_RTUControl     ,     
         _CTMsgID_C_RestoreConfig ,     
         _CTMsgID_R_AckNack       ,     
        null,
        null);
  }

  
  @Override
  public void cmdEraseConfig() throws IOException, SerializationException {
    send(CTMsgType_RTUControl  ,     
         _CTMsgID_C_EraseConfig,     
         _CTMsgID_R_AckNack    ,     
       null,
       null);
  }
  
  
  public void cmdEraseCommissioningCache() throws IOException, SerializationException {
    send(CTMsgType_RTUControl               ,     
         _CTMsgID_C_EraseCommissioningCache ,     
         _CTMsgID_R_AckNack                 ,     
       null,
       null);
  }

  // ================== RTURestartCMD Implementation ===================

  
  @Override
  public void cmdRestart(RestartMode mode, boolean forced) throws IOException, SerializationException {
    short msgType;
    short msgID_reply = _CTMsgID_R_AckNack;
    short msgID_send;

    switch (mode) {
    case REBOOT:
      msgType = CTMsgType_RTUControl;
      msgID_send = forced ? _CTMsgID_C_Forced_Reset : _CTMsgID_C_Reset;
      break;

    case RESTART:
      msgType = CTMsgType_RTUControl;
      msgID_send = forced ? _CTMsgID_C_Forced_Restart : _CTMsgID_C_Restart;
      break;

    case SHUT_DOWN:
      msgType = CTMsgType_RTUControl;
      msgID_send = forced ? _CTMsgID_C_Forced_Shutdown : _CTMsgID_C_Shutdown;
      break;

    case ENTER_UPGRADE:
      msgType = CTMsgType_Upgrade;
      msgID_send = _CTMsgID_C_UpgradeModeOn;
      break;

    case EXIT_UPGRADE:
      msgType = CTMsgType_Upgrade;
      msgID_send = _CTMsgID_C_UpgradeModeOff;
      break;

    default:
      msgType = CTMsgType_RTUControl;
      msgID_send = _CTMsgID_C_Restart;
      break;
    }
    
    send(msgType,
        msgID_send,
        msgID_reply,
        null,
        null);
  }

  // ================== CommissioningCMD Implementation ===================

  
  @Override
  public void cmdRegisterModules() throws IOException, SerializationException {
    send(CTMsgType_RTUControl ,
         _CTMsgID_C_Register  ,
         _CTMsgID_R_AckNack   ,
        null,
        null);
  }

  /**
   * Sends command to operate a switch.
   */
  public void cmdOperateSwitch(short logicID,
      SWITCH_OPERATION operation,
      LocalRemoteCode localRemote)
      throws IOException, SerializationException {

    IG3MsgPayload sendPayload = new PL_Send_OperateSwitch(logicID, operation, localRemote);
    IG3MsgPayload replyPayload = null;
    
    send(CTMsgType_Testing,           
        _CTMsgID_C_OperateSwitch, 
        _CTMsgID_R_AckNack,   
        sendPayload,
        replyPayload);
  }

  public void cmdOperateLogic(short logicGroup, short period)
      throws IOException, SerializationException {

    PL_Send_CLogicTest sendPayload = new PL_Send_CLogicTest(logicGroup, period);
    IG3MsgPayload replyPayload = null;

    send(CTMsgType_Testing,
        _CTMsgID_C_OperateCLogic,
        _CTMsgID_R_AckNack,
        sendPayload,
        replyPayload);
  }

  @Override
  public CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException {
    return rtuStatus.cmdCheckAlive();
  }
}

