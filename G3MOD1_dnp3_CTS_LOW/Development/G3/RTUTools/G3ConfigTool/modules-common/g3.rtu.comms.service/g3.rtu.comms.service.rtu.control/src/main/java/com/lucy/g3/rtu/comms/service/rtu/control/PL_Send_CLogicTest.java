/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

class PL_Send_CLogicTest extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_CLogicTest;
  private final short logicID;
  private final short period;


  PL_Send_CLogicTest(short logicID, short period) {
    super(PAYLOAD_SIZE);
    this.logicID = logicID;
    this.period = period;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);

    buf.putShort(logicID);
    buf.putShort(period);

    return buf.array();
  }
}