/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.login;

import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.rtu.comms.shared.LoginResult;


/**
 * The Interface RTULoginAPI.
 */
public interface RTULoginAPI extends ICommsAPI {
  /**
   * Login to RTU by sending user name and password. <br>
   * If success, return the user level for the given name. Otherwise, a NACK
   * exception will be thrown.
   */
  LoginResult cmdLogin(String userName, char[] password)
      throws IOException, SerializationException;
  
}

