/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.login;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_UserLogin;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_UserLogin;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Login;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetMDAlgorithm;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_MDAlgorithm;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.PL_Reply_String;
import com.lucy.g3.rtu.comms.protocol.PL_Request_String;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.security.support.SecurityUtils;


/**
 * The Class RTULogin.
 */
public class RTULogin extends AbstractCommsModule implements RTULoginAPI{
  private static final String DEFAULT_PASSWORD_ALGORITHM = SecurityUtils.MD_ALGORITHM_MD5;
  private Logger log = Logger.getLogger(RTULogin.class);
  public RTULogin(IClient client) {
    super(client);
  }
  
  @Override
  public LoginResult cmdLogin(String userName, char[] password)
      throws IOException, SerializationException {

    final String algorithm = cmdGetMDAlgorithm(userName);
    log.info("Detected password cipher:"+algorithm);
    if(algorithm == null)
      log.warn("Password ciper not detected. User default one: " + DEFAULT_PASSWORD_ALGORITHM);
    
    byte[] pwdHash = SecurityUtils.calculateMD(password, algorithm == null ? DEFAULT_PASSWORD_ALGORITHM : algorithm );
    
    PL_Send_Login send = new PL_Send_Login(userName, pwdHash, algorithm);
    PL_Reply_Login reply = new PL_Reply_Login();
    
    send(CTMsgType_Login,
        _CTMsgID_C_UserLogin,
        _CTMsgID_R_UserLogin,
        send,
        reply,
        0);
    
    getClient().getSession().setSessionID(reply.getLoginResult().sessionID);
    
    return reply.getLoginResult();
  }

  /**
   * @param userName
   * @return null the RTU doesn't support message.
   */
  public String cmdGetMDAlgorithm(String userName) {
    PL_Request_String request = new PL_Request_String(userName);
    PL_Reply_String reply = new PL_Reply_String();
    
    try {
      send(CTMsgType_Login,
          _CTMsgID_C_GetMDAlgorithm,
          _CTMsgID_R_MDAlgorithm,
          request,
          reply,
          0);
      String md = reply.getStr().trim();
      
      if(md == null || md.trim().isEmpty()) {
        md = DEFAULT_PASSWORD_ALGORITHM;
      }
      
      return md;
          
    } catch (IOException | SerializationException e) {
      return null;
    }
  }



}

