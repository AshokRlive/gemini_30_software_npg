/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.login;

import java.nio.ByteBuffer;
import java.util.Arrays;

import com.lucy.g3.network.support.NetworkUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.security.support.SecurityUtils;

/**
 * The Class PL_Send_Login.
 */
class PL_Send_Login extends RequestPayload {
  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_Login; // minimum bytes
  private static final int HASH_BUF_SIZE = 128;
 

  private final byte[] userName;
  private final byte[] passwordHash;
  private final byte[] ipAddress;
  private final byte[] macAddress;

  private final String algorithm;
  
  /**
   * Construct a Login message payload to be sent.
   *
   * @param userName
   *          login user name.
   * @param passwordHash
   *          login password
   * @param algorithm 
   */
  PL_Send_Login(String userName, byte[] passwordHash, String algorithm) {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
    this.algorithm = algorithm;
    
    // Prepare user name bytes
    this.userName = stringToBytes(userName + '\0');
    this.passwordHash = passwordHash;

    // Fill in IP
    byte[] ip = NetworkUtils.getLocalHostIP();
    if (ip != null && ip.length == 4) {
      ipAddress = Arrays.copyOf(ip, 4);
    } else {
      log.warn("IP address of this machine was not found!");
      this.ipAddress = new byte[4];
      Arrays.fill(ipAddress, (byte) 0);
    }

    // Fill in Mac
    byte[] mac = NetworkUtils.getLocalMac();
    if (mac != null && mac.length == 6) {
      this.macAddress = Arrays.copyOf(mac, 6);
    } else {
      log.warn("Mac address of this machine was not found!");
      this.macAddress = new byte[6];
      Arrays.fill(macAddress, (byte) 0);
    }
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(size());

    buf.put(passwordHash);
    
    // Algorithm is null for old version RTUs which only accept MD5 password. 
    if (algorithm != null) {
      // Fill padding bytes
      int paddingSize = HASH_BUF_SIZE - passwordHash.length;
      for (int i = 0; i < paddingSize; i++) {
        buf.put((byte) 0);
      }
      buf.putInt(passwordHash.length);
    }

    buf.put(ipAddress);
    buf.put(macAddress);
    buf.put(userName);

    return buf.array();
  }

  @Override
  public int size() {
    // For supporting old version RTUs which only accept MD5 password. 
    if (algorithm == null)
      return passwordHash.length + ipAddress.length + macAddress.length + userName.length;
    
    return HASH_BUF_SIZE + 4 + ipAddress.length + macAddress.length + userName.length;
  }
}