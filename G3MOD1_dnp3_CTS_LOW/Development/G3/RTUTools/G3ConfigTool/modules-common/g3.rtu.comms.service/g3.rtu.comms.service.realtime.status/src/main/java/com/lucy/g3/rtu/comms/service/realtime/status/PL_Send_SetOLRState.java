/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;

/**
 * The Class PL_Send_SetOLRState.
 */
class PL_Send_SetOLRState extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_SetOLRState;
  private OLR_STATE state;


  PL_Send_SetOLRState(OLR_STATE state) {
    super(PAYLOAD_SIZE);
    if (state == null)
      throw new NullPointerException("The state must not be null");
    this.state = state;
  }

  @Override
  public byte[] toBytes() {
    if (state == null) {
      throw new IllegalStateException("OLR state is not available");
    }
    return new byte[] { (byte) state.getValue() };
  }
}
