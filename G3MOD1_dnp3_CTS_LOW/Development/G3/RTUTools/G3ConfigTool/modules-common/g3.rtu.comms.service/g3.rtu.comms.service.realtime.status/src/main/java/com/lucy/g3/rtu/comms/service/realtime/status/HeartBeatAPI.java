/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * This interface defines the commands for checking RTU alive.
 */
public interface HeartBeatAPI extends ICommsAPI {

  /**
   * Checks if the connected RTU can response a CheckAlive message. If it is
   * alive, the current running application of RTU will be replied.
   */
  CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException;
}
