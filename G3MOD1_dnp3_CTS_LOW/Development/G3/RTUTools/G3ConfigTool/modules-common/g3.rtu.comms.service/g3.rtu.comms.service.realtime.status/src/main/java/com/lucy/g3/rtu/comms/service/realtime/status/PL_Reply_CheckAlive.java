/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * The Class PL_Reply_CheckAlive.
 */
class PL_Reply_CheckAlive extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_CheckAlive;

  private CTH_RUNNINGAPP mode;// RTU running mode
  private long sysUptime; // RTU System uptime
  private long appUptime; // RTU application uptime
  private boolean doorOpen;

  private long byte0;

  @SuppressWarnings("unused")
  private long byte1;
  @SuppressWarnings("unused")
  private long byte2;


  PL_Reply_CheckAlive() {
    super(PAYLOAD_SIZE);
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    ByteBuffer buf = createByteBuffer(payload);

    short modeValue = NumUtils.convert2Unsigned(buf.get());
    mode = CTH_RUNNINGAPP.forValue(modeValue);
    if (mode == null) {
      mode = CTH_RUNNINGAPP.CTH_RUNNINGAPP_UNKNOWN;
    }

    sysUptime = NumUtils.convert2Unsigned(buf.getInt());
    appUptime = NumUtils.convert2Unsigned(buf.getInt());

    byte0 = NumUtils.convert2Unsigned(buf.get());
    byte1 = NumUtils.convert2Unsigned(buf.get());
    byte2 = NumUtils.convert2Unsigned(buf.get());

    if (byte0 > 0) {
      doorOpen = true;
    } else {
      doorOpen = false;
    }

  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);
    buf.put((byte) CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP.getValue());
    buf.putInt((int) sysUptime);
    buf.putInt((int) appUptime);
    return buf.array();
  }

  @Override
  public String toString() {
    return "PL_Reply_CheckAlive [mode=" + mode + ", sysUptime="
        + sysUptime + ", appUptime=" + appUptime + "]";
  }

  public CTH_RUNNINGAPP getMode() {
    return mode;
  }

  public long getSysUptime() {
    return sysUptime;
  }

  public long getAppUptime() {
    return appUptime;
  }

  public boolean isDoorOpen() {
    return doorOpen;
  }

}
