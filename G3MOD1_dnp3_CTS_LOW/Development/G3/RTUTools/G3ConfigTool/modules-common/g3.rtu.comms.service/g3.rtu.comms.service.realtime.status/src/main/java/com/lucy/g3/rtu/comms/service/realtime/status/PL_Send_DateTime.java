/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

/**
 * The Class PL_Send_DateTime.
 */
class PL_Send_DateTime extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_DateTime;
  private final Date date;
  private final boolean timesynched;


  PL_Send_DateTime(Date date) {
    super(PAYLOAD_SIZE);
    if (date == null)
      throw new NullPointerException("date muts not be null");
    this.date = date;
    this.timesynched = false;// Unused field when setting date.
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);

    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(TimeZone.getTimeZone("UTC"));
    cal.setTimeInMillis(date.getTime());

    // Debug
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd 'at' HH:mm:ss z");
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    log.info("Write RTU time: " + sdf.format(cal.getTime()));

    buf.putInt((cal.get(Calendar.SECOND))); // seconds
    buf.putInt((cal.get(Calendar.MINUTE))); // minutes
    buf.putInt((cal.get(Calendar.HOUR_OF_DAY))); // hours
    buf.putInt((cal.get(Calendar.DAY_OF_MONTH))); // day of month
    buf.putInt((cal.get(Calendar.MONTH))); // month
    buf.putInt((cal.get(Calendar.YEAR) - G3Protocol.RTU_YEAR_BEGINNING)); // year
    buf.putInt((cal.get(Calendar.DAY_OF_WEEK) - cal.getFirstDayOfWeek())); // day
    buf.putInt((cal.get(Calendar.DAY_OF_YEAR) - 1));// day since first day of
                                                    // year

    byte attributes = 0; // unused
    if (cal.getTimeZone().useDaylightTime())
    {
      attributes = (byte) (attributes & 0x01); // winter time
    }
    if (cal.getTimeZone().inDaylightTime(date))
    {
      attributes = (byte) (attributes & 0x02); // summer time
    }
    if (timesynched) {
      attributes = (byte) (attributes & 0x04);
    }
    buf.put(attributes);

    buf.put((byte) 0);// Synch source not used.

    return buf.array();
  }
}