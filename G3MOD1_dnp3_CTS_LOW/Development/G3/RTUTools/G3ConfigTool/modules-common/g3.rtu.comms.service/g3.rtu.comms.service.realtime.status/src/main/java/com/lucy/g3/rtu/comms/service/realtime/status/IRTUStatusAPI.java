/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * This interface defines the commands for getting/setting RTU service mode.
 */
public interface IRTUStatusAPI extends ICommsAPI {

  void cmdSetServiceEnabled(boolean enabled, OUTSERVICEREASON reason)
      throws IOException, SerializationException;

  SERVICEMODE cmdGetServiceStatus() throws IOException, SerializationException;
}
