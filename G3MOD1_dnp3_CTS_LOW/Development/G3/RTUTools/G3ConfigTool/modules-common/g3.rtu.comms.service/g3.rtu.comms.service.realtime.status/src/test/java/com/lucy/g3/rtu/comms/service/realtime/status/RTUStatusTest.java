/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.beans.BeanAdapter;
import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.datalink.DummyDataLink;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class RTUStatusTest {

  private IClient client = new G3RTUClient(new DummyDataLink());

  private RTUStatus rtuStatus;
  private PresentationModel<RTUStatus> pm;
  private BeanAdapter<RTUStatus> adapter;

  private String[] properties = {
      RTUStatus.PROPERTY_APP_UPTIME_SEC,
      RTUStatus.PROPERTY_SYS_UPTIME_SEC,
      RTUStatus.PROPERTY_OUTSERVICE_REASON,
      RTUStatus.PROPERTY_SERVICE_MODE,
      RTUStatus.PROPERTY_RTU_TIME,
      RTUStatus.PROPERTY_TIME_SYNCHED,
  };


  @Before
  public void setUp() throws Exception {
    rtuStatus = new RTUStatus(client);
    pm = new PresentationModel<RTUStatus>(rtuStatus);
    adapter = new BeanAdapter<RTUStatus>(rtuStatus);
  }

  @After
  public void tearDown() throws Exception {
    rtuStatus = null;
    adapter.release();
    adapter = null;
    pm.release();
    pm = null;
  }

  @Test
  public void testReadOnlyProperties() {
    BeanTestUtil.testReadOnlyProperties(rtuStatus, properties);
  }

  @Test
  public void testTimeConversion() {
    Date utc = new Date(System.currentTimeMillis());
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(utc.getTime());

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

    System.out.println(sdf.format(utc));
    System.out.println(RTUTimeDecoder.formatEventLog(utc));
    System.out.println(RTUTimeDecoder.formatEventLog(cal.getTime()));
  }
}
