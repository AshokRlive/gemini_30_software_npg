/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;

/**
 * The Class PL_Send_DisableService.
 */
class PL_Send_DisableService extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_DisableService;
  private final OUTSERVICEREASON reason;


  PL_Send_DisableService(OUTSERVICEREASON reason) {
    super(PAYLOAD_SIZE);
    this.reason = reason != null ? reason : OUTSERVICEREASON.OUTSERVICEREASON_UNDEFINED;
  }

  @Override
  public byte[] toBytes() {
    return new byte[] { (byte) reason.getValue() };
  }
}
