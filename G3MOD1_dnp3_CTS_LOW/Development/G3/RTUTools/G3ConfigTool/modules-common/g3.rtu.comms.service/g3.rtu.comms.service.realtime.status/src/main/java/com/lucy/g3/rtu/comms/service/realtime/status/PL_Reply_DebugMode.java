/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * The Class PL_Reply_DebugMode.
 */
class PL_Reply_DebugMode extends ReplyPayload {

  public static final int PAYLOAD_SIZE = 1;
  private boolean debugEnabled;


  PL_Reply_DebugMode() {
    super(PAYLOAD_SIZE);
  }

  public boolean isDebugEnabled() {
    return debugEnabled;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    ByteBuffer buf = createByteBuffer(payload);
    short debugEnabledVal = NumUtils.convert2Unsigned(buf.get());
    debugEnabled = debugEnabledVal > 0;
  }
}