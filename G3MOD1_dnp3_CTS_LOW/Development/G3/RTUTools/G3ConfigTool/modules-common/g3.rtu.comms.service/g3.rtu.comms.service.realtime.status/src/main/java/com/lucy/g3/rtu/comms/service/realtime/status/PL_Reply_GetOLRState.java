/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;

/**
 * The Class PL_Reply_GetOLRState.
 */
class PL_Reply_GetOLRState extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_OLRState;
  private OLR_STATE state;


  PL_Reply_GetOLRState() {
    super(PAYLOAD_SIZE);
  }

  public OLR_STATE getState() {
    return state;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    short olrStateValue = NumUtils.convert2Unsigned(payload[0]);
    state = OLR_STATE.forValue(olrStateValue);
    if (state == null) {
      throw new SerializationException("Unrecognized OLR_STATE enum value: " + olrStateValue);
    }
  }
}
