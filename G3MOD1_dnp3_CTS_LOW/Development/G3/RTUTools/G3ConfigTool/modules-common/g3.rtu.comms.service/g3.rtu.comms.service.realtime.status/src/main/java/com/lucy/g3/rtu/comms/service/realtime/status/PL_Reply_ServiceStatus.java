/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * The Class PL_Reply_ServiceMode.
 */
class PL_Reply_ServiceStatus extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_ServiceState;
  private SERVICEMODE mode;
  private OUTSERVICEREASON reason;
  private boolean autoRunning;
  private boolean autoFailed;
  private boolean autoInhibited;
  private boolean autoDebugging;
  private boolean autoStatusValid;


  PL_Reply_ServiceStatus() {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
  }

  public SERVICEMODE getMode() {
    return mode;
  }

  public OUTSERVICEREASON getReason() {
    return reason;
  }
  
  public boolean isAutoRunning() {
    return autoRunning;
  }
  
  public boolean isAutoFailed() {
    return autoFailed;
  }
  
  public boolean isAutoInhibited() {
    return autoInhibited;
  }
  
  public boolean isAutoDebugging() {
    return autoDebugging;
  }

  public boolean isAutoStatusValid() {
    return autoStatusValid;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    ByteBuffer buf = createByteBuffer(payload);
    short serviceModeValue = NumUtils.convert2Unsigned(buf.get());
    short reasonValue = NumUtils.convert2Unsigned(buf.get());
    
    // Convert service mode to enum
    mode = SERVICEMODE.forValue(serviceModeValue);
    if (mode == null) {
      log.error("Unrecognized service mode  enum value: " + serviceModeValue);
      mode = SERVICEMODE.SERVICEMODE_INVALID;
    }

    reason = OUTSERVICEREASON.forValue(reasonValue);
    if (reason == null) {
      log.error("Unrecognized outServiceReason enum value: " + reasonValue);
      reason = OUTSERVICEREASON.OUTSERVICEREASON_UNDEFINED;
    }
    
    
    /* Parse auto status*/
    byte autoStatus = 0;
    if(buf.hasRemaining()) {
      autoStatus = buf.get();
      autoStatusValid = true;
      autoRunning   = NumUtils.getBitsValue(autoStatus, 0, 0) > 0;
      autoFailed    = NumUtils.getBitsValue(autoStatus, 1, 1) > 0;
      autoInhibited = NumUtils.getBitsValue(autoStatus, 2, 2) > 0;
      autoDebugging = NumUtils.getBitsValue(autoStatus, 3, 3) > 0;
    }
  }
}