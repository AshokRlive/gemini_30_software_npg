/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_CheckAlive;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetDate;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetOLR;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetServiceMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_OnServiceMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_OutServiceMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SetDate;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_SetOLR;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_EnableDebugMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_DisableDebugMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetDebugMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetDebugMode;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_CheckAlive;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetDate;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetOLR;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetServiceMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Date;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Login;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_RTUControl;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Testing;

import java.io.IOException;
import java.util.Date;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.client.Session;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.OUTSERVICEREASON;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * A bean for retrieving and storing all RTU status information.
 */
public class RTUStatus extends AbstractCommsModule implements IRTUStatusAPI, HeartBeatAPI{

  // @formatter:off
  public static final String PROPERTY_SYS_UPTIME_SEC    = "sysUptimeSecs";
  public static final String PROPERTY_APP_UPTIME_SEC    = "appUptimeSecs";
  public static final String PROPERTY_OUTSERVICE_REASON = "outServiceReason";
  public static final String PROPERTY_SERVICE_MODE      = "serviceMode";
  public static final String PROPERTY_RTU_TIME          = "rtuTime";
  public static final String PROPERTY_TIME_SYNCHED      = "timeSynched";
  public static final String PROPERTY_DOOR_OPEN         = "doorOpen";
  public static final String PROPERTY_OLR_STATUS        = "olrStatus";
  public static final String PROPERTY_AUTODEBUGGING   = "autoDebugging";
  public static final String PROPERTY_AUTORUNNING     = "autoRunning";
  public static final String PROPERTY_AUTOFAILED      = "autoFailed";
  public static final String PROPERTY_AUTOINHIBITED   = "autoInhibited";
  public static final String PROPERTY_AUTO_STATUS     = "autoStatus";
  // @formatter:on

  private SERVICEMODE serviceMode;
  
  private OLR_STATE olrStatus;

  private String outServiceReason;

  private Date rtuTime; // Current RTU time
  private Boolean timeSynched; // RTU time is synched

  private Long appUptimeSecs;
  private Long sysUptimeSecs;

  private boolean doorOpen;

  private boolean autoRunning;
  private boolean autoFailed;
  private boolean autoInhibited;
  private boolean autoDebugging; 
  private String  autoStatus;

  public RTUStatus(IClient client) {
    super(client);
  }

  public Date getRtuTime() {
    return rtuTime;
  }

  private void setRtuTime(Date rtuTime) {
    Object oldValue = this.rtuTime;
    this.rtuTime = rtuTime;
    firePropertyChange(PROPERTY_RTU_TIME, oldValue, rtuTime);
  }

  public String getRTUTimeStr() {
    return rtuTime == null ? "" : RTUTimeDecoder.formatRTUTime(rtuTime);
  }

  private void setDoorOpen(boolean doorOpen) {
    Object oldValue = isDoorOpen();
    this.doorOpen = doorOpen;
    firePropertyChange(PROPERTY_DOOR_OPEN, oldValue, doorOpen);
  }

  public boolean isDoorOpen() {
    return doorOpen;
  }

  public SERVICEMODE getServiceMode() {
    return serviceMode;
  }

  
  public boolean isAutoRunning() {
    return autoRunning;
  }

  
  private void setAutoRunning(boolean autoRunning) {
    Object oldValue = this.autoRunning;
    this.autoRunning = autoRunning;
    firePropertyChange(PROPERTY_AUTORUNNING, oldValue, autoRunning);
  }

  
  public boolean isAutoFailed() {
    return autoFailed;
  }

  
  private void setAutoFailed(boolean autoFailed) {
    Object oldValue = this.autoFailed;
    this.autoFailed = autoFailed;
    firePropertyChange(PROPERTY_AUTOFAILED, oldValue, autoFailed);
  }

  
  public boolean isAutoInhibited() {
    return autoInhibited;
  }

  
  private void setAutoInhibited(boolean autoInhibited) {
    Object oldValue = this.autoInhibited;
    this.autoInhibited = autoInhibited;
    firePropertyChange(PROPERTY_AUTOINHIBITED, oldValue, autoInhibited);
  }

  
  public boolean isAutoDebugging() {
    return autoDebugging;
  }

  
  private void setAutoDebugging(boolean autoDebugging) {
    Object oldValue = this.autoDebugging;
    this.autoDebugging = autoDebugging;
    firePropertyChange(PROPERTY_AUTODEBUGGING, oldValue, autoDebugging);
  }

  
  public String getAutoStatus() {
    return autoStatus;
  }

  
  private void setAutoStatus(String autoStatus) {
    Object oldValue = this.autoStatus;
    this.autoStatus = autoStatus;
    firePropertyChange(PROPERTY_AUTO_STATUS, oldValue, autoStatus);
  }

  private void setServiceMode(SERVICEMODE newServiceMode) {
    Object oldValue = getServiceMode();
    this.serviceMode = newServiceMode;

    firePropertyChange(PROPERTY_SERVICE_MODE, oldValue, newServiceMode);

    // Clean reason when it comes back in service
    if (newServiceMode == SERVICEMODE.SERVICEMODE_INSERVICE) {
      setOutServiceReason("");
    }
  }

  public String getOutServiceReason() {
    return outServiceReason;
  }

  private void setOutServiceReason(String outserviceReason) {
    Object oldValue = getOutServiceReason();
    this.outServiceReason = outserviceReason;
    firePropertyChange(PROPERTY_OUTSERVICE_REASON, oldValue,
        outserviceReason);
  }

  public boolean inService() {
    return serviceMode == SERVICEMODE.SERVICEMODE_INSERVICE;
  }

  private void setAppUptimeSecs(Long appUptime) {
    Object oldValue = getAppUptimeSecs();
    appUptimeSecs = appUptime;
    firePropertyChange(PROPERTY_APP_UPTIME_SEC, oldValue, appUptime);
  }

  public Long getAppUptimeSecs() {
    return appUptimeSecs;
  }

  private void setSysUptimeSecs(Long sysUptime) {
    Object oldValue = getSysUptimeSecs();
    sysUptimeSecs = sysUptime;
    firePropertyChange(PROPERTY_SYS_UPTIME_SEC, oldValue, sysUptime);
  }

  public Long getSysUptimeSecs() {
    return sysUptimeSecs;
  }

  public Boolean getTimeSynched() {
    return timeSynched;
  }

  private void setTimeSynched(Boolean timeSynched) {
    Object oldValue = getTimeSynched();
    this.timeSynched = timeSynched;
    firePropertyChange(PROPERTY_TIME_SYNCHED, oldValue, timeSynched);
  }
  
  public OLR_STATE getOlrStatus() {
    return olrStatus;
  }

  private void setOlrStatus(OLR_STATE olrStatus) {
    Object oldValue = this.olrStatus;
    this.olrStatus = olrStatus;
    firePropertyChange(PROPERTY_OLR_STATUS, oldValue, olrStatus);
  }

  /**
   * Checks if the connected RTU can response a CheckAlive message. If it is
   * alive, the current running application of RTU will be replied.
   */
  @Override
  public CTH_RUNNINGAPP cmdCheckAlive() throws IOException, SerializationException {
    try {
      PL_Reply_CheckAlive reply = new PL_Reply_CheckAlive();
      send(CTMsgType_Login,
          _CTMsgID_C_CheckAlive,
          _CTMsgID_R_CheckAlive,
          null,
          reply);
      
    
      setSysUptimeSecs(reply.getSysUptime());
      setAppUptimeSecs(reply.getAppUptime());
      setDoorOpen(reply.isDoorOpen());
      getClient().getSession().setData(Session.SESSSION_DATA_RTU_RUNNING_MODE, reply.getMode());
      return reply.getMode();
      
    }catch(Throwable e) {
      getClient().getSession().setData(Session.SESSSION_DATA_RTU_RUNNING_MODE, null);
      throw e;
    }
  }

  /**
   * Gets the service status from RTU.
   */
  @Override
  public SERVICEMODE cmdGetServiceStatus() throws IOException,
      SerializationException {

    PL_Reply_ServiceStatus reply = new PL_Reply_ServiceStatus();
    
    send(CTMsgType_RTUControl,
        _CTMsgID_C_GetServiceMode,
        _CTMsgID_R_GetServiceMode,
        null,
        reply);
    

    setServiceMode(reply.getMode());
    setOutServiceReason(reply.getReason().getDescription());
    
    setAutoDebugging(reply.isAutoDebugging());
    setAutoFailed(reply.isAutoFailed());
    setAutoInhibited(reply.isAutoInhibited());
    setAutoRunning(reply.isAutoRunning());
    setAutoStatus(composeAutoStatusStr(reply));

    return reply.getMode();
  }

  private static String composeAutoStatusStr(PL_Reply_ServiceStatus reply) {
    if(reply.isAutoStatusValid() == false)
      return "n/a";
    
    String status;
    if(reply.isAutoFailed()) {
      status = "Failed";
    } else if(reply.isAutoInhibited()) {
      // Do not show Inhibited state that is not supported by Auto LED.
      status = "Not Running"; //"Auto Inhibited";
    } else if(reply.isAutoRunning()) {
      status = "Running";
    } else {
      status = "Not Running";
    }
    
//    if(reply.isAutoDebugging()) 
//      status += " (Debug Mode)";
    return status;
  }

  /**
   * Change the service status of RTU.
   */
  @Override
  public void cmdSetServiceEnabled(boolean enabled, OUTSERVICEREASON reason)
      throws IOException, SerializationException {

    // Enable service
    if (enabled) {
      send(CTMsgType_RTUControl,
          _CTMsgID_C_OnServiceMode,
          _CTMsgID_R_AckNack,
          null,
          null);
    
    // Disable service
    } else {
      PL_Send_DisableService send = new PL_Send_DisableService(reason);
      send(CTMsgType_RTUControl,
          _CTMsgID_C_OutServiceMode,
          _CTMsgID_R_AckNack,
          send,
          null);
    }
  }

  /**
   * Get the off-local-remote state of RTU.
   */
  public OLR_STATE cmdGetOLR() throws IOException, SerializationException {

    PL_Reply_GetOLRState reply = new PL_Reply_GetOLRState();
    send(CTMsgType_Testing,
        _CTMsgID_C_GetOLR,
        _CTMsgID_R_GetOLR,
        null,
        reply);
    
    OLR_STATE olr = reply.getState();
    setOlrStatus(olr);
    
    return olr;
  }

  /**
   * Change the off-local-remote state of RTU.
   */
  public void cmdSetOLR(OLR_STATE state) throws IOException,
      SerializationException {

    PL_Send_SetOLRState send = new PL_Send_SetOLRState(state);
    send(CTMsgType_Testing,
         _CTMsgID_C_SetOLR,
         _CTMsgID_R_AckNack,
        send,
        null);
  }

  /**
   * Gets the current time of RTU.
   */
  public Date cmdGetDateTime() throws IOException, SerializationException {

    PL_Reply_DateTime reply = new PL_Reply_DateTime();
    send(CTMsgType_Date    ,
         _CTMsgID_C_GetDate,
         _CTMsgID_R_GetDate,
       null,
       reply);
    
    setRtuTime(reply.getDate());
    setTimeSynched(reply.isTimesynched());

    return reply.getDate();
  }


  /**
   * Set the current time of RTU.
   */
  public boolean cmdSetDateTime(Date newRtuTime)
      throws IOException, SerializationException {

    PL_Send_DateTime send = new PL_Send_DateTime(newRtuTime);
    send(CTMsgType_Date     ,
         _CTMsgID_C_SetDate ,
         _CTMsgID_R_AckNack ,
       send,
       null);
    
    setRtuTime(newRtuTime);

    return true;
  }
  


  public static void clearAll(RTUStatus rtustate) {
    if (rtustate == null) {
      return;
    }

    rtustate.setOutServiceReason(null);
    rtustate.setServiceMode(null);

    rtustate.setRtuTime(null);
    rtustate.setSysUptimeSecs(null);
    rtustate.setAppUptimeSecs(null);
    rtustate.setTimeSynched(null);
    rtustate.setOlrStatus(null);
    rtustate.setAutoDebugging(false);
  }

  public void cmdSetRTUDebugEnabled(boolean enabled) 
      throws IOException, SerializationException {
    send(CTMsgType_RTUControl ,
        enabled?_CTMsgID_C_EnableDebugMode:_CTMsgID_C_DisableDebugMode,
         _CTMsgID_R_AckNack ,
       null,
       null);
    
    setAutoDebugging(enabled);
  }

  public boolean cmdGetRTUDebugState() 
      throws IOException, SerializationException {
    PL_Reply_DebugMode reply = new PL_Reply_DebugMode();
    send(CTMsgType_RTUControl ,
        _CTMsgID_C_GetDebugMode,
        _CTMsgID_R_GetDebugMode ,
       null,
       reply);    
    
    setAutoDebugging(reply.isDebugEnabled());

    return reply.isDebugEnabled();
    
  }

}
