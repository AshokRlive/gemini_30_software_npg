/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.realtime.status;

import java.nio.ByteBuffer;
import java.util.Date;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;

/**
 * The Class PL_Reply_DateTime.
 */
class PL_Reply_DateTime extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_DateTime;
  private Date date;
  private boolean timesynched;


  public PL_Reply_DateTime() {
    super(PAYLOAD_SIZE);
  }

  public Date getDate() {
    return date;
  }

  public boolean isTimesynched() {
    return timesynched;
  }

  @Override
  @SuppressWarnings("unused")
  public void parseBytes(byte[] payload) throws SerializationException {

    ByteBuffer buf = createByteBuffer(payload);
    int sec = buf.getInt();
    int min = buf.getInt();
    int hrs = buf.getInt();
    int mday = buf.getInt();// day of month 1~31
    int month = buf.getInt();
    int year = buf.getInt() + G3Protocol.RTU_YEAR_BEGINNING;
    int weekDay = buf.getInt(); // unused
    int yearDay = buf.getInt(); // unused
    byte attributes = buf.get();
    byte syncource = buf.get();// unused

    int daylistSaving = NumUtils.getBitsValue(attributes, 1, 0);
    timesynched = NumUtils.getBitsValue(attributes, 3, 3) == 1;
    date = RTUTimeDecoder.decode(sec, min, hrs, mday, month, year);

    // Debug
    log.info("Read RTU time: " + RTUTimeDecoder.formatEventLog(date));
  }

}