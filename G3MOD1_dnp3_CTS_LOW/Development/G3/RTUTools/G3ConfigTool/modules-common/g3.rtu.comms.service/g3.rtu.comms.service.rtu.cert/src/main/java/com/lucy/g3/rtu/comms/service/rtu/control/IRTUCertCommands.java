/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import java.io.File;
import java.io.IOException;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 *
 */
public interface IRTUCertCommands {

  String OWNER_HTTPS = "https";
  String OWNER_SDNP3 = "sdnp3";
  String OWNER_S104 = "s104";
  
  String FILE_NAME_CSR ="g3.csr";
  String FILE_NAME_PRIVATE_KEY ="g3.key";
  String FILE_NAME_CERT ="g3.crt";
  String FILE_NAME_LOG =".log";
  String FILE_NAME_STATUS =".status";
  
  int STATUS_SUCCESS = 0;


  /**
   * Sends a command to RTu to install certificate.
   * Certificate file must be written previously.
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @throws IOException
   * @throws SerializationException
   */
  void cmdInstallCert(String owner) throws IOException, SerializationException;

  /**
   * Sends a command to RTU to generate a csr.
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @throws IOException
   * @throws SerializationException
   */
  void cmdGenCSR(String owner) throws IOException, SerializationException;

  /**
   * Sends a command to RTU to generate a new private key
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @throws IOException
   * @throws SerializationException
   */
  void cmdGenPrivateKey(String owner) throws IOException, SerializationException;

  /**
   * Sends a command to RTU to generate self signed certificate.
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @throws IOException
   * @throws SerializationException
   */
  void cmdGenSelfSignedCert(String owner) throws IOException, SerializationException;

  /**
   * 
   * Reads the csr from RTU and save to a destination file.
   * This will block the caller until the file transferring is finished. 
   * @param destPath the path csr will be saved to.
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   */
  void readCSRFile(String owner, String destPath) throws Exception;
  
  /**
   * Read a file in pending folder.
   * @param owner
   * @param fileName
   * @param destPath
   * @throws Exception
   */
  void readPendingFile(String owner, String fileName, String destPath) throws Exception;
  
  /**
   * Writes a certificate file to RTU.
   * This will block the caller until the file transferring is finished.
   * @param sourcePath path of the certificate file to be written. 
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   */
  void writeCertFile(String owner, String sourcePath) throws Exception;
  
  
  /**
   * Gets the running status of certificate commands.
   *
   * @return 0 is success, otherwise failed.
   */
  int getStatus(String owner, int maxRetries)throws IOException, InterruptedException;
  
  /**
   * Gets the running log of certificate commands.
   * @param dest file the log will be saved to.
   */
  void getLog(String owner, File dest) throws IOException ;
  
  /**
   * Clean the certificate logs of RTU.
   * @param dest file the log will be saved to.
   */
  void cmdCleanLog(String owner) throws IOException,SerializationException ;
}
