/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.rtu.control;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.RawG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.filetransfer.IFileTransfer;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol;


public class RTUCertCommands extends AbstractCommsModule implements G3ConfigProtocol, IRTUCertCommands {

  private static final CTH_TRANSFERTYPE CERT_FILE_TYPE = CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC;

  private IFileTransfer fileTransfer;
  
  private Logger log = Logger.getLogger(RTUCertCommands.class);

  public RTUCertCommands(IClient client) {
    this(client, null);
  }

  public RTUCertCommands(IClient client, IFileTransfer fileTransfer) {
    super(client);
    if (fileTransfer == null)
      fileTransfer = new FileTransfer(client, new File("").getParentFile());
    this.fileTransfer = fileTransfer;
  }

  @Override
  public void cmdGenPrivateKey(String owner) throws IOException,
      SerializationException {
    send(CTMsgType_Certificate,
        (short) CTMsgID_Certificate.CTMsgID_C_GenPKey.getValue(),
        _CTMsgID_R_AckNack,
        new RawG3MsgPayload(owner.getBytes()),
        null);
  }

  @Override
  public void cmdGenCSR(String owner) throws IOException,
      SerializationException {
    send(CTMsgType_Certificate,
        (short) CTMsgID_Certificate.CTMsgID_C_GenCSR.getValue(),
        _CTMsgID_R_AckNack,
        new RawG3MsgPayload(owner.getBytes()),
        null);
  }

  @Override
  public void cmdInstallCert(String owner) throws IOException, SerializationException {
    send(CTMsgType_Certificate,
        (short) CTMsgID_Certificate.CTMsgID_C_InstallCert.getValue(),
        _CTMsgID_R_AckNack,
        new RawG3MsgPayload(owner.getBytes()),
        null);
  }

  @Override
  public void cmdGenSelfSignedCert(String owner) throws IOException, SerializationException {
    send(CTMsgType_Certificate,
        (short) CTMsgID_Certificate.CTMsgID_C_GenSelfSigned.getValue(),
        _CTMsgID_R_AckNack,
        new RawG3MsgPayload(owner.getBytes()),
        null);
  }

  @Override
  public void cmdCleanLog(String owner) throws IOException,SerializationException {
    send(CTMsgType_Certificate,
        (short) CTMsgID_Certificate.CTMsgID_C_CleanCertLog.getValue(),
        _CTMsgID_R_AckNack,
        new RawG3MsgPayload(owner.getBytes()),
        null);
  }

  @Override
  public void readCSRFile(String owner, String destPath) throws Exception {
    fileTransfer.readFile(null, CERT_FILE_TYPE,
        getRelativePendingFilePath(owner, FILE_NAME_CSR), destPath);
  }

  @Override
  public void writeCertFile(String owner, String sourcePath) throws Exception {
    fileTransfer.writeFile(null, CERT_FILE_TYPE,
        getRelativePendingFilePath(owner, FILE_NAME_CERT), new File(sourcePath));
  }

  /**
   * Gets the pending file path relative to "upload" folder in RTU.<br>
   * The "upload" folder is the default folder for
   * {@code CTH_TRANSFERTYPE_GENERIC} file type which is used for transferring
   * certificate files.
   * 
   * @param owner
   *          the owner directory that contains the certificate.
   * @param fileName
   *          pending file name
   * @return
   */
  private static String getRelativePendingFilePath(String owner, String fileName) {
    return String.format("../certs/%s/pending/%s", owner, fileName);
  }

  @Override
  public int getStatus(String owner, int maxRetries) throws IOException, InterruptedException{
    if(maxRetries <= 0)
      throw new IllegalArgumentException("maxRetries must be greater than 0");
    
    int status = -1;
    
    File statusFile = File.createTempFile("cert", "status", fileTransfer.getTempDir());
    statusFile.deleteOnExit();
    
    // Try to read status file from RTU.
    int retries = 0;
    while(true) {
      try {
        fileTransfer.readFile(null, CERT_FILE_TYPE,
            getRelativePendingFilePath(owner, FILE_NAME_STATUS),
            statusFile.getAbsolutePath());
        break;
      } catch (Exception e) {
        retries ++;
        if(retries > maxRetries)
          throw new IOException("Failed to read status file from RTU",e);
        Thread.sleep(1000);
      }
    }
    
    
    // Read status value from file
    BufferedReader br = null;
    try { 
      br = new BufferedReader(new FileReader(statusFile));
      String statusStr = br.readLine();
      if(statusStr != null) {
        try{
          status = Integer.valueOf(statusStr);
        }catch(Exception e){
          log.error("Unknown status:" + statusStr);
        } 
      } else {
        log.error("No status found!");
      }
      
    } catch(Exception e){
      throw new IOException("Failed to read status file", e);
      
    } finally {
      try {
        if(br != null)
        br.close();
      } catch (IOException e) {
      }
    }
    
    return status;
  }

  @Override
  public void getLog(String owner, File dest) throws IOException {
    try {
      fileTransfer.readFile(null, CERT_FILE_TYPE,
          getRelativePendingFilePath(owner, FILE_NAME_LOG),
          dest.getAbsolutePath());
    } catch (Exception e) {
      throw new IOException("Failed to read cert log file", e);
    }
  }

  @Override
  public void readPendingFile(String owner, String fileName, String destPath) throws Exception {
    try {
      fileTransfer.readFile(null, CERT_FILE_TYPE,
          getRelativePendingFilePath(owner, fileName),
          destPath);
    } catch (Exception e) {
      throw new IOException("Failed to read pending file", e);
    }
  }

}
