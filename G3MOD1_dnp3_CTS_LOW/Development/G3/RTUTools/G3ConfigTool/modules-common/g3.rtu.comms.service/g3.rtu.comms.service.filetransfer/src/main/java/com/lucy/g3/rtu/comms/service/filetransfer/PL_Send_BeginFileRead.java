/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

class PL_Send_BeginFileRead extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_FileReadStart;
  private final CTH_TRANSFERTYPE type;
  private final String fileName;


  PL_Send_BeginFileRead(CTH_TRANSFERTYPE type, String fileName) {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);

    if (fileName == null)
      throw new NullPointerException("fileName must not be null");

    if (type == null)
      throw new NullPointerException("type must not be null");

    this.fileName = fileName;
    this.type = type;
  }

  @Override
  public byte[] toBytes() {
    byte[] fileNameBytes = stringToBytes(fileName);

    // Get actual payload size
    int actualPayloadSize = PAYLOAD_SIZE;
    actualPayloadSize += fileNameBytes.length;
    actualPayloadSize += 1; // C string terminator

    ByteBuffer buf = createByteBuffer(actualPayloadSize);
    buf.put((byte) type.getValue()); // file type
    buf.put(fileNameBytes); // file name
    buf.put((byte) 0x00); // terminator

    return buf.array();
  }
}
