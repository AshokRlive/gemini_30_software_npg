/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FReadCancel;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FReadFileList;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FReadFragment;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FReadStart;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FWriteFinish;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FWriteFragment;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_FWriteStart;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_C_GetLogFileName;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_FReadFileList;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_FReadFragment;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_FReadStart;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_FWriteFinish;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_GetLogFileName;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_FileRead;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_FileWrite;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Log;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.client.AbstractCommsModule;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.protocol.exceptions.EndOfFileGetException;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * The component that implements the commands for reading/writing files from/to
 * RTU.
 */
public class FileTransfer extends AbstractCommsModule implements IFileTransfer{


  private Logger log = Logger.getLogger(FileTransfer.class);

  /** The temporary file for receiving a file from RTU. */
  private File tempDir; 
  private File tempFile;
  private RandomAccessFile tempFileWriter;

  private static File DEFAULT_DIR; 

  static {
    try {
      DEFAULT_DIR = Files.createTempDirectory(".filetransfer").toFile();
      DEFAULT_DIR.deleteOnExit();
    } catch (IOException e) {
      DEFAULT_DIR = null;
      e.printStackTrace();
    }
  }
  public FileTransfer(IClient client) {
    this(client, null);
  }
  /**
   * Instantiates a new file transfer.
   *
   * @param client
   *          the client
   * @param tempDir
   *          the local dir the location for storing temporary files.
   */
  public FileTransfer(IClient client, File tempDir) {
    super(client);
    this.tempDir = tempDir == null ? DEFAULT_DIR : tempDir;
  }

  void cmdFileWriteStart(int fileSize, int filePermission, int CRC32,
      boolean overwrite, String fileName, CTH_TRANSFERTYPE fileType)
      throws SerializationException, IOException {
    
    PL_Send_BeginFileWrite send = new PL_Send_BeginFileWrite(fileSize,
        filePermission, CRC32, overwrite, fileName, fileType);
    
    send(CTMsgType_FileWrite, 
        _CTMsgID_C_FWriteStart,
        _CTMsgID_R_AckNack    ,
      send,
      null);
  }

  void cmdFileWriteFragment(byte[] fragment, int size, int offset)
      throws SerializationException, IOException {

    PL_Send_WriteFileFrag send = new PL_Send_WriteFileFrag(fragment, size, offset);
    
    send(CTMsgType_FileWrite     , 
        _CTMsgID_C_FWriteFragment,
        _CTMsgID_R_AckNack       ,
        send,
        null);
  }

  FileEndInfo cmdFileWriteFinish(boolean cancel)
      throws SerializationException, IOException {

    PL_Send_WriteFileFinish send = new PL_Send_WriteFileFinish(cancel);
    PL_Reply_WriteFileFinish reply = new PL_Reply_WriteFileFinish();
    
    send(CTMsgType_FileWrite,       
         _CTMsgID_C_FWriteFinish,   
         _CTMsgID_R_FWriteFinish,   
        send,
        reply);

    return reply.getEndInfo();
  }
  /**
   * Get the log file name of RTU.
   */
  public String cmdGetLogFileName() throws IOException, SerializationException {

    PL_Reply_LogFileName reply = new PL_Reply_LogFileName();

    send(CTMsgType_Log            ,       
         _CTMsgID_C_GetLogFileName,   
         _CTMsgID_R_GetLogFileName,   
        null,
        reply);

    return reply.getRTULogFileName();
  }
  
  public String[] cmdGetFileList(String dir) throws IOException, SerializationException {

    PL_Reply_FileList reply = new PL_Reply_FileList();
    PL_Send_FileListDir request = new PL_Send_FileListDir(dir);

    send(CTMsgType_FileRead            ,       
         _CTMsgID_C_FReadFileList,   
         _CTMsgID_R_FReadFileList,   
        request,
        reply);

    return reply.getFileList();
  }
  

  /**
   * The command for starting reading a file from device.
   */

  FileReadInfo cmdFileReadStart(CTH_TRANSFERTYPE type, String fileName)
      throws IOException, SerializationException {

    PL_Send_BeginFileRead request = new PL_Send_BeginFileRead(type, fileName);
    PL_Reply_BeginFileRead reply = new PL_Reply_BeginFileRead();
    
    send(CTMsgType_FileRead,          
         _CTMsgID_C_FReadStart,          
         _CTMsgID_R_FReadStart,          
        request,                                
        reply);                              

    // Close previous temp file.
    if (tempFileWriter != null) {
      try {
        tempFileWriter.close();
      } catch (Exception e) {
        log.error("Fail to close fileWriter");
      }
      tempFileWriter = null;
    }

    // Create a temp file fore receiving the read file.
    tempFile = File.createTempFile(fileName, "", getTempDir());
    tempFile.deleteOnExit();
    tempFileWriter = new RandomAccessFile(tempFile, "rw");

    return reply.getFileReadInfo();
  }

  /**
   * Reading a file fragment.
   */

  long cmdFileReadFragment() throws IOException, SerializationException,
      EndOfFileGetException {

    if (tempFileWriter == null) {
      throw new IOException("File receive not started");
    }

    PL_Reply_FileReadFrag reply = new PL_Reply_FileReadFrag();
    
    send(CTMsgType_FileRead      ,          
         _CTMsgID_C_FReadFragment,          
         _CTMsgID_R_FReadFragment,          
       null,                            
       reply);
    
    if (reply.getFragSize() == 0) {
      /* No fragment has been received */
      tempFileWriter.close();
      tempFileWriter = null;

      throw new EndOfFileGetException("End of FileGet");
    } else {
      /* Write fragment to file */
      tempFileWriter.seek(reply.getOffset());
      tempFileWriter.write(reply.getFrag());
    }

    return reply.getFragSize();
  }

  /**
   * The command for cancelling the current file reading process.
   */

  void cmdFileReadCancel() throws IOException, SerializationException {
    if (tempFileWriter == null) {
      return;
    }

    deleteTempFile();
    

    send(CTMsgType_FileRead   ,          
         _CTMsgID_C_FReadCancel,          
         _CTMsgID_R_AckNack   ,          
        null,                            
        null);            
  }

  /**
   * Delete the temporary file which is for receiving file from device.
   */
  void deleteTempFile() {
    if (tempFileWriter != null) {
      try {
        tempFileWriter.close();
      } catch (IOException e) {
        log.warn("Cannot delete temp file",e);
      }
      tempFileWriter = null;
    }

    if (tempFile != null && tempFile.exists()) {
      if (!tempFile.delete()) {
        log.warn("Cannot delete temp file");
      }
    }
    tempFile = null;
  }

  /**
   * Get received temp file.
   *
   */
  File getTempFile() {
    return tempFile;
  }

  @Override
  public File getTempDir() {
    if (!tempDir.exists()) {
      tempDir.mkdirs();
    }
    
    return tempDir;
  }

  
  @Override
  public void writeFile(ICommsTaskInvoker callback, CTH_TRANSFERTYPE type, String targetPath, File... files) throws Exception {
    FileWritingCommsTask task = new FileWritingCommsTask(callback, this, type, files);
    if(targetPath != null)
      task.setTargetPath(targetPath);
    task.run();
  }

  @Override
  public void readFile(ICommsTaskInvoker callback, CTH_TRANSFERTYPE type, String sourcePath, String targetPath)
      throws Exception {
    new FileReadingCommsTask(callback, this, type, sourcePath, new File(targetPath)).run();
  }
}
