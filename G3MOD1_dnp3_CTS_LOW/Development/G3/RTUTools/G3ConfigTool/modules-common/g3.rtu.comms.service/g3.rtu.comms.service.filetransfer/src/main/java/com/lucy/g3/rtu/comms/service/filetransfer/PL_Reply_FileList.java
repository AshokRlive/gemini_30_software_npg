/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

final class PL_Reply_FileList extends ReplyPayload {

  private String[] fileList;


  public PL_Reply_FileList() {
    super(PayloadType.MINIMUM_SIZE, 0);
  }

  public String[] getFileList() {
    return fileList;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    if (payload != null && payload.length > 0) {
      ByteBuffer buf = createByteBuffer(payload);
      String fileListStr = new String(buf.array());
      this.fileList = fileListStr.split("\\r?\\n");
    }
  }

}