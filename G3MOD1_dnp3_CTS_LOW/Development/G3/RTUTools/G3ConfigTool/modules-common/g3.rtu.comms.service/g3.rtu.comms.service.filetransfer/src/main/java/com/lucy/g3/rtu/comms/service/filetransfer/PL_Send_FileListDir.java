
package com.lucy.g3.rtu.comms.service.filetransfer;

import com.lucy.g3.rtu.comms.protocol.RequestPayload;


public class PL_Send_FileListDir extends RequestPayload {

  private String dir;
  public PL_Send_FileListDir(String dir) {
    super(PayloadType.MINIMUM_SIZE, 0);
    this.dir = dir == null ? "./" : dir;
  }

  @Override
  public byte[] toBytes() {
    return createByteBuffer(dir.getBytes()).array();
  }

}

