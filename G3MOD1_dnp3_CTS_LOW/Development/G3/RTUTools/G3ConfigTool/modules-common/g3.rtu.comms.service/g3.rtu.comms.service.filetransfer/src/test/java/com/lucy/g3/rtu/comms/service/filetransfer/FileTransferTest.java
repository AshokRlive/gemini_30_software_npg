/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.datalink.http.HttpDataLink;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.test.support.CommsTestSupport;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

@Ignore  //This test is only used for development
public class FileTransferTest {

  final private static String HOST = CommsTestSupport.HOST;
  
  
  private FileTransfer fileTransfer;


  @Before
  public void setUp() throws Exception {
    Assume.assumeNotNull(HOST);
    
    HttpDataLink http = new HttpDataLink(HOST);
    IClient client = new G3RTUClient(http);
    
    File folder = new File("target/filetransferTest");
    
    assertNotNull(folder);
    
    fileTransfer = new FileTransfer(client, folder); 
    Logger.getRootLogger().setLevel(Level.DEBUG);
  }

  @After
  public void tearDown() throws Exception {
  }
  
  @Test
  public void testFileWriteStart(int fileSize, int filePermission, boolean overwrite, String fileName,
      CTH_TRANSFERTYPE fileType) throws Exception {
    fileTransfer.cmdFileWriteStart(fileSize, filePermission, 0, overwrite, fileName, fileType);
  }
  
  @Test
  public void testFileWriteFragment(byte[] fragment, int size, int offset) throws Exception {
    fileTransfer.cmdFileWriteFragment(fragment, size, offset);
  }

  @Test
  public void testFileWriteFinish() throws Exception {
    fileTransfer.cmdFileWriteFinish(true);
  }

  @Test
  public void testFileReadStart() throws Exception {
    fileTransfer.cmdFileReadStart(CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_G3CONFIG, "G3Config.xml");

  }

  @Test
  public void testFileReadFragment() throws Exception {
    fileTransfer.cmdFileReadFragment();
  }

  @Test
  public void testFileReadCancel() throws Exception {
    fileTransfer.cmdFileReadCancel();
  }
  
}
