/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

class PL_Reply_WriteFileFinish extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_FileWriteInfo;
  private FileEndInfo endInfo;


  PL_Reply_WriteFileFinish() {
    super(PAYLOAD_SIZE);
  }

  public FileEndInfo getEndInfo() {
    return endInfo;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    ByteBuffer buff = createByteBuffer(payload);
    long receiveFileSize = NumUtils.convert2Unsigned(buff.getInt());
    long actualReceivedSize = NumUtils.convert2Unsigned(buff.getInt());
    long fragmentSize = NumUtils.convert2Unsigned(buff.getInt());

    endInfo = new FileEndInfo(receiveFileSize, actualReceivedSize, fragmentSize);
  }
}
