/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.io.File;

import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

public interface IFileTransfer {

  /**
   * Writes a file to RTU.
   * 
   * @param callback
   *          the functions to be called while transferring file for notifying
   *          the progress, status, etc. Set to null if caller is not interested
   *          in observing the file transferring progress.
   * @param type
   *          the file type to be transferred. Must not be null.
   * @param destPath
   *          the destination path relative to the default location on RTU. If
   *          null, the default destination based on type will be applied.
   * @param files
   *          the files to be written.
   * @throws Exception
   */
  void writeFile(ICommsTaskInvoker callback, CTH_TRANSFERTYPE type, String destPath, File... files) throws Exception;

  /**
   * Reads a file from RTU.
   * 
   * @param callback
   *          the functions to be called while transferring file for notifying
   *          the progress, status, etc. Set to null if caller is not interested
   *          in observing the file transferring progress.
   * @param type
   *          the file type to be transferred. Must not be null.
   * @param sourcePath
   *          the relative file path to the default location for reading file.
   * @param destPath
   *          the destination path for store the read file.
   * @throws Exception
   */
  void readFile(ICommsTaskInvoker callback, CTH_TRANSFERTYPE type, String sourcePath, String destPath) throws Exception;

  File getTempDir();

}
