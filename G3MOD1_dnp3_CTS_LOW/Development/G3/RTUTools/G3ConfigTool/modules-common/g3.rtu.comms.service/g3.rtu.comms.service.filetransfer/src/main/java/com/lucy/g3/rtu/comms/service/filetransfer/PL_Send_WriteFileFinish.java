/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;

class PL_Send_WriteFileFinish extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_FileWriteFinish;
  private final boolean cancel;


  PL_Send_WriteFileFinish(boolean cancel) {
    super(PAYLOAD_SIZE);
    this.cancel = cancel;
  }

  @Override
  public byte[] toBytes() {
    byte flag = (byte) (cancel ? 0x01 : 0x00);

    ByteBuffer buf = createByteBuffer(PAYLOAD_SIZE);
    buf.put(flag);

    return buf.array();
  }
}
