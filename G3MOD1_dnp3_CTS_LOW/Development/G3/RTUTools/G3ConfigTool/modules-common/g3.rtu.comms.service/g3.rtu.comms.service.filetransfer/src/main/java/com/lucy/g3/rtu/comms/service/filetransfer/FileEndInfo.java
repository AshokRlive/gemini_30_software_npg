/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * This class contains the file information required by file transfer.
 */
public final class FileEndInfo {

  private final long fileSize;
  private final long receivedSize;
  private final long receivedFragments;
  private String fileName;
  private CTH_TRANSFERTYPE type;


  FileEndInfo(long fileSize, long receivedSize, long receivedFragments) {
    this.fileSize = fileSize;
    this.receivedSize = receivedSize;
    this.receivedFragments = receivedFragments;
  }

  public long getFileSize() {
    return fileSize;
  }

  public long getReceivedSize() {
    return receivedSize;
  }

  public long getReceivedFragments() {
    return receivedFragments;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setType(CTH_TRANSFERTYPE type) {
    this.type = type;
  }

  public CTH_TRANSFERTYPE getType() {
    return type;
  }

  @Override
  public String toString() {
    return "FileEndInfo [fileSize=" + fileSize + ", receivedSize="
        + receivedSize + ", receivedFragments=" + receivedFragments
        + ", fileName=" + fileName + ", type=" + type + "]";
  }
}
