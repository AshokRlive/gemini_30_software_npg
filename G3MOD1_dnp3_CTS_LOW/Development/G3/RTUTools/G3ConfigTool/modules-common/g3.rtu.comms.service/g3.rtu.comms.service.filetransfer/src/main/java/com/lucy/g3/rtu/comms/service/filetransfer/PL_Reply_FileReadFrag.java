/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.protocol.exceptions.UnexpectedFragmentSizeException;

class PL_Reply_FileReadFrag extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_FileReadFrag;
  private long fragSize;
  private long offset;
  private byte[] frag = new byte[0];


  PL_Reply_FileReadFrag() {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
  }

  public long getFragSize() {
    return fragSize;
  }

  public long getOffset() {
    return offset;
  }

  public byte[] getFrag() {
    return frag;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    ByteBuffer buf = createByteBuffer(payload);
    offset = NumUtils.convert2Unsigned(buf.getInt()); // 0-3 bytes frag offset
    fragSize = NumUtils.convert2Unsigned(buf.getInt()); // 4-7 bytes frag size

    // Read fragment content
    int dataSize = buf.remaining();
    if (dataSize != fragSize) {
      throw new UnexpectedFragmentSizeException("File Read", fragSize, dataSize);
    }

    if (dataSize > 0) {
      frag = new byte[dataSize];
      buf.get(frag);
    }
  }
}
