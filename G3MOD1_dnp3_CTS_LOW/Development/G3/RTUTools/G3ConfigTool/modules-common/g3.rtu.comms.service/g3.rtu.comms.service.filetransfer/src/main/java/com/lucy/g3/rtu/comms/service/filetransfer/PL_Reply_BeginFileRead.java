/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;


class PL_Reply_BeginFileRead extends ReplyPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_R_FileReadInfo;

  private FileReadInfo readInfo;


  PL_Reply_BeginFileRead() {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
  }

  public FileReadInfo getFileReadInfo() {
    return readInfo;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    

    ByteBuffer buffer = createByteBuffer(payload);
    long fileSize = NumUtils.convert2Unsigned(buffer.getInt()); // Read file size
    long tempFileCRC32 = NumUtils.convert2Unsigned(buffer.getInt()); // Read file
    // CRC32

    readInfo = new FileReadInfo(fileSize, tempFileCRC32);
  }
}