/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

/**
 * This class store the information of the file to be read.
 */
public final class FileReadInfo {

  // GEN-BEGIN: turn off checkstyle
  public final long fileSize;
  public final long crc32;
  // GEN-END: turn on checkstyle


  FileReadInfo(long fileSize, long CRC32) {
    this.fileSize = fileSize;
    this.crc32 = CRC32;
  }

}
