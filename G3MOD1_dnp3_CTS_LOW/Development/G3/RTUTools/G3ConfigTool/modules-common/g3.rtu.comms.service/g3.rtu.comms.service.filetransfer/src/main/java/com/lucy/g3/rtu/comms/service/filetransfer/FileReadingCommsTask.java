/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.io.File;
import java.net.ProtocolException;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.client.AbstractCommsTask;
import com.lucy.g3.rtu.comms.protocol.exceptions.EndOfFileGetException;
import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * Read a file from RTU device.
 */
public class FileReadingCommsTask extends AbstractCommsTask{

  protected Logger log = Logger.getLogger(FileReadingCommsTask.class);

  private CTH_TRANSFERTYPE type;

  private String sourceFileName;
  private File destination;
  
  private File result = null;

  private final FileTransfer fileTransfer;


  /**
   * Construct a file reading task to read file from RTU into a destination
   * file.
   *
   * @param app
   *          the application which executes this task.
   * @param type
   *          the type of the file that is read.
   * @param sourceFileName
   *          the name of the file to be read from RTU.
   * @param destination
   *          the destination for saving the file.
   */
  public FileReadingCommsTask(ICommsTaskInvoker invoker, FileTransfer fileTransfer,
      CTH_TRANSFERTYPE type, String sourceFileName,
      File destination) {
    super(invoker);
    if(sourceFileName == null)
      throw new NullPointerException("sourceFileName must not be null");
    if(fileTransfer == null)
      throw new NullPointerException("fileTransfer must not be null");
    
    this.sourceFileName = sourceFileName;
    this.destination = destination;
    this.fileTransfer = fileTransfer;
    this.type = type == null ? CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC : type;

  }
  
  public void setFileTransferType(CTH_TRANSFERTYPE type) {
    if(type == null)
      throw new NullPointerException("type must not be null");
    this.type = type;
  }

  public void setSourceFileName(String sourceFileName) {
    this.sourceFileName = sourceFileName;
  }

  
  public FileTransfer getFileTransfer() {
    return fileTransfer;
  }

  public String getSourceFileName() {
    return sourceFileName;
  }

  public void setDestination(File destination) {
    this.destination = destination;
  }

  public File getDestination() {
    return destination;
  }

  @Override
  public void run() throws Exception {
    int count = 0;

    // Start file reading
    final FileReadInfo readInfo = fileTransfer.cmdFileReadStart(type, sourceFileName);
    log.info(String.format("Start File Reading. File Size:%d CRC:%d",
        readInfo.fileSize, readInfo.crc32));

    if (readInfo.fileSize < 0) {
      throw new ProtocolException("The transferred file is empty");
    }

    /* Reading file fragments */
    while (!isCancelled()) {
      try {
        count += fileTransfer.cmdFileReadFragment();
        int progress = (int) (count * 100 / readInfo.fileSize);
        if (progress > 100) {
          progress = 100;
        }
        setProgress(progress);
        setMessage(String.format("Reading \"%s\"...\nProgress:%s %%", sourceFileName, progress));

      } catch (EndOfFileGetException e1) {
        log.info("File reading completed! " + fileTransfer.getTempFile());
        break;
      }

    }

    if (!isCancelled()) {
      // Get the received file
      File tempFile = fileTransfer.getTempFile();

      if (destination != null) {
        copyFileToDest(tempFile, destination);
      } else {
        destination = tempFile;
      }
    }

    if (isCancelled()) {
      try {
        fileTransfer.cmdFileReadCancel();
      } catch (Exception e) {
        log.error("Failed to send cancel message");
      } 
      return;
    }

    // Check CRC32
    long crc32 = SecurityUtils.calculateCRC32(destination.getAbsolutePath());
    if (crc32 != readInfo.crc32) {
      log.error("File read error. CRC check failed: expected:"
          + readInfo.crc32 + " actual:" + crc32);
    } else {
      log.info("File read CRC check passed. CRC: " + crc32);
    }

    // Succeed to read file.
    result = destination;
    return;
  }
  
  public File getResult() {
    return result;
  }

  private void copyFileToDest(File source, final File dest) throws Exception {
    if (source == null || dest == null) {
      return;
    }

    if (source.exists() == false) {
      return;
    }

    if (dest.exists() && !shouldOverrite(dest)) {
      source.delete();
      return;
    }

    destination.delete(); // Delete before overwriting

    // Write source file to destination
    if (source.renameTo(destination)) {
      source = destination;
    } else {
      throw new Exception("Failed to write file : " + destination);
    }
  }





  
  
}
