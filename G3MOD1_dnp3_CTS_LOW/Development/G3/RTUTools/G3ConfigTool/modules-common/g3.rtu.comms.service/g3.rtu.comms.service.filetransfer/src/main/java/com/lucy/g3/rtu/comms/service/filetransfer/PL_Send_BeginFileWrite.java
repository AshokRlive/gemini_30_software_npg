/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.service.filetransfer;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.RequestPayload;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

class PL_Send_BeginFileWrite extends RequestPayload {

  public static final int PAYLOAD_SIZE = G3Protocol.PLSize_C_FileWriteStart;
  private final int fileSize;
  private final int filePermission;
  private final int CRC32;
  private final boolean overwrite;
  private final String fileName;
  private final CTH_TRANSFERTYPE fileType;


  PL_Send_BeginFileWrite(int fileSize, int filePermission,
      int CRC32, boolean overwrite,
      String fileName, CTH_TRANSFERTYPE fileType) {
    super(PayloadType.MINIMUM_SIZE, PAYLOAD_SIZE);
    if (fileName == null)
      throw new NullPointerException("fileName must not be null");

    if (fileType == null)
      throw new NullPointerException("fileType must not be null");

    this.fileSize = fileSize;
    this.filePermission = filePermission;
    this.CRC32 = CRC32;
    this.overwrite = overwrite;
    this.fileName = fileName;
    this.fileType = fileType;
  }

  @Override
  public byte[] toBytes() {
    // Get overwrite bit
    byte flag;
    if (overwrite) {
      flag = 0x01;
    } else {
      flag = 0x00;
    }

    // Get file name bytes
    byte[] fileNameBytes = stringToBytes(fileName);

    // Calculate payload size
    int actualPayloadSize = PAYLOAD_SIZE;
    actualPayloadSize += fileNameBytes.length;
    actualPayloadSize += 1; // C string terminator

    ByteBuffer buf = createByteBuffer(actualPayloadSize);
    buf.putInt(fileSize); // File Size
    buf.putInt(filePermission); // File permission
    buf.putInt(CRC32); // CRC32
    buf.put(flag); // Overwrite
    buf.put((byte) fileType.getValue()); // File type
    buf.put(fileNameBytes); // File name
    buf.put((byte) 0x00); // C string terminator

    return buf.array();
  }
}
