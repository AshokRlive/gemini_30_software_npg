/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimeUtils {
  public static String convertToDateTimeStr(Date date) {
    if (date == null) {
      return "";
    }

    DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM);
    return formatter.format(date);
  }

  public static String convertDurationFromSecsToDays(Long seconds) {
    if (seconds == null) {
      return "";
    }

    final long days = TimeUnit.SECONDS.toDays(seconds);
    seconds = seconds - TimeUnit.DAYS.toSeconds(days);

    final long hours = TimeUnit.SECONDS.toHours(seconds);
    seconds = seconds - TimeUnit.HOURS.toSeconds(hours);

    final long minutes = TimeUnit.SECONDS.toMinutes(seconds);
    seconds = seconds - TimeUnit.MINUTES.toSeconds(minutes);

    return String.format("%d days %d hours %d minutes %d seconds", days, hours, minutes, seconds);
  }
}

