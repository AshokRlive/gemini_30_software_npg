/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.lucy.g3.common.utils.TimeUtils;

public class TimeUtilsTest {
  @Test
  public void testConvertDurationFromSecsToDays() {
    long seconds = 60;
    String str = TimeUtils.convertDurationFromSecsToDays(seconds);
    assertEquals("0 days 0 hours 1 minutes 0 seconds", str);

    seconds = 70;
    str = TimeUtils.convertDurationFromSecsToDays(seconds);
    assertEquals("0 days 0 hours 1 minutes 10 seconds", str);

    seconds = 19555188;
    str = TimeUtils.convertDurationFromSecsToDays(seconds);
    assertEquals("226 days 7 hours 59 minutes 48 seconds", str);
  }
}

