/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

public class CollectionUtils {
  /**
   * Combines multiple collection to one collection.
   */
  @SafeVarargs
  public static <T> Collection<T> combineCollection(Collection<? extends T>... collections) {
    ArrayList<T> combinedlist = new ArrayList<T>();
    for (int i = 0; i < collections.length; i++) {
      combinedlist.addAll(collections[i]);
    }
    return combinedlist;
  }
  
  public static <T> T[] copy(T[] original) {
    return original == null ? null : Arrays.copyOf(original, original.length);
  }
  
  public static Object getKeyFromValue(Map<?, ?> hm, Object value) {
    for (Object o : hm.keySet()) {
      if (hm.get(o).equals(value)) {
        return o;
      }
    }
    return null;
  }
}

