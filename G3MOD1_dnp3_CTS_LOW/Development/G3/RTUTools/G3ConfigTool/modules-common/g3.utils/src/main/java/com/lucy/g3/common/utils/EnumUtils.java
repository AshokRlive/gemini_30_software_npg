/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import java.util.logging.Level;

public class EnumUtils {
  /**
   * A common method for all enums since they can't have another base class.
   *
   * @param <T>
   *          Enum type
   * @param c
   *          enum type. All enums must be all caps.
   * @param string
   *          case insensitive
   * @return corresponding enum, or null
   */
  public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
    if (c != null && string != null) {
      try {
        return Enum.valueOf(c, string.trim().toUpperCase());
      } catch (Exception ex) {
        java.util.logging.Logger.getLogger(EnumUtils.class.getCanonicalName())
        .log(Level.SEVERE, String.format("Enum \"%s\" not found in class:%s",
            string,
            c.getSimpleName()));
      }
    }
    return null;
  }
  
  public static String getStringFromEnum(Enum<?> e) {
    return e == null ? "" : e.name();
  }

}

