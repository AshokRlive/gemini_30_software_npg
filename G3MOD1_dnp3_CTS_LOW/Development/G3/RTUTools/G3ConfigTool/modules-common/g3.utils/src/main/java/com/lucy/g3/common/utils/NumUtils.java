/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * The Utility Class.
 */
public class NumUtils {
  public static String printBytes(byte[] array) {
    if (array == null || array.length == 0) {
      return  "";
    }

    StringBuilder builder = new StringBuilder(100);
    for (int k = 0; k < array.length; k++) {
      builder.append("0x" + NumUtils.byteToHex(array[k]));

      if (k != array.length - 1) {
        builder.append(",");
      }
    }
    builder.append(" [Size:" + array.length + "] ");

    return builder.toString();
  }
  
  public static int convert2Unsigned(short num) {
    return 0x0000FFFF & num;
  }

  public static short convert2Unsigned(byte num) {
    return (short) (0x00FF & num);
  }

  public static long convert2Unsigned(int num) {
    return 0x0FFFFFFFFL & num;
  }

  /**
   * Get the bit value from one byte and convert to an integer. Begin must be
   * larger or equal than end
   *
   * @param b
   *          the byte that get bit from.
   * @param highBit
   *          High bit
   * @param lowBit
   *          Low bit
   * @return the integer value of selected bits
   */
  public static int getBitsValue(byte b, int highBit, int lowBit) {
    if (highBit > 7 || lowBit < 0 || lowBit > highBit) {
      throw new IllegalArgumentException();
    }

    if (b == 0) {
      return 0;
    } else {
      int and = (2 << highBit) - (1 << lowBit);
      return (b & and) >> lowBit;
    }
  }
  

  /**
   * Get the bit value from one byte and convert to an integer. Begin must be
   * larger or equal than end
   *
   * @param b
   *          the byte that get bit from.
   * @param highBit
   *          High bit
   * @param lowBit
   *          Low bit
   * @return the integer value of selected bits
   */
  public static int getBitsValue(short b, int highBit, int lowBit) {
    if (highBit > 15 || lowBit < 0 || lowBit > highBit) {
      throw new IllegalArgumentException();
    }

    if (b == 0) {
      return 0;
    } else {
      int and = (2 << highBit) - (1 << lowBit);
      return (b & and) >> lowBit;
    }
  }

  public static String byteToHex(byte b) {
    // Returns hex String representation of byte b
    char[] hexDigit = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };
    char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
    return new String(array);
  }

  public static String byteToHex(byte[] bytes) {
    if (bytes == null || bytes.length == 0) {
      return "";
    }

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < bytes.length; i++) {
      sb.append(byteToHex(bytes[i]));
    }

    return sb.toString();
  }

  public static String charToHex(char c) {
    // Returns hex String representation of char c
    byte hi = (byte) (c >>> 8);
    byte lo = (byte) (c & 0xff);
    return byteToHex(hi) + byteToHex(lo);
  }

  public static double round(double value, int places) {
    if (places < 0) {
      throw new IllegalArgumentException();
    }

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
  }
  
  protected NumUtils() {
  }
}
