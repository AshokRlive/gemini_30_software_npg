/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

/**
 * The utility for checking two objects are equal.
 */
public final class EqualsUtil {

  public static boolean areEqual(boolean aThis, boolean aThat) {
    return aThis == aThat;
  }

  public static boolean areEqual(char aThis, char aThat) {
    return aThis == aThat;
  }

  public static boolean areEqual(long aThis, long aThat) {
    /*
     * Implementation Note Note that byte, short, and int are handled by this
     * method, through implicit conversion.
     */
    return aThis == aThat;
  }

  public static boolean areEqual(float aThis, float aThat) {
    return Float.floatToIntBits(aThis) == Float.floatToIntBits(aThat);
  }

  public static boolean areEqual(double aThis, double aThat) {
    return Double.doubleToLongBits(aThis) == Double.doubleToLongBits(aThat);
  }

  /**
   * Possibly-null object field. Includes type-safe enumerations and
   * collections, but does not include arrays. See class comment.
   */
  public static boolean areEqual(Object aThis, Object aThat) {
    return aThis == null ? aThat == null : aThis.equals(aThat);
  }
}
