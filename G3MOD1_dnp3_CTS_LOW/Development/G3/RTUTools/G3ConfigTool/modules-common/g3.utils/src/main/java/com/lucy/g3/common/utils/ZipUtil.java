/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.CheckedOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * The Utility for compressing or decrompressing files.
 */
public final class ZipUtil {

  private ZipUtil() {
  }



  public static boolean isGZipFile(String fileName) {
    return fileName.endsWith(".gz") /*|| fileName.endsWith(".zip")*/;
  }
  
  /**
   * Creates a zip file from a list of sources.
   */
  public static void zip(String targetFilePath, List<File> source) throws IOException {
    FileOutputStream fos = new FileOutputStream(targetFilePath);
    CheckedOutputStream csum = new CheckedOutputStream(fos, new CRC32());
    ZipOutputStream zos = new ZipOutputStream(csum);

    for (int i = 0; i < source.size(); i++) {
      FileInputStream is = new FileInputStream(source.get(i));
      zos.putNextEntry(new ZipEntry(source.get(i).getName()));

      int size;
      byte[] buffer = new byte[1024];

      // Read data to the end of the source file and write it
      // to the zip output stream.
      while ((size = is.read(buffer, 0, buffer.length)) > 0) {
        zos.write(buffer, 0, size);
      }

      zos.closeEntry();
      is.close();
    }
    // Finish zip process
    zos.close();
  }

  /**
   * Create a zip file from a list of sources.
   */
  public static void zip(String targetName, File... source) throws IOException {
    FileOutputStream fos = new FileOutputStream(targetName);
    CheckedOutputStream csum = new CheckedOutputStream(fos, new CRC32());
    ZipOutputStream zos = new ZipOutputStream(csum);

    for (int i = 0; i < source.length; i++) {
      if (source[i] == null) {
        continue;
      }

      FileInputStream is = new FileInputStream(source[i]);
      zos.putNextEntry(new ZipEntry(source[i].getName()));

      int size;
      byte[] buffer = new byte[1024];

      // Read data to the end of the source file and write it
      // to the zip output stream.
      while ((size = is.read(buffer, 0, buffer.length)) > 0) {
        zos.write(buffer, 0, size);
      }

      zos.closeEntry();
      is.close();
    }
    // Finish zip process
    zos.close();
  }

  /**
   * Copies entries in the source ZIP file to target zipped file.
   *
   * @param source
   *          ZIP file to be copied
   * @param target
   *          ZIP file to be generated
   * @param entries
   *          selected entries to be copied in the source
   * @return success or fail
   */
  public static boolean copyZip(File source, File target, String... entries) throws IOException {
    boolean result = false;
    // These are the files to include in the target ZIP file
    String[] filenames = entries;

    // Create a buffer for reading the files
    byte[] buf = new byte[1024];

    ZipOutputStream out = null;
    ZipInputStream in = null;

    // Create the target ZIP file
    out = new ZipOutputStream(new FileOutputStream(target));
    in = new ZipInputStream(new FileInputStream(source));

    ZipEntry zipentry = in.getNextEntry();
    
    int num;
    // search entry by comparing name
    while (zipentry != null) {
      for (int i = 0; i < filenames.length; i++) {
        if (zipentry.getName().equals(filenames[i])) {
          out.putNextEntry(new ZipEntry(filenames[i]));
          while ((num = in.read(buf, 0, 1024)) > -1) {
            out.write(buf, 0, num);
          }
          out.closeEntry();
          result = true;
        }
      }
      in.closeEntry();
      zipentry = in.getNextEntry();
    }// while

    // Complete the ZIP file
    if (in != null) {
      in.close();
    }

    if (out != null) {
      out.close();
    }

    return result;
  }

  /**
   * Gets file stream in the opened ZIP file by specifying file name.
   *
   * @param entryName
   *          the entry name in the ZIP
   * @return ByteArrayOutputStream or {@code null} if file is not found
   */
  public static byte[] getZipEntryStreamByName(String entryName, File zipFile) throws IOException {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CRC32 checksum = new CRC32();
    CheckedInputStream checkedIn = new CheckedInputStream(new FileInputStream(zipFile), checksum);
    ZipInputStream zipinputstream = new ZipInputStream(checkedIn);

    byte[] buf = new byte[1024];
    ZipEntry zipentry = zipinputstream.getNextEntry();

    // search entry by comparing name
    while (zipentry != null) {
      String en = zipentry.getName();
      // entry found
      if (!zipentry.isDirectory() && entryName.equalsIgnoreCase(en)) {
        int num;
        // copy file stream
        CheckedOutputStream checkedOut = new CheckedOutputStream(outputStream, checksum);
        while ((num = zipinputstream.read(buf, 0, 1024)) > -1) {
          checkedOut.write(buf, 0, num);
        }
        checkedOut.close();
        break;
      }
      zipinputstream.closeEntry();
      zipentry = zipinputstream.getNextEntry();
    }// while

    zipinputstream.close();
    outputStream.close();
    return outputStream.toByteArray();
  }

  public static ZipEntry[] getZipEntries(File zipFile) {
    if (zipFile == null) {
      return null;
    }

    ArrayList<ZipEntry> entrylist = new ArrayList<ZipEntry>();
    try {
      ZipInputStream zipinputstream = null;
      ZipEntry zipentry;
      zipinputstream = new ZipInputStream(new FileInputStream(zipFile));

      zipentry = zipinputstream.getNextEntry();

      while (zipentry != null) {
        entrylist.add(zipentry);
        zipinputstream.closeEntry();
        zipentry = zipinputstream.getNextEntry();
      }// while

      zipinputstream.closeEntry();
      zipinputstream.close();
      return entrylist.toArray(new ZipEntry[entrylist.size()]);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

  }

  public static ZipEntry getZipEntryByName(String entryName, File zipFile) {
    if (zipFile == null || entryName == null) {
      return null;
    }

    try {
      ZipInputStream zipinputstream = null;
      ZipEntry zipentry;
      zipinputstream = new ZipInputStream(new FileInputStream(zipFile));

      zipentry = zipinputstream.getNextEntry();

      while (zipentry != null) {
        String ename = zipentry.getName();
        if (ename.equals(entryName)) {
          break;
        }
        zipinputstream.closeEntry();
        zipentry = zipinputstream.getNextEntry();
      }// while

      zipinputstream.closeEntry();
      zipinputstream.close();
      return zipentry;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * Unzips a selected file from a zip file.
   */
  public static boolean unzip(String zipFile, String selectFile, String destination) throws IOException {
    boolean success = false;
    byte[] buf = new byte[1024];
    ZipInputStream zipinputstream = null;
    ZipEntry zipentry;
    zipinputstream = new ZipInputStream(new FileInputStream(zipFile));

    zipentry = zipinputstream.getNextEntry();

    if (zipentry != null) {
      new File(destination).mkdirs();
    }

    while (zipentry != null) {
      // for each entry to be extracted
      String entryName = zipentry.getName();

      if (entryName.equalsIgnoreCase(selectFile)) {
        //log.debug("Found entry: " + entryName);
        int num;
        FileOutputStream fileoutputstream;
        File newFile = new File(destination, entryName);
        String dir = newFile.getParent();

        if (dir == null) {
          if (newFile.isDirectory()) {
            continue;
          }
        }

        fileoutputstream = new FileOutputStream(new File(destination, entryName));

        while ((num = zipinputstream.read(buf, 0, 1024)) > -1) {
          fileoutputstream.write(buf, 0, num);
        }

        fileoutputstream.close();
        zipinputstream.closeEntry();
        success = true;
      }
      zipentry = zipinputstream.getNextEntry();
    }// while

    zipinputstream.close();

    return success;
  }

  /**
   * Unzips the given zip file.
   */
  public static void unzip(String zipFile, String destination) throws IOException {
    byte[] buf = new byte[1024];
    ZipInputStream zipinputstream = null;
    ZipEntry zipentry;
    zipinputstream = new ZipInputStream(new FileInputStream(zipFile));

    zipentry = zipinputstream.getNextEntry();

    if (zipentry != null) {
      new File(destination).mkdirs();
    }

    while (zipentry != null) {
      // for each entry to be extracted
      String entryName = zipentry.getName();
      //log.debug("Entryname " + entryName);
      int num;
      FileOutputStream fileoutputstream;
      File newFile = new File(destination, entryName);
      String dir = newFile.getParent();

      if (dir == null) {
        if (newFile.isDirectory()) {
          continue;
        }
      }

      fileoutputstream = new FileOutputStream(new File(destination, entryName));

      while ((num = zipinputstream.read(buf, 0, 1024)) > -1) {
        fileoutputstream.write(buf, 0, num);
      }

      fileoutputstream.close();
      zipinputstream.closeEntry();
      zipentry = zipinputstream.getNextEntry();

    }// while

    zipinputstream.close();
  }

  /**
   * Unzip the compressed file with GZIP and output to a destination file.
   */
  public static void ungzip(String gzipFile, String dest) throws IOException {
    // Open the compressed file
    GZIPInputStream in = new GZIPInputStream(new FileInputStream(gzipFile));

    // Open the output file
    OutputStream out = new FileOutputStream(dest);

    // Transfer bytes from the compressed file to the output file
    byte[] buf = new byte[1024];
    int len;
    while ((len = in.read(buf)) > 0) {
      out.write(buf, 0, len);
    }

    // Close the file and stream
    in.close();
    out.close();
  }

  /**
   * Unzip the compressed file with GZIP and output to a binary array.
   */
  public static byte[] ungzip(String source) throws IOException {
    // Open the compressed file
    GZIPInputStream in = new GZIPInputStream(new FileInputStream(source));

    // Open the output file
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    // Transfer bytes from the compressed file to the output file
    byte[] buf = new byte[1024];
    int len;
    while ((len = in.read(buf)) > 0) {
      out.write(buf, 0, len);
    }

    byte[] ret = out.toByteArray();

    // Close the file and stream
    in.close();
    out.close();

    return ret;
  }

  public static void gzip(final String sourceFile, final String gzipFile) throws IOException {
    gzip(sourceFile, gzipFile, true);
  }
  
  /**
   * Compress a file with GZIP.
   *
   * @param sourceFile
   *          the file to be compressed.
   * @return the generated compressed target file
   */
  public static void gzip(final String sourceFile, final String gzipFile, boolean deleteAfterZip) throws IOException {
    if(sourceFile == null)
      throw new IllegalArgumentException("sourceFile must not be null");
    if(gzipFile == null)
      throw new IllegalArgumentException("gzipFile must not be null");

    // Create the GZIP output stream
    GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(gzipFile));

    // Open the input file
    FileInputStream in = new FileInputStream(sourceFile);

    // Transfer bytes from the input file to the GZIP output stream
    byte[] buf = new byte[1024];
    int len;
    while ((len = in.read(buf)) > 0) {
      out.write(buf, 0, len);
    }
    in.close();
    out.finish();
    out.close();

    // Delete the source file after compressing.
    if(deleteAfterZip)
      new File(sourceFile).delete();
  }

  public static HashMap<String, String> parseScriptZip(File scriptZipFile) throws Exception {
    HashMap<String, String> scriptMap = new HashMap<String, String>();

    CRC32 checksum = new CRC32();
    CheckedInputStream checkedIn = new CheckedInputStream(new FileInputStream(scriptZipFile), checksum);
    ZipInputStream zipinputstream = new ZipInputStream(checkedIn);

    byte[] buf = new byte[1024];
    ZipEntry zipentry = zipinputstream.getNextEntry();

    // search entry by comparing name
    while (zipentry != null) {
      if (!zipentry.isDirectory()) {
        String entryName = zipentry.getName();

        // write entry into stream
        int num;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CheckedOutputStream checkedOut = new CheckedOutputStream(out, checksum);
        while ((num = zipinputstream.read(buf, 0, 1024)) > -1) {
          checkedOut.write(buf, 0, num);
        }
        checkedOut.close();

        scriptMap.put(entryName, out.toString());
      }

      // Next entry
      zipinputstream.closeEntry();
      zipentry = zipinputstream.getNextEntry();
    }

    zipinputstream.close();
    return scriptMap;
  }
  
  
  


  private static final int BUFFER_SIZE = 1024;

  /**
   * Create a zip file from a list of sources (including path).
   * 
   * @param targetFilePath
   *          Destination path
   * @param source
   *          List of files in a Map data structure, where key indicates the
   *          path by name and directory
   * @throws IOException if fails to read/write file.
   */
  public static void zip(String targetFilePath, Map<String, File> source) throws IOException {
    FileOutputStream fos = new FileOutputStream(targetFilePath);
    CheckedOutputStream csum = new CheckedOutputStream(fos, new CRC32());
    ZipOutputStream zos = new ZipOutputStream(csum);

    for (String path : source.keySet()) {
      FileInputStream is = new FileInputStream(source.get(path));
      zos.putNextEntry(new ZipEntry(path));

      int size;
      byte[] buffer = new byte[1024];

      // Read data to the end of the source file and write it
      // to the zip output stream.
      while ((size = is.read(buffer, 0, buffer.length)) > 0) {
        zos.write(buffer, 0, size);
      }

      zos.closeEntry();
      is.close();
    }
    // Finish zip process
    zos.close();
  }

  public static List<File> unzip(File zipFile, String targetDirectory) throws IOException {
    if (targetDirectory == null || targetDirectory.isEmpty())
      targetDirectory = System.getProperty("user.dir");
    File targetDir = new File(targetDirectory);
    if (!targetDir.exists()) {
      targetDir.mkdir();
    }
    BufferedOutputStream dest = null;
    FileInputStream sourceFileStream = null;
    ZipInputStream sourceZipStream = null;
    List<File> files = new ArrayList<File>();

    try {
      sourceFileStream = new FileInputStream(zipFile);
      sourceZipStream = new ZipInputStream(new BufferedInputStream(sourceFileStream));
      ZipEntry entry;
      while ((entry = sourceZipStream.getNextEntry()) != null) {
        File newFile = new File(targetDir, entry.getName());
        files.add(newFile);
        // folder
        if (entry.isDirectory()) {
          newFile.mkdirs();
        } else {
          int count;
          byte [] data = new byte[BUFFER_SIZE];
          // extract and write the file to target destination
          newFile.getParentFile().mkdirs();
          OutputStream out = new FileOutputStream(newFile);
          dest = new BufferedOutputStream(out, BUFFER_SIZE);
          while ((count = sourceZipStream.read(data, 0, BUFFER_SIZE)) != -1) {
            dest.write(data, 0, count);
          }
          dest.flush();
          dest.close();
          out.close();
        }
      }
    } finally {
      if (sourceZipStream != null)
        sourceZipStream.close();

      if (sourceFileStream != null)
        sourceFileStream.close();
    }
    return files;

  }
  // public static String extractZipComment(String filename) throws Exception{
  // String retStr = null;
  // File file = new File(filename);
  // int fileLen = (int) file.length();
  // FileInputStream in = new FileInputStream(file);
  // /*
  // * The whole ZIP comment (including the magic byte sequence) MUST
  // * fit in the buffer otherwise, the comment will not be recognized
  // * correctly
  // *
  // * You can safely increase the buffer size if you like
  // */
  // byte[] buffer = new byte[Math.min(fileLen, 8192)];
  // int len;
  // in.skip(fileLen - buffer.length);
  // if ((len = in.read(buffer)) > 0) {
  // retStr = getZipCommentFromBuffer(buffer, len);
  // }
  // in.close();
  //
  // return retStr;
  // }
  //
  // private static String getZipCommentFromBuffer(byte[] buffer, int len) {
  // byte[] magicDirEnd = { 0x50, 0x4b, 0x05, 0x06 };
  // int buffLen = Math.min(buffer.length, len);
  // // Check the buffer from the end
  // for (int i = buffLen - magicDirEnd.length - 22; i >= 0; i--) {
  // boolean isMagicStart = true;
  // for (int k = 0; k < magicDirEnd.length; k++) {
  // if (buffer[i + k] != magicDirEnd[k]) {
  // isMagicStart = false;
  // break;
  // }
  // }
  // if (isMagicStart) {
  // // Magic Start found!
  // int commentLen = buffer[i + 20] + buffer[i + 21] * 256;
  // int realLen = buffLen - i - 22;
  // System.out.println("ZIP comment found at buffer position " + (i + 22) + "
  // with len=" + commentLen
  // + ", good!");
  // if (commentLen != realLen) {
  // System.out.println("WARNING! ZIP comment size mismatch: directory says len
  // is " + commentLen
  // + ", but file ends after " + realLen + " bytes!");
  // }
  // String comment = new String(buffer, i + 22, Math.min(commentLen, realLen));
  // return comment;
  // }
  // }
  // System.out.println("ERROR! ZIP comment NOT found!");
  // return null;
  // }

}
