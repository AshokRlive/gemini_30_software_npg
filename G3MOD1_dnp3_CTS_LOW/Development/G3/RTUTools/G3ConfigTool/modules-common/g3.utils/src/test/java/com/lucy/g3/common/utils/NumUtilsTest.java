/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.lucy.g3.common.utils.NumUtils;

public class NumUtilsTest {

  @Test
  public void testGetBitValue() {
    byte a = -128; // 1000 0000 b
    byte b = -64; // 1100 0000 b
    int bitValue;

    bitValue = NumUtils.getBitsValue(a, 7, 7);
    assertEquals(1, bitValue);

    bitValue = NumUtils.getBitsValue(a, 7, 6);
    assertEquals(2, bitValue);

    bitValue = NumUtils.getBitsValue(a, 7, 5);
    assertEquals(4, bitValue);

    bitValue = NumUtils.getBitsValue(b, 7, 6);
    assertEquals(3, bitValue);

    bitValue = NumUtils.getBitsValue(b, 6, 6);
    assertEquals(1, bitValue);

    bitValue = NumUtils.getBitsValue(b, 7, 5);
    assertEquals(6, bitValue);
  }

  @Test
  public void testGetBitValue2() {
    short a = -128; // 1000 0000 b
    short b = -64; // 1100 0000 b
    short c = Short.MIN_VALUE;
    short d = (short) 0xFF00;
    int bitValue;

    bitValue = NumUtils.getBitsValue(a, 7, 7);
    assertEquals(1, bitValue);

    bitValue = NumUtils.getBitsValue(a, 7, 6);
    assertEquals(2, bitValue);

    bitValue = NumUtils.getBitsValue(a, 7, 5);
    assertEquals(4, bitValue);

    bitValue = NumUtils.getBitsValue(b, 7, 6);
    assertEquals(3, bitValue);

    bitValue = NumUtils.getBitsValue(b, 6, 6);
    assertEquals(1, bitValue);

    bitValue = NumUtils.getBitsValue(b, 7, 5);
    assertEquals(6, bitValue);

    bitValue = NumUtils.getBitsValue(c, 15, 15);
    assertEquals(1, bitValue);

    for (int i = 0; i < 15; i++) {
      bitValue = NumUtils.getBitsValue(c, i, i);
      assertEquals(0, bitValue);
    }

    for (int i = 0; i < 8; i++) {
      bitValue = NumUtils.getBitsValue(d, i, i);
      assertEquals(0, bitValue);
    }

    for (int i = 8; i < 15; i++) {
      bitValue = NumUtils.getBitsValue(d, i, i);
      assertEquals(1, bitValue);
    }
  }

  @Test
  public void testCovertInt2Unsigned() {
    int[] samples = new int[] { -1, 0, 1, -100, 100, Integer.MAX_VALUE, Integer.MIN_VALUE };

    for (int i = 0; i < samples.length; i++) {
      long result = NumUtils.convert2Unsigned(samples[i]);

      assertTrue("The conversion result is supposed to be unsigned. Actual: " + result, result >= 0);

      if (samples[i] >= 0) {
        assertEquals("The result should be the same when " +
            "convert an postive number to unsigned", samples[i], result);
      }
    }
  }

  @Test
  public void testCovertShort2Unsigned() {
    short[] samples = new short[] { -1, 0, 1, -100, 100, Short.MAX_VALUE, Short.MIN_VALUE };
    for (int i = 0; i < samples.length; i++) {
      int result = NumUtils.convert2Unsigned(samples[i]);

      assertTrue("The conversion result is supposed to be unsigned. Actual: " + result, result >= 0);

      if (samples[i] >= 0) {
        assertEquals("The result should be the same when " +
            "convert an postive number to unsigned", samples[i], result);
      }
    }
  }

  @Test
  public void testCovertByte2Unsigned() {
    byte[] samples = new byte[] { -1, 0, 1, -100, 100, Byte.MAX_VALUE, Byte.MIN_VALUE };
    for (int i = 0; i < samples.length; i++) {
      short result = NumUtils.convert2Unsigned(samples[i]);

      assertTrue("The conversion result is supposed to be unsigned. Actual: " + result, result >= 0);

      if (samples[i] >= 0) {
        assertEquals("The result should be the same when " +
            "convert an postive number to unsigned", samples[i], result);
      }

    }
  }
  
  
  @Test
  public void testRound() {
    double source = 1.00000000000009;

    double target = NumUtils.round(source, 2);

    assertEquals(target, 1.00, 3);
  }
  
  
}
