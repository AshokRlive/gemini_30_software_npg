/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.utils;

import java.io.File;
import java.net.URL;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.common.utils.ZipUtil;

public class ZipUtilitiesTest {

  final static URL SAMPLE_DIALLING_SCRIPT = ZipUtilitiesTest.class.getClassLoader().getResource("sampleDialScript.zip");


  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Ignore
  @Test
  public void testParseScriptZip() throws Exception {
    HashMap<String, String> map = ZipUtil.parseScriptZip(new File(SAMPLE_DIALLING_SCRIPT.toURI()));
    Object[] keys = map.keySet().toArray();
    for (int i = 0; i < keys.length; i++) {
      System.out.println("key: " + keys[i] + " Value: " + map.get(keys[i]));
    }
  }

}
