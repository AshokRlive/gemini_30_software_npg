/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.test.support;


/**
 * For supporting comms unit test. 
 */
public class CommsTestSupport {
  
  /**
   * The IP address of RTU for testing communications API.
   * Set this to null to skip all communication API test. 
   */
  public final static String HOST = null;//"10.11.11.102";
  public final static boolean TEST_NORMAL_API = true;
  public final static boolean TEST_UPDATE_API = false;

  
  public final static boolean TEST_SET_OLR = false;
  
  /**
   * The logic id of FAN test. Set to -1 if not available.
   */
  public final static int TEST_LOGIC_GROUP_FAN = -1;
  
  /**
   * The logic id of switch logic. Set to -1 if not available.
   */
  public final static int TEST_LOGIC_GROUP_SWITCH= -1;
  
}

