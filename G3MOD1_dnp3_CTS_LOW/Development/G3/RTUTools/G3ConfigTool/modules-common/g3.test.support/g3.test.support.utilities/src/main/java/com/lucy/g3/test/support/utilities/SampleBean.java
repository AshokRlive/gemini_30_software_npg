/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.test.support.utilities;

import com.jgoodies.binding.beans.Model;

public class SampleBean extends Model {

  private long longValue;
  private long intValue;


  public long getLongValue() {
    return longValue;
  }

  public void setLongValue(long longValue) {
    Object oldValue = getLongValue();
    this.longValue = longValue;
    firePropertyChange("longValue", oldValue, longValue);
  }

  public long getIntValue() {
    return intValue;
  }

  public void setIntValue(long intValue) {
    Object oldValue = getIntValue();
    this.intValue = intValue;
    firePropertyChange("intValue", oldValue, intValue);
  }

}
