/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.test.support.utilities;

import static org.junit.Assert.fail;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.value.AbstractValueModel;

/**
 *
 *
 */
public class BeanTestUtil {

  protected BeanTestUtil() {
  }

  /**
   * Test Read-only bound property.
   *
   * @param bean
   * @param readOnlyProperties
   */
  public static <T> void testReadOnlyProperties(T bean, String[] readOnlyProperties) {
    if (readOnlyProperties == null) {
      return;
    }
    PresentationModel<T> model = new PresentationModel<T>(bean);

    for (int i = 0; i < readOnlyProperties.length; i++) {
      AbstractValueModel vm = model.getModel(readOnlyProperties[i]);
      Object value = vm.getValue();

      try {
        vm.getValue();
      } catch (Exception e) {
        fail("Fal to read bean property: " + readOnlyProperties[i]);
      }

      try {
        vm.setValue(value);
        fail("Exception expected when trying to write a readonly property: "
            + readOnlyProperties[i]);
      } catch (Exception e) {
      }
    }
  }

  public static <T> void testReadWriteProperties(T bean, String[] readWriteProperties) {
    if (readWriteProperties == null) {
      return;
    }
    PresentationModel<T> model = new PresentationModel<T>(bean);

    for (int i = 0; i < readWriteProperties.length; i++) {
      AbstractValueModel vm = model.getModel(readWriteProperties[i]);
      try {
        Object value = vm.getValue();
        vm.setValue(value);
      } catch (Exception e) {
        fail(e.getMessage());
      }
    }
  }

  public static <T> void testWriteOnlyProperties(T bean, String[] writeOnlyProperties) {
    if (writeOnlyProperties == null) {
      return;
    }
    PresentationModel<T> model = new PresentationModel<T>(bean);

    for (int i = 0; i < writeOnlyProperties.length; i++) {
      AbstractValueModel vm = model.getModel(writeOnlyProperties[i]);
      try {
        vm.getValue();
        fail("Exception expected when trying to write a readonly property: "
            + writeOnlyProperties[i]);
      } catch (Exception e) {
      }

      try {
        vm.setValue(null);
      } catch (Exception e) {
        fail("Fal to write bean property: " + writeOnlyProperties[i]);
      }
    }
  }

  /**
   * Test simple property that doesn't fire change event.
   *
   * @param bean
   * @param readOnlyProperties
   */
  public static <T> void testReadOnlySimpleProperties(T bean, String[] readOnlyProperties)
      throws Exception {
    // TODO test bean accessor

  }


  public static class VetoAllListener implements VetoableChangeListener {

    /*
     * public void vetoableChange(PropertyChangeEvent event) throws
     * PropertyVetoException { if (!isUndo(event)) if
     * (event.getPropertyName().equals("value")) if ((Integer)
     * event.getNewValue() < 0) throw new PropertyVetoException("I object!",
     * event); } private static boolean isUndo(PropertyChangeEvent event) { try
     * { return event.getNewValue().equals(getCurrentValue(event)); } catch
     * (Exception exception) { return false; } } private static Object
     * getCurrentValue(PropertyChangeEvent event) throws Exception { Object bean
     * = event.getSource(); String name = event.getPropertyName(); BeanInfo info
     * = Introspector.getBeanInfo(bean.getClass()); for (PropertyDescriptor pd :
     * info.getPropertyDescriptors()) { if (pd.getName().equals(name)) { return
     * pd.getReadMethod().invoke(bean); } } throw new
     * IllegalArgumentException(name); }
     */
    @Override
    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
      throw new PropertyVetoException("I object!", event);
    }
  }
}
