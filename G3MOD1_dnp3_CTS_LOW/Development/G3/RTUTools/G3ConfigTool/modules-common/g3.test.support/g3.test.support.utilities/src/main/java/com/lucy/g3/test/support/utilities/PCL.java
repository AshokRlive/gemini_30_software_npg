/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.test.support.utilities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class PCL implements PropertyChangeListener {

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
  }
}
