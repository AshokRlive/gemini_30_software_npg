/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.test.support.utilities;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.InputStream;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.junit.Assert;

public class TestUtil extends BeanTestUtil {

  public static JFrame showFrame(JComponent comp) {
    return showFrame("frame", comp);
  }

  public static JFrame showFrame(String frameName, JComponent comp) {
    JFrame frame = createFrame(frameName, comp);
    frame.pack();
    frame.setVisible(true);
    return frame;
  }

  public static JFrame createFrame(String frameName, JComponent comp) {
    final JFrame frame = new JFrame();
    frame.setName(frameName);
    JPanel content = new JPanel(new BorderLayout());
    content.add(comp, BorderLayout.CENTER);
    frame.setTitle("GUI Test");
    frame.setContentPane(content);
    frame.setMinimumSize(new Dimension(300, 300));
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    return frame;
  }

  public static void showDialog(JDialog dialog) {
    dialog.pack();
    dialog.setVisible(true);
  }

  /**
   * Test if a *.properties file exists in resource folder.
   *
   * @param c
   */
  public static void testResourceFileExist(Class<?> c) {
    testResourceFileExist(c, null);
  }

  /**
   * Test if a *.properties file exists in resource folder.
   *
   * @param c
   * @param propertyFileName
   */
  public static void testResourceFileExist(Class<?> c, String propertyFileName) {
    if (propertyFileName == null) {
      propertyFileName = c.getSimpleName();
    }

    if (!propertyFileName.endsWith(".properties")) {
      propertyFileName += ".properties";
    }

    propertyFileName = "resources\\" + propertyFileName;

    InputStream res = c.getResourceAsStream(propertyFileName);
    if (res == null) {
      String path = c.getPackage().getName().replace(".", "\\");
      Assert.fail(String.format("Property file is missing! \n\"%s\\%s\" ",
          path,
          propertyFileName));
    }
  }

  /**
   * Checks if all property resource file exists.
   *
   * @param resources
   *          2-D array of resources to be tested. The first element is class
   *          type, the second is property file name which could be null.
   */
  public static void testAllResourceFilesExist(Object[][] resources) {
    for (int i = 0; i < resources.length; i++) {
      Class<?> c = (Class<?>) resources[i][0];
      String fileName = (String) resources[i][1];
      testResourceFileExist(c, fileName);
    }
  }
}
