/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package g3.common.test.resources;

import java.io.File;

import org.junit.Test;

import com.lucy.g3.test.support.resources.TestResources;

import org.junit.Assert;


public class TestResourcesTest {

  @Test
  public void testFileExist() {
    checkFileExists(TestResources.getSampleSDPFile()); 
    checkFileExists(TestResources.getSampleSDPBootloaderFile());
  }
  
  private void checkFileExists(File file) {
    Assert.assertNotNull(file);
    Assert.assertTrue("file not exists:"+file,file.exists());
  }

}

