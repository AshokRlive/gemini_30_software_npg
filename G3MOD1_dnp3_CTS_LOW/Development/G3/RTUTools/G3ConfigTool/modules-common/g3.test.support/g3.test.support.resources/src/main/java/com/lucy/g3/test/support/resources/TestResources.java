/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.test.support.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TestResources {
  private final static String CONFIG = "config/ukpn.xml"; 
  private final static String CONFIG_COMPRESSED = "config/ukpn.gz"; 
  private final static String SDP = "sdp/Sample_SDP.zip"; 
  private final static String SDPBOOT = "sdp/Sample_SDP_Bootloader.zip";
  
  public static File getFile(String relativePath)  {
    ClassLoader cl = TestResources.class.getClassLoader();
    InputStream is = cl.getResourceAsStream(relativePath);
    File outFile = new File("target/"+relativePath);
    outFile.getParentFile().mkdirs();
    if(outFile.exists())
      return outFile;
    
    FileOutputStream out;
    System.out.println("Created resource:" + outFile.getAbsolutePath());
    try {
      out = new FileOutputStream(outFile);
      int read = 0;
      byte[] bytes = new byte[1024];
  
      while ((read = is.read(bytes)) != -1) {
        out.write(bytes, 0, read);
      }
      
      out.close();
      
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
    
    return outFile;
  }
  
  public static File getSampleConfigFile() {
    return getFile(CONFIG);
  }
  
  public static File getSampleCompressedConfigFile() {
    return getFile(CONFIG_COMPRESSED);
  }
  
  public static File getSampleSDPFile() {
    return getFile(SDP);
  }
  
  public static File getSampleSDPBootloaderFile() {
    return getFile(SDPBOOT);
  }
  
  public static String getSampleSDP() {
    return getFile(SDP).getAbsolutePath();
  }
  
  public static String getSampleSDPBootloader() {
    return getFile(SDPBOOT).getAbsolutePath();
  }
  
}
