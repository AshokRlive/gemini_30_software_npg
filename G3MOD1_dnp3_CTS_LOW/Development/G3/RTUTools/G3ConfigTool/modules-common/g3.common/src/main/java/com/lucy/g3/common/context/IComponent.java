
package com.lucy.g3.common.context;


public interface IComponent {
  String getUniqueIdentifier();
  
  IContext getContext();

}

