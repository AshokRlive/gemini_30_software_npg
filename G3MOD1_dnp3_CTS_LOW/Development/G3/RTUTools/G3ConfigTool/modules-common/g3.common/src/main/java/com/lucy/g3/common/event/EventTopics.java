/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.event;


/**
 * Event topics definitions.
 */
public interface EventTopics {
  String  EVT_TOPIC_CONN_STATE_CHG    = "event.topic.connection.state.change";
  String  EVT_PROPERTY_OLD_STATE      = "event.property.old.state";
  String  EVT_PROPERTY_NEW_STATE      = "event.property.new.state";
  //String  EVT_PROPERTY_SELECTED_RTU   = "event.property.selected.rtu";
  
  String  EVT_TOPIC_LOGIN_ACCOUNT_CHG = "event.topic.login.account.change";
  String  EVT_PROPERTY_LOGIN_ACCOUNT  = "event.property.login.account";
  String  EVT_PROPERTY_LOGIN_RESULT   = "event.property.login.result";
  
  String  EVT_TOPIC_SERVICE_MODE_CHG     = "event.topic.service.mode.change";
  String  EVT_PROPERTY_SERVICE_MODE      = "event.property.service.mode";
  String  EVT_PROPERTY_OUTSERVICE_REASON = "event.property.outservice.reason";
  
  String  EVT_TOPIC_DEBUG_MODE_CHG     = "event.topic.debug.mode.change";
  
  String  EVT_TOPIC_RESTART_RTU         = "event.topics.restartRTU";
  String  EVT_PROPERTY_RESTART_MODE     = "event.property.restartMode"; // RestartMode
  String  EVT_PROPERTY_IP_ADDRESS       = "event.property.ipaddress";   // String
}

