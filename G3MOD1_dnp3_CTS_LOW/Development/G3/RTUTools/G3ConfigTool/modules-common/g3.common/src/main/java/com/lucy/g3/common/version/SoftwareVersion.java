/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.version;


/**
 * Software Version.
 */
public class SoftwareVersion extends BasicVersion {

  private final ReleaseType relType;

  private final long patch;


  public SoftwareVersion(ReleaseType relType, long major, long minor, long patch) {
    super(major, minor);
    if (patch < 0) {
      throw new IllegalArgumentException("Invalid patch number:" + patch);
    }

    if (relType == null) {
      throw new IllegalArgumentException("Invalid release type::" + relType);
    }

    this.patch = patch;
    this.relType = relType;
  }
  
//  public SoftwareVersion(short release, long major, long minor, long patch) {
//    this(VERSION_TYPE.forValue(release), major, minor, patch);
//  }
  
  public ReleaseType relType() {
    return relType;
  }

  public long patch() {
    return patch;
  }

  @Override
  public String toString() {
    return relType.getDescription() + "-" + major() + "." + minor() + "." + patch;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj instanceof SoftwareVersion) {
      SoftwareVersion objVer = (SoftwareVersion) obj;
      return relType .equals(objVer.relType)
          && patch == objVer.patch
          && major() == objVer.major()
          && minor() == objVer.minor();
    }
    return super.equals(obj);
  }
  
  @Override
  public int hashCode() {
    assert false : "hashCode not designed";
    return 42; // any arbitrary constant will do
  }

}
