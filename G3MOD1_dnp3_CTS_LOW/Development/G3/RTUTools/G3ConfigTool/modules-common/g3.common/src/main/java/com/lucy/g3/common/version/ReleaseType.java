
package com.lucy.g3.common.version;


public class ReleaseType {
  private  final String description; 
  private  final int value;
  
  public ReleaseType(String description, int value) {
    super();
    this.description = description;
    this.value = value;
  }

  
  public String getDescription() {
    return description;
  }

  
  public int getValue() {
    return value;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + value;
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ReleaseType other = (ReleaseType) obj;
    if (value != other.value)
      return false;
    return true;
  }
  
}

