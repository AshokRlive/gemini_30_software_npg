/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.version;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The Class BasicVersion.
 */
public class BasicVersion {

  public static final String PATTERN = "\\d+\\.\\d+";

  private final long major;
  private final long minor;


  public BasicVersion(long major, long minor) {
    if (major < 0 || minor < 0) {
      throw new IllegalArgumentException("Invalid version. Major: " + major + " Minor: " + minor);
    }

    this.major = major;
    this.minor = minor;
  }

  
  public long major() {
    return major;
  }

  
  public long minor() {
    return minor;
  }

  @Override
  public String toString() {
    return major + "." + minor;
  }

  @Override
  public int hashCode() {
    final long prime = 31;
    long result = 1;
    result = prime * result + major;
    result = prime * result + minor;
    return (int)result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BasicVersion other = (BasicVersion) obj;
    if (major != other.major)
      return false;
    if (minor != other.minor)
      return false;
    return true;
  }

  /**
   * Create an instance provided that a parameter has the correct format.<br>
   * Otherwise handle errors accordingly.<br>
   * Format: Major.Minor (values are integers)
   * 
   * @param majorMinor
   *          - String
   * @return new {@link BasicVersion} instance
   * @throws IllegalArgumentException
   *           when invalid format is provided
   */
  public static BasicVersion createFromString(String majorMinor) {
    if(majorMinor == null || majorMinor.trim().isEmpty())
      throw new IllegalArgumentException("majorMinor must not be blank");
    
    majorMinor = majorMinor.trim();

    if (!majorMinor.matches(PATTERN)) {
      throw new IllegalArgumentException("Parse error: expecting major.minor format but actual:" + majorMinor);
    }

    long major = 0;
    long minor = 0;
    Pattern numberPtn = Pattern.compile("\\d+");
    Matcher matcher = numberPtn.matcher(majorMinor);
    if (matcher.find())
      major = Long.valueOf(matcher.group());
    if (matcher.find())
      minor = Long.valueOf(matcher.group());
    return new BasicVersion(major, minor);
  }

}
