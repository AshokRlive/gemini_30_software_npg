
package com.lucy.g3.common.context;

public interface IContext {
  String COMMS_TASK_SERVICE_NAME = "Comms Task Service";
  
  //TODO destroy?
  
  void addComponent(IComponent module) throws ConflictedComponentIdException;

  void removeComponent(IComponent module);

  void removeComponent(String moduleId);

  <T extends IComponent> T getComponent(String componentId) throws ComponentNotFoundException;

  IComponent[] getAllComponents();


  /**
   * This exception will be thrown if a component' id conflicts with others.
   */
  public class ConflictedComponentIdException extends RuntimeException {

    public ConflictedComponentIdException(String message) {
      super(message);
    }
  }

  /**
   * This exception will be thrown if a component is not found.
   */
  public class ComponentNotFoundException extends RuntimeException {

    public ComponentNotFoundException(String message) {
      super(message);
    }

  }
}
