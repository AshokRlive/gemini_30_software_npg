
package com.lucy.g3.common.context;

import java.util.Collection;
import java.util.HashMap;
import java.util.WeakHashMap;

public abstract class AbstractContext implements IContext {

  private final WeakHashMap<String, IComponent> comps = new WeakHashMap<>();


  @Override
  public void addComponent(IComponent module) throws ConflictedComponentIdException {
    if (module == null)
      throw new NullPointerException("module must not be null");

    if (comps.containsKey(module.getUniqueIdentifier())) {
      throw new ConflictedComponentIdException("Module already exists: " + module.getUniqueIdentifier());
    }

    comps.put(module.getUniqueIdentifier(), module);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends IComponent> T getComponent(String moduleId) {
    IComponent sub = comps.get(moduleId);
    if (sub == null) {
      throw new ComponentNotFoundException("Module not found: " + moduleId);
    }
    return (T) sub;
  }

  @Override
  public void removeComponent(IComponent module) {
    comps.remove(module.getContext());
  }

  @Override
  public void removeComponent(String moduleId) {
    comps.remove(comps.get(moduleId));
  }

  @Override
  public IComponent[] getAllComponents() {
    Collection<IComponent> modules = comps.values();
    return modules.toArray(new IComponent[modules.size()]);
  }
  
}
