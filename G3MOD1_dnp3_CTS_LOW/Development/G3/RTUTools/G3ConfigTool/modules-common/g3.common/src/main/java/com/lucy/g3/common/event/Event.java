/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 *
 */
public class Event {

  private final HashMap<String, Object> properties;
  
  private final String topic;
  
  public Event(String topic, Map<String, ?> properties) {
    this.topic = topic;
    this.properties = new HashMap<String, Object>(properties);
  }
  
  public Object getEventData(){
    if(properties.containsKey(EventConstants.EVENT_DATA))
      return properties.get(EventConstants.EVENT_DATA);
    return properties;
  }
  
  public String[] getPropertyNames() {
    Set<String> keys = properties.keySet();
    return keys.toArray(new String[keys.size()]);
  }
  
  public Object getProperty(String name) {
    return properties.get(name);
  }
  
  public String getTopic() {
    return topic;
  }
}

