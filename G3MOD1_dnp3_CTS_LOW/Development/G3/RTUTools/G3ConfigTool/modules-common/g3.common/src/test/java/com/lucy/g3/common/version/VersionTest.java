/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.version;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SystemAPIVersion;

public class VersionTest {

  @Before
  public void setUp() throws Exception {

  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testFeatureVersionCompare() {
    FeatureVersion f0 = new FeatureVersion(1, 2);
    FeatureVersion f1 = new FeatureVersion(1, 2);
    FeatureVersion f2 = new FeatureVersion(1, 3);
    assertTrue(f0.equals(f0));
    assertTrue(f1.equals(f1));
    assertTrue(f0.equals(f1));
    assertFalse(f0.equals(f2));
    assertFalse(f0.equals(null));
  }

  @Test
  public void testAPIVersionCompare() {
    SystemAPIVersion f0 = new SystemAPIVersion(1, 2);
    SystemAPIVersion f1 = new SystemAPIVersion(1, 2);
    SystemAPIVersion f2 = new SystemAPIVersion(1, 3);
    assertTrue(f0.equals(f0));
    assertTrue(f1.equals(f1));
    assertTrue(f0.equals(f1));
    assertFalse(f0.equals(f2));
    assertFalse(f0.equals(null));
  }

}
