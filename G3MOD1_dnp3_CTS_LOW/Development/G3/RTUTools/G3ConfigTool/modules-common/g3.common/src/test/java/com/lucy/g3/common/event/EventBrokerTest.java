/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.event;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.IEventHandler;


/**
 *
 */
public class EventBrokerTest {
  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    String topicA = "topicA";
    String topicB = "topicB";
    Handler handler = new Handler();
    EventBroker.getInstance().subscribe(topicA, handler);
    
    Assert.assertFalse(handler.handled);
    
    EventBroker.getInstance().send(topicB, null);
    Assert.assertFalse(handler.handled);
    
    EventBroker.getInstance().send(topicA, null);
    Assert.assertTrue(handler.handled);
  }


  private static class Handler implements IEventHandler {
    private boolean handled = false;
    @Override
    public void handleEvent(Event event) {
      handled = true;
    }
  }

}

