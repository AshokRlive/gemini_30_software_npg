/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.version;

import org.apache.log4j.Logger;

/**
 * System API Version.
 */
public class SystemAPIVersion extends BasicVersion {

  private Logger log = Logger.getLogger(SystemAPIVersion.class);


  public SystemAPIVersion(long major, long minor) {
    super(major, minor);

    if (major < 0) {
      log.warn("Negative major number:" + major);
    }
    if (minor < 0) {
      log.warn("Negative minor number:" + minor);
    }
  }

  public SystemAPIVersion(SystemAPIVersion systemAPI) {
    this(systemAPI.major(), systemAPI.minor());
  }

  public boolean isCompatible(SystemAPIVersion api) {
    if (api == null) {
      return false;
    }

    return major() == api.major() && minor() <= api.minor();
  }
}
