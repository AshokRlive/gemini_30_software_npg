/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.version;


/**
 * Feature Version.
 */
public class FeatureVersion extends BasicVersion {

  public FeatureVersion(long major, long minor) {
    super(major, minor);
  }
  
  public FeatureVersion(FeatureVersion feature) {
    this(feature.major(), feature.minor());
  }
}
