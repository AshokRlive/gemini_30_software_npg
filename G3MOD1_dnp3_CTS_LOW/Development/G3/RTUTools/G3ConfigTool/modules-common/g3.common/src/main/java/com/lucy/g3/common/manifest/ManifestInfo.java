/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.manifest;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;


public class ManifestInfo {
  private static final String DEFAULT_MANIFEST = "META-INF/MANIFEST.MF";

  private String version = "Unknown";
  
  private String appName;
  
  private final LinkedHashMap<String, String> propertiesMap 
    = new LinkedHashMap<String, String>();
  
  public ManifestInfo() {
    this("");
  }

  public ManifestInfo(String appName) {
    this.appName = appName;
    readFromManifest();
  }
  
  public String getAppName() {
    return appName;
  }

  public void addProperties(Map<String, String> features) {
    if(features != null)
      propertiesMap.putAll(features);
  }
  
  public String getFormattedText() {
    StringBuilder sb = new StringBuilder();
    sb.append(appName);
    
    String[][] feautres = getProperties();
    for (int i = 0; i < feautres.length; i++) {
      sb.append("\n");
      sb.append(feautres[i][0]);
      sb.append(": ");
      sb.append(feautres[i][1] == null ? "Unknown" : feautres[i][1]);
    }
    
    return sb.toString();
  }
  
  /**
   * Read default manifest in the jar.
   * @return true if success.
   */
  public boolean readFromManifest() {
    // Load building information from MANIFEST.MF
    URLClassLoader cl = (URLClassLoader) ManifestInfo.class.getClassLoader();
    
    URL url = cl.findResource(DEFAULT_MANIFEST);
    if(url == null) {
      throw new RuntimeException("Failed to get URL:" + DEFAULT_MANIFEST);
    }
    
    return readFromManifest(url);
  }
  
  public String getSVNRev(){
    return propertiesMap.get("SCM-Revision");
  }
  
  /**
   * Reads the content of a mainifest and store as features.
   * @return true if success.
   */
  public boolean readFromManifest(URL manifestFileUrl) {
		Manifest manifest = null;
		try {
			manifest = new Manifest(manifestFileUrl.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
      Attributes att = manifest.getMainAttributes();
      
      propertiesMap.clear();
      
      String attName;
      String attValue;
      
      
      if(appName == null || appName.trim().equals(""))
        appName = att.getValue("Implementation-Title");

      attName = "SDP-Revision";
      attValue = att.getValue(attName);
      if(attValue != null && !attValue.isEmpty())
        propertiesMap.put("Software Package Version", attValue);
      
      attName = "Implementation-Version";
      attValue = att.getValue(attName);
      propertiesMap.put(attName, attValue);
      version = attValue;
      
      attName = "SCM-Revision";
      attValue = att.getValue(attName);
      propertiesMap.put(attName, attValue);
      
      attName = "Build-Date";
      attValue = att.getValue(attName);
      propertiesMap.put(attName, attValue);

      attName = "Build-Jdk";
      attValue = att.getValue(attName);
      propertiesMap.put(attName, attValue);
      
      attName = "Implementation-Vendor";
      attValue = att.getValue(attName);
      propertiesMap.put("Vendor", attValue);
      
      propertiesMap.put("Java Running Environment", System.getProperty("java.version"));
      
      return true;
  }

  public String[][] getProperties() {
    return toArray(propertiesMap);
  }
  
  private static String[][] toArray(LinkedHashMap<String, String> map) {
    String[][] array = new String[map.size()][2];
    Set<Entry<String, String>> entries = map.entrySet();
    Iterator<Entry<String, String>> entriesIterator = entries.iterator();
    int index = 0;
    while (entriesIterator.hasNext()) {
      Entry<String, String> entry = entriesIterator.next();
      array[index][0] = entry.getKey();
      array[index][1] = entry.getValue();
      index++;
    }
    
    return array;
  }

  public String getVersion() {
    return version;
  }
}

