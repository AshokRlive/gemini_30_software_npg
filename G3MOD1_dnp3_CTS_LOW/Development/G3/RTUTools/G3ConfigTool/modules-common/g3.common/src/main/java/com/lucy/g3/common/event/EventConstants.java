/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.event;


/**
 *
 */
public class EventConstants {
  public static final String  EVENT_TOPIC         = "event.topics";
  public static final String  EVENT_DATA = "com.lucy.g3.services.events.data"; 


}

