
package com.lucy.g3.common.context;

import org.apache.log4j.Logger;

import com.lucy.g3.common.bean.Bean;

public abstract class AbstractComponent extends Bean implements IComponent {

  private final IContext context;

  private final String uniqueId;


  public AbstractComponent(IContext context, String uniqueId) {
    this.context = context;
    this.uniqueId = uniqueId;

    if (context != null) {
      context.addComponent(this);
    } else {
      Logger.getLogger(getClass()).warn("Context is null");
    }
  }

  protected void destroy() {
    if (context != null)
      context.removeComponent(this);
  }

  @Override
  public String getUniqueIdentifier() {
    return uniqueId;
  }

  @Override
  public IContext getContext() {
    return context;
  }

}
