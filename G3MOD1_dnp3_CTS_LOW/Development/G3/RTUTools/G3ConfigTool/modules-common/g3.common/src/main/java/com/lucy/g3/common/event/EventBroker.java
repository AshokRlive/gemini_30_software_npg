/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.common.event;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 *
 */
public class EventBroker {
  
  private final HashMap<String, ArrayList<IEventHandler>> registrations = new HashMap<>(); 
  
  public void subscribe(String topic, IEventHandler handler) {
    ArrayList<IEventHandler> registration = registrations.get(topic);
    
    if(registration == null) {
      registration = new ArrayList<>();
      registrations.put(topic, registration);
    }
    registration.add(handler);
  }
  
  public void unsubscribe(IEventHandler handler) {
    Collection<ArrayList<IEventHandler>> handlers = registrations.values();
    for (ArrayList<IEventHandler> hdlList : handlers) {
      hdlList.remove(handler);
    }
  }
  
  public void send(String topic, Object data) {
    ArrayList<IEventHandler> registration = registrations.get(topic);
    if(registration != null) {
      Event event = constructEvent(topic, data);
      for (IEventHandler handler:registration) {
        handler.handleEvent(event);
      }
    }
  }
  
  @SuppressWarnings("unchecked")
  private Event constructEvent(String topic, Object data) {
    Event event;
    if (data instanceof Map<?,?>) {
      event = new Event(topic, (Map<String,?>)data);
    } else {
      HashMap<String, Object> d = new HashMap<String, Object>(2);
      d.put(EventConstants.EVENT_TOPIC, topic);
      d.put(EventConstants.EVENT_DATA, data);
      event = new Event(topic, d);
    }
    return event;
  }

  private EventBroker() {}

  private final static EventBroker INSTNACE = new EventBroker();
  
  public static EventBroker getInstance() {
    return INSTNACE;
  }
  
}

