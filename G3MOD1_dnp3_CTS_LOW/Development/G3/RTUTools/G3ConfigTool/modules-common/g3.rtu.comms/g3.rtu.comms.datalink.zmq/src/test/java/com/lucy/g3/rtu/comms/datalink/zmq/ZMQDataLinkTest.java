/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink.zmq;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.lucy.g3.rtu.comms.datalink.zmq.ZMQDataLink;
import com.lucy.g3.test.support.CommsTestSupport;

@Ignore // Not yet supported by RTU
public class ZMQDataLinkTest {

  final private static byte[] checkAliveMsg = new byte[] { 0x0b, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00 };
  final private static byte[] getMInfoMsg = new byte[] { 0x0b, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00 };


  @BeforeClass
  public static void setup() {
    Logger.getLogger(ZMQDataLink.class).setLevel(Level.DEBUG);
  }

  @Test
  public void testTimeCost() throws IOException {
    ZMQDataLink test = new ZMQDataLink(CommsTestSupport.HOST);
    long start = System.currentTimeMillis();
    for (int i = 0; i < 100; i++) {
      test.send("checkAlive", checkAliveMsg);
    }
    long end = System.currentTimeMillis();
    System.out.println("100 MsgsTime cost: " + (end - start) + " ms");
  }

  @Test
  public void testCancelSending() {
    String host = "tcp://10.11.11.102:5555";
    ZContext ctx = new ZContext();
    Socket socket = ctx.createSocket(ZMQ.REQ);
    socket.setReceiveTimeOut(100);
    socket.connect(host);
    socket.send(checkAliveMsg);
    byte[] reply = socket.recv();
    System.out.println("Reply: " + Arrays.toString(reply));
    if (reply == null) {
      socket.setLinger(0);
      socket.close();
    }
    ctx.destroySocket(socket);
    ctx.close();
    // while(true)
    // {}
  }

  private static void requestMInfo(ZMQDataLink zmq) {
    boolean disconnected = false;
    while (!disconnected) {
      byte[] reply;
      try {
        reply = zmq.send("getMInfoMsg", getMInfoMsg);
        assertTrue(reply.length > 0);

        if (disconnected) {
          System.err.println("MINFO: Connection resumed!!!");
          disconnected = false;
        }
      } catch (IOException e) {
        disconnected = true;
        System.err.println("MINFO: Connection Lost:" + e.getMessage());
      }
    }
  }

  private static void requestCheckAlive(ZMQDataLink zmq) {
    boolean disconnected = false;
    while (!disconnected) {
      byte[] reply;
      try {
        reply = zmq.send("checkAlive", checkAliveMsg);
        assertTrue(reply.length > 0);

        if (disconnected) {
          System.err.println("CHECKALIVE: Connection resumed!!!");
          disconnected = false;
        }
      } catch (IOException e) {
        disconnected = true;
        System.err.println("CHECKALIVE: Connection Lost:" + e.getMessage());
      }
    }
  }

  @Test
  public void testReconnect() throws InterruptedException {
    final ZMQDataLink zmq = new ZMQDataLink("10.11.11.102");

    Runnable run0 = new Runnable() {

      @Override
      public void run() {
        requestCheckAlive(zmq);
      }
    };

    Runnable run1 = new Runnable() {

      @Override
      public void run() {
        requestMInfo(zmq);
      }
    };

    Thread t0 = new Thread(run0);
    Thread t1 = new Thread(run1);

    t0.start();
    t1.start();

    t0.join();
    t1.join();
  }


  private static class ServerThread extends Thread {

    private void delay(int secs) {
      try {
        Thread.sleep(secs * 1000);
      } catch (InterruptedException e) {
      }
    }

    @Override
    public void run() {
      Random rand = new Random(System.nanoTime());

      Context context = ZMQ.context(1);
      Socket server = context.socket(ZMQ.REP);
      server.bind("tcp://*:5555");

      int cycles = 0;
      while (!interrupted()) {
        String request = server.recvStr();
        // cycles++;

        // Simulate various problems, after a few cycles
        if (cycles > 3 && rand.nextInt(3) == 0) {
          System.out.println("I: simulating a crash(sleep 10 secs)");
          delay(10);
        } else if (cycles > 3 && rand.nextInt(3) == 0) {
          System.out.println("I: simulating CPU overload");
        }
        System.out.printf("I: normal request (%s)\n", request);

        delay(1);
        // Do some heavy work
        server.send(request);
      }

      server.close();
      context.term();
    }
  }


  // Run server
  public static void main(String[] args) {
    ServerThread server = new ServerThread();
    server.start();

    try {
      server.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
