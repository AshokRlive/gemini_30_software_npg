/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink.zmq;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.PollItem;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

import com.lucy.g3.rtu.comms.datalink.AbstractDataLink;
import com.lucy.g3.rtu.comms.datalink.ISecurityManager;

/**
 * Implementation of <code>DataLink</code> using ZeroMQ lib.
 */
public class ZMQDataLink extends AbstractDataLink{

  private Logger log = Logger.getLogger(ZMQDataLink.class);

  public static final int DEFAULT_PORT = 5555;

  private String hostAddress;

  private static final int REQUEST_TIMEOUT = 500;// 2500; // msecs, (> 1000!)
  private static final int REQUEST_RETRIES = 1;

  private final ZContext ctx;
  private Socket socket;


  public ZMQDataLink() {
    this(null);
  }

  public ZMQDataLink(String address) {
    setHost(address);
    ctx = new ZContext();
    openSocket();
  }

  @Override
  public void setHost(String address/* , long port */) {

    if (socket != null) {
      socket.disconnect(hostAddress);
    }

    hostAddress = createFullURL(address, DEFAULT_PORT);
    log.info("Host changed to: " + hostAddress);

    if (socket != null) {
      socket.connect(hostAddress);
    }
  }

  @Override
  public String toString() {
    return hostAddress;
  }

  public int getPort() {
    return DEFAULT_PORT;
  }

  @Override
  protected synchronized byte[] send(String messageName, byte[] requestData) throws IOException {
    if (requestData == null || requestData.length <= 0) {
      log.error("Request is null");
      return null;
    }
    printByteData(log, "Request [" + messageName + "]", requestData);

    byte[] reply = null;
    int retries = 0;

    /*
     * Here we send a request and process a server reply and exit our loop if
     * the reply is valid. If we didn't receive a reply we close the client
     * socket and re-send the request. We try a number of times before finally
     * abandoning.
     */
    while (retries <= REQUEST_RETRIES && !Thread.currentThread().isInterrupted()) {
      /*
       * Always close socket before retrying because the last sent message could
       * still be queued in that socket, which prevents retrying sending again.
       */
      if (retries > 0) {
        log.warn("Retrying sending message...");
        closeSocket();
      }

      // Open socket if needed
      if (socket == null) {
        openSocket();
      }

      retries++;

      /* We send a request, then we work to get a reply */
      try {
        if (socket.send(requestData) == false) {
          log.error("Fail to send request message.");
          continue;
        }
      } catch (Exception e) {
        log.error("Fail to send request message: " + e.getMessage());
        continue;
      }

      /* Poll socket for a reply, with timeout */
      PollItem[] items = { new PollItem(socket, Poller.POLLIN) };
      int rc = ZMQ.poll(items, REQUEST_TIMEOUT);
      if (rc == -1) {
        /* Thread interrupted */
        continue;
      }

      /* Receive reply data */
      if (items[0].isReadable()) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        while (true) {
          byte[] recvBytes = socket.recv();
          assert recvBytes != null;
          bout.write(recvBytes);

          if (!socket.hasReceiveMore()) {
            break;
          }
        }

        reply = bout.toByteArray();
        log.info(String.format("server replied OK (Length:%d)", reply.length));
        break; // Success
      }
    }

    if (!Thread.currentThread().isInterrupted()) {
      if (reply != null) {
        printByteData(log, "Reply   [" + messageName + "]", reply);
      } else {
        closeSocket();
        throw new IOException("No Reply");
      }
    }

    return reply;
  }

  private void closeSocket() {
    if (socket != null) {
      try {
        socket.setLinger(0);

        /*
         * !!! Don't call this. ClosedByInterruptException will be thrown and
         * cause failure to close socket.
         */
        // socket.disconnect(hostAddress);

        socket.close();
        log.info("ZMQ socket closed");
      } catch (Exception e) {
        log.error("Socket close error:" + e.getMessage());
      } finally {
        ctx.destroySocket(socket);
        socket = null;
      }
    }
  }

  private void openSocket() {
    socket = ctx.createSocket(ZMQ.REQ);
    socket.connect(hostAddress);
    log.info("ZMQ socket opened and connected to: " + hostAddress);
  }

  /**
   * Create the full URL for communication.
   */
  private static String createFullURL(String host, long port) {
    return "tcp://" + host + ":" + port;
  }

  @Override
  public String getHost() {
    return hostAddress;
  }

  @Override
  public void setTimeout(int timeout) {
    
  }


  @Override
  public ISecurityManager getSecurityManager() {
    return null;
  }
}
