/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.common;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.junit.Test;

public class CharsetTest {

  @Test
  public void testCharset() throws UnsupportedEncodingException {
    String sample = "this is a example";
    byte[] bytes = null;
    System.out.println("Default charset:" + Charset.defaultCharset());
    String charset = "UTF-8";
    bytes = sample.getBytes(charset);
    System.out.println(charset);
    printByteData( "", bytes);

    charset = "ISO-8859-1";
    bytes = sample.getBytes(charset);
    System.out.println(charset);
    printByteData( "", bytes);

    charset = "windows-1251";
    bytes = sample.getBytes(charset);
    System.out.println(charset);
    printByteData( "", bytes);

    charset = "iso-8859-2";
    bytes = sample.getBytes(charset);
    System.out.println(charset);
    printByteData( "", bytes);
  }

  @Test
  public void testChar() {
    Charset iso88591charset = Charset.forName("ISO-8859-1");
    Charset utf8charset = Charset.forName("UTF-8");
    /*
     * Modified character depiction from char to int to bypass issues with
     * 'invalid character constant' error message char c0 = '你'; char c1 = '我';
     * /*
     */
    char c0 = 20320;
    char c1 = 25105;

    printByteData( "c0", String.valueOf(c0).getBytes());
    printByteData( "c1", String.valueOf(c1).getBytes());

    CharBuffer buf = CharBuffer.wrap(new char[] { c0, c1 });
    ByteBuffer bytes = iso88591charset.encode(buf);
    printByteData( "\n88591", bytes.array());

    buf = CharBuffer.wrap(new char[] { c0, c1 });
    bytes = utf8charset.encode(buf);
    printByteData( "\nUTF", bytes.array());
  }

  @Test
  public void testCharEncodeConversion() throws UnsupportedEncodingException {
    Charset utf8charset = Charset.forName("UTF-8");
    Charset iso88591charset = Charset.forName("ISO-8859-1");

    // Data
    byte[] b0 =
        { 0x0b, 0x00, 0x00, 0x00, 0x3f, 0x00, 0x00, 0x00, 0x3f, 0x3f, 0x3f, 0x00, 0x47, 0x3f, 0x3f, 0x59, 0x3f, 0x3f,
            0x3f, 0x10, 0x3f, 0x00, 0x3f, 0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x41, 0x64,
            0x6d, 0x69, 0x6e, 0x00 };
    ByteBuffer inputBuffer = ByteBuffer.wrap(b0);

    // decode UTF-8
    CharBuffer data = utf8charset.decode(inputBuffer);

    // encode UTF-8
    ByteBuffer outputBuffer = utf8charset.encode(data);
    printByteData( "\nUTF-8", outputBuffer.array());

    // encode ISO-8559-1
    outputBuffer = iso88591charset.encode(data);
    printByteData( "\nISO-8559-1", outputBuffer.array());

    String s = new String(b0, "UTF-8");
    printByteData( "\nstringToBytesASCII ", stringToBytesASCII(s));
    printByteData( "\nstringToBytesUTFCustom ", stringToBytesUTFCustom(s));
    printByteData( "\nstringToBytesUTFNIO ", stringToBytesUTFNIO(s));
    printByteData( "\nconvert back", bytesToStringUTFNIO(stringToBytesUTFNIO(s)).getBytes());
  }

  @Test
  public void testCharEncode() {
    String s0 = "afaklsdj*(&(*^(^\"&*!£()";
    String s1 = "你好奋进功2人";

    printByteData( "\nstringToBytesASCII s0", stringToBytesASCII(s0));
    printByteData( "\nstringToBytesASCII s1", stringToBytesASCII(s1));
    printByteData( "\nstringToBytesUTFCustom s0", stringToBytesUTFCustom(s0));
    printByteData( "\nstringToBytesUTFCustom s1", stringToBytesUTFCustom(s1));
    printByteData( "\nstringToBytesUTFNIO s0", stringToBytesUTFNIO(s0));
    printByteData( "\nstringToBytesUTFNIO s1", stringToBytesUTFNIO(s1));
  }

  // ================== Help Methods ===================

  private static byte[] stringToBytesASCII(String str) {
    char[] buffer = str.toCharArray();
    byte[] b = new byte[buffer.length];
    for (int i = 0; i < b.length; i++) {
      b[i] = (byte) buffer[i];
    }
    return b;
  }

  private static byte[] stringToBytesUTFCustom(String str) {
    char[] buffer = str.toCharArray();
    byte[] b = new byte[buffer.length << 1];
    for (int i = 0; i < buffer.length; i++) {
      int bpos = i << 1;
      b[bpos] = (byte) ((buffer[i] & 0xFF00) >> 8);
      b[bpos + 1] = (byte) (buffer[i] & 0x00FF);
    }
    return b;
  }

  private static byte[] stringToBytesUTFNIO(String str) {
    char[] buffer = str.toCharArray();
    byte[] b = new byte[buffer.length << 1];
    CharBuffer cBuffer = ByteBuffer.wrap(b).asCharBuffer();
    for (int i = 0; i < buffer.length; i++) {
      cBuffer.put(buffer[i]);
    }
    return b;
  }

  private static String bytesToStringUTFNIO(byte[] bytes) {
    CharBuffer cBuffer = ByteBuffer.wrap(bytes).asCharBuffer();
    return cBuffer.toString();
  }

  private static void printByteData(String name, byte[] data) {
    String str = Arrays.toString(data);

    if (name != null && !name.isEmpty())
      str = name + ":" + str;

    System.out.print(str);
  }

}
