/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.shared;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 * Returned result when login RTU.
 */
public class LoginResult {

  public final USER_LEVEL level;
  public final short sessionID;


  public LoginResult(USER_LEVEL level, short sessionID) {
    this.level = level;
    this.sessionID = sessionID;
  }
  
  public USER_LEVEL getUserLevel(){
    return level;
  }

  public String getUserLevelText() {
    return level == null ? "" : level.getDescription();
  }

  @Override
  public String toString() {
    return "LoginResult [level=" + level + ", sessionID=" + sessionID + "]";
  }
}
