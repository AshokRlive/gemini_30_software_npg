/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.common;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;


/**
 *
 */
public class RTUTimeTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testFormat() {
    assertEquals("01-Jan-1970 00:00:00.000", RTUTimeDecoder.formatEventLog(new Date(0L)));
  }
  
  @Test
  public void testDecode() {
    long ms = 1436890489001L;
    String str = "14-Jul-2015 16:14:49.001 UTC";
    Date date = RTUTimeDecoder.decode(ms);
    assertEquals(ms, date.getTime());
    assertEquals(str, RTUTimeDecoder.formatRTUTime(date));
  }
  
}

