/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.shared;

import java.io.File;


/**
 * The interface of communication task invoker.
 */
public interface ICommsTaskInvoker {

  /**
   * Checks if the task is cancelled.
   * @return true if cancelled, false otherwise.
   */
  boolean isCancelled();

  /**
   * Sets the task progress.
   * @param progress the progress of task, from 0 -100
   */
  void setProgress(int progress);

  /**
   * Sets the task message. 
   * @param message a text that could be shown to user.
   */
  void setMessage(String message);
  
  /**
   * This method will be called when a file is going to be overwritten by
   * communication task.
   */
  boolean confirmOverrite(File overwriteFile);
}

