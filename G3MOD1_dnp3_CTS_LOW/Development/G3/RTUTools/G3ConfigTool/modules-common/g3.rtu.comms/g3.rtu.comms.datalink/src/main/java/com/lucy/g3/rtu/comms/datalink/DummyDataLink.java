/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink;

import java.io.IOException;
import java.util.Arrays;

/**
 * The Class DummyDataLink.
 */
public class DummyDataLink extends AbstractDataLink {

  private byte[] reply;
  private boolean ioexcept;
  private int delay = 500;// ms


  public void setReplyBytes(byte[] reply) {
    this.reply = reply;
  }

  public void setIOException(boolean ioexcept) {
    this.ioexcept = ioexcept;
  }

  @Override
  public byte[] send(String msgName, byte[] data) throws IOException {
    System.out.println("Send: " + Arrays.toString(data));

    // Simulate exception
    if (ioexcept) {
      throw new IOException("Network is down");
    }

    // Simulate delay
    try {
      Thread.sleep(delay);
    } catch (InterruptedException e) {
    }

    System.out.println("Reply: " + Arrays.toString(reply));
    return reply;
  }

  @Override
  public void setHost(String address) {

  }


  @Override
  public String getHost() {
    return null;
  }

  @Override
  public void setTimeout(int timeout) {
    
  }


  @Override
  public ISecurityManager getSecurityManager() {
    return null;
  }

}
