/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink;

import java.beans.PropertyChangeListener;
import java.io.File;

public interface ISecurityManager  {
  
  String PROPERTY_IGNORECERTIFICATEERROR = "ignoreCertificateError";
  
  String getHostAddress();
  
  void setIgnoreCertificateError(boolean ignoreCertificateError);

  boolean isIgnoreCertificateError();
  
  void importKeyStore(File keyStoreFile, char[] password) throws Exception;
  
  void addPropertyChangeListener(PropertyChangeListener listener);
  void removePropertyChangeListener(PropertyChangeListener listener);
  void addPropertyChangeListener(String propertyName, PropertyChangeListener listener);
  void removePropertyChangeListener(String propertyName, PropertyChangeListener listener);
  
}

