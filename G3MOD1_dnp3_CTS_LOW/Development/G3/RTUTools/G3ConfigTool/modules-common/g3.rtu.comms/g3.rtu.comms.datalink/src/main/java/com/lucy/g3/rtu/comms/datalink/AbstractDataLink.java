/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;

import org.apache.log4j.Logger;


/**
 * The Class AbstractDataLink.
 */
public abstract class AbstractDataLink implements DataLink{
  private Logger log = Logger.getLogger(AbstractDataLink.class);
  
  private final PropertyChangeSupport support = new PropertyChangeSupport(this);
  
  @Override
  public void removePropertyChangeListener(PropertyChangeListener listener) {
    support.removePropertyChangeListener(listener);
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    support.addPropertyChangeListener(listener);
  }
  
  protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
    support.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  @Override
  public void ping() throws IOException{
    send("Ping", new byte[]{0});
  }
  
  @Override
  public byte[] send(String messageName, byte[] messageData, int maxRetries) throws IOException {
    int retries = 0;
    byte[] ret;
    do {
      ret = send(messageName, messageData);
      if(ret != null && ret.length > 0)
        break;
      
      retries++;
      
      /* Log warning only for non-check-alive message retries.*/
      if(!"CheckAlive".equals(messageName))
        log.warn(String.format("Retrying to send:%s [retries: %d]", messageName,retries));
      
    } while (retries <= maxRetries);
    
    return ret;
  }

  protected abstract byte[] send(String messageName, byte[] messageData) throws IOException;

  
  protected void printByteData(Logger log, String name, byte[] array) {
    if (!log.isDebugEnabled()) {
      return;
    }

    if (array == null || array.length == 0) {
      log.debug(name + " is Empty");
      return;
    }

    StringBuilder builder = new StringBuilder(100);
    if (name != null && !name.isEmpty()) {
      builder.append(name + " :");
    }
    for (int k = 0; k < array.length; k++) {
      builder.append("0x" + byteToHex(array[k]));

      if (k != array.length - 1) {
        builder.append(",");
      }
    }
    builder.append(" [Size:" + array.length + "] ");
    log.debug(builder.toString());
    builder = null;
  }
  
  
  private static String byteToHex(byte b) {
    // Returns hex String representation of byte b
    char[] hexDigit = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };
    char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
    return new String(array);
  }
}

