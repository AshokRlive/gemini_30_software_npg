/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink;

import java.beans.PropertyChangeListener;
import java.io.IOException;

/**
 * Interface of component for sending data to a remote server. 
 */
public interface DataLink  {
  
  String PROPERTY_HOST = "host";
  
  ISecurityManager getSecurityManager();
  
  /**
   * Sends a message to remote server and wait for reply. This will blocks the caller
   * until a reply is received or communication fails.
   *
   * @param messageData
   *          data to be sent.
   * @param messageName
   *          the name of message for debug usage.
   * @return message data received from remote server.
   */
  byte[] send(String messageName, byte[] messageData, int maxRetries) throws IOException;


  /**
   * Changes the address of the host to communicate.
   *
   * @param hostAddress
   *          new host address
   */
  void setHost(String address);

  String getHost();
  

  void setTimeout(int timeoutMs);

  void addPropertyChangeListener(PropertyChangeListener listener);
  
  void removePropertyChangeListener(PropertyChangeListener listener);

  void ping() throws IOException;

}
