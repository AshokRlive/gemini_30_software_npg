
package com.lucy.g3.rtu.comms.datalink.http;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.junit.Test;

public class HttpsClient {
  static{ 
    initVerifier();
  }
  
  @Test
  public void printCipher() throws NoSuchAlgorithmException {
    HostnameVerifier verifier = HttpsURLConnection.getDefaultHostnameVerifier();
    //con.getSSLSocketFactory().getDefaultCipherSuites();
    
    String p = SSLContext.getDefault().getProtocol();
    System.out.println("P: " +p);
  }
  
  public static void main(String[] args) throws Exception {
//    System.setProperty("javax.net.ssl.trustStore", "E:\\sw_dev\\temp\\OpenSSL\\g3.cert\\g3truststore");
    //HttpSSLSupport.disableSSL();
    
    {KeyStore ks = KeyStore.getInstance("jks");
    String password = "password";
    ks.load(new FileInputStream(new File("E:\\sw_dev\\temp\\OpenSSL\\g3.cert\\g3truststore")), password.toCharArray() );
    Certificate cert = ks.getCertificate("g3devices");
    System.out.println("cert: " +cert);

    TrustManagerFactory tm = TrustManagerFactory.getInstance("PKIX");
    tm.init(ks);
    
    SSLContext sc = SSLContext.getInstance("TLS");
    sc.init(null, tm.getTrustManagers(), new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    }
    
    new HttpsClient().testIt();
  }
  
  private  static void  initVerifier() {
    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
        {
            @Override
            public boolean verify(String hostname, SSLSession session)
            {
                // ip address of the service URL(like.23.28.244.244)
//                if (hostname.equals("192.168.1.1"))
//                    return true;
//                return false;
              System.out.println("--------------------------------------verified host:"+hostname);
              return true;
            }
        });
}
  
  private void testIt() {

     String https_url = "https://192.168.1.1/";
//    String https_url = "https://www.google.com/";
    URL url;
    try {

      url = new URL(https_url);
      HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
      
      // dumpl all cert info
      print_https_cert(con);

      // dump all the content
      print_content(con);

    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private static void getSSLSocketFactory() {
    TrustManager[]  tm = new TrustManager[] { new X509TrustManager() {

      @Override
      public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      @Override
      public void checkClientTrusted(
          java.security.cert.X509Certificate[] arg0, String arg1)
          throws CertificateException {

      }

      @Override
      public void checkServerTrusted(
          java.security.cert.X509Certificate[] arg0, String arg1)
          throws CertificateException {

      }
    }
    };
  }

  private void print_https_cert(HttpsURLConnection con) {

    if (con != null) {

      try {

        System.out.println("Response Code : " + con.getResponseCode());
        System.out.println("Cipher Suite : " + con.getCipherSuite());
        System.out.println("\n");

        Certificate[] certs = con.getServerCertificates();
        for (Certificate cert : certs) {
          System.out.println("Cert Type : " + cert.getType());
          System.out.println("Cert Hash Code : " + cert.hashCode());
          System.out.println("Cert Public Key Algorithm : "
              + cert.getPublicKey().getAlgorithm());
          System.out.println("Cert Public Key Format : "
              + cert.getPublicKey().getFormat());
          System.out.println("\n");
          
          System.out.println("=================Certificate Details ==============\n"+cert.toString());
          
        }

      } catch (SSLPeerUnverifiedException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }

    }

  }

  private void print_content(HttpsURLConnection con) {
    if (con != null) {

      try {

        System.out.println("****** Content of the URL ********");
        BufferedReader br =
            new BufferedReader(
                new InputStreamReader(con.getInputStream()));

        String input;

        while ((input = br.readLine()) != null) {
          System.out.println(input);
        }
        br.close();

      } catch (IOException e) {
        e.printStackTrace();
      }

    }

  }

}