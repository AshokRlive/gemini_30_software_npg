/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink.http;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.Certificate;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.datalink.AbstractDataLink;

/**
 * Implementation of <code>DataLink</code> using HTTP.
 */
public class HttpDataLink extends AbstractDataLink {

  private static final String HTTP_MSG_TYPE = "message/g3";

  private Logger log = Logger.getLogger(HttpDataLink.class);

  private String g3URL; // Full connection URL
  private String host; // Host address
  
  private int timeoutMs = 3000; // ms
  
  private final HashMap<String, SecurityManager> sslMap = new HashMap<>();
  
  public HttpDataLink() {
    this(null);
  }

  public HttpDataLink(String address) {
    setHost(address);
  }

  @Override
  public String getHost() {
    return host;
  }
  
  @Override
  public void setHost(String host) {
    if(host == null)
      host = "0.0.0.0";
    
    Object oldValue = getHost();
    this.host = host;
    firePropertyChange(PROPERTY_HOST, oldValue, host);

    g3URL = createFullURL();
  }


  @Override
  public String toString() {
    return g3URL;
  }

  /**
   * Create the full URL for communication.
   */
  private String createFullURL() {
    String url = getHost() + "/g3/comm.cgi";
    
    if(!url.startsWith("https")) {
      url = "https://"+ url;
    }
    
    return url;
  }

  private URLConnection initURLConnection() throws IOException {
    g3URL = createFullURL();

    URL url = new URL(g3URL);
    HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
    getSecurityManager().configure(conn);
    
    conn.setDoOutput(true);
    conn.setDoInput(true);
    conn.setAllowUserInteraction(true);
    conn.setConnectTimeout(timeoutMs);
    conn.setReadTimeout(timeoutMs);
    conn.setRequestProperty("Content-Type", HTTP_MSG_TYPE);

    return conn;
  }

  @Override
  protected synchronized byte[] send(String messageName, byte[] requestData) throws IOException {
    if (requestData == null || requestData.length <= 0) {
      log.error("Request is null");
      return null;
    }
    
    printByteData(log, "Request [" + messageName + "]", requestData);

    URLConnection conn = initURLConnection();
    
    BufferedOutputStream out = new BufferedOutputStream(conn.getOutputStream());
    out.write(requestData);
    out.flush();
    out.close();
    
    BufferedInputStream bis = null;
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    try {
      bis = new BufferedInputStream(conn.getInputStream());
      int nbytes;
      byte[] buffer = new byte[16384];

      while ((nbytes = bis.read(buffer, 0, buffer.length)) != -1) {
        bout.write(buffer, 0, nbytes);
      }
      bout.flush();
    } catch (IOException e) {
      String msg = "No response for request: " + messageName + " cause:" + e.getMessage();
      if("CheckAlive".equals(messageName))
        log.info(msg);
      else
        log.error(msg);
      
    } finally {
      if (bis != null) {
        bis.close();
      }
      
    }

    byte[] reply = bout.toByteArray();
    printByteData(log, "Reply   [" + messageName + "]", reply);
    return reply;
  }


  public int getTimeout() {
    return timeoutMs;
  }

  @Override
  public void setTimeout(int timeoutMs) {
    if (timeoutMs >= 500)
      this.timeoutMs = timeoutMs;
  }
  

  @Override
  public SecurityManager getSecurityManager() {
    String key = getHost();
    SecurityManager ssl = sslMap.get(key);
    if(ssl == null) {
      ssl = new SecurityManager(this);
      sslMap.put(key, ssl);
    }
    
    return ssl;
  }
 
}
