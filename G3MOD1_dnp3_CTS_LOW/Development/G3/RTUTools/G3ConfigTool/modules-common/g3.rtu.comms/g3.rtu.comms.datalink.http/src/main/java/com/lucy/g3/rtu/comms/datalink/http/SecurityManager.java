/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink.http;

import com.lucy.g3.rtu.comms.datalink.DataLink;
import com.lucy.g3.rtu.comms.datalink.ISecurityManager;
import com.lucy.g3.security.support.SSLSupport;

/**
 * Implementation of security manager.
 */
public class SecurityManager extends SSLSupport implements ISecurityManager {

  private final DataLink link;

  SecurityManager(DataLink link) {
    this.link = link;
  }

  @Override
  public String getHostAddress() {
    return link.getHost();
  }

}
