
package com.lucy.g3.rtu.comms.datalink.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.util.Iterator;


public class HttpsClient2 {

  public static void main(String[] args) throws java.security.cert.CertificateException {
    System.out.println("java version:"+System.getProperty("java.version"));
    listCA();
    
    System.setProperty("javax.net.ssl.trustStore", "E:\\sw_dev\\temp\\OpenSSL\\g3.cert\\g3truststore");
  }
  
  private static void listCA() throws java.security.cert.CertificateException{
    try {
      // Load the JDK's cacerts keystore file
      String filename = System.getProperty("java.home") + "/lib/security/cacerts".replace('/', File.separatorChar);
      FileInputStream is = new FileInputStream(filename);
      KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
      String password = "changeit";
      keystore.load(is, password.toCharArray());

      // This class retrieves the most-trusted CAs from the keystore
      PKIXParameters params = new PKIXParameters(keystore);

      // Get the set of trust anchors, which contain the most-trusted CA certificates
      Iterator it = params.getTrustAnchors().iterator();
      while( it.hasNext() ) {
          TrustAnchor ta = (TrustAnchor)it.next();
          // Get certificate
          java.security.cert.X509Certificate cert = ta.getTrustedCert(); //X509Certificate 
          System.out.println(cert);
      }
  }  catch (KeyStoreException e) {
  } catch (NoSuchAlgorithmException e) {
  } catch (InvalidAlgorithmParameterException e) {
  } catch (IOException e) {
  } 
  }

}