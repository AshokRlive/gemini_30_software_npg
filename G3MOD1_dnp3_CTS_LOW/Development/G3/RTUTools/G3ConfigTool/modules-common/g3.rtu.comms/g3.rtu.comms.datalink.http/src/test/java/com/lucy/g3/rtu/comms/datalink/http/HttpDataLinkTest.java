/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.datalink.http;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.test.support.CommsTestSupport;

import org.junit.Assert;

/**
 * The Class HttpDataLinkTest.
 */
public class HttpDataLinkTest {

  final byte[] pingData = new byte[] { 0 };

  private HttpDataLink link;
  
  private final static int TIMEOUT_MS = 10000;
  private final static int TIMEOUT_ERROR = 1000;


  @Before
  public void setUp() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    
    Logger.getLogger(HttpDataLink.class).setLevel(Level.DEBUG);
    link = new HttpDataLink(CommsTestSupport.HOST);
    link.setTimeout(TIMEOUT_MS);
  }
  
  @Test
  public void testSetIgnoreCertificateError() {
    link.getSecurityManager().setIgnoreCertificateError(true);
    try {
      link.ping();
    } catch (IOException e) {
      Assert.fail("No exception expected");
    }
    
    /*Create a new link */
    link = new HttpDataLink(CommsTestSupport.HOST);
    
    try {
      link.ping();
      Assert.fail("Exception expected");
    } catch (IOException e) {
    }
  }

//  @Test
//  public void testTSL() throws IOException {
//    link.enableSSL("TLSv1.2");
//    link.send("", pingData);
//  }
//
//  @Test
//  public void testSSL() throws IOException {
//    link.enableSSL("SSL");
//    link.send("ping", pingData);
//  }
//  
//  @Test(expected = ConnectException.class)
//  public void testSSLDisabled() throws IOException {
//    link.disableSSL();
//    link.send("ping", pingData);
//  }

//  @Test
//  public void testDetection() throws Exception {
//    long start = System.nanoTime();
//
//    String protocol = link.autoSelectSSL();
//    long elapsedTime = System.nanoTime() - start;
//    double seconds = elapsedTime / 1000000000.0;
//    double ms = seconds * 1000;
//    
//    System.out.println(String.format("== Result ==\nDetect protocol:%s\nTime cost:%s secs", protocol, seconds));
//    
//    // Check protocol
//    if ( protocol == null) {
//      assertFalse(link.isSslEnabled());
//      
//      // Check timeout
//      double err = ms - TIMEOUT_MS; 
//      assertTrue("Timeout not correct. Actual:" + ms +" expects:"+TIMEOUT_MS, err < TIMEOUT_ERROR);
//      
//    } else
//      assertTrue(link.isSslEnabled());
//  }

}
