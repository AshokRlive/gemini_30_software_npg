/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.rtu.comms.protocol.exceptions;
/**
 * An <code>UnexpectedRequestIDException</code> is thrown if the Request ID in
 * received message is different from the one in sent message.
 */
public final class UnexpectedRequestIDException extends ProtocolException {

  public UnexpectedRequestIDException(String messageName, short expected, short actual) {
    super(String.format("%s: unexpected replied Request ID! Expected:%d Actual:%d",
        messageName, expected, actual));
  }
}