/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Common.CTMsgID_R_AckNack;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Date.CTMsgID_C_GetDate;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Date.CTMsgID_C_SetDate;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Date.CTMsgID_R_GetDate;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_C_GetChannel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_C_GetPointAnalogue;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_C_GetPointCounter;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_C_GetPointDigital;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_R_GetChannel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_R_GetPointAnalogue;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_R_GetPointCounter;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic.CTMsgID_R_GetPointDigital;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_C_FReadCancel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_C_FReadFileList;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_C_FReadFragment;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_C_FReadStart;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_R_FReadFileList;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_R_FReadFragment;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead.CTMsgID_R_FReadStart;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileWrite.CTMsgID_C_FWriteFinish;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileWrite.CTMsgID_C_FWriteFragment;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileWrite.CTMsgID_C_FWriteStart;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileWrite.CTMsgID_R_FWriteFinish;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_BeginEventLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_ClearEventLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_GetEventLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_GetLastLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_GetLogFileName;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_GetLogLevel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_C_SetLogLevel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_R_GetEventLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_R_GetLastLog;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_R_GetLogFileName;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log.CTMsgID_R_GetLogLevel;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login.CTMsgID_C_CheckAlive;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login.CTMsgID_C_GetMDAlgorithm;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login.CTMsgID_C_UserLogin;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login.CTMsgID_R_CheckAlive;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login.CTMsgID_R_MDAlgorithm;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login.CTMsgID_R_UserLogin;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_C_ModuleAlarm;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_C_ModuleCANStats;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_C_ModuleClearAlarm;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_C_ModuleDetail;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_C_ModuleInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_R_ModuleAlarm;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_R_ModuleCANStats;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_R_ModuleDetail;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module.CTMsgID_R_ModuleInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_C_PollPointAnalogue;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_C_PollPointCounter;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_C_PollPointDigital;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_C_SetPointAList;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_C_SetPointCList;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_C_SetPointDList;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_R_PollPointAnalogue;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_R_PollPointCounter;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData.CTMsgID_R_PollPointDigital;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_ActivateConfig;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_EnableDebugMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_DisableDebugMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_GetDebugMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_R_GetDebugMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_EraseCommissioningCache;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_EraseConfig;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_FactoryReset;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Forced_Reset;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Forced_Restart;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Forced_Shutdown;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_GetServiceMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_OnServiceMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_OutServiceMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Register;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Reset;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Restart;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_RestoreConfig;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_C_Shutdown;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl.CTMsgID_R_GetServiceMode;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_C_GetBatteryCfg;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_C_GetConfigInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_C_GetNetInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_C_GetRTUInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_C_GetSDPVersion;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_C_GetSysAPI;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_R_GetBatteryCfg;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_R_GetConfigInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_R_GetNetInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_R_GetRTUInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_R_GetSDPVersion;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo.CTMsgID_R_GetSysAPI;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Testing.CTMsgID_C_GetOLR;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Testing.CTMsgID_C_OperateCLogic;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Testing.CTMsgID_C_OperateSwitch;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Testing.CTMsgID_C_SetOLR;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Testing.CTMsgID_R_GetOLR;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_C_EraseFirmware;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_C_GetUpgradeState;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_C_SDPVersion;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_C_UpgradeModeOff;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_C_UpgradeModeOn;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_C_UpgradeModule;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_R_EraseFirmware;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade.CTMsgID_R_GetUpgradeState;

import java.nio.ByteOrder;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol;

/**
 * This interface contains the constants/enums defined in G3 Protocol.
 *
 * @see <a href="http://10.11.0.6:81/svn/gemini_30_software/trunk/Documentation
 *      /Specification/DOC00435(G3%20Configuration%20Tool%20Communication%20
 *      Protocol).docx"> G3 Configuration Tool Communication Protocol</a>
 */
public interface G3Protocol extends G3ConfigProtocol {

  /** Byte order of the byte stream read from RTU system. */
  ByteOrder RTU_BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;

  /** Default Character Encoding for G3 Protocol. */
  String CHAR_SET = "UTF-8";

  /** Default Character Encoding for RTU site name. */
  String RTU_NAME_CHARSET = "UTF-8";

  /** Message ID for AckNack messages. */
  short ACK_NACK_MSG_ID = (short) 0xFFFF;

  int RTU_YEAR_BEGINNING = 1900;
  
  /** Default file permission when creating a file for RTU system. */
  int DEFAULT_FILE_PERMISSION = 493;
  
  String  DEFAULT_COMMISSIONING_CACHE_FILE_NAME = "commissioning.cache";



  /**
   * This enum lists all kinds of interface used for communication.
   */
  enum LinkMode {
    HTTPLINK,
    SERIALLINK
  }

  /**
   * This enum lists the polling mode for monitoring points real-time data.
   */
  enum PollingMode {
    ANALOG,
    DIGITAL,
    COUNTER
  }

  /**
   * This enum lists the operation mode used for controlling RTU.
   */
  enum LocalRemoteCode {
    LOCAL(1), REMOTE(0);

    LocalRemoteCode(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }


    private final int value;
  }

  /**
   * This enum lists the modes for doing restart of RTU.
   */
  enum RestartMode {
    RESTART("Warm Restart", "Restart only RTU application"),
    REBOOT("Cold Restart", "Restart RTU application and operating system"),
    SHUT_DOWN("Shut Down", "Shutting down"),
    ENTER_UPGRADE("Enter Upgrade Mode", "Restart to enter upgrade mode"),
    EXIT_UPGRADE("Exit Upgrade Mode", "Restart to exit upgrade mode");

    RestartMode(String name, String description) {
      this.description = description;
      this.name = name;
    }

    public String getDescription() {
      return description;
    }

    public String getName() {
      return name;
    }

    public boolean isRestart() {
      return !isShutDown();
    }

    public boolean isShutDown() {
      return SHUT_DOWN == this;
    }

    @Override
    public String toString() {
      return getName();
    }


    private final String name;
    private final String description;
  }


  /* Configuration Tool Protocol 'Message IDs' */
  // @formatter:off GEN-BEGIN:
  short _CTMsgID_C_SetPointDList       = (short)CTMsgID_C_SetPointDList       .getValue();
  short _CTMsgID_C_SetPointAList       = (short)CTMsgID_C_SetPointAList       .getValue();    // Set list of Analogue Points to poll
  short _CTMsgID_C_SetPointCList       = (short)CTMsgID_C_SetPointCList       .getValue();    // Set list of Analogue Points to poll
  short _CTMsgID_C_PollPointDigital    = (short)CTMsgID_C_PollPointDigital    .getValue();    // Poll (with time stamp) the Digital Points list previously set
  short _CTMsgID_C_PollPointAnalogue   = (short)CTMsgID_C_PollPointAnalogue   .getValue();    // Poll (with time stamp) the Analogue Points list previously set
  short _CTMsgID_C_PollPointCounter    = (short)CTMsgID_C_PollPointCounter    .getValue();    // Poll (with time stamp) the Analogue Points list previously set
  short _CTMsgID_R_PollPointDigital    = (short)CTMsgID_R_PollPointDigital    .getValue();    // List of Digital Point values
  short _CTMsgID_R_PollPointAnalogue   = (short)CTMsgID_R_PollPointAnalogue   .getValue();    // List of Analogue Point values
  short _CTMsgID_R_PollPointCounter    = (short)CTMsgID_R_PollPointCounter    .getValue();    // List of Counter Point values
  short _CTMsgID_C_ModuleInfo          = (short)CTMsgID_C_ModuleInfo          .getValue();    // Request information for all the modules detected.
  short _CTMsgID_C_ModuleAlarm         = (short)CTMsgID_C_ModuleAlarm         .getValue();    // Module alarm request
  short _CTMsgID_C_ModuleClearAlarm    = (short)CTMsgID_C_ModuleClearAlarm    .getValue();    // Acknowledge Module alarms
  short _CTMsgID_C_ModuleDetail        = (short)CTMsgID_C_ModuleDetail        .getValue();    // Module detailed info request
  short _CTMsgID_C_ModuleCANStats      = (short)CTMsgID_C_ModuleCANStats      .getValue();    // Module CAN stats
  short _CTMsgID_R_ModuleInfo          = (short)CTMsgID_R_ModuleInfo          .getValue();    // List of all the modules detected.
  short _CTMsgID_R_ModuleAlarm         = (short)CTMsgID_R_ModuleAlarm         .getValue();    // Module alarm reply
  short _CTMsgID_R_ModuleDetail        = (short)CTMsgID_R_ModuleDetail        .getValue();    // Module detailed info reply
  short _CTMsgID_R_ModuleCANStats      = (short)CTMsgID_R_ModuleCANStats      .getValue();    // Module CAN stats
  short _CTMsgID_C_FWriteStart         = (short)CTMsgID_C_FWriteStart         .getValue();    // Start writing file Message
  short _CTMsgID_C_FWriteFragment      = (short)CTMsgID_C_FWriteFragment      .getValue();    // Write file chunk to the RTU Message
  short _CTMsgID_C_FWriteFinish        = (short)CTMsgID_C_FWriteFinish        .getValue();    // Finish file writing Message
  short _CTMsgID_R_FWriteFinish        = (short)CTMsgID_R_FWriteFinish        .getValue();    // Written file recap Message
  short _CTMsgID_C_FReadStart          = (short)CTMsgID_C_FReadStart          .getValue();    // Start reading file Message
  short _CTMsgID_C_FReadFragment       = (short)CTMsgID_C_FReadFragment       .getValue();    // Read file chunk from the RTU Message
  short _CTMsgID_C_FReadFileList        = (short)CTMsgID_C_FReadFileList       .getValue();    // Read file chunk from the RTU Message
  short _CTMsgID_C_FReadCancel         = (short)CTMsgID_C_FReadCancel         .getValue();    // Cancel file reading Message
  short _CTMsgID_R_FReadStart          = (short)CTMsgID_R_FReadStart          .getValue();    // Info of the file to be read Message
  short _CTMsgID_R_FReadFragment       = (short)CTMsgID_R_FReadFragment       .getValue();    // Fragment of file Message
  short _CTMsgID_R_FReadFileList       = (short)CTMsgID_R_FReadFileList       .getValue();    // Fragment of file Message
  short _CTMsgID_C_GetDate             = (short)CTMsgID_C_GetDate             .getValue();    // Get date and time Message
  short _CTMsgID_C_SetDate             = (short)CTMsgID_C_SetDate             .getValue();    // Get file data Message
  short _CTMsgID_R_GetDate             = (short)CTMsgID_R_GetDate             .getValue();    // Date and time Message
  short _CTMsgID_C_OnServiceMode       = (short)CTMsgID_C_OnServiceMode       .getValue();    // Put RTU back on service
  short _CTMsgID_C_OutServiceMode      = (short)CTMsgID_C_OutServiceMode      .getValue();    // Put RTU out of service
  short _CTMsgID_C_GetServiceMode      = (short)CTMsgID_C_GetServiceMode      .getValue();    // Get Service status
  short _CTMsgID_C_Reset               = (short)CTMsgID_C_Reset               .getValue();    // Reboot RTU
  short _CTMsgID_C_Restart             = (short)CTMsgID_C_Restart             .getValue();    // Restart RTU application
  short _CTMsgID_C_Shutdown            = (short)CTMsgID_C_Shutdown            .getValue();    // Shutdown RTU
  short _CTMsgID_C_Forced_Reset        = (short)CTMsgID_C_Forced_Reset        .getValue();    // Reboot RTU
  short _CTMsgID_C_Forced_Restart      = (short)CTMsgID_C_Forced_Restart      .getValue();    // Restart RTU application
  short _CTMsgID_C_Forced_Shutdown     = (short)CTMsgID_C_Forced_Shutdown     .getValue();    // Shutdown RTU
  short _CTMsgID_C_EraseCommissioningCache= (short)CTMsgID_C_EraseCommissioningCache.getValue();    // Shutdown RTU
  short _CTMsgID_C_ActivateConfig      = (short)CTMsgID_C_ActivateConfig      .getValue();    // Restart RTU application
  short _CTMsgID_C_RestoreConfig       = (short)CTMsgID_C_RestoreConfig       .getValue();    // Shutdown RTU
  short _CTMsgID_C_Register            = (short)CTMsgID_C_Register            .getValue();    // Commission modules
  short _CTMsgID_C_EraseConfig         = (short)CTMsgID_C_EraseConfig         .getValue();    // Erase RTU Configuration
  short _CTMsgID_C_FactoryReset        = (short)CTMsgID_C_FactoryReset        .getValue();    // RTU Factory reset
  short _CTMsgID_R_GetServiceMode      = (short)CTMsgID_R_GetServiceMode      .getValue();    // Service mode status
  short _CTMsgID_C_GetLastLog          = (short)CTMsgID_C_GetLastLog          .getValue();    // Get latest log entries Message
  short _CTMsgID_C_GetLogLevel         = (short)CTMsgID_C_GetLogLevel         .getValue();    // Get log level Message
  short _CTMsgID_C_SetLogLevel         = (short)CTMsgID_C_SetLogLevel         .getValue();    // Set log level Message
  short _CTMsgID_C_GetEventLog         = (short)CTMsgID_C_GetEventLog         .getValue();    // Get latest event log entries Message
  short _CTMsgID_C_ClearEventLog       = (short)CTMsgID_C_ClearEventLog       .getValue();    // Erase all events from the event log
  short _CTMsgID_C_BeginEventLog       = (short)CTMsgID_C_BeginEventLog       .getValue();    // Start to read event log from the older event entries
  short _CTMsgID_C_GetLogFileName      = (short)CTMsgID_C_GetLogFileName      .getValue();    // Get the log file name
  short _CTMsgID_R_GetLastLog          = (short)CTMsgID_R_GetLastLog          .getValue();    // Log entries list Message
  short _CTMsgID_R_GetLogLevel         = (short)CTMsgID_R_GetLogLevel         .getValue();    // Log level list Message
  short _CTMsgID_R_GetEventLog         = (short)CTMsgID_R_GetEventLog         .getValue();    // Event log entries list Message
  short _CTMsgID_R_GetLogFileName      = (short)CTMsgID_R_GetLogFileName      .getValue();    // Log file name
  short _CTMsgID_C_GetSysAPI           = (short)CTMsgID_C_GetSysAPI           .getValue();    // Get G3 System API revision
  short _CTMsgID_C_GetRTUInfo          = (short)CTMsgID_C_GetRTUInfo          .getValue();    // Get RTU info block Message
  short _CTMsgID_C_GetSDPVersion       = (short)CTMsgID_C_GetSDPVersion       .getValue();    // Get RTU info block Message
  short _CTMsgID_R_GetSDPVersion       = (short)CTMsgID_R_GetSDPVersion       .getValue();    // Get RTU info block Message
  short _CTMsgID_C_GetConfigInfo       = (short)CTMsgID_C_GetConfigInfo       .getValue();    // Get RTU info block Message
  short _CTMsgID_R_GetConfigInfo       = (short)CTMsgID_R_GetConfigInfo       .getValue();    // Get RTU info block Message
  short _CTMsgID_C_GetNetInfo          = (short)CTMsgID_C_GetNetInfo          .getValue();    // Get net info list Message
  short _CTMsgID_C_GetBatteryCfg       = (short)CTMsgID_C_GetBatteryCfg       .getValue();    // Get battery configuration
  short _CTMsgID_R_GetSysAPI           = (short)CTMsgID_R_GetSysAPI           .getValue();    // G3 System API Message
  short _CTMsgID_R_GetRTUInfo          = (short)CTMsgID_R_GetRTUInfo          .getValue();    // RTU info block Message
  short _CTMsgID_R_GetNetInfo          = (short)CTMsgID_R_GetNetInfo          .getValue();    // Net info list Message
  short _CTMsgID_R_GetBatteryCfg       = (short)CTMsgID_R_GetBatteryCfg       .getValue();    // Battery Configuration
  short _CTMsgID_C_UpgradeModeOff      = (short)CTMsgID_C_UpgradeModeOff      .getValue();    // Disable Upgrade mode
  short _CTMsgID_C_UpgradeModeOn       = (short)CTMsgID_C_UpgradeModeOn       .getValue();    // Enable Upgrade mode
  short _CTMsgID_C_UpgradeModule       = (short)CTMsgID_C_UpgradeModule       .getValue();    // Start a module upgrade procedure
  short _CTMsgID_C_SDPVersion          = (short)CTMsgID_C_SDPVersion          .getValue();    // Start a module upgrade procedure
  short _CTMsgID_C_GetUpgradeState     = (short)CTMsgID_C_GetUpgradeState     .getValue();    // Check state of upgrade procedure
  short _CTMsgID_C_EraseFirmware       = (short)CTMsgID_C_EraseFirmware       .getValue();    // Erase the firmware of the selected modules
  short _CTMsgID_R_GetUpgradeState     = (short)CTMsgID_R_GetUpgradeState     .getValue();    // Get Upgrade procedure state
  short _CTMsgID_R_EraseFirmware       = (short)CTMsgID_R_EraseFirmware       .getValue();    // Reply the firmware erase command with the erased modules
  short _CTMsgID_C_GetOLR              = (short)CTMsgID_C_GetOLR              .getValue();    // Get Off/local/remote state
  short _CTMsgID_C_SetOLR              = (short)CTMsgID_C_SetOLR              .getValue();    // Set Off/local/remote state
  short _CTMsgID_C_OperateSwitch       = (short)CTMsgID_C_OperateSwitch       .getValue();    // Operate a switch
  short _CTMsgID_C_OperateCLogic       = (short)CTMsgID_C_OperateCLogic       .getValue();    // Start Control Logic Operation
  short _CTMsgID_R_GetOLR              = (short)CTMsgID_R_GetOLR              .getValue();    // Off/local/remote state
  short _CTMsgID_C_CheckAlive          = (short)CTMsgID_C_CheckAlive          .getValue();    // Check if RTU is alive
  short _CTMsgID_C_UserLogin           = (short)CTMsgID_C_UserLogin           .getValue();    // User login
  short _CTMsgID_C_GetMDAlgorithm      = (short)CTMsgID_C_GetMDAlgorithm      .getValue();    // User login
  short _CTMsgID_R_MDAlgorithm         = (short)CTMsgID_R_MDAlgorithm         .getValue();    // User login
  short _CTMsgID_R_CheckAlive          = (short)CTMsgID_R_CheckAlive          .getValue();    // Alive response
  short _CTMsgID_R_UserLogin           = (short)CTMsgID_R_UserLogin           .getValue();    // User information
  short _CTMsgID_C_GetChannel          = (short)CTMsgID_C_GetChannel          .getValue();    // Poll channel raw value
  short _CTMsgID_C_GetPointDigital     = (short)CTMsgID_C_GetPointDigital     .getValue();    // Get  (with time stamp) the Digital Points status
  short _CTMsgID_C_GetPointAnalogue    = (short)CTMsgID_C_GetPointAnalogue    .getValue();    // Get  (with time stamp) the Analogue Points status
  short _CTMsgID_C_GetPointCounter     = (short)CTMsgID_C_GetPointCounter     .getValue();    // Get  (with time stamp) the Counter Points status
  short _CTMsgID_R_GetChannel          = (short)CTMsgID_R_GetChannel          .getValue();    // Replied channel raw value
  short _CTMsgID_R_GetPointDigital     = (short)CTMsgID_R_GetPointDigital     .getValue();    // List of Digital Point values with time stamp
  short _CTMsgID_R_GetPointAnalogue    = (short)CTMsgID_R_GetPointAnalogue    .getValue();    // List of Analogue Point values with time stamp
  short _CTMsgID_R_GetPointCounter     = (short)CTMsgID_R_GetPointCounter     .getValue();    // List of Counter Point values with time stamp
  short _CTMsgID_R_AckNack             = (short)CTMsgID_R_AckNack				      .getValue();    // Ack/Nack
  short _CTMsgID_C_EnableDebugMode     = (short)CTMsgID_C_EnableDebugMode.getValue();     // Enable RTU debug
  short _CTMsgID_C_DisableDebugMode    = (short)CTMsgID_C_DisableDebugMode.getValue();     // Enable RTU debug
  short _CTMsgID_C_GetDebugMode        = (short)CTMsgID_C_GetDebugMode.getValue();     // Enable RTU debug
  short _CTMsgID_R_GetDebugMode        = (short)CTMsgID_R_GetDebugMode.getValue();     // Enable RTU debug

  /* Payload Size (bytes) of G3 reply message */
  int PLSize_AckNack = 1;

  int PLSize_C_CheckAlive = 0;
  int PLSize_R_CheckAlive = 12;

  int PLSize_C_Login = 27; // >=
  int PLSize_R_Login = 3;

  int PLSize_C_ModuleRef = 6;

  int PLSize_C_SDPVersion = 1024;

  int PLSize_R_UpgradeState = 2;

  int PLSize_R_EventEntry = 20; // *n

  int PLSize_R_LogEntry = 12; // >=
  int PLSize_R_LogFileName = 0; // >=

  int PLSize_C_LogLevel = 2;
  int PLSize_R_LogLevel = 2; // *n

  int PLSize_C_SwitchTest = 3;
  int PLSize_C_CLogicTest = 4;

  int PLSize_C_DateTime = 34;
  int PLSize_R_DateTime = 34;

  int PLSize_C_SetOLRState = 1;
  int PLSize_R_OLRState = 1;

  int PLSize_C_DisableService = 1;
  int PLSize_R_ServiceState = 2;

  int PLSize_C_SelectPoint = 4; // *n
  int PLSize_R_APointPolling = 17; // *n
  int PLSize_R_DPointPolling = 13; // *n
  int PLSize_R_CPointPolling = 30; // *n

  int PLSize_R_ConfigAPI = 24;

  int PLSize_R_NetInfo = 132; // *n
  int PLSize_R_RTUInfo = 610; // >=
  int PLSize_R_ConfigInfo = 64;

  int PLSize_C_ModuleAlarm = 2;
  int PLSize_R_ModuleAlarm = 9; // *n

  int PLSize_C_ModuleDetail = 2;
  int PLSize_R_ModuleDetail = 48;

  int PLSize_C_ModuleCANStat = 2;
  int PLSize_R_ModuleCANStat = 38; // *n

  int PLSize_R_ModuleInfo = 54; // *n

  int PLSize_C_ChannelPolling = 4; // *n
  int PLSize_R_ChannelPolling = 9; // *n

  int PLSize_C_FileWriteStart = 14;
  int PLSize_C_FileWriteFrag = 8;
  int PLSize_C_FileWriteFinish = 1;
  int PLSize_R_FileWriteInfo = 12;
  int PLSize_C_FileReadStart = 1;
  int PLSize_R_FileReadInfo = 8;
  int PLSize_R_FileReadFrag = 8; // >=
  
  // @formatter:on GEN-END:
}
