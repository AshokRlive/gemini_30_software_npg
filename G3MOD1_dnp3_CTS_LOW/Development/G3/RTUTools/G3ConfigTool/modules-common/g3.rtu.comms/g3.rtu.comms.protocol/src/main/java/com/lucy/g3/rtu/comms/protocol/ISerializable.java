/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;


/**
 * The Interface ISerializable.
 */
public interface ISerializable {

  byte[] toBytes();
  
  void parseBytes(byte[] bytes) throws SerializationException;
  
  final class Buffer {
    private Buffer(){}
    
    /**
     * Wrap bytes in a byte buffer ( which has compatible ByteOrder with G3 RTU).
     */
    public static final ByteBuffer createByteBuffer(byte[] bytes) {
      ByteBuffer buf = ByteBuffer.wrap(bytes);
      buf.order(G3Protocol.RTU_BYTE_ORDER);
      return buf;
    }

    /**
     * Allocate a byte buffer ( which has compatible ByteOrder with G3 RTU).
     */
    public static final ByteBuffer createByteBuffer(int capacity) {
      ByteBuffer buf = ByteBuffer.allocate(capacity);
      buf.order(G3Protocol.RTU_BYTE_ORDER);
      return buf;
    }
  }
}

