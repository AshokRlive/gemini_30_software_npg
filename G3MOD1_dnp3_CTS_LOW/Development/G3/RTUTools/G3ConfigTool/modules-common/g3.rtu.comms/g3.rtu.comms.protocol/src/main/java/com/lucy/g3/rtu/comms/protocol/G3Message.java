/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.nio.ByteBuffer;
import java.util.Arrays;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * <code>G3Message</code> defines the structure of message that can be
 * sent/received to/from G3 RTU.
 *
 * @see G3 Configuration Tool Communication Protocol).doc
 */
public final class G3Message implements IG3Message{

  private final G3MsgHeader header;
  private final IG3MsgPayload payload;
  
  private  static final IG3MsgPayload EMPTY_PAYLOAD = new RawG3MsgPayload(new byte[0]);

  public G3Message(short msgType, short msgID, short requestID, short sessionID, IG3MsgPayload payload) {
    this(new G3MsgHeader(msgType, msgID, requestID, sessionID), payload);
  }

  public G3Message(short msgType, short msgID, short requestID, short sessionID) {
    this(new G3MsgHeader(msgType, msgID, requestID, sessionID), null);
  }

  public G3Message(G3MsgHeader header) {
    this(header, null);
  }

  public G3Message(G3MsgHeader header, IG3MsgPayload payload) {
    if(header == null)
      throw new NullPointerException("header must not be null");
    this.header = header;
    this.payload = payload == null ? EMPTY_PAYLOAD : payload;
  }

  public int size() {
    return header.getSize() + (payload == null ? 0 : payload.size());
  }

  @Override
  public String name() {
    return header.getName();
  }

  @Override
  public G3MsgHeader getHeader() {
    return header;
  }

  @Override
  public IG3MsgPayload getPayload() {
    return payload;
  }

  @Override
  public byte[] toBytes() {
    byte[] hbytes = header.toBytes();
    byte[] plbytes = payload.toBytes();
    ByteBuffer buf = IG3Message.Buffer.createByteBuffer(hbytes.length + (plbytes == null ? 0 :plbytes.length));
    buf.put(hbytes);
    if(plbytes != null)
      buf.put(plbytes);
  
    return buf.array();
  }

  @Override
  public void parseBytes(byte[] bytes) throws SerializationException {
    header.parseBytes(Arrays.copyOfRange(bytes, 0, G3Protocol.CTMsg_HeaderSize));
    payload.parseBytes(Arrays.copyOfRange(bytes, G3Protocol.CTMsg_HeaderSize,bytes.length));
  }

}
