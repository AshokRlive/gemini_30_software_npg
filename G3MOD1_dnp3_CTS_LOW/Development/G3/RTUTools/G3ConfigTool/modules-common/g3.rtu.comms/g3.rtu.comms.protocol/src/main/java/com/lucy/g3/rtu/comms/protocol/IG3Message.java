/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;


/**
 * The Interface IG3Message.
 */
public interface IG3Message extends ISerializable {

  G3MsgHeader getHeader();
  
  IG3MsgPayload getPayload();

  String name();
  
}

