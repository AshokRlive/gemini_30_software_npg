/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.rtu.comms.protocol.exceptions;
/**
 * An <code>UnexpectedMsgTypeException</code> is thrown if the type of a
 * received message is not what expected.
 */
public final class UnexpectedMsgTypeException extends ProtocolException {

  public UnexpectedMsgTypeException(String msgName,
      short expectedMsgType, short actualMsgType) {
    super(String.format(
        "%s: unexpected replied Message Type! Expected:%d, Actual %d",
        msgName, expectedMsgType, actualMsgType));
  }
}