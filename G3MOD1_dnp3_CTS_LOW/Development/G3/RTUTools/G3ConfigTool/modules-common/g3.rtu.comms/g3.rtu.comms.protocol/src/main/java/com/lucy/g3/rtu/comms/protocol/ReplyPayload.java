/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;


/**
 * The Class ReplyPayload.
 */
public abstract class ReplyPayload extends AbstractG3MsgPayload {
  
  /**
   * 
   * @param type 
   * @param size minimum size for VARIABLE_SIZE.
   */
  public ReplyPayload(PayloadType type, int size) {
    super(type, size);
  }

  public ReplyPayload(int size) {
    super(size);
  }
  
  @Override
  public byte[] toBytes() {
    throw new UnsupportedOperationException("Not implemented");
  }

}

