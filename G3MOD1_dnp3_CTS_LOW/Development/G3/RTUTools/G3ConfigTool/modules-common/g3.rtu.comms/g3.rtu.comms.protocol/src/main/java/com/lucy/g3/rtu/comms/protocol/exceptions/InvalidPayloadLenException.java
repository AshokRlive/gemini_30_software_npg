/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/


package com.lucy.g3.rtu.comms.protocol.exceptions;
/**
 * An <code>InvalidMsgLengthException</code> is thrown if the received message
 * size is not what we expected.
 */
public final class InvalidPayloadLenException extends InvalidPayloadException{

  public InvalidPayloadLenException(String msgName, String error) {
    super(String.format("%s - %s", msgName,error));
  }

  public InvalidPayloadLenException(String msgName, int expectedLen, int actualLen) {
    super(String.format("%s - Invalid payload length. Expected:%d,Actual:%d",
        msgName, expectedLen, actualLen));
  }
}