/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

public class PL_Reply_FileName extends ReplyPayload {

  private String RTULogFileName;


  public PL_Reply_FileName(int payloadSize) {
    super(PayloadType.MINIMUM_SIZE, payloadSize);
  }

  public String getRTULogFileName() {
    return RTULogFileName;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    if (payload != null && payload.length > 0) {
      ByteBuffer buf = createByteBuffer(payload);
      RTULogFileName = new String(buf.array());
    }
  }
}