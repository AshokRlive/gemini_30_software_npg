/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;

/**
 * <code>EndOfFileGetException</code> is thrown when reaching the end of a file
 * that is being read.
 */
public class EndOfFileGetException extends Exception {

  public EndOfFileGetException() {
  }

  public EndOfFileGetException(String message) {
    super(message);
  }

  public EndOfFileGetException(Throwable cause) {
    super(cause);
  }

  public EndOfFileGetException(String message, Throwable cause) {
    super(message, cause);
  }

}
