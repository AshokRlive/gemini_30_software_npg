/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;

/**
 * <code>ProtocolException</code> is thrown if a invalid upgrade state is
 * reported by RTU.
 */
public class UpgradeStateErrorException extends Exception {

  public UpgradeStateErrorException(String message) {
    super(message);
  }

}