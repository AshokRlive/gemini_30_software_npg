/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_ACKNACKCODE;

/**
 * A <code>ProtocolException</code> is thrown if a received G3 message doesn't
 * comply with G3 Protocol.
 */
public abstract class ProtocolException extends SerializationException {

  /**
   * Constructs a new exception instance with the specified detail message. The
   * cause is not initialized.
   *
   * @param message
   *          the detail message which is saved for later retrieval by the
   *          {@link #getMessage()} method.
   */
  public ProtocolException(String message) {
    super(message);
  }

  /**
   * Constructs a new exception instance with the specified detail message and
   * cause.
   *
   * @param message
   *          the detail message which is saved for later retrieval by the
   *          {@link #getMessage()} method.
   * @param cause
   *          the cause which is saved for later retrieval by the
   *          {@link #getCause()} method. A {@code null} value is permitted, and
   *          indicates that the cause is nonexistent or unknown.
   */
  public ProtocolException(String message, Throwable cause) {
    super(message, cause);
  }


  /**
   * An <code>NackException</code> is thrown if the received message is a NACK
   * message.
   */
  public static final class NackException extends ProtocolException {

    public final CTH_ACKNACKCODE nackCode;


    public NackException(CTH_ACKNACKCODE nackCode) {
      super(nackCode.getDescription());
      this.nackCode = nackCode;
    }
  }







}