/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/


package com.lucy.g3.rtu.comms.protocol.exceptions;
/**
 * An <code>InvalidMsgLengthException</code> is thrown if the received message
 * size is not what we expected.
 */
public final class InvalidMsgLengthException extends ProtocolException {

  public InvalidMsgLengthException(String message) {
    super(message);
  }

  public InvalidMsgLengthException(String msgName, int expectedLen, int actualLen) {
    this(String.format("%s - Invalid message length. Expected:%d,Actual:%d",
        msgName, expectedLen, actualLen));
  }
}