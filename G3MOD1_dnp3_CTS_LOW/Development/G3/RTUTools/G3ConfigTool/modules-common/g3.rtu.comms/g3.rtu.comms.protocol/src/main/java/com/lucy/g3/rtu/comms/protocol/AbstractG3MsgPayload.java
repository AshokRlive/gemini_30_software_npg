/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import org.apache.log4j.Logger;

/**
 * Basic G3 Message Payload.
 */
public abstract class AbstractG3MsgPayload implements IG3MsgPayload {

  protected Logger log = Logger.getLogger(getClass());

  private final PayloadType type;
  private final int size;


  public AbstractG3MsgPayload(PayloadType type, int size) {
    if (size < 0)
      throw new IllegalArgumentException("size must be >= 0");

    if (type == null)
      throw new IllegalArgumentException("type must not be null");

    this.type = type;
    this.size = size;
  }

  public AbstractG3MsgPayload(int size) {
    this(PayloadType.FIXED_SIZE, size);
  }


  @Override
  public PayloadType type() {
    return type;
  }

  @Override
  public int size() {
    return size;
  }

  protected static final ByteBuffer createByteBuffer(byte[] bytes) {
    return ISerializable.Buffer.createByteBuffer(bytes);
  }

  protected static final ByteBuffer createByteBuffer(int capacity) {
    return ISerializable.Buffer.createByteBuffer(capacity);
  }

  protected static String bytesToString(byte[] bytes) {
    String str = null;
    try {
      str = new String(bytes, G3Protocol.CHAR_SET);
    } catch (UnsupportedEncodingException e) {
      str = new String(bytes);
      Logger.getLogger(AbstractG3MsgPayload.class).error(e.getMessage());
    }
    return str.trim();
  }

  protected static String bytesToStringASCII(byte[] bytes) {
    String str = null;
    try {
      str = new String(bytes, "ASCII");
    } catch (UnsupportedEncodingException e) {
      str = new String(bytes);
      Logger.getLogger(AbstractG3MsgPayload.class).error(e.getMessage());
    }
    return str.trim();
  }

  protected static byte[] stringToBytes(String s) {
    byte[] bytes = null;
    try {
      bytes = s.getBytes(G3Protocol.CHAR_SET);
    } catch (UnsupportedEncodingException e) {
      bytes = s.getBytes();
      Logger.getLogger(AbstractG3MsgPayload.class).error(e.getMessage());
    }
    return bytes;
  }

}
