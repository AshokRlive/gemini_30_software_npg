/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;
/**
 * A <code>MismatchedFragmentSizeException</code> is thrown if the received
 * file fragment size is not as expected.
 */
public class UnexpectedFragmentSizeException extends ProtocolException {

  public UnexpectedFragmentSizeException(String msgName, long expectedLen, long actualLen) {
    super(String.format("%s - Invalid file fragment size. Expected:%d,Actual:%d",
        msgName, expectedLen, actualLen));
  }

}