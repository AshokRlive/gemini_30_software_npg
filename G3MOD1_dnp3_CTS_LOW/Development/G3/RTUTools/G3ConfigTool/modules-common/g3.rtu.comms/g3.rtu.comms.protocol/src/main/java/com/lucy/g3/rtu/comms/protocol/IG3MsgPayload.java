/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

/**
 *
 */
public interface IG3MsgPayload extends ISerializable {

  int size();

  PayloadType type();
  
  /**
   * The Enum PayloadType.
   */
  enum PayloadType {
    /** Indicates that payload has fixed size. */
    FIXED_SIZE,
    /** Indicates that payload has minimum size. */
    MINIMUM_SIZE,
    /** Indicates that payload has multiple objects. The size is allowed to be 0 */
    FRAGMENTED,
    RAW,
  }


}

