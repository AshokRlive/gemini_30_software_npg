/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.nio.ByteBuffer;

public class PL_Request_String extends RequestPayload {
  private final String requestStr;
  public PL_Request_String(String requestStr) {
    super(requestStr.length());
    this.requestStr= requestStr;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(requestStr.length());
    buf.put(stringToBytes(requestStr));

    return buf.array();
  }

}

