/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;


/**
 * The Class RequestPayload.
 */
public abstract class RequestPayload extends AbstractG3MsgPayload {

  public RequestPayload(PayloadType type, int size) {
    super(type, size);
  }

  public RequestPayload(int size) {
    super(size);
  }
  
  @Override
  public void parseBytes(byte[] bytes) throws SerializationException {
    throw new UnsupportedOperationException("Not implemented");
  }
}

