/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

public class PL_Reply_String extends ReplyPayload {

  private String str;


  public PL_Reply_String() {
    super(PayloadType.MINIMUM_SIZE, 0);
  }

  public String getStr() {
    return str;
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {

    if (payload != null && payload.length > 0) {
      ByteBuffer buf = createByteBuffer(payload);
      str = new String(buf.array());
    }
  }
}