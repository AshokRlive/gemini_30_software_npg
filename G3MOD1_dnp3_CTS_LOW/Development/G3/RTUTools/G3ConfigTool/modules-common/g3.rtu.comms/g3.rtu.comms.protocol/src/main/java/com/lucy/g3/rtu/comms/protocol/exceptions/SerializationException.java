/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;


/**
 * The Class SerializationException.
 */
public class SerializationException extends Exception{
  public SerializationException(String message) {
    super(message);
  }


  public SerializationException(String message, Throwable cause) {
    super(message, cause);
  }

  
}
