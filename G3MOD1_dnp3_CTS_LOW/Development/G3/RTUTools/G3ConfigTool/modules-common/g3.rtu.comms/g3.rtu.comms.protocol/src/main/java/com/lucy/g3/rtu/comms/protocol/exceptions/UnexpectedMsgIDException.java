/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;

/**
 * An <code>UnexpectedMsgIDException</code> is thrown if the message id in
 * received message is different from the one in sent message.
 */
public class UnexpectedMsgIDException extends ProtocolException {

  public UnexpectedMsgIDException(String msgName, short expectedMsgID,
      short actualMsgID) {
    super(String.format(
        "%s: unexpected replied Message ID! Expected:%d, Actual %d",
        msgName, expectedMsgID, actualMsgID));
  }
}