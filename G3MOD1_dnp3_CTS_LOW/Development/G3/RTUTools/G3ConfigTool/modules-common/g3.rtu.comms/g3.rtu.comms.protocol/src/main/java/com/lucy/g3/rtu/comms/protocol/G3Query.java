/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

// GEN-BEGIN:
/**
 * {@linkplain G3Query} contains a request message to be sent to G3RTU, along
 * with a replied message expected to be received from G3RTU.
 */
public class G3Query {
  public final static int DEFAULT_RETRIES = 3;
  /**
   * The message to be sent as a request.
   */
  public final IG3Message requestMsg;

  /**
   * Expected reply message, maybe <code>null</code> if there is no reply
   * expected for this request.
   */
  public final IG3Message replyMsg;
  
  private int retries = DEFAULT_RETRIES;


  public G3Query(IG3Message requestMsg, IG3Message replyMsg) {
    this(requestMsg, replyMsg, DEFAULT_RETRIES);
  }
  
  public G3Query(IG3Message requestMsg, IG3Message replyMsg, int retries) {
    if(requestMsg == null)
      throw new NullPointerException("requestMsg must not be null");
    this.retries = retries;
    this.requestMsg = requestMsg;
    this.replyMsg = replyMsg;
  }


  
  public int getRetries() {
    return retries;
  }
  
  public void setRetries(int retries) {
      this.retries = retries;
  }

}
//GEN-END: