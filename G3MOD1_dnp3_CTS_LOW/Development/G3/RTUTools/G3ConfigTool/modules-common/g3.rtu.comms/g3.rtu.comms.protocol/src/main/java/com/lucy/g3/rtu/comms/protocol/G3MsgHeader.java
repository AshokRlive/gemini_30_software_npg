/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol.ACK_NACK_MSG_ID;
import static com.lucy.g3.rtu.comms.protocol.G3Protocol._CTMsgID_R_AckNack;
import static com.lucy.g3.rtu.comms.protocol.ISerializable.Buffer.createByteBuffer;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Date;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Diagnostic;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_FileRead;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_FileWrite;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Log;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Login;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Module;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_PointData;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_RTUControl;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_RTUInfo;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Testing;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgType_Upgrade;
import static com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsg_HeaderSize;

import java.nio.ByteBuffer;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Common;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Date;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Diagnostic;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileRead;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_FileWrite;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Log;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Login;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Module;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_PointData;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUControl;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_RTUInfo;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Testing;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTMsgID_Upgrade;

/**
 * The header of {@linkplain G3Message}.
 */
public final class G3MsgHeader implements ISerializable{

  private short msgType;
  private short msgID;
  private short sessionID;
  private short requestID;
  
  public short type() {
    return msgType;
  }
  
  public short id() {
    return msgID;
  }
  
  public short session() {
    return sessionID;
  }

  public short requestId() {
    return requestID;
  }

  public G3MsgHeader() {
    
  }
  
  public G3MsgHeader(short msgType, short msgID, short requestID, short sessionID) {
    this.msgType = msgType;
    this.msgID = msgID;
    this.requestID = requestID;
    this.sessionID = sessionID;
  }

  @Override
  public byte[] toBytes() {
    ByteBuffer buf = createByteBuffer(CTMsg_HeaderSize);
    buf.putShort(msgType);
    buf.putShort(msgID);
    buf.putShort(requestID);
    buf.putShort(sessionID);
    return buf.array();
  }

  public boolean isAckNack() {
    return msgID == ACK_NACK_MSG_ID;
  }

  public int getSize() {
    return G3Protocol.CTMsg_HeaderSize;
  }

  public static G3MsgHeader createFromBytes(byte[] header) {
    assert header != null && header.length == CTMsg_HeaderSize;

    ByteBuffer buf = createByteBuffer(header);
    
    short msgType = buf.getShort(); // 0-1 bytes
    short msgID = buf.getShort(); // 2-3 bytes
    short requestID = buf.getShort(); // 4-5 bytes
    short sessionID = buf.getShort(); // 6-7 bytes
    return new G3MsgHeader(msgType, msgID, requestID, sessionID);
  }

  public String getName() {
    // AckNack is a common ID which is defined CTMsgID_Common
    if (msgID == _CTMsgID_R_AckNack) {
      return CTMsgID_Common.forValue(msgID).getDescription();
    }

    switch (msgType) {
    case CTMsgType_PointData:
      return CTMsgID_PointData.forValue(msgID).getDescription();
    case CTMsgType_Module:
      return CTMsgID_Module.forValue(msgID).getDescription();
    case CTMsgType_FileWrite:
      return CTMsgID_FileWrite.forValue(msgID).getDescription();
    case CTMsgType_FileRead:
      return CTMsgID_FileRead.forValue(msgID).getDescription();
    case CTMsgType_Date:
      return CTMsgID_Date.forValue(msgID).getDescription();
    case CTMsgType_RTUControl:
      return CTMsgID_RTUControl.forValue(msgID).getDescription();
    case CTMsgType_Log:
      return CTMsgID_Log.forValue(msgID).getDescription();
    case CTMsgType_RTUInfo:
      return CTMsgID_RTUInfo.forValue(msgID).getDescription();
    case CTMsgType_Upgrade:
      return CTMsgID_Upgrade.forValue(msgID).getDescription();
    case CTMsgType_Testing:
      return CTMsgID_Testing.forValue(msgID).getDescription();
    case CTMsgType_Login:
      return CTMsgID_Login.forValue(msgID).getDescription();
    case CTMsgType_Diagnostic:
      return CTMsgID_Diagnostic.forValue(msgID).getDescription();
    default:
      return String.format("Message[%d:%d]", msgType, msgID);
    }
  }

  @Override
  public void parseBytes(byte[] bytes) throws SerializationException {
    if(bytes == null || bytes.length != G3Protocol.CTMsg_HeaderSize)
      throw new SerializationException("Invalid header length!");
    
    ByteBuffer buf = createByteBuffer(bytes);
    msgType = buf.getShort(); // 0-1 bytes
    msgID = buf.getShort(); // 2-3 bytes
    requestID = buf.getShort(); // 4-5 bytes
    sessionID = buf.getShort(); // 6-7 bytes
  }
}