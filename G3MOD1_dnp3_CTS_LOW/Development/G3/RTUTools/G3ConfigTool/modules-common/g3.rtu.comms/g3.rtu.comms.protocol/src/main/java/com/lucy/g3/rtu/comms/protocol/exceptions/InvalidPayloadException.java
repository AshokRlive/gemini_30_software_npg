/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol.exceptions;

/**
 * An <code>InvalidPayloadException</code> is thrown if the payload of a
 * received message cannot be parsed.
 */
public class InvalidPayloadException extends ProtocolException {

  public InvalidPayloadException(String message) {
    super(message);
  }
}