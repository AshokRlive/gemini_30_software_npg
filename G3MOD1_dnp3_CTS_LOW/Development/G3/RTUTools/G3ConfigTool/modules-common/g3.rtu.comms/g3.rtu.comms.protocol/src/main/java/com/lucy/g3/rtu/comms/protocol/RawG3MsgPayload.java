/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.protocol;

import java.util.Arrays;

import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * Raw Payload.
 */
public final class RawG3MsgPayload implements IG3MsgPayload {

  private final byte[] rawPayload;


  public RawG3MsgPayload(byte[] rawPayload) {
    if (rawPayload == null)
      throw new IllegalArgumentException("rawPayload must not be null");

    this.rawPayload = rawPayload;
  }

  @Override
  public byte[] toBytes() {
    return Arrays.copyOf(rawPayload, rawPayload.length);
  }

  @Override
  public void parseBytes(byte[] payload) throws SerializationException {
    // Do nothing
  }

  @Override
  public int size() {
    return rawPayload.length;
  }

  @Override
  public PayloadType type() {
    return PayloadType.FIXED_SIZE;
  }
}