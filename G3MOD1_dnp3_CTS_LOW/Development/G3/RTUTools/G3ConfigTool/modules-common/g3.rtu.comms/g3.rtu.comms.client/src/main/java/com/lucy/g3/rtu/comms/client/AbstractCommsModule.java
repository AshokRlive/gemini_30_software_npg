/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import java.io.IOException;

import com.lucy.g3.common.bean.Bean;
import com.lucy.g3.rtu.comms.protocol.G3Message;
import com.lucy.g3.rtu.comms.protocol.G3Query;
import com.lucy.g3.rtu.comms.protocol.IG3Message;
import com.lucy.g3.rtu.comms.protocol.IG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * A JavaBean class for fetching and storing the real-time data of RTU by
 * communicating with RTU.
 */
public abstract class AbstractCommsModule extends Bean {

  /**
   * Non-available text.
   */
  public static final String TEXT_NA = "n/a";

  private final IClient client;


  public AbstractCommsModule(IClient client) {
    if (client == null)
      throw new NullPointerException("client must not be null");
    
    this.client = client;
  }
  
  protected final void send(
      short msgType,
      short msgID_send,
      short msgID_reply,
      IG3MsgPayload sendPL,
      IG3MsgPayload recvPL) throws IOException, SerializationException {
    send(msgType, msgID_send, msgID_reply, sendPL, recvPL, G3Query.DEFAULT_RETRIES);
  }
  
  protected final void send(
          short msgType,
          short msgID_send,
          short msgID_reply,
          IG3MsgPayload sendPL,
          IG3MsgPayload recvPL,
          int retries) throws IOException, SerializationException {
    
    short requestId = client.getRequestID();
    short sessionId = client.getSession().getSessionID();
    
    IG3Message send = new G3Message(msgType, msgID_send, requestId, sessionId, sendPL);
    IG3Message reply = new G3Message(msgType, msgID_reply, requestId, sessionId, recvPL);
    
    client.send(new G3Query(send, reply, retries));
  }
  
  protected IClient getClient() {
    return client;
  }
  
  protected CTH_RUNNINGAPP getState(){
    return (CTH_RUNNINGAPP) getClient().getSession().getData(Session.SESSSION_DATA_RTU_RUNNING_MODE);
  }
}
