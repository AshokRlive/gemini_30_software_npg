/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import java.io.File;

import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;


/**
 * The Class AbstractCommsTask.
 */
public abstract class AbstractCommsTask {
  private final ICommsTaskInvoker invoker;
  
  public AbstractCommsTask(ICommsTaskInvoker invoker) {
    if(invoker != null)
    this.invoker = invoker;
    else
      this.invoker = new DummyInvoker();
  }
  
  protected boolean isCancelled() {
    return invoker.isCancelled();
  }
  
  protected void setProgress(int progress) {
    this.invoker.setProgress(progress);
  }
  
  protected boolean shouldOverrite(File file) {
    return invoker.confirmOverrite(file);
  }
  
  protected void setMessage(String message) {
    this.invoker.setMessage(message);
  }
  
  protected abstract void run() throws Exception;
  
  private static class DummyInvoker implements ICommsTaskInvoker{

    @Override
    public boolean isCancelled() {
      return false;
    }

    @Override
    public void setProgress(int progress) {
    }

    @Override
    public void setMessage(String message) {
    }

    @Override
    public boolean confirmOverrite(File dest) {
      return true;
    }
  }
}

