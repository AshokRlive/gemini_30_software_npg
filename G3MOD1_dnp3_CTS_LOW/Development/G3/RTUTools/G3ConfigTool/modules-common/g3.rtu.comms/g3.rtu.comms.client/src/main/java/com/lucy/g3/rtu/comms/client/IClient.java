/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import java.io.IOException;

import com.lucy.g3.rtu.comms.datalink.DataLink;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.G3Query;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;

/**
 * Interface of component for sending/receiving G3 query.
 */
public interface IClient extends G3Protocol {
  /**
   * Send a query to remote RTU.
   */
  void send(G3Query g3Query) throws IOException, SerializationException;

  DataLink getDataLink();

  Session getSession();

  short getRequestID();
}
