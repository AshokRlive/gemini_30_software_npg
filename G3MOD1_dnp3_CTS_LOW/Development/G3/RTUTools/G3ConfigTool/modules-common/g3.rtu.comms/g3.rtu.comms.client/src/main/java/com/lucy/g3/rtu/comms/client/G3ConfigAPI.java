/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import static com.lucy.g3.rtu.comms.protocol.G3Protocol.RTU_BYTE_ORDER;
import static com.lucy.g3.rtu.comms.protocol.ISerializable.Buffer.createByteBuffer;

import java.nio.ByteBuffer;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.xml.gen.common.versions;

/**
 * This class stores the version information used for checking the compatibility
 * between ConfigTool and RTU.
 */
public class G3ConfigAPI {  
  
  /** The current API of this configuration tool. */
  public final static G3ConfigAPI CURRENT_API = new G3ConfigAPI(
      versions.G3_CONFIG_SYSTEM_API_MAJOR,
      versions.G3_CONFIG_SYSTEM_API_MINOR,
      versions.SCHEMA_MAJOR,
      versions.SCHEMA_MINOR,
      versions.MODULE_SYSTEM_API_VER_MAJOR,
      versions.MODULE_SYSTEM_API_VER_MINOR);
  
  //GEN-BEGIN: turn off checkstyle
  public final int G3ProtocolMajor;
  public final int G3ProtocolMinor;
  public final int SchemaVerMajor;
  public final int SchemaVerMinor;
  public final int ModuleProtocolMajor;
  public final int ModuleProtocolMinor;
  //GEN-END: turn on checkstyle

  public G3ConfigAPI() {
    this(0, 0, 0, 0, 0, 0);
  }

  public G3ConfigAPI(int g3ProtocolMajor, int g3ProtocolMinor, int schemaVerMajor, int schemaVerMinor,
      int moduleProtocolMajor, int moduleProtocolMinor) {
    G3ProtocolMajor = g3ProtocolMajor;
    G3ProtocolMinor = g3ProtocolMinor;
    SchemaVerMajor = schemaVerMajor;
    SchemaVerMinor = schemaVerMinor;
    ModuleProtocolMajor = moduleProtocolMajor;
    ModuleProtocolMinor = moduleProtocolMinor;
  }

  public String getG3ProtocolVersion() {
    return G3ProtocolMajor + "." + G3ProtocolMinor;
  }

  public String getG3SchemaVersion() {
    return SchemaVerMajor + "." + SchemaVerMinor;
  }

  public String getModuleProtocollVersion() {
    return ModuleProtocolMajor + "." + ModuleProtocolMinor;
  }

  public String toHTML() {
    return "<ul><li>G3 Protocol: " + G3ProtocolMajor + "." + G3ProtocolMinor
        + "</li><li>Schema Version: " + SchemaVerMajor + "." + SchemaVerMinor
        + "</li><li>Module Protocol:" + ModuleProtocolMajor + "." + ModuleProtocolMinor + "</li></ul>";
  }

  public byte[] getBytes() {
    ByteBuffer buf = ByteBuffer.allocate(G3Protocol.PLSize_R_ConfigAPI);
    buf.order(RTU_BYTE_ORDER);

    buf.putInt(G3ProtocolMajor);
    buf.putInt(G3ProtocolMinor);
    buf.putInt(SchemaVerMajor);
    buf.putInt(SchemaVerMinor);
    buf.putInt(ModuleProtocolMajor);
    buf.putInt(ModuleProtocolMinor);

    return buf.array();
  }

  public static G3ConfigAPI parseBytes(byte[] data) {
    if (data != null && data.length == G3Protocol.PLSize_R_ConfigAPI) {
      ByteBuffer buf = createByteBuffer(data);

      int g3ProtocolMajor = buf.getInt();
      int g3ProtocolMinor = buf.getInt();
      int schemaVerMajor = buf.getInt();
      int schemaVerMinor = buf.getInt();
      int moduleProtocolMajor = buf.getInt();
      int moduleProtocolMinor = buf.getInt();

      return new G3ConfigAPI(g3ProtocolMajor, g3ProtocolMinor, schemaVerMajor,
          schemaVerMinor, moduleProtocolMajor, moduleProtocolMinor);

    } else {
      Logger.getLogger(G3ConfigAPI.class).error("Fail to create ConfigAPI from data: invalid data size");
      return null;
    }
  }
  

  public static String checkEquals(G3ConfigAPI source, G3ConfigAPI target) {

    if (source == null || target == null) {
      return "Configuration API incompatible.";
    }
  
    // Major must be exactly the same
    if (source.G3ProtocolMajor != target.G3ProtocolMajor || source.G3ProtocolMinor != target.G3ProtocolMinor) {
      return "Incompatible configuration API version. RTU:"
          + target.getG3ProtocolVersion()
          + "  Configuration Tool:" + source.getG3ProtocolVersion();
    }
  
    if (source.SchemaVerMajor != target.SchemaVerMajor || source.SchemaVerMinor != target.SchemaVerMinor) {
      return ("Incompatible configuration schema version. RTU:"
          + target.getG3SchemaVersion()
          + "  Configuration tool:" + source.getG3SchemaVersion());
    }
  
    if (source.ModuleProtocolMajor != target.ModuleProtocolMajor
        || source.ModuleProtocolMinor != target.ModuleProtocolMinor) {
      return (
          "Incompatible Module System API. RTU: " + target.getModuleProtocollVersion()
              + "  Configuration Tool:" + source.getModuleProtocollVersion());
    }
  
    return null;
  }
  
  
  /**
   * Checks compatibility and return an error message if it not compatible.
   * @return an error message if incompatible, null if compatible.
   */
  public static String checkCompatibility(G3ConfigAPI source, G3ConfigAPI target) {
    if (source == null || target == null) {
      return "Configuration API incompatible.";
    }
  
    // Major must be exactly the same
    if (source.G3ProtocolMajor != target.G3ProtocolMajor || source.G3ProtocolMinor < target.G3ProtocolMinor) {
      return "Configuration API version is incompatible. RTU:"
          + target.getG3ProtocolVersion()
          + "  Configuration Tool:" + source.getG3ProtocolVersion();
    }
  
    if (source.SchemaVerMajor != target.SchemaVerMajor || source.SchemaVerMinor < target.SchemaVerMinor) {
      return ("Configuration schema version is incompatible. RTU:"
          + target.getG3SchemaVersion()
          + "  Configuration tool:" + source.getG3SchemaVersion());
    }
  
    if (source.ModuleProtocolMajor != target.ModuleProtocolMajor
        || source.ModuleProtocolMinor < target.ModuleProtocolMinor) {
      return (
          " Module System API is incompatible. RTU: " + target.getModuleProtocollVersion()
              + "  Configuration Tool:" + source.getModuleProtocollVersion());
    }
  
    return null;
  }

  
  public static String checkConfigSchemaAPI(G3ConfigAPI source, G3ConfigAPI target){
    String err = null;
    
    if (source.SchemaVerMajor != target.SchemaVerMajor || source.SchemaVerMinor != target.SchemaVerMinor) {
      err = ("Configuration schema version is incompatible. RTU:"
          + target.getG3SchemaVersion()
          + "  Configuration tool:" + source.getG3SchemaVersion());
    }
    
    return err;
  }
    
}
