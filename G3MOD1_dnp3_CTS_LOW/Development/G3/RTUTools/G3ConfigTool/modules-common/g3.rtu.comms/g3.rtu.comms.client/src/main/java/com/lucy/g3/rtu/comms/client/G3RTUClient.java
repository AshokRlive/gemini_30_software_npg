/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import java.io.IOException;
import java.util.Arrays;

import org.apache.log4j.Logger;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.comms.datalink.DataLink;
import com.lucy.g3.rtu.comms.protocol.G3MsgHeader;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.G3Query;
import com.lucy.g3.rtu.comms.protocol.IG3Message;
import com.lucy.g3.rtu.comms.protocol.IG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.ReplyPayload;
import com.lucy.g3.rtu.comms.protocol.IG3MsgPayload.PayloadType;
import com.lucy.g3.rtu.comms.protocol.exceptions.InvalidMsgLengthException;
import com.lucy.g3.rtu.comms.protocol.exceptions.InvalidPayloadLenException;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.protocol.exceptions.UnexpectedMsgIDException;
import com.lucy.g3.rtu.comms.protocol.exceptions.UnexpectedMsgTypeException;
import com.lucy.g3.rtu.comms.protocol.exceptions.UnexpectedRequestIDException;
import com.lucy.g3.rtu.comms.protocol.exceptions.ProtocolException.NackException;

/**
 * <code>G3RTUClient</code> is a component for sending/receiving G3 message
 * to/from G3 RTU over TCP/IP.
 */
public class G3RTUClient implements IClient {

  private Logger log = Logger.getLogger(G3RTUClient.class);

  private DataLink datalink;

  private final Session session = new Session();

  private static short uniqueRequestID = 0;


  public G3RTUClient(DataLink datalink) {
    super();
    session.setSessionID((short) 0xFFFF);

    if (datalink == null)
      throw new IllegalArgumentException("data link must not be null");

    this.datalink = datalink;
  }

  @Override
  public DataLink getDataLink() {
    return this.datalink;
  }


  @Override
  public synchronized void send(G3Query query) throws IOException, SerializationException {
    if (query == null || query.requestMsg == null) {
      throw new NullPointerException("query must not be null");
    }

    final IG3Message request = query.requestMsg;
    final IG3Message reply = query.replyMsg;
    final String msgName = request.name();

    // Validate request
    final byte[] requestBytes = request.toBytes();
    validatePayload(request.name(), request.getPayload(), getPayload(requestBytes));

    // Sending request
    byte[] replyBytes = datalink.send(request.name(), requestBytes, query.getRetries());

    // Parse reply header
    G3MsgHeader header = parseHeader(replyBytes);

    // Parse reply payload
    final byte[] payloadBytes = getPayload(replyBytes);
    if (header.isAckNack()) {
      // Parse AckNack payload
      ReplyAckNack ackNack;
      if (reply != null && reply.getPayload() instanceof ReplyAckNack)
        ackNack = (ReplyAckNack) reply.getPayload();
      else
        ackNack = new ReplyAckNack();

      validatePayload(msgName, ackNack, payloadBytes);
      ackNack.parseBytes(payloadBytes);
      handleAckNack(request.name(), ackNack);

    } else if (reply != null) {
      validateHeader(msgName, reply.getHeader(), header);
      validatePayload(msgName, reply.getPayload(), payloadBytes);
      reply.getPayload().parseBytes(payloadBytes);
    }
  }

  private G3MsgHeader parseHeader(byte[] replyBytes) throws SerializationException {

    // Check reply not empty
    if (replyBytes == null || replyBytes.length == 0) {
      throw new InvalidMsgLengthException("No Reply!");
    }

    // Check reply header size
    if (replyBytes.length < CTMsg_HeaderSize) {
      throw new InvalidMsgLengthException("Invalid length of reply:" + replyBytes.length);
    }

    G3MsgHeader header = new G3MsgHeader();
    header.parseBytes(getHeader(replyBytes));

    return header;
  }

  /**
   * Validation procedure:
   * <ol>
   * <li>Validate request: header size, payload size (non-fragmented, fragmented).
   * <li>Validate reply: header size, payload size(non-fragmented, fragmented).
   * <li>Validate reply: message type and message id must match with expected message type
   * and id.
   * <li>Validate optional: request and reply have the same message type
   * <li>Validate AckNack message must be parsed before validate those.
   * </ol>
   */
  private void validateHeader(String msgName, G3MsgHeader expected, G3MsgHeader actual) throws SerializationException {
    // Check message type
    if (actual.type() != expected.type()) {
      throw new UnexpectedMsgTypeException(msgName, expected.type(), actual.type());
    }

    // Check message id
    if (actual.id() != expected.id()) {
      throw new UnexpectedMsgIDException(msgName, expected.id(), actual.id());
    }

    // Check requestID
    if (actual.requestId() != expected.requestId()) {
      throw new UnexpectedRequestIDException(msgName, expected.requestId(), actual.requestId());
    }
  }

  /**
   * Validates the payload size.
   *
   * @throws SerializationException
   *           if the payload size is invalid
   */
  private void validatePayload(String msgName, IG3MsgPayload payload, byte[] payloadBytes)
      throws SerializationException {
    if (payload == null) {
      throw new IllegalArgumentException("Payload must not be null!!!");
    }

    if (payloadBytes == null) {
      throw new IllegalArgumentException("Payload bytes must not be null!!!");
    }

    PayloadType type = payload.type();
    int size = payload.size();

    switch (type) {
    case FIXED_SIZE:
      if (payloadBytes.length != size) {
        throw new InvalidPayloadLenException(msgName,
            String.format("Wrong payload size: %d, expected: %d", payloadBytes.length, size));
      }
      break;

    case MINIMUM_SIZE:
      if (payloadBytes.length < size) {
        throw new InvalidPayloadLenException(msgName,
            String.format("Too short payload size: %d, expected: %d", payloadBytes.length, size));
      }
      break;

    case FRAGMENTED:
      if (payloadBytes.length % size != 0) {
        throw new InvalidPayloadLenException(msgName,
            String.format("Wrong payload size:%d, expected: %d*n bytes", payloadBytes.length, size));
      }
      break;

    case RAW:
      // No validation for UNLIMITED payload
      break;
    default:
      break;
    }
  }

  private void handleAckNack(String msgName, ReplyAckNack payload) throws NackException {
    CTH_ACKNACKCODE acknack = payload.getAckNackCode();

    switch (acknack) {
    case CTH_ACK:
      break;
    case CTH_ACK_EXIST:
      log.warn("ACK Reply: File Existed");
      break;
    case CTH_ACK_CANCEL:
      log.info("ACK Reply: operation cannelled!");
      break;
    case CTH_NACK_NOLIST:
      log.warn("NACK Reply: no selected points");
      break;
    case CTH_NACK_LOGLISTEMPTY:
      log.debug("NACK Reply: no log entry");
      break;
    default:
      String ackNackHex = "0x" + NumUtils.byteToHex((byte) acknack.getValue());
      log.warn(String.format("\"%s\"Reply Nack: [%s] %s", msgName, ackNackHex, acknack.getDescription()));

      throw new NackException(acknack);
    }
  }

  private static byte[] getPayload(byte[] rawMessage) {
    return Arrays.copyOfRange(rawMessage, CTMsg_HeaderSize, rawMessage.length);
  }

  private static byte[] getHeader(byte[] rawMessage) {
    return Arrays.copyOfRange(rawMessage, 0, CTMsg_HeaderSize);
  }

  @Override
  public synchronized short getRequestID() {
    return uniqueRequestID++;
  }

  @Override
  public Session getSession() {
    return session;
  }

  public short getSessionID() {
    return session.getSessionID();
  }

  private static class ReplyAckNack extends ReplyPayload {

    public static final int PAYLOAD_SIZE = G3Protocol.PLSize_AckNack;
    private CTH_ACKNACKCODE ackNackCode;


    ReplyAckNack() {
      super(PAYLOAD_SIZE);
    }

    @Override
    public void parseBytes(byte[] payload) throws SerializationException {
      
      assert payload.length == PAYLOAD_SIZE;

      short ackNackValue = NumUtils.convert2Unsigned(payload[0]);
      ackNackCode = CTH_ACKNACKCODE.forValue(ackNackValue);

      if (ackNackCode == null) {
        log.error("Unrecongnized CTH_ACKNACKCODE enum value: " + ackNackValue);
        ackNackCode = CTH_ACKNACKCODE.CTH_NACK_GENERIC;
      }
    }

    public CTH_ACKNACKCODE getAckNackCode() {
      return ackNackCode;
    }
  }
}
