/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import java.io.IOException;
import java.util.Random;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.client.IClient;
import com.lucy.g3.rtu.comms.datalink.DummyDataLink;
import com.lucy.g3.rtu.comms.protocol.AbstractG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.G3Message;
import com.lucy.g3.rtu.comms.protocol.G3MsgHeader;
import com.lucy.g3.rtu.comms.protocol.G3Query;
import com.lucy.g3.rtu.comms.protocol.IG3MsgPayload;
import com.lucy.g3.rtu.comms.protocol.exceptions.InvalidMsgLengthException;
import com.lucy.g3.rtu.comms.protocol.exceptions.InvalidPayloadLenException;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.protocol.exceptions.UnexpectedMsgIDException;
import com.lucy.g3.rtu.comms.protocol.exceptions.UnexpectedRequestIDException;

public class G3RTUClientTest {

  private DummyDataLink dummy;// Dummy link layer for simulating HTTP link layer
  private IClient fixture;


  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Logger.getRootLogger().setLevel(Level.DEBUG);
  }

  @Before
  public void setUp() {
    dummy = new DummyDataLink();
    dummy.setReplyBytes(new byte[8]);
    
    fixture = new G3RTUClient(dummy);
  }

  @Test(expected = NullPointerException.class)
  public void testSend_Null2() throws IOException, SerializationException {
    fixture.send(new G3Query(null, null));
  }
  
  @Test
  public void testSend_Null3() throws IOException, SerializationException {
    dummy.setReplyBytes(new G3MsgHeader().toBytes());// Set dummy reply
    fixture.send(new G3Query(new G3Message(new G3MsgHeader()), null));
  }
  
  @Test(expected = NullPointerException.class)
  public void testSend_Null() throws IOException, SerializationException {
    fixture.send(null);
  }
  
  @Test
  public void testSend() throws IOException, SerializationException {
    G3Query req = createRequest();

    dummy.setReplyBytes(req.replyMsg.toBytes());// Set dummy reply

    fixture.send(req);
  }

  @Test(expected = InvalidMsgLengthException.class)
  public void testSend_NoReply() throws IOException, SerializationException {
    dummy.setReplyBytes(null);
    fixture.send(createRequest());
  }

  @Test(expected = IOException.class)
  public void testSend_IOException() throws IOException, SerializationException {
    dummy.setIOException(true);
    fixture.send(createRequest());
  }

  
  
  @Test(expected = InvalidPayloadLenException.class)
  public void testSend_InvalidPayloadLenException() throws IOException, SerializationException {
    IG3MsgPayload invalidPayload = new AbstractG3MsgPayload(10) {
      
      @Override
      public byte[] toBytes() {
        return null;
      }
      
      @Override
      public void parseBytes(byte[] bytes) throws SerializationException {
        
      }
    };
    fixture.send(new G3Query(new G3Message(new G3MsgHeader(), invalidPayload), null));
  }
  
  @Test
  public void testSend_ValidPayloadLenException() throws IOException, SerializationException {
    IG3MsgPayload invalidPayload = new AbstractG3MsgPayload(0) {
      
      @Override
      public byte[] toBytes() {
        return null;
      }
      
      @Override
      public void parseBytes(byte[] bytes) throws SerializationException {
        
      }
    };    fixture.send(new G3Query(new G3Message(new G3MsgHeader(), invalidPayload), null));

  }
  
  @Test(expected = UnexpectedMsgIDException.class)
  public void testSend_UnexpectedMsgIDException() throws IOException, SerializationException {
    G3Query req = createRequest();

    // Alter replied message ID.
    byte[] replied = req.replyMsg.toBytes();
    replied[3] = (byte) (replied[3] + 1);

    dummy.setReplyBytes(replied);
    fixture.send(req);
  }

  @Test(expected = UnexpectedRequestIDException.class)
  public void testSend_UnexpectedRequestIDException() throws IOException, SerializationException {
    G3Query req = createRequest();

    // Alter replied request ID.
    byte[] replied = req.replyMsg.toBytes();
    replied[4] = (byte) (replied[4] + 1);

    dummy.setReplyBytes(replied);// Set dummy reply
    fixture.send(req);
  }

  private static G3Query createRequest(short msgType,
      short msgID_send, short requestID_send,
      short msgID_reply, short requestID_reply) {

    G3Message send = new G3Message(new G3MsgHeader(msgType, msgID_send, requestID_send, sessionID));
    G3Message reply = new G3Message(new G3MsgHeader(msgType, msgID_reply, requestID_reply, sessionID));
    return new G3Query(send, reply);
  }

  private static G3Query createRequest() {
    short msgType = (short) random.nextInt();
    short msgID_send = (short) random.nextInt();
    short requestID = (short) random.nextInt();
    short msgID_reply = (short) random.nextInt();

    return createRequest(msgType, msgID_send, requestID, msgID_reply, requestID);
  }


  final private static short sessionID = (short) 0;
  final private static Random random = new Random();
}
