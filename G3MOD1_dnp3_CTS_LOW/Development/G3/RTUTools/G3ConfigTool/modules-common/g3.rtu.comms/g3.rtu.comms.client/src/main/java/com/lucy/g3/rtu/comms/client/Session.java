/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import java.util.HashMap;


/**
 * Stores all session information when connecting to a RTU.
 */
public final class Session {
  /**
   * Key of session data {@value}. Type: CTH_RUNNINGAPP 
   */
  public final static String SESSSION_DATA_RTU_RUNNING_MODE = "rtuRunningMode";
  
  private short sessionID;
  
  private final HashMap<String, Object> sessionData = new HashMap<>();
  

  public void setSessionID(short newSessionID) {
    this.sessionID = newSessionID;
  }
  
  public void setData(String key, Object value){
    sessionData.put(key, value);
  }

  public Object getData(String key) {
    return sessionData.get(key);
  }
  
  public short getSessionID() {
    return sessionID;
  }
}
