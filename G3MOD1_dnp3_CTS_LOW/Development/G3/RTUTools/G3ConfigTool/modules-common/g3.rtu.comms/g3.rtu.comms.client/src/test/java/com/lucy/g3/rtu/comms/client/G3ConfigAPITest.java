/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.comms.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.junit.Test;

import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;

public class G3ConfigAPITest {

  @Test
  public void testGetBytes() {
    G3ConfigAPI api = new G3ConfigAPI();
    byte[] bytes = api.getBytes();
    assertNotNull(bytes);
    assertEquals(bytes.length, G3Protocol.PLSize_R_ConfigAPI);
  }

  @Test
  public void testParseBytes() {
    // Prepare bytes
    byte[] bytes = new byte[G3Protocol.PLSize_R_ConfigAPI];
    Arrays.fill(bytes, (byte) 2);

    // Parse
    G3ConfigAPI api = G3ConfigAPI.parseBytes(bytes);
    assertNotNull(api);

    // Check bytes the same
    Arrays.equals(bytes, api.getBytes());
  }

  @Test
  public void testParseInvalidLenBytes() {
    byte[] bytes = new byte[G3Protocol.PLSize_R_ConfigAPI - 1];
    assertNull(G3ConfigAPI.parseBytes(bytes));
  }

}
