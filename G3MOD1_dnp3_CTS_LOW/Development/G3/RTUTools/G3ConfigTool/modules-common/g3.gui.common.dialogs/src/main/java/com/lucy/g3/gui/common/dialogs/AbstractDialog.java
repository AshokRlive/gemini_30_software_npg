/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingConstants;

import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXErrorPane;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.widgets.ext.swing.action.DelegateAction;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;

/**
 * StandardDialog is a dialog template. However several things are added to it
 * to make it easier to use.
 * <UL>
 * <LI>Default action and cancel action. User can set the default action and
 * cancel action of this dialog. By default, the ENTER key will trigger the
 * default action and the ESC key will trigger the cancel action and set the
 * dialog result to RESULT_CANCELLED.
 * <LI>Divide the whole ContentPane of the dialog into three parts - content
 * panel, button panel and banner panel. By default, they are added to the
 * CENTER, SOUTH and NORTH of a BorderLayout respectively. There isn't anything
 * special about this. However if all your dialogs use this pattern, it will
 * automatically make the user interface more consistent.
 * </UL>
 * <p/>
 * This class is abstract. Subclasses need to implement createBannerPanel(),
 * createButtonPanel() and createContentPanel()
 */
public abstract class AbstractDialog extends JDialog {

  /* Action keys */
  public static final String ACTION_KEY_OK = "ok";
  public static final String ACTION_KEY_CANCEL = "cancel";
  public static final String ACTION_KEY_CLOSE = "close";

  /* Property Names */
  public static final String PROPERTY_CANCEL_ACTION = "defaultCancelAction";
  public static final String PROPERTY_DEFAULT_ACTION = "defaultAction";
  public static final String PROPERTY_OK_ENABLEMENT = "okEnabled";

  /* Dialog result */
  public static final int RESULT_CANCELLED = -1;
  public static final int RESULT_AFFIRMED = 0;

  private boolean lazyConstructorCalled = false;

  private BannerPanel banner;
  private JComponent contentPanel;
  private JPanel buttonPanel;

  private Action defaultCancelAction;
  private Action defaultAction;

  private Component initFocusedComponent;

  /**
   * Layout alignment:
   * {@code SwingConstants.HORIZONTAL, SwingConstants.VERTICAL}.
   */
  private int layout;

  /**
   * {@code RESULT_CANCELLED, RESULT_AFFIRMED}.
   */
  private int dialogResult;

  /**
   * Enablement property for OK action.
   */
  private boolean okEnabled = true;


  public AbstractDialog() throws HeadlessException {
    this(null);
  }

  public AbstractDialog(Frame owner) throws HeadlessException {
    this(owner, true);
  }

  public AbstractDialog(Frame owner, boolean modal) throws HeadlessException {
    this(owner, null, modal);
  }

  public AbstractDialog(Frame owner, String title) throws HeadlessException {
    this(owner, title, true);
  }

  public AbstractDialog(Frame owner, String title, boolean modal) throws HeadlessException {
    this(owner, title, modal, SwingConstants.VERTICAL);
  }

  public AbstractDialog(Frame owner, String title, boolean modal, int alignment) throws HeadlessException {
    super(owner, title, modal);
    layout = alignment;
    initDialog();
  }

  public AbstractDialog(Dialog owner, boolean modal) throws HeadlessException {
    this(owner, null, modal);
  }

  public AbstractDialog(Dialog owner, String title) throws HeadlessException {
    this(owner, title, true);
  }

  public AbstractDialog(Dialog owner, String title, boolean modal) throws HeadlessException {
    this(owner, title, true, SwingConstants.VERTICAL);
  }

  public AbstractDialog(Dialog owner, String title, boolean modal, int alignment) throws HeadlessException {
    super(owner, title, modal);
    layout = alignment;
    initDialog();
  }

  public AbstractDialog(Window parent) {
    this(parent, null, true);
  }
  public AbstractDialog(Window parent, String title, boolean modal) {
    super(parent, title);
    setModal(true);
    layout = SwingConstants.VERTICAL;
    initDialog();
  }

  private void initDialog() {
    setDefaultCancelAction(getAction(ACTION_KEY_CANCEL));
    setDefaultAction(getAction(ACTION_KEY_OK));

    addPropertyChangeListener(new DialogPCL());
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        cancel();
      }
    });
  }

  /**
   * Gets the result.
   *
   * @return the result.<br>
   *         {@code RESULT_AFFIRMED, RESULT_CANCELLED}
   */
  public int getDialogResult() {
    return dialogResult;
  }

  public boolean hasBeenCanceled() {
    return (dialogResult == RESULT_CANCELLED);
  }

  public boolean hasBeenAffirmed() {
    return (dialogResult == RESULT_AFFIRMED);
  }

  /**
   * Sets the dialog result.
   *
   * @param dialogResult
   *          the new dialog result.<br>
   *          {@code RESULT_AFFIRMED, RESULT_CANCELLED}
   */
  protected void setDialogResult(int dialogResult) {
    this.dialogResult = dialogResult;
  }

  /**
   * Force the initComponent() method implemented in the child class to be
   * called. If this method is called more than once on a given object, all
   * calls but the first do nothing.
   */
  public final synchronized void initialize() {
    if ((!lazyConstructorCalled) /* && (getParent() != null) */) {
      buildDialog();
      lazyConstructorCalled = true;
      validate();
    }
  }

  @Override
  public void pack() {
    try {
      initialize();
    } catch (Exception e) {
      e.printStackTrace();
      JXErrorPane.showDialog(e);
    }
    super.pack();
    setLocationRelativeTo(getOwner());
  }

  /**
   * Show this dialog.
   *
   * @deprecated deprecated in Java.
   */
  @Override
  @Deprecated
  public void show() {
    try {
      initialize();
    } catch (Exception e) {
      e.printStackTrace();
      JXErrorPane.showDialog(e);
    }
    super.show();
  }

  /**
   * Call three createXxxPanel methods and layout them using BorderLayout. By
   * default, banner panel, content panel and button panel are added to NORTH,
   * CENTER and SOUTH of BorderLayout respectively.
   * <p/>
   * You can override this method if you want to layout them in another way.
   */
  private void buildDialog() {
    getContentPane().setLayout(new BorderLayout());
    buttonPanel = createButtonPanel();
    banner = createBannerPanel();
    contentPanel = createScrollPane(createContentPanel());
    layoutComponents(banner, contentPanel, buttonPanel);

    if (getRootPane() != null) {
      if (getRootPane().getDefaultButton() != null) {
        getRootPane().getDefaultButton().requestFocus();
      }

      if (getDefaultCancelAction() != null) {
        getRootPane().registerKeyboardAction(new DelegateAction(getDefaultCancelAction()) {

          @Override
          public boolean delegateActionPerformed(ActionEvent e) {
            if (hasSelectionPath()) {
              MenuSelectionManager.defaultManager().clearSelectedPath();
              return true;
            }
            return false;
          }

          @Override
          public boolean isDelegateEnabled() {
            return hasSelectionPath();
          }

          private boolean hasSelectionPath() {
            MenuElement[] selectedPath = MenuSelectionManager.defaultManager().getSelectedPath();
            return selectedPath != null && selectedPath.length > 0;
          }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
      }
      if (getDefaultAction() != null) {
        getRootPane().registerKeyboardAction(getDefaultAction(), KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
            JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
      }
    }

    if (getInitFocusedComponent() != null) {
      addWindowListener(new WindowAdapter() {

        @Override
        public void windowActivated(WindowEvent e) {
          getInitFocusedComponent().requestFocus();
        }
      });
    }
  }

  /**
   * Setups the layout for the three panels - banner panel, content panel and
   * button panel. By default, we will use BorderLayout, put the content panel
   * in the middle, banner panel on the top and button panel either right or
   * bottom depending on its alignment.
   * <p/>
   * Subclass can override it to do your own layout. The three panels are the
   * three parameters.
   *
   * @param bannerPanel
   *          the banner panel
   * @param contentPanel
   *          the content panel
   * @param buttonPanel
   *          the button panel
   */
  protected void layoutComponents(JComponent bannerPanel, JComponent contentPanel, JComponent buttonPanel) {
    setLayout(new BorderLayout());
    if (bannerPanel != null) {

      setUnfocusable(bannerPanel);

      // if(isBannerScrollable()){
      // JScrollPane bannerScroll = new JScrollPane(bannerPanel,
      // JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
      // JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
      // // Customise container
      // bannerScroll.setBorder(bannerPanel.getBorder());
      // bannerPanel.setBorder(BorderFactory.createEmptyBorder());
      // bannerScroll.setPreferredSize(getBannerSize());
      // UIUtilities.increaseScrollSpeed(bannerScroll);
      // bannerScroll.getVerticalScrollBar().setPreferredSize(new
      // Dimension(10,0));
      // bannerScroll.getHorizontalScrollBar().setPreferredSize(new
      // Dimension(0,10));
      // bannerPanel.setFocusable(false);
      //
      // add(bannerScroll, BorderLayout.BEFORE_FIRST_LINE);
      //
      // }else
      {
        add(bannerPanel, BorderLayout.BEFORE_FIRST_LINE);
      }
    }

    if (contentPanel != null) {
      contentPanel.setBorder(Borders.DIALOG_BORDER);
      add(contentPanel, BorderLayout.CENTER);
    }

    if (buttonPanel != null) {
      buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
      if (layout == SwingConstants.HORIZONTAL) {
        add(buttonPanel, BorderLayout.AFTER_LINE_ENDS);
      } else {
        add(buttonPanel, BorderLayout.AFTER_LAST_LINE);
      }
    }
  }

  private void setUnfocusable(JComponent comp) {
    if (comp != null) {
      comp.setFocusable(false);

      Component[] children = comp.getComponents();
      for (Component child : children) {
        if (child != null && child instanceof JComponent) {
          setUnfocusable((JComponent) child);
        }
      }
    }
  }

  /**
   * Subclasses should implement this method to create the banner panel. By
   * default banner panel will appear on top of the dialog unless you override
   * initComponent() method. Banner panel is really used to balance the layout
   * of dialog to make the dialog looking good. However it can be used to show
   * some help text. It is highly recommended to use our {@link BannerPanel}
   * <p/>
   * If subclass doesn't want to have a banner panel, just return null.
   *
   * @return the banner panel.
   */
  protected abstract BannerPanel createBannerPanel();

  /**
   * Subclasses should implement this method to create the content panel. This
   * is the main panel of the dialog which will be added to the center of the
   * dialog. Subclass should never return null.
   *
   * @return the content panel.
   */
  protected abstract JComponent createContentPanel();

  /**
   * Subclasses should implement this method to create the button panel. 90% of
   * dialogs have buttons. It is highly recommended to use our {@link JPanel}.
   *
   * @return the button panel.
   * @see JPanel
   */
  protected abstract JPanel createButtonPanel();

  public Action getAction(String key) {
    return Application.getInstance().getContext().getActionMap(AbstractDialog.class, this).get(key);
  }

  /**
   * Gets the initial focused component when dialog is shown.
   *
   * @return the initial focused component
   */
  public Component getInitFocusedComponent() {
    return initFocusedComponent;
  }

  /**
   * Sets the initial focused component when dialog is shown.
   *
   * @param initFocusedComponent
   *          the initial focused component
   */
  public void setInitFocusedComponent(Component initFocusedComponent) {
    this.initFocusedComponent = initFocusedComponent;
  }

  /**
   * Gets the banner panel created by createBannerPanel.
   *
   * @return the banner panel.
   */
  public BannerPanel getBannerPanel() {
    return banner;
  }

  /**
   * Gets the banner panel created by createContentPanel.
   *
   * @return the content panel.
   */
  public JComponent getContentPanel() {
    return contentPanel;
  }

  /**
   * Gets the banner panel created by createButtonPanel.
   *
   * @return the button panel.
   */
  public JPanel getButtonPanel() {
    return buttonPanel;
  }

  /**
   * Get default cancel action. Default cancel action will be triggered when ESC
   * is pressed.
   *
   * @return the default cancel action
   */
  public Action getDefaultCancelAction() {
    return defaultCancelAction;
  }

  /**
   * Set default cancel action. Default cancel action will be triggered when ESC
   * is pressed.
   *
   * @param defaultCancelAction
   *          the default cancel action
   */
  public void setDefaultCancelAction(Action defaultCancelAction) {
    if (this.defaultCancelAction != defaultCancelAction) {
      Action oldAction = defaultCancelAction;
      this.defaultCancelAction = defaultCancelAction;
      if (getRootPane() != null) {
        ActionListener actionForKeyStroke =
            getRootPane().getActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
        if (actionForKeyStroke instanceof DelegateAction) {
          getRootPane().unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
        }
        if (getDefaultCancelAction() != null) {
          getRootPane().registerKeyboardAction(new DelegateAction(getDefaultCancelAction()) {

            @Override
            public boolean delegateActionPerformed(ActionEvent e) {
              if (hasSelectionPath()) {
                MenuSelectionManager.defaultManager().clearSelectedPath();
                return true;
              }
              return false;
            }

            @Override
            public boolean isDelegateEnabled() {
              return hasSelectionPath();
            }

            private boolean hasSelectionPath() {
              MenuElement[] selectedPath = MenuSelectionManager.defaultManager().getSelectedPath();
              return selectedPath != null && selectedPath.length > 0;
            }
          }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        }
      }
      firePropertyChange(PROPERTY_CANCEL_ACTION, oldAction, defaultCancelAction);
    }
  }

  /**
   * Gets the default action. Default action will be trigger when ENTEY key is
   * pressed.
   *
   * @return the default action.
   */
  public Action getDefaultAction() {
    return defaultAction;
  }

  /**
   * Sets the default action. Default action will be trigger when ENTEY key is
   * pressed.
   *
   * @param defaultAction
   *          the default action.
   */
  public void setDefaultAction(Action defaultAction) {
    Action oldAction = defaultAction;
    this.defaultAction = defaultAction;
    firePropertyChange(PROPERTY_DEFAULT_ACTION, oldAction, defaultAction);
  }

  public final boolean isOkEnabled() {
    return okEnabled;
  }

  protected final void setOkEnabled(boolean okEnabled) {
    boolean oldValue = this.isOkEnabled();
    this.okEnabled = okEnabled;
    firePropertyChange(PROPERTY_OK_ENABLEMENT, oldValue, okEnabled);
  }

  // ================== Default Actions ===================

  /**
   * Default Cancel action, the name of which is defined in property file.
   */
  @org.jdesktop.application.Action
  public void close() {
    setDialogResult(RESULT_CANCELLED);
    dispose();
  }

  /**
   * Default Cancel action, the name of which is defined in property file.
   */
  @org.jdesktop.application.Action
  public void cancel() {
    setDialogResult(RESULT_CANCELLED);
    dispose();
  }

  /**
   * Default OK action, the name of which is defined in property file. The
   * enabled status of OK action is controlled by property
   * {@code PROPERTY_OK_ENABLEMENT}.
   */
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_OK_ENABLEMENT)
  public void ok() {
    setDialogResult(RESULT_AFFIRMED);
    dispose();
  }

  /**
   * Help method for creating Button Bar Panel.
   *
   * @return a button bar panel.
   */
  protected final JPanel createOKCancelButtonPanel() {
    Action cancelAction = getAction(ACTION_KEY_CANCEL);
    setDefaultCancelAction(cancelAction);

    Action okAction = getAction(ACTION_KEY_OK);
    setDefaultAction(okAction);
    JPanel buttonBar = ButtonBarFactory.buildOKCancelBar(new JButton(okAction), new JButton(cancelAction));
    return buttonBar;
  }

  /**
   * Help method for creating Button Bar Panel.
   *
   * @return a button bar panel.
   */
  protected final JPanel createOKButtonPanel() {
    Action okAction = getAction(ACTION_KEY_OK);
    setDefaultAction(okAction);
    JPanel buttonBar = ButtonBarFactory.buildOKBar(new JButton(okAction));
    return buttonBar;
  }

  /**
   * Help method for creating Button Bar Panel.
   *
   * @return a button bar panel.
   */
  protected final JPanel createCloseButtonPanel() {
    Action closeAction = getAction(ACTION_KEY_CLOSE);
    setDefaultAction(closeAction);
    JButton closeButton = new JButton(closeAction);
    closeButton.setIcon(null);
    JPanel buttonBar = ButtonBarFactory.buildOKBar(closeButton);
    return buttonBar;
  }
  
  protected final JPanel createHelpButtonPanel(JButton help, JButton... buttons) {
    JPanel buttonBar = ButtonBarFactory.buildHelpBar(help, buttons);
    return buttonBar;
  }

  /**
   * Help method for creating Button Bar Panel.
   *
   * @return a button bar panel.
   */
  protected final JPanel createHelpOKCancelButtonPanel(JButton helpButton) {
    Action cancelAction = getAction(ACTION_KEY_CANCEL);
    setDefaultCancelAction(cancelAction);

    Action okAction = getAction(ACTION_KEY_OK);
    setDefaultAction(okAction);
    return ButtonBarFactory.buildHelpOKCancelBar(helpButton, new JButton(okAction), new JButton(cancelAction));
  }

  /**
   * Create a scroll pane for the given content.
   */
  private static JScrollPane createScrollPane(JComponent content) {
    JScrollPane sp = new JScrollPane(content);
    UIUtils.increaseScrollSpeed(sp);
    sp.setViewportBorder(BorderFactory.createEmptyBorder());
    return sp;
  }

  protected void setVerticalScrollBarPolicy(int policy){
    ((JScrollPane)contentPanel).setVerticalScrollBarPolicy(policy);
  }
  
  protected void setHorizontalScrollBarPolicy(int policy){
    ((JScrollPane)contentPanel).setHorizontalScrollBarPolicy(policy);
  }
  
  

  private class DialogPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (PROPERTY_CANCEL_ACTION.equals(evt.getPropertyName())) {
        DelegateAction delegateAction = new DelegateAction(getDefaultCancelAction()) {

          @Override
          public boolean delegateActionPerformed(ActionEvent e) {
            if (hasSelectionPath()) {
              MenuSelectionManager.defaultManager().clearSelectedPath();
              return true;
            }
            return false;
          }

          @Override
          public boolean isDelegateEnabled() {
            return hasSelectionPath();
          }

          private boolean hasSelectionPath() {
            MenuElement[] selectedPath = MenuSelectionManager.defaultManager().getSelectedPath();
            return selectedPath != null && selectedPath.length > 0;
          }
        };
        DelegateAction.replaceAction(getRootPane(), JComponent.WHEN_IN_FOCUSED_WINDOW,
            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), delegateAction, false);

      } else if (PROPERTY_DEFAULT_ACTION.equals(evt.getPropertyName())) {
        getRootPane().unregisterKeyboardAction(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
        getRootPane().registerKeyboardAction(getDefaultAction(), KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
            JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

      }
    }
  }
}
