/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;


import org.jdesktop.application.Application;

import com.lucy.g3.gui.common.dialogs.AboutBox;


/**
 *
 */
public class AboutBoxTest extends Application {
  public static void main(String[] args) {
    Application.launch(AboutBoxTest.class, args);
  }

  @Override
  protected void startup() {
    new AboutBox(null).setVisible(true);      
  }
    
}

