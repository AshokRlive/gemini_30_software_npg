/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.l2fprod.common.propertysheet.PropertySheet;
import com.l2fprod.common.propertysheet.PropertySheetPanel;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.widgets.propertysheet.PropertyEditorBinder;

/**
 * Property Editor dialog for Java Bean.
 */
public class PropertyEditorDialog extends AbstractDialog {

  private final PropertySheetPanel editorPanel = new PropertySheetPanel();

  private final Model bean;

  private PropertyEditorBinder binder;


  public PropertyEditorDialog(Window parent, String title, Model bean) {
    super(parent);
    setModal(true);
    this.bean = Preconditions.checkNotNull(bean, "bean must not be null");
    setTitle(title);
    setName("PropertyEditorDialog");
    initComponents();
    initComponentsBinding();

    pack();
  }

  @Override
  public void close() {
    super.close();
    if (binder != null) {
      binder.unbind();
    }
  }

  private void initComponentsBinding() {
    if (bean != null) {
      binder = new PropertyEditorBinder(bean, editorPanel);
      binder.readFromObject();
    }
  }

  private void initComponents() {
    configurePropertyPanel(editorPanel);
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return editorPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createCloseButtonPanel();
  }

  public static void configurePropertyPanel(PropertySheetPanel propertyPanel) {
    propertyPanel.setMode(PropertySheet.VIEW_AS_CATEGORIES);
    propertyPanel.setDescriptionVisible(false);
    propertyPanel.setSortingCategories(false);
    propertyPanel.setPreferredSize(new Dimension(100,50));
    propertyPanel.setSortingProperties(false);
    propertyPanel.setRestoreToggleStates(true);
    propertyPanel.getTable().setRowHeight(20);
    propertyPanel.getTable().setGridColor(Color.LIGHT_GRAY);
    propertyPanel.getTable().setShowGrid(true);
    propertyPanel.getTable().setWantsExtraIndent(true);
    propertyPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
  }
  
  public static void createAndShowDialog(Window parent, Model bean,  String name) {
    String title = "Editing \"" + name + "\"";
    
    PropertyEditorDialog dialog = new PropertyEditorDialog(parent, title, bean);
    
    
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(dialog);
    } else {
      dialog.pack();
      dialog.setVisible(true);
    }
  }
}
