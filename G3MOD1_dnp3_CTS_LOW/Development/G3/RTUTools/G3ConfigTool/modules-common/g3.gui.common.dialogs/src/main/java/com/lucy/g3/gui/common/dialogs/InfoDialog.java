/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.AbstractTreeTableModel;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.help.Helper;

/**
 * A generic dialog for showing information.
 */
public final class InfoDialog extends AbstractDialog {

  private final JComponent content;
  private JButton btnHelp;
  private Action[] additionalActions;


  public InfoDialog(Dialog owner, boolean modal, String title, JComponent content, String helpKey) {
    super(owner, modal);
    this.content = Preconditions.checkNotNull(content, "content must not be null");
    
    init(title, helpKey);
  }
  
  public InfoDialog(Frame owner, String title, JComponent content, String helpKey) {
    super(owner, false);
    this.content = Preconditions.checkNotNull(content, "content must not be null");

    init(title, helpKey);
  }

  protected void init(String title, String helpKey) {
    // Register help
    if (helpKey != null) {
      btnHelp = Helper.createHelpButton(helpKey);
    } else {
      btnHelp = null;
    }

    Action closeAction = getAction(ACTION_KEY_CLOSE);
    setDefaultAction(closeAction);
    setDefaultCancelAction(closeAction);

    setTitle(title);
    setLocationRelativeTo(getOwner());
  }
  
  public void setAdditionalActions(Action[] additionalActions) {
    this.additionalActions = additionalActions;
  }

  private JButton[] createButtonBarButtons() {
    ArrayList<JButton> buttons = new ArrayList<>();

    if(additionalActions != null ) {
      for (int i = 0; i < additionalActions.length; i++) {
        if(additionalActions[i] != null) {
          buttons.add(new JButton(additionalActions[i]));
        }
      }
    }
    
    // Add print button
//    try {
//      Method printMethod = content.getClass().getMethod("print");
//      buttons.add(new JButton(new PrintAction(printMethod, content)));
//    } catch (NoSuchMethodException | SecurityException e) {
//      // Nothing to do
//    }
    
    // Add copy button
    CopyAction copyAction = CopyAction.create(content);
    if(copyAction != null)
      buttons.add(new JButton(copyAction));

    // Add more buttons here...
    buttons.add(new JButton(getAction(ACTION_KEY_CLOSE)));
    return buttons.toArray(new JButton[buttons.size()]);
  }
  
  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    JScrollPane sp = new JScrollPane(content);
    sp.setBorder(BorderFactory.createEmptyBorder());
    UIUtils.increaseScrollSpeed(sp);
    return sp;
  }

  @Override
  protected JPanel createButtonPanel() {

    JButton btnClose = new JButton(getAction(ACTION_KEY_CLOSE));
    btnClose.setIcon(null);

    JPanel buttonBar;
    if (btnHelp != null) {
      buttonBar = ButtonBarFactory.buildHelpBar(btnHelp, createButtonBarButtons());
    } else {
      buttonBar = ButtonBarFactory.buildRightAlignedBar(createButtonBarButtons());
    }

    return buttonBar;
  }

  private static JXTreeTable buildTreeTable(Properties... content) {
    JXTreeTable table = new JXTreeTable(new PropertiesTreeModel(content));
    table.setPreferredScrollableViewportSize(new Dimension(400, 300));

    // Customise table
    table.setOpenIcon(null);
    table.setClosedIcon(null);

    // Set row expanded
    for (int i = content.length - 1; i >= 0; i--) {
      if (content[i].expanded) {
        table.expandRow(i);
      }
    }

    return table;
  }

  private static JTable buildTable(Object[][] content) {
    Preconditions.checkNotNull(content, "content data must not be null");

    DefaultTableModel model = new DefaultTableModel(content, new String[] { "Property", "Value" }) {

      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    final JTable table = new JTable(model);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    table.setFillsViewportHeight(true);
    UIUtils.packTableColumns(table);
    return table;
  }

  private static void installPopupMenu(final JTable table) {
    JPopupMenu popupMenu = new JPopupMenu();

    // Add copy button
    CopyAction copyAction = CopyAction.create(table);
    if(copyAction != null) {
      final JMenuItem copyButton = new JMenuItem(copyAction);
      popupMenu.add(copyButton);

//      // Set copy button enable state
//      copyButton.setEnabled(table.getSelectedRowCount() > 0);
//      table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
//  
//        @Override
//        public void valueChanged(ListSelectionEvent e) {
//          if (!e.getValueIsAdjusting()) {
//            copyButton.setEnabled(table.getSelectedRowCount() > 0);
//          }
//        }
//      });
    }

    // Add expand/collapse button
    if (table instanceof JXTreeTable) {
      popupMenu.addSeparator();
      popupMenu.add(new ExpandAction((JXTreeTable) table));
      popupMenu.add(new CollapseAction((JXTreeTable) table));
    }

    // Add pack button
    if (table instanceof JXTable) {
      popupMenu.addSeparator();
      Action packAction = table.getActionMap().get(JXTable.PACKALL_ACTION_COMMAND);
      popupMenu.add(packAction);
    }
    table.setComponentPopupMenu(popupMenu);
  }


  private static class PrintAction extends AbstractAction {

    private final Method printMethod;
    private final JComponent invoker;


    public PrintAction(Method printMethod, JComponent invoker) {
      super("Print");
      this.printMethod = printMethod;
      this.invoker = invoker;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        printMethod.invoke(invoker);
      } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {
        JOptionPane.showMessageDialog(invoker,
            "Failed to print cause:\n" + e1.getMessage(), "Failure", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  private static class CopyAction extends AbstractAction {

    private final JComponent invoker;
    private final Action selectAll;
    private final Action copy;

    static CopyAction create(JComponent invoker) {
      Action selectAllAction = invoker.getActionMap().get("selectAll");
      Action copyAction = invoker.getActionMap().get("copy");
      if(selectAllAction != null && copyAction != null)
        return new CopyAction(invoker, selectAllAction, copyAction);
      else
        return null;
    }
    
    private CopyAction(JComponent invoker, Action selectAll, Action copy) {
      super("Copy");
      this.invoker = invoker;
      this.selectAll = selectAll;
      this.copy = copy;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
      // Select all contents before copy
      selectAll.actionPerformed(new ActionEvent(invoker, ActionEvent.ACTION_PERFORMED, "selectAll"));
      
      // Copy the selected content.
      copy.actionPerformed(new ActionEvent(invoker, ActionEvent.ACTION_PERFORMED, "copy"));
      JOptionPane.showMessageDialog(invoker,
          "The content has been copied to your clipboard!",
          "Copied to clipboard", JOptionPane.INFORMATION_MESSAGE);
    }
  }

  private static class ExpandAction extends AbstractAction {

    private JXTreeTable invoker;


    public ExpandAction(JXTreeTable invoker) {
      super("Expand All");
      this.invoker = invoker;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      invoker.expandAll();
    }
  }

  private static class CollapseAction extends AbstractAction {

    private JXTreeTable invoker;


    public CollapseAction(JXTreeTable invoker) {
      super("Collapse All");
      this.invoker = invoker;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      invoker.collapseAll();
    }
  }


  public static void show(Frame owner, String title, Properties... content) {
    show(owner, title, null, content);
  }

  public static void show(Frame owner, String title, String helpKey, Properties... content) {
    show(owner, title, buildTreeTable(content), helpKey);
  }

  /**
   * Shows a dialog that presents the content data in a table.
   *
   * @param owner
   *          the owner of Info dialog
   * @param title
   *          the title of dialog.
   * @param content
   *          the 2D array data that will be presented in a table.
   */
  public static void show(Frame owner, String title, Object[][] content) {
    show(owner, title, content, null);
  }

  /**
   * Shows a dialog that presents the content data in a table.
   *
   * @param owner
   *          the owner of Info dialog
   * @param title
   *          the title of dialog.
   * @param content
   *          the 2D array data that will be presented in a table.
   * @param helpKey
   *          the key for help button.
   */
  public static void show(Frame owner, String title, Object[][] content, String helpKey) {
    show(owner, title, buildTable(content), helpKey);
  }

  public static void show(Frame owner, String title, JComponent content) {
    show(owner, title, content, null);
  }

  public static void show(Frame owner, String title, JComponent content, String helpKey) {
    if (content != null && content instanceof JTable) {
      installPopupMenu((JTable) content);
    }

    InfoDialog dlg = new InfoDialog(owner, title, content, helpKey);
    dlg.pack();
    dlg.setVisible(true);
  }

  /**
   * Creates the properties.
   *
   * @param name
   *          the name
   * @param data
   *          the data
   * @return the properties
   */
  public static Properties createProperties(String name, Object[][] data) {
    return createProperties(name, false, data);
  }

  /**
   * Creates the properties.
   *
   * @param name
   *          the name
   * @param expanded
   *          the expanded
   * @param data
   *          the data
   * @return the properties
   */
  public static Properties createProperties(String name, boolean expanded, Object[][] data) {
    HashMap<String, Object> values = new LinkedHashMap<>();
    for (int i = 0; data != null && i < data.length; i++) {
      values.put((String) data[i][0], data[i][1]);
    }
    Properties properties = new Properties(name, values);
    properties.expanded = expanded;

    return properties;
  }


  /**
   * This class stores all properties to be displayed by {@linkplain InfoDialog}.
   */
  public static final class Properties {

    private final String name;

    private final ArrayList<Entry<String, Object>> entries;

    private boolean expanded;


    public Properties(String name, HashMap<String, Object> valueMap) {
      super();
      this.name = name;
      if (valueMap != null) {
        entries = new ArrayList<>(valueMap.entrySet());
      } else {
        this.entries = new ArrayList<>(0);
      }
    }

    @Override
    public String toString() {
      return name;
    }

    public Entry<String, Object> getEntry(int index) {
      if (index >= 0 && index < entries.size()) {
        return entries.get(index);
      } else {
        return null;
      }
    }

    public String getName() {
      return name;
    }

    public int getSize() {
      return entries.size();
    }

    public int indexOf(Object entry) {
      for (int i = 0; i < entries.size(); i++) {
        if (entry == entries.get(i)) {
          return i;
        }
      }
      return -1;
    }
  }

  private static class PropertiesTreeModel extends AbstractTreeTableModel {

    static final String[] COL_NAMES = { "Property", "Value" };


    public PropertiesTreeModel(Properties... properties) {
      super(properties);
    }

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public String getColumnName(int column) {
      return COL_NAMES[column];
    }

    @Override
    public Object getValueAt(Object node, int column) {
      if (node instanceof Properties) {
        return column == 0 ? ((Properties) node).getName() : "";
      }

      if (node instanceof Entry<?, ?>) {
        Entry<?, ?> entry = (Entry<?, ?>) node;
        return column == 0 ? entry.getKey() : entry.getValue();
      }

      return "-";
    }

    @Override
    public Object getChild(Object parent, int index) {
      if (parent.getClass().isArray()) {
        return ((Object[]) parent)[index];
      }

      if (parent instanceof Properties) {
        return ((Properties) parent).getEntry(index);
      }

      return "-";
    }

    @Override
    public int getChildCount(Object parent) {
      if (parent.getClass().isArray()) {
        return ((Object[]) parent).length;
      } else if (parent instanceof Properties) {
        return ((Properties) parent).getSize();
      } else {
        return 0;
      }
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
      if (parent.getClass().isArray()) {
        Object[] parray = ((Object[]) parent);
        for (int i = 0; i < parray.length; i++) {
          if (child == parray[i]) {
            return i;
          }
        }
      }

      if (parent instanceof Properties) {
        return ((Properties) parent).indexOf(child);
      }

      return -1;
    }

  }
}
