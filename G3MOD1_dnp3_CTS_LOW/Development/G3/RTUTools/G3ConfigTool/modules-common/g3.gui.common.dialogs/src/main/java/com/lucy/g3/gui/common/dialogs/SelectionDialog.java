/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel;


public class SelectionDialog<ItemT> extends AbstractDialog{
  /** Used for error messages. */
  public static final int  ERROR_MESSAGE = 0;
  /** Used for information messages. */
  public static final int  INFORMATION_MESSAGE = 1;
  /** Used for warning messages. */
  public static final int  WARNING_MESSAGE = 2;
  /** Used for questions. */
  public static final int  QUESTION_MESSAGE = 3;
  
  private final SelectionTableModel<ItemT> model;
  
  private JTable table;
  private JLabel lblMsg;
  private JPanel contentPane;

  private JCheckBox cboxSelectAll;
  
  public SelectionDialog(Frame parent, Collection<ItemT> items){
    this(parent, items, null);
  }
  
  public SelectionDialog(Frame parent, Collection<ItemT> items, String columnName){
    super(parent);
    this.model = new SelectionTableModel<>(items, columnName);
    
    init();
  }

  public SelectionDialog(Window parent, Collection<ItemT> items){
    this(parent, items, null);
  }
  
  public SelectionDialog(Window parent, Collection<ItemT> items, String columnName){
    super(parent);
    this.model = new SelectionTableModel<>(items);
    
    init();
  }
  
  private void init() {
    setModal(true);
    setTitle("Selection");
    initComponents();
  }
  
  public void setMessage(String message) {
    lblMsg.setText(message);
  }
  
  public void setIntialSelection(ItemT [] initial){
    deselectAll();
    
    if(initial != null) {
      for (int i = 0; i < initial.length; i++) {
        if(initial[i] != null) {
          model.setItemSelected(initial[i], true);
        }
      }
    }
    
    // Initialise selections in view
    ListSelectionModel selectionModel = table.getSelectionModel();
    for (int i = 0; i < model.getRowCount(); i++) {
      if(model.getValueAt(i, SelectionTableModel.COLUMN_SELECTED) == Boolean.TRUE) {
        selectionModel.addSelectionInterval(i, i);
      }
    }
    
    // Scroll to the selections
    int index = selectionModel.getMinSelectionIndex();
    if(index >= 0)
      table.scrollRectToVisible(table.getCellRect(index, 0, true));

  }
  
  public void setMessageOption(int option){
    switch (option) {
    case WARNING_MESSAGE:
      lblMsg.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
      break;
    case ERROR_MESSAGE:
      lblMsg.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
      break;
    case INFORMATION_MESSAGE:
      lblMsg.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
      break;
      
    case QUESTION_MESSAGE:
      lblMsg.setIcon(UIManager.getIcon("OptionPane.questionIcon"));
      break;

    default:
      lblMsg.setIcon(null);
      break;
    }
  }
  
  public void selectAll(){
    model.selectAll();
  }
  
  public void deselectAll(){
    model.clearSelection();
  }
  
  public ArrayList<ItemT> getSelections(){
    if(hasBeenAffirmed())
      return model.getSelections();
    return 
      null;
  }
  
  private void initComponents(){
    table = buildSelectionTable(model);
    lblMsg = new JLabel("Please select the items below:");
    cboxSelectAll = new JCheckBox("Select/Deselect All");
    cboxSelectAll.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if(cboxSelectAll.isSelected())
          selectAll();
        else
          deselectAll();
      }
    });
    
    JScrollPane sc = new JScrollPane(table);
    sc.setPreferredSize(new Dimension(450, 300));
    contentPane = new JPanel(new BorderLayout(0, 10));
    contentPane.add(BorderLayout.CENTER, sc);
    contentPane.add(BorderLayout.NORTH, lblMsg);
    contentPane.add(BorderLayout.SOUTH, cboxSelectAll);
  }
  
  private static JTable buildSelectionTable(SelectionTableModel<?> model) {
    // Build selection table
    JTable selTable = new JTable(model);
    selTable.setAutoCreateRowSorter(false);
    selTable.setFillsViewportHeight(true);

    // Adjust column width
    selTable.getColumnModel()
        .getColumn(SelectionTableModel.COLUMN_SELECTED).setMaxWidth(50);

    // Build table pop menu
    JPopupMenu menu = new JPopupMenu();
    menu.add(model.getActionSelectAll());
    menu.add(model.getActionDeselectAll());
    selTable.setComponentPopupMenu(menu);
    return selTable;
  }
  
  public void setItemRender(TableCellRenderer renderer){
    table.getColumnModel().getColumn(SelectionTableModel.COLUMN_ITEM)
        .setCellRenderer(renderer);
  }
  
  @Override
  protected BannerPanel createBannerPanel() {
    
    return null;
  }
  
  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }
  

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
  
  
  public static <T> Collection<T> showRemoveDialog(Window parent, T[] items, T[] initial){
    if(items != null && items.length > 0)
      return showDialog(parent, "Warning", "Do you want to remove the selected items?", WARNING_MESSAGE, items, initial);
    else {
      JOptionPane.showMessageDialog(parent, "No item to be removed!", "Item Not Found", 
          JOptionPane.ERROR_MESSAGE);
      return null;
    }
  }
  
  public static <T> Collection<T> showDialog(Window parent, String title, String msg, int msgOption, T[] items, T[] initial){
    return showDialog(parent, title, msg, msgOption, Arrays.asList(items), initial);
  }
  
  public static <T> Collection<T> showDialog(Window parent,String title, String msg, int msgOption, Collection<T> items, T[] initial){
    ArrayList<T> itemsList;
    if(items == null)
      itemsList = new ArrayList<T>(0);
    else
      itemsList = new ArrayList<T>(items);
    
    itemsList.removeAll(Collections.singleton(null));// remove all null.
    
    SelectionDialog<T> dialog = new SelectionDialog<>(parent, itemsList);
    
    if(msg != null)
      dialog.setMessage(msg);
    if(title != null)
      dialog.setTitle(title);
    
    dialog.setMessageOption(msgOption);
    dialog.setIntialSelection(initial);
    
    dialog.pack();
    dialog.setVisible(true);
    return dialog.getSelections();
  }

}

