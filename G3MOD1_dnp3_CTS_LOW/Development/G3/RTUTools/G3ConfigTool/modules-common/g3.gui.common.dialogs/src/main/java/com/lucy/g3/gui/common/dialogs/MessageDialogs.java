/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.io.File;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;


/**
 * Utility class for showing messages in dialog.
 */
public final class MessageDialogs {

  private MessageDialogs() {}

  public static void error(String error, Throwable cause) {
    error(getMainFrame(), error, cause);
  }
  
  public static void error(Component owner, String error, String details) {
    ErrorInfo info = new ErrorInfo("Error", error, details,
        null, null, Level.SEVERE, null);

    JXErrorPane.showDialog(owner, info);
  }
  
  public static void error(Component owner, String error, Throwable cause) {
    String details = cause == null? null : cause.getMessage();
    
    if(error == null) {
      error = details;
      details = null;
    }
    
    if(error == null && cause != null) {
      error = "Catched exception: " + cause.getClass(); 
    }
      
    ErrorInfo info = new ErrorInfo("Error", error, details,
        null, cause, Level.SEVERE, null);

    JXErrorPane.showDialog(owner, info);
  }
  
  /**
   * Shows a dialog that list the given items with a message.
   * @param parent
   * @param msgType an integer designating the kind of message this is;
   *                  <code>JOptionPane.ERROR_MESSAGE</code>,
   *                  <code>JOptionPane.INFORMATION_MESSAGE</code>,
   *                  <code>JOptionPane.WARNING_MESSAGE</code>,
   *                  <code>JOptionPane.QUESTION_MESSAGE</code>,
   *                  or <code>JOptionPane.PLAIN_MESSAGE</code>
   * @param msg
   * @param items
   */
  public static void list(Window parent, int msgType, String title, String msg, Collection<?> items){
    list(parent, msgType, title, msg, items.toArray(new Object[items.size()]));
  }
  
  /**
   * Shows a dialog that list the given items with a message.
   * @param parent
   * @param msgType an integer designating the kind of message this is;
   *                  <code>JOptionPane.ERROR_MESSAGE</code>,
   *                  <code>JOptionPane.INFORMATION_MESSAGE</code>,
   *                  <code>JOptionPane.WARNING_MESSAGE</code>,
   *                  <code>JOptionPane.QUESTION_MESSAGE</code>,
   *                  or <code>JOptionPane.PLAIN_MESSAGE</code>
   * @param msg
   * @param items
   */
  public static void list(Window parent,int msgType, String title, String msg, Object[] items){
    JLabel msgLabel = new JLabel(msg);
    JList<Object> plist = new JList<Object>(items);
    JScrollPane plistPane = new JScrollPane(plist);
    plistPane.setPreferredSize(new Dimension(300, 200));
    JPanel container = new JPanel(new BorderLayout(5, 5));
    container.add(msgLabel, BorderLayout.NORTH);
    container.add(plistPane, BorderLayout.CENTER);
    JOptionPane.showMessageDialog(parent, container, title, msgType);
  }

  public static boolean confirmRemoveAll(Component parent) {
    return confirmRemoveAll(parent, null);
  }
  
  public static boolean confirmRemoveAll(Component parent, String itemsName) {
    String msg;
    if(itemsName == null)
      msg = "Do you want to remove all selected items?";
    else
      msg = MessageFormat.format("Do you want to remove all: {0} ?", itemsName);
    
    int option = JOptionPane.showConfirmDialog(parent,
        msg,
        "Remove All",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    
    return (option == JOptionPane.YES_OPTION);
  }
  
  public static boolean confirmRemove(Component parent, String itemName) {
    int option = JOptionPane.showConfirmDialog(parent,
        MessageFormat.format("Do you want to remove: {0} ?", itemName),
        "Remove",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);

    return (option == JOptionPane.YES_OPTION);
  }

  public static boolean confirm(Component parent, String message) {
    int option = JOptionPane.showConfirmDialog(parent,
        message,
        "Confirm",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);

    return (option == JOptionPane.YES_OPTION);
    
  }
  
  public static void warning(String warningMsg) {
    warning(null, warningMsg);
  }
  
  public static void warning(Component owner, String warningMsg) {
    if(owner == null)
      owner = getMainFrame();
    JOptionPane.showMessageDialog(owner, warningMsg, "Warning", JOptionPane.WARNING_MESSAGE);
  }
  
  public static JFrame getMainFrame() {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      return ((SingleFrameApplication) app).getMainFrame();
    }
    return null;
  }
  
  /**
   * Show a warning dialog if the given file exists.
   */
  public static boolean showOverwriteDialog(Component parentComponent, File file) {
    if (file != null && file.exists()) {
      int choice = JOptionPane.showConfirmDialog(parentComponent,
          "The file \"" + file.getName() + "\" already exists. \nDo you want to overwrite?",
          "Overwrite", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
      return choice == JOptionPane.YES_OPTION;
    }

    return true;
  }
  
  public static void finish(Component owner, String message) {
    JOptionPane.showMessageDialog(owner, message, "Finish", JOptionPane.INFORMATION_MESSAGE);
  }
}
