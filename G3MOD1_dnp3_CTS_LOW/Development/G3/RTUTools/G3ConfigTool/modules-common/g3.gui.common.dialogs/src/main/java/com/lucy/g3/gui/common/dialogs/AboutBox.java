/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.dialogs;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXLabel;

import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.common.manifest.ManifestInfo;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.help.Helper;

/**
 * A window for displaying ConfigTool software information.
 *<p> It works with jestktop.appframework and load application information(name, info,etc) from application properties files.
 *</p>
 */
public class AboutBox extends AbstractDialog {

  private final String[] columnNames = { "Property", "Value" };


  private final String[][] allProperties;
  
  private final ManifestInfo manifest;
  
  public AboutBox(JFrame owner) {
    this(owner, null);
  }
  public AboutBox(JFrame owner, ManifestInfo manifest) {
    super(owner, true);
    if(manifest == null)
      manifest = getDefaultManifest();
    this.manifest = manifest;
    
    setTitle("About "+manifest.getAppName());
    allProperties = getProperties();
    pack();
  }
  
  private static ManifestInfo getDefaultManifest() {
    String appName = ResourceUtils.getString(AboutBox.class, "Application.name");
    return new ManifestInfo(appName);
  }
  
  protected String[][] getProperties(){
    return manifest.getProperties();
  }

  private void createUIComponents() {

    propertiesTable = new JTable(allProperties, columnNames) {

      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
  }

  private void customiseComponents() {
    // HyperLink
    try {
      labelWebAddress.setURI(new URI(labelWebAddress.getText()));
    } catch (URISyntaxException e) {
      // Nothing to do.
    }

    labelVersionNo.setText(manifest.getVersion());
  }
  
  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    initComponents();
    customiseComponents();
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    Action closeAction = getAction(ACTION_KEY_CLOSE);
    setDefaultAction(closeAction);
    setDefaultCancelAction(closeAction);

    JPanel buttonBar = ButtonBarFactory.buildHelpBar(
        Helper.createHelpButton(getClass()),
        new JButton(closeAction));
    return buttonBar;
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    labelLogo = new JLabel();
    labelTitle = new JLabel();
    labelDesp = new JXLabel();
    JLabel labelVersion = new JLabel();
    labelVersionNo = new JLabel();
    JLabel labelVendor = new JLabel();
    labelVendorName = new JLabel();
    JLabel labelWeb = new JLabel();
    labelWebAddress = new JXHyperlink();
    scrollPane1 = new JScrollPane();

    //======== contentPanel ========
    {
      contentPanel.setOpaque(false);
      contentPanel.setLayout(new FormLayout(
        "default, $ugap, default, $lcgap, pref:grow",
        "4*(default, 6dlu), default, 20dlu, $lgap, fill:80dlu:grow"));

      //---- labelLogo ----
      labelLogo.setIcon(new ImageIcon(getClass().getResource("/com/lucy/g3/gui/common/dialogs/resources/about_logo.png")));
      contentPanel.add(labelLogo, CC.xywh(1, 1, 1, 9));

      //---- labelTitle ----
      labelTitle.setFont(labelTitle.getFont().deriveFont(labelTitle.getFont().getStyle() | Font.BOLD, labelTitle.getFont().getSize() + 4f));
      labelTitle.setName("labelTitle");
      labelTitle.setText("Application.name");
      contentPanel.add(labelTitle, CC.xywh(3, 1, 3, 1));

      //---- labelDesp ----
      labelDesp.setText("Application.description");
      labelDesp.setMaximumSize(new Dimension(200, 28));
      labelDesp.setName("labelDesp");
      contentPanel.add(labelDesp, CC.xywh(3, 3, 3, 1));

      //---- labelVersion ----
      labelVersion.setText("Product Version:");
      labelVersion.setFont(labelVersion.getFont().deriveFont(labelVersion.getFont().getStyle() | Font.BOLD));
      contentPanel.add(labelVersion, CC.xy(3, 5));

      //---- labelVersionNo ----
      labelVersionNo.setName("labelVersionNo");
      contentPanel.add(labelVersionNo, CC.xy(5, 5));

      //---- labelVendor ----
      labelVendor.setText("Vendor:");
      labelVendor.setFont(labelVendor.getFont().deriveFont(labelVendor.getFont().getStyle() | Font.BOLD));
      contentPanel.add(labelVendor, CC.xy(3, 7));

      //---- labelVendorName ----
      labelVendorName.setName("labelVendorName");
      labelVendorName.setText("Application.vendorId");
      contentPanel.add(labelVendorName, CC.xy(5, 7));

      //---- labelWeb ----
      labelWeb.setText("Website:");
      labelWeb.setFont(labelWeb.getFont().deriveFont(labelWeb.getFont().getStyle() | Font.BOLD));
      contentPanel.add(labelWeb, CC.xy(3, 9));

      //---- labelWebAddress ----
      labelWebAddress.setText("Application.homepage");
      labelWebAddress.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      labelWebAddress.setName("labelWebAddress");
      labelWebAddress.setDisabledIcon(null);
      contentPanel.add(labelWebAddress, CC.xy(5, 9));

      //======== scrollPane1 ========
      {

        //---- propertiesTable ----
        propertiesTable.setFocusable(false);
        propertiesTable.setName("featureTable");
        propertiesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        propertiesTable.setFillsViewportHeight(true);
        scrollPane1.setViewportView(propertiesTable);
      }
      contentPanel.add(scrollPane1, CC.xywh(1, 12, 5, 1));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
    
    /* Load text from global application resource*/
    Class<? extends Application> appClass = Application.getInstance().getClass();
    labelTitle.setText(ResourceUtils.getString(appClass, labelTitle.getText()));    
    labelDesp.setText(ResourceUtils.getString(appClass, labelDesp.getText()));    
    labelVendorName.setText(ResourceUtils.getString(appClass, labelVendorName.getText()));    
    labelWebAddress.setText(ResourceUtils.getString(appClass, labelWebAddress.getText()));    
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel labelLogo;
  private JLabel labelTitle;
  private JXLabel labelDesp;
  private JLabel labelVersionNo;
  private JLabel labelVendorName;
  private JXHyperlink labelWebAddress;
  private JScrollPane scrollPane1;
  private JTable propertiesTable;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
