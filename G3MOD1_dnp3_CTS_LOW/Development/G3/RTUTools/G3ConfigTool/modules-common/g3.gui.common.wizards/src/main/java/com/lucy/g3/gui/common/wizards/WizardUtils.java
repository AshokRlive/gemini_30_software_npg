/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.wizards;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.Window;

import javax.swing.JLabel;

import org.netbeans.spi.wizard.WizardPage;

/**
 * Wizard Utility.
 */
public class WizardUtils {

  public static Rectangle createRect(Window parent, int width, int height) {
    Rectangle rect = new Rectangle(width, height);
    setPositionRelativeToWindow(parent, rect);
    return rect;
  }
  
  private static void setPositionRelativeToWindow(Window parent, Rectangle rect) {
    if (parent != null) {
      rect.setLocation(parent.getBounds().getLocation());
    }
  }
  
  public static WizardPage createMessagePage(String title, String msg) {
    return new MessagePage(title, msg);
  }
  
  private static class MessagePage extends WizardPage {
    public MessagePage(String title, String msg){
      super(title); 
      
      //setBorder(Borders.DIALOG_BORDER);
      setLayout(new BorderLayout());
      JLabel lblMsg = new JLabel(msg);
      add(lblMsg, BorderLayout.CENTER);
    }
  }
  
}

