/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.itemlist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;

/**
 * Basic implementation of {@linkplain IItemListManager}.
 *
 * @param <ItemT>
 *          the type of items managed by this manager.
 */
public class ItemListManager<ItemT> extends Model implements IItemListManager<ItemT> {

  private Logger log = Logger.getLogger(ItemListManager.class);

  private final ArrayListModel<ItemT> items;


  public ItemListManager() {
    this(null);
  }

  public ItemListManager(ArrayListModel<ItemT> items) {
    if(items == null)
      items = new ArrayListModel<ItemT>(20);
    
    this.items = items;
  }
  
  protected ItemT get(int index) {
    if(index < items.size())
      return items.get(index);
    return null;
  }
  
  @Override
  public final Collection<ItemT> getAllItems() {
    ArrayList<ItemT> all = new ArrayList<ItemT>(items);
    all.removeAll(Collections.singleton(null)); // remove null items
    return all;
  }

  @SuppressWarnings("unchecked")
  @Override
  public final ListModel<ItemT> getItemListModel() {
    return items;
  }

  @Override
  public final boolean add(ItemT item) throws AddItemException {
    if (item == null) {
      throw new AddItemException("Cannot add null item to item manager");
    }

    if (contains(item)) {
      throw new AddItemException("Cannot add item which already exists: " + item);
    }

    if (!allowToAdd(item)) {
      log.error("Not allowed to add item:" + item);
      return false;
    }

    if (items.add(item)) {
      postAddAction(item);
      return true;
    } else {
      log.error("Failed to add item:" + item);
      return false;
    }
  }

  @Override
  public final void addAll(Collection<ItemT> items) throws AddItemException {
    if (items != null) {
      for (ItemT t : items) {
         add(t);
      }
    }
  }

  @Override
  public final boolean remove(ItemT item) {
    if (allowToRemove(item)) {
      if (items.remove(item)) {
        postRemoveAction(item);
        return true;
      } else {
        log.error("Failed to remove item:" + item);
        return false;
      }
    } else {
      log.error("Not allowed to remove item:" + item);
      return false;
    }
  }

  @Override
  public final void removeAll(Collection<ItemT> items) {
    if (items != null) {
      for (ItemT t : items) {
        remove(t);
      }
    }
  }

  @Override
  public final boolean contains(ItemT item) {
    return item == null ? false : this.items.contains(item);
  }

  @Override
  public final int getSize() {
    return this.items.getSize();
  }

  @Override
  public boolean allowToRemove(ItemT item) {
    return true;
  }

  @Override
  public boolean allowToAdd(ItemT item) throws AddItemException {
    return true;
  }

  final public boolean isEmpty() {
    return items.isEmpty();
  }
  
  /**
   * <p>
   * Post action of adding item.This method will be called when an item is added
   * successfully.
   * </p>
   * Overload this method if it is needed to handle something when adding item.
   *
   * @param addedItem
   *          the added item
   */
  protected void postAddAction(ItemT addedItem) {
  }

  /**
   * <p>
   * Post action of removing item.This method will be called when an item is
   * removed successfully.
   * </p>
   * Overload this method if it is needed to handle something when removing
   * item.
   *
   * @param removedItem
   *          the removed item
   */
  protected void postRemoveAction(ItemT removedItem) {
  }
  
  protected boolean swap(ItemT p0, ItemT p1) {
    // Swap position in list model.
    int index0 = items.indexOf(p0);
    int index1 = items.indexOf(p1);
    if (index0 >= 0 && index1 >= 0) {
      items.set(index0, p1);
      items.set(index1, p0);
      return true;
    } else {
      return false;
    }
  }
}
