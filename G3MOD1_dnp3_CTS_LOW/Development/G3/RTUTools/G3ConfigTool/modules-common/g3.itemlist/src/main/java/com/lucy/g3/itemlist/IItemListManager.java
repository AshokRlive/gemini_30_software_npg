/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.itemlist;

import java.util.Collection;

import javax.swing.ListModel;

/**
 * The interface of items manager which manages a list of item in the same type.
 *
 * @param <ItemT>
 *          the type of the managed items.
 */
public interface IItemListManager<ItemT> {

  /**
   * Adds a new item to this manager.
   *
   * @return <code>true</code> if succeeds adding the item.
   * @throws AddItemException
   *           if failed to add the item.
   */
  boolean add(ItemT item) throws AddItemException;

  /**
   * Add a collection of items to this manager.
   */
  void addAll(Collection<ItemT> items) throws AddItemException;

  /**
   * Removes an item from this manager.
   *
   * @param item
   *          the item to be removed.
   * @return <code>true</code> if succeeds removing the item..
   */
  boolean remove(ItemT item);

  /**
   * Remove a collection of items from this manager.
   */
  void removeAll(Collection<ItemT> items);

  /**
   * Checks if this manager contains an item.
   *
   * @param item
   *          the item to be checked.
   * @return <code>true</code> if it is contained, <code>false</code> otherwise.
   */
  boolean contains(ItemT item);

  /**
   * Checks if it is allowed to remove an item from this manager by users.
   *
   * @param item
   *          the non-null item to be removed.
   * @return <code>true</code> if allowed, <code>true</code> otherwise.
   */
  boolean allowToRemove(ItemT item);

  /**
   * Checks if it is allowed to add an item to this manager by users.
   *
   * @param item
   *          the non-null item to be added.
   * @return <code>true</code> if allowed, <code>true</code> otherwise.
   */
  boolean allowToAdd(ItemT item) throws AddItemException;

  /**
   * Gets all items contained by this manager.
   *
   * @return the all items
   */
  Collection<ItemT> getAllItems();

  /**
   * Gets the list model of items contained by this manager.
   */
  ListModel<ItemT> getItemListModel();

  /**
   * Gets the total number of items.
   *
   * @return the size of items.
   */
  int getSize();


  /**
   * This exception should be thrown when it is not able to add an item to a
   * list.
   */
  public static class AddItemException extends RuntimeException {

    public AddItemException(String msg) {
      super(msg);
    }
  }

}
