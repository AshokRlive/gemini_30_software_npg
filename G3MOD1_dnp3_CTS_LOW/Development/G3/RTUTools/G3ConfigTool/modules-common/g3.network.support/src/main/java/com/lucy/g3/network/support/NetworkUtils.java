/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.network.support;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;

/**
 *
 */
public class NetworkUtils {

  public static byte[] getLocalHostIP() {
    byte[] ip = null;

    try {
      ip = InetAddress.getLocalHost().getAddress();
    } catch (UnknownHostException e) {
      // Fail to get IP.
    }

    return ip;
  }

  public static byte[] getLocalMac() {
    byte[] mac = null;

    try {
      InetAddress ip = InetAddress.getLocalHost();
      if (ip != null) {
        NetworkInterface network = NetworkInterface.getByInetAddress(ip);

        if (network != null) {
          mac = network.getHardwareAddress();
        }
      }

    } catch (Exception e) {
      // Fail to get MAC
    }

    return mac;
  }

  public static String macToString(byte[] mac) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < mac.length; i++) {
      sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
    }
    return sb.toString();
  }

}

