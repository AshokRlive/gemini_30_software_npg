/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.network.support;

import java.text.ParseException;
import java.util.Arrays;


/**
 * The IP v4 address.
 */
public class IPAddress {

  private static final int IP_BYTE_NUM = 4;

  // private final int[] rawIpAddress = new int[IP_BYTE_NUM];
  private String ipAddressStr;

  private final boolean allowBlank;
  private final boolean allowAny;


  public IPAddress(String ipAddressStr) throws ParseException {
    this(ipAddressStr, false, false);
  }

  public IPAddress(String ipAddressStr, boolean allowBlank, boolean allowAny) throws ParseException {
    this.allowBlank = allowBlank;
    this.allowAny = allowAny;
    parse(ipAddressStr);
  }

  /**
   * Gets the IP address string.
   *
   * @return the IP address string
   */
  public String getIPAddressString() {
    return ipAddressStr;
  }

  /**
   * Tries to get the raw IP address.
   *
   * @return the raw IP address
   * @throws ParseException
   *           if the IP address is blank or {@link #ANY_IP_STRING}.
   */
  public int[] getRawIPAddress() throws ParseException {
    String[] ipStr = ipAddressStr.split("\\.");
    int[] rawIpAddress = new int[IP_BYTE_NUM];
    try {
      for (int i = 0; i < rawIpAddress.length; i++) {
        rawIpAddress[i] = Integer.parseInt(ipStr[i]);
      }
      return rawIpAddress;
    } catch (Throwable e) {
      throw new ParseException("Cannot get raw IP from:" + ipAddressStr, 0);
    }
  }

  public final void parse(String ipAddress) throws ParseException {
    if (validateIPAddress(ipAddress, allowBlank, allowAny)) {
      this.ipAddressStr = ipAddress;
    } else {
      throw new ParseException("cannot parser IP address: " + ipAddress, 0);
    }
  }

  @Override
  public String toString() {
    return getIPAddressString();
  }

  @Override
  public final int hashCode() {
    String ipAddressStr = getIPAddressString();
    final int prime = 31;
    int result = 1;
    result = prime * result + ((ipAddressStr == null) ? 0 : ipAddressStr.hashCode());
    return result;
  }

  @Override
  public final boolean equals(Object obj) {
    String ipAddressStr = getIPAddressString();

    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    IPAddress other = (IPAddress) obj;
    if (ipAddressStr == null) {
      if (other.getIPAddressString() != null) {
        return false;
      }
    } else if (!ipAddressStr.equals(other.getIPAddressString())) {
      return false;
    }
    return true;
  }

  // ================== Static ===================

  public static IPAddress valueOf(int[] rawIPAddress) throws ParseException {
    if (rawIPAddress == null || rawIPAddress.length != IP_BYTE_NUM) {
      throw new ParseException("Invalid raw IP address: " + Arrays.toString(rawIPAddress), 0);
    }
    
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < rawIPAddress.length; i++) {
      sb.append(rawIPAddress[i]);
      if(i < rawIPAddress.length - 1)
        sb.append(".");
    }
    String ipAddressStr = sb.toString();
    return valueOf(ipAddressStr, false, false);
  }

  public static IPAddress valueOf(String ipAddress) throws ParseException {
    return valueOf(ipAddress, false, false);
  }

  public static IPAddress valueOf(String ipAddress, boolean allowBlank) throws ParseException {
    return valueOf(ipAddress, allowBlank, false);
  }

  public static IPAddress valueOf(String ipAddress, boolean allowBlank, boolean allowAny) throws ParseException {
    return new IPAddress(ipAddress, allowBlank, allowAny);
  }

  
  public static boolean validateIPPort(long ipPort) {
    return ipPort > 0;
  }
  
  public static boolean validateIPAddress(String ipAddress) {
    return validateIPAddress(ipAddress, false, false);
  }

  public static boolean validateIPAddress(String ipAddress, boolean allowBlank) {
    return validateIPAddress(ipAddress, allowBlank, false);
  }

  /**
   * Check if a string is representing an IP address, with leading and trailing
   * whitespace omitted.
   *
   * @param ipAddress
   *          the IP address string
   */
  public static boolean validateIPAddress(String ipAddress, boolean allowBlank, boolean allowAny) {
    if (ipAddress == null) {
      return false;
    }

    return ipAddress.matches(getPattern(allowBlank, allowAny));
  }

  public static String getPattern(boolean allowBlank, boolean allowAny) {
    String pattern;

    if (allowBlank && allowAny) {
      pattern = IP_PATTERN_WITH_ANY_AND_BLANK;
    } else if (allowBlank) {
      pattern = IP_PATTERN_WITH_BLANK;
    } else if (allowAny) {
      pattern = IP_PATTERN_WITH_ANY;
    } else {
      pattern = IP_PATTERN;
    }

    return pattern;
  }

  /**
   * Applies submask to a IP address.
   */
  public static IPAddress applySubMask(IPAddress ip, IPAddress submask) throws ParseException {
    if(submask == null)
     throw new IllegalArgumentException("submask must not be null");
    
    if(ip == null)
      throw new IllegalArgumentException("ip must not be null");
    
    int[] mask = submask.getRawIPAddress();
    int[] raw = ip.getRawIPAddress();

    for (int i = 0; i < raw.length; i++) {
      raw[i] &= mask[i];
    }

    return valueOf(raw);
  }


  /**
   * IP address regx pattern.
   */
  public static final String IP_PATTERN =
      "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
          + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
          + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
          + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

  /**
   * IP address regx pattern that allows blank string.
   */
  public static final String IP_PATTERN_WITH_BLANK = IP_PATTERN + "|()";

  /**
   * IP address regx pattern that allows {@value #ANY_IP_STRING}.
   */
  public static final String IP_PATTERN_WITH_ANY = IP_PATTERN + "|(\\*\\.\\*\\.\\*\\.\\*)";

  /**
   * IP address regx pattern that allows {@value #ANY_IP_STRING} and blank
   * string.
   */
  public static final String IP_PATTERN_WITH_ANY_AND_BLANK = IP_PATTERN_WITH_BLANK + "|(\\*\\.\\*\\.\\*\\.\\*)";

  /**
   * The string of "Any IP".
   */
  public static final String ANY_IP_STRING = "*.*.*.*";

}
