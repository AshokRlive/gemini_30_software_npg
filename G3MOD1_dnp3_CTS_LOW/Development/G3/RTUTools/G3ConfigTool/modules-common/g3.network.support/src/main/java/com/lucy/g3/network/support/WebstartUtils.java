
package com.lucy.g3.network.support;

import java.lang.reflect.Method;
import java.net.URL;

public class WebstartUtils {
  /**
   * GetS the codebase with JNLP service if the application is launched from
   * webstarter.
   *
   * @return the found codebase, maybe null.
   */
  public static URL getWebstartCodeBase() {
    URL codebase = null;
    try {
      Class<?> smClass = Class.forName("javax.jnlp.ServiceManager");
      Class<?> bsClass = Class.forName("javax.jnlp.BasicService");

      Method lookupMethod = smClass.getMethod("lookup", String.class);
      Object bsObj = lookupMethod.invoke(null, "javax.jnlp.BasicService");

      Method getCodebaseMethod = bsClass.getMethod("getCodeBase");
      codebase = (URL) getCodebaseMethod.invoke(bsObj);
    } catch (Exception e) {
      System.err.println("Cannot get codeBase. JNLP service not found: " + e.getMessage());
    }

    return codebase;
  }

  public static Boolean isWebstartedOffline() {
    Boolean offline = null;
    try {
      Class<?> smClass = Class.forName("javax.jnlp.ServiceManager");
      Class<?> bsClass = Class.forName("javax.jnlp.BasicService");

      Method lookupMethod = smClass.getMethod("lookup", String.class);
      Object bsObj = lookupMethod.invoke(null, "javax.jnlp.BasicService");

      Method isOffLineMethod = bsClass.getMethod("isOffline");
      offline = (Boolean) isOffLineMethod.invoke(bsObj);
    } catch (Exception e) {
      System.err.println("Cannot get Webstart offline state. JNLP service not found: " + e.getMessage());
    }

    return offline;
  }

  public static boolean isRunningJavaWebStart() {
    return System.getProperty("javawebstart.version", null) != null;
  }
}

