/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.network.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.network.support.IPAddress;

/**
 * The Class IPAddressTest.
 */
public class IPAddressTest {

  protected IPAddress fixture;


  @Before
  public void setup() throws ParseException {
    fixture = new IPAddress("0.0.0.0", false, false);
  }

  @Test
  public void testParse() throws ParseException {
    fixture.parse("0.0.0.0");
    fixture.parse("255.255.255.255");
  }

  @Test(expected = ParseException.class)
  public void testParseInvalid() throws ParseException {
    fixture.parse("-1.0.0.0");
  }

  @Test(expected = ParseException.class)
  public void testParseInvalid2() throws ParseException {
    fixture.parse("(*.*.*.*");
  }

  @Test(expected = ParseException.class)
  public void testParseInvalid3() throws ParseException {
    fixture.parse("(1.1.1.256");
  }

  @Test
  public void testGetString() throws ParseException {
    testGetString("0.0.0.0");
    testGetString("255.255.255.255");
    testGetString("1.1.1.1");
  }

  protected final void testGetString(String ip) throws ParseException {
    fixture.parse(ip);
    Assert.assertSame(ip, fixture.getIPAddressString());
  }

  @Test
  public void testcheckIPAddress() {
    assertFalse(IPAddress.validateIPAddress(null));
    assertFalse(IPAddress.validateIPAddress(""));
    assertFalse(IPAddress.validateIPAddress("123"));
    assertFalse(IPAddress.validateIPAddress("1.1.1.256"));
    assertFalse(IPAddress.validateIPAddress("-1.1.1.256"));
    assertFalse(IPAddress.validateIPAddress("  255.255.255.255  "));

    assertTrue(IPAddress.validateIPAddress("0.0.0.0"));
    assertTrue(IPAddress.validateIPAddress("255.255.255.255"));
  }

  @Test
  public void testEquals() throws ParseException {
    String ip = "1.2.3.4";
    IPAddress ip0 = IPAddress.valueOf(ip);
    IPAddress ip1 = IPAddress.valueOf(ip);
    assertTrue("expect two IP equal", ip0.equals(ip1));
  }

  @Test
  public void testNotEquals() throws ParseException {
    IPAddress ip0 = IPAddress.valueOf("1.2.3.4");
    IPAddress ip1 = IPAddress.valueOf("1.2.3.5");
    assertFalse("expect two IP not equal", ip0.equals(ip1));
  }

  @Test
  public void testValueOf() throws ParseException {
    int[] raw = { 1, 2, 3, 4 };
    IPAddress ip = IPAddress.valueOf(raw);
    assertNotNull(ip);
    assertEquals("1.2.3.4", ip.toString());
  }

  @Test
  public void testApplyMask() throws ParseException {
    IPAddress ip0 = IPAddress.valueOf("10.11.11.102");
    IPAddress ip1 = IPAddress.valueOf("10.11.12.103");
    IPAddress mask0 = IPAddress.valueOf("255.255.0.0");
    IPAddress mask1 = IPAddress.valueOf("255.255.255.0");

    assertTrue(IPAddress.applySubMask(ip0, mask0).equals(IPAddress.applySubMask(ip1, mask0)));
    assertFalse(IPAddress.applySubMask(ip0, mask1).equals(IPAddress.applySubMask(ip1, mask1)));

  }

  @Test
  public void testPatternWithBlank() {
    assertTrue("blank string should be legal", "".matches(IPAddress.IP_PATTERN_WITH_BLANK));
    assertFalse("blank string should be illegal", "".matches(IPAddress.IP_PATTERN));
  }
}
