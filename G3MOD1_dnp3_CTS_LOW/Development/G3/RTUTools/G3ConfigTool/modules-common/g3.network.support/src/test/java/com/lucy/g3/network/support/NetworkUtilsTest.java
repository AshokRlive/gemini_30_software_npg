/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.network.support;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;


public class NetworkUtilsTest {
  @Test@Ignore //Failed without network permission.
  public void testGetLocalHostIP() {
    assertNotNull(NetworkUtils.getLocalHostIP());
  }
  
  @Test @Ignore // Failed without network permission.
  public void testGetLocalMac() {
    assertNotNull(NetworkUtils.getLocalMac());
  }
  
  
}

