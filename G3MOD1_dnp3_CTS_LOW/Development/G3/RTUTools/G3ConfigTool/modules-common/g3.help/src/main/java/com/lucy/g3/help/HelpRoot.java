/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.help;

import java.io.*;
import java.net.URL;
import java.security.*;
import java.util.Enumeration;
import java.util.jar.*;

/**
 * Gets the help root path - See developer notes at end of class.
 * Developer notes There are three scenarios for the help to be called
 * under... 1) Local class files (eg running via Eclipse) 2) Local jar file 3)
 * RTU jar file The end result for each is... 1) The path is configured to the
 * target\classes folder within the g3.help project folder. The help files are
 * not present so the help system will not display the helkp pages. It is
 * possible to test the pages by copying the entire help folder from the
 * source position into the target\classes folder. 2) The path is correctly
 * configured and the help files are unpacked from the jar and placed adjacent
 * to the jar (ie they will have the same parent folder. It is possible to
 * test the help system directly. 3) The original path is correct, and the
 * location is understood but the RTU blocks any attempt to unpack the files.
 * As a result the unpack process fails, returning a null for the path which
 * is propagated. This eventually triggers a dirty reset of the path (via the
 * G3ConfigTool MainApp & MainApplet classes) to the RTU IP address. This
 * already holds the unpacked help files, so is testable.
 */
class HelpRoot {

  /* Constants */
  public static final String JAR = "jar";

  private static final String JPROTO = ".JAR";
  private static final String HPROTO = "HTTP:";
  public static final String FPROTO = "FILE:///";


  public static String getDefaultRoot() {
    return new HelpRoot().getRootPath();
  }

  /* Private constructor */
  private HelpRoot() {
  }

  /* Get the root path */
  private String getRootPath() {
    String path = null;

    Class<?> clz = HelpRoot.class;
    ProtectionDomain pd = clz.getProtectionDomain();
    CodeSource cs = pd.getCodeSource();
    URL url = cs.getLocation();

    path = url.toString();
    path = path.toUpperCase();

    if (path.startsWith(HPROTO)) {
      path = handleRemote(path);
    } else if (path.endsWith(JPROTO)) {
      path = handleLocal(path);
    } else {
      path = handleClass(path);
    }

    return path;
  }

  /* Handle remote file system jar ie over browser */
  private String handleRemote(String path) {

    File f = new File(path);
    path = f.getParent();
    path = path.replace("HTTP:\\", "HTTP://");
    path = path + "/";

    return path;
  }

  /* Handle local file system jar */
  private String handleLocal(String path) {

    path = path.substring(5);
    path = path.replaceAll("%20", " "); //TODO FIXME
    path = getJar(path);
    path = FPROTO + path;
    return path;
  }

  /* Handle elipse/compile run, not jar */
  private String handleClass(String path) {

    /* TODO See developer notes at end of class */

    return path;
  }

  /* Extract files and set file path for compiled help (a jar) */
  private String getJar(String path) {

    try {

      JarFile jar = new JarFile(path);
      Enumeration<JarEntry> entries = jar.entries();
      JarEntry entry;

      path = getPath(path);
      entry = getFirst(entries);

      write(path, jar, entry);
      getHelp(path, jar, entries);
      path = path.substring(1);

      jar.close();

    } catch (Throwable t) {
      path = null;
    }

    return path;
  }

  /* Modify the path to the extracted files */
  private String getPath(String path) {

    int i = path.lastIndexOf("/");
    path = path.substring(0, i);
    path = path + "/";

    return path;
  }

  /* Get the first help entry */
  private JarEntry getFirst(Enumeration<JarEntry> entries) {
    JarEntry result = null;

    String name;
    while (entries.hasMoreElements()) {
      result = entries.nextElement();
      name = result.getName();
      if (name.startsWith("help")) {
        break;
      }
      result = null;
    }

    return result;
  }

  /* Iterate the help entries */
  private void getHelp(String path, JarFile jar, Enumeration<JarEntry> entries) {

    JarEntry entry;
    String name;

    while (entries.hasMoreElements()) {
      entry = entries.nextElement();
      name = entry.getName();
      if (name.startsWith("help")) {
        write(path, jar, entry);
      } else {
        break;
      }
    }

  }

  /* Write a help file out */
  private void write(String path, JarFile jar, JarEntry entry) {

    File out = new File(path, entry.getName());
    if (entry.isDirectory()) {
      out.mkdir();
    } else {
      try {
        InputStream ins = new BufferedInputStream(jar.getInputStream(entry));
        OutputStream ous = new BufferedOutputStream(new FileOutputStream(out));
        byte[] buffer = new byte[2048];
        for (;;) {
          int nBytes = ins.read(buffer);
          if (nBytes <= 0)
            break;
          ous.write(buffer, 0, nBytes);
        }
        ous.flush();
        ous.close();
        ins.close();
      } catch (Throwable t) {
        t.printStackTrace();
      }
    }

  }

}
