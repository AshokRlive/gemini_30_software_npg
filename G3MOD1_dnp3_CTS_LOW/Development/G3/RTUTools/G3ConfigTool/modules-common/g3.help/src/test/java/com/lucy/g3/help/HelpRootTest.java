/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.help;

import org.junit.Test;


public class HelpRootTest {

  @Test
  public void test() {
    System.out.println("root:" + HelpRoot.getDefaultRoot());
  }

}

