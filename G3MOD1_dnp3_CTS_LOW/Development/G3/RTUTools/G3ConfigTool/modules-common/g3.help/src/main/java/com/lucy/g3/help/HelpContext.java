/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.help;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.ref.WeakReference;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.tree.TreePath;

/******************************************************************************
 * Gets the selected UI component and returns the help ID
 *****************************************************************************/
class HelpContext {
  
  /* Static convenience method */
  public static String getContext(ActionEvent event) {

    HelpContext ctx = new HelpContext();
    String str = ctx.doit(event);

    return str;
  }


  /******************************************************************************
   * Following code lifted from javax.help.CSH and tidied
   *****************************************************************************/
  private Component source;
  private ArrayList<Component> parent;
  private Hashtable<Component, Cursor> cursors;
  private String context;


  /* Get the context help ID */
  private String doit(ActionEvent evt) {
    try {
      preSelection(evt);

      MouseEvent selection = getMouseEvent();

      postSelection(selection);

    } finally {
      resetCursor();
      dinitTrack();

    }

    return context;
  }

  /* Do the context setup */
  private void preSelection(ActionEvent evt) {
    initTrack(evt);
    getRoot(source);
    getApplets();
    getFrames();
    setCursor(Helper.getCursor());
  }

  /* Initialise members etc */
  private void initTrack(ActionEvent event) {

    source = (Component) event.getSource();
    cursors = new Hashtable<Component, Cursor>();
    parent = new ArrayList<Component>();

  }

  /* Get the root container */
  private void getRoot(Object comp) {
    Object parent = comp;

    while (parent != null) {
      comp = parent;

      if (comp instanceof MenuComponent) {
        parent = ((MenuComponent) comp).getParent();

      } else if (comp instanceof Component) {
        if (comp instanceof Window) {
          break;
        } else if (comp instanceof Applet) {
          break;
        } else {
          parent = ((Component) comp).getParent();
        }

      } else {
        break;
      }
    }

    if (comp instanceof Component) {
      source = ((Component) comp);
    }
  }

  /* Get the parent Applets */
  private void getApplets() {

    if (source instanceof Applet) {
      try {
        Applet ap = (Applet) source;
        AppletContext ac = ap.getAppletContext();
        Enumeration<Applet> ea = ac.getApplets();

        Applet child;
        while (ea.hasMoreElements()) {
          child = ea.nextElement();
          parent.add(child);
        }

      } catch (NullPointerException npe) {
        parent.add(source);
      }
    }

  }

  /* Get the parent Frames */
  private void getFrames() {

    Frame frames[] = Frame.getFrames();
    for (Frame f : frames) {

      Window[] windows = f.getOwnedWindows();
      for (Window w : windows) {
        parent.add(w);
      }

      if (parent.contains(f)) {
        // Do not add
      } else {
        parent.add(f);
      }

    }
  }

  /* Iterate parent Component setting all cursors to help */
  private void setCursor(Cursor cursor) {
    if (parent != null) {
      for (Component c : parent) {
        setCursor(c, cursor);
      }
    }

  }

  /* Set the help cursor for a component */
  private void setCursor(Component comp, Cursor cursor) {

    if (comp == null) {
      // No action

    } else {
      Cursor tcurs = comp.getCursor();

      if (tcurs != cursor) {
        cursors.put(comp, tcurs);
        comp.setCursor(cursor);
      }

      Container tcon;
      if (comp instanceof Container) {
        tcon = (Container) comp;
        Component[] acomp = tcon.getComponents();
        for (Component c : acomp) {
          setCursor(c, cursor);
        }
      }

    }
  }

  /* Iterate parent Component resetting all cursors */
  private void resetCursor() {
    if (parent != null) {
      for (Component c : parent) {
        resetCursor(c);
      }
    }
  }

  /* Reset the cursor for a component */
  private void resetCursor(Component comp) {

    if (comp == null) {
      // No action

    } else {
      Cursor tcurs = cursors.get(comp);

      if (tcurs != null) {
        comp.setCursor(tcurs);
      }

      Container tcon;
      if (comp instanceof Container) {
        tcon = (Container) comp;
        Component[] acomp = tcon.getComponents();
        for (Component c : acomp) {
          resetCursor(c);
        }
      }

    }
  }

  /* De-initialise members etc */
  private void dinitTrack() {

    source = null;
    cursors = null;
    parent = null;

  }


  /******************************************************************************
   * Following code lifted from javax.help.CSH and requires substantial work
   *****************************************************************************/
  @SuppressWarnings("rawtypes")
  static private java.util.Map comps;
  @SuppressWarnings("rawtypes")
  static private java.util.Map parents;


  /* Get the component from the selection event */
  private void postSelection(MouseEvent event) {
    if (event != null) {
      Object obj = getDeepestObjectAt(event.getSource(), event.getX(), event.getY());

      if (obj != null) {
        context = getHelpIDString(obj, event);

      }
    }
  }

  private static MouseEvent getMouseEvent() {
    try {
      if (EventQueue.isDispatchThread()) {
        EventQueue eq = null;
        try {
          eq = Toolkit.getDefaultToolkit().getSystemEventQueue();
        } catch (Exception ee) {
          System.err.println(ee);
        }

        if (eq == null) {
          return null;
        }

        int eventNumber = -1;

        while (true) {

          eventNumber++;
          AWTEvent event = eq.getNextEvent();
          Object src = event.getSource();

          if (event instanceof ActiveEvent) {
            ((ActiveEvent) event).dispatch();
            continue;
          }

          if (src instanceof Component) {

            if (event instanceof KeyEvent) {
              KeyEvent e = (KeyEvent) event;

              if (e.getKeyCode() == KeyEvent.VK_CANCEL ||
                  e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                e.consume();
                return null;
              } else {
                e.consume();
                // dispatchEvent(event);
              }
            } else if (event instanceof MouseEvent) {
              MouseEvent e = (MouseEvent) event;
              int eID = e.getID();
              if ((eID == MouseEvent.MOUSE_CLICKED ||
                  eID == MouseEvent.MOUSE_PRESSED ||
                  eID == MouseEvent.MOUSE_RELEASED) &&
                  SwingUtilities.isLeftMouseButton(e)) {
                if (eID == MouseEvent.MOUSE_CLICKED) {
                  if (eventNumber == 0) {
                    dispatchEvent(event);
                    continue;
                  }
                }
                e.consume();
                return e;
              } else {
                e.consume();
              }
            } else {
              dispatchEvent(event);
            }
          } else if (src instanceof MenuComponent) {
            if (event instanceof InputEvent) {
              ((InputEvent) event).consume();
            }
          } else {
            System.err.println("unable to dispatch event: " + event);
          }
        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return null;
  }

  private static void dispatchEvent(AWTEvent event) {
    Object src = event.getSource();
    if (event instanceof ActiveEvent) {
      // This could become the sole method of dispatching in time.
      ((ActiveEvent) event).dispatch();
    } else if (src instanceof Component) {
      ((Component) src).dispatchEvent(event);
    } else if (src instanceof MenuComponent) {
      ((MenuComponent) src).dispatchEvent(event);
    } else {
      System.err.println("unable to dispatch event: " + event);
    }
  }

  private static Object getDeepestObjectAt(Object parent, int x, int y) {
    if (parent instanceof Container) {
      Container cont = (Container) parent;
      // use a copy of 1.3 Container.findComponentAt
      Component child = findComponentAt(cont, cont.getWidth(), cont.getHeight(), x, y);
      if (child != null && child != cont) {
        if (child instanceof JRootPane) {
          JLayeredPane lp = ((JRootPane) child).getLayeredPane();
          Rectangle b = lp.getBounds();
          child = (Component) getDeepestObjectAt(lp, x - b.x, y - b.y);
        }
        if (child != null) {
          return child;
        }
      }
    }

    return parent;
  }

  private static Component findComponentAt(Container cont, int width, int height, int x, int y) {
    synchronized (cont.getTreeLock()) {

      if (!((x >= 0) && (x < width) && (y >= 0) && (y < height) && cont.isVisible() && cont.isEnabled())) {
        return null;
      }

      Component[] component = cont.getComponents();
      int ncomponents = cont.getComponentCount();

      // Two passes: see comment in sun.awt.SunGraphicsCallback
      for (int i = 0; i < ncomponents; i++) {
        Component comp = component[i];
        Rectangle rect = null;

        if (comp instanceof CellRendererPane) {
          Component c = getComponentAt((CellRendererPane) comp, x, y);
          if (c != null) {
            rect = getRectangleAt((CellRendererPane) comp, x, y);
            comp = c;
          }
        }

        if (comp != null && !comp.isLightweight()) {
          if (rect == null || rect.width == 0 || rect.height == 0) {
            rect = comp.getBounds();
          }
          if (comp instanceof Container) {
            comp = findComponentAt((Container) comp, rect.width, rect.height, x - rect.x, y - rect.y);
          } else {
            comp = comp.getComponentAt(x - rect.x, y - rect.y);
          }
          if (comp != null && comp.isVisible() && comp.isEnabled()) {
            return comp;
          }
        }
      }

      for (int i = 0; i < ncomponents; i++) {
        Component comp = component[i];
        Rectangle rect = null;

        if (comp instanceof CellRendererPane) {
          Component c = getComponentAt((CellRendererPane) comp, x, y);
          if (c != null) {
            rect = getRectangleAt((CellRendererPane) comp, x, y);
            comp = c;
          }
        }

        if (comp != null && comp.isLightweight()) {
          if (rect == null || rect.width == 0 || rect.height == 0) {
            rect = comp.getBounds();
          }
          if (comp instanceof Container) {
            comp = findComponentAt((Container) comp, rect.width, rect.height, x - rect.x, y - rect.y);
          } else {
            comp = comp.getComponentAt(x - rect.x, y - rect.y);
          }
          if (comp != null && comp.isVisible() && comp.isEnabled()) {
            return comp;
          }
        }
      }
      return cont;
    }
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  private static Component getComponentAt(CellRendererPane cont, int x, int y) {
    Component comp = null;
    Container c = cont.getParent();
    // I can process only this four components at present time
    if (c instanceof JTable) {
      comp = getComponentAt((JTable) c, x, y);
    } else if (c instanceof JTableHeader) {
      comp = getComponentAt((JTableHeader) c, x, y);
    } else if (c instanceof JTree) {
      comp = getComponentAt((JTree) c, x, y);
    } else if (c instanceof JList) {
      comp = getComponentAt((JList) c, x, y);
    }

    // store reference from comp to CellRendererPane
    // It is needed for backtrack searching of HelpSet and HelpID
    // in getHelpSet() and getHelpIDString().
    if (comp != null) {
      if (parents == null) {
        // WeakHashMap of WeakReferences
        parents = new WeakHashMap(4) {

          @Override
          public Object put(Object key, Object value) {
            return super.put(key, new WeakReference(value));
          }

          @Override
          public Object get(Object key) {
            WeakReference wr = (WeakReference) super.get(key);
            if (wr != null) {
              return wr.get();
            } else {
              return null;
            }
          }
        };
      }
      parents.put(comp, cont);
    }
    return comp;
  }

  private static Component getComponentAt(JTableHeader header, int x, int y) {
    try {

      if (!(header.contains(x, y) && header.isVisible() && header.isEnabled())) {
        return null;
      }

      TableColumnModel columnModel = header.getColumnModel();
      int columnIndex = columnModel.getColumnIndexAtX(x);
      TableColumn column = columnModel.getColumn(columnIndex);

      TableCellRenderer renderer = column.getHeaderRenderer();
      if (renderer == null) {
        renderer = header.getDefaultRenderer();
      }

      return renderer.getTableCellRendererComponent(
          header.getTable(), column.getHeaderValue(), false, false, -1, columnIndex);

    } catch (Exception e) {
      // NullPointerException in case of (header == null) or (columnModel ==
      // null)
      // ArrayIndexOutOfBoundsException from getColumn(columnIndex)
    }
    return null;
  }

  public static String getHelpIDString(Object comp, AWTEvent evt) {

    if (comp == null) {
      return null;
    }

    String helpID = _getHelpIDString(comp);

    if (helpID == null) {
      helpID = getHelpIDString(getParent(comp), evt);
    }

    return helpID;
  }

  @SuppressWarnings("rawtypes")
  private static String _getHelpIDString(Object comp) {
    String helpID = null;
    if (comp != null) {
      if (comp instanceof JComponent) {
        helpID = (String) ((JComponent) comp).getClientProperty(Help.PROPERTY_HELP_PATH);
      } else if ((comp instanceof Component) || (comp instanceof MenuItem)) {
        if (comps != null) {
          Hashtable clientProps = (Hashtable) comps.get(comp);
          if (clientProps != null) {
            helpID = (String) clientProps.get(Help.PROPERTY_HELP_PATH);
          }
        }
      } else {
        throw new IllegalArgumentException("Invalid Component");
      }
    }
    return helpID;
  }

  private static Object getParent(Object comp) {

    if (comp == null) {
      return null;
    }

    Object parent = null;
    if (comp instanceof MenuComponent) {
      parent = ((MenuComponent) comp).getParent();
    } else if (comp instanceof JPopupMenu) {
      parent = ((JPopupMenu) comp).getInvoker();
    } else if (comp instanceof Component) {
      parent = ((Component) comp).getParent();
    } else {
      throw new IllegalArgumentException("Invalid Component");
    }

    if (parent == null && parents != null) {
      parent = parents.get(comp);
    }

    return parent;
  }

  private static Component getComponentAt(JTree tree, int x, int y) {
    try {

      TreePath path = tree.getPathForLocation(x, y);

      if (tree.isEditing() && tree.getSelectionPath() == path) {
        return null;
      }

      int row = tree.getRowForPath(path);
      Object value = path.getLastPathComponent();
      boolean isSelected = tree.isRowSelected(row);
      boolean isExpanded = tree.isExpanded(path);
      boolean isLeaf = tree.getModel().isLeaf(value);
      boolean hasFocus = tree.hasFocus() && tree.getLeadSelectionRow() == row;

      return tree.getCellRenderer().getTreeCellRendererComponent(
          tree, value, isSelected, isExpanded, isLeaf, row, hasFocus);

    } catch (Exception e) {
      return null;
    }
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  private static Component getComponentAt(JList list, int x, int y) {
    try {

      int index = list.locationToIndex(new Point(x, y));
      Object value = list.getModel().getElementAt(index);
      boolean isSelected = list.isSelectedIndex(index);
      boolean hasFocus = list.hasFocus() && list.getLeadSelectionIndex() == index;

      return list.getCellRenderer().getListCellRendererComponent(
          list, value, index, isSelected, hasFocus);
    } catch (Exception e) {
      return null;
    }
  }

  @SuppressWarnings("rawtypes")
  private static Rectangle getRectangleAt(CellRendererPane cont, int x, int y) {
    Rectangle rect = null;
    Container c = cont.getParent();
    // I can process only this four components at present time
    if (c instanceof JTable) {
      rect = getRectangleAt((JTable) c, x, y);
    } else if (c instanceof JTableHeader) {
      rect = getRectangleAt((JTableHeader) c, x, y);
    } else if (c instanceof JTree) {
      rect = getRectangleAt((JTree) c, x, y);
    } else if (c instanceof JList) {
      rect = getRectangleAt((JList) c, x, y);
    }
    return rect;
  }

  private static Component getComponentAt(JTable table, int x, int y) {
    try {

      if (!(table.contains(x, y) && table.isVisible() && table.isEnabled())) {
        return null;
      }

      Point point = new Point(x, y);
      int row = table.rowAtPoint(point);
      int column = table.columnAtPoint(point);

      if (table.isEditing() && table.getEditingRow() == row && table.getEditingColumn() == column) {
        // Pointed component is provided by TableCellEditor. Editor
        // component is part of component hierarchy so it is checked
        // directly in loop in findComponentAt()
        // comp = table.getEditorComponent();
        return null;
      }

      TableCellRenderer renderer = table.getCellRenderer(row, column);
      return table.prepareRenderer(renderer, row, column);

    } catch (Exception e) {
    }
    return null;
  }

  private static Rectangle getRectangleAt(JTree tree, int x, int y) {
    Rectangle rect = null;
    try {
      TreePath path = tree.getPathForLocation(x, y);
      rect = tree.getPathBounds(path);
    } catch (Exception e) {
    }
    return rect;
  }

  @SuppressWarnings("rawtypes")
  private static Rectangle getRectangleAt(JList list, int x, int y) {
    Rectangle rect = null;
    try {
      int index = list.locationToIndex(new Point(x, y));
      rect = list.getCellBounds(index, index);
    } catch (Exception e) {
    }
    return rect;
  }

  private static Rectangle getRectangleAt(JTable table, int x, int y) {
    Rectangle rect = null;
    try {
      Point point = new Point(x, y);
      int row = table.rowAtPoint(point);
      int column = table.columnAtPoint(point);
      rect = table.getCellRect(row, column, true);
    } catch (Exception e) {
    }
    return rect;
  }

  private static Rectangle getRectangleAt(JTableHeader header, int x, int y) {
    Rectangle rect = null;
    try {
      int column = header.columnAtPoint(new Point(x, y));
      rect = header.getHeaderRect(column);
    } catch (Exception e) {
    }
    return rect;
  }

  /* End */
}
