/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.help;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;


/******************************************************************************
 * Static convenience wrapper for Help.
 *****************************************************************************/
public class Helper {

  /** Static initialisation */
  private static final Help help = new Help();


  /** Get the help cursor */
  public static Cursor getCursor() {
    return help.getCursor();
  }

  /** Get the help icon */
  public static Icon getIcon() {
    return help.getIcon();
  }

  /** Set the root help path */
  public static void setRoot(String root) {
    help.setRoot(root);
  }

  /** Get the root help path */
  public static String getRoot() {
    return help.getRoot();
  }

  /** Register the F1 key to (highest) component */
  public static void register(JComponent component) {
    help.register(component);
  }

  /** Register a context help button. */
  public static void register(AbstractButton button) {
    help.register(button);
  }

  /** Register a button with a help page. */
  public static void register(AbstractButton button, String id) {
    help.register(button, id);
  }

  /**
   * Register a Swing component with a help page. This is when a widget is
   * clicked after context help is selected.
   */
  public static void register(JComponent component, String key) {
    help.register(component, key);
  }
  
  /**
   * Register a Swing component with a help page. This is when a widget is
   * clicked after context help is selected.
   */
  public static void register(JComponent component, Class<?> keyClass) {
    help.register(component, getKey(keyClass));
  }

  public static JButton createHelpButton(Class<?> c) {
    return createHelpButton(getKey(c));
  }

  public static JButton createHelpButton(String helpKey) {
    JButton helpButton = new JButton("Help");
    Helper.register(helpButton, helpKey);
    helpButton.setIcon(Helper.getIcon());
    return helpButton;
  }

  public static Action createHelpAction(Class<?> target) {
    return createHelpAction(getKey(target));
  }
  
  public static Action createHelpAction(String id) {
    return new HelpAction(id);
  }

  private static class HelpAction extends AbstractAction {

    private final String key;
    HelpAction(String key) {
      super("Help");
      putValue(Action.SMALL_ICON, getIcon());
      this.key = key;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
      help.showHelp(key);
    }
  }
  
  /* Get a key based on the class */
  public static String getKey(Class<?> clazz) {
    if (clazz == null)
      return Help.ROOT_ID;
    
    return clazz.getSimpleName();
  }

}
