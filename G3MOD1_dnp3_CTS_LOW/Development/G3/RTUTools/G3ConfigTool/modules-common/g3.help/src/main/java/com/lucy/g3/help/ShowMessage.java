/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.help;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/******************************************************************************
 * Quick warning/confirm convenience dialogs
 *****************************************************************************/
class ShowMessage  {

  /* Show a warning message (with arguments) */
  public static void warning(String msg, Object... args) {

    msg = String.format(msg, args);

    JOptionPane pane = createPane();
    pane.setMessage(msg);
    pane.setMessageType(JOptionPane.WARNING_MESSAGE);

    JDialog dialog = pane.createDialog(null, "Warning");

    // Static.setCentered(dialog);
    dialog.setVisible(true);

  }

  /* Show a warning message if arg is true */
  public static void warning(boolean warn, JComponent parent, String msg, Object... args) {
    if (warn) {
      warning(parent, msg, args);
    }
  }

  /* Show a warning message */
  public static void warning(JComponent parent, String msg, Object... args) {

    msg = String.format(msg, args);

    JOptionPane pane = createPane();
    pane.setMessage(msg);
    pane.setMessageType(JOptionPane.WARNING_MESSAGE);

    JDialog dialog = pane.createDialog(parent, "Warning");
    dialog.setVisible(true);

  }

  /* Confirm dialog */
  public static boolean confirm(JComponent parent, String title, String msg, Object... args) {
    boolean result = true;

    msg = String.format(msg, args);

    JOptionPane pane = createPane();

    pane.setMessage(msg);
    pane.setMessageType(JOptionPane.QUESTION_MESSAGE);
    pane.setOptionType(JOptionPane.YES_NO_OPTION);

    JDialog dialog = pane.createDialog(parent, title);
    dialog.setVisible(true);

    int i = (int) pane.getValue();
    if (i == 1) {
      result = false;
    }

    return result;
  }

  static private JOptionPane createPane(){
    return new JOptionPane() {

      @Override
      public int getMaxCharactersPerLineCount() {
        return 50;
      }
    };
  }

}
