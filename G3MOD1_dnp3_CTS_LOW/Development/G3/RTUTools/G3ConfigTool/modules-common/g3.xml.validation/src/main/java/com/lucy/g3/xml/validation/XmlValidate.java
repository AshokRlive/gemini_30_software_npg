/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.validation;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.InputSource;


/**
 * The Class is for created for validating a XML against its schema.
 */
public class XmlValidate {
  private Logger log = Logger.getLogger(XmlValidate.class);

  private final String schemaFilePath;
  private final String schemaDir;
  private final boolean schemaInJar;


  /**
   * Constructs a XML validator.
   * @param schemaFilePath the path of schema file. 
   */
  public XmlValidate(String schemaFilePath) {
    this(schemaFilePath, true);
  }
  
  /**
   * Constructs a XML validator.
   * @param schemaFilePath the path of schema file.
   * @param schemaInJar indicates if the schema is from Jar. 
   */
  public XmlValidate(String schemaFilePath, boolean schemaInJar) {
    if (schemaFilePath == null)
      throw new NullPointerException("schemaFilePath must not be null");
    
    this.schemaDir = getRoot(schemaFilePath);
    this.schemaFilePath = schemaFilePath;
    this.schemaInJar = schemaInJar;
  }

  static String getRoot(String path) {
    int index = path.lastIndexOf('/');
    String root;
    
    if (index >= 0) {
      root = path.substring(0, index);
    } else {
      root = "";
    }
    
    root += "/";
    return root;
  }

  /**
   * Validate a target XML file.
   *
   * @param xmlFilePath
   *          The XML file to be validated.
   * @throws Exception
   *           an exception will be thrown if the validation fails.
   */
  public void validate(String xmlFilePath) throws Exception {
    InputStream xmlFileStream;

    URL xmlFileURL = new File(xmlFilePath).toURI().toURL();
    xmlFileStream = xmlFileURL.openStream();
    
    try {
      validate(xmlFileStream);
    } catch (Throwable e) {
      xmlFileStream.close();
      throw e;
    }
  }

  public void validate(InputStream xmlStream) throws Exception {
    DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
    builderFactory.setNamespaceAware(true);

    // parse the XML into a document object
    InputSource xmlInput = new InputSource(xmlStream);

    // convert to source which can be validated
    DocumentBuilder parser = builderFactory.newDocumentBuilder();
    Document document = parser.parse(xmlInput);
    Source source = new DOMSource(document);

    SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

    /*
     * Associate the schema factory with the resource resolver, which is
     * responsible for resolving the imported XSD's
     */
    factory.setResourceResolver(new ResourceResolver());

    /*
     * Note that if your XML already declares the XSD to which it has to
     * conform, then there's no need to create a validator from a Schema object
     */
    Schema schema = null;

    try {
      schema = factory.newSchema(new StreamSource(getSchemaInputStream(schemaFilePath)));
    } catch (Exception e) {
      log.error("Failed to load schema:" + schemaFilePath, e);
    }

    if (schema != null) {
      Validator validator = schema.newValidator();
      validator.validate(source);
    } else {
      throw new RuntimeException("Schema not found:"+schemaFilePath);
    }
  }

  private InputStream getSchemaInputStream(String path) {
    if (schemaInJar)
      return XmlValidate.class.getClassLoader().getResourceAsStream(path);
    else {
      try {
        return new File(path).toURI().toURL().openStream();
      } catch (IOException e) {
        log.error("Failed to open input stream:" + path);
        return null;
      }
    }
  }

  /**
   * Gets the name of folders that contain schema files. 
   */
  protected String[] getSchemaFolder() {
    return new String[]{"protocolstack/", "generated/"};
  }
  
  private class ResourceResolver implements LSResourceResolver {

    /* Find XSD in resources/schema folder */
    @Override
    public LSInput resolveResource(String type, String namespaceURI,
        String publicId, String systemId, String baseURI) {
      InputStream stream = null;

      final String xsdName = parseSystemId(systemId);
      String xsdPath = null;
      if (xsdName != null) {
        stream = getSchemaInputStream(xsdPath = schemaDir + xsdName);

        
        /* Try to find resource in module folder. */
        if (stream == null) {
          String[] moduleFolder = getSchemaFolder();
          if (moduleFolder != null) {
            for (int i = 0; i < moduleFolder.length; i++) {
              if (moduleFolder[i] != null) {
                stream = getSchemaInputStream(xsdPath = schemaDir + moduleFolder[i] + xsdName);
                if (stream != null)
                  break;
              }
            }
          }
        }
      }

      if (stream == null) {
        log.error("Fail to load resource at path:" + xsdPath + " systemId:" + systemId);
      } else {
        log.debug("Resolved xsd:" + xsdPath + ". systemId:" + systemId);
      }
      return new Input(publicId, systemId, stream);
    }

    /**
     * Parse baseURI and return the parent folder name of the resource.
     */
    private String parseSystemId(String systemId) {
      if (systemId == null) {
        return null;
      }

      return systemId.replaceAll(".*\\/", "");
    }

  }

  private class Input implements LSInput {

    private String publicId;

    private String systemId;


    @Override
    public String getPublicId() {
      return publicId;
    }

    @Override
    public void setPublicId(String publicId) {
      this.publicId = publicId;
    }

    @Override
    public String getBaseURI() {
      return null;
    }

    @Override
    public InputStream getByteStream() {
      return null;
    }

    @Override
    public boolean getCertifiedText() {
      return false;
    }

    @Override
    public Reader getCharacterStream() {
      return null;
    }

    @Override
    public String getEncoding() {
      return null;
    }

    @Override
    public String getStringData() {
      if (inputStream == null) {
        log.error("stream is null");
        return null;
      }

      synchronized (inputStream) {
        try {
          byte[] input = new byte[inputStream.available()];
          inputStream.read(input);
          String contents = new String(input);
          return contents;
        } catch (IOException e) {
          log.error("Fail to read inputstream: " + e.getMessage());
          return null;
        }
      }
    }

    @Override
    public void setBaseURI(String baseURI) {
    }

    @Override
    public void setByteStream(InputStream byteStream) {
    }

    @Override
    public void setCertifiedText(boolean certifiedText) {
    }

    @Override
    public void setCharacterStream(Reader characterStream) {
    }

    @Override
    public void setEncoding(String encoding) {
    }

    @Override
    public void setStringData(String stringData) {
    }

    @Override
    public String getSystemId() {
      return systemId;
    }

    @Override
    public void setSystemId(String systemId) {
      this.systemId = systemId;
    }


    private BufferedInputStream inputStream;


    public Input(String publicId, String sysId, InputStream input) {
      this.publicId = publicId;
      this.systemId = sysId;
      this.inputStream = new BufferedInputStream(input);
    }
  }

}

