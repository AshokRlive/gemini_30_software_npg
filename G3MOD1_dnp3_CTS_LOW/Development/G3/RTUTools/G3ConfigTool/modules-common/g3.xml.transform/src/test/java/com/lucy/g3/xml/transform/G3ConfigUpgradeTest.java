/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.transform;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


/**
 * The Class G3ConfigUpgradeTest.
 */
public class G3ConfigUpgradeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Ignore
  @Test
  public void testUpgradeG3Config() throws Exception {
    InputStream xslsheet = XsltProcessor.class.getClassLoader().getResourceAsStream("xslt/G3Config_upgrade_01.xsl");
    FileInputStream input = new FileInputStream("G3Config.xml");
    FileOutputStream output = new FileOutputStream("G3Config_out.xml");
    XsltProcessor.transform(xslsheet, input, output);
  }
}
