/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter;

import com.jgoodies.binding.value.BindingConverter;

/**
 * BindingConverter to convert object to string.
 */
public class ObjectToStringConverter implements
    BindingConverter<Object, String> {

  private final String textNull;


  public ObjectToStringConverter() {
    this("n/a");
  }

  public ObjectToStringConverter(String textNull) {
    super();
    this.textNull = textNull;
  }

  @Override
  public Object sourceValue(String arg0) {
    return null;
  }

  @Override
  public String targetValue(Object subjectValue) {
    if (subjectValue == null) {
      return textNull;
    }

    return String.valueOf(subjectValue);
  }
}
