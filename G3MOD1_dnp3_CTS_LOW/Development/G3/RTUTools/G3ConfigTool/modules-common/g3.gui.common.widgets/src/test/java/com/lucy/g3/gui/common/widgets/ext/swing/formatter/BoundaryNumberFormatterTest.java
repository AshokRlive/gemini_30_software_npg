/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.formatter;

import java.text.ParseException;

import javax.swing.text.NumberFormatter;

import org.junit.Assert;

import org.junit.Test;

import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;


public class BoundaryNumberFormatterTest {

  @Test
  public void testUnsignedLong() throws ParseException {
    NumberFormatter format = BoundaryNumberFormatter.createUnsginedLong();
    Long value = (Long)format.stringToValue("1");
    Assert.assertTrue(value == 1);
  }
  
  @Test
  public void testUnsignedInt() throws ParseException {
    NumberFormatter format = BoundaryNumberFormatter.createUnsginedInt();
    Integer value = (Integer)format.stringToValue("1");
    Assert.assertTrue(value == 1);
  }

}

