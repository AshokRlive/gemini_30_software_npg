/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;

/**
 * The customised table that auto fill columns and rows in ViewPort.
 * <p>
 * The table register to keyStroke "enter" to "Edit" action in ActionMap, and
 * keyStroke "Delete" to "Delete" action in ActionMap.
 * </p>
 */
public class AutoFillJTable extends JXTable {

  private static final CellRendererPane CELL_RENDER_PANE = new CellRendererPane();


  public AutoFillJTable(TableModel dm) {
    super(dm);
    init();
  }

  private void init() {
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    setTableHeader(createTableHeader());
    getTableHeader().setReorderingAllowed(false);
    setIntercellSpacing(new Dimension(0, 0));
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Edit");
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
    setShowGrid(true);
    setAutoCreateRowSorter(false);
  }

  /**
   * Creates a JTableHeader that paints the table header background to the right
   * of the right-most column if necessary.
   */
  private JTableHeader createTableHeader() {
    return new JTableHeader(getColumnModel()) {

      @Override
      protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // if this JTableHEader is parented in a JViewport, then paint the
        // table header background to the right of the last column if
        // neccessary.
        JViewport viewport = (JViewport) table.getParent();
        if (viewport != null && table.getWidth() < viewport.getWidth()) {
          int tableWidth = table.getWidth();
          int width = viewport.getWidth() - table.getWidth();
          paintHeader(g, getTable(), tableWidth, width);
        }
      }
    };
  }

  /**
   * Paints the given JTable's table default header background at given x for
   * the given width.
   */
  private static void paintHeader(Graphics g, JTable table, int x, int width) {
    TableCellRenderer renderer = table.getTableHeader().getDefaultRenderer();
    Component component = renderer.getTableCellRendererComponent(
        table, "", false, false, -1, 2);

    component.setBounds(0, 0, width, table.getTableHeader().getHeight());

    ((JComponent) component).setOpaque(false);
    CELL_RENDER_PANE.paintComponent(g, component, null, x, 0,
        width, table.getTableHeader().getHeight(), true);
  }

  /**
   * Creates a component that paints the header background for use in a
   * JScrollPane corner.
   */
  private static JComponent createCornerComponent(final JTable table) {
    return new JComponent() {

      @Override
      protected void paintComponent(Graphics g) {
        paintHeader(g, table, 0, getWidth());
      }
    };
  }

  public static JScrollPane createNoBorderScrollPane(JTable table) {
    table.setFillsViewportHeight(true);
    JScrollPane scrollPane = new JScrollPane(table);
    scrollPane.getViewport().setView(table);
    scrollPane.getViewport().setOpaque(true);
    scrollPane.getViewport().setBackground(Color.white);
    scrollPane.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.lightGray));
    scrollPane.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER,
        createCornerComponent(table));
    return scrollPane;
  }

}
