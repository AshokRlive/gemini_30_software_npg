/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.formatter;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

/**
 * Extended NumberFormatter which check string value convert strictly by
 * comparing the result of {@code stringToValue} and {@code valueToString}. If
 * the accuracy changes in conversion, parse exception will be thrown.
 *
 * @see {@linkplain BoundaryNumberFormatter}
 */
public class StrictNumberFormatter extends javax.swing.text.NumberFormatter {

  @Override
  public Object stringToValue(String text) throws ParseException {
    Object parsedValue = super.stringToValue(text);
    String expectedText = super.valueToString(parsedValue);
    if (!super.stringToValue(expectedText).equals(parsedValue)) {
      throw new ParseException("Rounding occurred", 0);
    }

    return parsedValue;
  }

  public StrictNumberFormatter(NumberFormat nf) {
    super(nf);
  }

  public static NumberFormatter createDefault() {
    NumberFormat format = NumberFormat.getNumberInstance();
    format.setMaximumFractionDigits(10);
    format.setMinimumFractionDigits(0);
    return new StrictNumberFormatter(format);
  }
}
