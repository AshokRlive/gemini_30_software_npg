/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.button;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

/**
 * This class is for adding an hover effect to a button.
 * <p>
 * When mouse moves to the inside of a button, it enables contentFilled state,
 * otherwise disables contentFilled state.
 * </p>
 */
public class ButtonHoverEffect extends MouseAdapter {

  public static void install(JButton button) {
    if (button != null) {
      button.setContentAreaFilled(false);
      button.addMouseListener(newInstance());
    }
  }

  static MouseAdapter newInstance() {
    return new ButtonHoverEffect();
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    Object source = e.getSource();
    if (source instanceof JButton) {
      ((JButton) source).setContentAreaFilled(((JButton) source).isEnabled());
    }
  }

  @Override
  public void mouseExited(MouseEvent e) {
    Object source = e.getSource();
    if (source instanceof JButton) {
      ((JButton) source).setContentAreaFilled(false);
    }
  }
}
