/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.textfield;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * A class for loading constraints values from property file and providing API
 * for checking value against the constraints.
 */
public class Constraints {

  private final Properties prop = new Properties();
  private final String constraintFileName;

  private static Logger log = Logger.getLogger(Constraints.class);


  public Constraints(String constraintFileName) {
    this.constraintFileName = constraintFileName;
    String path = "resources/" + constraintFileName;
    InputStream in = getClass().getResourceAsStream(path);
    try {
      prop.load(in);
      in.close();
    } catch (Exception e) {
      throw new RuntimeException("Constraints file not found:" + path, e);
    }
  }

  public final void checkPropertyBoundary(Object source, long oldValue,
      long newValue, String prefix, String property)
      throws PropertyVetoException {
    long min = getMin(prefix, property);
    long max = getMax(prefix, property);
    if (newValue < min || newValue > max) {
      log.warn(String.format("The new value:%d of \"%s\" is out of boundary [%d,%d]",
          newValue, property, min, max));

      throw new PropertyVetoException(
          String.format(
              "Invalid \"%s\" configuration. %s.%s value:%d is out of boundary [%d,%d]",
              source, prefix, property, newValue, min, max),
          new PropertyChangeEvent(source, property, oldValue,
              newValue));
    }
  }

  /**
   * Gets the maximum value of a number property.
   *
   * @param prefix
   *          one of <code>PREFIX_TCPIP</code>,<code>PREFIX_SESSION</code>,
   *          <code>PREFIX_LINK</code>
   * @param property
   *          the name of a property.
   */
  public final long getMax(String prefix, String property) {
    try {
      return getPropertyValue(prefix, property, "max");
    } catch (Exception e) {
      return Long.MAX_VALUE;
    }
  }

  /**
   * Gets the minimum value of a number property.
   *
   * @param prefix
   *          one of <code>PREFIX_TCPIP</code>,<code>PREFIX_SESSION</code>,
   *          <code>PREFIX_LINK</code>
   * @param property
   *          the name of a property.
   */
  public final long getMin(String prefix, String property) {
    try {
      return getPropertyValue(prefix, property, "min");
    } catch (Exception e) {
      return Long.MIN_VALUE;
    }
  }

  /**
   * Gets the default value of a number property.
   *
   * @param prefix
   *          one of <code>PREFIX_CHANNEL</code>,<code>PREFIX_SESSION</code>,
   *          <code>PREFIX_CHANNEL_IO</code>
   * @param property
   *          the name of a property.
   */
  public final long getDefault(String prefix, String property) {
    try {
      return getPropertyValue(prefix, property, "default");
    } catch (Exception e) {
      return 0;
    }
  }

  /**
   * Gets the description of a property.
   *
   * @param prefix
   *          one of <code>PREFIX_TCPIP</code>,<code>PREFIX_SESSION</code>,
   *          <code>PREFIX_LINK</code>
   * @param property
   *          the name of a property.
   */
  public final String getDescript(String prefix, String property) {
    String key = getKey(prefix, property, "descript");
    return prop.getProperty(key);
  }

  private String getKey(String prefix, String property, String suffix) {
    return String.format("%s.%s.%s", prefix, property, suffix);
  }

  private long getPropertyValue(String prefix, String property, String suffix) throws Exception {
    long value;
    String key = null;
    String valueStr = null;
    try {
      key = getKey(prefix, property, suffix);
      valueStr = prop.getProperty(key);
      value = Long.valueOf(String.valueOf(valueStr));
      return value;
    } catch (Exception e) {
      String msg = String.format("Property value not found in file:\"%s\" for key:%s, result valueStr:%s",
          constraintFileName, key, valueStr);
      log.warn(msg);
      throw e;
    }
  }

}
