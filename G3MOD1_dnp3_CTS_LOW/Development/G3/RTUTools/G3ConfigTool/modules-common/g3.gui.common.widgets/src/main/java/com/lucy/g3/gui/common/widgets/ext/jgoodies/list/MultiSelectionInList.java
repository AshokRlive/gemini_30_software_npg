/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.list;

import java.util.Arrays;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.common.collect.ObservableList;

/**
 * This class represents a multi-selection in has a ListModel representing the
 * set of possible and an ObservableList representing the current selection.
 * <strong>Note:</strong> Unlike SingleSelectionInList this class does not
 * enforce constraints on the selection. That means you can add items to the
 * selection that are not in the list. But we should definitely think about it.
 * Firstly we could add a listener to the selection which removes invalid
 * elements that are added. The disadvantage of this solution lies in the fact
 * that there is phase in which we are invalid. Secondly we might consider
 * wrapping the ObservableList to prevent illegal selections. So only while
 * reducing the possible we might run into problems. (What circumstances?)
 *
 * @param <T>
 *          the item type.
 */
public class MultiSelectionInList<T> extends Model {

  /**
   * The name of the bound read-write <em>selection</em> property.
   */
  public static final String PROPERTY_SELECTION = "selection";

  private ObservableList<T> selection;
  private ListModel<T> list;


  @SuppressWarnings("unchecked")
  public MultiSelectionInList(ListModel<T> list) {
    this.list = list == null ? new ArrayListModel<T>() : list;
    selection = new ArrayListModel<T>();
    selection.addListDataListener(new SelectionListener());
  }

  @SuppressWarnings("unchecked")
  public MultiSelectionInList(T[] list) {
    this(new ArrayListModel<T>(Arrays.asList(list)));
  }

  public ObservableList<T> getSelection() {
    return selection;
  }

  public ListModel<T> getList() {
    return list;
  }

  /**
   * Checks and answers if an element is selected.
   *
   * @return true if an element is selected, false otherwise
   */
  public boolean hasSelection() {
    return selection.isEmpty() == false;
  }
  
  /**
   * Property getter for: {@link #PROPERTY_SELECTION}.
   * @return
   */
  public boolean isSelection() {
    return selection.isEmpty() == false;
  }
  

  private void fireSelectionChangeEvent() {
    firePropertyChange(PROPERTY_SELECTION, null, hasSelection());
  }


  private class SelectionListener implements ListDataListener {

    @Override
    public void intervalAdded(ListDataEvent e) {
      fireSelectionChangeEvent();
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      fireSelectionChangeEvent();
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
    }
  }

}
