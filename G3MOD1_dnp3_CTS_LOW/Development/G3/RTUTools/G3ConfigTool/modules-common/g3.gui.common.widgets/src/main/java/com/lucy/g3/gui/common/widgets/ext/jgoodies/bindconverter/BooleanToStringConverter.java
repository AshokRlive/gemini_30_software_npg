/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter;

import com.jgoodies.binding.value.BindingConverter;

/**
 * BindingCoverter to convert boolean to string.
 */
public class BooleanToStringConverter implements
    BindingConverter<Boolean, String> {

  private final String textNull;
  private final String textTrue;
  private final String textFalse;


  public BooleanToStringConverter() {
    this("", "True", "False");
  }

  public BooleanToStringConverter(String textNull, String textTrue,
      String textFalse) {
    super();
    this.textNull = textNull;
    this.textTrue = textTrue;
    this.textFalse = textFalse;
  }

  @Override
  public Boolean sourceValue(String arg0) {
    throw new UnsupportedOperationException("Not allow to convert string to boolean");
  }

  @Override
  public String targetValue(Boolean subjectValue) {
    if (subjectValue == null) {
      return textNull;
    }

    return subjectValue == true ? textTrue : textFalse;
  }
}
