/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter;

import javax.swing.ImageIcon;

import com.jgoodies.binding.value.BindingConverter;

/**
 * BindingConverter to convert boolean to icon.
 */
public class BooleanToIconConverter implements
    BindingConverter<Boolean, ImageIcon> {

  private final ImageIcon iconOk;
  private final ImageIcon iconError;


  public BooleanToIconConverter(ImageIcon iconOk, ImageIcon iconError) {
    this.iconError = iconError;
    this.iconOk = iconOk;
  }

  @Override
  public Boolean sourceValue(ImageIcon arg0) {
    return null;
  }

  @Override
  public ImageIcon targetValue(Boolean subjectValue) {
    if (subjectValue == null) {
      return null;
    }

    return subjectValue == true ? iconOk : iconError;
  }
}
