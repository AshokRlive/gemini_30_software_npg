/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter;

import java.util.concurrent.TimeUnit;

import com.jgoodies.binding.value.BindingConverter;

/**
 * BindingConverter to convert seconds to formatted time string.
 */
public class SecondsToStrConverter implements BindingConverter<Long, String> {

  private final String nullText;


  public SecondsToStrConverter(String nullText) {
    this.nullText = nullText;
  }

  public SecondsToStrConverter() {
    this("");
  }

  @Override
  public Long sourceValue(String dateString) {
    throw new UnsupportedOperationException("Not supported to convert dateString to seconds");
  }

  @Override
  public String targetValue(Long subjectValue) {
    if (subjectValue == null) {
      return nullText;
    } else {
      return convertDurationFromSecsToDays(subjectValue);
    }
  }
  
  private static String convertDurationFromSecsToDays(Long seconds) {
    if (seconds == null) {
      return "";
    }

    final long days = TimeUnit.SECONDS.toDays(seconds);
    seconds = seconds - TimeUnit.DAYS.toSeconds(days);

    final long hours = TimeUnit.SECONDS.toHours(seconds);
    seconds = seconds - TimeUnit.HOURS.toSeconds(hours);

    final long minutes = TimeUnit.SECONDS.toMinutes(seconds);
    seconds = seconds - TimeUnit.MINUTES.toSeconds(minutes);

    return String.format("%d days %d hours %d minutes %d seconds", days, hours, minutes, seconds);
  }
}
