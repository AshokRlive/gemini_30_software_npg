/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.formatter;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

/**
 * This number formatter only allows Integer value within a range of min and
 * max. By default, commitOnValidEdit property of this formatter is true.
 *
 * @see {@linkplain StrictNumberFormatter}
 */
public class BoundaryNumberFormatter extends NumberFormatter {

  private BoundaryNumberFormatter(NumberFormat format) {
    super(format);

    setCommitsOnValidEdit(true);
    format.setGroupingUsed(false);
  }

  public BoundaryNumberFormatter(Comparable<?> min, Comparable<?> max) {
    this(NumberFormat.getIntegerInstance());
    setMinimum(min);
    setMaximum(max);
  }

  @Override
  public Object stringToValue(String text) throws ParseException {
    try {
      Object parseValue = super.stringToValue(text);
      /*
       * Parse text with primitive type class for more strictly parsing.
       */
      if (parseValue instanceof Long) {
        Long.parseLong(text);
      } else if (parseValue instanceof Integer) {
        Integer.parseInt(text);
      } else if (parseValue instanceof Byte) {
        Byte.parseByte(text);
      } else if (parseValue instanceof Float) {
        Float.parseFloat(text);
      }

      setEditValid(true);
      return parseValue;
    } catch (Exception e) {
      setEditValid(false);
      throw new ParseException("Cannot parse Long " + text, 0);
    }
  }
  
  public static BoundaryNumberFormatter createUnsginedInt() {
    return new BoundaryNumberFormatter(Integer.valueOf(0), Integer.MAX_VALUE);
  }
  
  public static BoundaryNumberFormatter createUnsginedLong() {
    return new BoundaryNumberFormatter(Long.valueOf(0), Long.MAX_VALUE);
  }
}
