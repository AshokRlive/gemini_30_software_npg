/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.utils;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;

import com.jgoodies.common.base.Preconditions;


/**
 * Utility for key binding.
 */
public final class KeyBindingUtil {
  private KeyBindingUtil() {}
  
  private static final KeyStroke STROKE_ESC = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
  private static final KeyStroke STROKE_ENTER = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);


  public static void registerEscapeKey(JComponent comp, ActionListener al) {
    comp.registerKeyboardAction(al, STROKE_ESC,
        JComponent.WHEN_IN_FOCUSED_WINDOW);
  }

  public static void registerEscapeKey(JDialog dialog, ActionListener al) {
    dialog.getRootPane().registerKeyboardAction(al, STROKE_ESC,
        JComponent.WHEN_IN_FOCUSED_WINDOW);
  }

  public static void registerEnterKey(JDialog dialog, ActionListener al) {
    dialog.getRootPane().registerKeyboardAction(al, STROKE_ENTER,
        JComponent.WHEN_IN_FOCUSED_WINDOW);
  }

  public static void registerEscapeEnterKey(JRootPane root,
      ActionListener ok, ActionListener cancel) {
    Preconditions.checkNotNull(root, "root must not be null");

    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    root.registerKeyboardAction(cancel, stroke,
        JComponent.WHEN_IN_FOCUSED_WINDOW);

    stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
    root.registerKeyboardAction(ok, stroke,
        JComponent.WHEN_IN_FOCUSED_WINDOW);
  }

}

