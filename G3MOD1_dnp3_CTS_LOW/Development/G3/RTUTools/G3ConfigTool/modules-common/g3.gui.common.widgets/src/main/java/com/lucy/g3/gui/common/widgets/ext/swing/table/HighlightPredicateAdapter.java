/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.table;

import java.awt.Component;

import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.jgoodies.common.base.Preconditions;

/**
 * Adapts {@link ISupportHighlight} to {@linkplain HighlightPredicate}.  
 */
public class HighlightPredicateAdapter implements HighlightPredicate {

  private final ISupportHighlight tm;


  public HighlightPredicateAdapter(ISupportHighlight tm) {
    this.tm = Preconditions.checkNotNull(tm, "table model must not be null");
  }

  @Override
  public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
    int rowIndex = adapter.convertRowIndexToModel(adapter.row);
    int colIndex = adapter.convertColumnIndexToModel(adapter.column);
    return tm.shouldHightlight(rowIndex, colIndex);
  }
}
