/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.textfield;

import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ComponentValueModel;
import com.jgoodies.binding.value.ConverterFactory.BooleanNegator;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.RegexFormatter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;

/**
 * The formatted text field for editing IP address.
 */
public class IPAddressField extends JFormattedTextField {

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_ALLOWANYIP = "allowAnyIp";

  private Logger log = Logger.getLogger(IPAddressField.class);

  private boolean allowAnyIp;
  private boolean allowBlank;

  private RegexFormatter formatter;
  
  public IPAddressField() {
    this(false);
  }

  public IPAddressField(boolean allowAnyIp) {
    this(allowAnyIp, false);
  }

  public IPAddressField(boolean allowAnyIp, boolean allowBlank) {
    super();
    setColumns(15);
    this.allowAnyIp = allowAnyIp;
    this.allowBlank = allowBlank;
    setFormatter();
    FormattedTextFieldSupport.installCommitOnType(this);
    setValue("");
  }

  private void setFormatter() {
    String pattern = IPAddress.getPattern(allowBlank, allowAnyIp);
    formatter = new RegexFormatter(pattern);
    formatter.setCommitsOnValidEdit(true);
    formatter.setAllowsInvalid(true);
    formatter.setOverwriteMode(false);
    super.setFormatterFactory(new DefaultFormatterFactory(formatter));
  }

  public void setCommitsOnValidEdit(boolean commitOnValidEdit){
    formatter.setCommitsOnValidEdit(commitOnValidEdit);
  }
  
  @Override
  public void setFormatterFactory(AbstractFormatterFactory tf) {
    log.warn("Not allowed to change formatter factory");
  }

  public boolean isAllowAnyIp() {
    return allowAnyIp;
  }

  public void setAllowAnyIp(boolean allowAnyIp) {
    if (this.allowAnyIp == allowAnyIp) {
      return;
    }

    this.allowAnyIp = allowAnyIp;
    setFormatter();
  }

  public boolean isAllowBlank() {
    return allowBlank;
  }

  public void setAllowBlank(boolean allowBlank) {
    if (this.allowBlank == allowBlank) {
      return;
    }

    this.allowBlank = allowBlank;
    setFormatter();
  }

  public void bindTo(ValueModel vm) {
    Bindings.bind(this, vm);
  }

  /**
   * Creates a IP field that binds to a value mode.
   *
   * @param ipAddress
   *          the IP address value model.
   * @param allowBlank
   *          allow blank IP address
   */
  public static IPAddressField create(ValueModel ipAddress, boolean allowBlank) {
    IPAddressField field = new IPAddressField(false, allowBlank);
    field.bindTo(ipAddress);
    return field;
  }

  /**
   * Creates a specific IP field which is binding to two value model: value
   * model and state model.
   *
   * @param ipAddress
   *          value model that store the IP address value of the field
   * @param anyIp
   *          state model that control the editable state of IP field
   * @return IPAddressField object
   */
  public static IPAddressField create(ComponentValueModel ipAddress, ValueModel anyIp, boolean allowBlank) {
    IPAddressField field = new IPAddressField(true, allowBlank);
    bind(field, ipAddress, anyIp);
    return field;
  }

  /**
   * Binds IP field to two value model: value model and state model.
   *
   * @param ipAddress
   *          value model that store the IP address value of the field
   * @param anyIp
   *          state model that control the editable state of IP field
   * @return IPAddressField object
   */
  public static void bind(IPAddressField component, ComponentValueModel ipAddress, ValueModel anyIp) {
    // Bind IP address value
    Bindings.bind(component, ipAddress);

    // Enable IP address field when "AnyIp" is false, disable it otherwise.
    PropertyConnector.connectAndUpdate(new ConverterValueModel(anyIp, new BooleanNegator()),
        ipAddress, ComponentValueModel.PROPERTY_ENABLED);

    // Update IP address field when "AnyIp" changes
    PropertyConnector.connectAndUpdate(anyIp, component, IPAddressField.PROPERTY_ALLOWANYIP);
  }

}
