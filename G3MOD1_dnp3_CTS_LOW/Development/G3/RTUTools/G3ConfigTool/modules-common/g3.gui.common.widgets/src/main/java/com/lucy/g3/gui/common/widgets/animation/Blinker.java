/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.animation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.Timer;

import com.jgoodies.common.base.Preconditions;

/**
 * A blinker a utility component which helps us set a Swing component blinking
 * by changing its background periodically.
 */
public class Blinker implements ActionListener {

  private final Timer timer = new Timer(1000, this);

  private final JComponent target;

  private final boolean orgOpaque; // Original opaque state

  private final Color orgBg; // Original background colour
  private final Color orgFg; // Original foreground colour
  private final Font orgFont; // Original font

  private Color blinkBg;
  private Color blinkFg;
  private Font blinkFont;

  private boolean isColorChanged;


  public Blinker(JComponent target) {
    this(target, Color.WHITE, Color.black);
  }

  public Blinker(JComponent target, Font font) {
    this(target, font, null, null);
  }

  public Blinker(JComponent target, Color foreground, Color background) {
    this(target, null, foreground, background);
  }

  public Blinker(JComponent target, Font font, Color foreground, Color background) {
    this.target = Preconditions.checkNotNull(target, "target is null");
    this.orgBg = target.getBackground();
    this.orgFg = target.getForeground();
    this.orgOpaque = target.isOpaque();
    this.orgFont = target.getFont();

    this.blinkBg = background;
    this.blinkFg = foreground;
    this.blinkFont = font;
  }

  public void start() {
    timer.start();
  }

  public void stop() {
    timer.stop();
    reset();
  }

  /**
   * Reset the target to original status.
   */
  private void reset() {
    target.setBackground(orgBg);
    target.setForeground(orgFg);
    target.setOpaque(orgOpaque);
    target.setFont(orgFont);
  }

  /**
   * Perform blinking.
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    /* Swap foreground and background colour */
    if (isColorChanged) {
      reset();
    } else {
      // Change background
      if (blinkBg != null) {
        target.setOpaque(true);// Enable opaque so the blinking effect can be
        // seen.
        target.setBackground(blinkBg);
      }

      // Change foreground
      if (blinkFg != null) {
        target.setForeground(blinkFg);
      }

      // Change font
      if (blinkFont != null) {
        target.setFont(blinkFont);
      }
    }

    isColorChanged = !isColorChanged;
  }

  public void setBlinkBg(Color blinkBg) {
    this.blinkBg = blinkBg;
  }

  public void setBlinkFg(Color blinkFg) {
    this.blinkFg = blinkFg;
  }

  public void setBlinkFont(Font blinkFont) {
    this.blinkFont = blinkFont;
  }

  public void setBlinkingPeriod(int periodMs) {
    Preconditions.checkArgument(periodMs > 0, "periodMs must be > 0");
    timer.setDelay(periodMs);
  }

}
