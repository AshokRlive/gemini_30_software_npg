/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.table;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.AbstractAction;
import javax.swing.Action;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;

/**
 * <p>
 * A two column table model for selecting one or more items from the given item
 * collection.The first column is for toggling selection, the second is the item
 * to be selected.
 * </p>
 * <p>
 * It supports two selection mode: <code>SELECTION_MODE_SINGLE</code> and
 * <code>SELECTION_MODE_MULTI</code>. By default it is set to
 * SELECTION_MODE_MULTI.
 * </p>
 * <p>
 * This mode provides API to select/deselect all items
 * </p>
 *
 * @param <T>
 *          the type of items.
 */
public class SelectionTableModel<T> extends AbstractTableAdapter<T> {

  /**
   * The Enum SelectionMode.
   */
  public enum SelectionMode {
    SELECTION_MODE_MULTI,
    SELECTION_MODE_SINGLE
  }


  // Column names
  protected static final String COLUMN_TITLE_SEELCTED = "Select";
  protected static final String COLUMN_TITLE_ITEM = "Item";

  // Column indexes
  public static final int COLUMN_SELECTED = 0;
  public static final int COLUMN_ITEM = 1;

  protected final ArrayListModel<T> itemListModel = new ArrayListModel<T>();
  protected final ArrayList<T> selectedItems = new ArrayList<T>();

  private final ArrayList<T> allItems = new ArrayList<>();

  private SelectionMode selectionMode = SelectionMode.SELECTION_MODE_MULTI;
  private Action deselectAllAction;
  private Action selectAllAction;


  public SelectionTableModel(Collection<T> allItems, String itemColumnTitle) {
    super(COLUMN_TITLE_SEELCTED, itemColumnTitle == null ? COLUMN_TITLE_ITEM : itemColumnTitle);
    setListModel(itemListModel);
    setItems(allItems);
  }

  public SelectionTableModel(Collection<T> allItems) {
    this(allItems, null);
  }
  
  public SelectionTableModel() {
    this(null,null);
  }

  public void setItems(Collection<T> allItems) {
    if(allItems != null) {
      this.allItems.clear();
      this.allItems.addAll(allItems);
      
      itemListModel.clear();
      itemListModel.addAll(allItems);
  
      selectedItems.retainAll(allItems);
    }
  }
  
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    T item = getRow(rowIndex);

    switch (columnIndex) {
    case COLUMN_SELECTED:
      return selectedItems.contains(item);
    case COLUMN_ITEM:
      return item;
    default:
      return "";
    }
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return columnIndex == COLUMN_SELECTED;
  }

  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    if (columnIndex == COLUMN_SELECTED) {
      T item = getRow(rowIndex);
      setItemSelected(item, (Boolean) aValue);
    }
  }

  public void setItemSelected(T item, boolean select) {
    if (item != null) {
      if (select == true && !selectedItems.contains(item)) {
        // Other selections need to be cleared under single selection
        // mode.
        if (selectionMode == SelectionMode.SELECTION_MODE_SINGLE) {
          selectedItems.clear();
        }

        selectedItems.add(item);
      } else if (select == false) {
        selectedItems.remove(item);
      }

      if (getRowCount() > 0) {
        fireTableRowsUpdated(0, getRowCount() - 1);
      }
    }
  }

  public ArrayList<T> getSelections() {
    return new ArrayList<T>(selectedItems);
  }

  public int getSelectionsNum() {
    return selectedItems.size();
  }

  public Collection<T> getUnSelections() {
    ArrayList<T> unselection = new ArrayList<T>(allItems);
    unselection.removeAll(selectedItems);

    return unselection;
  }

  public T getSelection() {
    return selectedItems.size() > 0 ? selectedItems.get(0) : null;
  }
  
  public int getSelectionIndex() {
    Object selection = getSelection();
    if(selection != null)
      return itemListModel.indexOf(selection);
    else
      return -1;
  }

  public boolean hasSelection() {
    return selectedItems.size() > 0;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    if (columnIndex == COLUMN_SELECTED) {
      return Boolean.class;
    }

    return super.getColumnClass(columnIndex);
  }

  /**
   * Change the mode of selection between single and multiple .
   *
   * @param selectionMode
   *          <li>0: for single selection <li>1: for multiple selection
   */
  public void setSelectionMode(SelectionMode selectionMode) {
    // Invalid mode
    if (selectionMode == null) {
      return;
    }

    this.selectionMode = selectionMode;

    if (selectionMode == SelectionMode.SELECTION_MODE_SINGLE) {

      /* Clear selected entries. */
      if (selectedItems.size() > 1) {
        T item = selectedItems.get(0);
        selectedItems.clear();
        selectedItems.add(item);
      }
    }

    /* Set action enablement */
    if (selectAllAction != null) {
      selectAllAction.setEnabled(selectionMode == SelectionMode.SELECTION_MODE_MULTI);
    }
  }

  public SelectionMode getSelectionMode() {
    return selectionMode;
  }

  public void toggleSelectionAtIndex(int rowInModel) {
    if (rowInModel >= 0) {
      T ch = getRow(rowInModel);
      // Deselect the Object if it is already selected
      if (selectedItems.contains(ch)) {
        setItemSelected(ch, false);
      
      // Select the Object if it is not selected yet
      } else {
        setItemSelected(ch, true);
      }
    }
  }

  public void selectAll() {
    if (selectionMode != SelectionMode.SELECTION_MODE_SINGLE) {
      selectedItems.clear();
      selectedItems.addAll(itemListModel);
      if (getRowCount() > 0) {
        fireTableRowsUpdated(0, getRowCount() - 1);
      }
    } else {
      Logger.getLogger(getClass()).warn(
          "Cannot select all cause it is single selection mode");
    }
  }

  public void clearSelection() {
    selectedItems.clear();
    if (getRowCount() > 0) {
      fireTableRowsUpdated(0, getRowCount() - 1);
    }
  }

  public Action getActionDeselectAll() {
    if (deselectAllAction == null) {
      deselectAllAction = new DeselectAllAction();
    }
    return deselectAllAction;
  }

  public Action getActionSelectAll() {
    if (selectAllAction == null) {
      selectAllAction = new SelectAllAction();
      selectAllAction.setEnabled(selectionMode == SelectionMode.SELECTION_MODE_MULTI);
    }
    return selectAllAction;
  }


  private class DeselectAllAction extends AbstractAction {

    public DeselectAllAction() {
      super("Deselect All");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      clearSelection();
    }
  }

  private class SelectAllAction extends AbstractAction {

    public SelectAllAction() {
      super("Select All");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      selectAll();
    }
  }
}