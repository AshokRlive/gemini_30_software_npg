/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.textfield;

/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Strings;

/**
 * The Class LabelledFTFSupport.
 */
public final class LabelledFTFSupport {
  public static final String DEFAULT_LABEL_DISABLED = "Disabled";

  private LabelledFTFSupport() {
  }

  /**
   * Decorates a field to display a specific value with a label. 
   */
  public static void decorateWithLabel(JFormattedTextField field, Object value, String label) {
    HashMap<Object, String> labelMap = new HashMap<>();
    labelMap.put(value, label);
    decorateWithLabel(field, labelMap);
  }

  /**
   * Decorates a field to display a specific value with a label. 
   */
  public static void decorateWithLabel(JFormattedTextField field, Map<Object, String> labelMap) {
    AbstractFormatter formatter = field.getFormatter();
    if (formatter != null && formatter instanceof DefaultFormatter) {
      LabelNumberFormatter decoFormatter =
          new LabelNumberFormatter((DefaultFormatter) formatter, labelMap);
      field.setFormatterFactory(new DefaultFormatterFactory(decoFormatter));
    } else {
      Logger.getLogger(LabelledFTFSupport.class).error("Failed to apply decorator. DefaultFormatter not available!");
    }
  }


  /**
   * The Class LabelNumberFormatter.
   */
  static class LabelNumberFormatter extends DefaultFormatter{

    private final DefaultFormatter formatter;
    private final Map<Object, String> labelMap;
    private final NumberFormat numberFormat = NumberFormat.getInstance();

    public LabelNumberFormatter(DefaultFormatter formatter, Map<Object, String> labelMap) {
      this.formatter = formatter;
      this.labelMap = labelMap;
      setOverwriteMode(formatter.getOverwriteMode());
      setValueClass(formatter.getValueClass());
      setCommitsOnValidEdit(formatter.getCommitsOnValidEdit());
      setAllowsInvalid(formatter.getAllowsInvalid());
    }

    @Override
    public String valueToString(Object value) throws ParseException {
      if (labelMap.containsKey(value) && !Strings.isBlank(labelMap.get(value)))
        return String.format("%s (%s)", formatter.valueToString(value), labelMap.get(value));
      else
        return formatter.valueToString(value);
    }
    
    @Override
    public Object stringToValue(String text) throws ParseException {
      /* Parse number from the labelled string*/
      try {
        Number value = numberFormat.parse(text);
        text = value.toString();
      } catch (Exception e) {
        // Do nothing
      }
      
      return formatter.stringToValue(text);
    }
    
  }
}
