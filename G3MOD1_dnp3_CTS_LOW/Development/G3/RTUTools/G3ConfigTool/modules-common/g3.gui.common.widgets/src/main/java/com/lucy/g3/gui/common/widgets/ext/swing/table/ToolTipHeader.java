/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.table;

import java.awt.event.MouseEvent;

import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

/**
 * A table header that can be set tooltips easily.
 */
public class ToolTipHeader extends JTableHeader {

  private String[] toolTips;


  public ToolTipHeader(TableColumnModel model) {
    super(model);
  }

  @Override
  public String getToolTipText(MouseEvent e) {
    int col = columnAtPoint(e.getPoint());
    int modelCol = getTable().convertColumnIndexToModel(col);
    String retStr;
    try {
      retStr = toolTips[modelCol];
    } catch (NullPointerException ex) {
      retStr = "";
    } catch (ArrayIndexOutOfBoundsException ex) {
      retStr = "";
    }
    if (retStr.length() < 1) {
      retStr = super.getToolTipText(e);
    }
    return retStr;
  }

  public void setToolTipStrings(String[] toolTips) {
    this.toolTips = toolTips;
  }
}
