/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.splitbutton;

import java.awt.event.ActionEvent;

import javax.swing.Action;

/**
 * Adapts action to split button action.
 */
public class SplitButtonActionAdapter implements SplitButtonActionListener {

  private final Action action;


  public SplitButtonActionAdapter(Action action) {
    this.action = action;
  }

  @Override
  public void buttonClicked(ActionEvent e) {
    action.actionPerformed(e);
  }

  @Override
  public void splitButtonClicked(ActionEvent e) {
  }

}
