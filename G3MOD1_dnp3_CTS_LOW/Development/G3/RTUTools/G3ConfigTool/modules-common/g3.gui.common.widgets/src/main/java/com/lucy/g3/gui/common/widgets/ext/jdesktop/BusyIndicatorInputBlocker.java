/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jdesktop;

import java.awt.Component;
import java.awt.Cursor;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.RootPaneContainer;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;

import org.jdesktop.application.Task;
import org.jdesktop.application.Task.BlockingScope;
import org.jdesktop.application.Task.InputBlocker;

/**
 * Task blocker for blocking application with busy cursor while a task is
 * running.<br>
 */
public class BusyIndicatorInputBlocker extends InputBlocker {

  private static JComponent glassPane = new BusyGlassPane();


  /**
   * Simple blocker which merely shows wait cursor.
   *
   * @task the task which uses this blocker
   * @param target
   *          the target component which will be blocked. It must NOT be null.
   */
  public BusyIndicatorInputBlocker(final Task<?, ?> task, Component target) {
    super(task, BlockingScope.WINDOW, target);
  }

  private void showBusyGlassPane(boolean f) {
    RootPaneContainer rpc = null;
    Component root = (Component) getTarget();
    while (root != null) {
      if (root instanceof RootPaneContainer) {
        rpc = (RootPaneContainer) root;
        break;
      }
      root = root.getParent();
    }
    if (rpc != null) {
      if (f) {
        JMenuBar menuBar = rpc.getRootPane().getJMenuBar();
        if (menuBar != null) {
          menuBar.putClientProperty(this, menuBar.isEnabled());
          menuBar.setEnabled(false);
        }

        InputVerifier retainFocusWhileVisible = new InputVerifier() {

          @Override
          public boolean verify(JComponent c) {
            return !c.isVisible();
          }
        };
        glassPane.setInputVerifier(retainFocusWhileVisible);
        Component oldGlassPane = rpc.getGlassPane();
        rpc.getRootPane().putClientProperty(this, oldGlassPane);
        rpc.setGlassPane(glassPane);
        glassPane.setVisible(true);
        glassPane.requestFocusInWindow();
        glassPane.revalidate();
      
      } else {
        JMenuBar menuBar = rpc.getRootPane().getJMenuBar();
        if (menuBar != null) {
          boolean enabled = (Boolean) menuBar.getClientProperty(this);
          menuBar.putClientProperty(this, null);
          menuBar.setEnabled(enabled);
        }
        Component oldGlassPane = (Component) rpc.getRootPane().getClientProperty(this);
        rpc.getRootPane().putClientProperty(this, null);
        if (!oldGlassPane.isVisible()) {
          rpc.getGlassPane().setVisible(false);
        }
        rpc.setGlassPane(oldGlassPane); // sets oldGlassPane.visible
      }
    }
  }

  @Override
  protected void block() {
    showBusyGlassPane(true);
  }

  @Override
  protected void unblock() {
    showBusyGlassPane(false);
  }


  private static class BusyGlassPane extends JPanel {

    BusyGlassPane() {
      super(null, false);
      setVisible(false);
      setOpaque(false);
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      MouseInputListener blockMouseEvents = new MouseInputAdapter() {
      };
      addMouseMotionListener(blockMouseEvents);
      addMouseListener(blockMouseEvents);
    }
  }
}
