/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.formatter;

import java.text.DecimalFormat;

import javax.swing.text.NumberFormatter;

/**
 * NumberFormatter for formating double value.
 */
public class DoubleNumberFormatter extends NumberFormatter {

  public DoubleNumberFormatter() {
    super(createFormat());
  }

  private static DecimalFormat createFormat() {
    DecimalFormat format = new DecimalFormat();
    format.setMinimumFractionDigits(1);
    format.setMaximumFractionDigits(10);
    return format;
  }

  public static NumberFormatter create() {
    return new DoubleNumberFormatter();
  }

  @Override
  public Class<?> getValueClass() {
    return Double.class;
  }

}
