/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.utils;


import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import org.jdesktop.swingx.JXTable;

/**
 * Allows table columns to be resized to its preferred width by double-clicking
 * at the column-splitter in table header. the preferred width here is the min
 * width reqd to make the data of all rows of that column visible.
 */
class TableColumnAutoResizer extends MouseAdapter {

  @Override
  public void mouseClicked(MouseEvent e) {
    if (e.getClickCount() != 2) {
      return;
    }

    JTableHeader header = (JTableHeader) e.getSource();
    TableColumn tableColumn = getResizingColumn(header, e.getPoint());
    if (tableColumn == null) {
      return;
    }
    int col = header.getColumnModel().getColumnIndex(tableColumn.getIdentifier());
    JTable table = header.getTable();
    
    /*
    * Bug fixes #3187: this pack method causes problem when applying to a editable
    * JXTable. Once a JXTable is packed with this method, its
    * editable column cannot be edited until users click it multiples.
    */
    if(table instanceof JXTable) {
      ((JXTable)table).packColumn(col, 0);
    }else {
      int rowCount = table.getRowCount();
      int width = (int) header.getDefaultRenderer()
          .getTableCellRendererComponent(table, tableColumn.getIdentifier(),
              false, false, -1, col).getPreferredSize().getWidth();
      for (int row = 0; row < rowCount; row++) {
        int preferedWidth = (int) table.getCellRenderer(row, col).getTableCellRendererComponent(table,
            table.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth();
        width = Math.max(width, preferedWidth);
      }
      header.setResizingColumn(tableColumn); // this line is very important
      tableColumn.setWidth(width + table.getIntercellSpacing().width);
    }
  }

  // copied of BasicTableHeader.MouseInputHandler.getResizingColumn
  private TableColumn getResizingColumn(JTableHeader header, Point p) {
    return getResizingColumn(header, p, header.columnAtPoint(p));
  }

  // copied of BasicTableHeader.MouseInputHandler.getResizingColumn
  private TableColumn getResizingColumn(JTableHeader header, Point p, int column) {
    if (column == -1) {
      return null;
    }
    Rectangle rect = header.getHeaderRect(column);
    rect.grow(-3, 0);
    if (rect.contains(p)) {
      return null;
    }
    int midPoint = rect.x + rect.width / 2;
    int columnIndex;
    if (header.getComponentOrientation().isLeftToRight()) {
      columnIndex = (p.x < midPoint) ? column - 1 : column;
    } else {
      columnIndex = (p.x < midPoint) ? column : column - 1;
    }
    if (columnIndex == -1) {
      return null;
    }
    return header.getColumnModel().getColumn(columnIndex);
  }

  /**
   * when double click the header seperator, the column width will be set as
   * minimum.
   *
   */
  public static void enableColumnResizble(final JTable table) {
    if (columnAutoResizer == null) {
      columnAutoResizer = new TableColumnAutoResizer();
    }

    table.getTableHeader().addMouseListener(columnAutoResizer);
  }


  private static TableColumnAutoResizer columnAutoResizer;

}
