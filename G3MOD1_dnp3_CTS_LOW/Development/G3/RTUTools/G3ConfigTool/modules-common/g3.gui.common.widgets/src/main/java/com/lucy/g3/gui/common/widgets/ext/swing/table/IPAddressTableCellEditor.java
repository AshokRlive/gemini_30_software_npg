/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.table;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;

import com.lucy.g3.gui.common.widgets.ext.swing.textfield.IPAddressField;

public class IPAddressTableCellEditor extends AbstractCellEditor implements TableCellEditor {
  private IPAddressField field = new IPAddressField();
  
  public IPAddressTableCellEditor() {
    super();
    
    /* Select all on focus */
    field.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent e) {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
              field.selectAll();
            }
        });
      }
      @Override
      public void focusLost(FocusEvent e) {
      }
    });
  }

  public boolean stopCellEditing() {
    try {
      field.commitEdit();
      fireEditingStopped();
      return true;
    } catch (ParseException e) {
      return false;
    }
  }
  
  /**
   * Override to start edit when double click.
   */
  @Override
  public boolean isCellEditable(EventObject e) {
    if (e instanceof MouseEvent) {
      MouseEvent me = (MouseEvent) e;
      return me.getClickCount() >= 2;
    }
    return true;
  }
  
  @Override
  public Object getCellEditorValue() {
    return field.getValue();
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
    field.setText(value == null ? null : value.toString());
    //field.selectAll();
    field.select(0, field.getText().length());
    return field;
  }

//  @Override
//  public Object getCellEditorValue() {
//    IPAddressField field = getComponent();
//    return super.getCellEditorValue();
//  }
  
  
}
