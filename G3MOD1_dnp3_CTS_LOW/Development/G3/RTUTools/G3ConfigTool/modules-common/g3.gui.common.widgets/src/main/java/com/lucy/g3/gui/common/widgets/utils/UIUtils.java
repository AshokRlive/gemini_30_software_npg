/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.Action;
import javax.swing.DefaultRowSorter;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.JTextComponent;
import javax.swing.text.NumberFormatter;
import javax.swing.text.TextAction;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.swingx.JXTable;

/**
 * The utility for GUI.
 */
public final class UIUtils {
  private static Logger log = Logger.getLogger(UIUtils.class);
  
  private UIUtils() {
  }
  
  public static JFrame getMainFrame () {
    Application application = Application .getInstance();
    if (application instanceof SingleFrameApplication) {
      return ((SingleFrameApplication) application).getMainFrame();
    }
    
    return null;
  }
  
  public static File getLocalStorageDir() {
    File userdir = Application.getInstance().getContext().getLocalStorage().getDirectory();

    // Create local storage directories if they are not exists
    if (!userdir.exists()) {
      userdir.mkdirs();
    }
    return userdir;
  }

  public static void increaseScrollSpeed(JScrollPane pane) {
    pane.getVerticalScrollBar().setUnitIncrement(16);
    pane.getHorizontalScrollBar().setUnitIncrement(16);
  }
  
  /*
   * A listener for setting text filed's caret to where mouse is clicked.
   */
  private static class TextComponentMouseListener extends MouseAdapter {

    @Override
    public void mousePressed(final MouseEvent e) {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          if (e.getSource() instanceof JTextComponent) {
            JTextComponent tf = (JTextComponent) e.getSource();
            int offset = tf.viewToModel(e.getPoint());
            tf.setCaretPosition(offset);
          }
        }
      });
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      if (e.getClickCount() > 1) {
        JTextField tf = (JTextField) e.getSource();
        tf.selectAll();
      }
    }
  }


  /**
   * Add caret mouse listener to all text components in the parent container.
   *
   * @param parent
   *          top container
   */
  public static void setAllTextFieldsCaretPositionOnClick(Container parent) {
    Component[] comps = parent.getComponents();
    for (Component comp : comps) {
      if (comp instanceof JTextComponent) {
        comp.addMouseListener(new TextComponentMouseListener());
      } else if (comp instanceof JPanel) {
        setAllTextFieldsCaretPositionOnClick((JPanel) comp);
      }
    }
  }

  public static void setTextFieldsCaretPositionOnClick(JTextComponent comp) {
    if (comp != null) {
      comp.addMouseListener(new TextComponentMouseListener());
    }

  }

  public static void scrollToAddedRow(final JTable table) {
    table.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(TableModelEvent e) {
        final int row = e.getFirstRow();

        if (e.getType() == TableModelEvent.INSERT && row >= 0) {
          SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
              int viewRow = table.convertRowIndexToView(row);
              table.scrollRectToVisible(table.getCellRect(viewRow, 0, true));
              table.setRowSelectionInterval(viewRow, viewRow);
            }
          });
        }
      }
    });
  }
  
  public static void scrollTable(JTable table, int rowIndex, int columnIndex ){
    boolean includeSpacing = true;
    Rectangle cellRect = table.getCellRect(rowIndex, columnIndex, includeSpacing);
    table.scrollRectToVisible(cellRect);
  }
  

  /**
   * Adjusts all columns' width to fit their content.
   * @param table
   *          the table to be set column width
   */
  public static void packTableColumns(JTable table) {
    /*
     * Bug #3187: this pack method causes problem when applying to a
     * editable JXTable. Once a JXTable is packed with this method, its editable
     * column cannot be edited until users click it multiples.
     */
    if(table != null && table instanceof JXTable) {
      ((JXTable)table).packAll();
      return;
    }
    
    JTableHeader header = table.getTableHeader();
    int rowCount = table.getRowCount();
    Enumeration<?> columns = table.getColumnModel().getColumns();
    while (columns.hasMoreElements()) {
      TableColumn column = (TableColumn) columns.nextElement();
      int col = header.getColumnModel().getColumnIndex(
          column.getIdentifier());
      int width = (int) table.getTableHeader().getDefaultRenderer()
          .getTableCellRendererComponent(table, column.getIdentifier(), false, false, -1, col)
          .getPreferredSize().getWidth();
      for (int row = 0; row < rowCount; row++) {
        int preferedWidth = (int) table
            .getCellRenderer(row, col)
            .getTableCellRendererComponent(table,
                table.getValueAt(row, col), false, false, row,
                col).getPreferredSize().getWidth();
        width = Math.max(width, preferedWidth);
      }
      header.setResizingColumn(column);
      column.setWidth(width + table.getIntercellSpacing().width + 10);
    }
  }
  
  public static void sortTable(JTable table, int columnIndex, SortOrder order) {
    try {
      @SuppressWarnings("unchecked")
      DefaultRowSorter<TableModel, String> sorter = (DefaultRowSorter<TableModel, String>) table.getRowSorter();
      ArrayList<RowSorter.SortKey> list = new ArrayList<RowSorter.SortKey>();
      list.add(new RowSorter.SortKey(columnIndex, order));
      sorter.setSortKeys(list);
      sorter.sort();
    }catch (Throwable e) {
      log.error("Failed to sort table",e);
    }
  }

  /**
   * Derives a color by adding the specified offsets to the base color's hue,
   * saturation, and brightness values. The resulting hue, saturation, and
   * brightness values will be constrained to be between 0 and 1.
   *
   * @param base
   *          the color to which the HSV offsets will be added
   * @param dH
   *          the offset for hue
   * @param dS
   *          the offset for saturation
   * @param dB
   *          the offset for brightness
   * @return Color with modified HSV values
   */
  public static Color deriveColorHSB(Color base, float dH, float dS, float dB) {
    float[] hsb = Color.RGBtoHSB(
        base.getRed(), base.getGreen(), base.getBlue(), null);

    hsb[0] += dH;
    hsb[1] += dS;
    hsb[2] += dB;
    return Color.getHSBColor(
        hsb[0] < 0 ? 0 : (hsb[0] > 1 ? 1 : hsb[0]),
        hsb[1] < 0 ? 0 : (hsb[1] > 1 ? 1 : hsb[1]),
        hsb[2] < 0 ? 0 : (hsb[2] > 1 ? 1 : hsb[2]));

  }



  public static void registerKeyAction(JComponent component, int keyEvent, Action action){
    Object name = action.getValue(Action.NAME);
    component.getInputMap().put(KeyStroke.getKeyStroke(keyEvent, 0), name);
    component.getActionMap().put(name, action);
  }

  public static boolean isDoubleClick(MouseEvent e) {
    return SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2;
  }

  public static boolean isPopupTrigger(MouseEvent e) {
    return e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON3;
  }
  
  public static void enableComponents(Container container, boolean enable) {
    Component[] components = container.getComponents();
    for (Component component : components) {
      component.setEnabled(enable);
      if (component instanceof Container) {
        enableComponents((Container) component, enable);
      }
    }
  }
  
  public static void setDefaultConfig(JXTable table, boolean sortable, boolean packed, boolean columnController) {
    if(sortable) {
      table.setAutoCreateRowSorter(true);
      table.setSortable(true);
      table.setSortOrderCycle(SortOrder.ASCENDING, SortOrder.DESCENDING, SortOrder.UNSORTED);
    } else {
      table.setAutoCreateRowSorter(false);
      table.setSortable(false);
      table.setRowSorter(null);
    }
    
    if(packed) {
      table.packAll();
    }
    
    if(columnController){
      table.setColumnControlVisible(columnController);
    }

    // enable double-click to auto-resize column width
    TableColumnAutoResizer.enableColumnResizble(table);
  }

  public static DefaultFormatterFactory createUnsigIntFactory() {
    NumberFormatter intFormatter = new NumberFormatter(NumberFormat.getIntegerInstance());
    intFormatter.setMinimum(0);
    return new DefaultFormatterFactory(intFormatter);
  }
  
  public static JPopupMenu createTextFieldPopup() {
    {
      JPopupMenu menu = new JPopupMenu();
      Action cut = new DefaultEditorKit.CutAction();
      cut.putValue(Action.NAME, "Cut");
      cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
      menu.add(cut);

      Action copy = new DefaultEditorKit.CopyAction();
      copy.putValue(Action.NAME, "Copy");
      copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
      menu.add(copy);

      Action paste = new DefaultEditorKit.PasteAction();
      paste.putValue(Action.NAME, "Paste");
      paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
      menu.add(paste);

      Action selectAll = new SelectAll();
      menu.add(selectAll);
      return menu;
    }
  }


  private static class SelectAll extends TextAction {

    public SelectAll() {
      super("Select All");
      putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      JTextComponent component = getFocusedComponent();
      if(component != null) {
        component.selectAll();
        component.requestFocusInWindow();
      }
    }
  }
  
  public static void showFileInExplorer(File targetFile) throws IOException{
    Runtime.getRuntime().exec("explorer.exe /select," + targetFile.getAbsolutePath());
  }
  
  public static void setOpaqueForAll(JComponent aComponent, Class<? extends JComponent> componentClass, boolean isOpaque) {
    aComponent.setOpaque(isOpaque);
    Component[] comps = aComponent.getComponents();
    for (Component c : comps) {
      if (componentClass.isInstance(c)) {
        setOpaqueForAll((JComponent) c, componentClass, isOpaque);
      }
    }
  }
}
