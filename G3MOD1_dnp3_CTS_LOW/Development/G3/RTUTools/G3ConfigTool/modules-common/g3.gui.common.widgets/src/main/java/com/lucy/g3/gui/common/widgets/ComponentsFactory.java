/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets;

import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.ListModel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.AbstractWrappedValueModel;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Strings;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.Constraints;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.IPAddressField;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;

/**
 * A factory for creating and binding components, and also, the boundary will be
 * set once the component is created.
 */
public class ComponentsFactory extends BasicComponentFactory {

  private final Constraints constraint;

  private final String prefix;
  private final PresentationModel<?> model;


  public ComponentsFactory(Constraints constraint, String prefix, PresentationModel<?> model) {
    super();
    this.constraint = constraint;
    this.prefix = prefix;
    this.model = model;
  }

  public JCheckBox createCheckBox(String property) {
    return BasicComponentFactory.createCheckBox(model.getComponentModel(property), "");
  }

  public JComboBox<Object> createComboBox(String property, ListModel<?> listMode) {
    @SuppressWarnings("unchecked")
    ComboBoxModel<Object> comboModel = new ComboBoxAdapter<Object>(listMode, model.getComponentModel(property));

    return createComboBox(comboModel);
  }

  public JComboBox<Object> createComboBox(String property, Object[] choices) {
    return createComboBox(property, choices, null);
  }

  public JComboBox<Object> createComboBox(String property, Object[] choices, BindingConverter<?, ?> converter) {
    ValueModel valueModel = model.getComponentModel(property);

    if (converter != null) {
      valueModel = new ConverterValueModel(valueModel, converter);
    }

    @SuppressWarnings("unchecked")
    ComboBoxModel<Object> comboModel = new ComboBoxAdapter<Object>(choices, valueModel);

    return createComboBox(comboModel);
  }

  private JComboBox<Object> createComboBox(ComboBoxModel<Object> model) {
    JComboBox<Object> comboBox = new JComboBox<>(model);
    ComboBoxUtil.makeWider(comboBox);
    return comboBox;
  }

  public JFormattedTextField createTextField(String property) {
    JFormattedTextField ftf = new JFormattedTextField();
    Bindings.bind(ftf, model.getComponentModel(property));

    installCommitOnType(ftf);
    installHintForTextField(property, ftf);
    return ftf;
  }

  private static void installCommitOnType(JFormattedTextField ftf) {
    FormattedTextFieldSupport.installCommitOnType(ftf);
  }

  private void installHintForNumberField(String property, JFormattedTextField inputField) {
    String hint = constraint.getDescript(prefix, property);

    if (Strings.isBlank(hint)) {
      hint =
          String.format("Value range: [%d, %d]", constraint.getMin(prefix, property),
              constraint.getMax(prefix, property));
    }
    ValidationComponentUtils.setInputHint(inputField, hint);
  }

  private void installHintForTextField(String property, JFormattedTextField inputField) {
    String hint = constraint.getDescript(prefix, property);
    if (!Strings.isBlank(hint)) {
      ValidationComponentUtils.setInputHint(inputField, hint);
    } else {
      ValidationComponentUtils.setInputHint(inputField, null);
    }
  }

  public JFormattedTextField createNumberField(String property) {
    return createNumberField(property, model.getComponentModel(property));
  }

  private JFormattedTextField createNumberField(String property, ValueModel valueModel) {
    JFormattedTextField ftf = createNumberField(valueModel, 
        constraint.getMin(prefix, property),
        constraint.getMax(prefix, property));
    
    installHintForNumberField(property, ftf);
    
    return ftf;
  }
  
  public static JFormattedTextField createNumberField(ValueModel valueModel, Comparable<?> min, Comparable<?> max) {
    AbstractFormatter formatter = new BoundaryNumberFormatter(
        min,
        max);

    JFormattedTextField ftf = BasicComponentFactory.createFormattedTextField(valueModel, formatter);
    installCommitOnType(ftf);

    return ftf;
  }

  // public JFormattedTextField createIpField(String ipAddressProperty, boolean
  // allowBlank){
  // return createIpField(ipAddressProperty, null, allowBlank);
  // }
  //
  // public JFormattedTextField createIpField(String ipAddressProperty, String
  // anyIp){
  // return createIpField(ipAddressProperty, anyIp, false);
  // }

  public JFormattedTextField createIpField(String ipAddressProperty, String anyIpProperty) {
    return createIpField(ipAddressProperty, anyIpProperty, false);
  }

  public JFormattedTextField createIpField(String ipAddressProperty, String anyIpProperty, boolean allowBlank) {
    AbstractValueModel anyIpModel = model.getComponentModel(anyIpProperty);
    AbstractWrappedValueModel ipAddressModel = model.getComponentModel(ipAddressProperty);

    return IPAddressField.create(ipAddressModel, anyIpModel, allowBlank);
  }
}
