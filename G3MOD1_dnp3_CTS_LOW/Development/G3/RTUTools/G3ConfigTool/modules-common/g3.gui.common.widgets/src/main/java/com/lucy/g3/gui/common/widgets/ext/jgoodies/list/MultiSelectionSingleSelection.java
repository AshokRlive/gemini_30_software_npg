/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.list;

import java.beans.PropertyChangeListener;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;

import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;

/**
 * Adapts a ListModel to a ValueModel If the list has exactly one item the
 * valueModel is set to this item. Otherwise the ValueModel is set to null. The
 * intended use is with multi-selections, that should for operations that work
 * only on single objects be adapted to single selections.
 */
public class MultiSelectionSingleSelection implements ValueModel {

  private ListModel<?> multiSelection;
  private ValueHolder singleSelection;


  public MultiSelectionSingleSelection(ListModel<?> multiSelection) {
    this.multiSelection = multiSelection;
    this.singleSelection = new ValueHolder();

    final ListDataListener listener = new ListDataListener();
    multiSelection.addListDataListener(listener);
    listener.intervalAdded(new ListDataEvent(this, 0, 0, 0));
  }

  @Override
  public Object getValue() {
    return singleSelection.getValue();
  }

  @Override
  public void setValue(Object newValue) {
    singleSelection.setValue(newValue);
  }

  @Override
  public void addValueChangeListener(PropertyChangeListener l) {
    singleSelection.addValueChangeListener(l);
  }

  @Override
  public void removeValueChangeListener(PropertyChangeListener l) {
    singleSelection.removeValueChangeListener(l);
  }


  private class ListDataListener implements javax.swing.event.ListDataListener {

    @Override
    public void contentsChanged(ListDataEvent e) {
      // todo think harder ;-)
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
      if (multiSelection.getSize() == 1) {
        singleSelection.setValue(multiSelection.getElementAt(0));
      } else {
        singleSelection.setValue(null);
      }
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      if (multiSelection.getSize() == 1) {
        singleSelection.setValue(multiSelection.getElementAt(0));
      } else {
        singleSelection.setValue(null);
      }
    }
  }
}
