/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.textfield;

import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.IPAddressField;

/**
 * The Class IPAddressFieldTest.
 */
public class IPAddressFieldTest {

  public static void main(String[] args) {
    test(false, true);
  }

  private static void test(boolean allowAny, boolean allowBlank) {
    IPAddressField comp = new IPAddressField(allowAny, allowBlank);
    ValueModel vm = new PropertyAdapter<>(new ExampleIP(), "ip");
    comp.bindTo(vm);
    showFrame(comp);
  }


  private static void showFrame(IPAddressField comp) {
    JFrame frame = new JFrame();
    frame.add(comp);
    frame.setVisible(true);
  }


  public static class ExampleIP extends Model {

    private Logger log = Logger.getLogger(IPAddressFieldTest.ExampleIP.class);
    private String ip;


    public ExampleIP() {
      log.setLevel(Level.DEBUG);
      //LoggingUtil.logPropertyChanges(this);
    }

    @Override
    public String toString() {
      return "ExampleIP";
    }

    public String getIp() {
      return ip;
    }

    public void setIp(String ip) {
      Object oldValue = this.ip;
      this.ip = ip;
      firePropertyChange("ip", oldValue, ip);
    }

  }
}
