/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.action;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JFrame;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * Wrap an action to display busy cursor when performing that action.
 */
public class BusyCursorAction implements Action {

  private final Action action;
  private final Component owner;


  public BusyCursorAction(Action action, Component owner) {
    if(action == null)
      throw new IllegalArgumentException("action must not be null");
    this.action = action;
    this.owner = owner;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Component parent = owner;
    
    if(parent == null) {
      Application app = Application.getInstance();
      if(app instanceof SingleFrameApplication) {
        parent = ((SingleFrameApplication)app).getMainFrame();
      }
    }
    
    try {
      if (parent != null) {
        parent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      }
      action.actionPerformed(e);
    } finally {
      if (parent != null) {
        parent.setCursor(Cursor.getDefaultCursor());
      }
    }
  }

  @Override
  public Object getValue(String key) {
    return action.getValue(key);
  }

  @Override
  public void putValue(String key, Object value) {
    action.putValue(key, value);
  }

  @Override
  public void setEnabled(boolean b) {
    action.setEnabled(b);
  }

  @Override
  public boolean isEnabled() {
    return action.isEnabled();
  }

  @Override
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    action.addPropertyChangeListener(listener);
  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener listener) {
    action.removePropertyChangeListener(listener);
  }

  public static Action wrap(Action action, Component owner){
    return new BusyCursorAction(action, owner);
  }
  
  public static Action wrap(Action action){
    return wrap(action, null);
  }
  
  public static ActionListener wrap(Component component, final ActionListener mainActionListener) {
    return new BusyCursorActionListener(component, mainActionListener);
  }
  
  private static class BusyCursorActionListener implements ActionListener{
    private final Component busyComp;
    private final  ActionListener mainActionListener;
    
    public BusyCursorActionListener(Component busyComp, ActionListener mainActionListener){
      this.busyComp = busyComp; 
      this.mainActionListener = mainActionListener;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {

      try {
        if (busyComp != null) {
          busyComp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
        mainActionListener.actionPerformed(ae);
      } finally {
        if (busyComp != null) {
          busyComp.setCursor(Cursor.getDefaultCursor());
        }
      }
    }
  }
  
  public static void apply(AbstractButton button, Component owner){
    ActionListener[] acls = button.getActionListeners();
    for (int i = 0; i < acls.length; i++) {
      if(!(acls[i] instanceof BusyCursorActionListener))
      button.addActionListener(wrap(owner, acls[i]));
      button.removeActionListener(acls[i]);
    }
  }
}
