/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.console;

import javax.swing.JComponent;


/**
 *
 */
public interface IConsole {

  void appendBlankLine();

  void appendErr(String msg);

  void appendSuccess(String msg);

  void appendMsg(String msg);

  JComponent getComponent();

}

