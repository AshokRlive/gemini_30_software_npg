/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.utils;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;


/**
 *
 */
public class ResourceUtils {
  private static Logger log = Logger.getLogger(ResourceUtils.class);
  /**
   * Safely retrieving string in resource properties. If the value is not found
   * or the value is empty, return the key instead.
   */
  public static String getString(Class<?> start, Class<?> end, String key, Object... args) {
    String value = Application.getInstance().getContext().getResourceMap(start, end).getString(key, args);
    if (value == null) {
      log.error("No String found in " + end.getSimpleName() + ".properties"
          + " for key:" + key);
      value = key;
    }
    return value;
  }

  public static String getString(Class<?> end, String key) {
    return getString(end, end, key);
  }

  /**
   * Get an image icon defined in properties.
   * <p>
   * An empty imageIcon will be returned if the image resource is unavailable.
   * </p>
   *
   * @param urlKey
   *          the icons URL key
   */
  public static ImageIcon getImg(String key, Class<?> c) {
    ResourceMap map = Application.getInstance().getContext().getResourceMap(c);
    ImageIcon icon = map.getImageIcon(key);
    if (icon == null) {
      String err = "Couldn't find any image resource:" + key + " in \"" + c.getSimpleName() + ".properties\"";
      throw new RuntimeException(err);
      // log.error(err);
      // icon = EMPTY_ICON;
    }
    return icon;
  }

  public static String getAppName() {
    return getString(ResourceUtils.class, "Application.name");
  }
  
  public static ImageIcon getAppIcon() {
    return Application.getInstance().getContext().getResourceMap().getImageIcon("Application.icon");
  }

}

