/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.propertysheet;

import java.beans.BeanInfo;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.beans.PropertyVetoException;

import javax.swing.UIManager;

import org.apache.log4j.Logger;

import com.l2fprod.common.beans.ExtendedPropertyDescriptor;
import com.l2fprod.common.model.DefaultBeanInfoResolver;
import com.l2fprod.common.propertysheet.Property;
import com.l2fprod.common.propertysheet.PropertySheetPanel;

/**
 * Binds a bean object to a PropertySheet. <b>Note: this class is not part of
 * the library</b>
 */
public class PropertyEditorBinder {

  private Logger log = Logger.getLogger(PropertyEditorBinder.class);
  private final Object bean;
  private final PropertySheetPanel sheet;
  private final PropertyChangeListener listener;


  public PropertyEditorBinder(Object bean, PropertySheetPanel sheet) {
    this(bean, sheet, new DefaultBeanInfoResolver().getBeanInfo(bean));
  }

  public PropertyEditorBinder(Object bean, final PropertySheetPanel sheet, BeanInfo beanInfo) {
    this.bean = bean;
    this.sheet = sheet;

    if (beanInfo == null) {
      throw new RuntimeException("BeanInfo cannot be resolved from bean:" + bean);
    }
    // -----------------------Support sub properties--------------------------
    PropertyDescriptor[] des = beanInfo.getPropertyDescriptors();
    try {
      ExtendedPropertyDescriptor[] exDes = new ExtendedPropertyDescriptor[des.length];
      for (int i = 0; i < exDes.length; i++) {
        exDes[i] = (ExtendedPropertyDescriptor) des[i];
      }
      // add all properties to sheet
      this.sheet.setProperties(exDes);

    } catch (Exception e) {
      // Sub properties is not supported.
      this.sheet.setProperties(beanInfo.getPropertyDescriptors());
      e.printStackTrace();
    }

    this.sheet.readFromObject(bean);

    // everytime a property change, update the bean with it
    listener = new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        Property prop = (Property) evt.getSource();
        try {
          log.info("Writing value into Data object from property.");
          prop.writeToObject(PropertyEditorBinder.this.bean);
        } catch (RuntimeException e) {
          // handle PropertyVetoException and restore previous value
          if (e.getCause() instanceof PropertyVetoException) {
            UIManager.getLookAndFeel().provideErrorFeedback(PropertyEditorBinder.this.sheet);
            log.info("Restore property value.");
            prop.setValue(evt.getOldValue());
          } else {
            log.error(e.getMessage());
          }
        }
      }
    };
    sheet.addPropertySheetChangeListener(listener);
  }

  public void readFromObject() {
    sheet.readFromObject(bean);
  }

  public void unbind() {
    sheet.removePropertyChangeListener(listener);
    sheet.setProperties(new Property[0]);
  }

}
