/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.action;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.logging.Level;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;


/**
 * Action for opening a file.
 */
public class FileLinkAction extends AbstractAction {

  private final File file;


  public FileLinkAction(File file) {
    super();
    if(file == null)
      throw new IllegalArgumentException("file must not be null");
    
    this.file = file;
    putValue(NAME, file.getName());
    putValue(SHORT_DESCRIPTION, file.getAbsolutePath());
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (Desktop.isDesktopSupported()) {
      try {
        Desktop.getDesktop().open(file);

      } catch (Exception e1) {
        JOptionPane.showMessageDialog(null,
            e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    } else {
      java.util.logging.Logger.getGlobal().log(Level.SEVERE, "Desktop unsupported");
    }

  }
}
