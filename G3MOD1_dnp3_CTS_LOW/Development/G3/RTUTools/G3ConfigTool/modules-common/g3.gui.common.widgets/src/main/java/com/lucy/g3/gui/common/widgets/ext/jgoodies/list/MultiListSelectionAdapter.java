/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.list;

import java.util.EventListener;

import javax.swing.DefaultListSelectionModel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ObservableList;

/**
 * Adapts a ListModel holding the and an ObservableList to Swings
 * ListSelectionModel.
 *
 * @param <T>
 *          the item type.
 */
public class MultiListSelectionAdapter<T> extends Model implements ListSelectionModel {

  private ListModel<T> list;
  private final ObservableList<T> selectionListModel;
  private final DefaultListSelectionModel dlsm;
  private SwingSelectionListener swingSelectionListener;
  private BindingListener bindingListener;


  /**
   * Constructor.
   * @param list
   *          from which the user may choose
   * @param selection
   *          the model for the selection
   */
  public MultiListSelectionAdapter(ListModel<T> list, ObservableList<T> selection) {
    this.list = list;
    this.selectionListModel = selection;
    swingSelectionListener = new SwingSelectionListener();
    dlsm = new DefaultListSelectionModel();
    dlsm.setSelectionMode(MULTIPLE_INTERVAL_SELECTION);
    dlsm.addListSelectionListener(swingSelectionListener);
    bindingListener = new BindingListener();
    selectionListModel.addListDataListener(bindingListener);
  }

  /**
   * Constructor.
   * @param multiSelection
   *          the model for the selection
   */
  public MultiListSelectionAdapter(MultiSelectionInList<T> multiSelection) {
    this(multiSelection.getList(), multiSelection.getSelection());
  }

  @Override
  public int getAnchorSelectionIndex() {
    return dlsm.getAnchorSelectionIndex();
  }

  @Override
  public int getLeadSelectionIndex() {
    return dlsm.getLeadSelectionIndex();
  }

  @Override
  public int getMaxSelectionIndex() {
    return dlsm.getMaxSelectionIndex();
  }

  @Override
  public int getMinSelectionIndex() {
    return dlsm.getMinSelectionIndex();
  }

  @Override
  public int getSelectionMode() {
    return dlsm.getSelectionMode();
  }

  @Override
  public void clearSelection() {
    dlsm.clearSelection();
  }

  @Override
  public boolean getValueIsAdjusting() {
    return dlsm.getValueIsAdjusting();
  }

  public boolean isLeadAnchorNotificationEnabled() {
    return dlsm.isLeadAnchorNotificationEnabled();
  }

  @Override
  public boolean isSelectionEmpty() {
    return dlsm.isSelectionEmpty();
  }

  @Override
  public void setAnchorSelectionIndex(int anchorIndex) {
    dlsm.setAnchorSelectionIndex(anchorIndex);
  }

  @Override
  public void setLeadSelectionIndex(int leadIndex) {
    dlsm.setLeadSelectionIndex(leadIndex);
  }

  @Override
  public void setSelectionMode(int selectionMode) {
    dlsm.setSelectionMode(selectionMode);
  }

  @Override
  public boolean isSelectedIndex(int index) {
    return dlsm.isSelectedIndex(index);
  }

  @Override
  public void addSelectionInterval(int index0, int index1) {
    dlsm.addSelectionInterval(index0, index1);
  }

  @Override
  public void removeIndexInterval(int index0, int index1) {
    dlsm.removeIndexInterval(index0, index1);
  }

  @Override
  public void removeSelectionInterval(int index0, int index1) {
    dlsm.removeSelectionInterval(index0, index1);
  }

  @Override
  public void setSelectionInterval(int index0, int index1) {
    dlsm.setSelectionInterval(index0, index1);
  }

  @Override
  public void insertIndexInterval(int index, int length, boolean before) {
    dlsm.insertIndexInterval(index, length, before);
  }

  public void setLeadAnchorNotificationEnabled(boolean flag) {
    dlsm.setLeadAnchorNotificationEnabled(flag);
  }

  @Override
  public void setValueIsAdjusting(boolean isAdjusting) {
    dlsm.setValueIsAdjusting(isAdjusting);
  }

  public ListSelectionListener[] getListSelectionListeners() {
    return dlsm.getListSelectionListeners();
  }

  @Override
  public void addListSelectionListener(ListSelectionListener l) {
    dlsm.addListSelectionListener(l);
  }

  @Override
  public void removeListSelectionListener(ListSelectionListener l) {
    dlsm.removeListSelectionListener(l);
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public EventListener[] getListeners(Class listenerType) {
    return dlsm.getListeners(listenerType);
  }


  private class SwingSelectionListener implements ListSelectionListener {

    @Override
    public void valueChanged(ListSelectionEvent e) {
      selectionListModel.removeListDataListener(bindingListener);
      selectionListModel.clear();
      for (int i = 0; i < MultiListSelectionAdapter.this.list.getSize(); i++) {
        if (dlsm.isSelectedIndex(i)) {
          selectionListModel.add((MultiListSelectionAdapter.this.list.getElementAt(i)));
        }

      }

      selectionListModel.addListDataListener(bindingListener);
    }
  }

  private class BindingListener implements ListDataListener {

    @Override
    public void contentsChanged(ListDataEvent e) {
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
      dlsm.removeListSelectionListener(swingSelectionListener);
      dlsm.addSelectionInterval(e.getIndex0(), e.getIndex1());
      dlsm.addListSelectionListener(swingSelectionListener);

    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      dlsm.removeListSelectionListener(swingSelectionListener);
      dlsm.removeSelectionInterval(e.getIndex0(), e.getIndex1());
      dlsm.addListSelectionListener(swingSelectionListener);
    }
  }
}
