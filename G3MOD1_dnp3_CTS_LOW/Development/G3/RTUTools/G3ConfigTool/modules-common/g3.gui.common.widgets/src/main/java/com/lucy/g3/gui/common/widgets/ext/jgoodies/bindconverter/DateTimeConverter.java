/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import com.jgoodies.binding.value.BindingConverter;

/**
 * BindingConverter to convert date to string.
 */
public class DateTimeConverter implements BindingConverter<Date, String> {

  private final DateFormat format;

  private final String nullText;


  public DateTimeConverter() {
    this(DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT), "");
  }

  public DateTimeConverter(String nullText) {
    this(DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT), nullText);
  }

  public DateTimeConverter(DateFormat format) {
    this(format, "");
  }

  public DateTimeConverter(DateFormat format, String nullText) {
    if (format == null) {
      throw new IllegalArgumentException("format is null");
    }
    this.format = format;
    this.nullText = nullText;
  }

  @Override
  public Date sourceValue(String dateString) {
    Date date = null;

    try {
      date = format.parse(dateString);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return date;
  }

  @Override
  public String targetValue(Date subjectValue) {
    if (subjectValue == null) {
      return nullText;
    }

    return format.format(subjectValue);

  }
}
