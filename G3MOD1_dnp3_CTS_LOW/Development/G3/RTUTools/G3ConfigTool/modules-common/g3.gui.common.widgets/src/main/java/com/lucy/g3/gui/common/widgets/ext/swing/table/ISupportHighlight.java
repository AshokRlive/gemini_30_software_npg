/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.table;

/**
 * A table model that implements this interface can control the highlight cell
 * in the table view.
 */
public interface ISupportHighlight {

  boolean shouldHightlight(int rowIndex, int columnIndex);
}
