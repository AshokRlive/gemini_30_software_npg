/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.utils;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

public class WindowUtils {
  public static JFrame getMainFrame() {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      return ((SingleFrameApplication) app).getMainFrame();
    }
    return null;
  }
  
  public static void showFrame(JFrame frame) {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(frame);
    } else {
      frame.pack();
      frame.setVisible(true);
    }
  }
  
  public static void showDialog(JDialog dialog) {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(dialog);
    } else {
      dialog.pack();
      dialog.setVisible(true);
    }
  }
}

