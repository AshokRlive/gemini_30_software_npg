/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.animation;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.gui.common.widgets.animation.Blinker;

/**
 *
 */
public class BlinkerTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  // Test
  public static void main(String[] args) throws InterruptedException {
    JLabel target = new JLabel("Blinking Text");
    JFrame frame = new JFrame();
    frame.setContentPane(target);
    frame.pack();
    Blinker blinker = new Blinker(target);
    blinker.start();
    frame.setVisible(true);

    Thread.sleep(2000);
    blinker.setBlinkBg(null);
    blinker.setBlinkFg(Color.YELLOW);

    Thread.sleep(2000);
    blinker.setBlinkFg(Color.GREEN);

    Thread.sleep(2000);
    blinker.setBlinkFg(null);
    blinker.setBlinkFont(target.getFont().deriveFont(Font.ITALIC));

  }

}
