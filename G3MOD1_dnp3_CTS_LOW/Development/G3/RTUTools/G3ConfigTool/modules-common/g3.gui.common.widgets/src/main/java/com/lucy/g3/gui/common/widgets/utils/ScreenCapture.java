/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.utils;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * A tool for capturing screen.
 */
public class ScreenCapture {

  public void captureToClipboard(Rectangle rect) throws AWTException {
    BufferedImage capture = new Robot().createScreenCapture(rect);

    // place that image on the clipboard
    setClipboard(capture);
  }

  public void captureToFile(Rectangle rect, String filePath) throws AWTException, IOException {
    BufferedImage capture = new Robot().createScreenCapture(rect);

    // Write that image into file
    ImageIO.write(capture, "bmp", new File(filePath));
  }

  /**
   * This method writes a image to the system clipboard. otherwise it returns
   * null.
   */
  public static void setClipboard(Image image) {
    ImageSelection imgSel = new ImageSelection(image);
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
  }


  /** This class is used to hold an image while on the clipboard. */
  private static class ImageSelection implements Transferable {

    private Image image;


    public ImageSelection(Image image) {
      this.image = image;
    }

    // Returns supported flavors
    @Override
    public DataFlavor[] getTransferDataFlavors() {
      return new DataFlavor[] { DataFlavor.imageFlavor };
    }

    // Returns true if flavor is supported
    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
      return DataFlavor.imageFlavor.equals(flavor);
    }

    // Returns image
    @Override
    public Object getTransferData(DataFlavor flavor)
        throws UnsupportedFlavorException, IOException {
      if (!DataFlavor.imageFlavor.equals(flavor)) {
        throw new UnsupportedFlavorException(flavor);
      }
      return image;
    }
  }
}
