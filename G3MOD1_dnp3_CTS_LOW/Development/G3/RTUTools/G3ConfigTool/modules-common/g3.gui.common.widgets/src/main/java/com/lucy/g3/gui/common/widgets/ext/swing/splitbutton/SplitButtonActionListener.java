/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.splitbutton;

import java.awt.event.ActionEvent;
import java.util.EventListener;

/**
 * The listener interface for receiving action events. The class that is
 * interested in processing an action event implements this interface, and the
 * object created with that class is registered with a component, using the
 * component's <code>addSplitButtonActionListener</code> method. When the action
 * event occurs, that object's <code>buttonClicked</code> or
 * <code>splitButtonClicked</code> method is invoked.
 *
 * @see ActionEvent
 */
public interface SplitButtonActionListener extends EventListener {

  /**
   * Invoked when the button part is clicked.
   */
  void buttonClicked(ActionEvent e);

  /**
   * Invoked when split part is clicked.
   */
  void splitButtonClicked(ActionEvent e);

}
