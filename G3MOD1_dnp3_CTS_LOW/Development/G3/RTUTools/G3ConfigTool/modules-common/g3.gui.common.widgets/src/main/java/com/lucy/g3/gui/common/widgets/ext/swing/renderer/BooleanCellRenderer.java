/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets.ext.swing.renderer;

import javax.swing.SwingConstants;

import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.common.base.Preconditions;

/**
 * TableCellRenderer for boolean value.
 */
public class BooleanCellRenderer extends DefaultTableRenderer {
  public final static String TEXT_TRUE = "Yes";
  public final static String TEXT_FALSE = "No";
  public final static String TEXT_NULL = "-";

  public BooleanCellRenderer(String textTrue, String textFalse,
      String textNull) {
    super(new BooleanStringValue(textTrue, textFalse, textNull), SwingConstants.CENTER);
  }

  public BooleanCellRenderer() {
    this(TEXT_TRUE, TEXT_FALSE, TEXT_NULL);
  }
  
  
  
  /**
   * Creates a boolean renderer using the provided text.  
   * @return boolean table cell renderer.
   */
  public static BooleanCellRenderer create(String textTrue, String textFalse){
    return new BooleanCellRenderer(textTrue, textFalse, TEXT_NULL);
  }
  
  /**
   * Creates a boolean renderer using text: {@value #TEXT_TRUE} ,{@value #TEXT_FALSE}, {@value #TEXT_NULL}  
   * @return boolean table cell renderer.
   */
  public static BooleanCellRenderer create(){
    return new BooleanCellRenderer();
  }
  
  /**
   * Creates an online/offline boolean renderer.
   * @return boolean table cell renderer.
   */
  public static BooleanCellRenderer createOnline(){
    return new BooleanCellRenderer("Online", "Offline", TEXT_NULL);
  }
  
  /**
   * Creates an valid/invalid boolean renderer.
   * @return boolean table cell renderer.
   */
  public static BooleanCellRenderer createValid(){
    return new BooleanCellRenderer("Valid", "Invalid", TEXT_NULL);
  }

  public static class BooleanStringValue implements StringValue {
    private final String textTrue;
    private final String textFalse;
    private final String textNull;

    public BooleanStringValue(String textTrue, String textFalse, String textNull) {
      super();
      this.textTrue = Preconditions.checkNotNull(textTrue, "textTrue is null");
      this.textFalse = Preconditions.checkNotNull(textFalse, "textFalse is null");
      this.textNull = textNull;
    }
    
    public BooleanStringValue() {
      this(TEXT_TRUE, TEXT_FALSE, TEXT_NULL);
    }

    @Override
    public String getString(Object value) {
      if (value == Boolean.TRUE)
        return textTrue;
      else if (value == Boolean.FALSE)
        return textFalse;
      else if(value == null)
        return textNull;
      
      return "Unkown";
    }  
    
  }
}