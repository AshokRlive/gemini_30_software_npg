
package com.lucy.g3.security.support;

import static org.junit.Assert.*;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.junit.Test;

import com.lucy.g3.security.support.SecurityUtils;


public class SecurityUtilsTest {
  private static final String MD5 = "MD5"; 
  final static URL CRCTestFile = SecurityUtilsTest.class.getClassLoader().getResource("CRCTestFile");


  @Test
  public void testCalculateMD5() {
    final String sample = "fjasdlfDSALjkf";
    final byte[] sample_MD5 = { (byte) 0xfa, (byte) 0xed, (byte) 0xa8, (byte) 0x8f, 0x57,
        (byte) 0x9e, (byte) 0xd1, 0x39, (byte) 0xe6, (byte) 0xd5, 0x25, 0x76, 0x6a, (byte) 0xf3, 0x5b, (byte) 0xa6 };
    byte[] result = SecurityUtils.calculateMD(sample.toCharArray(),MD5);
    assertTrue("Calcuated MD5 not equal. \nExpected:" + Arrays.toString(sample_MD5)
        + "\nActual: " + Arrays.toString(result), Arrays.equals(sample_MD5, result));
  }

  @Test
  public void testCalculateSha512(){
    String sample = "sampleString";
    byte[] result = SecurityUtils.calculateMD(sample.toCharArray(), SecurityUtils.MD_ALGORITHM_SHA512);
    assertNotNull(result);
  }
  
  @Test
  public void testCalculateMD5String() {
    final String sample = "fjasdlfDSALjkf";
    final String sample_MD5String = "faeda88f579ed139e6d525766af35ba6";
    assertEquals(sample_MD5String, SecurityUtils.calculateMDStr(sample.toCharArray(),MD5));
  }

  @Test
  public void testCalculateMD5OfEmptyStr() {
    final String sample = "";
    final String sample_MD5String = "d41d8cd98f00b204e9800998ecf8427e";// MD5 of
    // empty
    // string
    assertEquals(sample_MD5String, SecurityUtils.calculateMDStr(sample.toCharArray(),MD5));
  }

  @Test
  public void testCalculateMD5String2() throws Exception {
    File file = new File(CRCTestFile.toURI());
    String checksum = "44ff70dd4998eb7d69b3dec3b004b60a";
    assertEquals(checksum, SecurityUtils.calculateMDStrForFile(file.getAbsolutePath(),MD5));
  }

  @Test
  public void testCalculateCRC32() throws Exception {
    File file = new File(CRCTestFile.toURI());
    byte[] CRC32 = new byte[] { 0x29, 0x7d, (byte) 0xb8, (byte) 0xef };

    long result = SecurityUtils.calculateCRC32(file.getAbsolutePath());

    assertEquals("Calcuated CRC32 is not equal to expected result", CRC32[0], (byte) (result >> 24 & 0x00FF));
    assertEquals("Calcuated CRC32 is not equal to expected result", CRC32[1], (byte) (result >> 16 & 0x00FF));
    assertEquals("Calcuated CRC32 is not equal to expected result", CRC32[2], (byte) (result >> 8 & 0x00FF));
    assertEquals("Calcuated CRC32 is not equal to expected result", CRC32[3], (byte) (result & 0x00FF));
  }

  @Test
  public void testEncodeUTF8() throws UnsupportedEncodingException {
    String sample = "Hello²‰";
    String bytes = SecurityUtils.encodeStringToHex(sample, "UTF-8");
    String text = SecurityUtils.decodeHexToString(bytes, "UTF-8");
    assertEquals(sample, text);
    text = SecurityUtils.decodeHexToString(bytes, "ASCII");
    assertNotSame(sample, text);
  }

  @Test
  public void testSHA1() throws NoSuchAlgorithmException{
    String ret = SecurityUtils.calculateMDStr("123".toCharArray(), SecurityUtils.MD_ALGORITHM_MD5);
    System.out.println(ret);
  }
}
