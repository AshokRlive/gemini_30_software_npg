/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.security.support;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.DSAKey;
import java.security.interfaces.ECKey;
import java.security.interfaces.RSAKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;

import javax.crypto.interfaces.DHKey;

import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.common.utils.StringUtils;


public class SecurityUtils {
  /** Logger */
  private static final Logger log = Logger.getLogger(SecurityUtils.class.getName());
  
  public final static String MD_ALGORITHM_MD5 = "MD5";
  public final static String MD_ALGORITHM_SHA1 = "SHA-1";
  public final static String MD_ALGORITHM_SHA256 = "SHA-256";
  public final static String MD_ALGORITHM_SHA512 = "SHA-512";
  
  public static long calculateCRC32(String filepath) throws IOException {
    InputStream inputStreamn = new FileInputStream(filepath);

    CRC32 crc = new CRC32();

    byte[] buf = new byte[1024 * 64];

    int gbyte;
    while ((gbyte = inputStreamn.read(buf)) > 0) {
      crc.update(buf, 0, gbyte);
    }
    inputStreamn.close();
    return crc.getValue();// & 0x00FFFFFFFF

  }

  /**
   * Calculates message digest bytes for a file.
   * @param algorithm message digest algorithm.E.g. MD_ALGORITHM_MD5, MD_ALGORITHM_SHA1, MD_ALGORITHM_SHA256.
   */
  public static byte[] calculateMDForFile(String filepath,String algorithm) throws Exception {
    InputStream fis = new FileInputStream(filepath);

    byte[] buffer = new byte[1024];
    MessageDigest complete = MessageDigest.getInstance("MD5");
    int numRead;

    do {
      numRead = fis.read(buffer);
      if (numRead > 0) {
        complete.update(buffer, 0, numRead);
      }
    } while (numRead != -1);

    fis.close();
    return complete.digest();
  }

  /**
   * Calculates message digest string for a file.
   * @param algorithm message digest algorithm.E.g. MD_ALGORITHM_MD5, MD_ALGORITHM_SHA1, MD_ALGORITHM_SHA256.
   */
  public static String calculateMDStrForFile(String filepath, String algorithm) throws Exception {
    byte[] md5 = calculateMDForFile(filepath, algorithm);
    String result = "";
  
    for (int i = 0; i < md5.length; i++) {
      result += Integer.toString((md5[i] & 0xff) + 0x100, 16).substring(1);
    }
    return result;
  }

  /**
   * Calculates message digest for chars.
   * @param algorithm message digest algorithm.E.g. MD_ALGORITHM_MD5, MD_ALGORITHM_SHA1, MD_ALGORITHM_SHA256.
   */
  public static byte[] calculateMD(char[] chars, String algorithm) {
    MessageDigest md;
    try {
      md = MessageDigest.getInstance(algorithm);

      md.update(String.valueOf(chars).getBytes());
      return md.digest();

    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      System.err.println("Calculate MD5 Failed: " + e.getMessage());
      return null;
    }

  }
  
  /**
   * Calculates message digest string for chars.
   * @param algorithm message digest algorithm.E.g. MD_ALGORITHM_MD5, MD_ALGORITHM_SHA1, MD_ALGORITHM_SHA256.
   */
  public static String calculateMDStr(char[] chars,String algorithm) {
    MessageDigest md;
    try {
      md = MessageDigest.getInstance(algorithm);

      md.update(String.valueOf(chars).getBytes());
      byte[] bytes = md.digest();
      
      return convertHashToStr(bytes);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public static String convertHashToStr(byte[] hash){
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < hash.length; i++) {
      sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();
  }

  
  /**
   * Encode a string into bytes using specific charSet, and convert the result
   * to hex format string, e.g. "0x010xab0xde".
   *
   * @see #decodeHexToString(String, String)
   */
  public static String encodeStringToHex(String text, String charSet) throws UnsupportedEncodingException {
    if (text == null) {
      return null;
    }

    byte[] siteNameBytes = text.getBytes(charSet);
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < siteNameBytes.length; i++) {
      sb.append("0x");
      sb.append(NumUtils.byteToHex(siteNameBytes[i]));
      sb.append(",");// delimiter
    }
    return sb.toString();
  }

  /**
   * Decode bytes string generated by
   * {@linkplain #encodeStringToHex(String, String)} to the string.
   *
   * @see #encodeStringToHex(String, String)
   */
  public static String decodeHexToString(String bytes, String charSet) throws UnsupportedEncodingException {
    if (bytes == null) {
      return null;
    }

    String[] hex = bytes.split(",");// split by delimiter
    byte[] byteArray = new byte[hex.length];

    for (int i = 0; i < byteArray.length; i++) {
      byteArray[i] = (byte) (Short.decode(hex[i]) & 0x00ff);// (byte)(Integer.parseInt(hex[i],16)&0x00FF);
    }
    return new String(byteArray, charSet);
  }
  

  /** Constant representing unknown key size */
  public static final int UNKNOWN_KEY_SIZE = -1;

  /**
   * Get the key size of a public key.
   * 
   * @param pubKey The public key
   * @return The key size, {@link #UNKNOWN_KEY_SIZE} if not known
   */
  public static int getKeyLength(PublicKey pubKey)
  {
    if (pubKey instanceof RSAKey)
    {
      return ((RSAKey) pubKey).getModulus().bitLength();
    }
    else if (pubKey instanceof DSAKey)
    {
      return ((DSAKey) pubKey).getParams().getP().bitLength();
    }
    else if (pubKey instanceof DHKey)
    {
      return ((DHKey) pubKey).getParams().getP().bitLength();
    }
    else if (pubKey instanceof ECKey)
    {
      // TODO: how to get key size from these?
      return UNKNOWN_KEY_SIZE;
    }

    log.warning("Don't know how to get key size from key " + pubKey);
    return UNKNOWN_KEY_SIZE;
  }
  
  /**
   * Get the digest of a message as a formatted String.
   * 
   * @param bMessage The message to digest
   * @param digestType The message digest algorithm
   * @return The message digest
   * @throws NoSuchAlgorithmException 
   */
  public static String getMessageDigest(byte[] bMessage, String digestType) 
  {
    // Create message digest object using the supplied algorithm
    MessageDigest messageDigest = null;;
    try {
      messageDigest = MessageDigest.getInstance(digestType);
    } catch (NoSuchAlgorithmException e) {
      log.log(Level.SEVERE, e.getMessage());
      return null;
    }

    // Create raw message digest
    byte[] bFingerPrint = messageDigest.digest(bMessage);

    // Return the formatted message digest
    StringBuilder sb = StringUtils.toHex(bFingerPrint, 2, ":");
    return sb.toString();
  }
  
  @SuppressWarnings("restriction")
  public static String base64Encode(byte[] bytes){
    sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
    return encoder.encodeBuffer(bytes);
  }
  
  @SuppressWarnings("restriction")
  public static byte[] base64Decode(String str) throws IOException{
    sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
    return decoder.decodeBuffer(str);
  }
}

