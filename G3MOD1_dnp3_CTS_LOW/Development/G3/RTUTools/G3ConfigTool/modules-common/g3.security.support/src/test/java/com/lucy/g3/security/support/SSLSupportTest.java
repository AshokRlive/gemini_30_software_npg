/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.security.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import com.lucy.g3.security.support.SSLSupport;

/**
 *
 */
public class SSLSupportTest {
  public static void main(String[] args) throws IOException, GeneralSecurityException {
    testReadCert();
  }

  private static void testReadCert() {
    try {
      URL url = new URL("https://10.11.3.162");
//       URL url = new URL("https://www.google.co.uk");
      
      SSLSupport support = new SSLSupport();
      support.importKeyStore(new File("E:\\sw_dev\\temp\\OpenSSL\\emptyStore_111.jks"), "111".toCharArray());
      
      HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
      support.configure(conn);
//      conn.setHostnameVerifier(SSLSupport.noopNameVerifier);
      //conn.setSSLSocketFactory(SSLSupport.trustAllSslSocketFactory);
      print_https_cert(conn);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void print_https_cert(HttpsURLConnection con) {

    if (con != null) {

      try {

        System.out.println("Response Code : " + con.getResponseCode());
        System.out.println("Cipher Suite : " + con.getCipherSuite());
        System.out.println("\n");

        Certificate[] certs = con.getServerCertificates();
        for (Certificate cert : certs) {
          System.out.println("Cert Type : " + cert.getType());
          System.out.println("Cert Hash Code : " + cert.hashCode());
          System.out.println("Cert Public Key Algorithm : "
              + cert.getPublicKey().getAlgorithm());
          System.out.println("Cert Public Key Format : "
              + cert.getPublicKey().getFormat());
          System.out.println("\n");
        }

      } catch (SSLPeerUnverifiedException e) {
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }

    }

  }
}
