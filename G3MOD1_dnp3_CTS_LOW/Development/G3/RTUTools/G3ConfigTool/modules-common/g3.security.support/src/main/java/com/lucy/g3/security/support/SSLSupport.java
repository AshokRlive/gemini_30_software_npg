/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.security.support;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SSLSupport {

  public static final int DEFAULT_PORT = 443;

  private PropertyChangeSupport support = new PropertyChangeSupport(this);

  private boolean ignoreCertificateError;

  /*
   * A ssl socket factory that verifies host certificate using the imported
   * keystore.
   */
  private SSLSocketFactory sslFactory;


  public void setIgnoreCertificateError(boolean ignoreCertificateError) {
    support.firePropertyChange("ignoreCertificateError", isIgnoreCertificateError(),
        this.ignoreCertificateError = ignoreCertificateError);
  }

  public boolean isIgnoreCertificateError() {
    return ignoreCertificateError;
  }

  public void configure(HttpsURLConnection conn) {
    conn.setHostnameVerifier(noopNameVerifier);

    if (isIgnoreCertificateError()) {
      conn.setSSLSocketFactory(trustAllSslSocketFactory);
    } else {
      conn.setSSLSocketFactory(sslFactory != null ? sslFactory : HttpsURLConnection.getDefaultSSLSocketFactory());
    }
  }

  public void clearKeyStore() {
    sslFactory = null;
  }

  public void importKeyStore(File keyStoreFile, char[] password)
      throws KeyManagementException, KeyStoreException, NoSuchAlgorithmException,
      CertificateException, FileNotFoundException, IOException {
    sslFactory = createSocketFactory(keyStoreFile, password);

    if (sslFactory == null) {
      throw new IllegalArgumentException("Cannot create SSL socket factory with the provided keystore!");
    }
  }

  public void addPropertyChangeListener(PropertyChangeListener listener) {
    support.addPropertyChangeListener(listener);
  }

  public void removePropertyChangeListener(PropertyChangeListener listener) {
    support.removePropertyChangeListener(listener);
  }

  public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    support.addPropertyChangeListener(propertyName, listener);
  }

  public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    support.removePropertyChangeListener(propertyName, listener);
  }


  // ================== Static Fields ===================

  static final HostnameVerifier noopNameVerifier;

  /* A ssl socket factory that trusts all certificates. */
  static final SSLSocketFactory trustAllSslSocketFactory;

  static {
    noopNameVerifier = new HostnameVerifier() {

      @Override
      public boolean verify(String hostname, SSLSession session) {
        return true;
      }
    };

    TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

      @Override
      public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      @Override
      public void checkClientTrusted(
          java.security.cert.X509Certificate[] arg0, String arg1)
          throws CertificateException {
      }

      @Override
      public void checkServerTrusted(
          java.security.cert.X509Certificate[] arg0, String arg1)
          throws CertificateException {
      }
    }
    };

    SSLSocketFactory factory = null;
    try {
      SSLContext sc = SSLContext.getInstance("TLS");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      factory = sc.getSocketFactory();
    } catch (KeyManagementException | NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    trustAllSslSocketFactory = factory;
  };


  public static SSLSocketFactory createSocketFactory(File keyStoreFile, char[] password)
      throws KeyStoreException, NoSuchAlgorithmException, CertificateException,
      FileNotFoundException, IOException, KeyManagementException {

    FileInputStream is = new FileInputStream(keyStoreFile);
    KeyStore ks = KeyStore.getInstance("jks");
    ks.load(is, password);

    TrustManagerFactory tm = TrustManagerFactory.getInstance("PKIX");
    tm.init(ks);

    SSLContext sc = SSLContext.getInstance("TLS");
    sc.init(null, tm.getTrustManagers(), new java.security.SecureRandom());

    is.close();
    return sc.getSocketFactory();
  }


  private static String protocol;
  private static String cipherSuite;


  public static String getPeerProtocol() {
    return protocol;
  }

  public static String getPeerCipherSuite() {
    return cipherSuite;
  }

  public static X509Certificate[] getPeerCerts(InetSocketAddress ia) throws Exception {
    int timeOut = 10000;

    // Get the certificates received from the connection
    X509Certificate[] certs = null;
    SSLSocket ss = null;
    Socket socket = null;

    try {
      SSLSocketFactory sf;
      {
        SSLContext sc = SSLContext.getInstance("SSL");
        X509TrustManager[] tm = { new X509TrustManager() {

          @Override
          public void checkClientTrusted(X509Certificate[] chain, String authType) {
            // Trust anything
          }

          @Override
          public void checkServerTrusted(X509Certificate[] chain, String authType) {
            // Trust anything
          }

          @Override
          public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
          }
        } };
        sc.init(null, tm, new SecureRandom());
        sf = sc.getSocketFactory();
      }

      // Go through a regular SocketFactory in order to be able to:
      // - control connection timeouts before connecting, and
      // - be able to use a host(String), port based method; otherwise
      // apparently no SNI

      socket = new Socket(Proxy.NO_PROXY);//SocketFactory.getDefault().createSocket();
      socket.setSoTimeout(timeOut);
      socket.connect(ia, timeOut);
      ss = (SSLSocket) sf.createSocket(socket, ia.getHostString(), ia.getPort(), false);

      SSLSession sess = ss.getSession();
      certs = (X509Certificate[]) sess.getPeerCertificates();
      protocol = sess.getProtocol();
      cipherSuite = sess.getCipherSuite();
      sess.invalidate();
    } finally {
      if (ss != null && !ss.isClosed()) {
        try {
          ss.close();
        } catch (IOException e) {

        }
      }
      if (socket != null && !socket.isClosed()) {
        try {
          socket.close();
        } catch (IOException e) {
        }
      }
    }

    return certs;
  }

}
