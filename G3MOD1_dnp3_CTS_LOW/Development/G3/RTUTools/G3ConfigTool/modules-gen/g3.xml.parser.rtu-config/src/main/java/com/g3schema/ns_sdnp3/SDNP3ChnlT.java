////////////////////////////////////////////////////////////////////////
//
// SDNP3ChnlT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_sdnp3;

public class SDNP3ChnlT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_sdnp3_altova_SDNP3ChnlT]); }
	
	public SDNP3ChnlT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		config= new MemberElement_config (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_SDNP3ChnlT._config]);
		session= new MemberElement_session (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_SDNP3ChnlT._session]);
	}
	// Attributes


	// Elements
	
	public MemberElement_config config;

		public static class MemberElement_config
		{
			public static class MemberElement_config_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_config member;
				public MemberElement_config_Iterator(MemberElement_config member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_sdnp3.SDNP3ChnlConfT nx = new com.g3schema.ns_sdnp3.SDNP3ChnlConfT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_config (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_sdnp3.SDNP3ChnlConfT at(int index) {return new com.g3schema.ns_sdnp3.SDNP3ChnlConfT(owner.getElementAt(info, index));}
			public com.g3schema.ns_sdnp3.SDNP3ChnlConfT first() {return new com.g3schema.ns_sdnp3.SDNP3ChnlConfT(owner.getElementFirst(info));}
			public com.g3schema.ns_sdnp3.SDNP3ChnlConfT last(){return new com.g3schema.ns_sdnp3.SDNP3ChnlConfT(owner.getElementLast(info));}
			public com.g3schema.ns_sdnp3.SDNP3ChnlConfT append(){return new com.g3schema.ns_sdnp3.SDNP3ChnlConfT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_config_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_session session;

		public static class MemberElement_session
		{
			public static class MemberElement_session_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_session member;
				public MemberElement_session_Iterator(MemberElement_session member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_sdnp3.SDNP3SesnT nx = new com.g3schema.ns_sdnp3.SDNP3SesnT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_session (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_sdnp3.SDNP3SesnT at(int index) {return new com.g3schema.ns_sdnp3.SDNP3SesnT(owner.getElementAt(info, index));}
			public com.g3schema.ns_sdnp3.SDNP3SesnT first() {return new com.g3schema.ns_sdnp3.SDNP3SesnT(owner.getElementFirst(info));}
			public com.g3schema.ns_sdnp3.SDNP3SesnT last(){return new com.g3schema.ns_sdnp3.SDNP3SesnT(owner.getElementLast(info));}
			public com.g3schema.ns_sdnp3.SDNP3SesnT append(){return new com.g3schema.ns_sdnp3.SDNP3SesnT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_session_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "protocolstack.dnp3.slave", "SDNP3ChnlT");}
}
