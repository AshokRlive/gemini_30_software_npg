////////////////////////////////////////////////////////////////////////
//
// LU_LIN232_PARITY.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_gen2;

public class LU_LIN232_PARITY extends com.altova.xml.TypeBase
{

	public static com.altova.xml.meta.SimpleType getStaticInfo() { return new com.altova.xml.meta.SimpleType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_gen2_altova_LU_LIN232_PARITY]); }
	public static final int ELU_LIN232_PARITY_NONE = 0; // LU_LIN232_PARITY_NONE
	public static final int ELU_LIN232_PARITY_EVEN = 1; // LU_LIN232_PARITY_EVEN
	public static final int ELU_LIN232_PARITY_ODD = 2; // LU_LIN232_PARITY_ODD
	public static final int Invalid = -1; // Invalid enum value
	
	public LU_LIN232_PARITY(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

	}
	// Attributes


	// Elements
}
