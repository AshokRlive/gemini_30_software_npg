////////////////////////////////////////////////////////////////////////
//
// ControlLogicT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_clogic;

public class ControlLogicT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_clogic_altova_ControlLogicT]); }
	
	public ControlLogicT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{
		group = new MemberAttribute_group (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._group]);
		name = new MemberAttribute_name (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._name]);
		logicType = new MemberAttribute_logicType (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._logicType]);
		enabled = new MemberAttribute_enabled (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._enabled]);
		uuid = new MemberAttribute_uuid (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._uuid]);

		binaryPoints= new MemberElement_binaryPoints (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._binaryPoints]);
		dbinaryPoints= new MemberElement_dbinaryPoints (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._dbinaryPoints]);
		analoguePoints= new MemberElement_analoguePoints (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._analoguePoints]);
		counterPoints= new MemberElement_counterPoints (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._counterPoints]);
		inputs= new MemberElement_inputs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._inputs]);
		outputs= new MemberElement_outputs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._outputs]);
		parameters= new MemberElement_parameters (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ControlLogicT._parameters]);
	}
	// Attributes
	public MemberAttribute_group group;
		public static class MemberAttribute_group
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_group (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_name name;
		public static class MemberAttribute_name
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_name (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_logicType logicType;
		public static class MemberAttribute_logicType
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_logicType (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_enabled enabled;
		public static class MemberAttribute_enabled
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_enabled (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public boolean getValue() {
				return (boolean)com.altova.xml.XmlTreeOperations.castToBool(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(boolean value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_uuid uuid;
		public static class MemberAttribute_uuid
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_uuid (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}


	// Elements
	
	public MemberElement_binaryPoints binaryPoints;

		public static class MemberElement_binaryPoints
		{
			public static class MemberElement_binaryPoints_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_binaryPoints member;
				public MemberElement_binaryPoints_Iterator(MemberElement_binaryPoints member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicBinaryPointT nx = new com.g3schema.ns_clogic.CLogicBinaryPointT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_binaryPoints (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicBinaryPointT at(int index) {return new com.g3schema.ns_clogic.CLogicBinaryPointT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicBinaryPointT first() {return new com.g3schema.ns_clogic.CLogicBinaryPointT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicBinaryPointT last(){return new com.g3schema.ns_clogic.CLogicBinaryPointT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicBinaryPointT append(){return new com.g3schema.ns_clogic.CLogicBinaryPointT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_binaryPoints_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_dbinaryPoints dbinaryPoints;

		public static class MemberElement_dbinaryPoints
		{
			public static class MemberElement_dbinaryPoints_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_dbinaryPoints member;
				public MemberElement_dbinaryPoints_Iterator(MemberElement_dbinaryPoints member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicDoubleBinaryPointT nx = new com.g3schema.ns_clogic.CLogicDoubleBinaryPointT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_dbinaryPoints (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicDoubleBinaryPointT at(int index) {return new com.g3schema.ns_clogic.CLogicDoubleBinaryPointT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicDoubleBinaryPointT first() {return new com.g3schema.ns_clogic.CLogicDoubleBinaryPointT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicDoubleBinaryPointT last(){return new com.g3schema.ns_clogic.CLogicDoubleBinaryPointT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicDoubleBinaryPointT append(){return new com.g3schema.ns_clogic.CLogicDoubleBinaryPointT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_dbinaryPoints_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_analoguePoints analoguePoints;

		public static class MemberElement_analoguePoints
		{
			public static class MemberElement_analoguePoints_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_analoguePoints member;
				public MemberElement_analoguePoints_Iterator(MemberElement_analoguePoints member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicAnalogPointT nx = new com.g3schema.ns_clogic.CLogicAnalogPointT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_analoguePoints (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicAnalogPointT at(int index) {return new com.g3schema.ns_clogic.CLogicAnalogPointT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicAnalogPointT first() {return new com.g3schema.ns_clogic.CLogicAnalogPointT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicAnalogPointT last(){return new com.g3schema.ns_clogic.CLogicAnalogPointT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicAnalogPointT append(){return new com.g3schema.ns_clogic.CLogicAnalogPointT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_analoguePoints_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_counterPoints counterPoints;

		public static class MemberElement_counterPoints
		{
			public static class MemberElement_counterPoints_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_counterPoints member;
				public MemberElement_counterPoints_Iterator(MemberElement_counterPoints member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicCounterPointT nx = new com.g3schema.ns_clogic.CLogicCounterPointT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_counterPoints (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicCounterPointT at(int index) {return new com.g3schema.ns_clogic.CLogicCounterPointT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicCounterPointT first() {return new com.g3schema.ns_clogic.CLogicCounterPointT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicCounterPointT last(){return new com.g3schema.ns_clogic.CLogicCounterPointT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicCounterPointT append(){return new com.g3schema.ns_clogic.CLogicCounterPointT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_counterPoints_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_inputs inputs;

		public static class MemberElement_inputs
		{
			public static class MemberElement_inputs_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_inputs member;
				public MemberElement_inputs_Iterator(MemberElement_inputs member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicInputsT nx = new com.g3schema.ns_clogic.CLogicInputsT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_inputs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicInputsT at(int index) {return new com.g3schema.ns_clogic.CLogicInputsT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicInputsT first() {return new com.g3schema.ns_clogic.CLogicInputsT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicInputsT last(){return new com.g3schema.ns_clogic.CLogicInputsT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicInputsT append(){return new com.g3schema.ns_clogic.CLogicInputsT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_inputs_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_outputs outputs;

		public static class MemberElement_outputs
		{
			public static class MemberElement_outputs_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_outputs member;
				public MemberElement_outputs_Iterator(MemberElement_outputs member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicOutputsT nx = new com.g3schema.ns_clogic.CLogicOutputsT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_outputs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicOutputsT at(int index) {return new com.g3schema.ns_clogic.CLogicOutputsT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicOutputsT first() {return new com.g3schema.ns_clogic.CLogicOutputsT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicOutputsT last(){return new com.g3schema.ns_clogic.CLogicOutputsT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicOutputsT append(){return new com.g3schema.ns_clogic.CLogicOutputsT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_outputs_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_parameters parameters;

		public static class MemberElement_parameters
		{
			public static class MemberElement_parameters_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_parameters member;
				public MemberElement_parameters_Iterator(MemberElement_parameters member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.CLogicParametersT nx = new com.g3schema.ns_clogic.CLogicParametersT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_parameters (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.CLogicParametersT at(int index) {return new com.g3schema.ns_clogic.CLogicParametersT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.CLogicParametersT first() {return new com.g3schema.ns_clogic.CLogicParametersT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.CLogicParametersT last(){return new com.g3schema.ns_clogic.CLogicParametersT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.CLogicParametersT append(){return new com.g3schema.ns_clogic.CLogicParametersT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_parameters_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.controllogic", "ControlLogicT");}
}
