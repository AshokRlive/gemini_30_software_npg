////////////////////////////////////////////////////////////////////////
//
// PortsT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_port;

public class PortsT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_port_altova_PortsT]); }
	
	public PortsT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		ethernet= new MemberElement_ethernet (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_port_altova_PortsT._ethernet]);
		serial= new MemberElement_serial (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_port_altova_PortsT._serial]);
	}
	// Attributes


	// Elements
	
	public MemberElement_ethernet ethernet;

		public static class MemberElement_ethernet
		{
			public static class MemberElement_ethernet_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_ethernet member;
				public MemberElement_ethernet_Iterator(MemberElement_ethernet member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_port.EthernetPortConfT nx = new com.g3schema.ns_port.EthernetPortConfT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_ethernet (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_port.EthernetPortConfT at(int index) {return new com.g3schema.ns_port.EthernetPortConfT(owner.getElementAt(info, index));}
			public com.g3schema.ns_port.EthernetPortConfT first() {return new com.g3schema.ns_port.EthernetPortConfT(owner.getElementFirst(info));}
			public com.g3schema.ns_port.EthernetPortConfT last(){return new com.g3schema.ns_port.EthernetPortConfT(owner.getElementLast(info));}
			public com.g3schema.ns_port.EthernetPortConfT append(){return new com.g3schema.ns_port.EthernetPortConfT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_ethernet_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_serial serial;

		public static class MemberElement_serial
		{
			public static class MemberElement_serial_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_serial member;
				public MemberElement_serial_Iterator(MemberElement_serial member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_port.SerialPortConfT nx = new com.g3schema.ns_port.SerialPortConfT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_serial (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_port.SerialPortConfT at(int index) {return new com.g3schema.ns_port.SerialPortConfT(owner.getElementAt(info, index));}
			public com.g3schema.ns_port.SerialPortConfT first() {return new com.g3schema.ns_port.SerialPortConfT(owner.getElementFirst(info));}
			public com.g3schema.ns_port.SerialPortConfT last(){return new com.g3schema.ns_port.SerialPortConfT(owner.getElementLast(info));}
			public com.g3schema.ns_port.SerialPortConfT append(){return new com.g3schema.ns_port.SerialPortConfT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_serial_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.ports", "PortsT");}
}
