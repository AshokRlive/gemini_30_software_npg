////////////////////////////////////////////////////////////////////////
//
// CustomLabelsT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_vpoint;

public class CustomLabelsT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_vpoint_altova_CustomLabelsT]); }
	
	public CustomLabelsT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		labelSet= new MemberElement_labelSet (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_vpoint_altova_CustomLabelsT._labelSet]);
	}
	// Attributes


	// Elements
	
	public MemberElement_labelSet labelSet;

		public static class MemberElement_labelSet
		{
			public static class MemberElement_labelSet_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_labelSet member;
				public MemberElement_labelSet_Iterator(MemberElement_labelSet member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_vpoint.CustomLabelSetT nx = new com.g3schema.ns_vpoint.CustomLabelSetT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_labelSet (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_vpoint.CustomLabelSetT at(int index) {return new com.g3schema.ns_vpoint.CustomLabelSetT(owner.getElementAt(info, index));}
			public com.g3schema.ns_vpoint.CustomLabelSetT first() {return new com.g3schema.ns_vpoint.CustomLabelSetT(owner.getElementFirst(info));}
			public com.g3schema.ns_vpoint.CustomLabelSetT last(){return new com.g3schema.ns_vpoint.CustomLabelSetT(owner.getElementLast(info));}
			public com.g3schema.ns_vpoint.CustomLabelSetT append(){return new com.g3schema.ns_vpoint.CustomLabelSetT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_labelSet_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.virtualpoint", "CustomLabelsT");}
}
