////////////////////////////////////////////////////////////////////////
//
// ActionParamT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_clogic;

public class ActionParamT extends com.g3schema.ns_clogic.CLogicParametersBaseT
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_clogic_altova_ActionParamT]); }
	
	public ActionParamT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{
		delay = new MemberAttribute_delay (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ActionParamT._delay]);

		inputSetting= new MemberElement_inputSetting (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_clogic_altova_ActionParamT._inputSetting]);
	}
	// Attributes
	public MemberAttribute_delay delay;
		public static class MemberAttribute_delay
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_delay (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}


	// Elements
	
	public MemberElement_inputSetting inputSetting;

		public static class MemberElement_inputSetting
		{
			public static class MemberElement_inputSetting_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_inputSetting member;
				public MemberElement_inputSetting_Iterator(MemberElement_inputSetting member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_clogic.ActionInputSettingT nx = new com.g3schema.ns_clogic.ActionInputSettingT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_inputSetting (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_clogic.ActionInputSettingT at(int index) {return new com.g3schema.ns_clogic.ActionInputSettingT(owner.getElementAt(info, index));}
			public com.g3schema.ns_clogic.ActionInputSettingT first() {return new com.g3schema.ns_clogic.ActionInputSettingT(owner.getElementFirst(info));}
			public com.g3schema.ns_clogic.ActionInputSettingT last(){return new com.g3schema.ns_clogic.ActionInputSettingT(owner.getElementLast(info));}
			public com.g3schema.ns_clogic.ActionInputSettingT append(){return new com.g3schema.ns_clogic.ActionInputSettingT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_inputSetting_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.controllogic", "ActionParamT");}
}
