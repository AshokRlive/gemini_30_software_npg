////////////////////////////////////////////////////////////////////////
//
// FanSettingT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_g3module;

public class FanSettingT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_g3module_altova_FanSettingT]); }
	
	public FanSettingT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{
		fanFitted = new MemberAttribute_fanFitted (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._fanFitted]);
		fanTempThreshold = new MemberAttribute_fanTempThreshold (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._fanTempThreshold]);
		fanTempHysteresis = new MemberAttribute_fanTempHysteresis (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._fanTempHysteresis]);
		faultHysteresis = new MemberAttribute_faultHysteresis (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._faultHysteresis]);
		fanSpdSensorFitted = new MemberAttribute_fanSpdSensorFitted (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._fanSpdSensorFitted]);
		fanOutAsLED = new MemberAttribute_fanOutAsLED (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._fanOutAsLED]);
		fanOutAsDO = new MemberAttribute_fanOutAsDO (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_FanSettingT._fanOutAsDO]);

	}
	// Attributes
	public MemberAttribute_fanFitted fanFitted;
		public static class MemberAttribute_fanFitted
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_fanFitted (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public boolean getValue() {
				return (boolean)com.altova.xml.XmlTreeOperations.castToBool(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(boolean value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_fanTempThreshold fanTempThreshold;
		public static class MemberAttribute_fanTempThreshold
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_fanTempThreshold (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public int getValue() {
				return (int)com.altova.xml.XmlTreeOperations.castToInt(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(int value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_fanTempHysteresis fanTempHysteresis;
		public static class MemberAttribute_fanTempHysteresis
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_fanTempHysteresis (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public int getValue() {
				return (int)com.altova.xml.XmlTreeOperations.castToInt(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(int value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_faultHysteresis faultHysteresis;
		public static class MemberAttribute_faultHysteresis
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_faultHysteresis (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public int getValue() {
				return (int)com.altova.xml.XmlTreeOperations.castToInt(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(int value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_fanSpdSensorFitted fanSpdSensorFitted;
		public static class MemberAttribute_fanSpdSensorFitted
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_fanSpdSensorFitted (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public boolean getValue() {
				return (boolean)com.altova.xml.XmlTreeOperations.castToBool(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(boolean value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_fanOutAsLED fanOutAsLED;
		public static class MemberAttribute_fanOutAsLED
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_fanOutAsLED (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public boolean getValue() {
				return (boolean)com.altova.xml.XmlTreeOperations.castToBool(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(boolean value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_fanOutAsDO fanOutAsDO;
		public static class MemberAttribute_fanOutAsDO
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_fanOutAsDO (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public boolean getValue() {
				return (boolean)com.altova.xml.XmlTreeOperations.castToBool(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(boolean value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}


	// Elements

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuraiton.modules", "FanSettingT");}
}
