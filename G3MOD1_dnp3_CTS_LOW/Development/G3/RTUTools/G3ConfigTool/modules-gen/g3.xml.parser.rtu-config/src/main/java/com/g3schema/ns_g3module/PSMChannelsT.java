////////////////////////////////////////////////////////////////////////
//
// PSMChannelsT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_g3module;

public class PSMChannelsT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_g3module_altova_PSMChannelsT]); }
	
	public PSMChannelsT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		digitalInput= new MemberElement_digitalInput (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_PSMChannelsT._digitalInput]);
		analogueInput= new MemberElement_analogueInput (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_PSMChannelsT._analogueInput]);
		digitalOutput= new MemberElement_digitalOutput (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_PSMChannelsT._digitalOutput]);
		switchOut= new MemberElement_switchOut (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_PSMChannelsT._switchOut]);
		fpi= new MemberElement_fpi (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_g3module_altova_PSMChannelsT._fpi]);
	}
	// Attributes


	// Elements
	
	public MemberElement_digitalInput digitalInput;

		public static class MemberElement_digitalInput
		{
			public static class MemberElement_digitalInput_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_digitalInput member;
				public MemberElement_digitalInput_Iterator(MemberElement_digitalInput member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_g3module.DigitalInputChnlT nx = new com.g3schema.ns_g3module.DigitalInputChnlT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_digitalInput (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_g3module.DigitalInputChnlT at(int index) {return new com.g3schema.ns_g3module.DigitalInputChnlT(owner.getElementAt(info, index));}
			public com.g3schema.ns_g3module.DigitalInputChnlT first() {return new com.g3schema.ns_g3module.DigitalInputChnlT(owner.getElementFirst(info));}
			public com.g3schema.ns_g3module.DigitalInputChnlT last(){return new com.g3schema.ns_g3module.DigitalInputChnlT(owner.getElementLast(info));}
			public com.g3schema.ns_g3module.DigitalInputChnlT append(){return new com.g3schema.ns_g3module.DigitalInputChnlT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_digitalInput_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_analogueInput analogueInput;

		public static class MemberElement_analogueInput
		{
			public static class MemberElement_analogueInput_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_analogueInput member;
				public MemberElement_analogueInput_Iterator(MemberElement_analogueInput member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_g3module.AnalogueInputChnlT nx = new com.g3schema.ns_g3module.AnalogueInputChnlT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_analogueInput (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_g3module.AnalogueInputChnlT at(int index) {return new com.g3schema.ns_g3module.AnalogueInputChnlT(owner.getElementAt(info, index));}
			public com.g3schema.ns_g3module.AnalogueInputChnlT first() {return new com.g3schema.ns_g3module.AnalogueInputChnlT(owner.getElementFirst(info));}
			public com.g3schema.ns_g3module.AnalogueInputChnlT last(){return new com.g3schema.ns_g3module.AnalogueInputChnlT(owner.getElementLast(info));}
			public com.g3schema.ns_g3module.AnalogueInputChnlT append(){return new com.g3schema.ns_g3module.AnalogueInputChnlT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_analogueInput_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_digitalOutput digitalOutput;

		public static class MemberElement_digitalOutput
		{
			public static class MemberElement_digitalOutput_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_digitalOutput member;
				public MemberElement_digitalOutput_Iterator(MemberElement_digitalOutput member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_g3module.DigitalOutputChnlT nx = new com.g3schema.ns_g3module.DigitalOutputChnlT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_digitalOutput (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_g3module.DigitalOutputChnlT at(int index) {return new com.g3schema.ns_g3module.DigitalOutputChnlT(owner.getElementAt(info, index));}
			public com.g3schema.ns_g3module.DigitalOutputChnlT first() {return new com.g3schema.ns_g3module.DigitalOutputChnlT(owner.getElementFirst(info));}
			public com.g3schema.ns_g3module.DigitalOutputChnlT last(){return new com.g3schema.ns_g3module.DigitalOutputChnlT(owner.getElementLast(info));}
			public com.g3schema.ns_g3module.DigitalOutputChnlT append(){return new com.g3schema.ns_g3module.DigitalOutputChnlT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_digitalOutput_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_switchOut switchOut;

		public static class MemberElement_switchOut
		{
			public static class MemberElement_switchOut_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_switchOut member;
				public MemberElement_switchOut_Iterator(MemberElement_switchOut member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_g3module.SwitchOutChnlT nx = new com.g3schema.ns_g3module.SwitchOutChnlT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_switchOut (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_g3module.SwitchOutChnlT at(int index) {return new com.g3schema.ns_g3module.SwitchOutChnlT(owner.getElementAt(info, index));}
			public com.g3schema.ns_g3module.SwitchOutChnlT first() {return new com.g3schema.ns_g3module.SwitchOutChnlT(owner.getElementFirst(info));}
			public com.g3schema.ns_g3module.SwitchOutChnlT last(){return new com.g3schema.ns_g3module.SwitchOutChnlT(owner.getElementLast(info));}
			public com.g3schema.ns_g3module.SwitchOutChnlT append(){return new com.g3schema.ns_g3module.SwitchOutChnlT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_switchOut_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_fpi fpi;

		public static class MemberElement_fpi
		{
			public static class MemberElement_fpi_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_fpi member;
				public MemberElement_fpi_Iterator(MemberElement_fpi member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_g3module.FPIChannelT nx = new com.g3schema.ns_g3module.FPIChannelT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_fpi (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_g3module.FPIChannelT at(int index) {return new com.g3schema.ns_g3module.FPIChannelT(owner.getElementAt(info, index));}
			public com.g3schema.ns_g3module.FPIChannelT first() {return new com.g3schema.ns_g3module.FPIChannelT(owner.getElementFirst(info));}
			public com.g3schema.ns_g3module.FPIChannelT last(){return new com.g3schema.ns_g3module.FPIChannelT(owner.getElementLast(info));}
			public com.g3schema.ns_g3module.FPIChannelT append(){return new com.g3schema.ns_g3module.FPIChannelT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_fpi_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuraiton.modules", "PSMChannelsT");}
}
