////////////////////////////////////////////////////////////////////////
//
// PAKNETModemT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_comms;

public class PAKNETModemT extends com.g3schema.ns_comms.BaseModemT
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_comms_altova_PAKNETModemT]); }
	
	public PAKNETModemT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{
		promptString = new MemberAttribute_promptString (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._promptString]);
		dialString = new MemberAttribute_dialString (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._dialString]);
		dialString2 = new MemberAttribute_dialString2 (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._dialString2]);
		regularCallIntervalMins = new MemberAttribute_regularCallIntervalMins (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._regularCallIntervalMins]);
		modemInactivityTimeoutMins = new MemberAttribute_modemInactivityTimeoutMins (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._modemInactivityTimeoutMins]);
		connectionInactivityTimeoutSecs = new MemberAttribute_connectionInactivityTimeoutSecs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._connectionInactivityTimeoutSecs]);
		connectTimeoutMs = new MemberAttribute_connectTimeoutMs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._connectTimeoutMs]);
		mainNUA = new MemberAttribute_mainNUA (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._mainNUA]);
		standyNUA = new MemberAttribute_standyNUA (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._standyNUA]);

		dialRetryIntervalSecs= new MemberElement_dialRetryIntervalSecs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_PAKNETModemT._dialRetryIntervalSecs]);
	}
	// Attributes
	public MemberAttribute_promptString promptString;
		public static class MemberAttribute_promptString
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_promptString (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_dialString dialString;
		public static class MemberAttribute_dialString
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_dialString (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_dialString2 dialString2;
		public static class MemberAttribute_dialString2
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_dialString2 (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_regularCallIntervalMins regularCallIntervalMins;
		public static class MemberAttribute_regularCallIntervalMins
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_regularCallIntervalMins (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_modemInactivityTimeoutMins modemInactivityTimeoutMins;
		public static class MemberAttribute_modemInactivityTimeoutMins
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_modemInactivityTimeoutMins (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_connectionInactivityTimeoutSecs connectionInactivityTimeoutSecs;
		public static class MemberAttribute_connectionInactivityTimeoutSecs
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_connectionInactivityTimeoutSecs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_connectTimeoutMs connectTimeoutMs;
		public static class MemberAttribute_connectTimeoutMs
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_connectTimeoutMs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_mainNUA mainNUA;
		public static class MemberAttribute_mainNUA
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_mainNUA (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_standyNUA standyNUA;
		public static class MemberAttribute_standyNUA
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_standyNUA (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}


	// Elements
	
	public MemberElement_dialRetryIntervalSecs dialRetryIntervalSecs;

		public static class MemberElement_dialRetryIntervalSecs
		{
			public static class MemberElement_dialRetryIntervalSecs_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_dialRetryIntervalSecs member;
				public MemberElement_dialRetryIntervalSecs_Iterator(MemberElement_dialRetryIntervalSecs member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.xs.unsignedIntType nx = new com.g3schema.xs.unsignedIntType(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_dialRetryIntervalSecs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.xs.unsignedIntType at(int index) {return new com.g3schema.xs.unsignedIntType(owner.getElementAt(info, index));}
			public com.g3schema.xs.unsignedIntType first() {return new com.g3schema.xs.unsignedIntType(owner.getElementFirst(info));}
			public com.g3schema.xs.unsignedIntType last(){return new com.g3schema.xs.unsignedIntType(owner.getElementLast(info));}
			public com.g3schema.xs.unsignedIntType append(){return new com.g3schema.xs.unsignedIntType(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_dialRetryIntervalSecs_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.commsdevice", "PAKNETModemT");}
}
