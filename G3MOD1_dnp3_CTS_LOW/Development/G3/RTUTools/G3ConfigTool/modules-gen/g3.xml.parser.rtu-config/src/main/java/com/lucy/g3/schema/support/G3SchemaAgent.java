/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.schema.support;

import com.g3schema.ScadaProtocolT;
import com.g3schema.ns_mmb.MMBIOMapT;
import com.g3schema.ns_mmb.ModbusMasterT;
import com.g3schema.ns_s101.S101SessionT;
import com.g3schema.ns_s104.S104SessionT;
import com.g3schema.ns_sdnp3.SDNP3SesnT;


public class G3SchemaAgent {
  private final com.g3schema.g3schema2 xmlRoot;
  private final ScadaProtocolT xmlScada;
  private ModbusMasterT xmlMMB;

  public G3SchemaAgent() {
    try {
      xmlRoot = com.g3schema.g3schema2.createDocument();
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e.getMessage());
    }
    
    xmlScada = xmlRoot.configuration.append().scadaProtocol.append();
    xmlMMB = xmlRoot.configuration.first().fieldDevices.append().ModbusMaster.append();
  }

  public SDNP3SesnT getSDNP3Session() {
    return xmlScada.sDNP3.append().channel.append().session.append();
  }
  
  public MMBIOMapT getMMBIoMap() {
    return xmlMMB.channel.append().session.append().iomap.append();
  }

  public S101SessionT getS101Session() {
    return xmlScada.sIEC101.append().channel.append().session.append();
  }

  public S104SessionT getS104Session() {
    return xmlScada.sIEC104.append().channel.append().session.append();
  }
}

