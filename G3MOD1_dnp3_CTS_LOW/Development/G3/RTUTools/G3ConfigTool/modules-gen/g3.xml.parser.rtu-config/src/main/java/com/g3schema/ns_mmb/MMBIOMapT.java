////////////////////////////////////////////////////////////////////////
//
// MMBIOMapT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_mmb;

public class MMBIOMapT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_mmb_altova_MMBIOMapT]); }
	
	public MMBIOMapT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		AIChs= new MemberElement_AIChs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_mmb_altova_MMBIOMapT._AIChs]);
		DIChs= new MemberElement_DIChs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_mmb_altova_MMBIOMapT._DIChs]);
		DOChs= new MemberElement_DOChs (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_mmb_altova_MMBIOMapT._DOChs]);
	}
	// Attributes


	// Elements
	
	public MemberElement_AIChs AIChs;

		public static class MemberElement_AIChs
		{
			public static class MemberElement_AIChs_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_AIChs member;
				public MemberElement_AIChs_Iterator(MemberElement_AIChs member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_mmb.MMBDeviceChnlAnalogT nx = new com.g3schema.ns_mmb.MMBDeviceChnlAnalogT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_AIChs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_mmb.MMBDeviceChnlAnalogT at(int index) {return new com.g3schema.ns_mmb.MMBDeviceChnlAnalogT(owner.getElementAt(info, index));}
			public com.g3schema.ns_mmb.MMBDeviceChnlAnalogT first() {return new com.g3schema.ns_mmb.MMBDeviceChnlAnalogT(owner.getElementFirst(info));}
			public com.g3schema.ns_mmb.MMBDeviceChnlAnalogT last(){return new com.g3schema.ns_mmb.MMBDeviceChnlAnalogT(owner.getElementLast(info));}
			public com.g3schema.ns_mmb.MMBDeviceChnlAnalogT append(){return new com.g3schema.ns_mmb.MMBDeviceChnlAnalogT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_AIChs_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_DIChs DIChs;

		public static class MemberElement_DIChs
		{
			public static class MemberElement_DIChs_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_DIChs member;
				public MemberElement_DIChs_Iterator(MemberElement_DIChs member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_mmb.MMBDeviceChnlT nx = new com.g3schema.ns_mmb.MMBDeviceChnlT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_DIChs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_mmb.MMBDeviceChnlT at(int index) {return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.getElementAt(info, index));}
			public com.g3schema.ns_mmb.MMBDeviceChnlT first() {return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.getElementFirst(info));}
			public com.g3schema.ns_mmb.MMBDeviceChnlT last(){return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.getElementLast(info));}
			public com.g3schema.ns_mmb.MMBDeviceChnlT append(){return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_DIChs_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_DOChs DOChs;

		public static class MemberElement_DOChs
		{
			public static class MemberElement_DOChs_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_DOChs member;
				public MemberElement_DOChs_Iterator(MemberElement_DOChs member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_mmb.MMBDeviceChnlT nx = new com.g3schema.ns_mmb.MMBDeviceChnlT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_DOChs (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_mmb.MMBDeviceChnlT at(int index) {return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.getElementAt(info, index));}
			public com.g3schema.ns_mmb.MMBDeviceChnlT first() {return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.getElementFirst(info));}
			public com.g3schema.ns_mmb.MMBDeviceChnlT last(){return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.getElementLast(info));}
			public com.g3schema.ns_mmb.MMBDeviceChnlT append(){return new com.g3schema.ns_mmb.MMBDeviceChnlT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_DOChs_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "protocolstack.modbus.master", "MMBIOMapT");}
}
