/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.schema.support;

/**
 *
 *
 */
public class RTUConfigSchemaResource {

  /** The relative path of G3 Configuration schema*/
  final static public String FILE_PATH = "schema/RTU Configuration/g3schema.xsd";
}
