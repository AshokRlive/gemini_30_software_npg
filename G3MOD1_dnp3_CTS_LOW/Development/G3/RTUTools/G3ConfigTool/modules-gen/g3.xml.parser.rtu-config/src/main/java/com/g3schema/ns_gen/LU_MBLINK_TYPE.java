////////////////////////////////////////////////////////////////////////
//
// LU_MBLINK_TYPE.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_gen;

public class LU_MBLINK_TYPE extends com.altova.xml.TypeBase
{

	public static com.altova.xml.meta.SimpleType getStaticInfo() { return new com.altova.xml.meta.SimpleType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_gen_altova_LU_MBLINK_TYPE]); }
	public static final int ELU_MBLINK_TYPE_TCP = 0; // LU_MBLINK_TYPE_TCP
	public static final int ELU_MBLINK_TYPE_ASCII = 1; // LU_MBLINK_TYPE_ASCII
	public static final int ELU_MBLINK_TYPE_RTU = 2; // LU_MBLINK_TYPE_RTU
	public static final int Invalid = -1; // Invalid enum value
	
	public LU_MBLINK_TYPE(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

	}
	// Attributes


	// Elements
}
