////////////////////////////////////////////////////////////////////////
//
// AuthenticationT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_sdnp3;

public class AuthenticationT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_sdnp3_altova_AuthenticationT]); }
	
	public AuthenticationT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{
		authenticationEnabled = new MemberAttribute_authenticationEnabled (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._authenticationEnabled]);
		authSecStatMaxEvents = new MemberAttribute_authSecStatMaxEvents (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._authSecStatMaxEvents]);
		authSecStatEventMode = new MemberAttribute_authSecStatEventMode (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._authSecStatEventMode]);

		authConfig= new MemberElement_authConfig (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._authConfig]);
		authConfigV2= new MemberElement_authConfigV2 (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._authConfigV2]);
		authConfigV5= new MemberElement_authConfigV5 (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._authConfigV5]);
		users= new MemberElement_users (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_sdnp3_altova_AuthenticationT._users]);
	}
	// Attributes
	public MemberAttribute_authenticationEnabled authenticationEnabled;
		public static class MemberAttribute_authenticationEnabled
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_authenticationEnabled (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public boolean getValue() {
				return (boolean)com.altova.xml.XmlTreeOperations.castToBool(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(boolean value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_authSecStatMaxEvents authSecStatMaxEvents;
		public static class MemberAttribute_authSecStatMaxEvents
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_authSecStatMaxEvents (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_authSecStatEventMode authSecStatEventMode;
		public static class MemberAttribute_authSecStatEventMode
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_authSecStatEventMode (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public String getValue() {
				return (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(String value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

			public int getEnumerationValue()
			{
				return com.altova.xml.TypeBase.getEnumerationIndex(info, (String)com.altova.xml.XmlTreeOperations.castToString(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info), 0, 4);
			}
			
			public void setEnumerationValue( int index) throws IndexOutOfBoundsException
			{
				if( index >= 0 && index < 4) 
					com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, info.getDataType().facets[index + 0].stringValue);
				else
					throw new IndexOutOfBoundsException();
			}

		}


	// Elements
	
	public MemberElement_authConfig authConfig;

		public static class MemberElement_authConfig
		{
			public static class MemberElement_authConfig_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_authConfig member;
				public MemberElement_authConfig_Iterator(MemberElement_authConfig member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_sdnp3.AuthConfigT nx = new com.g3schema.ns_sdnp3.AuthConfigT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_authConfig (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_sdnp3.AuthConfigT at(int index) {return new com.g3schema.ns_sdnp3.AuthConfigT(owner.getElementAt(info, index));}
			public com.g3schema.ns_sdnp3.AuthConfigT first() {return new com.g3schema.ns_sdnp3.AuthConfigT(owner.getElementFirst(info));}
			public com.g3schema.ns_sdnp3.AuthConfigT last(){return new com.g3schema.ns_sdnp3.AuthConfigT(owner.getElementLast(info));}
			public com.g3schema.ns_sdnp3.AuthConfigT append(){return new com.g3schema.ns_sdnp3.AuthConfigT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_authConfig_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_authConfigV2 authConfigV2;

		public static class MemberElement_authConfigV2
		{
			public static class MemberElement_authConfigV2_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_authConfigV2 member;
				public MemberElement_authConfigV2_Iterator(MemberElement_authConfigV2 member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_sdnp3.AuthConfigV2T nx = new com.g3schema.ns_sdnp3.AuthConfigV2T(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_authConfigV2 (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_sdnp3.AuthConfigV2T at(int index) {return new com.g3schema.ns_sdnp3.AuthConfigV2T(owner.getElementAt(info, index));}
			public com.g3schema.ns_sdnp3.AuthConfigV2T first() {return new com.g3schema.ns_sdnp3.AuthConfigV2T(owner.getElementFirst(info));}
			public com.g3schema.ns_sdnp3.AuthConfigV2T last(){return new com.g3schema.ns_sdnp3.AuthConfigV2T(owner.getElementLast(info));}
			public com.g3schema.ns_sdnp3.AuthConfigV2T append(){return new com.g3schema.ns_sdnp3.AuthConfigV2T(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_authConfigV2_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_authConfigV5 authConfigV5;

		public static class MemberElement_authConfigV5
		{
			public static class MemberElement_authConfigV5_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_authConfigV5 member;
				public MemberElement_authConfigV5_Iterator(MemberElement_authConfigV5 member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_sdnp3.AuthConfigV5T nx = new com.g3schema.ns_sdnp3.AuthConfigV5T(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_authConfigV5 (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_sdnp3.AuthConfigV5T at(int index) {return new com.g3schema.ns_sdnp3.AuthConfigV5T(owner.getElementAt(info, index));}
			public com.g3schema.ns_sdnp3.AuthConfigV5T first() {return new com.g3schema.ns_sdnp3.AuthConfigV5T(owner.getElementFirst(info));}
			public com.g3schema.ns_sdnp3.AuthConfigV5T last(){return new com.g3schema.ns_sdnp3.AuthConfigV5T(owner.getElementLast(info));}
			public com.g3schema.ns_sdnp3.AuthConfigV5T append(){return new com.g3schema.ns_sdnp3.AuthConfigV5T(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_authConfigV5_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_users users;

		public static class MemberElement_users
		{
			public static class MemberElement_users_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_users member;
				public MemberElement_users_Iterator(MemberElement_users member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_sdnp3.AuthUsersT nx = new com.g3schema.ns_sdnp3.AuthUsersT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_users (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_sdnp3.AuthUsersT at(int index) {return new com.g3schema.ns_sdnp3.AuthUsersT(owner.getElementAt(info, index));}
			public com.g3schema.ns_sdnp3.AuthUsersT first() {return new com.g3schema.ns_sdnp3.AuthUsersT(owner.getElementFirst(info));}
			public com.g3schema.ns_sdnp3.AuthUsersT last(){return new com.g3schema.ns_sdnp3.AuthUsersT(owner.getElementLast(info));}
			public com.g3schema.ns_sdnp3.AuthUsersT append(){return new com.g3schema.ns_sdnp3.AuthUsersT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_users_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "protocolstack.dnp3.slave", "AuthenticationT");}
}
