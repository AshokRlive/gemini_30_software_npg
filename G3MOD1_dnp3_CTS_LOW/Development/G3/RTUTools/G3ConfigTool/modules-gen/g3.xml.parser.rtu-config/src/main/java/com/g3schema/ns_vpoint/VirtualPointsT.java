////////////////////////////////////////////////////////////////////////
//
// VirtualPointsT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_vpoint;

public class VirtualPointsT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_vpoint_altova_VirtualPointsT]); }
	
	public VirtualPointsT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		analogues= new MemberElement_analogues (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_vpoint_altova_VirtualPointsT._analogues]);
		counters= new MemberElement_counters (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_vpoint_altova_VirtualPointsT._counters]);
		doubleBinaries= new MemberElement_doubleBinaries (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_vpoint_altova_VirtualPointsT._doubleBinaries]);
		binaries= new MemberElement_binaries (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_vpoint_altova_VirtualPointsT._binaries]);
		customLabels= new MemberElement_customLabels (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_vpoint_altova_VirtualPointsT._customLabels]);
	}
	// Attributes


	// Elements
	
	public MemberElement_analogues analogues;

		public static class MemberElement_analogues
		{
			public static class MemberElement_analogues_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_analogues member;
				public MemberElement_analogues_Iterator(MemberElement_analogues member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_vpoint.AnaloguesT nx = new com.g3schema.ns_vpoint.AnaloguesT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_analogues (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_vpoint.AnaloguesT at(int index) {return new com.g3schema.ns_vpoint.AnaloguesT(owner.getElementAt(info, index));}
			public com.g3schema.ns_vpoint.AnaloguesT first() {return new com.g3schema.ns_vpoint.AnaloguesT(owner.getElementFirst(info));}
			public com.g3schema.ns_vpoint.AnaloguesT last(){return new com.g3schema.ns_vpoint.AnaloguesT(owner.getElementLast(info));}
			public com.g3schema.ns_vpoint.AnaloguesT append(){return new com.g3schema.ns_vpoint.AnaloguesT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_analogues_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_counters counters;

		public static class MemberElement_counters
		{
			public static class MemberElement_counters_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_counters member;
				public MemberElement_counters_Iterator(MemberElement_counters member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_vpoint.CountersT nx = new com.g3schema.ns_vpoint.CountersT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_counters (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_vpoint.CountersT at(int index) {return new com.g3schema.ns_vpoint.CountersT(owner.getElementAt(info, index));}
			public com.g3schema.ns_vpoint.CountersT first() {return new com.g3schema.ns_vpoint.CountersT(owner.getElementFirst(info));}
			public com.g3schema.ns_vpoint.CountersT last(){return new com.g3schema.ns_vpoint.CountersT(owner.getElementLast(info));}
			public com.g3schema.ns_vpoint.CountersT append(){return new com.g3schema.ns_vpoint.CountersT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_counters_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_doubleBinaries doubleBinaries;

		public static class MemberElement_doubleBinaries
		{
			public static class MemberElement_doubleBinaries_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_doubleBinaries member;
				public MemberElement_doubleBinaries_Iterator(MemberElement_doubleBinaries member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_vpoint.DoubleBinariesT nx = new com.g3schema.ns_vpoint.DoubleBinariesT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_doubleBinaries (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_vpoint.DoubleBinariesT at(int index) {return new com.g3schema.ns_vpoint.DoubleBinariesT(owner.getElementAt(info, index));}
			public com.g3schema.ns_vpoint.DoubleBinariesT first() {return new com.g3schema.ns_vpoint.DoubleBinariesT(owner.getElementFirst(info));}
			public com.g3schema.ns_vpoint.DoubleBinariesT last(){return new com.g3schema.ns_vpoint.DoubleBinariesT(owner.getElementLast(info));}
			public com.g3schema.ns_vpoint.DoubleBinariesT append(){return new com.g3schema.ns_vpoint.DoubleBinariesT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_doubleBinaries_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_binaries binaries;

		public static class MemberElement_binaries
		{
			public static class MemberElement_binaries_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_binaries member;
				public MemberElement_binaries_Iterator(MemberElement_binaries member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_vpoint.BinariesT nx = new com.g3schema.ns_vpoint.BinariesT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_binaries (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_vpoint.BinariesT at(int index) {return new com.g3schema.ns_vpoint.BinariesT(owner.getElementAt(info, index));}
			public com.g3schema.ns_vpoint.BinariesT first() {return new com.g3schema.ns_vpoint.BinariesT(owner.getElementFirst(info));}
			public com.g3schema.ns_vpoint.BinariesT last(){return new com.g3schema.ns_vpoint.BinariesT(owner.getElementLast(info));}
			public com.g3schema.ns_vpoint.BinariesT append(){return new com.g3schema.ns_vpoint.BinariesT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_binaries_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_customLabels customLabels;

		public static class MemberElement_customLabels
		{
			public static class MemberElement_customLabels_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_customLabels member;
				public MemberElement_customLabels_Iterator(MemberElement_customLabels member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_vpoint.CustomLabelsT nx = new com.g3schema.ns_vpoint.CustomLabelsT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_customLabels (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_vpoint.CustomLabelsT at(int index) {return new com.g3schema.ns_vpoint.CustomLabelsT(owner.getElementAt(info, index));}
			public com.g3schema.ns_vpoint.CustomLabelsT first() {return new com.g3schema.ns_vpoint.CustomLabelsT(owner.getElementFirst(info));}
			public com.g3schema.ns_vpoint.CustomLabelsT last(){return new com.g3schema.ns_vpoint.CustomLabelsT(owner.getElementLast(info));}
			public com.g3schema.ns_vpoint.CustomLabelsT append(){return new com.g3schema.ns_vpoint.CustomLabelsT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_customLabels_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.virtualpoint", "VirtualPointsT");}
}
