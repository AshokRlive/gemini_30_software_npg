////////////////////////////////////////////////////////////////////////
//
// CommDevicesT.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.g3schema.ns_comms;

public class CommDevicesT extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.g3schema.g3schema_TypeInfo.binder.getTypes()[com.g3schema.g3schema_TypeInfo._altova_ti_ns_comms_altova_CommDevicesT]); }
	
	public CommDevicesT(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{

		GPRSModem= new MemberElement_GPRSModem (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_CommDevicesT._GPRSModem]);
		PSTNModem= new MemberElement_PSTNModem (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_CommDevicesT._PSTNModem]);
		PAKNETModem= new MemberElement_PAKNETModem (this, com.g3schema.g3schema_TypeInfo.binder.getMembers()[com.g3schema.g3schema_TypeInfo._altova_mi_ns_comms_altova_CommDevicesT._PAKNETModem]);
	}
	// Attributes


	// Elements
	
	public MemberElement_GPRSModem GPRSModem;

		public static class MemberElement_GPRSModem
		{
			public static class MemberElement_GPRSModem_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_GPRSModem member;
				public MemberElement_GPRSModem_Iterator(MemberElement_GPRSModem member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_comms.GPRSModemT nx = new com.g3schema.ns_comms.GPRSModemT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_GPRSModem (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_comms.GPRSModemT at(int index) {return new com.g3schema.ns_comms.GPRSModemT(owner.getElementAt(info, index));}
			public com.g3schema.ns_comms.GPRSModemT first() {return new com.g3schema.ns_comms.GPRSModemT(owner.getElementFirst(info));}
			public com.g3schema.ns_comms.GPRSModemT last(){return new com.g3schema.ns_comms.GPRSModemT(owner.getElementLast(info));}
			public com.g3schema.ns_comms.GPRSModemT append(){return new com.g3schema.ns_comms.GPRSModemT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_GPRSModem_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_PSTNModem PSTNModem;

		public static class MemberElement_PSTNModem
		{
			public static class MemberElement_PSTNModem_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_PSTNModem member;
				public MemberElement_PSTNModem_Iterator(MemberElement_PSTNModem member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_comms.PSTNModemT nx = new com.g3schema.ns_comms.PSTNModemT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_PSTNModem (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_comms.PSTNModemT at(int index) {return new com.g3schema.ns_comms.PSTNModemT(owner.getElementAt(info, index));}
			public com.g3schema.ns_comms.PSTNModemT first() {return new com.g3schema.ns_comms.PSTNModemT(owner.getElementFirst(info));}
			public com.g3schema.ns_comms.PSTNModemT last(){return new com.g3schema.ns_comms.PSTNModemT(owner.getElementLast(info));}
			public com.g3schema.ns_comms.PSTNModemT append(){return new com.g3schema.ns_comms.PSTNModemT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_PSTNModem_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}
	
	public MemberElement_PAKNETModem PAKNETModem;

		public static class MemberElement_PAKNETModem
		{
			public static class MemberElement_PAKNETModem_Iterator implements java.util.Iterator
			{
				private org.w3c.dom.Node nextNode;
				private MemberElement_PAKNETModem member;
				public MemberElement_PAKNETModem_Iterator(MemberElement_PAKNETModem member) {this.member=member; nextNode=member.owner.getElementFirst(member.info);}
				public boolean hasNext() 
				{
					while (nextNode != null)
					{
						if (com.altova.xml.TypeBase.memberEqualsNode(member.info, nextNode))
							return true;
						nextNode = nextNode.getNextSibling();
					}
					return false;
				}
				
				public Object next()
				{
					com.g3schema.ns_comms.PAKNETModemT nx = new com.g3schema.ns_comms.PAKNETModemT(nextNode);
					nextNode = nextNode.getNextSibling();
					return nx;
				}
				
				public void remove () {}
			}
			protected com.altova.xml.TypeBase owner;
			protected com.altova.typeinfo.MemberInfo info;
			public MemberElement_PAKNETModem (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) { this.owner = owner; this.info = info;}
			public com.g3schema.ns_comms.PAKNETModemT at(int index) {return new com.g3schema.ns_comms.PAKNETModemT(owner.getElementAt(info, index));}
			public com.g3schema.ns_comms.PAKNETModemT first() {return new com.g3schema.ns_comms.PAKNETModemT(owner.getElementFirst(info));}
			public com.g3schema.ns_comms.PAKNETModemT last(){return new com.g3schema.ns_comms.PAKNETModemT(owner.getElementLast(info));}
			public com.g3schema.ns_comms.PAKNETModemT append(){return new com.g3schema.ns_comms.PAKNETModemT(owner.createElement(info));}
			public boolean exists() {return count() > 0;}
			public int count() {return owner.countElement(info);}
			public void remove() {owner.removeElement(info);}
			public void removeAt(int index) {owner.removeElementAt(info, index);}
			public java.util.Iterator iterator() {return new MemberElement_PAKNETModem_Iterator(this);}
			public com.altova.xml.meta.Element getInfo() { return new com.altova.xml.meta.Element(info); }
		}

		public void setXsiType() {com.altova.xml.XmlTreeOperations.setAttribute(getNode(), "http://www.w3.org/2001/XMLSchema-instance", "xsi:type", "g3.configuration.commsdevice", "CommDevicesT");}
}
