////////////////////////////////////////////////////////////////////////
//
// SupportedFeatureType.java
//
// This file was generated by XMLSpy 2011r2sp1 Enterprise Edition.
//
// YOU SHOULD NOT MODIFY THIS FILE, BECAUSE IT WILL BE
// OVERWRITTEN WHEN YOU RE-RUN CODE GENERATION.
//
// Refer to the XMLSpy Documentation for further details.
// http://www.altova.com/xmlspy
//
////////////////////////////////////////////////////////////////////////

package com.SDPSchema;

public class SupportedFeatureType extends com.altova.xml.TypeBase
{
	public static com.altova.xml.meta.ComplexType getStaticInfo() { return new com.altova.xml.meta.ComplexType(com.SDPSchema.SDPSchema_TypeInfo.binder.getTypes()[com.SDPSchema.SDPSchema_TypeInfo._altova_ti_altova_SupportedFeatureType]); }
	
	public SupportedFeatureType(org.w3c.dom.Node init)
	{
		super(init);
		instantiateMembers();
	}

	private void instantiateMembers()
	{
		Major = new MemberAttribute_Major (this, com.SDPSchema.SDPSchema_TypeInfo.binder.getMembers()[com.SDPSchema.SDPSchema_TypeInfo._altova_mi_altova_SupportedFeatureType._Major]);
		Minor = new MemberAttribute_Minor (this, com.SDPSchema.SDPSchema_TypeInfo.binder.getMembers()[com.SDPSchema.SDPSchema_TypeInfo._altova_mi_altova_SupportedFeatureType._Minor]);

	}
	// Attributes
	public MemberAttribute_Major Major;
		public static class MemberAttribute_Major
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_Major (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}
	public MemberAttribute_Minor Minor;
		public static class MemberAttribute_Minor
		{
			private com.altova.xml.TypeBase owner;
			private com.altova.typeinfo.MemberInfo info; 
			public MemberAttribute_Minor (com.altova.xml.TypeBase owner, com.altova.typeinfo.MemberInfo info) {this.owner = owner; this.info = info;}
			public long getValue() {
				return (long)com.altova.xml.XmlTreeOperations.castToLong(com.altova.xml.XmlTreeOperations.findAttribute(owner.getNode(), info), info);
			}
			public void setValue(long value) {
				com.altova.xml.XmlTreeOperations.setValue(owner.getNode(), info, value);		
			}
			public boolean exists() {return owner.getAttribute(info) != null;}
			public void remove() {owner.removeAttribute(info);} 
			public com.altova.xml.meta.Attribute getInfo() {return new com.altova.xml.meta.Attribute(info);}

		}


	// Elements
}
