
package com.lucy.g3.automation.scheme.templates;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.Test;

import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutoSchemeTemplate;


public class IEC61131ResTest {
  @Test
  public void testAutoSchemeTemplateFilesExist(){
    AutoSchemeTemplate[] templates = AutoSchemeTemplate.values();
    
    for (int i = 0; i < templates.length; i++) {
      if(templates[i] == AutoSchemeTemplate.TEMPLATE_CUSTOM)
        continue;
      
      String path = String.format(IEC61131Res.AUTOMATION_SCHEME_TEMPLATE_DIR+"%s", templates[i].getDescription());
      InputStream is = IEC61131Res.class.getResourceAsStream(path);
      assertNotNull("Scheme template not found: "+ path, is);
    }
  }
  
}

