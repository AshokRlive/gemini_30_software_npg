
package com.lucy.g3.automation.scheme.templates;


public class IEC61131Res {
  static final String AUTOMATION_SCHEME_TEMPLATE_DIR = "/Automation/templates/";
  
  static public  String getTemplateXmlFilePath(AutomationEnum.AutoSchemeType type) {
    AutomationEnum.AutoSchemeTemplate templateEnum = AutomationEnum.AutoSchemeTemplate.forValue(type.getValue());
    return templateEnum == null ? null : AUTOMATION_SCHEME_TEMPLATE_DIR+templateEnum.getDescription();
  }
  

}

