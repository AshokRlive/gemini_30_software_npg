/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.gen.api;

/**
 * The interface of generated pseudo point enum.
 */
public interface ILogicPointEnum extends IXmlEnum {

  /**
   * Gets the option of creating a point for this channel.
   */
  DefaultCreateOption getDefCreateOption();

  LogicPointType getType();

  Double getDefaultScalingFactor();

  String getUnit();


  enum LogicPointType {
    analogue,
    binary,
    doubleBinary,
    counter,
  }
}
