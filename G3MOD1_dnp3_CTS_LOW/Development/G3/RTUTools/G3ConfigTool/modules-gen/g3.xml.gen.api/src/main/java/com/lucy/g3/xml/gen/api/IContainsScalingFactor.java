/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.gen.api;

import java.util.HashMap;

/**
 * Interface of XML item which contains scaling factor.
 */
interface IContainsScalingFactor {

  /**
   * Try to get the scaling & unit map for this channel.
   *
   * @return hashMap that contains entries of scaling factor and unit.
   *         <code>Null</code> if they are not defined in XML.
   */
  HashMap<Double, String> getAllScaleFactorMap();

  int getDefaultEventRate();

  Double getDefaultScalingFactor();
}
