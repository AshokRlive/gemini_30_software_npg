/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.gen.api;


/**
 * This interface specifies the APIs for channel ENUM, which is supposed to be
 * generated from XML. All methods are defined for retrieving the information in
 * channel ENUM.
 */
public interface IChannelEnum extends IXmlEnum, IContainsScalingFactor {

  int getID();

  /**
   * Gets the channel name text defined in XML.
   * 
   * @return the name of this channel.
   */
  String getName();

  /**
   * Gets the channel group text defined in XML.
   * 
   * @return the group string of this channel.
   */
  String getGroup();

  
  /**
   * Gets the option of creating a point for this channel.
   *
   * @return
   */
  DefaultCreateOption getDefCreateOption();

  /**
   * <p>
   * Gets the suggested default value of a parameter, which could belong to this
   * channel or a point that is connected to this channel.
   * </p>
   * e.g. to get the default filter value of an analogue point, we access its
   * channel and try to get default config via <code>getDefConf("filter")</code>
   * , then convert return value to @link {@link FILTER}.
   *
   * @param key
   * @return
   */
  String getDefConf(String parameterKey);
}
