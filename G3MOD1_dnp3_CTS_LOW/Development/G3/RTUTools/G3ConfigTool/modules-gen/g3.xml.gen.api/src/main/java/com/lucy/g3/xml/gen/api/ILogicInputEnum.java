/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.gen.api;

/**
 * The interface of generated LogicInput enum.
 */
public interface ILogicInputEnum extends IXmlEnum {

  boolean isMandatory();

  LogicInputType getType();


  enum LogicInputType {
    analogue,
    binary,
    doubleBinary,
  }
}
