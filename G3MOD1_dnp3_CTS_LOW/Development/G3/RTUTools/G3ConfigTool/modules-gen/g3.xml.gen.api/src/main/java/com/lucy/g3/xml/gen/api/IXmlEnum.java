/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.xml.gen.api;

/**
 * The interface of the enum generated from XML.
 */
public interface IXmlEnum {

  /**
   * Gets the description of this enum.
   * 
   * @return description text.
   */
  String getDescription();

  /**
   * Gets value of this enum item.
   */
  int getValue();


  /**
   * The <code>DefaultCreateOption</code> is used when generating configuration.
   * It can be used to decide what configuration item(such as virtual point)
   * should be generated.
   */
  enum DefaultCreateOption {
    DoNotCreatePoint,
    CreateVirtualPoint,
    CreateVirtualPointAndScadaPoint,
  }

}
