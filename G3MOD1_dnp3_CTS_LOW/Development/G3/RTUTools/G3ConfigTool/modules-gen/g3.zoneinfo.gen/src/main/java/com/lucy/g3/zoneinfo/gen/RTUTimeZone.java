
package com.lucy.g3.zoneinfo.gen;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.io.IOUtils;

public class RTUTimeZone {

  private static TimeZone[] tzList;


  private RTUTimeZone() {
  }

  public static TimeZone[] getAvailableRTUTimeZone() {
    if (tzList == null) {
      InputStream is = RTUTimeZone.class.getResourceAsStream("/zoneinfo/zoneinfo.list");
      try {
        String result = IOUtils.toString(is, "UTF-8");
        result = result.replaceAll("\\\\{2,2}", "/"); // Replace windows path separator
        result = result.replaceAll("\\[|\\]|\"|\\,",""); // remove symbols
        result = result.replaceAll("^\\s+|\\s+$/m","");//trim lines
        //result = result.replaceAll("^(?:[\t ]*(?:\r?\n|\r))+","");//remove empty lines
        
        String[] zoneinfo = result.split("\\r?\\n");
        
        // Trim line
        tzList = new TimeZone[zoneinfo.length]; 
        for (int i = 0; i < zoneinfo.length; i++) {
          tzList[i] = TimeZone.getTimeZone(zoneinfo[i].trim());
        }
        
        if(tzList.length > 1) {
        // Remove same timezone
        for (int i = 0; i < tzList.length - 1; i++) {
          for (int j = i+1; j < tzList.length; j++) {
            if(tzList[i] != null && tzList[j] != null && tzList[i].hasSameRules(tzList[j]))
              tzList[j] = null;
          }
        }
        }
        
        // Remove null timezone
        List<TimeZone> list = new ArrayList<>(Arrays.asList(tzList));
        list.removeAll(Collections.singleton(null));
        tzList = list.toArray(new TimeZone[list.size()]);
        
      } catch (IOException e) {
        tzList = new TimeZone[0]; 
        e.printStackTrace();
      } finally {
        IOUtils.closeQuietly(is);
      }
    }

    return tzList;
  }
}
