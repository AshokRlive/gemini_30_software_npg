
package com.lucy.g3.zoneinfo.gen;

import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;


public class RTUTimeZoneTest {

  @Test
  public void testGetTimezone() {
    TimeZone[] tz = RTUTimeZone.getAvailableRTUTimeZone();
    Assert.assertTrue("empty tz list", tz.length > 0);
    for (int i = 0; i < tz.length; i++) {
      Assert.assertNotNull(tz[i]);
      System.out.println(tz[i].getID()+":"+tz[i].getDisplayName());
    }
  }

}

