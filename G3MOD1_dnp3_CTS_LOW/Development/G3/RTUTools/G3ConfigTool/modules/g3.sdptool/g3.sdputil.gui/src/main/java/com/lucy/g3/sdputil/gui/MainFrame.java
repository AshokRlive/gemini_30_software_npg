/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.sdp.exporter.SdpExporter;
import com.lucy.g3.sdp.importer.SdpImporter;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.SDPEntryType;
import com.lucy.g3.sdp.model.WritableSDP;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntryFactory;
import com.lucy.g3.sdputil.gui.dialogs.AdditionalFeatureEditorDialog;
import com.lucy.g3.sdputil.support.csv.CSVSupport;

/**
 * The Class MainFrame.
 */
public class MainFrame extends JFrame {

  private Logger log = Logger.getLogger(MainFrame.class);

  private WindowListener exitListener = new ExitListener();

  private final WritableSDP sdp;

  private final SoftwareTableModel swListTableModel;
  private final ConfigFileTableModel confListTableModel;
  private final GenericItemTableModel genericItemListTableModel;

  private AboutDialog about = new AboutDialog();


  public MainFrame(WritableSDP sdp) {
    this.sdp = Preconditions.checkNotNull(sdp, "sdp must not be null");
    swListTableModel = new SoftwareTableModel(sdp.getSDPEntryListModel(SDPEntryType.SOFTWARE));
    confListTableModel = new ConfigFileTableModel(sdp.getSDPEntryListModel(SDPEntryType.CONFIG));
    genericItemListTableModel = new GenericItemTableModel(sdp.getSDPEntryListModel(SDPEntryType.GENERIC));
    initComponents();
    initEventHandling();
    updateEnablement();
  }

  private void initEventHandling() {
    this.addWindowListener(exitListener);

    this.tableSw.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        updateEnablement();
      }
    });

    this.tableConfig.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        updateEnablement();
      }
    });
  }

  private void addSoftware() {
    File[] files = FileChooserUtils.chooseSoftwareFiles(this);

    if (files != null) {
      try {
        for (int i = 0; i < files.length; i++) {
          SoftwareEntry entry = SoftwareEntryFactory.create(files[i]);
          if (entry != null)
            sdp.addEntry(SDPEntryType.SOFTWARE, entry);
        }

      } catch (Exception e) {
        showErrorMsg("Failed to add new software files cause:\n" + e.getMessage());
      }
    }
  }

  private boolean confirmOverwrite() {
    int ret = JOptionPane.showConfirmDialog(this, "The selected file already exists, do you want to overwrite?",
        "Overwrite?", JOptionPane.YES_NO_CANCEL_OPTION);
    return (ret == JOptionPane.YES_OPTION);
  }

  private void exit() {
    dispose();
  }

  private void createUIComponents() {
    tableSw = new JTable(swListTableModel);
    tableConfig = new JTable(confListTableModel);
    tableOthers = new JTable(genericItemListTableModel);

    tableSw.getSelectionModel().addListSelectionListener(new EntryTableSelectionListener(tableSw));
    tableConfig.getSelectionModel().addListSelectionListener(new EntryTableSelectionListener(tableConfig));
    tableOthers.getSelectionModel().addListSelectionListener(new EntryTableSelectionListener(tableOthers));
  }

  @SuppressWarnings("unchecked")
  private <T> List<T> getSelections(JTable table) {
    ArrayList<T> selections = new ArrayList<>();

    int[] rows = table.getSelectedRows();
    if (rows != null) {
      for (int i = 0; i < rows.length; i++) {
        int modelRow = table.convertRowIndexToModel(rows[i]);
        AbstractTableAdapter<?> model = (AbstractTableAdapter<?>) table.getModel();
        selections.add((T) model.getRow(modelRow));
      }
    }

    return selections;
  }

  private void showInfoMsg(String msg) {
    JOptionPane.showMessageDialog(this,
        msg,
        "Information",
        JOptionPane.INFORMATION_MESSAGE);
  }

  private void showErrorMsg(String msg) {
    JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
  }

  private void showAbout() {
    about.show(this);
  }

  /**
   * Load an existing SDP file.
   */
  private void btnLoadActionPerformed() {
    File file = FileChooserUtils.chooseSDPFileToOpen(this);

    if (file != null) {
      try {
        SdpImporter.loadFromFile(sdp, file);
        tfSDPVersion.setText(sdp.getRevision());

        if (sdp.isEmpty())
          showInfoMsg("The selected SDP file is empty:\n" + file.getAbsolutePath());
        else
          showInfoMsg("The SDP file has been loaded from:\n" + file.getAbsolutePath());
      } catch (Exception e) {
        log.error("Failed to load SDP", e);
        showErrorMsg("Failed to load SDP:\n" + e.getMessage());
      }
    }
  }

  private void btnFeatureActionPerformed(ActionEvent e) {
    List<SoftwareEntry> selections = getSelections(tableSw);
    if (selections.isEmpty())
      return;
    else {
      SoftwareEntry first = selections.get(0);

      AdditionalFeatureEditorDialog dialog = new AdditionalFeatureEditorDialog(this, first.getAdditionalFeatures());
      dialog.setVisible(true);

      if (dialog.isAffirmed()) {
        first.setAdditionalFeatures(dialog.getFeatures());
        tableSw.repaint();
      }
    }
  }

  private void btnCSVActionPerformed(ActionEvent ev) {
    File csvFile = FileChooserUtils.chooseCSVFileToSave(this);

    if (csvFile != null) {
      if (csvFile.exists() && confirmOverwrite() == false) {
        return;
      }

      try {

        String revision = tfSDPVersion.getText();
        CSVSupport.generateCSV(sdp.getSDPEntries(SDPEntryType.SOFTWARE), csvFile.getAbsolutePath(), revision);
        showInfoMsg("CSV file has been created");

      } catch (Exception e) {
        log.error("Failed to generate CSV file", e);
        showErrorMsg("Failed to generate CSV file cause:\n" + e.getMessage());
      }
    }
  }

  private void btnGenerateActionPerformed() {
    File file = FileChooserUtils.chooseSDPFileToSave(this);
    if (file != null) {
      if (file.exists() && confirmOverwrite() == false) {
        return;
      }

      try {
        sdp.setRevision(tfSDPVersion.getText());
        SdpExporter.generateSDP(sdp, file.getAbsolutePath());
        showInfoMsg("The SDP file has been generated to:\n" + file.getAbsolutePath());

      } catch (Exception e) {
        log.error("Failed to generate SDP", e);
        showErrorMsg("Failed to generate SDP cause: \n" + e.getMessage());
      }
    }
  }

  private void btnDeleteSoftwareActionPerformed() {
    List<SoftwareEntry> sels = getSelections(tableSw);
    if (sels.isEmpty() == false) {
      sdp.removeAllEntries(sels);
    }
  }

  private void btnAddSoftwareActionPerformed() {
    addSoftware();
  }

  private void btnDeleteConfigActionPerformed() {
    List<ConfigFileEntry> sels = getSelections(tableConfig);
    sdp.removeAllEntries(sels);
  }

  private void btnAddConfigActionPerformed() {
    File[] configFiles = FileChooserUtils.chooseConfigFilesToOpen(this);
    if (configFiles != null && configFiles.length > 0) {
      for (int i = 0; i < configFiles.length; i++) {
        try {
          ConfigFileEntry entry = ConfigFileEntry.createFromFile(configFiles[i]);
          sdp.addEntry(SDPEntryType.CONFIG, entry);
        } catch (IllegalArgumentException | IOException e) {
          showErrorMsg("Failed to add configuration file cause:\n" + e.getMessage());
        }
      }
    }
  }

  private void btnDeleteOtherActionPerformed() {
    // TODO add your code here
    showErrorMsg("Not implemented");
  }

  private void btnAddOtherActionPerformed() {
    // TODO add your code here
    showErrorMsg("Not implemented");
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    statusPanel = new JPanel();
    labelStatus = new JLabel();
    contentPane = new JPanel();
    panelTop = new JPanel();
    label3 = new JLabel();
    tfSDPVersion = new JTextField();
    tabbedPane = new JTabbedPane();
    panelSw = new JPanel();
    scrollPaneSw = new JScrollPane();
    panelSwControl = new JPanel();
    btnAddSoftware = new JButton();
    btnDeleteSoftware = new JButton();
    btnFeature = new JButton();
    panelConfig = new JPanel();
    scrollPaneConfig = new JScrollPane();
    panelSwControl2 = new JPanel();
    btnAddConfig = new JButton();
    btnDeleteConfig = new JButton();
    panelOthers = new JPanel();
    scrollPaneOthers = new JScrollPane();
    panelOthersControl = new JPanel();
    btnAddOther = new JButton();
    btnDeleteOther = new JButton();
    panelMenuContainer = new JPanel();
    menuBar = new JMenuBar();
    menuFile = new JMenu();
    menuItemExit = new JMenuItem();
    menu1 = new JMenu();
    menuItemAbout = new JMenuItem();
    toolBar = new JToolBar();
    btnLoad = new JButton();
    btnGenerate = new JButton();
    btnCSV = new JButton();
    hSpacer2 = new JPanel(null);

    //======== this ========
    setTitle("Gemini 3 Software Distribution Package Utility");
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    Container contentPane2 = getContentPane();
    contentPane2.setLayout(new BorderLayout());

    //======== statusPanel ========
    {
      statusPanel.setLayout(new FormLayout(
        "default",
        "fill:10dlu"));
      statusPanel.add(labelStatus, CC.xy(1, 1));
    }
    contentPane2.add(statusPanel, BorderLayout.SOUTH);

    //======== contentPane ========
    {
      contentPane.setLayout(new BorderLayout());

      //======== panelTop ========
      {
        panelTop.setBorder(new EmptyBorder(0, 0, 10, 0));
        panelTop.setLayout(new FormLayout(
          "right:default, $lcgap, default, $lcgap, default:grow",
          "default"));

        //---- label3 ----
        label3.setText("SDP Version:");
        panelTop.add(label3, CC.xy(1, 1));

        //---- tfSDPVersion ----
        tfSDPVersion.setMaximumSize(new Dimension(100, 20));
        tfSDPVersion.setColumns(15);
        panelTop.add(tfSDPVersion, CC.xy(3, 1));
      }
      contentPane.add(panelTop, BorderLayout.NORTH);

      //======== tabbedPane ========
      {

        //======== panelSw ========
        {
          panelSw.setLayout(new BorderLayout());

          //======== scrollPaneSw ========
          {

            //---- tableSw ----
            tableSw.setFillsViewportHeight(true);
            tableSw.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            scrollPaneSw.setViewportView(tableSw);
          }
          panelSw.add(scrollPaneSw, BorderLayout.CENTER);

          //======== panelSwControl ========
          {
            panelSwControl.setBorder(Borders.DLU7_BORDER);
            panelSwControl.setLayout(new FormLayout(
              "80dlu",
              "2*(default, $lgap), default"));

            //---- btnAddSoftware ----
            btnAddSoftware.setText("Add");
            btnAddSoftware.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnAddSoftwareActionPerformed();
              }
            });
            panelSwControl.add(btnAddSoftware, CC.xy(1, 1));

            //---- btnDeleteSoftware ----
            btnDeleteSoftware.setText("Delete");
            btnDeleteSoftware.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnDeleteSoftwareActionPerformed();
              }
            });
            panelSwControl.add(btnDeleteSoftware, CC.xy(1, 3));

            //---- btnFeature ----
            btnFeature.setText("Features...");
            btnFeature.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnFeatureActionPerformed(e);
              }
            });
            panelSwControl.add(btnFeature, CC.xy(1, 5));
          }
          panelSw.add(panelSwControl, BorderLayout.EAST);
        }
        tabbedPane.addTab("Software", panelSw);

        //======== panelConfig ========
        {
          panelConfig.setLayout(new BorderLayout());

          //======== scrollPaneConfig ========
          {

            //---- tableConfig ----
            tableConfig.setFillsViewportHeight(true);
            tableConfig.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            scrollPaneConfig.setViewportView(tableConfig);
          }
          panelConfig.add(scrollPaneConfig, BorderLayout.CENTER);

          //======== panelSwControl2 ========
          {
            panelSwControl2.setBorder(Borders.DLU7_BORDER);
            panelSwControl2.setLayout(new FormLayout(
              "80dlu",
              "2*(default, $lgap), default"));

            //---- btnAddConfig ----
            btnAddConfig.setText("Add");
            btnAddConfig.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnAddConfigActionPerformed();
              }
            });
            panelSwControl2.add(btnAddConfig, CC.xy(1, 1));

            //---- btnDeleteConfig ----
            btnDeleteConfig.setText("Delete");
            btnDeleteConfig.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnDeleteConfigActionPerformed();
              }
            });
            panelSwControl2.add(btnDeleteConfig, CC.xy(1, 3));
          }
          panelConfig.add(panelSwControl2, BorderLayout.EAST);
        }
        tabbedPane.addTab("Configuration", panelConfig);

        //======== panelOthers ========
        {
          panelOthers.setLayout(new BorderLayout());

          //======== scrollPaneOthers ========
          {

            //---- tableOthers ----
            tableOthers.setFillsViewportHeight(true);
            tableOthers.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
            scrollPaneOthers.setViewportView(tableOthers);
          }
          panelOthers.add(scrollPaneOthers, BorderLayout.CENTER);

          //======== panelOthersControl ========
          {
            panelOthersControl.setBorder(Borders.DLU7_BORDER);
            panelOthersControl.setLayout(new FormLayout(
              "80dlu",
              "2*(default, $lgap), default"));

            //---- btnAddOther ----
            btnAddOther.setText("Add");
            btnAddOther.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnAddOtherActionPerformed();
              }
            });
            panelOthersControl.add(btnAddOther, CC.xy(1, 1));

            //---- btnDeleteOther ----
            btnDeleteOther.setText("Delete");
            btnDeleteOther.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnDeleteOtherActionPerformed();
              }
            });
            panelOthersControl.add(btnDeleteOther, CC.xy(1, 3));
          }
          panelOthers.add(panelOthersControl, BorderLayout.EAST);
        }
        tabbedPane.addTab("Others", panelOthers);
      }
      contentPane.add(tabbedPane, BorderLayout.CENTER);
    }
    contentPane2.add(contentPane, BorderLayout.CENTER);

    //======== panelMenuContainer ========
    {
      panelMenuContainer.setLayout(new BorderLayout());

      //======== menuBar ========
      {

        //======== menuFile ========
        {
          menuFile.setText("File");

          //---- menuItemExit ----
          menuItemExit.setText("Exit");
          menuItemExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              exit();
            }
          });
          menuFile.add(menuItemExit);
        }
        menuBar.add(menuFile);

        //======== menu1 ========
        {
          menu1.setText("Help");

          //---- menuItemAbout ----
          menuItemAbout.setText("About...");
          menuItemAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              showAbout();
            }
          });
          menu1.add(menuItemAbout);
        }
        menuBar.add(menu1);
      }
      panelMenuContainer.add(menuBar, BorderLayout.CENTER);

      //======== toolBar ========
      {
        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        //---- btnLoad ----
        btnLoad.setText("Load SDP");
        btnLoad.setIcon(UIManager.getIcon("Tree.openIcon"));
        btnLoad.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btnLoadActionPerformed();
          }
        });
        toolBar.add(btnLoad);

        //---- btnGenerate ----
        btnGenerate.setText("Generate SDP ");
        btnGenerate.setToolTipText("Generate Software Distribution Package");
        btnGenerate.setIcon(UIManager.getIcon("FileView.floppyDriveIcon"));
        btnGenerate.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btnGenerateActionPerformed();
          }
        });
        toolBar.add(btnGenerate);

        //---- btnCSV ----
        btnCSV.setText("Generate CSV");
        btnCSV.setIcon(UIManager.getIcon("FileView.floppyDriveIcon"));
        btnCSV.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btnCSVActionPerformed(e);
          }
        });
        toolBar.add(btnCSV);
        toolBar.add(hSpacer2);
      }
      panelMenuContainer.add(toolBar, BorderLayout.SOUTH);
    }
    contentPane2.add(panelMenuContainer, BorderLayout.NORTH);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel statusPanel;
  private JLabel labelStatus;
  private JPanel contentPane;
  private JPanel panelTop;
  private JLabel label3;
  private JTextField tfSDPVersion;
  private JTabbedPane tabbedPane;
  private JPanel panelSw;
  private JScrollPane scrollPaneSw;
  private JTable tableSw;
  private JPanel panelSwControl;
  private JButton btnAddSoftware;
  private JButton btnDeleteSoftware;
  private JButton btnFeature;
  private JPanel panelConfig;
  private JScrollPane scrollPaneConfig;
  private JTable tableConfig;
  private JPanel panelSwControl2;
  private JButton btnAddConfig;
  private JButton btnDeleteConfig;
  private JPanel panelOthers;
  private JScrollPane scrollPaneOthers;
  private JTable tableOthers;
  private JPanel panelOthersControl;
  private JButton btnAddOther;
  private JButton btnDeleteOther;
  private JPanel panelMenuContainer;
  private JMenuBar menuBar;
  private JMenu menuFile;
  private JMenuItem menuItemExit;
  private JMenu menu1;
  private JMenuItem menuItemAbout;
  private JToolBar toolBar;
  private JButton btnLoad;
  private JButton btnGenerate;
  private JButton btnCSV;
  private JPanel hSpacer2;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  private void updateEnablement() {
    // boolean hasEntry = !sdp.getSoftwareEntries().isEmpty();
    // btnGenerate.setEnabled(hasEntry);
    // btnCSV.setEnabled(hasEntry);

    boolean hasSelection = tableSw.getSelectedRowCount() > 0;
    btnDeleteSoftware.setEnabled(hasSelection);
    btnFeature.setEnabled(hasSelection);

    hasSelection = tableConfig.getSelectedRowCount() > 0;
    btnDeleteConfig.setEnabled(hasSelection);
    btnDeleteConfig.setEnabled(hasSelection);
  }


  private class ExitListener extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent e) {
      exit();
    }
  }

  private class EntryTableSelectionListener implements ListSelectionListener {

    private final JTable table;


    EntryTableSelectionListener(JTable table) {
      this.table = table;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
      List<Object> selections = getSelections(table);
      if (selections.isEmpty() == false) {
        Object sel = selections.get(0);
        if (sel instanceof ISDPEntry) {
          String text = ((ISDPEntry) sel).getEntryFile().getAbsolutePath();
          labelStatus.setText(text);
          labelStatus.setToolTipText(text);
        }
      } else {
        labelStatus.setText(null);
        labelStatus.setToolTipText(null);
      }
    }
  }

}
