/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.gui;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.impl.GenericItemEntry;

/**
 * The Class OtherItemTableModel.
 */
class GenericItemTableModel extends AbstractTableAdapter<ISDPEntry> {

  private static final String[] COLUMNS = { "Name", "Description", "File" };

  GenericItemTableModel(ListModel<ISDPEntry> listModel) {
    super(listModel, COLUMNS);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    GenericItemEntry entry = (GenericItemEntry) getRow(rowIndex);

    switch (columnIndex) {
    case 0:
      return entry.getEntryName();
    case 2:
      return entry.getDescription();
    case 3:
      return entry.getEntryFile().getPath();
    default:
      return null;
    }
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

}
