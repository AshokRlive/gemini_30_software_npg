/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.gui;

import java.util.List;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.impl.FeatureVersionFactory;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;

/**
 * The Class SoftwareTableModel.
 */
class SoftwareTableModel extends AbstractTableAdapter<ISDPEntry> {

  private static final String[] COLUMNS = { "Module Type", "System API", "Version", "Features", "File" };


  SoftwareTableModel(ListModel<ISDPEntry> listModel) {
    super(listModel, COLUMNS);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    SoftwareEntry fw = (SoftwareEntry) getRow(rowIndex);
    switch (columnIndex) {
    case 0:
      return fw.getModuleType();
    case 1:
      return fw.getSystemAPI();
    case 2:
      return fw.getVersion();
    case 3:
      List<FeatureVersion> flist = fw.getAllFeatures();
      return FeatureVersionFactory.createCommaSeparatedStr(flist);
    case 4:
      return fw.getEntryFile().getPath();
    default:
      return null;
    }
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }

}
