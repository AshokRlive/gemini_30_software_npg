/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.gui;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FilenameUtils;

/**
 * This utility provide consistent way to choose files used by the application. 
 */
final class FileChooserUtils {
  private static final FileNameExtensionFilter SOFTWARE_FILE_FILTER
    = new FileNameExtensionFilter("Firmware File(.zip .bin)", "bin","zip");
  
  private static final FileNameExtensionFilter SDP_FILE_FILTER
    = new FileNameExtensionFilter("Software Distribution Package (.zip, .sdp)", 
        "sdp","zip");
  
  private static final FileNameExtensionFilter CSV_FILE_FILTER
    = new FileNameExtensionFilter("Comma Seperated Values", "csv");
  
  private static final FileNameExtensionFilter CONFIG_FILE_FILTER
    = new FileNameExtensionFilter("G3 Configuration File (.xml)", "xml");
  
  private static String fileChooserDir = System.getProperty("user.dir");
  
  private FileChooserUtils(){}
  
  public static File[] chooseSoftwareFiles(Component parent) {
    JFileChooser fc = new JFileChooser(fileChooserDir);
    fc.setDialogTitle("Select G3 Firmware Files");
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setFileFilter(SOFTWARE_FILE_FILTER);
    fc.setMultiSelectionEnabled(true);
    fc.setAcceptAllFileFilterUsed(false);
    
    int option = fc.showDialog(parent, "Select");

    if (JFileChooser.APPROVE_OPTION == option) {
      File[] files = fc.getSelectedFiles();
      
      if (files.length > 0)
        fileChooserDir = files[0].getParent();
      
      return files;
    }
    
    return null;
  }
  
  public static File chooseSDPFileToSave(Component parent) {
    JFileChooser fc = new JFileChooser(fileChooserDir);
    fc.setDialogTitle("Create Software Distribution Package");
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setFileFilter(SDP_FILE_FILTER);
    fc.setAcceptAllFileFilterUsed(true);

    int option = fc.showDialog(parent, "Save");

    if (JFileChooser.APPROVE_OPTION == option) {
      File file = fc.getSelectedFile();
      fileChooserDir = file.getParent();
      
      /*Set default file extension*/
      if (!FilenameUtils.getExtension(file.getName()).equals("zip")
          && !FilenameUtils.getExtension(file.getName()).equals("sdp")) {
        file = new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName()) + ".zip");
      }
      return file;
    }
    
    return null;
  }
  
  public static File chooseSDPFileToOpen(Component parent) {
    JFileChooser fc = new JFileChooser(fileChooserDir);
    fc.setDialogTitle("Load Software Distribution Package");
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setFileFilter(SDP_FILE_FILTER);
    fc.setAcceptAllFileFilterUsed(true);
    
    
    int option = fc.showDialog(parent, "Open");

    if (JFileChooser.APPROVE_OPTION == option) {
      File file = fc.getSelectedFile();
      fileChooserDir = file.getParent();
      return file;
    }
    
    return null;
  }
  
  public static File chooseCSVFileToSave(Component parent) {

    JFileChooser fc = new JFileChooser(fileChooserDir);
    fc.setDialogTitle("Create CSV File");
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setFileFilter(CSV_FILE_FILTER);
    fc.setAcceptAllFileFilterUsed(true);

    int option = fc.showDialog(parent, "Save");

    if (JFileChooser.APPROVE_OPTION == option) {
      File file = fc.getSelectedFile();
      fileChooserDir = file.getParent();
      return file;
    }
  
    return null;
  }
  
  public static File[] chooseConfigFilesToOpen(Component parent) {
    JFileChooser fc = new JFileChooser(fileChooserDir);
    fc.setDialogTitle("Select Configuration Files");
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setMultiSelectionEnabled(true);
    fc.setFileFilter(CONFIG_FILE_FILTER);
    fc.setAcceptAllFileFilterUsed(true);
    
    int option = fc.showDialog(parent, "Select");
    
    if (JFileChooser.APPROVE_OPTION == option) {
      File[] files = fc.getSelectedFiles();
      if (files.length > 0)
        fileChooserDir = files[0].getParent();
      return files;
    }
    
    return null;
  }
}

