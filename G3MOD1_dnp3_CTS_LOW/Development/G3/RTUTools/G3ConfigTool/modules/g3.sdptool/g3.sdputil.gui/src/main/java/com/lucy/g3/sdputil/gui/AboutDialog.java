/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.gui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.lucy.g3.common.manifest.ManifestInfo;


/**
 * A dialog for showing versions information of the application.
 */
class AboutDialog {

  private ManifestInfo versionInfo;


  public void show(Component parent) {
    if (versionInfo == null)
      versionInfo = new ManifestInfo();

    TableModel tableModel = new DefaultTableModel(versionInfo.getProperties(), new String[] { "Feature", "Version" }) {
      @Override
      public boolean isCellEditable(int row, int column) {
        return false;
      }
    };
    JTable table = new JTable(tableModel);
    table.setBackground(UIManager.getColor("Panel.background"));

    JScrollPane scroll = new JScrollPane(table);
    scroll.setPreferredSize(new Dimension(300, 180));

    JOptionPane.showMessageDialog(parent, scroll,
        "About " + versionInfo.getAppName(), JOptionPane.PLAIN_MESSAGE);
  }
}
