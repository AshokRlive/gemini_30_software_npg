/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.sdp.model.impl.FeatureVersionFactory;

/**
 * The dialog for editing addtional features.
 */
public class AdditionalFeatureEditorDialog extends JDialog {
  
  private List<FeatureVersion> features;
  private boolean affirmed = false;
  
  public AdditionalFeatureEditorDialog(Frame owner, List<FeatureVersion> curFeatures) {
    super(owner);
    initComponents();
    
    txtFeature.setText(FeatureVersionFactory.createCommaSeparatedStr(curFeatures));
  }
  
  public boolean isAffirmed() {
    return affirmed;
  }

  public List<FeatureVersion> getFeatures() {
    return features;
  }

  private void btnAddActionPerformed(ActionEvent e) {
    try {
      features = FeatureVersionFactory.createFromStr(txtFeature.getText());
      affirmed = true;
      this.dispose();
    } catch (ParseException e1) {
      JOptionPane.showMessageDialog(this, "Invalid input features cause:\n " + e1.getMessage(), 
          "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  private void btnCancelActionPerformed(ActionEvent e) {
    this.dispose();
  }

  private void initComponents() {
    
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    txtFeature = new JTextField();
    buttonBar = new JPanel();
    btnAdd = new JButton();
    btnCancel = new JButton();

    //======== this ========
    setTitle("Additional Features");
    setModal(true);
    setResizable(false);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.createEmptyBorder("7dlu, 7dlu, 7dlu, 7dlu"));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "default, $lcgap, default",
            "2*(default, $lgap), default"));

        //---- label1 ----
        label1.setText("Enter the feature for selected module in major.minor format");
        contentPanel.add(label1, CC.xy(1, 1));

        //---- label2 ----
        label2.setText("Comma seperated for multiple entries.");
        contentPanel.add(label2, CC.xy(1, 3));
        contentPanel.add(txtFeature, CC.xywh(1, 5, 2, 1));
      }
      dialogPane.add(contentPanel, BorderLayout.WEST);

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.createEmptyBorder("5dlu, 0dlu, 0dlu, 0dlu"));
        buttonBar.setLayout(new FormLayout(
            "$glue, $button, $rgap, $button",
            "fill:default:grow"));

        //---- btnAdd ----
        btnAdd.setText("OK");
        btnAdd.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnAddActionPerformed(e);
          }
        });
        buttonBar.add(btnAdd, CC.xy(2, 1));

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnCancelActionPerformed(e);
          }
        });
        buttonBar.add(btnCancel, CC.xy(4, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JLabel label1;
  private JLabel label2;
  private JTextField txtFeature;
  private JPanel buttonBar;
  private JButton btnAdd;
  private JButton btnCancel;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
