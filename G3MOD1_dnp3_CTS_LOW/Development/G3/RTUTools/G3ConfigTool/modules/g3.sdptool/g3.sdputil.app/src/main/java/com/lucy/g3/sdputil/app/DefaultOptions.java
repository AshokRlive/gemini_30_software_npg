/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import java.io.PrintWriter;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Default implementation of command line options that contains options "help" and "version".
 */
public class DefaultOptions {
  public static final String OPTION_HELP       = "h";           
  public static final String OPTION_VERSION    = "v"; 
  
  private final Options options;
  private CommandLine line;
  
  private final String jarName;
  
  public DefaultOptions(String jarName) {
    this.jarName = jarName;
    options = initialiseOptions();
  }
  
  protected Options initialiseOptions() {
    Options options = new Options();
    
    Option version = Option.builder(OPTION_VERSION)
        .longOpt("version")
        .desc("Print version information")
        .build();
    
    Option help = Option.builder(OPTION_HELP)
        .longOpt("help")
        .desc("Print usage")
        .build();
    
    options.addOption(version);
    options.addOption(help);
    
    return options;
  }
  
  public void parse(String[] args) throws ParseException {
    CommandLineParser parser = new DefaultParser();
    
    parser.parse(options, args, false);
    
    line = parser.parse(options, args, false);
  }
  
  public void printHelp() {
    PrintWriter pw = new PrintWriter(System.out);
    printHelp(pw);
    pw.flush();
  }
  
  public void printHelp(PrintWriter out) {
    HelpFormatter formatter = new HelpFormatter();
    
    String cmdLineSyntax = "java -jar " + jarName;
    String header = "========== Parameters ===========";
    
    formatter.printHelp(
        out,
        800, 
        cmdLineSyntax,
        header, 
        options,
        formatter.getLeftPadding(), 
        formatter.getDescPadding(),
        "", 
        true);
  }
  
  public String getParsedArgsAsText() {
    StringBuilder sb = new StringBuilder();
    Option[] ops = line.getOptions();
    for (Option o:ops) {
      sb.append(o.getLongOpt());
      String val = o.getValue();
      if (val != null) {
        sb.append(":");
        sb.append(val);
      }
      sb.append("\n");
    }
    
    return sb.toString();
  }
  
 //================== Delegate Methods ===================
  
  public String getOptionValue(String option) {
    return line.getOptionValue(option);
  }
  
  public String[] getOptionValues(String option) {
    return line.getOptionValues(option);
  }
  
  public boolean hasOption(String option) {
    return line.hasOption(option);
  }

  Properties getProperty(String option) {
    return line.getOptionProperties(option);
  }
}

