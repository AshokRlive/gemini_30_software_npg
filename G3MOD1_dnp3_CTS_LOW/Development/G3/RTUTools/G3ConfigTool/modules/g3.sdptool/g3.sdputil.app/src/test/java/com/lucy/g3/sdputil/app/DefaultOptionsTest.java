/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import static org.junit.Assert.assertTrue;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.cli.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class DefaultOptionsTest {
  public static final String USAGE_STARTING_TEXT = "usage";

  @Rule
  public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

  
  private DefaultOptions fixture;
  
  @Before
  public void setUp() throws Exception {
    fixture = new DefaultOptions("SDPUtility.jar");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPrintHelp() {
    fixture.printHelp();
    assertTrue(systemOutRule.getLog().startsWith(USAGE_STARTING_TEXT));
  }

  @Test
  public void testPrintHelpToString() {
    StringWriter out = new StringWriter(200);
    fixture.printHelp(new PrintWriter(out));
    String help = out.toString();
    assertTrue(help.startsWith(USAGE_STARTING_TEXT));
  }
  
  @Test
  public void testParseOptionHelp() throws ParseException {
    String[] args = new String[]{ "-"+DefaultOptions.OPTION_HELP };
    fixture.parse(args);
    assertTrue(fixture.hasOption(DefaultOptions.OPTION_HELP));
  }
  
  @Test
  public void testParseOptionAbout() throws ParseException {
    String[] args = new String[]{ "-"+DefaultOptions.OPTION_VERSION };
    fixture.parse(args);
    assertTrue(fixture.hasOption(DefaultOptions.OPTION_VERSION));
  }
  
  @Test (expected = ParseException.class)
  public void testParseUnknownOption() throws ParseException{
    String[] args = new String[]{ "-123"};
    fixture.parse(args);
  }

}

