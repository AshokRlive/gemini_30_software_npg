/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import java.io.PrintWriter;
import java.util.List;

import org.apache.log4j.Logger;

import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPEntryType;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;

/**
 * A class for printing the content of SDP to string.
 */
public final class SDPPrinter {
  private SDPPrinter(){}
  
  public static void print(SDP sdp) {
    print(sdp, null);
  }
  
  public static void print(SDP sdp, PrintWriter pw) {
    if (pw == null) {
      pw = new PrintWriter(System.out);
    }
    
    String toPrint = printSDPToStr(sdp);
    pw.print(toPrint);
    pw.flush();
    
    Logger.getLogger(SDPPrinter.class).info("\n"+toPrint);
  }
  
  public static String printSDPToStr(SDP sdp) {
    List<ISDPEntry> swlist = sdp.getSDPEntries(SDPEntryType.SOFTWARE);
    
    StringBuilder sb = new StringBuilder();
    String lineSep = System.getProperty("line.separator");
    
    sb.append("This software package includes:");
    sb.append(lineSep);
    for (ISDPEntry entry: swlist) {
      sb.append(entry.printToStr());
      sb.append(lineSep);
    }
    
    List<ISDPEntry> configlist = sdp.getSDPEntries(SDPEntryType.CONFIG);
    for (ISDPEntry entry: configlist) {
      sb.append(entry.printToStr());
      sb.append(lineSep);
    }
    
    return sb.toString();
  }
  
}
