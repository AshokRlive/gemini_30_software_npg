/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;


/**
 * The system exit code of this application.
 */
public interface ErrorCode {
  int STATUS_OK = 0;
  int STATUS_ERR = -1;
  int STATUS_GUI_RUNNING = 1;
}

