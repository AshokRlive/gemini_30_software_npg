/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import org.junit.Rule;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.contrib.java.lang.system.SystemOutRule;


public class MainAppTest {
  @Rule
  public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

  @Test
  public void testLaunchAppWithInvalidArgs() {
    String[] args = new String[]{ "-invalid" };
    assertEquals(MainApp.STATUS_ERR, new MainApp().launch(args));
  }

  @Test
  public void testLaunchAppWithHelp() {
    String[] args = new String[]{ "-h" };
    assertEquals(MainApp.STATUS_OK, new MainApp().launch(args));
    
    String log = systemOutRule.getLog();
    assertTrue("Unexpected output:"+log, log.startsWith(DefaultOptionsTest.USAGE_STARTING_TEXT));
  }
  
  @Test
  public void testLaunchAppWithVersion() {
    String[] args = new String[]{ "-v" };
    assertEquals(MainApp.STATUS_OK, new MainApp().launch(args));
    assertTrue(systemOutRule.getLog().startsWith("About"));
  }

}

