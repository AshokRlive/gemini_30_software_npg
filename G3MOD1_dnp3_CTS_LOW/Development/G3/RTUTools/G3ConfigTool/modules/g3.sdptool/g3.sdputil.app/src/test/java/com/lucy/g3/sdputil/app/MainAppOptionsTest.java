/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Properties;

import org.apache.commons.cli.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class MainAppOptionsTest {
  private MainAppOptions fixture;
  
  @Rule
  public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
  
  @Before
  public void setUp() throws Exception {
    fixture = new MainAppOptions("SDPUtility.jar");
  }

  @After
  public void tearDown() throws Exception {
  }
  
  @Test
  public void testPrintHelp() {
    fixture.printHelp();
    assertTrue(systemOutRule.getLog().startsWith(DefaultOptionsTest.USAGE_STARTING_TEXT));
  }
  
  @Test
  public void testParseOptionFeature() throws ParseException{
    String[] args = new String[]{ "-fSCMNXP.bin=1.123","-fPSMnXP.bin=2.3,3.4", "-t=SDP.zip" };
    fixture.parse(args);
    
    Properties p = fixture.getFeatures();
    assertEquals("2.3,3.4", p.get("PSMnXP.bin"));
    assertEquals("1.123",   p.get("SCMNXP.bin"));
  }
  
  @Test
  public void testParseOptionFeature2() throws ParseException{
    String[] args = new String[]{ "-f=SCMNXP.bin=1.123","-f=PSMnXP.bin=2.3,3.4", "-t=SDP.zip" };
    fixture.parse(args);
    
    Properties p = fixture.getFeatures();
    assertEquals("2.3,3.4", p.get("PSMnXP.bin"));
    assertEquals("1.123",   p.get("SCMNXP.bin"));
  }
  
  @Test
  public void testParseOptionModule() throws ParseException{
    String[] modules = {
        "SCMNXP.bin",
        "slave/PSMNXP.bin",
        "bin/DSMNXP.bin",
    };
    ArrayList<String> args = new ArrayList<>();
    args.add("-t=SDP.zip" ); // Add mandatory option
    
    for (String m : modules) {
      args.add("-m="+m);
    }
    fixture.parse(args.toArray(new String[args.size()]));
    String[] values = fixture.getOptionValues(MainAppOptions.OPTION_MODULE);
    assertNotNull(values);
    assertEquals(values.length, modules.length);
    
    for (int i = 0; i < values.length; i++) {
      assertEquals(values[i], modules[i]);
    }
  }
  
  @Test
  public void testParseRevision() throws ParseException{
    fixture.parse(new String[]{"-t=test.sdp","-r=1.1.1-test"});
    assertEquals("1.1.1-test", fixture.getOptionValue(MainAppOptions.OPTION_REVISION));
  }

}

