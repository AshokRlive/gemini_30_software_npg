/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.zeroturnaround.zip.ZipUtil;

import com.lucy.g3.test.support.resources.TestResources;

public class OptionsProcessorTest {
  private OptionsProcessor fixture;
  private MainAppOptions options;
  
  @Before
  public void init(){
    fixture = new OptionsProcessor();
    options = new MainAppOptions("");
  }
  
  @Test(expected = NullPointerException.class)
  public void testInitWithNull() throws Exception {
    fixture.initialise(null);
  }
  
  @Test (expected = IOException.class)
  public void testDisplayNonexistSDP() throws Exception {
    options.parse(new String[]{"-t=nonexistsSDP.zip", "-d"});
    fixture.initialise(options);
    fixture.run();
  }
  
  @Test 
  public void testDisplay() throws Exception {
    String sdp = TestResources.getSampleSDP();
    options.parse(new String[]{"-t="+sdp, "-d"});
    fixture.initialise(options);
    fixture.run();
  }
  
  @Test 
  public void testGenerateSDPFromExisting() throws Exception {
    // Prepare target
    final File target = new File("target/testGenerateSDPFromExisting/ResultSDP.zip");
    target.delete();
    assertFalse(target.exists());
    
    FileUtils.copyFile(new File(TestResources.getSampleSDP()), target);
    
    // Prepare command line options
    ArrayList<String> args = new ArrayList<>();
    args.add("-t="+target.getAbsolutePath());
    args.add("-r=1.0.0-Test");

    // Generate target
    String[] argsArray = args.toArray(new String[args.size()]);
    options.parse(argsArray);
    fixture.initialise(options);
    fixture.run();
    
    assertTrue("SDP file was not generated",target.exists());
  }
  
  @Test 
  public void testGenerateSDPFromScratch() throws Exception {
    // Prepare software files
    File binDir = new File("target/testGenerateSDPFromScratch/bin/");
    ZipUtil.unpack(new File(TestResources.getSampleSDP()), binDir);
    final File[] binFiles = binDir.listFiles();
    
    // Prepare target
    final File target = new File("target/testGenerateSDPFromScratch/ResultSDP.zip");
    target.delete();
    assertFalse(target.exists());
    
    // Prepare command line options
    ArrayList<String> args = new ArrayList<>();
    args.add("-t="+target.getAbsolutePath());
    assertTrue(binFiles.length > 0);
    for (int i = 0; i < binFiles.length; i++) {
      if(binFiles[i].getName().endsWith(".bin")
          ||binFiles[i].getName().endsWith(".zip")) {
        args.add("-m="+binFiles[i].getAbsolutePath());
        args.add("-f="+binFiles[i].getName()+"=0.0,1.2,2.3,9.311");
      }
    }
    
    // Generate target
    String[] argsArray = args.toArray(new String[args.size()]);
    options.parse(argsArray);
    fixture.initialise(options);
    fixture.run();
    
    assertTrue("SDP file was not generated",target.exists());
  }

}

