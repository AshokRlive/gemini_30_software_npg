/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import javax.swing.UIManager;

import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.lucy.g3.common.manifest.ManifestInfo;
import com.lucy.g3.sdp.model.WritableSDP;
import com.lucy.g3.sdp.model.impl.WritableSDPImpl;
import com.lucy.g3.sdputil.gui.MainFrame;

/**
 * The main application for launching all components.
 */
public class MainApp implements ErrorCode {

  private Logger log = Logger.getLogger(MainApp.class);

  private static final String DEFAULT_JAR_NAME = "SDPUility.jar";


  public int launch(String[] args) {
    if (args.length <= 0) {
      launchGUI(new WritableSDPImpl());
      log.info("Running SDPUtility in GUI mode.");
      return STATUS_GUI_RUNNING;
    } else {
      log.info("Running SDPUtility in command line mode.");
    }

    MainAppOptions options = new MainAppOptions(DEFAULT_JAR_NAME);

    try {
      // ====== Parse default options ======
      options.parse(args);

      if (options.hasOption(DefaultOptions.OPTION_HELP)) {
        options.printHelp();

      } else if (options.hasOption(DefaultOptions.OPTION_VERSION)) {
        printAbout();

      } else {
        // ====== Parse application options ======
        OptionsProcessor proc = new OptionsProcessor();
        proc.initialise(options);
        proc.run();
      }

      return STATUS_OK;

    } catch (ParseException e) {
      System.out.println("Unable to parse command line options: " + e.getMessage() + "\n");
      options.printHelp();
      return STATUS_ERR;

    } catch (Throwable e) {
      System.out.println(e.getMessage());
      return STATUS_ERR;
    }
  }

  private void printAbout() {
    ManifestInfo ver = new ManifestInfo(DEFAULT_JAR_NAME.replace(".jar", ""));
    System.out.print("About " + ver.getFormattedText());
  }

  private void launchGUI(WritableSDP sdp) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception e) {
      e.printStackTrace();
    }

    new MainFrame(sdp).setVisible(true);
  }

  /**
   * The main method for launching SDPUtility.
   *
   * @param args
   *          the arguments.
   */
  public static void main(String[] args) {
    int status = new MainApp().launch(args);

    if (status != STATUS_GUI_RUNNING) {
      Logger.getLogger(MainApp.class).info("Exit application with status: " + status);

      /*
       * System exit shouldn't be called in GUI mode since it will kill GUI
       * thread and virtual machine
       */
      System.exit(status);
    }
  }

}
