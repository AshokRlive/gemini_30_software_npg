/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import static com.lucy.g3.sdputil.app.MainAppOptions.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.ValidationException;

import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.sdp.exporter.SdpExporter;
import com.lucy.g3.sdp.importer.SdpImporter;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPEntryType;
import com.lucy.g3.sdp.model.WritableSDP;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.FeatureVersionFactory;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntryFactory;
import com.lucy.g3.sdp.model.impl.WritableSDPImpl;
import com.lucy.g3.sdputil.support.csv.CSVSupport;
import com.lucy.g3.sdputil.support.legacy.LegacySupport;

/**
 * A class for processing command options.
 */
class OptionsProcessor implements ErrorCode {

  private Logger log = Logger.getLogger(OptionsProcessor.class);

  private final WritableSDP model = new WritableSDPImpl();
  private MainAppOptions options;
  private boolean sdpExists = false;


  /**
   * Initialises with parsed options.
   */
  public void initialise(MainAppOptions options) throws ValidationException, IOException, ParseException {
    this.options = Preconditions.checkNotNull(options, "options must not be null");

    // Init SDP model
    String target = options.getOptionValue(OPTION_TARGET);
    if (target == null) {
      throw new ParseException("Missing option:" + OPTION_TARGET);
    }

    File targetFile = new File(target);
    if (targetFile.exists()) {
      SdpImporter.loadFromFile(model, targetFile);
      sdpExists = true;
    } 

    if (model == null) {
      throw new IOException("Failed to read SDP file.");
    }
  }

  /**
   * Runs processor. {@link #initialise(MainAppOptions)} must be called before
   * this method.
   * 
   * @return ErrorCode
   * @throws Exception if failed to run with options.
   */
  public int run() throws Exception {
    /* Display SDP */
    if (options.hasOption(OPTION_DISPLAY)) {
      if (sdpExists == false)
        throw new IOException("SDP file not found!");

      SDPPrinter.print(model);
      return STATUS_OK;
    }

    /* Add software entries */
    String[] bins = options.getOptionValues(OPTION_MODULE);
    if (bins != null) {
      for (int i = 0; i < bins.length; i++) {
        File moduleFile = new File(bins[i]);
        SoftwareEntry entry = SoftwareEntryFactory.create(moduleFile);
        model.addEntry(SDPEntryType.SOFTWARE, entry);
        log.info("Added software : " + moduleFile.getAbsolutePath());
      }
    }

    /* Add additional features */
    Properties featuresProperties = options.getFeatures();
    if (featuresProperties != null) {
      Object[] keys = featuresProperties.keySet().toArray();
      for (int i = 0; i < keys.length; i++) {
        String softwareName = keys[i].toString();
        String featureStr = featuresProperties.getProperty(softwareName);
        List<FeatureVersion> features = FeatureVersionFactory.createFromStr(featureStr);
        SoftwareEntry entry = (SoftwareEntry) model.getEntryByName(SDPEntryType.SOFTWARE, softwareName);
        if (entry == null) {
          throw new Exception("Cannot add feature to software:" + softwareName + " that doesn't exist!");
        } else {
          entry.addAdditionalFeatures(features);
          log.info("Added features : " + featureStr + " to:" + softwareName);
        }
      }
    }

    /* Add configuration files */
    String[] confFiles = options.getOptionValues(OPTION_CONFIG);
    if (confFiles != null) {
      for (int i = 0; i < confFiles.length; i++) {
        File confFile = new File(confFiles[i]);
        model.addEntry(SDPEntryType.CONFIG, ConfigFileEntry.createFromFile(confFile));
        log.info("Added configuration file: " + confFile.getAbsolutePath());
      }
    }

    /* Set revision */
    if (options.hasOption(OPTION_REVISION)) {
      String sdpVersion = options.getOptionValue(OPTION_REVISION);
      model.setRevision(sdpVersion);
    }
    
    /* Generate SDP file */
    String target = options.getOptionValue(OPTION_TARGET);
    File targetFile = new File(target);
    SdpExporter.generateSDP(model, target);
    String msg = "SDP file generated:" + targetFile.getAbsolutePath();
    System.out.println(msg);
    log.info(msg);
    
    /* Set lighttpd user */
    if (options.hasOption(OPTION_LIGHTTPD_USER)) {
      String lighttdUserFile = options.getOptionValue(OPTION_LIGHTTPD_USER);
      SDPPatcher.patchLighttpdUser(new File(target), null, new File(lighttdUserFile));
    }

    /* Convert to legacy format */
    if (options.hasOption(OPTION_LEGACY)) {
      LegacySupport.convertToLegacyFormat(SDP.MANIFEST_FILE_NAME, targetFile);
    }

    /* Generate CSV file */
    if (options.hasOption(OPTION_CSV)) {
      List<ISDPEntry> swEntries = model.getSDPEntries(SDPEntryType.SOFTWARE);
      CSVSupport.generateCSV(swEntries,
          options.getOptionValue(OPTION_CSV),
          options.getOptionValue(OPTION_REVISION));
    }

    return STATUS_OK;
  }
}
