/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.app;

import java.io.File;
import java.io.PrintWriter;
import java.util.Properties;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

/**
 * The command line options of this application.
 */
class MainAppOptions extends DefaultOptions {

  public static final String OPTION_TARGET = "t";
  public static final String OPTION_DISPLAY = "d";
  public static final String OPTION_MODULE = "m";
  public static final String OPTION_FEATURE = "f";
  public static final String OPTION_CONFIG = "c";
  public static final String OPTION_REVISION = "r";
  public static final String OPTION_CSV = "csv";
  public static final String OPTION_LEGACY = "legacy";
  public static final String OPTION_LIGHTTPD_USER = "lighttpdUser";


  MainAppOptions(String jarName) {
    super(jarName);
  }

  @Override
  protected Options initialiseOptions() {
    Options options = super.initialiseOptions();

    Option target = Option.builder(OPTION_TARGET)
        .longOpt("target")
        .hasArg()
        .argName("SDP FILE").type(File.class)
        .desc("Target SDP file to be generated/updated(if exists)")
        .build();

    Option software = Option.builder(OPTION_MODULE)
        .longOpt("module")
        .desc("G3 module software file to be packaged into SDP")
        .hasArg().argName("BIN FILE").type(String.class)
        .build();

    Option feature = Option.builder(OPTION_FEATURE)
        .longOpt("feature")
        .argName("module=feature,feature...").numberOfArgs(2).valueSeparator()
        .desc("Supported additional features for module (comma seperated)").build();

    Option config = Option.builder(OPTION_CONFIG)
        .longOpt("config")
        .desc("G3 configuration file to be packaged into SDP")
        .hasArg().argName("CONFIG FILE").type(File.class)
        .build();

    Option csv = Option.builder(OPTION_CSV)
        .longOpt("csv")
        .hasArg().argName("CSV FILE").type(File.class)
        .desc("Generate CSV file from an existing package")
        .build();

    Option legacy = Option.builder(OPTION_LEGACY)
        .longOpt("legacy")
        .desc("Generate legacy format package")
        .build();

    Option revision = Option.builder(OPTION_REVISION)
        .longOpt("revision")
        .hasArg().argName("REVISION STRING").type(String.class)
        .desc("Set the revision of SDP")
        .build();

    Option display = Option.builder(OPTION_DISPLAY)
        .longOpt("display")
        .desc("Display the details of an existing SDP file")
        .build();
    
    Option lighttpdUser = Option.builder(OPTION_LIGHTTPD_USER)
        .longOpt(OPTION_LIGHTTPD_USER)
        .hasArg().argName("LIGHTTPD USER FILE").type(String.class)
        .desc("Replace the lighttpd user file in SDP")
        .build();

    options.addOption(target);
    options.addOption(software);
    options.addOption(feature);
    options.addOption(config);
    options.addOption(csv);
    options.addOption(legacy);
    options.addOption(revision);
    options.addOption(display);
    options.addOption(lighttpdUser);

    return options;
  }

  @Override
  public void printHelp(PrintWriter out) {
    super.printHelp(out);

    String footer = "Example:"
        + "java -jar SDPUtility.jar -t=SDP.zip -m=MCM.zip -m=PSMBoardNXP.bin -m=SCMBoardNXP.bin "
        + "-fMCM.zip=1.2,1.3 -fPSMBoardNXP.bin=0.2 "
        + "-c=ukpn.xml "
        + "-c=G3Config.gz "
        + "-v=1.0.0\n";
    out.println(footer);
    out.flush();
  }

  public Properties getFeatures() {
    return getProperty(OPTION_FEATURE);
  }
}
