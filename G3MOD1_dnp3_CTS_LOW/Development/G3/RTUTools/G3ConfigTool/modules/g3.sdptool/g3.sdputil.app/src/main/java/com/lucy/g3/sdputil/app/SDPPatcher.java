
package com.lucy.g3.sdputil.app;

import java.io.File;
import java.io.IOException;

import org.zeroturnaround.zip.FileSource;
import org.zeroturnaround.zip.ZipUtil;

public class SDPPatcher {

  public static void patchLighttpdUser(File sdpFileOld, File sdpFileNew, File lighttpdUserFile) throws InterruptedException,
      IOException {
    final File mcmZip = File.createTempFile("MCM", ".zip", new File("."));

    try {
      // Extract MCM
      ZipUtil.unpackEntry(sdpFileOld, "MCMBoard.zip", mcmZip);

      // Update content of MCM
      FileSource[] zipsources = new FileSource[1];
      zipsources[0] = new FileSource("config/lighttpd.users", lighttpdUserFile);

      ZipUtil.addOrReplaceEntries(mcmZip, zipsources);

      // Replaced MCM file in SDP.
      if (sdpFileNew == null)
        ZipUtil.replaceEntry(sdpFileOld, "MCMBoard.zip", mcmZip);
      else
        ZipUtil.replaceEntry(sdpFileOld, "MCMBoard.zip", mcmZip, sdpFileNew);

    } catch (Throwable e1) {
      throw e1;
    } finally {
      // Delete temp file always.
      if (mcmZip != null) {
        mcmZip.delete();
      }
    }
  }
}
