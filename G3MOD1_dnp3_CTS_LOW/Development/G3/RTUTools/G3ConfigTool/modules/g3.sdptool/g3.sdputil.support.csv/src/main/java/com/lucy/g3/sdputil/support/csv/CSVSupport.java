/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.support.csv;

import java.io.IOException;
import java.util.List;

import com.lucy.g3.sdp.model.ISDPEntry;

/**
 * This class provides a convenient method to export software entries to a csv file.
 */
public class CSVSupport {

  public static String generateCSV(List<ISDPEntry> entries, String target, String version)
      throws IOException {
    CSVWriter writer;
    if (target == null || target.isEmpty())
      writer = new CSVWriter();
    else {
      target = target.toLowerCase().endsWith(".csv") ? target : target + ".csv";
      writer = new CSVWriter(target);
    }
    writer.generate(entries, version);
    return target == null ? CSVWriter.DEFAULT_FILE_NAME : target;
  }
}
