/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.support.csv;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import com.SDPSchema.ModuleSoftwareType;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.impl.FeatureVersionFactory;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;

/**
 * The Class CSVWriter.
 */
public class CSVWriter {

  private static final String[] HEADER = getHeader();
  private static final String LINE_SEPARATOR = "\n";
  public static final String DEFAULT_FILE_NAME = "sdp.csv";

  private FileWriter writer;
  private CSVPrinter doc;


  public CSVWriter() throws IOException {
    this(DEFAULT_FILE_NAME);
  }

  public CSVWriter(String targetFile) throws IOException {
    this(new File(targetFile));
  }

  public CSVWriter(File target) throws IOException {
    writer = new FileWriter(target);
    CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator(LINE_SEPARATOR);
    doc = new CSVPrinter(writer, csvFormat);
  }

  private static String[] getHeader() {

    List<String> header = new ArrayList<String>();
    header.add("SVNVersion");
    // get a list of attributes from class - use of reflection
    for (Field field : ModuleSoftwareType.class.getDeclaredFields()) {
      header.add(field.getName());
    }

    // additional setup for features
    int pos;
    if ((pos = header.indexOf("SupportedFeature")) >= 0) {
      header.set(pos, "FeatureMajor");
      header.add("FeatureMinor");
    }
    header.add("AdditionalFeatures");

    // add header sorting - remember to change the order of insert
    Collections.sort(header, new Comparator<String>() {

      @Override
      public int compare(String a, String b) {
        return 0;
      }
    });

    return header.toArray(new String[header.size()]);
  }

  public void generate(List<ISDPEntry> entryList, String version) throws IOException {

    try {
      doc.printRecord((Object[]) HEADER);

      for (ISDPEntry e : entryList) {
        if(e == null || !(e instanceof SoftwareEntry))
          continue;
        SoftwareEntry entry = (SoftwareEntry) e;
        
        List<String> record = new ArrayList<String>();
        record.add(version);
        record.add(String.valueOf(entry.getModuleType().getValue()));
        record.add(String.valueOf(entry.getSystemAPI().major()));
        record.add(String.valueOf(entry.getSystemAPI().minor()));
        record.add(String.valueOf(entry.getVersion().relType().getValue()));
        record.add(String.valueOf(entry.getVersion().major()));
        record.add(String.valueOf(entry.getVersion().minor()));
        record.add(String.valueOf(entry.getVersion().patch()));
        record.add(String.valueOf(entry.getEntryName()));
        record.add(String.valueOf(entry.getSlaveImageType().getValue()));

        record.add(String.valueOf(entry.getDefaultFeature().major()));
        record.add(String.valueOf(entry.getDefaultFeature().minor()));
        List<FeatureVersion> additionalFeatures = entry.getAdditionalFeatures();
        if (!additionalFeatures.isEmpty()) {
          // remove the first element from the list
          String features = FeatureVersionFactory.createCommaSeparatedStr(additionalFeatures);
          record.add(features.trim());
        }
        doc.printRecord(record);

      }

    } catch (IOException e) {
      throw e;
    } finally {
      try {
        writer.flush();
        writer.close();
      } catch (IOException e) {
        throw e;
      }
    }

  }

}
