/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.support.legacy;

import java.io.File;

import org.junit.Test;

import com.lucy.g3.test.support.resources.TestResources;


public class LegacySupportTest {

  @Test
  public void testConvertToLegacyFormat() throws Exception {
    File sdp = TestResources.getFile("SDP.zip");
    LegacySupport.convertToLegacyFormat("mainfest.xml", sdp);
    System.out.println("Converted SDP: " + sdp.getAbsolutePath());
  }

}

