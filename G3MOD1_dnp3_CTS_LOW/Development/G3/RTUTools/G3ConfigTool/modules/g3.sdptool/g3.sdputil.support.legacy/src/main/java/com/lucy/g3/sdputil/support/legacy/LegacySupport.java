/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdputil.support.legacy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.zeroturnaround.zip.ZipUtil;


/**
 * Class provides sufficient support for generating SDP package to older
 * versions of Configuration Tool.
 *
 * @author george_j
 */
public final class LegacySupport {
  public static final String SDP_XML_NAME = "SDP.xml";

  private LegacySupport(){}
  
  private static void transformXML(Document doc) {
    NodeList nl = doc.getDocumentElement().getChildNodes();
    for (int i = 0; i < nl.getLength(); i++) {
      if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
        continue;
      Element el = (Element) nl.item(i);

      // remove configuration file entries
      if (el.getNodeName().equals("ConfigFile"))
        el.getParentNode().removeChild(el);

      // remove entries by image type and the attribute itself
      if (el.getNodeName().equals("ModuleSoftware")) {
        if (el.hasAttribute("SlaveImageType"))
          el.removeAttribute("SlaveImageType");
      }
    }
  }

  public static void convertToLegacyFormat(String SDPXmlName, File sdpFile) 
        throws Exception {
    byte[] sdpXml = ZipUtil.unpackEntry(sdpFile, SDPXmlName);
    
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true);
    DocumentBuilder builder = factory.newDocumentBuilder();
    ByteArrayInputStream in = new ByteArrayInputStream(sdpXml);
    Document doc = builder.parse(in);
    
    transformXML(doc);
    
    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    Result output = new StreamResult(out);
    Source input = new DOMSource(doc);

    transformer.transform(input, output);
    
    ZipUtil.replaceEntry(sdpFile, SDPXmlName, out.toByteArray());
  }
}
