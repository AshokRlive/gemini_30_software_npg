/*******************************************************************************
 * aLucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.manager;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;

/**
 * The manager of G3RTU instances.
 */
public final class G3RTUManager extends AbstractSubsystem {
  public final static String SUBSYSTEM_ID = "G3RTUManager";

  private final G3RTU defaultRTU = G3RTUFactory.getDefault();
  
  public G3RTU getDefaultRTU(){
    return defaultRTU;
  }
  
  public G3RTUManager(IContext context) {
    super(context, SUBSYSTEM_ID);
  }

  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    getDefaultRTU().setState(newState);
  }
}

