/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.manager;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;

import javax.swing.ImageIcon;

import com.lucy.g3.common.bean.Bean;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.client.Session;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * This bean class holds all communication modules.
 */
public final class G3RTU extends Bean {

  public static final String SYSTEM_PROPERTY_ZMQ_ENABLED = "ZMQEnabled";

  // Icon Resources
  private static final ImageIcon ICON_OFFLINE = getImg("rtuOffline24.png");
  private static final ImageIcon ICON_OUTSERVICE = getImg("rtuStatus_outservice24.png");
  private static final ImageIcon ICON_ONLINE = getImg("rtuOnline24.png");

  public static final String PROPERTY_ICON = "icon";

  private ImageIcon icon; // The icon of this RTU.

  private final RTUCommsService comms;

  public G3RTU(String hostIP) {
    comms = new RTUCommsService(hostIP);
    
    // Bind icon
    comms.getRtuStatus().addPropertyChangeListener(
        RTUStatus.PROPERTY_SERVICE_MODE,
        new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            updateIcon();
          }
        });
    updateIcon();
  }
  
  public RTUCommsService getComms(){
    return comms;
  }

  public String getHost() {
    return comms.getHost();
  }

  public void setHost(String address) {
    comms.setHost(address);
  }

  public void setConnectTimeout(int timeoutMs) {
    comms.setConnectTimeout(timeoutMs);
  }

  public ImageIcon getIcon() {
    return icon;
  }

  private void setIcon(ImageIcon icon) {
    Object oldValue = getIcon();
    this.icon = icon;
    firePropertyChange(PROPERTY_ICON, oldValue, icon);
  }

  private void updateIcon() {
    setIcon(createRTUIcon(getConnectionState(), getServiceMode()));
  }
  
  public CTH_RUNNINGAPP getConnectionState() {
    return comms.getRTURunningMode();
  }
  
  
  public boolean isDoorOpen() {
    return comms.getRtuStatus().isDoorOpen();
  }

  public SERVICEMODE getServiceMode(){
    return comms.getRtuStatus().getServiceMode();
  }
  
  void setState(ConnectionState newState) {

    if (ConnectionState.DISCONNECTED == newState) {
      comms.clearAll();
    } else if (ConnectionState.CONNECTION_LOST == newState) {
      comms.clearForCommsLost();
    }
    
    updateIcon();
  }
  

  private static ImageIcon createRTUIcon(CTH_RUNNINGAPP state, SERVICEMODE mode) {
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      if (mode == SERVICEMODE.SERVICEMODE_OUTSERVICE) {
        return ICON_OUTSERVICE;
      } else {
        return ICON_ONLINE;
      }

    } else if(state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      return ICON_OUTSERVICE;

    } else {
      return ICON_OFFLINE;
    }
  }

  /**
   * Get an image icon defined in properties.
   * <p>
   * An empty imageIcon will be returned if the image resource is unavailable.
   * </p>
   *
   * @param urlKey
   *          the icons URL key
   */
  private static ImageIcon getImg(String name) {
    URL res = G3RTU.class.getResource("resources/"+name);
    if (res == null) {
      throw new RuntimeException("Couldn't find any image resource:" + name);
    }
    
    return new ImageIcon(res);
  }
}
