
package com.lucy.g3.rtu.manager;

import com.lucy.g3.rtu.comms.service.RTUCommsService;

public class G3RTUFactory {
  private G3RTUFactory(){}
  
  public static G3RTU createDefault(String ipAddress) {
    G3RTU rtu = new G3RTU(ipAddress);
    return rtu;
  }


  private static final G3RTU defaultRTU = new G3RTU("0.0.0.0");


  /**
   * Gets default RTU instance.
   * @return
   */
  public static G3RTU getDefault() {
    return defaultRTU;
  }
  
  /**
   * Gets the comms of default RTU.
   * @return
   */
  public static RTUCommsService getComms() {
    return getDefault().getComms();
  }
}
