/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.rtu.comms.service.realtime.modules.RTUModules;
import com.lucy.g3.rtu.comms.service.realtime.points.ChannelData;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointValueFormatter;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.service.rtu.events.EventEntry;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogsAPI;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.ModuleRef;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.test.support.CommsTestSupport;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/*
 * This test is only for development.
 * To run this test, an RTU needs to be prepared and the HOST address needs to be modified.
 */
public class G3RTUTest {

  private static RTUCommsService rtu;
  private static RTUPoints  rtuPoints   ;
  private static RTUInfo    rtuInfo       ;
  private static RTUModules rtuModules ;
  private static RTUStatus  rtuStatus   ;
  private static RTULogsAPI rtuLogs       ;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    rtu = new G3RTU(CommsTestSupport.HOST).getComms();
    rtuPoints = rtu.getRtuPoints();   
    rtuInfo = rtu.getRtuInfo();         
    rtuModules = rtu.getRtuModules();
    rtuStatus = rtu.getRtuStatus();   
    rtuLogs = rtu.getRtuLogs();         
    Logger.getRootLogger().setLevel(Level.DEBUG);
  }

  @AfterClass
  public static void teardownAfterClass() throws Exception {
    rtu = null;
  }

  @Test
  public void testCMDCheckAlive() throws Exception {
    assertNotNull(rtu.cmdCheckAlive());
  }

  @Test
  public void testCMDGetModueInfo() throws Exception {
    rtuModules.cmdGetModueInfo();
  }

  @Test
  public void testCMDGetRTUInfo() throws Exception {
    rtuInfo.cmdGetRTUInfo();
  }

  @Test
  public void testCMDGetNetInfo() throws Exception {
    rtuInfo.cmdGetNetInfo();
  }

  @Test
  public void testCMDGetSystemAPI() throws Exception {
    G3ConfigAPI api = rtuInfo.cmdGetConfigAPI();
    System.out.println("G3ConfigAPI: " + api.toHTML());
  }

  @Test
  public void testCMDAuthenticate() throws Exception {
    for (int i = 0; i < 1; i++) {
      LoginResult rtn = rtu.getLogin().cmdLogin("Admin", "".toCharArray());
      Assert.assertNotNull(rtn);
    }
  }

  @Test
  public void testCMDComissionModules() throws Exception {
    rtu.getRtuControl().cmdRegisterModules();
  }

  @Test
  public void testCMDGetServiceStatus() throws Exception {
    rtuStatus.cmdGetServiceStatus();
  }

  @Test
  public void testCMDSetServiceEnabled() throws Exception {
    rtuStatus.cmdSetServiceEnabled(true, null);
    assertEquals(SERVICEMODE.SERVICEMODE_INSERVICE, rtuStatus.cmdGetServiceStatus());
  }

  @Test
  public void testCMDSetServiceDisabled() throws Exception {
    rtuStatus.cmdSetServiceEnabled(false, null);
    assertEquals(SERVICEMODE.SERVICEMODE_OUTSERVICE, rtuStatus.cmdGetServiceStatus());
  }

  @Test
  public void testCMDGetOLR() throws Exception {
    rtuStatus.cmdGetOLR();
  }

  @Test
  public void testCMDSetOLR() throws Exception {
    Assume.assumeTrue(CommsTestSupport.TEST_NORMAL_API);
    Assume.assumeTrue(CommsTestSupport.TEST_SET_OLR);
    OLR_STATE[] states = OLR_STATE.values();
    for (OLR_STATE state : states) {
      rtuStatus.cmdSetOLR(state);
      assertEquals(state, rtuStatus.cmdGetOLR());
    }
  }

  @Test
  public void testCMDTestFan() throws Exception {
    Assume.assumeTrue(CommsTestSupport.TEST_LOGIC_GROUP_FAN >= 0 );
    byte period = 10;
    rtu.getRtuControl().cmdOperateLogic((short) CommsTestSupport.TEST_LOGIC_GROUP_FAN, period);
  }

  @Test
  public void testCMDGetDateTime() throws Exception {
    assertNotNull(rtuStatus.cmdGetDateTime());
  }

  @Test
  public void testCMDSetDateTime() throws Exception {
    Date date = Calendar.getInstance().getTime();
    System.out.println("Test Set date: " + date);
    rtuStatus.cmdSetDateTime(date);

    date = rtuStatus.cmdGetDateTime();
    System.out.println("Get date: " + date + "\nTime Synched: " + rtuStatus.getTimeSynched());
    assertNotNull(date);
  }

  @Test
  public void testCMDGetLogLevel() throws Exception {
    assertNotNull(rtuLogs.cmdGetLogLevel());
  }

  @Test
  public void testCMDSetLogLevel() throws Exception {
    rtuLogs.cmdSetLogLevel("MCMApplication", "WARN");
  }

  @Test
  public void testCMDGetLatestLogs() throws Exception {
    assertNotNull(rtuLogs.cmdGetLatestLogs());
  }

  @Test
  public void testCMDGetLogFileName() throws Exception {
    String name = rtu.getRtuLogs().cmdGetLogFileName();
    System.out.println("Log File name:" + name);
    assertNotNull(name);
  }

  @Test
  public void testCMDGetLatestEvents() throws Exception {
    Collection<EventEntry> events = rtu.getRtuEvents().cmdGetLatestEvents();
    assertNotNull(events);
    System.out.println("Receive event size: " + events.size());
  }

  @Test
  public void testCMDGetAlarm() throws Exception {
    long start = System.currentTimeMillis();

    ModuleAlarm[] alarms = rtu.getRtuModules().cmdGetModueAlarm(
        MODULE.MODULE_PSM, MODULE_ID.MODULE_ID_0
        );

    if (alarms != null) {
      for (int i = 0; i < alarms.length; i++) {
        System.out.println(alarms[i].getPrintText());
      }
    }

    long end = System.currentTimeMillis();
    System.out.println("Time cost: " + (end - start));
  }


  @Test
  public void testCMDGetUpradeStatus() throws Exception {
    Assume.assumeTrue(CommsTestSupport.TEST_UPDATE_API);
    assertNotNull(rtu.getFwUpgrade().cmdGetUpradeStatus());
  }

  @Test
  public void testCMDOperateSwitch() throws Exception {
    Assume.assumeTrue(CommsTestSupport.TEST_NORMAL_API);
    Assume.assumeTrue(CommsTestSupport.TEST_LOGIC_GROUP_SWITCH >= 0);

    rtuStatus.cmdSetOLR(OLR_STATE.OLR_STATE_LOCAL);

    int switchlogicID = CommsTestSupport.TEST_LOGIC_GROUP_SWITCH; 

    SWITCH_OPERATION openClose = SWITCH_OPERATION.SWITCH_OPERATION_CLOSE;
    LocalRemoteCode localRemote = LocalRemoteCode.LOCAL;
    rtu.getRtuControl().cmdOperateSwitch((short) switchlogicID, openClose, localRemote);
  }

  @Ignore // Manual test
  @Test
  public void testCMDEraseModules() throws Exception {
    ArrayList<ModuleRef> refs = new ArrayList<ModuleRef>();
    refs.add(new ModuleRef(MODULE.MODULE_SCM, MODULE_ID.MODULE_ID_0, 0));
    ArrayList<ModuleRef> erased = rtu.getFwUpgrade().cmdEraseModules(refs);
    assertEquals(1, erased.size());
    assertEquals(MODULE.MODULE_SCM, erased.get(0).type);
  }

  // ======================= RTUPoints Command Test =========================

  @Test
  public void testCMDSelectPoints() throws Exception {
    VirtualPointData[] points = null;
    rtuPoints.cmdSelectPoints(points, G3Protocol.PollingMode.ANALOG);
    rtuPoints.cmdSelectPoints(points, G3Protocol.PollingMode.DIGITAL);
  }

  @Test
  public void testCMDClearSelectedPoints() throws Exception {
    rtuPoints.cmdClearSelectedPoints(G3Protocol.PollingMode.ANALOG);
    rtuPoints.cmdClearSelectedPoints(G3Protocol.PollingMode.DIGITAL);
  }

  @Test
  public void testCMDPollPointStatus() throws Exception {
    rtuPoints.cmdPollPointStatus(G3Protocol.PollingMode.ANALOG, false);
    rtuPoints.cmdPollPointStatus(G3Protocol.PollingMode.DIGITAL, false);
  }

  @Test
  public void testCMDGetPointStatus() throws Exception {
    Formatter format = new Formatter();
    VirtualPointData p0 = new VirtualPointData.DIPointData(format, "BINARY_INPUT", "", "", 0, 353, "Backplane-Inp", "", "");
    VirtualPointData p1 = new VirtualPointData.DIPointData(format, "BINARY_INPUT", "", "", 0, 355, "Door switch", "", "");
    ArrayList<IPointData> list = new ArrayList<IPointData>();
    list.add(p0);
    list.add(p1);
    assertNull(p0.getValue());
    assertNull(p1.getValue());
    rtuPoints.cmdGetPointStatus(list, G3Protocol.PollingMode.DIGITAL);
  }

  @Test
  public void testCMDGetChannelValue() throws Exception {
    ArrayList<ChannelData> sampleChs = new ArrayList<ChannelData>();
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 0, (byte) 0, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 0, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 1, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 2, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 3, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 4, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 5, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 6, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 7, "", ""));
    sampleChs.add(new ChannelData((byte) 0, (byte) 0, (byte) 1, (byte) 8, "", ""));
    rtuPoints.cmdPollingChannelValue(sampleChs);

    for (ChannelData ch : sampleChs) {
      System.out.println(ch);
    }
  }

  @Test
  public void testCMDGetFileList() throws IOException, SerializationException {
    //System.out.println("FileList:" + rtu.getFileTransfer().cmdGetLogFileName());
    System.out.println("FileList:" + Arrays.toString(rtu.getFileTransfer().cmdGetFileList("./lib/automation/")));
  }
  private static class Formatter implements IPointValueFormatter {

    @Override
    public String format(IPointData source, Number value, boolean showUnit) {
      return "No Implemented";
    }
    
  }
}
