/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.importer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.sdp.importer.SdpImporter;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPFinder;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;

import org.junit.Assert;

public class SDPImporterTest {

  final static public URL SAMPLE_SDP = SDPImporterTest.class.getClassLoader().getResource("sample_sdp.zip");
  final static public URL SAMPLE_SDP_WITHOUT_XML = SDPImporterTest.class.getClassLoader().getResource(
      "sdp_without_xml.zip");
  final static public URL SAMPLE_SDP_INVALID_XML = SDPImporterTest.class.getClassLoader().getResource(
      "sdp_invalid_xml.zip");


  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
  
  @Test
  public void testCreateSDPModel() {
    try {
      File sdpFile = new File(SAMPLE_SDP.toURI());
      SDP model = SdpImporter.createFromFile(sdpFile);
      assertEquals(sdpFile.getAbsolutePath(), model.getSDPFilePath());
    } catch (Exception e) {
      e.printStackTrace();
      fail("Fail to create ZipSDPModel: " + e.getMessage());
    }
  }

  @Test
  public void testGetOneSoftwareEntry() throws Exception {
    File file = new File(SAMPLE_SDP.toURI());
    SDP model = SdpImporter.createFromFile(file);

    SystemAPIVersion api = new SystemAPIVersion(0, 2);
    FeatureVersion feature = new FeatureVersion(0, 1);
    MODULE type = MODULE.MODULE_FDM;
    SoftwareVersion version = new SoftwareVersion(
        new ReleaseType(VERSION_TYPE.VERSION_TYPE_DEBUG.getDescription(),
            VERSION_TYPE.VERSION_TYPE_DEBUG.getValue()), 0, 1, 1);

    SoftwareEntry[] entries = SDPFinder.INSTANCE.findSoftwareEntries(model, api, feature, type);
    Assert.assertNotNull(entries);
    Assert.assertEquals(1, entries.length);
    Assert.assertEquals("FDMBoardNXP.bin", entries[0].getEntryName());
    Assert.assertEquals(api, entries[0].getSystemAPI());
    Assert.assertEquals(type, entries[0].getModuleType());
    Assert.assertEquals(version, entries[0].getVersion());

    boolean containFeature = false;
    List<FeatureVersion> features= entries[0].getAllFeatures();
    for (FeatureVersion f: features) {
      if (feature.equals(f)) {
        containFeature = true;
      }
    }
    Assert.assertTrue("The feature doesn't match", containFeature);

  }

  @Test
  public void testGetMultiSoftwareEntry() throws Exception {
    File file = new File(SAMPLE_SDP.toURI());
    SDP model = SdpImporter.createFromFile(file);

    SystemAPIVersion api = new SystemAPIVersion(0, 2);
    FeatureVersion feature = new FeatureVersion(0, 2);
    MODULE type = MODULE.MODULE_HMI;

    SoftwareEntry[] entries = SDPFinder.INSTANCE.findSoftwareEntries(model, api, feature, type);
    Assert.assertNotNull(entries);
    Assert.assertEquals(2, entries.length);
    for (int i = 0; i < entries.length; i++) {
      Assert.assertEquals(type, entries[i].getModuleType());
      Assert.assertNotNull(entries[i].getSystemAPI());
      Assert.assertTrue(entries[i].getSystemAPI().isCompatible(api));
    }

    boolean containFeature = false;
    List<FeatureVersion> features = entries[0].getAllFeatures();
    for (FeatureVersion f: features) {
      if (feature.equals(f)) {
        containFeature = true;
      }
    }
    Assert.assertTrue("The feature doesn't match", containFeature);

  }

}
