
package com.lucy.g3.sdp.importer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.xml.bind.ValidationException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.LocalStorage;
import org.zeroturnaround.zip.ZipUtil;

import com.SDPSchema.SDPSchema2;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPEntryType;
import com.lucy.g3.sdp.model.WritableSDP;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.sdp.model.impl.WritableSDPImpl;

public class SdpImporter {
  
  public static void loadFromFile(WritableSDP sdp, File sdpFile) throws IOException, ValidationException {
    if (!sdpFile.exists())
      throw new IOException("SDP file not found:" + sdpFile.getAbsolutePath());

    try {
      if (ZipUtil.containsEntry(sdpFile, SDP.MANIFEST_FILE_NAME) == false) {
        throw new ValidationException("Invalid SDP file: missing manifest file! " + sdpFile.getAbsolutePath());
      }
    } catch (Exception e) {
      throw new ValidationException("Invalid SDP file", e);
    }
    
//    Path tempDirPath = Paths.get(System.getProperty("user.dir"));
    final Path tempDirPath = Paths.get(Application.getInstance().getContext()
        .getLocalStorage().getDirectory().getAbsolutePath()+"/SDP.tmp/");
    final File tempDir = new File(tempDirPath.toUri());
    tempDir.mkdirs();
    
    final String tempSDPDir = Files.createTempDirectory(tempDirPath, ".sdp").toString();
    final File tempSDPDirFile = new File(tempSDPDir);
    
    // unZIP package
    ZipUtil.unpack(sdpFile, tempSDPDirFile);

    SdpManifestReader reader = new SdpManifestReader(ZipUtil.unpackEntry(sdpFile, SDP.MANIFEST_FILE_NAME));
    String revision = reader.readRevision();

    ArrayList<SoftwareEntry> swList = new ArrayList<>();
    ArrayList<ConfigFileEntry> configList = new ArrayList<>();

    swList.addAll(reader.readSoftwareEntries(tempSDPDirFile));
    configList.addAll(reader.readConfigEntries(tempSDPDirFile));

    // clean up
    FileUtils.forceDeleteOnExit(tempDir);
    
    sdp.setRevision(revision);
    sdp.setSDPEntries(SDPEntryType.SOFTWARE, swList);
    sdp.setSDPEntries(SDPEntryType.CONFIG, configList);
    sdp.setSDPFilePath(sdpFile.getAbsolutePath());
  }
  
  public static WritableSDP createFromFile(File sdpFile) throws IOException, ValidationException {
    if (!sdpFile.exists() || !sdpFile.isFile())
      throw new IOException("SDP file not found");

    WritableSDP sdp = new WritableSDPImpl();
    loadFromFile(sdp, sdpFile);

    return sdp;
  }

  public static String[] getConfigFileNames(File sdpFile) {
    SDPSchema2 manifest = getManifest(sdpFile);
    if(manifest == null)
      return null;
    
    com.SDPSchema.SDPContentType root = manifest.SDPContent.first();
    String[] configFileNames = new String[root.ConfigFile.count()];
    for (int i = 0; i < configFileNames.length; i++) {
      configFileNames[i] = root.ConfigFile.at(i).Location.getValue();
    }
    
    return configFileNames;
  }
  
  public static String getSDPRevision(File sdpFile){
    SDPSchema2 manifest = getManifest(sdpFile);
    if(manifest != null) {
      if(manifest.SDPContent.first().sdpversion.exists())
        return manifest.SDPContent.first().sdpversion.getValue();
    }
    
    return null;
  }
  
  public static com.SDPSchema.SDPSchema2 getManifest(File sdpFile){
   byte[] manifest = ZipUtil.unpackEntry(sdpFile, SDP.MANIFEST_FILE_NAME);
    
    if(manifest == null || manifest.length == 0)
      return null;
    
    try {
      return com.SDPSchema.SDPSchema2.loadFromBinary(manifest);
    } catch (Exception e) {
      Logger.getLogger(SdpImporter.class).error("Invalid SDP manifest file!");
      return null;
    }
  }
}

