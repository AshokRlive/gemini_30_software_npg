/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.importer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.ValidationException;

import org.apache.log4j.Logger;

import com.SDPSchema.ConfigFileType;
import com.SDPSchema.ModuleSoftwareType;
import com.SDPSchema.SDPContentType;
import com.SDPSchema.SDPSchema2;
import com.SDPSchema.SupportedFeatureType;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.schema.support.SDPSchemaFile;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;
import com.lucy.g3.xml.validation.XmlValidate;

/**
 * Defines methods to read SDP package XML file.
 */
public class SdpManifestReader {
  private Logger log = Logger.getLogger(SdpManifestReader.class);
  
  private SDPSchema2 document;
  
  public SdpManifestReader(byte[] sdpXmlBytes) throws IOException, ValidationException {
    if(sdpXmlBytes == null)
    throw new IllegalArgumentException("sdpXmlBytes must not be null");
    
    try {
      document = com.SDPSchema.SDPSchema2.loadFromBinary(sdpXmlBytes);
    } catch (Exception e) {
      throw new IOException("Failed to parse XML, please provide a valid XML file.\n", e);
    }
    
  }
  
  public SdpManifestReader(File file) throws IOException, ValidationException {
    if(file == null)
      throw new IllegalArgumentException("file must not be null");
    
    // validate XML 
    try {
      document = com.SDPSchema.SDPSchema2.loadFromFile(file.getAbsolutePath());
    } catch (Exception e) {
      throw new IOException("Failed to parse XML, please provide a valid XML file.\n", e);
    }
    
    validateXML(file);
  }
  
  /**
   * Validate input XML file to schema.
   * @param file XML File
   * @throws Exception when validation fails
   */
  private void validateXML(File file) throws ValidationException {
    //com.lucy.g3.configtool.common.xml.XmlValidator
    XmlValidate validate = new XmlValidate(SDPSchemaFile.FILE_PATH);
    try {
      validate.validate(file.getAbsolutePath());
    } catch (Exception e) {
      throw new ValidationException("XML file does not validate the given schema", e);
    }
  }
  
  /**
   * Read XML elements of ConfigFile.
   * @return list of {@link ConfigFileEntry}
   */
  public List<ConfigFileEntry> readConfigEntries(File configFileDir) {
    List<ConfigFileEntry> configList = new ArrayList<ConfigFileEntry>();
    SDPContentType root = document.SDPContent.first();    
    
    
    Iterator<?> iterator = root.ConfigFile.iterator();
    while (iterator.hasNext()) {
      
      ConfigFileType type =  (ConfigFileType) iterator.next();
      
      int major = (int) type.ConfigAPIMajor.getValue();
      int minor = (int) type.ConfigAPIMinor.getValue();
      
      BasicVersion version = new BasicVersion(major, minor);
      
      File file = new File(configFileDir, type.Location.getValue());
      ConfigFileEntry configEntry;
      try {
        configEntry = ConfigFileEntry.createFromFile(file);
        
        if (!version.equals(configEntry.getVersion())) {
          log.error("Mismatched config version for :" + file);
        }
        
        configList.add(configEntry);
        
      } catch (IOException e) {
        log.error("Config file not found",e);
      }
    }
    
    return configList;
  }
  
  public String readRevision() {
    SDPContentType root = document.SDPContent.first();
    if (root.sdpversion.exists())
      return root.sdpversion.getValue();
    else
      return null;
  }
  
  /**
   * Read XML elements for module entries.
   * @return list of  {@link SoftwareEntry}
   */
  public List<SoftwareEntry> readSoftwareEntries(File binFileDir)  {
    
    List<SoftwareEntry> sdpList = new ArrayList<SoftwareEntry>();
    SDPContentType root = document.SDPContent.first();    
    
    
    Iterator<?> iterator = root.ModuleSoftware.iterator();
    while (iterator.hasNext()) {
      // compile node data to object types
      // To create a SDP entry      
      ModuleSoftwareType mod = (ModuleSoftwareType) iterator.next();
      
      MODULE mtype = MODULE.forValue(mod.ModuleType.getValue());
      
      SLAVE_IMAGE_TYPE slaveFile = null;
      if (mod.SlaveImageType.exists()) {
        slaveFile = SLAVE_IMAGE_TYPE.forValue(mod.SlaveImageType.getValue());
      }
      
      VERSION_TYPE relType = VERSION_TYPE.forValue(mod.RelType.getValue());
      
      SoftwareVersion version = new SoftwareVersion( new ReleaseType(relType.getDescription(), relType.getValue()), 
            (int)mod.Major.getValue(), 
            (int)mod.Minor.getValue(),
            (int)mod.Patch.getValue());
      
      // create file
      String entryName = mod.FileName.getValue();
      File tempFile = new File(binFileDir, entryName);
              
      SystemAPIVersion systemAPI = new SystemAPIVersion((int)mod.APIMajor.getValue(), 
          (int) mod.APIMinor.getValue());
      
      // obtain first feature version > from file 
      SupportedFeatureType feature = mod.SupportedFeature.first();
      FeatureVersion fversion = new FeatureVersion((int) feature.Major.getValue(),
           (int)feature.Minor.getValue());
      
      // create module instance
      SoftwareEntry entry = new SoftwareEntry(entryName, mtype, version, systemAPI, slaveFile, fversion);
      entry.setEntryFile(tempFile);
      
      List<FeatureVersion> featureList = new ArrayList<>();
      Iterator<?> iteratorFeature = mod.SupportedFeature.iterator();
      iteratorFeature.next(); // skip first one
      
      while (iteratorFeature.hasNext()) {
        feature = (SupportedFeatureType) iteratorFeature.next();
        FeatureVersion bv = new FeatureVersion((int)feature.Major.getValue(), (int)feature.Minor.getValue());
        featureList.add(bv);
        
      }
      entry.addAdditionalFeatures(featureList);
      sdpList.add(entry);      
    }
    
    return sdpList;
  }

}
