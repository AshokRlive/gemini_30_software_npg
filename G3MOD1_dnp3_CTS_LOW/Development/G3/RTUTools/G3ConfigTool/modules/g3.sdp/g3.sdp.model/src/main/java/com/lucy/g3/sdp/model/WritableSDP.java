
package com.lucy.g3.sdp.model;

import java.util.Collection;

import com.lucy.g3.sdp.model.impl.SoftwareEntry;

public interface WritableSDP extends SDP {

  void setRevision(String revision);

  void addEntry(SDPEntryType type, ISDPEntry entry) throws IllegalArgumentException;

  void removeEntry(ISDPEntry entry);

  void removeAllEntries(Collection<? extends ISDPEntry> entriesToBeRemoved);

  void setSDPEntries(SDPEntryType entryType, Collection<? extends ISDPEntry> entryList);

 
}
