/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model.impl;

import java.io.File;
import java.io.IOException;

/**
 * SDP entry for other type of items.
 */
public class GenericItemEntry extends AbstractEntry {
  
  public static final String SDP_DIR_CONFIGURATION = "other";
  
  private final String description; 
  

  public GenericItemEntry(File file, String description) {
    super(file);
    this.description = description;
  }
  
  public String getDescription() {
    return description;
  }

  @Override
  public String printToStr() {
    StringBuilder str = new StringBuilder("Configuration ");
    str.append("file: " + getEntryName());
    if (description != null && !description.isEmpty()) {
      str.append(System.getProperty("line.separator"));
      str.append("Description: ");
      str.append(description);
    }
    return str.toString();
  }
  
  public static GenericItemEntry createFromFile(File configFile, String description) throws IOException  {
    return new GenericItemEntry(configFile, description);
  }
  
}

