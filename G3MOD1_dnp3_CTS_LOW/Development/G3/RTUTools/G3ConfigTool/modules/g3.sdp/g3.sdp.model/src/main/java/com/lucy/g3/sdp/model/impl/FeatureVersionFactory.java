/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.common.version.FeatureVersion;

/**
 * A factory for creating FeatureVersion objects.
 */
public class FeatureVersionFactory {

  /**Creates a list of features from formatted string.
   * @param features
   *          format of string: [SOFTWARE NAME]=FEATURE1, FEATURE2, ....
   * @return a list of features instances.
   * @throws ParseException if the features string is not valid.
   */
  public static List<FeatureVersion> createFromStr(String features) throws ParseException {

    List<FeatureVersion> featureVersions = new ArrayList<FeatureVersion>();
    try {
      String[] tempList = features.trim().split(",");
      for (int i = 0; i < tempList.length; i++) {
        BasicVersion ver = BasicVersion.createFromString(tempList[i]);
        FeatureVersion feature = new FeatureVersion(ver.major(), ver.minor());
        try {
          featureVersions.add(feature);
        } catch (IllegalArgumentException e) {
          throw new ParseException("parse error on " + tempList[i], 0);
        }
      }
    } catch (ParseException e) {
      throw e;
    } catch (Exception e) {
      e.printStackTrace();
      throw new ParseException("Invalid format of features:" + features, 0);
    }

    return featureVersions;
  }
  
  
  public static String createCommaSeparatedStr(List<FeatureVersion> features) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < features.size(); i++) {
      sb.append(features.get(i));
      if (i < features.size() - 1)
        sb.append(", ");
    }

    return sb.toString();
  }

}

