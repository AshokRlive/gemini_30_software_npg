
package com.lucy.g3.sdp.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.sdp.model.impl.SoftwareEntryFactory;

public class SoftwareEntryTest {

  final static String FW_FILE = "MCMBoard.zip";
  final static String FW_FILE_BORKEN = "MCMBoard-broken.zip";
  final static String FW_FILE_BORKEN2 = "MCMBoard-broken2.zip";


  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCreateMCMEntryFromFile() {
    URL res = getClass().getClassLoader().getResource(FW_FILE);
    try {
      assertNotNull(SoftwareEntryFactory.create(new File(res.toURI())));
    } catch (Exception e) {
      fail("Fail to create MCM entry from property file: " + e.getMessage());
    }
  }

  @Test
  public void testCreateMCMEntryFromFile2() {
    URL res = getClass().getClassLoader().getResource(FW_FILE_BORKEN);
    try {
      SoftwareEntryFactory.create(new File(res.toURI()));
      fail("Exception expected when load broken MCM firmware");
    } catch (Exception e) {
      System.out.println("Test Result: " + e.getMessage());
    }
  }

  @Test
  public void testCreateMCMEntryFromFile3() {
    URL res = getClass().getClassLoader().getResource(FW_FILE_BORKEN2);
    try {
      SoftwareEntryFactory.create(new File(res.toURI()));
      fail("Exception expected when load broken MCM firmware");
    } catch (Exception e) {
      System.out.println("Test Result: " + e.getMessage());
    }
  }

}
