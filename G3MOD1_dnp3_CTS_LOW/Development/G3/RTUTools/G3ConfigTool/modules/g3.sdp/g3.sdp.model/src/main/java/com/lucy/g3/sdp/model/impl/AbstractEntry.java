/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model.impl;

import java.io.File;

import com.lucy.g3.sdp.model.ISDPEntry;

/**
 * Abstract implementation of ISDPEntry. 
 */
abstract class AbstractEntry implements ISDPEntry {

  private String entryName;
  private File entryFile; 

  AbstractEntry(File entryFile){
    this(entryFile.getName());
    setEntryFile(entryFile);
  }
  
  AbstractEntry(String entryName){
    if(entryName == null || entryName.trim().isEmpty())
      throw new IllegalArgumentException("entry name must not be blank");
    this.entryName = entryName;
  }
  
  public void setEntryFile(File file) {
    this.entryFile = file;
  }

  @Override
  public String getEntryName() {
    return entryName;
  }

  @Override
  public File getEntryFile() {
    return entryFile;
  }

  @Override
  public String toString() {
    return printToStr();
  }

  //================== Auto Generated ===================
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((entryFile == null) ? 0 : entryFile.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AbstractEntry other = (AbstractEntry) obj;
    if (getEntryName() == null) {
      if (other.getEntryName() != null)
        return false;
    } else if (!getEntryName().equals(other.getEntryName()))
      return false;
    return true;
  }
  //================== End of Auto Generated ===================
}
