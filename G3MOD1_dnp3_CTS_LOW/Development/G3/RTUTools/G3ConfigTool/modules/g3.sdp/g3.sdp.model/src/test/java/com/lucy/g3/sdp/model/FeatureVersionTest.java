/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.List;

import org.junit.Test;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.sdp.model.impl.FeatureVersionFactory;

public class FeatureVersionTest {

  @Test
  public void testParseFromStr() throws ParseException {
    List<FeatureVersion> features = FeatureVersionFactory.createFromStr("0.0, 2.3,4.4");
    
    
    assertEquals(new FeatureVersion(0,0), features.get(0));
    assertEquals(new FeatureVersion(2,3), features.get(1));
    assertEquals(new FeatureVersion(4,4), features.get(2));
  }

}

