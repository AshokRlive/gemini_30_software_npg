/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;

/**
 * This Class defines the attributes of Gemini3 module firmware entry in SDP.
 */
public class SoftwareEntry extends AbstractEntry {

  private final MODULE moduleType;

  private final SoftwareVersion version;

  private final SystemAPIVersion systemAPI;

  private final SLAVE_IMAGE_TYPE slaveImageType;

  private final FeatureVersion defaultFeature;
  
  private final List<FeatureVersion> additionalFeatureList = new ArrayList<FeatureVersion>();


  public SoftwareEntry(String entryName, MODULE moduleType, SoftwareVersion version, SystemAPIVersion systemAPI, 
      SLAVE_IMAGE_TYPE slaveType, FeatureVersion defaultFeature) {
    super(entryName);
    this.slaveImageType = slaveType;
    this.moduleType = moduleType;
    this.version = version;
    this.systemAPI = systemAPI;
    this.defaultFeature = defaultFeature;
  }
  
  public MODULE getModuleType() {
    return moduleType;
  }
  
  
  public SoftwareVersion getVersion() {
    return version;
  }

  
  public SystemAPIVersion getSystemAPI() {
    return systemAPI;
  }

  
  public SLAVE_IMAGE_TYPE getSlaveImageType() {
    return slaveImageType;
  }

  public boolean isBootloader() {
    return getSlaveImageType() ==  SLAVE_IMAGE_TYPE.SLAVE_IMAGE_TYPE_APP_BOOT_PROGRAM;
  }

  
  public List<FeatureVersion> getAdditionalFeatures() {
    return additionalFeatureList;
  }

  
  public FeatureVersion getDefaultFeature() {
    return defaultFeature;
  }

  public void addAdditionalFeatures(List<FeatureVersion> features) {
    for (FeatureVersion feature : features) {
      if (!additionalFeatureList.contains(feature)) {
        additionalFeatureList.add(feature);
      }
    }
  }

  public void setAdditionalFeatures(List<FeatureVersion> features) {
    additionalFeatureList.clear();
    additionalFeatureList.addAll(features);
  }

  
  public List<FeatureVersion> getAllFeatures() {
    ArrayList<FeatureVersion> flist = new ArrayList<>(additionalFeatureList);
    flist.add(0, defaultFeature);
    return flist;
  }
  
  public String getAllFeatureAsString() {
    StringBuilder sb = new StringBuilder();
    List<FeatureVersion> features = getAllFeatures();
    for (int i = 0; i < features.size(); i++) {
      sb.append(features.get(i));

      if (i < features.size()- 1) {
        sb.append(", ");
      }
    }

    return sb.toString();
  }

  @Override
  public String printToStr() {
    StringBuffer sb = new StringBuffer();
    sb.append("\n");
    sb.append("File: ");
    sb.append(getEntryName());
    sb.append("\n");

    sb.append("Type: ");
    sb.append(getModuleType());
    sb.append("\n");

    sb.append("System API: ");
    sb.append(getSystemAPI());
    sb.append("\n");

    sb.append("Slave Image Type: ");
    sb.append(getSlaveImageType());
    sb.append("\n");

    sb.append("Hardware Feature: ");
    List<FeatureVersion> flist = getAllFeatures();
    sb.append(Arrays.toString(flist.toArray()));
    sb.append("\n");

    sb.append("Software Version: ");
    sb.append(getVersion());

    sb.append("Supported Features: ");
    sb.append(FeatureVersionFactory.createCommaSeparatedStr(getAllFeatures()));

    return sb.toString();
  }
  
}
