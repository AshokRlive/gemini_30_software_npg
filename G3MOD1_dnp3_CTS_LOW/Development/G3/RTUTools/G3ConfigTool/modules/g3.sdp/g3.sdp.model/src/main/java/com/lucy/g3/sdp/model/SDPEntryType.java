
package com.lucy.g3.sdp.model;


public enum SDPEntryType {
  SOFTWARE,
  CONFIG,
  GENERIC
}

