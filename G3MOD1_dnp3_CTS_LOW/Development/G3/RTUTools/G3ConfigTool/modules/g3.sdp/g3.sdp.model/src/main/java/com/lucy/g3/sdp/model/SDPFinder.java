
package com.lucy.g3.sdp.model;

import java.util.ArrayList;
import java.util.List;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Utility class for finding contents in a SDP model.
 */
public class SDPFinder {
  public static final SDPFinder INSTANCE = new SDPFinder();
  
  private SDPFinder(){}

  public SoftwareEntry[] findSoftwareEntries(SDP sdp, SystemAPIVersion api, FeatureVersion feature, MODULE type) {
    if(sdp == null)
      return null;
    
    ArrayList<SoftwareEntry> compatibleFirmware = new ArrayList<SoftwareEntry>();
    List<ISDPEntry> allSoftwareEntries = sdp.getSDPEntries(SDPEntryType.SOFTWARE);

    for (ISDPEntry entry : allSoftwareEntries) {
      if (entry != null && entry instanceof SoftwareEntry
          && isCompatible((SoftwareEntry) entry, api, feature, type)) {
        compatibleFirmware.add((SoftwareEntry) entry);
      }
    }
    return compatibleFirmware.toArray(new SoftwareEntry[compatibleFirmware.size()]);
  }

  public SoftwareEntry[] findBootSoftwareEntries(SDP sdp, SystemAPIVersion api,
      FeatureVersion feature, MODULE type) {
    return findSoftwareEntries(sdp, feature, type, true);
  }

  public SoftwareEntry[] findSoftwareEntries(SDP sdp, FeatureVersion feature, MODULE type) {
    return findSoftwareEntries(sdp, feature, type, false);
  }

  private SoftwareEntry[] findSoftwareEntries(SDP sdp, FeatureVersion feature, MODULE type,
      boolean isBootloader) {
    if(sdp == null)
      return null;
    
    ArrayList<SoftwareEntry> compatibleFirmware = new ArrayList<SoftwareEntry>();

    List<ISDPEntry> entries = sdp.getSDPEntries(SDPEntryType.SOFTWARE);
    for (ISDPEntry entry : entries) {
      if (entry != null && entry instanceof SoftwareEntry) {
        SoftwareEntry sw = (SoftwareEntry) entry;
        if (sw.isBootloader() == isBootloader && isCompatible(sw, feature, type)) {
          compatibleFirmware.add(sw);
        }
      }
    }
    return compatibleFirmware.toArray(new SoftwareEntry[compatibleFirmware.size()]);
  }

  public boolean isCompatible(SoftwareEntry sw, SystemAPIVersion api, FeatureVersion feature,
      MODULE type) {
    if (sw.getModuleType() != type) {
      return false;
    }

    boolean featureSupported = false;
    List<FeatureVersion> allFeature = sw.getAllFeatures();
    for (FeatureVersion f : allFeature) {
      if (f.equals(feature)) {
        featureSupported = true;
        break;
      }
    }
    if (!featureSupported) {
      return false;
    }

    return sw.getSystemAPI().isCompatible(api);
  }

  public boolean isCompatible(SoftwareEntry sw, FeatureVersion feature, MODULE type) {
    if (sw.getModuleType() != type) {
      return false;
    }

    List<FeatureVersion> allFeature = sw.getAllFeatures();
    for (FeatureVersion f : allFeature) {
      if (f.equals(feature)) {
        return true;
      }
    }

    return false;
  }

}
