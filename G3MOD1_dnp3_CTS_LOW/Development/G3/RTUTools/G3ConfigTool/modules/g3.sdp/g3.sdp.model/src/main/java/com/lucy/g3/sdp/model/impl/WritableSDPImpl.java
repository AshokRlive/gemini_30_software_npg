
package com.lucy.g3.sdp.model.impl;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.SDPEntryType;
import com.lucy.g3.sdp.model.WritableSDP;

public class WritableSDPImpl extends SDPImpl implements WritableSDP{
  private Logger log = Logger.getLogger(WritableSDPImpl.class);

  @Override
  public void addEntry(SDPEntryType type, ISDPEntry entry) throws IllegalArgumentException {
    if (type == null)
      throw new IllegalArgumentException("The type must not be null");
    
    if (entry == null)
      throw new IllegalArgumentException("The entry must not be null");

    ArrayListModel<ISDPEntry> list = getModel(type);
    if (list.contains(entry)) {
      log.warn("Duplicate entry file (" + entry.getEntryName() + ") has been replaced");
      list.set(list.indexOf(entry), entry);
    } else {
      list.add(entry);
    }

  }
  
  @Override
  public void removeEntry(ISDPEntry entry) {
    SDPEntryType[] types = SDPEntryType.values();
    for (int i = 0; i < types.length; i++) {
      ArrayListModel<ISDPEntry> list = getModel(types[i]);
      if(list != null)
        list.remove(entry);
    }
  }

  @Override
  public void removeAllEntries(Collection<? extends ISDPEntry> entriesToBeRemoved) {
    SDPEntryType[] types = SDPEntryType.values();
    for (int i = 0; i < types.length; i++) {
      ArrayListModel<ISDPEntry> list = getModel(types[i]);
      if(list != null)
        list.removeAll(entriesToBeRemoved);
    }
  }

  
  @Override
  public void setRevision(String revision) {
    this.revision = revision;
  }
  
  @Override
  public void setSDPEntries(SDPEntryType entryType, Collection<? extends ISDPEntry> entryList) {
    ArrayListModel<ISDPEntry> list = getModel(entryType);
    list.clear();
    list.addAll(entryList);
  }

}

