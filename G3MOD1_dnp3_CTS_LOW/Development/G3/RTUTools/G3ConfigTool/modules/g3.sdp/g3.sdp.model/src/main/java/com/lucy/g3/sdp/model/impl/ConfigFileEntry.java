/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model.impl;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import com.lucy.g3.common.version.BasicVersion;

/**
 * Class defines attributes for RTU Configuration files within SDP.
 */
public class ConfigFileEntry extends AbstractEntry {
  
  private static final String SDP_DIR_CONFIGURATION = "config";
  
  private static final String ATTR_CONFIG_FILE_VERSION_MAJOR = "configVersionMajor";
  private static final String ATTR_CONFIG_FILE_VERSION_MINOR = "configVersionMinor";
  private static final String ATTR_CONFIG_FILE_DESCRIPTION   = "configFileDescription";
  
  private final BasicVersion version;
  private final String description; 
  

  public ConfigFileEntry(File file, BasicVersion version, String description) {
    super(file);
    this.description = description;
    this.version = version;
  }

  @Override
  public String getEntryName() {
    return SDP_DIR_CONFIGURATION + File.separator + super.getEntryName();
  }

  public BasicVersion getVersion() {
    return version;
  }
  
  public String getDescription() {
    return description;
  }

  @Override
  public String printToStr() {
    StringBuilder str = new StringBuilder("Configuration ");
    str.append("file: " + getEntryName() + " (" + version + ")");
    if (description != null && !description.isEmpty()) {
      str.append(System.getProperty("line.separator"));
      str.append("Description: ");
      str.append(description);
    }
    return str.toString();
  }
  
  
  /**
   * Create entry instance from file.
   * @param location Configuration XML file 
   * @return new {@link ConfigFileEntry} instance
   * @throws Exception Error occurred when parsing information from XML
   */
  public static ConfigFileEntry createFromFile(File configFile) throws IOException  {
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      Document document = db.parse(configFile);
      
      String major = document.getDocumentElement().getAttribute(ATTR_CONFIG_FILE_VERSION_MAJOR);
      String minor = document.getDocumentElement().getAttribute(ATTR_CONFIG_FILE_VERSION_MINOR);
      String description = document.getDocumentElement().getAttribute(ATTR_CONFIG_FILE_DESCRIPTION);
      
      return new ConfigFileEntry(configFile, BasicVersion.createFromString(major + "." + minor), description);
      
    } catch (Exception e) {
      throw new IOException("Failed to parse XML config file " + configFile.getName(), e);
    }    
  }
  
}

