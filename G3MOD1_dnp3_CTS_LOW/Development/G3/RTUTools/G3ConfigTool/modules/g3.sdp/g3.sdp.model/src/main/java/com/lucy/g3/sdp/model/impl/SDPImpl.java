
package com.lucy.g3.sdp.model.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPEntryType;

public class SDPImpl implements SDP{

  private Logger log = Logger.getLogger(SDPImpl.class);
  
  private final HashMap<SDPEntryType, ArrayListModel<ISDPEntry>> entriesMap = new HashMap<>();
  private String sdpFilePath;
  
  protected String revision;

  @Override
  public String getRevision() {
    return revision;
  }

  @Override @SuppressWarnings("unchecked")
  public ListModel<ISDPEntry> getSDPEntryListModel(SDPEntryType type) {
    return getModel(type);
  }
    
  protected ArrayListModel<ISDPEntry> getModel(SDPEntryType type) {
    ArrayListModel<ISDPEntry> list = entriesMap.get(type);
    if(list == null) { 
      list = new ArrayListModel<>();
      entriesMap.put(type, list);
    }
    
    return list;
  }
  
  @Override
  public List<ISDPEntry> getSDPEntries(SDPEntryType type) {
    return new ArrayList<>(getModel(type));
  }

  @Override
  public String getSDPFilePath(){
    return sdpFilePath;
  }
  
  @Override
  public boolean isEmpty() {
    SDPEntryType[] types = SDPEntryType.values();
    for (int i = 0; i < types.length; i++) {
      if(getSDPEntries(types[i]).isEmpty() == false)
        return false;
    }
    
    return true;
  }
  
  @Override
  public void setSDPFilePath(String sdpFilePath){
    this.sdpFilePath = sdpFilePath;
  }

  public ISDPEntry getEntryByName(SDPEntryType type, String softwareName) {
    List<ISDPEntry> entries = getSDPEntries(type);
    for (ISDPEntry e : entries) {
      if (softwareName.equals(e.getEntryName()))
        return e;
    }
    
    return null;
  }
}

