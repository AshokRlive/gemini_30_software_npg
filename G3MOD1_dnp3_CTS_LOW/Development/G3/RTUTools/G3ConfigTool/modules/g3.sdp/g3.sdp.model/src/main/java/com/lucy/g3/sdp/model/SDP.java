
package com.lucy.g3.sdp.model;

import java.util.List;

import javax.swing.ListModel;

import com.lucy.g3.sdp.model.impl.SoftwareEntry;

public interface SDP {
  String SDP_EXTENSION = ".sdp";
  String MANIFEST_FILE_NAME = "SDP.xml";

  String getSDPFilePath();
  
   /**
    * Gets SDP revision.
    * @return SDP revision in string.
    */
  String getRevision();
  
  ListModel<ISDPEntry> getSDPEntryListModel(SDPEntryType type);
  
  List<ISDPEntry> getSDPEntries(SDPEntryType type);

  boolean isEmpty();
  
  void setSDPFilePath(String sdpFileAbsolutePath);
  
  ISDPEntry getEntryByName(SDPEntryType type, String softwareName);
}

