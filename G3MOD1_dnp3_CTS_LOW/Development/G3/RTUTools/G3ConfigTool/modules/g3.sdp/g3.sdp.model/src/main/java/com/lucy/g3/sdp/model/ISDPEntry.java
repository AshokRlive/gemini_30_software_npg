/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.model;

import java.io.File;

/**
 * The Interface of SDP item entry.
 */
public interface ISDPEntry {
  
  File getEntryFile();
  
  String getEntryName();
  
  String printToStr();
  
}
