
package com.lucy.g3.sdp.exporter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.sdp.importer.SdpManifestReader;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPEntryType;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;


public class SdpExporter {
  private static Logger log = Logger.getLogger(SdpExporter.class);
  
  public static String generateSDP(SDP sdp, String sdpFilePath) throws IOException {
    List<ISDPEntry> swList = sdp.getSDPEntries(SDPEntryType.SOFTWARE);
    List<ISDPEntry> configList = sdp.getSDPEntries(SDPEntryType.CONFIG);
    
    // Create XML manifest file
    SdpManifestWriter writer = new SdpManifestWriter();
    writer.writeSoftwareEntries(swList);
    writer.writeSDPVersion(sdp.getRevision());
    writer.writeConfigFiles(configList);

    File sdpManifest = writer.saveToFile(SDP.MANIFEST_FILE_NAME);

    // Prepare file list to be added into SDP
    Map<String, File> filesMap = new HashMap<String, File>();
    filesMap.put(sdpManifest.getName(), sdpManifest);
    for (ISDPEntry fw : swList) {
      filesMap.put(fw.getEntryName(), fw.getEntryFile());
    }

    // add configuration file to zip
    for (ISDPEntry cw : configList) {
      filesMap.put(cw.getEntryName(), cw.getEntryFile());
    }

    // Create SDP zip file
    ZipUtil.zip(sdpFilePath, filesMap);

    // delete temp manifest file
    sdpManifest.delete();
    
    try {
      injectSDPRevisionToConfigTool(new File(sdpFilePath));
    } catch (Exception e) {
      log.error("Failed to inject SDP revision to ConfigTool", e);
    }

    
    sdp.setSDPFilePath(sdpFilePath);
    return sdpFilePath;
  }
  
  static void injectSDPRevisionToConfigTool(File sdpFile) throws Exception{
    byte[] sdpXml = org.zeroturnaround.zip.ZipUtil.unpackEntry(sdpFile, SDP.MANIFEST_FILE_NAME);
    SdpManifestReader reader = new SdpManifestReader(sdpXml);
    String SDPRevision = reader.readRevision();


    File dir = Files.createTempDirectory(".SDP").toFile();
    dir.mkdir();
    final File mcmZip = new File(dir, "MCMBoard.zip");
    final File configTool = new File(dir, "G3ConfigTool.jar");
    final File manifest = new File(dir, "MAINFEST.MF");
    
    try {
      // Extract file 
      org.zeroturnaround.zip.ZipUtil.unpackEntry(sdpFile, "MCMBoard.zip", mcmZip);
      org.zeroturnaround.zip.ZipUtil.unpackEntry(mcmZip, "html/G3ConfigTool.jar", configTool);
      org.zeroturnaround.zip.ZipUtil.unpackEntry(configTool, "META-INF/MANIFEST.MF", manifest);

      // Write SDP version to manifest file
      updateSDPRevision(SDPRevision, manifest);
      
      org.zeroturnaround.zip.ZipUtil.replaceEntry(configTool, "META-INF/MANIFEST.MF", manifest);
      
      org.zeroturnaround.zip.ZipUtil.replaceEntry(mcmZip, "html/G3ConfigTool.jar", configTool);
      
      // Replaced MCM file in SDP.
      org.zeroturnaround.zip.ZipUtil.replaceEntry(sdpFile, "MCMBoard.zip", mcmZip);
      
    } catch (Throwable e1) {
      Logger.getLogger(SDP.class).error("Failed to update the SDP-Revision of ConfigTool in SDP file");
    } finally {
      // Delete temp file always.
      FileUtils.deleteDirectory(dir);
    }
  }
  
  private static void updateSDPRevision(String SDPRevision, final File manifest) throws IOException {
    // Remove existing SDP revision
    ArrayList<String> lines = new ArrayList<>(FileUtils.readLines(manifest));
    int sdpLineNum = -1;
    for (int i = 0; i < lines.size(); i++) {
      if(lines.get(i).contains("SDP-Revision:")) {
        sdpLineNum = i;
        break;
      }
    }
    if(sdpLineNum >=0 )
      lines.remove(sdpLineNum);
    
    // Add new SDP revision
    lines.add(0, String.format("SDP-Revision: %s",SDPRevision));
    FileUtils.writeLines(manifest, lines, false);
  }

}

