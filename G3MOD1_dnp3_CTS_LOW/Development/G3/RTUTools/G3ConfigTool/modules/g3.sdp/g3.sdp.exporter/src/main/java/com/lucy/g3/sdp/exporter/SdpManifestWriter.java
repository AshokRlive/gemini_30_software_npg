/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.sdp.exporter;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.SDPSchema.ConfigFileType;
import com.SDPSchema.ModuleSoftwareType;
import com.SDPSchema.SDPContentType;
import com.SDPSchema.SDPSchema2;
import com.SDPSchema.SupportedFeatureType;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.sdp.model.ISDPEntry;
import com.lucy.g3.sdp.model.impl.ConfigFileEntry;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;

/**
 * The Class SDPXmlWriter.
 */
public class SdpManifestWriter {

  private final SDPSchema2 document;
  private SDPContentType root;


  public SdpManifestWriter() {
    SDPSchema2 doc = null;
    try {
      doc = com.SDPSchema.SDPSchema2.createDocument();
      root = doc.SDPContent.append();
      doc.setSchemaLocation("SDPschema.xsd");

    } catch (Exception e) {
      e.printStackTrace();
    }

    this.document = doc;
  }

  public File saveToFile(String sdpFilePath) throws IOException {
    if(sdpFilePath == null || sdpFilePath.trim().isEmpty())
      throw new IllegalArgumentException("sdpFilePath must not be blank");

    // Add postfix ".xml" if user didn't specify it in file chooser.
    String postfix = sdpFilePath.substring(sdpFilePath.length() - 4, sdpFilePath.length());
    if (!".xml".equalsIgnoreCase(postfix)) {
      sdpFilePath += ".xml";
    }

    try {
      document.saveToFile(sdpFilePath, true);
    } catch (Exception e) {
      throw new IOException(e);
    }
    
    return new File(sdpFilePath);
  }

  public void writeSoftwareEntries(List<ISDPEntry> entryList)  {
    entryList.removeAll(Collections.singleton(null)); // remove null elements


    for (ISDPEntry e : entryList) {
      if (e == null || !(e instanceof SoftwareEntry))
        continue;
      
      SoftwareEntry entry = (SoftwareEntry) e;
      
      ModuleSoftwareType moduleSoftware = root.ModuleSoftware.append();
      moduleSoftware.APIMajor.setValue(entry.getSystemAPI().major());
      moduleSoftware.APIMinor.setValue(entry.getSystemAPI().minor());

      moduleSoftware.RelType.setValue(entry.getVersion().relType().getValue());
      moduleSoftware.Major.setValue(entry.getVersion().major());
      moduleSoftware.Minor.setValue(entry.getVersion().minor());
      moduleSoftware.Patch.setValue(entry.getVersion().patch());
            
      moduleSoftware.FileName.setValue(entry.getEntryName());
      
      if (entry.getSlaveImageType() != null) {
        moduleSoftware.SlaveImageType.setValue(entry.getSlaveImageType().getValue());
      }

      moduleSoftware.ModuleType.setValue(entry.getModuleType().getValue());

      List<FeatureVersion> featureList = entry.getAllFeatures();

      for (int i = 0; i < featureList.size(); i++) {
        if (featureList.get(i) != null) {
          SupportedFeatureType xml_feature = moduleSoftware.SupportedFeature.append();
          xml_feature.Major.setValue(featureList.get(i).major());
          xml_feature.Minor.setValue(featureList.get(i).minor());
        }
      }
    }
  }
  
  /**
   * Given a list of configuration entries, write valid XML to DOM.
   * @param configFileList list of Configuration {@link ConfigFileEntry} file entries 
   */
  public void writeConfigFiles(List<ISDPEntry> configFileList) {
    configFileList.removeAll(Collections.singleton(null));
    if (configFileList.isEmpty())
      return;

    for (ISDPEntry e : configFileList) {
      if(e == null || !(e instanceof ConfigFileEntry))
        continue;
      ConfigFileEntry entry = (ConfigFileEntry) e;
      ConfigFileType configType = root.ConfigFile.append();

      configType.Location.setValue(entry.getEntryName());
      configType.ConfigAPIMajor.setValue(entry.getVersion().major());
      configType.ConfigAPIMinor.setValue(entry.getVersion().minor());
    }
  }

  public void writeSDPVersion(String sdpVersion) {
    root.sdpversion.setValue(sdpVersion);
  }
}
