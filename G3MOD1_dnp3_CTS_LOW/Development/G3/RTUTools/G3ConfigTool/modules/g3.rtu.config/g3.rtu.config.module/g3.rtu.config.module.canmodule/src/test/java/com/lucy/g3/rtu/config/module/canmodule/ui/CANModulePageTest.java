/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui;

import static org.junit.Assert.assertNotNull;

import javax.swing.JFrame;

import org.fest.swing.annotation.GUITest;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.testing.FestSwingTestCaseTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleDSM;
import com.lucy.g3.rtu.config.module.canmodule.ui.CANModulePage;
import com.lucy.g3.test.support.utilities.TestUtil;

@GUITest
@Ignore
// Skipped GUI test cause it failes sometimes in linux
public class CANModulePageTest extends FestSwingTestCaseTemplate {

  private FrameFixture window;
  private ICANModule module;


  @BeforeClass
  static public void setUpOnce() {
    // FailOnThreadViolationRepaintManager.install();
  }

  @Before
  public void setup() {
    setUpRobot();

    module = new ModuleDSM();

    // Create window
    JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {

      @Override
      protected JFrame executeInEDT() {
        return TestUtil.showFrame(new CANModulePage(module)
            .getContent());
      }
    });

    // IMPORTANT: note the call to 'robot()'
    window = new FrameFixture(robot(), frame);
    window.show(); // shows the frame to test
  }

  @After
  public void teardown() {
    window.cleanUp();
  }

  @Ignore
  @Test
  public void testModulePage() throws InterruptedException {
    assertNotNull(window.table());

    assertNotNull(window.tabbedPane());
    window.tabbedPane().selectTab(0);

    Thread.sleep(100000);
  }

}
