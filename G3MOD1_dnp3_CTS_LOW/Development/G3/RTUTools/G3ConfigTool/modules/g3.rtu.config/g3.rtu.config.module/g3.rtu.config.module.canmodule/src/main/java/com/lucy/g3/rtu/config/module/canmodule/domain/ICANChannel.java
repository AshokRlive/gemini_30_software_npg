/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;

/**
 * A channel is an IO of a module. It is an observable <code>JavaBean</code>
 * that conforms to <code>JavaBean</code> convention.
 * <p>
 * A channel can be mapped to a {@link VirtualPoint} or controlled by a
 * {@link CLogic} if it is output channel.
 * </p>
 *
 * @see CANChannelFactory
 * @see CANChannel
 */
public interface ICANChannel extends IChannel {
  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code DIGITAL_INPUT}</li>
   */
  String PARAM_DBHIGH2LOWMS = "DB High to Low (ms)";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code DIGITAL_INPUT}</li>
   */
  String PARAM_DBLOW2HIGHMS = "DB Low to High (ms)";

  /**
   * <p>
   * Channel parameter name for parameter {@value} .
   * </p>
   * <strong>Note</strong> this parameter should NOT be modified by users. Its
   * value relies on if this channel is mapped to a point.
   * <li>Value type: {@link Boolean}.</li>
   * <li>Supported by channel type: {@code DIGITAL_INPUT} and
   * {@code ANALOG_INPUT}</li>
   */
  String PARAM_EVENTENABLED = "Event";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Boolean}.</li> <li>Supported by channel type: {@code DIGITAL_INPUT}</li>
   * <li>Supported by module type: {@code IOM} and {@code SCM}</li>
   */
  String PARAM_EXTEQUITPINVERT = "Invert";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code AUX_SWITCH_OUT}</li>
   */
  String PARAM_PULSELENGTH = "Pulse Length (ms)";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code SWITCH_OUT}</li>
   */
  String PARAM_OVERRUN_MS = "Motor Supply Overrun (ms)";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code SWITCH_OUT}</li>
   */
  String PARAM_OPTIMEOUT_SEC = "Operation Timeout (sec)";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Polarity}.</li> <li>Supported by channel type:
   * {@code AUX_SWITCH_OUT} and {@code SWITCH_OUT}</li>
   */
  String PARAM_POLARITY = "Polarity";

  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Inhibit}.</li> <li>Supported by channel type: {@code SWITCH_OUT}</li>
   */
  String PARAM_INHIBI = "Inhibit";
  
  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code SWITCH_OUT}</li>
   */
  String PARAM_MOTOR_OVER_DRIVE_MS = "Motor Over Drive(ms)";
  
  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code SWITCH_OUT}</li>
   */
  String PARAM_MOTOR_REVERSE_DRIVE_MS = "Motor Reverse Drive (ms)";


  /**
   * Overloaded {@link #getOwnerModule()}interface to return ICANModule type.
   */
  @Override
  ICANModule getOwnerModule();

}
