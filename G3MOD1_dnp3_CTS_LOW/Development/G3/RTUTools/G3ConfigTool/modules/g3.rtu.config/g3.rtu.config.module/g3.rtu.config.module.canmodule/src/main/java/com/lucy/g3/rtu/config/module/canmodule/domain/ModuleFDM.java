/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFDM.FDM_CH_AINPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Fault Detect Module.
 */
public final class ModuleFDM extends AbstractCANModule implements ICANModule, ISupportChangeID {

  public ModuleFDM() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleFDM(MODULE_ID id) {
    super(id, MODULE.MODULE_FDM);
  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise Analogue output channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, FDM_CH_AINPUT.values()));

    return chMap;
  }

  @Override
  public void setId(MODULE_ID moduleID) {
    super.modifyId(moduleID);
  }

  @Override
  public IModuleSettings getSettings() {
    return null;
  }

}