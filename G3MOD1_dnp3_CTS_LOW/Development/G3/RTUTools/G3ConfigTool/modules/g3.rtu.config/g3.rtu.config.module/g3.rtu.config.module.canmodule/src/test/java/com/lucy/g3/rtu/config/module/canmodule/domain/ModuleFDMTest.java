/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFDM;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModuleFDMTest {

  ModuleFDM module;

  public String[] readWriteProperties =
      new String[] { ModuleFDM.PROPERTY_MODULE_ID };

  public String[] readOnlyProperties = new String[] {
      ModuleFDM.PROPERTY_FULLNAME,
      ModuleFDM.PROPERTY_SHORTNAME };


  @Before
  public void setUp() throws Exception {
    module = new ModuleFDM(MODULE_ID.MODULE_ID_0);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstruct() {
    MODULE_ID id = MODULE_ID.MODULE_ID_1;

    Module m = new ModuleFDM(id);

    assertEquals(MODULE.MODULE_FDM, m.getType());
    assertEquals(id, m.getId());
  }

  @Test
  public void testReadOnlyProperties() {
    BeanTestUtil.testReadOnlyProperties(module, readOnlyProperties);
  }

  @Test
  public void testReadWriteProperties() {
    BeanTestUtil.testReadWriteProperties(module, readWriteProperties);
  }
}
