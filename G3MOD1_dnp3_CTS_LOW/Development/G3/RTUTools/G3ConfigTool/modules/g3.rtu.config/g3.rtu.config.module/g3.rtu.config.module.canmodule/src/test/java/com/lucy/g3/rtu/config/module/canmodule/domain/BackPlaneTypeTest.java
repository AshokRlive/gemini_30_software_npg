/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;

public class BackPlaneTypeTest {

  @Test
  public void testGetDescription() {
    BackPlaneType[] types = BackPlaneType.values();

    for (BackPlaneType t : types) {
      // Description not null
      assertNotNull(t.getDescription());

      // toString() equals to description
      assertEquals(t.getDescription(), t.toString());
    }
  }

}
