/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.table.TableCellEditor;

import org.jdesktop.swingx.JXTable;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.ext.swing.table.AutoFillJTable;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.ui.ChannelManagerModel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.Inhibit;
import com.lucy.g3.rtu.config.module.canmodule.ui.dialogs.InhibitChooser;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * Viewable page for showing all available channels of a module in table.
 * Channel parameters can be edited by user.
 */
public class CANModulePage extends AbstractConfigPage {

  private final ICANModule module;

  private JTabbedPane tabbedPane;

  private final TableCellEditor polarityCellEditor = new DefaultCellEditor(new JComboBox<Polarity>(Polarity.values()));
  private final TableCellEditor inhibitEditor = new InhibitEditor();

  private final JComponent[] addtionalTabs;


  /**
   * Construct a channels page for a module.
   *
   * @param module
   *          the module whose channels will be listed in this page.
   * @param addtionalTabs
   *          some additional tabs to be populated in this page.
   */
  public CANModulePage(ICANModule module, JComponent... additionalTabPanels) {
    super(module, ModuleResource.INSTANCE.getModuleTypeFullName(module.getType()));
    this.module = Preconditions.checkNotNull(module, "module is null");
    this.addtionalTabs = additionalTabPanels;
    PropertyConnector.connect(module, Module.PROPERTY_SHORTNAME, this, PROPERTY_NODE_NAME).updateProperty2();
    PropertyConnector.connect(module, Module.PROPERTY_SHORTNAME, this, PROPERTY_TITLE).updateProperty2();
    setNodeIcon(ModuleResource.INSTANCE.getModuleIconThumbnail(module.getType()));
    Helper.register(this, module.getClass());
  }

  @Override
  protected void init() {
    initComponents();
  }

  @Override
  public Icon getTitleIcon() {
    return this.module.getIcons().getNormalIcon();
  }


  private void initComponents() {
    this.tabbedPane = new JTabbedPane();
    this.add(this.tabbedPane, BorderLayout.CENTER);
    this.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));

    // Added channel table for all available types
    ChannelType[] chTypes = ChannelType.values(); // module.getAvailableChannelTypes();

    for (int i = 0; i < chTypes.length; i++) {
      ICANChannel[] channels = this.module.getChannels(chTypes[i]);

      if (channels == null || channels.length == 0) {
        continue;
      }

      String[] parameters = channels[0].getParameterNames();

      // Construct channel presentation model
      ChannelManagerModel pm = new ChannelManagerModel(channels, parameters);

      JComponent tableComp = buildChannelTablePane(pm);
      this.tabbedPane.addTab(chTypes[i].getDescription(), tableComp);
    }

    if (this.addtionalTabs != null) {
      for (int i = 0; i < this.addtionalTabs.length; i++) {
        if(addtionalTabs[i] == null)
          continue;
        String title = this.addtionalTabs[i].getName();
        if (title == null) {
          title = "Untitled";
        }

        if (this.addtionalTabs[i] instanceof Page) {
          this.tabbedPane.addTab(title, ((Page) this.addtionalTabs[i]).getContent());
        } else {
          this.tabbedPane.addTab(title, this.addtionalTabs[i]);
        }

      }
    }

  }

  private JComponent buildChannelTablePane(ChannelManagerModel pm) {
    JXTable table = pm.createChannelTable();
    // Set cell editors
    table.setDefaultEditor(Polarity.class, this.polarityCellEditor);
    table.setDefaultEditor(Inhibit.class, this.inhibitEditor);

    // Binding
    Bindings.bind(table, pm.getChannelSelectList());

    // Set Pop-up menu
    JPopupMenu popmenu = new JPopupMenu();
    // popmenu.add(pm.getActionEnableAll());
    // popmenu.add(pm.getActionDisableAll());
    popmenu.add(pm.getActionViewDetails());
    table.setComponentPopupMenu(popmenu);

    // Set keyboard action
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "View");
    getActionMap().put("View", pm.getAction(ChannelManagerModel.ACTION_KEY_VIEW_DETAILS));

    // Add to container
    JScrollPane scrollpane = AutoFillJTable.createNoBorderScrollPane(table);
    scrollpane.setViewportBorder(BorderFactory.createEmptyBorder());
    scrollpane.setBorder(BorderFactory.createEmptyBorder());

    return scrollpane;
  }

  @Override
  public String toString() {
    return this.module.toString();
  }

  @Override
  public Action[] getContextActions() {
    return null;
  }


  private static class InhibitEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

    protected static final String EDIT = "edit";
    private JButton button;
    private Inhibit currentInhibit;


    public InhibitEditor() {
      this.button = new JButton();
      this.button.setActionCommand(EDIT);
      this.button.addActionListener(this);
      this.button.setBorderPainted(false);
      this.button.setBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      InhibitChooser inhibitChooser = new InhibitChooser(WindowUtils.getMainFrame(), this.currentInhibit);
      inhibitChooser.setVisible(true);

      if (!inhibitChooser.hasBeenCanceled()) {
        this.currentInhibit = inhibitChooser.getInhibit();
        stopCellEditing();
      } else {
        cancelCellEditing();
      }
    }

    @Override
    public Object getCellEditorValue() {
      return this.currentInhibit;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      this.currentInhibit = (Inhibit) value;
      this.button.setText(this.currentInhibit.toString());
      return this.button;
    }
  }

}
