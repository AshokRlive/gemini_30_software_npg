/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM_MK2;


/**
 *
 */
public class ModuleSCMMK2Test {
  private ModuleSCM_MK2 fixture;
  @Before
  public void setUp() throws Exception {
    fixture = new ModuleSCM_MK2();
  }

  @Test
  public void testSwitchChannelParameters() {
    ICANChannel[] chnls = fixture.getChannels(ChannelType.SWITCH_OUT);
    for (ICANChannel ch : chnls) {
      assertFalse(ch.hasParameter(ICANChannel.PARAM_PULSELENGTH));
      assertTrue(ch.hasParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS));
      assertTrue(ch.hasParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS));
      assertFalse(ch.hasParameter(ICANChannel.PARAM_POLARITY));
    }
  }
  

}

