/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import java.util.HashMap;
import java.util.Map;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPanelProvider;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Wizard for adding a module.
 */
public class ModuleAddingWizard {

  static final String WIZARD_TITLE = "Add Module Wizard";

  static final MODULE[] TYPES_REQUIRE_EXTRA_STEPS = {
      MODULE.MODULE_IOM,
      MODULE.MODULE_PSM,
      MODULE.MODULE_DSM,
      MODULE.MODULE_SCM,
      MODULE.MODULE_SCM_MK2,
      MODULE.MODULE_FPM,
  };


  public ModuleAddingWizard(CANModuleManager manager, IIOModuleGenerator configurator) {
    String[] initialSteps = { ModulePanelProvider.KEY_TYPE_ID_PANEL, ModulePanelProvider.KEY_VIRTUAL_POINT_PANEL };
    String[] descriptions = { "Select Module Type", "Virtual Points" };

    WizardPanelProvider wpp = new ModulePanelProvider(WIZARD_TITLE, initialSteps, descriptions, manager, configurator);
    ModuleWizardBranchController wbc = new ModuleWizardBranchController(wpp, manager, configurator);
    WizardDisplayer.showWizard(
        wbc.createWizard(),
        WizardUtils.createRect(WindowUtils.getMainFrame(),500, 300),
        Helper.createHelpAction(ModuleAddingWizard.class), 
        null);
  }


  private class ModuleWizardBranchController extends WizardBranchController {

    private CANModuleManager manager;
    private IIOModuleGenerator configurator;
    private final HashMap<MODULE, WizardPanelProvider> wpps = new HashMap<MODULE, WizardPanelProvider>();


    public ModuleWizardBranchController(WizardPanelProvider wpp, CANModuleManager manager,
        IIOModuleGenerator configurator) {
      super(wpp);
      this.manager = manager;
      this.configurator = configurator;
    }

    @Override
    protected WizardPanelProvider getPanelProviderForStep(String step, @SuppressWarnings("rawtypes") Map settings) {
      MODULE type = (MODULE) settings.get(ModuleTypeSelection.KEY_MODULE_TYPE);

      if (type == null) {
        return null;
      }

      WizardPanelProvider wpp = wpps.get(type);
      if (wpp == null) {
        for (MODULE t : ModuleAddingWizard.TYPES_REQUIRE_EXTRA_STEPS) {
          if (t == type) {

            if (t == MODULE.MODULE_FPM) {
              String[] id = new String[] { t.name(), ModulePanelProvider.KEY_FPM_CT_RATIO };
              String[] description =
                  new String[] { t.getDescription() + " Options", t.getDescription() + " Options 2" };
              wpp = new ModulePanelProvider(
                  ModuleAddingWizard.WIZARD_TITLE,
                  id, // use name as ID
                  description,
                  manager, configurator);
            } else {
              wpp = new ModulePanelProvider(
                  ModuleAddingWizard.WIZARD_TITLE,
                  t.name(), // use name as ID
                  t.getDescription() + " Options",
                  manager, configurator);
            }
            wpps.put(t, wpp);
            break;
          }
        }
      }

      return wpp;
    }
  }
}