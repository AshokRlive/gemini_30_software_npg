/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.widgets;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;

/**
 * A comboBox component for editing {@linkplain CTRatio}.
 */
public class CTRatioEditorCombox extends JComboBox<CTRatio> {

  /**
   * A construct used by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private CTRatioEditorCombox() {
  }
  
  public CTRatioEditorCombox(CTRatio[] items) {
    super(items);
    init();
  }

  public CTRatioEditorCombox(ValueModel valueHolder, CTRatio[] items) {
    Bindings.bind(this, new ComboBoxAdapter<CTRatio>(items, valueHolder));
    init();
  }

  private void init() {
    setEditable(true);
    setEditor(new CTRatioEditor());
  }
  
  public JFormattedTextField getTextField() {
    return ((CTRatioEditor)getEditor()).editor;
  }

  private static final class CTRatioEditor implements ComboBoxEditor {

    private final JFormattedTextField editor = new JFormattedTextField(new RatioFormatter());


    public CTRatioEditor() {
      FormattedTextFieldSupport.installCommitOnType(editor);
      editor.setBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    public Component getEditorComponent() {
      return editor;
    }

    @Override
    public void setItem(Object anObject) {
      editor.setValue(anObject);
    }

    @Override
    public Object getItem() {
      return editor.getValue();
    }

    @Override
    public void selectAll() {
      editor.selectAll();
    }

    @Override
    public void addActionListener(ActionListener l) {
      editor.addActionListener(l);
    }

    @Override
    public void removeActionListener(ActionListener l) {
      editor.removeActionListener(l);
    }
  }
}
