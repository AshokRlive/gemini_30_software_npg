/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.IPredefinedChannelConfig;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

class PredefinedCANChannelConf implements IPredefinedChannelConfig {

  private Logger log = Logger.getLogger(PredefinedCANChannelConf.class);

  private final HashMap<Double, String> unitMap;

  private final IChannelEnum channelEnum;


  public PredefinedCANChannelConf(IChannelEnum channelEnum) {
    this.channelEnum = Preconditions.checkNotNull(channelEnum, "channelEnum is null");

    HashMap<Double, String> smap = channelEnum.getAllScaleFactorMap();
    this.unitMap = smap == null ? new HashMap<Double, String>() : smap;
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return channelEnum.getDefCreateOption();
  }

  @Override
  public String getRawUnit() {
    return getUnitForScaling(Double.valueOf(1.0));
  }

  @Override
  public double getRawMin() {
    try {
      return Double.parseDouble(channelEnum.getDefConf("rawMin"));
    } catch (Exception e) {
      log.warn("rawMin not avaialbe for " + channelEnum);
      return 0;
    }
  }

  @Override
  public double getRawMax() {
    try {
      return Double.parseDouble(channelEnum.getDefConf("rawMax"));
    } catch (Exception e) {
      log.warn("rawMax not avaialbe for " + channelEnum);
      return 1000;
    }
  }

  @Override
  public String getUnitForScaling(Double scalingFactor) {
    return unitMap.get(scalingFactor);
  }

  @Override
  public Map<Double, String> getPredefindScalingUnit() {
    return new HashMap<Double, String>(unitMap);
  }

  @Override
  public Double getDefaultScalingFactor() {
    Double factor = channelEnum.getDefaultScalingFactor();

    return factor == null ? 1.0D : factor;
  }

}
