/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.util;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Utility for finding modules from a collection of modules.
 */
public class ModuleFinder {

  public static <T extends Module> T findModule(Collection<T> modules,
      MODULE type, MODULE_ID id) {
    Preconditions.checkNotNull(modules, "modules is null");

    for (T module : modules) {
      if (module.getType() == type && module.getId() == id) {
        return module;
      }
    }

    return null;
  }

  public static <T extends Module> Collection<T> findModuleByTypes(
      Collection<T> modules, MODULE... types) {
    Preconditions.checkNotNull(types, "types is null");
    Preconditions.checkNotNull(modules, "modules is null");

    ArrayList<T> foundModules = new ArrayList<T>();

    for (T module : modules) {
      for (MODULE t : types) {
        if (module != null && module.getType() == t) {
          foundModules.add(module);
          break;
        }
      }
    }

    return foundModules;
  }

}
