/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_EVENTENABLED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANChannelFactory;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.CANModuleStub;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.ChannelEnumStub;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.VPointStub;
import com.lucy.g3.rtu.config.shared.model.IConnection;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_AINPUT;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class CANChannelTest {

  private String[] readOnlyProperties = {
      CANChannel.PROPERTY_NAME,
  };

  final private Enum<? extends IChannelEnum> dummyEnum = ChannelEnumStub.CHANNEL_A1;

  private CANModuleStub module;

  private CANChannel fixture;


  @Before
  public void setUp() throws Exception {
    module = new CANModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_SCM);
    fixture = new CANChannel(ChannelType.ANALOG_OUTPUT, module, dummyEnum);
  }

  @After
  public void tearDown() throws Exception {
    module = null;
    fixture = null;
  }

  @Test
  public void testConstruct() {
    ChannelType[] types = ChannelType.values();
    for (int i = 0; i < types.length; i++) {
      CANChannel ch = new CANChannel(types[i], module, dummyEnum);

      assertTrue(types[i] == ch.getType());
      assertTrue(module == ch.getOwnerModule());
      assertTrue(dummyEnum == ch.getEnum());

    }
  }

  @Test
  public void testReadOnlyProperties() {
    BeanTestUtil.testReadOnlyProperties(fixture, readOnlyProperties);
  }

  @Test
  public void testPropertyUpdate() {

    String oldDescription = fixture.getName();
    System.out.println("Old: " + oldDescription);

    module.setId(MODULE_ID.MODULE_ID_4);

    String newDescription = fixture.getName();
    System.out.println("New: " + newDescription);

    assertNotSame(oldDescription, newDescription);
  }

  @Test
  public void testDefaultScale() {
    PSM_CH_AINPUT[] psmAInputs = PSM_CH_AINPUT.values();
    for (int j = 0; j < psmAInputs.length; j++) {
      System.out.println(psmAInputs[j].name() + "->" + psmAInputs[j].getDefaultScalingFactor());
    }
  }

  @Test
  public void testGetName() {
    ICANModule psm = new CANModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_PSM);
    PSM_CH_AINPUT chenum = PSM_CH_AINPUT.PSM_CH_AINPUT_MS_CURRENT;

    ICANChannel ch = CANChannelFactory.createChannel(ChannelType.ANALOG_INPUT, psm, chenum);
    System.out.println("Channel Description:" + ch.getDescription());
    System.out.println("Channel Name:" + ch.getName());
    System.out.println("ENUM Name:" + chenum.getDescription());
    Assert.assertEquals(ch.getName(), "PSM - " + chenum.getDescription());
  }

  /*
   * A channel's "Event Enable" parameter should be true if the channel is
   * mapped to a point, and false if there no mapped point.
   */
  @Test
  public void testEventEnabledProperty() throws Exception {
    fixture.addParameter(PARAM_EVENTENABLED, Boolean.TRUE);

    // PARAM_EVENTENABLED value should be false because no point has been
    // mapped. ;
    assertEquals(Boolean.FALSE, fixture.getParameter(PARAM_EVENTENABLED));

    // PARAM_EVENTENABLED is not modifiable
    assertFalse(fixture.isParameterModifiable(PARAM_EVENTENABLED));

    // SetParameter not work cause it not modifiable.
    fixture.setParameter(PARAM_EVENTENABLED, Boolean.TRUE);
    assertEquals(Boolean.FALSE, fixture.getParameter(PARAM_EVENTENABLED));

    for (int i = 0; i < 3; i++) {
      VPointStub vpoint = new VPointStub();
      IConnection mapping = fixture.connectToTarget(vpoint);
      assertEquals(Boolean.TRUE, fixture.getParameter(PARAM_EVENTENABLED));

      // SetParameter not work cause it not modifiable
      fixture.setParameter(PARAM_EVENTENABLED, Boolean.FALSE);
      assertEquals(Boolean.TRUE, fixture.getParameter(PARAM_EVENTENABLED));

      // Channel Event should be disabled if it is NOT mapped to a point
      mapping.delete();
      assertEquals(Boolean.FALSE, fixture.getParameter(PARAM_EVENTENABLED));
    }
  }

}
