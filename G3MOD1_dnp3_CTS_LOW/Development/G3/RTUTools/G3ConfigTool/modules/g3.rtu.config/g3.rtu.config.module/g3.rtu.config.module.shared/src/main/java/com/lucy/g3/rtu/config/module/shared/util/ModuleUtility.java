/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.util;

import java.util.Collection;

import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;


/**
 *
 */
public class ModuleUtility implements IConfigUtility {
  private ModuleUtility() {}

  public static Collection<Module> getModulesByType(IConfig data, MODULE types) {
    ModuleRepository repo = data.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    return repo.getModulesByType(types);
  }
  
  
}

