/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleDSM;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumDSM.DSM_CH_DINPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModuleDSMTest {

  private ModuleDSM module;
  private PresentationModel<ModuleDSM> model;

  private String[] readWriteProperties = {
      ModuleDSM.PROPERTY_MODULE_ID
  };


  @Before
  public void setUp() throws Exception {
    module = new ModuleDSM(MODULE_ID.MODULE_ID_0);
    model = new PresentationModel<ModuleDSM>(module);
  }

  @After
  public void tearDown() throws Exception {
    model.release();
    model = null;
    module = null;
  }

  @Test
  public void testReadWriteProperties() {
    BeanTestUtil.testReadWriteProperties(module, readWriteProperties);
  }


  @Test
  public void testChannelID() {
    ICANChannel[] ch = module.getChannels(ChannelType.DIGITAL_INPUT);
    assertTrue(ch.length != 0);
    for (int i = 0; i < ch.length; i++) {
      assertEquals(i, ch[i].getId());
    }
  }

  @Test
  public void testChannelEnum() {
    DSM_CH_DINPUT[] enums = DSM_CH_DINPUT.values();
    for (int i = 0; i < enums.length; i++) {
      assertTrue(module.getChByEnum(enums[i]).getEnum() == enums[i]);
    }
  }
  
  @Test
  public void testSwitchChannelParameters() {
    ICANChannel[] chnls = module.getChannels(ChannelType.SWITCH_OUT);
    for (ICANChannel ch : chnls) {
      assertFalse(ch.hasParameter(ICANChannel.PARAM_POLARITY));
    }
  }
}
