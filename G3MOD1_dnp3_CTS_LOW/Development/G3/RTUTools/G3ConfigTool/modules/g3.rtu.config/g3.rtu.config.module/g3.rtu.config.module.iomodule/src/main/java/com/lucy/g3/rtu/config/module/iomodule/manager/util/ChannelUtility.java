/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.manager.util;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.iomodule.manager.ChannelRepository;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;


/**
 *
 */
public class ChannelUtility implements IConfigUtility {
  private ChannelUtility() {}
  
  public static Collection<IChannel> getAllChannels(IConfig config, ChannelType ...type) {
    ChannelRepository repo = config == null ? null : 
      (ChannelRepository)config.getConfigModule(ChannelRepository.CONFIG_MODULE_ID);
    
    if(repo == null) {
      Logger.getLogger(ChannelUtility.class).error("Channel Repository not found!");
      return new ArrayList<IChannel>(0);
    } else {
      return repo.getAllChannels(type);
    }
  }
}

