package com.lucy.g3.rtu.config.module.canmodule.domain;

import com.g3schema.ns_g3module.FPIFaultT;
import com.g3schema.ns_g3module.FPIPhaseFaultT;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigPhaseFault;

public abstract class FPIConfigFault extends Model {

    public static final String PROPERTY_MIN_FAULT_DURATION_MS = "minFaultDurationMs";
    public static final String PROPERTY_TIMED_FAULT_CURRENT = "timedFaultCurrent";
    public static final String PROPERTY_INSTANT_FAULT_CURRENT = "instantFaultCurrent";

    private long minFaultDurationMs;
    private long timedFaultCurrent;
    private long instantFaultCurrent;


    public FPIConfigFault() {
      // LoggingUtils.logPropertyChanges(this);
    }

    public long getMinFaultDurationMs() {
      return minFaultDurationMs;
    }

    public void setMinFaultDurationMs(long minFaultDurationMs) {
      Object oldValue = this.minFaultDurationMs;
      this.minFaultDurationMs = minFaultDurationMs;
      firePropertyChange(PROPERTY_MIN_FAULT_DURATION_MS, oldValue, minFaultDurationMs);
    }

    public long getTimedFaultCurrent() {
      return timedFaultCurrent;
    }

    public void setTimedFaultCurrent(long timedFaultCurrent) {
      Object oldValue = this.timedFaultCurrent;
      this.timedFaultCurrent = timedFaultCurrent;
      firePropertyChange(PROPERTY_TIMED_FAULT_CURRENT, oldValue, timedFaultCurrent);
    }

    public long getInstantFaultCurrent() {
      return instantFaultCurrent;
    }

    public void setInstantFaultCurrent(long instantFaultCurrent) {
      Object oldValue = this.instantFaultCurrent;
      this.instantFaultCurrent = instantFaultCurrent;
      firePropertyChange(PROPERTY_INSTANT_FAULT_CURRENT, oldValue, instantFaultCurrent);
    }
    
    public void readFromXML(CTRatio ratio, FPIFaultT xml) {
      setInstantFaultCurrent(CTRatio.applyCTRatio2(ratio, xml.InstantFaultCurrent.getValue()));
      setTimedFaultCurrent(CTRatio.applyCTRatio2(ratio, xml.timedFaultCurrent.getValue()));
      setMinFaultDurationMs(xml.minFaultDurationMs.getValue());
    }
    
    public void writeToXML(CTRatio ratio, FPIFaultT xml) {
      xml.InstantFaultCurrent.setValue(CTRatio.applyCTRatio(ratio, getInstantFaultCurrent()));
      xml.timedFaultCurrent.setValue(CTRatio.applyCTRatio(ratio, getTimedFaultCurrent()));
      xml.minFaultDurationMs.setValue(getMinFaultDurationMs());
    }

    
    public static final class FPIConfigEarthFault extends FPIConfigFault {

      public FPIConfigEarthFault() {
        /* Set defaults */
        setMinFaultDurationMs(50); // Minimum Fault Duration: 50ms
        setTimedFaultCurrent(90); // Timed Fault Current: 90 A
        setInstantFaultCurrent(0);// Instant Fault Current: 0 A
      }
    }
    
    public static final class FPIConfigCurrentFault extends FPIConfigFault {
      public FPIConfigCurrentFault() {
      }
    }

    public static final class FPIConfigPhaseFault extends FPIConfigFault {

      public static final String PROPERTY_MIN_OVERLOAD_DURATIONMS = "minOverloadDurationMs";
      public static final String PROPERTY_OVERLOAD_CURRENT = "overloadCurrent";

      private long minOverloadDurationMs;
      private long overloadCurrent;


      public FPIConfigPhaseFault() {
        /* Set defaults */
        setMinFaultDurationMs(50); // Minimum Fault Duration: 50ms
        setMinOverloadDurationMs(100); // Minimum Overload Duration: 100ms
        setTimedFaultCurrent(600); // Timed Fault Current: 600 A
        setInstantFaultCurrent(0); // Instant Fault Current 0 A
        setOverloadCurrent(24000); // Overload Current: 24000 A
      }

      public long getMinOverloadDurationMs() {
        return minOverloadDurationMs;
      }

      public void setMinOverloadDurationMs(long minOverloadDurationMs) {
        Object oldValue = this.minOverloadDurationMs;
        this.minOverloadDurationMs = minOverloadDurationMs;
        firePropertyChange("minOverloadDurationMs", oldValue, minOverloadDurationMs);
      }

      public long getOverloadCurrent() {
        return overloadCurrent;
      }

      public void setOverloadCurrent(long overloadCurrent) {
        Object oldValue = this.overloadCurrent;
        this.overloadCurrent = overloadCurrent;
        firePropertyChange("overloadCurrent", oldValue, overloadCurrent);
      }

      public void readFromXML(CTRatio ratio, FPIPhaseFaultT xml) {
        super.readFromXML(ratio, xml);
        setOverloadCurrent(CTRatio.applyCTRatio2(ratio, xml.overloadCurrent.getValue()));
        setMinOverloadDurationMs(xml.minOverloadDurationMs.getValue());
      }
      
      public void writeToXML(CTRatio ratio, FPIPhaseFaultT xml) {
        super.writeToXML(ratio, xml);
        xml.overloadCurrent.setValue(CTRatio.applyCTRatio(ratio, getOverloadCurrent()));
        xml.minOverloadDurationMs.setValue(getMinOverloadDurationMs());
      }
    }
  }