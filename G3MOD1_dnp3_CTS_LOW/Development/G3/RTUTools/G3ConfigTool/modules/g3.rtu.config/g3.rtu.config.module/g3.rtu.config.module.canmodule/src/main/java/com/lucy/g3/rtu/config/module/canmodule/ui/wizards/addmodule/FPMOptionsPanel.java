/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import java.awt.Component;

import javax.swing.JCheckBox;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

public class FPMOptionsPanel extends WizardPage {

  static final String KEY_CREATE_FPI_TEST = "CREATE_IOM_DIGITAL_OUTPUTS";
  static final boolean createFPITest = false;

  static final String KEY_CREATE_FPI_RESET = "CREATE_IOM_DIGITAL_OUTPUTS";
  static final boolean createFPIReset = false;


  public FPMOptionsPanel() {
    initComponents();
    chkCreateFPITest.setName(KEY_CREATE_FPI_TEST);
    chkCreateFPIReset.setName(KEY_CREATE_FPI_RESET);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE_OR_FINISH);
    return null;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    chkCreateFPITest = new JCheckBox();
    chkCreateFPIReset = new JCheckBox();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default",
        "2*(default, $lgap), default"));

    // ---- chkCreateFPITest ----
    chkCreateFPITest.setText("Create FPI Test Control Logic");
    add(chkCreateFPITest, CC.xy(1, 1));

    // ---- chkCreateFPIReset ----
    chkCreateFPIReset.setText("Create FPI Reset Control Logic");
    add(chkCreateFPIReset, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox chkCreateFPITest;
  private JCheckBox chkCreateFPIReset;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
