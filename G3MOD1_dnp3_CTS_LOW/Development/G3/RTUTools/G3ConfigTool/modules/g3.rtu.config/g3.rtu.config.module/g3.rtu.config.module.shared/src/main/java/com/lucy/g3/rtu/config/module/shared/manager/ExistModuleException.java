/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.manager;

import com.lucy.g3.itemlist.IItemListManager.AddItemException;
import com.lucy.g3.rtu.config.module.shared.domain.Module;

/**
 * This exception should be thrown when trying to add duplicated module to
 * manager.
 */
public class ExistModuleException extends AddItemException {

  public ExistModuleException(Module module) {
    super("The module \"" + module.getShortName() + "\" exists.");
  }
}
