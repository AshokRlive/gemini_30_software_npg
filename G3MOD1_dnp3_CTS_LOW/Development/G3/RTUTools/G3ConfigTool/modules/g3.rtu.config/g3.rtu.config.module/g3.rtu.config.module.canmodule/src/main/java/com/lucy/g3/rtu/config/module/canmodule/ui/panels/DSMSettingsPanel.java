/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.panels;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleDSMSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;

/**
 * SCM settings panel.
 */
public class DSMSettingsPanel extends JPanel implements IModuleSettingsEditor {

  private final ModuleDSMSettings settings;


  public DSMSettingsPanel(ModuleDSMSettings settings) {
    this.settings = settings;

    initComponents();
  }

  private void initComponents() {
    FormLayout layout = new FormLayout("right:default, $lcgap, [80dlu,default], $lcgap, default:grow", "default");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout, this);
    builder.setDefaultDialogBorder();

    PresentationModel<ModuleDSMSettings> dsmModel = new PresentationModel<ModuleDSMSettings>(settings);
    ValueModel vm;

    vm = dsmModel.getModel(ModuleDSMSettings.PROPERTY_OPEN_COLOUR);
    ComboBoxAdapter<LED_COLOUR> comboModel = new ComboBoxAdapter<LED_COLOUR>(LED_COLOUR.values(), vm);
    @SuppressWarnings("unchecked")
    JComponent comp = new JComboBox<LED_COLOUR>(comboModel);
    builder.append("Switch Open Colour:", comp);
    builder.nextLine();

    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();

    vm = dsmModel.getModel(ModuleDSMSettings.PROPERTY_POLARITY_A);
    ComboBoxAdapter<Polarity> comboModel2 = new ComboBoxAdapter<Polarity>(Polarity.values(), vm);
    @SuppressWarnings("unchecked")
    JComponent comp2 = new JComboBox<Polarity>(comboModel2);
    builder.append("Polarity A:", comp2);
    builder.nextLine();

    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();

    vm = dsmModel.getModel(ModuleDSMSettings.PROPERTY_POLARITY_B);
    ComboBoxAdapter<Polarity> comboModel3 = new ComboBoxAdapter<Polarity>(Polarity.values(), vm);
    @SuppressWarnings("unchecked")
    JComboBox<Polarity> comp3 = new JComboBox<Polarity>(comboModel3);
    builder.append("Polarity B:", comp3);
    builder.nextLine();

    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();

    vm = dsmModel.getModel(ModuleDSMSettings.PROPERTY_SUPPLY_ENABLE_24V);
    comp = BasicComponentFactory.createCheckBox(vm, "Enable 24 V Supply");
    builder.append("", comp);
    builder.nextLine();

    vm = dsmModel.getModel(ModuleDSMSettings.PROPERTY_ALLOWFORCEDOPERATIONA);
    comp = BasicComponentFactory.createCheckBox(vm, ModuleDSMSettings.FORCED_OPERATION_TEXT + " A");
    comp.setToolTipText(ModuleDSMSettings.FORCE_OPERATION_HINT);
    builder.append("", comp, 3);
    builder.nextLine();

    vm = dsmModel.getModel(ModuleDSMSettings.PROPERTY_ALLOWFORCEDOPERATIONB);
    comp = BasicComponentFactory.createCheckBox(vm, ModuleDSMSettings.FORCED_OPERATION_TEXT + " B");
    comp.setToolTipText(ModuleDSMSettings.FORCE_OPERATION_HINT);
    builder.append("", comp, 3);
    builder.nextLine();

  }

  @Override
  public JComponent getEditorComopnent() {
    return this;
  }

  @Override
  public String getEditorTitle() {
    return "DSM Settings";
  }

}
