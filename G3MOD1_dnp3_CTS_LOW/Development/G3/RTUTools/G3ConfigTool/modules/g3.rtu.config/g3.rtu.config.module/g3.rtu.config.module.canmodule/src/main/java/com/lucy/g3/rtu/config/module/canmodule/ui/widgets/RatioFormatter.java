/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.widgets;

import java.text.ParseException;

import javax.swing.text.DefaultFormatter;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;

public class RatioFormatter extends DefaultFormatter {

  private final boolean allowNullValue;


  public RatioFormatter() {
    this(false);
  }

  public RatioFormatter(boolean allowNullValue) {
    this.allowNullValue = allowNullValue;
  }

  @Override
  public CTRatio stringToValue(String text) throws ParseException {
    if (Strings.isBlank(text)) {
      if (allowNullValue) {
        return null;
      } else {
        throw new ParseException("Invalid CTRadio:" + text, 0);
      }
    }

    String[] tokens = text.split(":");

    // Validate
    if (tokens.length < 2 || Strings.isBlank(tokens[0])
        || Strings.isBlank(tokens[1])) {
      throw new ParseException("Invalid CTRadio:" + text, 0);
    }

    long dividend;
    long divisor;

    try {
      dividend = Long.parseLong(tokens[0]);
      divisor = Long.parseLong(tokens[1]);
    } catch (Exception e) {
      throw new ParseException("Invalid CTRadio:" + text, 0);
    }

    if (divisor == 0 || divisor < 0 || dividend < 0) {
      throw new ParseException("Invalid CTRadio:" + text, 0);
    }

    return new CTRatio(dividend, divisor);
  }

  @Override
  public String valueToString(Object value) throws ParseException {
    if (value == null) {
      return "";
    }

    if (value instanceof CTRatio) {
      CTRatio v = (CTRatio) value;
      return String.format("%d:%d", v.dividend, v.divisor);

    } else {
      throw new ParseException("Can not parse:" + value, 0);
    }
  }
}
