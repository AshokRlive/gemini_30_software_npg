/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.domain;

import com.lucy.g3.rtu.config.channel.domain.IChannelContainer;
import com.lucy.g3.rtu.config.module.shared.domain.Module;


/**
 * IO module is a module that is connected to RTU to provide inputs/outputs.
 * All inputs/outputs are implemented via standard module Channel. 
 */
public interface IOModule extends Module, IChannelContainer{

}

