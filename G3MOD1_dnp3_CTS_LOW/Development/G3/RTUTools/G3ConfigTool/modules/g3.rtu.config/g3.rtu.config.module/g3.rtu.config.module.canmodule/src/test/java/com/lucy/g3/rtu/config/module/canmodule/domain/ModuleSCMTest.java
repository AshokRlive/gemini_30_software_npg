/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModuleSCMTest {

  private ModuleSCM fixture;
  private PresentationModel<ModuleSCM> model;

  private String[] readWriteProperties = {
      ModuleSCM.PROPERTY_MODULE_ID
  };


  @Before
  public void setUp() throws Exception {
    fixture = new ModuleSCM(MODULE_ID.MODULE_ID_0);
    model = new PresentationModel<ModuleSCM>(fixture);
  }

  @After
  public void tearDown() throws Exception {
    model.release();
    model = null;
    fixture = null;
  }

  @Test
  public void testReadWriteProperties() {
    BeanTestUtil.testReadWriteProperties(fixture, readWriteProperties);
  }
  
  @Test
  public void testSwitchChannelParameters() {
    ICANChannel[] chnls = fixture.getChannels(ChannelType.SWITCH_OUT);
    for (ICANChannel ch : chnls) {
      assertTrue(ch.hasParameter(ICANChannel.PARAM_POLARITY));
    }
  }
}
