/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * <p>
 * Factory for constructing channel instances in different types.
 * <p>
 * Note different type of channels will be initialised with different
 * parameters.
 * </p>
 */
public final class CANChannelFactory {

  /**
   * The modules whose digital input channel support "Invert".
   */
  public static final Collection<MODULE> SUPPORT_INVERT = Collections.unmodifiableList(Arrays.asList(new MODULE[] {
      MODULE.MODULE_DSM,
      MODULE.MODULE_SCM,
      MODULE.MODULE_SCM_MK2,
      MODULE.MODULE_IOM
  }));


  private CANChannelFactory() {
  }

  /**
   * Creates a channel in a given type.
   *
   * @param type
   *          {@link ChannelType}
   * @param module
   *          non-null parent module.
   * @param channelEnum
   *          the enumeration converted from XML that represents the channel.
   * @return Non-null channel instance for the given type.
   * @throws NullPointerException
   *           if any of these arguments: <code>type</code>, <code>module</code>
   *           and <code>channelEnum</code> is null.
   * @throws IllegalArgumentException
   *           if the channel type doesn't match parent module's type.
   */
  public static CANChannel createChannel(ChannelType type, ICANModule module,
      Enum<? extends IChannelEnum> channelEnum) {
    return createChannel(type, module, channelEnum, false);
  }

  /**
   * Creates a channel in a given type.
   *
   * @param type
   *          {@link ChannelType}
   * @param module
   *          non-null parent module.
   * @param channelEnum
   *          the enumeration converted from XML that represents the channel.
   * @param readonly
   *          indicates the parameter of this channel can be edited by user.
   * @param invertAllowed
   *          indicates if the channel has parameter "Invert". This argument
   *          only applies when the channel type is <code>DIGITAL_INPUT</code>.
   * @return Non-null channel instance for the given type.
   * @throws NullPointerException
   *           if any of these arguments: <code>type</code>, <code>module</code>
   *           and <code>channelEnum</code> is null.
   * @throws IllegalArgumentException
   *           if the channel type doesn't match parent module's type.
   */
  public static CANChannel createChannel(ChannelType type, ICANModule module,
      Enum<? extends IChannelEnum> channelEnum, boolean readonly) {
    /* Check null */
    Preconditions.checkNotNull(type, "Channel Type must not be null");
    Preconditions.checkNotNull(module, "Module  must not be  null");
    Preconditions.checkNotNull(channelEnum, "Channel Enum  must not be null");

    // Create channel
    CANChannel ch = new CANChannel(type, module, channelEnum, readonly);
    IChannelEnum chEnum = (IChannelEnum) channelEnum;

    // Define parameters for the channel
    switch (type) {
    case ANALOG_OUTPUT:
    case ANALOG_INPUT:
      ch.addParameter(PARAM_EVENT_MS,
          Long.valueOf(chEnum.getDefaultEventRate()));
      ch.addParameter(PARAM_EVENTENABLED, Boolean.FALSE);
      break;

    case DIGITAL_OUTPUT:
      ch.addParameter(PARAM_PULSELENGTH, 1000L);
      break;

    case DIGITAL_INPUT:
      ch.addParameter(PARAM_DBHIGH2LOWMS, 100L);
      ch.addParameter(PARAM_DBLOW2HIGHMS, 100L);
      ch.addParameter(PARAM_EVENTENABLED, Boolean.FALSE);

      if (SUPPORT_INVERT.contains(module.getType())) {
        ch.addParameter(PARAM_EXTEQUITPINVERT, Boolean.FALSE);
      }
      break;

    case SWITCH_OUT:
      ch.addParameter(PARAM_PULSELENGTH, 1000L);
      ch.addParameter(PARAM_OVERRUN_MS, 3000L);
      ch.addParameter(PARAM_OPTIMEOUT_SEC, 30L);
      ch.addParameter(PARAM_INHIBI, new Inhibit(new ICANChannel[0]));
      break;

    default:
      break;
    }
    return ch;
  }
}
