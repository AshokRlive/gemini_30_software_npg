/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.module.canmodule.ui.panels.PSMSettingsPanel;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;


public class ModulePSMSettings extends AbstractModuleSettings{
  private Logger log = Logger.getLogger(ModulePSMSettings.class);
  
  public static final String PROPERTY_FAN_TEMP_THRESHOLD = "fanTempThreshold";

  public static final String PROPERTY_FAN_TEMP_HYSTERESIS = "fanTempHysteresis";

  public static final String PROPERTY_FAULT_HYSTERESIS = "faultHysteresis";

  public static final String PROPERTY_FAN_FITTED = "fanFitted";
  public static final String PROPERTY_FAN_AS_LED = "fanAsLED";
  public static final String PROPERTY_FAN_AS_DO = "fanAsDO";

  public static final String PROPERTY_FAN_SPEED_SENSOR_FITTED = "fanSpeedSensorFitted";

  public static final String PROPERTY_BATTERYTESTENABLED = "batteryTestEnabled";
  public static final String PROPERTY_BATTERYTESTPERIOD_DAYS = "batteryTestPeriod";
  public static final String PROPERTY_BATTERYTESTDURATION = "batteryTestDuration";

  // Boundary of fanTempHysteresis degree
  public static final int TEMP_HYST_DEF = 2;
  public static final int TEMP_HYST_MIN = 2;
  public static final int TEMP_HYST_MAX = 10;
  
  public static final int BATTERY_TEST_PERIOD_DAYS_MAX = 40;

  // Boundary of faultHysteresis ms
  public static final int FAULT_HYST_DEF = 5000;
  public static final int FAULT_HYST_MIN = 500;
  public static final int FAULT_HYST_MAX = 20000;


  private int fanTempThreshold = 30;// degree celsius

  private int fanTempHysteresis = TEMP_HYST_DEF;// degree celsius

  private int faultHysteresis = FAULT_HYST_DEF; // seconds

  private boolean fanSpeedSensorFitted;

  private boolean fanFitted;
  private boolean fanAsLED;
  private boolean fanAsDO;

  private boolean batteryTestEnabled;
  private long batteryTestPeriod;
  private long batteryTestDuration;

  public int getFanTempThreshold() {
    return fanTempThreshold;
  }

  public void setFanTempThreshold(int fanTempThreshold) {
    Object oldValue = getFanTempThreshold();
    this.fanTempThreshold = fanTempThreshold;
    firePropertyChange(PROPERTY_FAN_TEMP_THRESHOLD, oldValue, fanTempThreshold);
  }

  public int getFanTempHysteresis() {
    return fanTempHysteresis;
  }

  public void setFanTempHysteresis(int fanTempHysteresis) {
    /* Log warning if new value is out of range */
    if (fanTempHysteresis > TEMP_HYST_MAX || fanTempHysteresis < TEMP_HYST_MIN) {
      log.warn(String.format("Fan Temperature Hysteresis:%d is out of boundary(%d~%d).",
          fanTempHysteresis, TEMP_HYST_MIN, TEMP_HYST_MAX));
    }

    /* Set new value to default if it is out of range */
    if (fanTempHysteresis > TEMP_HYST_MAX) {
      log.warn(String.format("Fan Temperature Hysteresis is changed from %d to %d",
          fanTempHysteresis, TEMP_HYST_MAX));
      fanTempHysteresis = TEMP_HYST_MAX;
    }

    else if (fanTempHysteresis < TEMP_HYST_MIN) {
      log.warn(String.format("Fan Temperature Hysteresis is changed from %d to %d",
          fanTempHysteresis, TEMP_HYST_MIN));
      fanTempHysteresis = TEMP_HYST_MIN;
    }

    /* Make change */
    Object oldValue = getFanTempHysteresis();
    this.fanTempHysteresis = fanTempHysteresis;
    firePropertyChange(PROPERTY_FAN_TEMP_HYSTERESIS, oldValue, fanTempHysteresis);
  }

  public int getFaultHysteresis() {
    return faultHysteresis;
  }

  public void setFaultHysteresis(int faultHysteresis) {
    /* Log warning if new value is out of range */
    if (faultHysteresis > FAULT_HYST_MAX || faultHysteresis < FAULT_HYST_MIN) {
      log.warn(String.format("Fan Fault Hysteresis:%d is out of boundary(%d~%d).",
          faultHysteresis, FAULT_HYST_MIN, FAULT_HYST_MAX));
    }

    /* Set new value to default if it is out of range */
    if (faultHysteresis > FAULT_HYST_MAX) {
      log.warn(String.format("Fan Fault Hysteresis is changed from %d to %d",
          faultHysteresis, FAULT_HYST_MAX));
      faultHysteresis = FAULT_HYST_MAX;
    }

    else if (faultHysteresis < FAULT_HYST_MIN) {
      log.warn(String.format("Fan Fault Hysteresis is changed from %d to %d",
          faultHysteresis, FAULT_HYST_MIN));
      faultHysteresis = FAULT_HYST_MIN;
    }

    /* Make change */
    Object oldValue = getFaultHysteresis();
    this.faultHysteresis = faultHysteresis;
    firePropertyChange(PROPERTY_FAULT_HYSTERESIS, oldValue, faultHysteresis);
  }

  public boolean isFanSpeedSensorFitted() {
    return fanSpeedSensorFitted;
  }

  public void setFanSpeedSensorFitted(boolean fanSpeedSensorFitted) {
    Object oldValue = isFanSpeedSensorFitted();
    this.fanSpeedSensorFitted = fanSpeedSensorFitted;
    firePropertyChange(PROPERTY_FAN_SPEED_SENSOR_FITTED, oldValue, fanSpeedSensorFitted);
  }

  public boolean isFanFitted() {
    return fanFitted;
  }

  public void setFanFitted(boolean fanFitted) {
	 if(fanFitted){
		if(isFanAsDO())
		  setFanAsDO(false);
		if(isFanAsLED())
		  setFanAsLED(false);
	 }
    Object oldValue = isFanFitted();
    this.fanFitted = fanFitted;
    firePropertyChange(PROPERTY_FAN_FITTED, oldValue, fanFitted);
  }
  
  public boolean isFanAsLED() {
    return fanAsLED;
  }

  public void setFanAsLED(boolean fanAsLED) {
	  if(fanAsLED){
		  if(isFanFitted())
			  setFanFitted(false);
		  if(isFanAsDO())
			  setFanAsDO(false);
	  }
    Object oldValue = isFanAsLED();
    this.fanAsLED = fanAsLED;
    firePropertyChange(PROPERTY_FAN_AS_LED, oldValue, fanAsLED);
  }
	  
  public boolean isFanAsDO() {
	    return fanAsDO;
  }

  public void setFanAsDO(boolean fanAsDO) {
	  if(fanAsDO){
	  if(isFanFitted())
		  setFanFitted(false);
	  if(isFanAsLED())
		  setFanAsLED(false);
	  }
    Object oldValue = isFanAsDO();
    this.fanAsDO = fanAsDO;
    firePropertyChange(PROPERTY_FAN_AS_DO, oldValue, fanAsDO);
  }

  public boolean isBatteryTestEnabled() {
    return batteryTestEnabled;
  }

  public void setBatteryTestEnabled(boolean batteryTestEnabled) {
    Object oldValue = this.batteryTestEnabled;
    this.batteryTestEnabled = batteryTestEnabled;
    firePropertyChange("batteryTestEnabled", oldValue, batteryTestEnabled);
  }

  public long getBatteryTestPeriod() {
    return batteryTestPeriod;
  }

  public void setBatteryTestPeriod(long batteryTestPeriod) {
    if (batteryTestPeriod < 0) {
      return;
    }

    Object oldValue = this.batteryTestPeriod;
    this.batteryTestPeriod = batteryTestPeriod;
    firePropertyChange("batteryTestPeriod", oldValue, batteryTestPeriod);
  }

  public long getBatteryTestDuration() {
    return batteryTestDuration;
  }

  public void setBatteryTestDuration(long batteryTestDuration) {
    if (batteryTestDuration < 0) {
      return;
    }

    Object oldValue = this.batteryTestDuration;
    this.batteryTestDuration = batteryTestDuration;
    firePropertyChange("batteryTestDuration", oldValue, batteryTestDuration);
  }

  private PSMSettingsPanel editor;
  @Override
  public IModuleSettingsEditor getEditor() {
    if(editor == null)
      editor = new PSMSettingsPanel(this);
    return editor;
  }

}

