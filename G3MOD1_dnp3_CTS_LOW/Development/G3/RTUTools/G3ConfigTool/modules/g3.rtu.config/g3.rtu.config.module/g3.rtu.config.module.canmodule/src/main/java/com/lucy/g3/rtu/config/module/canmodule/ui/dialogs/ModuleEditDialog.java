/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;

import org.jdesktop.swingx.JXLabel;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * A dialog for editing the configuration of a CAN module.
 */
public class ModuleEditDialog extends AbstractDialog {

  /** Default dialog size */
  private static final Dimension PREF_DIALOG_SIZE = new Dimension(300, 300);

  private static MODULE lastSelectedType;

  private final HashMap<MODULE, DefaultComboBoxModel<MODULE_ID>> moduleTypeIdMap =
      new LinkedHashMap<MODULE, DefaultComboBoxModel<MODULE_ID>>();

  private final CANModuleManager manager;

  private ICANModule module = null;

  private final boolean isEditing;


  /**
   * Construct a dialog for creating new module.
   *
   * @param parent
   */
  public ModuleEditDialog(JFrame parent, CANModuleManager manager) {
    this(parent, manager, null);
  }

  /**
   * Construct a dialog for editing a given module or creating a new module.
   *
   * @param parent
   * @param manager
   * @param editModule
   *          the module to be edited. If it is null, a new module will be
   *          created.
   */
  public ModuleEditDialog(JFrame parent, CANModuleManager manager, ICANModule editModule) {
    super(parent);
    this.manager = manager;
    this.module = editModule;

    // Create a new module
    if (this.module == null) {
      this.isEditing = false;
      setTitle("New Module");
      MODULE[] allTypes = ModuleEnums.getSupportedTypes();

      for (MODULE mType : allTypes) {
        MODULE_ID[] ids = manager.getAvailableIDsByType(mType);
        if (ids != null && ids.length > 0) {
          this.moduleTypeIdMap.put(mType, new DefaultComboBoxModel<MODULE_ID>(ids));
        }
      }

      // Edit a given module
    } else {
      this.isEditing = true;
      setTitle("Edit Module");
      MODULE moduleType = this.module.getType();
      MODULE_ID[] ids = manager.getAvailableIDsByType(moduleType);
      DefaultComboBoxModel<MODULE_ID> m = new DefaultComboBoxModel<MODULE_ID>(ids);
      MODULE_ID thisID = this.module.getId();
      m.insertElementAt(thisID, 0);
      m.setSelectedItem(thisID);
      this.moduleTypeIdMap.put(moduleType, m);
    }

    initComponents();
    setMinimumSize(PREF_DIALOG_SIZE);
    pack();

    // init state
    updateBanner();
    updateModuleIDs();
    updateState();
  }

  public ICANModule getModule() {
    return this.module;
  }

  private void updateBanner() {
    MODULE type = (MODULE) this.combModuleType.getSelectedItem();
    if (type != null) {
      String desp = ModuleResource.INSTANCE.getModuleTypeDescription(type);
      String shortname = ModuleResource.INSTANCE.getModuleTypeShortName(type);
      BannerPanel banner = this.getBannerPanel();
      if (banner != null) {
        banner.setTitle(shortname);
        banner.setSubtitle(desp);
        banner.setIcon(ModuleResource.INSTANCE.getModuleIcon(type));
      }
    }
  }

  private void updateModuleIDs() {
    MODULE type = (MODULE) this.combModuleType.getSelectedItem();
    if (type != null) {
      this.combModuleID.setModel(this.moduleTypeIdMap.get(type));
    }

    // "ID" choices would be invisible if there is no more than one available id
    boolean visibleIDChoice = this.combModuleID.getItemCount() > 1;
    this.combModuleID.setVisible(visibleIDChoice);
    this.labelModuleID.setVisible(visibleIDChoice);
  }

  private void updateState() {
    Object selType = this.combModuleType.getSelectedItem();
    Object selID = this.combModuleID.getSelectedItem();

    // Edit exist
    if (this.isEditing) {
      // Update message
      this.labelMessage.setText("");
      // update Enablement
      setOkEnabled(selID != null);
      // Create new
    } else {
      boolean hasSlot = this.manager.hasAvailableSlot();
      boolean canCreate = false;
      // Update message
      if (selID == null && selType != null) {
        int max = ModuleEnums.getMaxModuleNumber((MODULE) selType);
        String msg =
            "Maximun number of " + ModuleResource.INSTANCE.getModuleTypeShortName((MODULE) selType) + " is " + max;
        this.labelMessage.setText(msg);
        canCreate = false;

      } else if (selType == null) {
        this.labelMessage.setText("You cannot add more modules");
        canCreate = false;

      } else if (!hasSlot && selType != MODULE.MODULE_HMI) {
        this.labelMessage.setText("There is not available slot."
            + " Please select another backplane type which supports more slots.");
        canCreate = false;

      } else {
        this.labelMessage.setText("");
        canCreate = true;
      }

      // update Enablement
      setOkEnabled(canCreate);
    }

    // Update description
    if ((MODULE) selType == MODULE.MODULE_SCM || (MODULE) selType == MODULE.MODULE_SCM_MK2) {
      this.labelDescription.setText("* PSM is required");
    } else {
      this.labelDescription.setText("");
    }
  }

  @Override
  public void ok() {
    lastSelectedType = (MODULE) this.combModuleType.getSelectedItem();
    MODULE_ID id = (MODULE_ID) this.combModuleID.getSelectedItem();

    // Create and add a new module
    if (!this.isEditing) {
      try {
        this.module = this.manager.addModule(lastSelectedType, id);
      } catch (Exception e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(this, "Cannot add module: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }

      // Update current module's ID
    } else {
      if (id != this.module.getId() && this.module instanceof ISupportChangeID) {
        ((ISupportChangeID) this.module).setId(id);
        // if(module instanceof ICANModule)
        // manager.notifyModuleChanged((ICANModule)module);
      }
    }

    // Dispose
    super.ok();
  }

  private void combModuleTypeItemStateChanged(ItemEvent e) {
    updateModuleIDs();
    updateBanner();
    updateState();
  }

  private void createUIComponents() {
    Set<MODULE> typeItems = this.moduleTypeIdMap.keySet();
    this.combModuleType = new JComboBox<MODULE>(typeItems.toArray(new MODULE[typeItems.size()]));
    this.combModuleType.setRenderer(new CombModuleTypeRenderer());

    // Select last selected item
    if (lastSelectedType != null) {
      this.combModuleType.setSelectedItem(lastSelectedType);
    }
  }

  private void combModuleIDItemStateChanged(ItemEvent e) {
    updateState();
  }

  protected void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    modulePanel = new JPanel();
    JLabel label1 = new JLabel();
    labelModuleID = new JLabel();
    combModuleID = new JComboBox<>();
    labelMessage = new JXLabel();
    labelDescription = new JXLabel();

    // ======== modulePanel ========
    {
      modulePanel.setLayout(new FormLayout("left:default, $lcgap, [100dlu,default]:grow",
          "default, $ugap, default, $lgap, fill:default, $lgap, bottom:default:grow"));

      // ---- label1 ----
      label1.setText("Module Type:");
      modulePanel.add(label1, CC.xy(1, 1));

      // ---- combModuleType ----
      combModuleType.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(ItemEvent e) {
          combModuleTypeItemStateChanged(e);
        }
      });
      modulePanel.add(combModuleType, CC.xy(3, 1));

      // ---- labelModuleID ----
      labelModuleID.setText("Module ID:");
      modulePanel.add(labelModuleID, CC.xy(1, 3));

      // ---- combModuleID ----
      combModuleID.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(ItemEvent e) {
          combModuleIDItemStateChanged(e);
        }
      });
      modulePanel.add(combModuleID, CC.xy(3, 3));

      // ---- labelMessage ----
      labelMessage.setForeground(Color.red);
      labelMessage.setBackground(UIManager.getColor("Panel.background"));
      labelMessage.setBorder(null);
      labelMessage.setLineWrap(true);
      modulePanel.add(labelMessage, CC.xy(3, 5));

      // ---- labelDescription ----
      labelDescription.setLineWrap(true);
      labelDescription.setText("   ");
      labelDescription.setForeground(Color.gray);
      modulePanel.add(labelDescription, CC.xywh(1, 7, 3, 1));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ----[ Custom code ]----
    this.combModuleID.setRenderer(new CombModuleIDRenderer());
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel modulePanel;
  private JComboBox<MODULE> combModuleType;
  private JLabel labelModuleID;
  private JComboBox<MODULE_ID> combModuleID;
  private JXLabel labelMessage;
  private JXLabel labelDescription;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected BannerPanel createBannerPanel() {
    BannerPanel b = new BannerPanel();
    b.setTitle("Creating a new module");
    return b;
  }

  @Override
  protected JComponent createContentPanel() {
    return this.modulePanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return createOKCancelButtonPanel();
  }


  static Border renderBorder = BorderFactory.createEmptyBorder(2, 8, 2, 8);


  private static class CombModuleTypeRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      MODULE type = (MODULE) value;
      if (type != null) {
        // String nameDisplay = String.format("[%s] %s",
        // ModuleFactory.instance.getModuleTypeShortName(type),
        // ModuleFactory.instance.getModuleTypeFullName(type));
        // this.setText(nameDisplay);
        this.setText(ModuleResource.INSTANCE.getModuleTypeShortName(type));
      }
      return this;
    }
  }

  private static class CombModuleIDRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {

      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      MODULE_ID id = (MODULE_ID) value;
      if (id != null) {
        this.setText(id.getDescription());
      }

      setBorder(renderBorder);
      return this;
    }
  }
}
