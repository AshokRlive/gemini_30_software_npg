/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain.stub;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.AbstractCANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_SWOUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class CANModuleStub extends AbstractCANModule implements ICANModule, ISupportChangeID, ISwitchModule {

  private SwitchModuleOutput[] outputs = new SwitchModuleOutput[2];


  public CANModuleStub() {
    this(MODULE_ID.MODULE_ID_0, MODULE.MODULE_SCM);
  }

  public CANModuleStub(MODULE type) {
    this(MODULE_ID.MODULE_ID_0, type);
  }

  public CANModuleStub(MODULE_ID id, MODULE type) {
    super(id, type);

    outputs = new SwitchModuleOutput[SwitchIndex.values().length];
    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new SwitchModuleOutput(this, SwitchIndex.forValue(i),
          SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_OPEN   ,
          SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_CLOSED ,
          SCM_CH_SWOUT.SCM_CH_SWOUT_OPEN_SWITCH     ,
          SCM_CH_SWOUT.SCM_CH_SWOUT_CLOSE_SWITCH         
          );
    }
  }

  @Override
  public void setId(MODULE_ID moduleID) {
    super.modifyId(moduleID);
  }

  @Override
  protected Map<ChannelType, ICANChannel[]> initChannels() {
    return null;
  }

  public static ICANModule newMCM() {
    return new CANModuleStub(MODULE.MODULE_MCM);
  }

  public static ICANModule newSCM(MODULE_ID id) {
    return new CANModuleStub(id, MODULE.MODULE_SCM);
  }

  public static ICANModule newPSM() {
    return new CANModuleStub(MODULE.MODULE_PSM);
  }

  public static ICANModule newIOM(MODULE_ID id) {
    return new CANModuleStub(id, MODULE.MODULE_IOM);
  }

  @Override
  public SwitchModuleOutput getSwitchOutput(SwitchIndex index) {
    return outputs[index.ordinal()];
  }

  @Override
  public Collection<SwitchModuleOutput> getAllSwitchOutputs() {
    return Arrays.asList(outputs);
  }

  @Override
  public boolean isSingleSwitch() {
    return false;
  }

  @Override
  public Object getOpenChannel(SwitchIndex index) {
    return null;
  }

  @Override
  public Object getCloseChannel(SwitchIndex index) {
    return null;
  }

  @Override
  public ISwitchModuleSettings getSettings() {
    return null;
  }

}
