/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.stub;

import java.util.Arrays;
import java.util.Collection;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModule;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_SWOUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 *
 *
 */
public class ModuleStub extends AbstractModule implements ISwitchModule {

  private SwitchModuleOutput[] outputs = new SwitchModuleOutput[2];


  public ModuleStub() {
    this(MODULE_ID.MODULE_ID_0, MODULE.MODULE_FDM);
  }

  public ModuleStub(MODULE_ID id, MODULE type) {
    super(id, type);

    outputs = new SwitchModuleOutput[SwitchIndex.values().length];
    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new SwitchModuleOutput(this, SwitchIndex.forValue(i),
          SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_OPEN   ,
          SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_CLOSED ,
          SCM_CH_SWOUT.SCM_CH_SWOUT_OPEN_SWITCH     ,
          SCM_CH_SWOUT.SCM_CH_SWOUT_CLOSE_SWITCH     );
    }
  }

  @Override
  public void delete() {
    for (SwitchModuleOutput output : outputs) {
      output.delete();
    }
    super.delete();
  }

  @Override
  public SwitchModuleOutput getSwitchOutput(SwitchIndex index) {
    return outputs[index.ordinal()];
  }

  @Override
  public Collection<SwitchModuleOutput> getAllSwitchOutputs() {
    return Arrays.asList(outputs);
  }

  public static ISwitchModule createSCM() {
    return new ModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_SCM);
  }

  public static ISwitchModule createDSM() {
    return new ModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_DSM);
  }

  @Override
  public boolean isSingleSwitch() {
    return false;
  }

  @Override
  public Object getOpenChannel(SwitchIndex index) {
    return null;
  }

  @Override
  public Object getCloseChannel(SwitchIndex index) {
    return null;
  }

  @Override
  public ISwitchModuleSettings getSettings() {
    return null;
  }

}
