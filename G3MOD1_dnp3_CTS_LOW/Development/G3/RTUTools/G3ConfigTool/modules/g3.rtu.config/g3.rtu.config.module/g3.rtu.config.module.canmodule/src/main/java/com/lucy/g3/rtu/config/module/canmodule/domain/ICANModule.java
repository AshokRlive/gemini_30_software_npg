/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.List;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 *
 *
 */
public interface ICANModule extends IOModule {

  @Override
  List<? extends ICANChannel> getAllChannels();

  @Override
  ICANChannel[] getChannels(ChannelType type);

  @Override
  ICANChannel getChByTyeAndID(int id, ChannelType type);

  @Override
  ICANChannel getChByEnum(IChannelEnum channelEnum);

  CANModuleManager getManager();
}
