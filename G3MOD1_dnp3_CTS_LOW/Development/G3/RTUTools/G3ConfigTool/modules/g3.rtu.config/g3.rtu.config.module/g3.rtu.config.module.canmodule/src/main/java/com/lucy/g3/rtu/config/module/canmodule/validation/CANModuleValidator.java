/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.validation;

import java.util.List;

import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public class CANModuleValidator extends AbstractValidator<ICANModule> {

  public CANModuleValidator(ICANModule target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {
    List<? extends ICANChannel> channels = target.getAllChannels();
    for (ICANChannel channel : channels) {
      channel.getValidator().validate();
      if (!channel.getValidator().isValid()) {
        result.addAllFrom(channel.getValidator().getResult());
      }
    }

  }

  @Override
  public String getTargetName() {
    return target.getName();
  }

}
