/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.ListModel;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.module.shared.AbstractModuleManager;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.manager.ExistModuleException;
import com.lucy.g3.rtu.config.module.shared.manager.MaxModuleNumException;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_PSUPPLY;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Implementation of {@code ModulesManager}.
 * <p>
 * All modules are managed in an {@code ArrayListModel}, which can be observed
 * by GUI layer.
 * </p>
 */
public final class CANModuleManagerImpl extends AbstractModuleManager<ICANModule> implements CANModuleManager {

  private BackPlaneType backplaneType = BackPlaneType.UNDEFINED;

  private final CANModuleManagerValidator validator = new CANModuleManagerValidator(this);

  private final ArrayListModel<IChannel> powerSupplyChannels = new ArrayListModel<>();
  
  public CANModuleManagerImpl(IConfig owner,ModuleRepository repo) {
    super(owner, repo);
  }

  @Override
  public boolean allowToAdd(ICANModule module) throws AddItemException {
    // Verify if the same module exists
    if (contains(module)) {
      throw new ExistModuleException(module);
    }

    // Verify if we can add more module of this type
    MODULE type = module.getType();
    int max = ModuleEnums.getMaxModuleNumber(type);
    if (getModuleNum(type) >= max) {
      throw new MaxModuleNumException("The type of module "
          + ModuleResource.INSTANCE.getModuleTypeShortName(type)
          + " has reached maximum number: " + max);
    }

    // Verify if there is available backplane slot
    if (module.getType() != MODULE.MODULE_HMI && !hasAvailableSlot()) {
      throw new MaxModuleNumException("Cannot add module. There is no spare backplane slots!");
    }

    return true;
  }

  @Override
  public ICANModule addModule(MODULE type, MODULE_ID id)
      throws MaxModuleNumException, ExistModuleException {

    ICANModule canModule = null;

    Module module = CANModuleFactory.createModule(type, id);
    if (module instanceof ICANModule) {
      canModule = (ICANModule) module;
      add(canModule);
    } else {
      this.log.error("Fail to add CAN Module. Type:" + type + " ID:" + id);
    }

    return canModule;
  }

  @Override
  public boolean contains(MODULE type, MODULE_ID id) {
    if (type == null || id == null) {
      return false;
    }
    Collection<ICANModule> allModules = getAllItems();
    for (ICANModule m : allModules) {
      if (m.getType() == type && m.getId() == id) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean allowToRemove(ICANModule item) {
    return isDefaultModule(item) == false;
  }
  
  public static boolean isDefaultModule(Module item) {
      return item != null && item.getType() == MODULE.MODULE_MCM;
  }

  @Override
  protected void postAddAction(ICANModule module) {
    // TODO sort module list
    
    // Add power supply channels
    if (module != null && module instanceof ModulePSM) {
      powerSupplyChannels.add(module.getChByEnum(PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_COMMS_PRIM));
      powerSupplyChannels.add(module.getChByEnum(PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_COMMS_SEC));
      powerSupplyChannels.add(module.getChByEnum(PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_24V_AUX));
    }
    
    ((AbstractCANModule)module).setManager(this);
    
    // Set HMI manager 
//    if(module instanceof ModuleHMI) {
//      ((ModuleHMI)module).setScreenManager(getOwner().getConfigModule("HMIScreenManager"));
//    }

  }

  @Override
  protected void postRemoveAction(ICANModule module) {
    module.delete();
    
    // Add power supply channels
    if (module != null && module instanceof ModulePSM) {
      powerSupplyChannels.remove(module.getChByEnum(PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_COMMS_PRIM));
      powerSupplyChannels.remove(module.getChByEnum(PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_COMMS_SEC));
      powerSupplyChannels.remove(module.getChByEnum(PSM_CH_PSUPPLY.PSM_CH_PSUPPLY_24V_AUX));
    }
    
    ((AbstractCANModule)module).setManager(null);
  }

  
  @Override
  @SuppressWarnings("unchecked")
  public ListModel<IChannel> getPowerSupplyChannelsListModel() {
    return powerSupplyChannels;
  }

  @Override
  public MODULE_ID[] getAvailableIDsByType(MODULE type) {
    if (type == null) {
      return new MODULE_ID[0];
    }

    // Get all supported IDs
    MODULE_ID[] ids = ModuleEnums.getSupportedIDs();

    /* Calculate the size of availableIDs list */
    int maxModuleNum = ModuleEnums.getMaxModuleNumber(type);
    int availableIDNum = Math.min(maxModuleNum, ids.length);

    /* Fill availableIDs list */
    List<MODULE_ID> availableIDs = new ArrayList<MODULE_ID>(availableIDNum);
    for (int i = 0; i < availableIDNum; i++) {
      availableIDs.add(ids[i]);
    }

    /* Remove used id from availableIDs list */
    Collection<ICANModule> allmodules = getAllItems();
    for (ICANModule module : allmodules) {
      if (module.getType() == type) {
        availableIDs.remove(module.getId());
      }
    }

    return availableIDs.toArray(new MODULE_ID[availableIDs.size()]);
  }

  @Override
  public int getModuleNum(MODULE moduleType) {
    if (moduleType == null) {
      return 0;
    }

    int num = 0;
    Collection<ICANModule> modules = getAllItems();
    for (ICANModule m : modules) {
      if (m.getType() == moduleType) {
        num++;
      }
    }
    return num;
  }

  /**
   * @return the number of modules these are mounted on backplane
   */
  @Override
  public int getBackPlaneModuleNum() {
    int counter = 0;
    Collection<ICANModule> modules = getAllItems();
    for (ICANModule module : modules) {
      // HMI doesn't take backplane slot.
      if (module.getType() != MODULE.MODULE_HMI) {
        counter++;
      }
    }

    return counter;
  }

  @Override
  public boolean hasAvailableSlot() {
    return getTotalBackPlaneSlotNum() - getBackPlaneModuleNum() > 0;
  }

  /**
   * Check if the modules number of the given type has reached maximum in this
   * manager.
   *
   * @param moduleType
   * @return if true, no more module in this type can be added
   */
  @Override
  public boolean hasReachedMaxNum(MODULE moduleType) {
    int totalAllowed = ModuleEnums.getMaxModuleNumber(moduleType);

    int existNum = getModuleNum(moduleType);

    return existNum < totalAllowed;
  }

  @Override
  public int getTotalBackPlaneSlotNum() {
    return this.backplaneType.getSlotNum();
  }

  @Override
  public BackPlaneType getBackplaneType() {
    return this.backplaneType;
  }

  @Override
  public void setBackplane(BackPlaneType backplane) throws MaxModuleNumException {
    if (backplane == null) {
      return;
    }
    int slotNum = backplane.getSlotNum();

    int existModuleNum = getBackPlaneModuleNum();
    if (slotNum > 0 && slotNum < existModuleNum) {
      throw new MaxModuleNumException("Not enough slots for the existing modules." +
          " \n Select a slot number at least: "
          + existModuleNum);
    }

    this.backplaneType = backplane;
  }

  @Override
  public CANModuleManagerValidator getValidator() {
    return validator;
  }


  private class CANModuleManagerValidator extends AbstractContainerValidator<CANModuleManager> {
    CANModuleManagerValidator (CANModuleManager target) {
      super(target);
    }
    
    @Override
    public Collection<IValidation> getValidatableChildrenItems() {
      return new ArrayList<IValidation>(getAllModules());
    }

    @Override
    public String getTargetName() {
      return "CAN Module Manager";
    }

  }
}