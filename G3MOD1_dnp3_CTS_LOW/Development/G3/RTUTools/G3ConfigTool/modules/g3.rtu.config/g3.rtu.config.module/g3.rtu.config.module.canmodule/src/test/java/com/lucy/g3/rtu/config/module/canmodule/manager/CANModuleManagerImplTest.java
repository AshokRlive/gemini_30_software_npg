/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManagerImpl;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.CANModuleStub;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.manager.MaxModuleNumException;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class CANModuleManagerImplTest {

  private CANModuleManager fixture;
  private ICANModule mcm;
  private ICANModule scm;
  private ArrayList<ICANModule> moduleList;


  @Before
  public void setUp() throws Exception {
    fixture = new CANModuleManagerImpl(null,null);
    mcm = CANModuleStub.newMCM();
    scm = CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0);

    moduleList = new ArrayList<ICANModule>();
    moduleList.add(CANModuleStub.newMCM());
    moduleList.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
    moduleList.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
    moduleList.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_2));
    moduleList.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_3));
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
    mcm = null;
    scm = null;
    moduleList = null;
  }

  @Test
  public void testGetModule() {
    assertNotNull(fixture.getAllItems());
    assertNotNull(fixture.getModulesByType(MODULE.MODULE_FDM));
    assertNotNull(fixture.getModulesByType(MODULE.MODULE_HMI));
    assertNotNull(fixture.getModulesByType(MODULE.MODULE_MCM));
    assertNotNull(fixture.getModulesByType(MODULE.MODULE_PSM));
    assertNotNull(fixture.getModulesByType(MODULE.MODULE_SCM));

    assertNull(fixture.getModule(MODULE.MODULE_FDM, MODULE_ID.MODULE_ID_0));
    assertNull(fixture.getModule(MODULE.MODULE_SCM, MODULE_ID.MODULE_ID_0));
    assertNull(fixture.getModule(MODULE.MODULE_HMI, MODULE_ID.MODULE_ID_0));
    assertNull(fixture.getModule(MODULE.MODULE_MCM, MODULE_ID.MODULE_ID_0));
  }

  @Test
  public void testAddModule() {
    try {
      fixture.add(mcm);
    } catch (Exception e1) {
      fail("No exception expected for adding MCM");
    }
    assertEquals(mcm, fixture.getModule(mcm.getType(), mcm.getId()));

    // Add SCM with
    try {
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
    } catch (Exception e) {
      fail("No exception expected: " + e.getClass().getSimpleName());
    }

    assertEquals(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0),
        fixture.getModule(MODULE.MODULE_SCM, MODULE_ID.MODULE_ID_0));
  }

  @Test
  public void testSetSlotNumber() {
    try {
      fixture.setBackplane(BackPlaneType.UNDEFINED);
    } catch (MaxModuleNumException e) {
      fail("No exception expected");
    }

    try {
      fixture.setBackplane(BackPlaneType.FOUR_SLOTS);
    } catch (MaxModuleNumException e) {
      fail("No exception expected");
    }

    try {
      fixture.add(mcm);
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_2));
    } catch (Exception e) {
      fail("No exception expected");
    }

    try {
      fixture.setBackplane(BackPlaneType.TWO_SLOTS);
      fail("Exception expected if new slot number < total module num");
    } catch (MaxModuleNumException e) {
    }

    try {
      fixture.setBackplane(BackPlaneType.FOUR_SLOTS);
      fixture.setBackplane(BackPlaneType.UNDEFINED);
    } catch (MaxModuleNumException e) {
      fail("No exception expected if new slot number >= total module num");
    }
  }

  @Test
  public void testAddMultipleMCMModule() {
    try {
      fixture.add(mcm);
    } catch (Exception e1) {
      fail("No exception expected");
    }

    // Add MCM twice
    try {
      fixture.add(mcm);
      fail("Exception expected if multiple MCM added");
    } catch (Exception e1) {
    }
  }

  @Test
  public void testAddMultipleSCMModule() {
    // Add same SCM twice
    try {
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
      fail("Exception expected");
    } catch (Exception e) {
    }

    try {
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_2));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_3));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_4));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_5));
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_6));
    } catch (Exception e) {
      fail("No exception expected if total number of SCM is less than 7");
    }

    try {
      fixture.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_6));
      fail("Exception expected if total number of SCM is larger than 7");
    } catch (Exception e) {

    }
  }

  @Test
  public void testLoadAllModules() {
    ArrayList<ICANModule> modules = new ArrayList<ICANModule>();
    modules.add(CANModuleStub.newMCM());
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_2));
    try {
      fixture.setBackplane(BackPlaneType.UNDEFINED);
      fixture.addAll(modules);
    } catch (Exception e) {
      fail("No exception expected if loading valid number of module");
    }

    try {
      fixture.addAll(modules);
      fail("Exception expected module list contains duplicated modules");
    } catch (Exception e) {
    }

    assertEquals(modules.size(), fixture.getSize());
  }

  @Test
  public void testLoadAllModulesWithDuplicatedModule() {
    ArrayList<ICANModule> modules = new ArrayList<ICANModule>();
    modules.add(CANModuleStub.newMCM());
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
    try {
      fixture.addAll(modules);
      fail("Exception expected module list contains duplicated modules");
    } catch (Exception e) {
      // Exception expected
    }
  }

  @Test
  public void testLoadAllModulesWithPrefSlotNum() {
    try {
      fixture.setBackplane(BackPlaneType.FOUR_SLOTS);
    } catch (MaxModuleNumException e1) {
      fail(e1.getMessage());
    }
    ArrayList<ICANModule> modules = new ArrayList<ICANModule>();
    modules.add(CANModuleStub.newMCM());
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_0));
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_1));
    modules.add(CANModuleStub.newSCM(MODULE_ID.MODULE_ID_2));
    try {
      fixture.addAll(modules);
    } catch (Exception e) {
      fail("No Exception expected if the size of module is no larger than slot num");
    }

    try {
      fixture.addModule(MODULE.MODULE_SCM, MODULE_ID.MODULE_ID_0);
      fail("Exception expected cause the SCM0 existed");
    } catch (Exception e) {
      // Exception expected
    }
  }

  @Test
  public void testLoadOversizedModules() {
    // 4 slots
    try {
      fixture.setBackplane(BackPlaneType.FOUR_SLOTS);
    } catch (MaxModuleNumException e1) {
      fail("Exception expected");
    }

    // try to load 5 module, exception occurs
    try {
      fixture.addAll(moduleList);
      fail("Exception expected if the size of module is larger than slot num");
    } catch (Exception e) {
      // Exception expected
    }
  }

  @Test
  public void testLoadAllModulesWithBackPlaneType() {
    // Set big enough slot number
    try {
      fixture.setBackplane(BackPlaneType.FIVE_SLOTS);
    } catch (MaxModuleNumException e1) {
      fail(e1.getMessage());
    }

    // Load 5 modules
    try {
      fixture.addAll(moduleList);
    } catch (Exception e) {
      fail("No Exception expected if the size of module is no larger than slot num");
    }

    assertEquals(moduleList.size(), fixture.getSize());
  }

  @Test
  public void testBackplaneType() {
    try {
      fixture.setBackplane(BackPlaneType.UNDEFINED);
    } catch (MaxModuleNumException e1) {
      fail(e1.getMessage());
    }

    try {
      // Add some modules
      fixture.addAll(moduleList);
    } catch (Exception e) {
      fail("Exception unexpected");
    }

    // try to set invalid slot number
    try {
      fixture.setBackplane(BackPlaneType.FOUR_SLOTS);
      fail("Exception expected");
    } catch (MaxModuleNumException e1) {
    }

    try {
      fixture.setBackplane(BackPlaneType.TWO_SLOTS);
      fail("Exception expected");
    } catch (MaxModuleNumException e1) {
    }

    // try to set valid slot number
    try {
      fixture.setBackplane(BackPlaneType.FIVE_SLOTS);
    } catch (MaxModuleNumException e1) {
      fail("Exception expected");
    }
  }

  @Test
  public void testRemoveModule() {
    // Remove an existed module
    try {
      fixture.add(scm);
      assertNotNull(fixture.getModule(scm.getType(), scm.getId()));
    } catch (Exception e) {
      fail("No exception expected here");
    }
    assertTrue(fixture.remove(scm));
    ICANModule m = fixture.getModule(scm.getType(), scm.getId());
    assertNull(m);

    // Remove an non-existed module
    try {
      fixture.remove(scm);
    } catch (Exception e) {
      fail("Unexpected exception when remove a unexisted module");
    }
  }

  @Test
  public void testRemoveMandatoryModule() {
    fixture.add(mcm);
    assertFalse(fixture.remove(mcm));
  }

  @Test
  public void testGetSwitchOutputs() {
    Collection<SwitchModuleOutput> outputs = fixture.getAllSwitchModuleOutputs();
    assertNotNull(outputs);
    assertEquals(0, outputs.size());

    fixture.add(scm);
    outputs = fixture.getAllSwitchModuleOutputs();
    assertNotNull(outputs);
    assertTrue(outputs.size() > 0);
  }

}
