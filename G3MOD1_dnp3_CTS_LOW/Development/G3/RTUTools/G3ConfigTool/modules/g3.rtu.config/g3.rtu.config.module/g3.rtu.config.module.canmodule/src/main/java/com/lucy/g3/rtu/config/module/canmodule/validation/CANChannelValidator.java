/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.validation;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.validation.AbstractChannelValidator;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public class CANChannelValidator extends AbstractChannelValidator {

  private static final HashMap<String, Long> LIMIT_MAX = new HashMap<String, Long>();
  private static final HashMap<String, Long> LIMIT_MIN = new HashMap<String, Long>();

  /* Initialise limits */
  static {
    LIMIT_MIN.put(ICANChannel.PARAM_OVERRUN_MS, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_OVERRUN_MS, 65535L);

    LIMIT_MIN.put(ICANChannel.PARAM_EVENT_MS, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_EVENT_MS, Long.MAX_VALUE);

    LIMIT_MIN.put(ICANChannel.PARAM_DBHIGH2LOWMS, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_DBHIGH2LOWMS, Long.MAX_VALUE);

    LIMIT_MIN.put(ICANChannel.PARAM_DBLOW2HIGHMS, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_DBLOW2HIGHMS, Long.MAX_VALUE);

    LIMIT_MIN.put(ICANChannel.PARAM_PULSELENGTH, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_PULSELENGTH, 65535L);

    LIMIT_MIN.put(ICANChannel.PARAM_OPTIMEOUT_SEC, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_OPTIMEOUT_SEC, 255L);
    
    LIMIT_MIN.put(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS, Long.MAX_VALUE);
    
    LIMIT_MIN.put(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS, 0L);
    LIMIT_MAX.put(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS, Long.MAX_VALUE);
  }

  private ICANChannel target;


  public CANChannelValidator(ICANChannel target) {
    super(target);
    this.target = target;
  }

  private void checkBoundary(ValidationResultExt result, String parameterName) {
    checkLongBoundary(result, parameterName, LIMIT_MIN.get(parameterName), LIMIT_MAX.get(parameterName));
  }

  @Override
  protected void validate(ValidationResultExt result) {

    if (target.hasParameter(ICANChannel.PARAM_OVERRUN_MS)) {
      checkBoundary(result, ICANChannel.PARAM_OVERRUN_MS);
    }

    if (target.hasParameter(ICANChannel.PARAM_EVENT_MS)) {
      checkBoundary(result, ICANChannel.PARAM_EVENT_MS);
    }

    if (target.hasParameter(ICANChannel.PARAM_DBHIGH2LOWMS)) {
      checkBoundary(result, ICANChannel.PARAM_DBHIGH2LOWMS);
    }

    if (target.hasParameter(ICANChannel.PARAM_DBLOW2HIGHMS)) {
      checkBoundary(result, ICANChannel.PARAM_DBLOW2HIGHMS);
    }

    if (target.hasParameter(ICANChannel.PARAM_PULSELENGTH)) {
      checkBoundary(result, ICANChannel.PARAM_PULSELENGTH);
    }
  }

  @Override
  public String getTargetName() {
    return target.getName();
  }

  @Override
  public void validate(String parameterName, Object valueObj)
      throws InvalidConfException {
    super.validate(parameterName, valueObj);

    /* Check boundary */
    if (valueObj instanceof Long) {
      String error = checkLongBoundary(parameterName, (Long) valueObj,
          LIMIT_MIN.get(parameterName), LIMIT_MAX.get(parameterName));
      if (error != null) {
        throw new InvalidConfException(error);
      }
    }
  }

}
