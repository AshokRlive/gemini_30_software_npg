/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


/**
 * The repository of G3 module instances.
 */
public final class ModuleRepository extends AbstractConfigModule implements IConfigModule {

  public final static String CONFIG_MODULE_ID = "ModuleRepository";
  
  private final ArrayList<IModuleManager<?>> managerList = new ArrayList<>();
  
  ModuleRepository(IConfig owner) {
    super(owner);
    log.debug("Created ModuleRepo: "+this);
  }
  

  /**
   * Gets all existing modules including CAN module, Field Device module, etc.
   *
   * @return
   */
  
  public Collection<Module> getAllModules() {
    ArrayList<Module> all = new ArrayList<>();
    for (IModuleManager<?> manager : managerList) {
      all.addAll(manager.getAllItems());
    }
    
    return all;
  }

  /**
   * Gets modules in specific types. If the type is empty or null, return all
   * existing modules.
   */
  
  public Collection<Module> getModulesByType(MODULE... types) {
    ArrayList<Module> all = new ArrayList<>();
    for (IModuleManager<?> manager : managerList) {
      all.addAll(manager.getModulesByType(types));
    }
    
    return all;
  }

  
  public Module getModule(MODULE type, MODULE_ID id) {
    Module module;
    for (IModuleManager<?> manager : managerList) {
      module = manager.getModule(type, id);
      if (module != null) {
        return module;
      }
    }
    
    return null;
  }

//  
//  public IChannel getChannel(MODULE mtype, MODULE_ID moduleID,
//      ChannelType type, int channelID) {
//
//    Module module = getModule(mtype, moduleID);
//    if (module != null && module instanceof IOModule) {
//      return ((IOModule)module).getChByTyeAndID(channelID, type);
//    } else {
//      log.warn("Module " + mtype + moduleID + " NOT found.");
//      return null;
//    }
//  }
//
//  
//  public Collection<IChannel> getAllChannels(ChannelType... type) {
//    return getAllChannels(null, type);
//  }
//
//  
//  public Collection<IChannel> getAllChannels(MODULE[] mtypes,
//      ChannelType... type) {
//
//    Collection<Module> modules = (mtypes == null) ? getAllModules() : getModulesByType(mtypes);
//
//    return ChannelFinderExt.findChannelInModules(modules, type);
//  }

  
  public Collection<SwitchModuleOutput> getAllSwitchModuleOutputs() {
    ArrayList<SwitchModuleOutput> all = new ArrayList<>();
    for (IModuleManager<?> manager : managerList) {
      all.addAll(manager.getAllSwitchModuleOutputs());
    }
    
    return all;
  }

  
  void addManager(IModuleManager<?> manager) {
    log.debug("Added module manager to repository: " + manager);
    
    if(!managerList.contains(manager)) {
      managerList.add(manager);
    } else {
      throw new IllegalStateException("Cannot add manager that already exists:" + manager);
    }
  }

}

