/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

/**
 * This enumeration lists all supported backplane types.
 */
public enum BackPlaneType {
  UNDEFINED(999, "Undefined"), // Unlimited slots number
  TWO_SLOTS(2, "2 Slots"),
  FOUR_SLOTS(4, "4 Slots"),
  FIVE_SLOTS(5, "5 Slots"),
  EIGHT_SLOTS(8, "8 Slots"),
  TEN_SLOTS(10, "10 Slots");

  BackPlaneType(int slotNum, String description) {
    this.slotNum = slotNum;
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return getDescription();
  }

  public int getSlotNum() {
    return slotNum;
  }


  private final String description;

  private final int slotNum;


  public static BackPlaneType Num2Enum(int slotNum) {
    BackPlaneType bp;
    switch (slotNum) {
    case 2:
      bp = BackPlaneType.TWO_SLOTS;
      break;
    case 4:
      bp = BackPlaneType.FOUR_SLOTS;
      break;
    case 5:
      bp = BackPlaneType.FIVE_SLOTS;
      break;
    case 8:
      bp = BackPlaneType.EIGHT_SLOTS;
      break;
    case 10:
      bp = BackPlaneType.TEN_SLOTS;
      break;
    default:
      bp = BackPlaneType.UNDEFINED;
      break;
    }
    return bp;
  }
}
