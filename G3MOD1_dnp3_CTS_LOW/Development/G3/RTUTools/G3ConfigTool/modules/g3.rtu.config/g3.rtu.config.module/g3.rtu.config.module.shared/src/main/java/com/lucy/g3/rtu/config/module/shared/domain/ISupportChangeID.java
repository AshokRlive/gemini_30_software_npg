/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The Module that implements this interface will support changing ID.
 */
public interface ISupportChangeID {

  /**
   * Sets this module's ID.
   *
   * @param moduleID
   *          the new module ID enumeration, which should not be
   *          <code>null</code>.
   * @see MODULE_ID
   */
  void setId(MODULE_ID moduleID);
}
