/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.module.shared.util.ModuleFinder;
import com.lucy.g3.rtu.config.shared.manager.AbstractListConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public abstract class AbstractModuleManager<T extends Module>
    extends AbstractListConfigModule<T>
    implements IModuleManager<T> {

  public AbstractModuleManager(IConfig owner, ModuleRepository repo) {
    this(owner,repo, null);
  }

  public AbstractModuleManager(IConfig owner, ModuleRepository repo, ArrayListModel<T> items) {
    super(owner, items);
    if(repo != null)
      repo.addManager(this);
    else
      log.warn("ModuleRepository is null");
  }

  @Override
  final public T getModule(MODULE type, MODULE_ID id) {
    return ModuleFinder.findModule(getAllItems(), type, id);
  }

  @Override
  final public Collection<T> getModulesByType(MODULE... types) {
    return ModuleFinder.findModuleByTypes(getAllItems(), types);
  }


  @Override
  final public Collection<T> getAllModules() {
    return getAllItems();
  }

  @Override
  public Collection<SwitchModuleOutput> getAllSwitchModuleOutputs() {
    ArrayList<SwitchModuleOutput> foundOutputs = new ArrayList<SwitchModuleOutput>();
    Collection<T> modules = getAllItems();
    for (Module m : modules) {
      if (m instanceof ISwitchModule) {
        foundOutputs.addAll(((ISwitchModule) m).getAllSwitchOutputs());
      }
    }

    return foundOutputs;
  }

}
