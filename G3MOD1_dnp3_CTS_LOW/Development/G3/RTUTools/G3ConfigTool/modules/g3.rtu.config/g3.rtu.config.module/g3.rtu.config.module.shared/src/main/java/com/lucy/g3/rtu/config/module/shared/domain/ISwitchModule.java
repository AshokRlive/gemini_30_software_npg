/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import java.util.Collection;

import com.lucy.g3.rtu.config.constants.SwitchIndex;

/**
 * The module that supports switch operation.
 */
public interface ISwitchModule extends Module {


  SwitchModuleOutput getSwitchOutput(SwitchIndex index);

  Collection<SwitchModuleOutput> getAllSwitchOutputs();

  boolean isSingleSwitch();
  
  Object getOpenChannel(SwitchIndex index);
  Object getCloseChannel(SwitchIndex index);
  
  @Override
  ISwitchModuleSettings getSettings();
}
