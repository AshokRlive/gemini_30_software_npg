/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleMCM;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModuleMCMTest {

  private ModuleMCM module;


  @Before
  public void setUp() throws Exception {
    module = new ModuleMCM(MODULE_ID.MODULE_ID_0);
  }

  @After
  public void tearDown() throws Exception {
    module = null;
  }

  @Test
  public void testGetChannel() {
    List<ICANChannel> chs = module.getAllChannels();
    assertNotNull(chs);
    assertTrue(chs.size() > 0);
  }
}
