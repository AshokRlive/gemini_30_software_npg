/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.jgoodies.forms.factories.Borders;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.NavigationEvent;
import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManagerImpl;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * This page shows the module list and action for managing module items.
 */
public class CANModuleManagePage extends AbstractConfigPage {

  private JScrollPane scrollPane1;

  private JList<?> moduleList;

  private JPopupMenu popMenu;

  private CANModuleManagerModel pm;

  private AbstractAction navAction = new NavigationAction();

  private final CANModuleManager manager;

  private Action[] contextActions;


  public CANModuleManagePage(CANModuleManager manager) {
    super(manager);
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
    this.pm = new CANModuleManagerModel(manager);
    setNodeName("Modules");
    this.contextActions = new Action[] { this.pm.getAction(CANModuleManagerModel.ACTION_KEY_ADD) };
  }

  @Override
  protected void init() {
    initComponents();
  }

  private JPopupMenu getPopupMenu() {
    if (this.popMenu == null) {
      this.popMenu = new JPopupMenu();
      this.popMenu.add(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_ADD));
      this.popMenu.add(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_REMOVE));
      this.popMenu.add(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_MODIFY_ID));
      this.popMenu.addSeparator();
      this.popMenu.add(this.navAction);
    }
    return this.popMenu;
  }

  private void initComponents() {
    this.moduleList = BasicComponentFactory.createList(this.pm.getModuleSelectionList());
    this.moduleList.setCellRenderer(new ModuleCellRenderer());
    this.moduleList.addMouseListener(new MouseListener());
    this.moduleList.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
    this.moduleList.getActionMap().put("Delete", this.pm.getAction(CANModuleManagerModel.ACTION_KEY_REMOVE));

    this.scrollPane1 = new JScrollPane(this.moduleList);

    setLayout(new BorderLayout(5, 5));
    setBorder(Borders.DIALOG_BORDER);

    add(this.scrollPane1, BorderLayout.CENTER);
    add(createControlPanel(), BorderLayout.EAST);
  }

  @SuppressWarnings("unchecked")
  private JPanel createControlPanel() {
    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    panelBuilder.setDefaultDialogBorder();
    panelBuilder.addGridded(new JButton(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_ADD)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_REMOVE)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_MODIFY_ID)));
    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_MOVE_UP)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(this.pm.getAction(CANModuleManagerModel.ACTION_KEY_MOVE_DOWN)));
    panelBuilder.addUnrelatedGap();

    panelBuilder.addGridded(new JLabel("Backplane Type: "));
    PropertyAdapter<CANModuleManagerModel> vmBp = new PropertyAdapter<CANModuleManagerModel>(this.pm,
        CANModuleManagerModel.PROPERTY_BACKPLANE);
    ComboBoxAdapter<BackPlaneType> combModel = new ComboBoxAdapter<BackPlaneType>(BackPlaneType.values(), vmBp);
    panelBuilder.addGridded(new JComboBox<BackPlaneType>(combModel));
    panelBuilder.addUnrelatedGap();

    JPanel panel = panelBuilder.getPanel();
    panel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent evt) {
        CANModuleManagePage.this.pm.getModuleSelectionList().clearSelection();
      }
    });
    return panel;
  }


  private class MouseListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      // Double click to go to configuration page of the selection
      if (UIUtils.isDoubleClick(e)) {
        CANModuleManagePage.this.navAction.actionPerformed(null);
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
      // Select index on mouse press
      int s = CANModuleManagePage.this.moduleList.locationToIndex(e.getPoint());
      CANModuleManagePage.this.moduleList.setSelectedIndex(s);

      mayShowPopupMenu(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      mayShowPopupMenu(e);
    }

    private void mayShowPopupMenu(MouseEvent e) {
      if (e.isPopupTrigger() && CANModuleManagePage.this.moduleList.getSelectedValue() != null) {
        getPopupMenu().show(e.getComponent(), e.getX(), e.getY());
      }
    }
  }

  private class ModuleCellRenderer extends JLabel implements ListCellRenderer<Object> {

    private Border cellBorder = new EmptyBorder(5, 10, 5, 0);


    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      final Module module = (Module) value;
      setIcon(module.getIcons().getNormalIcon());
      setText(String.format("[%s] %s", ModuleResource.INSTANCE.getModuleTypeShortName(module.getType()),
          module.getFullName()));

      setIconTextGap(10);
      setBorder(this.cellBorder);

      if (isSelected) {
        setOpaque(true);
        setBackground(list.getSelectionBackground());
        setForeground(list.getSelectionForeground());
      } else {
        setOpaque(false);
        setBackground(list.getBackground());

        if (CANModuleManagerImpl.isDefaultModule(module) == false) {
          setForeground(list.getForeground());
        } else {
          setForeground(Color.darkGray); // Grey colour if the module cannot be
          // removed
        }
      }
      return this;
    }

  }

  private class NavigationAction extends AbstractAction {

    public NavigationAction() {
      super("Detail...");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      fireNavigationEvent(new NavigationEvent(CANModuleManagePage.this.moduleList.getSelectedValue()));
    }
  }


  @Override
  public ListModel<ICANModule> getChildrenDataList() {
    return this.manager.getItemListModel();
  }

  @Override
  public Action[] getContextActions() {
    return this.contextActions;
  }

}
