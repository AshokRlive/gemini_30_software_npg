/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.manager;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.shared.AbstractModuleManager;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


/**
 *
 */
public abstract class AbstractIOModuleManager<T extends IOModule>
    extends AbstractModuleManager<T>
    implements IModuleManager<T>{
  
  
  protected AbstractIOModuleManager(IConfig owner, ModuleRepository repo) {
    super(owner,repo);
  }

  protected AbstractIOModuleManager(IConfig owner, ModuleRepository repo,ArrayListModel<T> items) {
    super(owner,repo, items);
  }

//  @Override
//  final public Collection<IChannel> getAllChannels(ChannelType... type) {
//    return getAllChannels(null, type);
//  }
//
//  @Override
//  final public Collection<IChannel> getAllChannels(MODULE[] mtypes,
//      ChannelType... type) {
//
//    Collection<T> modules = (mtypes == null) ? getAllItems() : getModulesByType(mtypes);
//
//    return ChannelFinderExt.findChannelInModules(modules, type);
//  }
//
//  @Override
//  final public IChannel getChannel(MODULE mtype, MODULE_ID moduleID,
//      ChannelType type, int channelID) {
//
//    IOModule module = getModule(mtype, moduleID);
//    if (module != null) {
//      return module.getChByTyeAndID(channelID, type);
//    } else {
//      log.warn("Module " + mtype + moduleID + " NOT found.");
//      return null;
//    }
//  }
}

