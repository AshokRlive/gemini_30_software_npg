/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

/**
 * This enumeration lists all options for switch polarity.
 */
public enum Polarity {
  SWITCH_POLARITY_UP("Pull Up"),
  SWITCH_POLARITY_DOWN("Pull Down"),
  SWITCH_POLARITY_LINK("Link(Volt Free)");

  Polarity(String name) {
    this.name = name;
  }

  public int getValue() {
    return ordinal();
  }

  @Override
  public String toString() {
    return name;
  }

  public static Polarity forValue(int value) {
    Polarity[] values = Polarity.values();
    if (value >= 0 && value < values.length) {
      return values[value];
    }

    return null;
  }


  private String name;

}
