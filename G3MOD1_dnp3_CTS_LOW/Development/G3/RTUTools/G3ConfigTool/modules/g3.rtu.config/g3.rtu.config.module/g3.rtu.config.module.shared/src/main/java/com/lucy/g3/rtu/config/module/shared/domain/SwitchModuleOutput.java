/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 * The output of switch module.
 */
public final class SwitchModuleOutput extends AbstractNode {
  
  private Logger log = Logger.getLogger(SwitchModuleOutput.class);
  
  private final ISwitchModule module;
  private final SwitchIndex index;

  /* The enum of Open/close channel to be mapped to this output.*/
  private final IChannelEnum openEnumInput;
  private final IChannelEnum closeEnumInput;
  
  private final IChannelEnum openEnumOutput;
  private final IChannelEnum closeEnumOutput;
  
  public SwitchModuleOutput(ISwitchModule module, SwitchIndex index,
      IChannelEnum openEnumInput,
      IChannelEnum closeEnumInput,
      IChannelEnum openEnumOutput,
      IChannelEnum closeEnumOutput) {
    super(NodeType.SWITCH_OUTPUT);

    this.module = Preconditions.checkNotNull(module, "module is null");
    this.index = Preconditions.checkNotNull(index, "index is null");
    this.openEnumInput   = openEnumInput  ;
    this.closeEnumInput  = closeEnumInput ;
    this.openEnumOutput  = openEnumOutput ;
    this.closeEnumOutput = closeEnumOutput;
    
    
    bindNameToModule();
  }

  
  public IChannelEnum getOpenEnumForOutput() {
    return openEnumOutput;
  }
  
  
  public IChannelEnum getCloseEnumForOutput() {
    return closeEnumOutput;
  }
  
  public IChannelEnum getOpenEnumForInput() {
    return openEnumInput;
  }

  
  public IChannelEnum getCloseEnumForInput() {
    return closeEnumInput;
  }
  
  public boolean isUsable(){
    // refs #3530 Commented out to allow this switch to be reused always.
    //return this.isConnectedToTarget(NodeType.CLOGIC);
    return true;
  }

  private void bindNameToModule() {
    module.addPropertyChangeListener(Module.PROPERTY_SHORTNAME, moduleNamePCL);
    updateName();
  }

  public ISwitchModule getModule() {
    return module;
  }

  public SwitchIndex getIndex() {
    return index;
  }

  @Override
  public void delete() {
    super.delete();
    module.removePropertyChangeListener(Module.PROPERTY_SHORTNAME, moduleNamePCL);
  }

  private void updateName() {
    if (module.isSingleSwitch()) {
      setName(String.format("%s", module.getShortName()));
    } else {
      setName(String.format("%s - %s", module.getShortName(), index.getName()));
    }

  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
  }


  private final PropertyChangeListener moduleNamePCL = new PropertyChangeListener() {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateName();
    }
  };

}
