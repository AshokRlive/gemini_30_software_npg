/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumDSM.DSM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumDSM.DSM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumDSM.DSM_CH_DOUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumDSM.DSM_CH_SWOUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Switch Control Module.
 */
public final class ModuleDSM extends AbstractCANModule implements ICANModule, ISupportChangeID, ISwitchModule {

  private Logger log = Logger.getLogger(ModuleDSM.class);


  private static SwitchIndex[] DSM_SWITCHES =  {
    SwitchIndex.SwitchA,
    SwitchIndex.SwitchB,
  };
  
  private final SwitchModuleOutput[] outputs;
  
  private final ModuleDSMSettings settings = new ModuleDSMSettings();


  public ModuleDSM() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleDSM(MODULE_ID id) {
    super(id, MODULE.MODULE_DSM);
    outputs = initSwitchOutputs();
  }
  
  private SwitchModuleOutput[] initSwitchOutputs() {
    SwitchIndex[] indexes = DSM_SWITCHES;
    SwitchModuleOutput[] outputs = new SwitchModuleOutput[indexes.length];
    for (int i = 0; i < indexes.length; i++) {
      switch (indexes[i]) {
      case SwitchA:
        outputs[i] = new SwitchModuleOutput(this, indexes[i],
            DSM_CH_DINPUT.DSM_CH_DINPUT_SWITCH_A_OPEN,     
            DSM_CH_DINPUT.DSM_CH_DINPUT_SWITCH_A_CLOSED,    
            DSM_CH_SWOUT.DSM_CH_SWOUT_OPEN_SWITCH_A,       
            DSM_CH_SWOUT.DSM_CH_SWOUT_CLOSE_SWITCH_A);
        break;
      case SwitchB:
        outputs[i] = new SwitchModuleOutput(this, indexes[i],
            DSM_CH_DINPUT.DSM_CH_DINPUT_SWITCH_B_OPEN,     
            DSM_CH_DINPUT.DSM_CH_DINPUT_SWITCH_B_CLOSED,  
            DSM_CH_SWOUT.DSM_CH_SWOUT_OPEN_SWITCH_B,     
            DSM_CH_SWOUT.DSM_CH_SWOUT_CLOSE_SWITCH_B);      
        break;
      default:
        throw new IllegalStateException("No open/close enum found for :" +indexes[i]);
      }
    }
    
    return outputs;
  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise analogue input channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, DSM_CH_AINPUT.values()));

    // Initialise digital input channels
    chMap.put(ChannelType.DIGITAL_INPUT,
        createChannels(ChannelType.DIGITAL_INPUT, DSM_CH_DINPUT.values()));

    // Initialise Switch output channels
    chMap.put(ChannelType.SWITCH_OUT,
        createChannels(ChannelType.SWITCH_OUT, DSM_CH_SWOUT.values()));

    // Initialise digital output channels
    chMap.put(ChannelType.DIGITAL_OUTPUT,
        createChannels(ChannelType.DIGITAL_OUTPUT, DSM_CH_DOUT.values()));

    // Initialise switch Output channels' inhibit mask
    final ICANChannel[] inputChs = chMap.get(ChannelType.DIGITAL_INPUT);
    ICANChannel[] swoutChs = chMap.get(ChannelType.SWITCH_OUT);
    for (int j = 0; j < swoutChs.length; j++) {
      swoutChs[j].setParameter(ICANChannel.PARAM_INHIBI, new Inhibit(inputChs));
    }

    return chMap;
  }

  @Override
  public void setId(MODULE_ID moduleID) {
    super.modifyId(moduleID);
  }
  
  @Override
  public SwitchModuleOutput getSwitchOutput(SwitchIndex index) {
    return outputs[index.ordinal()];
  }

  @Override
  public Collection<SwitchModuleOutput> getAllSwitchOutputs() {
    return Arrays.asList(outputs);
  }

  @Override
  public boolean isSingleSwitch() {
    return false;
  }

  @Override
  public Object getOpenChannel(SwitchIndex index) {
    switch (index) {
    case SwitchA:
      return getChByEnum(DSM_CH_SWOUT.DSM_CH_SWOUT_OPEN_SWITCH_A);
    case SwitchB:
      return getChByEnum(DSM_CH_SWOUT.DSM_CH_SWOUT_OPEN_SWITCH_B);
    default:
      break;
    }
    
    return null;
  }

  @Override
  public Object getCloseChannel(SwitchIndex index) {
    switch (index) {
    case SwitchA:
      return getChByEnum(DSM_CH_SWOUT.DSM_CH_SWOUT_CLOSE_SWITCH_A);
    case SwitchB:
      return getChByEnum(DSM_CH_SWOUT.DSM_CH_SWOUT_CLOSE_SWITCH_B);
    default:
      break;
    }
    
    return null;
  }

  @Override
  public ModuleDSMSettings getSettings() {
    return settings;
  }


}