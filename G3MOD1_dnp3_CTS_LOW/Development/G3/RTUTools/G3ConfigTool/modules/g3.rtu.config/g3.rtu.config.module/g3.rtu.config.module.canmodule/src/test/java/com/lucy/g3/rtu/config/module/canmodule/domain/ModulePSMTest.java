/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSMSettings;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModulePSMTest {

  private ModulePSM module;
  private PresentationModel<ModulePSM> model;

  public String[] readWriteProperties =
      new String[] {
          ModulePSMSettings.PROPERTY_FAN_FITTED,
          ModulePSMSettings.PROPERTY_FAN_SPEED_SENSOR_FITTED,
          ModulePSMSettings.PROPERTY_FAN_TEMP_HYSTERESIS,
          ModulePSMSettings.PROPERTY_FAN_TEMP_THRESHOLD,
          ModulePSMSettings.PROPERTY_FAULT_HYSTERESIS,
      };


  @Before
  public void setUp() throws Exception {
    module = new ModulePSM(MODULE_ID.MODULE_ID_0);
    model = new PresentationModel<ModulePSM>(module);
  }

  @After
  public void tearDown() throws Exception {
    model.release();
    model = null;
    module = null;
  }

  @Test
  public void testReadWriteProperties() {
  //  BeanTestUtil.testReadWriteProperties(module.getSettings(), readWriteProperties);
  }


  @Test
  public void testFanTestChannel() {
    Assert.assertNotNull(module.getFanTestChannel());
  }

}
