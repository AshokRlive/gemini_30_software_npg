/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleHMI;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleHMISettings;
import com.lucy.g3.test.support.utilities.BeanTestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModuleHMITest {

  private ModuleHMI module;
  private PresentationModel<ModuleHMI> model;

  public String[] readWriteProperties =
      new String[] {
      ModuleHMISettings.PROPERTY_BUTTONCLICK,
      ModuleHMISettings.PROPERTY_OPENCLOSE_PRESSTIME,
      ModuleHMISettings.PROPERTY_REQUESTOLR
      };


  @Before
  public void setUp() throws Exception {
    module = new ModuleHMI(MODULE_ID.MODULE_ID_0);
    model = new PresentationModel<ModuleHMI>(module);
  }

  @After
  public void tearDown() throws Exception {
    model.release();
    model = null;
    module = null;
  }

  @Test
  public void testReadWriteProperties() {
    BeanTestUtil.testReadWriteProperties(new ModuleHMISettings(null), readWriteProperties);
  }

}
