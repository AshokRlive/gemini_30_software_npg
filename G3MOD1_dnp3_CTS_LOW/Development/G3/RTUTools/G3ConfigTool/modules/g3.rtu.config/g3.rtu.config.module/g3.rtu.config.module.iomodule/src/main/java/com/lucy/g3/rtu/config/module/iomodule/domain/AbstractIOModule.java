/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.domain;

import java.util.List;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


/**
 *
 */
public abstract class AbstractIOModule extends AbstractModule implements IOModule{

  protected AbstractIOModule(MODULE_ID id, MODULE type) {
    super(id, type);
  }

  
  @Override
  public void delete() {
    // Delete all channel this module owns.
    List<? extends IChannel> allChs = getAllChannels();
    if (allChs != null) {
      for (IChannel ch : allChs) {
        if (ch != null) {
          ch.delete();
        }
      }
    }
    
    super.delete();
  }
  
  
  @Override
  public IChannel getChByTyeAndID(int id, ChannelType type) {
    IChannel[] channels = getChannels(type);
    if (channels != null) {
      for (int i = 0; i < channels.length; i++) {
        if (channels[i].getId() == id && channels[i].getType() == type) {
          return channels[i];
        }
      }
    }

    return null;
  }
}

