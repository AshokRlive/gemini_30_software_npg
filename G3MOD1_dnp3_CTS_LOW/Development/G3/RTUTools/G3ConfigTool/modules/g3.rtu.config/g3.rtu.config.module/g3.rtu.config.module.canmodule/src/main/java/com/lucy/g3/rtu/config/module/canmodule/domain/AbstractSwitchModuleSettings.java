/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import com.lucy.g3.rtu.config.module.shared.domain.AbstractModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModuleSettings;


/**
 *
 */
public abstract class AbstractSwitchModuleSettings extends AbstractModuleSettings implements ISwitchModuleSettings{
}

