/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.ui.widgets.CTRatioEditorCombox;

class FPMOptionsPanel2 extends WizardPage {

  public static final String KEY_FPM_CTRATIO = "fpmCTRatio"; // CTRatio
  static final int ctRatio = 0;


  public FPMOptionsPanel2() {
    initComponents();
    comboCTRatio.setName(KEY_FPM_CTRATIO);
    comboCTRatio.getEditor().getEditorComponent().setName(KEY_FPM_CTRATIO);
  }

  private void createUIComponents() {
    // TODO: add custom component creation code here
    comboCTRatio = new CTRatioEditorCombox(FPIConfig.getDefaultCTRatio());
  }

  public static int getCtratio() {
    return ctRatio;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default",
        "2*(default, $lgap), default"));

    // ---- label1 ----
    label1.setText("CT Ratio");
    add(label1, CC.xy(1, 1));
    add(comboCTRatio, CC.xy(3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JComboBox<CTRatio> comboCTRatio;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
