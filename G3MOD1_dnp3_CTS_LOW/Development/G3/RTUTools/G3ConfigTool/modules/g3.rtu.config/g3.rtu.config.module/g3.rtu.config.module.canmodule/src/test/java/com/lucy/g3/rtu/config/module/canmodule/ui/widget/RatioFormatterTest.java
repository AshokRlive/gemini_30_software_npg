/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.widget;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.text.DateFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.ui.widgets.RatioFormatter;

public class RatioFormatterTest {

  private RatioFormatter fixture;

  final private static Object[][] samples = {
      { new CTRatio(1, 1), "1:1" },
      { new CTRatio(2, 3), "2:3" },
      { new CTRatio(1, 2), "1:2" },
      { new CTRatio(0, 1), "0:1" },
  };


  @Before
  public void setUp() throws Exception {
    fixture = new RatioFormatter();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testFormatter_valueToStr() {
    for (int i = 0; i < samples.length; i++) {
      try {
        String result = fixture.valueToString(samples[i][0]);
        assertEquals(samples[i][1], result);

      } catch (ParseException e) {
        e.printStackTrace();
        fail("Testing sample failed at:" + i);
      }
    }
  }

  @Test
  public void testFormatter_strToValue() {
    for (int i = 0; i < samples.length; i++) {
      try {
        CTRatio result = fixture.stringToValue((String) samples[i][1]);
        assertEquals(samples[i][0], result);

      } catch (ParseException e) {
        e.printStackTrace();
        fail("Testing sample failed at:" + i);
      }
    }
  }

  public static void main(String[] args) {
    JFrame f = new JFrame();
    JPanel c = new JPanel();

    JFormattedTextField ftf;
    c.add(ftf = new JFormattedTextField(new RatioFormatter()));
    ftf.setColumns(10);
    ftf.setValue(new CTRatio(1, 2));

    c.add(ftf = new JFormattedTextField(new DateFormatter()));
    ftf.setColumns(10);
    ftf.setValue(new Date());

    f.setContentPane(c);
    f.pack();
    f.setVisible(true);
  }

}
