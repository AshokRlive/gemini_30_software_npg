/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.utils;

import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPM;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;


/**
 *
 */
public final class CANModuleUtility implements IConfigUtility{
  private CANModuleUtility() {}
  
  public static ListModel<IChannel> getPowerChannelListModel(IConfig config) {
    ListModel<IChannel> powerChannels = null;
    
    CANModuleManager manager = config == null ? null
        : (CANModuleManager) config.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
    if (manager != null) {
      powerChannels = manager.getPowerSupplyChannelsListModel();
    }
    
    if (powerChannels == null) {
      powerChannels = new DefaultListModel<IChannel>();
      Logger.getLogger(CANModuleUtility.class).error("power channel list mode not found!");
    }
    
    return powerChannels;
  }
  
  public static FPIConfig[] getAllFPIChannels(IConfig config) {
    CANModuleManager manager = config.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
    ArrayList<ICANModule> fpms = new ArrayList<>(manager.getModulesByType(MODULE.MODULE_FPM));
    ArrayList<FPIConfig> fpiList = new ArrayList<>();
    
    for (ICANModule fpm:fpms) {
      fpiList.addAll(((ModuleFPM)fpm).getSettings().getAllFpiAsList());
    }
    
    return fpiList.toArray(new FPIConfig[fpiList.size()]);
  }

}

