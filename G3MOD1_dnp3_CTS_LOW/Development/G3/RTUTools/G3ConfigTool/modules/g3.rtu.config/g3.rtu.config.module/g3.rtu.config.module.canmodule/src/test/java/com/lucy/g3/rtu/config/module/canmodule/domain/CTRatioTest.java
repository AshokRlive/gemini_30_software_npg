/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;

public class CTRatioTest {

  @Test
  public void testConstructor() {
    new CTRatio(0, 1);
  }

  @Test(expected = Exception.class)
  public void testDividendNotNegative() {
    new CTRatio(-1, 1);
  }

  @Test(expected = Exception.class)
  public void testDivisorNotZero() {
    new CTRatio(1, 0);
  }

  @Test(expected = Exception.class)
  public void testDivisorNotNegative() {
    new CTRatio(1, 0);
  }

  @Test
  public void testCalculateScaingFactor() {
    CTRatio ratio = new CTRatio(500, 1);
    assertEquals(0.05, ratio.calculateScaingFactor(), 0);
  }
}
