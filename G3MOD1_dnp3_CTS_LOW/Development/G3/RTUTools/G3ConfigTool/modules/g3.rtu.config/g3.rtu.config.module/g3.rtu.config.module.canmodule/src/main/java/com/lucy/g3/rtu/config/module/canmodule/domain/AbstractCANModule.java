/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.validation.CANModuleValidator;
import com.lucy.g3.rtu.config.module.iomodule.domain.AbstractIOModule;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public abstract class AbstractCANModule extends AbstractIOModule implements ICANModule {

  private ArrayList<ICANChannel> allChannels;

  private Map<ChannelType, ICANChannel[]> channelsMap;

  private CANModuleManager manager;
  
  
  public AbstractCANModule(MODULE_ID id, MODULE type) {
    super(id, type);
    init();
  }

  void setManager(CANModuleManager manager) {
    this.manager = manager; 
  }
  
  @Override
  public CANModuleManager getManager() {
    return manager;
  }
  
  private void init() {
    /* Initialise channels */
    channelsMap = initChannels();
    if (channelsMap == null) {
      channelsMap = new HashMap<ChannelType, ICANChannel[]>();
    }

    /* Scan channels map and remove null&empty channels */
    Set<ChannelType> keys = channelsMap.keySet();
    for (ChannelType key : keys) {
      ICANChannel[] chs = channelsMap.get(key);
      if (chs == null || chs.length == 0) {
        channelsMap.remove(key);
      }
    }

    validator = new CANModuleValidator(this);
  }

  @Override
  public ICANChannel getChByTyeAndID(int id, ChannelType type) {
    return (ICANChannel) super.getChByTyeAndID(id, type);
  }

  @Override
  public ChannelType[] getAvailableChannelTypes() {
    Set<ChannelType> keys = channelsMap.keySet();
    return keys.toArray(new ChannelType[keys.size()]);
  }

  @Override
  public ICANChannel[] getChannels(ChannelType type) {
    ICANChannel[] chs = channelsMap.get(type);

    if (chs == null) {
      chs = new ICANChannel[0];
    } else {
      chs = Arrays.copyOf(chs, chs.length);
    }
    return chs;
  }

  @Override
  public ICANChannel getChByEnum(IChannelEnum channelEnum) {
    List<ICANChannel> allchs = getAllChannels();
    for (ICANChannel ch : allchs) {
      if (ch.getEnum() == channelEnum) {
        return ch;
      }
    }

    return null;
  }

  @Override
  public List<ICANChannel> getAllChannels() {
    if (allChannels == null) {
      allChannels = new ArrayList<ICANChannel>();
      ChannelType[] types = getAvailableChannelTypes();
      if (types != null) {
        for (int i = 0; i < types.length; i++) {
          ICANChannel[] chs = getChannels(types[i]);
          if (chs != null) {
            allChannels.addAll(Arrays.asList(chs));
          }
        }
      }
    }

    return new ArrayList<ICANChannel>(allChannels);
  }

  /**
   * Initialise all channels for this module.
   * <p>
   * {@link #createChannels(ChannelType, Enum[])} could be used for constructing
   * channel instances.
   * </p>
   *
   * @return a set of types and channels supported by this module.
   */
  protected abstract Map<ChannelType, ICANChannel[]> initChannels();

  /**
   * Convenient method for constructing channels from channel enum.
   */
  protected final ICANChannel[] createChannels(ChannelType type,
      Enum<? extends IChannelEnum>[] enums) {
    return createChannels(type, enums, false);
  }

  /**
   * Convenient method for constructing channels from channel enum.
   */
  protected final ICANChannel[] createChannels(ChannelType type,
      Enum<? extends IChannelEnum>[] enums, boolean readonly) {
    ICANChannel[] channels;
    channels = new ICANChannel[enums.length];

    for (int i = 0; i < enums.length; i++) {
      channels[i] = createChannel(type, enums[i], readonly);
    }
    return channels;
  }
  
  protected CANChannel createChannel(ChannelType type,
      Enum<? extends IChannelEnum> e, boolean readonly) {
    return CANChannelFactory.createChannel(type, this, e, readonly);
  }
}
