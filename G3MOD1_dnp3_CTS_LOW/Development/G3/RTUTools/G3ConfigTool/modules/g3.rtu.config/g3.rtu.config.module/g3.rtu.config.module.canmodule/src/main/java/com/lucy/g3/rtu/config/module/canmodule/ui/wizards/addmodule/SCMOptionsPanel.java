/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import javax.swing.JCheckBox;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 *
 */
class SCMOptionsPanel extends WizardPage {

  static final String KEY_CREATE_SCM_SWITCHGEAR_CLOGIC = "CREATE_SCM_SWITCHGEAR_CLOGIC";
  static final boolean createSCMSwitchgearCLogicDefault = false;


  public SCMOptionsPanel() {
    initComponents();
    chkCreateCLogic.setName(KEY_CREATE_SCM_SWITCHGEAR_CLOGIC);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    chkCreateCLogic = new JCheckBox();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default",
        "2*(default, $lgap), default"));

    // ---- chkCreateCLogic ----
    chkCreateCLogic.setText("Create Switchgear Logic");
    add(chkCreateCLogic, CC.xy(1, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox chkCreateCLogic;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
