/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumDSM.DSM_CH_DOUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_BCHARGER;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_DOUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_FAN;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_PSUPPLY;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Power Supply Module.
 */
public final class ModulePSM extends AbstractCANModule implements ICANModule {

  private final ModulePSMSettings settings = new ModulePSMSettings();
  
  public ModulePSM() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModulePSM(MODULE_ID id) {
    super(id, MODULE.MODULE_PSM);
  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise analogue input channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, PSM_CH_AINPUT.values()));

    // Initialise digital input channels
    chMap.put(ChannelType.DIGITAL_INPUT,
        createChannels(ChannelType.DIGITAL_INPUT, PSM_CH_DINPUT.values()));
    
    // Initialise digital output channels
    chMap.put(ChannelType.DIGITAL_OUTPUT,
        createChannels(ChannelType.DIGITAL_OUTPUT, PSM_CH_DOUT.values()));

    // Initialise battery charger channels
    chMap.put(ChannelType.PSM_CH_BCHARGER,
        createChannels(ChannelType.PSM_CH_BCHARGER, PSM_CH_BCHARGER.values()));

    // Initialise Fan Test channels
    chMap.put(ChannelType.PSM_CH_FAN,
        createChannels(ChannelType.PSM_CH_FAN, PSM_CH_FAN.values()));

    chMap.put(ChannelType.PSM_CH_PSUPPLY, createChannels(ChannelType.PSM_CH_PSUPPLY, PSM_CH_PSUPPLY.values()));

    return chMap;
  }

  public ICANChannel[] getBatteryChargerChannels() {
    return getChannels(ChannelType.PSM_CH_BCHARGER);
  }

  public ICANChannel getFanTestChannel() {
    return getChByEnum(PSM_CH_FAN.PSM_CH_FAN_FAN_TEST);
  }
 
  @Override
  public ModulePSMSettings getSettings() {
    return settings;
  }
}