/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.rtu.config.module.canmodule.CANModulePlugin;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM;
import com.lucy.g3.rtu.config.module.canmodule.ui.dialogs.ModuleEditDialog;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class ModuleEditDialogTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  // For test
  public static void main(String[] args) {

    Module module = new ModuleSCM(MODULE_ID.MODULE_ID_0);
    System.out.println("before :" + module.getId());
    CANModuleManager manager = CANModulePlugin.getInstance().create(null);
    ModuleEditDialog dialog = new ModuleEditDialog(null, manager, null);
    dialog.setVisible(true);
  }

}
