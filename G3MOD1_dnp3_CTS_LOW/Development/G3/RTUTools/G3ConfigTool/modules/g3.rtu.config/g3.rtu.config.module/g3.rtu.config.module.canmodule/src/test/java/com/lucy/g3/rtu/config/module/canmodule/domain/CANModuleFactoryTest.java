/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleFactory;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class CANModuleFactoryTest {

  private MODULE[] supportTypes;
  private MODULE_ID[] supportIDs;


  @Before
  public void setUp() throws Exception {
    supportTypes = ModuleEnums.getSupportedTypes();
    supportIDs = ModuleEnums.getSupportedIDs();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCreateModule() {
    for (MODULE type : supportTypes) {
      for (MODULE_ID id : supportIDs) {

        Module module = CANModuleFactory.createModule(type, id);

        assertNotNull(module);
        assertEquals("Module type: " + type, id, module.getId());
        assertEquals(type, module.getType());

      }
    }
  }

//  @Test
//  public void testCreateUnsupportedModule() {
//    List<MODULE> supportTypesList = Arrays.asList(supportTypes);
//
//    MODULE_ID id = MODULE_ID.MODULE_ID_0;
//
//    for (MODULE type : MODULE.values()) {
//      // Unsupport type
//      if (!supportTypesList.contains(type)) {
//        try {
//          CANModuleFactory.createModule(type, id);
//          fail("IllegalArgumentException is expected to create an unsupported module");
//        } catch (IllegalArgumentException e) {
//        }
//      }
//
//    }
//  }


  final private ChannelType[] allChTypes = ChannelType.values();


  @Test
  public void testCreatedModuleHasValidChannels() {
    MODULE_ID id = MODULE_ID.MODULE_ID_1;

    for (MODULE t : supportTypes) {
      ICANModule module = CANModuleFactory.createModule(t, id);

      List<? extends IChannel> chs = module.getAllChannels();
      Assert.assertNotNull(chs);
      for (IChannel ch : chs) {
        Assert.assertNotNull(ch);
        Assert.assertTrue(ch.getOwnerModule() == module);
      }

      for (int i = 0; i < this.allChTypes.length; i++) {
        IChannel[] chsByType = module.getChannels(this.allChTypes[i]);
        if (chsByType != null) {
          for (int j = 0; j < chsByType.length; j++) {
            Assert.assertNotNull(chsByType[j]);
            Assert.assertEquals(module.getShortName() + " -> " + chsByType[j],
                this.allChTypes[i], chsByType[j].getType());
            Assert.assertEquals(module, chsByType[j].getOwnerModule());
          }
        }
      }
    }
  }
}
