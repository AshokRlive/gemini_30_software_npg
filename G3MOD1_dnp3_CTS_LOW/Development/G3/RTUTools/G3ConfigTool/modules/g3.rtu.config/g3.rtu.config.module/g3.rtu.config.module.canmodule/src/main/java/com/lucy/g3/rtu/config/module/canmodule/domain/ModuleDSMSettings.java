/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.module.canmodule.ui.panels.DSMSettingsPanel;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.MOTOR_MODE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;


/**
 *
 */
public class ModuleDSMSettings extends AbstractSwitchModuleSettings {
  public static final String PROPERTY_OPEN_COLOUR = "openColour";

  public static final String PROPERTY_POLARITY_A = "polarityA";
  public static final String PROPERTY_POLARITY_B = "polarityB";

  public static final String PROPERTY_SUPPLY_ENABLE_24V = "supplyEnable24V";

  public static final String PROPERTY_ALLOWFORCEDOPERATIONA = "allowForcedOperationA";

  public static final String PROPERTY_ALLOWFORCEDOPERATIONB = "allowForcedOperationB";

  private Logger log = Logger.getLogger(ModuleDSMSettings.class);
  
  private boolean allowForcedOperationA;
  private boolean allowForcedOperationB;

  private LED_COLOUR openColour = LED_COLOUR.LED_COLOUR_GREEN;

  private Polarity polarityA = Polarity.SWITCH_POLARITY_DOWN;
  private Polarity polarityB = Polarity.SWITCH_POLARITY_DOWN;

  private boolean supplyEnable24V;

  
  public LED_COLOUR getOpenColour() {
    return openColour;
  }

  public void setOpenColour(LED_COLOUR openColour) {
    if (openColour == null) {
      Logger.getLogger(getClass()).error("Cannot set LED colour to null");
      return;
    }

    Object oldValue = getOpenColour();
    this.openColour = openColour;
    firePropertyChange(PROPERTY_OPEN_COLOUR, oldValue, openColour);
  }

  public Polarity getPolarityA() {
    return polarityA;
  }

  public void setPolarityA(Polarity polarityA) {
    if (polarityA == null) {
      log.error("Cannot set polarityA to null");
      return;
    }

    Object oldValue = getPolarityA();
    this.polarityA = polarityA;
    firePropertyChange(PROPERTY_POLARITY_A, oldValue, polarityA);
  }

  public Polarity getPolarityB() {
    return polarityB;
  }

  public void setPolarityB(Polarity polarityB) {
    if (polarityB == null) {
      log.error("Cannot set polarityB to null");
      return;
    }

    Object oldValue = getPolarityB();
    this.polarityB = polarityB;
    firePropertyChange(PROPERTY_POLARITY_B, oldValue, polarityB);
  }

  public boolean isSupplyEnable24V() {
    return supplyEnable24V;
  }

  public void setSupplyEnable24V(boolean supplyEnable24V) {
    Object oldValue = this.supplyEnable24V;
    this.supplyEnable24V = supplyEnable24V;
    firePropertyChange(PROPERTY_SUPPLY_ENABLE_24V, oldValue, supplyEnable24V);
  }

  public boolean isAllowForcedOperationA() {
    return allowForcedOperationA;
  }

  public void setAllowForcedOperationA(boolean allowForcedOperationA) {
    Object oldValue = this.allowForcedOperationA;
    this.allowForcedOperationA = allowForcedOperationA;
    firePropertyChange(PROPERTY_ALLOWFORCEDOPERATIONA, oldValue,
        allowForcedOperationA);
  }

  public boolean isAllowForcedOperationB() {
    return allowForcedOperationB;
  }

  public void setAllowForcedOperationB(boolean allowForcedOperationB) {
    Object oldValue = this.allowForcedOperationB;
    this.allowForcedOperationB = allowForcedOperationB;
    firePropertyChange(PROPERTY_ALLOWFORCEDOPERATIONB, oldValue,
        allowForcedOperationB);
  }


  @Override
  public boolean setOpenPolarity(SwitchIndex index, Polarity polarity) {
    switch (index) {
    case SwitchA:
      setPolarityA(polarity);
      break;
    case SwitchB:
      setPolarityB(polarity);
      break;
    default:
      return false;
    }
    return true;
  }

  @Override
  public boolean setClosePolarity(SwitchIndex index, Polarity polarity) {
    return setOpenPolarity(index, polarity);
  }

  private DSMSettingsPanel editor;
  @Override
  public IModuleSettingsEditor getEditor() {
    if(editor == null)
      editor = new DSMSettingsPanel(this);
    return editor;
  }

  @Override
  public boolean setAllowForcedOperation(SwitchIndex index, boolean allowForceOperation) {
    switch (index) {
    case SwitchA:
      setAllowForcedOperationA(allowForceOperation);
      break;
      
    case SwitchB:
      setAllowForcedOperationB(allowForceOperation);
      break;
    default:
      return false;
    }
    
    return true;
  }

  @Override
  public boolean setMotorMode(SwitchIndex index, MOTOR_MODE mode) {
    return false; // Unsupported
  }

    
}

