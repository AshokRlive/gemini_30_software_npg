/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.domain.ModuleIcon;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

public class ModuleIconTest {

  @Test
  public void testIconNotNull() {
    MODULE[] types = ModuleEnums.getSupportedTypes();

    for (MODULE t : types) {
      ModuleIcon icons = ModuleIcon.getInstance(t);
      assertNotNull("large icon is null for:" + t, icons.getLargeIcon());
      assertNotNull("normal icon is null for:" + t, icons.getNormalIcon());
      assertNotNull("small icon is null for:" + t, icons.getThumbnail());
    }
  }

}
