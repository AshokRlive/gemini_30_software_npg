/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import com.lucy.g3.rtu.config.module.canmodule.ui.panels.HMISettingsPanel;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;

/**
 *
 */
public class ModuleHMISettings extends AbstractModuleSettings {

  public static String PROPERTY_BUTTONCLICK = "buttonClick";
  public static String PROPERTY_REQUESTOLR = "requestOLR";
  public static String PROPERTY_OPENCLOSE_PRESSTIME = "openclosePressTime";

  private boolean buttonClick = false;
  private boolean requestOLR = true;
  private int openclosePressTime = 3000;

  private Object screenManager;
  
  private final ICANModule hmi;
  
  public ModuleHMISettings(ICANModule hmi) {
    super();
    this.hmi = hmi;
  }

  public boolean isButtonClick() {
    return buttonClick;
  }

  public void setButtonClick(boolean buttonClick) {
    Object oldValue = isButtonClick();
    this.buttonClick = buttonClick;
    firePropertyChange(PROPERTY_BUTTONCLICK, oldValue, buttonClick);
  }

  public boolean isRequestOLR() {
    return requestOLR;
  }

  public void setRequestOLR(boolean requestOLR) {
    Object oldValue = isRequestOLR();
    this.requestOLR = requestOLR;
    firePropertyChange(PROPERTY_REQUESTOLR, oldValue, requestOLR);
  }

  public int getOpenclosePressTime() {
    return openclosePressTime;
  }

  public void setOpenclosePressTime(int openclosePressTime) {
    Object oldValue = getOpenclosePressTime();
    this.openclosePressTime = openclosePressTime;
    firePropertyChange(PROPERTY_OPENCLOSE_PRESSTIME, oldValue, openclosePressTime);
  }

  public Object getScreenManager() {
    return screenManager;
  }
  
  public void setScreenManager(Object screenManager) {
    this.screenManager = screenManager;
  }

  private HMISettingsPanel editor;
  @Override
  public IModuleSettingsEditor getEditor() {
    if(editor == null){
      IConfig config = hmi.getManager().getOwner();
      IConfigModule hmiMgr = config.getConfigModule(IConfigModule.HMI_SCREEN_MANAGER_ID);
      editor = new HMISettingsPanel(this, hmiMgr);
    }
    
    return editor;
  }
  
}
