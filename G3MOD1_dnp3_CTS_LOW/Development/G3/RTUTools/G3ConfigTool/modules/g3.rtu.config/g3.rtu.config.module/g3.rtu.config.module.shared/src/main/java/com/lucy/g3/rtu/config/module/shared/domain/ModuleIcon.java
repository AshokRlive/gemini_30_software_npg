/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import java.util.HashMap;

import javax.swing.ImageIcon;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * This class contains all icon resource of a module.
 */
public final class ModuleIcon {

  private final MODULE type;


  private ModuleIcon(MODULE type) {
    this.type = type;
  }

  /**
   * Gets icon resource of this module.
   *
   * @return the normal sized icon of this module.
   */
  public ImageIcon getNormalIcon() {
    return ModuleResource.INSTANCE.getModuleIcon(type);
  }

  /**
   * Gets icon resource of this module.
   *
   * @return the small sized icon of this module.
   */
  public ImageIcon getThumbnail() {
    return ModuleResource.INSTANCE.getModuleIconThumbnail(type);
  }

  /**
   * Gets icon resource of this module.
   *
   * @return the large sized icon of this module.
   */
  public ImageIcon getLargeIcon() {
    return ModuleResource.INSTANCE.getLargeModuleIcon(type);
  }


  private static HashMap<MODULE, ModuleIcon> instanceMap = new HashMap<MODULE, ModuleIcon>();


  public static ModuleIcon getInstance(MODULE type) {
    Preconditions.checkNotNull(type, "type must not be null");

    ModuleIcon instance = instanceMap.get(type);

    if (instance == null) {
      instance = new ModuleIcon(type);
      instanceMap.put(type, instance);
    }
    return instance;
  }

}
