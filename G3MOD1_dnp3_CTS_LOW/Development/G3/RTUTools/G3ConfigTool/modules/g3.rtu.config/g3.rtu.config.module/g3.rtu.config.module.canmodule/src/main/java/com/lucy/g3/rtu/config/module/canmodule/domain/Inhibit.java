/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;

/**
 * This class represents the inhibit configuration of a channel. It stores &
 * maintains inhibit as a bit mask, e.g 110011, which can be reconfigured by
 * either {@code SetBitValue()} or {@code SetInhibitMask()}.
 */
public class Inhibit implements Cloneable {

  private Logger log = Logger.getLogger(Inhibit.class);

  public static final int MAX_BIT_NUMBER = Long.bitCount(Long.MAX_VALUE);

  private final ICANChannel[] channels;

  private final int bitNum; // The bits number of inhibit mask.

  private long inhibitMask;

  private String inhibitMaskText = ""; // Current inhibit mask in the form of


  // string.

  /**
   * Constructs a Inhibit instance for the given channels.
   * <p>
   * <b>Note:</b> the size of inhibit channels must not be over
   * {@code MAX_BIT_NUMBER}.
   * </p>
   * @param inhibitChannels
   *          the channels to be used for inhibit. Must not be <code>null</code>
   *          .
   * @throws NullPointerException
   *           if the channels array is null.
   * @throws IllegalArgumentException
   *           if the channels array size is over {@value MAX_BIT_NUMBER}
   */
  public Inhibit(ICANChannel[] inhibitChannels) {
    Preconditions.checkNotNull(inhibitChannels, "Channels for inhibit is null");
    Preconditions.checkArgument(inhibitChannels.length <= MAX_BIT_NUMBER,
        String.format("The size of inhibit channels is too big.%d > %d(max)",
            inhibitChannels.length, MAX_BIT_NUMBER));

    this.channels = Arrays.copyOf(inhibitChannels, inhibitChannels.length);
    this.bitNum = inhibitChannels.length;
    if (bitNum > 0) {
      setInhibitMask(0L);
    }
  }

  public long getInhibitMask() {
    return inhibitMask;
  }

  /**
   * Modifies inhibit mask with new bitmask value.
   *
   * @param bitmask
   *          new bitmask value.
   */
  public void setInhibitMask(long bitmask) {
    /* Validate new bitmask */
    String binary = Long.toBinaryString(bitmask);
    if (binary.length() > bitNum) {
      log.error(String.format("Cannot set bit mask to: %s. The maximum bit number is %d",
          binary, bitNum));
      return;
    }

    this.inhibitMask = bitmask;

    /* Convert new bitmask to formatted text */
    String s = Long.toBinaryString(this.inhibitMask);
    inhibitMaskText = String.format("%" + getBitNum() + "s", s).replace(' ', '0');
  }

  /**
   * Try to get a channel at the specific position.
   *
   * @param position
   *          the channel index
   * @return the channel at the given position. Null the channel doesn't exist.
   */
  public ICANChannel getChannel(int position) {
    if (position >= channels.length || position < 0) {
      log.error(String.format("Index out of bounds: %d]", channels.length));
      return null;
    }

    return channels[position];
  }

  /**
   * Get the number of bit mask, which should equals to the size the inhibit
   * channels.
   *
   * @return
   */
  public int getBitNum() {
    return bitNum;
  }

  public void setBitValue(int bitIndex, boolean value) {
    long bitMask = 1L << bitIndex;

    if (value) {
      setInhibitMask(inhibitMask | bitMask); // set bit to 1
    } else {
      setInhibitMask(inhibitMask & (~bitMask)); // set bit to 0
    }
  }

  public boolean getBitValue(int bitIndex) {
    return ((inhibitMask >> bitIndex) & 1) == 1;
  }

  /**
   * Convert this inhibit to a fixed length(same as bitmask length) and
   * formatted inhibit bitmask text. e.g. "011101".
   */
  public String getBitmaskAsText() {
    return inhibitMaskText;
  }

  @Override
  public String toString() {
    return getBitmaskAsText();
  }

  @Override
  public Inhibit clone() {
    Inhibit cloneObj = new Inhibit(Arrays.copyOf(channels, channels.length));
    cloneObj.setInhibitMask(inhibitMask);
    return cloneObj;
  }

}
