/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain.stub;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.xml.gen.api.IChannelEnum;

public class CANChannelStub {

  private static ICANModule owner = new CANModuleStub();


  private CANChannelStub() {
  };

  public static CANChannel create() {
    return create(ChannelType.ANALOG_INPUT);
  }

  public static CANChannel create(ChannelType type) {
    return new CANChannel(type, owner, ChannelEnumStub.CHANNEL_A1);
  }

  public static CANChannel create(ChannelType channelType, ICANModule ownerModule,
      Enum<? extends IChannelEnum> channelEnum) {
    return new CANChannel(channelType, ownerModule, channelEnum);
  }

}
