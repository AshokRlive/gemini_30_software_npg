/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.test.support.utilities.TestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class AbstractModuleTest {

  final private static String[] readOnlyProperties =
      new String[] {
          AbstractModule.PROPERTY_MODULE_ID,
          AbstractModule.PROPERTY_FULLNAME,
          AbstractModule.PROPERTY_SHORTNAME,
          AbstractModule.PROPERTY_NAME,
          AbstractModule.PROPERTY_DESCRIPTION
      };

  private AbstractModule fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new ModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_FPM);
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

  @Test
  public void testConstruct() {
    AbstractModule m = new ModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_FDM);

    assertEquals(MODULE_ID.MODULE_ID_0, m.getId());
    assertEquals(MODULE.MODULE_FDM, m.getType());
  }

  @Test(expected = NullPointerException.class)
  public void testConstructWithNullArg1() {
    new ModuleStub(null, null);
  }

  @Test(expected = NullPointerException.class)
  public void testConstructWithNullArg2() {
    new ModuleStub(MODULE_ID.MODULE_ID_0, null);
  }

  @Test(expected = NullPointerException.class)
  public void testConstructWithNullArg3() {
    new ModuleStub(null, MODULE.MODULE_FDM);
  }

  @Test
  public void testReadOnlyProperties() {
    TestUtil.testReadOnlyProperties(fixture, readOnlyProperties);
  }

  @Test
  public void testModuleNotEqual() {
    final AbstractModule m1, m2, m3;
    m1 = new ModuleStub(MODULE_ID.MODULE_ID_2, MODULE.MODULE_HMI);
    m2 = new ModuleStub(MODULE_ID.MODULE_ID_2, MODULE.MODULE_FDM);
    m3 = new ModuleStub(MODULE_ID.MODULE_ID_3, MODULE.MODULE_FDM);

    assertNotSame(m1, m2);
    assertNotSame(m1, m3);
    assertNotSame(m2, m3);
    assertNotSame(m1, null);

    assertFalse(m1.equals(null));
    assertFalse(m1.equals(m2));
    assertFalse(m2.equals(m3));
  }

  @Test
  public void testDelete() {
    PropertyChangeListener pcl = new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
      }
    };
    // Register 1 observer
    fixture.addPropertyChangeListener(Module.PROPERTY_MODULE_ID, pcl);

    assertTrue(fixture.getPropertyChangeListeners().length > 0);
    fixture.delete();

    // Check observer has been removed automatically
    assertEquals(0, fixture.getPropertyChangeListeners().length);
  }

  @Test
  public void testModuleResource() {
    MODULE[] types = ModuleEnums.getSupportedTypes();

    for (int i = 0; i < types.length; i++) {
      AbstractModule m = new ModuleStub(MODULE_ID.MODULE_ID_0, types[i]);
      assertNotNull(types[i] + " name is null", m.getName());
      assertNotNull(types[i] + " description is null", m.getDescription());
      assertNotNull(types[i] + " full name is null", m.getFullName());
      assertNotNull(types[i] + " short name is null", m.getShortName());
      assertNotNull(types[i] + " icon is null", m.getIcons());
      assertNotNull(types[i] + " type is null", m.getType());
    }
  }

  @Test
  public void testSetID() {
    MODULE_ID id = MODULE_ID.MODULE_ID_1;
    fixture.modifyId(id);
    assertEquals(id, fixture.getId());

    id = MODULE_ID.MODULE_ID_3;
    fixture.modifyId(id);
    assertEquals(id, fixture.getId());

    fixture.modifyId(null);
    assertEquals(id, fixture.getId());
  }

  @Test
  public void testNameUpdate() {
    fixture.modifyId(MODULE_ID.MODULE_ID_0);
    String nameA = fixture.getName();

    fixture.modifyId(MODULE_ID.MODULE_ID_1);
    String nameB = fixture.getName();

    assertNotSame(nameA, nameB);
  }

  @Test
  public void testPropertyChange_EventModuleID() {
    PresentationModel<AbstractModule> model = new PresentationModel<AbstractModule>(fixture);
    assertFalse(model.isChanged());

    fixture.modifyId(MODULE_ID.MODULE_ID_6);
    assertTrue("Model not changed", model.isChanged());
    model.resetChanged();

    // NULL value ignored
    assertFalse(model.isChanged());
    fixture.modifyId(null);
    assertFalse("Model is not supposed to change when set NULL value", model.isChanged());

    model.release();
  }

  // @Test
  // public void testHasbeenControlledByCLogic() {
  // Module scm = new ModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_SCM);
  // assertFalse(scm.isMappedTo(MappingResourceType.CLOGIC));
  //
  // MappingResource clogic = new
  // AbstractMappingResource(MappingResourceType.CLOGIC) {
  //
  //
  // };
  //
  // Mapping<?,?> mapping = new
  // AbstractMapping<MappingResource,MappingResource>(MappingType.GENERAL, scm,
  // clogic) {
  // };
  //
  // assertTrue(scm.isMappedTo(MappingResourceType.CLOGIC));
  // mapping.destroy();
  //
  //
  // assertFalse(scm.isMappedTo(MappingResourceType.CLOGIC));
  // }

}
