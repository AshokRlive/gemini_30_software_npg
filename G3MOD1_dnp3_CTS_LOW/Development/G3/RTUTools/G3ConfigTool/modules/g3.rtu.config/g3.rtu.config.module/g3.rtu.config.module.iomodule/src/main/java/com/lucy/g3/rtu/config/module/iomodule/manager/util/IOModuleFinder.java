/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.manager.util;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;

/**
 *
 */
public class IOModuleFinder {

  private IOModuleFinder() {
  }

  public static Collection<IOModule> findIOModules(Collection<? extends Module> allModules) {
    ArrayList<IOModule> found = new ArrayList<>();

    if (allModules != null) {
      for (Module m : allModules) {
        if (m != null && m instanceof IOModule) {
          found.add((IOModule) m);
        }
      }
    }
    return found;
  }

}
