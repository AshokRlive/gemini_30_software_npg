/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui;

import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManagerImpl;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.ui.dialogs.ModuleEditDialog;
import com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule.ModuleAddingWizard;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.manager.MaxModuleNumException;

/**
 * Presentation Model for CAN module manager.
 */
public class CANModuleManagerModel extends Bean {

  public static final String ACTION_KEY_ADD = "addModule";
  public static final String ACTION_KEY_MODIFY_ID = "modifyID";
  public static final String ACTION_KEY_REMOVE = "removeSelection";
  public static final String ACTION_KEY_MOVE_UP = "moveSelectionUp";
  public static final String ACTION_KEY_MOVE_DOWN = "moveSelectionDown";

  /** The name of property {@value} . */
  public static final String PROPERTY_BACKPLANE = "backplane";

  private static final String PROPERTY_MODIFICABLE = "idModificable";
  private static final String PROPERTY_REMOVABLE = "removable";
  private static final String PROPERTY_MOVABLE = "movable";

  private Logger log = Logger.getLogger(CANModuleManagerModel.class);

  private final CANModuleManager manager;

  private final SelectionInList<ICANModule> moduleSelectionList;

  private final JFrame parent = WindowUtils.getMainFrame();

  private final ApplicationActionMap actionMap = Application.getInstance().getContext().getActionMap(this);


  /**
   * Construct a modules presentation model.
   *
   * @param manager
   */
  public CANModuleManagerModel(CANModuleManager manager) {
    this.manager = Preconditions.checkNotNull(manager, "Modules manager must not be null");
    this.moduleSelectionList = new SelectionInList<ICANModule>(manager.getItemListModel());

    this.moduleSelectionList.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        new ModuleSelectionHandler());
  }

  /**
   * Bean getter method for property {@linkplain PROPERTY_MOVABLE}.
   *
   * @return <code>true</code> if the selection can be moved, <code>false</code>
   *         otherwise.
   */
  public boolean isMovable() {
    return this.moduleSelectionList.getSelection() != null;
  }

  /**
   * Bean getter method for property {@linkplain PROPERTY_ID_REMOVABLE}.
   */
  public boolean isRemovable() {
    ICANModule selection = this.moduleSelectionList.getSelection();
    return selection != null && CANModuleManagerImpl.isDefaultModule(selection) == false;
  }

  /**
   * Bean getter method. Check if the selected module can be edit.
   */
  public boolean isIdModificable() {
    ICANModule sel = this.moduleSelectionList.getSelection();
    return sel != null && sel instanceof ISupportChangeID;
  }

  /**
   * Bean setter for property {@linkplain #PROPERTY_BACKPLANE}.
   *
   * @param newBackplaneType
   */
  public void setBackplane(BackPlaneType newBackplaneType) {
    try {
      this.manager.setBackplane(newBackplaneType);
    } catch (MaxModuleNumException e) {
      this.log.error("Fail to change backplane type: " + e.getMessage());
      JOptionPane.showMessageDialog(this.parent, e.getMessage(), e.getClass().getSimpleName(),
          JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Bean getter for property {@linkplain #PROPERTY_BACKPLANE}.
   *
   * @return
   */
  public BackPlaneType getBackplane() {
    return this.manager.getBackplaneType();
  }

  public SelectionInList<ICANModule> getModuleSelectionList() {
    return this.moduleSelectionList;
  }

  public void setSelectedModule(ICANModule module) {
    this.moduleSelectionList.setSelection(module);
  }

  public javax.swing.Action getAction(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");

    javax.swing.Action action = this.actionMap.get(actionKey);

    if (action != null) {
      if (actionKey.equals(ACTION_KEY_MODIFY_ID) || actionKey.equals(ACTION_KEY_ADD)) {
        action = new BusyCursorAction(action, this.parent);
      }
    } else {
      this.log.error("Action is not available for the key:" + actionKey);
    }
    return action;
  }

  @Action
  public void addModule() {
    new ModuleAddingWizard(manager, 
        ConfigGeneratorRegistry.getInstance().getGenerator(manager.getOwner(), IIOModuleGenerator.class));
  }

  @Action(enabledProperty = PROPERTY_REMOVABLE)
  public void removeSelection() {
    if (isRemovable() == false) {
      return;
    }

    ICANModule module = this.moduleSelectionList.getSelection();

    if (showModuleRemoveConfirmDialog(this.parent, module) == false) {
      return;
    }

    if (this.manager.remove(module) == false) {
      this.log.warn("Fail to delete module:" + module);
    }
  }

  @Action(enabledProperty = PROPERTY_MODIFICABLE)
  public void modifyID() {
    if (isIdModificable() == false) {
      return;
    }

    ICANModule sel = this.moduleSelectionList.getSelection();
    new ModuleEditDialog(this.parent, this.manager, sel).setVisible(true);
    this.parent.repaint();
  }

  @Action(enabledProperty = PROPERTY_MOVABLE)
  public void moveSelectionUp() {
    if (isMovable() == false) {
      return;
    }
    int selIndex = this.moduleSelectionList.getSelectionIndex();
    ICANModule target = this.moduleSelectionList.getSelection();
    if (selIndex > 0) {
      this.moduleSelectionList.getList().remove(target);
      this.moduleSelectionList.getList().add(selIndex - 1, target);

      this.moduleSelectionList.setSelectionIndex(selIndex - 1);
      this.log.debug("Swap module " + selIndex + " with " + (selIndex - 1) + " complemted.");
    }
  }

  @Action(enabledProperty = PROPERTY_MOVABLE)
  public void moveSelectionDown() {
    if (isMovable() == false) {
      return;
    }

    int selIndex = this.moduleSelectionList.getSelectionIndex();
    ICANModule target = this.moduleSelectionList.getElementAt(selIndex + 1);
    if (selIndex >= 0 && selIndex < this.moduleSelectionList.getSize() - 1) {
      moduleSelectionList.getList().remove(target);
      moduleSelectionList.getList().add(selIndex, target);
      moduleSelectionList.setSelectionIndex(selIndex + 1);
      log.debug("Swap module " + selIndex + " with " + (selIndex + 1) + " complemted.");
    }
  }

  /**
   * Show confirm dialog if this point is being used by other configuration
   * items.
   */
  private static boolean showModuleRemoveConfirmDialog(Window parent, Module module) {
    if (module != null) {
      String msg;
      if (!module.getAllConnections().isEmpty()) {
        msg = "This module is being used.\nDo you really want to remove it?";
      } else {
        msg = "Are you sure you want to remove module: \"" + module.getShortName() + "\"?";
      }

      int option =
          JOptionPane.showConfirmDialog(parent, msg, "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
      if (option == JOptionPane.NO_OPTION) {
        return false;
      }
    }

    return true;
  }


  private class ModuleSelectionHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      CANModuleManagerModel.this.firePropertyChange(PROPERTY_MOVABLE, null, isMovable());
      CANModuleManagerModel.this.firePropertyChange(PROPERTY_REMOVABLE, null, isRemovable());
      CANModuleManagerModel.this.firePropertyChange(PROPERTY_MODIFICABLE, null, isIdModificable());
    }
  }
}
