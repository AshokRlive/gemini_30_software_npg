/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.panels;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleHMISettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;

/**
 * A panel for configuring the attributes of HMI.
 */
public class HMISettingsPanel extends JPanel implements IModuleSettingsEditor{

  private PresentationModel<ModuleHMISettings> pm = null;

  public HMISettingsPanel(ModuleHMISettings settings, Object hmiScreenManager) {
    Preconditions.checkNotNull(settings, "settings must not be null");
    Preconditions.checkNotNull(hmiScreenManager, "hmiScreenManager must not be null");
    
    pm = new PresentationModel<ModuleHMISettings>(settings);
    initComponents();
    initComponentsBinding();
    
    Page hmiPage = PageFactories.getFactory(PageFactories.KEY_HMI).createPage(hmiScreenManager); 
    panelHMIScreen.add(hmiPage.getContent(), BorderLayout.CENTER);
  }
  
  @Override
  public JComponent getEditorComopnent() {
    return this;
  }

  @Override
  public String getEditorTitle() {
    return "HMI Settings";
  }

  private void initComponentsBinding() {
    Bindings.bind(checkBoxButtonClick, pm.getModel(ModuleHMISettings.PROPERTY_BUTTONCLICK));
    Bindings.bind(checkBoxRequestOLR, pm.getModel(ModuleHMISettings.PROPERTY_REQUESTOLR));

    Bindings.bind(ftfOpenClsoePressTime, pm.getModel(ModuleHMISettings.PROPERTY_OPENCLOSE_PRESSTIME));
  }

  private void thisMouseClicked(MouseEvent e) {
    requestFocus();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    scrollPane1 = new JScrollPane();
    panel2 = new JPanel();
    panelHMIScreen = new JPanel();
    panel1 = new JPanel();
    JLabel label1 = new JLabel();
    ftfOpenClsoePressTime = new JFormattedTextField();
    JLabel label2 = new JLabel();
    checkBoxButtonClick = new JCheckBox();
    checkBoxRequestOLR = new JCheckBox();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {
      scrollPane1.setBorder(null);

      //======== panel2 ========
      {
        panel2.setBorder(Borders.DIALOG_BORDER);
        panel2.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            thisMouseClicked(e);
          }
        });
        panel2.setLayout(new GridBagLayout());
        ((GridBagLayout) panel2.getLayout()).columnWidths = new int[] { 160, 0, 0 };
        ((GridBagLayout) panel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) panel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) panel2.getLayout()).rowWeights = new double[] { 1.0, 1.0, 1.0E-4 };

        //======== panelHMIScreen ========
        {
          panelHMIScreen.setBorder(new TitledBorder("HMI Menu Configuration"));
          panelHMIScreen.setLayout(new BorderLayout());
        }
        panel2.add(panelHMIScreen, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 10), 0, 0));

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("HMI Buttons Configuration"));
          panel1.setLayout(new FormLayout(
              "right:default, $lcgap, [50dlu,default], $lcgap, default:grow",
              "2*(default, $lgap), default"));

          //---- label1 ----
          label1.setText("Open/Close Press Time:");
          panel1.add(label1, CC.xy(1, 1));
          panel1.add(ftfOpenClsoePressTime, CC.xy(3, 1));

          //---- label2 ----
          label2.setText("ms");
          panel1.add(label2, CC.xy(5, 1));

          //---- checkBoxButtonClick ----
          checkBoxButtonClick.setText("Button Click");
          panel1.add(checkBoxButtonClick, CC.xy(3, 3));

          //---- checkBoxRequestOLR ----
          checkBoxRequestOLR.setText("Enable Off/Local/Remote Button");
          panel1.add(checkBoxRequestOLR, CC.xywh(3, 5, 3, 1));
        }
        panel2.add(panel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));
      }
      scrollPane1.setViewportView(panel2);
    }
    add(scrollPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    UIUtils.increaseScrollSpeed(scrollPane1);
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JPanel panel2;
  private JPanel panelHMIScreen;
  private JPanel panel1;
  private JFormattedTextField ftfOpenClsoePressTime;
  private JCheckBox checkBoxButtonClick;
  private JCheckBox checkBoxRequestOLR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
