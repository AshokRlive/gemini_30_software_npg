/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.iomodule.domain.AbstractIOModule;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModule;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class AbstractIOModuleTest {

  private AbstractIOModule fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new TestModule(MODULE_ID.MODULE_ID_0, MODULE.MODULE_FPM);
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }


  @Test
  public void testGetChannel() {
    ChannelType[] types = ChannelType.values();

    // No channel can be found for each type
    for (int i = 0; i < types.length; i++) {
      IChannel[] chs = fixture.getChannels(types[i]);
      assertNotNull(chs);
      assertEquals(0, chs.length);
    }

    List<? extends IChannel> allChs = fixture.getAllChannels();
    assertNotNull(allChs);
    assertEquals(0, allChs.size());

    // No available channel types
    ChannelType[] chTypes = fixture.getAvailableChannelTypes();
    assertNotNull(chTypes);
    assertEquals(0, chTypes.length);
  }

  @Test
  public void testDelete() {
    // TODO test channel deleted
  }

  @Test
  public void testModuleResource() {
    MODULE[] types = ModuleEnums.getSupportedTypes();

    for (int i = 0; i < types.length; i++) {
      AbstractModule m = new ModuleStub(MODULE_ID.MODULE_ID_0, types[i]);
      assertNotNull(types[i] + " name is null", m.getName());
      assertNotNull(types[i] + " description is null", m.getDescription());
      assertNotNull(types[i] + " full name is null", m.getFullName());
      assertNotNull(types[i] + " short name is null", m.getShortName());
      assertNotNull(types[i] + " icon is null", m.getIcons());
      assertNotNull(types[i] + " type is null", m.getType());
    }
  }

  private static class TestModule extends AbstractIOModule {

    protected TestModule(MODULE_ID id, MODULE type) {
      super(id, type);
    }

    @Override
    public ChannelType[] getAvailableChannelTypes() {
      return new ChannelType[0];
    }

    @Override
    public List<? extends IChannel> getAllChannels() {
      return new ArrayList<IChannel>(0);
    }

    @Override
    public IChannel[] getChannels(ChannelType type) {
      return new IChannel[0];
    }

    @Override
    public IChannel getChByEnum(IChannelEnum channelEnum) {
      return null;
    }

    @Override
    public IModuleSettings getSettings() {
      return null;
    }
    
  }

}
