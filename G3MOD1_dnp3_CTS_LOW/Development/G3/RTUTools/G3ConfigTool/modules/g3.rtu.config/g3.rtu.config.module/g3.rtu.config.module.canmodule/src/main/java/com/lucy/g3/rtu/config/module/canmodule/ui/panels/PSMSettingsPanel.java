/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.panels;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.*;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSMSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;

/**
 * Fan setting panel.
 *
 * @author wang_p
 */
public class PSMSettingsPanel extends JPanel implements IModuleSettingsEditor {

  private final ModulePSMSettings settings;


  public PSMSettingsPanel(ModulePSMSettings settings) {
    this.settings = settings;
    initComponents();
    binding();
  }

  @Override
  public JComponent getEditorComopnent() {
    return this;
  }

  @Override
  public String getEditorTitle() {
    return "PSM Settings";
  }

  
  private void binding() {
    PresentationModel<ModulePSMSettings> pm = new PresentationModel<>(settings);
    bindCyclicBatteryTest(pm);
    bindFanSetting(pm);
  }

  private void bindCyclicBatteryTest(PresentationModel<ModulePSMSettings> pm) {
    PropertyConnector.connectAndUpdate(pm.getModel(ModulePSMSettings.PROPERTY_BATTERYTESTENABLED), panelCyclicBatTest,
        "visible");
    Bindings.bind(ftfDuration, pm.getModel(ModulePSMSettings.PROPERTY_BATTERYTESTDURATION));
    Bindings.bind(ftfPeriod, pm.getModel(ModulePSMSettings.PROPERTY_BATTERYTESTPERIOD_DAYS));
    Bindings.bind(checkBoxBatTest, pm.getModel(ModulePSMSettings.PROPERTY_BATTERYTESTENABLED));

    FormattedTextFieldSupport.setBoundary(ftfDuration, 0, Long.MAX_VALUE);
    FormattedTextFieldSupport.setBoundary(ftfPeriod, 0, ModulePSMSettings.BATTERY_TEST_PERIOD_DAYS_MAX);
    FormattedTextFieldSupport.installCommitOnType(ftfDuration, true);
    FormattedTextFieldSupport.installCommitOnType(ftfPeriod, true);
  }

  private void bindFanSetting(PresentationModel<ModulePSMSettings> pm) {
    PropertyConnector.connectAndUpdate(pm.getModel(ModulePSMSettings.PROPERTY_FAN_FITTED), panelFanSetting, "visible");

    Bindings.bind(checkBoxFanFitted, pm.getModel(ModulePSMSettings.PROPERTY_FAN_FITTED));
    Bindings.bind(checkBoxFanSpFitted, pm.getModel(ModulePSMSettings.PROPERTY_FAN_SPEED_SENSOR_FITTED));

    SpinnerNumberModel smodel = SpinnerAdapterFactory.createNumberAdapter(
        pm.getModel(ModulePSMSettings.PROPERTY_FAN_TEMP_HYSTERESIS),
        ModulePSMSettings.TEMP_HYST_DEF,// defaultValue,
        ModulePSMSettings.TEMP_HYST_MIN,// minValue,
        ModulePSMSettings.TEMP_HYST_MAX,// maxValue
        1// stepSize
    );
    spinnerTempHys.setModel(smodel);

    smodel = SpinnerAdapterFactory.createNumberAdapter(
        pm.getModel(ModulePSMSettings.PROPERTY_FAN_TEMP_THRESHOLD),
        0,// defaultValue,
        -100,// minValue,
        100,
        1// stepSize
    );
    spinnerThreshold.setModel(smodel);

    smodel = SpinnerAdapterFactory.createNumberAdapter(
        pm.getModel(ModulePSMSettings.PROPERTY_FAULT_HYSTERESIS),
        ModulePSMSettings.FAULT_HYST_DEF,// defaultValue,
        ModulePSMSettings.FAULT_HYST_MIN,// minValue,
        ModulePSMSettings.FAULT_HYST_MAX,// maxValue
        500// stepSize
    );
    spinnerHys.setModel(smodel);
    
    Bindings.bind(checkBoxFanLEDDrive, pm.getModel(ModulePSMSettings.PROPERTY_FAN_AS_LED));
    Bindings.bind(checkBoxFanDO, pm.getModel(ModulePSMSettings.PROPERTY_FAN_AS_DO));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
	JPanel panelCyclicBatTestContainer = new JPanel();
	panelCyclicBatTest = new JPanel();
	label4 = new JLabel();
	ftfPeriod = new JFormattedTextField();
	label7 = new JLabel();
	label5 = new JLabel();
	ftfDuration = new JFormattedTextField();
	label8 = new JLabel();
	checkBoxBatTest = new JCheckBox();
	JPanel panelFanSettingContainer = new JPanel();
	checkBoxFanLEDDrive = new JCheckBox();
	checkBoxFanDO = new JCheckBox();
	checkBoxFanFitted = new JCheckBox();
	panelFanSetting = new JPanel();
	checkBoxFanSpFitted = new JCheckBox();
	JLabel label1 = new JLabel();
	spinnerThreshold = new JSpinner();
	JLabel label6 = new JLabel();
	JLabel label2 = new JLabel();
	spinnerTempHys = new JSpinner();
	lblTempHyst = new JLabel();
	JLabel label3 = new JLabel();
	spinnerHys = new JSpinner();
	lblFaultHyst = new JLabel();

	//======== this ========
	setBorder(Borders.DIALOG_BORDER);
	setLayout(new GridBagLayout());
	((GridBagLayout)getLayout()).columnWidths = new int[] {400, 0};
	((GridBagLayout)getLayout()).rowHeights = new int[] {160, 200, 0};
	((GridBagLayout)getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
	((GridBagLayout)getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

	//======== panelCyclicBatTestContainer ========
	{
		panelCyclicBatTestContainer.setBorder(new TitledBorder("Battery Test"));
		panelCyclicBatTestContainer.setLayout(new BorderLayout());

		//======== panelCyclicBatTest ========
		{
			panelCyclicBatTest.setLayout(new FormLayout(
				"right:default, $lcgap, [50dlu,default], $lcgap, default",
				"default, $lgap, default"));

			//---- label4 ----
			label4.setText("Schedule Period:");
			panelCyclicBatTest.add(label4, CC.xy(1, 1));
			panelCyclicBatTest.add(ftfPeriod, CC.xy(3, 1));

			//---- label7 ----
			label7.setText("days");
			panelCyclicBatTest.add(label7, CC.xy(5, 1));

			//---- label5 ----
			label5.setText("Duration:");
			panelCyclicBatTest.add(label5, CC.xy(1, 3));
			panelCyclicBatTest.add(ftfDuration, CC.xy(3, 3));

			//---- label8 ----
			label8.setText("minutes");
			panelCyclicBatTest.add(label8, CC.xy(5, 3));
		}
		panelCyclicBatTestContainer.add(panelCyclicBatTest, BorderLayout.CENTER);

		//---- checkBoxBatTest ----
		checkBoxBatTest.setText("Enable Cyclic Battery Test");
		panelCyclicBatTestContainer.add(checkBoxBatTest, BorderLayout.NORTH);
	}
	add(panelCyclicBatTestContainer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
		GridBagConstraints.CENTER, GridBagConstraints.BOTH,
		new Insets(0, 0, 10, 0), 0, 0));

	//======== panelFanSettingContainer ========
	{
		panelFanSettingContainer.setBorder(new TitledBorder("Fan Settings"));
		panelFanSettingContainer.setLayout(new GridBagLayout());
		((GridBagLayout)panelFanSettingContainer.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
		((GridBagLayout)panelFanSettingContainer.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
		((GridBagLayout)panelFanSettingContainer.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
		((GridBagLayout)panelFanSettingContainer.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};

		//---- checkBoxFanLEDDrive ----
		checkBoxFanLEDDrive.setText("Fan output as external LED drive");
		panelFanSettingContainer.add(checkBoxFanLEDDrive, new GridBagConstraints(0, 0, 5, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		//---- checkBoxFanDO ----
		checkBoxFanDO.setText("Fan output as digital output");
		panelFanSettingContainer.add(checkBoxFanDO, new GridBagConstraints(0, 1, 5, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		//---- checkBoxFanFitted ----
		checkBoxFanFitted.setText("Fan Fitted");
		panelFanSettingContainer.add(checkBoxFanFitted, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		//======== panelFanSetting ========
		{
			panelFanSetting.setLayout(new FormLayout(
				"right:default, $lcgap, [60dlu,default], $lcgap, default:grow",
				"3*(default, $ugap), default"));

			//---- checkBoxFanSpFitted ----
			checkBoxFanSpFitted.setText("Fan Speed Sensor Fitted");
			panelFanSetting.add(checkBoxFanSpFitted, CC.xywh(1, 1, 5, 1));

			//---- label1 ----
			label1.setText("Fan Temperature Threshold:");
			panelFanSetting.add(label1, CC.xy(1, 3));
			panelFanSetting.add(spinnerThreshold, CC.xy(3, 3));

			//---- label6 ----
			label6.setText("\u00b0C");
			panelFanSetting.add(label6, CC.xy(5, 3));

			//---- label2 ----
			label2.setText("Fan Temperature Hysteresis:");
			panelFanSetting.add(label2, CC.xy(1, 5));
			panelFanSetting.add(spinnerTempHys, CC.xy(3, 5));

			//---- lblTempHyst ----
			lblTempHyst.setText("\u00b0C");
			panelFanSetting.add(lblTempHyst, CC.xy(5, 5));

			//---- label3 ----
			label3.setText("Fault Hysteresis");
			panelFanSetting.add(label3, CC.xy(1, 7));
			panelFanSetting.add(spinnerHys, CC.xy(3, 7));

			//---- lblFaultHyst ----
			lblFaultHyst.setText("ms");
			panelFanSetting.add(lblFaultHyst, CC.xy(5, 7));
		}
		panelFanSettingContainer.add(panelFanSetting, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
	}
	add(panelFanSettingContainer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
		GridBagConstraints.CENTER, GridBagConstraints.BOTH,
		new Insets(0, 0, 0, 0), 0, 0));

	//---- buttonGroup1 ----
	ButtonGroup buttonGroup1 = new ButtonGroup();
	buttonGroup1.add(checkBoxFanLEDDrive);
	buttonGroup1.add(checkBoxFanDO);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    lblTempHyst.setText(String.format("°C [%d ~ %d]", ModulePSMSettings.TEMP_HYST_MIN, ModulePSMSettings.TEMP_HYST_MAX));
    lblFaultHyst.setText(String.format("ms [%d ~ %d]", ModulePSMSettings.FAULT_HYST_MIN, ModulePSMSettings.FAULT_HYST_MAX));

    SpinnerWheelSupport.installMouseWheelSupport(spinnerHys);
    SpinnerWheelSupport.installMouseWheelSupport(spinnerTempHys);
    SpinnerWheelSupport.installMouseWheelSupport(spinnerThreshold);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelCyclicBatTest;
  private JLabel label4;
  private JFormattedTextField ftfPeriod;
  private JLabel label7;
  private JLabel label5;
  private JFormattedTextField ftfDuration;
  private JLabel label8;
  private JCheckBox checkBoxBatTest;
  private JCheckBox checkBoxFanLEDDrive;
  private JCheckBox checkBoxFanDO;
  private JCheckBox checkBoxFanFitted;
  private JPanel panelFanSetting;
  private JCheckBox checkBoxFanSpFitted;
  private JSpinner spinnerThreshold;
  private JSpinner spinnerTempHys;
  private JLabel lblTempHyst;
  private JSpinner spinnerHys;
  private JLabel lblFaultHyst;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
