/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_DBHIGH2LOWMS;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_DBLOW2HIGHMS;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_EVENTENABLED;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_EVENT_MS;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_EXTEQUITPINVERT;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_INHIBI;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_OPTIMEOUT_SEC;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_OVERRUN_MS;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_POLARITY;
import static com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel.PARAM_PULSELENGTH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANChannelFactory;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.Inhibit;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.CANModuleStub;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.ChannelEnumStub;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Test factory methods and the parameters of created channel from factory.
 */
public class CANChannelFactoryTest {

  final private static String[] allParamNames = {
      PARAM_DBHIGH2LOWMS,
      PARAM_DBLOW2HIGHMS,
      PARAM_EVENT_MS,
      PARAM_EVENTENABLED,
      PARAM_EXTEQUITPINVERT,
      PARAM_INHIBI,
      PARAM_OPTIMEOUT_SEC,
      PARAM_OVERRUN_MS,
      PARAM_POLARITY,
      PARAM_PULSELENGTH,
  };

  final private static String[] SwitchOutParam = {
      PARAM_PULSELENGTH,
      PARAM_OVERRUN_MS,
      PARAM_OPTIMEOUT_SEC,
      PARAM_INHIBI
  };

  final private static String[] AuxSwitchOutParam = {
      PARAM_POLARITY,
      PARAM_PULSELENGTH,
  };

  final private static String[] DigiInParam = {
      PARAM_DBHIGH2LOWMS,
      PARAM_DBLOW2HIGHMS,
      PARAM_EVENTENABLED,
  };

  final private static String[] DigiInParam_withInvert = {
      PARAM_DBHIGH2LOWMS,
      PARAM_DBLOW2HIGHMS,
      PARAM_EVENTENABLED,
      PARAM_EXTEQUITPINVERT
  };

  final private static String[] AnalogInParam = {
      PARAM_EVENT_MS,
      PARAM_EVENTENABLED
  };

  final private static String[] DigiOutParam = {
      PARAM_PULSELENGTH,
  };

  final private static Object[][] paramTypes = new Object[][] {
      { PARAM_DBHIGH2LOWMS, Long.class },
      { PARAM_DBLOW2HIGHMS, Long.class },
      { PARAM_EVENT_MS, Long.class },
      { PARAM_EVENTENABLED, Boolean.class },
      { PARAM_EXTEQUITPINVERT, Boolean.class },
      { PARAM_INHIBI, Inhibit.class },
      { PARAM_OPTIMEOUT_SEC, Long.class },
      { PARAM_OVERRUN_MS, Long.class },
      { PARAM_POLARITY, Polarity.class },
      { PARAM_PULSELENGTH, Long.class },
  };

  final private Enum<? extends IChannelEnum> dummyEnum = ChannelEnumStub.CHANNEL_A1;

  final private ChannelType[] allTypes = ChannelType.values();

  final private ICANModule module = new CANModuleStub(MODULE.MODULE_SCM);


  private ICANChannel createChannel(ChannelType type, ICANModule module,
      Enum<? extends IChannelEnum> channelEnum) {
    return CANChannelFactory.createChannel(type, module, channelEnum);
  }

  @Test
  public void testCreateChannel() {

    for (ChannelType type : this.allTypes) {

      // Construct
      ICANChannel ch = createChannel(type, this.module, this.dummyEnum);

      // Test properties
      assertNotNull(ch);
      assertEquals(type, ch.getType());
      assertEquals(this.dummyEnum, ch.getEnum());
      assertTrue(this.module == ch.getOwnerModule());

      // Test predefined scaling factor
      Map<Double, String> sfactorMap = ch.predefined().getPredefindScalingUnit();
      if (sfactorMap != null) {
        Iterator<Double> it = sfactorMap.keySet().iterator();
        while (it.hasNext()) {
          assertNotNull("Predefined scaling factors contains null", it.next());
        }
      }
    }
  }

  @Test
  public void testChannelParameters() {
    for (ChannelType type : this.allTypes) {

      // Construct
      ICANChannel ch = createChannel(type, this.module, this.dummyEnum);

      // Test configurable channel
      Object value;
      String param;
      for (int i = 0; i < paramTypes.length; i++) {
        param = (String) paramTypes[i][0];

        if (ch.hasParameter(param)) {
          value = ch.getParameter(param);

          assertNotNull("Parameter value must not be null", value);
          assertEquals(paramTypes[i][1], value.getClass());
        }
      }
    }
  }

  // ================== NullPointerException ===================

  @Test(expected = NullPointerException.class)
  public void testCreateChannelWithNullArg1() {
    createChannel(ChannelType.ANALOG_INPUT, new CANModuleStub(MODULE.MODULE_SCM), null);
  }

  @Test(expected = NullPointerException.class)
  public void testCreateChannelWithNullArg2() {
    createChannel(ChannelType.ANALOG_INPUT, null, this.dummyEnum);
  }

  @Test(expected = NullPointerException.class)
  public void testCreateChannelWithNullArg3() {
    createChannel(ChannelType.ANALOG_INPUT, null, null);
  }

  @Test(expected = NullPointerException.class)
  public void testCreateChannelWithNullArg4() {
    channel(null);
  }

  @Test
  public void testCreateSwitchOutputCh() {
    checkChannelParam(Arrays.asList(SwitchOutParam), channel(ChannelType.SWITCH_OUT));
  }

  @Test
  public void testCreateDigitalInputCh() {
    MODULE[] types = ModuleEnums.getSupportedTypes();
    for (MODULE t : types) {
      ICANModule m = new CANModuleStub(t);

      List<String> params;
      if (CANChannelFactory.SUPPORT_INVERT.contains(t)) {
        params = Arrays.asList(DigiInParam_withInvert);
      } else {
        params = Arrays.asList(DigiInParam);
      }

      checkChannelParam(params, createChannel(ChannelType.DIGITAL_INPUT, m, this.dummyEnum));
    }
  }

  @Test
  public void testCreateDigitalOutputCh() {
    checkChannelParam(Arrays.asList(DigiOutParam), channel(ChannelType.DIGITAL_OUTPUT));
  }

  @Test
  public void testCreateAnalogInputCh() {
    checkChannelParam(Arrays.asList(AnalogInParam), channel(ChannelType.ANALOG_INPUT));
  }

  @Test
  public void testCreateAnalogOutputCh() {
    String[] supportParam = {
        PARAM_EVENT_MS,
        PARAM_EVENTENABLED
    };
    List<String> supportParamList = Arrays.asList(supportParam);

    ICANChannel ch = createChannel(ChannelType.ANALOG_OUTPUT, new CANModuleStub(), this.dummyEnum);

    // ====== Test hasParameter() ======
    for (String name : allParamNames) {
      if (supportParamList.contains(name)) {
        assertTrue(ch.hasParameter(name));
      }
    }

    // ====== Test getParameters() ======
    for (String name : allParamNames) {
      if (supportParamList.contains(name)) {
        assertNotNull(ch.getParameter(name));
      }
    }

    // ====== Test parameters's type ======
    assertTrue(Long.class.isInstance(ch.getParameter(PARAM_EVENT_MS)));
    assertTrue(Boolean.class.isInstance(ch.getParameter(PARAM_EVENTENABLED)));
  }

  // Create channel
  private ICANChannel channel(ChannelType type) {
    return createChannel(type, this.module, this.dummyEnum);
  }

  private static void checkChannelParam(List<String> supportParamList, ICANChannel ch) {

    // ====== Test hasParameter ======
    for (String name : allParamNames) {
      if (supportParamList.contains(name)) {
        assertTrue(String.format("channel: %s should contain param:%s", ch.getType().name(), name),
            ch.hasParameter(name));
      } else {
        assertFalse(String.format("channel: %s should NOT contain param:%s", ch.getType().name(), name),
            ch.hasParameter(name));
      }
    }

    // ====== Test getParameters ======
    for (String name : allParamNames) {
      if (supportParamList.contains(name)) {
        assertNotNull(ch.getParameter(name));
      }
    }

    // ====== Test parameters's type ======
    for (String param : supportParamList) {
      Object value = ch.getParameter(param);
      Class<?> valueType = getParamClass(param);
      assertEquals(valueType, value.getClass());
    }
  }

  private static Class<?> getParamClass(String param) {
    for (int i = 0; i < paramTypes.length; i++) {
      if (paramTypes[i][0].equals(param)) {
        return (Class<?>) paramTypes[i][1];
      }
    }

    return null;
  }
}
