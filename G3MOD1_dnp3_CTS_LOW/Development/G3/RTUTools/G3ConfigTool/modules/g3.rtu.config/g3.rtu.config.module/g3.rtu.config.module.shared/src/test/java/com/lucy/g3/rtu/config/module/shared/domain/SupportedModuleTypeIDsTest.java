/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class SupportedModuleTypeIDsTest {

  private MODULE[] types;


  @Before
  public void setUp() throws Exception {
    this.types = ModuleEnums.getSupportedTypes();
  }

  @Test
  public void testGetSupportedTypes() {
    MODULE[] types = ModuleEnums.getSupportedTypes();
    assertNotNull(types);

    for (MODULE t : types) {
      assertNotNull(t);
      assertFalse(Strings.isBlank(t.getDescription()));
    }
  }

  @Test
  public void testGetSupportedIDs() {
    MODULE_ID[] ids = ModuleEnums.getSupportedIDs();
    assertNotNull(ids);

    for (MODULE_ID id : ids) {
      assertNotNull(id);
    }
  }

  @Test
  public void testGetMaxModuleNumber() {
    for (MODULE t : types) {
      assertTrue(ModuleEnums.getMaxModuleNumber(t) > 0);
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetMaxModuleNumberlNullArg() {
    ModuleEnums.getMaxModuleNumber(null);
  }

}
