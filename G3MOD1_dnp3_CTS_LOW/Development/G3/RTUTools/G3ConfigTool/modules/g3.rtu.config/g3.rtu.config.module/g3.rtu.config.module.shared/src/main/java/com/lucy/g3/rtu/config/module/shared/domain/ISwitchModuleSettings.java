/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.MOTOR_MODE;


/**
 *
 */
public interface ISwitchModuleSettings extends IModuleSettings {
  String FORCE_OPERATION_HINT =
      "Allowing/disallowing the user to operate open when open and close when closed";

  String FORCED_OPERATION_TEXT = "Allow Forced Operation";
  
  
  boolean setOpenPolarity(SwitchIndex index, Polarity polarity);
  boolean setClosePolarity(SwitchIndex index, Polarity polarity);
  boolean setAllowForcedOperation(SwitchIndex index, boolean allowForceOperation);
  boolean setMotorMode(SwitchIndex index, MOTOR_MODE mode);
}

