/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared;

import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class ModulePlugin implements IConfigPlugin,IConfigModuleFactory{
  
  private ModulePlugin () {}
  private final static ModulePlugin  INSTANCE = new ModulePlugin();
  
  public static void init() {
    ConfigFactory.getInstance().registerFactory(ModuleRepository.CONFIG_MODULE_ID, INSTANCE);
  }
  
  @Override
  public ModuleRepository create(IConfig owner) {
    return new ModuleRepository(owner);
  }
}

