/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.dialogs;

import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

import org.jdesktop.swingx.JXTable;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.module.canmodule.domain.Inhibit;

/**
 * A dialog for choosing inhibit channels.
 */
public class InhibitChooser extends AbstractDialog {

  static final String[] COLUMNS = { "Inhibit", "Channel" };

  static final int COLUMN_INHIBIT = 0;

  static final int COLUMN_CHANNEL = 1;

  private final InhibitTableModel model;

  private final Inhibit currentInhibit;


  public InhibitChooser(Frame parent, Inhibit initialInhibit) {
    super(parent, true);
    setTitle("Inhibit Setting");
    currentInhibit = initialInhibit.clone();
    model = new InhibitTableModel();
    pack();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  public Inhibit getInhibit() {
    return currentInhibit;
  }

  @Override
  protected JComponent createContentPanel() {
    JXTable inhibitTable = new JXTable(model);
    inhibitTable.setFillsViewportHeight(true);
    inhibitTable.getTableHeader().setReorderingAllowed(false);
    inhibitTable.getColumnModel().getColumn(COLUMN_INHIBIT).setMaxWidth(50);
    JScrollPane scroll = new JScrollPane(inhibitTable);
    scroll.setPreferredSize(new Dimension(300, 200));
    return scroll;
  }

  @Override
  protected JPanel createButtonPanel() {
    return createOKCancelButtonPanel();
  }


  private class InhibitTableModel extends AbstractTableModel {

    public InhibitTableModel() {
      super();
    }

    @Override
    public String getColumnName(int column) {
      return COLUMNS[column];
    }

    @Override
    public int getRowCount() {
      return currentInhibit.getBitNum();
    }

    @Override
    public int getColumnCount() {
      return COLUMNS.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (rowIndex >= 0) {
        if (columnIndex == 0) {
          return currentInhibit.getBitValue(rowIndex);
        } else if (columnIndex == 1) {
          return currentInhibit.getChannel(rowIndex).getName();
        }
      }
      return "";
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      if (columnIndex == COLUMN_INHIBIT && rowIndex >= 0) {
        currentInhibit.setBitValue(rowIndex, (Boolean) aValue);
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex == COLUMN_INHIBIT ? true : false;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex == COLUMN_INHIBIT) {
        return Boolean.class;
      }
      return super.getColumnClass(columnIndex);
    }

  }
}