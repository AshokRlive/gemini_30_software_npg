/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import javax.swing.JCheckBox;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 *
 */
class IOMOptionsPanel extends WizardPage {

  static final String KEY_CREATE_IOM_DIGITAL_OUTPUTS = "CREATE_IOM_DIGITAL_OUTPUTS";
  static final boolean createIOMDigitalOutputsDefault = false;


  public IOMOptionsPanel() {
    initComponents();
    chkCreateDigitalOutputs.setName(KEY_CREATE_IOM_DIGITAL_OUTPUTS);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    chkCreateDigitalOutputs = new JCheckBox();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default",
        "2*(default, $lgap), default"));

    // ---- chkCreateDigitalOutputs ----
    chkCreateDigitalOutputs.setText("Create Three Digital Output Control Logics");
    add(chkCreateDigitalOutputs, CC.xy(1, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox chkCreateDigitalOutputs;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
