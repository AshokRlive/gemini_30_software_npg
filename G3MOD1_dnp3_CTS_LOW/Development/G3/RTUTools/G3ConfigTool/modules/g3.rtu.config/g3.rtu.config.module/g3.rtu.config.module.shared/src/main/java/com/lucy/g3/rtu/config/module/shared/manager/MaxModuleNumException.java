/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.manager;

import com.lucy.g3.itemlist.IItemListManager.AddItemException;

/**
 * This exception should be thrown when the number of modules reaches maximum.
 */
public class MaxModuleNumException extends AddItemException {

  public MaxModuleNumException(String message) {
    super(message);
  }
}
