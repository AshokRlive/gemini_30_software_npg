/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.shared.domain;

import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The interface of G3 RTU module.
 */
public interface Module extends INode, IValidation {
  /**
   * The name of property {@value} . Type: <code>MODULE_ID</code>.
   */
  String PROPERTY_MODULE_ID = "id";

  /**
   * The name of property {@value} . Type: <code>MODULE</code>.
   */
  String PROPERTY_MODULE_TYPE = "type";

  /**
   * The name of read-only property {@value} . Type: <code>String</code>.
   *
   * @see #getShortName()
   */
  String PROPERTY_SHORTNAME = "shortName";

  /**
   * The name of read-only property {@value} .Type: <code>String</code>.
   *
   * @see #getFullName()
   */
  String PROPERTY_FULLNAME = "fullName";


  /**
   * Gets this module's ID.
   *
   * @return the ID enumeration of this module.
   */
  MODULE_ID getId();

  /**
   * Gets the short name of this module.
   *
   * @return this module's short name. E.g. "PSM" for Power Supply, "SCM 0" for
   *         Switch Module 0, "FDM2" for Fault Detect Module 2.
   * @see #PROPERTY_SHORTNAME
   */
  String getShortName();

  /**
   * Gets the full name of this module.
   *
   * @return this module's full name. E.g. "Main Control Module" for MCM,
   *         "Switch Module 0" for Module SCM 0.
   * @see #PROPERTY_FULLNAME
   */
  String getFullName();

  /**
   * Gets this module's Type.
   *
   * @return the type enumeration of this module.
   */
  MODULE getType();

  /**
   * Gets icon resource of this module.
   */
  ModuleIcon getIcons();
  
  IModuleSettings getSettings();

}
