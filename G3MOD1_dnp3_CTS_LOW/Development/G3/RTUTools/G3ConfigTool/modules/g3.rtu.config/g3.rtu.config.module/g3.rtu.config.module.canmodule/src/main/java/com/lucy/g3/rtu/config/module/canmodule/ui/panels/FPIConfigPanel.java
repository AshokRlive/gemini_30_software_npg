/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.panels;

import java.awt.BorderLayout;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultFormatterFactory;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.LabelledFTFSupport;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigCurrentFault;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigEarthFault;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigPhaseFault;
import com.lucy.g3.rtu.config.module.canmodule.ui.widgets.CTRatioEditorCombox;

/**
 * FPI Channel configuration panel.
 */
class FPIConfigPanel extends JPanel {

  private final PresentationModel<FPIConfig> pmFpiconf;
  private final PresentationModel<FPIConfigEarthFault> pmEarth;
  private final PresentationModel<FPIConfigEarthFault> pmSensitiveEarth;
  private final PresentationModel<FPIConfigPhaseFault> pmPhase;
  private final PresentationModel<FPIConfigCurrentFault> pmCurrentPresence;
  private final PresentationModel<FPIConfigCurrentFault> pmCurrentAbsence;


  public FPIConfigPanel(FPIConfig bean) {
    pmFpiconf = new PresentationModel<>(bean);
    pmEarth = new PresentationModel<>(bean.earchFault);
    pmPhase = new PresentationModel<>(bean.phaseFault);
    pmSensitiveEarth = new PresentationModel<>(bean.sensitiveEarthFault);
    pmCurrentAbsence = new PresentationModel<>(bean.currentAbsence);
    pmCurrentPresence = new PresentationModel<>(bean.currentPresence);

    initComponents();
    initComponentsBinding();
  }

  private void initComponentsBinding() {
    ValueModel vm = pmFpiconf.getModel(FPIConfig.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, confPanel, "visible");
  }

  private static JFormattedTextField createNonNegativeLongField(ValueModel vm) {
    JFormattedTextField tf = BasicComponentFactory.createLongField(vm);
    tf.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(0L, Long.MAX_VALUE)));
    FormattedTextFieldSupport.installCommitOnType(tf);
    LabelledFTFSupport.decorateWithLabel(tf, 0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    return tf;
  }
  
  private void createUIComponents() {
    ValueModel vm;

    vm = pmFpiconf.getModel(FPIConfig.PROPERTY_ENABLED);
    checkBoxEnabled = BasicComponentFactory.createCheckBox(vm, "");

    vm = pmFpiconf.getModel(FPIConfig.PROPERTY_CTRATIO);
    comboBoxCTRatio = new CTRatioEditorCombox(vm, FPIConfig.getDefaultCTRatio());

    vm = pmFpiconf.getModel(FPIConfig.PROPERTY_SELF_RESET_MIN);
    selfResetTime = createNonNegativeLongField(vm);

    vm = pmPhase.getModel(FPIConfigPhaseFault.PROPERTY_MIN_FAULT_DURATION_MS);
    phaseFaultDuration = createNonNegativeLongField(vm);

    vm = pmPhase.getModel(FPIConfigPhaseFault.PROPERTY_INSTANT_FAULT_CURRENT);
    phaseInstantFaultCurrent = createNonNegativeLongField(vm);

    vm = pmPhase.getModel(FPIConfigPhaseFault.PROPERTY_TIMED_FAULT_CURRENT);
    phaseTimeFaultCurrent = createNonNegativeLongField(vm);

    vm = pmPhase.getModel(FPIConfigPhaseFault.PROPERTY_MIN_OVERLOAD_DURATIONMS);
    phaseMinOverloadDuration = createNonNegativeLongField(vm);

    vm = pmPhase.getModel(FPIConfigPhaseFault.PROPERTY_OVERLOAD_CURRENT);
    phaseOverloadCurrent = createNonNegativeLongField(vm);

    vm = pmEarth.getModel(FPIConfigEarthFault.PROPERTY_MIN_FAULT_DURATION_MS);
    earthFaultDuration = createNonNegativeLongField(vm);

    vm = pmEarth.getModel(FPIConfigEarthFault.PROPERTY_INSTANT_FAULT_CURRENT);
    earthInstantFaultCurrent = createNonNegativeLongField(vm);
    
    vm = pmEarth.getModel(FPIConfigEarthFault.PROPERTY_TIMED_FAULT_CURRENT);
    earthTimeFaultCurrent = createNonNegativeLongField(vm);
   
    vm = pmSensitiveEarth.getModel(FPIConfigEarthFault.PROPERTY_MIN_FAULT_DURATION_MS);
    earthFaultDuration2 = createNonNegativeLongField(vm);
    
    vm = pmSensitiveEarth.getModel(FPIConfigEarthFault.PROPERTY_INSTANT_FAULT_CURRENT);
    earthInstantFaultCurrent2 = createNonNegativeLongField(vm);
    
    vm = pmSensitiveEarth.getModel(FPIConfigEarthFault.PROPERTY_TIMED_FAULT_CURRENT);
    earthTimeFaultCurrent2 = createNonNegativeLongField(vm);
    
    /**=== Current Absence ===*/
    vm = pmCurrentAbsence.getModel(FPIConfigCurrentFault.PROPERTY_TIMED_FAULT_CURRENT);
    currentAbsenceFaultCurrent = createNonNegativeLongField(vm);
    
    vm = pmCurrentAbsence.getModel(FPIConfigCurrentFault.PROPERTY_MIN_FAULT_DURATION_MS);
    currentAbsenceFaultDuration = createNonNegativeLongField(vm);
    
    vm = pmCurrentAbsence.getModel(FPIConfigCurrentFault.PROPERTY_INSTANT_FAULT_CURRENT);
    currentAbsenceInstantFaultCurrent = createNonNegativeLongField(vm);
    
    /**=== Current Presence ===*/
    vm = pmCurrentPresence.getModel(FPIConfigCurrentFault.PROPERTY_TIMED_FAULT_CURRENT);
    currentPresenceFaultCurrent = createNonNegativeLongField(vm);
    
    vm = pmCurrentPresence.getModel(FPIConfigCurrentFault.PROPERTY_MIN_FAULT_DURATION_MS);
    currentPresenceFaultDuration = createNonNegativeLongField(vm);
    
    vm = pmCurrentPresence.getModel(FPIConfigCurrentFault.PROPERTY_INSTANT_FAULT_CURRENT);
    currentPresenceInstantFaultCurrent = createNonNegativeLongField(vm);
  }
  

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    confPanel = new JPanel();
    panel3 = new JPanel();
    label14 = new JLabel();
    JLabel label15 = new JLabel();
    JLabel label16 = new JLabel();
    panel6 = new JPanel();
    JLabel label29 = new JLabel();
    JLabel label30 = new JLabel();
    JLabel label31 = new JLabel();
    JLabel label32 = new JLabel();
    JLabel label33 = new JLabel();
    JLabel label34 = new JLabel();
    panel5 = new JPanel();
    JLabel label23 = new JLabel();
    JLabel label24 = new JLabel();
    JLabel label25 = new JLabel();
    JLabel label26 = new JLabel();
    JLabel label27 = new JLabel();
    JLabel label28 = new JLabel();
    panel1 = new JPanel();
    JLabel label1 = new JLabel();
    JLabel label4 = new JLabel();
    JLabel label2 = new JLabel();
    JLabel label9 = new JLabel();
    JLabel label3 = new JLabel();
    JLabel label10 = new JLabel();
    panel4 = new JPanel();
    JLabel label17 = new JLabel();
    JLabel label18 = new JLabel();
    JLabel label19 = new JLabel();
    JLabel label20 = new JLabel();
    JLabel label21 = new JLabel();
    JLabel label22 = new JLabel();
    panel2 = new JPanel();
    JLabel label5 = new JLabel();
    JLabel label6 = new JLabel();
    JLabel label7 = new JLabel();
    JLabel label11 = new JLabel();
    JLabel label8 = new JLabel();
    JLabel label12 = new JLabel();
    JLabel lblOverloadCurrent2 = new JLabel();
    JLabel label13 = new JLabel();
    JLabel lblMinOverloadDuration2 = new JLabel();
    JLabel minOverloadDurationUnit2 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout(0, 5));

    //---- checkBoxEnabled ----
    checkBoxEnabled.setText("Enabled");
    add(checkBoxEnabled, BorderLayout.NORTH);

    //======== confPanel ========
    {
      confPanel.setLayout(new FormLayout(
        "2*(pref, $ugap), pref, 2*($lcgap, default)",
        "fill:pref, $pgap, fill:pref"));

      //======== panel3 ========
      {
        panel3.setBorder(new TitledBorder("General"));
        panel3.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, default",
          "default, $ugap, default, $lgap, default"));

        //---- label14 ----
        label14.setText("Self Reset Time:");
        panel3.add(label14, CC.xy(1, 1));
        panel3.add(selfResetTime, CC.xy(3, 1));

        //---- label15 ----
        label15.setText("mins");
        panel3.add(label15, CC.xy(5, 1));

        //---- label16 ----
        label16.setText("CT Ratio:");
        panel3.add(label16, CC.xy(1, 3));
        panel3.add(comboBoxCTRatio, CC.xy(3, 3));
      }
      confPanel.add(panel3, CC.xy(1, 1));

      //======== panel6 ========
      {
        panel6.setBorder(new TitledBorder("Current Absence"));
        panel6.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, default",
          "2*(default, $lgap), default"));

        //---- label29 ----
        label29.setText("Minimum Duration:");
        panel6.add(label29, CC.xy(1, 1));
        panel6.add(currentAbsenceFaultDuration, CC.xy(3, 1));

        //---- label30 ----
        label30.setText("ms");
        panel6.add(label30, CC.xy(5, 1));

        //---- label31 ----
        label31.setText("Timed Current:");
        panel6.add(label31, CC.xy(1, 3));
        panel6.add(currentAbsenceFaultCurrent, CC.xy(3, 3));

        //---- label32 ----
        label32.setText("A");
        panel6.add(label32, CC.xy(5, 3));

        //---- label33 ----
        label33.setText("Instant Fault Current:");
        label33.setVisible(false);
        panel6.add(label33, CC.xy(1, 5));

        //---- currentAbsenceInstantFaultCurrent ----
        currentAbsenceInstantFaultCurrent.setVisible(false);
        panel6.add(currentAbsenceInstantFaultCurrent, CC.xy(3, 5));

        //---- label34 ----
        label34.setText("A");
        label34.setVisible(false);
        panel6.add(label34, CC.xy(5, 5));
      }
      confPanel.add(panel6, CC.xy(3, 1));

      //======== panel5 ========
      {
        panel5.setBorder(new TitledBorder("Current Presence"));
        panel5.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, default",
          "2*(default, $lgap), default"));

        //---- label23 ----
        label23.setText("Minimum Duration:");
        panel5.add(label23, CC.xy(1, 1));
        panel5.add(currentPresenceFaultDuration, CC.xy(3, 1));

        //---- label24 ----
        label24.setText("ms");
        panel5.add(label24, CC.xy(5, 1));

        //---- label25 ----
        label25.setText("Timed Current:");
        panel5.add(label25, CC.xy(1, 3));
        panel5.add(currentPresenceFaultCurrent, CC.xy(3, 3));

        //---- label26 ----
        label26.setText("A");
        panel5.add(label26, CC.xy(5, 3));

        //---- label27 ----
        label27.setText("Instant Fault Current:");
        label27.setVisible(false);
        panel5.add(label27, CC.xy(1, 5));

        //---- currentPresenceInstantFaultCurrent ----
        currentPresenceInstantFaultCurrent.setVisible(false);
        panel5.add(currentPresenceInstantFaultCurrent, CC.xy(3, 5));

        //---- label28 ----
        label28.setText("A");
        label28.setVisible(false);
        panel5.add(label28, CC.xy(5, 5));
      }
      confPanel.add(panel5, CC.xy(5, 1));

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Earth Fault"));
        panel1.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, default",
          "2*(default, $lgap), default"));

        //---- label1 ----
        label1.setText("Minimum Fault Duration:");
        panel1.add(label1, CC.xy(1, 1));
        panel1.add(earthFaultDuration, CC.xy(3, 1));

        //---- label4 ----
        label4.setText("ms");
        panel1.add(label4, CC.xy(5, 1));

        //---- label2 ----
        label2.setText("Timed Fault Current:");
        panel1.add(label2, CC.xy(1, 3));
        panel1.add(earthTimeFaultCurrent, CC.xy(3, 3));

        //---- label9 ----
        label9.setText("A");
        panel1.add(label9, CC.xy(5, 3));

        //---- label3 ----
        label3.setText("Instant Fault Current:");
        panel1.add(label3, CC.xy(1, 5));
        panel1.add(earthInstantFaultCurrent, CC.xy(3, 5));

        //---- label10 ----
        label10.setText("A");
        panel1.add(label10, CC.xy(5, 5));
      }
      confPanel.add(panel1, CC.xy(1, 3));

      //======== panel4 ========
      {
        panel4.setBorder(new TitledBorder("Sensitive Earth Fault"));
        panel4.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, default",
          "2*(default, $lgap), default"));

        //---- label17 ----
        label17.setText("Minimum Fault Duration:");
        panel4.add(label17, CC.xy(1, 1));
        panel4.add(earthFaultDuration2, CC.xy(3, 1));

        //---- label18 ----
        label18.setText("ms");
        panel4.add(label18, CC.xy(5, 1));

        //---- label19 ----
        label19.setText("Timed Fault Current:");
        panel4.add(label19, CC.xy(1, 3));
        panel4.add(earthTimeFaultCurrent2, CC.xy(3, 3));

        //---- label20 ----
        label20.setText("A");
        panel4.add(label20, CC.xy(5, 3));

        //---- label21 ----
        label21.setText("Instant Fault Current:");
        panel4.add(label21, CC.xy(1, 5));
        panel4.add(earthInstantFaultCurrent2, CC.xy(3, 5));

        //---- label22 ----
        label22.setText("A");
        panel4.add(label22, CC.xy(5, 5));
      }
      confPanel.add(panel4, CC.xy(3, 3));

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder("Phase Fault"));
        panel2.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, default",
          "4*(default, $lgap), default"));

        //---- label5 ----
        label5.setText("Minimum Fault Duration:");
        panel2.add(label5, CC.xy(1, 1));
        panel2.add(phaseFaultDuration, CC.xy(3, 1));

        //---- label6 ----
        label6.setText("ms");
        panel2.add(label6, CC.xy(5, 1));

        //---- label7 ----
        label7.setText("Timed Fault Current:");
        panel2.add(label7, CC.xy(1, 3));
        panel2.add(phaseTimeFaultCurrent, CC.xy(3, 3));

        //---- label11 ----
        label11.setText("A");
        panel2.add(label11, CC.xy(5, 3));

        //---- label8 ----
        label8.setText("Instant Fault Current:");
        panel2.add(label8, CC.xy(1, 5));
        panel2.add(phaseInstantFaultCurrent, CC.xy(3, 5));

        //---- label12 ----
        label12.setText("A");
        panel2.add(label12, CC.xy(5, 5));

        //---- lblOverloadCurrent2 ----
        lblOverloadCurrent2.setText("Overload Current:");
        panel2.add(lblOverloadCurrent2, CC.xy(1, 7));

        //---- phaseOverloadCurrent ----
        phaseOverloadCurrent.setEnabled(false);
        panel2.add(phaseOverloadCurrent, CC.xy(3, 7));

        //---- label13 ----
        label13.setText("A");
        panel2.add(label13, CC.xy(5, 7));

        //---- lblMinOverloadDuration2 ----
        lblMinOverloadDuration2.setText("Minimum Overload Duration:");
        panel2.add(lblMinOverloadDuration2, CC.xy(1, 9));

        //---- phaseMinOverloadDuration ----
        phaseMinOverloadDuration.setEnabled(false);
        panel2.add(phaseMinOverloadDuration, CC.xy(3, 9));

        //---- minOverloadDurationUnit2 ----
        minOverloadDurationUnit2.setText("ms");
        panel2.add(minOverloadDurationUnit2, CC.xy(5, 9));
      }
      confPanel.add(panel2, CC.xy(5, 3));
    }
    add(confPanel, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox checkBoxEnabled;
  private JPanel confPanel;
  private JPanel panel3;
  private JLabel label14;
  private JFormattedTextField selfResetTime;
  private JComboBox<CTRatio> comboBoxCTRatio;
  private JPanel panel6;
  private JFormattedTextField currentAbsenceFaultDuration;
  private JFormattedTextField currentAbsenceFaultCurrent;
  private JFormattedTextField currentAbsenceInstantFaultCurrent;
  private JPanel panel5;
  private JFormattedTextField currentPresenceFaultDuration;
  private JFormattedTextField currentPresenceFaultCurrent;
  private JFormattedTextField currentPresenceInstantFaultCurrent;
  private JPanel panel1;
  private JFormattedTextField earthFaultDuration;
  private JFormattedTextField earthTimeFaultCurrent;
  private JFormattedTextField earthInstantFaultCurrent;
  private JPanel panel4;
  private JFormattedTextField earthFaultDuration2;
  private JFormattedTextField earthTimeFaultCurrent2;
  private JFormattedTextField earthInstantFaultCurrent2;
  private JPanel panel2;
  private JFormattedTextField phaseFaultDuration;
  private JFormattedTextField phaseTimeFaultCurrent;
  private JFormattedTextField phaseInstantFaultCurrent;
  private JFormattedTextField phaseOverloadCurrent;
  private JFormattedTextField phaseMinOverloadDuration;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
