/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumHMI.HMI_CH_AINPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Human Machine Interface Module.
 */
public class ModuleHMI extends AbstractCANModule implements ICANModule {
  private final ModuleHMISettings settings = new ModuleHMISettings(this);
  
  public ModuleHMI() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleHMI(MODULE_ID id) {
    super(id, MODULE.MODULE_HMI);
  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise analogue input channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, HMI_CH_AINPUT.values()));

    return chMap;
  }

  @Override
  public ModuleHMISettings getSettings() {
    return settings;
  }

}