/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule;

import javax.swing.JComponent;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManagerImpl;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.ui.CANModuleManagePage;
import com.lucy.g3.rtu.config.module.canmodule.ui.CANModulePage;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class CANModulePlugin implements IConfigPlugin, IConfigModuleFactory, IPageFactory{
  
  private CANModulePlugin () {}
  private final static CANModulePlugin  INSTANCE = new CANModulePlugin();
  
  
  public static CANModulePlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory.getInstance().registerFactory(CANModuleManager.CONFIG_MODULE_ID, INSTANCE);
    
    PageFactories.registerFactory(PageFactories.KEY_CAN_MODULE, INSTANCE);
  }
  
  @Override
  public CANModuleManager create(IConfig owner) {
    ModuleRepository repo = null;
    if(owner != null) {
      repo =  owner.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    }
    
    return new CANModuleManagerImpl(owner,repo);
  }
  

  @Override
  public Page createPage(Object data) {
    if (data instanceof CANModuleManager) {
      return new CANModuleManagePage((CANModuleManager) data);

    } else if (data instanceof ICANModule) {
      ICANModule m = (ICANModule) data;
      return new CANModulePage(m, getSettingsComponent(m));
    }

    return null;
  }
  
  private static JComponent getSettingsComponent(ICANModule m) {
    JComponent editor = null;
    IModuleSettings settings = m.getSettings();
    
    if (settings != null) {
      IModuleSettingsEditor e = settings.getEditor();
      if (e != null) {
        editor = e.getEditorComopnent();
        editor.setName(e.getEditorTitle());
      }
    }

    return editor;
  }

}

