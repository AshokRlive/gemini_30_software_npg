/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.ISupportChangeID;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumIOM.IOM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumIOM.IOM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumIOM.IOM_CH_DOUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Input Output Module.
 */
public final class ModuleIOM extends AbstractCANModule implements ICANModule, ISupportChangeID {

  public ModuleIOM() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleIOM(MODULE_ID id) {
    super(id, MODULE.MODULE_IOM);
  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise analogue input channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, IOM_CH_AINPUT.values()));

    // Initialise digital input channels
    chMap.put(ChannelType.DIGITAL_INPUT,
        createChannels(ChannelType.DIGITAL_INPUT, IOM_CH_DINPUT.values()));

    // Initialise digital output channels
    chMap.put(ChannelType.DIGITAL_OUTPUT,
        createChannels(ChannelType.DIGITAL_OUTPUT, IOM_CH_DOUT.values()));

    return chMap;
  }

  @Override
  public void setId(MODULE_ID moduleID) {
    super.modifyId(moduleID);
  }

  @Override
  public IModuleSettings getSettings() {
    // TODO Auto-generated method stub
    return null;
  }

}