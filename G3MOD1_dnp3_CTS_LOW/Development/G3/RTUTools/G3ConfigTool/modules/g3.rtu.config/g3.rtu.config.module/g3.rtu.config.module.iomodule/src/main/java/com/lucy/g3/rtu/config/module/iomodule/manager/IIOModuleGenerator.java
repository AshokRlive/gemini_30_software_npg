/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.manager;

import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.generator.IConfigGenerator;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


/**
 *
 */
public interface IIOModuleGenerator extends IConfigGenerator {

  void createCLogic(Module... module);
    
  void createFullVirtualPoints(Module module);

  void createDefaultVirtualPoints(Module module);

  void createScreens(Module module);

  void createSwitchLogic(ISwitchModule module);

  void createDigitialOutputLogic(Module module);


  void createCANModule(IModuleManager<?> manager, MODULE type, int quantity);

  Module createCANModule(IModuleManager<?> manager, MODULE type, MODULE_ID id);

  void createVirtualPoints(GenerateOptions option, Module... module);

}

