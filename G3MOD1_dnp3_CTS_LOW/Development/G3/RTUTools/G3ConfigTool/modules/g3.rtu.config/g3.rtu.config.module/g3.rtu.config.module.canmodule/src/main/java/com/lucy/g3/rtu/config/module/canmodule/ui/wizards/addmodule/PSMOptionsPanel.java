/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.ui.wizards.addmodule;

import javax.swing.JCheckBox;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 *
 */
class PSMOptionsPanel extends WizardPage {

  static final String KEY_CREATE_FAN_TEST_CLOGIC = "CREATE_FAN_TEST_CLOGIC";
  static final String KEY_CREATE_BATTERY_CHARGER_CLOGIC = "CREATE_BATTERY_CHARGER_CLOGIC";

  static final boolean createFanTestCLogicDefault = false;
  static final boolean createBatteryChargerCLogicDefault = false;


  public PSMOptionsPanel() {
    initComponents();
    chkCreateFanCLogic.setName(KEY_CREATE_FAN_TEST_CLOGIC);
    chkCreateBatteryChargerCLogic.setName(KEY_CREATE_BATTERY_CHARGER_CLOGIC);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    chkCreateFanCLogic = new JCheckBox();
    chkCreateBatteryChargerCLogic = new JCheckBox();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default",
        "2*(default, $lgap), default"));

    // ---- chkCreateFanCLogic ----
    chkCreateFanCLogic.setText("Create Fan Test Control Logic");
    add(chkCreateFanCLogic, CC.xy(1, 1));

    // ---- chkCreateBatteryChargerCLogic ----
    chkCreateBatteryChargerCLogic.setText("Create Battery Charger Control Logic");
    add(chkCreateBatteryChargerCLogic, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox chkCreateFanCLogic;
  private JCheckBox chkCreateBatteryChargerCLogic;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
