/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 *
 *
 */
public class CANModuleFactory {

  /**
   * Factory method for creating a module.
   *
   * @param type
   * @param id
   * @return new module instance
   * @throws NullPointerException
   *           if type is null.
   * @throws IllegalArgumentException
   *           if type is not currently supported.
   */
  public static ICANModule createModule(MODULE type, MODULE_ID id) {
    Preconditions.checkNotNull(type, "type must not be null");
    Preconditions.checkNotNull(id, "id must not be null");

    // Create module
    ICANModule module = null;
    switch (type) {
    case MODULE_FDM:
      module = new ModuleFDM(id);
      break;
    case MODULE_SCM:
      module = new ModuleSCM(id);
      break;
    case MODULE_SCM_MK2:
      module = new ModuleSCM_MK2(id);
      break;
    case MODULE_HMI:
      module = new ModuleHMI(id);
      break;
    case MODULE_MCM:
      module = new ModuleMCM(id);
      break;
    case MODULE_PSM:
      module = new ModulePSM(id);
      break;
    case MODULE_IOM:
      module = new ModuleIOM(id);
      break;
    case MODULE_DSM:
      module = new ModuleDSM(id);
      break;
    case MODULE_FPM:
      module = new ModuleFPM(id);
      break;
    default:
      throw new IllegalArgumentException("Unsupported module type:" + type);
    }

    return module;
  }
}
