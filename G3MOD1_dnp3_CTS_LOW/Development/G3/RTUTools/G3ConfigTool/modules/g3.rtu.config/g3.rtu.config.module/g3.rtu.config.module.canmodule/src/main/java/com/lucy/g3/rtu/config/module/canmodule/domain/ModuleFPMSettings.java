/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.Arrays;
import java.util.List;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.ui.panels.FPMSettingsPanel;
import com.lucy.g3.rtu.config.module.shared.domain.AbstractModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;

public final class ModuleFPMSettings extends AbstractModuleSettings {

  private final FPIConfig[] fpi;

  private FPMSettingsPanel editor;
  public ModuleFPMSettings(IChannel... fpiResetChannels) {
    super();
    fpi = new FPIConfig[fpiResetChannels.length];

    for (int i = 0; i < fpi.length; i++) {
      fpi[i] = new FPIConfig(fpiResetChannels[i]);
    }
  }
  
  @Override
  public IModuleSettingsEditor getEditor() {
    if(editor == null)
      editor = new FPMSettingsPanel(this); 
    return editor;
  }

  public FPIConfig getFpi(int index) {
    return fpi[index];
  }

  public FPIConfig[] getAllFpi() {
    return Arrays.copyOf(fpi, fpi.length);
  }
  
  public List<FPIConfig> getAllFpiAsList() {
    return Arrays.asList(fpi);
  }
}
