/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static com.jgoodies.common.base.Preconditions.checkNotNull;

import com.lucy.g3.rtu.config.channel.domain.AbstractChannel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IPredefinedChannelConfig;
import com.lucy.g3.rtu.config.channel.validation.IChannelValidator;
import com.lucy.g3.rtu.config.module.canmodule.validation.CANChannelValidator;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 * A generic implementation of {@code Channel}. This implementation depends on
 * the generated Enum Class from Channel XML(e.g. ModuleProtocolPSM.xml).
 * <p>
 * The parameter of a channel can be defined by calling method
 * {@linkplain #addParameter(String, Object)}.
 * </p>
 */

public final class CANChannel extends AbstractChannel implements ICANChannel {

  private final Enum<? extends IChannelEnum> channelEnum;

  private final IChannelValidator validator;

  private final PredefinedCANChannelConf predefined;


  /**
   * Construct a channel from channel enumeration.
   *
   * @param chanenlType
   *          the type of this channel.
   * @param ownerModule
   *          the module this channel belongs to.
   * @param channelEnum
   *          the enumeration where this channel is defined.
   * @throws NullPointerException
   *           if any argument(channelType, parentModule or channelEnum) is
   *           null.
   */
  public CANChannel(ChannelType channelType, ICANModule ownerModule,
      Enum<? extends IChannelEnum> channelEnum) {

    this(channelType, ownerModule, channelEnum, false);
  }

  /**
   * Construct a channel from channel enumeration.
   *
   * @param chanenlType
   *          the type of this channel.
   * @param ownerModule
   *          the module this channel belongs to.
   * @param channelEnum
   *          the enumeration where this channel is defined.
   * @param readOnly
   *          indicates if this channel can be changed by users.
   * @throws NullPointerException
   *           if any argument(channelType, parentModule or channelEnum) is
   *           null.
   */
  public CANChannel(ChannelType channelType, ICANModule ownerModule, Enum<? extends IChannelEnum> channelEnum,
      boolean readOnly) {
    super(channelType, ownerModule, ((IChannelEnum) channelEnum).getDescription());
    this.channelEnum = checkNotNull(channelEnum, "Channel Enum  must not be null");
    this.predefined = new PredefinedCANChannelConf((IChannelEnum) channelEnum);

    setReadOnly(readOnly);

    validator = new CANChannelValidator(this);
  }

  @Override
  protected void addParameter(String paraName, Object value) {

    // Set Event Enable default value to False
    if (ICANChannel.PARAM_EVENTENABLED.equals(paraName)) {
      value = isConnectedToTarget(NodeType.VIRTUAL_POINT);
      super.addParameter(paraName, value);
      // Set Event Enable default value to false and not modifiable by users
      setParameterModifiable(PARAM_EVENTENABLED, Boolean.FALSE);

    } else {
      super.addParameter(paraName, value);
    }
  }
  
  
  @Override
  protected void removeParameter(String paraName) {
    super.removeParameter(paraName);
  }

  @Override
  public ICANModule getOwnerModule() {
    return (ICANModule) super.getOwnerModule();
  }

  @Override
  public IChannelEnum getEnum() {
    return (IChannelEnum) channelEnum;
  }

  @Override
  public int getId() {
    return getEnum().getID();
  }

  @Override
  public String getGroup() {
    return getEnum().getGroup();
  }

  @Override
  public IPredefinedChannelConfig predefined() {
    return predefined;
  }

  /**
   * Update Event Enabled parameter and force it to be enabled if this channel
   * is mapped to a point, otherwise disable it.
   */
  private void updateEventEnabled() {
    if (hasParameter(ICANChannel.PARAM_EVENTENABLED)) {
      addParameter(ICANChannel.PARAM_EVENTENABLED,
          isConnectedToTarget(NodeType.VIRTUAL_POINT) ? Boolean.TRUE : Boolean.FALSE);
    }
  }

  @Override
  public IChannelValidator getValidator() {
    return validator;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
    updateEventEnabled();
  }

}
