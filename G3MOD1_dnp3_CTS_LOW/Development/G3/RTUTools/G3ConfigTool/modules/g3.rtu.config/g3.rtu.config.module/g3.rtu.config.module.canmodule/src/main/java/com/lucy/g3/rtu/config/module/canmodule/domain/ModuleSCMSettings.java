/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.module.canmodule.ui.panels.SCMSettingsPanel;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettingsEditor;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.MOTOR_MODE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;

/**
 *
 */
public class ModuleSCMSettings extends AbstractSwitchModuleSettings {

  /**
   * The name of property {@value} .
   *
   * @see #getOpenColour()
   * @see #setOpenColour(LED_COLOUR)
   */
  public static final String PROPERTY_OPEN_COLOUR = "openColour";
  public static final String PROPERTY_ALLOWFORCEDOPERATION = "allowForcedOperation";

  private boolean allowForcedOperation;
  private LED_COLOUR openColour = LED_COLOUR.LED_COLOUR_GREEN;

  

  public LED_COLOUR getOpenColour() {
    return openColour;
  }

  public void setOpenColour(LED_COLOUR openColour) {
    if (openColour == null) {
      Logger.getLogger(getClass()).error("Cannot set LED colour to null");
      return;
    }

    Object oldValue = getOpenColour();
    this.openColour = openColour;
    firePropertyChange(PROPERTY_OPEN_COLOUR, oldValue, openColour);
  }
  
  public boolean isAllowForcedOperation() {
    return allowForcedOperation;
  }

  public void setAllowForcedOperation(boolean allowForcedOperation) {
    Object oldValue = this.allowForcedOperation;
    this.allowForcedOperation = allowForcedOperation;
    firePropertyChange(PROPERTY_ALLOWFORCEDOPERATION, oldValue,
        allowForcedOperation);
  }

  @Override
  public boolean setOpenPolarity(SwitchIndex index, Polarity polarity) {
    return false; // Not available
  }

  @Override
  public boolean setClosePolarity(SwitchIndex index, Polarity polarity) {
    return false; // Not available
  }
  
  @Override
  public boolean setAllowForcedOperation(SwitchIndex index, boolean allowForceOperation) {
    setAllowForcedOperation(allowForceOperation);
    return true;
  }
  
  @Override
  public boolean setMotorMode(SwitchIndex index, MOTOR_MODE mode) {
    return false; // Unsupported
  }
  
  private SCMSettingsPanel editor;
  @Override
  public IModuleSettingsEditor getEditor() {
    if(editor == null)
      editor = new SCMSettingsPanel(this);
    return editor;
  }

  

}
