/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumMCM.MCM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumMCM.MCM_CH_DINPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of Main Control Module.
 */
public final class ModuleMCM extends AbstractCANModule implements ICANModule {

  public ModuleMCM() {
    this(MODULE_ID.MODULE_ID_0);
  }

  public ModuleMCM(MODULE_ID id) {
    super(id, MODULE.MODULE_MCM);
  }

  @Override
  protected HashMap<ChannelType, ICANChannel[]> initChannels() {
    HashMap<ChannelType, ICANChannel[]> chMap = new HashMap<ChannelType, ICANChannel[]>();

    // Initialise read-only analogue output channels
    chMap.put(ChannelType.ANALOG_INPUT,
        createChannels(ChannelType.ANALOG_INPUT, MCM_CH_AINPUT.values(), true));

    // Initialise read-only digital output channels
    chMap.put(ChannelType.DIGITAL_INPUT,
        createChannels(ChannelType.DIGITAL_INPUT, MCM_CH_DINPUT.values(), true));

    return chMap;
  }

  @Override
  public IModuleSettings getSettings() {
    // TODO Auto-generated method stub
    return null;
  }
}