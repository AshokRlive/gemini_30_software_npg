/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.iomodule.manager;

import java.util.Collection;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.iomodule.manager.util.ChannelFinderExt;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


/**
 *
 */
public final class ChannelRepository extends AbstractConfigModule implements IConfigModule {
  
  public final static String CONFIG_MODULE_ID = "ChannelRepository";

  public ChannelRepository(IConfig owner) {
    super(owner);
  }
  
  
  public Collection<IChannel> getAllChannels(ChannelType... type) {
    return getAllChannels(null, type);
  }

  public Collection<IChannel> getAllChannels(MODULE[] mtypes,
      ChannelType... type) {
    ModuleRepository repo = getOwner().getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    Collection<Module> modules = (mtypes == null) ? repo.getAllModules() : repo.getModulesByType(mtypes);

    return ChannelFinderExt.findChannelInModules(modules, type);
  }

  public IChannel getChannel(MODULE mtype, MODULE_ID moduleID,
      ChannelType type, int channelID) {
    ModuleRepository repo = getOwner().getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    Module module =  repo.getModule(mtype, moduleID);
    if (module != null && module instanceof IOModule) {
      return ((IOModule)module).getChByTyeAndID(channelID, type);
    } else {
      log.error("Module \"" + mtype + moduleID + "\" NOT found:"+module);
      return null;
    }
  }
  
}

