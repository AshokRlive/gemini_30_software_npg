/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.canmodule.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.Inhibit;
import com.lucy.g3.rtu.config.module.canmodule.domain.stub.CANChannelStub;

public class InhibitTest {

  private ICANChannel[] validChannels;
  private String mask0, mask1, mask2, mask3, invalidMask;


  @Before
  public void setUp() throws Exception {
    validChannels = new ICANChannel[] {
        CANChannelStub.create(),// 0
        CANChannelStub.create(),// 1
        CANChannelStub.create(),// 2
        CANChannelStub.create(),// 3
        CANChannelStub.create() // 4
    };

    // 5 bits mask, valid
    mask0 = "00000";
    mask1 = "11111";
    mask2 = "01010";

    // 2 bits mask, valid
    mask3 = "10";

    // 6 bits mask, invalid
    invalidMask = "101101";
  }

  @Test
  public void testConstruct() {
    new Inhibit(validChannels);
  }

  @Test
  public void testConstructWithEmptyArg() {
    new Inhibit(new ICANChannel[0]);
    new Inhibit(new ICANChannel[Inhibit.MAX_BIT_NUMBER]);
  }

  @Test(expected = NullPointerException.class)
  public void testConstructWithNullArg() {
    new Inhibit(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructWithIllegalArg() {
    int illegalSize = Inhibit.MAX_BIT_NUMBER + 1;
    new Inhibit(new ICANChannel[illegalSize]);
  }

  @Test
  public void testInitialValue() {
    assertEquals(0, new Inhibit(validChannels).getInhibitMask());
  }

  @Test
  public void testModifyInhibitMask() {
    Inhibit inhibit = new Inhibit(validChannels);

    // Set mask 0
    Long maskValue = Long.parseLong(mask0, 2);
    inhibit.setInhibitMask(maskValue);
    assertEquals(maskValue.longValue(), inhibit.getInhibitMask());

    // Set mask 1
    maskValue = Long.parseLong(mask1, 2);
    inhibit.setInhibitMask(maskValue);
    assertEquals(maskValue.longValue(), inhibit.getInhibitMask());

    // Set mask 2
    maskValue = Long.parseLong(mask2, 2);
    inhibit.setInhibitMask(maskValue);
    assertEquals(maskValue.longValue(), inhibit.getInhibitMask());

    // Set mask 3
    maskValue = Long.parseLong(mask3, 2);
    inhibit.setInhibitMask(maskValue);
    assertEquals(maskValue.longValue(), inhibit.getInhibitMask());
  }

  @Test
  public void testModifyInhibitMaskWithInvalidValue() {
    Inhibit inhibit = new Inhibit(validChannels);

    // Set valid mask
    Long goodValue = Long.parseLong(mask0, 2);
    inhibit.setInhibitMask(goodValue);
    assertEquals(goodValue.longValue(), inhibit.getInhibitMask());

    // Set invalid mask should fail and has no effect
    Long badValue = Long.parseLong(invalidMask, 2);
    inhibit.setInhibitMask(badValue);

    // The mask value is not changed
    assertFalse(badValue.longValue() == inhibit.getInhibitMask());
    assertTrue(goodValue.longValue() == inhibit.getInhibitMask());
  }

  @Test
  public void testGetChannel() {
    Inhibit inhibit = new Inhibit(validChannels);
    for (int i = 0; i < validChannels.length; i++) {
      assertTrue(inhibit.getChannel(i) == validChannels[i]);
    }
    assertNull(inhibit.getChannel(validChannels.length)); // out of bounds

    inhibit = new Inhibit(new ICANChannel[10]);// empty channels
    for (int i = 0; i < 10; i++) {
      assertNull(inhibit.getChannel(i));
    }
  }

  @Test
  public void testBitNumEqualstoChannelNum() {
    int[] channelNum = { 0, 1, 3, 4, Inhibit.MAX_BIT_NUMBER };

    for (int i = 0; i < channelNum.length; i++) {
      Inhibit inhibit = new Inhibit(new ICANChannel[channelNum[i]]);
      assertEquals(channelNum[i], inhibit.getBitNum());
    }
  }

  @Test
  public void testGetBitmaskText() {
    Inhibit inhibit = new Inhibit(new ICANChannel[4]); // 4 bits

    String[][] mask = {
        // input expected
        { "1111", "1111" },
        { "1100", "1100" },
        { "0000", "0000" },
        { "0011", "0011" },
        { "0101", "0101" },
        { "1010", "1010" },
        { "111", "0111" },
        { "0", "0000" },
        { "01", "0001" },
        { "1", "0001" },
    };

    for (int i = 0; i < mask.length; i++) {
      inhibit.setInhibitMask(Long.parseLong(mask[i][0], 2));
      assertEquals("GetBitMaskText test fails at index: " + i,
          mask[i][1], inhibit.getBitmaskAsText());
    }
  }

  @Test
  public void testGetBitmaskText2() {
    final int size = Inhibit.MAX_BIT_NUMBER;
    Inhibit inhibit = new Inhibit(new ICANChannel[size]);
    String binaryText = inhibit.getBitmaskAsText();
    assertEquals(size, binaryText.length());
  }

  @Test
  public void testSetBit() {
    final int size = Inhibit.MAX_BIT_NUMBER;
    Inhibit inhibit = new Inhibit(new ICANChannel[size]);

    // init
    inhibit.setInhibitMask(0);
    assertEquals(0, inhibit.getInhibitMask());

    // Set first bit to 1
    inhibit.setBitValue(0, true);
    assertEquals(Long.parseLong("000001", 2), inhibit.getInhibitMask());
    inhibit.setBitValue(0, true);// double check
    assertEquals(Long.parseLong("000001", 2), inhibit.getInhibitMask());

    // Set 5th bit to 1
    inhibit.setBitValue(5, true);
    assertEquals(Long.parseLong("100001", 2), inhibit.getInhibitMask());
    inhibit.setBitValue(5, true); // double check
    assertEquals(Long.parseLong("100001", 2), inhibit.getInhibitMask());

    // Set 3rd bit to 1
    inhibit.setBitValue(3, true);
    inhibit.setBitValue(3, true);
    assertEquals(Long.parseLong("101001", 2), inhibit.getInhibitMask());

    // Set t5h bit to 0
    inhibit.setBitValue(5, false);
    inhibit.setBitValue(5, false);
    assertEquals(Long.parseLong("001001", 2), inhibit.getInhibitMask());

    // Set last bit to 1
    inhibit.setBitValue(size - 1, true);
  }

  @Test
  public void testGetBit() {
    final int size = Inhibit.MAX_BIT_NUMBER;
    Inhibit inhibit = new Inhibit(new ICANChannel[size]);

    int[] bitIndex = { 0, 1, 2, 10, 29, 28, size - 1 };

    for (int i = 0; i < bitIndex.length; i++) {
      // Set true
      inhibit.setBitValue(bitIndex[i], true);
      assertTrue("Bit value at index:" + i + " is supposed to be 1", inhibit.getBitValue(bitIndex[i]));

      // Set false
      inhibit.setBitValue(bitIndex[i], false);
      assertFalse("Bit value at index:" + i + " is supposed to be 0", inhibit.getBitValue(bitIndex[i]));
    }
  }

}
