/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.constants;

/**
 * The SwitchIndex enum for switch module.
 */
public enum SwitchIndex {
  SwitchA("A"), SwitchB("B");

  SwitchIndex(String name) {
    this.name = name;
  }


  private final String name;


  @Override
  public String toString() {
    return getName();
  }

  public String getName() {
    return name;
  }

  public static SwitchIndex forValue(int ordinal) {
    SwitchIndex[] values = SwitchIndex.values();
    if (ordinal >= 0 && ordinal < values.length) {
      return values[ordinal];
    } else {
      return null;
    }
  }
}
