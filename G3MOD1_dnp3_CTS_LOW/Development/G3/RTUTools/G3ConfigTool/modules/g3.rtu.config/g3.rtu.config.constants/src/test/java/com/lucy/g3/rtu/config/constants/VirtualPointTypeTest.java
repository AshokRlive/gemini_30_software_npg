/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.constants;

import static org.junit.Assert.assertFalse;

import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;

public class VirtualPointTypeTest {

  @Test
  public void testNameNotBlank() {
    VirtualPointType[] types = VirtualPointType.values();
    for (int i = 0; i < types.length; i++) {
      assertFalse(isBlank(types[i].getName()));
    }
  }

  @Test
  public void testDescriptionNotBlank() {
    VirtualPointType[] types = VirtualPointType.values();
    for (int i = 0; i < types.length; i++) {
      assertFalse(isBlank(types[i].getDescription()));
    }
  }
  
  private boolean isBlank(String str){
    return str == null || str.trim().isEmpty();
  }
}
