/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.constants;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

/**
 * <p>
 * This enumeration lists all supported virtual types. Each type has the
 * following read-only properties: "description", "name","isPseudo" and
 * "isAnalogue".
 * </p>
 * <b>Note:</b> it is required to update .properties file if adding/removing any
 * enumeration item.
 * 
 */
public enum VirtualPointType {
  ANALOGUE_INPUT,
  BINARY_INPUT,
  DOUBLE_BINARY_INPUT,
  COUNTER;

  VirtualPointType() {
    /* Load resource */
    ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(VirtualPointType.class);

    /* Initialise description from external resource file */
    String key = super.name();

    description = resourceMap.getString(key + ".description");
    name = resourceMap.getString(key + ".name");
  }

  public boolean isAnalogue() {
    return this == ANALOGUE_INPUT;
  }

  public boolean isCounter() {
    return this == COUNTER;
  }

  public boolean isDigital() {
    return this == BINARY_INPUT || this == DOUBLE_BINARY_INPUT;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return getName();
  }

  private final String name;

  private final String description;

  private static final VirtualPointType[] BINARY_TYPES = { BINARY_INPUT /*
                                                                         * ,
                                                                         * PSEUDO_BINARY_POINT
                                                                         */};
  private static final VirtualPointType[] DOUBLE_BINARY_TYPES = { DOUBLE_BINARY_INPUT /*
                                                                                       * ,
                                                                                       * PSEUDO_DOUBLE_BINARY_POINT
                                                                                       */};
  private static final VirtualPointType[] ANALOG_TYPES = { ANALOGUE_INPUT /*
                                                                           * ,
                                                                           * PSEUDO_ANALOGUE_POINT
                                                                           */};


  public static VirtualPointType[] binaryTypes() {
    return Arrays.copyOf(BINARY_TYPES, BINARY_TYPES.length);
  }

  public static VirtualPointType[] doubleBinaryTypes() {
    return Arrays.copyOf(DOUBLE_BINARY_TYPES, DOUBLE_BINARY_TYPES.length);
  }

  public static VirtualPointType[] analoygTypes() {
    return Arrays.copyOf(ANALOG_TYPES, ANALOG_TYPES.length);
  }
  
  public static VirtualPointType[] digitalTypes() {
    return new VirtualPointType[]{BINARY_INPUT, DOUBLE_BINARY_INPUT, COUNTER};
  }

}
