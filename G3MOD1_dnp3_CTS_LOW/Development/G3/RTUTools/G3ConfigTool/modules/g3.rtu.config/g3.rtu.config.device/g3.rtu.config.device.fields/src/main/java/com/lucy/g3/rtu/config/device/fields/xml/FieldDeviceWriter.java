/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.xml;

import java.util.Collection;

import com.g3schema.FieldDevicesT;
import com.g3schema.ns_mmb.MMBChnlT;
import com.g3schema.ns_mmb.ModbusMasterT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;

/**
 * The XML writer for Field Device.
 */
public class FieldDeviceWriter {

  private final FieldDeviceManager devMgr;
  private ValidationResult result;


  public FieldDeviceWriter(FieldDeviceManager manager) {
    this.devMgr = manager;
  }

  public void write(FieldDevicesT xml, ValidationResult result) throws InvalidConfException {
    this.result = result;
    MasterProtocolManager masterProtos = devMgr.getMasterProtoManager();
    Collection<IMasterProtocol<?>> protos = masterProtos.getAllItems();
    for (IMasterProtocol<?> proto : protos) {
      if (proto instanceof MMB) {
        MMB mbm = (MMB) proto;

        ModbusMasterT xml_modbus;
        if (xml.ModbusMaster.exists()) {
          xml_modbus = xml.ModbusMaster.first();
        } else {
          xml_modbus = xml.ModbusMaster.append();
        }

        // Write all channels
        Collection<MMBChannel> channels = mbm.getAllChannels();
        for (MMBChannel ch : channels) {
          MMBChnlT xml_channel = xml_modbus.channel.append();
          ch.getChannelConfig().writeToXML(xml_channel.config.append(), result);

          // Write all sessions
          Collection<MMBSession> sessions = ch.getAllSessions();
          for (MMBSession session : sessions) {
            session.writeToXML(xml_channel.session.append());
          }
        }

      }
    }
  }
}
