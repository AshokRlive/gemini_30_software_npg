/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of ModBus Field Device.
 */
public final class MMBDevice extends AbstractFieldDevice {

  private final MMBSession session;


  MMBDevice(MODULE_ID id, MMBSession session) {
    super(id, MODULE.MODULE_MBDEVICE);
    this.session = Preconditions.checkNotNull(session, "session is null");
    SessionPCL pcl = new SessionPCL();
    session.addPropertyChangeListener(MMBSession.PROPERTY_DEVICE_NAME, pcl);
    session.addPropertyChangeListener(MMBSession.PROPERTY_DEVICE_MODEL, pcl);
  }

  /**
   * Override to customise the short name of ModBus device.
   */
  @Override
  protected String createShortName() {
    String newShortName;

    String devName = getDeviceName();
    if (Strings.isBlank(devName)) {
      newShortName = super.createShortName();
    } else {
      String devModel = getDeviceModel();
      if (!Strings.isBlank(devModel)) {
        newShortName = String.format("[%s] %s", devModel, devName);
      } else {
        newShortName = devName;
      }
    }

    return newShortName;
  }

  @Override
  public MMBIoMap getIomap() {
    return session.getIomap();
  }

  @Override
  public ChannelType[] getAvailableChannelTypes() {
    return session.getIomap().getAvailableChannelTypes();
  }

  @Override
  public List<MMBIoChannel> getAllChannels() {
    return session.getIomap().getAllChannels();
  }

  @Override
  public MMBIoChannel[] getChannels(ChannelType type) {
    return session.getIomap().getChannels(type);
  }

  @Override
  public IChannel getChByEnum(IChannelEnum channelEnum) {
    return null;
  }

  public MMBChannel getMBMChannel() {
    return getMBMSession().getProtocolChannel();
  }

  public MMBSession getMBMSession() {
    return session;
  }

  @Override
  public IProtocolChannel<? extends IMasterProtocolSession> getChannel() {
    return getMBMChannel();
  }

  @Override
  public IMasterProtocolSession getSession() {
    return getMBMSession();
  }

  @Override
  public String getDeviceName() {
    return session == null ? null : session.getDeviceName();
  }

  @Override
  public String getDeviceModel() {
    return session.getDeviceModel();
  }

  @Override
  public void setDeviceName(String description) {
    session.setDeviceName(description);
  }


  private class SessionPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateNames();
    }
  }

  @Override
  public IModuleSettings getSettings() {
    return null;
  }
}
