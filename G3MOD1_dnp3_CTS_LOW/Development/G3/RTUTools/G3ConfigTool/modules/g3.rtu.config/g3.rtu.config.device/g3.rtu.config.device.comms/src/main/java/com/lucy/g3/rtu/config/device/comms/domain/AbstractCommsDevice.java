/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import java.beans.PropertyVetoException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.g3schema.ns_comms.BaseModemT;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.AbstractSerialPortUser;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The basic implementation of Modem.
 */
public abstract class AbstractCommsDevice extends AbstractSerialPortUser implements ICommsDevice {

  private Logger log = Logger.getLogger(AbstractCommsDevice.class);

  private final CommsDeviceType deviceType;

  private int deviceId;

  private String deviceName = "";
  
  private ArrayList<ICommsDeviceUser> users = new ArrayList<>();
  
  private CommsDeviceManager manager;


  AbstractCommsDevice(CommsDeviceType deviceType, int deviceId) {
    this.deviceType = Preconditions.checkNotNull(deviceType, "deviceType must not be null");
    this.deviceId = deviceId;
  }

  @Override
  public void copyConfigFrom(ICommsDevice device) {
    // Unsupported
  }

  @Override
  public final String getDeviceName() {
    return deviceName;
  }

  @Override
  public void setDeviceId(int deviceId) {
    Object oldValue = this.deviceId;
    this.deviceId = deviceId;
    firePropertyChange(PROPERTY_DEVICE_ID, oldValue, deviceId);
  }

  @Override
  public final void setDeviceName(String deviceName) {
    Object oldValue = this.deviceName;
    this.deviceName = deviceName;
    firePropertyChange(PROPERTY_DEVICE_NAME, oldValue, deviceName);
  }

  @Override
  public final int getDeviceId() {
    return deviceId;
  }

  @Override
  public final CommsDeviceType getDeviceType() {
    return deviceType;
  }

  @Override
  public final String getPortUserName() {
    return getDisplayName();
  }

  @Override
  public final String getDisplayName() {
    return String.format("[%s] %s", getDeviceType().getDescription(), getDeviceName());
  }

  @Override
  public final String toString() {
    return getDisplayName();
  }

  @Override
  public void delete() {
    try {
      setSerialPort(null);
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    
    firePropertyChange(PROPERTY_DEVICE_DELETE, false, true);
  }

  boolean checkInUse() {
    return !users.isEmpty();
  }
  
  @Override
  public void addUser(ICommsDeviceUser user) {
    users.add(user);
  }
  @Override
  public void removeUser(ICommsDeviceUser user) {
    users.remove(user);
  }
  
  void writeToXML(BaseModemT xml) {
    xml.name.setValue(getDeviceName());
    xml.id.setValue(getDeviceId());
    
    // Disable this comms device if it is not in use.
    if(!checkInUse()) {
      xml.disabled.setValue(true);
    }

    ISerialPort port = getSerialPort();
    if (port != null) {
      xml.serialPortName.setValue(port.getPortName());
    } else {
      xml.serialPortName.setValue("");
    }
  }

  void readFromXML(BaseModemT xml, PortsManager portsManager) {
    setDeviceName(xml.name.getValue());
    setDeviceId((int) xml.id.getValue());

    if(portsManager !=  null) {
      ISerialPort port = portsManager.getSerialPort(xml.serialPortName.getValue());
      try {
        setSerialPort(port);
      } catch (PropertyVetoException e) {
        log.error("Failed to read Port from XMl", e);
      }
    }
  }

  
  @Override
  public CommsDeviceManager getManager() {
    return manager;
  }

  
  void setManager(CommsDeviceManager manager) {
    this.manager = manager;
  }

  @Override
  public IValidator getValidator() {
    if(validator == null)
      validator = new CommsDeviceValidator(this);
    
    return validator;
  }
  
  private CommsDeviceValidator validator;
  
  static class CommsDeviceValidator extends AbstractValidator<AbstractCommsDevice> {

    public CommsDeviceValidator(AbstractCommsDevice target) {
      super(target);
    }

    @Override
    public String getTargetName() {
      return target.getDisplayName();
    }

    @Override
    protected void validate(ValidationResultExt result) {
      if(target.getSerialPort() == null) {
        result.addError("The serial port is not selected!");
      }
      
    }
    
  }
}
