/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.rtu.config.device.shared.IDevice;
import com.lucy.g3.rtu.config.port.serial.ISerialPortUser;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The Interface of comms device.
 */
public interface ICommsDevice extends IDevice, ISerialPortUser, ObservableBean2, IValidation {
  String PROPERTY_DEVICE_TYPE = "deviceType";
  
  String PROPERTY_DEVICE_ID = "deviceId";

  
  void setDeviceId(int deviceId);

  int getDeviceId();

  public CommsDeviceType getDeviceType();

  String getDisplayName();
  
  void copyConfigFrom(ICommsDevice device);

  void addUser(ICommsDeviceUser user);

  void removeUser(ICommsDeviceUser user);

  CommsDeviceManager getManager();
}
