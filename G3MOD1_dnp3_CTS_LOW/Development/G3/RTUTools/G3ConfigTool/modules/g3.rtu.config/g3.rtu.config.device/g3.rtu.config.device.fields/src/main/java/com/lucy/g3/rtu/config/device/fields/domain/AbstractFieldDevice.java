/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.domain;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.device.shared.IDevice;
import com.lucy.g3.rtu.config.module.iomodule.domain.AbstractIOModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * A basic implementation of {@linkplain IDevice}.
 */
public abstract class AbstractFieldDevice extends AbstractIOModule implements IFieldDevice {
  private String deviceName; 
  public AbstractFieldDevice(MODULE_ID id, MODULE type) {
    super(id, type);
  }

  @Override
  public final MODULE_ID getDeviceId() {
    return getId();
  }

  @Override
  public final void setDeviceId(MODULE_ID id) {
    Preconditions.checkNotNull(id, "module id is null");
    super.modifyId(id);
  }

  @Override
  public String getDeviceName() {
    return deviceName;
  }

  @Override
  public void setDeviceName(String deviceName) {
    Object oldValue = getDeviceName();
    this.deviceName = deviceName;
    firePropertyChange(PROPERTY_DEVICE_NAME, oldValue, deviceName);
  }

}
