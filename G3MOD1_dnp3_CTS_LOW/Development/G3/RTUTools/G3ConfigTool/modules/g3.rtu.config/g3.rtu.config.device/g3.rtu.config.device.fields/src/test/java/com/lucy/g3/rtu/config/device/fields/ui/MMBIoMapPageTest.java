/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui;

import org.junit.After;
import org.junit.Before;

public class MMBIoMapPageTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  // @Ignore// Manual test
  // @Test
  // public void testGUI() throws InterruptedException {
  // MMBSession sesn = new MMBChannel(new MMB()).addSession("");
  // MMBIoMap iomap = sesn.getIomap();
  // MMBDevice dev = (MMBDevice) sesn.getDevice();
  // iomap.addChannel(new MMBIOChannel(ChannelType.ANALOG_INPUT, 0,dev,
  // "channel 0"));
  // FieldDeviceIoMapPage page = new FieldDeviceIoMapPage(iomap);
  // TestUtil.showFrame(page.getContent());
  // Thread.sleep(300000);
  // }

}
