
package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.configtool.config.template.ITemplate;
import com.lucy.g3.configtool.config.template.TemplateRepositories;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolChannel;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.xml.gen.common.ModBusEnum.LU_MBLINK_TYPE;

/**
 * 
 * A wizard for adding a field device. It provides the following sequence of pages:
 * <pre>
 *                                                        /-CreateNew-ChooseSerialPort
 *                                                       /
 *                  /-SelectModbusTemplate-SelectChannel--SelectExisting
 *                 / 
 *  Select Protocol  -SelectDNP3Tmplate
 *                 \                      
 *                  \-Select103Template
 *                                        
 * </pre>
 */
public class FieldDeviceAddingWizard {
    
  private static final String WIZARD_TITLE = "Add New Field Device";

  private static MasterProtocolManager manager;
  private static PortsManager ports;
  private static WizardResultProducer finisher; 
  
  public static IFieldDevice showWizard(MasterProtocolManager _manager, PortsManager _ports) {
    return (IFieldDevice) showWizard(_manager, _ports, null);
  }
  
  static Object showWizard(MasterProtocolManager _manager, PortsManager _ports, WizardResultProducer _finisher) {
    manager = _manager;
    ports = _ports;
    finisher = _finisher == null ? new FieldDeviceAddingResult(manager) : _finisher;
    
    
    Wizard wiz = new MainBrancher().createWizard();
    Object result = wiz.show(wiz, WizardUtils.createRect(WindowUtils.getMainFrame(), 500, 300));
    
    manager = null;
    ports = null;
    
    return result;
  }
  
  private static class MainBrancher extends WizardBranchController {
    private final HashMap<ProtocolType, Wizard> subWizards = new HashMap<>();
    
    private MainBrancher() {
      super(WIZARD_TITLE,
          new WizardPage[]{
          new SelectProtocolPage(getProtocolTypes())
      });
    }

    @Override
    protected Wizard getWizardForStep(String step, Map settings) {
      if(SelectProtocolPage.ID.equals(step)) {
        ProtocolType type = (ProtocolType) settings.get(SelectProtocolPage.KEY_SELECTED_PROTOCOL_TYPE);
        if(type == null)
          return null;
        Wizard wiz = subWizards.get(type);
        
        if(wiz == null) {
          wiz = new SubBrancher(type).createWizard();
          subWizards.put(type, wiz);
        }
        
        return wiz;
      }
      
      return null;
    }
  }
  
  private static class SubBrancher extends WizardBranchController {
    private ProtocolType type;
    
    protected SubBrancher(ProtocolType type) {
      super(new WizardPage[]{
          new SelectTemplatePage(getAvailabelTemplates(type)),
          new SelectChannelPage(
              getAvailabelChannels(type), 
              getAvailabelChannelTypes(type))
      });
      
      this.type = type;
    }

    @Override
    protected Wizard getWizardForStep(String step, Map settings) {
      Wizard wiz = null;
      
      if(SelectChannelPage.ID.equals(step)) {
        IMasterProtocolChannel<?> ch = (IMasterProtocolChannel<?>) settings.get(SelectChannelPage.KEY_SELECTED_CHANNEL);
        Object chType = settings.get(SelectChannelPage.KEY_CHANNEL_TYPE);
        switch (type) {
        case MMB:
          wiz = getChannelWizard((LU_MBLINK_TYPE) chType, ch);
          break;
          
        default:
          throw new UnsupportedOperationException("Not implemented for:"+type);
        }
      }
      
      return wiz;
    }

    private Wizard getChannelWizard(LU_MBLINK_TYPE chType, IMasterProtocolChannel<?> channel) {
      if(chType == LU_MBLINK_TYPE.LU_MBLINK_TYPE_TCP) {
        return WizardPage.createWizard(new WizardPage[]{
            new ConfigureTcpPage(channel)
          }, finisher);
        
      } else {
         return WizardPage.createWizard(new WizardPage[]{
              new SelectSerialPortPage(channel, ports.getSerialManager().getAllItems())
          }, finisher);
        }
    }
  }

  private static Object[] getAvailabelChannelTypes(ProtocolType type) {
    switch (type) {
//    case M103:
//    return new String[]{"TCP/IP"};
//    
//    case MDNP3:
//      return DNPLINK_NETWORK_TYPE.values();
      
    case MMB:
      return LU_MBLINK_TYPE.values();
      
    default:
      throw new IllegalArgumentException("Unsupported type: " + type);
    }
  }

  private static IMasterProtocolChannel<?>[] getAvailabelChannels(ProtocolType type) {
    ArrayList<IMasterProtocolChannel<?>> channelList = new ArrayList<>();
    
    IMasterProtocol<?> protocol = manager.getProtocolByType(type);
    Collection<?> allChnls = protocol == null ? null : protocol.getAllChannels();
    
    if(allChnls != null) {
      for (Object ch : allChnls) {
        if(ch != null && ch instanceof IMasterProtocolChannel)
        channelList.add((IMasterProtocolChannel<?>) ch);
      }
    }
    
    return channelList.toArray(new IMasterProtocolChannel<?>[channelList.size()]);
  }

  private static ITemplate[] getAvailabelTemplates(ProtocolType type) {
    switch (type) {
    case MMB:
      ITemplate[] builtin = TemplateRepositories.getAvailabelTemplates(manager.getOwner(), TemplateRepositories.KEY_MMB_BUILTIN);
      ITemplate[] custom = TemplateRepositories.getAvailabelTemplates(manager.getOwner(), TemplateRepositories.KEY_MMB_CUSTOM);
      return ArrayUtils.addAll(builtin, custom);
      
      // TODO to be adding ...
//    case M103:
//    case MDNP3:
      
    default:
      break;
    }
    return new ITemplate[0]; 
  }
  
  private static ProtocolType[] getProtocolTypes() {
    return ProtocolType.getAllMasterProtocolTypes();
  }
}

