/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.port.ui.common.SerialPortSelectPanel;

public class PaknetPage extends AbstractCommsDevicePage {

  private final PresentationModel<PAKNETModem> pm;


  public PaknetPage(PAKNETModem paknet) {
    super(paknet);
    this.pm = new PresentationModel<>(paknet);
  }

  @Override
  protected void init() throws Exception {
    initComponents();
  }
  
  private void createUIComponents() {
    panelSerial = new SerialPortSelectPanel(pm.getModel(PAKNETModem.PROPERTY_SERIAL_PORT), getPortList());
    
    tfName = BasicComponentFactory.createTextField(pm.getModel(PAKNETModem.PROPERTY_DEVICE_NAME),false);
    tfId = BasicComponentFactory.createTextField(convertToString(pm.getModel(PAKNETModem.PROPERTY_DEVICE_ID)),false);
    tfType = BasicComponentFactory.createTextField(convertToString(pm.getModel(PAKNETModem.PROPERTY_DEVICE_TYPE)),false);
    tfMainNUA = BasicComponentFactory.createTextField(pm.getModel(PAKNETModem.PROPERTY_MAIN_NUA),false);
    tfStandbyNUA = BasicComponentFactory.createTextField(pm.getModel(PAKNETModem.PROPERTY_STANDBY_NUA),false);
    tfConnectTimeout = BasicComponentFactory.createLongField(pm.getModel(PAKNETModem.PROPERTY_CONNECTT_IMEOUT));
    tfRegularCallInterval = BasicComponentFactory.createLongField(pm.getModel(PAKNETModem.PROPERTY_REGULAR_CALL_INTERVAL));
    tfModemInactivityTimeout = BasicComponentFactory.createLongField(pm.getModel(PAKNETModem.PROPERTY_MODEM_INACTIVITY_TIMEOUT));
    tfConnInactivityTimeout = BasicComponentFactory.createLongField(pm.getModel(PAKNETModem.PROPERTY_CONNECTION_INACTIVITYT_IMEOUT));
    tfRetryDelays = BasicComponentFactory.createTextField(pm.getModel(PAKNETModem.PROPERTY_DIAL_RETRY_DELAYS),false);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    panel4 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    panel2 = new JPanel();
    label4 = new JLabel();
    label5 = new JLabel();
    panel3 = new JPanel();
    label6 = new JLabel();
    label11 = new JLabel();
    label7 = new JLabel();
    label12 = new JLabel();
    label8 = new JLabel();
    label13 = new JLabel();
    label9 = new JLabel();
    label14 = new JLabel();
    label10 = new JLabel();
    label15 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane1 ========
      {
        scrollPane1.setBorder(null);

        //======== panel1 ========
        {
          panel1.setBorder(Borders.DIALOG_BORDER);
          panel1.setLayout(new FormLayout(
              "[100dlu,default]",
              "3*(default, $ugap), default"));

          //======== panel4 ========
          {
            panel4.setBorder(new CompoundBorder(
                new TitledBorder("Basic"),
                Borders.DLU2_BORDER));
            panel4.setLayout(new FormLayout(
                "[100dlu,default], $lcgap, [80dlu,default]",
                "2*(default, $lgap), default"));

            //---- label1 ----
            label1.setText("Name:");
            panel4.add(label1, CC.xy(1, 1));
            panel4.add(tfName, CC.xy(3, 1));

            //---- label2 ----
            label2.setText("Device ID:");
            panel4.add(label2, CC.xy(1, 3));

            //---- tfId ----
            tfId.setEditable(false);
            tfId.setEnabled(false);
            panel4.add(tfId, CC.xy(3, 3));

            //---- label3 ----
            label3.setText("Device Type:");
            panel4.add(label3, CC.xy(1, 5));

            //---- tfType ----
            tfType.setEditable(false);
            tfType.setEnabled(false);
            panel4.add(tfType, CC.xy(3, 5));
          }
          panel1.add(panel4, CC.xy(1, 1));

          //======== panel2 ========
          {
            panel2.setBorder(new CompoundBorder(
                new TitledBorder("NUA"),
                Borders.DLU2_BORDER));
            panel2.setLayout(new FormLayout(
                "[100dlu,default], $lcgap, [80dlu,default]",
                "default, $lgap, default"));

            //---- label4 ----
            label4.setText("Main NUA:");
            panel2.add(label4, CC.xy(1, 1));
            panel2.add(tfMainNUA, CC.xy(3, 1));

            //---- label5 ----
            label5.setText("Backup NUA:");
            panel2.add(label5, CC.xy(1, 3));
            panel2.add(tfStandbyNUA, CC.xy(3, 3));
          }
          panel1.add(panel2, CC.xy(1, 3));

          //======== panel3 ========
          {
            panel3.setBorder(new CompoundBorder(
                new TitledBorder("Timeout"),
                Borders.DLU2_BORDER));
            panel3.setLayout(new FormLayout(
                "[100dlu,default], $lcgap, [80dlu,default], $lcgap, default",
                "4*(default, $lgap), default"));

            //---- label6 ----
            label6.setText("Connect Timeout:");
            panel3.add(label6, CC.xy(1, 1));
            panel3.add(tfConnectTimeout, CC.xy(3, 1));

            //---- label11 ----
            label11.setText("millisecs");
            panel3.add(label11, CC.xy(5, 1));

            //---- label7 ----
            label7.setText("Regular Call Interval:");
            panel3.add(label7, CC.xy(1, 3));
            panel3.add(tfRegularCallInterval, CC.xy(3, 3));

            //---- label12 ----
            label12.setText("minutes");
            panel3.add(label12, CC.xy(5, 3));

            //---- label8 ----
            label8.setText("Modem Inactivity Timeout:");
            panel3.add(label8, CC.xy(1, 5));
            panel3.add(tfModemInactivityTimeout, CC.xy(3, 5));

            //---- label13 ----
            label13.setText("mintues");
            panel3.add(label13, CC.xy(5, 5));

            //---- label9 ----
            label9.setText("Connection Inactivity Timeout:");
            panel3.add(label9, CC.xy(1, 7));
            panel3.add(tfConnInactivityTimeout, CC.xy(3, 7));

            //---- label14 ----
            label14.setText("seconds");
            panel3.add(label14, CC.xy(5, 7));

            //---- label10 ----
            label10.setText("Dialling Retry Delays:");
            panel3.add(label10, CC.xy(1, 9));
            panel3.add(tfRetryDelays, CC.xy(3, 9));

            //---- label15 ----
            label15.setText("mintues");
            panel3.add(label15, CC.xy(5, 9));
          }
          panel1.add(panel3, CC.xy(1, 5));

          //---- panelSerial ----
          panelSerial.setBorder(new CompoundBorder(
              new TitledBorder("Port"),
              Borders.DLU2_BORDER));
          panel1.add(panelSerial, CC.xy(1, 7));
        }
        scrollPane1.setViewportView(panel1);
      }
      tabbedPane1.addTab("Configuration", scrollPane1);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JScrollPane scrollPane1;
  private JPanel panel1;
  private JPanel panel4;
  private JLabel label1;
  private JTextField tfName;
  private JLabel label2;
  private JTextField tfId;
  private JLabel label3;
  private JTextField tfType;
  private JPanel panel2;
  private JLabel label4;
  private JTextField tfMainNUA;
  private JLabel label5;
  private JTextField tfStandbyNUA;
  private JPanel panel3;
  private JLabel label6;
  private JTextField tfConnectTimeout;
  private JLabel label11;
  private JLabel label7;
  private JTextField tfRegularCallInterval;
  private JLabel label12;
  private JLabel label8;
  private JTextField tfModemInactivityTimeout;
  private JLabel label13;
  private JLabel label9;
  private JTextField tfConnInactivityTimeout;
  private JLabel label14;
  private JLabel label10;
  private JTextField tfRetryDelays;
  private JLabel label15;
  private SerialPortSelectPanel panelSerial;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
