/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import java.beans.PropertyVetoException;

public interface ICommsDeviceUser {

  public static final String PROPERTY_COMMS_DEVICE = "commsDevice";


  ICommsDevice getCommsDevice();

  void setCommsDevice(ICommsDevice commsDevice) throws PropertyVetoException;

//  String getCommsDeviceName();

}
