/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.shared;

/**
 * Interface for the device that is connected to G3 RTU.
 */
public interface IDevice {

  /**
   * The name of property {@value} . Type: <code>String</code>.
   */
  String PROPERTY_DEVICE_NAME = "deviceName";

  String PROPERTY_DEVICE_DELETE = "delete";
  
  String getDeviceName();

  void setDeviceName(String description);

  void delete();

}
