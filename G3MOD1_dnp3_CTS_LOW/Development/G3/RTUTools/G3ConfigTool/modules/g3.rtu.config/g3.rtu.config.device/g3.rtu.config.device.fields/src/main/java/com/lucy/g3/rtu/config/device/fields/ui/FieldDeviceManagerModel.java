/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice.FieldDeviceAddingWizard;
import com.lucy.g3.rtu.config.device.shared.DeviceManagerPanel;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractItemManagerModel;

/**
 * Presentation model of field device manager.
 */
class FieldDeviceManagerModel extends AbstractItemManagerModel {

  private final FieldDeviceManager manager;
  private final SelectionInList<IFieldDevice> selectionInList;
  private PortsManager portsman;
  private AbstractTableAdapter<IFieldDevice> fieldDevTableModel;


  public FieldDeviceManagerModel(FieldDeviceManager manager, PortsManager portsman) {
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
    selectionInList = new SelectionInList<IFieldDevice>(manager.getItemListModel());
    this.portsman = portsman;
  }

  public JPanel createPanel(Action viewDeviceAction) {
    return new DeviceManagerPanel(getSelectionInList(),
        getTableModel(),
        getPageActions(), viewDeviceAction);
  }

  @Override
  public SelectionInList<IFieldDevice> getSelectionInList() {
    return selectionInList;
  }

  public AbstractTableAdapter<IFieldDevice> getTableModel() {
    if (fieldDevTableModel == null) {
      @SuppressWarnings("unchecked")
      ListModel<IFieldDevice> lm = selectionInList.getListModel();
      fieldDevTableModel = new FieldDeviceTableModel(lm);
    }

    return fieldDevTableModel;
  }

  @Override
  protected void addActionPerformed() {
    MasterProtocolManager pman = manager.getMasterProtoManager();
    // STRINGTASK
//    /Supplier.supply("com.lucy.generator.wizard.wizards.AddFieldDevice", pman, portsman);
    IFieldDevice result = FieldDeviceAddingWizard.showWizard(manager.getMasterProtoManager(), portsman);
    if(result != null)
      selectionInList.setSelection(result);
  }

  @Override
  protected void removeActionPerformed() {
    IFieldDevice sel = selectionInList.getSelection();

    if (sel != null) {
      if (showConfirmRemoveDialog(sel.getDeviceName())) {
        // Remove session from channel
        IMasterProtocolSession sesn = sel.getSession();
        @SuppressWarnings("unchecked")
        IProtocolChannel<IMasterProtocolSession> ch = (IProtocolChannel<IMasterProtocolSession>) sel.getChannel();
        ch.remove(sesn);
      }
    }
  }


  private static class FieldDeviceTableModel extends AbstractTableAdapter<IFieldDevice> {

    private static final String[] COLUMN_NAMES = new String[] {
        "Device ID", "Device Name", "Protocol", "Channel",
    };


    public FieldDeviceTableModel(ListModel<IFieldDevice> fieldDeviceListModel) {
      super(fieldDeviceListModel, COLUMN_NAMES);

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      IFieldDevice device = getRow(rowIndex);
      if (device == null) {
        return null;
      }

      IProtocolChannel<? extends IMasterProtocolSession> chnl = device.getChannel();

      switch (columnIndex) {
      case 0:
        return device.getDeviceId();

      case 1:
        return device.getDeviceName();

      case 2:
        return chnl.getProtocol().getProtocolName();

      case 3:
        return chnl.getChannelConfig().getChannelConfSummaryText();

      default:
        break;
      }
      return null;
    }

  }
}
