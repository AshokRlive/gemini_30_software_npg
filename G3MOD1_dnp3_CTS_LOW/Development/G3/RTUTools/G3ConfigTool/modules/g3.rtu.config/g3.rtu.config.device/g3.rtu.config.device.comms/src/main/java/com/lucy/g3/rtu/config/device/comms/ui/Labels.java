/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui;

/******************************************************************************
 * UI interface strings
 *****************************************************************************/
public interface Labels {

  /* Common units */
  public static final String MINUTES = "minutes";
  public static final String SECONDS = "seconds";
  public static final String MILLISECS = "millisecs";
  public static final String DIG14 = "(14 digit)";

  /* generator.model.data.enums.PortType */
  public static final String PTENUM01 = "Not selected";

  /* generator.view.tree.config.cdevices.cdevice.GprsView (modem) */
  public static final String GMVIEW01 = "Configuration";
  public static final String GMVIEW02 = "Name";
  public static final String GMVIEW03 = "Device ID";
  public static final String GMVIEW04 = "Device Type";
  public static final String GMVIEW05 = "Port";
  public static final String GMVIEW06 = "APN Host";
  public static final String GMVIEW07 = "User name";
  public static final String GMVIEW08 = "Password";
  public static final String GMVIEW09 = "PPP Script";
  public static final String GMVIEW10 = "PPP script path";
  public static final String GMVIEW11 = "PPP Script";
  public static final String GMVIEW12 = "Chat Script";
  public static final String GMVIEW13 = "Chat script path:";
  public static final String GMVIEW14 = "Chat Script";
  public static final String GMVIEW15 = "Unable to set the required port, it is either in use or unavailable.";
  public static final String GMVIEW16 = "Unable to read the selected script file - %1$s";

  /* generator.view.tree.config.cdevices.cdevice.ModemView (standard/PSTN) */
  public static final String SMVIEW01 = "Configuration";
  public static final String SMVIEW02 = "Name";
  public static final String SMVIEW03 = "Device ID";
  public static final String SMVIEW04 = "Device Type";
  public static final String SMVIEW05 = "Port";
  public static final String SMVIEW06 = "init String";
  public static final String SMVIEW07 = "dial String";
  public static final String SMVIEW08 = "connect String";
  public static final String SMVIEW09 = "hangup String";
  public static final String SMVIEW10 = "prompt String";
  public static final String SMVIEW11 = "Unable to set the required port, it is either in use or unavailable.";
  public static final String SMVIEW12 = "Secondary dial String";

  /* generator.view.tree.config.cdevices.cdevice.PaknetView */
  public static final String PNVIEW01 = "Configuration";
  public static final String PNVIEW02 = "Name";
  public static final String PNVIEW03 = "Device ID";
  public static final String PNVIEW04 = "Device Type";
  public static final String PNVIEW05 = "Port";
  public static final String PNVIEW06 = "init String";
  public static final String PNVIEW07 = "Main NUA";
  public static final String PNVIEW08 = "Standby NUA";
  public static final String PNVIEW09 = "connect String";
  public static final String PNVIEW10 = "hangup String";
  public static final String PNVIEW11 = "prompt String";
  public static final String PNVIEW12 = "Connection Timeout";
  public static final String PNVIEW13 = "Unable to set the required port, it is either in use or unavailable.";
  public static final String PNVIEW14 = "Connect Timeout";
  public static final String PNVIEW15 = "Regular Call Interval";
  public static final String PNVIEW16 = "Modem Inactivity Timeout";
  public static final String PNVIEW17 = "Connection Inactivity Timeout";
  public static final String PNVIEW18 = "Dialling Retry Delays";

  /* End */
}
