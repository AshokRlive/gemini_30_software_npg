/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import com.g3schema.ns_comms.GPRSModemT;
import com.g3schema.ns_comms.ScriptFileT;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;

/**
 * The GPRS Modem.
 */
public class GPRSModem extends AbstractCommsDevice {

  public static final String DEFAULT_CHAT_SCRIPT;
  public static final String DEFAULT_PEER_SCRIPT;

  public static final String PROPERTY_HOST = "host";
  public static final String PROPERTY_USER = "user";
  public static final String PROPERTY_PASS = "pass";

  private Logger log = Logger.getLogger(GPRSModem.class);

  private String host = "Eseye.com"; // APN host name
  private String user = "user"; // APN user name
  private String pass = "pass"; // APN user password

  private final HashMap<String, String> scriptsMap = new HashMap<String, String>();

  private String ppath = "No path";
  private String cpath = "No path";
  private String pscr = "No script";
  private String cscr = "No script";

  /* Load default scripts */
  static {
    // Load chat script from script template
    String scriptFile = "scripts/chatScriptTemplate";
    InputStream is = GPRSModem.class.getClassLoader().getResourceAsStream(scriptFile);
    DEFAULT_CHAT_SCRIPT = readTextFromStream(is);
    if (is == null) {
      Logger.getLogger(GPRSModem.class).warn(
          "Default Chat Script template is not found in \"" + scriptFile + "\"");
    }

    // Load peer script from script template
    scriptFile = "scripts/peerScriptTemplate";
    is = GPRSModem.class.getClassLoader().getResourceAsStream(scriptFile);
    DEFAULT_PEER_SCRIPT = readTextFromStream(is);
    if (is == null) {
      Logger.getLogger(GPRSModem.class).warn(
          "Default Peer Script template is not found in \"" + scriptFile + "\"");
    }
  }

  /**
   * Gets the content of input stream, and return it as a String. This style of
   * implementation does not throw Exceptions to the caller.
   *
   * @param is
   *          the inputstream which already exists and can be read.
   */
  private static String readTextFromStream(InputStream is) {
    if (is == null) {
      return "";
    }

    // ...checks on aFile are elided
    StringBuilder contents = new StringBuilder();
    String lineSeperator = System.getProperty("line.separator");
    InputStreamReader input = new InputStreamReader(is);
    Scanner scanner = new Scanner(input);

    try {
      while (scanner.hasNextLine()) {
        contents.append(scanner.nextLine() + lineSeperator);
      }
    } finally {
      scanner.close();
    }

    return contents.toString();
  }
  public GPRSModem(int deviceId) {
    super(CommsDeviceType.GPRS_MODEM, deviceId);
    // try{
    // if(DEFAULT_CHAT_SCRIPT != null)
    // addScript("chatEseye", DEFAULT_CHAT_SCRIPT);
    //
    // if(DEFAULT_PEER_SCRIPT != null)
    // addScript("peers/peerEseye", DEFAULT_PEER_SCRIPT);
    //
    // }catch (Exception e) {
    // log.error(e.getMessage());
    // }
  }

  //
  // @Override
  // public AbstractSerialPort getSerial(){
  // return serial;
  // }
  // @Override
  // public void attachToPort(AbstractSerialPort serial) {
  //
  // AbstractSerialPort oldValue = this.serial;
  // this.serial = serial;
  // firePropertyChange(PROPERTY_SERIAL, oldValue, serial);
  //
  // setTitle(createPPPTitle(serial));
  // }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    Object oldValue = getHost();
    this.host = host;
    firePropertyChange(PROPERTY_HOST, oldValue, host);
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    Object oldValue = getUser();
    this.user = user;
    firePropertyChange(PROPERTY_USER, oldValue, user);
  }

  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    Object oldValue = getPass();
    this.pass = pass;
    firePropertyChange(PROPERTY_PASS, oldValue, pass);
  }

  public String[] getScrtipPaths() {
    Set<String> keys = scriptsMap.keySet();
    return keys.toArray(new String[keys.size()]);
  }

  // public void loadScript(HashMap<String, String> scriptMap)throws Exception{
  // if(scriptMap == null || scriptMap.isEmpty()){
  // log.error("Cannot load scripts cause scriptMap is empty");
  // return;
  // }
  //
  // Object[] keys = scriptMap.keySet().toArray();
  //
  // for (int i = 0; i < keys.length; i++) {
  // if(manager.checkScriptExisted(this,keys[i].toString())){
  // throw new Exception("The script \""+keys[i]+"\" already exists!");
  // }
  // }
  //
  // // Clear previous scripts
  // scriptsMap.clear();
  //
  // // Load new scripts
  // for (int i = 0; i < keys.length; i++) {
  // scriptsMap.put(keys[i].toString(), scriptMap.get(keys[i]));
  // }
  // }

  // public void addScript(String path, String content) throws Exception{
  // if(path == null || path.isEmpty())
  // throw new Exception("Empty script path");
  //
  // if(manager.checkScriptExisted(this,path)){
  // throw new Exception("Script file \""+path+"\" already exists");
  // }
  //
  // scriptsMap.put(path,content);
  // }

  public void modifyScript(String path, String newContent) throws Exception {
    if (scriptsMap.containsKey(path)) {
      scriptsMap.put(path, newContent);
    } else {
      throw new Exception("Script file \"" + path + "\" doesn't exist");
    }
  }

  public String getScriptContent(String path) {
    String content = scriptsMap.get(path);

    if (content == null) {
      log.error("Script \"" + path + "\" is not contained in: " + this);
    }

    return content;
  }

  public void setPPath(String path) {
    ppath = path;
  }

  public void setCPath(String path) {
    cpath = path;
  }

  public void setPScript(String scr) {
    pscr = scr;
  }

  public void setCScript(String scr) {
    cscr = scr;
  }

  public String getPPath() {
    return ppath;
  }

  public String getCPath() {
    return cpath;
  }

  public String getPScript() {
    return pscr;
  }

  public String getCScript() {
    return cscr;
  }
  
  @Override
  public void copyConfigFrom(ICommsDevice device) {
    super.copyConfigFrom(device);
    if (device instanceof GPRSModem) {
      GPRSModem modem = (GPRSModem) device;
      setHost(modem.getHost());
      setUser(modem.getUser());
      setPass(modem.getPass());
    }
  }

  public void writeToXML(GPRSModemT xml) {
    super.writeToXML(xml);
    xml.apn.setValue(getHost());
    xml.user.setValue(getUser());
    xml.password.setValue(getPass());

    // Scripts
    String[] scriptPaths = getScrtipPaths();
    for (int i = 0; i < scriptPaths.length; i++) {
      String scriptContent = getScriptContent(scriptPaths[i]);
      if (scriptContent == null) {
        log.error("Script " + scriptPaths[i]
            + " is not exported cause its content is null");
      } else {
        ScriptFileT xml_script = xml.ScriptFile.append();
        xml_script.filePath.setValue(scriptPaths[i]);
        xml_script.template.append().setValue(scriptContent);
        xml_script.content.append().setValue(convertDialScript(this, scriptContent));
      }
    }
  }

  public void readFromXML(GPRSModemT xml, PortsManager portsManager) {
    super.readFromXML(xml, portsManager);

    setHost(xml.apn.getValue());
    setUser(xml.user.getValue());
    setPass(xml.password.getValue());
  }

  // ================== PPP Script Export ===================
  /**
   * Convert a script template to script and replace all field with value from
   * GPRSModem.
   *
   * @param modem
   *          the modem which uses & holds the script
   * @param scriptContent
   *          the script template
   * @return the converted script content
   */
  private static String convertDialScript(GPRSModem modem, String scriptContent) {
    scriptContent = scriptContent.replaceAll(PPP_SCRIPT_USER, modem.getUser());
    scriptContent = scriptContent.replaceAll(PPP_SCRIPT_HOST, modem.getHost());
    scriptContent = scriptContent.replaceAll(PPP_SCRIPT_PASS, modem.getPass());
    ISerialPort serial = modem.getSerialPort();
    if (serial != null) {
      String baudrate = String.valueOf(Integer.parseInt(serial.getBaudRate().getDescription()));
      scriptContent = scriptContent.replaceAll(PPP_SCRIPT_BAUDE_RATE, baudrate);
    }

    return scriptContent;
  }


  private static final String PPP_SCRIPT_HOST = "\\[host\\]";
  private static final String PPP_SCRIPT_USER = "\\[user\\]";
  private static final String PPP_SCRIPT_PASS = "\\[pass\\]";
  private static final String PPP_SCRIPT_BAUDE_RATE = "\\[baudRate\\]";
  
}
