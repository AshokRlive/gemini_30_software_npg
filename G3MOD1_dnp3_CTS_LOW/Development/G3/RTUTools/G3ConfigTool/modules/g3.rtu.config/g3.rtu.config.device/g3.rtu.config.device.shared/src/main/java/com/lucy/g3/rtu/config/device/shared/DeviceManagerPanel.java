/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.shared;

import javax.swing.Action;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.lucy.g3.rtu.config.shared.base.page.ItemListManagerPanel;
import com.lucy.g3.rtu.config.shared.base.page.StringFormatter;

/**
 * The panel for managing a list of devices.
 */
public final class DeviceManagerPanel extends ItemListManagerPanel {

  public DeviceManagerPanel(SelectionInList<?> selectionInList, AbstractTableAdapter<?> listTableModel,
      Action[] pageActions, Action viewAction) {
    super(selectionInList, listTableModel, pageActions, viewAction);

    init();
  }

  public DeviceManagerPanel(SelectionInList<?> selectionInList, Action[] pageActions, Action viewAction) {
    super(selectionInList, pageActions, viewAction);
    init();
  }

  private void init() {
    super.setListItemFormatter(new DeviceStringFormatter());
  }


  private static class DeviceStringFormatter implements StringFormatter {

    @Override
    public String format(Object value) {
      String text = "N/A";
      if (value != null) {
        if (value instanceof IDevice) {
          text = ((IDevice) value).getDeviceName();
        } else {
          text = value.toString();
          Logger.getLogger(getClass()).warn("No string found for: " + value);
        }
      }

      return text;
    }
  }
}
