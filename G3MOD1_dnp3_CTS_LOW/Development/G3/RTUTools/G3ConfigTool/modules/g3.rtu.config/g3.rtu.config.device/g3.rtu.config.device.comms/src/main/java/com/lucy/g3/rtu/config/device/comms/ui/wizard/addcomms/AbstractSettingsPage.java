/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;


abstract class AbstractSettingsPage extends WizardPage {
  private final static String CLIENT_PROPERTY_LABEL = "label";
  
  private String error = null;
  private final ArrayList<JTextField> textfields = new ArrayList<>();
  
  private final DefaultFormBuilder builder;
  
  protected AbstractSettingsPage(CommsDeviceType type) {
    super(String.format("%s Settings",type.getDescription()));
    
    JPanel contentPanel = new JPanel();
    JScrollPane sc = new JScrollPane(contentPanel);
    sc.setBorder(null);
    UIUtils.increaseScrollSpeed(sc);
    setLayout(new BorderLayout());
    add(sc, BorderLayout.CENTER);
    
    this.builder = AddCommsDeviceWizard.createBuilder(contentPanel);
  }
  
  protected JTextField addTextfield(String label, String key) {
    return addTextfield(label, key, null);
  }
  
  protected void addRowGap(){
    builder.appendParagraphGapRow();
    builder.nextLine();
  }
  
  protected JTextField addTextfield(String label, String key, String unit) {
    JTextField tf = new JTextField();
    tf.setName(key);
    tf.putClientProperty(CLIENT_PROPERTY_LABEL, label.replace(":", ""));
    builder.append(label, tf, new JLabel(unit));
    ValidationComponentUtils.setMandatoryBackground(tf);
    textfields.add(tf);
    builder.nextLine();
    return tf;
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    return validateAllTextfields();
  }
  
  private String validateAllTextfields() {
    String error = null;
    for (JTextField tf: textfields) {
      String label = (String) tf.getClientProperty(CLIENT_PROPERTY_LABEL);
      String ret = validateTextField(tf, label);
      
      // Update text field background
      if(ret != null) {
        ValidationComponentUtils.setErrorBackground(tf);
      }else {
        ValidationComponentUtils.setMandatoryBackground(tf);
      }
      
      // Store error
      if(error == null)
        error = ret;
    }
    
    return error;
  }
  
  protected String validateTextField(JTextField tf, String label) {
    String err = null;
    if(Strings.isBlank(tf.getText())) {
      err = String.format("The \"%s\" can not be blank!",label);
    }
    
    return err;
  }
  
}

