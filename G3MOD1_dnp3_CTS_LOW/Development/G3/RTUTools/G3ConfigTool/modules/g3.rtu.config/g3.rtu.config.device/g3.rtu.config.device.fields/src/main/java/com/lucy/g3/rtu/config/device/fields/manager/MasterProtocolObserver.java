/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.manager;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolObserver;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * An observer for observing the add/remove event of protocol sessions in order
 * to add/remove related field device to/from {@linkplain FieldDeviceManager}.
 */
public class MasterProtocolObserver implements IProtocolObserver {

  private Logger log = Logger.getLogger(MasterProtocolObserver.class);

  private final FieldDeviceManagerImpl manager;


  public MasterProtocolObserver(FieldDeviceManagerImpl manager) {
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
  }

  @Override
  public void notifySessionAdded(IProtocolSession session) {
    if (session instanceof IMasterProtocolSession) {
      IMasterProtocolSession masterSession = (IMasterProtocolSession) session;

      IFieldDevice device = null;
      try {
        device = (IFieldDevice) masterSession.getDevice();

        // It is important to give a unique id to device before adding to
        // manager.
        device.setDeviceId(MODULE_ID.forValue(manager.getSize()));

        if (manager.add(device)) {
          log.info("Succeeded to add field device:" + device);
        }
      } catch (Exception e) {
        log.error("Failed to add field device:" + device, e);
      }
    }
  }

  @Override
  public void notifySessionRemoved(IProtocolSession session) {
    if (session instanceof IMasterProtocolSession) {
      IMasterProtocolSession masterSession = (IMasterProtocolSession) session;
      IFieldDevice device = (IFieldDevice) masterSession.getDevice();
      manager.remove(device);
    }
  }
}