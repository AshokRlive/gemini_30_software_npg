/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms.AddCommsDeviceWizard;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.shared.base.page.AbstractItemManagerModel;

/**
 * Presentation model of Comms Device manager.
 */
class CommsDeviceManagerModel extends AbstractItemManagerModel {

  private final CommsDeviceManager manager;
  private PortsManager ports;

  private final SelectionInList<ICommsDevice> selectionInList;


  public CommsDeviceManagerModel(PortsManager ports, CommsDeviceManager manager) {
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
    this.ports = ports;
    selectionInList = new SelectionInList<ICommsDevice>(manager.getItemListModel());
  }

  @Override
  public SelectionInList<ICommsDevice> getSelectionInList() {
    return selectionInList;
  }

  @Override
  protected void addActionPerformed() {
    ICommsDevice result = AddCommsDeviceWizard.showAddCommsDeviceWizard(ports, manager);
    if(result != null)
      selectionInList.setSelection(result);
  }

  @Override
  protected void removeActionPerformed() {
    ICommsDevice sel = selectionInList.getSelection();
    if(sel != null){
      if(showConfirmRemoveDialog(sel.getDisplayName()))
        manager.remove(sel);
    }
  }

}
