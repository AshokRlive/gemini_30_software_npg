/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.jdesktop.swingx.prompt.PromptSupport;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolChannel;

class SelectChannelPage extends AbstractPage {
  public static final String ID =  "SelectChannelPageID";

  public static final String KEY_SELECTED_CHANNEL = "selectedChannel"; 
  public static final String KEY_CHANNEL_NAME = "channelName"; 
  public static final String KEY_CHANNEL_TYPE = "channelType";
  
  private static final String TEXT_CREATE_A_NEW_CHANNEL = "Create a new channel...";

  private final IMasterProtocolChannel<?>[] existingChannels;
  private final Object[] availableChannelTypes;

  private JComboBox<Object> combChannelType;
  private JComboBox<IMasterProtocolChannel<?>> combChannel;
  private JTextField tfChannelName;

  SelectChannelPage(IMasterProtocolChannel<?>[] existingChannels, Object[] availableChannelTypes) {
    super(ID, "Select a channel");
    this.existingChannels = existingChannels;
    this.availableChannelTypes = availableChannelTypes;
    
    initComponents();
    updateComponentsState();
  }

  @SuppressWarnings("unchecked")
  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);

    combChannel = new JComboBox<>(existingChannels);
    combChannel.setName(KEY_SELECTED_CHANNEL);
    combChannel.insertItemAt(null, existingChannels.length);
    combChannel.setRenderer(new DefaultListRenderer(new StringValue() {
      @Override
      public String getString(Object value) {
        if (value == null)
          return TEXT_CREATE_A_NEW_CHANNEL;
        IMasterProtocolChannel<?> ch = (IMasterProtocolChannel<?>) value;
        return String.format("%s >> %s", 
            ch.getProtocolType(), 
            ch.getChannelName());
      }
    }));
    combChannel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateComponentsState();
      }
    });
    ComboBoxUtil.makeWider(combChannel);
    builder.append("Select a channel:", combChannel);
    builder.nextLine();
    
    tfChannelName = new JTextField();
    tfChannelName.setName(KEY_CHANNEL_NAME);
    PromptSupport.setPrompt("e.g. TCP-channel", tfChannelName);
    builder.append("Channel Name:", tfChannelName);
    builder.nextLine();
    
    combChannelType = new JComboBox<>(availableChannelTypes);
    combChannelType.setName(KEY_CHANNEL_TYPE);
    builder.append("Channel Type:", combChannelType);
    builder.nextLine();
  }
  
  private void updateComponentsState() {
    IMasterProtocolChannel<?> ch = (IMasterProtocolChannel<?>) combChannel.getSelectedItem();
    
    boolean enabled = ch == null;
    tfChannelName.setEnabled(enabled);
    combChannelType.setEnabled(enabled);

    //Set default name
    tfChannelName.setText(ch == null ? "" : ch.getChannelName());

    //Select default type
    if(ch != null)
      combChannelType.setSelectedItem(ch.getChannelConfig().getLinkType());
  }

  @Override
  protected String validateContents(Component component, Object event) {
    error = null;
    
    checkNotBlank(tfChannelName, "Channel Name");

    IMasterProtocolChannel<?> ch = (IMasterProtocolChannel<?>) combChannel.getSelectedItem();
    if(ch != null) {
      ISerialPort sp = ch.getChannelConfig().getSerialConf().getSerialPort();
      if(sp != null && sp.isInUse() && !sp.allowMutipleUser()) {
        error = "Not allowed to add more session to a serial channel!";
      }
    }
    
    return error;
  }
  
  

}
