/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.GPRSModem;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.device.comms.domain.PSTNModem;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;


class AddCommsDeviceResult implements WizardResultProducer{
  private CommsDeviceType type;
  private final PortsManager ports; 
  private final CommsDeviceManager manager;
  
  public AddCommsDeviceResult(CommsDeviceType type, PortsManager ports, CommsDeviceManager manager) {
    this.type = type;
    this.manager = manager;
    this.ports = ports;
  }

  @Override
  public Object finish(Map data) throws WizardException {
    ICommsDevice result = null;
    
    if(type == null)
      type = (CommsDeviceType) data.get(SelectCommsTypePage.KEY_SELECTED_TYPE);
    
    if(type == null)
      throw new WizardException("The comms device type was not specified!");
    
    switch(type) {
    case PSTN_MODEM:
      result = new PSTNModem(0);
      setPstn(data,(PSTNModem) result);
      break;
    case GPRS_MODEM:
      result = new GPRSModem(0);
      setGprs(data,(GPRSModem) result);
      break;
    case PAKNET_MODEM:
      result = new PAKNETModem(0);
      setPaknet(data,(PAKNETModem) result);
      break;
    default:
      throw new WizardException("Unsupported type:"+type);
    }
    
    manager.add(result);

    // Set name
    result.setDeviceName((String) data.get(SelectPortPage.KEY_DEVICE_NAME));
    
    // Set port
    try {
      result.setSerialPort((ISerialPort) data.get(SelectPortPage.KEY_PORT));
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    
    
    return result;
  }
  
  private void setPstn(Map data, PSTNModem modem) {
    modem.setInitStr((String) data.get(PstnSettingsPage.KEY_INITSTR));
    modem.setDialStr((String) data.get(PstnSettingsPage.KEY_DIAL));
    modem.setDial2((String) data.get(PstnSettingsPage.KEY_DIAL2));
  }

  private void setGprs(Map data,GPRSModem gprs) {
    gprs.setHost((String) data.get(GprsSettingsPage.KEY_HOST));
    gprs.setUser((String) data.get(GprsSettingsPage.KEY_USER));
    gprs.setPass((String) data.get(GprsSettingsPage.KEY_PASS));
  }

  private void setPaknet(Map data,PAKNETModem pak) {
    pak.setDialStr((String) data.get(PaknetSettingsPage.KEY_MAIN_NUA));
    pak.setDial2((String) data.get(PaknetSettingsPage.KEY_STANDBY_NUA));

    pak.setConnectTimeout(Long.valueOf((String) data.get(PaknetSettingsPage.KEY_CONNECTT_IMEOUT)));
    pak.setRegularCallInterval(Long.valueOf((String) data.get(PaknetSettingsPage.KEY_REGULAR_CALL_INTERVAL)));
    pak.setModemInactivityTimeout(Long.valueOf((String) data.get(PaknetSettingsPage.KEY_MODEM_INACTIVITY_TIMEOUT)));
    pak.setConnectionInactivityTimeout(Long.valueOf((String) data.get(PaknetSettingsPage.KEY_CONNECTION_INACTIVITYT_IMEOUT)));
    try {
      pak.setDialRetryIntervalStr((String) data.get(PaknetSettingsPage.KEY_DIAL_RETRY_DELAYS));
    } catch (ParseException e) {
      e.printStackTrace(); 
    }
  }
  

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}

