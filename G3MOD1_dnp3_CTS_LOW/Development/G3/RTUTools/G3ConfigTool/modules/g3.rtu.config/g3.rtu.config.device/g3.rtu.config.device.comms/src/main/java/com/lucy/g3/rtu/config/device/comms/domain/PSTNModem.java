/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import com.g3schema.ns_comms.PSTNModemT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.port.PortsManager;

/**
 * The PSTN Modem.
 */
public class PSTNModem extends AbstractStandardModem {

  public static final String PROPERTY_INITSTR = "initStr";
  public static final String PROPERTY_CONNESTR = "conneStr";
  public static final String PROPERTY_HANUPSTR = "hanupStr";

  private String initStr = "atX0v0";
  private String conneStr = "CONNECT";
  private String hanupStr = "+++\\nATH \\n";


  public PSTNModem(int id) {
    super(CommsDeviceType.PSTN_MODEM, id);
  }

  public String getInitStr() {
    return initStr;
  }

  public String getConneStr() {
    return conneStr;
  }

  public String getHanupStr() {
    return hanupStr;
  }

  public void setInitStr(String initStr) {
    Object oldValue = this.initStr;
    this.initStr = initStr;
    firePropertyChange(PROPERTY_INITSTR, oldValue, initStr);
  }

  public void setConneStr(String conneStr) {
    Object oldValue = this.conneStr;
    this.conneStr = conneStr;
    firePropertyChange(PROPERTY_CONNESTR, oldValue, conneStr);
  }

  public void setHanupStr(String hanupStr) {
    Object oldValue = this.hanupStr;
    this.hanupStr = hanupStr;
    firePropertyChange(PROPERTY_HANUPSTR, oldValue, hanupStr);
  }

  public void writeToXML(PSTNModemT xml) {
    super.writeToXML(xml);

    xml.promptString.setValue(getPropmtStr());
    xml.dialString.setValue(getDialStr());

    if (!Strings.isBlank(getDial2())) {
      xml.dialString2.setValue(getDial2());
    }

    if (!Strings.isBlank(getInitStr())) {
      xml.initStr.setValue(getInitStr());

      xml.hangupString.setValue(getHanupStr());
      xml.connectString.setValue(getConneStr());
    }

  }

  public void readFromXML(PSTNModemT xml, PortsManager portsManager) {
    super.readFromXML(xml, portsManager);

    setPropmtStr(xml.promptString.getValue());
    setDialStr(xml.dialString.getValue());
    if (xml.dialString2.exists()) {
      setDial2(xml.dialString2.getValue());
    }

    if (xml.initStr.exists()) {
      setInitStr(xml.initStr.getValue());
    }

    setHanupStr(xml.initStr.getValue());
    setConneStr(xml.connectString.getValue());
  }

  
  @Override
  public void copyConfigFrom(ICommsDevice device) {
    super.copyConfigFrom(device);
    if(device instanceof PSTNModem) {
      PSTNModem modem = (PSTNModem) device;
      setConneStr(modem.getConneStr());
      setHanupStr(modem.getHanupStr());
      setInitStr(modem.getInitStr());
    }
  }
}
