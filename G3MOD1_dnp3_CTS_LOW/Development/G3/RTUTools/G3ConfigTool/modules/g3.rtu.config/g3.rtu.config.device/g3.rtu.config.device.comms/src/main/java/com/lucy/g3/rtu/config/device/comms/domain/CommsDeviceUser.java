/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

import com.jgoodies.binding.beans.Model;


/**
 *
 */
public class CommsDeviceUser extends Model implements ICommsDeviceUser, PropertyChangeListener{
  private ICommsDevice commsDevice;
  
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if(evt.getPropertyName().equals(ICommsDevice.PROPERTY_DEVICE_DELETE)) {
      if(evt.getSource() == getCommsDevice()) {
        try {
          setCommsDevice(null);
        } catch (PropertyVetoException e) {
          // never reach here.
        }
      }
    }
  }
  
  @Override
  public ICommsDevice getCommsDevice() {
    return commsDevice;
  }

  public void delete(){
    try {
      setCommsDevice(null);
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
  }
  
  @Override
  public void setCommsDevice(ICommsDevice commsDevice) throws PropertyVetoException {
    ICommsDevice oldCommsDevice = getCommsDevice();
    fireVetoableChange(PROPERTY_COMMS_DEVICE, oldCommsDevice, commsDevice);
    
    // Stop observing delete event
    if(oldCommsDevice != null) {
      oldCommsDevice.removePropertyChangeListener(ICommsDevice.PROPERTY_DEVICE_DELETE, this);
      oldCommsDevice.removeUser(this);
    }
    
    this.commsDevice = commsDevice;

    
    // Start observing delete event
    if(this.commsDevice != null) {
      this.commsDevice.addPropertyChangeListener(ICommsDevice.PROPERTY_DEVICE_DELETE, this);
      this.commsDevice.addUser(this);
    }
    
    
    firePropertyChange(PROPERTY_COMMS_DEVICE, oldCommsDevice,this.commsDevice);
  }

//  @Override
//  public String getCommsDeviceName() {
//    return commsDevice != null ? commsDevice.getDisplayName() : "No Commons Device";
//  }
  

}

