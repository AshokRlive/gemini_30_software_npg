/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.domain;

import com.lucy.g3.rtu.config.device.shared.IDevice;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;

/**
 * A kind of device that is deployed in field. e.g. Power Meter.
 * <p>
 * A field device is usually connected to RTU via TCP/Serial and communicates
 * via MasterProtocol, e.g. ModBus master.
 * </p>
 */
public interface IFieldDevice extends IMasterProtocolModule, IDevice {

}
