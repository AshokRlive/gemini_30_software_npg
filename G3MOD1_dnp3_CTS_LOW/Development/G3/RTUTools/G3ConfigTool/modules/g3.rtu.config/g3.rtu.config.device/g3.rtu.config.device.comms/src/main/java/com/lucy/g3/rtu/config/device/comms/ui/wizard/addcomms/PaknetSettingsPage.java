/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import javax.swing.JTextField;

import org.jdesktop.swingx.prompt.PromptSupport;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.shared.base.labels.Units;


class PaknetSettingsPage extends AbstractSettingsPage{
  public static final String KEY_MAIN_NUA                      = PAKNETModem.PROPERTY_MAIN_NUA;
  public static final String KEY_STANDBY_NUA                   = PAKNETModem.PROPERTY_STANDBY_NUA;
  public static final String KEY_CONNECTT_IMEOUT               = PAKNETModem.PROPERTY_CONNECTT_IMEOUT;
  public static final String KEY_REGULAR_CALL_INTERVAL         = PAKNETModem.PROPERTY_REGULAR_CALL_INTERVAL;
  public static final String KEY_MODEM_INACTIVITY_TIMEOUT      = PAKNETModem.PROPERTY_MODEM_INACTIVITY_TIMEOUT;
  public static final String KEY_CONNECTION_INACTIVITYT_IMEOUT = PAKNETModem.PROPERTY_CONNECTION_INACTIVITYT_IMEOUT;
  public static final String KEY_DIAL_RETRY_DELAYS             = PAKNETModem.PROPERTY_DIAL_RETRY_DELAYS;
  
  
  public PaknetSettingsPage (){
    super(CommsDeviceType.PAKNET_MODEM);
    
    initComponents();
  }
  
  private void initComponents() {
    PromptSupport.setPrompt("Enter a 14 digit string", addTextfield("Main NUA:", KEY_MAIN_NUA));
    PromptSupport.setPrompt("Enter a 14 digit string", addTextfield("Backup NUA:" ,KEY_STANDBY_NUA));
    addRowGap();
    addTextfield("Connection Timeout:", KEY_CONNECTT_IMEOUT, Units.MS).setText("3000");
    addTextfield("Regular Call Interval:", KEY_REGULAR_CALL_INTERVAL, Units.MINS).setText("1440");
    addTextfield("Modem Inactivity Timeout:", KEY_MODEM_INACTIVITY_TIMEOUT, Units.MINS).setText("4320");
    addTextfield("Connect Inactivity Timeout:", KEY_CONNECTION_INACTIVITYT_IMEOUT, Units.SECS).setText("60");
    addTextfield("Dialling Retry Delays:", KEY_DIAL_RETRY_DELAYS, Units.MINS).setText("3,4,10,30");
  }

  @Override
  protected String validateTextField(JTextField tf, String label) {
    String error = super.validateTextField(tf, label);
    if(error != null)
      return error;
    
    final String key = tf.getName();
    
    /* Validate NUA */
    if(KEY_MAIN_NUA.equals(key) || KEY_STANDBY_NUA.equals(key)) {
      error = validateNUA(tf);
      if(error != null)
        return error;
    }
    
    /* Validate retry delays */
    if(KEY_DIAL_RETRY_DELAYS.equals(key)) {
      error = validateRetryDelays(tf);
      if(error != null)
        return error;
    }
    
    /* Validate timeout */
    if(KEY_CONNECTT_IMEOUT.equals(key)               
        || KEY_REGULAR_CALL_INTERVAL.equals(key)         
        || KEY_MODEM_INACTIVITY_TIMEOUT.equals(key)      
        || KEY_CONNECTION_INACTIVITYT_IMEOUT.equals(key)){
      error = validateUnsingedInt(tf,label);
      if(error != null)
        return error;      
    }
        
      
    return null;
  }

  private String validateUnsingedInt(JTextField tf, String label) {
    String valueStr = tf.getText();
    try {
      Integer.parseInt(valueStr);
    }catch(Throwable e){
      return String.format("Invalid input value:%s for \"%s\".",valueStr, label);
    }
    return null;
  }

  private String validateRetryDelays(JTextField tf) {
    return null; // TODO  to be implemented
  }

  private String validateNUA(JTextField tf) {
      String nua = tf.getText();
      if(nua.length() != PAKNETModem.NUA_LENGTH) {
        return String.format("Invalid lenght of NUA:%d, expected: %d", nua.length(), PAKNETModem.NUA_LENGTH);
      }
    return null;
  }
  
  
}

