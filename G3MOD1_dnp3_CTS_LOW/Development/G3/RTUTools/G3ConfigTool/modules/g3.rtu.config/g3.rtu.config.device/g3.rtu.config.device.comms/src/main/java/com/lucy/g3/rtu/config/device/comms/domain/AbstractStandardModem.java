/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

/**
 * The abstract standard modem.
 */
abstract class AbstractStandardModem extends AbstractCommsDevice {

  public static final String PROPERTY_DIALSTR = "dialStr"; 
  public static final String PROPERTY_DIAL2 = "dial2";
  public static final String PROPERTY_PROPMTSTR = "propmtStr";

  private String dialStr = "";
  private String dial2 = "";
  private String propmtStr = "OK";


  AbstractStandardModem(CommsDeviceType type, int id) {
    super(type, id);
  }

  // ================== Setter/Getter ===================

  public String getDialStr() {
    return dialStr;
  }

  public void setDialStr(String dialStr) {
    Object oldValue = this.dialStr;
    this.dialStr = dialStr;
    firePropertyChange(PROPERTY_DIALSTR, oldValue, dialStr);
  }

  public String getDial2() {
    return dial2;
  }

  public void setDial2(String dial2) {
    Object oldValue = this.dial2;
    this.dial2 = dial2;
    firePropertyChange(PROPERTY_DIAL2, oldValue, dial2);
  }

  public String getPropmtStr() {
    return propmtStr;
  }

  public void setPropmtStr(String propmtStr) {
    Object oldValue = this.propmtStr;
    this.propmtStr = propmtStr;
    firePropertyChange(PROPERTY_PROPMTSTR, oldValue, propmtStr);
  }
  
  @Override
  public void copyConfigFrom(ICommsDevice device) {
    super.copyConfigFrom(device);
    if(device instanceof AbstractStandardModem) {
      AbstractStandardModem modem = (AbstractStandardModem) device;
      setDial2(modem.getDial2());
      setDialStr(modem.getDialStr());
      setPropmtStr(modem.getPropmtStr());
    }
  }

}
