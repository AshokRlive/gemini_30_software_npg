/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import com.lucy.g3.rtu.config.device.shared.DeviceManager;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;

/**
 * Interface for managing all G3 comms device instances.
 */
public interface CommsDeviceManager extends DeviceManager<ICommsDevice>, IContainerValidation{

  String CONFIG_MODULE_ID = "CommsDeviceManager";

  ICommsDevice getDevice(CommsDeviceType type, int deviceId);

  ICommsDevice getDevice(int deviceId);

}
