/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.beans.PropertyVetoException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.configtool.config.template.ITemplate;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolChannel;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;


 class FieldDeviceAddingResult implements WizardResultProducer{
   private Logger log = Logger.getLogger(FieldDeviceAddingResult.class);
   
   private final MasterProtocolManager manager;
   
   
  public FieldDeviceAddingResult(MasterProtocolManager manager) {
    super();
    this.manager = manager;
  }

  @Override
  public IFieldDevice finish(Map data) throws WizardException {
    ProtocolType ptype = (ProtocolType) data.get(SelectProtocolPage.KEY_SELECTED_PROTOCOL_TYPE);
    
    // Create protocol
    IMasterProtocol<?> protocol = manager.getProtocolByType(ptype);
    if(protocol == null)
      protocol = createProtocol(ptype);
    
    // Create channel
    IMasterProtocolChannel<?> channel = (IMasterProtocolChannel<?>) data.get(SelectChannelPage.KEY_SELECTED_CHANNEL);
    if(channel == null) {
      String name = (String) data.get(SelectChannelPage.KEY_CHANNEL_NAME);
      Object type = data.get(SelectChannelPage.KEY_CHANNEL_TYPE);
      channel = createChannel(protocol, name, type);
      
      // Configure port
      ISerialPort serialPort = (ISerialPort) data.get(SelectSerialPortPage.KEY_SERIAL_PORT);
      if(serialPort != null) {
        try {
          channel.getChannelConfig().getSerialConf().setSerialPort(serialPort);
        } catch (Throwable e) {
          log.error("Cannot configure serial port", e);
        }
      } else {
        String ip = (String) data.get(ConfigureTcpPage.KEY_TCPIP_ADDRESS);  
        String port = (String) data.get(ConfigureTcpPage.KEY_TCPIP_PORT);  
        String timeout = (String) data.get(ConfigureTcpPage.KEY_TCPIP_TIMEOUT);
        
        ProtocolChannelTCPConf tcpConf = channel.getChannelConfig().getTcpConf();
        tcpConf.setIpAddress(ip);
        tcpConf.setIpPort(Long.valueOf(port));
        tcpConf.setIpConnectTimeout(Long.valueOf(timeout));
      }
    }
    
    // Create session & device
    ITemplate templ = (ITemplate) data.get(SelectTemplatePage.KEY_DEVICE_TEMPLATE);
    String devName = (String) data.get(SelectTemplatePage.KEY_DEVICE_NAME);
    String devAddr= (String) data.get(SelectTemplatePage.KEY_DEVICE_ADDRESS);
    IMasterProtocolSession sesn = channel.addSession(devName);
    
    if(templ != null) {
      templ.applyTo(sesn, true);
    }
    
    try {
      sesn.getDevice().setDeviceName(devName);
      sesn.setSlaveAddress(Long.valueOf(devAddr));
    } catch (NumberFormatException | PropertyVetoException e) {
      log.error("Failed to set slave address to :" + devAddr);
    }
    
    return null;
  }

  private IMasterProtocolChannel<?> createChannel(IMasterProtocol<?> protocol, String name, Object type) {
    IMasterProtocolChannel<?> ch = protocol.addChannel(name);
    ch.getChannelConfig().setLinkType(type);
    return ch;
  }

  private IMasterProtocol<?> createProtocol(ProtocolType ptype) {
    IProtocolFactory factory = ProtocolFactories.getFactory(ptype);
    return (IMasterProtocol<?>) factory.createProtocol();
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}

