/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;

import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.application.Application;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.framework.page.AbstractListPage;
import com.lucy.g3.gui.framework.page.NavigationEvent;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.master.shared.ui.MasterProtocolManagerModel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigListPage;

/**
 * The page for managing field devices list and also protocols used by field
 * device.
 */
public class FieldDeviceManagerPage extends AbstractConfigListPage {

  public static final String PROPERTY_SELECTION_DEVICE_NOT_EMPTY = "selectionDeviceNotEmpty";

  private static final String ACTION_VIEW_DEVICE = "viewDevice";

  private final MasterProtocolManagerModel protocolMgrModel;

  private final FieldDeviceManagerModel fieldDevMgrModel;
  
  private boolean selectionDeviceNotEmpty;


  public FieldDeviceManagerPage(FieldDeviceManager manager, PortsManager portsman) {
    super(manager.getMasterProtoManager());
    Preconditions.checkNotNull(manager, "manager is null");
    protocolMgrModel = new MasterProtocolManagerModel(manager.getMasterProtoManager());
    fieldDevMgrModel = new FieldDeviceManagerModel(manager, portsman);

    setNodeName("Field Devices");
  }

  private void createUIComponents() {
    protocolPanel = protocolMgrModel.createPanel(getViewAction());
    fieldDevPanel = fieldDevMgrModel.createPanel(getViewDeviceAction());
  }

  @Override
  public ListModel<?> getSelsectionListModel() {
    return protocolMgrModel.getSelectionInList().getListModel();
  }

  @Override
  public Action[] getContextActions() {
    return ArrayUtils.addAll(fieldDevMgrModel.getNodeActions(), protocolMgrModel.getNodeActions());
  }

  @Override
  protected SelectionInList<?> getSelectionInList() {
    return protocolMgrModel.getSelectionInList();
  }

  private Action getViewDeviceAction() {
    return Application.getInstance().getContext().getActionMap(AbstractListPage.class, this)
        .get(ACTION_VIEW_DEVICE);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_SELECTION_DEVICE_NOT_EMPTY)
  public void viewDevice() {
    IFieldDevice source = fieldDevMgrModel.getSelectionInList().getSelection();
    if (source != null) {
      fireNavigationEvent(new NavigationEvent(source.getSession()));
    }
  }

  // Bean getter
  public boolean isSelectionDeviceNotEmpty() {
    return selectionDeviceNotEmpty;
  }
  
  public void setSelectionDeviceNotEmpty(boolean selectionDeviceNotEmpty) {
    Object oldValue = this.selectionDeviceNotEmpty;
    this.selectionDeviceNotEmpty = selectionDeviceNotEmpty;
    firePropertyChange(PROPERTY_SELECTION_DEVICE_NOT_EMPTY, oldValue, selectionDeviceNotEmpty);
  }

  @Override
  protected void init() throws Exception {
    super.init();
    initComponents();
    
    // Sync "not empty"
    PropertyConnector.connect(
        fieldDevMgrModel, FieldDeviceManagerModel.PROPERTY_SELECTION_NOT_EMPTY,
        this, PROPERTY_SELECTION_DEVICE_NOT_EMPTY);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {
      tabbedPane1.addTab("Field Devices", fieldDevPanel);
      tabbedPane1.addTab("Protocol", protocolPanel);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JPanel fieldDevPanel;
  private JPanel protocolPanel;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
