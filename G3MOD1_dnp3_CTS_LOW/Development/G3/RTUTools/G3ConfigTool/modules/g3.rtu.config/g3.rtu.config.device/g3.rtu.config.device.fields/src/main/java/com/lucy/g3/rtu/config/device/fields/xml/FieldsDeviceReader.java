/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.xml;

import com.g3schema.FieldDevicesT;
import com.g3schema.ns_mmb.MMBChnlT;
import com.g3schema.ns_mmb.MMBSesnT;
import com.g3schema.ns_mmb.ModbusMasterT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.device.fields.domain.FieldDeviceFactory;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;

/**
 * The XML reader for Field Device.
 */
public class FieldsDeviceReader {

  private final FieldDeviceManager devMgr;
  private final PortsManager ports;


  public FieldsDeviceReader(FieldDeviceManager manager, PortsManager ports, ValidationResult result) {
    this.devMgr = manager;
    this.ports = ports;
  }

  public void read(FieldDevicesT xml) {

    if (xml.ModbusMaster.exists()) {
      /* Read ModBus master */
      ModbusMasterT xml_mbm = xml.ModbusMaster.first();
      MMB mbm = new MMB(FieldDeviceFactory.INSTANCE);
      devMgr.getMasterProtoManager().add(mbm);

      int chnlNum = xml_mbm.channel.count();
      int index = 0;
      for (int i = 0; i < chnlNum; i++) {
        /* Read ModBus master channel */
        MMBChnlT xml_channel = xml_mbm.channel.at(i);
        MMBChannel channel = mbm.addChannel(null);
        channel.getChannelConfig().readFromXML(xml_channel.config.first(), ports);

        /* Read ModBus session */
        int sesnNum = xml_channel.session.count();
        for (int j = 0; j < sesnNum; j++) {

          MMBSesnT xml_sesn = xml_channel.session.at(j);
          MMBSession sesn = channel.addSession("session");
          sesn.readFromXML(xml_sesn);
          ;

          index++;
        }
      }
    }
  }

}
