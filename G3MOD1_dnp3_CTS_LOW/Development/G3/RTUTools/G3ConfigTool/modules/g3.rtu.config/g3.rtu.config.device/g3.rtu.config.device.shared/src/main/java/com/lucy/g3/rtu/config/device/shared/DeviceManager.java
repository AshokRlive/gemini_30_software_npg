/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.shared;

import com.lucy.g3.rtu.config.shared.manager.IListConfigModule;

/**
 * Interface of Device Manager.
 *
 * @param <T>
 *          the device type
 */
public interface DeviceManager<T extends IDevice> extends IListConfigModule<T> {

}
