/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;
import com.lucy.g3.rtu.config.shared.base.labels.Units;


class ConfigureTcpPage extends AbstractPage{
  public static final String KEY_TCPIP_ADDRESS = "TCPIPAddress"; 
  public static final String KEY_TCPIP_PORT  = "TCPIPPort"; 
  public static final String KEY_TCPIP_TIMEOUT = "TCPIPTimeout"; 

  private IMasterProtocolChannel<?> channel;
  
  private JTextField tfAddress ;
  private JTextField tfPort    ;
  private JTextField tfTimeout ;
  
  ConfigureTcpPage(IMasterProtocolChannel<?> channel){
    super("Configure TCP/IP");
    this.channel = channel;
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);
    
    tfAddress = new JTextField("0.0.0.0");
    tfAddress.setName(KEY_TCPIP_ADDRESS);
    builder.append("IP Address:",tfAddress,true);
    
    tfPort = new JTextField("502");
    tfPort.setName(KEY_TCPIP_PORT);
    builder.append("Port:",tfPort,true);
    
    tfTimeout = new JTextField("5000");
    tfTimeout.setName(KEY_TCPIP_TIMEOUT);
    builder.append("Connect Timeout:",tfTimeout, new JLabel(Units.MS));
    
    if(channel != null) {
      ProtocolChannelTCPConf tcpConf = channel.getChannelConfig().getTcpConf();
      tfAddress.setText(tcpConf.getIpAddress());
      tfPort.setText(String.valueOf(tcpConf.getIpPort()));
      tfTimeout.setText(String.valueOf(tcpConf.getIpConnectTimeout()));
    }

    boolean enable = channel == null;
    tfAddress.setEnabled(enable);
    tfPort.setEnabled(enable);
    tfTimeout.setEnabled(enable);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    if(channel != null)
      return null;// no validation for existing channel
    
    error = null;
    
    boolean anyIPEnabled = false;
    String ip = tfAddress.getText();
    if(IPAddress.validateIPAddress(ip, false, anyIPEnabled) == false){
      error = "Invalid IP address: " +ip;
      setErrorBackground(tfAddress);
    } else {
      setNormalBackground(tfAddress);
    }
    
    checkUnsignedLong(tfPort, "Port");
    checkUnsignedLong(tfTimeout, "Connection Timeout");
    
    return error;
  }

  
}

