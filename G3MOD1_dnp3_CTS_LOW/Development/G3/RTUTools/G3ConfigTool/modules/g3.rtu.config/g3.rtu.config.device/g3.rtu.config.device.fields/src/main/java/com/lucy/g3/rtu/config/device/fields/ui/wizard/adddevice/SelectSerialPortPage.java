/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.awt.Component;
import java.util.Collection;

import javax.swing.JComboBox;

import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.rtu.config.port.Port;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolChannel;

class SelectSerialPortPage extends AbstractPage {
  public static final String KEY_SERIAL_PORT = "serialPort"; 

  private final Collection<ISerialPort> ports;
  
  private JComboBox<Object> combPort;
  private final IMasterProtocolChannel<?> channel;
  
  public SelectSerialPortPage(IMasterProtocolChannel<?> channel, Collection<ISerialPort> ports) {
    super("Select a serial port");
    this.channel = channel;
    this.ports = ports;
    initComponents();
  }
  

  @SuppressWarnings("unchecked")
  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);

    combPort = new JComboBox<>(ports.toArray());
    combPort.setName(KEY_SERIAL_PORT);
    combPort.insertItemAt(null, 0);
    combPort.setRenderer(new DefaultListRenderer(new StringValue() {

      @Override
      public String getString(Object value) {
        return value == null? "Not Selected" : ((Port) value).getDisplayName();
      }
    }));
    combPort.setSelectedItem(null);
    builder.append("Select a serial port:", combPort);
    builder.nextLine();
    
    
    if(channel != null) {
      combPort.setSelectedItem(channel.getChannelConfig().getSerialConf().getSerialPort());
    }
    
    combPort.setEnabled(channel == null);
  }


  @Override
  protected String validateContents(Component component, Object event) {
    if(channel != null)
      return null; // no validation for existing channel
    
    ISerialPort selectedPort = (ISerialPort) combPort.getSelectedItem();
    if(selectedPort == null) {
      return "Please select a port for the field device!";
    } else if(selectedPort.isInUse() && !selectedPort.allowMutipleUser()) {
        return "The selected port is already in use!";
    }
    
    return null;
  }
  
  
  
}
