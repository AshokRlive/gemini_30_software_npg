/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.manager;

import java.util.Collection;

import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Interface for managing all G3 field device instances which are implemented as
 * {@linkplain Module}.
 */
public interface FieldDeviceManager extends IModuleManager<IFieldDevice>{

  String CONFIG_MODULE_ID = "FieldDeviceManager";
  
  MasterProtocolManager getMasterProtoManager();

  Collection<IFieldDevice> getDevicesByType(MODULE... types);

  IFieldDevice getDevice(MODULE type, MODULE_ID id);

  int getDeviceSize();

}
