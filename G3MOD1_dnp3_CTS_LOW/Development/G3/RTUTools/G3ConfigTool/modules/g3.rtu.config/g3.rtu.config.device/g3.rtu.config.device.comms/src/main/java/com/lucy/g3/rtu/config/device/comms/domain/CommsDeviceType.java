/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

/**
 * The Enum CommsDeviceType.
 */
public enum CommsDeviceType {
  GPRS_MODEM("GPRSM Modem"),
  PSTN_MODEM("PSTN Modem"),
  PAKNET_MODEM("PAKNET");

  CommsDeviceType(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return getDescription();
  }


  private final String description;
}
