/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.device.comms.utils.CommsDeviceFinder;
import com.lucy.g3.rtu.config.device.shared.AbstractDeviceManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The implementation of {@linkplain CommsDeviceManager}.
 */
public final class CommsDeviceManagerImpl extends AbstractDeviceManager<ICommsDevice>
    implements CommsDeviceManager {

  public CommsDeviceManagerImpl(IConfig owner) {
    super(owner);
  }

  @Override
  public ICommsDevice getDevice(CommsDeviceType type, int deviceId) {
    Collection<ICommsDevice> dev = getAllItems();
    return CommsDeviceFinder.findCommsDevice(dev, type, deviceId);
  }

  @Override
  public ICommsDevice getDevice(int deviceId) {
    Collection<ICommsDevice> dev = getAllItems();
    return CommsDeviceFinder.findCommsDevice(dev, deviceId);
  }

  @Override
  protected void postAddAction(ICommsDevice newDevice) {
    updateAllDeviceID();
    
    ((AbstractCommsDevice)newDevice).setManager(this);
  }

  @Override
  protected void postRemoveAction(ICommsDevice removedDevice) {
    super.postRemoveAction(removedDevice);
    updateAllDeviceID();
    ((AbstractCommsDevice)removedDevice).setManager(null);
  }

  /**
   * Updates all CommsDevices id using its index.
   */
  private void updateAllDeviceID() {
    Collection<ICommsDevice> devices = getAllItems();
    int id = 0;
    for (ICommsDevice dev : devices) {
      dev.setDeviceId(id++);
    }
  }

  @Override
  public IContainerValidator getValidator() {
    return validator;
  }
  
  private final Validator validator = new Validator(this);
  private static class Validator extends AbstractContainerValidator<CommsDeviceManager> {

    public Validator(CommsDeviceManager validatedItem) {
      super(validatedItem);
    }

    @Override
    public String getTargetName() {
      return "Comms Device Manager";
    }

    @Override
    protected Collection<IValidation> getValidatableChildrenItems() {
      return new ArrayList<IValidation>(target.getAllItems());
    }
    
  }

}
