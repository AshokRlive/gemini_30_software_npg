/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.domain;

import java.text.ParseException;

import com.g3schema.ns_comms.PAKNETModemT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.port.PortsManager;

/**
 * The PAKNET Modem.
 */
public class PAKNETModem extends AbstractStandardModem {
  public final static int NUA_LENGTH = 14;

  private static final String RETRY_INTERVAL_DELIMITER = ",";
  
  // ====== Properties ======
  public static final String PROPERTY_CONNECTT_IMEOUT = "connectTimeout";
  public static final String PROPERTY_REGULAR_CALL_INTERVAL = "regularCallInterval";
  public static final String PROPERTY_MODEM_INACTIVITY_TIMEOUT = "modemInactivityTimeout";
  public static final String PROPERTY_CONNECTION_INACTIVITYT_IMEOUT = "connectionInactivityTimeout";
  public static final String PROPERTY_DIAL_RETRY_INTERVAL_STR = "dialRetryIntervalStr";

  // ====== Properties Alias ======
  public static final String PROPERTY_MAIN_NUA = PROPERTY_DIALSTR;
  public static final String PROPERTY_STANDBY_NUA = PROPERTY_DIAL2;
  public static final String PROPERTY_DIAL_RETRY_DELAYS = PROPERTY_DIAL_RETRY_INTERVAL_STR;

  private long connectTimeout = 3000; // ms
  private long regularCallInterval = 1440; // mins. 24 hours
  private long modemInactivityTimeout = 4320; // mins. 3 days
  private long connectionInactivityTimeout = 60; // seconds

  private long[] dialRetryIntervalMins;
  private String dialRetryIntervalStr;


  public PAKNETModem(int deviceId) {
    super(CommsDeviceType.PAKNET_MODEM, deviceId);
    try {
      setDialRetryIntervalStr("3,5,10,30");
    } catch (ParseException e) {
      // No exception expected
    }
  }

  public long[] getDialRetryIntervalMins() {
    return dialRetryIntervalMins;
  }

  public void setDialRetryIntervalMins(long[] dialRetryIntervalMins) {
    _setDialRetryIntervals(dialRetryIntervalMins);

    if (dialRetryIntervalMins == null || dialRetryIntervalMins.length == 0) {
      _setDialRetryIntervalStr("");

    } else {
      /* Convert interval values to string */
      String newRetryStr = "";
      for (int i = 0; i < dialRetryIntervalMins.length; i++) {
        newRetryStr += String.valueOf(dialRetryIntervalMins[i]);
        if (i < dialRetryIntervalMins.length - 1) {
          newRetryStr += ",";
        }
      }
      _setDialRetryIntervalStr(newRetryStr);
    }
  }

  private void _setDialRetryIntervals(long[] dialRetryIntervalMins) {
    this.dialRetryIntervalMins = dialRetryIntervalMins;
  }

  public String getDialRetryIntervalStr() {
    return dialRetryIntervalStr;
  }

  public void setDialRetryIntervalStr(String dialRetryIntervalStr) throws ParseException {

    /* Update interval numbers */
    if (dialRetryIntervalStr == null) {
      _setDialRetryIntervals(null);
    } else {
      /* Convert interval string to interval values */
      String[] intervalStrs = dialRetryIntervalStr.split(RETRY_INTERVAL_DELIMITER);
      long[] intervals = new long[intervalStrs.length];
      for (int i = 0; i < intervals.length; i++) {
        try {
          intervals[i] = Long.parseLong(intervalStrs[i]);
        } catch (Throwable e) {
          throw new ParseException("Invalid interval str:" + dialRetryIntervalStr, 0);
        }
      }
      _setDialRetryIntervals(intervals);
    }

    _setDialRetryIntervalStr(dialRetryIntervalStr);
  }

  private void _setDialRetryIntervalStr(String dialRetryIntervalStr) {
    Object oldValue = this.dialRetryIntervalStr;
    this.dialRetryIntervalStr = dialRetryIntervalStr;
    firePropertyChange(PROPERTY_DIAL_RETRY_INTERVAL_STR, oldValue, dialRetryIntervalStr);
  }

  public long getConnectTimeout() {
    return connectTimeout;
  }

  public void setConnectTimeout(long connectTimeout) {
    Object oldValue = this.connectTimeout;
    this.connectTimeout = connectTimeout;
    firePropertyChange(PROPERTY_CONNECTT_IMEOUT, oldValue, connectTimeout);
  }

  public long getRegularCallInterval() {
    return regularCallInterval;
  }

  public void setRegularCallInterval(long regularCallInterval) {
    Object oldValue = this.regularCallInterval;
    this.regularCallInterval = regularCallInterval;
    firePropertyChange(PROPERTY_REGULAR_CALL_INTERVAL, oldValue, regularCallInterval);
  }

  public long getModemInactivityTimeout() {
    return modemInactivityTimeout;
  }

  public void setModemInactivityTimeout(long modemInactivityTimeout) {
    Object oldValue = this.modemInactivityTimeout;
    this.modemInactivityTimeout = modemInactivityTimeout;
    firePropertyChange(PROPERTY_MODEM_INACTIVITY_TIMEOUT, oldValue, modemInactivityTimeout);
  }

  public long getConnectionInactivityTimeout() {
    return connectionInactivityTimeout;
  }

  public void setConnectionInactivityTimeout(long connectionInactivityTimeout) {
    Object oldValue = this.connectionInactivityTimeout;
    this.connectionInactivityTimeout = connectionInactivityTimeout;
    firePropertyChange(PROPERTY_CONNECTION_INACTIVITYT_IMEOUT, oldValue, connectionInactivityTimeout);
  }

  public void writeToXML(PAKNETModemT xml) {
    super.writeToXML(xml);

    xml.promptString.setValue(getPropmtStr());
    xml.dialString.setValue(getDialStr());

    if (!Strings.isBlank(getDial2())) {
      xml.dialString2.setValue(getDial2());
    }

    xml.connectionInactivityTimeoutSecs.setValue(getConnectionInactivityTimeout());
    xml.regularCallIntervalMins.setValue(getRegularCallInterval());
    xml.modemInactivityTimeoutMins.setValue(getModemInactivityTimeout());
    xml.connectTimeoutMs.setValue(getConnectTimeout());

    long[] dialRetryMins = getDialRetryIntervalMins();
    for (int i = 0; i < dialRetryMins.length; i++) {
      xml.dialRetryIntervalSecs.append().setValue(dialRetryMins[i] * 60);
    }
  }

  public void readFromXML(PAKNETModemT xml, PortsManager portsManager) {
    super.readFromXML(xml, portsManager);

    setPropmtStr(xml.promptString.getValue());
    setDialStr(xml.dialString.getValue());
    if (xml.dialString2.exists()) {
      setDial2(xml.dialString2.getValue());
    }

    setConnectionInactivityTimeout(xml.connectionInactivityTimeoutSecs.getValue());
    setRegularCallInterval(xml.regularCallIntervalMins.getValue());
    setModemInactivityTimeout(xml.modemInactivityTimeoutMins.getValue());
    setConnectTimeout(xml.connectTimeoutMs.getValue());

    long[] dialRetryMins = new long[xml.dialRetryIntervalSecs.count()];
    for (int i = 0; i < dialRetryMins.length; i++) {
      dialRetryMins[i] = xml.dialRetryIntervalSecs.at(i).getValue() / 60;
    }
    setDialRetryIntervalMins(dialRetryMins);
  }
  
  @Override
  public void copyConfigFrom(ICommsDevice device) {
    super.copyConfigFrom(device);
    if(device instanceof PAKNETModem) {
      PAKNETModem modem = (PAKNETModem) device;
      setConnectionInactivityTimeout(modem.getConnectionInactivityTimeout());
      setConnectTimeout             (modem.getConnectTimeout             ());
      setDialRetryIntervalMins      (modem.getDialRetryIntervalMins      ());
      setModemInactivityTimeout     (modem.getModemInactivityTimeout     ());
      setRegularCallInterval        (modem.getRegularCallInterval        ());
      
      try {
        setDialRetryIntervalStr       (modem.getDialRetryIntervalStr       ());
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }
  }
}
