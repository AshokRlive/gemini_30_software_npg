/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManagerImpl;
import com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms.AddCommsDeviceWizard;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.PortsManagerImpl;


/**
 *
 */
public class AddCommsDeviceWizardTest {

  public static void main(String[] args) {
    PortsManager ports = new PortsManagerImpl(null);
    CommsDeviceManager comms = new CommsDeviceManagerImpl(null);
    AddCommsDeviceWizard.showAddCommsDeviceWizard(ports, comms);
    
//    AddCommsDeviceWizard.createPstnWizard(null,null).show();
  }

}

