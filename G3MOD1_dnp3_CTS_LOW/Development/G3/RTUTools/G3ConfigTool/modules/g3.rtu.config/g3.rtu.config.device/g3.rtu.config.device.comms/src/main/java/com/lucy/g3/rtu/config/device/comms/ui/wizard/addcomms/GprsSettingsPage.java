/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.GPRSModem;


class GprsSettingsPage extends AbstractSettingsPage {
  public static final String KEY_HOST = GPRSModem.PROPERTY_HOST;
  public static final String KEY_USER = GPRSModem.PROPERTY_USER;
  public static final String KEY_PASS = GPRSModem.PROPERTY_PASS;
  
  
  public GprsSettingsPage (){
    super(CommsDeviceType.GPRS_MODEM);
    
    initComponents();
  }
  
  private void initComponents() {
    addTextfield("Access Point Name:", KEY_HOST);
    addTextfield("User Name:" ,KEY_USER);
    addTextfield("Password:", KEY_PASS);
  }
  
}

