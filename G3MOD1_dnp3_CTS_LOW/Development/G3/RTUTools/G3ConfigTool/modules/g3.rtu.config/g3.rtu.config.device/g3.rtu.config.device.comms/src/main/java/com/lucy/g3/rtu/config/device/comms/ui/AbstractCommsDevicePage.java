/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui;

import javax.swing.ListModel;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.ObjectToStringConverter;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


abstract class AbstractCommsDevicePage extends AbstractConfigPage {
  private final ICommsDevice commsDevice;
  
  AbstractCommsDevicePage(ICommsDevice commsDevice) {
    super(commsDevice, commsDevice.getDeviceName());
    this.commsDevice = commsDevice;
    
    // Bind page node text and title to device name.
    PropertyConnector.connect(commsDevice, ICommsDevice.PROPERTY_DEVICE_NAME, this, PROPERTY_NODE_NAME).updateProperty2();
    PropertyConnector.connect(commsDevice, ICommsDevice.PROPERTY_DEVICE_NAME, this, PROPERTY_TITLE).updateProperty2();
  }
  
  ListModel<ISerialPort> getPortList() {
    IConfig conf = commsDevice.getManager().getOwner();
    PortsManager portMgr = conf.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    return portMgr.getSerialManager().getItemListModel();
  } 
  
  static ValueModel convertToString(ValueModel vm){
    return new ConverterValueModel(vm, new ObjectToStringConverter());
  }

}

