/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManagerImpl;
import com.lucy.g3.rtu.config.device.comms.domain.GPRSModem;
import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.device.comms.domain.PSTNModem;
import com.lucy.g3.rtu.config.device.comms.ui.CommsDeviceManagerPage;
import com.lucy.g3.rtu.config.device.comms.ui.GprsPage;
import com.lucy.g3.rtu.config.device.comms.ui.PaknetPage;
import com.lucy.g3.rtu.config.device.comms.ui.PstnPage;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class CommsDevicePlugin implements IConfigPlugin, IConfigModuleFactory, IPageFactory{
  
  private CommsDevicePlugin () {}
  private final static CommsDevicePlugin  INSTANCE = new CommsDevicePlugin();
  
  
  public static CommsDevicePlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory.getInstance().registerFactory(CommsDeviceManager.CONFIG_MODULE_ID, INSTANCE);
    
    PageFactories.registerFactory(PageFactories.KEY_COMMS_DEVICE, INSTANCE);
  }
  
  @Override
  public CommsDeviceManager create(IConfig owner) {
    return new CommsDeviceManagerImpl(owner);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof CommsDeviceManager) {
      IConfig config = ((CommsDeviceManager)data).getOwner();
      PortsManager ports = config.getConfigModule(PortsManager.CONFIG_MODULE_ID);
       return new CommsDeviceManagerPage(ports, (CommsDeviceManager) data);

    } else if (data instanceof PAKNETModem) {
        return new PaknetPage((PAKNETModem) data);

    } else if (data instanceof PSTNModem) {
      return new PstnPage((PSTNModem) data);
      
    } else if (data instanceof GPRSModem) {
      return new GprsPage((GPRSModem) data);
    }
    
    return null;
  }
}

