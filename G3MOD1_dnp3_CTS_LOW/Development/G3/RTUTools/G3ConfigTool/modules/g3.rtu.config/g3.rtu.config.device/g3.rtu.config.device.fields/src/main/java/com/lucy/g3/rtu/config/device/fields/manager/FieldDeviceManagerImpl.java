/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.device.shared.AbstractDeviceManager;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.module.shared.util.ModuleFinder;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of {@linkplain FieldDeviceManager}.
 */
public final class FieldDeviceManagerImpl extends AbstractDeviceManager<IFieldDevice>
    implements FieldDeviceManager {

  private final Logger log = Logger.getLogger(FieldDeviceManagerImpl.class);

  private final HashMap<MODULE, ArrayListModel<IFieldDevice>> deviceMap;
  private final IModuleManager<IFieldDevice> moduleMgr;


  public FieldDeviceManagerImpl(IConfig owner) {
    super(owner);
    
    deviceMap = new HashMap<MODULE, ArrayListModel<IFieldDevice>>();
    
    /*Create module manager.*/
    ModuleRepository repo = null;
    if(owner != null) {
      repo =  owner.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    }
    
    @SuppressWarnings("unchecked")
    ArrayListModel<IFieldDevice> listmodel = (ArrayListModel<IFieldDevice>) getItemListModel();
    moduleMgr = new FieldModuleManager(owner, repo, listmodel);
  }
  
  public IModuleManager<IFieldDevice> getModuleManager() {
    return moduleMgr;
  }
  
  @Override
  protected void postAddAction(IFieldDevice newDevice) {
    getDeviceList(newDevice.getType()).add(newDevice);
    updateAllDeviceID(newDevice.getType());
    log.info("Added new field device: " + newDevice);
  }

  @Override
  protected void postRemoveAction(IFieldDevice removedDevice) {
    getDeviceList(removedDevice.getType()).remove(removedDevice);
    updateAllDeviceID(removedDevice.getType());
    log.info("Removed field device: " + removedDevice);
  }

  private void updateAllDeviceID(MODULE deviceType) {
    ArrayListModel<IFieldDevice> list = getDeviceList(deviceType);
    if (list != null) {
      for (int i = 0; i < list.size(); i++) {
        list.get(i).setDeviceId(MODULE_ID.forValue(i));
      }
    }
  }

  @Override
  public MasterProtocolManager getMasterProtoManager() {
    IConfig owner = getOwner();
    return (MasterProtocolManager) 
        (owner == null ? null : owner.getConfigModule(MasterProtocolManager.CONFIG_MODULE_ID));
  }

  @Override
  public Collection<IFieldDevice> getDevicesByType(MODULE... types) {
    ArrayList<IFieldDevice> alldevs = new ArrayList<IFieldDevice>();
    if (types != null) {
      for (int i = 0; i < types.length; i++) {
        alldevs.addAll(getDeviceList(types[i]));
      }
    }

    return alldevs;
  }

  @Override
  public IFieldDevice getDevice(MODULE type, MODULE_ID id) {
    return ModuleFinder.findModule(getAllItems(), type, id);
  }

  /**
   * Finds available device id.
   */
  public MODULE_ID nextAvailableDeviceId(MODULE type) {
    ArrayListModel<IFieldDevice> deviceList = getDeviceList(type);
    if (deviceList != null) {
      return MODULE_ID.forValue(deviceList.size());
    }

    return null;
  }

  private ArrayListModel<IFieldDevice> getDeviceList(MODULE type) {
    if (type == null) {
      return null;
    }

    ArrayListModel<IFieldDevice> devlist = deviceMap.get(type);
    if (devlist == null) {
      devlist = new ArrayListModel<IFieldDevice>();
      deviceMap.put(type, devlist);
    }

    return devlist;
  }

  @Override
  public int getDeviceSize() {
    return getSize();
  }

  // ================== Delegate Methods ===================

  @Override
  public Collection<IFieldDevice> getAllModules() {
    return getModuleManager().getAllModules();
  }

  @Override
  public Collection<IFieldDevice> getModulesByType(MODULE... types) {
    return getModuleManager().getModulesByType(types);
  }

  @Override
  public IFieldDevice getModule(MODULE type, MODULE_ID id) {
    return getModuleManager().getModule(type, id);
  }

//  @Override
//  public IChannel getChannel(MODULE mtype, MODULE_ID moduleID,
//      ChannelType type, int channelID) {
//    return moduleMgr.getChannel(mtype, moduleID, type, channelID);
//  }
//
//  @Override
//  public Collection<IChannel> getAllChannels(ChannelType... type) {
//    return moduleMgr.getAllChannels(type);
//  }
//
//  @Override
//  public Collection<IChannel> getAllChannels(MODULE[] mtypes,
//      ChannelType... type) {
//    return moduleMgr.getAllChannels(mtypes, type);
//  }
//  
  @Override
  public Collection<SwitchModuleOutput> getAllSwitchModuleOutputs() {
    return getModuleManager().getAllSwitchModuleOutputs();
  }

  @Override
  public Module addModule(MODULE type, MODULE_ID module_ID) {
    //REVIEW
    throw new UnsupportedOperationException("Not supported");
  }


}
