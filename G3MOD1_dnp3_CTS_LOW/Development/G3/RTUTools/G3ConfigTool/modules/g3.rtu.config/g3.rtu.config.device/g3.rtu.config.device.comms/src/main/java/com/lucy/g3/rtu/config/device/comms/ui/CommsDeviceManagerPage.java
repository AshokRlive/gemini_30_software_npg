/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.shared.base.page.ItemListManagerPage;

public class CommsDeviceManagerPage extends ItemListManagerPage {

  public CommsDeviceManagerPage(PortsManager ports, CommsDeviceManager manager) {
    super(manager,
        "Comms Devices",
        null,
        new CommsDeviceManagerModel(ports, manager));
  }
  
  

}
