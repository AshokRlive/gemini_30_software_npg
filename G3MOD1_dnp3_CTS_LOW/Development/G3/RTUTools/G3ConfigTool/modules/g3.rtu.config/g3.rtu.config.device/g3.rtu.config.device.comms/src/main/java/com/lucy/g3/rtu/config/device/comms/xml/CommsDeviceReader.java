/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.xml;

import java.beans.PropertyVetoException;

import com.g3schema.ns_comms.CommDevicesT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.GPRSModem;
import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.device.comms.domain.PSTNModem;
import com.lucy.g3.rtu.config.port.PortsManager;

/**
 * The XML reader for CommsDevice.
 */
public final class CommsDeviceReader {

  private final CommsDeviceManager deviceManager;
  private final PortsManager ports;
  private final ValidationResult result;


  public CommsDeviceReader(CommsDeviceManager manager, PortsManager ports, ValidationResult result) {
    this.deviceManager = Preconditions.checkNotNull(manager, "manager must not be null");
    this.ports = Preconditions.checkNotNull(ports, "ports must not be null");
    this.result = result == null ? new ValidationResult() : result;
  }

  public void readCommsDevices(CommDevicesT commDevicesType) throws PropertyVetoException {
    int id = 0;

    /* Read GPRS Modem */
    for (int i = 0; i < commDevicesType.GPRSModem.count(); i++) {
      GPRSModem modem = new GPRSModem(id++);
      modem.readFromXML(commDevicesType.GPRSModem.at(i), ports);
      deviceManager.add(modem);
      result.addInfo("Read GPRS modem: " + modem);
    }

    /* Read PSTN Modem */
    for (int i = 0; i < commDevicesType.PSTNModem.count(); i++) {
      PSTNModem modem = new PSTNModem(id++);
      modem.readFromXML(commDevicesType.PSTNModem.at(i), ports);
      deviceManager.add(modem);
      result.addInfo("Read PSTN modem: " + modem);
    }

    /* Read Paknet */
    for (int i = 0; i < commDevicesType.PAKNETModem.count(); i++) {
      PAKNETModem modem = new PAKNETModem(id++);
      modem.readFromXML(commDevicesType.PAKNETModem.at(i), ports);
      deviceManager.add(modem);
      result.addInfo("Read PAKNET modem: " + modem);
    }
  }

}
