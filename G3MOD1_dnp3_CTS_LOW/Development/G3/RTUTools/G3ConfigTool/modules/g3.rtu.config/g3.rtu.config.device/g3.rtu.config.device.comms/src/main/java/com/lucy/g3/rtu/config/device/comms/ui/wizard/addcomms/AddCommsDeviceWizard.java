/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JPanel;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.port.PortsManager;

/**
 *
 */
public class AddCommsDeviceWizard extends WizardBranchController {

  private final HashMap<CommsDeviceType, Wizard> subWizards = new HashMap<>();
  private final PortsManager ports;
  private final CommsDeviceManager manager;

  private AddCommsDeviceWizard(PortsManager ports, CommsDeviceManager manager) {
    super("Add A New Comms Device", new WizardPage[] {
        new SelectCommsTypePage()
    });
    
    this.ports = ports;
    this.manager = manager;
  }

  @Override
  protected Wizard getWizardForStep(String step, Map settings) {
    CommsDeviceType type = (CommsDeviceType) settings.get(SelectCommsTypePage.KEY_SELECTED_TYPE);
    Wizard wiz = null;

    if (type != null) {
      wiz = subWizards.get(type);
      if (wiz == null) {
        wiz = createWizard(type);
        subWizards.put(type, wiz);
      }

    }

    return wiz;
  }

  private Wizard createWizard(CommsDeviceType type) {
    Wizard wiz = null;
    switch (type) {
    case GPRS_MODEM:
      wiz = createGprsWizard(ports, manager);
      break;

    case PAKNET_MODEM:
      wiz = createPaknetWizard(ports, manager);
      break;

    case PSTN_MODEM:
      wiz = createPstnWizard(ports, manager);
      break;

    default:
      break;
    }

    if (wiz == null) {
      wiz = WizardPage.createWizard(new WizardPage[] {
          WizardUtils.createMessagePage(
              "Unsupported Type",
              "The selected comms device is not supported")
      });
    }

    return wiz;
  }

  public static ICommsDevice showAddCommsDeviceWizard(PortsManager ports, CommsDeviceManager manager) {
    Wizard wiz = new AddCommsDeviceWizard(ports,manager).createWizard();

    return (ICommsDevice) WizardDisplayer.showWizard(wiz, WizardUtils.createRect(
        WindowUtils.getMainFrame(), 580, 350));
  }

  public static Wizard createPaknetWizard(PortsManager ports, CommsDeviceManager manager) {
    CommsDeviceType type = CommsDeviceType.PAKNET_MODEM;
    Wizard wiz = WizardPage.createWizard(getWizardTitle(type),
        new WizardPage[] {
            new SelectPortPage(ports),
            new PaknetSettingsPage()
        }, new AddCommsDeviceResult(type, ports, manager));
    return wiz;
  }

  public static Wizard createGprsWizard(PortsManager ports, CommsDeviceManager manager) {
    CommsDeviceType type = CommsDeviceType.GPRS_MODEM;
    Wizard wiz = WizardPage.createWizard(getWizardTitle(type),
        new WizardPage[] {
            new SelectPortPage(ports),
            new GprsSettingsPage()
        }, new AddCommsDeviceResult(type, ports, manager));
    return wiz;
  }

  public static Wizard createPstnWizard(PortsManager ports, CommsDeviceManager manager) {
    CommsDeviceType type = CommsDeviceType.PSTN_MODEM;
    Wizard wiz = WizardPage.createWizard(getWizardTitle(type),
        new WizardPage[] {
            new SelectPortPage(ports),
            new PstnSettingsPage()
        }, new AddCommsDeviceResult(type, ports, manager));
    return wiz;
  }

  private static String getWizardTitle(CommsDeviceType type) {
    return "Add A " + type.getDescription();
  }

  static DefaultFormBuilder createBuilder(JPanel panel) {
    return new DefaultFormBuilder(new FormLayout(
        "[50dlu,default], 3dlu, [50dlu,default]:grow, 3dlu, default",
        ""), panel);
  }
}
