package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;


class SelectProtocolPage extends AbstractPage {

  static final String KEY_SELECTED_PROTOCOL_TYPE=  "selectedProtocolType";
  static final String ID =  "SelectProtocolPageID";
  
  private final ProtocolType[] types;
  
  private ButtonGroup group;
  
  public SelectProtocolPage(ProtocolType[] types) {
    super(ID,"Select a protocol");
    this.types = Preconditions.checkNotNull(types,"types must not be null");
    
    initComponents();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    /* Validate protocol*/
    if(group.getSelection() == null) {
      return "Please select a protocol type!";
    }
    
    return null;
  }

  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);
    
    group = new ButtonGroup();
    JRadioButton radio;
    
    int i = 0;
    for (ProtocolType type : types) {
      if(type == null)
        continue;
      
      radio = new JRadioButton(type.getName());
      builder.append(i == 0 ? "Protocol:" : "", radio);
      builder.nextLine();
      group.add(radio);
      radio.setName(type.name());
      
      radio.addActionListener(new ActionListener() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          JRadioButton r = (JRadioButton) e.getSource();
          if(r.isSelected()) {
            putWizardData(KEY_SELECTED_PROTOCOL_TYPE, ProtocolType.valueOf(r.getName()));
          }
        }
      });
      
      // Select the first
      if(i == 0) {
        radio.setSelected(true);
        putWizardData(KEY_SELECTED_PROTOCOL_TYPE, type);
      }
      
      i ++;
    }
  }

}

