/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;

/**
 * The Class StandardModemTest.
 */
public class PAKNETModemTest {

  private PAKNETModem fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new PAKNETModem(0);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetRetryInterval() throws ParseException {
    fixture.setDialRetryIntervalStr("1,5,10,30");
    assertArrayEquals(new long[] { 1, 5, 10, 30 }, fixture.getDialRetryIntervalMins());
  }

  @Test
  public void testSetRetryInterval2() {
    fixture.setDialRetryIntervalMins(new long[] { 1, 5, 10, 30 });
    assertEquals("1,5,10,30", fixture.getDialRetryIntervalStr());
  }

}
