/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.device.fields.domain.FieldDeviceFactory;
import com.lucy.g3.rtu.config.device.fields.domain.MMBDevice;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;

public class MMBDeviceTest {

  private MMBDevice fixture;


  @Before
  public void setUp() throws Exception {
    MMBChannel chnl = new MMBChannel(new MMB(FieldDeviceFactory.INSTANCE), null);
    fixture = (MMBDevice) chnl.addSession("").getDevice();
  }

  @Test
  public void testGetDeviceName() {
    final String defaultName = fixture.getShortName();
    assertNotNull("default name is should  NOT be null ",
        defaultName);

    // Change device name
    fixture.setDeviceName("customname");
    assertEquals("customname", fixture.getDeviceName());
    assertEquals("customname", fixture.getShortName());

    fixture.setDeviceName(null);
    assertNull(fixture.getDeviceName());
    assertNotNull(fixture.getShortName());
  }

}
