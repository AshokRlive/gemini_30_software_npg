/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.utils;

import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;

/**
 * Utility for finding comms devices from a collection of comms devices.
 */
public class CommsDeviceFinder {

  public static <T extends ICommsDevice> T findCommsDevice(
      Collection<T> devices, CommsDeviceType type, int... id) {
    Preconditions.checkNotNull(devices, "devices is null");

    for (T d : devices) {
      for (int i = 0; i < id.length; i++) {
        if (d != null
            && d.getDeviceId() == id[i]
            && type == d.getDeviceType()) {
          return d;
        }
      }
    }

    return null;
  }

  public static <T extends ICommsDevice> T findCommsDevice(
      Collection<T> devices, int... id) {
    Preconditions.checkNotNull(devices, "devices is null");

    for (T d : devices) {
      for (int i = 0; i < id.length; i++) {
        if (d != null
            && d.getDeviceId() == id[i]) {
          return d;
        }
      }
    }

    return null;
  }
}
