/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.jdesktop.swingx.prompt.PromptSupport;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.configtool.config.template.ITemplate;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;

class SelectTemplatePage extends AbstractPage {
  public  static final String ID = "SelectTemplatePageID";
  
  private static final String TEXT_CREATE_A_NEW_DEVICE = "Create a new device...";
  
  private final ITemplate[] templates;
  
  public static final String KEY_DEVICE_TEMPLATE = "deviceTemplate"; 
  public static final String KEY_DEVICE_NAME     = "deviceName"; 
  public static final String KEY_DEVICE_ADDRESS  = "deviceAddress"; 

  private JComboBox<ITemplate> combTemplate;

  private JTextField tfDeviceName;
  private JTextField tfDeviceAddress;
  
  SelectTemplatePage(ITemplate[] templates) {
    super(ID, "Select a device");
    
    this.templates = templates;
    initComponents();
    updateDeviceName();
  }
  
  private void updateDeviceName() {
    ITemplate template = (ITemplate) combTemplate.getSelectedItem();
    tfDeviceName.setText(template == null ? "" :template.getName());
  }

  @SuppressWarnings("unchecked")
  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);
     
    combTemplate = new JComboBox<>(templates);
    combTemplate.insertItemAt(null, templates.length);
    combTemplate.setName(KEY_DEVICE_TEMPLATE);
    combTemplate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateDeviceName();        
      }
    });
    combTemplate.setRenderer(new DefaultListRenderer(new StringValue() {
      @Override
      public String getString(Object value) {
        if(value == null)
          return TEXT_CREATE_A_NEW_DEVICE ;
        
        return ((ITemplate)value).getDisplayText();
      }
    }));
    ComboBoxUtil.makeWider(combTemplate);
    builder.append("Device Model:", combTemplate);
    builder.nextLine();
    
    tfDeviceName = new JTextField();
    tfDeviceName.setName(KEY_DEVICE_NAME);
    PromptSupport.setPrompt("e.g. Modbus Device", tfDeviceName);
    builder.append("Device Name:", tfDeviceName);
    builder.nextLine();
    
    tfDeviceAddress = new JTextField("1");
    tfDeviceAddress.setName(KEY_DEVICE_ADDRESS);
    PromptSupport.setPrompt("Slave address of the device", tfDeviceAddress);
    builder.append("Device Address:", tfDeviceAddress);
    builder.nextLine();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    error = null;
    
    checkUnsignedLong(tfDeviceAddress, "Device Address");
    checkNotBlank(tfDeviceName, "Device Name");
    
    return error;
  }
  
  
}
