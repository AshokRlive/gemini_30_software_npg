
package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import com.lucy.g3.rtu.config.shared.base.wizard.WizardPageExt;


class AbstractPage extends WizardPageExt {
  
  public AbstractPage() {
    super();
  }

  public AbstractPage(boolean autoListen) {
    super(autoListen);
  }

  public AbstractPage(String stepDescription, boolean autoListen) {
    super(stepDescription, autoListen);
  }

  public AbstractPage(String stepId, String stepDescription, boolean autoListen) {
    super(stepId, stepDescription, autoListen);
  }

  public AbstractPage(String stepId, String stepDescription) {
    super(stepId, stepDescription);
  }

  public AbstractPage(String stepDescription) {
    super(stepDescription);
  }
}

