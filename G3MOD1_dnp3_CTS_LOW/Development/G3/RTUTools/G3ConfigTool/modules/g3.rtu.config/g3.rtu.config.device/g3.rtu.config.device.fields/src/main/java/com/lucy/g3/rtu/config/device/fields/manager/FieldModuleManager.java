
package com.lucy.g3.rtu.config.device.fields.manager;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.device.fields.domain.IFieldDevice;
import com.lucy.g3.rtu.config.module.iomodule.manager.AbstractIOModuleManager;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.manager.IModuleManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

final class FieldModuleManager
    extends AbstractIOModuleManager<IFieldDevice>
    implements IModuleManager<IFieldDevice> {

  FieldModuleManager(IConfig owner, ModuleRepository repo, ArrayListModel<IFieldDevice> items) {
    super(owner, repo, items);
  }

  @Override
  public Module addModule(MODULE type, MODULE_ID module_ID) {
    //REVIEW
    throw new UnsupportedOperationException("Not supported");
  }
}
