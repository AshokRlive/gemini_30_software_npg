/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.device.comms.domain.GPRSModem;
import com.lucy.g3.rtu.config.port.ui.common.SerialPortSelectPanel;
public class GprsPage extends AbstractCommsDevicePage {

  private PresentationModel<GPRSModem> pm;
  
  public GprsPage(GPRSModem gprs) {
    super(gprs);
    this.pm = new PresentationModel<>(gprs);
  }

  @Override
  protected void init() throws Exception {
    initComponents();
  }

  private void createUIComponents() {
    panelSerial = new SerialPortSelectPanel(pm.getModel(GPRSModem.PROPERTY_SERIAL_PORT), getPortList());
    
    tfName = BasicComponentFactory.createTextField(pm.getModel(GPRSModem.PROPERTY_DEVICE_NAME),false);
    tfId = BasicComponentFactory.createTextField(convertToString(pm.getModel(GPRSModem.PROPERTY_DEVICE_ID)),false);
    tfType = BasicComponentFactory.createTextField(convertToString(pm.getModel(GPRSModem.PROPERTY_DEVICE_TYPE)),false);
    tfAPN = BasicComponentFactory.createTextField(pm.getModel(GPRSModem.PROPERTY_HOST),false);
    tfPass = BasicComponentFactory.createTextField(pm.getModel(GPRSModem.PROPERTY_PASS),false);
    tfUser = BasicComponentFactory.createTextField(pm.getModel(GPRSModem.PROPERTY_USER),false);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    panel4 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    panel2 = new JPanel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane1 ========
      {
        scrollPane1.setBorder(null);

        //======== panel1 ========
        {
          panel1.setBorder(Borders.DIALOG_BORDER);
          panel1.setLayout(new FormLayout(
              "[100dlu,default]",
              "2*(default, $ugap), default"));

          //======== panel4 ========
          {
            panel4.setBorder(new CompoundBorder(
                new TitledBorder("Basic"),
                Borders.DLU2_BORDER));
            panel4.setLayout(new FormLayout(
                "[100dlu,default], $lcgap, [80dlu,default]",
                "2*(default, $lgap), default"));

            //---- label1 ----
            label1.setText("Name:");
            panel4.add(label1, CC.xy(1, 1));
            panel4.add(tfName, CC.xy(3, 1));

            //---- label2 ----
            label2.setText("Device ID:");
            panel4.add(label2, CC.xy(1, 3));

            //---- tfId ----
            tfId.setEditable(false);
            tfId.setEnabled(false);
            panel4.add(tfId, CC.xy(3, 3));

            //---- label3 ----
            label3.setText("Device Type:");
            panel4.add(label3, CC.xy(1, 5));

            //---- tfType ----
            tfType.setEditable(false);
            tfType.setEnabled(false);
            panel4.add(tfType, CC.xy(3, 5));
          }
          panel1.add(panel4, CC.xy(1, 1));

          //======== panel2 ========
          {
            panel2.setBorder(new CompoundBorder(
                new TitledBorder("Authentication"),
                Borders.DLU2_BORDER));
            panel2.setLayout(new FormLayout(
                "[100dlu,default], $lcgap, [80dlu,default]",
                "2*(default, $lgap), default"));

            //---- label4 ----
            label4.setText("APN Host:");
            panel2.add(label4, CC.xy(1, 1));
            panel2.add(tfAPN, CC.xy(3, 1));

            //---- label5 ----
            label5.setText("User Name:");
            panel2.add(label5, CC.xy(1, 3));
            panel2.add(tfUser, CC.xy(3, 3));

            //---- label6 ----
            label6.setText("Password:");
            panel2.add(label6, CC.xy(1, 5));
            panel2.add(tfPass, CC.xy(3, 5));
          }
          panel1.add(panel2, CC.xy(1, 3));

          //---- panelSerial ----
          panelSerial.setBorder(new CompoundBorder(
              new TitledBorder("Port"),
              Borders.DLU2_BORDER));
          panel1.add(panelSerial, CC.xy(1, 5));
        }
        scrollPane1.setViewportView(panel1);
      }
      tabbedPane1.addTab("Configuration", scrollPane1);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JScrollPane scrollPane1;
  private JPanel panel1;
  private JPanel panel4;
  private JLabel label1;
  private JTextField tfName;
  private JLabel label2;
  private JTextField tfId;
  private JLabel label3;
  private JTextField tfType;
  private JPanel panel2;
  private JLabel label4;
  private JTextField tfAPN;
  private JLabel label5;
  private JTextField tfUser;
  private JLabel label6;
  private JTextField tfPass;
  private SerialPortSelectPanel panelSerial;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
