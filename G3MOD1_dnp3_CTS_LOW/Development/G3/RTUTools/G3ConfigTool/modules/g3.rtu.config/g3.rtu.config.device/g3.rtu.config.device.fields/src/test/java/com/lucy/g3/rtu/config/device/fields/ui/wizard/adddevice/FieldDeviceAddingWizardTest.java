
package com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice;

import static org.junit.Assert.assertNotNull;

import javax.swing.JDialog;

import org.fest.assertions.Fail;
import org.fest.swing.core.matcher.JButtonMatcher;
import org.fest.swing.finder.WindowFinder;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.testing.FestSwingTestCaseTemplate;
import org.jdesktop.swingx.util.OS;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.config.device.fields.ui.wizard.adddevice.FieldDeviceAddingWizard;
import com.lucy.g3.rtu.config.port.PortsManagerImpl;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManagerImpl;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;

@Ignore
public class FieldDeviceAddingWizardTest extends FestSwingTestCaseTemplate {
  private Throwable error;
  
  @Before
  public void windowsOnly() {
    org.junit.Assume.assumeTrue(OS.isWindows());
  }

  @Before
  public void setup() {
    setUpRobot();
  }

  @Test
  public void testWizard() {
    error = null;
    new Thread(new Runnable() {
      @Override
      public void run() {
        try{
          showTestWizard();
        }catch(Throwable e){
          error = e;
        }
      }
    }).start();

    DialogFixture wizDlg = WindowFinder.findDialog(JDialog.class).withTimeout(1000).using(robot());
    assertNotNull(wizDlg.target);

    // find name from NavButtonManager;
    wizDlg.radioButton(ProtocolType.MMB.name()).click();
    wizDlg.button(JButtonMatcher.withName("next")).requireEnabled();
    wizDlg.button(JButtonMatcher.withName("next")).click();
    wizDlg.button(JButtonMatcher.withName("next")).click();
    wizDlg.button(JButtonMatcher.withName("next")).click();
    wizDlg.button(JButtonMatcher.withName("next")).requireDisabled();
    wizDlg.button(JButtonMatcher.withName("finish")).click();
    
    if(error != null)
      Fail.fail(error.getMessage(), error);
    
    wizDlg.requireNotVisible();
  }

  private static void showTestWizard() {
    MasterProtocolManagerImpl manager = new MasterProtocolManagerImpl(null);
    PortsManagerImpl ports = new PortsManagerImpl(null);
    FieldDeviceAddingWizard.showWizard(manager, ports, WizardResultProducer.NO_OP);
  }
  
  public static void main(String[] args) {
    showTestWizard();
  }

}
