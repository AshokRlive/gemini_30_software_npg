/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.ui.wizard.addcomms;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.PSTNModem;


class PstnSettingsPage extends AbstractSettingsPage {
  public static final String KEY_INITSTR   = PSTNModem.PROPERTY_INITSTR  ;
  public static final String KEY_DIAL      = PSTNModem.PROPERTY_DIALSTR;
  public static final String KEY_DIAL2     = PSTNModem.PROPERTY_DIAL2 ;
  
  public PstnSettingsPage (){
    super(CommsDeviceType.PSTN_MODEM);
    
    initComponents();
    validateContents(null, null);
  }
  private void initComponents() {
    addTextfield("Initialisation String:", KEY_INITSTR);
    addTextfield("Primary Dialling String:", KEY_DIAL);
    addTextfield("Secondary Dialling String:", KEY_DIAL2);
  }
  
  
  
 
}

