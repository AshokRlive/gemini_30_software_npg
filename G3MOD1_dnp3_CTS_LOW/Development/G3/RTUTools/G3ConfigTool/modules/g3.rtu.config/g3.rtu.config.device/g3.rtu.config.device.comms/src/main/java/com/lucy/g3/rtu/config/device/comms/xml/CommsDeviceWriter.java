/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.comms.xml;

import java.util.ArrayList;
import java.util.Collection;

import com.g3schema.ns_comms.CommDevicesT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.GPRSModem;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.device.comms.domain.PSTNModem;

/**
 * The XML writer for CommsDevice.
 */
public final class CommsDeviceWriter {

  private final CommsDeviceManager manager;
  private final ValidationResult result;


  public CommsDeviceWriter(CommsDeviceManager manager, ValidationResult result) {
    this.manager = Preconditions.checkNotNull(manager, "manager must not be null");
    this.result = result == null ? new ValidationResult() : result;
  }

  public void write(CommDevicesT xml_commdev) {
    Collection<ICommsDevice> commsDevs = manager.getAllItems();

    ArrayList<GPRSModem> gprsModems = new ArrayList<GPRSModem>();
    ArrayList<PSTNModem> stdModems = new ArrayList<PSTNModem>();
    ArrayList<PAKNETModem> paknetModems = new ArrayList<PAKNETModem>();

    for (ICommsDevice dev : commsDevs) {
      if (dev instanceof GPRSModem) {
        gprsModems.add((GPRSModem) dev);
      } else if (dev instanceof PAKNETModem) {
        paknetModems.add((PAKNETModem) dev);
      } else if (dev instanceof PSTNModem) {
        stdModems.add((PSTNModem) dev);
      }

      // Check if port configured.
      if (dev.getSerialPort() == null) {
        result.addError(String.format("The serial port is not configured for Comms Device:\"%s\"",
            dev.getDisplayName()));
      }
    }

    for (GPRSModem m : gprsModems) {
      m.writeToXML(xml_commdev.GPRSModem.append());
    }

    for (PSTNModem m : stdModems) {
      m.writeToXML(xml_commdev.PSTNModem.append());
    }

    for (PAKNETModem m : paknetModems) {
      m.writeToXML(xml_commdev.PAKNETModem.append());
    }
  }

}
