/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.fields;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.device.fields.domain.FieldDeviceFactory;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManagerImpl;
import com.lucy.g3.rtu.config.device.fields.manager.MasterProtocolObserver;
import com.lucy.g3.rtu.config.device.fields.ui.FieldDeviceManagerPage;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class FieldDevicePlugin implements IConfigPlugin, IConfigModuleFactory, IPageFactory, IProtocolFactory{
  
  private FieldDevicePlugin () {}
  private final static FieldDevicePlugin  INSTANCE = new FieldDevicePlugin();
  
  
  public static FieldDevicePlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory.getInstance().registerFactory(FieldDeviceManager.CONFIG_MODULE_ID, INSTANCE);
    PageFactories.registerFactory(PageFactories.KEY_FIELD_DEVICE, INSTANCE);
    
    ProtocolFactories.registerFactory(ProtocolType.MMB, INSTANCE);
  }
  
  @Override
  public FieldDeviceManager create(IConfig owner) {
    FieldDeviceManagerImpl fieldMgr = new FieldDeviceManagerImpl(owner);
    
    /*
     * Observe the change of master protocol session changes, so related fields
     * device could be added/removed as necessary.
     */
    if(owner != null) {
      MasterProtocolManager masterMgr = owner.getConfigModule(MasterProtocolManager.CONFIG_MODULE_ID);
      masterMgr.registerObserver(new MasterProtocolObserver(fieldMgr));
    }
    return fieldMgr;
  }

  @Override
  public Page createPage(Object data) {
    if(data instanceof FieldDeviceManager){
      PortsManager ports = ((FieldDeviceManager) data).getOwner().getConfigModule(PortsManager.CONFIG_MODULE_ID);
      return new FieldDeviceManagerPage(((FieldDeviceManager) data), ports);
    }
    
    return null;
  }

  @Override
  public IMasterProtocol<?> createProtocol() {
    return new MMB(FieldDeviceFactory.INSTANCE);
  }
}

