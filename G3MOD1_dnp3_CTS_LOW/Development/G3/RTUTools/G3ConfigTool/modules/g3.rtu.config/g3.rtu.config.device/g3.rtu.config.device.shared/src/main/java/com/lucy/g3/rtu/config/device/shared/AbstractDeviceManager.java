/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.device.shared;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.shared.manager.AbstractListConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * The basic implementation of {@linkplain DeviceManager}.
 *
 * @param <DeviceT>
 *          the type of device
 */
public abstract class AbstractDeviceManager<DeviceT extends IDevice> extends AbstractListConfigModule<DeviceT>
    implements DeviceManager<DeviceT> {

  protected AbstractDeviceManager(IConfig owner) {
    super(owner);
  }
  
  protected AbstractDeviceManager(IConfig owner, ArrayListModel<DeviceT> items) {
    super(owner, items);
  }

  @Override
  protected void postRemoveAction(DeviceT removedItem) {
    removedItem.delete();
  }

}
