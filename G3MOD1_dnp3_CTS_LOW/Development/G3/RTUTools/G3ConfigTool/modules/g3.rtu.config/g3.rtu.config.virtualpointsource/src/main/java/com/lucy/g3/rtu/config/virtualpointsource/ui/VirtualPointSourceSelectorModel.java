/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpointsource.ui;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ComboBoxModel;
import javax.swing.table.TableModel;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * Presentation model for {@linkplain VirtualPointSourceSelector}.
 */
class VirtualPointSourceSelectorModel {

  public static final int COLUMN_SELECTED = ChannelTableModel.COLUMN_SELECTED;

  private static Module lastSelectedModule;

  private final String MODULE_ITEM_ALL = "All Modules";

  private final SelectionInList<Object> moduleSelectionList;

  private final Collection<IVirtualPointSource> allChs;

  private final ChannelTableModel channelTableModel;


  public VirtualPointSourceSelectorModel(Collection<IVirtualPointSource> channels, IVirtualPointSource initialSelection) {
    Preconditions.checkNotNull(channels, "channels must not be null");
    allChs = Collections.unmodifiableCollection(channels);
    moduleSelectionList = new SelectionInList<Object>();

    // Gets all available owner modules of channels
    List<Object> mlist = moduleSelectionList.getList();
    mlist.add(MODULE_ITEM_ALL);
    for (IVirtualPointSource ch : channels) {
      if (ch != null) {
        Module owner = ch.getSourceModule();
        if (owner != null && !mlist.contains(owner)) {
          mlist.add(owner);
        }
      }
    }

    // Set initially selected module
    if (lastSelectedModule == null) {
      moduleSelectionList.setSelection(MODULE_ITEM_ALL);
    } else {
      moduleSelectionList.setSelection(lastSelectedModule);
    }

    // Initialise channel table model
    channelTableModel = new ChannelTableModel(allChs, initialSelection);
    setModuleFilter();

    // Observe module selection
    moduleSelectionList.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            setModuleFilter();
          }
        });
  }

  @SuppressWarnings("unchecked")
  public ComboBoxModel<Object> getModuleComboModel() {
    return new ComboBoxAdapter<Object>(moduleSelectionList);
  }

  public TableModel getChannelTableModel() {
    return channelTableModel;
  }

  public void clearSelection() {
    channelTableModel.clearSelection();
  }

  public boolean hasSelection() {
    return channelTableModel.hasSelection();
  }

  public IVirtualPointSource getSelection() {
    return channelTableModel.getSelection();
  }

  public Collection<IVirtualPointSource> getSelections() {
    return channelTableModel.getSelections();
  }

  public int getSelectionsNum() {
    return channelTableModel.getSelectionsNum();
  }

  /**
   * Change the mode of selection between single and multiple .
   *
   * @param selectionMode
   *          <li>0: for single selection <li>1: for multiple selection
   */
  public void setSelectionMode(SelectionMode selectionMode) {
    channelTableModel.setSelectionMode(selectionMode);
  }

  public void setMaxSelectionNum(int maxSelectionNum) {
    channelTableModel.setMaxSelectionNum(maxSelectionNum);
  }

  public int getMaxSelectionNum() {
    return channelTableModel.getMaxSelectionNum();
  }

  public SelectionMode getSelectionMode() {
    return channelTableModel.getSelectionMode();
  }

  public void setSelectAll(boolean selectAll) {
    if (selectAll) {
      channelTableModel.selectAll();
    } else {
      channelTableModel.clearSelection();
    }
  }

  private void setModuleFilter() {
    Object selModuleItem = moduleSelectionList.getSelection();

    if (selModuleItem != null && selModuleItem instanceof Module) {
      channelTableModel.setModuleFilter((Module) selModuleItem);
      lastSelectedModule = (Module) selModuleItem;
    } else {
      channelTableModel.setModuleFilter((Module) null);
      lastSelectedModule = null;
    }
  }

  public void setRegxFilter(String regex, boolean caseSensitive) {
    if (caseSensitive == false && regex != null && !regex.isEmpty())
    {
      regex = "(?i)" + regex; // convert regex to case sensitive
    }

    channelTableModel.setRegxFilter(regex);
  }

  public void toggleSelectionAtIndex(Component parentComponent, int rowInModel) {
    // if(checkReachMaxSelection()){
    // JOptionPane.showMessageDialog(parentComponent,
    // String.format("Cannot select more than %d channels",getMaxSelectionNum()),
    // "Error", JOptionPane.ERROR_MESSAGE);
    // return;
    // }

    channelTableModel.toggleSelectionAtIndex(rowInModel);
  }


  /**
   * Channel table model for channel selecting.
   */
  private static final class ChannelTableModel extends SelectionTableModel<IVirtualPointSource> {

    private static final String[] COLUMNS = {
        SelectionTableModel.COLUMN_TITLE_SEELCTED,
        /* "Group","Channel Type", "Channel ID","Channel Name" */"Name" };

    // Column indexes
    public static final int COLUMN_SELECTED = 0;
    // final static public int COLUMN_GROUP = 1;
    // final static public int COLUMN_TYPE = 2;
    // final static public int COLUMN_ID = 3;
    public static final int COLUMN_NAME = 1;

    private final Collection<IVirtualPointSource> allChs;

    // Filters
    private Matcher regexFilter;
    private Module selectedModule;

    // The maximum number of items user can select. No limit if it is -1
    private int maxSelectionNum = -1;


    public ChannelTableModel(Collection<IVirtualPointSource> allChs, IVirtualPointSource initialSelection) {
      super(allChs);
      this.allChs = allChs;
      setSelectionMode(SelectionMode.SELECTION_MODE_SINGLE);
      setItemSelected(initialSelection, true);
    }

    @Override
    public int getColumnCount() {
      return COLUMNS.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
      return COLUMNS[columnIndex];
    }

    private boolean checkReachMaxSelection() {
      // Check if reaches max selection number
      if (maxSelectionNum >= 0 && getSelectionMode() == SelectionMode.SELECTION_MODE_MULTI) {
        int selectionNum = getSelectionsNum();
        if (selectionNum >= maxSelectionNum) {
          return true;
        }
      }

      return false;
    }

    /**
     * Override to check maximum selection number.
     */
    @Override
    public void setItemSelected(IVirtualPointSource item, boolean select) {
      if (select == false || checkReachMaxSelection() == false) {
        super.setItemSelected(item, select);
      }
    }

    public int getMaxSelectionNum() {
      return maxSelectionNum;
    }

    /**
     * Sets the maximum number of channels user can select. This feature is
     * supported only in multi-selection mode.
     *
     * @param maxSelectionNum
     */
    public void setMaxSelectionNum(int maxSelectionNum) {
      this.maxSelectionNum = maxSelectionNum;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      IVirtualPointSource source = getRow(rowIndex);

      switch (columnIndex) {
      case COLUMN_SELECTED:
        return selectedItems.contains(source);

        // case COLUMN_TYPE:
        // return source.getType().getShortDescription();
        //
        // case COLUMN_GROUP :
        // return source.getGroup();
        //
        // case COLUMN_ID :
        // return source.getgetId();

      case COLUMN_NAME:
        return source.getName();

      default:
        throw new IllegalStateException("Unknown column");
      }
    }

    // @Override
    // public Class<?> getColumnClass(int columnIndex) {
    // if(columnIndex == COLUMN_ID) {
    // return Integer.class;
    // }
    // return super.getColumnClass(columnIndex);
    // }

    /**
     * Filter channel list using regular expression.
     *
     * @param regex
     */
    public void setRegxFilter(String regex) {
      if (regex != null && !regex.isEmpty()) {
        regexFilter = Pattern.compile(regex).matcher("");
      } else {
        regexFilter = null;
      }

      applyFilters();
    }

    /**
     * Filter channel list by module.
     */
    public void setModuleFilter(Module selectedModule) {
      this.selectedModule = selectedModule;

      applyFilters();
    }

    private void applyFilters() {
      itemListModel.clear();
      itemListModel.addAll(allChs);

      ArrayList<IVirtualPointSource> toRemove = new ArrayList<IVirtualPointSource>();

      /* Apply module filter */
      if (selectedModule != null) {
        for (IVirtualPointSource ch : allChs) {
          if (ch != null && ch.getSourceModule() != selectedModule) {
            toRemove.add(ch);
          }
        }
      }

      /* Apply regex filter */
      if (regexFilter != null) {
        for (IVirtualPointSource ch : itemListModel) {
          regexFilter.reset(ch.getName());
          if (!regexFilter.find()) {
            toRemove.add(ch);
          }
        }
      }

      /* Remove filtered channels from display model */
      itemListModel.removeAll(toRemove);

      /* Remove filtered channels from selected channels */
      selectedItems.removeAll(toRemove);

      fireTableDataChanged();
    }
  }
}
