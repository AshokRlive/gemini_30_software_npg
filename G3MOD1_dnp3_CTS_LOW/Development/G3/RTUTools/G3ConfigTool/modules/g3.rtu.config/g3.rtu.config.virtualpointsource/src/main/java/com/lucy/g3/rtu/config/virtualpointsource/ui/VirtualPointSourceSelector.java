/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpointsource.ui;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * A dialog for selecting {@link IVirtualPointSource}.
 */
public class VirtualPointSourceSelector extends AbstractDialog {

  static final String DEFAULT_TITLE = "Select a source";

  static final String ACTION_KEY_DESELECT = "deselect";

  private VirtualPointSourceSelectorModel pm;

  private final Collection<IVirtualPointSource> allSources;

  private ISourceSelectorInvoker invoker;

  private String description;

  private final IVirtualPointSource initialSelection;

  private ActionListener filterAL = new FilterActionListener();
  private Timer filterTimer = new Timer(500, filterAL);


  public VirtualPointSourceSelector(Dialog parent,
      IVirtualPointSource initialSelection,
      Collection<IVirtualPointSource> allChannels,
      String description) {
    this(parent, initialSelection, allChannels, null, description);
  }

  /**
   * Construct a selection dialog whose parent is a Dialog
   *
   * @param parent
   *          parent window
   * @param modules
   *          the available modules whose sources can be listed and selected.
   * @param initialSelection
   *          initial selected source
   * @param channelTypes
   *          of these types the source will be listed
   */
  public VirtualPointSourceSelector(Dialog parent,
      IVirtualPointSource initialSelection,
      Collection<IVirtualPointSource> allChannels,
      ISourceSelectorInvoker invoker,
      String description) {
    super(parent, DEFAULT_TITLE, true, SwingConstants.VERTICAL);
    this.allSources = Preconditions.checkNotNull(allChannels, "allChannels must not be null");
    this.invoker = invoker;
    this.description = description;
    this.initialSelection = initialSelection;
    initialise();
  }

  /**
   * Construct a selection dialog whose parent is a Dialog
   *
   * @param parent
   *          parent window
   * @param modules
   *          the available modules whose channels can be listed and selected.
   * @param initialSelect
   *          initial selected source
   * @param channelTypes
   *          of these types the source will be listed
   */
  public VirtualPointSourceSelector(Frame parent,
      IVirtualPointSource initialSelect,
      Collection<IVirtualPointSource> chList,
      ISourceSelectorInvoker invoker,
      String description) {
    super(parent, DEFAULT_TITLE, true, SwingConstants.VERTICAL);
    allSources = Preconditions.checkNotNull(chList, "chList must not be null");
    this.invoker = invoker;
    this.description = description;
    initialSelection = initialSelect;
    initialise();
  }

  private void initialise() {
    setName("VirtualPointSourceSelectorDialog");

    pm = new VirtualPointSourceSelectorModel(allSources, initialSelection);

    filterTimer.setRepeats(false);

    setSelectionMode(SelectionMode.SELECTION_MODE_SINGLE);

    pack();
  }

  private void createUIComponents() {
    combModules = new JComboBox<Object>(pm.getModuleComboModel());

    // Create source table
    channelsListTable = new JTable(pm.getChannelTableModel());

    // Adjust table column width
    UIUtils.packTableColumns(channelsListTable);
  }

  public void setSubtitle(String subTitle) {
    if (getBannerPanel() != null) {
      getBannerPanel().setSubtitle(subTitle);
    }
  }

  private void checkBoxCaseActionPerformed() {
    filterTimer.restart();
  }

  private void checkBoxSelAllActionPerformed() {
    pm.setSelectAll(checkBoxSelAll.isSelected());
  }

  private void channelsTableMousePressed(MouseEvent e) {
    int col = channelsListTable.columnAtPoint(e.getPoint());

    // No effect for "SELECT" column
    if (channelsListTable.convertColumnIndexToModel(col) == VirtualPointSourceSelectorModel.COLUMN_SELECTED) {
      return;
    }

    // Toggle a row selection
    int rowInView = channelsListTable.rowAtPoint(e.getPoint());
    if (rowInView >= 0) {
      int rowInModel = channelsListTable.convertRowIndexToModel(rowInView);
      pm.toggleSelectionAtIndex(this, rowInModel);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    labelModule = new JLabel();
    scrollPane1 = new JScrollPane();
    checkBoxSelAll = new JCheckBox();
    labelSearch = new JLabel();
    filterField = new JTextField();
    checkBoxCase = new JCheckBox();

    // ======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "left:default, $rgap, default:grow, $ugap, default",
          "default, $ugap, fill:default:grow, $lgap, default, $ugap, default"));

      // ---- labelModule ----
      labelModule.setText("Module:");
      contentPanel.add(labelModule, CC.xy(1, 1));
      contentPanel.add(combModules, CC.xy(3, 1));

      // ======== scrollPane1 ========
      {
        scrollPane1.setPreferredSize(new Dimension(100, 100));

        // ---- channelsListTable ----
        channelsListTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        channelsListTable.setName("channelsTable");
        channelsListTable.addMouseListener(new MouseAdapter() {

          @Override
          public void mousePressed(MouseEvent e) {
            channelsTableMousePressed(e);
          }
        });
        scrollPane1.setViewportView(channelsListTable);
      }
      contentPanel.add(scrollPane1, CC.xywh(1, 3, 5, 1));

      // ---- checkBoxSelAll ----
      checkBoxSelAll.setText("Select/Deselect All");
      checkBoxSelAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          checkBoxSelAllActionPerformed();
        }
      });
      contentPanel.add(checkBoxSelAll, CC.xywh(1, 5, 3, 1));

      // ---- labelSearch ----
      labelSearch.setText("Search:");
      contentPanel.add(labelSearch, CC.xy(1, 7));

      // ---- filterField ----
      filterField.setColumns(8);
      contentPanel.add(filterField, CC.xy(3, 7));

      // ---- checkBoxCase ----
      checkBoxCase.setText("Case Sensitive");
      checkBoxCase.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          checkBoxCaseActionPerformed();
        }
      });
      contentPanel.add(checkBoxCase, CC.xy(5, 7));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    updateVisibility();
  }

  private void initEventHandling() {
    // Handle the input of search box
    filterField.getDocument().addDocumentListener(new DocumentListener() {

      @Override
      public void removeUpdate(DocumentEvent e) {
        filterTimer.restart();
      }

      @Override
      public void insertUpdate(DocumentEvent e) {
        filterTimer.restart();
      }

      @Override
      public void changedUpdate(DocumentEvent e) {
        filterTimer.restart();
      }
    });
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel labelModule;
  private JComboBox<Object> combModules;
  private JScrollPane scrollPane1;
  private JTable channelsListTable;
  private JCheckBox checkBoxSelAll;
  private JLabel labelSearch;
  private JTextField filterField;
  private JCheckBox checkBoxCase;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected BannerPanel createBannerPanel() {
    BannerPanel banner = new BannerPanel();
    banner.setTitle(DEFAULT_TITLE);
    return banner;
  }

  @Override
  protected JComponent createContentPanel() {
    initComponents();
    initEventHandling();

    getBannerPanel().setSubtitle(description);

    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    Action cancelAction = getAction(ACTION_KEY_CANCEL);
    setDefaultCancelAction(cancelAction);
    Action OKAction = getAction(ACTION_KEY_OK);
    setDefaultAction(OKAction);
    JButton btnSelect = new JButton(OKAction);
    JButton btnCancel = new JButton(cancelAction);
    btnSelect.setText("Select");
    btnCancel.setText("Cancel");

    // Help
    JButton btnHelp = new JButton();
    Helper.register(btnHelp, VirtualPointSourceSelector.class);
    btnHelp.setIcon(Helper.getIcon());

    btnCancel.setIcon(null);
    JPanel buttonBar;
    if (initialSelection == null) {
      buttonBar = ButtonBarFactory.buildHelpBar(btnHelp, new JButton[] { btnSelect, btnCancel });
    } else {
      JButton btnDeselect = new JButton(getAction(ACTION_KEY_DESELECT));
      buttonBar = ButtonBarFactory.buildHelpBar(btnHelp, new JButton[] { btnDeselect, btnSelect, btnCancel });
    }
    return buttonBar;
  }

  /**
   * Change the mode of selection between single and multiple .
   */
  public void setSelectionMode(SelectionMode selectionMode) {
    pm.setSelectionMode(selectionMode);
    updateVisibility();
  }

  public void setMaxSelectionNum(int maxSelectionNum) {
    pm.setMaxSelectionNum(maxSelectionNum);
    updateVisibility();
  }

  private void updateVisibility() {
    /*
     * Hide check box "select all" when it is single selection mode and max
     * selection number is NOT set.
     */
    if (checkBoxSelAll != null) {
      checkBoxSelAll.setVisible(
          pm.getSelectionMode() == SelectionMode.SELECTION_MODE_MULTI
              && pm.getMaxSelectionNum() < 0);
    }
  }

  @org.jdesktop.application.Action
  public void deselect() {
    pm.clearSelection();
    if (invoker != null) {
      try {
        invoker.setChannel(null);
      } catch (Exception e) {
        Logger.getLogger(getClass()).error("Fail to deselect");
      }
    }

    super.ok();
    invoker = null;
  }

  @Override
  public void cancel() {
    pm.clearSelection();

    super.cancel();
    invoker = null;
  }

  @Override
  public void ok() {
    if (invoker != null) {
      try {
        invoker.setChannel(pm.getSelection());
      } catch (Exception e) {
        Logger.getLogger(getClass()).error("Fail to select:" + e.getMessage());
        JOptionPane.showMessageDialog(this, e.getMessage(),
            "Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }

    super.ok();
    invoker = null;
  }

  public IVirtualPointSource getSelection() {
    if (hasBeenCanceled()) {
      return null;
    } else {
      return pm.getSelection();
    }
  }

  public Collection<IVirtualPointSource> getSelections() {
    if (hasBeenCanceled()) {
      return new ArrayList<IVirtualPointSource>();
    } else {
      return pm.getSelections();
    }
  }

  /**
   * Shows the modal dialog that list available channels, and return a source
   * selected by user.
   *
   * @return the selected source or <code>null</code> if no source is selected.
   */
  public IVirtualPointSource showDialog() {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(this);
    } else {
      pack();
      setVisible(true);
    }

    return getSelection();
  }


  private class FilterActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      pm.setRegxFilter(filterField.getText(), checkBoxCase.isSelected());
    }
  }

  /**
   * Interface for setting a source value when the dialog is affirmed by user.
   */
  public interface ISourceSelectorInvoker {

    void setChannel(IVirtualPointSource ch) throws Exception;
  }
}
