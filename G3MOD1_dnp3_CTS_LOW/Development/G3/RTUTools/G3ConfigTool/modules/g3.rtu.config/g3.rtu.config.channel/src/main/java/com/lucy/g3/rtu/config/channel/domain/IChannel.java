/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import com.lucy.g3.rtu.config.channel.validation.IChannelValidator;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 * Physical IO channel of G3 module.
 */
public interface IChannel extends INode, IChannelParameters, IVirtualPointSource, IValidation {
  /**
   * Channel parameter name for parameter {@value} . <li>Value type:
   * {@link Long}.</li> <li>Supported by channel type: {@code ANALOG_INPUT}</li>
   */
  String PARAM_EVENT_MS = "Event (ms)";

  /**
   * Gets channel id.
   *
   * @return
   */
  int getId();

  /**
   * Gets the type of this channel.
   *
   * @return the type enumeration {@link ChannelType} of this channel.
   */
  ChannelType getType();

  /**
   * The group name of this channel.
   *
   * @return
   */
  String getGroup();

  /**
   * Gets the owner module of this channel.
   *
   * @return non-null module instance.
   */
  Module getOwnerModule();

  /**
   * Gets the enumeration which defines the default property of this channel.
   *
   * @return enum instance of this channel. Null if it is unavailable.
   */
  IChannelEnum getEnum();

  /**
   * Checks if this channel is read-only. Read-only channel cannot be removed or
   * modified by users.
   */
  boolean isReadOnly();

  /**
   * Gets predefined configuration for this channel.
   *
   * @return predefined configuration. Null if it is unavailable.
   */
  IPredefinedChannelConfig predefined();

  @Override
  IChannelValidator getValidator();
}
