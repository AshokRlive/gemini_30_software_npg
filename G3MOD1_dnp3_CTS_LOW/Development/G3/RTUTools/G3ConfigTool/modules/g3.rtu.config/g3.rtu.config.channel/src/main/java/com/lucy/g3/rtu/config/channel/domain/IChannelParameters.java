/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

/**
 * An object(e.g. Channel) that implements this interface will support
 * configurable parameters.
 * <p>
 * The names of available parameters can be retrieved using method
 * {@link #getParameterNames()}.
 * </p>
 * <p>
 * To modify or access the value of a parameter, using method
 * {@link #setParameter(String, Object)} and {@link #getParameter(String)}.
 * </p>
 */
interface IChannelParameters {

  /**
   * Sets the value of a parameter.
   *
   * @param paraName
   *          parameter name
   * @param value
   *          new value. &nbsp<strong>Note</strong> the type of new value must
   *          be the same as the type of current value.
   * @throws UnsupportParamException
   *           if the specified parameter name is not supported.
   * @throws InvalidParamTypeException
   *           if the given value's type is different from the existed parameter
   *           value type.
   * @throws NullPointerException
   *           if parameter value is null.
   * @throws IllegalArgumentException
   *           if paraName is blank.
   */
  void setParameter(String paraName, Object value)
      throws UnsupportParamException,
      InvalidParamTypeException,
      NullPointerException,
      IllegalArgumentException;

  /**
   * Gets the value of a parameter.
   *
   * @param paraName
   *          the name of the parameter to be retrieved.
   * @return the parameter's value object.
   * @throws UnsupportParamException
   *           if the specified parameter name is not supported.
   * @throws IllegalArgumentException
   *           if paraName is blank.
   */
  Object getParameter(String paraName)
      throws UnsupportParamException,
      IllegalArgumentException;

  /**
   * Checks if a parameter exists.
   *
   * @param paraName
   *          the parameter name to be checked.
   * @return <code>true</code> if exits. <code>false</code> otherwise.
   */
  boolean hasParameter(String paraName);

  /**
   * Gets names of all existing parameters.
   *
   * @return an non-null array of parameter names.
   */
  String[] getParameterNames();

  /**
   * Checks if a parameter can by modified by calling
   * {@linkplain #setParameter(String, Object)}.
   *
   * @param propertyName
   * @return
   */
  boolean isParameterModifiable(String parameterName);

  void setParameterModifiable(String parameterName, boolean modifiable);


  /**
   * This exception should be thrown when trying to set/get a parameter that is
   * not supported.
   */
  public class UnsupportParamException extends RuntimeException {

    public UnsupportParamException(String message) {
      super(message);
    }
  }

  /**
   * This exception should be thrown when trying to set a parameter's value in
   * wrong type.
   */
  public class InvalidParamTypeException extends RuntimeException {

    public InvalidParamTypeException(String message) {
      super(message);
    }
  }

}
