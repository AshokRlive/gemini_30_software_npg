/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.AbstractChannel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IPredefinedChannelConfig;
import com.lucy.g3.rtu.config.channel.validation.IChannelValidator;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.xml.gen.api.IChannelEnum;

public class AbstractChannelTest {

  final private Module dummyModule = new ModuleStub();

  private AbstractChannel fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new AbstractChannel(ChannelType.ANALOG_INPUT, dummyModule, "") {

      @Override
      public IPredefinedChannelConfig predefined() {
        return null;
      }

      @Override
      public int getId() {
        return 0;
      }

      @Override
      public String getGroup() {
        return null;
      }

      @Override
      public IChannelEnum getEnum() {
        return null;
      }

      @Override
      protected void handleConnectEvent(ConnectEvent event) {

      }

      @Override
      public IChannelValidator getValidator() {
        return null;
      }

      @Override
      public String getSourceName() {
        return null;
      }
    };
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testReadOnly() {
    // By default it should not be read only
    assertFalse(fixture.isReadOnly());

    fixture.setReadOnly(true);
    assertTrue(fixture.isReadOnly());
  }

}
