/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.OUTPUT_CHANNEL_TYPE;

public class ChannelTypeTest {

  ChannelType[] alltypes = ChannelType.values();


  @Test
  public void testDescriptionNotBlank() {
    for (ChannelType t : alltypes) {
      assertFalse("description should not be blank:" + t.name(),
          Strings.isBlank(t.getDescription()));
    }
  }

  @Test
  public void testShortDescriptionNotBlank() {
    for (ChannelType t : alltypes) {
      assertFalse("short description should not be blank:" + t.name(),
          Strings.isBlank(t.getShortDescription()));
    }
  }

  @Test
  public void testToStringNotBlank() {
    for (ChannelType t : alltypes) {
      assertFalse("toString should not be blank:" + t.name(),
          Strings.isBlank(t.toString()));
    }
  }

  @Test
  public void testConversion() {
    OUTPUT_CHANNEL_TYPE[] values = OUTPUT_CHANNEL_TYPE.values();
    for (int i = 0; i < values.length; i++) {
      assertNotNull("Conversion result should not be null:" + values[i].name(),
          ChannelType.convertEnum(values[i]));
    }
  }

}
