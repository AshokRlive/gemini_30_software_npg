/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelParameters;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.channel.domain.IChannelParameters.InvalidParamTypeException;
import com.lucy.g3.rtu.config.channel.domain.IChannelParameters.UnsupportParamException;

public class ChannelParametersTest {

  final static private String SAMPLE_PARAM = "testparam";
  final private Object value = new Integer(123);
  final private Object value2 = new Integer(321);
  final private IChannel dummyChannel = new ChannelStub();

  private ChannelParameters fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new ChannelParameters(dummyChannel);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPutParameter() {
    fixture.addParameter(SAMPLE_PARAM, 123L);
  }

  @Test(expected = InvalidParamTypeException.class)
  public void testPutParameter2() {
    fixture.addParameter(SAMPLE_PARAM, 123L);
    fixture.addParameter(SAMPLE_PARAM, new Integer(1));
  }

  @Test
  public void testHasParameter() {
    assertFalse(fixture.hasParameter(null));
    assertFalse(fixture.hasParameter(SAMPLE_PARAM));

    fixture.addParameter(SAMPLE_PARAM, 1L);

    assertTrue(fixture.hasParameter(SAMPLE_PARAM));
  }
  
  @Test
  public void testRemoveParameter() {
    assertFalse(fixture.hasParameter(null));
    assertFalse(fixture.hasParameter(SAMPLE_PARAM));
    
    fixture.addParameter(SAMPLE_PARAM, 1L);
    fixture.removeParameter(SAMPLE_PARAM);
    
    assertFalse(fixture.hasParameter(SAMPLE_PARAM));
  }

  @Test
  public void testGetParameter() {
    fixture.addParameter(SAMPLE_PARAM, value);
    assertTrue(fixture.getParameter(SAMPLE_PARAM) == value);
  }

  @Test(expected = UnsupportParamException.class)
  public void testGetParameter2() {
    fixture.getParameter(SAMPLE_PARAM);
  }

  public void testGetParameter3() {
    assertNull(fixture.getParameter(null));
  }

  @Test
  public void testSetParameter() {
    fixture.addParameter(SAMPLE_PARAM, value);
    assertTrue(fixture.getParameter(SAMPLE_PARAM) == value);

    fixture.setParameter(SAMPLE_PARAM, value2);
    assertTrue(fixture.getParameter(SAMPLE_PARAM) == value2);
  }

  @Test(expected = UnsupportParamException.class)
  public void testSetInvalidParameter2() {
    fixture.setParameter(SAMPLE_PARAM, value2);
  }

  @Test(expected = NullPointerException.class)
  public void testSetInvalidParameter3() {
    fixture.addParameter(SAMPLE_PARAM, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetInvalidParameter4() {
    fixture.setParameter(null, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetInvalidParameter5() {
    fixture.setParameter(null, value);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetInvalidParameter6() {
    fixture.setParameter("  ", value);
  }

  @Test(expected = InvalidParamTypeException.class)
  public void testSetParameter6() {
    fixture.addParameter(SAMPLE_PARAM, 123L);
    fixture.setParameter(SAMPLE_PARAM, new Integer(123));
  }

  @Test
  public void testGetParameterNames() {
    // No parameter
    assertNotNull(fixture.getParameterNames());
    assertEquals(0, fixture.getParameterNames().length);

    // Prepare a list of parameter names
    String[] paraNames = { "para1", "xyz", "cc", "aaa" };
    List<String> paraNamesList = Arrays.asList(paraNames);

    // Added parameter names and values to channel
    for (String para : paraNamesList) {
      fixture.addParameter(para, value);
    }

    // Verify the channel contains all parameters we added.
    String[] rtnParams = fixture.getParameterNames();
    assertEquals(paraNamesList.size(), rtnParams.length);
    for (String rtnParam : rtnParams) {
      assertNotNull(rtnParam);
      assertTrue(paraNamesList.contains(rtnParam));
    }

  }
}
