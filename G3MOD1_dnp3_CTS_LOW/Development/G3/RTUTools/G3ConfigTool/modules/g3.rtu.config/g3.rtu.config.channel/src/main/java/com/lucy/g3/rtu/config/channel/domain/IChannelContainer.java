/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import java.util.List;

import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 * Object that implements this interface contains a collection of
 * {@linkplain IChannel}.
 */
public interface IChannelContainer {
  
  /**
   * Gets all channel types this module owns.
   *
   * @return an array of channel types.
   */
  ChannelType[] getAvailableChannelTypes();

  /**
   * Gets all channels this module owns.
   *
   * @return a list of channels.
   */
  List<? extends IChannel> getAllChannels();

  /**
   * Gets channels by specifying channel's type.
   *
   * @param type
   *          the channel type to be retrieved.
   * @return an array of channels in specified type.
   */
  IChannel[] getChannels(ChannelType type);

  /**
   * Gets a channel from this module by specifying channel ID and type.
   *
   * @param id
   *          the channel's ID of the channel to be retrieved.
   * @param type
   *          the channel's type of the channel to be retrieved.
   * @return the found channel. Null if not found.
   */
  IChannel getChByTyeAndID(int id, ChannelType type);

  IChannel getChByEnum(IChannelEnum channelEnum);
}
