/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import java.util.Map;

import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * That class wraps all pre-defined channel configuration items. Predefined
 * configuration is supposed to be used when configuring virtual points.
 */
public interface IPredefinedChannelConfig {

  DefaultCreateOption getDefaultCreateOption();

  String getRawUnit();

  double getRawMin();

  double getRawMax();

  String getUnitForScaling(Double scalingFactor);

  Map<Double, String> getPredefindScalingUnit();

  Double getDefaultScalingFactor();
}
