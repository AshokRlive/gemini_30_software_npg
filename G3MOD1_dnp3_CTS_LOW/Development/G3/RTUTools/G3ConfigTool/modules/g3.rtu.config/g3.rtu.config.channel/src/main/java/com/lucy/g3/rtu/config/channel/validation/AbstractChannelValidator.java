/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.validation;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 *
 *
 */
public abstract class AbstractChannelValidator extends AbstractValidator<IChannel> implements IChannelValidator {

  private static final String PROPERTY_ERR_MSG = "Invalid configuration of the paramter \"%s\" in channel \"%s\".";


  protected AbstractChannelValidator(IChannel target) {
    super(target);
  }

  /**
   * Checks if a value is within boundary.
   *
   * @return error message if the value is out of boundary. null if the value is
   *         valid.
   */
  protected final String checkLongBoundary(String parameterName, Long min, Long max) {
    Long value = (Long) target.getParameter(parameterName);
    return checkLongBoundary(parameterName, value, min, max);
  }

  /**
   * Checks if a value is within boundary.
   *
   * @return error message if the value is out of boundary. null if the value is
   *         valid.
   */
  protected final String checkLongBoundary(String parameterName, Long parameterValue, Long min, Long max) {
    if (parameterValue == null) {
      return String.format(PROPERTY_ERR_MSG + " the value is null!", parameterName, target.getDescription());
    }

    if (min != null && max != null) {
      if (parameterValue < min || parameterValue > max) {
        return String.format(PROPERTY_ERR_MSG + " the value is out of range[%d, %d]!",
            parameterName, target.getDescription(),
            min, max);
      }
    } else {
      Logger.getLogger(AbstractChannelValidator.class).error("Min/Max value not found for parameter:" + parameterName);
    }

    return null;
  }

  /**
   * Checks if a value is within boundary. The result will be updated if the
   * value is invalid.
   */
  protected final void checkLongBoundary(ValidationResultExt result, String parameterName, Long min, Long max) {
    String error = checkLongBoundary(parameterName, min, max);
    if (error != null && result != null) {
      result.addError(error, parameterName);
    }
  }

  @Override
  public void validate(String parameterName, Object valueObj) throws InvalidConfException {
    /* Check null */
    if (valueObj == null) {
      throw new InvalidConfException("Invalid parameter value: null parameter!");
    }

    /* Check parameter exists */
    if (!target.hasParameter(parameterName)) {
      throw new InvalidConfException(String.format("Parameter \"%s\" is not found in channel:\"%s\"",
          parameterName,
          target.getDescription()));
    }

    /* Check value type */
    if (target.getParameter(parameterName).getClass() != valueObj.getClass()) {
      throw new InvalidConfException("Invalid parameter type:" + valueObj.getClass());
    }
  }

  protected void checkNotNegative(String parameterName, Long value) throws InvalidConfException {
    if (value != null && value < 0) {
      String err = String.format(PROPERTY_ERR_MSG + " the value is negative!", parameterName, target.getDescription());

      throw new InvalidConfException(err);
    }
  }
}
