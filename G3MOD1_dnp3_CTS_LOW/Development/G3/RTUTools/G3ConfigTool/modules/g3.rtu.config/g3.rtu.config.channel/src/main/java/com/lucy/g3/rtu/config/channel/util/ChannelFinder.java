/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.util;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Utility for finding channels from a collection of channels.
 */
public class ChannelFinder {

  public static Collection<IChannel> findChannelByType(Collection<IChannel> channels, MODULE[] modules,
      ChannelType[] types) {
    Preconditions.checkNotNull(channels, "channels is null");

    ArrayList<IChannel> foundChls = new ArrayList<IChannel>();

    for (IChannel ch : channels) {
      for (ChannelType t : types) {
        if (modules != null) {
          for (MODULE m : modules) {
            if (m == ch.getOwnerModule().getType() && t == ch.getType()) {
              foundChls.add(ch);
            }
          }
        }
      }
    }
    return foundChls;
  }

  public static <T extends IChannel> Collection<T> findChannelsByEnum(
      Collection<T> channels, Enum<?>[] enums) {
    Preconditions.checkNotNull(channels, "channels is null");
    Preconditions.checkNotNull(enums, "enums is null");

    ArrayList<T> foundChs = new ArrayList<T>();
    for (T ch : channels) {
      for (int i = 0; i < enums.length; i++) {
        if (ch.getEnum() == enums[i] && enums[i] != null) {
          foundChs.add(ch);
          break;
        }
      }
    }

    return foundChs;
  }
}
