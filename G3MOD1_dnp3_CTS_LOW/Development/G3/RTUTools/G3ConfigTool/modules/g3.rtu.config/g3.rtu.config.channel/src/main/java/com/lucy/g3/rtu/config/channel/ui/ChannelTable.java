/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;

import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.ext.swing.table.AutoFillJTable;
import com.lucy.g3.gui.common.widgets.ext.swing.table.HighlightPredicateAdapter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;

/**
 * The Class ChannelTable.
 */
class ChannelTable extends AutoFillJTable {

  public ChannelTable(TableModel model) {
    super(model);
    init();
  }

  private void init() {
    UIUtils.setDefaultConfig(this, true, true, false);
    
    // Set properties
    setShowGrid(false);
    setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    setBorder(BorderFactory.createEmptyBorder());
    
    TableModel tableModel = getModel();
    // Set error highlighters
    if (tableModel instanceof ISupportHighlight) {
      HighlightPredicate predicator = new HighlightPredicateAdapter((ISupportHighlight) tableModel);
      addHighlighter(new ColorHighlighter(predicator, UIThemeResources.COLOUR_ERROR_BACKGROUND, Color.black));
    }

    // Set default sorting
    UIUtils.sortTable(this, ChannelManagerModel.COLUMN_GROUP, SortOrder.ASCENDING);
  }

  /*
   * The default behaviour when user typed on a selected row in JTable is
   * appending the typed text at the end of the editing cell. Override for
   * clearing the content when type on cell.
   */
  @Override
  public boolean editCellAt(int row, int column, EventObject e) {
    boolean result = super.editCellAt(row, column, e);

    final Component editor = getEditorComponent();
    if (editor != null && editor instanceof JTextComponent && e instanceof KeyEvent) {
      ((JTextComponent) editor).selectAll();
    }
    return result;
  }

  /* Set render component disabled/enabled state */
  @Override
  public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
    Component c = super.prepareRenderer(renderer, row, column);

    // Set editable cell different colour
    if (isCellEditable(row, column) == true) {
      if (isRowSelected(row)) {
        c.setForeground(Color.yellow);
      } else {
        c.setForeground(Color.blue);
      }
    }

    // Set enable/disable state of the renderer
    /*
     * boolean shouldDisable; final int rowInModel =
     * convertRowIndexToModel(row); Channel ch = model.getRow(rowInModel);
     * if(ch.isReadOnly() == true) shouldDisable = true; else
     * if(ch.hasParameter(columnName) == false) shouldDisable = false; else
     * if(ch.isParamModifiable(columnName) == false) shouldDisable = true; else
     * shouldDisable = false; c.setEnabled(!shouldDisable);
     */

    // Align ID column to centre
    if (c instanceof JLabel) {
      JLabel render = (JLabel) c;
      if (column == ChannelManagerModel.COLUMN_ID) {
        render.setHorizontalAlignment(SwingConstants.CENTER);
      } else {
        render.setHorizontalAlignment(SwingConstants.LEADING);
      }
    }

    return c;
  }

}
