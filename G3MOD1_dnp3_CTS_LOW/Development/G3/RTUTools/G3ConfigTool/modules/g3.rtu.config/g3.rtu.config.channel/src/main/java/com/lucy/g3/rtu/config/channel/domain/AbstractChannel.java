/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import static com.jgoodies.common.base.Preconditions.checkNotNull;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * The basic implementation of {@linkplain IChannel}.
 */
public abstract class AbstractChannel extends AbstractNode implements IChannel {

  private final ChannelParameters parameters;
  private final ChannelType type;
  private final Module ownerModule;

  private boolean readOnly;


  public AbstractChannel(ChannelType channelType, Module ownerModule, String description) {
    super(NodeType.CHANNEL);

    this.type = checkNotNull(channelType, "Channel Type must not be null");
    this.ownerModule = ownerModule;
    this.parameters = new ChannelParameters(this);
    super.setDescription(description);

    /*
     * A listener for updating the name of this channel.
     */
    PropertyChangeListener pcl = new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent arg0) {
        updateName();
      }
    };

    if (ownerModule != null) {
      ownerModule.addPropertyChangeListener(Module.PROPERTY_NAME, pcl);
    }

    addPropertyChangeListener(pcl);
    updateName();
  }

  @Override
  public final boolean isReadOnly() {
    return readOnly;
  }

  protected final void setReadOnly(boolean readOnly) {
    this.readOnly = readOnly;
  }

  @Override
  public final ChannelType getType() {
    return type;
  }

  @Override
  public Module getOwnerModule() {
    return ownerModule;
  }

  private void updateName() {
    String newName;
    if (ownerModule == null) {
      newName = getDescription();
    } else {
      newName = String.format("%s - %s", ownerModule.getShortName(), getDescription());
    }

    setName(newName);
  }

  @Override
  public final String toString() {
    return getName();
  }

  // ================== Parameters Delegate Methods ===================

  protected void addParameter(String paraName, Object value) {
    parameters.addParameter(paraName, value);
  }
  
  protected void removeParameter(String paraName) {
    parameters.removeParameter(paraName);
  }

  @Override
  public void setParameter(String paraName, Object value)
      throws UnsupportParamException, InvalidParamTypeException,
      NullPointerException, IllegalArgumentException {
    parameters.setParameter(paraName, value);
  }

  @Override
  public final Object getParameter(String paraName) throws UnsupportParamException,
      IllegalArgumentException {
    return parameters.getParameter(paraName);
  }

  @Override
  public final boolean hasParameter(String paraName) {
    return parameters.hasParameter(paraName);
  }

  @Override
  public final String[] getParameterNames() {
    return parameters.getParameterNames();
  }

  @Override
  public boolean isParameterModifiable(String paraName) {
    return parameters.isParameterModifiable(paraName);
  }

  @Override
  public final void setParameterModifiable(String paraName, boolean modifiable) {
    parameters.setParameterModifiable(paraName, modifiable);
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return getOwnerModule() == module;
  }

  @Override
  public boolean checkSourceModule(MODULE module) {
    return getOwnerModule().getType() == module;
  }

  @Override
  public Module getSourceModule() {
    return getOwnerModule();
  }

  @Override
  public String getSourceName() {
    return getSourceModule() == null ? null : getSourceModule().getShortName();
  }
}
