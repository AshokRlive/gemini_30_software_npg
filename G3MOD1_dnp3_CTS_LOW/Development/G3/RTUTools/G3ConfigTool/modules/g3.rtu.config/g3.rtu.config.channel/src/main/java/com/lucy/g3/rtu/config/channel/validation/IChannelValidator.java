/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.validation;

import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 *
 *
 */
public interface IChannelValidator extends IValidator {

  /**
   * Checks if a value is valid to be used by a parameter.
   *
   * @param parameterName
   *          the parameter name
   * @param valueObj
   *          the value to be validated
   * @throws InvalidConfException
   *           the exception
   */
  void validate(String parameterName, Object valueObj)
      throws InvalidConfException;
}
