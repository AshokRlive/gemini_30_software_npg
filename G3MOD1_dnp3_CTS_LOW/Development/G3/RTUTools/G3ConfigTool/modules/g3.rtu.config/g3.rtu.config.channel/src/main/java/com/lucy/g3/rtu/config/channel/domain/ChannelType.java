/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

import com.lucy.g3.xml.gen.common.MCMConfigEnum.OUTPUT_CHANNEL_TYPE;

/**
 * This enumeration lists all supported channel types. Each type contains
 * description field which is loaded from external .properties file.
 * <p>
 * * <b>Note:</b> it is required to update .properties file if adding/removing
 * any enumeration item.
 * </p>
 */
public enum ChannelType {
  ANALOG_INPUT(true),
  ANALOG_OUTPUT(true),
  DIGITAL_INPUT,
  DIGITAL_OUTPUT,
  SWITCH_OUT,
  PSM_CH_FAN,
  PSM_CH_BCHARGER,
  PSM_CH_PSUPPLY,
  FPI;

  ChannelType(boolean isAnalogue) {
    this.isAnalogue = isAnalogue;

    /* Load resource */
    ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(ChannelType.class);

    /* Initialise description from external resource file */
    String key = super.name();
    description = resourceMap.getString(key + ".description");
    shortDescription = resourceMap.getString(key + ".shortDescription");
  }

  ChannelType() {
    this(false);
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public String getDescription() {
    return description;
  }

  public boolean isAnalogue() {
    return isAnalogue;
  }

  @Override
  public String toString() {
    return getDescription();
  }


  private final String description;
  private final String shortDescription;

  private final boolean isAnalogue;


  public static final OUTPUT_CHANNEL_TYPE convertEnum(ChannelType t) {
    if (t == null) {
      return null;
    }

    switch (t) {
    case ANALOG_OUTPUT:
      return OUTPUT_CHANNEL_TYPE.OUTPUT_CHANNEL_TYPE_AOUTPUT;
    case PSM_CH_BCHARGER:
      return OUTPUT_CHANNEL_TYPE.OUTPUT_CHANNEL_TYPE_BCHARGER;
    case DIGITAL_OUTPUT:
      return OUTPUT_CHANNEL_TYPE.OUTPUT_CHANNEL_TYPE_DOUTPUT;
    case PSM_CH_FAN:
      return OUTPUT_CHANNEL_TYPE.OUTPUT_CHANNEL_TYPE_FAN;
    case FPI:
      return OUTPUT_CHANNEL_TYPE.OUTPUT_CHANNEL_TYPE_FPI;
    case PSM_CH_PSUPPLY:
      return OUTPUT_CHANNEL_TYPE.OUTPUT_CHANNEL_TYPE_PSUPPLY;
    default:
      throw new UnsupportedOperationException("Unsupported output channel type: " + t);
    }
  }

  public static final ChannelType convertEnum(OUTPUT_CHANNEL_TYPE t) {
    if (t == null) {
      return null;
    }

    switch (t) {
    case OUTPUT_CHANNEL_TYPE_AOUTPUT:
      return ChannelType.ANALOG_OUTPUT;
    case OUTPUT_CHANNEL_TYPE_BCHARGER:
      return ChannelType.PSM_CH_BCHARGER;
    case OUTPUT_CHANNEL_TYPE_DOUTPUT:
      return ChannelType.DIGITAL_OUTPUT;
    case OUTPUT_CHANNEL_TYPE_FAN:
      return ChannelType.PSM_CH_FAN;
    case OUTPUT_CHANNEL_TYPE_FPI:
      return ChannelType.FPI;
    case OUTPUT_CHANNEL_TYPE_PSUPPLY:
      return ChannelType.PSM_CH_PSUPPLY;
    default:
      throw new UnsupportedOperationException("Unsupported type: " + t);
    }
  }
}
