/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.ListModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.jgoodies.validation.ValidationMessage;
import com.lucy.g3.gui.common.dialogs.InfoDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.channel.domain.ISupportChangeDescription;
import com.lucy.g3.rtu.config.channel.validation.IChannelValidator;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * Channels manager presentation model which adapts channel ListModel to
 * TableModel.
 */
public class ChannelManagerModel extends Bean {

  public static final String PROPERTY_SELECTED = "selected";

  public static final String ACTION_KEY_VIEW_DETAILS = "viewDetails";

  // Table column index
  public static final int COLUMN_GROUP = 0;
  public static final int COLUMN_ID = 1;
  public static final int COLUMN_DESCRIPTION = 2;

  // Table column name
  private static final String COLUMN_NAME_GROUP = "Group";
  private static final String COLUMN_NAME_ID = "Channel ID";
  private static final String COLUMN_NAME_DESCRIPTION = "Description";

  private static final String[] COMMON_COLUMNS = {
      COLUMN_NAME_GROUP,
      COLUMN_NAME_ID,
      COLUMN_NAME_DESCRIPTION,
  };

  private Logger log = Logger.getLogger(ChannelManagerModel.class);

  private AbstractTableAdapter<IChannel> channelTableModel;

  private final SelectionInList<IChannel> channelSelectList;

  private final String[] parameterNames;


  public ChannelManagerModel(IChannel[] channels, String[] parameterNames) {
    Preconditions.checkNotNull(channels, "channels must not be null");
    this.parameterNames = parameterNames;

    channelSelectList = new SelectionInList<IChannel>(channels);
    initEventHandling();
  }

  public ChannelManagerModel(ListModel<? extends IChannel> channelListModel, String[] parameterNames) {
    Preconditions.checkNotNull(channelListModel, "channels must not be null");
    this.parameterNames = parameterNames;

    channelSelectList = new SelectionInList<IChannel>(channelListModel);
    initEventHandling();
  }

  private void initEventHandling() {
    channelSelectList.addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        ChannelManagerModel.this.firePropertyChange(PROPERTY_SELECTED, null, isSelected());
      }
    });
  }

  @SuppressWarnings("unchecked")
  public AbstractTableAdapter<IChannel> getChannelTableModel() {
    if (channelTableModel == null) {
      channelTableModel = new ChannelTableModel(channelSelectList, parameterNames);
    }
    return channelTableModel;
  }

  public JXTable createChannelTable() {
    return new ChannelTable(getChannelTableModel());
  }

  public IChannel getSelectedChannel() {
    return channelSelectList.getSelection();
  }

  public int getSelectedChannelIndex() {
    return channelSelectList.getSelectionIndex();
  }

  public SelectionInList<IChannel> getChannelSelectList() {
    return channelSelectList;
  }

  /**
   * Checks if there is channel selected.
   */
  public boolean isSelected() {
    return channelSelectList.hasSelection();
  }

  public javax.swing.Action getAction(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");

    ApplicationActionMap actionMap =
        Application.getInstance().getContext().getActionMap(ChannelManagerModel.class, this);
    javax.swing.Action action = actionMap.get(actionKey);

    if (action == null) {
      throw new IllegalArgumentException("Action is not available for the key:" + actionKey);
    }

    return action;
  }

  /**
   * Gets the action for viewing the detail of the selected channel.
   *
   * @return non-null action.
   */
  public Action getActionViewDetails() {
    return getAction(ACTION_KEY_VIEW_DETAILS);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_SELECTED)
  public void viewDetails() {
    IChannel ch = getSelectedChannel();
    if (ch == null) {
      log.warn("No selected channel");
      return;
    }

    String[] params = ch.getParameterNames();

    Object[][] details = new Object[params.length + 4][3];
    details[0][0] = "Description";
    details[0][1] = ch.getDescription();
    details[0][2] = "HH";

    details[1][0] = "Type";
    details[1][1] = ch.getType().getDescription();

    details[2][0] = "Group";
    details[2][1] = ch.getGroup();

    details[3][0] = "ID";
    details[3][1] = ch.getId();

    for (int j = 0; j < params.length; j++) {
      details[j + 4][0] = params[j];
      details[j + 4][1] = ch.getParameter(params[j]);
    }

    InfoDialog.show(WindowUtils.getMainFrame(), "[Channel] " + ch.getName(), details);
  }


  private static final class ChannelTableModel extends AbstractTableAdapter<IChannel> implements ISupportHighlight {

    private Logger log = Logger.getLogger(ChannelTableModel.class);

    private final String[] columnNames;


    private ChannelTableModel(ListModel<IChannel> listModel, String[] parameterNames) {
      super(listModel);

      // Prepare columns names
      if (parameterNames != null && parameterNames.length > 0) {
        columnNames = new String[COMMON_COLUMNS.length + parameterNames.length];
        System.arraycopy(COMMON_COLUMNS, 0, columnNames, 0, COMMON_COLUMNS.length);
        System.arraycopy(parameterNames, 0, columnNames, COMMON_COLUMNS.length,
            parameterNames.length);
      } else {
        columnNames = COMMON_COLUMNS;
      }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      IChannel ch = getRow(rowIndex);

      if (ch != null) {
        switch (columnIndex) {
        case COLUMN_GROUP:
          return ch.getGroup();
        case COLUMN_ID:
          return ch.getId();
        case COLUMN_DESCRIPTION:
          return ch.getDescription();
        default:
          return ch.getParameter(columnNames[columnIndex]);
        }
      } else {
        return null;
      }
    }

    @Override
    public String getColumnName(int column) {
      return columnNames[column];
    }

    @Override
    public int getColumnCount() {
      return columnNames.length;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      if (columnIndex == COLUMN_GROUP || columnIndex == COLUMN_ID) {
        return false;
      }

      IChannel ch = getRow(rowIndex);
      if (ch == null || ch.isReadOnly()) {
        return false;
      }

      if (ch instanceof ISupportChangeDescription
          && columnIndex == COLUMN_DESCRIPTION) {
        return true;
      }

      if (ch.isParameterModifiable(columnNames[columnIndex]) == false) {
        return false;
      }

      return true;
    }

    @Override
    public Class<?> getColumnClass(int c) {
      if (getRowCount() > 0) {
        return getValueAt(0, c).getClass();
      } else {
        return super.getColumnClass(c);
      }
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
      IChannel ch = getRow(row);
      if (ch == null) {
        return;
      }

      if (ch instanceof ISupportChangeDescription
          && col == COLUMN_DESCRIPTION) {
        ((ISupportChangeDescription) ch).setDescription(String.valueOf(value));
      } else {

        String paraName = columnNames[col];
        IChannelValidator validator = ch.getValidator();
        if (validator != null) {
          try {
            validator.validate(paraName, value);
          } catch (InvalidConfException e1) {
            JOptionPane.showMessageDialog(WindowUtils.getMainFrame(), e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            return;
          }
        }

        if (ch.isParameterModifiable(paraName) == false) {
          log.warn("Cannot change the value of parameter since it not modifiable");
          return;
        }

        try {
          ch.setParameter(paraName, value);
        } catch (Exception e) {
          log.error("Cannot set parameter: " + paraName, e);
        }
      }

      // update the row to refresh cell components's enable/disable status
      fireTableRowsUpdated(row, row);
    }

    @Override
    public boolean shouldHightlight(int rowIndex, int columnIndex) {
      if (rowIndex < getRowCount() && rowIndex >= 0 && getRow(rowIndex) != null) {

        IChannel ch = getRow(rowIndex);
        IChannelValidator validator = ch.getValidator();
        if (validator == null) {
          return false;
        }

        /* Check if there is error for a parameter */
        ValidationResultExt validResult = validator.getResult();
        String propertyName = getColumnName(columnIndex);
        if (validResult != null) {
          List<ValidationMessage> errors = validResult.getErrors();
          for (ValidationMessage e : errors) {
            if (e.key().equals(propertyName)) {
              return true;
            }
          }
        }
      }

      return false;
    }
  }
}
