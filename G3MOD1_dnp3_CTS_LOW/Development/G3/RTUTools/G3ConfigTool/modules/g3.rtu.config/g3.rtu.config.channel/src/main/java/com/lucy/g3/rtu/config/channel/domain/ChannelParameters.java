/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;

class ChannelParameters implements IChannelParameters {

  private Logger log = Logger.getLogger(ChannelParameters.class);

  /**
   * Stores property name and value of this channel.
   */
  private final HashMap<String, Object> parameters = new HashMap<String, Object>();

  private final HashMap<String, Boolean> modifiable = new HashMap<String, Boolean>();

  /**
   * The list of supported property names.
   */
  private final ArrayList<String> parameterNames = new ArrayList<String>();

  private final IChannel channel;


  ChannelParameters(IChannel channel) {
    this.channel = channel;
  }

  /**
   * Add an new parameter into this channel. If the parameter already exists,
   * its value and value type would be updated.
   *
   * @param paraName
   *          the parameter's name, used as the key when getting parameter
   *          value.
   * @param value
   *          the value of the specific parameter.
   * @throws IllegalArgumentException
   *           if {@code paraName} is blank.
   * @throws NullPointerExceptionor
   *           {@code value} is null.
   * @throws InvalidParamTypeException
   *           if the given value's type is different from the existed parameter
   *           value type.
   */
  void addParameter(String paraName, Object value) {
    Preconditions.checkArgument(Strings.isNotBlank(paraName), "Parameter name must not be blank");
    Preconditions.checkNotNull(value, "Parameter value  must not be null");

    if (!parameterNames.contains(paraName)) {
      // New parameter, store parameter name.
      parameterNames.add(paraName);
    } else {
      // Existed parameter, validate value type
      Object oldValue = getParameter(paraName);
      if (oldValue.getClass() != value.getClass()) {
        throw new InvalidParamTypeException("Invalid value type. Expected:"
            + oldValue.getClass() + " Actual:" + value.getClass());
      }
    }

    parameters.put(paraName, value);
  }
  
  void removeParameter(String paraName) {
    parameterNames.remove(paraName);
    parameters.remove(paraName);
  }

  @Override
  public void setParameter(String paraName, Object value)
      throws UnsupportParamException, InvalidParamTypeException, NullPointerException {
    Preconditions.checkArgument(Strings.isNotBlank(paraName), "Parameter name must not be blank");

    if (value == null) {
      log.warn("Cannot set parameter: " + paraName + " to null");
      return;
    }

    if (hasParameter(paraName) == false) {
      /* The parameter does NOT exists */
      throw new UnsupportParamException(
          String.format("Cannot set channel parameter \"%s\" cause channel \"%s\" doesn't have this parameter!",
              channel.getName(), paraName));
    }

    if (getParameter(paraName).getClass() != value.getClass()) {
      /* Wrong parameter value type */
      throw new InvalidParamTypeException("Invalid type of the new value for the parameter: "
          + paraName
          + ", expected: " + getParameter(paraName).getClass()
          + ", actual: " + value.getClass());
    }

    if (isParameterModifiable(paraName) == false) {
      /* This parameter is not modifiable */
      log.warn(String.format("Set channel unmodifiable parameter \"%s\" to %s " +
          "in channel \"%s\"", paraName, value, channel.getName()));
      return;
    }

    parameters.put(paraName, value);
  }

  @Override
  public Object getParameter(String paraName) throws UnsupportParamException, NullPointerException {
    /* Check parameter exists */
    if (!parameterNames.contains(paraName)) {
      throw new UnsupportParamException("Cannot get parameter value for channel: \""
          + channel.getName() + "\" cause the parameter \"" + paraName + "\" is not found");
    }

    return parameters.get(paraName);
  }

  @Override
  public boolean hasParameter(String paraName) {
    return paraName != null && parameters.containsKey(paraName);
  }

  @Override
  public String[] getParameterNames() {
    return parameterNames.toArray(new String[parameterNames.size()]);
  }

  @Override
  public boolean isParameterModifiable(String paraName) {
    boolean hasParam = hasParameter(paraName);
    boolean modif = modifiable.get(paraName) != Boolean.FALSE;
    return hasParam && modif;
  }

  @Override
  public void setParameterModifiable(String paraName, boolean modifiable) {
    this.modifiable.put(paraName, modifiable);
  }
}
