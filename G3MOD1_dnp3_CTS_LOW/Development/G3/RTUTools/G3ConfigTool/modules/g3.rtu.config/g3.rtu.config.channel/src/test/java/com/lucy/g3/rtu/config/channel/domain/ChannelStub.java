/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.channel.domain;

import com.lucy.g3.rtu.config.channel.domain.AbstractChannel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IPredefinedChannelConfig;
import com.lucy.g3.rtu.config.channel.validation.IChannelValidator;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 *
 *
 */
public class ChannelStub extends AbstractChannel {

  public ChannelStub(ChannelType channelType) {
    this(channelType, new ModuleStub(), "");
  }

  public ChannelStub(ChannelType channelType, Module ownerModule,
      String description) {
    super(channelType, ownerModule, description);
    setName("I am channel stub");
  }

  public ChannelStub() {
    this(ChannelType.ANALOG_INPUT, new ModuleStub(), "");
  }

  @Override
  public int getId() {
    return 0;
  }

  @Override
  public String getGroup() {
    return null;
  }

  @Override
  public IChannelEnum getEnum() {
    return null;
  }

  @Override
  public IPredefinedChannelConfig predefined() {
    return null;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
  }

  @Override
  public IChannelValidator getValidator() {
    return null;
  }

}
