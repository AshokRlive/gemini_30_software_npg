/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.manager;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;

/**
 * An update interface for receiving notifications about the change of Protocol
 * sessions.
 */
public interface IProtocolObserver {

  /**
   * This method is called when a session is added.
   *
   * @param session
   *          the session that was added
   */
  void notifySessionAdded(IProtocolSession session);

  /**
   * This method is called when a session is removed.
   *
   * @param session
   *          the session that was removed
   */
  void notifySessionRemoved(IProtocolSession session);
}
