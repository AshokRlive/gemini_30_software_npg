/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.EventsConfig;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Enums;

public class EventConfigPanel extends JPanel {

  private final PresentationModel<EventsConfig> pm;


  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private EventConfigPanel() {
    pm = new PresentationModel<>(new EventsConfig(IEC870Constraints.INSTANCE_101));
    initComponents();
  }

  public EventConfigPanel(EventsConfig bean) {
    pm = new PresentationModel<EventsConfig>(bean);
    initComponents();
  }

  private void createUIComponents() {
    ComponentsFactory factory =
        new ComponentsFactory(pm.getBean().getConstraints(), IEC870Constraints.PREFIX_SESSION_EVENT, pm);

    ftfMaxEvent = factory.createNumberField(EventsConfig.PROPERTY_MAX_EVENT);

    comboEventMode = factory.createComboBox(EventsConfig.PROPERTY_EVENT_MODE, IEC870Enums.getEventModeEnums());

    comboTimeFormat = factory.createComboBox(EventsConfig.PROPERTY_TIME_FORMAT, IEC870Enums.getTimeFormatEnums());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();


    //======== this ========
    setOpaque(false);
    setLayout(new FormLayout(
      "[70dlu,default]",
      "2*(default, $lgap), default"));
    add(ftfMaxEvent, CC.xy(1, 1));
    add(comboEventMode, CC.xy(1, 3));
    add(comboTimeFormat, CC.xy(1, 5));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JFormattedTextField ftfMaxEvent;
  private JComboBox comboEventMode;
  private JComboBox comboTimeFormat;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
