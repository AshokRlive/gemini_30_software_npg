/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import org.apache.log4j.Logger;

import com.g3schema.ns_pstack.TCPConfT;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.common.utils.EnumUtils;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;

/**
 * Common TCP/UDP configuration for all TCP protocol channels.
 */
public class ProtocolChannelTCPConf extends Model {

  public static final String ANY_IP_ADDRESS = "*.*.*.*";

  public static final String PROPERTY_ANY_IP_ENABLED = "anyIPEnabled";
  public static final String PROPERTY_IP_ADDRESS = "ipAddress";
  public static final String PROPERTY_IP_PORT = "ipPort";
  public static final String PROPERTY_IP_CONNECT_TIMEOUT = "ipConnectTimeout";
  public static final String PROPERTY_MODE = "mode";
  public static final String PROPERTY_DISCONNECT_ON_NEW_SYNC = "disconnectOnNewSync";


  private boolean anyIPEnabled = true;
  private String ipAddress = ANY_IP_ADDRESS;
  private long ipPort = 502;
  private long ipConnectTimeout = 5000;
  private LU_LINTCP_MODE mode = LU_LINTCP_MODE.LU_LINTCP_MODE_SERVER;
  private boolean disconnectOnNewSync = false;


  protected ProtocolChannelTCPConf() {
    super();

    LoggingUtil.logPropertyChanges(this);
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public long getIpPort() {
    return ipPort;
  }

  public void setIpPort(long ipPort) {
    Object oldValue = getIpPort();
    this.ipPort = ipPort;
    firePropertyChange(PROPERTY_IP_PORT, oldValue, ipPort);
  }

  public long getIpConnectTimeout() {
    return ipConnectTimeout;
  }

  public void setIpConnectTimeout(long ipConnectTimeout) {
    Object oldValue = getIpConnectTimeout();
    this.ipConnectTimeout = ipConnectTimeout;
    firePropertyChange(PROPERTY_IP_CONNECT_TIMEOUT, oldValue, ipConnectTimeout);
  }

  public String getIPSettingsText() {
    return String.format("TCP [%s:%d]", getIpAddress(), getIpPort());
  }

  public LU_LINTCP_MODE getMode() {
    return mode;
  }

  public void setMode(LU_LINTCP_MODE mode) {
    if (mode == null) {
      return;
    }

    Object oldValue = getMode();
    this.mode = mode;
    firePropertyChange(PROPERTY_MODE, oldValue, mode);
  }

  public boolean isAnyIPEnabled() {
    return anyIPEnabled;
  }

  public void setIpAddress(String ipAddress) {
    setIpAddress(ipAddress, true);
  }

  private void setIpAddress(String ipAddress, boolean updateAnyIpFlag) {
    if (IPAddress.validateIPAddress(ipAddress, false, true) == false) {
      Logger.getLogger(ProtocolChannelTCPConf.class).error("Fail to set IP address. Invalid IP address: " + ipAddress);
      return;
    }

    Object oldValue = getIpAddress();
    this.ipAddress = ipAddress.trim();
    firePropertyChange(PROPERTY_IP_ADDRESS, oldValue, ipAddress);

    if (updateAnyIpFlag) {
      /* Update any IP address flag */
      oldValue = isAnyIPEnabled();
      this.anyIPEnabled = ipAddress.equals(ANY_IP_ADDRESS);
      firePropertyChange(PROPERTY_ANY_IP_ENABLED, oldValue, this.anyIPEnabled);
    }
  }

  public void setAnyIPEnabled(boolean anyIPEnabled) {
    Object oldValue = this.anyIPEnabled;
    this.anyIPEnabled = anyIPEnabled;
    firePropertyChange(PROPERTY_ANY_IP_ENABLED, oldValue, anyIPEnabled);

    if (anyIPEnabled) {
      /* Update IP address to ANY */
      setIpAddress(ANY_IP_ADDRESS, false);
    }else {
      /* Update IP address not to be ANY */
      if(ANY_IP_ADDRESS.equals(getIpAddress()))
        setIpAddress("0.0.0.0", false);
    }
  }
  
  public boolean isDisconnectOnNewSync() {
    return disconnectOnNewSync;
  }
  
  public void setDisconnectOnNewSync(boolean disconnectOnNewSync) {
    Object oldValue = this.disconnectOnNewSync;
    this.disconnectOnNewSync = disconnectOnNewSync;
    firePropertyChange(PROPERTY_DISCONNECT_ON_NEW_SYNC, oldValue, disconnectOnNewSync);
  }


  protected void writeToXML(TCPConfT xml) {
    xml.ipAddress.setValue(getIpAddress());
    xml.ipPort.setValue(getIpPort());
    xml.ipConnectTimeout.setValue(getIpConnectTimeout());
    xml.mode.setValue(EnumUtils.getStringFromEnum(getMode()));
  }

  protected void readFromXML(TCPConfT xml) {
    setMode(EnumUtils.getEnumFromString(LU_LINTCP_MODE.class, xml.mode.getValue()));
    setIpAddress(xml.ipAddress.getValue());
    setIpPort(xml.ipPort.getValue());
    setIpConnectTimeout(xml.ipConnectTimeout.getValue());
  }
}
