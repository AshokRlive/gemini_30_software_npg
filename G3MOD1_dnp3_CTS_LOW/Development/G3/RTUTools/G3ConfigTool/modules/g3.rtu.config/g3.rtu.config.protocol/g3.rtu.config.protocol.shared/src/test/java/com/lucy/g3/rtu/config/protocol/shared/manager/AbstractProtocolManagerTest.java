/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.manager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.itemlist.IItemListManager.AddItemException;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.AbstractProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.stub.ProtocolStub;

/**
 * The Class AbstractProtocolManagerTest.
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class AbstractProtocolManagerTest {

  private AbstractProtocolManager fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new TestAbstractProtocolManager();
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

  @Test
  public void testAllowToAdd() {
    ProtocolType[] types = ProtocolType.values();
    for (int i = 0; i < types.length; i++) {
      assertTrue("Should allow to add protocol:" + types[i],
          fixture.allowToAdd(new ProtocolStub(types[i])));
    }
  }

  @Test
  public void testAllowToAdd2() {
    ProtocolType type = ProtocolType.S104;
    fixture.add(new ProtocolStub(type));

    try {
      assertFalse("Should not allow to add same protocol:" + type,
          fixture.allowToAdd(new ProtocolStub(type)));
    } catch (AddItemException e) {
      // Exception expected
    }
  }

  @Test
  public void testAdd() {
    ProtocolType[] types = ProtocolType.values();

    for (int i = 0; i < types.length; i++) {
      fixture.add(new ProtocolStub(types[i]));
    }
  }

  @Test(expected = AddItemException.class)
  public void testAdd2() {
    ProtocolType type = ProtocolType.S104;
    assertTrue(fixture.add(new ProtocolStub(type)));
    assertFalse(fixture.add(new ProtocolStub(type)));
  }


  private static class TestAbstractProtocolManager extends AbstractProtocolManager {

    protected TestAbstractProtocolManager() {
      super(null);
    }

    @Override
    public String getName() {
      return "Test";
    }


  }

}
