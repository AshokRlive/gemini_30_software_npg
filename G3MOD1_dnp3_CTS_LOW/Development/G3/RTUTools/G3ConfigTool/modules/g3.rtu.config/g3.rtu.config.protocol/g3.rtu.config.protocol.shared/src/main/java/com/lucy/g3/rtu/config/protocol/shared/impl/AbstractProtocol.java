/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.impl;

import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.validate.ProtocolValidator;

/**
 * The basic implementation of {@linkplain IProtocol}.
 *
 * @param <ChannelT>
 *          the type of protocol channel.
 */
public abstract class AbstractProtocol<ChannelT extends IProtocolChannel<?>>
    extends ItemListManager<ChannelT>
    implements IProtocol<ChannelT> {

  private final ProtocolType type;

  private IProtocolManager<?> manager;
  
  private final ProtocolValidator validator;

  protected AbstractProtocol(ProtocolType type) {
    this.type = Preconditions.checkNotNull(type, "type is null");
    this.validator = new ProtocolValidator(this);
  }

  @Override
  public final ProtocolType getProtocolType() {
    return type;
  }
  
  @Override
  public final String getProtocolName() {
    return type.getName();
  }


  @Override
  public final Collection<ChannelT> getAllChannels() {
    return getAllItems();
  }

  @Override
  public final String toString() {
    return getProtocolName();
  }

  @Override
  public final void delete() {
    Collection<ChannelT> items = super.getAllItems();
    for (ChannelT ch : items) {
      remove(ch);
    }
  }

  @Override
  protected void postRemoveAction(ChannelT removedItem) {
    super.postRemoveAction(removedItem);
    removedItem.delete();
  }

  @Override
  public void setManager(IProtocolManager<?> manager) {
    this.manager = manager;
  }

  @Override
  public IProtocolManager<?> getManager() {
    return manager;
  }

  @Override
  public final ProtocolValidator getValidator() {
    return validator;
  }
  
}
