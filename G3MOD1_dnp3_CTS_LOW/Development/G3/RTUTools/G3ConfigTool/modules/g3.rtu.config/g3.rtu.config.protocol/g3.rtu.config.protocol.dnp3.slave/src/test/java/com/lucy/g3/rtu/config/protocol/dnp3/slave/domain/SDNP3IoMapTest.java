/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3IoMap;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

public class SDNP3IoMapTest {

  private SDNP3IoMap fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new SDNP3Channel(new SDNP3()).addSession("").getIomap();
  }

  @After
  public void tearDown() throws Exception {
  }

  // It is not allowed to add point with same id and type.
  @Test(expected = DuplicatedException.class)
  public void testDuplicatedProtocolID4() throws DuplicatedException {
    final long id = 1;
    SDNP3Point p0 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, id);
    SDNP3Point p1 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, id);
    fixture.addPoint(p0);
    fixture.addPoint(p1);
  }

  // Add points with diff type and id
  @Test
  public void testAddPoint() throws DuplicatedException {
    SDNP3Point p0 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, 1);
    SDNP3Point p1 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, 2);
    fixture.addPoint(p0);
    fixture.addPoint(p1);
  }

  // Add points with same type and diff id
  @Test
  public void testAddPoint2() throws DuplicatedException {
    SDNP3Point p0 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, 1);
    SDNP3Point p1 = SDNP3PointFactory.create(fixture, SDNP3PointType.BinaryInput, 1);
    fixture.addPoint(p0);
    fixture.addPoint(p1);
  }

  // Add points with same type and same id
  @Test(expected = DuplicatedException.class)
  public void testAddPoint3() throws DuplicatedException {
    SDNP3Point p0 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, 1);
    SDNP3Point p1 = SDNP3PointFactory.create(fixture, SDNP3PointType.AnalogueInput, 1);
    fixture.addPoint(p0);
    fixture.addPoint(p1);
  }
}
