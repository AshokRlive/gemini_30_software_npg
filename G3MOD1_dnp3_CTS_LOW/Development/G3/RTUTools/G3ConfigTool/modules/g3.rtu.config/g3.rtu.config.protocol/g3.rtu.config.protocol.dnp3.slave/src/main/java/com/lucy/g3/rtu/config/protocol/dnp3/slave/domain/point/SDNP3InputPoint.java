/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import java.util.HashMap;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.GlobalVariationsConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;

/**
 *
 *
 */
public class SDNP3InputPoint extends SDNP3Point {
  
  private final DNP3ObjConfig conf; 


  /**
   * Construct a input point for DNP3 slave session.
   *
   * @param session
   *          must not be null.
   * @param type
   * @param id
   */
  SDNP3InputPoint(IScadaIoMap<SDNP3Point> iomap, SDNP3PointType type, long id) {
    super(iomap, type, id);
    
    GlobalVariationsConfig globalVar = iomap == null ?  
        null : ((SDNP3Session) iomap.getSession()).getGlobalEvtVariation();
    conf = new DNP3ObjConfig( globalVar, DNP3ObjGroup.convertFrom(type));
    
    // Input point should enable class0 by default
    conf.setEnableClass0(true);
  }

  @Override
  public DNP3ObjConfig getEventConf() {
    return conf;
  }

  @Override
  public void applyParameters(HashMap<String, Object> parameters) {
    String param;
    
    configureDNP3Obj(conf, parameters);
    
    param = SDNP3Point.PARAMETER_CUSTOM_DESCRIPTION;
    if (parameters.containsKey(param)) {
      setCustomDescription((String) parameters.get(param));
    }
    
    param = SDNP3Point.PARAMETER_DISABLED;
    if (parameters.containsKey(param)) {
      setEnabled(!(boolean)parameters.get(param));
    }
    
    param = SDNP3Point.PARAMETER_ENABLE_STORE_EVENT;
    if (parameters.containsKey(param)) {
      setEnableStoreEvent((boolean)parameters.get(param));
    }
  }
 
}
