/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.ConnectOnClass;

/**
 * The Class ConnectOnClassTest.
 */
public class ConnectOnClassTest {

  private ConnectOnClass fixture = null;


  @Before
  public void setUp() throws Exception {
    fixture = new ConnectOnClass();
  }

  @After
  public void Teardown() throws Exception {

  }

  @Test
  public void testSetMask() {
    fixture.setConnectOnClassMask(0);
    assertFalse(fixture.isConnectOnClass1());
    assertFalse(fixture.isConnectOnClass2());
    assertFalse(fixture.isConnectOnClass3());

    fixture.setConnectOnClassMask(1);
    assertTrue(fixture.isConnectOnClass1());
    assertFalse(fixture.isConnectOnClass2());
    assertFalse(fixture.isConnectOnClass3());

    fixture.setConnectOnClassMask(2);
    assertFalse(fixture.isConnectOnClass1());
    assertTrue(fixture.isConnectOnClass2());
    assertFalse(fixture.isConnectOnClass3());

    fixture.setConnectOnClassMask(4);
    assertFalse(fixture.isConnectOnClass1());
    assertFalse(fixture.isConnectOnClass2());
    assertTrue(fixture.isConnectOnClass3());

    fixture.setConnectOnClassMask(7);
    assertTrue(fixture.isConnectOnClass1());
    assertTrue(fixture.isConnectOnClass2());
    assertTrue(fixture.isConnectOnClass3());
  }

  @Test
  public void testGetMask() {
    fixture.setConnectOnClassMask(0);
    assertEquals(0, fixture.getConnectOnClassMask());

    fixture.setConnectOnClass3(true);
    assertEquals(4, fixture.getConnectOnClassMask());

    fixture.setConnectOnClass2(true);
    assertEquals(6, fixture.getConnectOnClassMask());

    fixture.setConnectOnClass1(true);
    assertEquals(7, fixture.getConnectOnClassMask());
  }

}
