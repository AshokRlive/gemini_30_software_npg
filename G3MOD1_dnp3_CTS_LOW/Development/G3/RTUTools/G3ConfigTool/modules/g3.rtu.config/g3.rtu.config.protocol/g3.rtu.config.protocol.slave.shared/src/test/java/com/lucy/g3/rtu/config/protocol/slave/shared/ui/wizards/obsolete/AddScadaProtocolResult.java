/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.wizards.obsolete;

import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.IScadaProtocolGenerator;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.ScadaProtocolGenerators;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;


/**
 *
 */
class AddScadaProtocolResult implements WizardResultProducer {
  private final ScadaProtocolManager manager;
  
  public AddScadaProtocolResult(ScadaProtocolManager manager) {
    super();
    this.manager = manager;
  }

  @Override
  public Object finish(Map wizardData) throws WizardException {
    ProtocolType type = AddScadaProtocolInitial.getSelectedProtocol(wizardData);
    
    IProtocolFactory factory = ProtocolFactories.getFactory(type);
    if(factory == null)
      throw new WizardException("The factory is not found for protocol:"+type);
    
    
    IScadaProtocol newProtocol = (IScadaProtocol) factory.createProtocol();
    manager.add(newProtocol);
    
    
    if(Boolean.TRUE == wizardData.get(AddScadaProtocolSecond.KEY_ADD_CHANNEL)) {
      IProtocolChannel channel = newProtocol.addChannel("Sample Channel");
      
      if(Boolean.TRUE == wizardData.get(AddScadaProtocolSecond.KEY_ADD_SESSION)) {
        IProtocolSession sesn = channel.addSession("Sample Session");
        
        if(Boolean.TRUE == wizardData.get(AddScadaProtocolSecond.KEY_GEN_POINTS)) {
          IScadaProtocolGenerator gen = ScadaProtocolGenerators.getFactory(type);
          if(gen == null)
            throw new WizardException("The generator is not found for protocol:"+type);
          
          gen.genPoints(manager.getOwner(), sesn);
        }
      }
    }
    
    
    return newProtocol;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}

