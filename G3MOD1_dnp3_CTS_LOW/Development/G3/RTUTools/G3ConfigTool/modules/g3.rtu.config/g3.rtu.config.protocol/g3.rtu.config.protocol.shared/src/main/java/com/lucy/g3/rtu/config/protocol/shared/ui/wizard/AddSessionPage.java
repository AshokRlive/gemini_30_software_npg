/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.builder.DefaultFormBuilder;

class AddSessionPage extends WizardPage {

  public final static String KEY_SESSION_NAME = "sessionName";
  public final static String KEY_GEN_IO_MAP = "genIOMap";


  public AddSessionPage() {
    super("Add a session");
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = ProtocolAddingWizards.createPanelBuilder(this);
    JCheckBox cbox;

    JTextField tf = new JTextField("New Session");
    tf.setName(KEY_SESSION_NAME);
    builder.append("Session Name:", tf);
    builder.nextLine();

    
    builder.appendGlueRow();
    builder.nextLine();
    
    cbox = new JCheckBox("Generate IO map");
    cbox.setName(KEY_GEN_IO_MAP);
    cbox.setSelected(true);
    builder.append("", cbox);
    builder.nextLine();
    cbox.setSelected(true);
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    /* Validate Create Session*/
    if(getWizardData(KEY_GEN_IO_MAP) == Boolean.TRUE) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    } else {
      setForwardNavigationMode(WizardController.MODE_CAN_FINISH);
    }
    
    return null;
  }
}
