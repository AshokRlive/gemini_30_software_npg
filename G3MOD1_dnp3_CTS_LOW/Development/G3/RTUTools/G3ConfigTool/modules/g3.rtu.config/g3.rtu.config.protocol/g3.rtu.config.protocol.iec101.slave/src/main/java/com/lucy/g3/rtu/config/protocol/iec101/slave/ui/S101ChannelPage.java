/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.PortsUtility;
import com.lucy.g3.rtu.config.port.ui.common.SerialPortSelectPane;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101ChannelConf;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101ChannelSerialConf;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Session;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolItemManagerPanel;
import com.lucy.g3.rtu.config.protocol.shared.ui.panels.HintPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigListPage;
import com.lucy.g3.xml.gen.common.IEC870Enum.LINKCNFM;
import com.lucy.g3.xml.gen.common.IEC870Enum.LINK_MODE;

/**
 * The page of S101 channel.
 */
public class S101ChannelPage extends AbstractConfigListPage {

  private final S101SessionManagerModel sessionMgrModel;

  private final PresentationModel<S101ChannelConf> pmChannel;
  private final PresentationModel<S101ChannelSerialConf> pmSerial;

  private final S101Channel channel;

  private final PortsManager portsManager;
  public S101ChannelPage(S101Channel channel, PortsManager portsManager) {
    super(channel);
    this.portsManager = portsManager;
    this.channel = channel;

    sessionMgrModel = new S101SessionManagerModel(channel);
    pmChannel = new PresentationModel<>(channel.getChannelConfig());
    pmSerial = new PresentationModel<>(channel.getChannelConfig().getSerialConf());

    // Bind node name to channel name.
    ValueModel vm = pmChannel.getModel(S101ChannelConf.PROPERTY_CHANNEL_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }

  @Override
  protected void init() throws Exception {
    super.init();
    initComponents();
    populate();
    updateEnableState();
  }

  @Override
  public Action[] getContextActions() {
    return sessionMgrModel.getNodeActions();
  }

  private void createUIComponents() {
    panelGeneral = new JXTaskPane("General");
    
    panelSerial = new SerialPortSelectPane(
        pmSerial.getModel(S101ChannelSerialConf.PROPERTY_SERIAL_PORT),
        PortsUtility.getSerialPortsListModel(portsManager));
    
    ComponentsFactory factory; 
    
    factory = new ComponentsFactory(IEC870Constraints.INSTANCE_101, IEC870Constraints.PREFIX_CHANNEL, pmSerial);
    ftfFirstCharWait = factory.createNumberField(S101ChannelSerialConf.PROPERTY_FIRST_CHAR_WAIT);
    
    // ================== Create channel components ===================
    factory = new ComponentsFactory(IEC870Constraints.INSTANCE_101, IEC870Constraints.PREFIX_CHANNEL, pmChannel);

    tfChannelName = factory.createTextField(S101ChannelConf.PROPERTY_CHANNEL_NAME);
    ftfTransmitted = factory.createNumberField(S101ChannelConf.PROPERTY_TX_FRAME_SIZE);
    ftfReceived = factory.createNumberField(S101ChannelConf.PROPERTY_RX_FRAME_SIZE);
    ftfLinkAddressSize = factory.createNumberField(S101ChannelConf.PROPERTY_LINK_ADDR_SIZE);
    comboLinkMode = factory.createComboBox(S101ChannelConf.PROPERTY_LINK_MODE, LINK_MODE.values());
    checkBoxOneCharAckAllowed = factory.createCheckBox(S101ChannelConf.PROPERTY_ONECHAR_ACK_ALLOWED);
    checkBoxOneCharRespAllowed = factory.createCheckBox(S101ChannelConf.PROPERTY_ONECHAR_RESP_ALLOWED);
    ftfRcvTimeout = factory.createNumberField(S101ChannelConf.PROPERTY_RX_FRAME_TIMEOUT);
    comboConfirmMode = factory.createComboBox(S101ChannelConf.PROPERTY_CONFIRM_MODE, LINKCNFM.values());
    ftfConfirmTimeout = factory.createNumberField(S101ChannelConf.PROPERTY_CONFIRM_TIMEOUT);
    ftfMaxRetries = factory.createNumberField(S101ChannelConf.PROPERTY_MAX_RETRIES);
    ftfTestFramePeriod = factory.createNumberField(S101ChannelConf.PROPERTY_TEST_FRAME_PERIOD);
    ftfOfflinePollPeriod = factory.createNumberField(S101ChannelConf.PROPERTY_OFFLINE_POLL_PERIOD);
    ftfIncrementalTimeout = factory.createNumberField(S101ChannelConf.PROPERTY_INCREMENTAL_TIMEOUT);

    // Create session manager panel
    sessionListPanel = new ProtocolItemManagerPanel(
        sessionMgrModel.getSelectionInList(),
        sessionMgrModel.getPageActions(),
        getViewAction());

    panelHint = new HintPanel(S101ChannelPage.class);
  }

  private void populate() {
    settingsPane.add(panelGeneral);
    settingsPane.add(panelSerial);
    settingsPane.add(panelDatalinkLayer);
  }

  @Override
  protected SelectionInList<?> getSelectionInList() {
    return sessionMgrModel.getSelectionInList();
  }

  @Override
  protected ListModel<S101Session> getSelsectionListModel() {
    return channel.getItemListModel();
  }

  private void comboLinkModeActionPerformed(ActionEvent e) {
    updateEnableState();
  }

  private void updateEnableState() {
    Object mode = comboLinkMode.getSelectedItem();
    ftfTestFramePeriod.setEnabled( mode == LINK_MODE.LINK_MODE_BALANCED);
    ftfOfflinePollPeriod.setEnabled(mode == LINK_MODE.LINK_MODE_UNBALANCED);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    JScrollPane scrollPane0 = new JScrollPane();
    settingsPane = new JXTaskPaneContainer();
    labelChnName = new JLabel();
    panelDatalinkLayer = new JXTaskPane("Link Layer");
    label2 = new JLabel();
    label1 = new JLabel();
    labeloctets2 = new JLabel();
    lblRxFrameSize = new JLabel();
    labeloctets0 = new JLabel();
    lblTxFrameSize = new JLabel();
    labeloctets1 = new JLabel();
    label6 = new JLabel();
    label8 = new JLabel();
    label15 = new JLabel();
    label5 = new JLabel();
    label16 = new JLabel();
    label11 = new JLabel();
    label17 = new JLabel();
    label12 = new JLabel();
    label3 = new JLabel();
    label7 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label4 = new JLabel();
    label18 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane0 ========
      {
        scrollPane0.setBorder(null);
        scrollPane0.setViewportView(settingsPane);
      }
      tabbedPane1.addTab("Channel Settings", scrollPane0);
      tabbedPane1.addTab("Sessions", sessionListPanel);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    add(panelHint, BorderLayout.SOUTH);

    //======== panelGeneral ========
    {
      panelGeneral.setOpaque(false);
      panelGeneral.setLayout(new FormLayout(
          "default, [80dlu,default]",
          "default"));

      //---- labelChnName ----
      labelChnName.setText("Channel Name:");
      panelGeneral.add(labelChnName, CC.xy(1, 1));

      //---- tfChannelName ----
      tfChannelName.setColumns(8);
      panelGeneral.add(tfChannelName, CC.xy(2, 1));
    }

    //======== panelDatalinkLayer ========
    {
      panelDatalinkLayer.setLayout(new FormLayout(
          "right:default, $lcgap, [80dlu,default], $lcgap, pref",
          "default, $lgap, default, $pgap, default, $lgap, default, $pgap, default, $lgap, default, $rgap, 2*(default, $lgap), default, $rgap, default, $pgap, default, $lgap, default, $pgap, default, $lgap, default"));

      //---- label2 ----
      label2.setText("Link Mode:");
      panelDatalinkLayer.add(label2, CC.xy(1, 1));

      //---- comboLinkMode ----
      comboLinkMode.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          comboLinkModeActionPerformed(e);
        }
      });
      panelDatalinkLayer.add(comboLinkMode, CC.xy(3, 1));

      //---- label1 ----
      label1.setText("Link Address Size:");
      panelDatalinkLayer.add(label1, CC.xy(1, 3));
      panelDatalinkLayer.add(ftfLinkAddressSize, CC.xy(3, 3));

      //---- labeloctets2 ----
      labeloctets2.setText("octets");
      panelDatalinkLayer.add(labeloctets2, CC.xy(5, 3));

      //---- lblRxFrameSize ----
      lblRxFrameSize.setText("Transmitted Frame:");
      panelDatalinkLayer.add(lblRxFrameSize, CC.xy(1, 5));
      panelDatalinkLayer.add(ftfTransmitted, CC.xy(3, 5));

      //---- labeloctets0 ----
      labeloctets0.setText("octets");
      panelDatalinkLayer.add(labeloctets0, CC.xy(5, 5));

      //---- lblTxFrameSize ----
      lblTxFrameSize.setText("Received Frame:");
      panelDatalinkLayer.add(lblTxFrameSize, CC.xy(1, 7));
      panelDatalinkLayer.add(ftfReceived, CC.xy(3, 7));

      //---- labeloctets1 ----
      labeloctets1.setText("octets");
      panelDatalinkLayer.add(labeloctets1, CC.xy(5, 7));

      //---- label6 ----
      label6.setText("Confirm Mode:");
      panelDatalinkLayer.add(label6, CC.xy(1, 9));
      panelDatalinkLayer.add(comboConfirmMode, CC.xy(3, 9));

      //---- label8 ----
      label8.setText("Confirm Timeout:");
      panelDatalinkLayer.add(label8, CC.xy(1, 11));
      panelDatalinkLayer.add(ftfConfirmTimeout, CC.xy(3, 11));

      //---- label15 ----
      label15.setText("ms");
      panelDatalinkLayer.add(label15, CC.xy(5, 11));

      //---- label5 ----
      label5.setText("Received Frame Timeout:");
      panelDatalinkLayer.add(label5, CC.xy(1, 13));
      panelDatalinkLayer.add(ftfRcvTimeout, CC.xy(3, 13));

      //---- label16 ----
      label16.setText("ms");
      panelDatalinkLayer.add(label16, CC.xy(5, 13));

      //---- label11 ----
      label11.setText("Incremental Timeout:");
      panelDatalinkLayer.add(label11, CC.xy(1, 15));
      panelDatalinkLayer.add(ftfIncrementalTimeout, CC.xy(3, 15, CC.FILL, CC.DEFAULT));

      //---- label17 ----
      label17.setText("ms");
      panelDatalinkLayer.add(label17, CC.xy(5, 15));

      //---- label12 ----
      label12.setText("Max Retries:");
      panelDatalinkLayer.add(label12, CC.xy(1, 17));
      panelDatalinkLayer.add(ftfMaxRetries, CC.xy(3, 17));

      //---- label3 ----
      label3.setText("First Char Wait:");
      panelDatalinkLayer.add(label3, CC.xy(1, 19));
      panelDatalinkLayer.add(ftfFirstCharWait, CC.xy(3, 19));

      //---- label7 ----
      label7.setText("ms");
      panelDatalinkLayer.add(label7, CC.xy(5, 19));

      //---- label13 ----
      label13.setText("Test Frame Period:");
      panelDatalinkLayer.add(label13, CC.xy(1, 21));
      panelDatalinkLayer.add(ftfTestFramePeriod, CC.xy(3, 21));

      //---- label14 ----
      label14.setText("ms");
      panelDatalinkLayer.add(label14, CC.xy(5, 21));

      //---- label9 ----
      label9.setText("Offline Poll Period:");
      panelDatalinkLayer.add(label9, CC.xy(1, 23));
      panelDatalinkLayer.add(ftfOfflinePollPeriod, CC.xy(3, 23));

      //---- label10 ----
      label10.setText("ms");
      panelDatalinkLayer.add(label10, CC.xy(5, 23));

      //---- label4 ----
      label4.setText("One Char Ack Allowed:");
      panelDatalinkLayer.add(label4, CC.xy(1, 25));
      panelDatalinkLayer.add(checkBoxOneCharAckAllowed, CC.xy(3, 25));

      //---- label18 ----
      label18.setText("One Char Response Allowed:");
      panelDatalinkLayer.add(label18, CC.xy(1, 27));
      panelDatalinkLayer.add(checkBoxOneCharRespAllowed, CC.xy(3, 27));
    }

    //---- panelSerial ----
    panelSerial.setTitle("Physical Layer");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JXTaskPaneContainer settingsPane;
  private JPanel sessionListPanel;
  private JPanel panelHint;
  private JPanel panelGeneral;
  private JLabel labelChnName;
  private JFormattedTextField tfChannelName;
  private JPanel panelDatalinkLayer;
  private JLabel label2;
  private JComboBox comboLinkMode;
  private JLabel label1;
  private JFormattedTextField ftfLinkAddressSize;
  private JLabel labeloctets2;
  private JLabel lblRxFrameSize;
  private JFormattedTextField ftfTransmitted;
  private JLabel labeloctets0;
  private JLabel lblTxFrameSize;
  private JFormattedTextField ftfReceived;
  private JLabel labeloctets1;
  private JLabel label6;
  private JComboBox comboConfirmMode;
  private JLabel label8;
  private JFormattedTextField ftfConfirmTimeout;
  private JLabel label15;
  private JLabel label5;
  private JFormattedTextField ftfRcvTimeout;
  private JLabel label16;
  private JLabel label11;
  private JFormattedTextField ftfIncrementalTimeout;
  private JLabel label17;
  private JLabel label12;
  private JFormattedTextField ftfMaxRetries;
  private JLabel label3;
  private JFormattedTextField ftfFirstCharWait;
  private JLabel label7;
  private JLabel label13;
  private JFormattedTextField ftfTestFramePeriod;
  private JLabel label14;
  private JLabel label9;
  private JFormattedTextField ftfOfflinePollPeriod;
  private JLabel label10;
  private JLabel label4;
  private JCheckBox checkBoxOneCharAckAllowed;
  private JLabel label18;
  private JCheckBox checkBoxOneCharRespAllowed;
  private SerialPortSelectPane panelSerial;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
