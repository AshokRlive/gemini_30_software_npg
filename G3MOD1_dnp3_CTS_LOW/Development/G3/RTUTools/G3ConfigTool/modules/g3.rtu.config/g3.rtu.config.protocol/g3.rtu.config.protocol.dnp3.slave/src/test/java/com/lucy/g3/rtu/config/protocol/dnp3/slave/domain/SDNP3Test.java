/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.util.Collections;
import java.util.HashMap;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;

public class SDNP3Test {

  private SDNP3 dnp3;


  @Before
  public void setUp() throws Exception {
    dnp3 = new SDNP3();
  }

  @After
  public void tearDown() throws Exception {
    dnp3 = null;
  }

  @Test
  public void test() {
    HashMap<String, Object> map = new HashMap<>();
    map.put("a", null);
    map.put("b", null);
    
    map.values().removeAll(Collections.singleton(null));
    
    Assert.assertTrue(map.isEmpty());

  }
  // @Test
  // public void test() {
  // VetoableBean bean = new VetoableBean();
  // bean.addVetoableChangeListener("value",new Listener());
  //
  // try {
  // bean.setValue(1);
  // fail("expected exception");
  // } catch (PropertyVetoException e) {
  // }
  // }

  // VetoableChangeListener vetoListener = new VetoableChangeListener() {
  // @Override
  // public void vetoableChange(PropertyChangeEvent evt)
  // throws PropertyVetoException {
  // // if((Long)(evt.getNewValue()) < 0)
  // throw new PropertyVetoException("txFrameSize should be >= 0",evt);
  // }
  // };
  // @Test
  // public void testValidate(){
  // ValidationResult result = dnp3.validate();
  // assertFalse(result.hasErrors());
  // assertFalse(result.hasWarnings());
  // }
  //
  // @Test
  // public void testValidate2() throws DuplicatedException{
  // DNP3Point p0 = DNP3Point.create(ProtocolPointType.AnalogueInput, null, 1);
  // DNP3Point p1 = DNP3Point.create(ProtocolPointType.AnalogueInput, null, 2);
  // dnp3.addPoint(p0);
  // dnp3.addPoint(p1);
  //
  // ValidationResult result = dnp3.validate();
  // //Expected 2 warnings cause points are not mapped to resource
  // assertTrue(result.hasErrors());
  // assertEquals(2,result.getErrors().size());
  // }

}
