/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import java.awt.Color;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3User;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_KEYWRAP;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_USER_ROLE;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
public class SDNP3UserEditorDialog extends AbstractEditorDialog<SDNP3User> {

  private Logger log = Logger.getLogger(SDNP3UserEditorDialog.class);
  private final SDNP3UserEditorDialogCallback callback;
  
  public SDNP3UserEditorDialog(Frame parent, SDNP3User user, SDNP3UserEditorDialogCallback callback) {
    super(parent, user);
    this.callback = callback;
    setTitle("Editing DNP3 User");
    initComponents();
    initComponentsBinding();
    initEventHandling();
    updateKeyFormatter(user.getKeyWrapAlgorithm());
  }

  private void updateKeyFormatter(DNPAUTH_KEYWRAP keyWrapAlgorithm) {
    StringBuffer maskBuf = new StringBuffer();
    for (int i = 0; i < keyWrapAlgorithm.getKeyLength(); i++) {
      maskBuf.append("HH"); // Append mask for hex decimal
      
      if(i < keyWrapAlgorithm.getKeyLength() - 1)
        maskBuf.append(SDNP3User.UDPATE_KEY_DELIMITER);
    }
    
    
    MaskFormatter formatter = null;
    try {
      formatter = new MaskFormatter(maskBuf.toString());
      formatter.setPlaceholderCharacter(SDNP3User.UDPATE_KEY_DELIMITER);
      formatter.setCommitsOnValidEdit(true);
      formatter.setAllowsInvalid(false);
    } catch (java.text.ParseException exc) {
        log.error("formatter is bad: " + exc.getMessage());
    }
    
    if(formatter != null) {
      ftfUpdateKey.setFormatterFactory(new DefaultFormatterFactory(formatter));
      labelKey.setText(keyWrapAlgorithm == DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES128 ? 
          "Update Key(16 octets):":"Update Key(32 octets):");
      
      // Fixing a bug that when change from 256 to 128, the buffered key is not updated.
      if(keyWrapAlgorithm == DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES128)
        ftfUpdateKey.setValue(ftfUpdateKey.getText());
    }
  }
  
  private void initEventHandling() {
    /* Observe the change of key wrap algorithm and update the formatter of key field*/
    getModel().getBufferedModel(SDNP3User.PROPERTY_KEY_WRAP_ALGORITHM)
    .addValueChangeListener(new PropertyChangeListener() {
      
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateKeyFormatter((DNPAUTH_KEYWRAP)evt.getNewValue());
      }

    });
    
    
    /* Update background when input invalid key*/
    final Color orginBG = ftfUpdateKey.getBackground();
    PropertyChangeListener pcl = new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if ("editValid".equals(evt.getPropertyName())) {
          JFormattedTextField jtf = (JFormattedTextField) evt.getSource();
          boolean valid = (Boolean) (evt.getNewValue());

          if (valid) {
            jtf.setBackground(orginBG);
            setOkEnabled(true);
          } else {
            setOkEnabled(false);
            ValidationComponentUtils.setErrorBackground(jtf);
          }
        }
      }
    };
    ftfUpdateKey.addPropertyChangeListener("editValid", pcl);
    
    
    SpinnerWheelSupport.installMouseWheelSupport(spUserNumber);
  }

  private void initComponentsBinding() {
    PresentationModel<SDNP3User> pm = getModel();
    BufferedValueModel vm;
    
    /* Bind user number spinner*/
    vm = pm.getBufferedModel(SDNP3User.PROPERTY_USER_NUMBER);
    int defaultValue = (int) vm.getValue();
    if(defaultValue < SDNP3User.MIN_USER_NUMBER || defaultValue > SDNP3User.MAX_USER_NUMBER)
      defaultValue = SDNP3User.MIN_USER_NUMBER;
    vm.setValue(defaultValue);
    SpinnerNumberModel fdigitsModel = SpinnerAdapterFactory.createNumberAdapter(vm,
         defaultValue, SDNP3User.MIN_USER_NUMBER, SDNP3User.MAX_USER_NUMBER, 1);
    spUserNumber.setModel(fdigitsModel);
    
    /* Bind algorithm combox*/
    vm = pm.getBufferedModel(SDNP3User.PROPERTY_KEY_WRAP_ALGORITHM);
    Bindings.bind(combKeyAlgorithm, new SelectionInList<>(DNPAUTH_KEYWRAP.values(), vm));
    
    /* Bind role combox*/
    vm = pm.getBufferedModel(SDNP3User.PROPERTY_USER_ROLE);
    Bindings.bind(combUserRole, new SelectionInList<>(DNPAUTH_USER_ROLE.values(), vm));
    
    /* Bind user name text field*/
    vm = pm.getBufferedModel(SDNP3User.PROPERTY_USER_NAME);
    Bindings.bind(tfUserName, vm);
    
    /* Bind update key text field*/
    vm = pm.getBufferedModel(SDNP3User.PROPERTY_UDPATE_KEY);
    Bindings.bind(ftfUpdateKey, vm);
  }


  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    label1 = new JLabel();
    spUserNumber = new JSpinner();
    label2 = new JLabel();
    tfUserName = new JTextField();
    label3 = new JLabel();
    combUserRole = new JComboBox();
    label5 = new JLabel();
    combKeyAlgorithm = new JComboBox();
    labelKey = new JLabel();
    ftfUpdateKey = new JFormattedTextField();

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "default, $lcgap, [100dlu,default], $lcgap, [200dlu,default]:grow",
          "4*(default, $ugap), fill:default, $lgap, fill:[50dlu,default]:grow"));

      //---- label1 ----
      label1.setText("User Number:");
      contentPanel.add(label1, CC.xy(1, 1));
      contentPanel.add(spUserNumber, CC.xy(3, 1));

      //---- label2 ----
      label2.setText("User Name:");
      contentPanel.add(label2, CC.xy(1, 3));
      contentPanel.add(tfUserName, CC.xy(3, 3));

      //---- label3 ----
      label3.setText("User Role:");
      contentPanel.add(label3, CC.xy(1, 5));
      contentPanel.add(combUserRole, CC.xy(3, 5));

      //---- label5 ----
      label5.setText("Key Wrap Algorithm:");
      contentPanel.add(label5, CC.xy(1, 7));
      contentPanel.add(combKeyAlgorithm, CC.xy(3, 7));

      //---- labelKey ----
      labelKey.setText("Update Key:");
      contentPanel.add(labelKey, CC.xy(1, 9));
      contentPanel.add(ftfUpdateKey, CC.xywh(3, 9, 3, 2));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel label1;
  private JSpinner spUserNumber;
  private JLabel label2;
  private JTextField tfUserName;
  private JLabel label3;
  private JComboBox combUserRole;
  private JLabel label5;
  private JComboBox combKeyAlgorithm;
  private JLabel labelKey;
  private JFormattedTextField ftfUpdateKey;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  @Override
  protected BufferValidator createValidator(PresentationModel<SDNP3User> target) {
    return new Validator(target);
  }

  @Override
  protected void editingItemChanged(SDNP3User newItem) {
    // Do nothing.
  }
  
  public interface SDNP3UserEditorDialogCallback {
    Collection<SDNP3User> getExistingUsers();
  }
  
  private class Validator extends BufferValidator {
    public Validator(PresentationModel<?> target) {
      super(target);
    }

    @Override
    public void validate(ValidationResult result) {
      checkUserNumberValid(result);
      checkUpdateKeyLength(result);
    }

    private void checkUpdateKeyLength(ValidationResult result) {
      DNPAUTH_KEYWRAP algorithm = (DNPAUTH_KEYWRAP) target.getBufferedValue(SDNP3User.PROPERTY_KEY_WRAP_ALGORITHM);
      String key = (String) target.getBufferedValue(SDNP3User.PROPERTY_UDPATE_KEY);
      int keyLength = key.split(SDNP3User.UDPATE_KEY_DELIMITER_STR).length;
      
      if(algorithm != null && algorithm.getKeyLength() != keyLength) {
        result.addError(String.format("Invalid key length:%d, expected:%d",keyLength, algorithm.getKeyLength()));
      }
    }

    private void checkUserNumberValid(ValidationResult result) {
      int userNum = (int) target.getBufferedValue(SDNP3User.PROPERTY_USER_NUMBER);
      
      // Check if the user number has been used
      Collection<SDNP3User> existingUsers = callback.getExistingUsers();
      for (SDNP3User user: existingUsers) {
        if(user != getModel().getBean() && userNum == user.getUserNumber()) {
          result.addError("The user number already exists: " + userNum);
        }
        
      }
    }
    
  }
}
