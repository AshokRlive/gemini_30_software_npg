/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import java.util.Collection;

import com.lucy.g3.itemlist.IItemListManager;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;

/**
 * The interface of protocol stack of RTU.
 *
 * @param <ChannelT>
 *          the type of Protocol Channel .
 */
public interface IProtocol<ChannelT extends IProtocolChannel<?>> extends IItemListManager<ChannelT>, IContainerValidation {

  /**
   * Gets the type enum of this protocol.
   *
   * @return non-null type enum
   */
  ProtocolType getProtocolType();
  
  /**
   * Gets the name of this protocol.
   *
   * @return Non-null string.
   */
  String getProtocolName();

  /**
   * Deletes this protocol.
   */
  void delete();

  /**
   * Adds a channel by giving a name.
   *
   * @param channelName
   *          the name of new channel
   * @return new channel object. Null if adding fails.
   */
  ChannelT addChannel(String channelName);

  Collection<ChannelT> getAllChannels();
  
  Collection<? extends IProtocolSession> getAllSessions();

  void setManager(IProtocolManager<?> manager);

  IProtocolManager<?> getManager();
  
}
