/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;


public interface IScadaProtocolSession extends IProtocolSession {
  
  @Override
  IScadaIoMap getIomap();

}

