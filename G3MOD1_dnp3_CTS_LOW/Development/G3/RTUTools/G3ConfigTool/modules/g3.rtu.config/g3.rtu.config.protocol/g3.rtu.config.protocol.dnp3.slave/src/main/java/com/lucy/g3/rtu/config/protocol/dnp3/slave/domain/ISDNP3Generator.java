/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.IScadaProtocolGenerator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;


public interface ISDNP3Generator extends IScadaProtocolGenerator {

  void genOutputPointsForAllSessions(GenerateOptions option, SDNP3 owner, Collection<ICLogic> logic);

  void genInputPointsForAllSessions(GenerateOptions option, SDNP3 owner, Collection<VirtualPoint> vpoints);

}

