/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.validate;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolNameFactory;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The abstract validator for validating protocol.
 */
public class ProtocolChannelValidator extends AbstractContainerValidator<IProtocolChannel<?>> {
  private ArrayList<IValidation> additionalValidations = new ArrayList<>();
  
  public ProtocolChannelValidator(IProtocolChannel<?> target) {
    super(target);
  }

  @Override
  public Collection<IValidation> getValidatableChildrenItems() {
    ArrayList<IValidation> items = new ArrayList<IValidation>();

     Collection<?> sessions = target.getAllSessions();
     for (Object ch : sessions) {
       if(ch instanceof IValidation)
         items.add((IValidation) ch);
    }
     
    items.addAll(additionalValidations);
    return items;
  }
  
  public void addAdditionalValidation(IValidation... validation){
    for (int i = 0; i < validation.length; i++) {
      additionalValidations.add(validation[i]);
    }
  }

  @Override
  public String getTargetName() {
    return ProtocolNameFactory.getAbsoluteName(target);
  }
}
