/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.domain;

import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * <p>
 * The Interface of Master Protocol Module.
 * </p>
 * A Master Protocol device is a module integrated to G3 RTU via Master Protocol
 * Stack, such as ModBus Master.
 */
public interface IMasterProtocolModule extends IOModule {

  String getDeviceName();

  void setDeviceName(String name);

  String getDeviceModel();

  MODULE_ID getDeviceId();

  void setDeviceId(MODULE_ID id);

  IProtocolChannel<? extends IMasterProtocolSession> getChannel();

  IMasterProtocolSession getSession();

  IMasterProtocolIoMap<?> getIomap();
}
