/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;


/**
 *
 */
public class SDNPAuthEnums {

  public enum DNPAUTH_KEYWRAP {
    DNPAUTH_KEYWRAP_AES128(1, "AES128", 16),
    DNPAUTH_KEYWRAP_AES256(2, "AES256", 32);
    
    DNPAUTH_KEYWRAP(int value, String description, int keyLength) {
      this.value = value;
      this.description = description;
      this.keyLength = keyLength;
    }
    
    public static DNPAUTH_KEYWRAP forValue(int value) {
      DNPAUTH_KEYWRAP[] values = DNPAUTH_KEYWRAP.values();
      for (int i = 0; i < values.length; i++) {
        if(values[i].value == value){
          return values[i];
        }
      }
      return null;
    }
    
    public int getValue() {
      return value;
    }

    
    public int getKeyLength() {
      return keyLength;
    }

    public String getDescription() {
      return description;
    }

    @Override
    public String toString() {
      return getDescription();
    }

    private final int keyLength;
    private final int value;
    private final String description;
  }

  
  public  enum DNPAUTH_MAC {
    DNPAUTH_MAC_SHA1_4OCTET        (1, "SHA1_4OCTET"),
    DNPAUTH_MAC_SHA1_10OCTET       (2, "SHA1_10OCTET"),
    DNPAUTH_MAC_SHA256_8OCTET      (3, "SHA256_8OCTET"),
    DNPAUTH_MAC_SHA256_16OCTET     (4, "SHA256_16OCTET"),
    DNPAUTH_MAC_SHA1_8OCTET        (5, "SHA1_8OCTET"),
    DNPAUTH_MAC_AESGMAC_12OCTET    (6, "AESGMAC_12OCTET");
    
    
    DNPAUTH_MAC(int value, String description) {
      this.value = value;
      this.description = description;
    }
    
    public static DNPAUTH_MAC forValue(int value) {
      DNPAUTH_MAC[] values = DNPAUTH_MAC.values();
      for (int i = 0; i < values.length; i++) {
        if(values[i].value == value){
          return values[i];
        }
      }
      
      return null;
    }
    
    public int getValue() {
      return value;
    }
    
    @Override
    public String toString() {
      return getDescription();
    }
    
    public String getDescription() {
      return description;
    }

    private final int value;
    private final String description;
  }

  
  public enum DNPAUTH_USER_ROLE{
    DNPAUTH_USER_ROLE_VIEWER        (0,"VIEWER"),/* Access to Monitor Data. */  
    DNPAUTH_USER_ROLE_OPERATOR      (1,"OPERATOR"),/* Access to Monitor Data and Controls */
    DNPAUTH_USER_ROLE_ENGINEER      (2,"ENGINEER"),/* Access to Monitor Data, Read/Write/Delete Data Files, Change Config, and Local Login */
    DNPAUTH_USER_ROLE_INSTALLER     (3,"INSTALLER"),/* Access to Monitor Data, Read/Write Data Files, Change Config, Firmware, and Local Login */
    DNPAUTH_USER_ROLE_SECADM        (4,"SECADM"),/* Access to Change Security Config, Firmware, and Local Login */   
    DNPAUTH_USER_ROLE_SECAUD        (5,"SECAUD"),/* Access to Monitor Data, Read Files, and Local Login */    
    DNPAUTH_USER_ROLE_RBACMNT       (6,"RBACMNT"),/* Access to Monitor Data, Delete Files, Change Configuration, and Change Security Roles */   

    /* Access to ALL functions. To be used when there is only a single user. 
     * This was added at March 14 WG C12(1815) meeting.
     */
    DNPAUTH_USER_ROLE_SINGLEUSER    (32768,"SINGLE USER"); 
    
    DNPAUTH_USER_ROLE(int value, String description) {
      this.value = value;
      this.description = description;
    }
    
    public static DNPAUTH_USER_ROLE forValue(int value) {
      DNPAUTH_USER_ROLE[] values = DNPAUTH_USER_ROLE.values();
      for (int i = 0; i < values.length; i++) {
        if(values[i].value == value){
          return values[i];
        }
      }
      
      return null;
    }
    
    public int getValue() {
      return value;
    }

    public String getDescription() {
      return description;
    }
    
    @Override
    public String toString() {
      return getDescription();
    }

    private final int value;
    private final String description;
  }
  
}

