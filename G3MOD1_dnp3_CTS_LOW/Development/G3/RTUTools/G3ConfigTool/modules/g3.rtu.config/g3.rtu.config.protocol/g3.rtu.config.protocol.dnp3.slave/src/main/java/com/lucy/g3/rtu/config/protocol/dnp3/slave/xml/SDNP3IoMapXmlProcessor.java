/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.xml;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Logger;

import com.g3schema.ns_common.VirtualPointRefT;
import com.g3schema.ns_sdnp3.FrozenCounterT;
import com.g3schema.ns_sdnp3.SDNP3CounterPointT;
import com.g3schema.ns_sdnp3.SDNP3EventConfigT;
import com.g3schema.ns_sdnp3.SDNP3InputPointT;
import com.g3schema.ns_sdnp3.SDNP3OutputPointT;
import com.g3schema.ns_sdnp3.SDNP3PointsT;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3IoMap;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3CounterPoint;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3InputPoint;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3OutputPoint;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointOutput;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.util.ScadaPointComparator;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.xml.gen.common.DNP3Enum.ACCEPTED_COMMAND;


/**
 * The Class SDNP3IoMapXmlProcessor.
 */
public class SDNP3IoMapXmlProcessor {
  
  private Logger log = Logger.getLogger(SDNP3IoMapXmlProcessor.class);
  
  private final SDNP3IoMap iomap;
  public SDNP3IoMapXmlProcessor(SDNP3IoMap iomap) {
    this.iomap = iomap;
  }
  public void writeToXML(SDNP3PointsT xml) {
    ScadaPointComparator sorter = new ScadaPointComparator();
    ArrayList<SDNP3Point> ppointlist;

    // AIConfig
    ppointlist = new ArrayList<SDNP3Point>(iomap.getPoints(SDNP3PointType.AnalogueInput));
    Collections.sort(ppointlist, sorter);
    for (SDNP3Point p : ppointlist) {
      
      if (p instanceof SDNP3InputPoint) {
        writeDNPInputPoint(xml.analogInput.append(), (SDNP3InputPoint) p);
      }
    }

    // BIConfig
    ppointlist = new ArrayList<SDNP3Point>(iomap.getPoints(SDNP3PointType.BinaryInput));
    Collections.sort(ppointlist, sorter);
    for (SDNP3Point p : ppointlist) {
      if (p instanceof SDNP3InputPoint) {
        writeDNPInputPoint(xml.binaryInput.append(), (SDNP3InputPoint) p);
      }
    }

    // DBIConfig
    ppointlist = new ArrayList<SDNP3Point>(iomap.getPoints(SDNP3PointType.DoubleBinaryInput));
    Collections.sort(ppointlist, sorter);
    for (SDNP3Point p : ppointlist) {
      writeDNPInputPoint(xml.doubleBinaryInput.append(), (SDNP3InputPoint) p);
    }

    // BOConfig
    ppointlist = new ArrayList<SDNP3Point>(iomap.getPoints(SDNP3PointType.BinaryOutput));
    Collections.sort(ppointlist, sorter);
    for (SDNP3Point p : ppointlist) {
      writeDNPOutputPoint(xml.binaryOutput.append(), (SDNP3OutputPoint)p);
    }

    // CounterConfig
    ppointlist = new ArrayList<SDNP3Point>(iomap.getPoints(SDNP3PointType.Counter));
    Collections.sort(ppointlist, sorter);
    for (SDNP3Point p : ppointlist) {
      writeDNPInputPoint(xml.counter.append(), (SDNP3InputPoint) p);
    }
    
    // AOConfig
    ppointlist = new ArrayList<SDNP3Point>(iomap.getPoints(SDNP3PointType.AnalogOutput));
    Collections.sort(ppointlist, sorter);
    for (SDNP3Point p : ppointlist) {
      writeDNPOutputPoint(xml.analogueOutput.append(), (SDNP3OutputPoint)p);
    }
  
  }
  private void writeDNPInputPoint(SDNP3InputPointT xml_point, SDNP3InputPoint inputPoint) {
    DNP3ObjConfig evt = inputPoint.getEventConf();
    
    // Common Attribute
    xml_point.protocolID.setValue(inputPoint.getPointID());
    xml_point.eventOnlyWhenConnected.setValue(evt.isEventOnlyWhenConnected());
    xml_point.defaultVariation.setValue(evt.getStaticVariation());
    xml_point.eventDefaultVariation.setValue(evt.getEventVariation());
    xml_point.eventClass.setValue(evt.getEventClass());
    xml_point.customVariation.setValue(evt.isCustomVariationEnabled());
    xml_point.enableClass0.setValue(evt.isEnableClass0());
    xml_point.enable.setValue(inputPoint.isEnabled());
    xml_point.enableStoreEvent.setValue(inputPoint.isEnableStoreEvent());
    xml_point.description.setValue(inputPoint.getCustomDescription());

    /* Virtual point reference*/
    ScadaPointSource mapRes = inputPoint.getSource();
    if (mapRes == null && inputPoint.isEnabled()) {
      String msg = "Protocol point \"" + inputPoint.getFullName() + "\" is not mapped to any resource";
      //TODO
//      scadaWriter.log.error(msg);
//      scadaWriter.result.addError(msg);
    } else if (mapRes != null) {
      VirtualPointRefT xml_vpoint = xml_point.vpoint.append();
      xml_vpoint.pointGroup.setValue(mapRes.getGroup());
      xml_vpoint.pointID.setValue(mapRes.getId());
    }

    /* Counter */
    if (xml_point instanceof SDNP3CounterPointT) {
      SDNP3CounterPointT xml_counter = (SDNP3CounterPointT) xml_point;
      SDNP3CounterPoint counter = (SDNP3CounterPoint) inputPoint;
      xml_counter.useRolloverFlag.setValue(counter.isUseRolloverFlag());
      xml_counter.frozenFlag.setValue(counter.isFrozenCounterEnabled());
      
      // Frozen counter
      evt = counter.getFrozenCounterConf();
      FrozenCounterT xml_frozen_counter = xml_counter.frozenCounter.append(); 
      xml_frozen_counter.eventOnlyWhenConnected.setValue(evt.isEventOnlyWhenConnected());
      xml_frozen_counter.defaultVariation.setValue(evt.getStaticVariation());
      xml_frozen_counter.eventDefaultVariation.setValue(evt.getEventVariation());
      xml_frozen_counter.eventClass.setValue(evt.getEventClass());
      xml_frozen_counter.customVariation.setValue(evt.isCustomVariationEnabled());
      xml_frozen_counter.enableClass0.setValue(evt.isEnableClass0());
    }

  }

  private void writeDNPOutputPoint(SDNP3OutputPointT xml_point, SDNP3OutputPoint point) {
    // Attribute
    xml_point.protocolID.setValue(point.getPointID());
    xml_point.enable.setValue(point.isEnabled());
    xml_point.enableStoreEvent.setValue(point.isEnableStoreEvent());
    xml_point.description.setValue(point.getCustomDescription());
    
    
    ACCEPTED_COMMAND ac = point.getAccpetedCommand();
    if(ac != null ) {
      xml_point.acceptedCommand.setValue(ac.getValue());
    }
    
    DNP3ObjConfig evtConf = point.getEventConf();
    if(evtConf != null)
      writeDNPEvtConf(xml_point.outputEvent.append(), evtConf);
    
    ScadaPointSource mapRes = point.getSource();
    if (mapRes == null && point.isEnabled()) {
      String msg = "Protocol point \"" + point.getFullName() + "\" is not mapped to any resource";
      //TODO
//      scadaWriter.log.error(msg);
//      scadaWriter.result.addError(msg);
    } else if (mapRes != null) {
      xml_point.controlLogicGroup.setValue(mapRes.getGroup());
    }
  }

  private void writeDNPEvtConf(SDNP3EventConfigT xml, DNP3ObjConfig evt) {
    xml.eventOnlyWhenConnected.setValue(evt.isEventOnlyWhenConnected());
    xml.defaultVariation.setValue(evt.getStaticVariation());
    xml.eventDefaultVariation.setValue(evt.getEventVariation());
    xml.eventClass.setValue(evt.getEventClass());
    xml.customVariation.setValue(evt.isCustomVariationEnabled());
    xml.enableClass0.setValue(evt.isEnableClass0());
  }
  

  public void readSDNP3SessionIoMap(SDNP3PointsT xml_sesn_iomap,
      ScadaPointSourceRepository resource) throws DuplicatedException {

    // Analogue input points
    for (int i = 0; i < xml_sesn_iomap.analogInput.count(); i++) {
      readDNP3InputPoint(SDNP3PointType.AnalogueInput,
          xml_sesn_iomap.analogInput.at(i),resource);
    }

    // Binary input points
    for (int i = 0; i < xml_sesn_iomap.binaryInput.count(); i++) {
      readDNP3InputPoint(SDNP3PointType.BinaryInput,
          xml_sesn_iomap.binaryInput.at(i),resource);
    }

    // Double Binary input points
    for (int i = 0; i < xml_sesn_iomap.doubleBinaryInput.count(); i++) {
      readDNP3InputPoint(SDNP3PointType.DoubleBinaryInput,
          xml_sesn_iomap.doubleBinaryInput.at(i),resource);

    }

    // Counter input points
    for (int i = 0; i < xml_sesn_iomap.counter.count(); i++) {
      readDNP3InputPoint(SDNP3PointType.Counter, 
          xml_sesn_iomap.counter.at(i),resource);

    }
    
    // Frozen Counter input points
//    for (int i = 0; i < xml_sesn_iomap.counter.count(); i++) {
//      loadDNP3InputPoint(SDNP3PointType.FrozenCounter, session,
//          xml_sesn_iomap.counter.at(i));
//      
//    }

    // Binary output points
    for (int i = 0; i < xml_sesn_iomap.binaryOutput.count(); i++) {
      readDNP3OutputPoint(SDNP3PointType.BinaryOutput, 
          xml_sesn_iomap.binaryOutput.at(i),resource);
    }
    
    // Analogue output points
    for (int i = 0; i < xml_sesn_iomap.analogueOutput.count(); i++) {
      readDNP3OutputPoint(SDNP3PointType.AnalogOutput, 
          xml_sesn_iomap.analogueOutput.at(i),resource);
    }
  }

  private void readDNP3InputPoint(SDNP3PointType type,
       SDNP3InputPointT xml_point,ScadaPointSourceRepository resource)
      throws DuplicatedException {
    
    SDNP3InputPoint point = SDNP3PointFactory.createInput(iomap, type,
        xml_point.protocolID.getValue());
   
    DNP3ObjConfig evt = point.getEventConf();
    evt.setEventOnlyWhenConnected(xml_point.eventOnlyWhenConnected.getValue());
    evt.setStaticVariation(xml_point.defaultVariation.getValue());
    evt.setEventVariation(xml_point.eventDefaultVariation.getValue());
    evt.setCustomVariationEnabled(xml_point.customVariation.getValue());
    evt.setEventClass(xml_point.eventClass.getValue());
    evt.setEnableClass0(xml_point.enableClass0.getValue());
    
    point.setEnabled(xml_point.enable.getValue());
    if(xml_point.enableStoreEvent.exists()) {
      point.setEnableStoreEvent(xml_point.enableStoreEvent.getValue());
    }
    if (xml_point.description.exists()) {
      point.setCustomDescription(xml_point.description.getValue());
    }

    // Load counter point configuration
    if (xml_point instanceof SDNP3CounterPointT) {
      SDNP3CounterPoint counter = (SDNP3CounterPoint) point;
      SDNP3CounterPointT xml_counter = (SDNP3CounterPointT) xml_point;
      counter.setUseRolloverFlag(xml_counter.useRolloverFlag.getValue());
      counter.setFrozenCounterEnabled(xml_counter.frozenFlag.getValue());
      
      // Load frozen counter configuration
      if(xml_counter.frozenCounter.exists()) {
        evt = counter.getFrozenCounterConf();
        FrozenCounterT xml_frozen = xml_counter.frozenCounter.first();
        evt.setEventOnlyWhenConnected(xml_frozen.eventOnlyWhenConnected.getValue());
        evt.setStaticVariation(xml_frozen.defaultVariation.getValue());
        evt.setEventVariation(xml_frozen.eventDefaultVariation.getValue());
        evt.setCustomVariationEnabled(xml_frozen.customVariation.getValue());
        evt.setEventClass(xml_frozen.eventClass.getValue());
        evt.setEnableClass0(xml_frozen.enableClass0.getValue());
      }
    
    }
    
    // Configure mapped virtual point
    if (xml_point.vpoint.exists()) {
      point.mapTo(resource.getInputSourceByRef(xml_point.vpoint.first()));
    }

    iomap.addPoint(point);

    // Verify mapping
    if (point.getSource() == null) {
      log.warn(String.format("Point \"%s\" is not mapped to any resource", point));
    }
  }

  private void readDNP3OutputPoint(SDNP3PointType type,
      SDNP3OutputPointT xml_point,ScadaPointSourceRepository resource)
      throws DuplicatedException {
    SDNP3OutputPoint point = SDNP3PointFactory.createOutput(iomap, type,
        xml_point.protocolID.getValue());
    point.setPointID(xml_point.protocolID.getValue());
    point.setEnabled(xml_point.enable.getValue());
    if(xml_point.enableStoreEvent.exists()) {
      point.setEnableStoreEvent(xml_point.enableStoreEvent.getValue());
    }
    if (xml_point.description.exists()) {
      point.setCustomDescription(xml_point.description.getValue());
    }

    if(xml_point.acceptedCommand.exists()) {
      point.setAccpetedCommand(ACCEPTED_COMMAND.forValue((int)xml_point.acceptedCommand.getValue()));
    }
    
    // Read output event config
    DNP3ObjConfig outputEvt = point.getEventConf();
    if(outputEvt != null) {
      if(xml_point.outputEvent.exists())
        readDNPEvtConf(xml_point.outputEvent.first(), outputEvt);
      else{
        readDNPEvtConf(null, outputEvt);
      }
    }    
    
    // Read source
    ScadaPointOutput source;
    if (xml_point.controlLogicGroup.exists()) {
      int group = (int) xml_point.controlLogicGroup.getValue();
      if(SDNP3PointType.isAnalogue(type))
        source = resource.getAnalogOutputSource(group);
      else
        source = resource.getDigitalOutputSource(group);
      
      point.mapTo(source);
    }
    iomap.addPoint(point);

    if (point.getSource() == null) {
      log.warn(String.format(
          "Point \"%s\" is not mapped to any resource", point));
    }
  }

  private void readDNPEvtConf(SDNP3EventConfigT xml, DNP3ObjConfig evt) {
    if(xml != null) {
      evt.setEventOnlyWhenConnected(xml.eventOnlyWhenConnected.getValue());
      evt.setStaticVariation(xml.defaultVariation.getValue());
      evt.setEventVariation(xml.eventDefaultVariation.getValue());
      evt.setCustomVariationEnabled(xml.customVariation.getValue());
      evt.setEventClass(xml.eventClass.getValue());
      evt.setEnableClass0(xml.enableClass0.getValue());
    } else {
      evt.setEventOnlyWhenConnected(false);
      evt.setStaticVariation(0);
      evt.setEventVariation(0);
      evt.setCustomVariationEnabled(true);
      evt.setEventClass(0);
      evt.setEnableClass0(false);
    }
  }
}

