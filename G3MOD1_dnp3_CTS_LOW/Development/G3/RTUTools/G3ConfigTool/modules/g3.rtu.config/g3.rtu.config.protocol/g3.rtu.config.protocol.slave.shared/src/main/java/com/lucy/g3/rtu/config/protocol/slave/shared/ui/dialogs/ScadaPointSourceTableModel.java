/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;

/**
 * The table model for selecting {@linkplain ScadaPointSource}.
 */
class ScadaPointSourceTableModel extends AbstractTableModel {

  static final int COLUMN_SELECT = 0;
  static final int COLUMN_RESOURCE_NAME = 1;
  static final int COLUMN_POINT_ID = 2;
  static final int COLUMN_DESCRIPTION = 3;

  private Logger log = Logger.getLogger(ScadaPointSourceTableModel.class);
  
  private final String[] columnNames;
  private final List<ScadaPointSource> sources;
  private final ScadaPointType type;

  private Long[] pointIDs;
  private String[] pointDescriptions;
  private Boolean[] selections;

  private boolean seqentialIdEnabled;
  private final IIdChecker idChecker;

  private final int sourceSize;

  public ScadaPointSourceTableModel(ScadaPointType type, List<ScadaPointSource> sources, IIdChecker idChecker) {
    this.columnNames =
        new String[] { "Select", type.isInput() ? "Input" : "Output", "Point ID", "Description(Optional)" };
    this.sources = sources;
    this.idChecker = idChecker;
    this.type = type;
    this.sourceSize = sources == null ? 0 : sources.size();
    this.pointIDs = new Long[sourceSize];
    this.pointDescriptions = new String[sourceSize];
    this.selections = new Boolean[sourceSize];
    for (int i = 0; i < selections.length; i++) {
      selections[i] = Boolean.FALSE;
    }
    
    if(sourceSize == 0) {
      log.warn("There is no mapping sources!");
    }
  }

  public void setSequentialIdEnabled(boolean seqentialIdEnabled) {
    this.seqentialIdEnabled = seqentialIdEnabled;
  }

  public void setSelection(int row, boolean selected) {
    if (row >= 0 && row < getRowCount() && isCellEditable(row, COLUMN_SELECT)) {
      setValueAt(selected, row, ScadaPointSourceTableModel.COLUMN_SELECT);
    }
  }

  public boolean hasSelections() {
    for (int i = 0; i < selections.length; i++) {
      if (selections[i] == Boolean.TRUE) {
        return true;
      }
    }
    return false;
  }

  public boolean checkPointIDNotEmpty() {
    for (int i = 0; i < selections.length; i++) {
      if (selections[i] == Boolean.TRUE && pointIDs[i] == null) {
        return false;
      }
    }
    return true;
  }

  @Override
  public int getRowCount() {
    return sourceSize;
  }

  @Override
  public int getColumnCount() {
    return columnNames.length;
  }

  @Override
  public String getColumnName(int column) {
    return columnNames[column];
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    switch (columnIndex) {
    case COLUMN_SELECT:
      return selections[rowIndex];

    case COLUMN_RESOURCE_NAME:
      return sources.get(rowIndex).getName();

    case COLUMN_POINT_ID:
      return pointIDs[rowIndex];

    case COLUMN_DESCRIPTION:
      return pointDescriptions[rowIndex];

    default:
      return null;
    }
  }

  public ScadaPointSource getResource(int index) {
    if (index >= 0 && index < sourceSize) {
      return sources.get(index);
    } else {
      return null;
    }
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
    case COLUMN_POINT_ID:
      return Long.class;
    case COLUMN_SELECT:
      return Boolean.class;
    case COLUMN_RESOURCE_NAME:
      return String.class;
    case COLUMN_DESCRIPTION:
      return String.class;
    default:
      return super.getColumnClass(columnIndex);
    }
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    /* Editable if the resource has not been mapped to any resource */
    if (columnIndex == COLUMN_SELECT) {
      return true;
    } else if (columnIndex == COLUMN_POINT_ID || columnIndex == COLUMN_DESCRIPTION) {
      return getValueAt(rowIndex, COLUMN_SELECT) == Boolean.TRUE;
    } else {
      return false;
    }
  }

  @Override
  public void setValueAt(Object aValue, final int rowIndex, final int columnIndex) {

    /* Set Protocol ID */
    if (columnIndex == COLUMN_POINT_ID) {
      Long id = (Long) aValue;
      if (idChecker.isExists(id) || idChecker.isOutOfRange(id) ) {
        return; // Invalid protocol ID value
      }

      long value = (Long) aValue;
      pointIDs[rowIndex] = value;

      /* Allocate sequential ID for the subsequent points */
      if (seqentialIdEnabled) {
        int rows = getRowCount();
        for (int i = rowIndex + 1; i < rows; i++) {
          // Check point is already selected
          if (selections[i] == Boolean.TRUE) {
            pointIDs[i] = findNextProtocolID(value + 1, type);
            value = pointIDs[i];
          }
        }

        // notify table to update
        if (rowIndex + 1 < rows) {
          fireTableRowsUpdated(rowIndex + 1, rows - 1);
        }
      }
    }

    /* Set protocol point selection */
    else if (columnIndex == COLUMN_SELECT) {
      if (aValue != null && aValue instanceof Boolean) {
        boolean selected = (boolean) aValue;
        selections[rowIndex] = selected;

        // ====== Clear protocol ID for unselection ======
        if (!selected) {
          pointIDs[rowIndex] = null;
        }

        // ====== Update protocol ID upon selection ======
        else {
          // 1st method: using spare ID
          if (!seqentialIdEnabled) {
            // Assign protocol ID if point is selected
            pointIDs[rowIndex] = findNextProtocolID(type);
          }

          // 2n method: using sequential ID
          else {
            /*
             * Do nothing here.Later when user changes a point's ID, the rest of
             * points will be allocated with sequential ID which started from
             * the user input.
             */
          }
        }

        // notify table to update
        fireTableRowsUpdated(rowIndex, rowIndex);
      }
    }

    /* Set description */
    else if (columnIndex == COLUMN_DESCRIPTION) {
      pointDescriptions[rowIndex] = (String) aValue;
    }
  }

  private long findNextProtocolID(ScadaPointType type) {
    return findNextProtocolID(0, type);
  }

  private long findNextProtocolID(long startingPoint, ScadaPointType type) {
    long id = startingPoint;

    while (id < type.getMaximumPointID()) {
      if (!idChecker.isExists(id)) {
        boolean containInModel = false;

        // Checks ID not exists in current model
        if (!seqentialIdEnabled) {
          for (int i = 0; i < getRowCount(); i++) {
            if (Long.valueOf(id).equals(getValueAt(i, COLUMN_POINT_ID))) {
              containInModel = true;
              break;
            }
          }
        }

        if (containInModel == false) {
          return id;
        }
      }
      id++;
    }

    return 0;// Fail to allocate a id, just return default 0
  }

  public void setColumnTitle(int column, String title) {
    if (column >= 0 && column < columnNames.length)
      columnNames[column] = title;
  }

}