/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolConstraints;

/**
 * The constraints of SDNP3 configuration.
 */
public final class SDNP3Constraints extends ProtocolConstraints {

  // ====== Constraints Prefixes ======
  public static final String PREFIX_SESSION = "session";
  public static final String PREFIX_SESSION_ADDRESS = "session.address";
  public static final String PREFIX_SESSION_UNSOLICATED_MAX_EVENTS = "session.unsolicated.maxevents";
  public static final String PREFIX_SESSION_UNSOLICATED_MAX_DELAYS = "session.unsolicated.maxdelays";
  public static final String PREFIX_SESSION_EVENT = "session.event";

  public static final String PREFIX_CHANNEL = "channel";
  public static final String PREFIX_CHANNEL_TCP = "channel.tcp";
  public static final String PREFIX_CHANNEL_COMMS = "channel.comms";
  public static final String PREFIX_CHANNEL_CONNECTION = "channel.connection";


  private SDNP3Constraints() {
    super("SDNP3Constraints.properties");
  }


  public static final SDNP3Constraints INSTANCE = new SDNP3Constraints();
}
