/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.manager;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.validate.ProtocolManagerValidator;
import com.lucy.g3.rtu.config.shared.manager.AbstractListConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;

/**
 * Basic implementation of {@linkplain IProtocolManager}.
 *
 * @param <ProtocolT>
 *          the type of protocol
 */
public abstract class AbstractProtocolManager<ProtocolT extends IProtocol<?>>
    extends AbstractListConfigModule<ProtocolT>
    implements IProtocolManager<ProtocolT> {

  private final ArrayList<IProtocolObserver> observers = new ArrayList<IProtocolObserver>();
  private final IContainerValidator validator;

  protected AbstractProtocolManager(IConfig owner) {
    super(owner);
    validator = new ProtocolManagerValidator(this);
  }

  @Override
  public IContainerValidator getValidator() {
    return validator;
  }
  
  @Override
  public boolean allowToAdd(ProtocolT item) throws AddItemException {
    if (hasProtocol(item.getProtocolType())) {
      throw new AddItemException("Protocol already exists: " + item.getProtocolName());
    }

    return true;
  }

  @Override
  protected void postRemoveAction(ProtocolT removedItem) {
    removedItem.delete();
    removedItem.setManager(null);
  }

  @Override
  protected void postAddAction(ProtocolT addedItem) {
    super.postAddAction(addedItem);
    addedItem.setManager(this);
  }

  @Override
  public final boolean hasProtocol(ProtocolType type) {
    Collection<ProtocolT> protocols = getAllItems();
    for (IProtocol<?> proto : protocols) {
      if (proto.getProtocolType() == type) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void registerObserver(IProtocolObserver observer) {
    if (observer != null && !observers.contains(observer)) {
      observers.add(observer);
    }
  }

  @Override
  public void deregisterObserver(IProtocolObserver observer) {
    observers.remove(observer);
  }

  @Override
  public void notifySessionAdded(IProtocolSession session) {
    for (IProtocolObserver ob : observers) {
      ob.notifySessionAdded(session);
    }
  }

  @Override
  public void notifySessionRemoved(IProtocolSession session) {
    for (IProtocolObserver ob : observers) {
      ob.notifySessionRemoved(session);
    }
  }

  @Override
  public ProtocolT getProtocolByType(ProtocolType type) {
    Collection<ProtocolT> protocols = getAllItems();
    for(ProtocolT p : protocols ){
      if(p.getProtocolType() == type)
        return p;
    }
    
    return null;
  }

}
