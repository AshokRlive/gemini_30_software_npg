/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.shared.base.math.BitMask;

/**
 *
 */
public class GroupMask extends BitMask {

  public GroupMask(IEC870PointType type) {
    super(getBitsNumber(type));
    setLabel("Group");
  }

  private static int getBitsNumber(IEC870PointType type) {
    if (type == IEC870PointType.MIT) 
        return 5;
      else
        return 17;
  }
}
