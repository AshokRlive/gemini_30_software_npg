/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolChannel;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolChannel;

/**
 * The protocol channel of ModBus Master.
 */
public class MMBChannel extends AbstractProtocolChannel<MMBSession> implements IMasterProtocolChannel<MMBSession> {

  private final Logger log = Logger.getLogger(MMBSession.class);

  private final MMBChannelConf channelConf;

  public MMBChannel(MMB mmb, String channelName) {
    super(mmb);
    channelConf = new MMBChannelConf(this, channelName);
    getValidator().addAdditionalValidation(channelConf.getSerialConf());
  }
  
  private MMBChannel() {
    super(ProtocolType.MMB);
    channelConf = new MMBChannelConf(this, "");
  }

  
  public static MMBChannel createDummy(){
    return new MMBChannel();
  }

  @Override
  public MMB getProtocol() {
    return (MMB) super.getProtocol();
  }

  @Override
  public MMBChannelConf getChannelConfig() {
    return channelConf;
  }

  @Override
  public MMBSession addSession(String sessionName) {
    if (allowMultiSession() || getSessionNum() == 0) {
      MMBSession newSession = new MMBSession(this, sessionName);
      if (add(newSession)) {
        return newSession;
      } else {
        log.error("Failed to add new session");
        return null;
      }
    } else {
      log.error("Not allowed to add more session");
      return null;
    }
  }

  /**
   * Override to notify device manager a session has been added.
   */
  @Override
  protected void postAddAction(MMBSession addedItem) {
    super.postAddAction(addedItem);

    MMB protocol = getProtocol();
    if(protocol != null) {
      MasterProtocolManager manager = protocol.getManager();
      if (manager != null) {
        manager.notifySessionAdded(addedItem);
      }
    }
  }

  /**
   * Override to notify device manager a session has been removed.
   */
  @Override
  protected void postRemoveAction(MMBSession removedItem) {
    super.postRemoveAction(removedItem);

    MasterProtocolManager manager = getProtocol().getManager();
    if (manager != null) {
      manager.notifySessionRemoved(removedItem);
    }
  }


}
