/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.wizards.obsolete;

import java.awt.Component;
import java.util.Collection;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
class AddScadaProtocolInitial extends WizardPage {
  private Collection<IScadaProtocol<?>> exitingProtcols; 
  private ButtonGroup group;
  
  public AddScadaProtocolInitial(ScadaProtocolManager manager) {
    super("Select a protocol");
    exitingProtcols = manager.getAllItems();
    initComponents();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    if(component != null) {
      String selectedProtocoLName = component.getName();
      for (IScadaProtocol p : exitingProtcols) {
        if(p.getProtocolType().getName().equals(selectedProtocoLName))
          return "The protocol already exists: "+selectedProtocoLName;
      }
    } 
    
    
    if(group.getSelection() == null) {
      return "Please select a protocol type!";
    }
    
    return null;
  }

  private void initComponents() {
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout(
        "default, 50dlu, default",
        ""), this);
    
    ProtocolType[] types = ProtocolType.getAllSlaveProtocolTypes();
    group = new ButtonGroup();
    int i = 0;
    JRadioButton checkbox;
    for (ProtocolType type : types) {
      checkbox = new JRadioButton(type.getName());
      builder.append( i == 0 ? "Protocol:" : "", checkbox);
      builder.nextLine();
      group.add(checkbox);
      //checkbox.setSelected(i == 0);
      checkbox.setName(type.getName());
      i ++;
    }
  }

  
  public static ProtocolType getSelectedProtocol(Map wizardData) {
    ProtocolType[] types = ProtocolType.getAllSlaveProtocolTypes();
    for (ProtocolType t : types) {
      if(wizardData.get(t.getName()) == Boolean.TRUE) {
        return t;
      }
    }
    
    return null;
  }
}
