/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.Failover;

class FailoverTableModel extends AbstractTableAdapter<Failover> {

  private static final String[] COLUMN_NAMES = { "IP Address", "Port", "Master Address" };


  public FailoverTableModel(ListModel<Failover> failoverListModel) {
    super(failoverListModel, COLUMN_NAMES);
  }

  @Override
  public int getColumnCount() {
    return COLUMN_NAMES.length;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    Failover failover = getRow(rowIndex);

    switch (columnIndex) {
    case 0:
      return failover.getIpAddress();
    case 1:
      return failover.getPort();
    case 2:
      return failover.getMasterAddress();
    default:
      throw new IllegalStateException("column not found");
    }
  }

  @Override
  public boolean isCellEditable(int row, int col) {
    return true;
  }

  @Override
  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    if (value == null) {
      return;
    }

    Failover failover = getRow(rowIndex);

    switch (columnIndex) {
    case 0:
      failover.setIpAddress(value.toString());
      break;
    case 1:
      failover.setPort(Integer.parseInt(value.toString()));
      break;
    case 2:
      failover.setMasterAddress(Integer.parseInt(value.toString()));
      break;
    default:
      throw new IllegalStateException("column not found");
    }
  }

}
