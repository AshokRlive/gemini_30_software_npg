/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;



import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_KEYWRAP;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_USER_ROLE;


/**
 *
 */
public class SDNP3User extends Model {
  public static final String PROPERTY_UDPATE_KEY = "updateKey";
  public static final String PROPERTY_KEY_WRAP_ALGORITHM = "keyWrapAlgorithm";
  public static final String PROPERTY_USER_NAME = "userName";
  public static final String PROPERTY_USER_NUMBER = "userNumber";
  public static final String PROPERTY_USER_ROLE = "userRole";
  
  public static final int MAX_USER_NUMBER = Short.MAX_VALUE;
  public static final int MIN_USER_NUMBER = 1;
  
  public static final char UDPATE_KEY_DELIMITER = ' ';
  public static final String UDPATE_KEY_DELIMITER_STR = String.valueOf(UDPATE_KEY_DELIMITER);
  
  private String updateKey = "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00";
  private DNPAUTH_KEYWRAP keyWrapAlgorithm = DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES128;
  private int userNumber = 0;
  private String userName = "New User";
  private DNPAUTH_USER_ROLE userRole = DNPAUTH_USER_ROLE.DNPAUTH_USER_ROLE_SINGLEUSER;
  
  public String getUpdateKey() {
    return updateKey;
  }
  
  public void setUpdateKey(String updateKey) {
    Object oldValue = this.updateKey;
    this.updateKey = updateKey;
    firePropertyChange(PROPERTY_UDPATE_KEY, oldValue, updateKey);
  }
  
  
  public DNPAUTH_KEYWRAP getKeyWrapAlgorithm() {
    return keyWrapAlgorithm;
  }
  
  public void setKeyWrapAlgorithm(DNPAUTH_KEYWRAP keyWrapAlgorithm) {
    if(keyWrapAlgorithm != null) {
      Object oldValue = this.keyWrapAlgorithm;
      this.keyWrapAlgorithm = keyWrapAlgorithm;
      firePropertyChange(PROPERTY_KEY_WRAP_ALGORITHM, oldValue, keyWrapAlgorithm);
    }
  }

  public int getUserNumber() {
    return userNumber;
  }
  
  public void setUserNumber(int userNumber) {
    Object oldValue = this.userNumber;
    this.userNumber = userNumber;
    firePropertyChange(PROPERTY_USER_NUMBER, oldValue, userNumber);
  }
  
  public String getUserName() {
    return userName;
  }
  
  public void setUserName(String userName) {
    Object oldValue = this.userName;
    this.userName = userName;
    firePropertyChange(PROPERTY_USER_NAME, oldValue, userName);
  }
  
  public DNPAUTH_USER_ROLE getUserRole() {
    return userRole;
  }
  
  public void setUserRole(DNPAUTH_USER_ROLE userRole) {
    if(userRole != null) {
      Object oldValue = this.userRole;
      this.userRole = userRole;
      firePropertyChange(PROPERTY_USER_ROLE, oldValue, userRole);
    }
  }
  
}

