/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui;

import java.util.ArrayList;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceStub;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.IScadaPointFactory;
import com.lucy.g3.rtu.config.protocol.slave.shared.stub.ScadaIoMapStub;
import com.lucy.g3.rtu.config.protocol.slave.shared.stub.ScadaPointTypeStub;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointBulkAddDialog;

/**
 *
 */
public class ScadaPointBulkAddDialogTest implements IScadaPointFactory<ScadaPoint> {

  public static void main(String[] args) {
    ArrayList<ScadaPointSource> sources = new ArrayList<>();
    sources.add(new ScadaPointSourceStub());
    sources.add(new ScadaPointSourceStub());

    new ScadaPointBulkAddDialog<ScadaPoint>(
        null,
        sources,
        ScadaPointTypeStub.create(),
        new ScadaIoMapStub<ScadaPoint>(),
        new ScadaPointBulkAddDialogTest())
        .setVisible(true);
  }

  @Override
  public ScadaPoint createPoint(ScadaPointSource source, IScadaIoMap<ScadaPoint> iomap, ScadaPointType type,
      long pointID, String description) {
    return null;
  }

}
