/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.MMBIoBitMask;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterDataType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterType;
import com.lucy.g3.rtu.config.shared.base.wizard.WizardPageExt;   


abstract class AbstractPage extends WizardPageExt {
  protected static final String IKEY_REGISTER_TYPE   = "registerType";  
  protected static final String IKEY_CHANNEL_NAME    = "channelName";  
  protected static final String IKEY_NUMBER_OF_CHNL  = "numberOfChannel";  
  protected static final String IKEY_START_REG       = "startingRegister";  
  protected static final String IKEY_DATA_TYPE       = "dataType";  
  protected static final String IKEY_POLLING_RATE    = "pollingRate";
  protected static final String IKEY_BIT_MASK        = "bitMask";  
  
  private JComboBox<?> combRegType;
  private JComboBox<?> combDataType;
  private JComboBox<?> combBitMask;
  private JTextField tfChnlName;
  private JTextField tfAddr;
  private JTextField tfNum;
  private JTextField tfPollingRate;
  
  protected final ChannelType type;
  
  public AbstractPage(ChannelType type){
    super("Parameters");
    this.type = type;
    setLongDescription(type.getDescription() +" Parameters");
  }
  
  protected JComboBox<RegisterType> createRegTypeCombobox(ChannelType type) {
    final JComboBox<RegisterType> comp = new JComboBox<>(MMBIoChannel.getAvailabelRegisterTypes(type));
    comp.setName(IKEY_REGISTER_TYPE);
    comp.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        // Update name
        if(tfChnlName != null) {
          RegisterType reg = (RegisterType) comp.getSelectedItem();
          tfChnlName.setText(reg.getDescription());
        }
      }
    });
    
    combRegType = comp;
    
    // Init name
    if(tfChnlName != null) {
      RegisterType reg = (RegisterType) comp.getSelectedItem();
      tfChnlName.setText(reg.getDescription());
    }
    
    return comp;
  }
  
  protected JTextField createPollingRateField() {
    JTextField comp = new JTextField();
    comp.setName(IKEY_POLLING_RATE);
    comp.setText("0");
    tfPollingRate = comp;
    return comp;
  }
  
  protected JTextField createNumField() {
    JTextField comp = new JTextField();
    comp.setName(IKEY_NUMBER_OF_CHNL);
    comp.setText("1");
    tfNum = comp;
    return comp;
  }
  
  protected JComboBox<RegisterDataType> createDataTypeCombobox(ChannelType type) {
    JComboBox<RegisterDataType> comp = new JComboBox<>(MMBIoChannel.getAvailableDataTypes(type));
    comp.setName(IKEY_DATA_TYPE);
    combDataType = comp;
    return comp;
  }
  
  protected JComboBox<MMBIoBitMask> createBitMaskCombox(ChannelType type) {
    JComboBox<MMBIoBitMask> comp = new JComboBox<>(MMBIoBitMask.values());
    comp.setName(IKEY_BIT_MASK);
    combBitMask = comp;
    return comp;
  }
  
  protected JTextField createAddressField() {
    JTextField comp = new JTextField();
    comp.setName(IKEY_START_REG);
    comp.setText("0");
    tfAddr = comp;
    return comp; 
  }
  
  protected JTextField createChannelNameField() {
    JTextField comp = new JTextField();
    comp.setName(IKEY_CHANNEL_NAME);
    
    // Init name
    if(combRegType != null)
      comp.setText(((RegisterType) combRegType.getSelectedItem()).getDescription());
    
    tfChnlName = comp;
    return comp;
  }

  @Override
  protected String validateContents(Component comp, Object event) {
    error = null;
    
    if(comp != null) {
      if(comp == tfPollingRate) {
        checkUnsignedLong(tfPollingRate, "Polling Rate");
        
      } else if(comp == tfAddr) {
        checkUnsignedLong(tfAddr, "Starting Register");
        
      } else if(tfNum == comp){
        checkUnsignedLong(tfNum, "Number Of Channels");
        
      } else if(tfChnlName == comp) {
        checkNotBlank(tfChnlName, "Base Channel Name");
        
      } else if(combDataType == comp) {
        checkNotNull(combDataType, "Data Type");
        
      } else if(combRegType == comp) {
        checkNotNull(combRegType, "RegisterType");
        
      } else if(combBitMask == comp) {
        checkNotNull(combBitMask, "Bit Number");
      }
    }
      
    return error;
  }
}

