/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import java.util.ArrayList;

import com.jgoodies.common.collect.ObservableList;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3MappingResources;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point.SDNP3PointDialogs;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.AbstractIoMapPageModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;

/**
 * Presentation model for DNP3 points manager, which adapts a list of DNP3
 * points to table model and provides related operation actions.
 */
public final class SDNP3IoMapPageModel extends AbstractIoMapPageModel<SDNP3Point> {


  private final ScadaPointSourceRepository sourceRepo;
  
  /**
   * Construct a IO map model.
   * @param pointsList
   *          the source list to be converted into model
   * @param type
   *          is used to specify E.
   */
  SDNP3IoMapPageModel(IScadaIoMap<SDNP3Point> iomap, SDNP3PointType type, ScadaPointSourceRepository sourceRepo) {
    super(iomap, type);
    this.sourceRepo = sourceRepo;
  }

  @Override
  protected SDNP3Point createPoint(IScadaIoMap<SDNP3Point> iomap, ScadaPointType type, long id) {
    return SDNP3PointFactory.create(iomap, (SDNP3PointType) type, id);
  }

  @Override
  protected ArrayList<SDNP3Point> showBulkAdd(ArrayList<ScadaPointSource> sources, ScadaPointType type,
      IScadaIoMap<SDNP3Point> iomap) {
    return SDNP3PointDialogs.showBulkAdd(parent, sources, (SDNP3PointType) type, iomap);
  }

  @Override
  protected boolean showBulkEditor(ScadaPointType type, IScadaIoMap<SDNP3Point> iomap,
      ObservableList<SDNP3Point> selection) {
    return SDNP3PointDialogs.showBulkEditor(parent, (SDNP3PointType) type, iomap, selection);
  }

  @Override
  protected boolean showEditor(SDNP3Point point) {
    return SDNP3PointDialogs.showEditor(parent, point, sourceRepo);
  }

  @Override 
  protected void showEditor(ScadaPointType type, IScadaPointEditorDialogInvoker<SDNP3Point> invoker) {
    SDNP3PointDialogs.showEditor(parent, (SDNP3PointType)type, invoker, sourceRepo);
  }

  @Override
  protected ArrayList<ScadaPointSource> getMappingRes(ScadaPointType type, IScadaIoMap<SDNP3Point> iomap,
      boolean spareOnly) {
    return SDNP3MappingResources.getMappingRes(sourceRepo, (SDNP3PointType) type, iomap, spareOnly);
  }


}
