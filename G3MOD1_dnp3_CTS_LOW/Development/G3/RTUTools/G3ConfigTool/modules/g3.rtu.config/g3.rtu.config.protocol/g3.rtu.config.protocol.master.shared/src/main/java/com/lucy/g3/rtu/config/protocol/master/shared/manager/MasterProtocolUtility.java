/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.manager;

import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;


/**
 *
 */
public class MasterProtocolUtility implements IConfigUtility {
  private MasterProtocolUtility() {}

  public static IMasterProtocol<?> getModbusMaster(IConfig config) {
    MasterProtocolManager manager = config == null ? null : 
      (MasterProtocolManager)config.getConfigModule(MasterProtocolManager.CONFIG_MODULE_ID);
    
    return manager == null ? null : manager.getModbusMaster();
  }
  
}

