/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.apache.log4j.Logger;

import com.g3schema.ns_sdnp3.CommsPowerCycleT;
import com.g3schema.ns_sdnp3.IncomingConnectionsT;
import com.g3schema.ns_sdnp3.IpConnectivityCheckT;
import com.g3schema.ns_sdnp3.OutgoingConnectionsT;
import com.g3schema.ns_sdnp3.PowerCycleInhibitPointT;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * The Class CommsPowerCycle.
 */
public class CommsPowerCycle extends BeanWithEnable {

  public static final String PROPERTY_COMMS_POWER_SUPPLY = "commsPowerSupply";

  public static final String PROPERTY_COMMS_POWER_CYCLE_DURATION = "commsPowerCycleDuration";
  
  public static final String PROPERTY_COMMS_POWER_CYCLE_INHIBIT = "commsPowerCycleInhibit";

  public static final String PROPERTY_COMMS_INITTIME = "commsInitTime";

  private Logger log = Logger.getLogger(CommsPowerCycle.class);

  private final SDNP3Constraints cons = SDNP3Constraints.INSTANCE;

  private IChannel commsPowerSupply;
  private VirtualPoint commsPowerCycleInhibit;
  private long commsPowerCycleDuration; // seconds
  private long commsInitTime; // seconds

  public final IpConnectivityCheck connectivityCheck = new IpConnectivityCheck();
  public final OutGoingCheck outgoingCheck = new OutGoingCheck();
  public final IncomingCheck incomingCheck = new IncomingCheck();

  private final PropertyChangeListener inhibitRemovePCL = new PropertyChangeListener() {
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if(evt.getSource() == commsPowerCycleInhibit 
          && VirtualPoint.PROPERTY_DELETED.equals(evt.getPropertyName())){
        setCommsPowerCycleInhibit(null);
      }
        
    }
  };

  public CommsPowerCycle() {
    connectivityCheck.setEnabled(true);
    commsPowerCycleDuration = cons.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION, "commsPowerCycleDuration");
    cons.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION, "commsInitTime");
  }

  public IChannel getCommsPowerSupply() {
    return commsPowerSupply;
  }

  public void setCommsPowerSupply(IChannel commsPowerSupply) {
    Object oldValue = this.commsPowerSupply;
    this.commsPowerSupply = commsPowerSupply;
    firePropertyChange(PROPERTY_COMMS_POWER_SUPPLY, oldValue, commsPowerSupply);
  }

  public long getCommsPowerCycleDuration() {
    return commsPowerCycleDuration;
  }

  public void setCommsPowerCycleDuration(long commsPowerCycleDuration) {
    Object oldValue = this.commsPowerCycleDuration;
    this.commsPowerCycleDuration = commsPowerCycleDuration;
    firePropertyChange(PROPERTY_COMMS_POWER_CYCLE_DURATION, oldValue,
        commsPowerCycleDuration);
  }

  
  public VirtualPoint getCommsPowerCycleInhibit() {
    return commsPowerCycleInhibit;
  }

  public void setCommsPowerCycleInhibit(VirtualPoint commsPowerCycleInhibit) {
    VirtualPoint oldValue = this.commsPowerCycleInhibit;
    
    if(oldValue != null){
      oldValue.removePropertyChangeListener(VirtualPoint.PROPERTY_DELETED, inhibitRemovePCL);
    }
    
    this.commsPowerCycleInhibit = commsPowerCycleInhibit;
    
    if(commsPowerCycleInhibit  != null){
      commsPowerCycleInhibit .addPropertyChangeListener(VirtualPoint.PROPERTY_DELETED, inhibitRemovePCL);
    }
    
    firePropertyChange(PROPERTY_COMMS_POWER_CYCLE_INHIBIT, oldValue, commsPowerCycleInhibit);
  }

  public long getCommsInitTime() {
    return commsInitTime;
  }

  public void setCommsInitTime(long commsInitTime) {
    Object oldValue = this.commsInitTime;
    this.commsInitTime = commsInitTime;
    firePropertyChange(PROPERTY_COMMS_INITTIME, oldValue, commsInitTime);
  }

  void writeToXML(CommsPowerCycleT xml, VirtualPointWriteSupport support) {
    xml.initTimeSecs.setValue(getCommsInitTime());
    xml.durationSecs.setValue(getCommsPowerCycleDuration());

    support.writeChannelRef("SDNP3 Comms Power Cycle", xml.commsPowerSupply.append(), getCommsPowerSupply());

    if (connectivityCheck.isEnabled()) {
      IpConnectivityCheckT xml_connectivityCheck = xml.ipConnectivityCheck.append();
      xml_connectivityCheck.periodMins.setValue(connectivityCheck.getPeriod());
      xml_connectivityCheck.port.setValue(connectivityCheck.getPort());
      xml_connectivityCheck.retries.setValue(connectivityCheck.getConnectivityRetries());
    }

    if (outgoingCheck.isEnabled()) {
      OutgoingConnectionsT xml_outgoingConnections = xml.outgoingConnections.append();
      xml_outgoingConnections.retries.setValue(outgoingCheck.getOutgoingRetries());

    }

    if (incomingCheck.isEnabled()) {
      IncomingConnectionsT xml_incomingConnections = xml.incomingConnections.append();
      xml_incomingConnections.notReceivedHours.setValue(incomingCheck.getNotReceivePeriod());
    }

    if(commsPowerCycleInhibit != null) {
      PowerCycleInhibitPointT xml_inhibit = xml.powerCycleInhibitPoint.append();
      xml_inhibit.group.setValue(commsPowerCycleInhibit.getGroup());
      xml_inhibit.id.setValue(commsPowerCycleInhibit.getId());
    }
  }

  void readFromXML(CommsPowerCycleT xml, VirtualPointReadSupport channelResources) {
    setCommsPowerCycleDuration(xml.durationSecs.getValue());
    setCommsInitTime(xml.initTimeSecs.getValue());

    IChannel powerchannel = channelResources.getChannelByRef(xml.commsPowerSupply.first(), ChannelType.PSM_CH_PSUPPLY);
    if (powerchannel != null) {
      setCommsPowerSupply(powerchannel);
    } else {
      log.error("Power supply channel not found for Comms Power Cycle");
    }

    if (xml.ipConnectivityCheck.exists()) {
      connectivityCheck.setEnabled(true);
      connectivityCheck.readFromXML(xml.ipConnectivityCheck.first());
    } else {
      connectivityCheck.setEnabled(false);
    }

    if (xml.outgoingConnections.exists()) {
      outgoingCheck.setEnabled(true);
      outgoingCheck.readFromXML(xml.outgoingConnections.first());
    } else {
      outgoingCheck.setEnabled(false);
    }

    if (xml.incomingConnections.exists()) {
      incomingCheck.setEnabled(true);
      incomingCheck.readFromXML(xml.incomingConnections.first());
    } else {
      incomingCheck.setEnabled(false);
    }

    if(xml.powerCycleInhibitPoint.exists()) {
      PowerCycleInhibitPointT xml_inhibit = xml.powerCycleInhibitPoint.first();
      
      long group = xml_inhibit.group.getValue();
      long id = xml_inhibit.id.getValue();
      VirtualPoint vp = channelResources.getVirtualPoint((int)group,(int)id);
      setCommsPowerCycleInhibit(vp);
    }
  }


  public static final class IpConnectivityCheck extends BeanWithEnable {

    public static final String PROPERTY_PERIOD = "period";
    public static final String PROPERTY_PORT = "port";
    public static final String PROPERTY_CONNECTIVITY_RETRIES = "connectivityRetries";

    private long period = SDNP3Constraints.INSTANCE.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION, "period"); // minutes
    private long port = SDNP3Constraints.INSTANCE.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION, "port"); // tcp
    // port
    private long connectivityRetries = SDNP3Constraints.INSTANCE.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION,
        "connectivityRetries");


    public long getPeriod() {
      return period;
    }

    public void setPeriod(long period) {
      Object oldValue = getPeriod();
      this.period = period;
      firePropertyChange(PROPERTY_PERIOD, oldValue, period);
    }

    public long getPort() {
      return port;
    }

    public void setPort(long port) {
      Object oldValue = getPort();
      this.port = port;
      firePropertyChange(PROPERTY_PORT, oldValue, port);
    }

    public long getConnectivityRetries() {
      return connectivityRetries;
    }

    public void setConnectivityRetries(long connectivityRetries) {
      Object oldValue = this.connectivityRetries;
      this.connectivityRetries = connectivityRetries;
      firePropertyChange(PROPERTY_CONNECTIVITY_RETRIES, oldValue, connectivityRetries);
    }

    public void readFromXML(IpConnectivityCheckT xml) {
      setPeriod(xml.periodMins.getValue());
      setPort(xml.port.getValue());
      setConnectivityRetries(xml.retries.getValue());
    }
  }

  public static final class OutGoingCheck extends BeanWithEnable {

    public static final String PROPERTY_OUTGOING_RETRIES = "outgoingRetries";

    private long outgoingRetries = SDNP3Constraints.INSTANCE.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION,
        "outgoingRetries");


    public long getOutgoingRetries() {
      return outgoingRetries;
    }

    public void setOutgoingRetries(long outgoingRetries) {
      Object oldValue = this.outgoingRetries;
      this.outgoingRetries = outgoingRetries;
      firePropertyChange(PROPERTY_OUTGOING_RETRIES, oldValue, outgoingRetries);
    }

    public void readFromXML(OutgoingConnectionsT xml) {
      setOutgoingRetries(xml.retries.getValue());
    }
  }

  public static final class IncomingCheck extends BeanWithEnable {

    public static final String PROPERTY_NOT_RECEIVE_PERIOD = "notReceivePeriod";

    public static final Long[] PERIOD_MINS_OPTIONS = {
        1L,
        6L,
        12L,
        24L,
        48L,
        72L,
        168L,
    }; // Hours

    private long notReceivePeriod = 72L; // mins


    public long getNotReceivePeriod() {
      return notReceivePeriod;
    }

    public void setNotReceivePeriod(long notReceivePeriod) {
      Object oldValue = this.notReceivePeriod;
      this.notReceivePeriod = notReceivePeriod;
      firePropertyChange("notReceivePeriod", oldValue, notReceivePeriod);
    }

    public void readFromXML(IncomingConnectionsT xml) {
      setNotReceivePeriod(xml.notReceivedHours.getValue());
    }
  }
}
