/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.beans.PropertyVetoException;

import com.g3schema.ns_sdnp3.AuthConfigT;
import com.g3schema.ns_sdnp3.AuthConfigV2T;
import com.g3schema.ns_sdnp3.AuthConfigV5T;
import com.g3schema.ns_sdnp3.AuthenticationT;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.EventConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_MAC;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_EVENT_MODE;


/**
 *
 */
public class SDNPAuthConfig extends Model{
  // @formatter:off
  public static final String PROPERTY_AUTHENTICATION_ENABLED    = "authenticationEnabled";
  public static final String PROPERTY_ASSOCID                   = "assocId";
  public static final String PROPERTY_MACALGORITHM              = "MACAlgorithm";
  public static final String PROPERTY_REPLYTIMEOUTMS            = "replyTimeoutMs";
  public static final String PROPERTY_KEYCHANGEINTERVALMS       = "keyChangeIntervalMs";
  public static final String PROPERTY_MAXAPPLTIMEOUTCOUNT       = "maxApplTimeoutCount";
  public static final String PROPERTY_MAXKEYCHANGECOUNT         = "maxKeyChangeCount";
  public static final String PROPERTY_AGGRESSIVEMODESUPPORT     = "aggressiveModeSupport";
  public static final String PROPERTY_DISALLOWSHA1              = "disallowSHA1";
  public static final String PROPERTY_EXTRADIAGS                = "extraDiags";
  public static final String PROPERTY_OPERATEINV2MODE           = "operateInV2Mode";
  public static final String PROPERTY_MAXERRORCOUNT             = "maxErrorCount";
  public static final String PROPERTY_RANDOMCHALLENGEDATALENGTH = "randomChallengeDataLength"; 
  public static final String PROPERTY_MAXAUTHENTICATIONFAILURES = "maxAuthenticationFailures";
  public static final String PROPERTY_MAXSESSIONKEYSTATUSCOUNT  = "maxSessionKeyStatusCount";
  public static final String PROPERTY_MAXREPLYTIMEOUTS          = "maxReplyTimeouts";
  public static final String PROPERTY_MAXAUTHENTICATIONREKEYS   = "maxAuthenticationRekeys";
  public static final String PROPERTY_MAXERRORMESSAGESSENT      = "maxErrorMessagesSent";
  // @formatter:on
  
  /* Association Id. Sent in error message to master. When error message is sent 
   * to a different master, this identifies which association error occurred on.
   */
  private int assocId = 0;

  /* MAC algorithm to be used in challenges  
   *  1 DNPAUTH_HMAC_SHA1_4OCTET   (SHA1 truncated to    4 octets   Only for SA V2)
   *  2 DNPAUTH_MAC_SHA1_10OCTET   (SHA1 truncated to   10 octets)
   *  3 DNPAUTH_MAC_SHA256_8OCTET  (SHA256 truncated to  8 octets)
   *  4 DNPAUTH_MAC_SHA256_16OCTET (SHA256 truncated to 16 octets)
   *  5 DNPAUTH_MAC_SHA1_8OCTET    (SHA1 truncated to    8 octets)
   *  6 DNPAUTH_MAC_AESGMAC_12OCTET
   */
  private DNPAUTH_MAC   MACAlgorithm = DNPAUTH_MAC.DNPAUTH_MAC_SHA1_10OCTET;

  /* Number of consecutive application layer timeouts before declaring a 
   * Communications Failure Event. You may choose to set this to the same 
   * value as maxErrorCount in SAv2 or Max Reply Timeout statistic in SAv5, 
   * but this is separately configured and counted.
   */
  private short  maxApplTimeoutCount = 2;

  /* How long to wait for any authentication reply 
   * Default shall be 2 seconds 
   */
  private long  replyTimeoutMs = 2000;

  /* Expected session key interval and count. When this amount of time elapses
   * or this quantity of Authentication messages are sent or received, the session 
   * keys for this user will be invalidated.
   * Interval and count should be 2 times the master key change interval and count.
   * For systems that communicate infrequently, keyChangeInterval may be set to  
   * zero, using only the maxKeyChangeCount to determine when keys should be 
   * considered old and should be invalidated.
   *  default 30 minutes and 4000 messages.
   */
  private long      keyChangeIntervalMs = 30000;
  private int       maxKeyChangeCount = 2000;
  
  /* Aggressive mode is enabled, allows aggressive mode requests from master */
  private boolean        aggressiveModeSupport = false;

  /* Version 5 requires ability to disallow SHA1 */
  private boolean        disallowSHA1 = false;

  /* Extra Diagnostics support. This enables additional diagnostic messages related
   * to the state machine in Table 6 of the Secure Authentication spec
   */
  private boolean        extraDiags = true;
  
  /* Secure Authentication Version 2 */ 
  private boolean        operateInV2Mode = true;

  /* Number of errors messages to be sent before disabling error message 
   * transmission.
   * default shall be 2 
   * range 0-255
   */
  private short       maxErrorCount = 2; 

  /* Length of random challenge data to send in g120v1 challenge and g120v5 key status */
  private int  randomChallengeDataLength = 4;

  /* SA V4 4.1.4.6 says that if this number of Session Key Status Requests within 
   * the Expected Session Key Change Interval, the outstation shall alert a human.
   * If a different DNP association is in use, the outstation shall send an Error 
   * (g120v7) event object on that association with the code <12> Max Session Key 
   * Status Requests Exceeded. This value shall be configurable up to a maximum of 
   * 255 or down to 2. The default value shall be 5.
   */
  private int  maxSessionKeyStatusCount = 5;

  /* These 4 maximum values are in addition to the thresholds for the individual statistics 
   * The fifth one maxKeysDueToRestarts is on master.
   * This is made clear in the Secure Authentication Test Procedures. 
   * The outstation will take special actions when a statistic exceeds these Max Values. 
   */
  private int  maxAuthenticationFailures = 5;
  private int  maxReplyTimeouts = 3;
  private int  maxAuthenticationRekeys = 3;
  private int  maxErrorMessagesSent = 2;
  
  private boolean authenticationEnabled = false;
  
  private final EventConfig authSecStatEvtConfig = new EventConfig();
  
  private final SDNPAuthUsersConfig usersConfig = new SDNPAuthUsersConfig();

  public boolean isAuthenticationEnabled() {
    return authenticationEnabled;
  }

  public void setAuthenticationEnabled(boolean authenticationEnabled) {
    Object oldValue = this.authenticationEnabled;
    this.authenticationEnabled = authenticationEnabled;
    firePropertyChange(PROPERTY_AUTHENTICATION_ENABLED, oldValue, authenticationEnabled);
  }
  

  public EventConfig getAuthSecStatEvtConfig() {
    return authSecStatEvtConfig;
  }

  
  public SDNPAuthUsersConfig getUsersConfig() {
    return usersConfig;
  }

  public int getAssocId() {
    return assocId;
  }
  
  public void setAssocId(int assocId) {
    Object oldValue = this.assocId;
    this.assocId = assocId;
    firePropertyChange(PROPERTY_ASSOCID, oldValue, assocId);
  }
  
  public DNPAUTH_MAC getMACAlgorithm() {
    return MACAlgorithm;
  }
  
  public void setMACAlgorithm(DNPAUTH_MAC mACAlgorithm) {
    if(mACAlgorithm != null) {
      Object oldValue = MACAlgorithm;
      MACAlgorithm = mACAlgorithm;
      firePropertyChange(PROPERTY_MACALGORITHM, oldValue, mACAlgorithm);
    }
  }
  
  public short getMaxApplTimeoutCount() {
    return maxApplTimeoutCount;
  }
  
  public void setMaxApplTimeoutCount(short maxApplTimeoutCount) {
    Object oldValue = this.maxApplTimeoutCount;
    this.maxApplTimeoutCount = maxApplTimeoutCount;
    firePropertyChange(PROPERTY_MAXAPPLTIMEOUTCOUNT, oldValue, maxApplTimeoutCount);
  }
  
  public long getReplyTimeoutMs() {
    return replyTimeoutMs;
  }
  
  public void setReplyTimeoutMs(long replyTimeoutMs) {
    Object oldValue = this.replyTimeoutMs;
    this.replyTimeoutMs = replyTimeoutMs;
    firePropertyChange(PROPERTY_REPLYTIMEOUTMS, oldValue, replyTimeoutMs);
  }
  
  public long getKeyChangeIntervalMs() {
    return keyChangeIntervalMs;
  }
  
  public void setKeyChangeIntervalMs(long keyChangeIntervalMs) {
    Object oldValue = this.keyChangeIntervalMs;
    this.keyChangeIntervalMs = keyChangeIntervalMs;
    firePropertyChange(PROPERTY_KEYCHANGEINTERVALMS, oldValue, keyChangeIntervalMs);
  }
  
  public int getMaxKeyChangeCount() {
    return maxKeyChangeCount;
  }
  
  public void setMaxKeyChangeCount(int maxKeyChangeCount) {
    Object oldValue = this.maxKeyChangeCount;
    this.maxKeyChangeCount = maxKeyChangeCount;
    firePropertyChange(PROPERTY_MAXKEYCHANGECOUNT, oldValue, maxKeyChangeCount);
  }
  
  public boolean isAggressiveModeSupport() {
    return aggressiveModeSupport;
  }
  
  public void setAggressiveModeSupport(boolean aggressiveModeSupport) {
    Object oldValue = this.aggressiveModeSupport;
    this.aggressiveModeSupport = aggressiveModeSupport;
    firePropertyChange(PROPERTY_AGGRESSIVEMODESUPPORT, oldValue, aggressiveModeSupport);
  }
  
  public boolean isDisallowSHA1() {
    return disallowSHA1;
  }
  
  public void setDisallowSHA1(boolean disallowSHA1) {
    Object oldValue = this.disallowSHA1;
    this.disallowSHA1 = disallowSHA1;
    firePropertyChange(PROPERTY_DISALLOWSHA1, oldValue, disallowSHA1);
  }
  
  public boolean isExtraDiags() {
    return extraDiags;
  }
  
  public void setExtraDiags(boolean extraDiags) {
    Object oldValue = this.extraDiags;
    this.extraDiags = extraDiags;
    firePropertyChange(PROPERTY_EXTRADIAGS, oldValue, extraDiags);
  }
  
  public boolean isOperateInV2Mode() {
    return operateInV2Mode;
  }
  
  public void setOperateInV2Mode(boolean operateInV2Mode) {
    Object oldValue = this.operateInV2Mode;
    this.operateInV2Mode = operateInV2Mode;
    firePropertyChange(PROPERTY_OPERATEINV2MODE, oldValue, operateInV2Mode);
  }
  
  public short getMaxErrorCount() {
    return maxErrorCount;
  }
  
  public void setMaxErrorCount(short maxErrorCount) {
    Object oldValue = this.maxErrorCount;
    this.maxErrorCount = maxErrorCount;
    firePropertyChange(PROPERTY_MAXERRORCOUNT, oldValue, maxErrorCount);
  }
  
  public int getRandomChallengeDataLength() {
    return randomChallengeDataLength;
  }
  
  public void setRandomChallengeDataLength(int randomChallengeDataLength) {
    Object oldValue = this.randomChallengeDataLength;
    this.randomChallengeDataLength = randomChallengeDataLength;
    firePropertyChange(PROPERTY_RANDOMCHALLENGEDATALENGTH, oldValue, randomChallengeDataLength);
  }
  
  public int getMaxSessionKeyStatusCount() {
    return maxSessionKeyStatusCount;
  }
  
  public void setMaxSessionKeyStatusCount(int maxSessionKeyStatusCount) {
    Object oldValue = this.maxSessionKeyStatusCount;
    this.maxSessionKeyStatusCount = maxSessionKeyStatusCount;
    firePropertyChange(PROPERTY_MAXSESSIONKEYSTATUSCOUNT, oldValue, maxSessionKeyStatusCount);
  }
  
  public int getMaxAuthenticationFailures() {
    return maxAuthenticationFailures;
  }
  
  public void setMaxAuthenticationFailures(int maxAuthenticationFailures) {
    Object oldValue = this.maxAuthenticationFailures;
    this.maxAuthenticationFailures = maxAuthenticationFailures;
    firePropertyChange(PROPERTY_MAXAUTHENTICATIONFAILURES, oldValue, maxAuthenticationFailures);
  }
  
  public int getMaxReplyTimeouts() {
    return maxReplyTimeouts;
  }
  
  public void setMaxReplyTimeouts(int maxReplyTimeouts) {
    Object oldValue = this.maxReplyTimeouts;
    this.maxReplyTimeouts = maxReplyTimeouts;
    firePropertyChange(PROPERTY_MAXREPLYTIMEOUTS, oldValue, maxReplyTimeouts);
  }
  
  public int getMaxAuthenticationRekeys() {
    return maxAuthenticationRekeys;
  }
  
  public void setMaxAuthenticationRekeys(int maxAuthenticationRekeys) {
    Object oldValue = this.maxAuthenticationRekeys;
    this.maxAuthenticationRekeys = maxAuthenticationRekeys;
    firePropertyChange(PROPERTY_MAXAUTHENTICATIONREKEYS, oldValue, maxAuthenticationRekeys);
  }
  
  public int getMaxErrorMessagesSent() {
    return maxErrorMessagesSent;
  }
  
  public void setMaxErrorMessagesSent(int maxErrorMessagesSent) {
    Object oldValue = this.maxErrorMessagesSent;
    this.maxErrorMessagesSent = maxErrorMessagesSent;
    firePropertyChange(PROPERTY_MAXERRORMESSAGESSENT, oldValue, maxErrorMessagesSent);
  }

  public void writeToXML(AuthenticationT xml) {
    xml.authenticationEnabled.setValue(isAuthenticationEnabled());
    xml.authSecStatEventMode.setValue(getAuthSecStatEvtConfig().getEventMode().name());
    xml.authSecStatMaxEvents.setValue(getAuthSecStatEvtConfig().getMaxEvents());
    
    AuthConfigT xmlAuth = xml.authConfig.append();
    xmlAuth.assocId.setValue(getAssocId());
    xmlAuth.MACAlgorithm.setValue(getMACAlgorithm().getValue());
    xmlAuth.maxApplTimeoutCount.setValue(getMaxApplTimeoutCount());
    xmlAuth.replyTimeout.setValue(getReplyTimeoutMs());
    xmlAuth.keyChangeInterval.setValue(getKeyChangeIntervalMs());
    xmlAuth.maxKeyChangeCount.setValue(getMaxKeyChangeCount());
    xmlAuth.aggressiveModeSupport.setValue(isAggressiveModeSupport());
    xmlAuth.disallowSHA1.setValue(isDisallowSHA1());
    xmlAuth.extraDiags.setValue(isExtraDiags());
    xmlAuth.operateInV2Mode.setValue(isOperateInV2Mode());
    
    AuthConfigV2T xmlAuth2 = xml.authConfigV2.append();
    xmlAuth2.maxErrorCount.setValue(getMaxErrorCount());
    
    
    AuthConfigV5T xmlAuth5 = xml.authConfigV5.append();
    xmlAuth5.randomChallengeDataLength.setValue(getRandomChallengeDataLength());
    xmlAuth5.maxSessionKeyStatusCount.setValue(getMaxSessionKeyStatusCount());
    xmlAuth5.maxAuthenticationFailures.setValue(getMaxAuthenticationFailures());
    xmlAuth5.maxReplyTimeouts.setValue(getMaxReplyTimeouts());
    xmlAuth5.maxAuthenticationRekeys.setValue(getMaxAuthenticationRekeys());
    xmlAuth5.maxErrorMessagesSent.setValue(getMaxErrorMessagesSent());
    
    usersConfig.writeToXML(xml.users.append());
  }

  public void readFromXML(AuthenticationT xml) {
    setAuthenticationEnabled(xml.authenticationEnabled.getValue());
    getAuthSecStatEvtConfig().setEventMode(LU_EVENT_MODE.valueOf(xml.authSecStatEventMode.getValue()));
    try {
      getAuthSecStatEvtConfig().setMaxEvents(xml.authSecStatMaxEvents.getValue());
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    
    AuthConfigT xmlAuth = xml.authConfig.first();
    setAssocId              ((int) xmlAuth.assocId.getValue());
    setMACAlgorithm         (DNPAUTH_MAC.forValue((short) xmlAuth.MACAlgorithm.getValue()));
    setMaxApplTimeoutCount  ((short) xmlAuth.maxApplTimeoutCount.getValue());
    setReplyTimeoutMs       (xmlAuth.replyTimeout.getValue());
    setKeyChangeIntervalMs  (xmlAuth.keyChangeInterval.getValue());
    setMaxKeyChangeCount    ((int) xmlAuth.maxKeyChangeCount.getValue());
    setAggressiveModeSupport(xmlAuth.aggressiveModeSupport.getValue());
    setDisallowSHA1         (xmlAuth.disallowSHA1.getValue());
    setExtraDiags           (xmlAuth.extraDiags.getValue());
    setOperateInV2Mode      (xmlAuth.operateInV2Mode.getValue());
    
    AuthConfigV2T xmlAuth2 = xml.authConfigV2.first();
    setMaxErrorCount((short) xmlAuth2.maxErrorCount.getValue());
    
    
    AuthConfigV5T xmlAuth5 = xml.authConfigV5.first();
    setRandomChallengeDataLength((int) xmlAuth5.randomChallengeDataLength .getValue());
    setMaxSessionKeyStatusCount ((int) xmlAuth5.maxSessionKeyStatusCount  .getValue());
    setMaxAuthenticationFailures((int) xmlAuth5.maxAuthenticationFailures .getValue());
    setMaxReplyTimeouts         ((int) xmlAuth5.maxReplyTimeouts          .getValue());
    setMaxAuthenticationRekeys  ((int) xmlAuth5.maxAuthenticationRekeys   .getValue());
    setMaxErrorMessagesSent     ((int) xmlAuth5.maxErrorMessagesSent      .getValue());
    
    
    usersConfig.readFromXML(xml.users.first());
  }

}

