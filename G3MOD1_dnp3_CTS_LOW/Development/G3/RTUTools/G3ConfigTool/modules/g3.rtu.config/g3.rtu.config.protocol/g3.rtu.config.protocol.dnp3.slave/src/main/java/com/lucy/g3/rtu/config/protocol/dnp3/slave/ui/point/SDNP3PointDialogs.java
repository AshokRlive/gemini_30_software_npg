/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointBulkAddDialog;

public class SDNP3PointDialogs {

  private static String createTitle(ScadaPointType type) {
    String title = "DNP3 Point ";

    if (type != null) {
      title = title + " - " + type.getDescription();
    }

    return title;
  }

  public static boolean showEditor(Frame parent, SDNP3Point point, ScadaPointSourceRepository sources) {
    SDNP3PointEditDialog dlg = new SDNP3PointEditDialog(parent, createTitle(point.getType()), point, sources);
    dlg.showDialog();
    return dlg.hasBeenAffirmed();
  }

  /**
   * Shows a editor dialog which has navigation buttons such as "Next", "Prev".
   */
  public static void showEditor(Frame parent, SDNP3PointType type, IScadaPointEditorDialogInvoker<SDNP3Point> invoker,
      ScadaPointSourceRepository sources) {
    Preconditions.checkNotNull(invoker, "provider is null");
    String title = createTitle(type);
    SDNP3PointEditDialog dlg = new SDNP3PointEditDialog(parent, title, type, invoker, sources);
    dlg.showDialog();
  }

  public static ArrayList<SDNP3Point> showBulkAdd(Frame parent, List<ScadaPointSource> sources,
      SDNP3PointType type, IScadaIoMap<SDNP3Point> iomap) {

    ScadaPointBulkAddDialog<SDNP3Point> dialog;
    dialog = new ScadaPointBulkAddDialog<SDNP3Point>(parent, sources, type, iomap, new SDNP3PointFactory());
    showDialog(dialog);
    return dialog.getCreatedPoints();
  }

  public static boolean showBulkEditor(Frame parent, SDNP3PointType type, IScadaIoMap<SDNP3Point> iomap,
      List<SDNP3Point> selections) {

    /* Edit multiple selections */
    SDNP3PointBulkEditDialog editor = new SDNP3PointBulkEditDialog(parent, selections, type, iomap);
    showDialog(editor);

    return !editor.isCancelled();
  }

  private static void showDialog(JDialog dialog) {
    // Show dialogue
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(dialog);
    } else {
      dialog.setVisible(true);
    }
  }

}
