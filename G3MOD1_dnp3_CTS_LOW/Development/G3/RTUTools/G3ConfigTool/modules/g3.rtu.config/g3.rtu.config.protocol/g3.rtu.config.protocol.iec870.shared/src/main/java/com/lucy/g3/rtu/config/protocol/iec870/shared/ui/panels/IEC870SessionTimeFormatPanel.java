/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels;

import java.awt.Component;

import javax.swing.*;

import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Enums;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;
public class IEC870SessionTimeFormatPanel extends JXTaskPane {
  private final PresentationModel<? extends IEC870Session> pm;
  
  @SuppressWarnings("unused")
  private IEC870SessionTimeFormatPanel()
  {
    this.pm = null;
    initComponents();
  }
  public IEC870SessionTimeFormatPanel(PresentationModel<? extends IEC870Session> pm) {
    this.pm = pm;
    initComponents();
    initBindings();
  }

  private void initBindings() {
    Bindings.bind(comboGI, new SelectionInList<>(getTimeFormatOptions(), 
        pm.getModel(IEC870Session.PROPERTY_CICNA_TIMEFORMAT)));
    
    Bindings.bind(comboRead, new SelectionInList<>(getTimeFormatOptions(), 
        pm.getModel(IEC870Session.PROPERTY_READ_TIMEFORMAT)));
    
    Bindings.bind(comboReadMSRD, new SelectionInList<>(getTimeFormatOptions(), 
        pm.getModel(IEC870Session.PROPERTY_READMSRND_TIMEFORMAT)));
    
    
    comboGI.setRenderer(new TimeFormatRenderer("Interrogation Command"));
    comboRead.setRenderer(new TimeFormatRenderer("Read Command"));
    comboReadMSRD.setRenderer(new TimeFormatRenderer("Read Command (Analogue Points)"));
  }
  
  private static LU_TIME_FORMAT[] getTimeFormatOptions() {
    LU_TIME_FORMAT[] options = IEC870Enums.getTimeFormatEnums();
    return ArrayUtils.add(options, 0, null);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    label7 = new JLabel();
    label1 = new JLabel();
    comboGI = new JComboBox<>();
    label2 = new JLabel();
    comboRead = new JComboBox<>();
    label3 = new JLabel();
    comboReadMSRD = new JComboBox<>();

    //======== this ========
    setLayout(new FormLayout(
      "right:default, $lcgap, [50dlu,default], $lcgap, default:grow",
      "3*(default, $ugap), default"));

    //---- label7 ----
    label7.setText("Time format for response to the command:");
    add(label7, CC.xywh(1, 1, 5, 1));

    //---- label1 ----
    label1.setText("Interrogation Command:");
    add(label1, CC.xy(1, 3));
    add(comboGI, CC.xy(3, 3));

    //---- label2 ----
    label2.setText("Read Command:");
    add(label2, CC.xy(1, 5));
    add(comboRead, CC.xy(3, 5));

    //---- label3 ----
    label3.setText("Read Command (Analogue Points):");
    add(label3, CC.xy(1, 7));
    add(comboReadMSRD, CC.xy(3, 7));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  
    setTitle("Time Format");
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label7;
  private JLabel label1;
  private JComboBox<Object> comboGI;
  private JLabel label2;
  private JComboBox<Object> comboRead;
  private JLabel label3;
  private JComboBox<Object> comboReadMSRD;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  private static class TimeFormatRenderer extends DefaultListCellRenderer {
    private final String commandName;
    
    public TimeFormatRenderer(String commandName) {
      super();
      this.commandName = commandName;
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      
      LU_TIME_FORMAT timeFormat = (LU_TIME_FORMAT) value;
      if(timeFormat == null) {
        setText("Default");
        setToolTipText(String.format("Do not configure response time stamp for command:\"%s\"."
            + "\nThe default event time format will be applied.",commandName));
      } else {
        setText(timeFormat.getDescription());
        setToolTipText(String.format("Configure response time stamp to be \"%s\" for command: \"%s\"", 
            timeFormat.getDescription(), commandName));
      }
      return comp;
    }
    
  }
}
