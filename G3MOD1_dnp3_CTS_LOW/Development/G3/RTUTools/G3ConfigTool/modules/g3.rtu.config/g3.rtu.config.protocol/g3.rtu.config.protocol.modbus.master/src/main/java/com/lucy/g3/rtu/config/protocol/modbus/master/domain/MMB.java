/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.master.shared.factory.IMasterProtocolDeviceFactory;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocol;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;

/**
 * The ModBus Master protocol stack.
 */
public class MMB extends AbstractProtocol<MMBChannel> implements IMasterProtocol<MMBChannel> {

  private MasterProtocolManager manager;

  private final IMasterProtocolDeviceFactory deviceFactory;
  

  public MMB(IMasterProtocolDeviceFactory deviceFactory) {
    super(ProtocolType.MMB);
    this.deviceFactory = Preconditions.checkNotNull(deviceFactory, "deviceFactory is null");
  }

  @Override
  public MMBChannel addChannel(String channelName) {
    MMBChannel newchannel = new MMBChannel(this, channelName);
    if (add(newchannel)) {
      return newchannel;
    } else {
      return null;
    }
  }

  public IMasterProtocolDeviceFactory getDeviceFactory() {
    return deviceFactory;
  }

  @Override
  public void setManager(IProtocolManager<?> manager) {
    if (manager != null && !(manager instanceof MasterProtocolManager)) {
      throw new IllegalArgumentException("Cannot set manager. MasterProtocolManager expected!");
    }

    this.manager = (MasterProtocolManager) manager;
  }

  @Override
  public MasterProtocolManager getManager() {
    return manager;
  }

  @Override
  public Collection<MMBSession> getAllSessions() {
    Collection<MMBChannel> chs = getAllChannels();
    ArrayList<MMBSession> sesns = new ArrayList<>();
    for (MMBChannel ch : chs) {
      sesns.addAll(ch.getAllItems());
    }
    return sesns;
  }

}
