/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;

/**
 * The interface of slave protocol for communicating with SCADA.
 *
 * @param <ChannelT>
 *          the protocol channel type.
 */
public interface IScadaProtocol<ChannelT extends IProtocolChannel<?>>
    extends IProtocol<ChannelT>, IScadaPointsContainer {

  /* Override to return ScadaProtocolManager. */
  @Override
  ScadaProtocolManager getManager();
}
