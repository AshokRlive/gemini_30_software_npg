/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;


/**
 *
 */
public final class MMBChannelSerialConf extends ProtocolChannelSerialConf {

  public MMBChannelSerialConf(MMBChannel channel) {
    super(channel);
  }

  // Override to allow access from package.
  @Override
  protected void setActive(boolean active) {
    super.setActive(active);
  }
}

