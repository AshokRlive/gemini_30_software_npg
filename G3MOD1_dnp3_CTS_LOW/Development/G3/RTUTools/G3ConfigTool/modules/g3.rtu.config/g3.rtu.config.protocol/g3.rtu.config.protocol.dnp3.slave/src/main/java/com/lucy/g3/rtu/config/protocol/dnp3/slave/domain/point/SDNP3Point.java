/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import java.util.HashMap;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.xml.gen.api.IXmlEnum;

/**
 * Abstract Protocol Point which is associated with a virtual point
 */
public abstract class SDNP3Point extends AbstractScadaPoint {
  /** The IXmlEnum type parameter {@value}. */
  public final static String PARAMETER_DEFAULT_VARIATION    = "defaultVariation  ";
  /** The Long type parameter {@value}. */
  public final static String PARAMETER_EVENT_DEF_VARIATION  = "eventDefVariation ";
  /** The Long type parameter {@value}. */
  public final static String PARAMETER_EVENT_CLASS          = "eventClass        ";
  /** The Boolean type parameter {@value}. */
  public final static String PARAMETER_ENABLE_CLASS0        = "enableClass0      ";
  /** The String type parameter {@value}. */
  public final static String PARAMETER_CUSTOM_DESCRIPTION   = "customDescription ";
  /** The Boolean type parameter {@value}. */
  public final static String PARAMETER_EVENT_ONLY_CONNECTED = "eventOnlyConnected";
  /** The Boolean type parameter {@value}. */
  public final static String PARAMETER_DISABLED             = "disabled          ";
  /** The Boolean type parameter {@value}. */
  public final static String PARAMETER_ENABLE_STORE_EVENT   = "enableStoreEvent  ";
  /** The Boolean type parameter {@value}. */
  public final static String PARAMETER_CUSTOM_VARIATION_ENABLED = "customVariationEnabled";

  
  SDNP3Point(IScadaIoMap<SDNP3Point> ioMap, SDNP3PointType type, long id) {
    super(ioMap, type, id);
  }


  @Override
  public SDNP3PointType getType() {
    return (SDNP3PointType) super.getType();
  }

  public abstract DNP3ObjConfig getEventConf();
  
  protected static void configureDNP3Obj(DNP3ObjConfig obj, HashMap<String, Object> parameters) {
    String param;
    param = PARAMETER_CUSTOM_VARIATION_ENABLED;
    if (parameters.containsKey(param)) {
      obj.setCustomVariationEnabled((Boolean)parameters.get(param));
    }
    
    param = PARAMETER_DEFAULT_VARIATION;
    if (parameters.containsKey(param)) {
      obj.setStaticVariation(((IXmlEnum)parameters.get(param)).getValue());
    }

    param = PARAMETER_EVENT_DEF_VARIATION;
    if (parameters.containsKey(param)) {
      obj.setEventVariation(((IXmlEnum)parameters.get(param)).getValue());
    }

    param = PARAMETER_ENABLE_CLASS0;
    if (parameters.containsKey(param)) {
      obj.setEnableClass0((boolean) parameters.get(param));
    }
    
    param = PARAMETER_EVENT_CLASS;
    if (parameters.containsKey(param)) {
      obj.setEventClass((long) parameters.get(param));
    }

    param = PARAMETER_EVENT_ONLY_CONNECTED;
    if (parameters.containsKey(param)) {
      obj.setEnableClass0((boolean) parameters.get(param));
    }
  }

}
