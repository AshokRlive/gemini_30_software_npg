/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import com.g3schema.ns_mmb.MMBIOMapT;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.schema.support.G3SchemaAgent;


/**
 * For supporting copy&paste SCADA Points between IO Maps.
 */
class MMBIoMapCopySupport {
  private final G3SchemaAgent schema = new G3SchemaAgent();

  private MMBIoMapCopySupport() {
  }

  public static void copyIoMap(ProtocolType type, MMBIoMap from, MMBIoMap to) {
    Preconditions.checkNotNull(from, "from must not be null");
    Preconditions.checkNotNull(to,   "to must not be null");
    to.clearIoMap();
    
    MMBIoMapCopySupport support = new MMBIoMapCopySupport();
    MMBIOMapT  xml = support.schema.getMMBIoMap();
    from.writeToXML(xml);
    
    to.clearIoMap();
    to.readFromXML(xml);
  }
}

