/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.stub.ScadaPointTypeStub;

/**
 *
 */
public class AbstractScadaPointTest {

  private AbstractScadaPoint fixture;


  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    fixture = new DummyScadaPoint();
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetName() {
    System.out.println(fixture.getName());
  }


  private class DummyScadaPoint extends AbstractScadaPoint {

    protected DummyScadaPoint() {
      super(null, new ScadaPointTypeStub(), 0);
    }

    @Override
    public void applyParameters(HashMap<String, Object> parameters) {
      
    }

  }
}
