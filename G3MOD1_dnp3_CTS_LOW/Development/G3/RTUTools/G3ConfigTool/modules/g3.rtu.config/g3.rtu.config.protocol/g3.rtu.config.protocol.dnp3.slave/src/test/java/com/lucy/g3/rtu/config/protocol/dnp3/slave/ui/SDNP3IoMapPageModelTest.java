/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3IoMap;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3IoMapPageModel;

public class SDNP3IoMapPageModelTest {

  private SDNP3IoMapPageModel pm;
  private final int pointCount = 5;


  @Before
  public void setUp() throws Exception {
    SDNP3Session session = new SDNP3Channel(new SDNP3()).addSession("");
    SDNP3IoMap iomap = session.getIomap();

    for (int i = 0; i < pointCount; i++) {
      iomap.addPoint(SDNP3PointFactory.create(iomap, SDNP3PointType.AnalogueInput, i));
    }
    pm = new SDNP3IoMapPageModel(iomap, SDNP3PointType.AnalogueInput, null);
  }

  @After
  public void tearDown() throws Exception {
    pm = null;
  }

  @Test
  public void testGetAction() {
    String[] actionKeys = {
        SDNP3IoMapPageModel.ACTION_KEY_ADD,
        SDNP3IoMapPageModel.ACTION_KEY_EDIT,
        SDNP3IoMapPageModel.ACTION_KEY_REMOVE,
    };

    for (String key : actionKeys) {
      assertNotNull(pm.getAction(key));
    }
  }

  @Test
  public void testPointCount() {
    assertEquals(pointCount, pm.getItemsCount());
  }

  @Test
  public void testSelection() {
    ArrayList<SDNP3Point> selections = pm.getSelectedItems();
    assertEquals("No selection initially", 0, selections.size());

    pm.selectFirst();
    selections = pm.getSelectedItems();
    assertEquals("One selection", 1, selections.size());

    pm.selectAll();
    selections = pm.getSelectedItems();
    assertEquals("All selection", pointCount, selections.size());

    pm.clearSelection();
    selections = pm.getSelectedItems();
    assertEquals("Selection cleared", 0, selections.size());
  }

  @Test
  public void testEditable() {
    assertFalse("Not editable initially", pm.isEditable());

    pm.selectFirst();
    assertTrue("Editable after one entry selected", pm.isEditable());

    pm.clearSelection();
    assertFalse("Not editable after clearing selection", pm.isEditable());

    pm.selectAll();
    assertTrue("Multip editable if multi entries selected", pm.isEditable());
  }

  @Test
  public void testRemovable() {
    assertFalse("Not removeable initially", pm.isRemoveable());

    pm.selectFirst();
    assertTrue("Editable after one entry selected", pm.isRemoveable());

    pm.clearSelection();
    assertFalse("Not removeable after clearing selection", pm.isRemoveable());

    pm.selectAll();
    assertTrue("Removeable if multi entries selected", pm.isRemoveable());
  }

  @Test
  public void testSelectAll() {
  }

}
