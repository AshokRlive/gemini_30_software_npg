/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.GlobalVariationsConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;

/**
 * The Class SDNP3CounterPoint.
 */
public class SDNP3CounterPoint extends SDNP3InputPoint {

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_USE_ROLLOVER_FLAG = "useRolloverFlag";

  /**
   * The name of read-only property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_IS_FROZEN_COUNTER_ENABLED = "frozenCounterEnabled";

  private boolean useRolloverFlag;
  
  private boolean frzCntEnabled; // frozen counter enabled
  
  private final DNP3ObjConfig frzctrConf;


  public SDNP3CounterPoint(IScadaIoMap<SDNP3Point> iomap, long id) {
    super(iomap, SDNP3PointType.Counter, id);
    
    GlobalVariationsConfig globalVar = iomap == null 
        ? null 
        : ((SDNP3Session) iomap.getSession()).getGlobalEvtVariation();
    frzctrConf = new DNP3ObjConfig(globalVar, DNP3ObjGroup.FrozenCounter);
    frzctrConf.setEnableClass0(!getEventConf().isEnableClass0());
  }

  public boolean isUseRolloverFlag() {
    return useRolloverFlag;
  }

  public void setUseRolloverFlag(boolean useRollover) {
    Object oldValue = this.useRolloverFlag;
    this.useRolloverFlag = useRollover;
    firePropertyChange(PROPERTY_USE_ROLLOVER_FLAG, oldValue, useRollover);
  }

  
  public void setFrozenCounterEnabled(boolean frozenCounterEnabled) {
    Object oldValue = this.frzCntEnabled;
    this.frzCntEnabled = frozenCounterEnabled;
    firePropertyChange(PROPERTY_IS_FROZEN_COUNTER_ENABLED, oldValue, frozenCounterEnabled);
  }

  public boolean isFrozenCounterEnabled() {
    return frzCntEnabled;
  }

  
  public DNP3ObjConfig getFrozenCounterConf() {
    return frzctrConf;
  }

}
