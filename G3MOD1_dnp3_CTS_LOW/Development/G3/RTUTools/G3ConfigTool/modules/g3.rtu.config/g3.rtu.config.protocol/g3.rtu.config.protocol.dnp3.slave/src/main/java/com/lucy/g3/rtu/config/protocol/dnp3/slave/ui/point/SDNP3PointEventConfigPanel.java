/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.JCollapsiblePane;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Enums;
import com.lucy.g3.xml.gen.api.IXmlEnum;
class SDNP3PointEventConfigPanel extends JCollapsiblePane {

  private PresentationModel<DNP3ObjConfig> pm;

  
  public SDNP3PointEventConfigPanel() {
    initComponents();
    setAnimated(false);
  }
  
  
  public boolean bind(PresentationModel<DNP3ObjConfig> pm) {
    if(checkModel(pm) == false)
      return false;
    
    this.pm = pm;
    
    
    DNP3ObjGroup group = pm.getBean().getGroup();
    ValueModel vm;

    // ====== Bind Default Static Variation ======
    vm = pm.getBufferedModel(DNP3ObjConfig.PROPERTY_STATIC_VARIATION);
    vm = VariationConverter.createConvertedValueModel(vm, group, true);
    Bindings.bind(comboDefVar, new ComboBoxAdapter<IXmlEnum>(SDNP3Enums.getStaticVariationEnums(group), vm));

    // ====== Bind Default Event Variation======
    vm = pm.getBufferedModel(DNP3ObjConfig.PROPERTY_EVENT_VARIATION);
    vm = VariationConverter.createConvertedValueModel(vm, group, false);
    Bindings.bind(comboEventDefVar, new ComboBoxAdapter<IXmlEnum>(SDNP3Enums.getEventVariationEnums(group),vm));

    // ====== Bind Event Class ======
    Bindings.bind(comboEventClass, new ComboBoxAdapter<Long>(
        DNP3ObjConfig.getEventClassItems(),
        pm.getBufferedModel(DNP3ObjConfig.PROPERTY_EVENT_CLASS)));

    // ====== Bind Enable Class0======
    Bindings.bind(checkBoxEnableClass0, pm.getBufferedModel(DNP3ObjConfig.PROPERTY_ENABLECLASS0));
    Bindings.bind(checkBoxEventOnly, pm.getBufferedModel(DNP3ObjConfig.PROPERTY_EVENT_ONLY_WHEN_CONNECTED));
    
    // ====== Bind Customise CheckBox ======
    vm = pm.getBufferedModel(DNP3ObjConfig.PROPERTY_CUSTOM_VARIATION_ENABLED);
    Bindings.bind(checkBoxCustomise, vm);
    
    // Update GUI state when buffered "customise" value changes
    vm.addValueChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
          updateEnableState();
      }
    });
    
    updateEnableState();
    updateVisibility();
    
    return true;
  }

  private void updateVisibility() {
    boolean hasEvent = pm.getBean().isHasEventVariation();
    boolean hasStatic = pm.getBean().isHasStaticVariation();
    
    lblDefVar.setVisible(hasStatic);
    comboDefVar.setVisible(hasStatic);
    
    lblEvtDefVar.setVisible(hasEvent);
    comboEventDefVar.setVisible(hasEvent);
    lblEvtClass.setVisible(hasEvent);
    comboEventClass.setVisible(hasEvent);
    checkBoxEventOnly.setVisible(hasEvent);
  }

  private boolean checkModel(PresentationModel<DNP3ObjConfig> pm) {
    return pm != null && pm.getBean() != null;
  }
  
  private void updateEnableState() {
    boolean enabled = checkBoxCustomise.isSelected();
    comboDefVar.setEnabled(enabled );
    comboEventDefVar.setEnabled(enabled );
  }

  private void initRenderers() {
    comboEventClass.setRenderer(new EventClassRenderer());
    comboEventDefVar.setRenderer(new VariationsRenderer());
    comboDefVar.setRenderer(new VariationsRenderer());
  }

  private void checkBoxCustomiseActionPerformed(ActionEvent e) {
    updateCustomiseVariation();
    updateEnableState();
  }
  
  private void updateCustomiseVariation() {
    if (checkModel(pm) && checkBoxCustomise.isSelected() == false) {
      /* Update variation with global value*/
      pm.setBufferedValue(DNP3ObjConfig.PROPERTY_STATIC_VARIATION, pm.getBean().getGlobalStaticVariation());
      pm.setBufferedValue(DNP3ObjConfig.PROPERTY_EVENT_VARIATION, pm.getBean().getGlobalEventVariation());
    }
  }

  public JCheckBox getCheckBoxEnableClass0() {
    return checkBoxEnableClass0;
  }

  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    lblDefVar = new JLabel();
    comboDefVar = new JComboBox<>();
    checkBoxCustomise = new JCheckBox();
    lblEvtDefVar = new JLabel();
    comboEventDefVar = new JComboBox<>();
    comboEventClass = new JComboBox<>();
    lblEvtClass = new JLabel();
    checkBoxEnableClass0 = new JCheckBox();
    checkBoxEventOnly = new JCheckBox();

    //======== this ========
    setLayout(new BorderLayout());

    //======== contentPanel ========
    {
      contentPanel.setBorder(new TitledBorder("DNP3 Object Settings"));
      contentPanel.setLayout(new FormLayout(
          "right:default, $lcgap, [50dlu,default], $lcgap, [80dlu,default]:grow",
          "4*(default, $lgap), default"));

      //---- lblDefVar ----
      lblDefVar.setText("Default Variation:");
      contentPanel.add(lblDefVar, CC.xy(1, 1));
      contentPanel.add(comboDefVar, CC.xy(3, 1));

      //---- checkBoxCustomise ----
      checkBoxCustomise.setText("Customise");
      checkBoxCustomise.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          checkBoxCustomiseActionPerformed(e);
        }
      });
      contentPanel.add(checkBoxCustomise, CC.xy(5, 1));

      //---- lblEvtDefVar ----
      lblEvtDefVar.setText("Event Default Variation:");
      contentPanel.add(lblEvtDefVar, CC.xy(1, 3));
      contentPanel.add(comboEventDefVar, CC.xy(3, 3));
      contentPanel.add(comboEventClass, CC.xy(3, 5));

      //---- lblEvtClass ----
      lblEvtClass.setText("Event Class:");
      contentPanel.add(lblEvtClass, CC.xy(1, 5));

      //---- checkBoxEnableClass0 ----
      checkBoxEnableClass0.setText("Enable Class0");
      contentPanel.add(checkBoxEnableClass0, CC.xy(3, 7));

      //---- checkBoxEventOnly ----
      checkBoxEventOnly.setText("Event Only When Connected to SCADA");
      contentPanel.add(checkBoxEventOnly, CC.xywh(3, 9, 3, 1));
    }
    add(contentPanel, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    
    initRenderers();
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel lblDefVar;
  private JComboBox<Object> comboDefVar;
  private JCheckBox checkBoxCustomise;
  private JLabel lblEvtDefVar;
  private JComboBox<Object> comboEventDefVar;
  private JComboBox<Object> comboEventClass;
  private JLabel lblEvtClass;
  private JCheckBox checkBoxEnableClass0;
  private JCheckBox checkBoxEventOnly;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

 
}
