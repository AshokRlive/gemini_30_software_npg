/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.domain;

import org.apache.log4j.Logger;

import com.g3schema.ns_s104.S104SesnConfT;
import com.g3schema.ns_s104.S104SessionT;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * The Session of S104.
 */
public class S104Session extends IEC870Session {

  private Logger log = Logger.getLogger(S104Session.class);
  

  // @formatter:off
  
  private static final long S104_IOA_SIZE        = 3;
  private static final long S104_ASDU_ADDR_SIZE  = 2;
  private static final long S104_COT_SIZE        = 2;
  
  public static final String PROPERTY_MAX_APDU_SIZE            = "maxApduSize";
  public static final String PROPERTY_MAXCOMMANDAGE            = "maxCommandAge";
  public static final String PROPERTY_MAXCOMMAND_FUTURE        = "maxCommandFuture";
  
  // ====== Read only properties ======
  public static final String PROPERTY_COT_SIZE                 = "cotSize";
  public static final String PROPERTY_ASDU_ADDR_SIZE           = "asduAddrSize";
  public static final String PROPERTY_INFO_OBJ_ADDR_SIZE       = "infoObjAddrSize";
  
  private long        maxCommandAge          = getDefault(PROPERTY_MAXCOMMANDAGE);  // ms
  private long        maxCommandFuture       = getDefault(PROPERTY_MAXCOMMAND_FUTURE);  // ms;
  private long        maxApduSize            = getDefault(PROPERTY_MAX_APDU_SIZE);
  private long infoObjAddrSize = (int) getDefault("infoObjAddrSize");
  // @formatter:on

  private final IEC870IoMap iomap = new IEC870IoMap(this);

  private final S104Channel channel;


  public S104Session(S104Channel channel, String sessionName) {
    super(channel, sessionName);
    this.channel = channel;

    LoggingUtil.logPropertyChanges(this);
  }

  private static long getDefault(String property) {
    return IEC870Constraints.INSTANCE_104.getDefault(IEC870Constraints.PREFIX_SESSION, property);
  }

  @Override
  public IProtocolChannel<? extends IProtocolSession> getProtocolChannel() {
    return channel;
  }

  @Override
  public void delete() {
    iomap.clearIoMap();

  }

  @Override
  public IEC870IoMap getIomap() {
    return iomap;
  }

  private void readFromXML(S104SesnConfT xml) {
    super.readIEC870FromXML(xml);
    
    setMaxCommandAge(xml.maxCommandAge.getValue());
    setMaxCommandFuture(xml.maxCommandFuture.getValue());
    setMaxApduSize(xml.maxAPDUSize.getValue());
    
    if(xml.infoObjAddrSize.exists())
        setInfoObjAddrSize(xml.infoObjAddrSize.getValue());
  }

  public void readFromXML(S104SessionT xml, ScadaPointSourceRepository resource) {
    readFromXML(xml.config.first());
    getIomap().readFromXML(xml.iomap.first(), resource);
  }

  private void writeToXML(S104SesnConfT xml) {
    super.writeIEC870ToXML(xml);
    xml.maxCommandAge.setValue(getMaxCommandAge());
    xml.maxCommandFuture.setValue(getMaxCommandFuture());
    xml.maxAPDUSize.setValue(getMaxApduSize());
    xml.infoObjAddrSize.setValue((int) getInfoObjAddrSize());
  }

  public void writeToXML(S104SessionT xml) {
    writeToXML(xml.config.append());
    getIomap().writeToXML(xml.iomap.append());
  }

  public long getCotSize() {
    return S104_COT_SIZE;
  }
  
  public long getAsduAddrSize() {
    return S104_ASDU_ADDR_SIZE;
  }
  
  public long getInfoObjAddrSize() {
	    return infoObjAddrSize;
  }
  
  public void setInfoObjAddrSize(long infoObjAddrSize) {
	    Object oldValue = this.infoObjAddrSize;
	    this.infoObjAddrSize = infoObjAddrSize;
	    firePropertyChange(PROPERTY_INFO_OBJ_ADDR_SIZE, oldValue, infoObjAddrSize);
  }

  public long getMaxCommandAge() {
    return maxCommandAge;
  }

  public void setMaxCommandAge(long maxCommandAge) {
    Object oldValue = this.maxCommandAge;
    this.maxCommandAge = maxCommandAge;
    firePropertyChange(PROPERTY_MAXCOMMANDAGE, oldValue, maxCommandAge);
  }

  public long getMaxCommandFuture() {
    return maxCommandFuture;
  }

  public void setMaxCommandFuture(long maxCommandFuture) {
    Object oldValue = this.maxCommandFuture;
    this.maxCommandFuture = maxCommandFuture;
    firePropertyChange(PROPERTY_MAXCOMMAND_FUTURE, oldValue, maxCommandFuture);
  }

  public long getMaxApduSize() {
    return maxApduSize;
  }

  public void setMaxApduSize(long maxApduSize) {
    Object oldValue = this.maxApduSize;
    this.maxApduSize = maxApduSize;
    firePropertyChange(PROPERTY_MAX_APDU_SIZE, oldValue, maxApduSize);
  }

  @Override
  public IEC870Constraints getConstraints() {
    return IEC870Constraints.INSTANCE_104;
  }

}
