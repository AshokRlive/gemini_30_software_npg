/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import java.util.HashMap;

import com.g3schema.ns_iec870.IEC870OutputPointT;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.base.math.BitMask;

/**
 * The implementation of IEC870 Output Point.
 */
public class IEC870OutputPoint extends IEC870Point {

  public static final String PROPERTY_SELECTREQUIRED = "selectRequired";

  private boolean selectRequired;


  IEC870OutputPoint(IScadaIoMap<IEC870Point> ioMap, long id, IEC870PointType type) {
    super(ioMap, type, id);
  }

  public boolean isSelectRequired() {
    return selectRequired;
  }

  public void setSelectRequired(boolean selectRequired) {
    Object oldValue = this.selectRequired;
    this.selectRequired = selectRequired;
    firePropertyChange(PROPERTY_SELECTREQUIRED, oldValue, selectRequired);
  }

  public void writeToXML(IEC870OutputPointT xml) {
    super.writeToXML(xml);

    xml.selectRequired.setValue(isSelectRequired());

    ScadaPointSource res = getSource();
    if (res != null) {
      xml.controlLogicGroup.setValue(res.getGroup());
    }
  }

  public void readFromXML(IEC870OutputPointT xml) {
    super.readFromXML(xml);

    if (xml.selectRequired.exists()) {
      setSelectRequired(xml.selectRequired.getValue());
    }
  }

  @Override
  public BitMask getGroupMask() {
    return null;
  }

  @Override
  public void applyParameters(HashMap<String, Object> parameters) {
    String param;
    
    param = PROPERTY_ENABLED;
    if (parameters.containsKey(param)) {
      setEnabled(!(boolean)parameters.get(param));
    }
    
    param = PROPERTY_SELECTREQUIRED;
    if (parameters.containsKey(param)) {
      setSelectRequired((boolean)parameters.get(param));
    }
  }

}
