/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Session;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

public class S104SessionManagerModel extends AbstractProtocolItemManagerModel {

  private final S104Channel iec104Chnl;

  private final SelectionInList<S104Session> selectionInList;


  S104SessionManagerModel(S104Channel iec104Chnl) {
    this.iec104Chnl = Preconditions.checkNotNull(iec104Chnl, "iec104Chnl is null");
    this.selectionInList = new SelectionInList<S104Session>(iec104Chnl.getItemListModel());
  }

  @Override
  public SelectionInList<?> getSelectionInList() {
    return selectionInList;
  }

  @Override
  protected void addActionPerformed() {
    Object result = ProtocolAddingWizards.showAddSession(iec104Chnl);
    if (result != null)
      selectionInList.setSelection((S104Session) result);
  }

  @Override
  protected void removeActionPerformed() {
    S104Session sel = selectionInList.getSelection();
    if (sel != null) {
      if (showConfirmRemoveDialog(sel.getSessionName())) {
        iec104Chnl.remove(sel);
      }
    }
  }

}
