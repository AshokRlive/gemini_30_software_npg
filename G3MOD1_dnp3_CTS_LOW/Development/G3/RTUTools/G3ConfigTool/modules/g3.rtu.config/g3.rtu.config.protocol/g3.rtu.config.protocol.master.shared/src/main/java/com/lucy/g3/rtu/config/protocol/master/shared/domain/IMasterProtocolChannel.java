/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.domain;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;

/**
 *
 */
public interface IMasterProtocolChannel<SessionT extends IMasterProtocolSession>
    extends IProtocolChannel<SessionT> {

}
