/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;

/**
 * DNP3 slave protocol stack, contains all DNP3 slave configurations and
 * protocol channels.
 */
public class SDNP3 extends AbstractScadaProtocol<SDNP3Channel> {

  public SDNP3() {
    super(ProtocolType.SDNP3);
  }

  @Override
  public Collection<SDNP3Session> getAllSessions() {
    ArrayList<SDNP3Session> allSesns = new ArrayList<SDNP3Session>();
    Collection<SDNP3Channel> channelList = getAllChannels();
    for (SDNP3Channel chnl : channelList) {
      allSesns.addAll(chnl.getAllSessions());
    }

    return allSesns;
  }

  @Override
  public SDNP3Channel addChannel(String channelName) {
    SDNP3Channel newChnl = new SDNP3Channel(this, channelName);
    if (super.add(newChnl)) {
      return newChnl;
    } else {
      return null;
    }
  }

  public Collection<SDNP3Point> getAllSDNP3InputPoints() {
    return findSDNP3Points(super.getAllInputPoints());
  }

  public Collection<SDNP3Point> getAllSDNP3Points() {
    return findSDNP3Points(super.getAllPoints());
  }

  private static Collection<SDNP3Point> findSDNP3Points(Collection<ScadaPoint> points) {
    ArrayList<SDNP3Point> sdnp3Points = new ArrayList<SDNP3Point>();

    for (ScadaPoint p : points) {
      if (p instanceof SDNP3Point) {
        sdnp3Points.add((SDNP3Point) p);
      }
    }

    return sdnp3Points;
  }
}
