/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;

import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.validation.view.ValidationComponentUtils;


/**
 * The Class IOAEditor.
 */
public final class IOAEditor extends JFormattedTextField {
  
  public IOAEditor() {
    super(IOAFormatter.getInstance());
    
    setColumns(12);
    addPropertyChangeListener("editValid", new EditValidPCL(this));
  }
  
  private static class EditValidPCL implements PropertyChangeListener {

    private final Color orginBG;


    public EditValidPCL(JFormattedTextField ftf) {
      this.orginBG = ftf.getBackground();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if ("editValid".equals(evt.getPropertyName())) {
        JFormattedTextField jtf = (JFormattedTextField) evt.getSource();
        boolean valid = (Boolean) (evt.getNewValue());

        if (valid) {
          jtf.setBackground(orginBG);
        } else {
          ValidationComponentUtils.setErrorBackground(jtf);
        }
      }
    }
  }


  public static DefaultTableRenderer createCellRenderer() {
    return new IOAEditorRenderer();
  }

  public static DefaultCellEditor createCellEditor() {
    final IOAEditor editor = new IOAEditor();
    return new DefaultCellEditor(editor) {

      @Override
      public Object getCellEditorValue() {
        Object val = editor.getValue();
        return val;
      }

      @Override
      public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
      }
      
    };
  }
  
  private static class IOAEditorRenderer extends DefaultTableRenderer {
    IOAEditorRenderer() {
      super(new StringValue() {
        @Override
        public String getString(Object value) {
          try {
            long ioaValue = Long.valueOf(value.toString());
            return IOAFormatter.getInstance().valueToString(ioaValue);
          } catch (Throwable e) {
            e.printStackTrace();
            return String.valueOf(value);
          }
        }
      });
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      /**
       * Fixed a bug that unselected background is blue which is caused by highlighter. 
       */
      if (!isSelected) {
        comp.setBackground(table.getBackground());
        comp.setForeground(table.getForeground());
      }
      
      return comp;
    }
    
  }

}

