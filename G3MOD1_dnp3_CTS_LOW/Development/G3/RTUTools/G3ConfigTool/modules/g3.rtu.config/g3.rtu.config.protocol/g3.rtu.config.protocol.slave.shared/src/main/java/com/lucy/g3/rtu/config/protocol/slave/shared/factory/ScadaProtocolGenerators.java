/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.factory;

import java.util.HashMap;

import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;


/**
 *
 */
public class ScadaProtocolGenerators {

  private ScadaProtocolGenerators() {}
  private static final HashMap<ProtocolType, IScadaProtocolGenerator> factories = new HashMap<>();
  
  public static void registerFactory(ProtocolType type, IScadaProtocolGenerator factory) {
    factories.put(type, factory);
  }
  
  public static IScadaProtocolGenerator getFactory(ProtocolType type) {
    return factories.get(type);
  }

}

