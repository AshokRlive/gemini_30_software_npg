/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.util;

import javax.swing.JFormattedTextField;
import javax.swing.text.DefaultFormatterFactory;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.Constraints;

/**
 * Utility for setting the constraint of protocol input fields.
 */
public class ProtocolConstraints extends Constraints {

  private String prefix;


  public ProtocolConstraints(String constraintFileName) {
    super(constraintFileName);
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public void setBoundary(JFormattedTextField ftf, String property) {
    Preconditions.checkNotNull(prefix, "prefix is null. Set prefix before setBoundary!");
    setBoundary(ftf, prefix, property);
  }

  public void setBoundary(JFormattedTextField ftf, String prefix,
      String property) {
    ftf.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(
            getMin(prefix, property),
            getMax(prefix, property))));
  }

}
