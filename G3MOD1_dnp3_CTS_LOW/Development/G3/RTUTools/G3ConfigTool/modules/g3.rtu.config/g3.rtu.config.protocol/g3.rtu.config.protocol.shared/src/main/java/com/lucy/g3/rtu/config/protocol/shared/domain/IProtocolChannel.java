/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import java.util.Collection;

import com.lucy.g3.itemlist.IItemListManager;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;

/**
 * The interface of Protocol channel which contain a list of sessions.
 *
 * @param <SessionT>
 *          The type of protocol session.
 */
public interface IProtocolChannel<SessionT extends IProtocolSession> extends IItemListManager<SessionT>, IContainerValidation{
  
  String PROPERTY_ENABLED = "enabled";
  
  /**
   * Gets the name of this channel.
   *
   * @return
   */
  String getChannelName();

  void delete();

  /**
   * Gets the protocol.
   *
   * @return the non-null protocol.
   */
  IProtocol<?> getProtocol();

  /**
   * Gets the configuration settings of this channel.
   *
   * @return Non-null configuration object.
   */
  IProtocolChannelConf getChannelConfig();

  /**
   * Gets all session of this channel.
   *
   * @return a collection of sessions.
   */
  Collection<? extends IProtocolSession> getAllSessions();

  /**
   * Adds a session by specifying the name.
   *
   * @param sessionName
   *          the name of the session to be added.
   * @return new added session object. Null if adding session fails.
   */
  SessionT addSession(String sessionName);

  /**
   * Gets the total number of sessions of this channel.
   *
   * @return the session num
   */
  int getSessionNum();

  /**
   * Checks if this channel allows to have multiple session.
   */
  boolean allowMultiSession();
  
  boolean isEnabled();
  
  void setEnabled(boolean enabled);

  String getUUID();
  void setUUID(String uuid);

  ProtocolType getProtocolType();

}
