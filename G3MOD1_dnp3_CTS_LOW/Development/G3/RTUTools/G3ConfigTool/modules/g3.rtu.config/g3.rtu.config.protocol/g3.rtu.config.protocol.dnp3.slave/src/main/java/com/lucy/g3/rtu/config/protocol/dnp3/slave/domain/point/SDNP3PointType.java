/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;

/**
 * The Enum SDNP3 Point Type.
 */
public enum SDNP3PointType implements ScadaPointType {
  AnalogueInput("Analogue Input", true),
  BinaryInput("Binary Input", true),
  DoubleBinaryInput("Double Binary Input", true),
  Counter("Counter", true),
  BinaryOutput("Binary Output", false),
  AnalogOutput("Analogue Output", false);

  SDNP3PointType(String description, boolean isInput) {
    this.description = description;
    this.isInput = isInput;
  }

  @Override
  public boolean isInput() {
    return isInput;
  }

  @Override
  public boolean isOutput() {
    return !isInput;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return description;
  }

  @Override
  public long getMaximumPointID() {
    return MAX_ID;
  }

  @Override
  public long getMinimumPointID() {
    return MIN_ID;
  }


  private final String description;
  private final boolean isInput;

  public static final long MAX_ID = Integer.MAX_VALUE;
  public static final long MIN_ID = 0;
  
  public static final SDNP3PointType[] OUTPUT_TYPES = {BinaryOutput,AnalogOutput};
  
  public static boolean isAnalogue(SDNP3PointType type) {
    return type == AnalogOutput
      ||type == AnalogueInput;
  }
}
