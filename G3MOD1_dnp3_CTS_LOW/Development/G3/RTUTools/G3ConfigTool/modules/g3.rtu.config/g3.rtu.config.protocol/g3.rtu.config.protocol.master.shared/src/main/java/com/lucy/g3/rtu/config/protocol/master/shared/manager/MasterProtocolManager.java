/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.manager;

import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The interface of Master Protocol Stack Manager.
 */
public interface MasterProtocolManager extends IProtocolManager<IMasterProtocol<?>>,IConfigModule, IValidation  {

  String CONFIG_MODULE_ID = "MasterProtocolManager";

  /**
   * Gets ModBus master protocol if it exists.
   *
   * @return instance of of Modbus master if it exists, null if not.
   */
  IMasterProtocol<?> getModbusMaster();

}
