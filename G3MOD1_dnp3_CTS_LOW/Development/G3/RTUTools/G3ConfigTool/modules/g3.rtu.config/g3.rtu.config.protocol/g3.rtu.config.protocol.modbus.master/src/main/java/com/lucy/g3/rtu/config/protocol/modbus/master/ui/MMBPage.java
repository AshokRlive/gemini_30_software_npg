/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

public class MMBPage extends ProtocolChannelManagerPage {

  public MMBPage(MMB mmb) {
    super(mmb, new MMBPageModel(mmb));
  }


  private static final class MMBPageModel extends AbstractProtocolItemManagerModel {

    private final MMB mbm;

    private final SelectionInList<MMBChannel> selectionInList;


    public MMBPageModel(MMB mbm) {
      this.mbm = Preconditions.checkNotNull(mbm, "mbm is null");
      this.selectionInList = new SelectionInList<MMBChannel>(mbm.getItemListModel());
    }

    @Override
    public SelectionInList<MMBChannel> getSelectionInList() {
      return selectionInList;
    }

    @Override
    protected void addActionPerformed() {
      IProtocolChannel result = ProtocolAddingWizards.showAddChannel(mbm);
      if(result != null)
        selectionInList.setSelection((MMBChannel) result);
    }

    @Override
    protected void removeActionPerformed() {
      MMBChannel sel = selectionInList.getSelection();
      if (sel != null) {
        if (showConfirmRemoveDialog(sel.getChannelName())) {
          mbm.remove(sel);
        }
      }

    }

  }

}
