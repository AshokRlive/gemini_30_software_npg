/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

/**
 * Presentation model of SCADA session page.
 */
public class SDNP3SessionManagerModel extends AbstractProtocolItemManagerModel {

  private final SDNP3Channel sdnp3Chnl;

  private final SelectionInList<SDNP3Session> selectionInList;


  public SDNP3SessionManagerModel(SDNP3Channel sdnp3Chnl) {
    this.sdnp3Chnl = Preconditions.checkNotNull(sdnp3Chnl, "sdnp3Chnl is null");
    this.selectionInList = new SelectionInList<SDNP3Session>(sdnp3Chnl.getItemListModel());
  }

  @Override
  public SelectionInList<SDNP3Session> getSelectionInList() {
    return selectionInList;
  }

  @Override
  protected void addActionPerformed() {
    Object result = ProtocolAddingWizards.showAddSession(sdnp3Chnl);
    if (result != null)
      selectionInList.setSelection((SDNP3Session) result);
  }

  @Override
  protected void removeActionPerformed() {
    SDNP3Session sel = selectionInList.getSelection();
    if (sel != null) {
      if (showConfirmRemoveDialog(sel.getSessionName())) {
        sdnp3Chnl.remove(sel);
      }
    }
  }

}
