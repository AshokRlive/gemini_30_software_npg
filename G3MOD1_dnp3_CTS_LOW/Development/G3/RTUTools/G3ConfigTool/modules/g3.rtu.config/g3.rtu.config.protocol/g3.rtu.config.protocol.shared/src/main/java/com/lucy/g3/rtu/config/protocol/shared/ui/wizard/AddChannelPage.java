/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.builder.DefaultFormBuilder;

class AddChannelPage extends WizardPage {
  public final static String KEY_CHANNEL_NAME = "channelName";
  public final static String KEY_ADD_SESSION  = "addSession";
  
  public AddChannelPage() {
    super("Add a channel");
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = ProtocolAddingWizards.createPanelBuilder(this);
    
    JTextField tf = new JTextField("New Channel");
    tf.setName(KEY_CHANNEL_NAME);
    builder.append("Channel Name:", tf);
    builder.nextLine();
    
    
    builder.appendGlueRow();
    builder.nextLine();
    
    JCheckBox cbox = new JCheckBox("Add a session");
    cbox.setName(KEY_ADD_SESSION);
    builder.append("", cbox);
    builder.nextLine();
    cbox.setSelected(true);
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    /* Validate Create Session*/
    if(getWizardData(KEY_ADD_SESSION) == Boolean.TRUE) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    } else {
      setForwardNavigationMode(WizardController.MODE_CAN_FINISH);
    }
    
    return null;
  }
}
