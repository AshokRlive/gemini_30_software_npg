/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.validation;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.message.SimpleValidationMessage;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;

/**
 * Validate the buffer value in DNP3 point presentation model.
 */
public class ScadaPointBufferValidator extends BufferValidator {

  public ScadaPointBufferValidator(PresentationModel<? extends ScadaPoint> target) {
    super(target);
  }

  @Override
  public void validate(ValidationResult result) {
    Object bean = target.getBean();
    if (bean != null && bean instanceof ScadaPoint) {

      ScadaPoint point = (ScadaPoint) bean;
      IScadaIoMap<? extends ScadaPoint> iomap = point.getIoMap();

      long bufProtocolID = (Long) target.getBufferedValue(ScadaPoint.PROPERTY_POINT_ID);

      // Check duplicated protocol ID
      if (iomap != null && iomap.exists(point.getType(), bufProtocolID)
          && iomap.getPoint(point.getType(), bufProtocolID) != point) {
        SimpleValidationMessage msg = new SimpleValidationMessage(
            "Conflict protocol ID: " + bufProtocolID,
            Severity.ERROR,
            AbstractScadaPoint.PROPERTY_POINT_ID);
        result.add(msg);
      }

      // Check invalid protocol ID range
      else if (bufProtocolID < point.getType().getMinimumPointID()
          || bufProtocolID > point.getType().getMaximumPointID()) {
        SimpleValidationMessage msg = new SimpleValidationMessage(
            "Invalid protocol point ID: " + bufProtocolID,
            Severity.ERROR,
            AbstractScadaPoint.PROPERTY_POINT_ID);
        result.add(msg);
      }

      // Check if this point is mapped to any resource
      BufferValidator.validatePropertyEmpty(result, target, AbstractScadaPoint.PROPERTY_SOURCE, "the Source");
    }
  }

}
