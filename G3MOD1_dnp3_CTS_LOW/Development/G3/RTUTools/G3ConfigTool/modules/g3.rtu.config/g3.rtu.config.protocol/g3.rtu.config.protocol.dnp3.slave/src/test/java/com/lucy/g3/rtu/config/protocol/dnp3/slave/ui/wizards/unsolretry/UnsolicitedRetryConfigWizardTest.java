/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.wizards.unsolretry;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.UnsolRetry;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.wizards.unsolretry.UnsolicitedRetryWizard;


/**
 *
 */
public class UnsolicitedRetryConfigWizardTest {

  public static void main(String[] args) {
    UnsolicitedRetryWizard.showWizard(new UnsolRetry());
  }
}

