/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.domain;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.g3schema.ns_s104.RedundancyChnlT;
import com.g3schema.ns_s104.RedundancyT;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;


/**
 * The S104 Redundancy Support.
 */
public class S104Redundancy extends BeanWithEnable {
  private final ArrayListModel<S104RdcyChannel> rdcyChannels = new ArrayListModel<>();
  
  
  public ListModel<S104RdcyChannel> getRdcyChannelsListModel() {
    return rdcyChannels;
  }

  public void add(S104RdcyChannel newRdcyChnl) {
    if(newRdcyChnl != null)
      rdcyChannels.add(newRdcyChnl);
  }
  
  public void remove(S104RdcyChannel removeRdcyChnl) {
    rdcyChannels.remove(removeRdcyChnl);
  }
  
  public static class S104RdcyChannel extends Model{
    final public static String PROPERTY_IPADDRESS = "ipAddress";
    final public static String PROPERTY_IPPORT = "ipPort";
    
    private Logger log = Logger.getLogger(S104RdcyChannel.class);
    
    private String ipAddress = "0.0.0.0";
    private long ipPort = S104ChannelTCPConf.DEFAULT_PORT;
    
    
    public String getIpAddress() {
      return ipAddress;
    }
    
    public void setIpAddress(String ipAddress) {
      if (IPAddress.validateIPAddress(ipAddress, false, true) == false) {
        log.error("Failed to set IP address. Invalid IP address: " + ipAddress);
        return;
      }
      
      Object oldValue = this.ipAddress;
      this.ipAddress = ipAddress;
      firePropertyChange(PROPERTY_IPADDRESS, oldValue, ipAddress);
    }
    
    public long getIpPort() {
      return ipPort;
    }
    
    public void setIpPort(long ipPort) {
      if (IPAddress.validateIPPort(ipPort) == false) {
        log.error("Failed to set IP port. Invalid IP port: " + ipPort);
        return;
      }
      
      Object oldValue = this.ipPort;
      this.ipPort = ipPort;
      firePropertyChange(PROPERTY_IPPORT, oldValue, ipPort);
    }
  }
  public void writeToXML(RedundancyT xml) {
    xml.enabled.setValue(isEnabled());
    
    RedundancyChnlT xmlChnl;
    for (S104RdcyChannel ch : rdcyChannels) {
      xmlChnl = xml.channel.append();
      xmlChnl.ipAddress.setValue(ch.getIpAddress());
      xmlChnl.ipPort.setValue(ch.getIpPort());
    }
  }

  public void readFromXML(RedundancyT xml) {
    setEnabled(xml.enabled.getValue());
    
    S104RdcyChannel ch;
    int count = xml.channel.count();
    for (int i = 0; i < count; i++) {
      ch = new S104RdcyChannel();
      ch.setIpAddress(xml.channel.at(i).ipAddress.getValue());
      ch.setIpPort(xml.channel.at(i).ipPort.getValue());
      add(ch);
    }
  }
}

