/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;

import com.l2fprod.common.beans.editor.AbstractPropertyEditor;
import com.l2fprod.common.beans.editor.ComboBoxPropertyEditor;
import com.l2fprod.common.swing.LookAndFeelTweaks;
import com.lucy.g3.rtu.config.shared.base.bean.BeanInfo;


/**
 *
 */
public class SDNP3UserBeanInfo extends BeanInfo {

  public SDNP3UserBeanInfo() {
    super(SDNP3User.class);
    
    addProperty(SDNP3User.PROPERTY_USER_NAME  );
    addProperty(SDNP3User.PROPERTY_USER_ROLE   ).setPropertyEditorClass(RoleEditor.class);
    addProperty(SDNP3User.PROPERTY_USER_NUMBER);
    addProperty(SDNP3User.PROPERTY_KEY_WRAP_ALGORITHM);
    addProperty(SDNP3User.PROPERTY_UDPATE_KEY ).setPropertyEditorClass(UpdateKeyPropertyEditor.class);
  }

  public static class RoleEditor extends ComboBoxPropertyEditor {

    public RoleEditor() {
      super();
      setAvailableValues(SDNPAuthEnums.DNPAUTH_USER_ROLE.values());
    }
  }
  
  public static class UpdateKeyPropertyEditor extends AbstractPropertyEditor {

    public UpdateKeyPropertyEditor() {
      MaskFormatter formatter = null;
      try {
        formatter = new MaskFormatter("HH HH HH HH HH HH HH HH HH HH HH HH HH HH HH HH "
            + "HH HH HH HH HH HH HH HH HH HH HH HH HH HH HH HH");
        formatter.setPlaceholderCharacter(' ');
      } catch (ParseException e) {
        e.printStackTrace();
      }
      editor = new JFormattedTextField();
      
      ((JTextField)editor).setBorder(LookAndFeelTweaks.EMPTY_BORDER);
    }
    
    @Override
    public Object getValue() {
      return ((JTextComponent)editor).getText();
    }
    
    @Override
    public void setValue(Object value) {
      if (value == null) {
        ((JTextComponent)editor).setText("");
      } else {
        ((JTextComponent)editor).setText(String.valueOf(value));
      }
    }
    
  }

}

