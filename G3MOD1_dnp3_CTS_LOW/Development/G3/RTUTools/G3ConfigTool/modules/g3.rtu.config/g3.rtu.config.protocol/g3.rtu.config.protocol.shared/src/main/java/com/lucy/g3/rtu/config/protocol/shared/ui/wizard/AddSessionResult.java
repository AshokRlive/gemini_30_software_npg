/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;

class AddSessionResult implements WizardResultProducer {

  private final IProtocolChannel channel;


  public AddSessionResult(IProtocolChannel channel) {
    super();
    this.channel = Preconditions.checkNotNull(channel, "channel must not be null");
  }

  @Override
  public IProtocolSession finish(Map data) throws WizardException {
    String sesnName = (String) data.get(AddSessionPage.KEY_SESSION_NAME);
    IProtocolSession sesn = channel.addSession(sesnName);

    if (Boolean.TRUE == data.get(AddSessionPage.KEY_GEN_IO_MAP)) {
      new GenerateIOMapResult(sesn).finish(data);
    }
    return sesn;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}
