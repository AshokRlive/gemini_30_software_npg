/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared;

import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManagerImpl;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class MasterProtocolPlugin implements IConfigPlugin, IConfigModuleFactory{
  private MasterProtocolPlugin () {}
  public final static MasterProtocolPlugin  INSTANCE = new MasterProtocolPlugin();
  
  public static void init() {
    ConfigFactory.getInstance().registerFactory(MasterProtocolManager.CONFIG_MODULE_ID, INSTANCE);
  }
  
  @Override
  public MasterProtocolManager create(IConfig owner) {
    return new MasterProtocolManagerImpl(owner);
  }
}

