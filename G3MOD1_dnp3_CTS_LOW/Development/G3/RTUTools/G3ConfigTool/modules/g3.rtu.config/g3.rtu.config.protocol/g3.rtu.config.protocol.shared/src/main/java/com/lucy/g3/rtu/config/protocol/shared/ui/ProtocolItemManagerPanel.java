/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui;

import javax.swing.Action;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.lucy.g3.rtu.config.shared.base.page.ItemListManagerPanel;

/**
 * The panel for managing protocol items,such as channels and sessions.
 */
public final class ProtocolItemManagerPanel extends ItemListManagerPanel {

  public ProtocolItemManagerPanel(SelectionInList<?> selectionInList, AbstractTableAdapter<?> listTableModel,
      Action[] pageActions, Action viewAction) {
    super(selectionInList, listTableModel, pageActions, viewAction);

    init();
  }

  public ProtocolItemManagerPanel(SelectionInList<?> selectionInList, Action[] pageActions, Action viewAction) {
    super(selectionInList, pageActions, viewAction);
    init();
  }

  private void init() {
    super.setListItemFormatter(new ProtocolStringFormatter());
  }

}
