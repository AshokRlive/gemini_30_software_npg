/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolConstraints;

/**
 * The constraints of MMB configuration.
 */
public final class MMBConstraints extends ProtocolConstraints {

  // ====== Constraints Prefixes ======
  public static final String PREFIX_SESSION = "session";
  public static final String PREFIX_CHANNEL = "channel";
  public static final String PREFIX_CHANNEL_IO = "channel.io";


  private MMBConstraints() {
    super("MMBConstraints.properties");
  }


  public static final MMBConstraints INSTANCE = new MMBConstraints();
}
