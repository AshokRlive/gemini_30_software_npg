/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.validation;

import com.lucy.g3.rtu.config.channel.validation.AbstractChannelValidator;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The Class MMBChannelValidator.
 */
public class MMBIoChannelValidator extends AbstractChannelValidator {

  private final MMBIoChannel target;


  public MMBIoChannelValidator(MMBIoChannel target) {
    super(target);
    this.target = target;
  }

  @Override
  protected void validate(ValidationResultExt result) {
    // Nothing to validate 
  }

  @Override
  public String getTargetName() {
    return target.getDescription();
  }

  @Override
  public void validate(String parameterName, Object valueObj) throws InvalidConfException {
    super.validate(parameterName, valueObj);

    if (MMBIoChannel.PARAM_REG_ADDRESS.equals(parameterName)
        || MMBIoChannel.PARAM_POLL_RATE.equals(parameterName)) {
      checkNotNegative(parameterName, (Long) valueObj);
    }

  }

}
