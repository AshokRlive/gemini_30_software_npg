/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

/**
 * The page of S104 channel list.
 */
public class S104ChannelManagerPage extends ProtocolChannelManagerPage {

  public S104ChannelManagerPage(S104 data) {
    super(data, new S104ChannelManagerModel(data));
  }


  private static final class S104ChannelManagerModel extends AbstractProtocolItemManagerModel {

    private final S104 iec104;

    private final SelectionInList<S104Channel> selectionInList;


    private S104ChannelManagerModel(S104 iec104) {
      this.iec104 = Preconditions.checkNotNull(iec104, "iec104 is null");
      this.selectionInList = new SelectionInList<S104Channel>(iec104.getItemListModel());
    }

    @Override
    public SelectionInList<?> getSelectionInList() {
      return selectionInList;
    }

    @Override
    protected void addActionPerformed() {
      IProtocolChannel result = ProtocolAddingWizards.showAddChannel(iec104);
      if (result != null)
        selectionInList.setSelection((S104Channel) result);
    }

    @Override
    protected void removeActionPerformed() {
      S104Channel sel = selectionInList.getSelection();

      if (sel != null) {
        if (showConfirmRemoveDialog(sel.getChannelName())) {
          iec104.remove(sel);
        }
      }
    }

  }
}
