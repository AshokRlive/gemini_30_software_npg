/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.domain;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.g3schema.ns_s104.S104ChannelT;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Channel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * The channel of S104.
 */
public class S104Channel extends IEC870Channel<S104Session> {

  private Logger log = Logger.getLogger(S104Channel.class);

  private final S104ChannelConf channelSettings;

  private final S104Redundancy redundancy;

  public S104Channel(S104 iec104, String channelName) {
    super(iec104);
    channelSettings = new S104ChannelConf(this, channelName);
    redundancy = new S104Redundancy();
  }

  @Override
  public S104ChannelConf getChannelConfig() {
    return channelSettings;
  }
  
  public S104Redundancy getRedundancy() {
    return redundancy;
  }

  @Override
  public S104Session addSession(String sessionName) {
    S104Session newSession = new S104Session(this, sessionName);
    if (super.add(newSession)) {
      return newSession;
    } else {
      log.error("Failed to add a new session:" + sessionName);
      return null;
    }
  }

  public void writeToXML(S104ChannelT xml_channel) {
    getChannelConfig().writeToXML(xml_channel.config.append());
    Collection<S104Session> sesns = getAllSessions();
    for (S104Session session : sesns) {
      session.writeToXML(xml_channel.session.append());
    }    
    getRedundancy().writeToXML(xml_channel.redundancy.append());
  }
  
  public void readFromXML(S104ChannelT xmlChannel, ScadaPointSourceRepository sourceRepository) {
    getChannelConfig().readFromXML(xmlChannel.config.first());

    /* Read session */
    int sessionNum = xmlChannel.session.count();
    for (int j = 0; j < sessionNum; j++) {
      addSession("New Session").readFromXML(xmlChannel.session.at(j), sourceRepository);
    }
    
    if(xmlChannel.redundancy.exists())
      getRedundancy().readFromXML(xmlChannel.redundancy.first());
  }

}
