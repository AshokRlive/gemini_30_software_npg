/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr;

import static org.junit.Assert.assertNotNull;

import javax.swing.Action;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.ConnectionManager;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr.ConnectionManagerPanel;

public class ConnectionManagerPanelTest {

  private ConnectionManagerPanel fixture;


  @Before
  public void setup() {
    fixture = new ConnectionManagerPanel(new ConnectionManager(), null);
  }

  @Test
  public void testGetAction() {
    Action action;
    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(fixture);

    assertNotNull(action = actions.get(ConnectionManagerPanel.ACTION_ADD_FAILOVER));
    // action.actionPerformed(null);
    System.out.println(actions.getProxyActions().size());

    assertNotNull(action = actions.get(ConnectionManagerPanel.ACTION_REMOVE_FAILOVER));
    // action.actionPerformed(null);

  }

}
