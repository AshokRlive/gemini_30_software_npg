/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBConstraints;

public class MMBConstraintsTest {

  @Test
  public void testGetMax() {
    assertEquals(4294967295L, MMBConstraints.INSTANCE.getMax(MMBConstraints.PREFIX_CHANNEL_IO, "ipConnectTimeout"));
    assertEquals(1, MMBConstraints.INSTANCE.getMin(MMBConstraints.PREFIX_CHANNEL_IO, "ipConnectTimeout"));
  }

}
