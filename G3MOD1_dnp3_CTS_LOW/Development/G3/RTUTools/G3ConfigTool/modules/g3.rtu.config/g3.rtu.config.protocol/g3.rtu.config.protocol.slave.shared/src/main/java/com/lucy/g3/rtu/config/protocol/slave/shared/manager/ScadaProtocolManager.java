/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.manager;

import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaPointsContainer;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The interface of SCADA Protocol Stack Manager.
 */
public interface ScadaProtocolManager extends IProtocolManager<IScadaProtocol<?>>, IScadaPointsContainer, IConfigModule, IValidation {

  String CONFIG_MODULE_ID = "ScadaProtocolManager";

  /**
   * Gets the repository of SCADA point sources that can be mapped to SCADA
   * points.
   */
  ScadaPointSourceRepository getSourceRepository();

  IScadaProtocol<?> getProtocol(ProtocolType type);
}
