/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

import org.apache.log4j.Logger;

import com.g3schema.ns_pstack.SesnEventConfT;
import com.g3schema.ns_sdnp3.DNP3SesnAddressT;
import com.g3schema.ns_sdnp3.ObjDefaultVariationsT;
import com.g3schema.ns_sdnp3.SDNP3PointsT;
import com.g3schema.ns_sdnp3.SDNP3SesnConfT;
import com.g3schema.ns_sdnp3.SDNP3SesnT;
import com.g3schema.ns_sdnp3.UnsolClassMaxDelaysT;
import com.g3schema.ns_sdnp3.UnsolClassMaxEventsT;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.xml.gen.common.DNP3Enum.EVT_BUF_OVERFLOW_BEHAVIOR;
import com.lucy.g3.xml.gen.common.DNP3Enum.UNSOLICITED_RETRY_MODE;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP1;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP10;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP2;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP20;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP21;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP22;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP23;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP3;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP30;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP32;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP4;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP40;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP42;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_EVENT_MODE;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * The Class SDNP3Session.
 */
public class SDNP3Session extends AbstractProtocolSession implements IScadaProtocolSession {

  public static final String PROPERTY_ADDRESS = "address";
  public static final String PROPERTY_LINKSTATUS_PERIOD = "linkStatusPeriod";
  public static final String PROPERTY_MULTIFRAGRESPALLOWED = "multiFragRespAllowed";
  public static final String PROPERTY_MULTIFRAGCONFIRM = "multiFragConfirm";
  public static final String PROPERTY_RESPONDNEEDTIME = "respondNeedTime";

  public static final String PROPERTY_APPLCONFIRMTIMEOUT = "applConfirmTimeout";
  public static final String PROPERTY_SELECTTIMEOUT = "selectTimeout";
  public static final String PROPERTY_UNSOLALLOWED = "unsolAllowed";

  public static final String PROPERTY_UNSOLCLASSMASK = "unsolClassMask";
  public static final String PROPERTY_CLASS1ENABLED = "class1Enabled";
  public static final String PROPERTY_CLASS2ENABLED = "class2Enabled";
  public static final String PROPERTY_CLASS3ENABLED = "class3Enabled";

  public static final String PROPERTY_UNSOLCLASSMAXEVENTS = "unsolClassMaxEvents";
  public static final String PROPERTY_UNSOLCLASSMAXDELAYS = "unsolClassMaxDelays";
  public static final String PROPERTY_GLOBAL_EVT_VARIATIONS = "globalEvtVariation";
  public static final String PROPERTY_EVENTCONFIG_BI = "eventConfigBI";
  public static final String PROPERTY_EVENTCONFIG_DI = "eventConfigDI";
  public static final String PROPERTY_EVENTCONFIG_AI = "eventConfigAI";
  public static final String PROPERTY_EVENTCONFIG_CT = "eventConfigCT"; // Counter
  public static final String PROPERTY_EVENTCONFIG_FCT = "eventConfigFCT"; // FrozenCounter
  
  public static final String PROPERTY_EVT_BUF_OVERFLOW_BEHAVIOR = "evtBufOverflowBehavior"; 
  

  public static final String PROPERTY_SELFADDRESS_ENABLED = "selfAddressEnabled";

  public static final String PROPERTY_VALIDATE_SRC_ADDRESS = "validateSourceAddress";

  public static final String PROPERTY_ALLOW_CTRL_ON_SELFADDRESS = "allowControlsOnSelfAddress";
  
  public static final String PROPERTY_IDENTICAL_RETRIES = "identicalRetriesAllowed";
  

  // Unso class bit mask
  private static final int UNSOLCLASSMASK_BIT0 = 1;
  private static final int UNSOLCLASSMASK_BIT1 = 1 << 1;
  private static final int UNSOLCLASSMASK_BIT2 = 1 << 2;

  private Logger log = Logger.getLogger(SDNP3Session.class);

  private final SDNP3IoMap iomap = new SDNP3IoMap(this);

  private static SDNP3Constraints cons = SDNP3Constraints.INSTANCE;

  private final String prefix = SDNP3Constraints.PREFIX_SESSION;

  // ====== Read Write Properties ======
  private long linkStatusPeriod = cons.getDefault(prefix, "linkStatusPeriod");
  private boolean multiFragRespAllowed = true;
  private boolean multiFragConfirm = false;
  private boolean respondNeedTime = true;

  private long applConfirmTimeout = cons.getDefault(prefix, "applConfirmTimeout");
  private long selectTimeout = cons.getDefault(prefix, "selectTimeout");
  private boolean unsolAllowed = true;
  private boolean identicalRetriesAllowed = false;

  private long unsolClassMask = cons.getDefault(prefix, "unsolClassMask");
  private boolean class1Enabled; // unsolClassMask bit 0
  private boolean class2Enabled; // unsolClassMask bit 1
  private boolean class3Enabled; // unsolClassMask bit 2


  private final Address address = new Address();
  private final UnsolClassMaxEvents unsolClassMaxEvents = new UnsolClassMaxEvents();
  private final UnsolClassMaxDelays unsolClassMaxDelays = new UnsolClassMaxDelays();
  private final UnsolRetry          unsolRetry          = new UnsolRetry();

  private final GlobalVariationsConfig globalEvtVariation = new GlobalVariationsConfig();
  private final EventConfig eventConfigBI = new EventConfig();
  private final EventConfig eventConfigDI = new EventConfig();
  private final EventConfig eventConfigAI = new EventConfig();
  private final EventConfig eventConfigAO = new EventConfig();
  private final EventConfig eventConfigCT = new EventConfig();
  private final EventConfig eventConfigFCT = new EventConfig();

  private boolean selfAddressEnabled = false;
  private boolean validateSourceAddress = true;
  private boolean allowControlsOnSelfAddress = false;
  private EVT_BUF_OVERFLOW_BEHAVIOR evtBufOverflowBehavior =
      EVT_BUF_OVERFLOW_BEHAVIOR.EVT_BUF_OVERFLOW_BEHAVIOR_DEL_OLDEST;

  private final SDNP3Channel channel;

  private final SDNPAuthConfig authConfig = new SDNPAuthConfig();
  
  SDNP3Session(SDNP3Channel channel) {
    this(channel, null);
  }

  // Constructor
  SDNP3Session(SDNP3Channel channel, String sessionName) {
    super(channel, sessionName);
    this.channel = Preconditions.checkNotNull(channel, "channel is null");
    try {
      setUnsolClassMask(cons.getDefault(prefix, "unsolClassMask"));
    } catch (PropertyVetoException e) {
      log.error(e);
    }

    LoggingUtil.logPropertyChanges(this);
  }

  @Override
  public SDNP3IoMap getIomap() {
    return iomap;
  }

  @Override
  public void delete() {
    iomap.clearIoMap();
  }
  
  public SDNPAuthConfig getAuthConfig() {
    return authConfig;
  }

  public EventConfig getEventConfigBI() {
    return eventConfigBI;
  }

  public EventConfig getEventConfigDI() {
    return eventConfigDI;
  }

  public EventConfig getEventConfigAI() {
    return eventConfigAI;
  }
  
  public EventConfig getEventConfigAO() {
    return eventConfigAO;
  }

  public EventConfig getEventConfigCT() {
    return eventConfigCT;
  }

  public EventConfig getEventConfigFCT() {
    return eventConfigFCT;
  }

  
  public EVT_BUF_OVERFLOW_BEHAVIOR getEvtBufOverflowBehavior() {
    return evtBufOverflowBehavior;
  }

  
  public void setEvtBufOverflowBehavior(EVT_BUF_OVERFLOW_BEHAVIOR evtBufOverflowBehavior) {
    if (evtBufOverflowBehavior == null)
      return;
    
    Object oldValue = this.evtBufOverflowBehavior;
    this.evtBufOverflowBehavior = evtBufOverflowBehavior;
    firePropertyChange(PROPERTY_EVT_BUF_OVERFLOW_BEHAVIOR, oldValue, evtBufOverflowBehavior);
  }

  public GlobalVariationsConfig getGlobalEvtVariation() {
    return globalEvtVariation;
  }

  public UnsolClassMaxEvents getUnsolClassMaxEvents() {
    return unsolClassMaxEvents;
  }

  public UnsolClassMaxDelays getUnsolClassMaxDelays() {
    return unsolClassMaxDelays;
  }

  
  public UnsolRetry getUnsolRetry() {
    return unsolRetry;
  }

  public Address getAddress() {
    return address;
  }

  public long getLinkStatusPeriod() {
    return linkStatusPeriod;
  }

  public void setLinkStatusPeriod(long linkStatusPeriod) throws PropertyVetoException {
    checkBoundary(this.linkStatusPeriod, linkStatusPeriod, prefix, "linkStatusPeriod");

    Object oldValue = getLinkStatusPeriod();
    this.linkStatusPeriod = linkStatusPeriod;
    firePropertyChange(PROPERTY_LINKSTATUS_PERIOD, oldValue, linkStatusPeriod);

  }

  public boolean getMultiFragRespAllowed() {
    return multiFragRespAllowed;
  }

  public void setMultiFragRespAllowed(boolean multiFragRespAllowed) {
    Object oldValue = getMultiFragRespAllowed();
    this.multiFragRespAllowed = multiFragRespAllowed;
    firePropertyChange(PROPERTY_MULTIFRAGRESPALLOWED, oldValue, multiFragRespAllowed);

  }

  public boolean getMultiFragConfirm() {
    return multiFragConfirm;
  }

  public void setMultiFragConfirm(boolean multiFragConfirm) {
    Object oldValue = getMultiFragConfirm();
    this.multiFragConfirm = multiFragConfirm;
    firePropertyChange(PROPERTY_MULTIFRAGCONFIRM, oldValue, multiFragConfirm);

  }

  
  public boolean isAllowControlsOnSelfAddress() {
    return allowControlsOnSelfAddress;
  }
  
  public void setAllowControlsOnSelfAddress(boolean allowControlsOnSelfAddress) {
    Object oldValue = this.allowControlsOnSelfAddress;
    this.allowControlsOnSelfAddress = allowControlsOnSelfAddress;
    firePropertyChange(PROPERTY_ALLOW_CTRL_ON_SELFADDRESS, oldValue, allowControlsOnSelfAddress);
  }

  public boolean getRespondNeedTime() {
    return respondNeedTime;
  }

  public void setRespondNeedTime(boolean respondNeedTime) {
    Object oldValue = getRespondNeedTime();
    this.respondNeedTime = respondNeedTime;
    firePropertyChange(PROPERTY_RESPONDNEEDTIME, oldValue, respondNeedTime);
  }

  public long getApplConfirmTimeout() {
    return applConfirmTimeout;
  }

  public void setApplConfirmTimeout(long applConfirmTimeout) throws PropertyVetoException {
    checkBoundary(this.applConfirmTimeout, applConfirmTimeout, prefix, "applConfirmTimeout");

    Object oldValue = getApplConfirmTimeout();
    this.applConfirmTimeout = applConfirmTimeout;
    firePropertyChange(PROPERTY_APPLCONFIRMTIMEOUT, oldValue, applConfirmTimeout);
  }
  
  
  public long getSelectTimeout() {
    return selectTimeout;
  }

  public void setSelectTimeout(long selectTimeout) throws PropertyVetoException {
    checkBoundary(this.selectTimeout, selectTimeout, prefix, "selectTimeout");

    Object oldValue = getSelectTimeout();
    this.selectTimeout = selectTimeout;
    firePropertyChange(PROPERTY_SELECTTIMEOUT, oldValue, applConfirmTimeout);
  }

  public boolean getUnsolAllowed() {
    return unsolAllowed;
  }

  public void setUnsolAllowed(boolean unsolAllowed) {
    Object oldValue = getUnsolAllowed();
    this.unsolAllowed = unsolAllowed;
    firePropertyChange(PROPERTY_UNSOLALLOWED, oldValue, unsolAllowed);
  }
  
  public boolean getIdenticalRetriesAllowed() {
	   return identicalRetriesAllowed;
 }

 public void setIdenticalRetriesAllowed(boolean identicalRetriesAllowed) {
   Object oldValue = getIdenticalRetriesAllowed();
   this.identicalRetriesAllowed = identicalRetriesAllowed;
   firePropertyChange(PROPERTY_IDENTICAL_RETRIES, oldValue, identicalRetriesAllowed);
 }

  public long getUnsolClassMask() {
    return unsolClassMask;
  }

  public void setUnsolClassMask(long unsolClassMask) throws PropertyVetoException {
    setUnsolClassMask(unsolClassMask, true);
  }

  private void setUnsolClassMask(long unsolClassMask, boolean updateMask) throws PropertyVetoException {
    checkBoundary(this.unsolClassMask, unsolClassMask, prefix, "unsolClassMask");

    Object oldValue = getUnsolClassMask();
    this.unsolClassMask = unsolClassMask;
    firePropertyChange(PROPERTY_UNSOLCLASSMASK, oldValue, unsolClassMask);

    if (updateMask) {
      updateClassEnable();
    }
  }

  public boolean isClass1Enabled() {
    return class1Enabled;
  }

  public void setClass1Enabled(boolean class1Enabled) {
    setClass1Enabled(class1Enabled, true);
  }

  private void setClass1Enabled(boolean class1Enabled, boolean updateMask) {
    Object oldValue = isClass1Enabled();
    this.class1Enabled = class1Enabled;
    firePropertyChange(PROPERTY_CLASS1ENABLED, oldValue, class1Enabled);

    if (updateMask) {
      updateUnsolClassMask();
    }
  }

  public boolean isClass2Enabled() {
    return class2Enabled;
  }

  public void setClass2Enabled(boolean class2Enabled) {
    setClass2Enabled(class2Enabled, true);
  }

  private void setClass2Enabled(boolean class2Enabled, boolean updateMask) {
    Object oldValue = isClass2Enabled();
    this.class2Enabled = class2Enabled;
    firePropertyChange(PROPERTY_CLASS2ENABLED, oldValue, class2Enabled);

    if (updateMask) {
      updateUnsolClassMask();
    }
  }

  public boolean isClass3Enabled() {
    return class3Enabled;
  }

  public void setClass3Enabled(boolean class3Enabled) {
    setClass3Enabled(class3Enabled, true);
  }

  private void setClass3Enabled(boolean class3Enabled, boolean updateMask) {
    Object oldValue = isClass3Enabled();
    this.class3Enabled = class3Enabled;
    firePropertyChange(PROPERTY_CLASS3ENABLED, oldValue, class3Enabled);

    if (updateMask) {
      updateUnsolClassMask();
    }
  }

  private void updateClassEnable() {
    setClass1Enabled((unsolClassMask & UNSOLCLASSMASK_BIT0) != 0, false);
    setClass2Enabled((unsolClassMask & UNSOLCLASSMASK_BIT1) != 0, false);
    setClass3Enabled((unsolClassMask & UNSOLCLASSMASK_BIT2) != 0, false);
  }

  private void updateUnsolClassMask() {
    int newMask = 0;
    if (class1Enabled) {
      newMask = newMask | UNSOLCLASSMASK_BIT0;
    }

    if (class2Enabled) {
      newMask = newMask | UNSOLCLASSMASK_BIT1;
    }

    if (class3Enabled) {
      newMask = newMask | UNSOLCLASSMASK_BIT2;
    }

    try {
      setUnsolClassMask(newMask, false);
    } catch (PropertyVetoException e) {
      log.warn("Fail to update UnsolClassMask:" + e.getMessage());
    }
  }

  public boolean isSelfAddressEnabled() {
    return selfAddressEnabled;
  }

  public void setSelfAddressEnabled(boolean selfAddressEnabled) {
    Object oldValue = isSelfAddressEnabled();
    this.selfAddressEnabled = selfAddressEnabled;
    firePropertyChange(PROPERTY_SELFADDRESS_ENABLED, oldValue, selfAddressEnabled);
  }

  public boolean isValidateSourceAddress() {
    return validateSourceAddress;
  }

  public void setValidateSourceAddress(boolean validateSourceAddress) {
    Object oldValue = isValidateSourceAddress();
    this.validateSourceAddress = validateSourceAddress;
    firePropertyChange(PROPERTY_VALIDATE_SRC_ADDRESS, oldValue, validateSourceAddress);
  }

  private void checkBoundary(long oldValue, long newValue, String prefix, String property)
      throws PropertyVetoException {
    checkBoundary(this, oldValue, newValue, prefix, property);
  }

  private static void checkBoundary(Model bean, long oldValue, long newValue, String prefix, String property)
      throws PropertyVetoException {
    cons.checkPropertyBoundary(bean, oldValue, newValue, prefix, property);
  }

  @Override
  public SDNP3Channel getProtocolChannel() {
    return channel;
  }

  public void writeToXML(SDNP3SesnT xmlSesn) {
    SDNP3SesnConfT xml = xmlSesn.config.append();
    
    xml.enabled.setValue(this.isEnabled());
    xml.uuid.setValue(this.getUUID());
    
    xml.sessionName.setValue(this.getSessionName());
    xml.selfAddress.setValue(this.isSelfAddressEnabled());
    xml.allowControlsOnSelfAddress.setValue(this.isAllowControlsOnSelfAddress());
    xml.validateSourceAddress.setValue(this.isValidateSourceAddress());

    DNP3SesnAddressT address = xml.address.append();
    address.source.setValue(this.getAddress().getSource());
    address.destination.setValue(this.getAddress().getDestination());
    xml.linkStatusPeriod.setValue(this.getLinkStatusPeriod());
    xml.multiFragRespAllowed.setValue(this.getMultiFragRespAllowed());
    xml.multiFragConfirm.setValue(this.getMultiFragConfirm());
    xml.respondNeedTime.setValue(this.getRespondNeedTime());
    xml.applConfirmTimeout.setValue(this.getApplConfirmTimeout());
    xml.selectTimeout.setValue(this.getSelectTimeout());
    xml.unsolAllowed.setValue(this.getUnsolAllowed());
    xml.identicalRetriesAllowed.setValue(this.getIdenticalRetriesAllowed());
    xml.unsolClassMask.setValue(this.getUnsolClassMask());
    UnsolClassMaxEventsT events = xml.unsolClassMaxEvents.append();
    events.class1.setValue(this.getUnsolClassMaxEvents().getClass1());
    events.class2.setValue(this.getUnsolClassMaxEvents().getClass2());
    events.class3.setValue(this.getUnsolClassMaxEvents().getClass3());
    UnsolClassMaxDelaysT delays = xml.unsolClassMaxDelays.append();
    delays.class1.setValue(this.getUnsolClassMaxDelays().getClass1());
    delays.class2.setValue(this.getUnsolClassMaxDelays().getClass2());
    delays.class3.setValue(this.getUnsolClassMaxDelays().getClass3());
    xml.unsolConfirmTimeout.setValue(this.unsolRetry.getUnsolConfirmTimeout());
    xml.unsolRetryMode.setValue(this.unsolRetry.getMode().getValue());
    xml.unsolMaxRetries.setValue(this.unsolRetry.getUnsolMaxRetries());
    xml.unsolRetryDelay.setValue(this.unsolRetry.getUnsolRetryDelay());
    xml.unsolOfflineRetryDelay.setValue(this.unsolRetry.getUnsolOfflineRetryDelay());
    ObjDefaultVariationsT objDefVar = xml.objDefaultVariations.append();
    GlobalVariationsConfig globalVar = this.getGlobalEvtVariation();
    objDefVar.obj01.setValue(globalVar.getObj01().getValue());
    objDefVar.obj02.setValue(globalVar.getObj02().getValue());
    objDefVar.obj03.setValue(globalVar.getObj03().getValue());
    objDefVar.obj04.setValue(globalVar.getObj04().getValue());
    objDefVar.obj30.setValue(globalVar.getObj30().getValue());
    objDefVar.obj32.setValue(globalVar.getObj32().getValue());
    objDefVar.obj20.setValue(globalVar.getObj20().getValue());
    objDefVar.obj22.setValue(globalVar.getObj22().getValue());
    objDefVar.obj21.setValue(globalVar.getObj21().getValue());
    objDefVar.obj23.setValue(globalVar.getObj23().getValue());
    objDefVar.obj40.setValue(globalVar.getObj40().getValue());
    objDefVar.obj42.setValue(globalVar.getObj42().getValue());

    /* Events */
    getEventConfigBI().writeToXML(xml.binaryInputEvent.append());
    getEventConfigDI().writeToXML(xml.doubleInputEvent.append());
    getEventConfigAI().writeToXML(xml.analogInputEvent.append());
    getEventConfigAO().writeToXML(xml.analogOutputEvent.append());
    getEventConfigCT().writeToXML(xml.counterEvent.append());
    getEventConfigFCT().writeToXML(xml.frozenCounterEvent.append());
    
    xml.evtBufOverflowBehaviour.setValue(getEvtBufOverflowBehavior().getValue());
  
    // Write authentication
    getAuthConfig().writeToXML(xml.authentication.append());
    
    // Write session IO map
    getIomap().writeToXML(xmlSesn.iomap.append());
  }
  
  public void readFromXML(SDNP3SesnT xmlSession, ScadaPointSourceRepository resource) throws PropertyVetoException {
    SDNP3SesnConfT xml = xmlSession.config.first();
    
    if (xml.enabled.exists())
      this.setEnabled(xml.enabled.getValue());
    
    if (xml.uuid.exists())
      setUUID(xml.uuid.getValue());
    
    this.getAddress().setDestination(
        xml.address.first().destination.getValue());
    this.getAddress().setSource(
        xml.address.first().source.getValue());
    this.setLinkStatusPeriod(xml.linkStatusPeriod.getValue());
    this.setMultiFragRespAllowed(xml.multiFragRespAllowed
        .getValue());
    this.setMultiFragConfirm(xml.multiFragConfirm.getValue());
    this.setRespondNeedTime(xml.respondNeedTime.getValue());
    this.setApplConfirmTimeout(xml.applConfirmTimeout.getValue());
    this.setSelectTimeout(xml.selectTimeout.getValue());
    this.setUnsolAllowed(xml.unsolAllowed.getValue());
    this.setUnsolClassMask(xml.unsolClassMask.getValue());
    this.getUnsolClassMaxEvents().setClass1(
        xml.unsolClassMaxEvents.first().class1.getValue());
    this.getUnsolClassMaxEvents().setClass2(
        xml.unsolClassMaxEvents.first().class2.getValue());
    this.getUnsolClassMaxEvents().setClass3(
        xml.unsolClassMaxEvents.first().class3.getValue());
    this.getUnsolClassMaxDelays().setClass1(
        xml.unsolClassMaxDelays.first().class1.getValue());
    this.getUnsolClassMaxDelays().setClass2(
        xml.unsolClassMaxDelays.first().class2.getValue());
    this.getUnsolClassMaxDelays().setClass3(
        xml.unsolClassMaxDelays.first().class3.getValue());
    
    if (xml.unsolRetryMode.exists()) {
      this.unsolRetry.setMode(UNSOLICITED_RETRY_MODE.forValue(xml.unsolRetryMode.getValue()));
    } else {
      this.unsolRetry.setMode(UNSOLICITED_RETRY_MODE.UNSOLICITED_RETRY_MODE_WITH_OFFLINE);
    }
    
    if (xml.unsolConfirmTimeout.exists())
      this.unsolRetry.setUnsolConfirmTimeout(xml.unsolConfirmTimeout.getValue());
    this.unsolRetry.setUnsolMaxRetries(xml.unsolMaxRetries.getValue());
    this.unsolRetry.setUnsolRetryDelay(xml.unsolRetryDelay.getValue());
    this.unsolRetry.setUnsolOfflineRetryDelay(xml.unsolOfflineRetryDelay.getValue());

    GlobalVariationsConfig defVar = this.getGlobalEvtVariation();
    ObjDefaultVariationsT xmlObjVar = xml.objDefaultVariations.first();
    defVar.setObj01(VARIATION_GROUP1.forValue((int)  xmlObjVar.obj01.getValue()));
    defVar.setObj02(VARIATION_GROUP2.forValue((int)  xmlObjVar.obj02.getValue()));
    defVar.setObj03(VARIATION_GROUP3.forValue((int)  xmlObjVar.obj03.getValue()));
    defVar.setObj04(VARIATION_GROUP4.forValue((int)  xmlObjVar.obj04.getValue()));
    defVar.setObj30(VARIATION_GROUP30.forValue((int) xmlObjVar.obj30.getValue()));
    defVar.setObj32(VARIATION_GROUP32.forValue((int) xmlObjVar.obj32.getValue()));
    defVar.setObj20(VARIATION_GROUP20.forValue((int) xmlObjVar.obj20.getValue()));
    defVar.setObj22(VARIATION_GROUP22.forValue((int) xmlObjVar.obj22.getValue()));
    defVar.setObj21(VARIATION_GROUP21.forValue((int) xmlObjVar.obj21.getValue()));
    defVar.setObj23(VARIATION_GROUP23.forValue((int) xmlObjVar.obj23.getValue()));
    
    if(xmlObjVar.obj40.exists())
      defVar.setObj40(VARIATION_GROUP40.forValue((int) xmlObjVar.obj40.getValue()));
    if(xmlObjVar.obj42.exists())
      defVar.setObj42(VARIATION_GROUP42.forValue((int) xmlObjVar.obj42.getValue()));

    EventConfig ecfg;

    ecfg = this.getEventConfigBI();
    ecfg.setMaxEvents(xml.binaryInputEvent.first().maxEvents.getValue());
    ecfg.setEventMode(LU_EVENT_MODE.valueOf(xml.binaryInputEvent.first().eventMode.getValue()));

    ecfg = this.getEventConfigDI();
    ecfg.setMaxEvents(xml.doubleInputEvent.first().maxEvents.getValue());
    ecfg.setEventMode(LU_EVENT_MODE.valueOf(xml.doubleInputEvent.first().eventMode.getValue()));

    ecfg = this.getEventConfigAI();
    ecfg.setMaxEvents(xml.analogInputEvent.first().maxEvents.getValue());
    ecfg.setEventMode(LU_EVENT_MODE.valueOf(xml.analogInputEvent.first().eventMode.getValue()));
    
    if(xml.analogOutputEvent.exists()) {
      ecfg = this.getEventConfigAO();
      ecfg.setMaxEvents(xml.analogOutputEvent.first().maxEvents.getValue());
      ecfg.setEventMode(LU_EVENT_MODE.valueOf(xml.analogOutputEvent.first().eventMode.getValue()));
    }

    if (xml.counterEvent.exists()) {
      ecfg = this.getEventConfigCT();
      ecfg.setMaxEvents(xml.counterEvent.first().maxEvents.getValue());
      ecfg.setEventMode(LU_EVENT_MODE.valueOf(xml.counterEvent.first().eventMode.getValue()));
    }

    if (xml.frozenCounterEvent.exists()) {
      ecfg = this.getEventConfigFCT();
      ecfg.setMaxEvents(xml.frozenCounterEvent.first().maxEvents.getValue());
      ecfg.setEventMode(LU_EVENT_MODE.valueOf(xml.frozenCounterEvent.first().eventMode.getValue()));
    }

    this.setSelfAddressEnabled(xml.selfAddress.getValue());
    if(xml.allowControlsOnSelfAddress.exists())
      this.setAllowControlsOnSelfAddress(xml.allowControlsOnSelfAddress.getValue());
    this.setValidateSourceAddress(xml.validateSourceAddress.getValue());
    
    if(xml.identicalRetriesAllowed.exists())
    	this.setIdenticalRetriesAllowed(xml.identicalRetriesAllowed.getValue());
    
    if(xml.evtBufOverflowBehaviour.exists()) {
      this.setEvtBufOverflowBehavior(
          EVT_BUF_OVERFLOW_BEHAVIOR.forValue(xml.evtBufOverflowBehaviour.getValue()));
    }
  
    // Read authentication
    if(xml.authentication.exists())
      getAuthConfig().readFromXML(xml.authentication.first());
    
    SDNP3PointsT xml_sesn_iomap = xmlSession.iomap.first();
    getIomap().readFromXML(xml_sesn_iomap, resource);

  }
  
  public static class Address extends Model {

    public static final String PROPERTY_DEST = "destination";
    public static final String PROPERTY_SOURCE = "source";

    private final String prefix = SDNP3Constraints.PREFIX_SESSION_ADDRESS;

    private long destination = cons.getDefault(SDNP3Constraints.PREFIX_SESSION_ADDRESS, PROPERTY_DEST);
    private long source = cons.getDefault(SDNP3Constraints.PREFIX_SESSION_ADDRESS, PROPERTY_SOURCE);


    Address() {
      LoggingUtil.logPropertyChanges(this);
    }

    public long getSource() {
      return source;
    }

    public void setSource(long source) throws PropertyVetoException {
      checkBoundary(this, this.source, source, prefix, PROPERTY_SOURCE);

      Object oldValue = getSource();
      this.source = source;
      firePropertyChange("source", oldValue, source);
    }

    public long getDestination() {
      return destination;
    }

    public void setDestination(long destination) throws PropertyVetoException {
      checkBoundary(this, this.destination, destination, prefix, PROPERTY_DEST);

      Object oldValue = getDestination();
      this.destination = destination;
      firePropertyChange("destination", oldValue, destination);
    }
  }

  public static class UnsolRetry extends Model {
    
    public static final String LABEL_OFFLINE_RETRY_DELAY = "Offline Retry Delay: ";
    public static final String LABEL_UNSOLICITED_CONFIRM_TIMEOUT = "Unsolicited Confirm Timeout: ";
    public static final String LABEL_MAXIMUM_RETRY_TIMES = "Maximum Retry Times: ";
    public static final String LABEL_RETRY_DELAY = "Retry Delay: ";
    public static final String LABEL_MS = " ms";
    
    
    public static final String PROPERTY_UNSOLCONFIRMTIMEOUT = "unsolConfirmTimeout";
    public static final String PROPERTY_UNSOLMAXRETRIES = "unsolMaxRetries";
    public static final String PROPERTY_UNSOLRETRYDELAY = "unsolRetryDelay";
    public static final String PROPERTY_UNSOLOFFLINERETRYDELAY = "unsolOfflineRetryDelay";
    public static final String PROPERTY_MODE = "mode";
    public static final String PROPERTY_SUMMARY = "summary";
    
    private final String prefix = SDNP3Constraints.PREFIX_SESSION;
    private long unsolConfirmTimeout = cons.getDefault(prefix, "unsolConfirmTimeout");
    private long unsolMaxRetries = cons.getDefault(prefix, "unsolMaxRetries");
    private long unsolRetryDelay = cons.getDefault(prefix, "unsolRetryDelay");
    private long unsolOfflineRetryDelay = cons.getDefault(prefix, "unsolOfflineRetryDelay");
    
    private UNSOLICITED_RETRY_MODE mode = UNSOLICITED_RETRY_MODE.UNSOLICITED_RETRY_MODE_NO_RETRY;
    private String summary;
    
    public UnsolRetry() {
      // Updating the summary once property changes
      addPropertyChangeListener(new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          if (!PROPERTY_SUMMARY.equals(evt.getPropertyName()))
            updateSummary();
        }
      });
      updateSummary();
    }
    
    public String getSummary() {
      return summary;
    }

    private void updateSummary() {
      StringBuilder sb = new StringBuilder();
      sb.append("Mode: ");
      sb.append(mode.getDescription());
      
      switch (mode) {
      case UNSOLICITED_RETRY_MODE_NO_RETRY:
        break;
        
      case UNSOLICITED_RETRY_MODE_SIMPLE_RETRY:
        appendRetryTimes(sb);
        appendRetryDelay(sb);
        appendConfirmTimeout(sb);
        break;
        
      case UNSOLICITED_RETRY_MODE_WITH_OFFLINE:
        appendRetryTimes(sb);
        appendRetryDelay(sb);
        appendConfirmTimeout(sb);
        appendOfflineDelay(sb);
        break;
        
      case UNSOLICITED_RETRY_MODE_UNLIMITED:
        appendConfirmTimeout(sb);
        appendRetryDelay(sb);
        break;
        
      default:
        break;
      }
      
      setSummary(sb.toString());
    }

    private void appendOfflineDelay(StringBuilder sb) {
      sb.append("\n");
      sb.append(LABEL_OFFLINE_RETRY_DELAY);
      sb.append(getUnsolOfflineRetryDelay());
      sb.append(LABEL_MS);
    }

    private void appendConfirmTimeout(StringBuilder sb) {
      sb.append("\n");
      sb.append(LABEL_UNSOLICITED_CONFIRM_TIMEOUT);
      sb.append(getUnsolConfirmTimeout());
      sb.append(LABEL_MS);
    }

    private void appendRetryTimes(StringBuilder sb) {
      sb.append("\n");
      sb.append(LABEL_MAXIMUM_RETRY_TIMES);
      sb.append(getUnsolMaxRetries());
    }

    private void appendRetryDelay(StringBuilder sb) {
      sb.append("\n");
      sb.append(LABEL_RETRY_DELAY);
      sb.append(getUnsolRetryDelay());
      sb.append(LABEL_MS);
    }
    
    private void setSummary(String summary) {
      Object oldValue = this.summary;
      this.summary = summary;
      firePropertyChange(PROPERTY_SUMMARY, oldValue, summary);
    }
    
    
    public UNSOLICITED_RETRY_MODE getMode() {
      return mode;
    }
    
    public void setMode(UNSOLICITED_RETRY_MODE mode) {
      if (mode != null) {
        Object oldValue = this.mode;
        this.mode = mode;
        firePropertyChange(PROPERTY_MODE, oldValue, mode);
      }
    }

    public long getUnsolMaxRetries() {
      return unsolMaxRetries;
    }

    public long getUnsolConfirmTimeout() {
      return unsolConfirmTimeout;
    }

    public void setUnsolConfirmTimeout(long unsolConfirmTimeout) {
      Object oldValue = this.unsolConfirmTimeout;
      this.unsolConfirmTimeout = unsolConfirmTimeout;
      firePropertyChange(PROPERTY_UNSOLCONFIRMTIMEOUT, oldValue, unsolConfirmTimeout);
    }
    
    public void setUnsolMaxRetries(long unsolMaxRetries) throws PropertyVetoException {
      checkBoundary(this, this.unsolMaxRetries, unsolMaxRetries, prefix, "unsolMaxRetries");

      Object oldValue = getUnsolMaxRetries();
      this.unsolMaxRetries = unsolMaxRetries;
      firePropertyChange(PROPERTY_UNSOLMAXRETRIES, oldValue, unsolMaxRetries);
    }

    public long getUnsolRetryDelay() {
      return unsolRetryDelay;
    }

    public void setUnsolRetryDelay(long unsolRetryDelay) throws PropertyVetoException {
      checkBoundary(this, this.unsolRetryDelay, unsolRetryDelay, prefix, "unsolRetryDelay");

      Object oldValue = getUnsolRetryDelay();
      this.unsolRetryDelay = unsolRetryDelay;
      firePropertyChange(PROPERTY_UNSOLRETRYDELAY, oldValue, unsolRetryDelay);
    }

    public long getUnsolOfflineRetryDelay() {
      return unsolOfflineRetryDelay;
    }

    public void setUnsolOfflineRetryDelay(long unsolOfflineRetryDelay) throws PropertyVetoException {
      checkBoundary(this, this.unsolOfflineRetryDelay, unsolOfflineRetryDelay, prefix, "unsolOfflineRetryDelay");

      Object oldValue = getUnsolOfflineRetryDelay();
      this.unsolOfflineRetryDelay = unsolOfflineRetryDelay;
      firePropertyChange(PROPERTY_UNSOLOFFLINERETRYDELAY, oldValue, unsolOfflineRetryDelay);
    }
  }
  
  public static class UnsolClassMaxEvents extends Model {

    public static final String PROPERTY_CLASS1 = "class1";
    public static final String PROPERTY_CLASS2 = "class2";
    public static final String PROPERTY_CLASS3 = "class3";

    private long class1;
    private long class2;
    private long class3;

    private final String prefix = SDNP3Constraints.PREFIX_SESSION_UNSOLICATED_MAX_EVENTS;


    UnsolClassMaxEvents() {

      // Set defaults values
      class1 = cons.getDefault(prefix, PROPERTY_CLASS1);
      class2 = cons.getDefault(prefix, PROPERTY_CLASS2);
      class3 = cons.getDefault(prefix, PROPERTY_CLASS3);

      LoggingUtil.logPropertyChanges(this);
    }

    public long getClass1() {
      return class1;
    }

    public void setClass1(long class1) throws PropertyVetoException {
      checkBoundary(this, this.class1, class1, prefix, PROPERTY_CLASS1);

      Object oldValue = getClass1();
      this.class1 = class1;
      firePropertyChange("class1", oldValue, class1);

    }

    public long getClass2() {
      return class2;
    }

    public void setClass2(long class2) throws PropertyVetoException {
      checkBoundary(this, this.class2, class2, prefix, PROPERTY_CLASS2);

      Object oldValue = getClass2();
      this.class2 = class2;
      firePropertyChange("class2", oldValue, class2);

    }

    public long getClass3() {
      return class3;
    }

    public void setClass3(long class3) throws PropertyVetoException {
      checkBoundary(this, this.class3, class3, prefix, PROPERTY_CLASS3);

      Object oldValue = getClass3();
      this.class3 = class3;
      firePropertyChange("class3", oldValue, class3);

    }
  }

  public static class UnsolClassMaxDelays extends Model {

    public static final String PROPERTY_CLASS1 = "class1";
    public static final String PROPERTY_CLASS2 = "class2";
    public static final String PROPERTY_CLASS3 = "class3";

    private long class1;
    private long class2;
    private long class3;

    private final String prefix = SDNP3Constraints.PREFIX_SESSION_UNSOLICATED_MAX_DELAYS;


    UnsolClassMaxDelays() {

      // Set defaults values
      class1 = cons.getDefault(prefix, PROPERTY_CLASS1);
      class2 = cons.getDefault(prefix, PROPERTY_CLASS2);
      class3 = cons.getDefault(prefix, PROPERTY_CLASS3);

      LoggingUtil.logPropertyChanges(this);
    }

    public long getClass1() {
      return class1;
    }

    public void setClass1(long class1) throws PropertyVetoException {
      checkBoundary(this, this.class1, class1, prefix, PROPERTY_CLASS1);

      Object oldValue = getClass1();
      this.class1 = class1;
      firePropertyChange("class1", oldValue, class1);

    }

    public long getClass2() {
      return class2;
    }

    public void setClass2(long class2) throws PropertyVetoException {
      checkBoundary(this, this.class2, class2, prefix, PROPERTY_CLASS2);

      Object oldValue = getClass2();
      this.class2 = class2;
      firePropertyChange("class2", oldValue, class2);

    }

    public long getClass3() {
      return class3;
    }

    public void setClass3(long class3) throws PropertyVetoException {
      checkBoundary(this, this.class3, class3, prefix, PROPERTY_CLASS3);

      Object oldValue = getClass3();
      this.class3 = class3;
      firePropertyChange("class3", oldValue, class3);

    }

  }

  /**
   * Variation configuration at session level.
   */
  public static class GlobalVariationsConfig extends Model {

    // @formatter:off
    public static final String PROPERTY_OBJ01 = "obj01";// Binary   STATIC
    public static final String PROPERTY_OBJ02 = "obj02";// Binary   EVENT
    public static final String PROPERTY_OBJ03 = "obj03";// Double   STATIC
    public static final String PROPERTY_OBJ04 = "obj04";// Double   EVENT
    public static final String PROPERTY_OBJ30 = "obj30";// Analogue STATIC
    public static final String PROPERTY_OBJ32 = "obj32";// Analogue EVENT
    public static final String PROPERTY_OBJ20 = "obj20";// Counter  STATIC
    public static final String PROPERTY_OBJ21 = "obj21";// Frozen   STATIC
    public static final String PROPERTY_OBJ22 = "obj22";// Counter  EVENT
    public static final String PROPERTY_OBJ23 = "obj23";// Frozen   EVENT
    public static final String PROPERTY_OBJ40 = "obj40";// Analogue STATIC
    public static final String PROPERTY_OBJ42 = "obj42";// Analogue EVENT
    public static final String PROPERTY_OBJ10 = "obj10";// Binary Output STATIC

    private VARIATION_GROUP1  obj01 = VARIATION_GROUP1.WITH_FLAGS;
    private VARIATION_GROUP2  obj02 = VARIATION_GROUP2.WITH_ABSOLUTE_TIME;
    private VARIATION_GROUP3  obj03 = VARIATION_GROUP3.WITH_FLAGS;
    private VARIATION_GROUP4  obj04 = VARIATION_GROUP4.WITH_ABSOLUTE_TIME;
    private VARIATION_GROUP30 obj30 = VARIATION_GROUP30.SINGLE_PREC_FLT_PT_WITH_FLAG;
    private VARIATION_GROUP32 obj32 = VARIATION_GROUP32.SINGLE_PREC_FLT_PT_WITH_TIME;
    private VARIATION_GROUP20 obj20 = VARIATION_GROUP20.BIT32_WITHOUT_FLAG;
    private VARIATION_GROUP22 obj22 = VARIATION_GROUP22.BIT32_WITH_FLAG_TIME;
    private VARIATION_GROUP21 obj21 = VARIATION_GROUP21.BIT32_WITH_FLAG_TIME;
    private VARIATION_GROUP23 obj23 = VARIATION_GROUP23.BIT32_WITH_FLAG_TIME;
    private VARIATION_GROUP40 obj40 = VARIATION_GROUP40.SINGLE_PREC_FLT_PT_WITH_FLAG;
    private VARIATION_GROUP42 obj42 = VARIATION_GROUP42.SINGLE_PREC_FLT_PT_WITH_TIME;
    private VARIATION_GROUP10 obj10 = VARIATION_GROUP10.STATUS_WITH_FLAGS;
    
    // @formatter:on

    GlobalVariationsConfig() {
      LoggingUtil.logPropertyChanges(this);
    }


    public VARIATION_GROUP1 getObj01() {
      return obj01;
    }

    public void setObj01(VARIATION_GROUP1 obj01) {
      if (obj01 == null) {
        return;
      }

      Object oldValue = this.obj01;
      this.obj01 = obj01;
      firePropertyChange("obj01", oldValue, obj01);
    }

    public VARIATION_GROUP2 getObj02() {
      return obj02;
    }

    public void setObj02(VARIATION_GROUP2 obj02) {
      if (obj02 == null) {
        return;
      }
      Object oldValue = this.obj02;
      this.obj02 = obj02;
      firePropertyChange("obj02", oldValue, obj02);
    }

    public VARIATION_GROUP3 getObj03() {
      return obj03;
    }

    public void setObj03(VARIATION_GROUP3 obj03) {
      if (obj03 == null) {
        return;
      }
      Object oldValue = this.obj03;
      this.obj03 = obj03;
      firePropertyChange("obj03", oldValue, obj03);
    }

    public VARIATION_GROUP4 getObj04() {
      return obj04;
    }

    public void setObj04(VARIATION_GROUP4 obj04) {
      if (obj04 == null) {
        return;
      }
      Object oldValue = this.obj04;
      this.obj04 = obj04;
      firePropertyChange("obj04", oldValue, obj04);
    }

    public VARIATION_GROUP30 getObj30() {
      return obj30;
    }

    public void setObj30(VARIATION_GROUP30 obj30) {
      if (obj30 == null) {
        return;
      }

      Object oldValue = this.obj30;
      this.obj30 = obj30;
      firePropertyChange("obj30", oldValue, obj30);
    }

    public VARIATION_GROUP32 getObj32() {
      return obj32;
    }

    public void setObj32(VARIATION_GROUP32 obj32) {
      if (obj32 == null) {
        return;
      }
      Object oldValue = this.obj32;
      this.obj32 = obj32;
      firePropertyChange("obj32", oldValue, obj32);
    }

    public VARIATION_GROUP20 getObj20() {
      return obj20;
    }

    public void setObj20(VARIATION_GROUP20 obj20) {
      if (obj20 == null) {
        return;
      }
      Object oldValue = this.obj20;
      this.obj20 = obj20;
      firePropertyChange("obj20", oldValue, obj20);
    }

    public VARIATION_GROUP22 getObj22() {
      return obj22;
    }

    public void setObj22(VARIATION_GROUP22 obj22) {
      if (obj22 == null) {
        return;
      }
      Object oldValue = this.obj22;
      this.obj22 = obj22;
      firePropertyChange("obj22", oldValue, obj22);
    }

    public VARIATION_GROUP21 getObj21() {
      return obj21;
    }

    public void setObj21(VARIATION_GROUP21 obj21) {
      if (obj21 == null) {
        return;
      }
      Object oldValue = this.obj21;
      this.obj21 = obj21;
      firePropertyChange("obj21", oldValue, obj21);
    }

    public VARIATION_GROUP23 getObj23() {
      return obj23;
    }

    public void setObj23(VARIATION_GROUP23 obj23) {
      if (obj23 == null) {
        return;
      }
      Object oldValue = this.obj23;
      this.obj23 = obj23;
      firePropertyChange("obj23", oldValue, obj23);
    }


    
    public VARIATION_GROUP40 getObj40() {
      return obj40;
    }


    
    public void setObj40(VARIATION_GROUP40 obj40) {
      if(obj40 == null)
        return;
      
      Object oldValue = this.obj40;
      this.obj40 = obj40;
      firePropertyChange("obj40", oldValue, obj40);
    }

    
    public VARIATION_GROUP42 getObj42() {
      return obj42;
    }
    
    public void setObj42(VARIATION_GROUP42 obj42) {
      if(obj42 == null)
        return;
      Object oldValue = this.obj42;
      this.obj42 = obj42;
      firePropertyChange("obj42", oldValue, obj42);
    }
    
    
    public VARIATION_GROUP10 getObj10() {
      return obj10;
    }


    public void setObj10(VARIATION_GROUP10 obj10) {
      if(obj10 == null)
        return;
      
      Object oldValue = this.obj10;
      this.obj10 = obj10;
      firePropertyChange("obj10", oldValue, obj10);
    }
    
  }

  public static class EventConfig extends Model {

    public static final int AI = 0;
    public static final int BI = 1;
    public static final int DI = 2;
    public static final int CT = 3;
    public static final int FCT = 4;
    public static final int AO = 5;
    public static final int SA = 6; //Secure Authentication

    public static final String PROPERTY_MAX_EVENTS = "maxEvents";
    public static final String PROPERTY_EVENTMODE = "eventMode";

    private Logger log = Logger.getLogger(SDNP3Session.EventConfig.class);

    private final String prefix = SDNP3Constraints.PREFIX_SESSION_EVENT;

    private long maxEvents;
    private LU_EVENT_MODE eventMode = LU_EVENT_MODE.LU_EVENT_MODE_SOE;


    public EventConfig() {
      maxEvents = cons.getDefault(prefix, PROPERTY_MAX_EVENTS);
      LoggingUtil.logPropertyChanges(this);
    }

    public long getMaxEvents() {
      return maxEvents;
    }

    public void setMaxEvents(long maxEvents) throws PropertyVetoException {
      checkBoundary(this, this.maxEvents, maxEvents, prefix, PROPERTY_MAX_EVENTS);

      Object oldValue = getMaxEvents();
      this.maxEvents = maxEvents;
      firePropertyChange("maxEvents", oldValue, maxEvents);

    }

    public LU_EVENT_MODE getEventMode() {
      return eventMode;
    }

    public void setEventMode(LU_EVENT_MODE eventMode) {

      Object oldValue = getEventMode();
      this.eventMode = eventMode;
      firePropertyChange("eventMode", oldValue, eventMode);

    }

    public void writeToXML(SesnEventConfT xml) {
      xml.maxEvents.setValue(getMaxEvents());
      xml.eventMode.setValue(getEventMode().name());
    }

    public void readFromXML(SesnEventConfT xml) {
      try {
        setMaxEvents(xml.maxEvents.getValue());
      } catch (PropertyVetoException e) {
        log.error("Fail to read maxEvent value from XML", e);
      }
      setEventMode(LU_EVENT_MODE.valueOf(xml.eventMode.getValue()));
    }
  }

}
