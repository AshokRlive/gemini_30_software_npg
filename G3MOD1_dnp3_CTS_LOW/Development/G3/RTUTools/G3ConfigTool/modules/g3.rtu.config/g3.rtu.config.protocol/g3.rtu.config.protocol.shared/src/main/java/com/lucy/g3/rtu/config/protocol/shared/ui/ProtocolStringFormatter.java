/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.shared.base.page.StringFormatter;

/**
 * The String formatter for formatting Protocol stack items to a string.
 */
public final class ProtocolStringFormatter implements StringFormatter {

  @Override
  public String format(Object value) {
    String text = "N/A";
    if (value != null) {
      if (value instanceof IProtocol<?>) {
        text = "[ Protocol ] " + ((IProtocol<?>) value).getProtocolName();
      } else if (value instanceof IProtocolChannel) {
        text = "[ Channel ] " + ((IProtocolChannel<?>) value).getChannelName();
      } else if (value instanceof IProtocolSession) {
        text = "[ Session ] " + ((IProtocolSession) value).getSessionName();
      } else {
        text = value.toString();
        Logger.getLogger(getClass()).warn("No string found for: " + value);
      }
    }

    return text;
  }
}
