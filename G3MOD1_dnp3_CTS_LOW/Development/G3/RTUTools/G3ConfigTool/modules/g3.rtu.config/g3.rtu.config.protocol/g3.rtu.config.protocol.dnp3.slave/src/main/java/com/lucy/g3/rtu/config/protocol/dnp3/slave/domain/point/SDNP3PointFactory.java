/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.IScadaPointFactory;

/**
 * A factory for creating SDNP3Point objects.
 */
public final class SDNP3PointFactory implements IScadaPointFactory<SDNP3Point> {

  private Logger log = Logger.getLogger(SDNP3PointFactory.class);


  @Override
  public SDNP3Point createPoint(ScadaPointSource source, IScadaIoMap<SDNP3Point> iomap, ScadaPointType type,
      long id, String description) {
    Preconditions.checkNotNull(type, "type is null");
    Preconditions.checkArgument(type instanceof SDNP3PointType, "expected SDNP3PointType");

    SDNP3Point point = null;
    try {
      point = create(iomap, (SDNP3PointType) type, id);

      if (!Strings.isBlank(description)) {
        point.setCustomDescription(description);
      }

      iomap.addPoint(point);
      point.mapTo(source);

    } catch (Throwable e) {
      log.fatal("Fail to create SDNP3 point", e);
    }

    return point;
  }

  public static SDNP3InputPoint createInput(IScadaIoMap<SDNP3Point> iomap, SDNP3PointType type, long id) {
    Preconditions.checkNotNull(type, "type is null");
    switch (type) {
    case AnalogueInput:
    case BinaryInput:
    case DoubleBinaryInput:
      return new SDNP3InputPoint(iomap, type, id);
    case Counter:
      return new SDNP3CounterPoint(iomap, id);
    default:
      throw new UnsupportedOperationException("Unsupported type: " + type);
    }
  }

  public static SDNP3OutputPoint createOutput(IScadaIoMap<SDNP3Point> iomap, SDNP3PointType type, long id) {
    Preconditions.checkNotNull(type, "type is null");
    switch (type) {
    case BinaryOutput:
      return SDNP3OutputPoint.createBinaryOutput(iomap, id);
    case AnalogOutput:
      return SDNP3OutputPoint.createAnalogueOutput(iomap, id);
    default:
      throw new UnsupportedOperationException("Unsupported type: " + type);
    }
  }

  public static SDNP3Point create(IScadaIoMap<SDNP3Point> iomap, SDNP3PointType type, long id) {
    Preconditions.checkNotNull(type, "type is null");
    switch (type) {

    case AnalogueInput:
    case BinaryInput:
    case DoubleBinaryInput:
      return new SDNP3InputPoint(iomap, type, id);
    case BinaryOutput:
      return SDNP3OutputPoint.createBinaryOutput(iomap, id);
    case AnalogOutput:
      return SDNP3OutputPoint.createAnalogueOutput(iomap, id);
    case Counter:
      return new SDNP3CounterPoint(iomap,  id);
    default:
      throw new UnsupportedOperationException("Unsupported type: " + type);
    }
  }

}
