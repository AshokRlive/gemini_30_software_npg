/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import static org.junit.Assert.*;

import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Test;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.test.support.utilities.TestUtil;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.ObjectToStringConverter;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points.IOAEditor;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points.IOAFormatter;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_IOA_FORMAT;


/**
 * The Class IOAEditorTest.
 */
public class IOAEditorTest {
  
  private IOAFormatter formatter = new IOAFormatter();
  
  @Test public void testIOAFormat8_8_8() throws ParseException {
    formatter.setIoaFormat(LU_IOA_FORMAT.LU_IOA_FORMAT_8_8_8);
    
    assertTrue(1L  ==        formatter.stringToValue("1"));
    assertTrue(65793L ==      formatter.stringToValue("1.1.1"));
    assertTrue(16777215L==   formatter.stringToValue("255.255.255"));
    
    assertEquals("0.0.1",       formatter.valueToString(1));
    assertEquals("1.1.1",       formatter.valueToString(65793));
    assertEquals("255.255.255", formatter.valueToString(16777215));
  }
  
  @Test public void testIOAFormat16_8() throws ParseException {
    formatter.setIoaFormat(LU_IOA_FORMAT.LU_IOA_FORMAT_16_8);
    
    assertTrue(1L==          formatter.stringToValue("1"));
    assertTrue(257L==        formatter.stringToValue("1.1"));
    assertTrue(16777215L==   formatter.stringToValue("65535.255"));
    
    assertEquals("0.1",         formatter.valueToString(1));
    assertEquals("257.1",     formatter.valueToString(65793));
    assertEquals("65535.255",   formatter.valueToString(16777215));
  }

  public static void main(String[] args) {
    MyBean ioaBean = new MyBean();

    Logger log = Logger.getLogger(IOAEditorTest.class);
    log .setLevel(Level.DEBUG);
    LoggingUtil.logPropertyChanges(ioaBean,log);
    
    JPanel panel = new JPanel();
    final IOAEditor ioaField = new IOAEditor();
    PropertyAdapter<MyBean> valueModel = new PropertyAdapter<MyBean>(ioaBean, "ioa",true);
    Bindings.bind(ioaField, valueModel);
    JLabel ioaLabel = BasicComponentFactory.createLabel(
        new ConverterValueModel(valueModel, new ObjectToStringConverter()));
    
    JFormattedTextField ioaField2 = BasicComponentFactory.createIntegerField(valueModel);
    ioaField2.setColumns(12);
    
    panel.add(ioaField);
    panel.add(ioaLabel);
    panel.add(ioaField2);
    
    TestUtil.showFrame(panel);
  }
  
  public static class MyBean extends Model {
    private int ioa;

    
    public int getIoa() {
      return ioa;
    }

    
    public void setIoa(int ioa) {
      Object oldValue = this.ioa;
      this.ioa = ioa;
      firePropertyChange("ioa", oldValue, ioa);
    }
  }
}

