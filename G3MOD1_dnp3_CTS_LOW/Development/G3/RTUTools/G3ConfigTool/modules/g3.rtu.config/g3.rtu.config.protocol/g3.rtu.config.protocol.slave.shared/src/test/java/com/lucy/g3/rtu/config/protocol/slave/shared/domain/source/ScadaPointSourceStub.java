/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.AbstractScadaPointSource;
import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;

/**
 *
 */
public class ScadaPointSourceStub extends AbstractScadaPointSource<ObservableBean2> {

  public ScadaPointSourceStub() {
    super(new BeanWithEnable());
  }

  @Override
  public String getDescription() {
    return null;
  }

  @Override
  public int getGroup() {
    return 0;
  }

  @Override
  public int getId() {
    return 0;
  }

  @Override
  protected String generateName() {
    return "stub";
  }

  @Override
  public boolean isAnalogue() {
    return false;
  }

  @Override
  public String getType() {
    return null;
  }

}
