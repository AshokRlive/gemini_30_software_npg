/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;


public class SDNP3ChannelSerialConf extends ProtocolChannelSerialConf{

  public SDNP3ChannelSerialConf(IProtocolChannel<?> channel) {
    super(channel);
  }

  @Override
  protected void setActive(boolean active) {
    super.setActive(active);
  }
  
}

