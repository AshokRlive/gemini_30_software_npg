/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;

/**
 * The interface of Protocol Session, which is supposed to be managed by a
 * {@linkplain IProtocolChannel}.
 */
public interface IProtocolSession extends ObservableBean2, IContainerValidation {

  String PROPERTY_SESSION_NAME = "sessionName";


  String getSessionName();

  /**
   * Gets the protocol channel.
   *
   * @return the Non-null protocol channel
   */
  IProtocolChannel<? extends IProtocolSession> getProtocolChannel();

  void delete();

  /**
   * Gets the IO map of this session.
   *
   * @return IO Map object, null if it is not available.
   */
  IIoMap getIomap();

  ProtocolType getProtocolType();

}
