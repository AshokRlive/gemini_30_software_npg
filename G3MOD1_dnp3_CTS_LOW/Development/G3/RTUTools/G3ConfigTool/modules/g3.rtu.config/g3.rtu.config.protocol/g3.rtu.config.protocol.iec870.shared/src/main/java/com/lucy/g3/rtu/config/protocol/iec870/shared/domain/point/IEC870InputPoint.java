/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import java.util.HashMap;

import com.g3schema.ns_common.VirtualPointRefT;
import com.g3schema.ns_iec870.IEC870InputPointT;
import com.lucy.g3.common.utils.EnumUtils;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.base.math.BitMask;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_IOA_FORMAT;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;

/**
 * The input point of IEC870.
 */
public class IEC870InputPoint extends IEC870Point {

  public static final String PROPERTY_ENABLE_CYCLIC = "enableCyclic";
  public static final String PROPERTY_ENABLE_BACKGROUND = "enableBackground";
  public static final String PROPERTY_ENABLE_GENERAL_INTEROGATION = "enableGeneralInterogation";
  public static final String PROPERTY_EVENT_ONLY_WHEN_CONNECTED = "eventOnlyWhenConnected";
  public static final String PROPERTY_SPONTANEOUS_EVENT_ENABLED = "spontaneousEventEnabled";
  public static final String PROPERTY_TIME_FORMAT = "timeFormat";
  public static final String PROPERTY_CUSTOMISE_TIME_FORMAT = "customiseTimeFormat";
  public static final String PROPERTY_IOA_DISPLAY_PATTERN = "ioaDisplayPattern";
  
  /**
   * Read-only property.
   */
  public static final String PROPERTY_GROUP_MASK = "groupMask";

  private boolean enableCyclic;
  private boolean enableBackground;
  private boolean enableGeneralInterogation = true;
  private boolean eventOnlyWhenConnected;
  private boolean spontaneousEventEnabled;
  private LU_IOA_FORMAT ioaDisplayPattern = LU_IOA_FORMAT.LU_IOA_FORMAT_16_8;
  private LU_TIME_FORMAT timeFormat = LU_TIME_FORMAT.LU_TIME_FORMAT_56;
  private boolean customiseTimeFormat;
  private final GroupMask groupMask;


  public IEC870InputPoint(IScadaIoMap<IEC870Point> ioMap, IEC870PointType type, long id) {
    super(ioMap, type, id);
    groupMask = new GroupMask(type);

    // Set defaults
    setEnableCyclic(type.isAnalog());
    setEnableBackground(true);
    setSpontaneousEventEnabled(true);
  }

  public boolean isEnableCyclic() {
    return enableCyclic;
  }
  
  public boolean isCustomiseTimeFormat() {
    return customiseTimeFormat;
  }

  public void setCustomiseTimeFormat(boolean customiseTimeFormat) {
    Object oldValue = this.customiseTimeFormat;
    this.customiseTimeFormat = customiseTimeFormat;
    firePropertyChange(PROPERTY_CUSTOMISE_TIME_FORMAT, oldValue, customiseTimeFormat);
  }

  public void setEnableCyclic(boolean enableCyclic) {
    Object oldValue = this.enableCyclic;
    this.enableCyclic = enableCyclic;
    firePropertyChange(PROPERTY_ENABLE_CYCLIC, oldValue, enableCyclic);
  }

  public boolean isEnableBackground() {
    return enableBackground;
  }

  public void setEnableBackground(boolean enableBackground) {
    Object oldValue = this.enableBackground;
    this.enableBackground = enableBackground;
    firePropertyChange(PROPERTY_ENABLE_BACKGROUND, oldValue, enableBackground);
  }

  public boolean isEnableGeneralInterogation() {
    return enableGeneralInterogation;
  }

  public void setEnableGeneralInterogation(boolean enableGeneralInterogation) {
    Object oldValue = this.enableGeneralInterogation;
    this.enableGeneralInterogation = enableGeneralInterogation;
    firePropertyChange(PROPERTY_ENABLE_GENERAL_INTEROGATION, oldValue,
        enableGeneralInterogation);
  }

  public boolean isEventOnlyWhenConnected() {
    return eventOnlyWhenConnected;
  }

  public void setEventOnlyWhenConnected(boolean eventOnlyWhenConnected) {
    Object oldValue = this.eventOnlyWhenConnected;
    this.eventOnlyWhenConnected = eventOnlyWhenConnected;
    firePropertyChange(PROPERTY_EVENT_ONLY_WHEN_CONNECTED, oldValue,
        eventOnlyWhenConnected);
  }

  @Override
  public GroupMask getGroupMask() {
    return groupMask;
  }

  public boolean isSpontaneousEventEnabled() {
    return spontaneousEventEnabled;
  }

  public void setSpontaneousEventEnabled(boolean spontaneousEventEnabled) {
    Object oldValue = this.spontaneousEventEnabled;
    this.spontaneousEventEnabled = spontaneousEventEnabled;
    firePropertyChange(PROPERTY_SPONTANEOUS_EVENT_ENABLED, oldValue, spontaneousEventEnabled);
  }

  public LU_IOA_FORMAT getIoaDisplayPattern() {
    return ioaDisplayPattern;
  }

  public void setIoaDisplayPattern(LU_IOA_FORMAT ioaDisplayPattern) {
    Object oldValue = this.ioaDisplayPattern;
    this.ioaDisplayPattern = ioaDisplayPattern;
    firePropertyChange(PROPERTY_IOA_DISPLAY_PATTERN, oldValue, ioaDisplayPattern);
  }

  public LU_TIME_FORMAT getTimeFormat() {
    return timeFormat;
  }

  public void setTimeFormat(LU_TIME_FORMAT timeFormat) {
    Object oldValue = this.timeFormat;
    this.timeFormat = timeFormat;
    firePropertyChange(PROPERTY_TIME_FORMAT, oldValue, timeFormat);
  }

  public void writeToXML(IEC870InputPointT xml) {
    super.writeToXML(xml);
    xml.enableCyclic.setValue(isEnableCyclic());

    xml.enableBackground.setValue(isEnableBackground());
    xml.enableGeneralInterrogation.setValue(isEnableGeneralInterogation());
    xml.groupMask.setValue(getGroupMask().getValue());
    xml.spontaneousEvent.setValue(isSpontaneousEventEnabled());
    xml.eventOnlyWhenConnected.setValue(isEventOnlyWhenConnected());

    if(customiseTimeFormat && timeFormat != null)
      xml.timeformat.setValue(timeFormat.name());
    
    xml.ioaDisplayPattern.setValue(ioaDisplayPattern != null ? ioaDisplayPattern.name() : null);

    ScadaPointSource res = getSource();
    if (res != null) {
      VirtualPointRefT xmlPoint = xml.vpoint.append();
      xmlPoint.pointGroup.setValue(res.getGroup());
      xmlPoint.pointID.setValue(res.getId());
    }
  }

  public void readFromXML(IEC870InputPointT xml) {
    super.readFromXML(xml);

    setEnableBackground(xml.enableBackground.getValue());
    setEnableGeneralInterogation(xml.enableGeneralInterrogation.getValue());
    groupMask.setValue(xml.groupMask.getValue());

    if (xml.enableCyclic.exists()) {
      setEnableCyclic(xml.enableCyclic.getValue());
    }

    if (xml.spontaneousEvent.exists()) {
      setSpontaneousEventEnabled(xml.spontaneousEvent.getValue());
    }

    if (xml.eventOnlyWhenConnected.exists()) {
      setEventOnlyWhenConnected(xml.eventOnlyWhenConnected.getValue());
    }

    if (xml.timeformat.exists()) {
      setCustomiseTimeFormat(true);
      setTimeFormat(EnumUtils.getEnumFromString(LU_TIME_FORMAT.class, xml.timeformat.getValue()));
    }else{
      setCustomiseTimeFormat(false);
    }

    if (xml.ioaDisplayPattern.exists()) {
      setIoaDisplayPattern(EnumUtils.getEnumFromString(LU_IOA_FORMAT.class, xml.ioaDisplayPattern.getValue()));
    }

  }

  @Override
  public void applyParameters(HashMap<String, Object> parameters) {
 String param;
    
    param = PROPERTY_ENABLED;
    if (parameters.containsKey(param)) {
      setEnabled(!(boolean)parameters.get(param));
    }
    
    param = PROPERTY_ENABLE_BACKGROUND;
    if (parameters.containsKey(param)) {
      setEnableBackground((boolean)parameters.get(param));
    }
    
    param = PROPERTY_ENABLE_CYCLIC;
    if (parameters.containsKey(param)) {
      setEnableCyclic((boolean)parameters.get(param));
    }
    
    param = PROPERTY_ENABLE_GENERAL_INTEROGATION;
    if (parameters.containsKey(param)) {
      setEnableGeneralInterogation((boolean)parameters.get(param));
    }
    
    param = PROPERTY_SPONTANEOUS_EVENT_ENABLED;
    if (parameters.containsKey(param)) {
      setSpontaneousEventEnabled((boolean)parameters.get(param));
    }
    
    param = PROPERTY_EVENT_ONLY_WHEN_CONNECTED;
    if (parameters.containsKey(param)) {
      setEventOnlyWhenConnected((boolean)parameters.get(param));
    }
    
    param = PROPERTY_TIME_FORMAT;
    if (parameters.containsKey(param)) {
      setTimeFormat((LU_TIME_FORMAT) parameters.get(param));
      setCustomiseTimeFormat(true);
    }
    
    param = PROPERTY_GROUP_MASK;
    if (parameters.containsKey(param)) {
      BitMask b = (BitMask) parameters.get(param);
      getGroupMask().setValue(b.getValue());
    }
    
  }

}
