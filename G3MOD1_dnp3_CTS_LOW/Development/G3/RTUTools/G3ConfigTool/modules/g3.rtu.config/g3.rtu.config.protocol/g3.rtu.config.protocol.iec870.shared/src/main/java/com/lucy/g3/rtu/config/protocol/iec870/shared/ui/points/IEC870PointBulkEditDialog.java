/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.renderer.BooleanListRenderer;
import com.lucy.g3.gui.common.widgets.utils.KeyBindingUtil;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.GroupMask;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870InputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870OutputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointIdChecker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointIdEditor;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.base.math.BitMaskEditor;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;

class IEC870PointBulkEditDialog extends JDialog {

  private final IEC870PointType type;

  private final PointTableModel pointsTableModel;

  private boolean cancelled = false;
  private IScadaIoMap<IEC870Point> iomap;
  
  private final GroupMask groupMask;

  public IEC870PointBulkEditDialog(Frame owner, List<? extends IEC870Point> editingPoints,
      IEC870PointType type, IScadaIoMap<IEC870Point> iomap) {
    super(owner, true);
    this.type = Preconditions.checkNotNull(type, "type must not be null");
    this.groupMask = new GroupMask(type);

    this.iomap = iomap;
    
    setName("IEC870PointBulkEditDialog."+type.name());

    Preconditions.checkNotNull(editingPoints, "editingPoints must not be null");
    editingPoints.removeAll(Collections.singleton(null));// Remove null points.
    IEC870Point[] pointsArray = editingPoints.toArray(new IEC870Point[editingPoints.size()]);
    this.pointsTableModel = new PointTableModel(pointsArray, type, iomap);

    initComponents();
    initEventHandling();
    initComponentStates();
    setTitle(getTitle() + " - " + type.getDescription());

    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());

  }

  private static void buildComboBox(JComboBox<Object> combo, Object[] items, ListCellRenderer<Object> renderer) {
    DefaultComboBoxModel<Object> model = new DefaultComboBoxModel<>(items);
    model.insertElementAt(null, 0); // insert NULL item for de-selecting
    combo.setModel(model);
    if(renderer != null)
    combo.setRenderer(renderer);
    combo.setSelectedIndex(0);
  }

  private void applyChanges() {
    // ====== Apply parameters in the table ======
    TableCellEditor editor = pointsTable.getCellEditor();
    if (editor != null) {
      editor.stopCellEditing();
    }
    pointsTableModel.saveChanges();

    // ====== Apply other parameters ======
    HashMap<String, Object> params = new HashMap<>();
    
    Boolean disbabled = (Boolean) comboDisable.getSelectedItem();
    params.put(IEC870InputPoint.PROPERTY_ENABLED, disbabled == null ? null : !disbabled);
    params.put(IEC870InputPoint.PROPERTY_ENABLE_BACKGROUND, combBackground.getSelectedItem());
    params.put(IEC870InputPoint.PROPERTY_ENABLE_CYCLIC, combCyclic.getSelectedItem());
    params.put(IEC870InputPoint.PROPERTY_EVENT_ONLY_WHEN_CONNECTED, combEventOnlyConn.getSelectedItem());
    params.put(IEC870InputPoint.PROPERTY_ENABLE_GENERAL_INTEROGATION, combGeneralInter.getSelectedItem());
    params.put(IEC870InputPoint.PROPERTY_SPONTANEOUS_EVENT_ENABLED, combSponEvent.getSelectedItem());
    params.put(IEC870InputPoint.PROPERTY_TIME_FORMAT, combTimeFormat.getSelectedItem());
    
    if(combGroupMask.getSelectedItem() == Boolean.TRUE) {
      params.put(IEC870InputPoint.PROPERTY_GROUP_MASK, groupMask);
    }
    
    params.put(IEC870OutputPoint.PROPERTY_SELECTREQUIRED, comboSelectBeforeOperate.getSelectedItem());

    // Remove null values
    params.values().removeAll(Collections.singleton(null));
    
    // Apply all parameters to all points.
    for (int i = 0; i < pointsTableModel.pointNum; i++) {
      pointsTableModel.points[i].applyParameters(params);
    }
  }

  private void checkBoxSeqIDActionPerformed(ActionEvent e) {
    pointsTableModel.setSequentialID(checkBoxSeqID.isSelected());
  }

  private void createUIComponents() {
    pointsTable = new JXTable(pointsTableModel);
    pointsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
  
    TableColumnModel columnModel = pointsTable.getColumnModel();
  
    /* Set editor for protocol ID column */
    NumberFormat format = NumberFormat.getIntegerInstance();
    format.setParseIntegerOnly(true);
    TableCellEditor idEditor = new ScadaPointIdEditor(new ScadaPointIdChecker(iomap, type));
    columnModel.getColumn(PointTableModel.COLUMN_PROTOCOL_ID).setCellEditor(idEditor);
  
    /* Customise column width */
    TableColumn pidCol = columnModel.getColumn(PointTableModel.COLUMN_PROTOCOL_ID);
    pidCol.setMinWidth(100);
    columnModel.getColumn(PointTableModel.COLUMN_PROTOCOL_ID).setMaxWidth(50);
  
    /* Set protocol ID column centre alignment */
    DefaultTableCellRenderer render = new DefaultTableCellRenderer();
    render.setHorizontalAlignment(SwingConstants.CENTER);
    pidCol.setCellRenderer(render);
    
    groupMaskEditor = new BitMaskEditor(new PresentationModel<GroupMask>(groupMask), false);
    groupMaskEditor.setVisible(0, false);
  }

  private void initEventHandling() {
    ActionListener okAL = new OKActionListener();
    ActionListener cancelAL = new CancelActionListener();
    cancelButton.addActionListener(cancelAL);
    okButton.addActionListener(okAL);
  
    KeyBindingUtil.registerEscapeEnterKey(getRootPane(), okAL, cancelAL);
  }

  /**
   * Initialises default states of components.
   */
  private void initComponentStates() {
    pointsTableModel.setSequentialID(checkBoxSeqID.isSelected());
    
    final BooleanListRenderer boolRender = new BooleanListRenderer("Enabled", "Disabled", "(No change)");
    final Boolean[] BOOL_ITEMS = {Boolean.TRUE, Boolean.FALSE };
    
    buildComboBox(comboDisable, BOOL_ITEMS, boolRender);
    buildComboBox(combBackground, BOOL_ITEMS, boolRender);
    buildComboBox(combCyclic, BOOL_ITEMS, boolRender);
    buildComboBox(combEventOnlyConn, BOOL_ITEMS, boolRender);
    buildComboBox(combGeneralInter, BOOL_ITEMS, boolRender);
    buildComboBox(comboSelectBeforeOperate, BOOL_ITEMS, boolRender);
    buildComboBox(combSponEvent, BOOL_ITEMS, boolRender);
    buildComboBox(combTimeFormat, LU_TIME_FORMAT.values(), new TimeFormatRenderer());
    buildComboBox(combGroupMask, new Boolean[]{Boolean.TRUE}, new BooleanListRenderer("Change", "", "(No change)"));
  
    // Show/hide cards
    CardLayout cl = (CardLayout)(panelSettings.getLayout());
    if (type.isOutput()) {
      cl.show(panelSettings, "cardOutput");
    }else{
      cl.show(panelSettings, "cardInput");
    }
    
    // Show/hide groupMaskEditor
    groupMaskEditor.setVisible(combGroupMask.getSelectedItem() == Boolean.TRUE);
    combGroupMask.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        groupMaskEditor.setVisible(combGroupMask.getSelectedItem() == Boolean.TRUE);        
      }
    });
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    JXTitledSeparator separator2 = new JXTitledSeparator();
    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    checkBoxSeqID = new JCheckBox();
    label2 = new JLabel();
    comboDisable = new JComboBox<>();
    inputPointParamPane = new JPanel();
    JXTitledSeparator separator1 = new JXTitledSeparator();
    panelSettings = new JPanel();
    panelInputPointParamPanel = new JPanel();
    label1 = new JLabel();
    combCyclic = new JComboBox<>();
    label3 = new JLabel();
    combBackground = new JComboBox<>();
    label6 = new JLabel();
    combSponEvent = new JComboBox<>();
    label8 = new JLabel();
    combEventOnlyConn = new JComboBox<>();
    label4 = new JLabel();
    combTimeFormat = new JComboBox<>();
    label5 = new JLabel();
    combGroupMask = new JComboBox<>();
    label7 = new JLabel();
    combGeneralInter = new JComboBox<>();
    panelOutputPointParamPanel = new JPanel();
    label9 = new JLabel();
    comboSelectBeforeOperate = new JComboBox<>();
    buttonBar = new JPanel();
    btnHelp = new JButton();
    okButton = new JButton();
    cancelButton = new JButton();

    //======== this ========
    setTitle("Editing IEC870-101/104 Points");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("dnpbulkedit");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "default:grow",
            "default, $lgap, fill:[60dlu,default]:grow, $ugap, default, $pgap, default"));

        //---- separator2 ----
        separator2.setTitle("1. IEC870 101/104 Points ");
        contentPanel.add(separator2, CC.xy(1, 1));

        //======== scrollPane1 ========
        {
          scrollPane1.setPreferredSize(new Dimension(650, 200));

          //---- pointsTable ----
          pointsTable.setRowSorter(null);
          pointsTable.setFillsViewportHeight(true);
          pointsTable.setGridColor(Color.lightGray);
          pointsTable.setName("pointsTable");
          scrollPane1.setViewportView(pointsTable);
        }
        contentPanel.add(scrollPane1, CC.xy(1, 3));

        //======== panel1 ========
        {
          panel1.setLayout(new FormLayout(
              "default, $lcgap, default:grow, $lcgap, right:[50dlu,default], $lcgap, [50dlu,default]",
              "default"));

          //---- checkBoxSeqID ----
          checkBoxSeqID.setText("Sequential Protocol ID");
          checkBoxSeqID.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              checkBoxSeqIDActionPerformed(e);
            }
          });
          panel1.add(checkBoxSeqID, CC.xy(1, 1));

          //---- label2 ----
          label2.setText("Disabled All Points:");
          panel1.add(label2, CC.xy(5, 1));
          panel1.add(comboDisable, CC.xy(7, 1));
        }
        contentPanel.add(panel1, CC.xy(1, 5));

        //======== inputPointParamPane ========
        {
          inputPointParamPane.setLayout(new FormLayout(
              "2*(default, $lcgap), [50dlu,default]:grow",
              "default, $ugap, default"));

          //---- separator1 ----
          separator1.setTitle("2. Parameters");
          inputPointParamPane.add(separator1, CC.xywh(1, 1, 5, 1));

          //======== panelSettings ========
          {
            panelSettings.setLayout(new CardLayout());

            //======== panelInputPointParamPanel ========
            {
              panelInputPointParamPanel.setLayout(new FormLayout(
                  "right:default, $lcgap, default, 10dlu, right:default, $lcgap, default, $lcgap, default:grow",
                  "3*(default, $ugap), default, $lgap, top:default"));

              //---- label1 ----
              label1.setText("Cyclic:");
              panelInputPointParamPanel.add(label1, CC.xy(1, 1));
              panelInputPointParamPanel.add(combCyclic, CC.xy(3, 1));

              //---- label3 ----
              label3.setText("Background:");
              panelInputPointParamPanel.add(label3, CC.xy(1, 3));
              panelInputPointParamPanel.add(combBackground, CC.xy(3, 3));

              //---- label6 ----
              label6.setText("Spontaneous Event:");
              panelInputPointParamPanel.add(label6, CC.xy(5, 3));
              panelInputPointParamPanel.add(combSponEvent, CC.xy(7, 3));

              //---- label8 ----
              label8.setText("Event Only When Connected:");
              panelInputPointParamPanel.add(label8, CC.xy(1, 5));
              panelInputPointParamPanel.add(combEventOnlyConn, CC.xy(3, 5));

              //---- label4 ----
              label4.setText("Time Format:");
              panelInputPointParamPanel.add(label4, CC.xy(5, 5));
              panelInputPointParamPanel.add(combTimeFormat, CC.xy(7, 5));

              //---- label5 ----
              label5.setText("Group Mask:");
              panelInputPointParamPanel.add(label5, CC.xy(1, 7));
              panelInputPointParamPanel.add(combGroupMask, CC.xy(3, 7));

              //---- label7 ----
              label7.setText("General Interrogation:");
              panelInputPointParamPanel.add(label7, CC.xy(5, 7));
              panelInputPointParamPanel.add(combGeneralInter, CC.xy(7, 7));

              //---- groupMaskEditor ----
              groupMaskEditor.setRequestFocusEnabled(false);
              groupMaskEditor.setMinimumSize(new Dimension(200, 33));
              groupMaskEditor.setPreferredSize(new Dimension(200, 150));
              panelInputPointParamPanel.add(groupMaskEditor, CC.xywh(3, 9, 7, 1));
            }
            panelSettings.add(panelInputPointParamPanel, "cardInput");

            //======== panelOutputPointParamPanel ========
            {
              panelOutputPointParamPanel.setLayout(new FormLayout(
                  "right:default, $lcgap, default",
                  "default"));

              //---- label9 ----
              label9.setText("Select Before Operate Required:");
              panelOutputPointParamPanel.add(label9, CC.xy(1, 1));
              panelOutputPointParamPanel.add(comboSelectBeforeOperate, CC.xy(3, 1));
            }
            panelSettings.add(panelOutputPointParamPanel, "cardOutput");
          }
          inputPointParamPane.add(panelSettings, CC.xywh(1, 3, 5, 1));
        }
        contentPanel.add(inputPointParamPane, CC.xy(1, 7));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$lcgap, default, $glue, $button, $rgap, $button",
            "pref"));

        //---- btnHelp ----
        btnHelp.setText("Help");
        buttonBar.add(btnHelp, CC.xy(2, 1));

        //---- okButton ----
        okButton.setText("OK");
        okButton.setToolTipText("Create and add DNP3 protocol points from selected resources");
        buttonBar.add(okButton, CC.xy(4, 1));

        //---- cancelButton ----
        cancelButton.setText("Close");
        buttonBar.add(cancelButton, CC.xy(6, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JTable pointsTable;
  private JPanel panel1;
  private JCheckBox checkBoxSeqID;
  private JLabel label2;
  private JComboBox<Object> comboDisable;
  private JPanel inputPointParamPane;
  private JPanel panelSettings;
  private JPanel panelInputPointParamPanel;
  private JLabel label1;
  private JComboBox<Object> combCyclic;
  private JLabel label3;
  private JComboBox<Object> combBackground;
  private JLabel label6;
  private JComboBox<Object> combSponEvent;
  private JLabel label8;
  private JComboBox<Object> combEventOnlyConn;
  private JLabel label4;
  private JComboBox<Object> combTimeFormat;
  private JLabel label5;
  private JComboBox<Object> combGroupMask;
  private JLabel label7;
  private JComboBox<Object> combGeneralInter;
  private BitMaskEditor groupMaskEditor;
  private JPanel panelOutputPointParamPanel;
  private JLabel label9;
  private JComboBox<Object> comboSelectBeforeOperate;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class OKActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      // Validate
      Long duplicatedID = pointsTableModel.checkDuplicatedProtocolID();
      if (duplicatedID != null) {
        JOptionPane.showMessageDialog(IEC870PointBulkEditDialog.this,
            "Duplicated Protocol ID: " + duplicatedID,
            "Error", JOptionPane.ERROR_MESSAGE);
      } else {

        applyChanges();
        dispose();
      }
    }
  }

  private class CancelActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      dispose();
      cancelled = true;
    }
  }


  public boolean isCancelled() {
    return cancelled;
  }


  private static class PointTableModel extends AbstractTableModel {

    static final int COLUMN_RESOURCE_NAME = 0;
    static final int COLUMN_PROTOCOL_ID = 1;
    static final int COLUMN_DESCRIPTION = 2;

    private static final String[] COLUMN_NAMES = { "Source", "Point ID", "Description (Optional)" };

    private final IEC870Point[] points;

    private final IEC870PointType type;

    private final IScadaIoMap<IEC870Point> iomap;

    private final int pointNum;

    private boolean sequentialID;

    private String[] mappedRes;
    private Long[] protocolID;
    private String[] description;


    public PointTableModel(IEC870Point[] points, IEC870PointType type, IScadaIoMap<IEC870Point> iomap) {
      this.type = type;
      this.iomap = iomap;
      this.points = points;

      pointNum = points.length;
      mappedRes = new String[pointNum];
      protocolID = new Long[pointNum];
      description = new String[pointNum];

      ScadaPointSource res;
      for (int i = 0; i < pointNum; i++) {
        if (points[i] != null) {
          res = points[i].getSource();
          mappedRes[i] = res == null ? "- Not Configured -" : res.getName();
          protocolID[i] = points[i].getPointID();
          description[i] = points[i].getDescription();
        }
      }

    }

    public void setSequentialID(boolean sequentialID) {
      this.sequentialID = sequentialID;
    }

    @Override
    public int getRowCount() {
      return pointNum;
    }

    @Override
    public int getColumnCount() {
      return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
      return COLUMN_NAMES[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      switch (columnIndex) {
      case COLUMN_RESOURCE_NAME:
        return mappedRes[rowIndex];

      case COLUMN_PROTOCOL_ID:
        return protocolID[rowIndex];

      case COLUMN_DESCRIPTION:
        return description[rowIndex];

      default:
        return null;
      }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex == COLUMN_PROTOCOL_ID) {
        return Long.class;
      } else {
        return super.getColumnClass(columnIndex);
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex != COLUMN_RESOURCE_NAME;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      /* Set Protocol ID */
      if (columnIndex == COLUMN_PROTOCOL_ID) {
        if (iomap.checkPointID((Long) aValue, type) == false)
        {
          return; // Invalid protocol ID value
        }

        long value = (Long) aValue;
        protocolID[rowIndex] = value;

        /* Update subsequent points with sequential ID */
        if (sequentialID) {
          int rows = getRowCount();
          for (int i = rowIndex + 1; i < rows; i++) {
            protocolID[i] = findNextProtocolID(value + 1, type);
            value = protocolID[i];
          }

          // notify table to update
          if (rowIndex + 1 < rows) {
            fireTableRowsUpdated(rowIndex + 1, rows - 1);
          }
        }
      }

      /* Set protocol point description */
      else if (columnIndex == COLUMN_DESCRIPTION) {
        if (aValue != null) {
          description[rowIndex] = aValue.toString();
        }
      }

    }

    private long findNextProtocolID(long startingPoint, IEC870PointType type) {
      long id = startingPoint;

      while (id < type.getMaximumPointID()) {

        if (isProtocolIDValid(type, id)) {
          return id;
        }

        id++;
      }

      return 0;// Fail to allocate a id, just return default 0
    }

    /**
     * Checks if all input protocol ID are valid.
     *
     * @return duplicated protocol ID. Null if not found.
     */
    private Long checkDuplicatedProtocolID() {
      for (int i = 0; i < pointNum; i++) {
        for (int j = i + 1; j < pointNum; j++) {
          if (protocolID[i].equals(protocolID[j])) {
            return protocolID[i];
          }
        }
      }

      return null;
    }

    public void saveChanges() {
      for (int i = 0; i < points.length; i++) {
        try {
          points[i].setPointID(protocolID[i]);
        } catch (DuplicatedException e) {
          e.printStackTrace();
        }
        points[i].setCustomDescription(description[i]);
      }
    }

    /**
     * Checks if protocol ID is valid for assigning to a editing point. A
     * protocol point id is valid if it is not used in DNP3 or it is in current
     * editing list.
     */
    private boolean isProtocolIDValid(IEC870PointType type, long id) {

      if (iomap.exists(type, id)) {
        boolean inEditinglist = false;
        for (int i = 0; i < points.length; i++) {
          if (points[i].getPointID() == id) {
            inEditinglist = true;
            break;
          }
        }

        return inEditinglist;

      } else {

        return true;
      }
    }
  }

  private static class TimeFormatRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
        int index, boolean isSelected, boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index,
          isSelected, cellHasFocus);

      if (value != null && value instanceof LU_TIME_FORMAT) {
        LU_TIME_FORMAT format = (LU_TIME_FORMAT) value;
        setText(format.getDescription());
      } else if (value == null) {
        setText("(No change)");
      }

      return comp;
    }
  }
}
