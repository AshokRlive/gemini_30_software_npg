/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.prompt.PromptSupport;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceComparator;

/**
 * The panel for configuring basic settings in a {@linkplain ScadaPoint}.
 */
public class ScadaPointBasicPanel extends JPanel {

  private final PresentationModel<? extends ScadaPoint> pm;
  private final SelectionInList<ScadaPointSource> resSelectionModel;

  private final JFormattedTextField customProtocolIDField;

  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private ScadaPointBasicPanel() {
    initComponents();
    pm = null;
    resSelectionModel = null;
    customProtocolIDField = null;
  }
  
  public ScadaPointBasicPanel(PresentationModel<? extends ScadaPoint> pm,
      SelectionInList<ScadaPointSource> sourceSelectionModel) {
    this(pm, sourceSelectionModel, null);
  }

  public ScadaPointBasicPanel(PresentationModel<? extends ScadaPoint> pm,
      SelectionInList<ScadaPointSource> sourceSelectionModel, JFormattedTextField customProtocolIDField) {
    
    this.customProtocolIDField = customProtocolIDField;
    this.pm = pm;
    this.resSelectionModel = sourceSelectionModel;
    
    initComponents();
    initComponentsBinding();
    initComponentCustom();
  }

  private void btnSelectActionPerformed(ActionEvent e) {
    List<ScadaPointSource> sourceList = new ArrayList<ScadaPointSource>(resSelectionModel.getList());
    sourceList.removeAll(Collections.singleton(null)); // remove null;
    Collections.sort(sourceList, ScadaPointSourceComparator.INSTANCE);
    
    ScadaPointSource ret = ScadaSourceSelectorDialog.showDialog(
        SwingUtilities.getWindowAncestor(this),
        sourceList, 
        resSelectionModel.getSelection());
    
    
    if (ret != null) {
      resSelectionModel.setSelection(ret);
    }
  }

  public void setProtocolIDText(String text) {
    labelProtocolID.setText(text);
  }

  public String getProtocolIDText() {
    return labelProtocolID.getText();
  }

  private void initComponentsBinding() {
    Bindings.bind(tfDescription, pm.getBufferedModel(ScadaPoint.PROPERTY_CUSTOM_DESCRIPTION));
    Bindings.bind(lblSesnName, pm.getBufferedModel(ScadaPoint.PROPERTY_SESSION_NAME));
    Bindings.bind(lblName, pm.getBufferedModel(ScadaPoint.PROPERTY_NAME));
    Bindings.bind(comboVPoint, resSelectionModel);
    Bindings.bind(ftfProtocolID,  pm.getBufferedModel(ScadaPoint.PROPERTY_POINT_ID));
  }

  private void initComponentCustom() {
    PromptSupport.setPrompt("Custom description (optional)", tfDescription);



    // Align protocol ID spinner to the left

    ValidationComponentUtils.setMessageKey(ftfProtocolID, ScadaPoint.PROPERTY_POINT_ID);
  }

  private void createUIComponents() {
    ftfProtocolID = customProtocolIDField == null ? new JFormattedTextField() : customProtocolIDField;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    separatorRes = new JXTitledSeparator();
    label3 = new JLabel();
    lblName = new JLabel();
    label2 = new JLabel();
    lblSesnName = new JLabel();
    labelMapto = new JLabel();
    comboVPoint = new JComboBox<>();
    btnSelect = new JButton();
    tfDescription = new JTextField();
    label1 = new JLabel();
    labelProtocolID = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "right:55dlu, $lcgap, [91dlu,min], $lcgap, default:grow, $lcgap, 10dlu",
        "default, 2*($lgap, fill:default), $ugap, 2*(default, $rgap), default, $lgap, default"));

    //---- separatorRes ----
    separatorRes.setTitle("Basic");
    add(separatorRes, CC.xywh(1, 1, 7, 1));

    //---- label3 ----
    label3.setText("Name:");
    add(label3, CC.xy(1, 3));
    add(lblName, CC.xywh(3, 3, 3, 1));

    //---- label2 ----
    label2.setText("Session:");
    add(label2, CC.xy(1, 5));
    add(lblSesnName, CC.xywh(3, 5, 3, 1, CC.DEFAULT, CC.FILL));

    //---- labelMapto ----
    labelMapto.setText("Source:");
    add(labelMapto, CC.xy(1, 7));
    add(comboVPoint, CC.xywh(3, 7, 3, 1));

    //---- btnSelect ----
    btnSelect.setText("...");
    btnSelect.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnSelectActionPerformed(e);
      }
    });
    add(btnSelect, CC.xy(7, 7));
    add(tfDescription, CC.xywh(3, 9, 3, 1));

    //---- label1 ----
    label1.setText("Description:");
    add(label1, CC.xy(1, 9));

    //---- labelProtocolID ----
    labelProtocolID.setText("Protocol ID:");
    add(labelProtocolID, CC.xy(1, 11));
    add(ftfProtocolID, CC.xy(3, 11));
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTitledSeparator separatorRes;
  private JLabel label3;
  private JLabel lblName;
  private JLabel label2;
  private JLabel lblSesnName;
  private JLabel labelMapto;
  private JComboBox<Object> comboVPoint;
  private JButton btnSelect;
  private JTextField tfDescription;
  private JLabel label1;
  private JLabel labelProtocolID;
  private JFormattedTextField ftfProtocolID;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
