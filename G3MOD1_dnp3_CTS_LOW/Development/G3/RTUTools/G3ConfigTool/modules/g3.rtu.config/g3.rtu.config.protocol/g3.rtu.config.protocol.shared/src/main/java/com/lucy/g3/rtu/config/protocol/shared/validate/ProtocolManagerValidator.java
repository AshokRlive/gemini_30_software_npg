/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.validate;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ICleaner;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The validator for validating SCADA protocol manager.
 */
public class ProtocolManagerValidator extends AbstractContainerValidator<IProtocolManager<?>> {

  public ProtocolManagerValidator(IProtocolManager<?> target) {
    super(target);
  }

  @Override
  public String getTargetName() {
    return target.getName();
  }

  @Override
  public Collection<IValidation> getValidatableChildrenItems() {
    ArrayList<IValidation> validationItems = new ArrayList<>();
    Collection<? extends IProtocol<?>> protocols = target.getAllItems();
    validationItems.addAll(protocols);
    
    return validationItems;
  }

  @Override
  public ICleaner getCleaner() {
    return null;
  }
}
