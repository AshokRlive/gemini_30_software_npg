/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.schema.support.G3SchemaAgent;


/**
 * For supporting copy&paste SCADA Points between IO Maps.
 */
class ScadaIoMapCopySupport {
  private final G3SchemaAgent schema = new G3SchemaAgent();
  
  private Object createIoMapXmlNode(ProtocolType type){
    switch (type) {
    case S101:
      return schema.getS101Session().iomap.append();
      
    case S104:
      return schema.getS104Session().iomap.append();
      
    case SDNP3:
      return schema.getSDNP3Session().iomap.append();
      
    default:
      throw new UnsupportedOperationException("Not implemented for: "+type);
    }
  }

  public static void copyIoMap(ProtocolType type, IScadaIoMap<? extends ScadaPoint> from, IScadaIoMap<? extends ScadaPoint> to) {
    Preconditions.checkNotNull(from, "from must not be null");
    Preconditions.checkNotNull(to,   "to must not be null");
    to.clearIoMap();
    
    ScadaIoMapCopySupport support = new ScadaIoMapCopySupport();
    Object xml = support.createIoMapXmlNode(type);
    from.writeToXML(xml);
    
    to.clearIoMap();
    ScadaProtocolManager manager = (ScadaProtocolManager) to.getSession().getProtocolChannel().getProtocol().getManager();
    to.readFromXML(xml, manager.getSourceRepository());
  }
}

