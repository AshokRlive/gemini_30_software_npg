/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr;

public class Failover {

  private String ipAddress;
  private int port;
  private long masterAddress;


  public Failover(String ipAddress, int port, int masterAddress) {
    this.ipAddress = ipAddress;
    this.port = port;
    this.masterAddress = masterAddress;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public long getMasterAddress() {
    return masterAddress;
  }

  public void setMasterAddress(long masterAddress) {
    this.masterAddress = masterAddress;
  }
}
