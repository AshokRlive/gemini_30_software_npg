/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;


/**
 * The Interface IIOMapGenerator.
 */
public interface IIOMapGenerator {
  /**
   * Updates existing IOs in the iomap but do not add new IOs.
   * @param iomap
   * @param option
   */
  void update(IIoMap iomap, Object option);
  
  /**
   * Generates new IOs in the iomap, overwrite existing IOs if exists.
   * @param iomap
   * @param option
   */
  void generate(IIoMap iomap, Object option);
  
  /**
   * Gets the option objects for users to select.
   * @return
   */
  Object[] getGenerateOptions();
}

