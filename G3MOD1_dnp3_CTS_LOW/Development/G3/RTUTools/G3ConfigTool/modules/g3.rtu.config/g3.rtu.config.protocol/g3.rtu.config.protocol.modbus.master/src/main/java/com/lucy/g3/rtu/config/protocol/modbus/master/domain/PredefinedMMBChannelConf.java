/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import java.util.HashMap;
import java.util.Map;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.IPredefinedChannelConfig;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * The predefined configuration for a ModBus IO channel.
 */
class PredefinedMMBChannelConf implements IPredefinedChannelConfig {

  private final MMBIoChannel channel;


  public PredefinedMMBChannelConf(MMBIoChannel channel) {
    super();
    this.channel = Preconditions.checkNotNull(channel, "channel must not be null");
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return DefaultCreateOption.CreateVirtualPointAndScadaPoint;
  }

  @Override
  public String getRawUnit() {
    return getUnitForScaling(1.0D);
  }

  @Override
  public double getRawMin() {
    return -1000;
  }

  @Override
  public double getRawMax() {
    return 1000;
  }

  @Override
  public String getUnitForScaling(Double scalingFactor) {
    return channel.getUnit();
  }

  @Override
  public Map<Double, String> getPredefindScalingUnit() {
    return new HashMap<Double, String>();
  }

  @Override
  public Double getDefaultScalingFactor() {
    return 1.0D;
  }

}
