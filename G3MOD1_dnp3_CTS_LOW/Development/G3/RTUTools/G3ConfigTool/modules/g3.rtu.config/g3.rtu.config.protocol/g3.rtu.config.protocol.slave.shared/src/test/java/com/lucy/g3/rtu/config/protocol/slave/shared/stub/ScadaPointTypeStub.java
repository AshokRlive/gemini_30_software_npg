/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.stub;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;

public class ScadaPointTypeStub implements ScadaPointType {

  public static ScadaPointTypeStub create() {
    return new ScadaPointTypeStub();
  }

  @Override
  public boolean isInput() {
    return false;
  }

  @Override
  public boolean isOutput() {
    return false;
  }

  @Override
  public String getDescription() {
    return "AI";
  }

  @Override
  public long getMaximumPointID() {
    return 0;
  }

  @Override
  public long getMinimumPointID() {
    return Long.MAX_VALUE;
  }

}
