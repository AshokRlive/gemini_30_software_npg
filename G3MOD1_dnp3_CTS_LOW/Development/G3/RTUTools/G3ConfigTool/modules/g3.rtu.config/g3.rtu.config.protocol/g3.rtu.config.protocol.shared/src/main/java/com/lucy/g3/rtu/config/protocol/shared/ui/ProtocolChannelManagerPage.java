/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.shared.base.page.IItemListManagerModel;
import com.lucy.g3.rtu.config.shared.base.page.ItemListManagerPage;

/**
 * The basic implementation of a page for managing protocol channels.
 */
public class ProtocolChannelManagerPage extends ItemListManagerPage {

  public ProtocolChannelManagerPage(IProtocol<?> protocol, IItemListManagerModel model) {
    super(protocol,
        protocol.getProtocolName(), // Tree node name
        "Channel List", // Page title
        model);
  }

}