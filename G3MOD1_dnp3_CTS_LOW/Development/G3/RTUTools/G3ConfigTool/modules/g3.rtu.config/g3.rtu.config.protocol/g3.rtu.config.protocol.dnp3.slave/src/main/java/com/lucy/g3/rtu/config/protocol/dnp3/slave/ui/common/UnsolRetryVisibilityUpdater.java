/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.common;

import com.lucy.g3.rtu.config.shared.base.misc.ComponentGroup;
import com.lucy.g3.xml.gen.common.DNP3Enum.UNSOLICITED_RETRY_MODE;


/**
 * For updating the components visibility upon the change of retry mode.
 */
public class UnsolRetryVisibilityUpdater {
  private final ComponentGroup groupMaxRetries;
  private final ComponentGroup groupRetryDelay;
  private final ComponentGroup groupOfflineRetryDelay;
  
  public UnsolRetryVisibilityUpdater(
      ComponentGroup groupMaxRetries,
      ComponentGroup groupRetryDelay,
      ComponentGroup groupOfflineRetryDelay) {
    super();
    this.groupMaxRetries = groupMaxRetries;
    this.groupRetryDelay = groupRetryDelay;
    this.groupOfflineRetryDelay = groupOfflineRetryDelay;
  }

  public void update(UNSOLICITED_RETRY_MODE mode) {
    groupOfflineRetryDelay.setVisible(false);
    groupRetryDelay.setVisible(false);
    groupMaxRetries.setVisible(false);
    
    if (mode == null)
      return;
    
    switch (mode) {
    case UNSOLICITED_RETRY_MODE_SIMPLE_RETRY:
      groupRetryDelay.setVisible(true);
      groupMaxRetries.setVisible(true);
      break;
      
    case UNSOLICITED_RETRY_MODE_WITH_OFFLINE:
      groupOfflineRetryDelay.setVisible(true);
      groupRetryDelay.setVisible(true);
      groupMaxRetries.setVisible(true);
      break;
      
    case UNSOLICITED_RETRY_MODE_UNLIMITED:
      groupRetryDelay.setVisible(true);
      break;
      
    default:
      break;
    }
  }
}

