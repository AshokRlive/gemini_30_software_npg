/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
public class IEC870SessionConfigPanel extends JXTaskPane {

  private final PresentationModel<? extends IEC870Session> pm;
  
  @SuppressWarnings("unused")
  private IEC870SessionConfigPanel() {
    super("General");
    this.pm = new PresentationModel<>();
    initComponents(); 
  }
  
  public IEC870SessionConfigPanel(PresentationModel<? extends IEC870Session> pm) {
    super("General");
    this.pm = pm;
    initComponents();
  }

  public JLabel getLabel6() {
    return label6;
  }

  public JLabel getLabel7() {
    return label7;
  }

  private void createUIComponents() {
    ComponentsFactory factory = new ComponentsFactory(pm.getBean().getConstraints(), IEC870Constraints.PREFIX_SESSION, pm);

    tfSessionName = factory.createTextField(IEC870Session.PROPERTY_SESSION_NAME);

    ftfASDUAddress = factory.createNumberField(IEC870Session.PROPERTY_ASDU_ADDRESS);
    
    ftfCyclicPeriod = factory.createNumberField(IEC870Session.PROPERTY_CYCLIC_PERIOD);
    ftfCyclicFirstPeriod = factory.createNumberField(IEC870Session.PROPERTY_CYCLIC_FIRST_PERIOD);
    ftfBackgroundPeriod = factory.createNumberField(IEC870Session.PROPERTY_BACKGROUND_PERIOD);
    ftfSelectTimeout = factory.createNumberField(IEC870Session.PROPERTY_SELECT_TIMEOUT);
    ftfDefResponseTimeout = factory.createNumberField(IEC870Session.PROPERTY_DEFAULT_RESPONSE_TIMEOUT);
 

    cbSendClkSynEvents = factory.createCheckBox(IEC870Session.PROPERTY_SEND_CLOCK_SYNC_EVENTS);
    cbDelOldestEvent = factory.createCheckBox(IEC870Session.PROPERTY_DELETE_OLDEST_EVENT);
    cbCseUseActTerm = factory.createCheckBox(IEC870Session.PROPERTY_CSEUSEACTTERM);
    cbCmdUseActTerm = factory.createCheckBox(IEC870Session.PROPERTY_CMDUSEACTTERM);
    cbStrictAdherence = factory.createCheckBox(IEC870Session.PROPERTY_MULTIPLE_PMEN);
    cbUseDayOfWeek = factory.createCheckBox(IEC870Session.PROPERTY_USE_DAYOFWEEK);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    lblSessionName = new JLabel();
    labelsource = new JLabel();
    labelAppConfirmTimeout = new JLabel();
    labelms4 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    labelLinkStatusPeriod = new JLabel();
    labelms = new JLabel();
    label4 = new JLabel();
    labelms5 = new JLabel();
    label5 = new JLabel();
    labelms6 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "right:default, $lcgap, [50dlu,default], left:3dlu, default, 10dlu, left:default, 2*($lcgap, default:grow)",
        "3*(default, $lgap), default, $rgap, default, $lgap, 2*(default, $rgap), default"));

    //---- lblSessionName ----
    lblSessionName.setText("Session Name:");
    add(lblSessionName, CC.xy(1, 1));
    add(tfSessionName, CC.xy(3, 1));

    //---- labelsource ----
    labelsource.setText("ASDU Address:");
    add(labelsource, CC.xy(1, 3));

    //---- ftfASDUAddress ----
    ftfASDUAddress.setColumns(8);
    add(ftfASDUAddress, CC.xy(3, 3));

    //---- cbSendClkSynEvents ----
    cbSendClkSynEvents.setText("Send Clock Sync Events");
    cbSendClkSynEvents.setOpaque(false);
    add(cbSendClkSynEvents, CC.xywh(7, 5, 3, 1));

    //---- labelAppConfirmTimeout ----
    labelAppConfirmTimeout.setText("Cyclic Period:");
    add(labelAppConfirmTimeout, CC.xy(1, 7));

    //---- ftfCyclicPeriod ----
    ftfCyclicPeriod.setColumns(8);
    add(ftfCyclicPeriod, CC.xy(3, 7));

    //---- labelms4 ----
    labelms4.setText("ms");
    add(labelms4, CC.xy(5, 7));

    //---- cbDelOldestEvent ----
    cbDelOldestEvent.setText("Delete Oldest Event");
    cbDelOldestEvent.setOpaque(false);
    add(cbDelOldestEvent, CC.xywh(7, 7, 3, 1));

    //---- label6 ----
    label6.setText("Cyclic First Period:");
    add(label6, CC.xy(1, 9));

    //---- ftfCyclicFirstPeriod ----
    ftfCyclicFirstPeriod.setColumns(8);
    add(ftfCyclicFirstPeriod, CC.xy(3, 9));

    //---- label7 ----
    label7.setText("ms");
    add(label7, CC.xy(5, 9));

    //---- cbCseUseActTerm ----
    cbCseUseActTerm.setText("Send ACT TERM upon completion of set point commands ");
    cbCseUseActTerm.setToolTipText(" Specify whether to send ACT TERM upon completion of set point commands");
    cbCseUseActTerm.setOpaque(false);
    add(cbCseUseActTerm, CC.xywh(7, 9, 5, 1));

    //---- labelLinkStatusPeriod ----
    labelLinkStatusPeriod.setText("Background Period:");
    add(labelLinkStatusPeriod, CC.xy(1, 11));

    //---- ftfBackgroundPeriod ----
    ftfBackgroundPeriod.setColumns(8);
    add(ftfBackgroundPeriod, CC.xy(3, 11));

    //---- labelms ----
    labelms.setText("ms");
    add(labelms, CC.xy(5, 11));

    //---- cbCmdUseActTerm ----
    cbCmdUseActTerm.setText("Send ACT TERM upon completion of commands other than set point commands ");
    cbCmdUseActTerm.setOpaque(false);
    add(cbCmdUseActTerm, CC.xywh(7, 11, 5, 1));

    //---- label4 ----
    label4.setText("Select Timeout:");
    add(label4, CC.xy(1, 13));
    add(ftfSelectTimeout, CC.xy(3, 13));

    //---- labelms5 ----
    labelms5.setText("ms");
    add(labelms5, CC.xy(5, 13));

    //---- cbStrictAdherence ----
    cbStrictAdherence.setText("Multiple PMEN");
    cbStrictAdherence.setOpaque(false);
    cbStrictAdherence.setToolTipText("Send PMENA/B/C in multiple point response.");
    add(cbStrictAdherence, CC.xywh(7, 13, 3, 1));

    //---- label5 ----
    label5.setText("Default Response Timeout:");
    add(label5, CC.xy(1, 15));
    add(ftfDefResponseTimeout, CC.xy(3, 15));

    //---- labelms6 ----
    labelms6.setText("ms");
    add(labelms6, CC.xy(5, 15));

    //---- cbUseDayOfWeek ----
    cbUseDayOfWeek.setText("Use Day Of Week");
    cbUseDayOfWeek.setOpaque(false);
    cbUseDayOfWeek.setToolTipText("Send PMENA/B/C in multiple point response.");
    add(cbUseDayOfWeek, CC.xywh(7, 15, 3, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel lblSessionName;
  private JTextField tfSessionName;
  private JLabel labelsource;
  private JFormattedTextField ftfASDUAddress;
  private JCheckBox cbSendClkSynEvents;
  private JLabel labelAppConfirmTimeout;
  private JFormattedTextField ftfCyclicPeriod;
  private JLabel labelms4;
  private JCheckBox cbDelOldestEvent;
  private JLabel label6;
  private JFormattedTextField ftfCyclicFirstPeriod;
  private JLabel label7;
  private JCheckBox cbCseUseActTerm;
  private JLabel labelLinkStatusPeriod;
  private JFormattedTextField ftfBackgroundPeriod;
  private JLabel labelms;
  private JCheckBox cbCmdUseActTerm;
  private JLabel label4;
  private JFormattedTextField ftfSelectTimeout;
  private JLabel labelms5;
  private JCheckBox cbStrictAdherence;
  private JLabel label5;
  private JFormattedTextField ftfDefResponseTimeout;
  private JLabel labelms6;
  private JCheckBox cbUseDayOfWeek;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
