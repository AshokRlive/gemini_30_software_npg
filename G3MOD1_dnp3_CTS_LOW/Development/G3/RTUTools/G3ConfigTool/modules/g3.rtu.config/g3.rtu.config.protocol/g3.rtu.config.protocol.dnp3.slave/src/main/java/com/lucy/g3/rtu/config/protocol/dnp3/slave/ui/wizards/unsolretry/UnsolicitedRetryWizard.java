/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.wizards.unsolretry;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;


/**
 * A wizard for configuring <code>UnsolRetry</code>.
 */
public class UnsolicitedRetryWizard {
  private static final int WIDTH  = 500;
  private static final int HEIGHT = 300;
  
  public static void showWizard(SDNP3Session.UnsolRetry bean) {
    Wizard wizard = WizardPage.createWizard(
        new WizardPage[]{new StepInitial(bean)},
        new ResultProducer(bean));
    WizardDisplayer.showWizard(wizard, WizardUtils.createRect(WindowUtils.getMainFrame(),WIDTH, HEIGHT));
  }
}

