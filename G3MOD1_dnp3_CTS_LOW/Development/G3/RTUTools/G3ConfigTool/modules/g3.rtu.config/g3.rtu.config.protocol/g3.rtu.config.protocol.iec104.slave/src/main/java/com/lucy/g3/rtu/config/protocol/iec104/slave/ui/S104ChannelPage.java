/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.ui;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104ChannelConf;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104ChannelTCPConf;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Redundancy;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Session;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.panels.S104RedundancyPanel;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Enums;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolItemManagerPanel;
import com.lucy.g3.rtu.config.protocol.shared.ui.panels.HintPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigListPage;

/**
 * The page of S104 channel.
 */
public class S104ChannelPage extends AbstractConfigListPage {

  private final S104SessionManagerModel sessionMgrModel;

  private final PresentationModel<S104ChannelConf> pmChannel;
  private final PresentationModel<S104Redundancy> pmRedundancy;
  private final PresentationModel<S104ChannelTCPConf> pmTcp;

  private final S104Channel channel;


  public S104ChannelPage(S104Channel channel) {
    super(channel);

    this.channel = channel;

    sessionMgrModel = new S104SessionManagerModel(channel);
    pmChannel = new PresentationModel<>(channel.getChannelConfig());
    pmTcp = new PresentationModel<>(channel.getChannelConfig().getTcpConf());
    this.pmRedundancy = new PresentationModel<>(channel.getRedundancy());

    // Bind node name to channel name.
    ValueModel vm = pmChannel.getModel(S104ChannelConf.PROPERTY_CHANNEL_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }

  @Override
  protected void init() throws Exception {
    super.init();
    initComponents();
    initComponentStates();
    populate();
  }

  private void initComponentStates() {
    /*Bind enable state to redundancy enabled, see #3347*/
    ValueModel redundancyDisabledVM = ConverterFactory.createBooleanNegator(
        pmRedundancy.getModel(S104Redundancy.PROPERTY_ENABLED));
    PropertyConnector.connectAndUpdate(redundancyDisabledVM, cbDiscardFramesOnDisconnect, "enabled");
    PropertyConnector.connectAndUpdate(redundancyDisabledVM, label8, "enabled");
    
    redundancyDisabledVM.addValueChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getNewValue() == Boolean.FALSE) {
          cbDiscardFramesOnDisconnect.setSelected(false);
        }
      }
    });
    
  }

  @Override
  public Action[] getContextActions() {
    return sessionMgrModel.getNodeActions();
  }

  private void createUIComponents() {
    ComponentsFactory factory;
    
    panelGeneral = new JXTaskPane("General");
    
    // ================== Create TCP components ===================
    factory = new ComponentsFactory(IEC870Constraints.INSTANCE_104, IEC870Constraints.PREFIX_CHANNEL_TCP, pmTcp);

    cbAnyIP = factory.createCheckBox(S104ChannelTCPConf.PROPERTY_ANY_IP_ENABLED);
    ftfIPAddress =
        factory.createIpField(S104ChannelTCPConf.PROPERTY_IP_ADDRESS, S104ChannelTCPConf.PROPERTY_ANY_IP_ENABLED);
    ftfPort = factory.createNumberField(S104ChannelTCPConf.PROPERTY_IP_PORT);
    ftfConnectionTimeout = factory.createNumberField(S104ChannelTCPConf.PROPERTY_IP_CONNECT_TIMEOUT);
    cbConnectionMode = factory.createComboBox(S104ChannelTCPConf.PROPERTY_MODE, IEC870Enums.getTCPModeEnums());
    cbDisOnNewSync      = factory.createCheckBox(S104ChannelTCPConf.PROPERTY_DISCONNECT_ON_NEW_SYNC);

    // ================== Create channel components ===================
    factory = new ComponentsFactory(IEC870Constraints.INSTANCE_104, IEC870Constraints.PREFIX_CHANNEL, pmChannel);

    tfChannelName = factory.createTextField(S104ChannelConf.PROPERTY_CHANNEL_NAME);
    ftfTransmitted = factory.createNumberField(S104ChannelConf.PROPERTY_TX_FRAME_SIZE);
    ftfReceived = factory.createNumberField(S104ChannelConf.PROPERTY_RX_FRAME_SIZE);
    ftfT1AckPeriod = factory.createNumberField(S104ChannelConf.PROPERTY_T1_ACK_PERIOD);
    ftfT2sFramePeriod = factory.createNumberField(S104ChannelConf.PROPERTY_T2_FRAME_PERIOD);
    ftfT3TestPeriod = factory.createNumberField(S104ChannelConf.PROPERTY_T3_TEST_PERIOD);
    ftfOfflinePollPeriod = factory.createNumberField(S104ChannelConf.PROPERTY_OFFLINE_POLL_PERIOD);
    ftfKValue = factory.createNumberField(S104ChannelConf.PROPERTY_KVALUE);
    ftfWValue = factory.createNumberField(S104ChannelConf.PROPERTY_WVALUE);
    cbDiscardFramesOnDisconnect = factory.createCheckBox(S104ChannelConf.PROPERTY_DISCARD_FRAMES_ON_DISCONNECT);
    ftfIncrementalTimeout = factory.createNumberField(S104ChannelConf.PROPERTY_INCREMENTAL_TIMEOUT);

    // Create session manager panel
    sessionListPanel = new ProtocolItemManagerPanel(
        sessionMgrModel.getSelectionInList(),
        sessionMgrModel.getPageActions(),
        getViewAction());

    panelHint = new HintPanel(S104ChannelPage.class);
    
    // Redundancy
    ValueModel vm = pmRedundancy.getModel(S104Redundancy.PROPERTY_ENABLED);
    panelRedundancyConfig = new S104RedundancyPanel(channel.getRedundancy());
    cboxRedundancyEnable = ComponentsFactory.createCheckBox(vm, "");
    
    // Bind "visible" property of Redundancy Panel
    PropertyConnector.connectAndUpdate(vm, panelRedundancyConfig, "visible");
  }

  private void populate() {
    settingsPane.add(panelGeneral);
    settingsPane.add(panelPhysicalLayer);
    settingsPane.add(panelLinkLayer);
    settingsPane.add(panelRedundancy);
  }

  @Override
  protected SelectionInList<?> getSelectionInList() {
    return sessionMgrModel.getSelectionInList();
  }

  @Override
  protected ListModel<S104Session> getSelsectionListModel() {
    return channel.getItemListModel();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    JScrollPane scrollPane0 = new JScrollPane();
    settingsPane = new JXTaskPaneContainer();
    labelChnName = new JLabel();
    panelLinkLayer = new JXTaskPane("Link Layer");
    lblRxFrameSize = new JLabel();
    labeloctets0 = new JLabel();
    lblTxFrameSize = new JLabel();
    labeloctets1 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    lblT1AckPeriodMs = new JLabel();
    label6 = new JLabel();
    label1 = new JLabel();
    label5 = new JLabel();
    label2 = new JLabel();
    label4 = new JLabel();
    label13 = new JLabel();
    label11 = new JLabel();
    label14 = new JLabel();
    label12 = new JLabel();
    label7 = new JLabel();
    label3 = new JLabel();
    label8 = new JLabel();
    panelPhysicalLayer = new JXTaskPane("Physical Layer");
    labelIPAdd = new JLabel();
    labelPort = new JLabel();
    labelConTimeout = new JLabel();
    labelCTMS = new JLabel();
    labelMode = new JLabel();
    panelRedundancy = new JXTaskPane("Redundancy");

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane0 ========
      {
        scrollPane0.setBorder(null);
        scrollPane0.setViewportView(settingsPane);
      }
      tabbedPane1.addTab("Channel Settings", scrollPane0);
      tabbedPane1.addTab("Sessions", sessionListPanel);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    add(panelHint, BorderLayout.SOUTH);

    //======== panelGeneral ========
    {
      panelGeneral.setOpaque(false);
      panelGeneral.setLayout(new FormLayout(
        "default, [80dlu,default]",
        "default"));

      //---- labelChnName ----
      labelChnName.setText("Channel Name:");
      panelGeneral.add(labelChnName, CC.xy(1, 1));

      //---- tfChannelName ----
      tfChannelName.setColumns(8);
      panelGeneral.add(tfChannelName, CC.xy(2, 1));
    }

    //======== panelLinkLayer ========
    {
      panelLinkLayer.setLayout(new FormLayout(
        "right:default, $lcgap, [80dlu,default], $lcgap, pref, $lcgap, default",
        "default, $lgap, default, $pgap, fill:default, $rgap, 4*(default, $lgap), default, $pgap, default, $lgap, default"));

      //---- lblRxFrameSize ----
      lblRxFrameSize.setText("Transmitted Frame:");
      panelLinkLayer.add(lblRxFrameSize, CC.xy(1, 1));
      panelLinkLayer.add(ftfTransmitted, CC.xy(3, 1));

      //---- labeloctets0 ----
      labeloctets0.setText("octets");
      panelLinkLayer.add(labeloctets0, CC.xy(5, 1));

      //---- lblTxFrameSize ----
      lblTxFrameSize.setText("Received Frame:");
      panelLinkLayer.add(lblTxFrameSize, CC.xy(1, 3));
      panelLinkLayer.add(ftfReceived, CC.xy(3, 3));

      //---- labeloctets1 ----
      labeloctets1.setText("octets");
      panelLinkLayer.add(labeloctets1, CC.xy(5, 3));

      //---- label9 ----
      label9.setText("Offline Poll Period -- t0:");
      panelLinkLayer.add(label9, CC.xy(1, 5));
      panelLinkLayer.add(ftfOfflinePollPeriod, CC.xy(3, 5));

      //---- label10 ----
      label10.setText("ms");
      panelLinkLayer.add(label10, CC.xy(5, 5));

      //---- lblT1AckPeriodMs ----
      lblT1AckPeriodMs.setText("Ack Period -- t1:");
      panelLinkLayer.add(lblT1AckPeriodMs, CC.xy(1, 7));
      panelLinkLayer.add(ftfT1AckPeriod, CC.xy(3, 7, CC.FILL, CC.DEFAULT));

      //---- label6 ----
      label6.setText("ms");
      panelLinkLayer.add(label6, CC.xy(5, 7));

      //---- label1 ----
      label1.setText("SFrame Period -- t2:");
      panelLinkLayer.add(label1, CC.xy(1, 9));
      panelLinkLayer.add(ftfT2sFramePeriod, CC.xy(3, 9, CC.FILL, CC.DEFAULT));

      //---- label5 ----
      label5.setText("ms");
      panelLinkLayer.add(label5, CC.xy(5, 9));

      //---- label2 ----
      label2.setText("Test Period -- t3:");
      panelLinkLayer.add(label2, CC.xy(1, 11));
      panelLinkLayer.add(ftfT3TestPeriod, CC.xy(3, 11, CC.FILL, CC.DEFAULT));

      //---- label4 ----
      label4.setText("ms");
      panelLinkLayer.add(label4, CC.xy(5, 11));

      //---- label13 ----
      label13.setText("k:");
      panelLinkLayer.add(label13, CC.xy(1, 13));
      panelLinkLayer.add(ftfKValue, CC.xy(3, 13));

      //---- label11 ----
      label11.setText("APDUs");
      panelLinkLayer.add(label11, CC.xy(5, 13));

      //---- label14 ----
      label14.setText("w:");
      panelLinkLayer.add(label14, CC.xy(1, 15));
      panelLinkLayer.add(ftfWValue, CC.xy(3, 15));

      //---- label12 ----
      label12.setText("APDUs");
      panelLinkLayer.add(label12, CC.xy(5, 15));

      //---- label7 ----
      label7.setText("Incremental Timeout:");
      panelLinkLayer.add(label7, CC.xy(1, 17));
      panelLinkLayer.add(ftfIncrementalTimeout, CC.xy(3, 17, CC.FILL, CC.DEFAULT));

      //---- label3 ----
      label3.setText("ms");
      panelLinkLayer.add(label3, CC.xy(5, 17));

      //---- label8 ----
      label8.setText("Discard Information Frames on Disconnect :");
      panelLinkLayer.add(label8, CC.xy(1, 19));

      //---- cbDiscardFramesOnDisconnect ----
      cbDiscardFramesOnDisconnect.setOpaque(false);
      cbDiscardFramesOnDisconnect.setToolTipText("Do not set to TRUE when REDUNDANCY is enabled ");
      panelLinkLayer.add(cbDiscardFramesOnDisconnect, CC.xy(3, 19));
    }

    //======== panelPhysicalLayer ========
    {
      panelPhysicalLayer.setLayout(new FormLayout(
        "right:default, $lcgap, [86dlu,default], $ugap, [40dlu,default]",
        "4*(default, $lgap), default"));

      //---- labelIPAdd ----
      labelIPAdd.setText("IP Address:");
      panelPhysicalLayer.add(labelIPAdd, CC.xy(1, 1));
      panelPhysicalLayer.add(ftfIPAddress, CC.xy(3, 1));

      //---- cbAnyIP ----
      cbAnyIP.setText("Any");
      cbAnyIP.setOpaque(false);
      panelPhysicalLayer.add(cbAnyIP, CC.xy(5, 1));

      //---- labelPort ----
      labelPort.setText("Port:");
      panelPhysicalLayer.add(labelPort, CC.xy(1, 3));

      //---- ftfPort ----
      ftfPort.setColumns(4);
      panelPhysicalLayer.add(ftfPort, CC.xy(3, 3));

      //---- labelConTimeout ----
      labelConTimeout.setText("Connection Timeout:");
      panelPhysicalLayer.add(labelConTimeout, CC.xy(1, 5));

      //---- ftfConnectionTimeout ----
      ftfConnectionTimeout.setColumns(8);
      panelPhysicalLayer.add(ftfConnectionTimeout, CC.xy(3, 5));

      //---- labelCTMS ----
      labelCTMS.setText("ms");
      panelPhysicalLayer.add(labelCTMS, CC.xy(5, 5));

      //---- labelMode ----
      labelMode.setText("Mode:");
      panelPhysicalLayer.add(labelMode, CC.xy(1, 7));
      panelPhysicalLayer.add(cbConnectionMode, CC.xy(3, 7));

      //---- cbDisOnNewSync ----
      cbDisOnNewSync.setText("Disconnect on New SYN");
      panelPhysicalLayer.add(cbDisOnNewSync, CC.xy(5, 7));
    }

    //======== panelRedundancy ========
    {
      panelRedundancy.setOpaque(false);
      panelRedundancy.setLayout(new FormLayout(
        "default, $lcgap, 150dlu",
        "default, $lgap, 100dlu, $lgap, default"));

      //---- cboxRedundancyEnable ----
      cboxRedundancyEnable.setText("Enable Redundancy");
      cboxRedundancyEnable.setOpaque(false);
      panelRedundancy.add(cboxRedundancyEnable, CC.xy(1, 1));
      panelRedundancy.add(panelRedundancyConfig, CC.xywh(1, 3, 3, 1));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JXTaskPaneContainer settingsPane;
  private JPanel sessionListPanel;
  private JPanel panelHint;
  private JPanel panelGeneral;
  private JLabel labelChnName;
  private JFormattedTextField tfChannelName;
  private JPanel panelLinkLayer;
  private JLabel lblRxFrameSize;
  private JFormattedTextField ftfTransmitted;
  private JLabel labeloctets0;
  private JLabel lblTxFrameSize;
  private JFormattedTextField ftfReceived;
  private JLabel labeloctets1;
  private JLabel label9;
  private JFormattedTextField ftfOfflinePollPeriod;
  private JLabel label10;
  private JLabel lblT1AckPeriodMs;
  private JFormattedTextField ftfT1AckPeriod;
  private JLabel label6;
  private JLabel label1;
  private JFormattedTextField ftfT2sFramePeriod;
  private JLabel label5;
  private JLabel label2;
  private JFormattedTextField ftfT3TestPeriod;
  private JLabel label4;
  private JLabel label13;
  private JFormattedTextField ftfKValue;
  private JLabel label11;
  private JLabel label14;
  private JFormattedTextField ftfWValue;
  private JLabel label12;
  private JLabel label7;
  private JFormattedTextField ftfIncrementalTimeout;
  private JLabel label3;
  private JLabel label8;
  private JCheckBox cbDiscardFramesOnDisconnect;
  private JPanel panelPhysicalLayer;
  private JLabel labelIPAdd;
  private JFormattedTextField ftfIPAddress;
  private JCheckBox cbAnyIP;
  private JLabel labelPort;
  private JFormattedTextField ftfPort;
  private JLabel labelConTimeout;
  private JFormattedTextField ftfConnectionTimeout;
  private JLabel labelCTMS;
  private JLabel labelMode;
  private JComboBox<Object> cbConnectionMode;
  private JCheckBox cbDisOnNewSync;
  private JPanel panelRedundancy;
  private JCheckBox cboxRedundancyEnable;
  private S104RedundancyPanel panelRedundancyConfig;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
