/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

/**
 * The page of S104 channel list.
 */
public class S101ChannelManagerPage extends ProtocolChannelManagerPage {

  public S101ChannelManagerPage(S101 data) {
    super(data, new S101ChannelManagerModel(data));
  }


  private static final class S101ChannelManagerModel extends AbstractProtocolItemManagerModel {

    private final S101 iec101;

    private final SelectionInList<S101Channel> selectionInList;


    private S101ChannelManagerModel(S101 iec101) {
      this.iec101 = Preconditions.checkNotNull(iec101, "iec101 is null");
      this.selectionInList = new SelectionInList<S101Channel>(iec101.getItemListModel());
    }

    @Override
    public SelectionInList<?> getSelectionInList() {
      return selectionInList;
    }

    @Override
    protected void addActionPerformed() {
      IProtocolChannel result = ProtocolAddingWizards.showAddChannel(iec101);
      if(result != null)
        selectionInList.setSelection((S101Channel) result);
    }

    @Override
    protected void removeActionPerformed() {
      S101Channel sel = selectionInList.getSelection();

      if (sel != null) {
        if (showConfirmRemoveDialog(sel.getChannelName())) {
          iec101.remove(sel);
        }
      }
    }

  }
}
