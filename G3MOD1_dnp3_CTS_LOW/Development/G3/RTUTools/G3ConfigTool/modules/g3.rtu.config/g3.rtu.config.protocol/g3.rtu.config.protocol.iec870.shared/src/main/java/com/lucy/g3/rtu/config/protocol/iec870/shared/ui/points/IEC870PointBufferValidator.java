/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.slave.shared.validation.ScadaPointBufferValidator;

/**
 * Validate the buffer value in IEC870 point presentation model.
 */
class IEC870PointBufferValidator extends ScadaPointBufferValidator {

  public IEC870PointBufferValidator(PresentationModel<IEC870Point> target) {
    super(target);
  }
}
