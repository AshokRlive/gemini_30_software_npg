/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

/**
 * The IScadaIoMap is for managing a list of {@linkplain ScadaPoint}.
 *
 * @param <PointT>
 *          The type of SCADA Point.
 */
public interface IScadaIoMap<PointT extends ScadaPoint> extends IIoMap {

  void addPoint(PointT p) throws DuplicatedException;

  void removePoint(PointT p);

  void removeAllPoints(Collection<PointT> p);

  PointT getPoint(ScadaPointType type, long id);

  Collection<PointT> getPoints(ScadaPointType type);

  /**
   * Checks if a protocol point exists.
   *
   * @param type
   * @param pointId
   * @return
   */
  boolean exists(ScadaPointType type, long pointId);

  /**
   * Finds next available protocol ID which has not being used.
   *
   * @param type
   *          provided protocol point type
   * @return the found protocol ID. 0 if not found within range(0~5000).
   */
  long findAvailablePointID(ScadaPointType type);

  long findAvailablePointID(long startingPoint, ScadaPointType type);

  boolean checkPointID(long pointID, ScadaPointType type);

  int getPointsNum(ScadaPointType type);

  Collection<PointT> getAllInputPoints();

  Collection<PointT> getAllOutputPoints();

  Collection<PointT> getAllPoints();

  ListModel<PointT> getPointListModel(ScadaPointType type);

  ListModel<PointT> getAllInputPointsModel();

  void removeScadaPoint(ScadaPoint point);

  void writeToXML(Object xml);

  void readFromXML(Object xml, ScadaPointSourceRepository sourceRepository);

}
