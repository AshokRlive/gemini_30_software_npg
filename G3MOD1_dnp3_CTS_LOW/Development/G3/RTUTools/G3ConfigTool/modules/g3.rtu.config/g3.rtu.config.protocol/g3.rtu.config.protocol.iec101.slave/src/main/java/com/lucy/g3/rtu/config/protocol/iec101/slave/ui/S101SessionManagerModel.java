/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Session;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

public class S101SessionManagerModel extends AbstractProtocolItemManagerModel {

  private final S101Channel chnl;

  private final SelectionInList<S101Session> selectionInList;


  S101SessionManagerModel(S101Channel chnl) {
    this.chnl = Preconditions.checkNotNull(chnl, "iec101Chnl is null");
    this.selectionInList = new SelectionInList<S101Session>(chnl.getItemListModel());
  }

  @Override
  public SelectionInList<?> getSelectionInList() {
    return selectionInList;
  }

  @Override
  protected void addActionPerformed() {
    IProtocolSession result = ProtocolAddingWizards.showAddSession(chnl);
    if (result != null)
      selectionInList.setSelection((S101Session) result);
  }

  @Override
  protected void removeActionPerformed() {
    S101Session sel = selectionInList.getSelection();
    if (sel != null) {
      if (showConfirmRemoveDialog(sel.getSessionName())) {
        chnl.remove(sel);
      }
    }
  }

}
