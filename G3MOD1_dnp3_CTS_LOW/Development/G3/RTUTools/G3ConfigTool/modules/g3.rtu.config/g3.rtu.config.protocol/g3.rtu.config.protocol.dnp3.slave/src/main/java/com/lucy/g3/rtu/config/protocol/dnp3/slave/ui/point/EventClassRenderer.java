/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;

class EventClassRenderer extends DefaultListCellRenderer {
  final private Long[] evtClasses = DNP3ObjConfig.getEventClassItems();
  final private String[] evtClassDescript = DNP3ObjConfig.getEventClassDescriptItems();

  @Override
  public Component getListCellRendererComponent(JList list, Object value,
      int index, boolean isSelected, boolean cellHasFocus) {
    Component comp = super.getListCellRendererComponent(list, value, index,
        isSelected, cellHasFocus);

    if (value != null) {
      
      for (int i = 0; i < evtClasses.length; i++) {
        if (value.equals(evtClasses[i])) {
          setText(evtClassDescript[i]);
          break;
        }
      }
    } else {
      setText("(No change)");
    }

    return comp;
  }
}
