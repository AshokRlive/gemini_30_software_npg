/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;

/**
 * The source for SCADA output points.
 */
public class ScadaPointOutput extends AbstractScadaPointSource<IOperableLogic> {

  public ScadaPointOutput(IOperableLogic rawSource) {
    super(rawSource);
  }

  @Override
  protected String generateName() {
    return rawSource.getName();
  }

  @Override
  public String getDescription() {
    return rawSource.getDescription();
  }

  @Override
  public int getGroup() {
    return rawSource.getGroup();
  }

  @Override
  public int getId() {
    throw new UnsupportedOperationException("getId() not supported by ScadaPointOutput");
  }

  @Override
  public IOperableLogic getRawSource() {
    return rawSource;
  }

  @Override
  public boolean isAnalogue() {
    return ICLogicType.ANALOGCTRL == (rawSource.getType().getId());
  }

  @Override
  public String getType() {
    return rawSource.getType().getTypeName();
  }
}
