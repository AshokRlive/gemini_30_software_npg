/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.ui.panels;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.jdesktop.swingx.JXTable;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionSingleSelection;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Redundancy;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Redundancy.S104RdcyChannel;
public class S104RedundancyPanel extends JPanel {
  
  @SuppressWarnings("unused")
  private final MultiSelectionSingleSelection singleSelection;
  private final MultiSelectionInList<S104RdcyChannel> selectionList;
  private final MultiListSelectionAdapter<S104RdcyChannel> selectionModel;
  
  private ListModel<S104RdcyChannel> listModel;
  
  private final S104Redundancy redundancy;
  
  /*
   * Required by JFormDesigner
   */
  @SuppressWarnings({ "unused", "unchecked" })
  private S104RedundancyPanel() {
    this.redundancy = null;
    this.listModel = new ArrayListModel<S104RdcyChannel>();
    this.selectionList = new MultiSelectionInList<>(listModel);
    this.singleSelection = new MultiSelectionSingleSelection(selectionList.getSelection());
    this.selectionModel = new MultiListSelectionAdapter<>(selectionList);
    
    init();
  }
  
  public S104RedundancyPanel(S104Redundancy redundancy) {
    this.redundancy = Preconditions.checkNotNull(redundancy, "redundancy must not be null");
    this.listModel = redundancy.getRdcyChannelsListModel();
    this.selectionList = new MultiSelectionInList<>(listModel);
    this.singleSelection = new MultiSelectionSingleSelection(selectionList.getSelection());
    this.selectionModel = new MultiListSelectionAdapter<>(selectionList);
    
    init();
  }

  private void init() {
    initComponents();
    initEventHandling();
    updateButtonStates();
  }

  private void updateButtonStates() {
    btnRemove.setEnabled(selectionList.hasSelection());
  }
  
  private void initEventHandling() {
    selectionList.getSelection().addListDataListener(new ListDataListener() {
      
      @Override
      public void intervalRemoved(ListDataEvent e) {
        updateButtonStates();
      }

      @Override
      public void intervalAdded(ListDataEvent e) {
        updateButtonStates();
      }
      
      @Override
      public void contentsChanged(ListDataEvent e) {
      }
    });
    
  }

  private void createUIComponents() {
    tableRdcy = new JXTable(new RdcyChannelTableModel(listModel));
    Bindings.bind(tableRdcy, listModel, selectionModel);
    
    UIUtils.setDefaultConfig((JXTable)tableRdcy, true, true, false);
  }

  private void btnAddActionPerformed(ActionEvent e) {
    redundancy.add(new S104RdcyChannel());
  }

  private void btnRemoveActionPerformed(ActionEvent e) {
    int ret = JOptionPane.showConfirmDialog(this, 
        "Do you want to remove the selections?",
        "Remove", 
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    
    if (ret == JOptionPane.YES_OPTION) {
      selectionList.getSelection();
      Object[] selected = selectionList.getSelection().toArray();
      for (Object sel : selected) {
        redundancy.remove((S104RdcyChannel) sel);
      }
    }
  }

  private void panel1MouseClicked(MouseEvent e) {
    selectionList.getSelection().clear();
  }
  
  private static class RdcyChannelTableModel extends AbstractTableAdapter<S104RdcyChannel> {
    static final String[] COLUMN_NAMES = new String[]{
      "IP Address",
      "Port"
    };
    static final int COL_IP = 0;
    static final int COL_PORT = 1;
    
    RdcyChannelTableModel(ListModel<S104RdcyChannel> listModel) {
      super(listModel,COLUMN_NAMES);
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      S104RdcyChannel chnl = getRow(rowIndex);
      switch (columnIndex) {
      case COL_IP:
        return chnl.getIpAddress();
      case COL_PORT:
        return chnl.getIpPort();
      default:
        return null;
      }
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      S104RdcyChannel chnl = getRow(rowIndex);
      try {
        switch (columnIndex) {
        case COL_IP:
          chnl.setIpAddress(aValue.toString());
          break;
          
        case COL_PORT:
          chnl.setIpPort(Long.valueOf(aValue.toString()));
          break;
          
        default:
        }
      }catch (Throwable e) {
        MessageDialogs.error("Invalid input value: "+aValue, e);
      }
    }
    

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return true;
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    btnAdd = new JButton();
    btnRemove = new JButton();

    //======== this ========
    setOpaque(false);
    setBorder(new TitledBorder("Redundancy Channels"));
    setLayout(new BorderLayout(10, 10));

    //======== scrollPane1 ========
    {
      scrollPane1.setViewportView(tableRdcy);
    }
    add(scrollPane1, BorderLayout.CENTER);

    //======== panel1 ========
    {
      panel1.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
      panel1.setOpaque(false);
      panel1.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          panel1MouseClicked(e);
        }
      });
      panel1.setLayout(new FormLayout(
          "default",
          "3*(default, $lgap), default"));

      //---- btnAdd ----
      btnAdd.setText("Add");
      btnAdd.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnAddActionPerformed(e);
        }
      });
      panel1.add(btnAdd, CC.xy(1, 1));

      //---- btnRemove ----
      btnRemove.setText("Remove");
      btnRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnRemoveActionPerformed(e);
        }
      });
      panel1.add(btnRemove, CC.xy(1, 3));
    }
    add(panel1, BorderLayout.EAST);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JTable tableRdcy;
  private JPanel panel1;
  private JButton btnAdd;
  private JButton btnRemove;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
