/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IIOMapGeneratorFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.IOMapGeneratorFactories;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

class GenerateIOMapPage extends WizardPage {
  public final static String KEY_OPTIONS_TABLE_MODEL = "optionsTableModel";
  public final static String KEY_CREATE_NEW_IO =  "createNewIO";

  private Logger log = Logger.getLogger(GenerateIOMapPage.class);
  
  private SelectionTableModel<Object> tableModel;
  
  private final IConfig config;
  private final ProtocolType type;
  private final boolean forceToCreateNew;
  
  public GenerateIOMapPage(String title, IConfig config, ProtocolType type) {
    this(title, config, type, false);
  }
  
  public GenerateIOMapPage(IConfig config, ProtocolType type, boolean forceToCreateNew) {
    this(null, config, type, forceToCreateNew);
  }
  
  private GenerateIOMapPage(String title, IConfig config, ProtocolType type, boolean forceToCreateNew) {
    super(title == null ? "Generate IO Map" : title);
    this.type = type;
    this.config = Preconditions.checkNotNull(config, "config must not be null");
    this.tableModel = new SelectionTableModel<>(null, "Template");
    this.tableModel.setSelectionMode(SelectionMode.SELECTION_MODE_SINGLE);
    this.forceToCreateNew = forceToCreateNew;
    putWizardData(KEY_OPTIONS_TABLE_MODEL, tableModel);
    
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = ProtocolAddingWizards.createPanelBuilder(this);
    builder.setBorder(Borders.DLU14_BORDER);
    
    builder.append("Choose a template:");
    builder.nextLine();
    
    final JTable table = new JTable(tableModel);
    table.getColumnModel().getColumn(0).setMaxWidth(50);
    table.setFillsViewportHeight(true);
    table.setShowGrid(false);
    table.setName("template table");
    final JScrollPane pane = new JScrollPane(table);
    pane.setPreferredSize(new Dimension(100,150));
    UIUtils.increaseScrollSpeed(pane);
    builder.append(pane,4);
    builder.nextLine();
    
    JCheckBox cboxNewIO = new JCheckBox("Create new IO if not exits", true);
    cboxNewIO.setName(KEY_CREATE_NEW_IO);
    cboxNewIO.setToolTipText("If set to false, the wizard will only update the configuration of existing IO.");
    builder.append(cboxNewIO);
    builder.nextLine();
    
    if(forceToCreateNew) {
      cboxNewIO.setSelected(true);
      cboxNewIO.setVisible(false);
    }
  }

  @Override
  public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
    /* Validate Create Session*/
    if(tableModel.hasSelection() == false) {
      JOptionPane.showMessageDialog(this, "Please select a template!","Error", JOptionPane.ERROR_MESSAGE);
      return WizardPanelNavResult.REMAIN_ON_PAGE;
    }
    
    return super.allowFinish(stepName, settings, wizard);
  }

  @Override
  public WizardPanelNavResult allowNext(String stepName, Map settings, Wizard wizard) {
    /* Validate Create Session*/
    if(tableModel.hasSelection() == false) {
      JOptionPane.showMessageDialog(this, "Please select a template!","Error", JOptionPane.ERROR_MESSAGE);
      return WizardPanelNavResult.REMAIN_ON_PAGE;
    }
    
    return super.allowNext(stepName, settings, wizard);
  }

  @Override
  protected void renderingPage() {
    ProtocolType selectedType = type;
    if(wizardDataContainsKey(AddProtocolPage.KEY_SELECTED_PROTOCOL_TYPE))
      selectedType = (ProtocolType) getWizardData(AddProtocolPage.KEY_SELECTED_PROTOCOL_TYPE);
    
    IIOMapGeneratorFactory genFactory = IOMapGeneratorFactories.get(selectedType);
    if(genFactory != null) {
      IIOMapGenerator gen = genFactory.getGenerator(config);
      Object[] options = gen.getGenerateOptions();
      tableModel.setItems(Arrays.asList(options));
      if(options.length > 0)
        tableModel.setItemSelected(options[0], true);
    } else {
      log.error("IIOMapGeneratorFactory not found for type: " + selectedType);
    }
  }
  
}
