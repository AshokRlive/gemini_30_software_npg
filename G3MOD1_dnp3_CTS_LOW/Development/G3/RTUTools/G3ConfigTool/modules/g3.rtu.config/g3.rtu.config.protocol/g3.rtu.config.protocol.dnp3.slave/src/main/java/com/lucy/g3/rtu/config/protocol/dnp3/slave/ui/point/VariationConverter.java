/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjVarConsts;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Enums;
import com.lucy.g3.xml.gen.api.IXmlEnum;

/**
 * Convert Variation enum to value and vice verse.
 */
abstract class VariationConverter implements BindingConverter<Long, IXmlEnum> {

  protected final DNP3ObjGroup group;


  private VariationConverter(DNP3ObjGroup group) {
    super();
    this.group = Preconditions.checkNotNull(group, "group must not be null");
  }

  @Override
  public IXmlEnum targetValue(Long sourceValue) {
    IXmlEnum ret;

    if (sourceValue == null
        || sourceValue.intValue() == SDNP3Enums.EMPTY_VARIATION.getValue()) {
      ret = SDNP3Enums.EMPTY_VARIATION;
    } else {
      ret = forValue(sourceValue.intValue());
    }

    if (ret == null) {
      ret = new UnDefinedVariation(sourceValue.intValue(), "");
    }
    return ret;
  }

  @Override
  public Long sourceValue(IXmlEnum targetValue) {
    return targetValue == null ? 0 : (long) targetValue.getValue();
  }

  abstract IXmlEnum forValue(int value);

  public static ValueModel createConvertedValueModel(ValueModel source, DNP3ObjGroup group, boolean isStatic) {
    VariationConverter converter = isStatic ? new StaticVariationConverter(group) : new EventVariationConverter(group);
    return new ConverterValueModel(source, converter);
  }


  /**
   * The variation item that is not defined in DNP3 xml enum.
   */
  private static class UnDefinedVariation implements IXmlEnum {

    private final int value;
    private final String description;


    public UnDefinedVariation(int value, String description) {
      super();
      this.value = value;
      this.description = description;
    }

    @Override
    public String getDescription() {
      return description;
    }

    @Override
    public int getValue() {
      return value;
    }

  }

  private static class StaticVariationConverter extends VariationConverter {

    public StaticVariationConverter(DNP3ObjGroup  group) {
      super(group);
    }

    @Override
    protected IXmlEnum forValue(int value) {
      return DNP3ObjVarConsts.getStaticVariationEnum(group, value);
    }
  }

  /**
   * Convert Event Variation enum to value and vice verse.
   */
  private static class EventVariationConverter extends VariationConverter {

    public EventVariationConverter(DNP3ObjGroup group) {
      super(group);
    }

    @Override
    protected IXmlEnum forValue(int value) {
      return DNP3ObjVarConsts.getEventVariationEnum(group, value);
    }
  }
}
