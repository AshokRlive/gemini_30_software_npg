/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.beans.PropertyVetoException;

import com.g3schema.ns_sdnp3.SDNP3ChnlTCPConfT;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.ConnectionManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_ROLE;

/**
 * The TCP configuration of SDNP3 channel.
 */
public final class SDNP3ChannelTCPConf extends ProtocolChannelTCPConf {

  public static final String PROPERTY_ROLE = "role";
  public static final String PROPERTY_DUAL_ENDPOINT_IP_PORT = "dualEndPointIpPort";
  public static final String PROPERTY_MASTER_ADDRESS = "masterAddress";
  public static final String PROPERTY_LOCAL_UDP_PORT = "localUDPPort";
  public static final String PROPERTY_DEST_UDP_PORT = "destUDPPort";
  public static final String PROPERTY_INIT_UNSOL_UDP_PORT = "initUnsolUDPPort";
  public static final String PROPERTY_VALIDATE_MASTER_IP = "validateMasterIP";

  private final ConnectionManager connectionManager = new ConnectionManager();

  private final SDNP3Constraints constraint = SDNP3Constraints.INSTANCE;
  private final String prefix = SDNP3Constraints.PREFIX_CHANNEL_TCP;

  private long masterAddress = 0;
  private boolean validateMasterIP = false;
  private long dualEndPointIpPort;
  private LU_LINTCP_ROLE role = LU_LINTCP_ROLE.LU_LINTCP_ROLE_OUTSTATION;

  private long localUDPPort = constraint.getDefault(prefix, "localUDPPort");
  private long destUDPPort = constraint.getDefault(prefix, "destUDPPort");
  private long initUnsolUDPPort = constraint.getDefault(prefix, "initUnsolUDPPort");


  public ConnectionManager getConnectionManager() {
    return connectionManager;
  }
  
  public boolean isValidateMasterIP() {
    return validateMasterIP;
  }

  public void setValidateMasterIP(boolean validateMasterIP) {
    Object oldValue = this.validateMasterIP;
    this.validateMasterIP = validateMasterIP;
    firePropertyChange(PROPERTY_VALIDATE_MASTER_IP, oldValue, validateMasterIP);
  }

  public long getMasterAddress() {
    return masterAddress;
  }

  public void setMasterAddress(long masterAddress) {
    Object oldValue = getMasterAddress();
    this.masterAddress = masterAddress;
    firePropertyChange(PROPERTY_MASTER_ADDRESS, oldValue, masterAddress);
  }

  public LU_LINTCP_ROLE getRole() {
    return role;
  }

  public void setRole(LU_LINTCP_ROLE role) {
    Object oldValue = getRole();
    this.role = role;
    firePropertyChange(PROPERTY_ROLE, oldValue, role);
  }

  public long getDualEndPointIpPort() {
    return dualEndPointIpPort;
  }

  public void setDualEndPointIpPort(long dualEndPointIpPort) {
    Object oldValue = getDualEndPointIpPort();
    this.dualEndPointIpPort = dualEndPointIpPort;
    firePropertyChange(PROPERTY_DUAL_ENDPOINT_IP_PORT, oldValue, dualEndPointIpPort);

  }

  public long getLocalUDPPort() {
    return localUDPPort;
  }

  public void setLocalUDPPort(long localUDPPort) throws PropertyVetoException {
    checkBoundary(this.localUDPPort, localUDPPort, "localUDPPort");

    Object oldValue = getLocalUDPPort();
    this.localUDPPort = localUDPPort;
    firePropertyChange(PROPERTY_LOCAL_UDP_PORT, oldValue, localUDPPort);
  }

  public long getDestUDPPort() {
    return destUDPPort;
  }

  public void setDestUDPPort(long destUDPPort) throws PropertyVetoException {
    checkBoundary(this.destUDPPort, destUDPPort, "destUDPPort");

    Object oldValue = getDestUDPPort();
    this.destUDPPort = destUDPPort;
    firePropertyChange(PROPERTY_DEST_UDP_PORT, oldValue, destUDPPort);
  }

  public long getInitUnsolUDPPort() {
    return initUnsolUDPPort;
  }

  public void setInitUnsolUDPPort(long initUnsolUDPPort) throws PropertyVetoException {
    checkBoundary(this.initUnsolUDPPort, initUnsolUDPPort, "initUnsolUDPPort");

    Object oldValue = getInitUnsolUDPPort();
    this.initUnsolUDPPort = initUnsolUDPPort;
    firePropertyChange(PROPERTY_INIT_UNSOL_UDP_PORT, oldValue, initUnsolUDPPort);
  }

  private void checkBoundary(long oldValue, long newValue, String property)
      throws PropertyVetoException {
    constraint.checkPropertyBoundary(this, oldValue, newValue, prefix, property);
  }

  public void writeToXML(SDNP3ChnlTCPConfT xml, VirtualPointWriteSupport support) {
    super.writeToXML(xml);

    xml.masterAddress.setValue((int) getMasterAddress());
    xml.validateMasterIP.setValue(isValidateMasterIP());
    xml.dualEndPointIpPort.setValue(getDualEndPointIpPort());
    xml.disconnectOnNewSyn.setValue(isDisconnectOnNewSync());

    xml.localUDPPort.setValue(getLocalUDPPort());
    xml.destUDPPort.setValue(getDestUDPPort());
    xml.initUnsolUDPPort.setValue(getInitUnsolUDPPort());

    if (connectionManager.isEnabled()) {
      connectionManager.writeToXML(xml.connectionManager.append(), support);
    }
  }

  public void readFromXML(SDNP3ChnlTCPConfT xml, VirtualPointReadSupport support) {
    super.readFromXML(xml);

    setValidateMasterIP(xml.validateMasterIP.getValue());
    setMasterAddress(xml.masterAddress.getValue());
    setDualEndPointIpPort(xml.dualEndPointIpPort.getValue());
    
    if (xml.disconnectOnNewSyn.exists()) {
      setDisconnectOnNewSync(xml.disconnectOnNewSyn.getValue());
    }
    
    try {
      setLocalUDPPort(xml.localUDPPort.getValue());
      setDestUDPPort(xml.destUDPPort.getValue());
      setInitUnsolUDPPort(xml.initUnsolUDPPort.getValue());
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }

    if (xml.connectionManager.exists()) {
      connectionManager.readFromXML(xml.connectionManager.first(), support);
    } else {
      connectionManager.setEnabled(false);
    }
  }
}
