/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;

/**
 * The IEC870 Point type enum.
 */
public enum IEC870PointType implements ScadaPointType {
  // @formatter:off
  MMEA(" Measured Normalized", true, true),
  MMEB("Measured Scaled",     true, true),
  MMEC("Measured Float",      true, true),
  MSP("Single Binary",        true, false),
  MDP("Double Binary",        true, false),
  MIT("Integrated Total",     true, false),
  CSC("Single Command",       false,false),
  CDC("Double Command",       false,false);
  // @formatter:off

  IEC870PointType(String description, boolean isInput, boolean isAnalog) {
    this.description = description;
    this.isInput = isInput;
    this.isAnalog = isAnalog;
  }

  @Override
  public boolean isInput() {
    return isInput;
  }

  @Override
  public boolean isOutput() {
    return !isInput;
  }

  public boolean isAnalog() {
    return isAnalog;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return description;
  }

  @Override
  public long getMaximumPointID() {
    return MAX_ID;
  }

  @Override
  public long getMinimumPointID() {
    return MIN_ID;
  }


  private final String description;
  private final boolean isInput;
  private final boolean isAnalog;

  public static final long MAX_ID = 16777215;
  public static final long MIN_ID = 0;
}
