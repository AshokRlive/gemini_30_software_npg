/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.EventsConfig;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
public class EventPanelAI extends JXTaskPane {
  private EventsConfig ain, ais, aif;

  @SuppressWarnings("unused")
  private EventPanelAI(){
    super("Analogue Point Events");
    this.ain = new EventsConfig(IEC870Constraints.INSTANCE_101);
    this.ais = new EventsConfig(IEC870Constraints.INSTANCE_101);
    this.aif = new EventsConfig(IEC870Constraints.INSTANCE_101);
    
    initComponents();
  }
  
  public EventPanelAI(EventsConfig ain, EventsConfig ais, EventsConfig aif) {
    super("Analogue Point Events");
    this.ain = ain;
    this.ais = ais;
    this.aif = aif;
    initComponents();
  }
  
  private void createUIComponents() {
    panelEventAIN = new EventConfigPanel(ain);
    panelEventAIS = new EventConfigPanel(ais);
    panelEventAIF = new EventConfigPanel(aif);  
    
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label19 = new JLabel();
    label20 = new JLabel();
    label26 = new JLabel();
    panel2 = new JPanel();
    label22 = new JLabel();
    label25 = new JLabel();
    label1 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
      "3*(default, $ugap), default",
      "default, $lgap, fill:default"));

    //---- label19 ----
    label19.setText("- Normalized -");
    label19.setHorizontalAlignment(SwingConstants.CENTER);
    add(label19, CC.xy(3, 1));

    //---- label20 ----
    label20.setText("- Scaled-");
    label20.setHorizontalAlignment(SwingConstants.CENTER);
    add(label20, CC.xy(5, 1));

    //---- label26 ----
    label26.setText("- Float -");
    label26.setHorizontalAlignment(SwingConstants.CENTER);
    add(label26, CC.xy(7, 1));

    //======== panel2 ========
    {
      panel2.setOpaque(false);
      panel2.setLayout(new FormLayout(
        "default",
        "2*(fill:default:grow, $lgap), bottom:default:grow"));

      //---- label22 ----
      label22.setText("Max Events:");
      panel2.add(label22, CC.xy(1, 1));

      //---- label25 ----
      label25.setText("Event Mode:");
      panel2.add(label25, CC.xy(1, 3));

      //---- label1 ----
      label1.setText("Time Format:");
      panel2.add(label1, CC.xy(1, 5));
    }
    add(panel2, CC.xy(1, 3));
    add(panelEventAIN, CC.xy(3, 3));
    add(panelEventAIS, CC.xy(5, 3));
    add(panelEventAIF, CC.xy(7, 3));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label19;
  private JLabel label20;
  private JLabel label26;
  private JPanel panel2;
  private JLabel label22;
  private JLabel label25;
  private JLabel label1;
  private EventConfigPanel panelEventAIN;
  private EventConfigPanel panelEventAIS;
  private EventConfigPanel panelEventAIF;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
