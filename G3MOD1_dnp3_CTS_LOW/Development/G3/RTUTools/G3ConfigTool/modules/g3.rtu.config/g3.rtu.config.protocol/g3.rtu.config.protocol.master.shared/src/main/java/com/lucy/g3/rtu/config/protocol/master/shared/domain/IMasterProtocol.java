/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.domain;

import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The Interface Master Protocol Stack.
 *
 * @param <ChannelT>
 *          the type of Master Protocol Channel.
 */
public interface IMasterProtocol<ChannelT extends IMasterProtocolChannel<?>> extends IProtocol<ChannelT>, IValidation {

  @Override
  MasterProtocolManager getManager();

}
