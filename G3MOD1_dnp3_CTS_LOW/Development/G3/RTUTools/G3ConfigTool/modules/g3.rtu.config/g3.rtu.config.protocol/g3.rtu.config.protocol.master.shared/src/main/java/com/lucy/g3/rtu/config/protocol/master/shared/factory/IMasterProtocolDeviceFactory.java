/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.factory;

import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * A factory for creating IMasterDevice objects.
 */
public interface IMasterProtocolDeviceFactory {

  IMasterProtocolModule createMasterDevice(MODULE_ID id, IMasterProtocolSession session);
}
