/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.util;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;

/**
 * The utility for SCADA protocol.
 */
public class ScadaProtocolUtility implements IConfigUtility {

  public static ArrayList<ScadaPointSource> findSpareSource(
      Collection<? extends ScadaPointSource> sources,
      IScadaIoMap<?> iomap, ScadaPointType type) {

    ArrayList<ScadaPointSource> spared = new ArrayList<ScadaPointSource>(sources);

    // Remove invalid resources which already exist in the current IO map.
    for (ScadaPointSource source : sources) {
      if (checkUsedByIoMap(source, iomap, type)) {
        spared.remove(source);
      }
    }

    return spared;
  }

  private static boolean checkUsedByIoMap(ScadaPointSource source, IScadaIoMap<?> iomap, ScadaPointType type) {
    Collection<? extends ScadaPoint> points = iomap.getAllPoints();
    for (ScadaPoint p : points) {
      if (p.getSource() == source && p.getType() == type) {
        return true;
      }
    }
    return false;
  }

  public static void removeScadaPoint(ScadaPoint item) {
    ScadaPoint point = item;
    IScadaIoMap<? extends ScadaPoint> iomap = point.getIoMap();
    iomap.removeScadaPoint(point);
  }
}
