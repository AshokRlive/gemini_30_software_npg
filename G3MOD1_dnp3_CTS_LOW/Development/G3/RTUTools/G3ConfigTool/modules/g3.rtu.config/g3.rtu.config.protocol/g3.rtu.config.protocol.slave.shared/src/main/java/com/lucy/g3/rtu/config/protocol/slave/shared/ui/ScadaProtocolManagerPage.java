/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolManagerPage;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;

/**
 * The page for managing SCADA protocol stack list.
 */
public final class ScadaProtocolManagerPage extends ProtocolManagerPage {

  public ScadaProtocolManagerPage(ScadaProtocolManager manager) {
    super("SCADA Protocol", manager, new ScadaProtoManagerModel(manager));
  }


  /**
   * Presentation model of Scada protocol manager.
   */
  private static class ScadaProtoManagerModel extends AbstractProtocolItemManagerModel {

    private final ScadaProtocolManager manager;

    private final SelectionInList<IScadaProtocol<?>> selectionInList;


    public ScadaProtoManagerModel(ScadaProtocolManager manager) {
      this.manager = Preconditions.checkNotNull(manager, "manager is null");
      selectionInList = new SelectionInList<IScadaProtocol<?>>(manager.getItemListModel());
    }

    @Override
    public SelectionInList<IScadaProtocol<?>> getSelectionInList() {
      return selectionInList;
    }

    @Override
    protected void addActionPerformed() {
      ProtocolAddingWizards.showAddProtocol(manager, ProtocolType.getAllSlaveProtocolTypes());
    }

    @Override
    public String getDisplayText(Object item) {
      return "[ Protocol ] " + ((IProtocol<?>) item).getProtocolName();
    }

    @Override
    protected void removeActionPerformed() {
      IScadaProtocol<?> sel = selectionInList.getSelection();

      if (sel != null) {
        if (showConfirmRemoveDialog(sel.getProtocolName())) {
          manager.remove(sel);
        }
      }
    }

  }

}
