/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import org.apache.log4j.Logger;

import com.g3schema.ns_mmb.MMBChnlConfT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.common.utils.EnumUtils;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolChannelConf;
import com.lucy.g3.xml.gen.common.ModBusEnum.LU_MBLINK_TYPE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;

/**
 * The configuration of {@linkplain MMBChannel}.
 * <p><b>TODO</b>: checking boundary is required when setter is called.
 * requires defining boundary in Constraint Class, look at
 * SDNP3Constraints.properties.
 * </p>
 */
public class MMBChannelConf extends AbstractProtocolChannelConf {

  // ====== Property Names ======
  public static final String PROPERTY_MBTYPE = "mbType";
  public static final String PROPERTY_MAX_QUEUESIZE = "maxQueueSize";
  public static final String PROPERTY_RX_FRAMETIMEOUT = "rxFrameTimeout";
  public static final String PROPERTY_CHNL_RES_TIMEOUT = "channelResponseTimeout";

  private final Logger log = Logger.getLogger(MMBChannelConf.class);

  // ====== Property Fields ======
  private LU_MBLINK_TYPE mbType = LU_MBLINK_TYPE.LU_MBLINK_TYPE_RTU;
  private long maxQueueSize = 1000;
  private long rxFrameTimeout = 1000;
  private long channelResponseTimeout = 1000;

  private final MMBChannelTCPConf tcpConfig;
  private final MMBChannelSerialConf serialConfig;


  public MMBChannelConf(MMBChannel mmbChannel, String chnlName) {
    super(mmbChannel, chnlName);
    tcpConfig = new MMBChannelTCPConf();
    serialConfig = new MMBChannelSerialConf(mmbChannel);

    // Init defaults TCP settings
    // setIpAddress("0.0.0.0");
    
    tcpConfig.setIpPort(502);
    tcpConfig.setMode(LU_LINTCP_MODE.LU_LINTCP_MODE_SERVER);
  }

  public LU_MBLINK_TYPE getMbType() {
    return mbType;
  }

  /**
   * Change the ModBus type. If the type is set to TCP, TCP configuration must
   * be configured, otherwise, Serial Port must be configured for this channel.
   *
   * @param mbType
   */
  public void setMbType(LU_MBLINK_TYPE mbType) {
    if (mbType == null) {
      log.error("mbType cannot be set to null");
      return;
    }

    Object oldValue = getMbType();
    this.mbType = mbType;
    firePropertyChange(PROPERTY_MBTYPE, oldValue, mbType);

    // Changed to TCP, release previously configured serial port.
    if (mbType == LU_MBLINK_TYPE.LU_MBLINK_TYPE_TCP) {
      serialConfig.releaseSerialPort();
      serialConfig.setActive(false);
    } else {
      serialConfig.setActive(true);
    }
  }

  public long getRxFrameTimeout() {
    return rxFrameTimeout;
  }

  public void setRxFrameTimeout(long rxFrameTimeout) {
    Object oldValue = getRxFrameTimeout();
    this.rxFrameTimeout = rxFrameTimeout;
    firePropertyChange(PROPERTY_RX_FRAMETIMEOUT, oldValue, rxFrameTimeout);
  }

  public long getMaxQueueSize() {
    return maxQueueSize;
  }

  public void setMaxQueueSize(long maxQueueSize) {
    Object oldValue = getMaxQueueSize();
    this.maxQueueSize = maxQueueSize;
    firePropertyChange(PROPERTY_MAX_QUEUESIZE, oldValue, maxQueueSize);
  }

  public long getChannelResponseTimeout() {
    return channelResponseTimeout;
  }

  public void setChannelResponseTimeout(long channelResponseTimeout) {
    Object oldValue = getChannelResponseTimeout();
    this.channelResponseTimeout = channelResponseTimeout;
    firePropertyChange(PROPERTY_CHNL_RES_TIMEOUT, oldValue, channelResponseTimeout);
  }

  @Override
  public String getChannelConfSummaryText() {
    return getMbType() == LU_MBLINK_TYPE.LU_MBLINK_TYPE_TCP ? getTCPConfigAsString() : getSerialConfigAsString();
  }

  @Override
  public MMBChannelSerialConf getSerialConf() {
    return serialConfig;
  }

  @Override
  public MMBChannelTCPConf getTcpConf() {
    return tcpConfig;
  }

  public void writeToXML(MMBChnlConfT xml, ValidationResult result) {
    xml.chnlName.setValue(getChannelName());
    xml.enabled.setValue(getChannel().isEnabled());
    xml.mbType.setValue(getMbType().name());
    xml.maxQueueSize.setValue(getMaxQueueSize());
    xml.rxFrameTimeout.setValue(getRxFrameTimeout());
    xml.channelResponseTimeout.setValue(getChannelResponseTimeout());
    xml.uuid.setValue(getChannel().getUUID());

    if (getMbType() != LU_MBLINK_TYPE.LU_MBLINK_TYPE_TCP) {
      getSerialConf().writeToXML(xml.serial.append(), result);
    } else {
      getTcpConf().writeToXML(xml.tcp.append());
    }
  }

  public void readFromXML(MMBChnlConfT xml, PortsManager ports) {
    setChannelName(xml.chnlName.getValue());
    setMbType(EnumUtils.getEnumFromString(LU_MBLINK_TYPE.class, xml.mbType.getValue()));
    
    if(xml.enabled.exists())
      getChannel().setEnabled(xml.enabled.getValue());
    
    if(xml.uuid.exists())
      getChannel().setUUID(xml.uuid.getValue());
    
    if (xml.maxQueueSize.exists()) {
      setMaxQueueSize(xml.maxQueueSize.getValue());
    }
    setRxFrameTimeout(xml.rxFrameTimeout.getValue());
    setChannelResponseTimeout(xml.channelResponseTimeout.getValue());

    if (xml.tcp.exists()) {
      getTcpConf().readFromXML(xml.tcp.first());
    } else if (xml.serial.exists()) {
      getSerialConf().readFromXML(xml.serial.first(), ports);
    }
  }

  @Override
  public Object getLinkType() {
    return getMbType();
  }

  @Override
  public void setLinkType(Object type) {
    setMbType((LU_MBLINK_TYPE) type);
  }

}
