/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;


/**
 * The enum of DNP3 group.
 */
public enum DNP3ObjGroup {
  AnalogInput,
  AnalogOutput,
  BinaryInput,
  BinaryOutput,
  DoubleBinaryInput,
  Counter,
  FrozenCounter;
  
  public static DNP3ObjGroup convertFrom(SDNP3PointType type) {
    switch (type) {
    case AnalogueInput:
      return DNP3ObjGroup.AnalogInput;
    case BinaryInput:
      return DNP3ObjGroup.BinaryInput;
    case Counter:
      return DNP3ObjGroup.Counter;
    case DoubleBinaryInput:
      return DNP3ObjGroup.DoubleBinaryInput;
    case BinaryOutput:
      return DNP3ObjGroup.BinaryOutput;
    case AnalogOutput:
      return DNP3ObjGroup.AnalogOutput;
    default:
      Logger.getLogger(DNP3ObjGroup.class).warn("not convertible for type:"+type);
      return null;
    }
  }
  
}

