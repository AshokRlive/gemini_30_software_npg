/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import org.apache.log4j.Logger;

import com.g3schema.ns_iec870.IEC870SesnEventT;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.common.utils.EnumUtils;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_EVENT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;

/**
 * The event configuration of IEC870 Points.
 */
public final class EventsConfig extends Model {

  private Logger log = Logger.getLogger(EventsConfig.class);

  public static final String PROPERTY_MAX_EVENT = "maxEvents";
  public static final String PROPERTY_EVENT_MODE = "eventMode";
  public static final String PROPERTY_TIME_FORMAT = "timeFormat";

  private long maxEvents;

  private LU_EVENT_MODE eventMode = LU_EVENT_MODE.LU_EVENT_MODE_SOE;
  private LU_TIME_FORMAT timeFormat = LU_TIME_FORMAT.LU_TIME_FORMAT_56;
  
  private final IEC870Constraints constraints;
  
  public EventsConfig(IEC870Constraints constraints) {
    super();
    this.constraints = constraints;
    maxEvents = getDefault(PROPERTY_MAX_EVENT);
  }
  
  private long getDefault(String property) {
    return constraints.getDefault(IEC870Constraints.PREFIX_SESSION_EVENT, property);
  }

  public IEC870Constraints getConstraints() {
    return constraints;
  }

  public long getMaxEvents() {
    return maxEvents;
  }

  public void setMaxEvents(long maxEvents) {
    Object oldValue = this.maxEvents;
    this.maxEvents = maxEvents;
    firePropertyChange(PROPERTY_MAX_EVENT, oldValue, maxEvents);
  }

  public LU_EVENT_MODE getEventMode() {
    return eventMode;
  }

  public void setEventMode(LU_EVENT_MODE eventMode) {
    if (eventMode == null) {
      log.error("eventMode must not be null");
      return;
    }

    Object oldValue = this.eventMode;
    this.eventMode = eventMode;
    firePropertyChange(PROPERTY_EVENT_MODE, oldValue, eventMode);
  }

  public LU_TIME_FORMAT getTimeFormat() {
    return timeFormat;
  }

  public void setTimeFormat(LU_TIME_FORMAT timeFormat) {
    if (timeFormat == null) {
      log.error("timeFormat must not be null");
      return;
    }

    Object oldValue = this.timeFormat;
    this.timeFormat = timeFormat;
    firePropertyChange(PROPERTY_TIME_FORMAT, oldValue, timeFormat);
  }

  public void writeToXML(IEC870SesnEventT xml) {
    xml.eventMode.setValue(eventMode == null ? null : eventMode.name());
    xml.timeFormat.setValue(timeFormat == null ? null : timeFormat.name());
    xml.maxEvents.setValue(maxEvents);
  }

  public void readFromXML(IEC870SesnEventT xml) {
    setEventMode(EnumUtils.getEnumFromString(LU_EVENT_MODE.class, xml.eventMode.getValue()));
    setTimeFormat(EnumUtils.getEnumFromString(LU_TIME_FORMAT.class, xml.timeFormat.getValue()));
    setMaxEvents(xml.maxEvents.getValue());
  }

}
