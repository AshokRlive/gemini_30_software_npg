/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.g3schema.ns_mmb.MMBDeviceChnlT;
import com.g3schema.ns_mmb.MMBIOMapT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolIoMap;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractIoMap;

/**
 * Modbus master IO map.
 */
public class MMBIoMap extends AbstractIoMap implements IMasterProtocolIoMap<MMBIoChannel> {

  private static final ChannelType[] SUPPORT_CHNL_TYPES = {
      ChannelType.ANALOG_INPUT,
      ChannelType.DIGITAL_INPUT,
      ChannelType.DIGITAL_OUTPUT,
  };

  private final Logger log = Logger.getLogger(MMBIoMap.class);

  private final ArrayListModel<MMBIoChannel> analogInputChs = new ArrayListModel<MMBIoChannel>();

  private final ArrayListModel<MMBIoChannel> digitalInputChs = new ArrayListModel<MMBIoChannel>();

  private final ArrayListModel<MMBIoChannel> digitalOutputChs = new ArrayListModel<MMBIoChannel>();

  private final MMBSession session;


  public MMBIoMap(MMBSession sesn) {
    session = sesn;//Preconditions.checkNotNull(sesn, "sesn is null");
  }

  @Override
  public String getDeviceName() {
    if(session != null && session.getDevice() != null)
      return session.getDevice().getDeviceName();
    return "N/A";
  }

  // ================== IFieldDeviceIoMap Implementation ===================

  @Override
  public MMBIoChannel addChannel(ChannelType type, String description) {
    MMBIoChannel newCh = new MMBIoChannel(type, getChannelNum(type), 
        session == null? null: session.getDevice(), description);
    if (addChannel(newCh)) {
      return newCh;
    } else {
      return null;
    }
  }

  @Override
  public boolean addChannel(MMBIoChannel channel) {
    Preconditions.checkNotNull(channel, "channel must not be null");
    
    boolean success = false;
    
    if (containsChannel(channel)) {
      log.error("Cannot add channel. Channel already exists: " + channel);
      
    } else {
      ChannelType type = channel.getType();
      if (type == ChannelType.ANALOG_INPUT) {
        success = analogInputChs.add(channel);
      } else if (type == ChannelType.DIGITAL_INPUT) {
        success = digitalInputChs.add(channel);
      } else if (type == ChannelType.DIGITAL_OUTPUT) {
        success = digitalOutputChs.add(channel);
      } else {
        log.error("Cannot add channel. Unsupported channel type: " + channel.getType());
      }
    }
    return success;
  }

  private boolean containsChannel(MMBIoChannel channel) {
    boolean result = false;

    if (channel == null) {
      // discard
    } else {
      ChannelType ct = channel.getType();
      int id = channel.getId();
      channel = getChannel(ct, id);
      if (channel == null) {
        result = false;
      } else {
        result = true;
      }
    }

    return result;
  }

  @Override
  public MMBIoChannel getChannel(ChannelType type, int index) {
    MMBIoChannel result = null;

    if (index < 0) {
      // discard
    } else if (index >= getChannelNum(type)) {
      // discard
    } else if (type == ChannelType.ANALOG_INPUT) {
      result = analogInputChs.get(index);
    } else if (type == ChannelType.DIGITAL_INPUT) {
      result = digitalInputChs.get(index);
    } else if (type == ChannelType.DIGITAL_OUTPUT) {
      result = digitalOutputChs.get(index);
    } else {
      // discard
    }

    return result;
  }

  public int getChannelNum(ChannelType type) {
    int result = 0;

    if (type == ChannelType.ANALOG_INPUT) {
      result = analogInputChs.size();
    } else if (type == ChannelType.DIGITAL_INPUT) {
      result = digitalInputChs.size();
    } else if (type == ChannelType.DIGITAL_OUTPUT) {
      result = digitalOutputChs.size();
    }

    return result;
  }

  @Override
  public void removeChannel(MMBIoChannel channel) {
    if (analogInputChs.remove(channel)) {
      channel.delete();
      updateChannelId(analogInputChs);
    }

    if (digitalInputChs.remove(channel)) {
      channel.delete();
      updateChannelId(digitalInputChs);
    }

    if (digitalOutputChs.remove(channel)) {
      channel.delete();
      updateChannelId(digitalOutputChs);
    }
  }

  @Override
  public void removeChannel(ChannelType type, int index) {
    removeChannel(getChannel(type, index));
  }

  @Override
  public void removeAllChannel(Collection<IChannel> channels) {
    analogInputChs.removeAll(channels);
    digitalInputChs.removeAll(channels);
    digitalOutputChs.removeAll(channels);
    
    for (IChannel ch : channels) {
      ch.delete();
    }
    
    updateChannelId(analogInputChs);
    updateChannelId(digitalInputChs);
    updateChannelId(digitalOutputChs);

  }

  private void updateChannelId(ArrayListModel<MMBIoChannel> list) {
    for (int i = 0; i < list.size(); i++) {
      list.get(i).setId(i);
    }
  }

  @Override
  public ChannelType[] getAvailableChannelTypes() {
    return Arrays.copyOf(SUPPORT_CHNL_TYPES, SUPPORT_CHNL_TYPES.length);
  }

  @Override
  public List<MMBIoChannel> getAllChannels() {
    ArrayList<MMBIoChannel> all = new ArrayList<MMBIoChannel>();
    all.addAll(analogInputChs);
    all.addAll(digitalInputChs);
    all.addAll(digitalOutputChs);
    return all;
  }

  @Override
  public MMBIoChannel[] getChannels(ChannelType type) {

    if (type == ChannelType.ANALOG_INPUT) {
      return analogInputChs.toArray(new MMBIoChannel[analogInputChs.size()]);
    } else if (type == ChannelType.DIGITAL_INPUT) {
      return digitalInputChs.toArray(new MMBIoChannel[digitalInputChs.size()]);
    } else if (type == ChannelType.DIGITAL_OUTPUT) {
      return digitalOutputChs.toArray(new MMBIoChannel[digitalOutputChs.size()]);
    } else {
      return new MMBIoChannel[0];
    }

  }

  // ================== IIoMap Implementation ===================

  @Override
  public void clearIoMap() {
    List<MMBIoChannel> allChs = getAllChannels();
    for (MMBIoChannel ch : allChs) {
      removeChannel(ch);
    }

    analogInputChs.clear();
    digitalInputChs.clear();
    digitalOutputChs.clear();
  }

  @Override
  @SuppressWarnings("unchecked")
  public ListModel<MMBIoChannel> getChannelListModel(ChannelType type) {
    if (type == ChannelType.ANALOG_INPUT) {
      return analogInputChs;
    } else if (type == ChannelType.DIGITAL_INPUT) {
      return digitalInputChs;
    } else if (type == ChannelType.DIGITAL_OUTPUT) {
      return digitalOutputChs;
    } else {
      return null;
    }
  }

  @Override
  public String[] getChannelParamNames(ChannelType type) {
    return MMBIoChannel.getParamNames(type);
  }

  public void readFromXML(MMBIOMapT xml) {
    IMasterProtocolModule device = session.getDevice();

    int num = xml.AIChs.count();
    for (int i = 0; i < num; i++) {
      createChannel(device, this, ChannelType.ANALOG_INPUT, xml.AIChs.at(i)).readFromXML(xml.AIChs.at(i));
    }

    num = xml.DIChs.count();
    for (int i = 0; i < num; i++) {
      createChannel(device, this, ChannelType.DIGITAL_INPUT, xml.DIChs.at(i)).readFromXML(xml.DIChs.at(i));
    }

    num = xml.DOChs.count();
    for (int i = 0; i < num; i++) {
      createChannel(device, this, ChannelType.DIGITAL_OUTPUT, xml.DOChs.at(i)).readFromXML(xml.DOChs.at(i));
    }
  }

  private static MMBIoChannel createChannel(IMasterProtocolModule device, MMBIoMap iomap, ChannelType channelType,
      MMBDeviceChnlT xml) {
    long channelId = xml.channelID.getValue();
    String description = xml.description.getValue();
    MMBIoChannel ch = iomap.getChannel(channelType, (int) channelId);
    if (ch == null) {
      ch = new MMBIoChannel(channelType, (int) channelId, device, description);
      iomap.addChannel(ch);
    }
    return ch;
  }

  public void writeToXML(MMBIOMapT xml) {

    ChannelType[] types = getAvailableChannelTypes();

    /*
     * Export channels by types. The order should be the same as what is defined
     * in schema: AI, DI, DO.
     */

    // AI
    for (int i = 0; i < types.length; i++) {
      if (types[i] == ChannelType.ANALOG_INPUT) {
        MMBIoChannel[] chs = getChannels(types[i]);
        for (int j = 0; j < chs.length; j++) {
          chs[j].writeToXML(xml.AIChs.append());
        }
      }
    }

    // DI
    for (int i = 0; i < types.length; i++) {
      if (types[i] == ChannelType.DIGITAL_INPUT) {
        MMBIoChannel[] chs = getChannels(types[i]);
        for (int j = 0; j < chs.length; j++) {
          chs[j].writeToXML(xml.DIChs.append());
        }
      }
    }

    // DO
    for (int i = 0; i < types.length; i++) {
      if (types[i] == ChannelType.DIGITAL_OUTPUT) {
        MMBIoChannel[] chs = getChannels(types[i]);
        for (int j = 0; j < chs.length; j++) {
          chs[j].writeToXML(xml.DOChs.append());
        }
      }
    }

  }

  @Override
  public void copyFrom(IIoMap iomap) {
    if (iomap != null && iomap instanceof MMBIoMap) {
      @SuppressWarnings("unchecked")
      MMBIoMap mmbIoMap = (MMBIoMap) iomap;
      
      MMBIoMapCopySupport.copyIoMap(session.getProtocolType(), mmbIoMap, this);
      log.info("Copying IOMap is done sucessfully!");
      
    } else {
      log.error("Cannot copy from iomap: " + iomap);
    }
  }

  @Override
  public IProtocolSession getSession() {
    return session;
  }


  @Override
  public Collection<?> getAllItems() {
    return getAllChannels();
  } 
}
