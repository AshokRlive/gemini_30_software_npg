/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.domain;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;

/**
 * The Serial configuration of S101 channel.
 */
public final class S101ChannelSerialConf extends ProtocolChannelSerialConf{
  public static final String PROPERTY_FIRST_CHAR_WAIT = "firstCharWait";

  private long firstCharWait = getDefault(PROPERTY_FIRST_CHAR_WAIT);
  
  public S101ChannelSerialConf(IProtocolChannel<?> channel) {
    super(channel);
  }
  

  public long getFirstCharWait() {
    return firstCharWait;
  }
  
  public void setFirstCharWait(long firstCharWait) {
    Object oldValue = this.firstCharWait;
    this.firstCharWait = firstCharWait;
    firePropertyChange(PROPERTY_FIRST_CHAR_WAIT, oldValue, firstCharWait);
  }
  
  protected final long getDefault(String property) {
    return IEC870Constraints.INSTANCE_101.getDefault(IEC870Constraints.PREFIX_CHANNEL, property);
  }
}
