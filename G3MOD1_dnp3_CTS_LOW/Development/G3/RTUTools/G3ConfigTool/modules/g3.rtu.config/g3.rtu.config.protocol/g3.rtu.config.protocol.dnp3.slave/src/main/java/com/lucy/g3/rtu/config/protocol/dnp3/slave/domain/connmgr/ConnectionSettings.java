/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr;

import com.g3schema.ns_sdnp3.ConnectionManagerT;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.ConnectOnClass;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;

/**
 * The settings of Connection Manager.
 */
public final class ConnectionSettings extends ConnectOnClass {

  public static final String PROPERTY_ACTIVITY_TIMEOUT = "activityTimeout";
  public static final String PROPERTY_CONNECT_RETRY_DELAY = "connectRetryDelay";

  /** SDNP3 constraint instance. */
  private SDNP3Constraints cons = SDNP3Constraints.INSTANCE;

  private long activityTimeout = cons.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION, "activityTimeout");
  private long connectRetryDelay = cons.getDefault(SDNP3Constraints.PREFIX_CHANNEL_CONNECTION, "connectRetryDelay");


  public long getActivityTimeout() {
    return activityTimeout;
  }

  public void setActivityTimeout(long activityTimeout) {
    Object oldValue = getActivityTimeout();
    this.activityTimeout = activityTimeout;
    firePropertyChange(PROPERTY_ACTIVITY_TIMEOUT, oldValue, activityTimeout);
  }

  public long getConnectRetryDelay() {
    return connectRetryDelay;
  }

  public void setConnectRetryDelay(long connectRetryDelay) {
    Object oldValue = this.connectRetryDelay;
    this.connectRetryDelay = connectRetryDelay;
    firePropertyChange(PROPERTY_CONNECT_RETRY_DELAY, oldValue, connectRetryDelay);
  }

  void readFromXML(ConnectionManagerT xml) {
    setConnectOnClassMask(xml.connectionClass.getValue());
    setActivityTimeout(xml.inactivityTimeoutSecs.getValue());
    setConnectRetryDelay(xml.connectRetryDelaySecs.getValue());
  }

  void writeToXML(ConnectionManagerT xml) {
    xml.connectionClass.setValue(getConnectOnClassMask());
    xml.inactivityTimeoutSecs.setValue(getActivityTimeout());
    xml.connectRetryDelaySecs.setValue(getConnectRetryDelay());
  }
}
