/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.validate;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolNameFactory;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

public final class IoMapValidator extends AbstractContainerValidator<IIoMap> {

  public IoMapValidator(IIoMap target) {
    super(target);
  }

  @Override
  public String getTargetName() {
    return ProtocolNameFactory.getAbsoluteName(target);
  }

  @Override
  protected Collection<IValidation> getValidatableChildrenItems() {
    ArrayList<IValidation> validationItems = new ArrayList<>();
    Collection<?> items = target.getAllItems();
    for (Object item : items) {
      if (item != null && item instanceof IValidation)
        validationItems.add((IValidation) item);
    }
    return validationItems;
  }
}
