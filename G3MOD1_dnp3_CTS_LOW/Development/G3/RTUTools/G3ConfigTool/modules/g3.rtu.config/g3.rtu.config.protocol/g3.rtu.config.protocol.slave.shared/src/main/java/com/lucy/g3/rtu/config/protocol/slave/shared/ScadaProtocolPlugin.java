/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManagerImpl;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.ScadaProtocolManagerPage;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class ScadaProtocolPlugin implements IConfigPlugin, IConfigModuleFactory, IPageFactory{
  private ScadaProtocolPlugin () {}
  private final static ScadaProtocolPlugin  INSTANCE = new ScadaProtocolPlugin();
  
  public static void init() {
    ConfigFactory.getInstance().registerFactory(ScadaProtocolManager.CONFIG_MODULE_ID, INSTANCE);
    
    PageFactories.registerFactory(PageFactories.KEY_SCADA_PROTOCOL, INSTANCE);
  }
  
  @Override
  public ScadaProtocolManager create(IConfig owner) {
    return new ScadaProtocolManagerImpl(owner);
  }
  

  @Override
  public Page createPage(Object data) {
    if (data instanceof ScadaProtocolManager) {
      /* Create Scada protocol management page */
      return  new ScadaProtocolManagerPage((ScadaProtocolManager) data);

    } 
    
    return null;
  }
}

