/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.jgoodies.common.collect.ObservableList;
import com.lucy.g3.gui.common.dialogs.SelectionDialog;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IIdChecker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointIdChecker;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialogInvoker;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

/**
 * Presentation model for Scada points manager, which adapts a list of Scada
 * points to table model and provides related operation actions.
 *
 * @param <ScadaPointT>
 *          the generic type the type of Scada Point
 */
public abstract class AbstractIoMapPageModel<ScadaPointT extends ScadaPoint> extends Bean {

  public static final int COLUMN_INDEX_ENABLED = ScadaPointTableModel.COLUMN_ENABLE;
  public static final int COLUMN_INDEX_STORE_EVENT = ScadaPointTableModel.COLUMN_ENABLE_STORE_EVENT;
  public static final String ACTION_KEY_ADD = "addPoint";
  public static final String ACTION_KEY_BULK_ADD = "addBulkPoints";
  public static final String ACTION_KEY_EDIT = "editSelection";
  public static final String ACTION_KEY_REMOVE = "removeSelection";

  /** The name of property which indicates if the selected point can be edit. */
  private static final String PROPERTY_EDITITABLE = "editable";

  /** The name of property which indicates if the selected points can be removed. */
  private static final String PROPERTY_REMOVEABLE = "removeable";
  

  private Logger log = Logger.getLogger(AbstractIoMapPageModel.class);

  private final ScadaPointType type;

  private final IScadaIoMap<ScadaPointT> iomap;

  /** The selection list of Scada points, which serves the selection model. */
  private final MultiSelectionInList<ScadaPointT> pointsList;

  /** The selection model of Scada points.*/
  private final MultiListSelectionAdapter<ScadaPointT> selectionModel;

  protected final JFrame parent = WindowUtils.getMainFrame();

  private ScadaPointTableModel<ScadaPointT> pointsTableModel;

  protected AbstractIoMapPageModel(IScadaIoMap<ScadaPointT> iomap, ScadaPointType type) {
    this.type = Preconditions.checkNotNull(type, "type is null");
    this.iomap = Preconditions.checkNotNull(iomap, "iomap is null");

    pointsList = new MultiSelectionInList<ScadaPointT>(iomap.getPointListModel(type));
    selectionModel = new MultiListSelectionAdapter<ScadaPointT>(pointsList);
    selectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

    // Add selection handling
    pointsList.addPropertyChangeListener(MultiSelectionInList.PROPERTY_SELECTION, new PointSelectionHandler());
  }

  /**
   * Bean getter method. Checks if the selected points can be edit.
   */
  public boolean isEditable() {
    return pointsList.getSelection().size() > 0;
  }

  /**
   * Bean getter method. Checks if there are points that can be removed.
   */
  public boolean isRemoveable() {
    return pointsList.getSelection().size() > 0;
  }

  public ArrayList<ScadaPointT> getSelectedItems() {
    return new ArrayList<>(pointsList.getSelection());
  }

  /**
   * Clear the selection.
   */
  public void clearSelection() {
    selectionModel.clearSelection();
  }

  /**
   * Gets the amount of Scada points.
   *
   */
  public final  int getItemsCount() {
    return pointsList.getList().getSize();
  }

  public final  void selectAll() {
    int size = getItemsCount();

    if (size > 0) {
      selectionModel.setSelectionInterval(0, size - 1);
    }
  }

  public final  void selectFirst() {
    int size = getItemsCount();

    if (size > 0) {
      selectionModel.setSelectionInterval(0, 0);
    }
  }

  public final TableModel getTableModel() {
    if (pointsTableModel == null) {
      IIdChecker idChecker = new ScadaPointIdChecker<ScadaPointT>(iomap, type);
      pointsTableModel = new ScadaPointTableModel(
          pointsList.getList(), 
          idChecker, 
          type.isOutput());
    }
    return pointsTableModel;
  }

  public final ListSelectionModel getSelectionModel() {
    return selectionModel;
  }

  public final ListModel<ScadaPointT> getListModel() {
    return pointsList.getList();
  }

  public final ScadaPointT getPointAt(int rowIndex) {
    return pointsList.getList().getElementAt(rowIndex);
  }

  public final String getModelName() {
    return type.getDescription();
  }

  public final javax.swing.Action getAction(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");
    ApplicationActionMap actionMap = Application.getInstance().getContext()
        .getActionMap(AbstractIoMapPageModel.class, this);
    javax.swing.Action action = actionMap.get(actionKey);

    if (action != null) {
      action = new BusyCursorAction(action, parent);
    } else {
      throw new NullPointerException("Action is not available for the key:" + actionKey);
    }
    return action;
  }

  protected abstract ScadaPointT createPoint(
      IScadaIoMap<ScadaPointT> iomap, 
      ScadaPointType type, 
      long protocolID);

  protected abstract ArrayList<ScadaPointT> showBulkAdd(
      ArrayList<ScadaPointSource> res, 
      ScadaPointType type,  
      IScadaIoMap<ScadaPointT> iomap);

  protected abstract boolean showBulkEditor(
      ScadaPointType type, 
      IScadaIoMap<ScadaPointT> iomap,
      ObservableList<ScadaPointT> selection);

  protected abstract boolean showEditor(ScadaPointT point);
  
  protected abstract void showEditor(ScadaPointType type, 
      IScadaPointEditorDialogInvoker<ScadaPointT> editingPointProviderImpl);

  protected abstract ArrayList<ScadaPointSource> getMappingRes(
      ScadaPointType type,
      IScadaIoMap<ScadaPointT> iomap, 
      boolean spareOnly);
  
  /**
   * Shows a dialog to add a single point.
   */
  @Action
  public void addPoint() {
    ScadaPointT point;
    long protocolID = iomap.findAvailablePointID(type);
    point = createPoint(iomap, type, protocolID);
    boolean affirmed = showEditor(point);
  
    if (affirmed) {
      try {
        iomap.addPoint(point);
  
        // Clear all selected points
        selectionModel.clearSelection();
  
        // Select the last added point
        int selIndex = pointsList.getList().getSize() - 1;
        selectionModel.setSelectionInterval(selIndex, selIndex);
  
      } catch (DuplicatedException e) {
        JOptionPane.showMessageDialog(parent, e.getMessage(),
            "Conflict", JOptionPane.WARNING_MESSAGE);
      }
    }
  }

  /**
   * Shows a dialog to add multi-points.
   */
  @Action
  public void addBulkPoints() {
    ArrayList<ScadaPointSource> res = getMappingRes(type, iomap, true);

    /* Select added points */
    int sizeAdded = showBulkAdd(res, type, iomap).size();
    int sizeTotal = pointsList.getList().getSize();
    if (sizeAdded > 0) {
      selectionModel.setSelectionInterval(sizeTotal - sizeAdded, sizeTotal - 1);
    }
  }


  @Action(enabledProperty = PROPERTY_EDITITABLE)
  public void editSelection() {
    int selectIdxMin = selectionModel.getMinSelectionIndex();
    int selectIdxMax = selectionModel.getMaxSelectionIndex();
    if (0 <= selectIdxMin && selectIdxMin < selectIdxMax) {
      /* Edit multiple selections */
      if (showBulkEditor(type, iomap, pointsList.getSelection())) {
        pointsTableModel.fireTableRowsUpdated(selectIdxMin, selectIdxMax);
      }

    } else if (selectIdxMin >= 0 && selectIdxMax == selectIdxMin) {
      /* Edit single selections */
      showEditor(type, new EditingPointProviderImpl(selectIdxMin));
    }

  }

  @SuppressWarnings("unchecked")
  @Action(enabledProperty = PROPERTY_REMOVEABLE)
  public void removeSelection() {
    ArrayList<ScadaPointT> initialSelections = getSelectedItems();
    Collection<ScadaPointT> all = iomap.getPoints(type);
    Collection<ScadaPoint> selected = SelectionDialog.showRemoveDialog(parent, 
        all.toArray(new ScadaPoint[all.size()]), 
        initialSelections.toArray(new ScadaPoint[initialSelections.size()]));
    if(selected == null)
      return;
    
    
    if (!selected.isEmpty()) {
      for (ScadaPoint p : selected) {
        if (p != null) {
          p.delete();
          iomap.removePoint((ScadaPointT) p);
        }
      }
    } else {
      log.error("Cannot remove points, no selection!");
    }
  }

  private void fireActionStateChangeEvents() {
    firePropertyChange(PROPERTY_EDITITABLE, null, isEditable());
    firePropertyChange(PROPERTY_REMOVEABLE, null, isRemoveable());
  }


  private class PointSelectionHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      fireActionStateChangeEvents();
    }
  }

  private final class EditingPointProviderImpl extends AbstractEditorDialogInvoker<ScadaPointT>
      implements IScadaPointEditorDialogInvoker<ScadaPointT> {

    private EditingPointProviderImpl(int initialSelectionIndex) {
      super(pointsList.getList(), selectionModel, initialSelectionIndex);
    }

    @Override
    public void fireEditingItemChanged() {
      int index = getSelectionIndex();
      pointsTableModel.fireTableRowsUpdated(index, index);
    }
  }

}
