/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.util.ScadaProtocolUtility;


/**
 *
 */
public class SDNP3MappingResources {
  /**
   * Gets available resource that can be mapped to SDNP3 points.
   */
  public static ArrayList<ScadaPointSource> getMappingRes(
      ScadaPointSourceRepository provider,
      SDNP3PointType type,
      IScadaIoMap<?> iomap, boolean spareOnly) {

    Collection<? extends ScadaPointSource> res = null;
    
    if (provider != null) {
      switch (type) {
      case AnalogueInput:
        res = new ArrayList<ScadaPointSource>(provider.getAnalogueInputSources());
        ((ArrayList<ScadaPointSource>)res).addAll(provider.getCounterSources());
        break;
        
      case BinaryInput:
        res = provider.getBinaryInputSources();
        break;
        
      case DoubleBinaryInput:
        res = provider.getDoubleBinaryInputSources();
        break;
        
      case Counter:
        res = provider.getCounterSources();
        break;
        
      case BinaryOutput:
        res = provider.getDigitalOutputSouces();
        break;
        
      case AnalogOutput:
        res = provider.getAnalogOutputSouces();
        break;

      default:
        throw new IllegalStateException("Invlalid DNP point Type: " + type);
      }
    }

    if (res == null) {
      res = new ArrayList<ScadaPointSource>(0);
    }

    ArrayList<ScadaPointSource> ret = new ArrayList<ScadaPointSource>(res);
    return spareOnly ? ScadaProtocolUtility.findSpareSource(ret, iomap, type) : ret;
  }
}

