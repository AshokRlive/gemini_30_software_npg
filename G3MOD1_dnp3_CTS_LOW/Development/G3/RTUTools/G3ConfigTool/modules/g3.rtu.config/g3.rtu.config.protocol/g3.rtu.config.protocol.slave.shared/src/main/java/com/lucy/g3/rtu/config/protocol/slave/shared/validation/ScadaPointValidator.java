/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.validation;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The validator for validating SCADA protocol points.
 */
public class ScadaPointValidator extends AbstractValidator<ScadaPoint> {

  private final ScadaPoint target;


  public ScadaPointValidator(ScadaPoint target) {
    super(target);
    this.target = Preconditions.checkNotNull(target, "target must not be null");
  }

  @Override
  protected void validate(ValidationResultExt result) {
    ScadaPointSource source = target.getSource();

    if (source == null) {
      result.addError(target.getName() + " is not mapped to any resource");
    } else {
      validateSource(result, source);
    }

    if (target.getPointID() < 0) {
      result.addError(ScadaPoint.PROPERTY_POINT_ID + " is invalid");
    }

  }

  private void validateSource(ValidationResultExt result, ScadaPointSource source) {
    Object raw = source.getRawSource();
    if (raw instanceof IValidation) {
      IValidator validator = ((IValidation) raw).getValidator();

      if (validator != null && !validator.isValid()) {
        result.addError(" is mapped to an invalid resource: " + source.getName());
      }
    }
  }

  @Override
  public String getTargetName() {
    return target.getFullName();
  }

}
