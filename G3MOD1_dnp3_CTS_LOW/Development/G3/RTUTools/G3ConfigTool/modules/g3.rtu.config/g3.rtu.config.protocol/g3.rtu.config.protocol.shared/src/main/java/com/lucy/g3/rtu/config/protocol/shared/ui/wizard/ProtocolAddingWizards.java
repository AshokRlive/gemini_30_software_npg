/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.awt.Rectangle;

import javax.swing.JPanel;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;

/**
 * This class provides factory methods for showing protocol related wizards.
 */
public class ProtocolAddingWizards {
  
  private static final Rectangle SIZE = WizardUtils.createRect(WindowUtils.getMainFrame(),550, 350);
  private ProtocolAddingWizards() {
  }

  public static IProtocol showAddProtocol(IProtocolManager<?> manager, ProtocolType[] types) {
    Wizard wiz = WizardPage.createWizard("Add New Protocol", new WizardPage[]{
        new AddProtocolPage(manager, types),
        new AddChannelPage(),
        new AddSessionPage(),
        new GenerateIOMapPage(manager.getOwner(), null, true)
    }, new AddProtocolResult(manager));
    return (IProtocol)show(wiz);
  }
  
  public static IProtocolChannel showAddChannel(IProtocol protocol) {
    ProtocolType type = protocol.getProtocolType();
    Wizard wiz = WizardPage.createWizard(String.format("%s - Add New Channel",type.getName()), 
        new WizardPage[]{
        new AddChannelPage(),
        new AddSessionPage(),
        new GenerateIOMapPage(protocol.getManager().getOwner(), type, true)
    }, new AddChannelResult(protocol));
    return (IProtocolChannel) show(wiz);
  }
  
  public static IProtocolSession showAddSession(IProtocolChannel channel) {
    ProtocolType type = channel.getProtocolType();
    Wizard wiz = WizardPage.createWizard(String.format("%s - Add New Session",type.getName()),
        new WizardPage[]{
        new AddSessionPage(),
        new GenerateIOMapPage(channel.getProtocol().getManager().getOwner(),type, true)
    }, new AddSessionResult(channel));
    
    return (IProtocolSession) show(wiz);
  }
  
  
  public static boolean showGenerateIoMap(IProtocolSession session, String wizardTitle, String stepDescription) {
    ProtocolType type = session.getProtocolType();
    if(Strings.isBlank(wizardTitle))
      wizardTitle = "Generate IO Map";

    GenerateIOMapPage page = new GenerateIOMapPage(stepDescription,
        session.getProtocolChannel().getProtocol().getManager().getOwner(),
        type);

    Wizard wiz = WizardPage.createWizard(String.format("%s - %s", type.getName(), wizardTitle),
        new WizardPage[]{page}, new GenerateIOMapResult(session));
    
    Object result = show(wiz); 
        
    return result != null;
  }
  
  private static Object show(Wizard wiz) {
    return WizardDisplayer.showWizard(wiz, SIZE);
  }
  
  static DefaultFormBuilder createPanelBuilder(JPanel panel){
    return new DefaultFormBuilder(new FormLayout(
        "[50dlu,default], 7dlu, [80dlu,default],default:grow",
        ""), panel);
  }
}
