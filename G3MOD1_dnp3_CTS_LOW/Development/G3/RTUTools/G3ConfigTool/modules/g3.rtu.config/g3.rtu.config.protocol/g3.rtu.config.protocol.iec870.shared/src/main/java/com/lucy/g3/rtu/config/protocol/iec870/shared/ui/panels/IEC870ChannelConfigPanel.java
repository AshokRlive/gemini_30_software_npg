/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870ChannelConf;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
public class IEC870ChannelConfigPanel extends JXTaskPane {
  private final PresentationModel<? extends IEC870ChannelConf> pmChannel;
  
  @SuppressWarnings("unused")
  private IEC870ChannelConfigPanel() {
    super("Channel");
    this.pmChannel = new PresentationModel<>();
    initComponents();
  }
  
  public IEC870ChannelConfigPanel(PresentationModel<? extends IEC870ChannelConf> pmChannel) {
    super("Channel");
    this.pmChannel = pmChannel;
    initComponents();
  }

  private void createUIComponents() {
    ComponentsFactory factory = new ComponentsFactory(pmChannel.getBean().getConstraints(),
        IEC870Constraints.PREFIX_SESSION, pmChannel);

    ftfIncrementalTimeout = factory.createNumberField(IEC870ChannelConf.PROPERTY_INCREMENTAL_TIMEOUT);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label7 = new JLabel();
    label3 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, [50dlu,default], $lcgap, default:grow",
        "default"));

    //---- label7 ----
    label7.setText("Incremental Timeout:");
    add(label7, CC.xy(1, 1));
    add(ftfIncrementalTimeout, CC.xy(3, 1, CC.FILL, CC.DEFAULT));

    //---- label3 ----
    label3.setText("ms");
    add(label3, CC.xy(5, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label7;
  private JFormattedTextField ftfIncrementalTimeout;
  private JLabel label3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
