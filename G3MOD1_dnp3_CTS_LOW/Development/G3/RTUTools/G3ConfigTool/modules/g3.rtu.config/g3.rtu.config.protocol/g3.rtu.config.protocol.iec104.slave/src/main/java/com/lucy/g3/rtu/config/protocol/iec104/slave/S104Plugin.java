/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Session;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.S104ChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.S104ChannelPage;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.S104SessionPage;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.IEC870IoMapPage;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class S104Plugin implements IConfigPlugin, IPageFactory, IProtocolFactory{
  private S104Plugin () {}
  private final static S104Plugin  INSTANCE = new S104Plugin();
  
  public static void init() {
    PageFactories.registerFactory(PageFactories.KEY_S104, INSTANCE);
    
    ProtocolFactories.registerFactory(ProtocolType.S104, INSTANCE);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof S104) {
      return new S104ChannelManagerPage((S104) data);

    } else if (data instanceof S104Channel) {
      // (SDNP3Channel) data,configdata.getPortsManager(),
      // configdata.getCommsDeviceManager()
      return new S104ChannelPage((S104Channel) data);

    } else if (data instanceof S104Session) {
      return new S104SessionPage((S104Session) data);

    }  else if (data instanceof IEC870IoMap) {
      IConfig config = ((IEC870IoMap)data).getSession().getProtocolChannel().getProtocol().getManager().getOwner();
      ScadaProtocolManager scada = config.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
      return new IEC870IoMapPage((IEC870IoMap) data, scada.getSourceRepository());
    }
    
    return null;
  }

  @Override
  public IScadaProtocol createProtocol() {
    return new S104();
  }
  
}

