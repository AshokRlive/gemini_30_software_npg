/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui;

import javax.swing.JOptionPane;

import org.jdesktop.application.Action;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

/**
 * Presentation model of ModBus session manager.
 */
public class MMBSessionManagerModel extends AbstractProtocolItemManagerModel {

  private static final String ACTION_APPLY_TEMPLATE = "applyTemplate";

  private final MMBChannel mbmChnl;

  private final SelectionInList<MMBSession> selectionInList;



  public MMBSessionManagerModel(MMBChannel mbmChnl) {
    this.mbmChnl = Preconditions.checkNotNull(mbmChnl, "mbmChnl is null");
    this.selectionInList = new SelectionInList<MMBSession>(mbmChnl.getItemListModel());

    addPageAction(1, getAction(ACTION_APPLY_TEMPLATE));
    addPageAction(2, null);
  }

  @Override
  public SelectionInList<MMBSession> getSelectionInList() {
    return selectionInList;
  }

  @Action(enabledProperty = PROPERTY_SELECTION_NOT_EMPTY)
  public void applyTemplate() {
    MMBSession session = selectionInList.getSelection();
    
    if(ProtocolAddingWizards.showGenerateIoMap(session, "Applying Session Template", "Choose Template")) {
      JOptionPane.showMessageDialog(WindowUtils.getMainFrame(), "Applied template successfully!", 
          "Success", JOptionPane.INFORMATION_MESSAGE);
    }
  }

  /**
   * Adds a new ModBus master session.
   */
  @Override
  protected void addActionPerformed() {
    addSession();
  }


  
  private void addSession() {
    IProtocolSession result = ProtocolAddingWizards.showAddSession(mbmChnl);
    if (result != null)
      selectionInList.setSelection((MMBSession) result);
  }

  /**
   * Removes a ModBus master session.
   */
  @Override
  protected void removeActionPerformed() {
    MMBSession sel = selectionInList.getSelection();
    if (sel != null) {
      if (showConfirmRemoveDialog(sel.getSessionName())) {
        mbmChnl.remove(sel);
      }
    }
  }
}
