/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.ui;

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels.*;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101ChannelConf;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Session;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.EventsConfig;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
import com.lucy.g3.rtu.config.protocol.shared.ui.panels.HintPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_TIMETGA_MODE;

/**
 * The page for configuring IEC104-slave session.
 */
public class S101SessionPage extends AbstractConfigPage {

  private final PresentationModel<S101Session> pm;

  private final ArrayListModel<IEC870IoMap> ioMapListModel;


  public S101SessionPage(S101Session session) {
    super(session, "IEC101 Session");
    this.pm = new PresentationModel<S101Session>(session);
    this.ioMapListModel = new ArrayListModel<IEC870IoMap>(1);
    this.ioMapListModel.add(session.getIomap());

    // Bind node name to session name.
    ValueModel vm = pm.getModel(S101Session.PROPERTY_SESSION_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }

  @Override
  protected void init() throws Exception {
    initComponents();
    populate();
    initEventHandling();
  }

  @SuppressWarnings("unchecked")
  @Override
  public ListModel<IEC870IoMap> getChildrenDataList() {
    return ioMapListModel;
  }

  private void createUIComponents() {
    ComponentsFactory factory = new ComponentsFactory(IEC870Constraints.INSTANCE_101, IEC870Constraints.PREFIX_SESSION, pm);

    tfSessionName = factory.createTextField(IEC870Session.PROPERTY_SESSION_NAME);
    ftfASDUAddress = factory.createNumberField(IEC870Session.PROPERTY_ASDU_ADDRESS);
    ftfCyclicPeriod = factory.createNumberField(IEC870Session.PROPERTY_CYCLIC_PERIOD);
    ftfCyclicFirstPeriod = factory.createNumberField(IEC870Session.PROPERTY_CYCLIC_FIRST_PERIOD);
    ftfBackgroundPeriod = factory.createNumberField(IEC870Session.PROPERTY_BACKGROUND_PERIOD);
    ftfSelectTimeout = factory.createNumberField(IEC870Session.PROPERTY_SELECT_TIMEOUT);
    ftfDefResponseTimeout = factory.createNumberField(IEC870Session.PROPERTY_DEFAULT_RESPONSE_TIMEOUT);
    combCmdTimetagMode = factory.createComboBox(IEC870Session.PROPERTY_CMD_TIMETAG_MODE, LU_TIMETGA_MODE.values());

    ftfCOTSize = factory.createNumberField(S101Session.PROPERTY_COT_SIZE);
    ftfMaxPollDelay = factory.createNumberField(S101Session.PROPERTY_MAX_POLL_DELAY);
    ftfLinkAddress = factory.createNumberField(S101Session.PROPERTY_LINK_ADDRESS);
    ftfMaxFrameSize = factory.createNumberField(S101Session.PROPERTY_MAX_FRAME_SIZE);
    ftfASDUAddrSize = factory.createNumberField(S101Session.PROPERTY_ASDU_ADDR_SIZE);
    ftfObjAddrSize = factory.createNumberField(S101Session.PROPERTY_INFO_OBJ_ADDR_SIZE);
    
    cbSendClkSynEvents = factory.createCheckBox(IEC870Session.PROPERTY_SEND_CLOCK_SYNC_EVENTS);
    cbDelOldestEvent = factory.createCheckBox(IEC870Session.PROPERTY_DELETE_OLDEST_EVENT);
    cbCseUseActTerm = factory.createCheckBox(IEC870Session.PROPERTY_CSEUSEACTTERM);
    cbCmdUseActTerm = factory.createCheckBox(IEC870Session.PROPERTY_CMDUSEACTTERM);
    cbStrictAdherence = factory.createCheckBox(IEC870Session.PROPERTY_MULTIPLE_PMEN);
    cbUseDayOfWeek = factory.createCheckBox(IEC870Session.PROPERTY_USE_DAYOFWEEK);

    panelHint = new HintPanel(S101SessionPage.class);
    
    EventsConfig ain = (EventsConfig) pm.getValue(S101Session.PROPERTY_EVENTCONFIG_AIN);
    EventsConfig ais = (EventsConfig) pm.getValue(S101Session.PROPERTY_EVENTCONFIG_AIS);
    EventsConfig aif = (EventsConfig) pm.getValue(S101Session.PROPERTY_EVENTCONFIG_AIF);
    EventsConfig bi = (EventsConfig) pm.getValue(S101Session.PROPERTY_EVENTCONFIG_BI);
    EventsConfig dbi = (EventsConfig) pm.getValue(S101Session.PROPERTY_EVENTCONFIG_DBI);
    EventsConfig ct = (EventsConfig) pm.getValue(S101Session.PROPERTY_EVENTCONFIG_CT);
    panelEventAnlogInput = new EventPanelAI(ain, ais, aif);
    panelEventDigitalInput = new EventPanelDI(bi, dbi, ct);
    
    /*Bind link address size from channel settings.*/
    S101Channel ch = (S101Channel) pm.getBean().getProtocolChannel();
    PropertyAdapter<S101ChannelConf> vm = new PropertyAdapter<>(ch.getChannelConfig(),
        S101ChannelConf.PROPERTY_LINK_ADDR_SIZE,true);
    ftfLinkAddrSize = ComponentsFactory.createNumberField(vm, Long.MIN_VALUE,Long.MAX_VALUE);
    
    panelSessionTimeFormat = new IEC870SessionTimeFormatPanel(pm);
  }
  
  private void initEventHandling() {
    /* Disable link address if the size is 0 */
    S101Channel ch = (S101Channel) pm.getBean().getProtocolChannel();
    PropertyAdapter<S101ChannelConf> vm = new PropertyAdapter<>(ch.getChannelConfig(),
        S101ChannelConf.PROPERTY_LINK_ADDR_SIZE, true);

    vm.addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent arg0) {
        boolean enabled = (long) arg0.getNewValue() != 0;
        label3.setEnabled(enabled);
        ftfLinkAddress.setEnabled(enabled);
      }
    });
    boolean enabled = (long) vm.getValue() != 0;
    label3.setEnabled(enabled);
    ftfLinkAddress.setEnabled(enabled);
  }

  private void populate() {
    sessionConfigPane.add(panelSessionGeneral);
    sessionConfigPane.add(panelSessionFunctions);
    sessionConfigPane.add(panelSessionMiscs);
    sessionConfigPane.add(panelEventAnlogInput);
    sessionConfigPane.add(panelEventDigitalInput);
    sessionConfigPane.add(panelSessionTimeFormat);

    /* Set collapsed by default */
    ((JXTaskPane) panelEventAnlogInput).setCollapsed(true);
    ((JXTaskPane) panelEventDigitalInput).setCollapsed(true);
    ((JXTaskPane) panelSessionMiscs).setCollapsed(true);
    ((JXTaskPane) panelSessionTimeFormat).setCollapsed(true);
  }

  public JLabel getLabel8() {
    return label8;
  }

  public JLabel getLabel12() {
    return label12;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
	createUIComponents();

	tabbedPane1 = new JTabbedPane();
	JScrollPane scrollPane2 = new JScrollPane();
	sessionConfigPane = new JXTaskPaneContainer();
	panelSessionMiscs = new JXTaskPane("Miscellaneous");
	labelAppConfirmTimeout = new JLabel();
	labelms4 = new JLabel();
	label8 = new JLabel();
	label12 = new JLabel();
	labelLinkStatusPeriod = new JLabel();
	labelms = new JLabel();
	label13 = new JLabel();
	labelms5 = new JLabel();
	label14 = new JLabel();
	labelms6 = new JLabel();
	label1 = new JLabel();
	label7 = new JLabel();
	panelSessionGeneral = new JXTaskPane("General");
	lblSessionName = new JLabel();
	label3 = new JLabel();
	label15 = new JLabel();
	label16 = new JLabel();
	label17 = new JLabel();
	labelsource = new JLabel();
	label5 = new JLabel();
	label10 = new JLabel();
	label4 = new JLabel();
	label9 = new JLabel();
	label6 = new JLabel();
	label11 = new JLabel();
	label2 = new JLabel();
	label18 = new JLabel();
	label19 = new JLabel();
	panelSessionFunctions = new JXTaskPane("Functions");

	//======== this ========
	setLayout(new BorderLayout());
	add(panelHint, BorderLayout.PAGE_END);

	//======== tabbedPane1 ========
	{

		//======== scrollPane2 ========
		{
			scrollPane2.setBorder(null);
			scrollPane2.setViewportView(sessionConfigPane);
		}
		tabbedPane1.addTab("Session Settings", scrollPane2);
	}
	add(tabbedPane1, BorderLayout.CENTER);

	//======== panelSessionMiscs ========
	{
		panelSessionMiscs.setLayout(new FormLayout(
			"right:default, $lcgap, [80dlu,default], $lcgap, default",
			"2*(default, $lgap), default, $pgap, default, $lgap, default, $rgap, default"));

		//---- labelAppConfirmTimeout ----
		labelAppConfirmTimeout.setText("Cyclic Period:");
		panelSessionMiscs.add(labelAppConfirmTimeout, CC.xy(1, 1));

		//---- ftfCyclicPeriod ----
		ftfCyclicPeriod.setColumns(8);
		panelSessionMiscs.add(ftfCyclicPeriod, CC.xy(3, 1));

		//---- labelms4 ----
		labelms4.setText("ms");
		panelSessionMiscs.add(labelms4, CC.xy(5, 1));

		//---- label8 ----
		label8.setText("Cyclic First Period:");
		panelSessionMiscs.add(label8, CC.xy(1, 3));

		//---- ftfCyclicFirstPeriod ----
		ftfCyclicFirstPeriod.setColumns(8);
		panelSessionMiscs.add(ftfCyclicFirstPeriod, CC.xy(3, 3));

		//---- label12 ----
		label12.setText("ms");
		panelSessionMiscs.add(label12, CC.xy(5, 3));

		//---- labelLinkStatusPeriod ----
		labelLinkStatusPeriod.setText("Background Period:");
		panelSessionMiscs.add(labelLinkStatusPeriod, CC.xy(1, 5));

		//---- ftfBackgroundPeriod ----
		ftfBackgroundPeriod.setColumns(8);
		panelSessionMiscs.add(ftfBackgroundPeriod, CC.xy(3, 5));

		//---- labelms ----
		labelms.setText("ms");
		panelSessionMiscs.add(labelms, CC.xy(5, 5));

		//---- label13 ----
		label13.setText("Select Timeout:");
		panelSessionMiscs.add(label13, CC.xy(1, 7));
		panelSessionMiscs.add(ftfSelectTimeout, CC.xy(3, 7));

		//---- labelms5 ----
		labelms5.setText("ms");
		panelSessionMiscs.add(labelms5, CC.xy(5, 7));

		//---- label14 ----
		label14.setText("Default Response Timeout:");
		panelSessionMiscs.add(label14, CC.xy(1, 9));
		panelSessionMiscs.add(ftfDefResponseTimeout, CC.xy(3, 9));

		//---- labelms6 ----
		labelms6.setText("ms");
		panelSessionMiscs.add(labelms6, CC.xy(5, 9));

		//---- label1 ----
		label1.setText("Max Poll Delay:");
		panelSessionMiscs.add(label1, CC.xy(1, 11));
		panelSessionMiscs.add(ftfMaxPollDelay, CC.xy(3, 11));

		//---- label7 ----
		label7.setText("ms");
		panelSessionMiscs.add(label7, CC.xy(5, 11));
	}

	//======== panelSessionGeneral ========
	{
		panelSessionGeneral.setLayout(new FormLayout(
			"right:default, $lcgap, [80dlu,default], left:3dlu, default, 30dlu, right:default, $lcgap, [80dlu,default], $lcgap, default, $ugap, default, $lcgap, default",
			"default, $ugap, default, $lgap, default, $pgap, default, $rgap, default, $lgap, default, $ugap, default"));

		//---- lblSessionName ----
		lblSessionName.setText("Session Name:");
		panelSessionGeneral.add(lblSessionName, CC.xy(1, 1));
		panelSessionGeneral.add(tfSessionName, CC.xy(3, 1));

		//---- label3 ----
		label3.setText("Link Address:");
		panelSessionGeneral.add(label3, CC.xy(1, 3));
		panelSessionGeneral.add(ftfLinkAddress, CC.xy(3, 3));

		//---- label15 ----
		label15.setText("Link Address Size:");
		panelSessionGeneral.add(label15, CC.xy(7, 3));

		//---- ftfLinkAddrSize ----
		ftfLinkAddrSize.setEditable(false);
		panelSessionGeneral.add(ftfLinkAddrSize, CC.xy(9, 3));

		//---- label16 ----
		label16.setText("octets");
		panelSessionGeneral.add(label16, CC.xy(11, 3));

		//---- label17 ----
		label17.setText("(Note this parameter is configured in Channel Settings)");
		panelSessionGeneral.add(label17, CC.xy(13, 3));

		//---- labelsource ----
		labelsource.setText("ASDU Address:");
		panelSessionGeneral.add(labelsource, CC.xy(1, 5));

		//---- ftfASDUAddress ----
		ftfASDUAddress.setColumns(8);
		panelSessionGeneral.add(ftfASDUAddress, CC.xy(3, 5));

		//---- label5 ----
		label5.setText("ASDU Address Size:");
		panelSessionGeneral.add(label5, CC.xy(7, 5));
		panelSessionGeneral.add(ftfASDUAddrSize, CC.xy(9, 5));

		//---- label10 ----
		label10.setText("octets");
		panelSessionGeneral.add(label10, CC.xy(11, 5));

		//---- label4 ----
		label4.setText("Max Frame Size:");
		panelSessionGeneral.add(label4, CC.xy(1, 7));
		panelSessionGeneral.add(ftfMaxFrameSize, CC.xy(3, 7));

		//---- label9 ----
		label9.setText("octets");
		panelSessionGeneral.add(label9, CC.xy(5, 7));

		//---- label6 ----
		label6.setText("IOA Size:");
		panelSessionGeneral.add(label6, CC.xy(1, 9));
		panelSessionGeneral.add(ftfObjAddrSize, CC.xy(3, 9));

		//---- label11 ----
		label11.setText("octets");
		panelSessionGeneral.add(label11, CC.xy(5, 9));

		//---- label2 ----
		label2.setText("COT Size:");
		panelSessionGeneral.add(label2, CC.xy(1, 11));
		panelSessionGeneral.add(ftfCOTSize, CC.xy(3, 11));

		//---- label18 ----
		label18.setText("octets");
		panelSessionGeneral.add(label18, CC.xy(5, 11));

		//---- label19 ----
		label19.setText("Commands Time Tag:");
		panelSessionGeneral.add(label19, CC.xy(1, 13));
		panelSessionGeneral.add(combCmdTimetagMode, CC.xy(3, 13));
	}

	//======== panelSessionFunctions ========
	{
		panelSessionFunctions.setLayout(new FormLayout(
			"left:default",
			"5*(default, $lgap), default"));

		//---- cbSendClkSynEvents ----
		cbSendClkSynEvents.setText("Send Clock Sync Events");
		cbSendClkSynEvents.setOpaque(false);
		panelSessionFunctions.add(cbSendClkSynEvents, CC.xy(1, 1));

		//---- cbDelOldestEvent ----
		cbDelOldestEvent.setText("Delete Oldest Event");
		cbDelOldestEvent.setOpaque(false);
		panelSessionFunctions.add(cbDelOldestEvent, CC.xy(1, 3));

		//---- cbCseUseActTerm ----
		cbCseUseActTerm.setText("Send ACT TERM upon completion of set point commands ");
		cbCseUseActTerm.setToolTipText(" Specify whether to send ACT TERM upon completion of set point commands");
		cbCseUseActTerm.setOpaque(false);
		panelSessionFunctions.add(cbCseUseActTerm, CC.xy(1, 5));

		//---- cbCmdUseActTerm ----
		cbCmdUseActTerm.setText("Send ACT TERM upon completion of commands other than set point commands ");
		cbCmdUseActTerm.setOpaque(false);
		panelSessionFunctions.add(cbCmdUseActTerm, CC.xy(1, 7));

		//---- cbStrictAdherence ----
		cbStrictAdherence.setText("Send PMENA/B/C in multiple point response.");
		cbStrictAdherence.setOpaque(false);
		cbStrictAdherence.setToolTipText("Send PMENA/B/C in multiple point response.");
		panelSessionFunctions.add(cbStrictAdherence, CC.xy(1, 9));

		//---- cbUseDayOfWeek ----
		cbUseDayOfWeek.setText("Use Day Of Week");
		cbUseDayOfWeek.setOpaque(false);
		cbUseDayOfWeek.setToolTipText("Send PMENA/B/C in multiple point response.");
		panelSessionFunctions.add(cbUseDayOfWeek, CC.xy(1, 11));
	}
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelHint;
  private JTabbedPane tabbedPane1;
  private JXTaskPaneContainer sessionConfigPane;
  private JPanel panelSessionMiscs;
  private JLabel labelAppConfirmTimeout;
  private JFormattedTextField ftfCyclicPeriod;
  private JLabel labelms4;
  private JLabel label8;
  private JFormattedTextField ftfCyclicFirstPeriod;
  private JLabel label12;
  private JLabel labelLinkStatusPeriod;
  private JFormattedTextField ftfBackgroundPeriod;
  private JLabel labelms;
  private JLabel label13;
  private JFormattedTextField ftfSelectTimeout;
  private JLabel labelms5;
  private JLabel label14;
  private JFormattedTextField ftfDefResponseTimeout;
  private JLabel labelms6;
  private JLabel label1;
  private JFormattedTextField ftfMaxPollDelay;
  private JLabel label7;
  private EventPanelAI panelEventAnlogInput;
  private JPanel panelSessionGeneral;
  private JLabel lblSessionName;
  private JTextField tfSessionName;
  private JLabel label3;
  private JFormattedTextField ftfLinkAddress;
  private JLabel label15;
  private JFormattedTextField ftfLinkAddrSize;
  private JLabel label16;
  private JLabel label17;
  private JLabel labelsource;
  private JFormattedTextField ftfASDUAddress;
  private JLabel label5;
  private JFormattedTextField ftfASDUAddrSize;
  private JLabel label10;
  private JLabel label4;
  private JFormattedTextField ftfMaxFrameSize;
  private JLabel label9;
  private JLabel label6;
  private JFormattedTextField ftfObjAddrSize;
  private JLabel label11;
  private JLabel label2;
  private JFormattedTextField ftfCOTSize;
  private JLabel label18;
  private JLabel label19;
  private JComboBox combCmdTimetagMode;
  private JPanel panelSessionFunctions;
  private JCheckBox cbSendClkSynEvents;
  private JCheckBox cbDelOldestEvent;
  private JCheckBox cbCseUseActTerm;
  private JCheckBox cbCmdUseActTerm;
  private JCheckBox cbStrictAdherence;
  private JCheckBox cbUseDayOfWeek;
  private EventPanelDI panelEventDigitalInput;
  private IEC870SessionTimeFormatPanel panelSessionTimeFormat;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
