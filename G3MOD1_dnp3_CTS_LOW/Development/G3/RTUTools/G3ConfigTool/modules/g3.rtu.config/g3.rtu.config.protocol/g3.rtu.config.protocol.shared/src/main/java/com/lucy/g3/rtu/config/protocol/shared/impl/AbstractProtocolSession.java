/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.impl;

import java.util.UUID;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.validate.ProtocolSessionValidator;
import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;

/**
 * Basic implementation of {@linkplain IProtocolSession}.
 */
public abstract class AbstractProtocolSession extends BeanWithEnable implements IProtocolSession {

  private String sessionName = "untitiled session";

  private final IProtocolChannel<? extends IProtocolSession> channel;

  private final ProtocolType protocolType;

  private String uuid = UUID.randomUUID().toString();
  
  private final ProtocolSessionValidator validator;

  protected AbstractProtocolSession(ProtocolType type, String sessionName) {
    this.protocolType = Preconditions.checkNotNull(type, "type must not be null");
    this.channel = null;
    
    this.validator = new ProtocolSessionValidator(this);
    setEnabled(true);
    setSessionName(sessionName);
  }
  
  protected AbstractProtocolSession(IProtocolChannel<? extends IProtocolSession> channel, String sessionName) {
    this.channel = Preconditions.checkNotNull(channel, "channel must not be null");
    this.protocolType  = channel.getProtocolType();
    
    this.validator = new ProtocolSessionValidator(this);
    setEnabled(true);
    setSessionName(sessionName);
  }

  @Override
  public ProtocolSessionValidator getValidator() {
    return validator;
  }

  @Override
  public final String getSessionName() {
    return sessionName;
  }

  public final void setSessionName(String name) {
    Object oldValue = getSessionName();
    this.sessionName = name;
    firePropertyChange(PROPERTY_SESSION_NAME, oldValue, name);
  }

  @Override
  public IProtocolChannel<? extends IProtocolSession> getProtocolChannel() {
    return channel;
  }

  @Override
  public ProtocolType getProtocolType() {
    return protocolType;
  }
  
  public final String getUUID() {
    return uuid;
  }
  
  public final void setUUID(String uuid) {
    this.uuid = uuid;
  }
}
