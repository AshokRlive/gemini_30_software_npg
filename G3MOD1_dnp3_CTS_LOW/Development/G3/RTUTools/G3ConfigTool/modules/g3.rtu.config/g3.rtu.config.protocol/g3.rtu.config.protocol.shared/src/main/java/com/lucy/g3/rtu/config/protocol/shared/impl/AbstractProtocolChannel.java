/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.impl;

import java.util.Collection;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.validate.ProtocolChannelValidator;

/**
 * Basic Implementation of IProtoSession.
 *
 * @param <SessionT>
 *          the Protocol session Type.
 */
public abstract class AbstractProtocolChannel<SessionT extends IProtocolSession>
    extends ItemListManager<SessionT>
    implements IProtocolChannel<SessionT> {

  private Logger log = Logger.getLogger(AbstractProtocolChannel.class);

  private final IProtocol<?> protocol;
  
  private boolean enabled = true;
  
  private String uuid = UUID.randomUUID().toString();
  
  private final ProtocolChannelValidator validator;
  private final ProtocolType protocolType;

  protected AbstractProtocolChannel(ProtocolType protocolType) {
    this.protocol = null;
    this.protocolType = protocolType;
    this.validator = new ProtocolChannelValidator(this);
  }
  protected AbstractProtocolChannel(IProtocol<?> protocol) {
    this.protocol = Preconditions.checkNotNull(protocol, "protocol must not be null");
    this.protocolType = protocol.getProtocolType();
    this.validator = new ProtocolChannelValidator(this);
  }
  
  @Override
  public final ProtocolChannelValidator getValidator() {
    return validator;
  }

  @Override
  public IProtocol<?> getProtocol() {
    return protocol;
  }

  @Override
  public ProtocolType getProtocolType() {
    return protocolType;
  }

  @Override
  public final Collection<SessionT> getAllSessions() {
    return getAllItems();
  }

  @Override
  public final int getSessionNum() {
    return getSize();
  }

  @Override
  public final String getChannelName() {
    return getChannelConfig().getChannelName();
  }

  @Override
  public final String toString() {
    return String.format("[%s] %s", getProtocol().getProtocolName(), getChannelName());
  }

  @Override
  public final void delete() {
    getChannelConfig().releaseResources();

    Collection<SessionT> sesns = getAllSessions();
    for (SessionT s : sesns) {
      remove(s);
    }

    log.info("Channel deleted: " + getChannelName());
  }

  @Override
  protected void postRemoveAction(SessionT removedItem) {
    super.postRemoveAction(removedItem);
    removedItem.delete();
  }

  @Override
  public boolean allowToAdd(SessionT item) throws AddItemException {
    return super.allowToAdd(item)
        && (allowMultiSession() || getSessionNum() == 0);
  }

  @Override
  public boolean allowMultiSession() {
    return true;
  }
  
  @Override
  final public boolean isEnabled() {
    return enabled;
  }
  
  @Override
  final public void setEnabled(boolean enabled) {
    Object oldValue = isEnabled();
    this.enabled = enabled;
    firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    
  }
  
  @Override
  public final String getUUID() {
    return uuid;
  }
  
  @Override
  public final void setUUID(String uuid) {
    this.uuid = uuid;
  }
}
