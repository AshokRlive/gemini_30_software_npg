/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio;

import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;


/**
 * A wizard for adding new field device channels to IO map. 
 */
public class MMBIOChannelAddingWizard{

  public static void showWizard(MMBIoMap iomap, ChannelType type) {
    Wizard wiz = WizardPage.createWizard(
        "Add New Field Device Channels", // title
        createPages(type),  // pages
        new Result(iomap, type)); // result
    
    wiz.show();
  }

  private static WizardPage[] createPages(ChannelType type) {
    WizardPage[] pages;
    
    switch (type) {
    case ANALOG_INPUT:
      pages = new WizardPage[]{
          new AIPage()
      };
      break;
      
    case DIGITAL_INPUT:
      pages = new WizardPage[]{
          new DIPage()
      };
      break;
      
    case DIGITAL_OUTPUT:
      pages = new WizardPage[]{
          new DOPage()
      };
      break;
      
    default:
      throw new IllegalArgumentException("Unsupported channel type: " + type);
    }
    
    return pages;
  }

}

