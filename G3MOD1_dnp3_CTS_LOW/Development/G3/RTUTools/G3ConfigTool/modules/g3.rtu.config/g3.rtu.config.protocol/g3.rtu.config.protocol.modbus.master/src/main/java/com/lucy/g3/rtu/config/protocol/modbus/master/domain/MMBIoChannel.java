/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import com.g3schema.ns_mmb.MMBDeviceChnlAnalogT;
import com.g3schema.ns_mmb.MMBDeviceChnlT;
import com.lucy.g3.rtu.config.channel.domain.AbstractChannel;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.channel.domain.IPredefinedChannelConfig;
import com.lucy.g3.rtu.config.channel.domain.ISupportChangeDescription;
import com.lucy.g3.rtu.config.channel.validation.IChannelValidator;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.protocol.modbus.master.validation.MMBIoChannelValidator;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.xml.gen.api.IChannelEnum;

/**
 * <p>
 * The ModBus IO channel.
 * </p>
 * {@code MMBIOChannel} implements the same interface of Gemini3 Slave module
 * channel(see {@code CANChannel}), so it can be mapped to virtual point and
 * integrated to G3 RTU.
 */
public final class MMBIoChannel extends AbstractChannel implements IChannel, ISupportChangeDescription {

  /**
   * The name of channel parameter {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PARAM_REG_ADDRESS = "Register Address";

  /**
   * The name of channel parameter {@value} . <li>Value type:
   * {@link RegisterType}.</li>
   */
  public static final String PARAM_REG_TYPE = "Register Type";

  /**
   * The name of channel parameter {@value} . Only for analogue channel. <li>
   * Value type: {@link RegisterDataType}.</li>
   */
  public static final String PARAM_DATA_TYPE = "Data Type";

  /**
   * The name of channel parameter {@value} . Only for digital channel. <li>
   * Value type: {@link MMBIoBitMask}.</li>
   */
  public static final String PARAM_BIT_MASK = "Bit Number";

  /**
   * The name of channel parameter {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PARAM_POLL_RATE = "Polling Rate(ms)";

  /**
   * The name of analogue channel parameter {@value} . <li>Value type:
   * {@link Double}.</li>
   */
  public static final String PARAM_SCALE_FACTOR = "Scaling Factor";

  /**
   * The name of analogue channel parameter {@value} . <li>Value type:
   * {@link Double}.</li>
   */
  public static final String PARAM_OFFSET = "Offset";

  /**
   * The name of analogue channel parameter {@value} . <li>Value type:
   * {@link String}.</li>
   */
  public static final String PARAM_UNIT = "Unit (Optional)";

  private int channelId;

  private final PredefinedMMBChannelConf predefined = new PredefinedMMBChannelConf(this);

  private final MMBIoChannelValidator validator;


  public MMBIoChannel(ChannelType channelType, int channelId, Module ownerModule,
      String description) {
    this(channelType, channelId, ownerModule, description, false);
  }

  public MMBIoChannel(ChannelType channelType, int channelId, Module ownerModule,
      String description, boolean readOnly) {
    super(channelType, ownerModule, description);

    this.channelId = channelId;
    this.validator = new MMBIoChannelValidator(this);

    setReadOnly(readOnly);

    initParams();
  }

  private void initParams() {
    super.addParameter(PARAM_REG_ADDRESS, 0L);
    ChannelType type = getType();

    if (type == ChannelType.ANALOG_INPUT) {
      super.addParameter(PARAM_REG_TYPE, RegisterType.INPUT_STATUS);
      super.addParameter(PARAM_DATA_TYPE, RegisterDataType.FLOAT32);
      super.addParameter(PARAM_POLL_RATE, 500L);
      super.addParameter(PARAM_SCALE_FACTOR, 1D);
      super.addParameter(PARAM_OFFSET, 0D);
      super.addParameter(PARAM_UNIT, "");

    } else if (type == ChannelType.DIGITAL_INPUT) {
      super.addParameter(PARAM_REG_TYPE, RegisterType.COIL_STATUS);
      super.addParameter(PARAM_BIT_MASK, MMBIoBitMask.BIT0);
      super.addParameter(PARAM_POLL_RATE, 500L);

    } else if (type == ChannelType.DIGITAL_OUTPUT) {
      super.addParameter(PARAM_REG_TYPE, RegisterType.COIL_STATUS);
    }

    setParameterModifiable(MMBIoChannel.PROPERTY_DESCRIPTION, true);
  }

  static String[] getParamNames(ChannelType type) {
    if (type == ChannelType.ANALOG_INPUT) {
      return new String[] {
          PARAM_REG_ADDRESS,
          PARAM_REG_TYPE,
          PARAM_DATA_TYPE,
          PARAM_POLL_RATE,
          PARAM_SCALE_FACTOR,
          PARAM_OFFSET,
          PARAM_UNIT,
      };
    }

    if (type == ChannelType.DIGITAL_INPUT) {
      return new String[] {
          PARAM_REG_ADDRESS,
          PARAM_REG_TYPE,
          PARAM_BIT_MASK,
          PARAM_POLL_RATE,
      };
    }

    if (type == ChannelType.DIGITAL_OUTPUT) {
      return new String[] {
          PARAM_REG_ADDRESS,
          PARAM_REG_TYPE,
      };
    }

    return null;
  }

  @Override
  public int getId() {
    return channelId;
  }

  public void setId(int channelId) {
    this.channelId = channelId;
  }

  @Override
  public String getGroup() {
    return ""; // group not supported
  }

  @Override
  public IChannelEnum getEnum() {
    return null;
  }

  @Override
  public boolean isParameterModifiable(String propertyName) {
    /*
     * For coils, bit mask is not applied, so don't allow user to modify it.
     */
    if (hasParameter(PARAM_BIT_MASK)) {
      if (getParameter(PARAM_REG_TYPE) == RegisterType.COIL_STATUS) {
        return !PARAM_BIT_MASK.equals(propertyName);
      }
    }

    /*
     * Digital Output channel's register type must not be changed since only the
     * coil type is supported for now.
     */
    if (PARAM_REG_TYPE.equals(propertyName) && getType() == ChannelType.DIGITAL_OUTPUT) {
      return false;
    }

    return super.isParameterModifiable(propertyName);
  }

  @Override
  public void setParameter(String paraName, Object value)
      throws UnsupportParamException, InvalidParamTypeException,
      NullPointerException {
    /*
     * When set register type to be coils, bit mask should also be forced to 0,
     * because coils is single bit value and cannot be applied bit mask.
     */
    if (hasParameter(PARAM_BIT_MASK)) {
      // Value is set to coil
      if (getParameter(paraName) != RegisterType.COIL_STATUS && value == RegisterType.COIL_STATUS) {
        setParameter(PARAM_BIT_MASK, MMBIoBitMask.BIT0);
      }
    }

    super.setParameter(paraName, value);
  }

  @Override
  public IPredefinedChannelConfig predefined() {
    return predefined;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
    // Nothing to do
  }

  /*
   * Override to change to public.
   */
  @Override
  public void setDescription(String description) {
    super.setDescription(description);
  }


  /**
   * This data type enum comes from REG_DATA_TYPE in ModbusConfigProtocol.h.
   */
  public enum RegisterDataType {
    FLOAT16("Float16", 2),
    FLOAT32("Float32", 4),
    INT16("Int16", 2),
    INT32("Int32", 4),
    UINT16("Unsigned Int16", 2),
    UINT32("Unsigned Int32", 4);

    RegisterDataType(String description,int bytes) {
      this.description = description;
      this.bytes = bytes;
    }

    @Override
    public String toString() {
      return description;
    }


    private final String description;
    private final int bytes;


    public static RegisterDataType forValue(int value) {
      RegisterDataType[] values = RegisterDataType.values();
      if (value >= 0 && value < values.length) {
        return values[value];
      } else {
        return null;
      }
    }
    
    public int getBytes() {
      return bytes;
    }
  }

  /**
   * This data type enum comes from REG_TYPE in ModbusConfigProtocol.h.
   */
  public enum RegisterType {
    COIL_STATUS("Coil"),
    INPUT_STATUS("Discrete Inputs"),
    HOLDING_REGISTER("Holding Register"),
    INPUT_REGISTER("Input Register");

    RegisterType(String description) {
      this.description = description;
    }

    @Override
    public String toString() {
      return getDescription();
    }

    public String getDescription(){
      return description;
    }
    private final String description;


    public static RegisterType forValue(int value) {
      RegisterType[] values = RegisterType.values();
      if (value >= 0 && value < values.length) {
        return values[value];
      } else {
        return null;
      }
    }
  }


  @Override
  public IChannelValidator getValidator() {
    return validator;
  }

  @Override
  public String getSourceName() {
    return getSourceModule() == null ? null : getSourceModule().getShortName();
  }

  String getUnit() {
    return hasParameter(PARAM_UNIT) ? (String) getParameter(PARAM_UNIT) : null;
  }

  public void writeToXML(MMBDeviceChnlAnalogT xml) {
    writeToXML((MMBDeviceChnlT) xml);

    xml.scalingFactor.setValue((Double) getParameter(PARAM_SCALE_FACTOR));
    xml.offset.setValue((Double) getParameter(PARAM_OFFSET));
    xml.unit.setValue((String) getParameter(PARAM_UNIT));
  }

  public void writeToXML(MMBDeviceChnlT xml) {
    xml.channelID.setValue(getId());
    xml.regAddress.setValue((Long) getParameter(MMBIoChannel.PARAM_REG_ADDRESS));

    if (hasParameter(MMBIoChannel.PARAM_REG_TYPE)) {
      xml.regType.setValue(((RegisterType) getParameter(MMBIoChannel.PARAM_REG_TYPE)).ordinal());
    } else {
      xml.regType.setValue(0);
    }

    if (hasParameter(MMBIoChannel.PARAM_DATA_TYPE)) {
      xml.dataType.setValue(((RegisterDataType) getParameter(MMBIoChannel.PARAM_DATA_TYPE)).ordinal());
    } else {
      xml.dataType.setValue(0);
    }

    if (hasParameter(MMBIoChannel.PARAM_POLL_RATE)) {
      xml.pollRate.setValue((Long) getParameter(MMBIoChannel.PARAM_POLL_RATE));
    } else {
      xml.pollRate.setValue(0);
    }

    if (hasParameter(MMBIoChannel.PARAM_BIT_MASK)) {
      xml.bitMask.setValue(((MMBIoBitMask) getParameter(MMBIoChannel.PARAM_BIT_MASK)).getMask());
    } else {
      xml.bitMask.setValue(0);
    }

    xml.description.setValue(getDescription());
  }

  public void readFromXML(MMBDeviceChnlAnalogT xml) {
    readFromXML((MMBDeviceChnlT) xml);

    if (xml.scalingFactor.exists()) {
      setIoChannelParam(MMBIoChannel.PARAM_SCALE_FACTOR, xml.scalingFactor.getValue());
    }

    if (xml.offset.exists()) {
      setIoChannelParam(MMBIoChannel.PARAM_OFFSET, xml.offset.getValue());
    }

    if (xml.unit.exists()) {
      setIoChannelParam(MMBIoChannel.PARAM_UNIT, xml.unit.getValue());
    }
  }

  public void readFromXML(MMBDeviceChnlT xml) {
    setIoChannelParam(MMBIoChannel.PARAM_DATA_TYPE, RegisterDataType.forValue(xml.dataType.getValue()));
    setIoChannelParam(MMBIoChannel.PARAM_POLL_RATE, xml.pollRate.getValue());
    setIoChannelParam(MMBIoChannel.PARAM_REG_ADDRESS, xml.regAddress.getValue());
    setIoChannelParam(MMBIoChannel.PARAM_REG_TYPE, RegisterType.forValue(xml.regType.getValue()));
    setIoChannelParam(MMBIoChannel.PARAM_BIT_MASK, MMBIoBitMask.forMask(xml.bitMask.getValue()));
  }

  private void setIoChannelParam(String paramName, Object paramValue) {
    if (hasParameter(paramName) && isParameterModifiable(paramName)) {
      setParameter(paramName, paramValue);
    }
  }

  public static RegisterType[] getAvailabelRegisterTypes(ChannelType type) {
    if(type == ChannelType.ANALOG_INPUT)
      return new RegisterType[] {
        RegisterType.HOLDING_REGISTER,
        RegisterType.INPUT_REGISTER};
    
    else if(type == ChannelType.DIGITAL_OUTPUT)
      return new RegisterType[] {
        RegisterType.COIL_STATUS};
    
    return MMBIoChannel.RegisterType.values();
  }

  public static RegisterDataType[] getAvailableDataTypes(ChannelType type) {
    if(type == ChannelType.ANALOG_INPUT)
      return new RegisterDataType[]{
        RegisterDataType.FLOAT32,
        RegisterDataType.INT16,
        RegisterDataType.INT32,
        RegisterDataType.UINT16,
        RegisterDataType.UINT32,
    };
    
    return new RegisterDataType[0];
  }

  
  /**
   * This data type enum comes from BIT_MASK in ModbusConfigProtocol.h.
   */
  public enum MMBIoBitMask {
    BIT0("Bit 0 "),
    BIT1("Bit 1 "),
    BIT2("Bit 2 "),
    BIT3("Bit 3 "),
    BIT4("Bit 4 "),
    BIT5("Bit 5 "),
    BIT6("Bit 6 "),
    BIT7("Bit 7 "),
    BIT8("Bit 8 "),
    BIT9("Bit 9 "),
    BIT10("Bit 10"),
    BIT11("Bit 11"),
    BIT12("Bit 12"),
    BIT13("Bit 13"),
    BIT14("Bit 14"),
    BIT15("Bit 15");

    MMBIoBitMask(String description) {
      this.description = description;
    }

    @Override
    public String toString() {
      return description;
    }


    private final String description;


    public short getMask() {
      int bit = ordinal();
      short mask = 1;
      mask = (short) (mask << bit);
      return mask;
    }

    public static MMBIoBitMask forMask(int mask) {
      short bit;

      for (int i = 0; i < 16; i++) {
        bit = 1;
        bit = (short) (bit << i);
        int result = (mask & bit);
        if (result != 0) {
          return MMBIoBitMask.values()[i];
        }

      }

      return null;
    }
  }
}
