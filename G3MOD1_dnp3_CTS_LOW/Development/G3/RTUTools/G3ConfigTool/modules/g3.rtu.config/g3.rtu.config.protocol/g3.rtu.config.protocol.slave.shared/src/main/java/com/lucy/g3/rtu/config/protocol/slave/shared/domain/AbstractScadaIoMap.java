/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractIoMap;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

/**
 * The basic implementation of ScadaIoMap.
 *
 * @param <PointT>
 *          the ScadaPoint type.
 */
public abstract class AbstractScadaIoMap<PointT extends ScadaPoint> extends AbstractIoMap implements IScadaIoMap<PointT> {

  private Logger log = Logger.getLogger(AbstractScadaIoMap.class);
  
  protected final IProtocolSession session;
  
  private final HashMap<ScadaPointType, ArrayListModel<PointT>> pointsMap =
      new HashMap<ScadaPointType, ArrayListModel<PointT>>();
  
  private final ArrayListModel<PointT> inputPointsModel = new ArrayListModel<>();
  
  private final ScadaPointType[] pointTypes;
  
  protected AbstractScadaIoMap(IProtocolSession session, ScadaPointType[] supportedPointTypes) {
    this.session = Preconditions.checkNotNull(session, "session must not be null");
    this.pointTypes = Preconditions.checkNotNull(supportedPointTypes, "supportedPointTypes must not be null");
    
    /* Init points map */
    for (int i = 0; i < supportedPointTypes.length; i++) {
      pointsMap.put(supportedPointTypes[i], new ArrayListModel<PointT>());
    }
  }

  @Override
  public final IProtocolSession getSession() {
    return session;
  }
  
  @Override
  public final void addPoint(PointT newPoint) throws DuplicatedException {
    if (newPoint == null) {
      log.error("Cannot add point. Invalid point:" + newPoint);
      return;
    }

    PointT point = newPoint;
    if (exists(point.getType(), point.getPointID())) {
      throw new DuplicatedException("IEC104 point " + point.getPointID() + " - duplicated point found!");
    }

    ScadaPointType type = newPoint.getType();
    getPointList(type).add(newPoint);

    if (type.isInput()) {
      inputPointsModel.add(newPoint);
    }

    log.debug("Add Scada Protocol point: " + point);
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public void removeScadaPoint(ScadaPoint point) {
    if (point != null) {
      removePoint((PointT) point);
    }
  }
  
  @Override
  public void removePoint(PointT point) {
    if (point == null) {
      return;
    }
    ScadaPointType type = point.getType();
    getPointList(type).remove(point);
    if (type.isInput()) {
      inputPointsModel.remove(point);
    }
    point.delete();
  }

  @Override
  public void removeAllPoints(Collection<PointT> allpoints) {
    if (allpoints != null) {
      for (PointT p : allpoints) {
        removePoint(p);
      }
    }
  }

  @Override
  public final void clearIoMap() {
    for (ScadaPointType type : pointTypes) {
      ArrayListModel<PointT> list = pointsMap.get(type);
      for (PointT p : list) {
        p.delete();
      }
      list.clear();
    }
    
    inputPointsModel.clear();
  }
  
  private ArrayListModel<PointT> getPointList(ScadaPointType type) {
    return pointsMap.get(type);
  }

  @Override
  public Collection<PointT> getAllOutputPoints() {
    ArrayList<PointT> list = new ArrayList<>();
    
    for (ScadaPointType type : pointTypes) {
      if(type.isOutput())
        list.addAll(pointsMap.get(type));
    }
    
    return list;
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public ListModel<PointT> getAllInputPointsModel() {
    return inputPointsModel;
  }
  
  @Override
  public PointT getPoint(ScadaPointType type, long id) {
    Collection<PointT> points = getPoints(type);
    for (PointT p : points) {
      if (p.getPointID() == id) {
        return p;
      }
    }
    return null;
  }

  @Override
  public Collection<PointT> getPoints(ScadaPointType type) {
    ArrayListModel<PointT> list = pointsMap.get(type);
    return list == null ? new ArrayList<PointT>(0) : new ArrayList<PointT>(list);
  }

  @Override
  public boolean exists(ScadaPointType type, long pointId) {
    return getPoint(type, pointId) != null;
  }

  @Override
  public int getPointsNum(ScadaPointType type) {
    ArrayListModel<PointT> list = pointsMap.get(type);
    return list == null ? 0 : list.size();
  }

  @Override
  public Collection<PointT> getAllInputPoints() {
    return new ArrayList<PointT>(inputPointsModel);
  }

  @Override
  public Collection<PointT> getAllPoints() {
    ArrayList<PointT> allPoints = new ArrayList<PointT>();

    Set<ScadaPointType> types = pointsMap.keySet();
    for (ScadaPointType type : types) {
      allPoints.addAll(pointsMap.get(type));
    }
    return allPoints;
  }

  @Override
  @SuppressWarnings("unchecked")
  public ListModel<PointT> getPointListModel(ScadaPointType type) {
    return pointsMap.get(type);
  }

  @Override
  public final long findAvailablePointID(long startingPoint, ScadaPointType type) {
    Preconditions.checkNotNull(type, "type must not be null");

    long id = startingPoint;
    while (id < type.getMaximumPointID()) {
      if (!exists(type, id)) {
        return id;
      }
      id++;
    }
    return 0;
  }

  @Override
  public long findAvailablePointID(ScadaPointType type) {
    Preconditions.checkNotNull(type, "type must not be null");

    return findAvailablePointID(type.getMinimumPointID(), type);
  }

  @Override
  public boolean checkPointID(long pointID, ScadaPointType type) {
    return pointID <= type.getMaximumPointID() && pointID >= type.getMinimumPointID();
  }
  
  @Override
  public final Collection<?> getAllItems() {
    return getAllPoints();
  }
  
  @Override
  public void copyFrom(IIoMap iomap) {
    if (iomap != null && iomap instanceof IScadaIoMap) {
      @SuppressWarnings("unchecked")
      IScadaIoMap<? extends ScadaPoint> scadaIoMap = (IScadaIoMap<? extends ScadaPoint>) iomap;
      
      ScadaIoMapCopySupport.copyIoMap(session.getProtocolType(), scadaIoMap, this);
      log.info("Copying IOMap is done sucessfully!");
      
    } else {
      log.error("Cannot copy from iomap: " + iomap);
    }
  }
   
}
