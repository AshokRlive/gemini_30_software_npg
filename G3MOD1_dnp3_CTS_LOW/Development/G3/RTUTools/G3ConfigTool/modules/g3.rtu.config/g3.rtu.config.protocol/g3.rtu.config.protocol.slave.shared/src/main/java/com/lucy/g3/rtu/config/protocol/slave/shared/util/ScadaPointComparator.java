/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.util;

import java.util.Comparator;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;

/**
 * The comparator for ScadaPoint.
 */
public final class ScadaPointComparator implements Comparator<ScadaPoint> {

  @Override
  public int compare(ScadaPoint o1, ScadaPoint o2) {
    if (o1 == null || o2 == null) {
      return -1;
    }

    long id0 = o1.getPointID();
    long id1 = o2.getPointID();

    return (int) (id0 - id1);
  }
}