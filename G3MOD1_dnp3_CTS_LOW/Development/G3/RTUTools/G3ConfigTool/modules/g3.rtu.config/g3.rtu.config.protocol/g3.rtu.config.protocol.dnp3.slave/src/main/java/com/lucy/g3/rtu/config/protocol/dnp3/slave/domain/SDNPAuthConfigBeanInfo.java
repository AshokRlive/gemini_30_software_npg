/****************************************************************r***************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.ue
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import com.l2fprod.common.beans.editor.ComboBoxPropertyEditor;
import com.lucy.g3.rtu.config.shared.base.bean.BeanInfo;


/**
 *
 */
public class SDNPAuthConfigBeanInfo extends BeanInfo {

  public SDNPAuthConfigBeanInfo() {
    super(SDNPAuthConfig.class);
    
    addProperty(SDNPAuthConfig.PROPERTY_AUTHENTICATION_ENABLED   ).setHidden(true); 
    addProperty(SDNPAuthConfig.PROPERTY_ASSOCID                  );
    addProperty(SDNPAuthConfig.PROPERTY_MACALGORITHM             ).setPropertyEditorClass(MACEditor.class);
    addProperty(SDNPAuthConfig.PROPERTY_MAXAPPLTIMEOUTCOUNT      );
    addProperty(SDNPAuthConfig.PROPERTY_REPLYTIMEOUTMS           );
    addProperty(SDNPAuthConfig.PROPERTY_KEYCHANGEINTERVALMS      );
    addProperty(SDNPAuthConfig.PROPERTY_MAXKEYCHANGECOUNT        );
    addProperty(SDNPAuthConfig.PROPERTY_AGGRESSIVEMODESUPPORT    );
    addProperty(SDNPAuthConfig.PROPERTY_DISALLOWSHA1             );
    addProperty(SDNPAuthConfig.PROPERTY_EXTRADIAGS               );
    addProperty(SDNPAuthConfig.PROPERTY_OPERATEINV2MODE          );
    addProperty(SDNPAuthConfig.PROPERTY_MAXERRORCOUNT            );
    addProperty(SDNPAuthConfig.PROPERTY_RANDOMCHALLENGEDATALENGTH); 
    addProperty(SDNPAuthConfig.PROPERTY_MAXSESSIONKEYSTATUSCOUNT );
    addProperty(SDNPAuthConfig.PROPERTY_MAXAUTHENTICATIONFAILURES); 
    addProperty(SDNPAuthConfig.PROPERTY_MAXREPLYTIMEOUTS         );
    addProperty(SDNPAuthConfig.PROPERTY_MAXAUTHENTICATIONREKEYS  );
    addProperty(SDNPAuthConfig.PROPERTY_MAXERRORMESSAGESSENT     ); 
  }
  
  
  public static class MACEditor extends ComboBoxPropertyEditor {

    public MACEditor() {
      super();
      setAvailableValues(SDNPAuthEnums.DNPAUTH_MAC.values());
    }
  }

}

