/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr;

import com.g3schema.ns_sdnp3.ConnectionManagerT;
import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * This Bean class contains all configuration for ConnectionManager.
 * <p>
 * Note some configuration can be accessed from children beans inside this bean.
 * </p>
 */
public final class ConnectionManager extends BeanWithEnable {

  private final FailoverGroup failoverGroup = new FailoverGroup();
  private final CommsPowerCycle commsPowerCycle = new CommsPowerCycle();
  private final ConnectionSettings connectionSettings = new ConnectionSettings();


  public ConnectionManager() {
    super();
    setEnabled(false);// Disable Connection Manager by default
  }

  public FailoverGroup getFailoverGroup() {
    return failoverGroup;
  }

  public CommsPowerCycle getCommsPowerCycle() {
    return commsPowerCycle;
  }

  public ConnectionSettings getConnectionSettings() {
    return connectionSettings;
  }

  public void readFromXML(ConnectionManagerT xml, VirtualPointReadSupport chnlRes) {
    getFailoverGroup().readFromXML(xml);
    getConnectionSettings().readFromXML(xml);

    CommsPowerCycle powercycle = getCommsPowerCycle();
    if (xml.commsPowerCycle.exists()) {
      powercycle.setEnabled(true);
      powercycle.readFromXML(xml.commsPowerCycle.first(), chnlRes);
    } else {
      powercycle.setEnabled(false);
    }

    setEnabled(xml.enabled.getValue());
  }

  public void writeToXML(ConnectionManagerT xml, VirtualPointWriteSupport support) {
    getFailoverGroup().writeToXML(xml);
    getConnectionSettings().writeToXML(xml);

    CommsPowerCycle powercycle = getCommsPowerCycle();
    if (powercycle.isEnabled()) {
      powercycle.writeToXML(xml.commsPowerCycle.append(), support);
    }

    xml.enabled.setValue(isEnabled());
  }

}
