/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;

public class SDNP3ChannelManagerPage extends ProtocolChannelManagerPage {

  public SDNP3ChannelManagerPage(SDNP3 sdnp3) {
    super(sdnp3, new SDNP3ChannelManagerModel(sdnp3));
  }


  private static final class SDNP3ChannelManagerModel extends AbstractProtocolItemManagerModel {

    private final SDNP3 sdnp3;
    private final SelectionInList<SDNP3Channel> selectionInList;


    public SDNP3ChannelManagerModel(SDNP3 dnp3) {
      this.sdnp3 = Preconditions.checkNotNull(dnp3, "dnp3 is null");
      this.selectionInList = new SelectionInList<SDNP3Channel>(dnp3.getItemListModel());
    }

    @Override
    public SelectionInList<SDNP3Channel> getSelectionInList() {
      return selectionInList;
    }

    @Override
    protected void addActionPerformed() {
      Object result = ProtocolAddingWizards.showAddChannel(sdnp3);
      if (result != null)
        selectionInList.setSelection((SDNP3Channel) result);
    }

    @Override
    protected void removeActionPerformed() {
      SDNP3Channel sel = selectionInList.getSelection();

      if (sel != null) {
        if (showConfirmRemoveDialog(sel.getChannelName())) {
          sdnp3.remove(sel);
        }
      }
    }

  }
}