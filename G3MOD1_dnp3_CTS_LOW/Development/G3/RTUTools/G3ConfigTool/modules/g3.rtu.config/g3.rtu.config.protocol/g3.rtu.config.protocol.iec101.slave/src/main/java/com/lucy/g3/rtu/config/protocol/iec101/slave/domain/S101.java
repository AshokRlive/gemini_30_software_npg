/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;

/**
 * The IEC104 slave protocol stack.
 */
public class S101 extends IEC870<S101Channel> {
  
  public static final String IOA_TEXT = "IOA";
  
  public S101() {
    super(ProtocolType.S101);
  }

  @Override
  public Collection<S101Session> getAllSessions() {
    ArrayList<S101Session> allSesns = new ArrayList<S101Session>();
    Collection<S101Channel> channelList = getAllChannels();
    for (S101Channel chnl : channelList) {
      allSesns.addAll(chnl.getAllSessions());
    }

    return allSesns;
  }
  
  @Override
  public S101Channel addChannel(String channelName) {
    S101Channel newChnl = new S101Channel(this, channelName);
    if (add(newChnl)) {
      return newChnl;
    } else {
      return null;
    }
  }

  public Collection<IEC870Point> getAllS104InputPoints() {
    return findS104Points(super.getAllInputPoints());
  }

  public Collection<IEC870Point> getAllS104Points() {
    return findS104Points(super.getAllPoints());
  }

  private static Collection<IEC870Point> findS104Points(Collection<ScadaPoint> points) {
    ArrayList<IEC870Point> sdnp3Points = new ArrayList<IEC870Point>();

    for (ScadaPoint p : points) {
      if (p instanceof IEC870Point) {
        sdnp3Points.add((IEC870Point) p);
      }
    }

    return sdnp3Points;
  }

}
