/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import org.apache.commons.lang3.ArrayUtils;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.EventConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjVarConsts;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_EVENT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;

/**
 * The enum constants of SDNP3.
 */
public final class SDNP3Enums extends DNP3ObjVarConsts {

  public static LU_EVENT_MODE[] getEventModeEnums(int type) {
    LU_EVENT_MODE[] modes = LU_EVENT_MODE.values();
    /* refs #F1958 "Per Point" Event Mode not supported yet */
    modes = ArrayUtils.removeElement(modes, LU_EVENT_MODE.LU_EVENT_MODE_PER_POINT);

    /* "Current" Event Mode is only applicable to Analogue Input values */
    if (EventConfig.AI != type) {
      modes = ArrayUtils.removeElement(modes, LU_EVENT_MODE.LU_EVENT_MODE_CURRENT);
    }

    return modes;
  }

  public static LU_LINTCP_MODE[] getTCPModeEnums() {
    return LU_LINTCP_MODE.values();
  }

  private SDNP3Enums() {
  }

}
