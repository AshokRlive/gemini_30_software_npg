/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui;

import com.lucy.g3.rtu.config.shared.base.page.AbstractItemManagerModel;

/**
 * An abstract item manager model for manager protocols items such as channel,
 * session.
 */
public abstract class AbstractProtocolItemManagerModel extends AbstractItemManagerModel {

//  protected final String askUserForSessionName() {
//    return JOptionPane.showInputDialog(parent, "Please input session name:", "New Session");
//  }
  
//
//  protected final String askUserForChannelName() {
//    return JOptionPane.showInputDialog(parent, "Please input channel name:", "New Channel");
//  }
}
