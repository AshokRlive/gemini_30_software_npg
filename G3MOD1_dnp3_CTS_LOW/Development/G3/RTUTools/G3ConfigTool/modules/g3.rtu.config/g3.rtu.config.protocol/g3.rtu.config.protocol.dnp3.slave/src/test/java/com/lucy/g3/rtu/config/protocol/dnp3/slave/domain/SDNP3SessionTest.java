/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.beans.PropertyVetoException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class SDNP3SessionTest {

  final static private String[] READ_ONLY_PROPERTIES = {
      SDNP3Session.PROPERTY_ADDRESS,
      SDNP3Session.PROPERTY_UNSOLCLASSMAXEVENTS,
      SDNP3Session.PROPERTY_UNSOLCLASSMAXDELAYS,
      SDNP3Session.PROPERTY_GLOBAL_EVT_VARIATIONS,
      SDNP3Session.PROPERTY_EVENTCONFIG_BI,
      SDNP3Session.PROPERTY_EVENTCONFIG_DI,
      SDNP3Session.PROPERTY_EVENTCONFIG_AI,
      SDNP3Session.PROPERTY_EVENTCONFIG_CT,
      SDNP3Session.PROPERTY_EVENTCONFIG_FCT,
  };

  final static private String[] READ_WRITE_PROPERTIES = {
      SDNP3Session.PROPERTY_SESSION_NAME,
      SDNP3Session.PROPERTY_LINKSTATUS_PERIOD,
      SDNP3Session.PROPERTY_MULTIFRAGRESPALLOWED,
      SDNP3Session.PROPERTY_MULTIFRAGCONFIRM,
      SDNP3Session.PROPERTY_RESPONDNEEDTIME,
      SDNP3Session.PROPERTY_APPLCONFIRMTIMEOUT,
      SDNP3Session.PROPERTY_SELECTTIMEOUT,
      SDNP3Session.PROPERTY_UNSOLALLOWED,
      SDNP3Session.PROPERTY_UNSOLCLASSMASK,
      SDNP3Session.PROPERTY_CLASS1ENABLED,
      SDNP3Session.PROPERTY_CLASS2ENABLED,
      SDNP3Session.PROPERTY_CLASS3ENABLED,
//      SDNP3Session.PROPERTY_UNSOLMAXRETRIES,
//      SDNP3Session.PROPERTY_UNSOLRETRYDELAY,
//      SDNP3Session.PROPERTY_UNSOLOFFLINERETRYDELAY,
  };

  private SDNP3Session fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new SDNP3Channel(new SDNP3()).addSession("");
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

  @Test
  public void testReadONLYProperties() {
    BeanTestUtil.testReadOnlyProperties(fixture, READ_ONLY_PROPERTIES);
  }

  @Test
  public void testReadWriteProperties() {
    BeanTestUtil.testReadWriteProperties(fixture, READ_WRITE_PROPERTIES);
  }

  @Test
  public void testUnsoBitMaskUpdate() throws PropertyVetoException {
    fixture.setUnsolClassMask(0L);

    fixture.setClass1Enabled(true);
    assertEquals(1, fixture.getUnsolClassMask());

    fixture.setClass2Enabled(true);
    assertEquals(3, fixture.getUnsolClassMask());

    fixture.setClass3Enabled(true);
    assertEquals(7, fixture.getUnsolClassMask());

    fixture.setClass1Enabled(false);
    assertEquals(6, fixture.getUnsolClassMask());

    fixture.setClass2Enabled(false);
    assertEquals(4, fixture.getUnsolClassMask());
  }

  @Test
  public void testUnsoBitMask() throws PropertyVetoException {
    fixture.setUnsolClassMask(0);
    assertFalse(fixture.isClass1Enabled());
    assertFalse(fixture.isClass2Enabled());
    assertFalse(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(1);
    assertTrue(fixture.isClass1Enabled());
    assertFalse(fixture.isClass2Enabled());
    assertFalse(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(2);
    assertFalse(fixture.isClass1Enabled());
    assertTrue(fixture.isClass2Enabled());
    assertFalse(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(3);
    assertTrue(fixture.isClass1Enabled());
    assertTrue(fixture.isClass2Enabled());
    assertFalse(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(4);
    assertFalse(fixture.isClass1Enabled());
    assertFalse(fixture.isClass2Enabled());
    assertTrue(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(5);
    assertTrue(fixture.isClass1Enabled());
    assertFalse(fixture.isClass2Enabled());
    assertTrue(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(6);
    assertFalse(fixture.isClass1Enabled());
    assertTrue(fixture.isClass2Enabled());
    assertTrue(fixture.isClass3Enabled());

    fixture.setUnsolClassMask(7);
    assertTrue(fixture.isClass1Enabled());
    assertTrue(fixture.isClass2Enabled());
    assertTrue(fixture.isClass3Enabled());
  }
}
