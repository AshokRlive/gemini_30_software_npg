/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import org.apache.commons.lang3.ArrayUtils;

import com.lucy.g3.common.utils.CollectionUtils;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_EVENT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;

/**
 * The enum constants of IEC870.
 */
public final class IEC870Enums {

  // @formatter:off
  private static LU_LINTCP_MODE   [] linTCPModes;
  private static LU_EVENT_MODE    [] eventModes;
  private static LU_TIME_FORMAT   [] timeformats;
  // @formatter:on

  public static LU_TIME_FORMAT[] getTimeFormatEnums() {
    if (timeformats == null) {
      timeformats = LU_TIME_FORMAT.values();
    }

    return CollectionUtils.copy(timeformats);
  }

  public static LU_LINTCP_MODE[] getTCPModeEnums() {
    if (linTCPModes == null) {
      linTCPModes = LU_LINTCP_MODE.values();

      // Remove the mode IEC870 doesn't support
      linTCPModes = ArrayUtils.removeElement(linTCPModes,
          LU_LINTCP_MODE.LU_LINTCP_MODE_DUAL_ENDPOINT);

      linTCPModes = ArrayUtils.removeElement(linTCPModes,
          LU_LINTCP_MODE.LU_LINTCP_MODE_CLIENT);
    }

    return CollectionUtils.copy(linTCPModes);
  }

  public static LU_EVENT_MODE[] getEventModeEnums() {
    if (eventModes == null) {
      eventModes = LU_EVENT_MODE.values();

      // Remove the mode IEC870 doesn't support
      eventModes = ArrayUtils.removeElement(eventModes,
          LU_EVENT_MODE.LU_EVENT_MODE_CURRENT);

      eventModes = ArrayUtils.removeElement(eventModes,
          LU_EVENT_MODE.LU_EVENT_MODE_PER_POINT);
    }

    return ArrayUtils.clone(eventModes);
  }

  private IEC870Enums() {
  }

}
