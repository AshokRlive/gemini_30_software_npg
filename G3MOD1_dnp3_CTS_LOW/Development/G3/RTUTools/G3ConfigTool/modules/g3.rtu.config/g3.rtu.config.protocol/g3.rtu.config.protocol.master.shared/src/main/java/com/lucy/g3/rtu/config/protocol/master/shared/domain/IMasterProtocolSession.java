/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.domain;

import java.beans.PropertyVetoException;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;

/**
 * The Interface Master Protocol Session.
 */
public interface IMasterProtocolSession extends IProtocolSession {

  /**
   * Gets the device of this session.
   *
   * @return the non-null device object.
   */
  IMasterProtocolModule getDevice();

  /**
   * @return
   */
  long getSlaveAddress();

  /**
   * @param slaveAddress
   * @throws PropertyVetoException
   */
  void setSlaveAddress(long slaveAddress) throws PropertyVetoException;
}
