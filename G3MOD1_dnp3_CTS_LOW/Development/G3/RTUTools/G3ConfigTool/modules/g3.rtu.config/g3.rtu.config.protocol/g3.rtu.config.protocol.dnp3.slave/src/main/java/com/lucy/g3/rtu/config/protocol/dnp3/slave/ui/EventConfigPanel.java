/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.EventConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Enums;

/**
 * A panel for configuring Event mode and max events for SDNP3.
 */
class EventConfigPanel extends JPanel {

  private final PresentationModel<EventConfig> pm;

  private final int type;


  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private EventConfigPanel() {
    type = 0;
    pm = new PresentationModel<>(new EventConfig());
    initComponents();
  }

  public EventConfigPanel(EventConfig bean, int type) {
    pm = new PresentationModel<EventConfig>(bean);
    this.type = type;
    initComponents();
  }

  private void createUIComponents() {
    ComponentsFactory factory =
        new ComponentsFactory(SDNP3Constraints.INSTANCE, SDNP3Constraints.PREFIX_SESSION_EVENT, pm);

    ftfMaxEvent = factory.createNumberField(EventConfig.PROPERTY_MAX_EVENTS);
    comboEventMode = factory.createComboBox(EventConfig.PROPERTY_EVENTMODE, SDNP3Enums.getEventModeEnums(type));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    // ======== this ========
    setOpaque(false);
    setLayout(new FormLayout(
        "[70dlu,default]",
        "default, $lgap, default"));
    add(ftfMaxEvent, CC.xy(1, 1));
    add(comboEventMode, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JFormattedTextField ftfMaxEvent;
  private JComboBox comboEventMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
