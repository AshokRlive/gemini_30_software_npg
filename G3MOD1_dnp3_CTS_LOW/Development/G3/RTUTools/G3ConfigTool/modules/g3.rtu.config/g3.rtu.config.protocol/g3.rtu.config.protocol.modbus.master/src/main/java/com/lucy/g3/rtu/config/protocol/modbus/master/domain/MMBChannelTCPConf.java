/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import com.g3schema.ns_pstack.TCPConfT;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;

/**
 * The TCP configuration of MMB channel.
 */
public final class MMBChannelTCPConf extends ProtocolChannelTCPConf {
  
  public MMBChannelTCPConf() {
    setAnyIPEnabled(false);
  }
  
  /* Override to change to public */
  @Override
  public void writeToXML(TCPConfT xml) {
    super.writeToXML(xml);
  }

  /* Override to change to public */
  @Override
  public void readFromXML(TCPConfT xml) {
    super.readFromXML(xml);
  }

}
