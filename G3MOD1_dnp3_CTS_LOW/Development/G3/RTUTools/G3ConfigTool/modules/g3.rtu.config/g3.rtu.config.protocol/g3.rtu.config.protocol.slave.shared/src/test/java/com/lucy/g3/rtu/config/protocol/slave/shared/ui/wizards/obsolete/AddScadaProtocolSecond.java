/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.wizards.obsolete;

import javax.swing.JCheckBox;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
class AddScadaProtocolSecond extends WizardPage {
  public final static String KEY_ADD_CHANNEL = "addChannel";
  public final static String KEY_ADD_SESSION  = "addSession";
  public final static String KEY_GEN_POINTS  = "genPoints";
  
  public AddScadaProtocolSecond() {
    super("Auto Generate");
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout(
        "default, 50dlu, default",
        ""), this);
    
    JCheckBox cbox;
    
    cbox = new JCheckBox("Add a channel");
    cbox.setName(KEY_ADD_CHANNEL);
    cbox.setSelected(true);
    builder.append("Options:", cbox);
    builder.nextLine();
    
    cbox = new JCheckBox("Add a session");
    cbox.setName(KEY_ADD_SESSION);
    cbox.setSelected(true);
    builder.append("", cbox);
    builder.nextLine();
    
    cbox = new JCheckBox("Generate protocol points");
    cbox.setName(KEY_GEN_POINTS);
    cbox.setSelected(true);
    builder.append("", cbox);
    builder.nextLine();
    
  }
}
