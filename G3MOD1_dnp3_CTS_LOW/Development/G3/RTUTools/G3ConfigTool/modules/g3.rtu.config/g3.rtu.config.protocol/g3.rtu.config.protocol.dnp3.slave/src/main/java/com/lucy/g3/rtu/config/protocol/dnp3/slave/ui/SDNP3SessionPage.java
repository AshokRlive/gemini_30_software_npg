/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.AbstractWrappedValueModel;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.PropertyEditorDialog;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.ObjectToStringConverter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3IoMap;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.Address;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.EventConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.GlobalVariationsConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.UnsolClassMaxDelays;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.UnsolClassMaxEvents;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3User;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.common.UnsolRetryVisibilityUpdater;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point.VariationsRenderer;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.wizards.unsolretry.UnsolicitedRetryWizard;
import com.lucy.g3.rtu.config.protocol.shared.ui.panels.HintPanel;
import com.lucy.g3.rtu.config.shared.base.misc.ComponentGroup;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.xml.gen.common.DNP3Enum.EVT_BUF_OVERFLOW_BEHAVIOR;
import com.lucy.g3.xml.gen.common.DNP3Enum.UNSOLICITED_RETRY_MODE;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP1;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP10;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP2;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP20;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP21;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP22;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP23;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP3;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP30;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP32;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP4;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP40;
import com.lucy.g3.xml.gen.common.DNP3Enum.VARIATION_GROUP42;

/**
 * The page of SDNP3 session.
 */
public class SDNP3SessionPage extends AbstractConfigPage {

  private final ArrayListModel<SDNP3IoMap> ioMapListModel;

  private final PresentationModel<SDNP3Session> sesnPM;
  private final PresentationModel<SDNPAuthConfig> sesnAuthPM;
  private final PresentationModel<GlobalVariationsConfig> objDefVarPM;
  private final PresentationModel<UnsolClassMaxDelays> unsolClassMaxDelayPM;
  private final PresentationModel<UnsolClassMaxEvents> unsolClassMaxEventPM;
  private final PresentationModel<SDNP3Session.Address> addressPM;
  private final PresentationModel<SDNP3Session.UnsolRetry> unsoRetryPM;
  private final SDNP3UsersManagerModel usersPM;

  public SDNP3SessionPage(SDNP3Session sdnp3Sesn) {
    super(sdnp3Sesn, "DNP3 Session");

    sesnPM = new PresentationModel<>(sdnp3Sesn);
    sesnAuthPM = new PresentationModel<>(sdnp3Sesn.getAuthConfig());
    objDefVarPM = new PresentationModel<>(sdnp3Sesn.getGlobalEvtVariation());
    unsolClassMaxDelayPM = new PresentationModel<>(sdnp3Sesn.getUnsolClassMaxDelays());
    unsolClassMaxEventPM = new PresentationModel<>(sdnp3Sesn.getUnsolClassMaxEvents());
    addressPM = new PresentationModel<>(sdnp3Sesn.getAddress());
    unsoRetryPM = new PresentationModel<>(sdnp3Sesn.getUnsolRetry());
    usersPM = new SDNP3UsersManagerModel(sdnp3Sesn.getAuthConfig().getUsersConfig());

    ioMapListModel = new ArrayListModel<SDNP3IoMap>(1);
    ioMapListModel.add(sdnp3Sesn.getIomap());

    // Bind node name to session name.
    ValueModel vm = sesnPM.getModel(SDNP3Session.PROPERTY_SESSION_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }

  private void initComponentsStateBindings() {
    ValueModel booleanVM;

    /* Bind multi fragment components enable state */
    booleanVM = sesnPM.getModel(SDNP3Session.PROPERTY_MULTIFRAGRESPALLOWED);
    PropertyConnector.connectAndUpdate(booleanVM, checkBoxFragConfirm, "enabled");
    PropertyConnector.connectAndUpdate(booleanVM, checkBoxRespNeedTime, "enabled");

    /* Hide Unsoli panels if checkBox is not selected */
    booleanVM = sesnPM.getModel(SDNP3Session.PROPERTY_UNSOLALLOWED);
    PropertyConnector.connectAndUpdate(booleanVM, panelUnsoli, "visible");

    /* Set master fields hidden if connection manager is enabled and network is TCP*/
    SDNP3ChannelConf channel = sesnPM.getBean().getProtocolChannel().getChannelConfig();
    booleanVM = new PropertyAdapter<SDNP3ChannelConf>(channel, SDNP3ChannelConf.PROPERTY_SESSION_MASTER_SUPPORTED, 
        true);
    PropertyConnector.connectAndUpdate(booleanVM, ftfMasterAddress, "visible");
    PropertyConnector.connectAndUpdate(booleanVM, cbValidSource, "visible");
    PropertyConnector.connectAndUpdate(booleanVM, labeldest, "visible");
    
    
    // Bind unsol retry components visibility
    {
      String property = SDNP3Session.UnsolRetry.PROPERTY_MODE;
      
      final UnsolRetryVisibilityUpdater updater = new UnsolRetryVisibilityUpdater(
          new ComponentGroup(lblMaxRetry, ftfMaximumRetryTimes),
          new ComponentGroup(labelDelay, ftfRetryDelay, labelms6),
          new ComponentGroup(lblOfflineRetryDelay, ftfOfflineRetryDelay, lblmsOfflineRetry));
          
      updater.update((UNSOLICITED_RETRY_MODE) unsoRetryPM.getValue(property));
      
      unsoRetryPM.addBeanPropertyChangeListener(property, 
          new PropertyChangeListener() {
  
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
              updater.update((UNSOLICITED_RETRY_MODE) evt.getNewValue());
            }
          });
    }
    
    
    /* Bind Authentication panel visibility*/
    booleanVM = sesnAuthPM.getModel(SDNPAuthConfig.PROPERTY_AUTHENTICATION_ENABLED);
    PropertyConnector.connectAndUpdate(booleanVM, panelAuthSettings, "visible");

    /* Bind button enable state to selection */
    PropertyAdapter<MultiSelectionInList<SDNP3User>> hasSelctionVM 
      = new PropertyAdapter<>(usersPM.getSelctionList(), MultiSelectionInList.PROPERTY_SELECTION, true);
    PropertyConnector.connectAndUpdate(hasSelctionVM, btnEditUser, "enabled");
    PropertyConnector.connectAndUpdate(hasSelctionVM, btnRemoveUser, "enabled");
    
    /* Bind the visibility of "Allow Control on Self Address"*/
    AbstractWrappedValueModel vm = sesnPM.getComponentModel(SDNP3Session.PROPERTY_ALLOW_CTRL_ON_SELFADDRESS);
    PropertyConnector.connectAndUpdate(sesnPM.getModel(SDNP3Session.PROPERTY_SELFADDRESS_ENABLED),
        vm, AbstractWrappedValueModel.PROPERTY_VISIBLE);
  }
  


  // @formatter:off
  private void createUIComponents() {
    panelHint = new HintPanel(SDNP3SessionPage.class);

    ComponentsFactory factory;

    // ====== Address ======
    factory = new ComponentsFactory(SDNP3Constraints.INSTANCE, SDNP3Constraints.PREFIX_SESSION_ADDRESS, addressPM);
    ftfSource = factory.createNumberField(Address.PROPERTY_SOURCE);
    ftfMasterAddress = factory.createNumberField(Address.PROPERTY_DEST);

    // ====== Unsolicited Retry ======
    factory = new ComponentsFactory(SDNP3Constraints.INSTANCE, SDNP3Constraints.PREFIX_SESSION,unsoRetryPM);
    ftfRetryDelay = factory.createNumberField(SDNP3Session.UnsolRetry.PROPERTY_UNSOLRETRYDELAY);
    ftfMaximumRetryTimes = factory.createNumberField(SDNP3Session.UnsolRetry.PROPERTY_UNSOLMAXRETRIES);
    ftfOfflineRetryDelay = factory.createNumberField(SDNP3Session.UnsolRetry.PROPERTY_UNSOLOFFLINERETRYDELAY);
    ftfUnsolConfirmTimeout = factory.createNumberField(SDNP3Session.UnsolRetry.PROPERTY_UNSOLCONFIRMTIMEOUT);
    AbstractValueModel vm = unsoRetryPM.getModel(SDNP3Session.UnsolRetry.PROPERTY_MODE);
    vm = new ConverterValueModel(vm, new ObjectToStringConverter());
    lblUnsolRetryMode = ComponentsFactory.createLabel(vm);
    
    // ====== Session ======
    factory = new ComponentsFactory(SDNP3Constraints.INSTANCE, SDNP3Constraints.PREFIX_SESSION, sesnPM);

    tfSessionName = factory.createTextField(SDNP3Session.PROPERTY_SESSION_NAME);
    ftfLinkStatusPeriod = factory.createNumberField(SDNP3Session.PROPERTY_LINKSTATUS_PERIOD);
    checkBoxResponseAllowed = factory.createCheckBox(SDNP3Session.PROPERTY_MULTIFRAGRESPALLOWED);
    checkBoxFragConfirm = factory.createCheckBox(SDNP3Session.PROPERTY_MULTIFRAGCONFIRM);
    checkBoxRespNeedTime = factory.createCheckBox(SDNP3Session.PROPERTY_RESPONDNEEDTIME);
    ftfAppLayerConfirmTimeout = factory.createNumberField(SDNP3Session.PROPERTY_APPLCONFIRMTIMEOUT);
    ftfSelTimeout = factory.createNumberField(SDNP3Session.PROPERTY_SELECTTIMEOUT);
    checkBoxUnsoliAllowed = factory.createCheckBox(SDNP3Session.PROPERTY_UNSOLALLOWED);
    checkBoxClass1Enabled = factory.createCheckBox(SDNP3Session.PROPERTY_CLASS1ENABLED);
    checkBoxClass2Enabled = factory.createCheckBox(SDNP3Session.PROPERTY_CLASS2ENABLED);
    checkBoxClass3Enabled = factory.createCheckBox(SDNP3Session.PROPERTY_CLASS3ENABLED);
    
    checkBoxIdenticalRetries = factory.createCheckBox(SDNP3Session.PROPERTY_IDENTICAL_RETRIES);

    cbSelfAdd = factory.createCheckBox(SDNP3Session.PROPERTY_SELFADDRESS_ENABLED);
    cbAllowControlOnSelfAddress = factory.createCheckBox(SDNP3Session.PROPERTY_ALLOW_CTRL_ON_SELFADDRESS);
    cbValidSource = factory.createCheckBox(SDNP3Session.PROPERTY_VALIDATE_SRC_ADDRESS);
    combOverflowBehav = factory.createComboBox(SDNP3Session.PROPERTY_EVT_BUF_OVERFLOW_BEHAVIOR,
        EVT_BUF_OVERFLOW_BEHAVIOR.values());

    // ====== Unsolicated Event Class ======
    factory = new ComponentsFactory(
        SDNP3Constraints.INSTANCE,
        SDNP3Constraints.PREFIX_SESSION_UNSOLICATED_MAX_EVENTS,
        unsolClassMaxEventPM);

    tfEventClass1 = factory.createNumberField(UnsolClassMaxEvents.PROPERTY_CLASS1);
    tfEventClass2 = factory.createNumberField(UnsolClassMaxEvents.PROPERTY_CLASS2);
    tfEventClass3 = factory.createNumberField(UnsolClassMaxEvents.PROPERTY_CLASS3);

    // ====== Unsolicated Events ======
    factory = new ComponentsFactory(
        SDNP3Constraints.INSTANCE,
        SDNP3Constraints.PREFIX_SESSION_UNSOLICATED_MAX_DELAYS,
        unsolClassMaxDelayPM);
    tfDelayClass1 = factory.createNumberField(UnsolClassMaxDelays.PROPERTY_CLASS1);
    tfDelayClass2 = factory.createNumberField(UnsolClassMaxDelays.PROPERTY_CLASS2);
    tfDelayClass3 = factory.createNumberField(UnsolClassMaxDelays.PROPERTY_CLASS3);

    // ====== Event Variation ======
    factory = new ComponentsFactory(
        SDNP3Constraints.INSTANCE,
        null,
        objDefVarPM);

    cbObj01 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ01, VARIATION_GROUP1.values());
    cbObj02 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ02, VARIATION_GROUP2.values());
    cbObj03 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ03, VARIATION_GROUP3.values());
    cbObj04 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ04, VARIATION_GROUP4.values());
    cbObj30 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ30, VARIATION_GROUP30.values());
    cbObj32 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ32, VARIATION_GROUP32.values());
    cbObj20 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ20, VARIATION_GROUP20.values());
    cbObj22 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ22, VARIATION_GROUP22.values());
    cbObj21 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ21, VARIATION_GROUP21.values());
    cbObj23 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ23, VARIATION_GROUP23.values());
    cbObj40 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ40, VARIATION_GROUP40.values());
    cbObj42 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ42, VARIATION_GROUP42.values());
    cbObj10 = factory.createComboBox(GlobalVariationsConfig.PROPERTY_OBJ10, VARIATION_GROUP10.values());

    VariationsRenderer render = new VariationsRenderer();
    cbObj01.setRenderer(render);
    cbObj02.setRenderer(render);
    cbObj03.setRenderer(render);
    cbObj04.setRenderer(render);
    cbObj30.setRenderer(render);
    cbObj32.setRenderer(render);
    cbObj20.setRenderer(render);
    cbObj22.setRenderer(render);
    cbObj21.setRenderer(render);
    cbObj23.setRenderer(render);
    cbObj40.setRenderer(render);
    cbObj42.setRenderer(render);
    cbObj10.setRenderer(render);

    // ====== EventsConfig ======
    SDNP3Session session = sesnPM.getBean();
    eventConfigPanelAI  = new EventConfigPanel(session.getEventConfigAI() ,EventConfig.AI);
    eventConfigPanelAO  = new EventConfigPanel(session.getEventConfigAO() ,EventConfig.AO);
    eventConfigPanelBI  = new EventConfigPanel(session.getEventConfigBI() ,EventConfig.BI);
    eventConfigPanelDI  = new EventConfigPanel(session.getEventConfigDI() ,EventConfig.DI);
    eventConfigPanelCT  = new EventConfigPanel(session.getEventConfigCT() ,EventConfig.CT);
    eventConfigPanelFCT = new EventConfigPanel(session.getEventConfigFCT(),EventConfig.FCT);
    eventConfigPanelAuth= new EventConfigPanel(session.getAuthConfig().getAuthSecStatEvtConfig() ,EventConfig.SA);
    
  }
  // @formatter:on

  private void populate() {
    sessionConfigPane.add(panelSessionGeneral);
    sessionConfigPane.add(panelSessionDefVar);
    sessionConfigPane.add(panelEvent);
    sessionConfigPane.add(panelSessionUnsolicated);
    sessionConfigPane.add(panelSessionTimeout);
    sessionConfigPane.add(panelSessionMultiFrag);
    sessionConfigPane.add(panelAuth);

    // Set default collapsed
    ((JXTaskPane) panelSessionDefVar).setCollapsed(true);
    ((JXTaskPane) panelSessionMultiFrag).setCollapsed(true);
    ((JXTaskPane) panelSessionTimeout).setCollapsed(true);
    ((JXTaskPane) panelEvent).setCollapsed(true);
    ((JXTaskPane) panelSessionUnsolicated).setCollapsed(true);
    ((JXTaskPane) panelAuth).setCollapsed(false);
  }

  private void btnModifyUnsoRetryActionPerformed(ActionEvent e) {
    UnsolicitedRetryWizard.showWizard(unsoRetryPM.getBean());
  }

  private void btnAuthAdvancedActionPerformed(ActionEvent e) {
    PropertyEditorDialog.createAndShowDialog(WindowUtils.getMainFrame(), 
        sesnPM.getBean().getAuthConfig(), "DNP3 Authentication Settings");
  }

  private void tableUsersMouseClicked(MouseEvent e) {
    if(e.getClickCount() > 1)
      usersPM.editUser();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
	createUIComponents();

	JScrollPane scrollPane2 = new JScrollPane();
	sessionConfigPane = new JXTaskPaneContainer();
	panelSessionGeneral = new JXTaskPane("General");
	JLabel labelChnName = new JLabel();
	JLabel labelsource = new JLabel();
	labeldest = new JLabel();
	panelSessionMultiFrag = new JXTaskPane("Multiple Fragment");
	panelMultFrag = new JPanel();
	panelSessionTimeout = new JXTaskPane("Timeout");
	JLabel labelAppConfirmTimeout = new JLabel();
	JLabel labelms4 = new JLabel();
	JLabel labelSelTimeout = new JLabel();
	JLabel labelms3 = new JLabel();
	JLabel labelLinkStatusPeriod = new JLabel();
	JLabel labelms = new JLabel();
	panelSessionDefVar = new JXTaskPane("Default Variation");
	JPanel panel4 = new JPanel();
	JLabel label16 = new JLabel();
	JLabel label10 = new JLabel();
	JLabel label15 = new JLabel();
	JLabel label7 = new JLabel();
	JLabel label13 = new JLabel();
	JLabel label8 = new JLabel();
	JLabel label11 = new JLabel();
	JLabel label26 = new JLabel();
	JLabel label14 = new JLabel();
	JLabel label9 = new JLabel();
	JLabel label12 = new JLabel();
	JLabel label1 = new JLabel();
	JLabel label32 = new JLabel();
	JLabel label6 = new JLabel();
	JLabel label27 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label33 = new JLabel();
	JLabel label4 = new JLabel();
	JLabel label28 = new JLabel();
	JLabel label3 = new JLabel();
	JLabel label34 = new JLabel();
	JLabel label5 = new JLabel();
	JLabel label41 = new JLabel();
	JLabel label42 = new JLabel();
	JLabel label43 = new JLabel();
	panelSessionUnsolicated = new JXTaskPane("Unsolicited Class");
	panelUnsoli = new JPanel();
	JLabel label19 = new JLabel();
	JPanel panel1 = new JPanel();
	JLabel label20 = new JLabel();
	JLabel labelDelays = new JLabel();
	JLabel labelClass1 = new JLabel();
	JLabel labelClass4 = new JLabel();
	label31 = new JLabel();
	JLabel labelClass2 = new JLabel();
	JLabel labelClass5 = new JLabel();
	label37 = new JLabel();
	JLabel labelClass3 = new JLabel();
	JLabel labelClass6 = new JLabel();
	label38 = new JLabel();
	lblConfirmTimeout = new JLabel();
	lblms1 = new JLabel();
	panel6 = new JPanel();
	panel5 = new JPanel();
	lblRetryMode = new JLabel();
	lblMaxRetry = new JLabel();
	labelDelay = new JLabel();
	labelms6 = new JLabel();
	lblOfflineRetryDelay = new JLabel();
	lblmsOfflineRetry = new JLabel();
	btnModifyUnsoRetry = new JButton();
	panelEvent = new JXTaskPane("Events");
	JLabel label17 = new JLabel();
	JLabel label18 = new JLabel();
	JLabel label21 = new JLabel();
	JPanel panel3 = new JPanel();
	JLabel label22 = new JLabel();
	JLabel label25 = new JLabel();
	JLabel label35 = new JLabel();
	JLabel label23 = new JLabel();
	JLabel label24 = new JLabel();
	JPanel panel2 = new JPanel();
	JLabel label29 = new JLabel();
	JLabel label30 = new JLabel();
	label36 = new JLabel();
	panelAuth = new JXTaskPane("Authentication");
	checkBoxAuthEnabled = new JCheckBox();
	panelAuthSettings = new JPanel();
	panel8 = new JPanel();
	panel7 = new JPanel();
	JLabel label39 = new JLabel();
	JLabel label40 = new JLabel();
	panel9 = new JPanel();
	scrollPane1 = new JScrollPane();
	tableUsers = new JXTable();
	btnAddUser = new JButton();
	btnEditUser = new JButton();
	btnRemoveUser = new JButton();
	btnAuthAdvanced = new JButton();

	//======== this ========
	setLayout(new BorderLayout());

	//======== scrollPane2 ========
	{
		scrollPane2.setBorder(null);
		scrollPane2.setViewportView(sessionConfigPane);
	}
	add(scrollPane2, BorderLayout.CENTER);
	add(panelHint, BorderLayout.SOUTH);

	//======== panelSessionGeneral ========
	{
		panelSessionGeneral.setLayout(new FormLayout(
			"right:default, $lcgap, [50dlu,default], $ugap, left:default, $rgap, default",
			"2*(default, $lgap), default"));

		//---- labelChnName ----
		labelChnName.setText("Session Name:");
		panelSessionGeneral.add(labelChnName, CC.xy(1, 1));

		//---- tfSessionName ----
		tfSessionName.setColumns(8);
		panelSessionGeneral.add(tfSessionName, CC.xy(3, 1));

		//---- labelsource ----
		labelsource.setText("RTU Address:");
		panelSessionGeneral.add(labelsource, CC.xy(1, 3));
		panelSessionGeneral.add(ftfSource, CC.xy(3, 3));

		//---- cbSelfAdd ----
		cbSelfAdd.setText("Self Address Enable (except controls)");
		cbSelfAdd.setToolTipText("The Slave will respond to address 0xFFFC as a request for its configured address. It allows the Master to automatically discover the Slave Address.");
		cbSelfAdd.setOpaque(false);
		panelSessionGeneral.add(cbSelfAdd, CC.xy(5, 3));

		//---- cbAllowControlOnSelfAddress ----
		cbAllowControlOnSelfAddress.setText("Allow control on self address");
		cbAllowControlOnSelfAddress.setOpaque(false);
		panelSessionGeneral.add(cbAllowControlOnSelfAddress, CC.xy(7, 3));

		//---- labeldest ----
		labeldest.setText("Master Address:");
		panelSessionGeneral.add(labeldest, CC.xy(1, 5));
		panelSessionGeneral.add(ftfMasterAddress, CC.xy(3, 5));

		//---- cbValidSource ----
		cbValidSource.setText("Validate");
		cbValidSource.setToolTipText("Validate Source Address");
		cbValidSource.setOpaque(false);
		panelSessionGeneral.add(cbValidSource, CC.xy(5, 5));
	}

	//======== panelSessionMultiFrag ========
	{
		panelSessionMultiFrag.setLayout(new FormLayout(
			"default, right:default",
			"default, $lgap, default"));

		//---- checkBoxResponseAllowed ----
		checkBoxResponseAllowed.setText("Multi Fragment Response Allowed");
		panelSessionMultiFrag.add(checkBoxResponseAllowed, CC.xywh(1, 1, 2, 1));

		//======== panelMultFrag ========
		{
			panelMultFrag.setOpaque(false);
			panelMultFrag.setLayout(new FormLayout(
				"default, $lcgap, default",
				"default"));

			//---- checkBoxFragConfirm ----
			checkBoxFragConfirm.setText("Multi Fragment Confirm");
			panelMultFrag.add(checkBoxFragConfirm, CC.xy(1, 1));

			//---- checkBoxRespNeedTime ----
			checkBoxRespNeedTime.setText("Response Need Time");
			panelMultFrag.add(checkBoxRespNeedTime, CC.xy(3, 1));
		}
		panelSessionMultiFrag.add(panelMultFrag, CC.xy(2, 3));
	}

	//======== panelSessionTimeout ========
	{
		panelSessionTimeout.setLayout(new FormLayout(
			"right:default, $lcgap, [50dlu,default], left:3dlu, left:default, 10dlu, default, $lcgap, [50dlu,default], $lcgap, default",
			"default, $ugap, default"));

		//---- labelAppConfirmTimeout ----
		labelAppConfirmTimeout.setText("Application Layer Confirm Timeout:");
		panelSessionTimeout.add(labelAppConfirmTimeout, CC.xy(1, 1));
		panelSessionTimeout.add(ftfAppLayerConfirmTimeout, CC.xy(3, 1));

		//---- labelms4 ----
		labelms4.setText("ms");
		panelSessionTimeout.add(labelms4, CC.xy(5, 1));

		//---- labelSelTimeout ----
		labelSelTimeout.setText("Select Timeout:");
		panelSessionTimeout.add(labelSelTimeout, CC.xy(7, 1));
		panelSessionTimeout.add(ftfSelTimeout, CC.xy(9, 1));

		//---- labelms3 ----
		labelms3.setText("ms");
		panelSessionTimeout.add(labelms3, CC.xy(11, 1));

		//---- labelLinkStatusPeriod ----
		labelLinkStatusPeriod.setText("Link Status Period:");
		panelSessionTimeout.add(labelLinkStatusPeriod, CC.xy(1, 3));
		panelSessionTimeout.add(ftfLinkStatusPeriod, CC.xy(3, 3));

		//---- labelms ----
		labelms.setText("ms");
		panelSessionTimeout.add(labelms, CC.xy(5, 3));
	}

	//======== panelSessionDefVar ========
	{
		panelSessionDefVar.setLayout(new BorderLayout());

		//======== panel4 ========
		{
			panel4.setOpaque(false);
			panel4.setLayout(new FormLayout(
				"default, $ugap, default, $lcgap, [50dlu,default,200dlu], 10dlu, $lcgap, right:default, $lcgap, [50dlu,default,200dlu], 10dlu, default, $lcgap, [50dlu,default,200dlu], $lcgap, default",
				"2*(default, $lgap), default, $pgap, 2*(default, $lgap), default, 9dlu, default, $lgap, default"));
			((FormLayout)panel4.getLayout()).setColumnGroups(new int[][] {{5, 10, 14}});

			//---- label16 ----
			label16.setText("- Analogue Input-");
			label16.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label16, CC.xywh(3, 1, 3, 1));

			//---- label10 ----
			label10.setText("- Binary Input -");
			label10.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label10, CC.xywh(8, 1, 3, 1));

			//---- label15 ----
			label15.setText("- Double Binary Input-");
			label15.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label15, CC.xywh(12, 1, 3, 1));

			//---- label7 ----
			label7.setText("Static: ");
			panel4.add(label7, CC.xy(1, 3));

			//---- label13 ----
			label13.setText("Obj30:");
			panel4.add(label13, CC.xy(3, 3));
			panel4.add(cbObj30, CC.xy(5, 3));

			//---- label8 ----
			label8.setText("Obj01:");
			panel4.add(label8, CC.xy(8, 3));
			panel4.add(cbObj01, CC.xy(10, 3));

			//---- label11 ----
			label11.setText("Obj03:");
			panel4.add(label11, CC.xy(12, 3));
			panel4.add(cbObj03, CC.xy(14, 3));

			//---- label26 ----
			label26.setText("Event: ");
			panel4.add(label26, CC.xy(1, 5));

			//---- label14 ----
			label14.setText("Obj32:");
			panel4.add(label14, CC.xy(3, 5));
			panel4.add(cbObj32, CC.xy(5, 5));

			//---- label9 ----
			label9.setText("Obj02:");
			panel4.add(label9, CC.xy(8, 5));
			panel4.add(cbObj02, CC.xy(10, 5));

			//---- label12 ----
			label12.setText("Obj04:");
			panel4.add(label12, CC.xy(12, 5));
			panel4.add(cbObj04, CC.xy(14, 5));

			//---- label1 ----
			label1.setText("- Analogue Output -");
			label1.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label1, CC.xywh(3, 7, 3, 1));

			//---- label32 ----
			label32.setText("- Counter -");
			label32.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label32, CC.xywh(8, 7, 3, 1));

			//---- label6 ----
			label6.setText("- Frozen Counter -");
			label6.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label6, CC.xywh(12, 7, 3, 1));

			//---- label27 ----
			label27.setText("Static: ");
			panel4.add(label27, CC.xy(1, 9));

			//---- label2 ----
			label2.setText("Obj40:");
			panel4.add(label2, CC.xy(3, 9));
			panel4.add(cbObj40, CC.xy(5, 9));

			//---- label33 ----
			label33.setText("Obj20:");
			panel4.add(label33, CC.xy(8, 9));
			panel4.add(cbObj20, CC.xy(10, 9));

			//---- label4 ----
			label4.setText("Obj21:");
			panel4.add(label4, CC.xy(12, 9));
			panel4.add(cbObj21, CC.xy(14, 9));

			//---- label28 ----
			label28.setText("Event: ");
			panel4.add(label28, CC.xy(1, 11));

			//---- label3 ----
			label3.setText("Obj42:");
			panel4.add(label3, CC.xy(3, 11));
			panel4.add(cbObj42, CC.xy(5, 11));

			//---- label34 ----
			label34.setText("Obj22:");
			panel4.add(label34, CC.xy(8, 11));
			panel4.add(cbObj22, CC.xy(10, 11));

			//---- label5 ----
			label5.setText("Obj23:");
			panel4.add(label5, CC.xy(12, 11));
			panel4.add(cbObj23, CC.xy(14, 11));

			//---- label41 ----
			label41.setText("- Binary Output -");
			label41.setHorizontalAlignment(SwingConstants.CENTER);
			panel4.add(label41, CC.xywh(3, 13, 3, 1));

			//---- label42 ----
			label42.setText("Obj10:");
			panel4.add(label42, CC.xy(3, 15));

			//---- label43 ----
			label43.setText("Static: ");
			panel4.add(label43, CC.xy(1, 15));
			panel4.add(cbObj10, CC.xy(5, 15));
		}
		panelSessionDefVar.add(panel4, BorderLayout.CENTER);
	}

	//======== panelSessionUnsolicated ========
	{
		panelSessionUnsolicated.setLayout(new FormLayout(
			"left:default",
			"default, $lgap, top:default"));

		//---- checkBoxUnsoliAllowed ----
		checkBoxUnsoliAllowed.setText("Unsolicited Allowed");
		checkBoxUnsoliAllowed.setOpaque(false);
		panelSessionUnsolicated.add(checkBoxUnsoliAllowed, CC.xy(1, 1));

		//======== panelUnsoli ========
		{
			panelUnsoli.setOpaque(false);
			panelUnsoli.setLayout(new FormLayout(
				"default, $lcgap, [50dlu,default], $lcgap, 10dlu, default, $lcgap, [50dlu,default], $lcgap, default:grow",
				"default, $ugap, 4*(default, $lgap), default, $ugap, default, $pgap, default"));

			//---- label19 ----
			label19.setText("Unsolicited Class Mask:");
			label19.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(label19, CC.xy(1, 1));

			//======== panel1 ========
			{
				panel1.setOpaque(false);
				panel1.setLayout(new FormLayout(
					"[50dlu,default], 2*($lcgap, default)",
					"default"));
				((FormLayout)panel1.getLayout()).setColumnGroups(new int[][] {{1, 3, 5}});

				//---- checkBoxClass1Enabled ----
				checkBoxClass1Enabled.setText("Class 1");
				checkBoxClass1Enabled.setOpaque(false);
				panel1.add(checkBoxClass1Enabled, CC.xy(1, 1));

				//---- checkBoxClass2Enabled ----
				checkBoxClass2Enabled.setText("Class 2");
				checkBoxClass2Enabled.setOpaque(false);
				panel1.add(checkBoxClass2Enabled, CC.xy(3, 1));

				//---- checkBoxClass3Enabled ----
				checkBoxClass3Enabled.setText("Class 3");
				checkBoxClass3Enabled.setOpaque(false);
				panel1.add(checkBoxClass3Enabled, CC.xy(5, 1));
			}
			panelUnsoli.add(panel1, CC.xywh(3, 1, 8, 1));

			//---- label20 ----
			label20.setText("- Maximum Events -");
			label20.setHorizontalAlignment(SwingConstants.CENTER);
			panelUnsoli.add(label20, CC.xywh(1, 3, 3, 1));

			//---- labelDelays ----
			labelDelays.setText("- Maximum Delays -");
			labelDelays.setHorizontalAlignment(SwingConstants.CENTER);
			panelUnsoli.add(labelDelays, CC.xywh(6, 3, 3, 1));

			//---- labelClass1 ----
			labelClass1.setText("Class 1:");
			labelClass1.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(labelClass1, CC.xy(1, 5));
			panelUnsoli.add(tfEventClass1, CC.xy(3, 5));

			//---- labelClass4 ----
			labelClass4.setText("Class 1:");
			labelClass4.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(labelClass4, CC.xy(6, 5));
			panelUnsoli.add(tfDelayClass1, CC.xy(8, 5));

			//---- label31 ----
			label31.setText("ms");
			panelUnsoli.add(label31, CC.xy(10, 5));

			//---- labelClass2 ----
			labelClass2.setText("Class 2:");
			labelClass2.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(labelClass2, CC.xy(1, 7));
			panelUnsoli.add(tfEventClass2, CC.xy(3, 7));

			//---- labelClass5 ----
			labelClass5.setText("Class 2:");
			labelClass5.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(labelClass5, CC.xy(6, 7));
			panelUnsoli.add(tfDelayClass2, CC.xy(8, 7));

			//---- label37 ----
			label37.setText("ms");
			panelUnsoli.add(label37, CC.xy(10, 7));

			//---- labelClass3 ----
			labelClass3.setText("Class 3:");
			labelClass3.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(labelClass3, CC.xy(1, 9));
			panelUnsoli.add(tfEventClass3, CC.xy(3, 9));

			//---- labelClass6 ----
			labelClass6.setText("Class 3:");
			labelClass6.setHorizontalAlignment(SwingConstants.RIGHT);
			panelUnsoli.add(labelClass6, CC.xy(6, 9));
			panelUnsoli.add(tfDelayClass3, CC.xy(8, 9));

			//---- label38 ----
			label38.setText("ms");
			panelUnsoli.add(label38, CC.xy(10, 9));

			//---- checkBoxIdenticalRetries ----
			checkBoxIdenticalRetries.setText("Identical Retries");
			checkBoxIdenticalRetries.setToolTipText("Identical Retries");
			checkBoxIdenticalRetries.setOpaque(false);
			checkBoxIdenticalRetries.setActionCommand("IdenticalRetries");
			panelUnsoli.add(checkBoxIdenticalRetries, CC.xy(1, 11));

			//---- lblConfirmTimeout ----
			lblConfirmTimeout.setText("Unsolicited Confirm Timeout:");
			panelUnsoli.add(lblConfirmTimeout, CC.xy(1, 13));

			//---- ftfUnsolConfirmTimeout ----
			ftfUnsolConfirmTimeout.setColumns(8);
			panelUnsoli.add(ftfUnsolConfirmTimeout, CC.xy(3, 13));

			//---- lblms1 ----
			lblms1.setText("ms");
			panelUnsoli.add(lblms1, CC.xy(5, 13));

			//======== panel6 ========
			{
				panel6.setBorder(new TitledBorder("Unsolicited Retries"));
				panel6.setOpaque(false);
				panel6.setLayout(new FormLayout(
					"default:grow, $ugap, default",
					"top:default"));

				//======== panel5 ========
				{
					panel5.setOpaque(false);
					panel5.setLayout(new FormLayout(
						"default, $lcgap, [50dlu,default], $rgap, default, $lcgap, 20dlu",
						"3*(default, $lgap), default"));

					//---- lblRetryMode ----
					lblRetryMode.setText("Retry Mode:");
					lblRetryMode.setHorizontalAlignment(SwingConstants.RIGHT);
					panel5.add(lblRetryMode, CC.xy(1, 1));

					//---- lblUnsolRetryMode ----
					lblUnsolRetryMode.setText("text");
					panel5.add(lblUnsolRetryMode, CC.xywh(3, 1, 5, 1));

					//---- lblMaxRetry ----
					lblMaxRetry.setText("Maximum Retry Times:");
					lblMaxRetry.setHorizontalAlignment(SwingConstants.RIGHT);
					panel5.add(lblMaxRetry, CC.xy(1, 3));

					//---- ftfMaximumRetryTimes ----
					ftfMaximumRetryTimes.setColumns(8);
					ftfMaximumRetryTimes.setEditable(false);
					panel5.add(ftfMaximumRetryTimes, CC.xy(3, 3));

					//---- labelDelay ----
					labelDelay.setText("Retry Delay:");
					labelDelay.setHorizontalAlignment(SwingConstants.RIGHT);
					panel5.add(labelDelay, CC.xy(1, 5));

					//---- ftfRetryDelay ----
					ftfRetryDelay.setColumns(8);
					ftfRetryDelay.setEditable(false);
					panel5.add(ftfRetryDelay, CC.xy(3, 5));

					//---- labelms6 ----
					labelms6.setText("ms");
					panel5.add(labelms6, CC.xy(5, 5));

					//---- lblOfflineRetryDelay ----
					lblOfflineRetryDelay.setText("Offline Retry Delay:");
					lblOfflineRetryDelay.setHorizontalAlignment(SwingConstants.RIGHT);
					panel5.add(lblOfflineRetryDelay, CC.xy(1, 7));

					//---- ftfOfflineRetryDelay ----
					ftfOfflineRetryDelay.setColumns(8);
					ftfOfflineRetryDelay.setEditable(false);
					panel5.add(ftfOfflineRetryDelay, CC.xy(3, 7));

					//---- lblmsOfflineRetry ----
					lblmsOfflineRetry.setText("ms");
					panel5.add(lblmsOfflineRetry, CC.xy(5, 7));
				}
				panel6.add(panel5, CC.xy(1, 1));

				//---- btnModifyUnsoRetry ----
				btnModifyUnsoRetry.setText("Modify...");
				btnModifyUnsoRetry.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						btnModifyUnsoRetryActionPerformed(e);
					}
				});
				panel6.add(btnModifyUnsoRetry, CC.xy(3, 1));
			}
			panelUnsoli.add(panel6, CC.xywh(1, 15, 10, 1));
		}
		panelSessionUnsolicated.add(panelUnsoli, CC.xy(1, 3));
	}

	//======== panelEvent ========
	{
		panelEvent.setLayout(new FormLayout(
			"default, $lcgap, 2*([70dlu,default], $ugap), [70dlu,default], $lcgap, default",
			"default, $lgap, default, $pgap, default, $lgap, default, $pgap, default"));

		//---- label17 ----
		label17.setText("- Analogue Input -");
		label17.setHorizontalAlignment(SwingConstants.CENTER);
		panelEvent.add(label17, CC.xy(3, 1));

		//---- label18 ----
		label18.setText("- Binary Input -");
		label18.setHorizontalAlignment(SwingConstants.CENTER);
		panelEvent.add(label18, CC.xy(5, 1));

		//---- label21 ----
		label21.setText("- Double Binary Input -");
		label21.setHorizontalAlignment(SwingConstants.CENTER);
		panelEvent.add(label21, CC.xy(7, 1));

		//======== panel3 ========
		{
			panel3.setOpaque(false);
			panel3.setLayout(new FormLayout(
				"default",
				"default, $lgap, default"));

			//---- label22 ----
			label22.setText("Max Events:");
			panel3.add(label22, CC.xy(1, 1));

			//---- label25 ----
			label25.setText("Event Mode:");
			panel3.add(label25, CC.xy(1, 3));
		}
		panelEvent.add(panel3, CC.xy(1, 3));
		panelEvent.add(eventConfigPanelAI, CC.xy(3, 3));
		panelEvent.add(eventConfigPanelBI, CC.xy(5, 3));
		panelEvent.add(eventConfigPanelDI, CC.xy(7, 3));

		//---- label35 ----
		label35.setText("- Analogue Output -");
		label35.setHorizontalAlignment(SwingConstants.CENTER);
		panelEvent.add(label35, CC.xy(3, 5));

		//---- label23 ----
		label23.setText("- Counter -");
		label23.setHorizontalAlignment(SwingConstants.CENTER);
		panelEvent.add(label23, CC.xy(5, 5));

		//---- label24 ----
		label24.setText("- Frozen Counter -");
		label24.setHorizontalAlignment(SwingConstants.CENTER);
		panelEvent.add(label24, CC.xy(7, 5));

		//======== panel2 ========
		{
			panel2.setOpaque(false);
			panel2.setLayout(new FormLayout(
				"default",
				"fill:default, $lgap, fill:default"));

			//---- label29 ----
			label29.setText("Max Events:");
			panel2.add(label29, CC.xy(1, 1));

			//---- label30 ----
			label30.setText("Event Mode:");
			panel2.add(label30, CC.xy(1, 3));
		}
		panelEvent.add(panel2, CC.xy(1, 7));
		panelEvent.add(eventConfigPanelAO, CC.xy(3, 7));
		panelEvent.add(eventConfigPanelCT, CC.xy(5, 7));
		panelEvent.add(eventConfigPanelFCT, CC.xy(7, 7));

		//---- label36 ----
		label36.setText("Event Buffer Overflow Behaviour:");
		panelEvent.add(label36, CC.xywh(1, 9, 3, 1));
		panelEvent.add(combOverflowBehav, CC.xywh(5, 9, 5, 1));
	}

	//======== panelAuth ========
	{
		panelAuth.setLayout(new FormLayout(
			"default, $lcgap, default:grow",
			"default, $lgap, default"));

		//---- checkBoxAuthEnabled ----
		checkBoxAuthEnabled.setText("Authentication Enabled");
		checkBoxAuthEnabled.setOpaque(false);
		panelAuth.add(checkBoxAuthEnabled, CC.xy(1, 1));

		//======== panelAuthSettings ========
		{
			panelAuthSettings.setOpaque(false);
			panelAuthSettings.setLayout(new BorderLayout());

			//======== panel8 ========
			{
				panel8.setBorder(new TitledBorder("Authentication Security Statistics Event Settings"));
				panel8.setOpaque(false);
				panel8.setLayout(new FormLayout(
					"3*(default, $lcgap), default:grow",
					"default"));
				panel8.add(eventConfigPanelAuth, CC.xy(5, 1));

				//======== panel7 ========
				{
					panel7.setOpaque(false);
					panel7.setLayout(new FormLayout(
						"default",
						"default, $lgap, default"));

					//---- label39 ----
					label39.setText("Max Events:");
					panel7.add(label39, CC.xy(1, 1));

					//---- label40 ----
					label40.setText("Event Mode:");
					panel7.add(label40, CC.xy(1, 3));
				}
				panel8.add(panel7, CC.xy(3, 1));
			}
			panelAuthSettings.add(panel8, BorderLayout.SOUTH);

			//======== panel9 ========
			{
				panel9.setOpaque(false);
				panel9.setLayout(new FormLayout(
					"400dlu, $lcgap, default",
					"3*(default, $lgap), default, $pgap, 50dlu, $lgap, fill:default, $lgap, $ugap"));

				//======== scrollPane1 ========
				{
					scrollPane1.setOpaque(false);

					//---- tableUsers ----
					tableUsers.setFillsViewportHeight(true);
					tableUsers.setColumnControlVisible(true);
					tableUsers.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent e) {
							tableUsersMouseClicked(e);
						}
					});
					scrollPane1.setViewportView(tableUsers);
				}
				panel9.add(scrollPane1, CC.xywh(1, 1, 1, 11));

				//---- btnAddUser ----
				btnAddUser.setText("Add User");
				panel9.add(btnAddUser, CC.xy(3, 1));

				//---- btnEditUser ----
				btnEditUser.setText("Edit User");
				panel9.add(btnEditUser, CC.xy(3, 3));

				//---- btnRemoveUser ----
				btnRemoveUser.setText("Remove User");
				panel9.add(btnRemoveUser, CC.xy(3, 5));

				//---- btnAuthAdvanced ----
				btnAuthAdvanced.setText("Advanced Settings...");
				btnAuthAdvanced.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						btnAuthAdvancedActionPerformed(e);
					}
				});
				panel9.add(btnAuthAdvanced, CC.xy(3, 11));
			}
			panelAuthSettings.add(panel9, BorderLayout.CENTER);
		}
		panelAuth.add(panelAuthSettings, CC.xywh(1, 3, 3, 1));
	}
    // //GEN-END:initComponents

    UIUtils.increaseScrollSpeed(scrollPane2);
  }

  @Override
  protected void init() throws Exception {
    initComponents();
    initComponentsBindings();
    initComponentsStateBindings();
    populate();
  }

  private void initComponentsBindings() {
    Bindings.bind(checkBoxAuthEnabled, sesnAuthPM.getModel(SDNPAuthConfig.PROPERTY_AUTHENTICATION_ENABLED));
    
   
    tableUsers.setModel(usersPM.getUsersTableModel());
    Bindings.bind(tableUsers, usersPM.getUsersListModel(), usersPM.getSelectionModel());
    
    /* Bind actions*/
    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(usersPM);
    btnAddUser.setAction(actions.get(SDNP3UsersManagerModel.ACTION_ADD));
    btnEditUser.setAction(actions.get(SDNP3UsersManagerModel.ACTION_EDIT));
    btnRemoveUser.setAction(actions.get(SDNP3UsersManagerModel.ACTION_REMOVE));
  }

  @SuppressWarnings("unchecked")
  @Override
  public ListModel<SDNP3IoMap> getChildrenDataList() {
    return ioMapListModel;
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JXTaskPaneContainer sessionConfigPane;
  private JPanel panelHint;
  private JPanel panelSessionGeneral;
  private JTextField tfSessionName;
  private JFormattedTextField ftfSource;
  private JCheckBox cbSelfAdd;
  private JCheckBox cbAllowControlOnSelfAddress;
  private JLabel labeldest;
  private JFormattedTextField ftfMasterAddress;
  private JCheckBox cbValidSource;
  private JPanel panelSessionMultiFrag;
  private JCheckBox checkBoxResponseAllowed;
  private JPanel panelMultFrag;
  private JCheckBox checkBoxFragConfirm;
  private JCheckBox checkBoxRespNeedTime;
  private JPanel panelSessionTimeout;
  private JFormattedTextField ftfAppLayerConfirmTimeout;
  private JFormattedTextField ftfSelTimeout;
  private JFormattedTextField ftfLinkStatusPeriod;
  private JPanel panelSessionDefVar;
  private JComboBox<Object> cbObj30;
  private JComboBox<Object> cbObj01;
  private JComboBox<Object> cbObj03;
  private JComboBox<Object> cbObj32;
  private JComboBox<Object> cbObj02;
  private JComboBox<Object> cbObj04;
  private JComboBox<Object> cbObj40;
  private JComboBox<Object> cbObj20;
  private JComboBox<Object> cbObj21;
  private JComboBox<Object> cbObj42;
  private JComboBox<Object> cbObj22;
  private JComboBox<Object> cbObj23;
  private JComboBox<Object> cbObj10;
  private JPanel panelSessionUnsolicated;
  private JCheckBox checkBoxUnsoliAllowed;
  private JPanel panelUnsoli;
  private JCheckBox checkBoxClass1Enabled;
  private JCheckBox checkBoxClass2Enabled;
  private JCheckBox checkBoxClass3Enabled;
  private JFormattedTextField tfEventClass1;
  private JFormattedTextField tfDelayClass1;
  private JLabel label31;
  private JFormattedTextField tfEventClass2;
  private JFormattedTextField tfDelayClass2;
  private JLabel label37;
  private JFormattedTextField tfEventClass3;
  private JFormattedTextField tfDelayClass3;
  private JLabel label38;
  private JCheckBox checkBoxIdenticalRetries;
  private JLabel lblConfirmTimeout;
  private JFormattedTextField ftfUnsolConfirmTimeout;
  private JLabel lblms1;
  private JPanel panel6;
  private JPanel panel5;
  private JLabel lblRetryMode;
  private JLabel lblUnsolRetryMode;
  private JLabel lblMaxRetry;
  private JFormattedTextField ftfMaximumRetryTimes;
  private JLabel labelDelay;
  private JFormattedTextField ftfRetryDelay;
  private JLabel labelms6;
  private JLabel lblOfflineRetryDelay;
  private JFormattedTextField ftfOfflineRetryDelay;
  private JLabel lblmsOfflineRetry;
  private JButton btnModifyUnsoRetry;
  private JPanel panelEvent;
  private EventConfigPanel eventConfigPanelAI;
  private EventConfigPanel eventConfigPanelBI;
  private EventConfigPanel eventConfigPanelDI;
  private EventConfigPanel eventConfigPanelAO;
  private EventConfigPanel eventConfigPanelCT;
  private EventConfigPanel eventConfigPanelFCT;
  private JLabel label36;
  private JComboBox combOverflowBehav;
  private JPanel panelAuth;
  private JCheckBox checkBoxAuthEnabled;
  private JPanel panelAuthSettings;
  private JPanel panel8;
  private EventConfigPanel eventConfigPanelAuth;
  private JPanel panel7;
  private JPanel panel9;
  private JScrollPane scrollPane1;
  private JXTable tableUsers;
  private JButton btnAddUser;
  private JButton btnEditUser;
  private JButton btnRemoveUser;
  private JButton btnAuthAdvanced;
  // JFormDesigner - End of variables declaration //GEN-END:variables


}
