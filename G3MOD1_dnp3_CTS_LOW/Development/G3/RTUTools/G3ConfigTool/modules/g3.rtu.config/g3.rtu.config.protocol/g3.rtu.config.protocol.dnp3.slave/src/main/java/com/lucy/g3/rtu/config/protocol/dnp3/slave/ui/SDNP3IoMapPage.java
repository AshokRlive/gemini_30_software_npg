/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.AbstractIoMapPageModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.AbstractScadaIoMapPage;

/**
 * <p>
 * The panel that consists of a points table and control panel which manages
 * point list. The content of table depends on the given
 * {@code PointManageModel}.
 * </p>
 */
public class SDNP3IoMapPage extends AbstractScadaIoMapPage<SDNP3Point> {

  private ImageIcon icon;

  public SDNP3IoMapPage(IScadaIoMap<SDNP3Point> iomap, ScadaPointSourceRepository resource) {
    super("DNP 3.0 Protocol Points", iomap, resource, SDNP3PointType.values());
    this.icon = ResourceUtils.getImg("title.icon", SDNP3IoMapPage.class);
  }


  @Override
  public Icon getTitleIcon() {
    return icon;
  }


  @Override
  protected AbstractIoMapPageModel<SDNP3Point>[] createModels(IScadaIoMap<SDNP3Point> iomap,
      ScadaPointType[] pointTypes, ScadaPointSourceRepository resource) {
    SDNP3IoMapPageModel[] model = new SDNP3IoMapPageModel[pointTypes.length];
    for (int i = 0; i < model.length; i++) {
      model[i] = new SDNP3IoMapPageModel(iomap, (SDNP3PointType)pointTypes[i], resource);
    }
    return model;
  }
}
