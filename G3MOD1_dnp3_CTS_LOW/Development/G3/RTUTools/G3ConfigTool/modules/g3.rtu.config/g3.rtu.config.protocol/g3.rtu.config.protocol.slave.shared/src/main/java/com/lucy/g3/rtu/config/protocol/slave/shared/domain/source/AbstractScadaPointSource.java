/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ListModel;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.ObservableBean2;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidResourceException;
import com.lucy.g3.rtu.config.shared.model.INode;

/**
 * The basic implementation of {@linkplain ScadaPointSource}.
 *
 * @param <SourceT>
 *          the type of raw source.
 */
abstract class AbstractScadaPointSource<SourceT extends ObservableBean2> extends Model implements ScadaPointSource,
    PropertyChangeListener {

  private final ArrayListModel<ScadaPoint> registeredPoints = new ArrayListModel<ScadaPoint>(MAX_SCADA_POINT_AMOUNT);

  protected final SourceT rawSource;

  private String name;


  AbstractScadaPointSource(SourceT rawSource) {
    this.rawSource = Preconditions.checkNotNull(rawSource, "rawSource must not be null");
    updateName();
    this.rawSource.addPropertyChangeListener(INode.PROPERTY_NAME, this);
  }

  @Override
  public SourceT getRawSource() {
    return rawSource;
  }

  protected void delete() {
    /* Remove all registered points */
    ScadaPoint[] allMappedPoints = registeredPoints.toArray(new ScadaPoint[registeredPoints.size()]);
    for (ScadaPoint p : allMappedPoints) {
      p.setSource(null);
    }

    registeredPoints.clear();
  }

  @Override
  public final void registerPoint(ScadaPoint p)
      throws InvalidResourceException {
    if (p != null) {
      if (registeredPoints.size() >= MAX_SCADA_POINT_AMOUNT) {
        throw new InvalidResourceException(
            String.format("\"%s\" is already mapped to \"%s\"",
                getName(), getRegisteredPoints()));
      }

      if (!registeredPoints.contains(p)) {
        registeredPoints.add(p);
      }
    }
  }

  @Override
  public final void deregisterPoint(ScadaPoint p) {
    if (p != null && registeredPoints.contains(p)) {
      registeredPoints.remove(p);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public final ListModel<ScadaPoint> getRegisteredPointsModel() {
    return registeredPoints;
  }

  @Override
  public final Collection<ScadaPoint> getRegisteredPoints() {
    return new ArrayList<ScadaPoint>(registeredPoints);
  }

  @Override
  public final String toString() {
    return getName();
  }

  @Override
  public final String getName() {
    return name;
  }

  private void updateName() {
    Object oldValue = getName();
    this.name = generateName();
    firePropertyChange(PROPERTY_NAME, oldValue, this.name);
  }

  protected abstract String generateName();

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    updateName();
  }

}
