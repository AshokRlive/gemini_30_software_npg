/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.factory;

import java.util.HashMap;

import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;


/**
 *
 */
public class ProtocolFactories {
  private ProtocolFactories() {}
  private static final HashMap<ProtocolType, IProtocolFactory> factories = new HashMap<>();
  
  public static void registerFactory(ProtocolType type, IProtocolFactory factory) {
    factories.put(type, factory);
  }
  
  public static IProtocolFactory getFactory(ProtocolType type) {
    return factories.get(type);
  }
}

