/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.g3schema.ns_sdnp3.AuthUserT;
import com.g3schema.ns_sdnp3.AuthUsersT;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_KEYWRAP;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthEnums.DNPAUTH_USER_ROLE;


/**
 * Configuration of SDNP3 users.
 */
public final class SDNPAuthUsersConfig extends ItemListManager<SDNP3User>{
  
  public void writeToXML(AuthUsersT xml) {
    Collection<SDNP3User> allUsers = getAllItems();
    AuthUserT xmlUser;
    for (SDNP3User user : allUsers) {
      if(user != null) {
        xmlUser = xml.user.append();
        xmlUser.updateKey.setValue(convertKeyFromHexToXml(user.getUpdateKey().getBytes()));
        xmlUser.userName.setValue(user.getUserName());
        xmlUser.userNumber.setValue(user.getUserNumber());
        xmlUser.userRole.setValue(user.getUserRole().getValue());
      }
    }
  }

  public void readFromXML(AuthUsersT xml) {
    int count = xml.user.count();
    SDNP3User newUser;
    ArrayList<SDNP3User> users = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      newUser = new SDNP3User();
      
      // Read update key and key wrap algorithm
      try {
        byte[] keyBytes = xml.user.at(i).updateKey.getValue();
        final DNPAUTH_KEYWRAP algorithm = keyBytes.length > 16 
        ? DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES256
        : DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES128;
        newUser.setKeyWrapAlgorithm(algorithm);
        newUser.setUpdateKey(new String(convertKeyFromXmlToHex(keyBytes)));
      } catch(Throwable e) {
        Logger.getLogger(getClass()).error("Failed to read user's update key from XML. User number:"
              + xml.user.at(i).userNumber.getValue(), e);
      }
      
      newUser.setUserName(xml.user.at(i).userName.getValue());
      newUser.setUserNumber((int) xml.user.at(i).userNumber.getValue());
      newUser.setUserRole(DNPAUTH_USER_ROLE.forValue((int) xml.user.at(i).userRole.getValue()));
      users.add(newUser);
    }
    
    addAll(users);
  }
  
  
  /**
   * Sets the key size to 16 or 32 according to the given algorithm.
   */
  private static byte[] correctKeySize(byte[] updateKeyBytes, DNPAUTH_KEYWRAP keyWrapAlgorithm) {
    final int keySize = keyWrapAlgorithm == DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES128 ? 16 : 32;
    byte[] keyBuf = new byte[keySize];
    Arrays.fill(keyBuf, (byte)0); // initialise buffer to 0.
    
    // Copy original key to the key buffer
    System.arraycopy(updateKeyBytes, 0, keyBuf, 0, Math.min(keySize , updateKeyBytes.length));
    
    return keyBuf;
  }
  
  
  final private static char[] HEX = "0123456789ABCDEF".toCharArray();

  static byte[] convertKeyFromXmlToHex(byte[] keyBytesInXml) {
    DNPAUTH_KEYWRAP algorithm = keyBytesInXml.length > 16 
    ? DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES256
        : DNPAUTH_KEYWRAP.DNPAUTH_KEYWRAP_AES128;
    keyBytesInXml = correctKeySize(keyBytesInXml, algorithm);
    
    char[] hexChars = new char[keyBytesInXml.length * 3 - 1];
    for (int j = 0; j < keyBytesInXml.length; j++) {
      int v = keyBytesInXml[j] & 0xFF;
      hexChars[j * 3] = HEX[v >>> 4];
      hexChars[j * 3 + 1] = HEX[v & 0x0F];
      
      // Append delimiter
      if(j < keyBytesInXml.length-1)
        hexChars[j * 3 + 2] = SDNP3User.UDPATE_KEY_DELIMITER;
    }
    return new String(hexChars).getBytes();
  }

  static byte[] convertKeyFromHexToXml(byte[] keyInHex) {
    // Remove delimiter
    String[] tokens = new String(keyInHex).split(SDNP3User.UDPATE_KEY_DELIMITER_STR);
    byte[] data = new byte[tokens.length];
    
    for (int i = 0; i < data.length; i ++) {
      data[i] = (byte) ((Character.digit(tokens[i].charAt(0), 16) << 4)
          + Character.digit(tokens[i].charAt(1), 16));
    }
    return data;
  }
}

