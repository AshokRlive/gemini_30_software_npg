/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.GlobalVariationsConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.xml.gen.common.DNP3Enum.ACCEPTED_COMMAND;

/**
 * The SDNP3 output point.
 */
public abstract class SDNP3OutputPoint extends SDNP3Point {
  
  private final DNP3ObjConfig conf; 


 private SDNP3OutputPoint(IScadaIoMap<SDNP3Point> ioMap, long id, SDNP3PointType type) {
    super(ioMap, type, id);
    
    conf = createDNP3ObjConfig(ioMap, type);

    // Output point shouldn't enable class0 by default
    if(conf != null)
      conf.setEnableClass0(false);
  }

 abstract public ACCEPTED_COMMAND getAccpetedCommand();
 
 abstract public void setAccpetedCommand(ACCEPTED_COMMAND accpetedCommand);

 private static DNP3ObjConfig createDNP3ObjConfig(IScadaIoMap<SDNP3Point> ioMap, SDNP3PointType type) {
   GlobalVariationsConfig globalVar = ioMap == null ?  
       null : ((SDNP3Session) ioMap.getSession()).getGlobalEvtVariation();
   
   DNP3ObjGroup objGroup = DNP3ObjGroup.convertFrom(type);
   boolean hasStatic = true;
   boolean hasEvent  = true;
   
   if(type == SDNP3PointType.BinaryOutput)
     hasEvent = false;
   
   return objGroup == null ? null : new DNP3ObjConfig(globalVar, objGroup, hasStatic, hasEvent);
 }
  @Override
  public DNP3ObjConfig getEventConf() {
    return conf;
  }
  
  public static SDNP3OutputPoint createAnalogueOutput(IScadaIoMap<SDNP3Point> ioMap, long id) {
    return new SDNP3AnalogueOutputPoint(ioMap,id);
  }
  
  public static SDNP3OutputPoint createBinaryOutput(IScadaIoMap<SDNP3Point> ioMap, long id) {
    return new SDNP3BinaryOutputPoint(ioMap,id);
  }


  @Override
  public void applyParameters(HashMap<String, Object> parameters) {
    configureDNP3Obj(conf, parameters);
    
    if (parameters.containsKey(SDNP3Point.PARAMETER_CUSTOM_DESCRIPTION)) {
      setCustomDescription((String) parameters.get(SDNP3Point.PARAMETER_CUSTOM_DESCRIPTION));
    }
    
    if (parameters.containsKey(SDNP3Point.PARAMETER_DISABLED)) {
      setEnabled(!(boolean)parameters.get(SDNP3Point.PARAMETER_DISABLED));
    }
    
    if (parameters.containsKey(SDNP3Point.PARAMETER_ENABLE_STORE_EVENT)) {
      setEnableStoreEvent((boolean)parameters.get(SDNP3Point.PARAMETER_ENABLE_STORE_EVENT));
    }
  }

  public static class SDNP3AnalogueOutputPoint extends SDNP3OutputPoint{
    SDNP3AnalogueOutputPoint(IScadaIoMap<SDNP3Point> ioMap, long id) {
      super(ioMap, id, SDNP3PointType.AnalogOutput);
    }

    @Override
    public ACCEPTED_COMMAND getAccpetedCommand() {
      return null;
    }

    @Override
    public void setAccpetedCommand(ACCEPTED_COMMAND accpetedCommand) {
      // Not supported
    }
  }
  
  public static class SDNP3BinaryOutputPoint extends SDNP3OutputPoint{
    public final static String PROPERTY_ACCEPTED_CMD = "accpetedCommand";
    
    private ACCEPTED_COMMAND accpetedCommand = ACCEPTED_COMMAND.ACCEPTED_COMMAND_ANY;

    SDNP3BinaryOutputPoint(IScadaIoMap<SDNP3Point> ioMap, long id) {
      super(ioMap, id, SDNP3PointType.BinaryOutput);
    }

    
    public ACCEPTED_COMMAND getAccpetedCommand() {
      return accpetedCommand;
    }
    
    public void setAccpetedCommand(ACCEPTED_COMMAND accpetedCommand) {
      if(accpetedCommand == null) {
        Logger.getLogger(SDNP3BinaryOutputPoint.class).error("accpetedCommand must not be null");
        return;
      }
      
      Object oldValue = this.accpetedCommand;
      this.accpetedCommand = accpetedCommand;
      firePropertyChange(PROPERTY_ACCEPTED_CMD, oldValue, accpetedCommand);
    }
  }
  
}
