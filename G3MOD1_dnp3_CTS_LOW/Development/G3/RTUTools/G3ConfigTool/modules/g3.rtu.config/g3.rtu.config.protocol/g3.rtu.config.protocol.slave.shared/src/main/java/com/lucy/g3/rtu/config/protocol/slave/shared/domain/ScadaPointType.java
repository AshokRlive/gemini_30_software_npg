/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

/**
 * The Interface of Scada Point Type.
 */
public interface ScadaPointType {

  boolean isInput();

  boolean isOutput();

  String getDescription();

  long getMaximumPointID();

  long getMinimumPointID();
}
