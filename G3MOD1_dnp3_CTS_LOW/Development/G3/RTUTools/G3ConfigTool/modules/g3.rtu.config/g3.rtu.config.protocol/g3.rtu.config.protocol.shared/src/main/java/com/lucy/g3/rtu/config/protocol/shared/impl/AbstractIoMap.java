/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.impl;

import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.validate.IoMapValidator;


public abstract class AbstractIoMap implements IIoMap {
  private final IoMapValidator validator;

  public AbstractIoMap() {
    super();
    this.validator = new IoMapValidator(this);
  }

  @Override
  public final IoMapValidator getValidator() {
    return validator;
  }
  
}

