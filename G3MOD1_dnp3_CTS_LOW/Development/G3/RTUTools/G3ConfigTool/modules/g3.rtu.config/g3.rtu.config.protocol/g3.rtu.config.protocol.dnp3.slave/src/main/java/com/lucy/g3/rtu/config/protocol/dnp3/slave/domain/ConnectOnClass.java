/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import com.jgoodies.binding.beans.Model;

/**
 * The configuration of Connection on Class Mask.
 */
public class ConnectOnClass extends Model {

  public static final String PROPERTY_CONNECT_ON_CLASS1 = "connectOnClass1";
  public static final String PROPERTY_CONNECT_ON_CLASS2 = "connectOnClass2";
  public static final String PROPERTY_CONNECT_ON_CLASS3 = "connectOnClass3";

  private boolean connectOnClass1; // connect on event class1
  private boolean connectOnClass2; // connect on event class2
  private boolean connectOnClass3; // connect on event class3


  public ConnectOnClass() {
    super();
    setConnectOnClassMask(1);
  }

  public void setConnectOnClassMask(int mask) {
    setConnectOnClass1((mask >> 0 & 1) == 1);
    setConnectOnClass2((mask >> 1 & 1) == 1);
    setConnectOnClass3((mask >> 2 & 1) == 1);
  }

  public int getConnectOnClassMask() {
    int mask = 0;

    if (connectOnClass1) {
      mask |= 1 << 0;
    }

    if (connectOnClass2) {
      mask |= 1 << 1;
    }

    if (connectOnClass3) {
      mask |= 1 << 2;
    }

    return mask;
  }

  public boolean isConnectOnClass1() {
    return connectOnClass1;
  }

  public void setConnectOnClass1(boolean connectOnClass1) {
    Object oldValue = this.connectOnClass1;
    this.connectOnClass1 = connectOnClass1;
    firePropertyChange(PROPERTY_CONNECT_ON_CLASS1, oldValue, connectOnClass1);
  }

  public boolean isConnectOnClass2() {
    return connectOnClass2;
  }

  public void setConnectOnClass2(boolean connectOnClass2) {
    Object oldValue = this.connectOnClass2;
    this.connectOnClass2 = connectOnClass2;
    firePropertyChange(PROPERTY_CONNECT_ON_CLASS2, oldValue, connectOnClass2);
  }

  public boolean isConnectOnClass3() {
    return connectOnClass3;
  }

  public void setConnectOnClass3(boolean connectOnClass3) {
    Object oldValue = this.connectOnClass3;
    this.connectOnClass3 = connectOnClass3;
    firePropertyChange(PROPERTY_CONNECT_ON_CLASS3, oldValue, connectOnClass3);
  }
}
