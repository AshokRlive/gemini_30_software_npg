/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.log4j.Logger;

import com.g3schema.ns_common.VirtualPointRefT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The implementation of {@linkplain ScadaPointSourceRepository}.
 */
public class ScadaPointSourceRepository implements 
    ListDataListener, PropertyChangeListener {

  // @formatter:off
  private final ArrayList<ScadaPointInput>  analogs    = new ArrayList<>();
  private final ArrayList<ScadaPointInput>  binaries   = new ArrayList<>();
  private final ArrayList<ScadaPointInput>  doubles    = new ArrayList<>();
  private final ArrayList<ScadaPointInput>  counters   = new ArrayList<>();
  private final ArrayList<ScadaPointOutput> outputs    = new ArrayList<>();

  @SuppressWarnings("unchecked")
  private final ArrayList<ScadaPointSource>[] all      = new ArrayList[]{
    analogs,
    binaries,
    doubles,
    counters,
    outputs
  };

  @SuppressWarnings("unchecked")
  private final ArrayList<ScadaPointInput>[] allInputs   = new ArrayList[]{
    analogs,
    binaries,
    doubles,
    counters,
  };

  private final HashMap<Object, ScadaPointSource> rawSourceMap = new HashMap<>();
  // @formatter:on

  private Logger log = Logger.getLogger(ScadaPointSourceRepository.class);

  private final ListModel<VirtualPoint> pointsModel;
  private final ListModel<ICLogic> clogicModel;


  public ScadaPointSourceRepository(ListModel<VirtualPoint> pointsModel, ListModel<ICLogic> clogicModel) {
    this.pointsModel = Preconditions.checkNotNull(pointsModel, "pointsModel must not be null");
    this.clogicModel = Preconditions.checkNotNull(clogicModel, "clogicModel must not be null");

    int size = pointsModel.getSize();
    for (int i = 0; i < size; i++) {
      add(pointsModel.getElementAt(i));
    }

    size = clogicModel.getSize();
    for (int i = 0; i < size; i++) {
      add(clogicModel.getElementAt(i));
    }

    pointsModel.addListDataListener(this);
    clogicModel.addListDataListener(this);
  }

  
  public ScadaPointInput getInputSourceByRef(VirtualPointRefT xml) {
    int group = (int) xml.pointGroup.getValue();
    int id = (int) xml.pointID.getValue();

    for (int i = 0; i < allInputs.length; i++) {
      for (ScadaPointInput p : allInputs[i]) {
        if (p.getGroup() == group && p.getId() == id) {
          return p;
        }
      }
    }
    return null;
  }

  
  public Collection<ScadaPointInput> getAnalogueInputSources() {
    return getSortedList(analogs);
  }

  
  public Collection<ScadaPointInput> getBinaryInputSources() {
    return getSortedList(binaries);
  }

  
  public Collection<ScadaPointInput> getDoubleBinaryInputSources() {
    return getSortedList(doubles);
  }

  
  public Collection<ScadaPointInput> getCounterSources() {
    return getSortedList(counters);
  }

  
  public ScadaPointOutput getDigitalOutputSource(int group) {
    for (ScadaPointOutput output : outputs) {
      if (output.getGroup() == group && !output.isAnalogue()) {
        return output;
      }
    }
  
    return null;
  }

  
  public Collection<ScadaPointOutput> getDigitalOutputSouces() {
    return getSortedList(filter(outputs,false));
  }
  
  private List<ScadaPointOutput> filter(ArrayList<ScadaPointOutput> sources, boolean filterAnalogue) {
    ArrayList<ScadaPointOutput> result = new ArrayList<>();
    for (ScadaPointOutput s :sources) {
      if(filterAnalogue && s.isAnalogue())
        result.add(s);
      else if(!filterAnalogue && !s.isAnalogue())
        result.add(s);
    }
    return result;
  }

  
  public ScadaPointOutput getAnalogOutputSource(int group) {
    for (ScadaPointOutput output : outputs) {
      if (output.getGroup() == group && output.isAnalogue()) {
        return output;
      }
    }
    
    return null;
  }
  
  
  public Collection<ScadaPointOutput> getAnalogOutputSouces() {
    return getSortedList(filter(outputs,true));
  }

  
  public ScadaPointSource getSourceByRaw(Object rawSource) {
    for (int i = 0; i < all.length; i++) {
      for (ScadaPointSource p : all[i]) {
        if (p.getRawSource() == rawSource) {
          return p;
        }
      }
    }

    return null;
  }

  
  @Override
  public void intervalAdded(ListDataEvent e) {
    int start = e.getIndex0();
    int end = e.getIndex1();
    if (e.getSource() == pointsModel) {
      for (int i = start; i >= 0 && i <= end; i++) {
        add(pointsModel.getElementAt(i));
      }

    } else if (e.getSource() == clogicModel) {
      for (int i = start; i >= 0 && i <= end; i++) {
        add(clogicModel.getElementAt(i));
      }
    }
  }

  
  @Override
  public void intervalRemoved(ListDataEvent e) {
    /*
     * Remove action has been done in PropertyChangeListener. We cannot
     * implement "remove" here because there is no way to find out the objects
     * that have been removed. What we can get from ListDataEvent is the index.
     */
  }

  
  @Override
  public void contentsChanged(ListDataEvent e) {
    // Do nothing since this is not supposed to happen
  }

  private ScadaPointSource add(VirtualPoint newPoint) {
    if (newPoint == null) {
      return null;
    }

    ScadaPointInput newSource = new ScadaPointInput(newPoint);

    VirtualPointType rawType = newSource.getRawSource().getType();
    boolean success = false;
    switch (rawType) {
    case ANALOGUE_INPUT:
      success = analogs.add(newSource);
      break;

    case BINARY_INPUT:
      success = binaries.add(newSource);
      break;

    case COUNTER:
      success = counters.add(newSource);
      break;

    case DOUBLE_BINARY_INPUT:
      success = doubles.add(newSource);
      break;

    default:
      log.error("Unsupported type:" + rawType);
    }

    if (success && newSource != null) {
      putSourceToMap(newSource);
    }

    return success ? newSource : null;
  }

  private ScadaPointSource add(ICLogic newLogic) {
    if (newLogic == null || !(newLogic instanceof IOperableLogic)) {
      return null;
    }

    ScadaPointOutput newSource = new ScadaPointOutput((IOperableLogic) newLogic);

    if (outputs.add(newSource)) {
      putSourceToMap(newSource);
      return newSource;
    } else {
      return null;
    }
  }

  private void putSourceToMap(ScadaPointSource newSource) {
    ObservableBean2 raw = newSource.getRawSource();
    rawSourceMap.put(raw, newSource);
    raw.addPropertyChangeListener(INode.PROPERTY_DELETED, this);
  }

  private void remove(Object rawSource) {
    for (int i = 0; i < all.length; i++) {
      for (ScadaPointSource p : all[i]) {
        if (p.getRawSource() == rawSource) {
          all[i].remove(p);
          ((AbstractScadaPointSource<?>) p).delete();
          return;
        }
      }
    }

  }

  
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (INode.PROPERTY_DELETED.equals(evt.getPropertyName())) {
      if (Boolean.TRUE.equals(evt.getNewValue())) {
        remove(evt.getSource());
      }
    }
  }

  private <T extends ScadaPointSource> List<T> getSortedList(List<T> list) {
    Collections.sort(list, ScadaPointSourceComparator.INSTANCE);
    return new ArrayList<T>(list);
  }
  

}
