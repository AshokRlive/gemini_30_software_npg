/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.domain;

import com.g3schema.ns_s104.S104ChlConfT;
import com.g3schema.ns_s104.S104LinkLayerConfT;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870ChannelConf;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;

/**
 * The configuration of {@link S104Channel}.
 */
public class S104ChannelConf extends IEC870ChannelConf {

  public static final String TYPE_TCP_IP = "TCP/IP";
  
  // ====== Link Layer ======
  public static final String PROPERTY_T1_ACK_PERIOD = "t1AckPeriod";
  public static final String PROPERTY_T2_FRAME_PERIOD = "t2sFramePeriod";
  public static final String PROPERTY_T3_TEST_PERIOD = "t3TestPeriod";
  public static final String PROPERTY_OFFLINE_POLL_PERIOD = "offlinePollPeriod";
  public static final String PROPERTY_DISCARD_FRAMES_ON_DISCONNECT = "discardFramesOnDisconnect";
  public static final String PROPERTY_KVALUE = "kvalue";
  public static final String PROPERTY_WVALUE = "wvalue";

  // @formatter:off
  private long t1AckPeriod        = getDefault(PROPERTY_T1_ACK_PERIOD);
  private long t2sFramePeriod     = getDefault(PROPERTY_T2_FRAME_PERIOD);
  private long t3TestPeriod       = getDefault(PROPERTY_T3_TEST_PERIOD);
  private long offlinePollPeriod  = getDefault(PROPERTY_OFFLINE_POLL_PERIOD);
  private long kvalue             = getDefault(PROPERTY_KVALUE);
  private long wvalue             = getDefault(PROPERTY_WVALUE);
  private boolean discardFramesOnDisconnect = false;
  // @formatter:on

  private final S104ChannelTCPConf tcpConfig;


  public S104ChannelConf(S104Channel channel, String channelName) {
    super(channel, channelName);
    this.tcpConfig = new S104ChannelTCPConf();

    LoggingUtil.logPropertyChanges(this);
  }

  public long getT1AckPeriod() {
    return t1AckPeriod;
  }

  public void setT1AckPeriod(long t1AckPeriod) {
    Object oldValue = this.t1AckPeriod;
    this.t1AckPeriod = t1AckPeriod;
    firePropertyChange(PROPERTY_T1_ACK_PERIOD, oldValue, t1AckPeriod);
  }

  public long getT2sFramePeriod() {
    return t2sFramePeriod;
  }

  public void setT2sFramePeriod(long t2sFramePeriod) {
    Object oldValue = this.t2sFramePeriod;
    this.t2sFramePeriod = t2sFramePeriod;
    firePropertyChange(PROPERTY_T2_FRAME_PERIOD, oldValue, t2sFramePeriod);
  }

  public long getT3TestPeriod() {
    return t3TestPeriod;
  }

  public void setT3TestPeriod(long t3TestPeriod) {
    Object oldValue = this.t3TestPeriod;
    this.t3TestPeriod = t3TestPeriod;
    firePropertyChange(PROPERTY_T3_TEST_PERIOD, oldValue, t3TestPeriod);
  }

  public long getOfflinePollPeriod() {
    return offlinePollPeriod;
  }

  public void setOfflinePollPeriod(long offlinePollPeriod) {
    Object oldValue = this.offlinePollPeriod;
    this.offlinePollPeriod = offlinePollPeriod;
    firePropertyChange(PROPERTY_OFFLINE_POLL_PERIOD, oldValue, offlinePollPeriod);
  }


  public boolean isDiscardFramesOnDisconnect() {
    return discardFramesOnDisconnect;
  }

  public void setDiscardFramesOnDisconnect(boolean discardFramesOnDisconnect) {
    Object oldValue = this.discardFramesOnDisconnect;
    this.discardFramesOnDisconnect = discardFramesOnDisconnect;
    firePropertyChange(PROPERTY_DISCARD_FRAMES_ON_DISCONNECT, oldValue,
        discardFramesOnDisconnect);
  }

  @Override
  public String getChannelConfSummaryText() {
    return getTCPConfigAsString();
  }

  @Override
  public ProtocolChannelSerialConf getSerialConf() {
    return null;
  }

  @Override
  public S104ChannelTCPConf getTcpConf() {
    return tcpConfig;
  }

  public long getKvalue() {
    return kvalue;
  }

  public void setKvalue(long kvalue) {
    Object oldValue = this.kvalue;
    this.kvalue = kvalue;
    firePropertyChange(PROPERTY_KVALUE, oldValue, kvalue);
  }

  public long getWvalue() {
    return wvalue;
  }

  public void setWvalue(long wvalue) {
    Object oldValue = this.wvalue;
    this.wvalue = wvalue;
    firePropertyChange(PROPERTY_WVALUE, oldValue, wvalue);
  }

  public void writeToXML(S104ChlConfT xml) {
    /* General */
    xml.chnlName.setValue(getChannelName());
    xml.enabled.setValue(getChannel().isEnabled());
    xml.incrementalTimeoutMs.setValue(getIncrementalTimeout());

    /* Link layer */
    S104LinkLayerConfT xmlLinklayer = xml.linkLayer.append();
    xmlLinklayer.txFrameSize.setValue(getTxFrameSize());
    xmlLinklayer.rxFrameSize.setValue(getRxFrameSize());
    xmlLinklayer.t1AckPeriodMs.setValue(getT1AckPeriod());
    xmlLinklayer.t2SFramePeriodMs.setValue(getT2sFramePeriod());
    xmlLinklayer.t3TestPeriodMs.setValue(getT3TestPeriod());
    xmlLinklayer.offlinePollPeriodMs.setValue(getOfflinePollPeriod());
    xmlLinklayer.discardIFramesOnDisconnect.setValue(isDiscardFramesOnDisconnect());
    xmlLinklayer.kValue.setValue(getKvalue());
    xmlLinklayer.wValue.setValue(getWvalue());

    /* Linux IO */
    getTcpConf().writeToXML(xml.linuxIO.append().tcp.append());

  }

  public void readFromXML(S104ChlConfT xml) {
    if (xml.enabled.exists()) {
      getChannel().setEnabled(xml.enabled.getValue());
    }
    
    /* General */
    setIncrementalTimeout(xml.incrementalTimeoutMs.getValue());
    setChannelName(xml.chnlName.getValue());

    /* Link layer */
    S104LinkLayerConfT xmlLinklayer = xml.linkLayer.first();
    setTxFrameSize(xmlLinklayer.txFrameSize.getValue());
    setRxFrameSize(xmlLinklayer.rxFrameSize.getValue());
    setT1AckPeriod(xmlLinklayer.t1AckPeriodMs.getValue());
    setT2sFramePeriod(xmlLinklayer.t2SFramePeriodMs.getValue());
    setT3TestPeriod(xmlLinklayer.t3TestPeriodMs.getValue());
    setOfflinePollPeriod(xmlLinklayer.offlinePollPeriodMs.getValue());
    setDiscardFramesOnDisconnect(xmlLinklayer.discardIFramesOnDisconnect.getValue());
    setKvalue(xmlLinklayer.kValue.getValue());
    setWvalue(xmlLinklayer.wValue.getValue());

    /* Linux IO */
    if (xml.linuxIO.first().tcp.exists()) {
      getTcpConf().readFromXML(xml.linuxIO.first().tcp.first());
    }
  }

  @Override
  public IEC870Constraints getConstraints() {
    return IEC870Constraints.INSTANCE_104;
  }
  
  @Override
  public Object getLinkType() {
    return TYPE_TCP_IP;
  }

  @Override
  public void setLinkType(Object type) {
    // No effect
  }
  
}
