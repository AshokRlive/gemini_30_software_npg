/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.Frame;
import java.util.ArrayList;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point.SDNP3PointBulkEditDialog;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

/**
 *
 */
public class SDNP3PointBulkEditDialogTest {

  public static void main(String[] args) {
    ArrayList<ScadaPointSource> targets = new ArrayList<ScadaPointSource>();
    SDNP3Channel channel = new SDNP3Channel(new SDNP3());
    SDNP3Session dnp3Session = channel.addSession("session");
    ArrayList<SDNP3Point> points = new ArrayList<SDNP3Point>();
    points.add(SDNP3PointFactory.create(dnp3Session.getIomap(), SDNP3PointType.BinaryInput, 100));
    points.add(SDNP3PointFactory.create(dnp3Session.getIomap(), SDNP3PointType.BinaryInput, 200));
    points.add(SDNP3PointFactory.create(dnp3Session.getIomap(), SDNP3PointType.BinaryInput, 300));
    points.add(null);
    for (int i = 0; i < points.size(); i++) {
      try {
        dnp3Session.getIomap().addPoint(points.get(i));
      } catch (DuplicatedException e) {
        e.printStackTrace();
      }
    }

    SDNP3PointBulkEditDialog dialog = new SDNP3PointBulkEditDialog((Frame) null, points,
        SDNP3PointType.BinaryInput, dnp3Session.getIomap());
    dialog.setVisible(true);

    for (int i = 0; i < points.size(); i++) {
      System.out.println("Index: " + 1);
      System.out.println("ID: " + points.get(i).getPointID());
      System.out.println("description: " + points.get(i).getCustomDescription());
      System.out.println("disabled: " + !points.get(i).isEnabled());
      System.out.println("");

    }
  }

}
