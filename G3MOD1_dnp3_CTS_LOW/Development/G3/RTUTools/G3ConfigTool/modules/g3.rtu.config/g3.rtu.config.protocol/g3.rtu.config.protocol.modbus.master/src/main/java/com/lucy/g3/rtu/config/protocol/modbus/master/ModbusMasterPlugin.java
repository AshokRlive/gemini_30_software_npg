/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBChannelPage;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBIoMapPage;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBPage;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.MMBSessionPage;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;

/**
 *
 */
public class ModbusMasterPlugin implements IConfigPlugin, IPageFactory {

  private ModbusMasterPlugin() {
  }


  private final static ModbusMasterPlugin INSTANCE = new ModbusMasterPlugin();


  public static void init() {
    PageFactories.registerFactory(PageFactories.KEY_MODBUS, INSTANCE);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof MMB) {
      return new MMBPage((MMB) data);
      
    } else if (data instanceof MMBChannel) {
      IConfig config = ((MMBChannel) data).getProtocol().getManager().getOwner();
      return new MMBChannelPage((MMBChannel) data, config);

    } else if (data instanceof MMBSession) {
      return new MMBSessionPage((MMBSession) data);

    } else if (data instanceof MMBIoMap) {
      return new MMBIoMapPage((MMBIoMap) data);

    }

    return null;
  }
}
