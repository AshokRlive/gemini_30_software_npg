/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.domain;

import com.g3schema.ns_pstack.TCPConfT;
import com.g3schema.ns_s104.S104ChnlTCPConfT;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LINTCP_MODE;

/**
 * The TCP configuration of S104 channel.
 */
public final class S104ChannelTCPConf extends ProtocolChannelTCPConf {

  public static final long DEFAULT_PORT = getDefault(PROPERTY_IP_PORT);
  private static final long DEFAULT_TIMEOUT = getDefault(PROPERTY_IP_CONNECT_TIMEOUT);


  S104ChannelTCPConf() {
    super();
    setMode(LU_LINTCP_MODE.LU_LINTCP_MODE_SERVER);
    setIpPort(DEFAULT_PORT);
    setIpConnectTimeout(DEFAULT_TIMEOUT);
  }

  private static long getDefault(String property) {
    return IEC870Constraints.INSTANCE_104.getDefault(IEC870Constraints.PREFIX_CHANNEL_TCP, property);
  }
  
 public void writeToXML(S104ChnlTCPConfT xml) {
    super.writeToXML(xml);
    xml.disconnectOnNewSyn.setValue(isDisconnectOnNewSync());
  }

  public void readFromXML(S104ChnlTCPConfT xml) {
    super.readFromXML(xml);
    
    if (xml.disconnectOnNewSyn.exists()) {
      setDisconnectOnNewSync(xml.disconnectOnNewSyn.getValue());
    }
  }

}
