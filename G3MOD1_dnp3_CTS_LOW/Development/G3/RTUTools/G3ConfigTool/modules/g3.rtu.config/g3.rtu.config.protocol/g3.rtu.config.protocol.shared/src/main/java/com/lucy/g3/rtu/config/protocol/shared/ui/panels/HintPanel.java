/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.jgoodies.validation.view.ValidationResultViewFactory;

/**
 * The panel for showing a hint message of a component when user selects it.
 */
public final class HintPanel extends JPanel {

  private static final HashMap<Class<?>, FocusChangeHandler> EXISTING_PCLS =
      new HashMap<Class<?>, FocusChangeHandler>();

  private boolean hintEnabled = true;


  public HintPanel(Class<?> parentClass) {
    Preconditions.checkNotNull(parentClass, "parentClass is null");

    initComponents();

    initEventHandling(parentClass);
  }

  public void enableHint() {
    this.hintEnabled = true;
  }

  public void disableHint() {
    this.hintEnabled = false;
  }

  private void initEventHandling(Class<?> parentClass) {
    FocusChangeHandler focusPCL = EXISTING_PCLS.get(parentClass);

    /* Remove previous PCL */
    if (focusPCL != null) {
      KeyboardFocusManager.getCurrentKeyboardFocusManager()
          .removePropertyChangeListener(focusPCL);
      EXISTING_PCLS.remove(parentClass);
    }

    /* Create and register new PCL */
    focusPCL = new FocusChangeHandler();

    KeyboardFocusManager.getCurrentKeyboardFocusManager()
        .addPropertyChangeListener(focusPCL);
    EXISTING_PCLS.put(parentClass, focusPCL);
  }

  private void createUIComponents() {
    infoLabel = new JLabel(ValidationResultViewFactory.getInfoIcon());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    infoArea = new JTextArea();

    // ======== this ========
    setVisible(false);
    setLayout(new FormLayout(
        "default, $lcgap, default:grow",
        "default"));

    // ---- infoLabel ----
    infoLabel.setText(" ");
    add(infoLabel, CC.xy(1, 1));

    // ---- infoArea ----
    infoArea.setEditable(false);
    infoArea.setOpaque(false);
    infoArea.setForeground(Color.blue);
    infoArea.setLineWrap(true);
    infoArea.setFont(UIManager.getFont("Label.font"));
    add(infoArea, CC.xy(3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel infoLabel;
  private JTextArea infoArea;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Displays an input hint for components that get the focus permanently.
   */
  private final class FocusChangeHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      String propertyName = evt.getPropertyName();
      if (!"permanentFocusOwner".equals(propertyName)) {
        return;
      }

      Component focusOwner = KeyboardFocusManager
          .getCurrentKeyboardFocusManager().getFocusOwner();

      String focusHint = focusOwner instanceof JComponent ? (String) ValidationComponentUtils
          .getInputHint((JComponent) focusOwner) : null;

      if (hintEnabled && !Strings.isBlank(focusHint) && focusHint != "null") {
        infoArea.setText(focusHint);
        setVisible(true);
      } else {
        setVisible(false);
      }
    }
  }
}
