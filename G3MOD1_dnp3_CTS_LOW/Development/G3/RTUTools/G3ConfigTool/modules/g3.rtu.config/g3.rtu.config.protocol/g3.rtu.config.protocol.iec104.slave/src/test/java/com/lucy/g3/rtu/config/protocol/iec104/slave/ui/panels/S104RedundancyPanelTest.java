/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.ui.panels;

import javax.swing.JFrame;

import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Redundancy;
import com.lucy.g3.rtu.config.protocol.iec104.slave.ui.panels.S104RedundancyPanel;


public class S104RedundancyPanelTest {
  public static void main(String[] args) {
    JFrame frame = new JFrame();
    S104Redundancy rdcy = new S104Redundancy();
    frame.getContentPane().add(new S104RedundancyPanel(rdcy));
    frame.pack();
    frame.setVisible(true);
  }
}

