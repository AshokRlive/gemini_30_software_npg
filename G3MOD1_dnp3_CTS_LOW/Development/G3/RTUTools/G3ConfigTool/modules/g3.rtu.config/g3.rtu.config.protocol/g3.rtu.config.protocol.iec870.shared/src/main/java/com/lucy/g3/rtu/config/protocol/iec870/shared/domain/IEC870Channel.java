/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolChannel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;

/**
 * The channel of IEC870.
 */
public abstract class IEC870Channel<SessionT extends IProtocolSession> extends AbstractProtocolChannel<SessionT> {

  private Logger log = Logger.getLogger(IEC870Channel.class);

  public IEC870Channel(IScadaProtocol<?> protocol) {
    super(protocol);
  }

}
