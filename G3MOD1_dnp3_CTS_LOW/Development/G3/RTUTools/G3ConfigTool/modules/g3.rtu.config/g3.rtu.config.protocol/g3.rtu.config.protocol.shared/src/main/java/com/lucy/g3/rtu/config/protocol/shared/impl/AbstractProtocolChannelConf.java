/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.impl;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannelConf;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;

/**
 * Basic implementation of {@linkplain IProtocolChannelConf}.
 */
public abstract class AbstractProtocolChannelConf extends Model implements IProtocolChannelConf {

  private final IProtocolChannel<?> channel;

  private String channelName = "Unnamed";


  public AbstractProtocolChannelConf(IProtocolChannel<?> channel, String channelName) {
    this.channel = channel;

    if (channelName != null) {
      this.channelName = channelName;
    }

    //LoggingUtil.logPropertyChanges(this);
  }

  @Override
  public IProtocolChannel<?> getChannel() {
    return channel;
  }

  @Override
  public final String getChannelName() {
    return channelName;
  }

  @Override
  public final void setChannelName(String channelName) {
    Object oldValue = getChannelName();
    this.channelName = channelName;
    firePropertyChange(PROPERTY_CHANNEL_NAME, oldValue, channelName);
  }

  /**
   * Gets the serial config as string.
   *
   * @return the serial config as string
   */
  protected final String getSerialConfigAsString() {
    ISerialPort port = null;

    ProtocolChannelSerialConf serialconf = getSerialConf();
    if (serialconf != null) {
      port = serialconf.getSerialPort();
    }

    return String.format("Serial [%s]", port == null ? "Not Configured" : port.getDisplayName());
  }

  /**
   * Gets the TCP config as string.
   *
   * @return the TCP config as string
   */
  protected final String getTCPConfigAsString() {
    ProtocolChannelTCPConf tcpconf = getTcpConf();
    String ip = null;
    if (tcpconf != null) {
      ip = tcpconf.getIpAddress();
    }

    return String.format("TCP [%s]", ip == null ? "Not Configured" : ip);
  }

  @Override
  public void releaseResources() {
    ProtocolChannelSerialConf serial = getSerialConf();
    if(serial != null)
      serial.releaseSerialPort();
  }

}
