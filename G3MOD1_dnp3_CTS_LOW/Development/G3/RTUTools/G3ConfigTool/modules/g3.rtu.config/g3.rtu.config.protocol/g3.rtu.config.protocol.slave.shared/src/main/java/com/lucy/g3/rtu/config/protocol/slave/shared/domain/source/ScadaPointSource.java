/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import java.util.Collection;

import javax.swing.ListModel;

import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidResourceException;

/**
 * The input/output source of {@link ScadaPoint}.
 */
public interface ScadaPointSource extends ObservableBean2 {

  String PROPERTY_NAME = "name";
  /**
   * The maximum amount of protocol points that can be mapped to one
   * ScadaPointSource.
   */
  int MAX_SCADA_POINT_AMOUNT = 256;


  ObservableBean2 getRawSource();

  String getName();

  String getDescription();

  int getGroup();
  
  boolean isAnalogue(); // TODO create enum for DIGITAL ANALOGUE?

  int getId();
  
  /**
   * Gets the type of this source as string which can be used to categorise SCADA sources.
   * @return
   */
  String getType();

  /**
   * <p>
   * Registers a SCADA point which is mapped to this source.
   * </p>
   *
   * @param point
   *          the SCADA point to be registered
   * @exception InvalidResourceException
   *              if this resource cannot be register with any more observers or
   *              this resource is invalid.
   * @see #deregisterPoint(ScadaPoint)
   * @see #getRegisteredPointsModel()
   */
  void registerPoint(ScadaPoint point) throws InvalidResourceException;

  /**
   * De-registers a SCADA point.
   *
   * @param point
   *          the SCADA point to be de-registered.
   * @see #registerPoint(ScadaPoint)
   */
  void deregisterPoint(ScadaPoint point);

  /**
   * Gets all registered SCADA points as a list model.
   *
   * @return non-null list model that can be observed.
   */
  ListModel<ScadaPoint> getRegisteredPointsModel();

  /**
   * Gets all registered SCADA points.
   */
  Collection<ScadaPoint> getRegisteredPoints();

}
