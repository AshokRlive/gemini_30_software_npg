/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The point for SCADA protocol.
 */
public interface ScadaPoint extends IValidation {

  /** The name of bound property {@value} . */
  String PROPERTY_NAME = "name";

  /** The name of bound property {@value} . */
  String PROPERTY_POINT_ID = "pointID";

  /** The name of bound property {@value} . */
  String PROPERTY_CUSTOM_DESCRIPTION = "customDescription";

  /** The name of bound property {@value} .Type: {@linkplain ScadaPointSource}. */
  String PROPERTY_SOURCE = "source";

  /** The name of read-only property {@value} . */
  String PROPERTY_SESSION_NAME = "sessionName";

  /** The property for enabling/disabling this DNP3 point. */
  String PROPERTY_ENABLED = "enabled";
  
  /** The property for enabling/disabling storing event into FRAM. */
  String PROPERTY_ENABLE_STORE_EVENT = "enableStoreEvent";



  long getPointID();

  void setPointID(long newPointID) throws DuplicatedException;

  String getName();

  String getFullName();

  String getDescription();

  ScadaPointType getType();

  /**
   * Gets the IO Map this point belongs to.
   *
   * @return IO map object, null if it is not available.
   */
  IScadaIoMap<? extends ScadaPoint> getIoMap();

  /**
   * Gets the name of session this point belongs to.
   *
   * @return session name string, null if it is not available.
   */
  String getSessionName();

  String getProtocolName();

  /**
   * <p>
   * Gets the mapped resource(Virtual Point, CLogic,etc) of this protocol point.
   * </p>
   * <em>This is a property getter method.</em>
   *
   * @return
   */
  ScadaPointSource getSource();

  void setSource(ScadaPointSource source);

  /**
   * Deletes this scada point.
   */
  void delete();

  boolean isEnabled();

  void setEnabled(boolean enabled);

  String getCustomDescription();

  void setCustomDescription(String customDescription);

  /**
   * @return
   */
  boolean isEnableStoreEvent();
}
