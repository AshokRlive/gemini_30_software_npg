/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio;

import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.MMBIoBitMask;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterDataType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterType;

class Result implements WizardResultProducer {

  private static final int REGISTER_SIZE = 2; // BYTES
  
  private final ChannelType type;
  private final MMBIoMap iomap;
  
  
  private Map wizardData;
  
  public Result(MMBIoMap iomap, ChannelType type) {
    super();
    this.type = type;
    this.iomap = iomap;
  }

  @Override
  public Object finish(Map wizardData) throws WizardException {
    this.wizardData = wizardData;
    
    IChannel[] newChnls = null;
    
    switch (type) {
    case ANALOG_INPUT:
      newChnls = createAI();
      break;
      
    case DIGITAL_INPUT:
      newChnls = createDI();
      break;
      
    case DIGITAL_OUTPUT:
      newChnls = createDO();
      break;
      
    default:
      throw new WizardException("Unsupported channel type:" + type);
    }
    
    return newChnls;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

  private IChannel[] createAI() {
    RegisterType registerType = (RegisterType) wizardData.get(AIPage.KEY_REGISTER_TYPE); 
    String name = (String) wizardData.get(AIPage.KEY_CHANNEL_NAME);      
    Integer number = getInt(AIPage.KEY_NUMBER_OF_CHNL);
    Long address = getLong(AIPage.KEY_START_REG);
    RegisterDataType dataType = (RegisterDataType) wizardData.get(AIPage.KEY_DATA_TYPE);
    Long pollingRate = getLong(AIPage.KEY_POLLING_RATE);

    IChannel[] chans = createChannels(name, number);

    int addrOffset = dataType.getBytes()/REGISTER_SIZE;
    
    for (int i = 0; i < number; i++) {
      chans[i].setParameter(MMBIoChannel.PARAM_REG_TYPE, registerType);
      chans[i].setParameter(MMBIoChannel.PARAM_REG_ADDRESS, address);
      chans[i].setParameter(MMBIoChannel.PARAM_DATA_TYPE, dataType);
      chans[i].setParameter(MMBIoChannel.PARAM_POLL_RATE, pollingRate);
      
      address += addrOffset;
    }

    return chans;
  }

  private Integer getInt(String key) {
    return Integer.valueOf((String)wizardData.get(key));
  }
  
  private Long getLong(String key) {
    return Long .valueOf((String)wizardData.get(key));
  }

  private IChannel[] createDI() {
    RegisterType registerType = (RegisterType) wizardData.get(AIPage.KEY_REGISTER_TYPE); 
    String name = (String) wizardData.get(DIPage.KEY_CHANNEL_NAME);      
    Integer number = getInt(DIPage.KEY_NUMBER_OF_CHNL);
    Long address = getLong(DIPage.KEY_START_REG);
    Long pollingRate = getLong(DIPage.KEY_POLLING_RATE);
    MMBIoBitMask bitMask = (MMBIoBitMask) wizardData.get(DIPage.KEY_BIT_MASK);
    
    IChannel[] chans = createChannels(name, number);
    for (int i = 0; i < number; i++) {
      chans[i].setParameter(MMBIoChannel.PARAM_REG_TYPE, registerType);
      chans[i].setParameter(MMBIoChannel.PARAM_REG_ADDRESS, address);
      chans[i].setParameter(MMBIoChannel.PARAM_POLL_RATE, pollingRate);

      if (registerType == RegisterType.HOLDING_REGISTER
          || registerType == RegisterType.INPUT_REGISTER) {
        chans[i].setParameter(MMBIoChannel.PARAM_BIT_MASK, bitMask);
      } 
      
      address += 1;
    }

    return chans;
  }

  private IChannel[] createDO() {
    RegisterType registerType = (RegisterType) wizardData.get(AIPage.KEY_REGISTER_TYPE); 
    String name = (String) wizardData.get(DOPage.KEY_CHANNEL_NAME);      
    Integer number = getInt(DOPage.KEY_NUMBER_OF_CHNL);
    Long address = getLong(DOPage.KEY_START_REG);
    
    IChannel[] chans = createChannels(name, number);
    for (int i = 0; i < number; i++) {
      chans[i].setParameter(MMBIoChannel.PARAM_REG_TYPE, registerType);
      chans[i].setParameter(MMBIoChannel.PARAM_REG_ADDRESS, address);
      
      address += 1;
    }

    return chans;
  }

  private IChannel[] createChannels(final String name, int number) {
    IChannel[] newChnls = new  IChannel[number];
    
    String chnlName;
    
    for (int i = 0; i < number; i++) {
      if (number > 1) {
        chnlName = name + " " + (i + 1);
      } else {
        chnlName = name;
      }
      
      newChnls[i] = iomap.addChannel(type, chnlName);
    }
    
    return newChnls;
  }
}
