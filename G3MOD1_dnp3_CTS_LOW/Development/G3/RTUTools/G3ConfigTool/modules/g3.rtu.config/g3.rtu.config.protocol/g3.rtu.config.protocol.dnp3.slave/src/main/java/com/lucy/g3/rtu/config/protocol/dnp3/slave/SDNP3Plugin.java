/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3IoMap;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3ChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3ChannelPage;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3IoMapPage;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3SessionPage;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class SDNP3Plugin implements IConfigPlugin, IPageFactory, IProtocolFactory{
  private SDNP3Plugin () {}
  private final static SDNP3Plugin  INSTANCE = new SDNP3Plugin();
  
  public static void init() {
    PageFactories.registerFactory(PageFactories.KEY_SDNP3, INSTANCE);
    
    ProtocolFactories.registerFactory(ProtocolType.SDNP3, INSTANCE);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof SDNP3) {
      return new SDNP3ChannelManagerPage((SDNP3) data);

    } else if (data instanceof SDNP3Channel) {
      IConfig config = ((SDNP3Channel) data).getProtocol().getManager().getOwner();
      return new SDNP3ChannelPage((SDNP3Channel) data, config);

    } else if (data instanceof SDNP3Session) {
      return new SDNP3SessionPage((SDNP3Session) data);

    } else if (data instanceof SDNP3IoMap) {
      IConfig config = ((SDNP3IoMap) data).getSession().getProtocolChannel().getProtocol().getManager().getOwner();
      ScadaProtocolManager scada = config.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
      return new SDNP3IoMapPage((SDNP3IoMap) data, scada.getSourceRepository());
    } 
    
    return null;
  }

  @Override
  public IScadaProtocol createProtocol() {
    return new SDNP3();
  }
  
}

