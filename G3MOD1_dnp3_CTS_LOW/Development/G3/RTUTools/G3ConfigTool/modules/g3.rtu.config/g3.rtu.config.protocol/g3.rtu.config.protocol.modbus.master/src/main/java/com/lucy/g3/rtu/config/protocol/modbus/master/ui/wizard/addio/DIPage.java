/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterType;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
public class DIPage extends AbstractPage {
  public static final String KEY_REGISTER_TYPE   = AbstractPage.IKEY_REGISTER_TYPE   ;
  public static final String KEY_CHANNEL_NAME    = AbstractPage.IKEY_CHANNEL_NAME    ;
  public static final String KEY_NUMBER_OF_CHNL  = AbstractPage.IKEY_NUMBER_OF_CHNL  ;
  public static final String KEY_START_REG       = AbstractPage.IKEY_START_REG       ;
  public static final String KEY_POLLING_RATE    = AbstractPage.IKEY_POLLING_RATE    ;
  public static final String KEY_BIT_MASK        = AbstractPage.IKEY_BIT_MASK        ;
  
  private JComboBox<?> combRegType;
  private JComboBox<?> combBitNumber;
  
  public DIPage() {
    super(ChannelType.DIGITAL_INPUT);
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);
    
    builder.append("Register Type:", combRegType = createRegTypeCombobox(type), true);
    builder.append("Bit Number:", combBitNumber = createBitMaskCombox(type), true);
    builder.append("Base Channel Name:", createChannelNameField(), true);
    builder.append("Number Of Channels:", createNumField(), true);
    builder.append("Starting Register:", createAddressField(), true);
    builder.append("Polling Rate:", createPollingRateField(), new JLabel(Units.MS));
    
    /*Disable bit number */
    combRegType.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateBitNumberEnableState();
      }
    });
    updateBitNumberEnableState();
  }
  
  
  private void updateBitNumberEnableState() {
    RegisterType regType = (RegisterType) combRegType.getSelectedItem();
    combBitNumber.setEnabled(regType == RegisterType.HOLDING_REGISTER
        ||regType == RegisterType.INPUT_REGISTER);
  }

}
