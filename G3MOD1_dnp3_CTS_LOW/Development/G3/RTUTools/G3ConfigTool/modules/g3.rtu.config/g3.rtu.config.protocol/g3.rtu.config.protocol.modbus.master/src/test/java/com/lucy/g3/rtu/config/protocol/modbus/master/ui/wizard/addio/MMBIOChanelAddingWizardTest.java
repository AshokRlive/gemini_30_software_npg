/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio.MMBIOChannelAddingWizard;


public class MMBIOChanelAddingWizardTest {
  public static void main(String[] args) throws Throwable{
    
    SwingUtilities.invokeAndWait(new Runnable() {
      
      @Override
      public void run() {
        try {
          UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
            | UnsupportedLookAndFeelException e) {
          e.printStackTrace();
        }
        
        MMBIOChannelAddingWizard.showWizard(new MMBIoMap(null), ChannelType.ANALOG_INPUT);
        MMBIOChannelAddingWizard.showWizard(new MMBIoMap(null), ChannelType.DIGITAL_INPUT);
        MMBIOChannelAddingWizard.showWizard(new MMBIoMap(null), ChannelType.DIGITAL_OUTPUT);
      }
    });
  }
}

