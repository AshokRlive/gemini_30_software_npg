/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.validate;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The abstract validator for validating protocol.
 */
public class ProtocolSessionValidator extends AbstractContainerValidator<IProtocolSession> {

  public ProtocolSessionValidator(IProtocolSession target) {
    super(target);
  }

  @Override
  public Collection<IValidation> getValidatableChildrenItems() {
    ArrayList<IValidation> items = new ArrayList<IValidation>();
    items.add(target.getIomap());

    return items;
  }
  

  @Override
  public String getTargetName() {
    return target.getSessionName();
  }
}
