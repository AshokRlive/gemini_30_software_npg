/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.manager;

import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.AbstractProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * The implementation of {@linkplain MasterProtocolManager}.
 */
public final class MasterProtocolManagerImpl extends AbstractProtocolManager<IMasterProtocol<?>>
    implements MasterProtocolManager {
  
  public MasterProtocolManagerImpl(IConfig owner) {
    super(owner);
  }

  @Override
  public IMasterProtocol<?> getModbusMaster() {
    Collection<IMasterProtocol<?>> protocols = getAllItems();
    for (IMasterProtocol<?> proto : protocols) {
      if (proto.getProtocolType() == ProtocolType.MMB) {
        return proto;
      }
    }
    return null;
  }

  @Override
  public String getName() {
    return "Master Protocol Manager";
  }

}
