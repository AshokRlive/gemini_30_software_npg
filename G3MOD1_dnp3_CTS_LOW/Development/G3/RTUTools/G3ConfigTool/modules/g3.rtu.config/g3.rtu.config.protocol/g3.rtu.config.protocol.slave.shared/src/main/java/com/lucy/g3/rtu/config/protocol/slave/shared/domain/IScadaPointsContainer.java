/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import java.util.Collection;

/**
 * The class that implements this interface manages a collection of
 * {@linkplain ScadaPoint}.
 */
public interface IScadaPointsContainer {

  Collection<ScadaPoint> getAllInputPoints();

  Collection<ScadaPoint> getAllPoints();

  void removeAllPoints(Collection<ScadaPoint> scadapoints);
}
