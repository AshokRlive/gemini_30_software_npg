/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.jdesktop.application.Application;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.SelectionDialog;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * Abstract class that implements IOMap copy functionality.
 */
public abstract class AbstractIoMapPage extends AbstractConfigPage {

  private static final String ACTION_COPY = "copyIoMap";
      
  private final IIoMap iomap;

  private final Action[] contextActions;
  
  public AbstractIoMapPage(IIoMap iomap, String title) {
    super(iomap, title);
    this.iomap = Preconditions.checkNotNull(iomap, "iomap must not be null");
    setNodeName(IIoMap.DEFAULT_NAME);
    
    contextActions = new Action[]{getActionCopy()}; 
  }

  @Override
  public Action[] getContextActions() {
    return contextActions;
  }
  
  protected Action getActionCopy() {
    return Application.getInstance().getContext().getActionMap(AbstractIoMapPage.class, this)
        .get(ACTION_COPY);
  }
  
  @Override
  public Icon getTitleIcon() {
    return null;
  }

  @org.jdesktop.application.Action
  public void copyIoMap() {
    ArrayList<IProtocolSession> copyToList = getOtherSessions();
    JFrame parent = WindowUtils.getMainFrame();
    SelectionDialog<IProtocolSession> selectDialog = new SelectionDialog<>(parent, copyToList, "Target Session");
    selectDialog.setItemRender(new DefaultTableCellRenderer() {
      @Override
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
          int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (value != null)
          setText(String.format("[%s] %s - %s",
              ((IProtocolSession)value).getProtocolType().getName(),
              ((IProtocolSession)value).getProtocolChannel().getChannelName(),
              ((IProtocolSession)value).getSessionName()));
        return comp;
      }
    });
    selectDialog.setTitle("Copying IO Map between sessions");
    selectDialog.setMessage(String.format("Copy the IO map of \"%s\" to the following selected sessions:", 
        iomap.getSession().getSessionName()));
    selectDialog.pack();
    selectDialog.setVisible(true);
    ArrayList<IProtocolSession> selections = selectDialog.getSelections();
    
    if(selectDialog.hasBeenAffirmed()) {
      if (selections != null && !selections.isEmpty()) {
        int ret = JOptionPane.showConfirmDialog(parent, 
            "The existing points in the selected sessions will be removed!\nDo you really want to continue?", 
            "Warning", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        
        if(ret == JOptionPane.YES_OPTION) {
          for (IProtocolSession selection : selections) {
            selection.getIomap().copyFrom(iomap);
          }
          
          JOptionPane.showMessageDialog(parent, 
              "Copying IO Map has been completed successfully!",
              "Succeess", 
              JOptionPane.INFORMATION_MESSAGE);
        }
        
      } else {
        JOptionPane.showMessageDialog(parent, 
            "No session has been selected!",
            "Failure", 
            JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * Gets sessions other then this.
   */
  protected ArrayList<IProtocolSession> getOtherSessions() {
    ArrayList<IProtocolSession> others = new ArrayList<>();
    IProtocolChannel<? extends IProtocolSession> chnl = iomap.getSession().getProtocolChannel();
    if(chnl != null) {
      IProtocol<?> protocol = chnl.getProtocol();
      Collection<? extends IProtocolSession> allSesns;
      if(protocol != null)
        allSesns = protocol.getAllSessions(); // sessions from all channels.
      else
        allSesns = chnl.getAllItems(); // sessions from current channel.
      
      others.addAll(allSesns);
      others.remove(iomap.getSession());
    }
    return others;
  }
  
  @Override
  public String toString() {
    return iomap.toString();
  }
}
