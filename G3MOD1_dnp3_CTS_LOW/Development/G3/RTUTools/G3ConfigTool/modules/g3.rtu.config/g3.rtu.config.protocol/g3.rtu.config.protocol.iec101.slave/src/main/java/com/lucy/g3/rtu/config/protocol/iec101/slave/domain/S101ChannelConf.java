/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.domain;

import com.g3schema.ns_s101.S101ChlConfT;
import com.g3schema.ns_s101.S101LinkLayerConfT;
import com.g3schema.ns_s101.S101LinuxIoT;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870ChannelConf;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;
import com.lucy.g3.xml.gen.common.IEC870Enum.LINKCNFM;
import com.lucy.g3.xml.gen.common.IEC870Enum.LINK_MODE;

/**
 * The configuration of {@link S101Channel}.
 */
public class S101ChannelConf extends IEC870ChannelConf {

  public static final String TYPE_SERIAL = "Serial";
  
  // ====== Link Layer ======
  public static final String PROPERTY_LINK_ADDR_SIZE = "linkAddressSize";
  public static final String PROPERTY_LINK_MODE = "linkMode";
  public static final String PROPERTY_ONECHAR_ACK_ALLOWED = "oneCharAckAllowed";
  public static final String PROPERTY_ONECHAR_RESP_ALLOWED = "oneCharRespAllowed";
  public static final String PROPERTY_RX_FRAME_TIMEOUT = "rxFrameTimeout";
  public static final String PROPERTY_CONFIRM_MODE = "confirmMode";
  public static final String PROPERTY_CONFIRM_TIMEOUT = "confirmTimeout";
  public static final String PROPERTY_MAX_RETRIES = "maxRetries";
  public static final String PROPERTY_TEST_FRAME_PERIOD = "testFramePeriod";
  public static final String PROPERTY_OFFLINE_POLL_PERIOD = "offlinePollPeriod";

  // @formatter:off
  private long linkAddressSize       = (short) getDefault("linkAddressSize");
  private LINK_MODE linkMode          = LINK_MODE.LINK_MODE_UNBALANCED;
  private boolean oneCharAckAllowed   = false;
  private boolean oneCharRespAllowed  = false;
  private long rxFrameTimeout         = getDefault("rxFrameTimeout");
  private LINKCNFM confirmMode        = LINKCNFM.LINKCNFM_ALWAYS;
  private long confirmTimeout         = getDefault("confirmTimeout");
  private long maxRetries            = (short) getDefault("maxRetries");
  private long testFramePeriod        = getDefault("testFramePeriod");
  private long offlinePollPeriod      = getDefault("offlinePollPeriod");
  // @formatter:on

  private final S101ChannelSerialConf serialConfig;


  public S101ChannelConf(S101Channel channel, String channelName) {
    super(channel, channelName);
    this.serialConfig = new S101ChannelSerialConf(channel);

    //LoggingUtil.logPropertyChanges(this);
  }

  public long getOfflinePollPeriod() {
    return offlinePollPeriod;
  }

  public void setOfflinePollPeriod(long offlinePollPeriod) {
    Object oldValue = this.offlinePollPeriod;
    this.offlinePollPeriod = offlinePollPeriod;
    firePropertyChange(PROPERTY_OFFLINE_POLL_PERIOD, oldValue, offlinePollPeriod);
  }

  @Override
  public String getChannelConfSummaryText() {
    return getSerialConfigAsString();
  }

  @Override
  public S101ChannelSerialConf getSerialConf() {
    return serialConfig;
  }

  public long getLinkAddressSize() {
    return linkAddressSize;
  }

  public void setLinkAddressSize(long linkAdddressSize) {
    Object oldValue = this.linkAddressSize;
    this.linkAddressSize = linkAdddressSize;
    firePropertyChange(PROPERTY_LINK_ADDR_SIZE, oldValue, linkAdddressSize);
  }

  public LINK_MODE getLinkMode() {
    return linkMode;
  }

  public void setLinkMode(LINK_MODE linkMode) {
    if(linkMode == null) {
      return;
    }
    
    Object oldValue = this.linkMode;
    this.linkMode = linkMode;
    firePropertyChange(PROPERTY_LINK_MODE, oldValue, linkMode);
  }

  public boolean isOneCharAckAllowed() {
    return oneCharAckAllowed;
  }

  public void setOneCharAckAllowed(boolean oneCharAckAllowed) {
    Object oldValue = this.oneCharAckAllowed;
    this.oneCharAckAllowed = oneCharAckAllowed;
    firePropertyChange(PROPERTY_ONECHAR_ACK_ALLOWED, oldValue, oneCharAckAllowed);
  }

  public boolean isOneCharRespAllowed() {
    return oneCharRespAllowed;
  }

  public void setOneCharRespAllowed(boolean oneCharRespAllowed) {
    Object oldValue = this.oneCharRespAllowed;
    this.oneCharRespAllowed = oneCharRespAllowed;
    firePropertyChange(PROPERTY_ONECHAR_RESP_ALLOWED, oldValue, oneCharRespAllowed);
  }

  public long getRxFrameTimeout() {
    return rxFrameTimeout;
  }

  public void setRxFrameTimeout(long rxFrameTimeout) {
    Object oldValue = this.rxFrameTimeout;
    this.rxFrameTimeout = rxFrameTimeout;
    firePropertyChange(PROPERTY_RX_FRAME_TIMEOUT, oldValue, rxFrameTimeout);
  }

  public LINKCNFM getConfirmMode() {
    return confirmMode;
  }

  public void setConfirmMode(LINKCNFM confirmMode) {
    Object oldValue = this.confirmMode;
    this.confirmMode = confirmMode;
    firePropertyChange(PROPERTY_CONFIRM_MODE, oldValue, confirmMode);
  }

  public long getConfirmTimeout() {
    return confirmTimeout;
  }

  public void setConfirmTimeout(long confirmTimeout) {
    Object oldValue = this.confirmTimeout;
    this.confirmTimeout = confirmTimeout;
    firePropertyChange(PROPERTY_CONFIRM_TIMEOUT, oldValue, confirmTimeout);
  }

  public long getMaxRetries() {
    return maxRetries;
  }

  public void setMaxRetries(long maxRetries) {
    Object oldValue = this.maxRetries;
    this.maxRetries = maxRetries;
    firePropertyChange(PROPERTY_MAX_RETRIES, oldValue, maxRetries);
  }

  public long getTestFramePeriod() {
    return testFramePeriod;
  }

  public void setTestFramePeriod(long testFramePeriod) {
    Object oldValue = this.testFramePeriod;
    this.testFramePeriod = testFramePeriod;
    firePropertyChange(PROPERTY_TEST_FRAME_PERIOD, oldValue, testFramePeriod);
  }

  public void writeToXML(S101ChlConfT xml) {
    /* General */
    xml.chnlName.setValue(getChannelName());
    xml.enabled.setValue(getChannel().isEnabled());
    xml.incrementalTimeoutMs.setValue(getIncrementalTimeout());

    /* Link layer */
    S101LinkLayerConfT xmlLink = xml.linkLayer.append();
    xmlLink.txFrameSize.setValue(getTxFrameSize());
    xmlLink.rxFrameSize.setValue(getRxFrameSize());
    xmlLink.linkAddressSize.setValue(getLinkAddressSize());
    xmlLink.linkMode.setValue(getLinkMode().getValue());
    xmlLink.oneCharAckAllowed.setValue(isOneCharAckAllowed());
    xmlLink.oneCharResponseAllowed.setValue(isOneCharRespAllowed());
    xmlLink.rxFrameTimeout.setValue(getRxFrameTimeout());
    xmlLink.confirmMode.setValue(getConfirmMode().getValue());
    xmlLink.confirmTimeout.setValue(getConfirmTimeout());
    xmlLink.maxRetries.setValue(getMaxRetries());
    xmlLink.testFramePeriod.setValue(getTestFramePeriod());
    xmlLink.offlinePollPeriod.setValue(getOfflinePollPeriod());

    /* Linux IO */
    S101LinuxIoT linuxIO = xml.linuxIO.append();
    linuxIO.firstCharWait.setValue(getSerialConf().getFirstCharWait());
    getSerialConf().writeToXML(linuxIO.serial.append(), null);

  }

  public void readFromXML(S101ChlConfT xml, PortsManager portMgr) {
    if (xml.enabled.exists()) {
      getChannel().setEnabled(xml.enabled.getValue());
    }

    /* General */
    setChannelName(xml.chnlName.getValue());
    setIncrementalTimeout(xml.incrementalTimeoutMs.getValue());

    /* Link layer */
    S101LinkLayerConfT xmlLink = xml.linkLayer.first();
    setTxFrameSize(xmlLink.txFrameSize.getValue());
    setRxFrameSize(xmlLink.rxFrameSize.getValue());
    setLinkAddressSize((short) xmlLink.linkAddressSize.getValue());
    setLinkMode(LINK_MODE.forValue((int) xmlLink.linkMode.getValue()));
    setOneCharAckAllowed(xmlLink.oneCharAckAllowed.getValue());
    setOneCharRespAllowed(xmlLink.oneCharResponseAllowed.getValue());
    setRxFrameTimeout(xmlLink.rxFrameTimeout.getValue());
    setConfirmMode(LINKCNFM.forValue((int) xmlLink.confirmMode.getValue()));
    setConfirmTimeout(xmlLink.confirmTimeout.getValue());
    setMaxRetries((short) xmlLink.maxRetries.getValue());
    setTestFramePeriod(xmlLink.testFramePeriod.getValue());
    setOfflinePollPeriod(xmlLink.offlinePollPeriod.getValue());

    /* Linux IO */
    getSerialConf().setFirstCharWait(xml.linuxIO.first().firstCharWait.getValue());
    getSerialConf().readFromXML(xml.linuxIO.first().serial.first(), portMgr);
  }

  @Override
  public ProtocolChannelTCPConf getTcpConf() {
    return null;
  }

  @Override
  public IEC870Constraints getConstraints() {
    return IEC870Constraints.INSTANCE_101;
  }

  @Override
  public Object getLinkType() {
    return TYPE_SERIAL;
  }

  @Override
  public void setLinkType(Object type) {
    // No effect
  }
}
