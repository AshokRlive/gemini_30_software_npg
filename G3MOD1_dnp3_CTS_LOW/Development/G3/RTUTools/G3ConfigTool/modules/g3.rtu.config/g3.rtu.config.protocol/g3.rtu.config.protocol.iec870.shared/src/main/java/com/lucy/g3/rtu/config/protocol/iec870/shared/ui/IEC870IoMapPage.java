/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui;

import javax.swing.table.TableColumn;

import org.jdesktop.swingx.JXTable;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points.IOAEditor;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.AbstractIoMapPageModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.AbstractScadaIoMapPage;

/**
 * The page of IEC870 IO map.
 */
public class IEC870IoMapPage extends AbstractScadaIoMapPage<IEC870Point>  {


  public IEC870IoMapPage(IScadaIoMap<IEC870Point> iomap, ScadaPointSourceRepository sourceRepo) {
    super("IEC101-104 Protocol Points", iomap, sourceRepo, IEC870PointType.values());
  }
  

  @Override
  protected AbstractIoMapPageModel<IEC870Point>[] createModels(IScadaIoMap<IEC870Point> iomap,
      ScadaPointType[] pointTypes, ScadaPointSourceRepository resource) {
    IEC870IoMapPageModel[] model = new IEC870IoMapPageModel[pointTypes.length];
    for (int i = 0; i < model.length; i++) {
      model[i] = new IEC870IoMapPageModel(iomap, (IEC870PointType)pointTypes[i], resource);
    }
    return model;
  }
  

  @Override
  protected JXTable buildPointTable(AbstractIoMapPageModel<IEC870Point> pm) {

    JXTable table = super.buildPointTable(pm);
    
    // Set IOA editor and renderer
    TableColumn colIoa = table.getColumn(IEC870IoMapPageModel.COLUMN_IOA);
    colIoa.setCellRenderer(IOAEditor.createCellRenderer());
    colIoa.setCellEditor(IOAEditor.createCellEditor());
    colIoa.setHeaderValue("IOA");
    
    return table;
  }


}
