/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultFormatterFactory;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBConstraints;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.ui.wizard.ProtocolAddingWizards;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * The page of ModBus Master Session.
 */
public class MMBSessionPage extends AbstractConfigPage {
  private static final String ACTION_APPLY_TEMPLATE = "applyTemplate";

  private final MMBSession session;

  private final ArrayListModel<MMBIoMap> iomapListModel;
  
  private final javax.swing.Action[] contextActions;


  public MMBSessionPage(MMBSession sesn) {
    super(sesn, "ModBus Session");

    this.session = Preconditions.checkNotNull(sesn, "mbmSesn is null");
    iomapListModel = new ArrayListModel<MMBIoMap>(1);
    iomapListModel.add(sesn.getIomap());
    
    javax.swing.Action applyTempl = Application.getInstance().getContext().getActionManager()
        .getActionMap(MMBSessionPage.class, this).get(ACTION_APPLY_TEMPLATE);
    contextActions = new javax.swing.Action[]{applyTempl};

    // Bind the device name to tree node and page title
    PropertyConnector.connect(sesn, MMBSession.PROPERTY_SESSION_NAME, this, PROPERTY_NODE_NAME).updateProperty2();
    PropertyConnector.connect(sesn, MMBSession.PROPERTY_SESSION_NAME, this, PROPERTY_TITLE).updateProperty2();
    IMasterProtocolModule dev = sesn.getDevice();
    if (dev != null) {
      setNodeIcon(dev.getIcons().getThumbnail());
    }
  }

  @Override
  public javax.swing.Action[] getContextActions() {
    if(session.isTemplate())
      return null;
    else
      return contextActions;
  }

  @Action
  public void applyTemplate() {
    if(ProtocolAddingWizards.showGenerateIoMap(session, "Applying Session Template", "Choose Template")) {
      JOptionPane.showMessageDialog(WindowUtils.getMainFrame(), "Applied template successfully!", 
          "Success", JOptionPane.INFORMATION_MESSAGE);
    }
  }

  @Override
  protected void init() throws Exception {
    initComponents();
    setBoundaries();
    initComponentsBinding();
    installCommitOnType();

    // Hide components not included by template session
    if (session.isTemplate()) {
      lblSlaveAddress.setVisible(false);
      ftfSlaveAddress.setVisible(false);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public ListModel<MMBIoMap> getChildrenDataList() {
    return iomapListModel;
  }

  private void initComponentsBinding() {
    PresentationModel<MMBSession> pmSesn = new PresentationModel<>(session);
    Bindings.bind(tfDeviceName, pmSesn.getModel(MMBSession.PROPERTY_DEVICE_NAME));
    Bindings.bind(tfDeviceModel, pmSesn.getModel(MMBSession.PROPERTY_DEVICE_MODEL));
    Bindings.bind(tfClassifier,  pmSesn.getModel(MMBSession.PROPERTY_CLASSIFIER));
    Bindings.bind(lblDeviceId, pmSesn.getModel(MMBSession.PROPERTY_DEVICE_ID));
    Bindings.bind(lblDeviceType, pmSesn.getModel(MMBSession.PROPERTY_DEVICE_TYPE));

    Bindings.bind(ftfSlaveAddress, pmSesn.getModel(MMBSession.PROPERTY_SLAVEADDRESS));
    Bindings.bind(ftfRepTimeout, pmSesn.getModel(MMBSession.PROPERTY_DEF_RESP_TIMEOUT));
    Bindings.bind(tfNumberOfRetries, pmSesn.getModel(MMBSession.PROPERTY_NUM_RETRIES));
    Bindings.bind(tfOnlinePollPeriod, pmSesn.getModel(MMBSession.PROPERTY_OFFLINE_POLL_PERIOD));
    Bindings.bind(cbByteSwap, pmSesn.getModel(MMBSession.PROPERTY_BYTE_SWAP));
    Bindings.bind(cbWordSwap, pmSesn.getModel(MMBSession.PROPERTY_WORD_SWAP));
  }

  private void panelMouseClicked(MouseEvent e) {
    ((JPanel) e.getSource()).requestFocus();
  }

  private void createUIComponents() {
    sessionConfigPane = new JXTaskPaneContainer();
    sessionConfigPane.add(panelDevice = new JXTaskPane("Device Settings"));
    sessionConfigPane.add(panelSession = new JXTaskPane("Session Settings"));
  }

  private void installCommitOnType() {
    FormattedTextFieldSupport.installCommitOnType(ftfSlaveAddress);
    FormattedTextFieldSupport.installCommitOnType(ftfRepTimeout);
  }

  private void setBoundary(JFormattedTextField ftf, String prefix, String property) {
    ftf.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(
            MMBConstraints.INSTANCE.getMin(prefix, property),
            MMBConstraints.INSTANCE.getMax(prefix, property))));
  }

  private void setBoundaries() {
    setBoundary(ftfSlaveAddress, MMBConstraints.PREFIX_SESSION, MMBSession.PROPERTY_SLAVEADDRESS);
    setBoundary(ftfRepTimeout, MMBConstraints.PREFIX_SESSION, MMBSession.PROPERTY_DEF_RESP_TIMEOUT);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    JScrollPane scrollPane2 = new JScrollPane();
    JLabel label1 = new JLabel();
    lblDeviceType = new JLabel();
    JLabel label2 = new JLabel();
    lblDeviceId = new JLabel();
    JLabel label11 = new JLabel();
    tfDeviceModel = new JTextField();
    lblDevName = new JLabel();
    tfDeviceName = new JTextField();
    label4 = new JLabel();
    tfClassifier = new JTextField();
    JPanel panel1 = new JPanel();
    cbByteSwap = new JCheckBox();
    cbWordSwap = new JCheckBox();
    lblSlaveAddress = new JLabel();
    ftfSlaveAddress = new JFormattedTextField();
    label7 = new JLabel();
    ftfRepTimeout = new JFormattedTextField();
    label8 = new JLabel();
    label3 = new JLabel();
    tfOnlinePollPeriod = new JFormattedTextField();
    label9 = new JLabel();
    label10 = new JLabel();
    tfNumberOfRetries = new JFormattedTextField();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane2 ========
    {
      scrollPane2.setBorder(null);

      //---- sessionConfigPane ----
      sessionConfigPane.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          panelMouseClicked(e);
        }
      });
      scrollPane2.setViewportView(sessionConfigPane);
    }
    add(scrollPane2, BorderLayout.CENTER);

    //======== panelDevice ========
    {
      panelDevice.setLayout(new FormLayout(
          "default, $lcgap, [150dlu,default]",
          "3*(fill:default, $lgap), default, $rgap, default, $pgap, $lgap, default"));

      //---- label1 ----
      label1.setText("Device Type:");
      panelDevice.add(label1, CC.xy(1, 1));
      panelDevice.add(lblDeviceType, CC.xy(3, 1));

      //---- label2 ----
      label2.setText("Device ID:");
      panelDevice.add(label2, CC.xy(1, 3));
      panelDevice.add(lblDeviceId, CC.xy(3, 3));

      //---- label11 ----
      label11.setText("Device Model:");
      panelDevice.add(label11, CC.xy(1, 5));
      panelDevice.add(tfDeviceModel, CC.xy(3, 5));

      //---- lblDevName ----
      lblDevName.setText("Device Name:");
      panelDevice.add(lblDevName, CC.xy(1, 7));
      panelDevice.add(tfDeviceName, CC.xy(3, 7));

      //---- label4 ----
      label4.setText("Classifier:");
      panelDevice.add(label4, CC.xy(1, 9));
      panelDevice.add(tfClassifier, CC.xy(3, 9));

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Endianness"));
        panel1.setOpaque(false);
        panel1.setLayout(new FormLayout(
            "default, $lcgap, [100dlu,default]",
            "default"));

        //---- cbByteSwap ----
        cbByteSwap.setText("Byte Swap");
        cbByteSwap.setOpaque(false);
        panel1.add(cbByteSwap, CC.xy(1, 1));

        //---- cbWordSwap ----
        cbWordSwap.setText("Word Swap");
        cbWordSwap.setOpaque(false);
        panel1.add(cbWordSwap, CC.xy(3, 1));
      }
      panelDevice.add(panel1, CC.xywh(1, 12, 3, 1));
    }

    //======== panelSession ========
    {
      panelSession.setLayout(new FormLayout(
          "default, $lcgap, [50dlu,default], $lcgap, default",
          "3*(default, $lgap), default"));

      //---- lblSlaveAddress ----
      lblSlaveAddress.setText("Slave Address:");
      panelSession.add(lblSlaveAddress, CC.xy(1, 1));
      panelSession.add(ftfSlaveAddress, CC.xy(3, 1));

      //---- label7 ----
      label7.setText("Default Response Timeout:");
      panelSession.add(label7, CC.xy(1, 3));
      panelSession.add(ftfRepTimeout, CC.xy(3, 3));

      //---- label8 ----
      label8.setText("ms");
      panelSession.add(label8, CC.xy(5, 3));

      //---- label3 ----
      label3.setText("Offline Poll Period:");
      panelSession.add(label3, CC.xy(1, 5));
      panelSession.add(tfOnlinePollPeriod, CC.xy(3, 5));

      //---- label9 ----
      label9.setText("ms");
      panelSession.add(label9, CC.xy(5, 5));

      //---- label10 ----
      label10.setText("Number of Retries:");
      panelSession.add(label10, CC.xy(1, 7));
      panelSession.add(tfNumberOfRetries, CC.xy(3, 7));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTaskPaneContainer sessionConfigPane;
  private JPanel panelDevice;
  private JLabel lblDeviceType;
  private JLabel lblDeviceId;
  private JTextField tfDeviceModel;
  private JLabel lblDevName;
  private JTextField tfDeviceName;
  private JLabel label4;
  private JTextField tfClassifier;
  private JCheckBox cbByteSwap;
  private JCheckBox cbWordSwap;
  private JPanel panelSession;
  private JLabel lblSlaveAddress;
  private JFormattedTextField ftfSlaveAddress;
  private JLabel label7;
  private JFormattedTextField ftfRepTimeout;
  private JLabel label8;
  private JLabel label3;
  private JFormattedTextField tfOnlinePollPeriod;
  private JLabel label9;
  private JLabel label10;
  private JFormattedTextField tfNumberOfRetries;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
