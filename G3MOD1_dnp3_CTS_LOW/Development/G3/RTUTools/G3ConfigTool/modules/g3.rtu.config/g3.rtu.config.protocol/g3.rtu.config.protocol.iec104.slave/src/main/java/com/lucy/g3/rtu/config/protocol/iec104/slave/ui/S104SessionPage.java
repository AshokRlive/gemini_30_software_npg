/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec104.slave.ui;

import java.awt.BorderLayout;
import java.text.NumberFormat;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;

import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels.*;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Session;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.EventsConfig;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels.EventPanelAI;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels.EventPanelDI;
import com.lucy.g3.rtu.config.protocol.shared.ui.panels.HintPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_TIMETGA_MODE;

/**
 * The page for configuring IEC104-slave session.
 */
public class S104SessionPage extends AbstractConfigPage {

  private final PresentationModel<S104Session> pm;

  private final ArrayListModel<IEC870IoMap> ioMapListModel;


  public S104SessionPage(S104Session iec104Session) {
    super(iec104Session, "IEC104 Session");
    this.pm = new PresentationModel<S104Session>(iec104Session);
    this.ioMapListModel = new ArrayListModel<IEC870IoMap>(1);
    this.ioMapListModel.add(iec104Session.getIomap());

    // Bind node name to session name.
    ValueModel vm = pm.getModel(S104Session.PROPERTY_SESSION_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }

  @Override
  protected void init() throws Exception {
    initComponents();
    populate();
  }

  @SuppressWarnings("unchecked")
  @Override
  public ListModel<IEC870IoMap> getChildrenDataList() {
    return ioMapListModel;
  }

  private void createUIComponents() {
    ComponentsFactory factory = new ComponentsFactory(IEC870Constraints.INSTANCE_104, IEC870Constraints.PREFIX_SESSION, pm);


    tfSessionName = factory.createTextField(IEC870Session.PROPERTY_SESSION_NAME);
    ftfASDUAddress = factory.createNumberField(IEC870Session.PROPERTY_ASDU_ADDRESS);
    ftfCyclicPeriod = factory.createNumberField(IEC870Session.PROPERTY_CYCLIC_PERIOD);
    ftfCyclicFirstPeriod = factory.createNumberField(IEC870Session.PROPERTY_CYCLIC_FIRST_PERIOD);
    ftfBackgroundPeriod = factory.createNumberField(IEC870Session.PROPERTY_BACKGROUND_PERIOD);
    ftfSelectTimeout = factory.createNumberField(IEC870Session.PROPERTY_SELECT_TIMEOUT);
    ftfDefResponseTimeout = factory.createNumberField(IEC870Session.PROPERTY_DEFAULT_RESPONSE_TIMEOUT);
    combCmdTimetagMode = factory.createComboBox(IEC870Session.PROPERTY_CMD_TIMETAG_MODE, LU_TIMETGA_MODE.values());

    cbSendClkSynEvents = factory.createCheckBox(IEC870Session.PROPERTY_SEND_CLOCK_SYNC_EVENTS);
    cbDelOldestEvent = factory.createCheckBox(IEC870Session.PROPERTY_DELETE_OLDEST_EVENT);
    cbCseUseActTerm = factory.createCheckBox(IEC870Session.PROPERTY_CSEUSEACTTERM);
    cbCmdUseActTerm = factory.createCheckBox(IEC870Session.PROPERTY_CMDUSEACTTERM);
    cbStrictAdherence = factory.createCheckBox(IEC870Session.PROPERTY_MULTIPLE_PMEN);
    cbUseDayOfWeek = factory.createCheckBox(IEC870Session.PROPERTY_USE_DAYOFWEEK);
    
    lblCOTSize = ComponentsFactory.createLabel(pm.getModel(S104Session.PROPERTY_COT_SIZE), NumberFormat.getInstance());
    lblASDUAddrSize = ComponentsFactory.createLabel(pm.getModel(S104Session.PROPERTY_ASDU_ADDR_SIZE), NumberFormat.getInstance());
    ftfObjAddrSize = factory.createNumberField(S104Session.PROPERTY_INFO_OBJ_ADDR_SIZE);
    
    ftfMaxCommandAge= factory.createNumberField(S104Session.PROPERTY_MAXCOMMANDAGE);
    ftfMaxCommandFuture= factory.createNumberField(S104Session.PROPERTY_MAXCOMMAND_FUTURE);
    ftfMaxAPDUSize = factory.createNumberField(S104Session.PROPERTY_MAX_APDU_SIZE);

    panelHint = new HintPanel(S104SessionPage.class);
    
    EventsConfig ain = (EventsConfig) pm.getValue(S104Session.PROPERTY_EVENTCONFIG_AIN);
    EventsConfig ais = (EventsConfig) pm.getValue(S104Session.PROPERTY_EVENTCONFIG_AIS);
    EventsConfig aif = (EventsConfig) pm.getValue(S104Session.PROPERTY_EVENTCONFIG_AIF);
    EventsConfig bi = (EventsConfig) pm.getValue(S104Session.PROPERTY_EVENTCONFIG_BI);
    EventsConfig dbi = (EventsConfig) pm.getValue(S104Session.PROPERTY_EVENTCONFIG_DBI);
    EventsConfig ct = (EventsConfig) pm.getValue(S104Session.PROPERTY_EVENTCONFIG_CT);
    panelEventAnlogInput = new EventPanelAI(ain, ais, aif);
    panelEventDigitalInput = new EventPanelDI(bi, dbi, ct);
    
    panelSessionTimeFormat = new IEC870SessionTimeFormatPanel(pm);
  }

  private void populate() {
    settingsPane.add(panelSessionGeneral);
    settingsPane.add(panelSessionFunctions);
    settingsPane.add(panelSessionMiscs);
    settingsPane.add(panelEventAnlogInput);
    settingsPane.add(panelEventDigitalInput);
    settingsPane.add(panelSessionTimeFormat);

    /* Set collapsed by default */
    ((JXTaskPane) panelEventAnlogInput).setCollapsed(true);
    ((JXTaskPane) panelEventDigitalInput).setCollapsed(true);
    ((JXTaskPane) panelSessionMiscs).setCollapsed(true);
    ((JXTaskPane) panelSessionTimeFormat).setCollapsed(true);
  }

  public JLabel getLabel9() {
    return label9;
  }

  public JLabel getLabel8() {
    return label8;
  }

  public JLabel getLabel6() {
    return label6;
  }

  public JLabel getLabel7() {
    return label7;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
	createUIComponents();

	tabbedPane1 = new JTabbedPane();
	JScrollPane scrollPane2 = new JScrollPane();
	settingsPane = new JXTaskPaneContainer();
	panelSessionMiscs = new JXTaskPane("Miscellaneous");
	labelAppConfirmTimeout = new JLabel();
	labelms4 = new JLabel();
	label6 = new JLabel();
	label7 = new JLabel();
	labelLinkStatusPeriod = new JLabel();
	labelms = new JLabel();
	label4 = new JLabel();
	labelms5 = new JLabel();
	label5 = new JLabel();
	labelms6 = new JLabel();
	panelSessionGeneral = new JXTaskPane("General");
	lblSessionName = new JLabel();
	labelsource = new JLabel();
	label10 = new JLabel();
	label11 = new JLabel();
	labeldest = new JLabel();
	label1 = new JLabel();
	label12 = new JLabel();
	label13 = new JLabel();
	label8 = new JLabel();
	labelms7 = new JLabel();
	label2 = new JLabel();
	label14 = new JLabel();
	label9 = new JLabel();
	labelms8 = new JLabel();
	label19 = new JLabel();
	panelSessionFunctions = new JXTaskPane("Functions");

	//======== this ========
	setLayout(new BorderLayout());
	add(panelHint, BorderLayout.PAGE_END);

	//======== tabbedPane1 ========
	{

		//======== scrollPane2 ========
		{
			scrollPane2.setBorder(null);
			scrollPane2.setViewportView(settingsPane);
		}
		tabbedPane1.addTab("Session Settings", scrollPane2);
	}
	add(tabbedPane1, BorderLayout.CENTER);

	//======== panelSessionMiscs ========
	{
		panelSessionMiscs.setLayout(new FormLayout(
			"right:default, $lcgap, [80dlu,default], $lcgap, default",
			"2*(default, $lgap), default, $pgap, default, $lgap, default"));

		//---- labelAppConfirmTimeout ----
		labelAppConfirmTimeout.setText("Cyclic Period:");
		panelSessionMiscs.add(labelAppConfirmTimeout, CC.xy(1, 1));

		//---- ftfCyclicPeriod ----
		ftfCyclicPeriod.setColumns(8);
		panelSessionMiscs.add(ftfCyclicPeriod, CC.xy(3, 1));

		//---- labelms4 ----
		labelms4.setText("ms");
		panelSessionMiscs.add(labelms4, CC.xy(5, 1));

		//---- label6 ----
		label6.setText("Cyclic First Period:");
		panelSessionMiscs.add(label6, CC.xy(1, 3));

		//---- ftfCyclicFirstPeriod ----
		ftfCyclicFirstPeriod.setColumns(8);
		panelSessionMiscs.add(ftfCyclicFirstPeriod, CC.xy(3, 3));

		//---- label7 ----
		label7.setText("ms");
		panelSessionMiscs.add(label7, CC.xy(5, 3));

		//---- labelLinkStatusPeriod ----
		labelLinkStatusPeriod.setText("Background Period:");
		panelSessionMiscs.add(labelLinkStatusPeriod, CC.xy(1, 5));

		//---- ftfBackgroundPeriod ----
		ftfBackgroundPeriod.setColumns(8);
		panelSessionMiscs.add(ftfBackgroundPeriod, CC.xy(3, 5));

		//---- labelms ----
		labelms.setText("ms");
		panelSessionMiscs.add(labelms, CC.xy(5, 5));

		//---- label4 ----
		label4.setText("Select Timeout:");
		panelSessionMiscs.add(label4, CC.xy(1, 7));
		panelSessionMiscs.add(ftfSelectTimeout, CC.xy(3, 7));

		//---- labelms5 ----
		labelms5.setText("ms");
		panelSessionMiscs.add(labelms5, CC.xy(5, 7));

		//---- label5 ----
		label5.setText("Default Response Timeout:");
		panelSessionMiscs.add(label5, CC.xy(1, 9));
		panelSessionMiscs.add(ftfDefResponseTimeout, CC.xy(3, 9));

		//---- labelms6 ----
		labelms6.setText("ms");
		panelSessionMiscs.add(labelms6, CC.xy(5, 9));
	}

	//======== panelSessionGeneral ========
	{
		panelSessionGeneral.setLayout(new FormLayout(
			"right:default, $lcgap, [80dlu,default], left:3dlu, default, $lcgap, 30dlu, $lcgap, right:default, $lcgap, 30dlu, $lcgap, default",
			"default, $ugap, fill:default, $lgap, default, $lgap, fill:default, $rgap, fill:default, $ugap, default, $lgap, default"));

		//---- lblSessionName ----
		lblSessionName.setText("Session Name:");
		panelSessionGeneral.add(lblSessionName, CC.xy(1, 1));
		panelSessionGeneral.add(tfSessionName, CC.xy(3, 1));

		//---- labelsource ----
		labelsource.setText("ASDU Address:");
		panelSessionGeneral.add(labelsource, CC.xy(1, 3));

		//---- ftfASDUAddress ----
		ftfASDUAddress.setColumns(8);
		panelSessionGeneral.add(ftfASDUAddress, CC.xy(3, 3));

		//---- label10 ----
		label10.setText("ASDU Address Size:");
		panelSessionGeneral.add(label10, CC.xy(9, 3));

		//---- lblASDUAddrSize ----
		lblASDUAddrSize.setHorizontalAlignment(SwingConstants.RIGHT);
		panelSessionGeneral.add(lblASDUAddrSize, CC.xy(11, 3));

		//---- label11 ----
		label11.setText("octets");
		panelSessionGeneral.add(label11, CC.xy(13, 3));

		//---- labeldest ----
		labeldest.setText("Max APDU size:");
		panelSessionGeneral.add(labeldest, CC.xy(1, 5));

		//---- ftfMaxAPDUSize ----
		ftfMaxAPDUSize.setColumns(8);
		panelSessionGeneral.add(ftfMaxAPDUSize, CC.xy(3, 5));

		//---- label1 ----
		label1.setText("octets");
		panelSessionGeneral.add(label1, CC.xy(5, 5));

		//---- label12 ----
		label12.setText("IOA Size:");
		panelSessionGeneral.add(label12, CC.xy(9, 5));
		panelSessionGeneral.add(ftfObjAddrSize, CC.xy(11, 5));

		//---- label13 ----
		label13.setText("octets");
		panelSessionGeneral.add(label13, CC.xy(13, 5));

		//---- label8 ----
		label8.setText("Max Command Age:");
		panelSessionGeneral.add(label8, CC.xy(1, 7));
		panelSessionGeneral.add(ftfMaxCommandAge, CC.xy(3, 7));

		//---- labelms7 ----
		labelms7.setText("ms");
		panelSessionGeneral.add(labelms7, CC.xy(5, 7));

		//---- label2 ----
		label2.setText("COT Size:");
		panelSessionGeneral.add(label2, CC.xy(9, 7));

		//---- lblCOTSize ----
		lblCOTSize.setHorizontalAlignment(SwingConstants.RIGHT);
		panelSessionGeneral.add(lblCOTSize, CC.xy(11, 7));

		//---- label14 ----
		label14.setText("octets");
		panelSessionGeneral.add(label14, CC.xy(13, 7));

		//---- label9 ----
		label9.setText("Max Command Future:");
		panelSessionGeneral.add(label9, CC.xy(1, 9));
		panelSessionGeneral.add(ftfMaxCommandFuture, CC.xy(3, 9));

		//---- labelms8 ----
		labelms8.setText("ms");
		panelSessionGeneral.add(labelms8, CC.xy(5, 9));
		panelSessionGeneral.add(combCmdTimetagMode, CC.xy(3, 11));

		//---- label19 ----
		label19.setText("Commands Time Tag:");
		panelSessionGeneral.add(label19, CC.xy(1, 11));
	}

	//======== panelSessionFunctions ========
	{
		panelSessionFunctions.setLayout(new FormLayout(
			"left:default",
			"5*(default, $lgap), default"));

		//---- cbSendClkSynEvents ----
		cbSendClkSynEvents.setText("Send Clock Sync Events");
		cbSendClkSynEvents.setOpaque(false);
		panelSessionFunctions.add(cbSendClkSynEvents, CC.xy(1, 1));

		//---- cbDelOldestEvent ----
		cbDelOldestEvent.setText("Delete Oldest Event");
		cbDelOldestEvent.setOpaque(false);
		panelSessionFunctions.add(cbDelOldestEvent, CC.xy(1, 3));

		//---- cbCseUseActTerm ----
		cbCseUseActTerm.setText("Send ACT TERM upon completion of set point commands ");
		cbCseUseActTerm.setToolTipText(" Specify whether to send ACT TERM upon completion of set point commands");
		cbCseUseActTerm.setOpaque(false);
		panelSessionFunctions.add(cbCseUseActTerm, CC.xy(1, 5));

		//---- cbCmdUseActTerm ----
		cbCmdUseActTerm.setText("Send ACT TERM upon completion of commands other than set point commands ");
		cbCmdUseActTerm.setOpaque(false);
		panelSessionFunctions.add(cbCmdUseActTerm, CC.xy(1, 7));

		//---- cbStrictAdherence ----
		cbStrictAdherence.setText("Send PMENA/B/C in multiple point response.");
		cbStrictAdherence.setOpaque(false);
		cbStrictAdherence.setToolTipText("Send PMENA/B/C in multiple point response.");
		panelSessionFunctions.add(cbStrictAdherence, CC.xy(1, 9));

		//---- cbUseDayOfWeek ----
		cbUseDayOfWeek.setText("Use Day Of Week");
		cbUseDayOfWeek.setOpaque(false);
		cbUseDayOfWeek.setToolTipText("Send PMENA/B/C in multiple point response.");
		panelSessionFunctions.add(cbUseDayOfWeek, CC.xy(1, 11));
	}
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelHint;
  private JTabbedPane tabbedPane1;
  private JXTaskPaneContainer settingsPane;
  private JPanel panelSessionMiscs;
  private JLabel labelAppConfirmTimeout;
  private JFormattedTextField ftfCyclicPeriod;
  private JLabel labelms4;
  private JLabel label6;
  private JFormattedTextField ftfCyclicFirstPeriod;
  private JLabel label7;
  private JLabel labelLinkStatusPeriod;
  private JFormattedTextField ftfBackgroundPeriod;
  private JLabel labelms;
  private JLabel label4;
  private JFormattedTextField ftfSelectTimeout;
  private JLabel labelms5;
  private JLabel label5;
  private JFormattedTextField ftfDefResponseTimeout;
  private JLabel labelms6;
  private EventPanelAI panelEventAnlogInput;
  private EventPanelDI panelEventDigitalInput;
  private JPanel panelSessionGeneral;
  private JLabel lblSessionName;
  private JTextField tfSessionName;
  private JLabel labelsource;
  private JFormattedTextField ftfASDUAddress;
  private JLabel label10;
  private JLabel lblASDUAddrSize;
  private JLabel label11;
  private JLabel labeldest;
  private JFormattedTextField ftfMaxAPDUSize;
  private JLabel label1;
  private JLabel label12;
  private JFormattedTextField ftfObjAddrSize;
  private JLabel label13;
  private JLabel label8;
  private JFormattedTextField ftfMaxCommandAge;
  private JLabel labelms7;
  private JLabel label2;
  private JLabel lblCOTSize;
  private JLabel label14;
  private JLabel label9;
  private JFormattedTextField ftfMaxCommandFuture;
  private JLabel labelms8;
  private JComboBox combCmdTimetagMode;
  private JLabel label19;
  private JPanel panelSessionFunctions;
  private JCheckBox cbSendClkSynEvents;
  private JCheckBox cbDelOldestEvent;
  private JCheckBox cbCseUseActTerm;
  private JCheckBox cbCmdUseActTerm;
  private JCheckBox cbStrictAdherence;
  private JCheckBox cbUseDayOfWeek;
  private IEC870SessionTimeFormatPanel panelSessionTimeFormat;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
