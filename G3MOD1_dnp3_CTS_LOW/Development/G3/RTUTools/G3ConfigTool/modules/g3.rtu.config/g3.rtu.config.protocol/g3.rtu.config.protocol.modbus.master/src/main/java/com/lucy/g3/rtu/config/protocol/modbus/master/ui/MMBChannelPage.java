/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;
import javax.swing.text.DefaultFormatterFactory;

import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.RegexFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.port.PortsUtility;
import com.lucy.g3.rtu.config.port.ui.common.SerialPortSelectPane;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannelConf;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBConstraints;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelTCPConf;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolItemManagerPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigListPage;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.ModBusEnum.LU_MBLINK_TYPE;

/**
 * The page for configuring ModBus Master Channel which consists of a collection
 * of sessions.
 */
public class MMBChannelPage extends AbstractConfigListPage {

  private final MMBChannel channel;
  private final MMBSessionManagerModel sessionMgrModel;
  private final PresentationModel<MMBChannelConf> mbmChnlConfModel;
  private final PresentationModel<ProtocolChannelSerialConf> serialConfigModel;
  private final PresentationModel<ProtocolChannelTCPConf> tcpConfigModel;

  private final IConfig config;

  public MMBChannelPage(MMBChannel channel, IConfig config) {
    super(channel);
    this.channel = Preconditions.checkNotNull(channel, "channel is null");
    this.config = Preconditions.checkNotNull(config, "config is null");

    sessionMgrModel = new MMBSessionManagerModel(channel);
    mbmChnlConfModel = new PresentationModel<MMBChannelConf>(channel.getChannelConfig());
    tcpConfigModel = new PresentationModel<ProtocolChannelTCPConf>(channel.getChannelConfig().getTcpConf());
    serialConfigModel = new PresentationModel<ProtocolChannelSerialConf>(channel.getChannelConfig().getSerialConf());

    // Bind node name to channel name.
    ValueModel vm = mbmChnlConfModel.getModel(MMBChannelConf.PROPERTY_CHANNEL_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_NODE_NAME);
    PropertyConnector.connectAndUpdate(vm, this, PROPERTY_TITLE);
  }

  private void createUIComponents() {
    sessionListPanel = new ProtocolItemManagerPanel(
        sessionMgrModel.getSelectionInList(),
        sessionMgrModel.getPageActions(), getViewAction());

    ioConfigPane = new JXTaskPaneContainer();
    ioConfigPane.add(panelGeneral = new JXTaskPane("General"));
    ioConfigPane.add(panelIOTCP = new JXTaskPane("TCP Connection"));
    
    panelSerial = new SerialPortSelectPane(
        serialConfigModel.getModel(ProtocolChannelSerialConf.PROPERTY_SERIAL_PORT),
        PortsUtility.getSerialPortsListModel(config));
    ioConfigPane.add(panelSerial);

    
    dataLinkPane = new JXTaskPaneContainer();
    dataLinkPane.add(panelSettings = new JXTaskPane("Settings"));
    panelSettings.setVisible(true);

    RegexFormatter ipFormatter = new RegexFormatter(IPAddress.IP_PATTERN_WITH_ANY);
    ftfIPAddress =
        BasicComponentFactory.createFormattedTextField(
            tcpConfigModel.getModel(ProtocolChannelTCPConf.PROPERTY_IP_ADDRESS), ipFormatter);

  }

  private void panelMouseClicked(MouseEvent e) {
    ((JPanel) e.getSource()).requestFocus();
  }

  private void comboNetworkTypeItemStateChanged(ItemEvent e) {
    updateNetworkPanelVisibility();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    JTabbedPane tabbedPane1 = new JTabbedPane();
    JScrollPane scrollPane1 = new JScrollPane();
    scrollPane2 = new JScrollPane();
    labelChnName = new JLabel();
    tfChannelName = new JFormattedTextField();
    labelNetworktype = new JLabel();
    comboNetworkType = new JComboBox<>();
    labelIPAdd = new JLabel();
    labelPort = new JLabel();
    ftfPort = new JFormattedTextField();
    labelConTimeout = new JLabel();
    ftfConnectionTimeout = new JFormattedTextField();
    maxQueueSizeLabel = new JLabel();
    ftfMaxQueueSize = new JFormattedTextField();
    lblReceiveFrameTimeout = new JLabel();
    ftfReceiveFrameTimeout = new JFormattedTextField();
    label5 = new JLabel();
    lblChannelResponseTimeout = new JLabel();
    ftfResponseTimeout = new JFormattedTextField();
    label9 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane1 ========
      {
        scrollPane1.setBorder(null);
        scrollPane1.setViewportView(ioConfigPane);
      }
      tabbedPane1.addTab("Channel", scrollPane1);

      //======== scrollPane2 ========
      {
        scrollPane2.setViewportView(dataLinkPane);
      }
      tabbedPane1.addTab("Channel Settings", scrollPane2);
      tabbedPane1.addTab("Sessions", sessionListPanel);
    }
    add(tabbedPane1, BorderLayout.CENTER);

    //======== panelGeneral ========
    {
      panelGeneral.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          panelMouseClicked(e);
        }
      });
      panelGeneral.setLayout(new FormLayout(
          "default, [80dlu,default]",
          "default, $ugap, fill:default"));

      //---- labelChnName ----
      labelChnName.setText("Channel Name:");
      panelGeneral.add(labelChnName, CC.xy(1, 1));

      //---- tfChannelName ----
      tfChannelName.setColumns(8);
      panelGeneral.add(tfChannelName, CC.xy(2, 1));

      //---- labelNetworktype ----
      labelNetworktype.setText("Modbus Type: ");
      panelGeneral.add(labelNetworktype, CC.xy(1, 3, CC.RIGHT, CC.DEFAULT));

      //---- comboNetworkType ----
      comboNetworkType.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(ItemEvent e) {
          comboNetworkTypeItemStateChanged(e);
        }
      });
      panelGeneral.add(comboNetworkType, CC.xy(2, 3));
    }

    //======== panelIOTCP ========
    {
      panelIOTCP.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          panelMouseClicked(e);
        }
      });
      panelIOTCP.setLayout(new FormLayout(
          "right:default, $lcgap, [100dlu,default], $ugap, [40dlu,default]",
          "2*(default, $lgap), default"));

      //---- labelIPAdd ----
      labelIPAdd.setText("IP Address:");
      panelIOTCP.add(labelIPAdd, CC.xy(1, 1));

      //---- ftfIPAddress ----
      ftfIPAddress.setColumns(8);
      panelIOTCP.add(ftfIPAddress, CC.xy(3, 1));

      //---- labelPort ----
      labelPort.setText("Port:");
      panelIOTCP.add(labelPort, CC.xy(1, 3));

      //---- ftfPort ----
      ftfPort.setColumns(4);
      panelIOTCP.add(ftfPort, CC.xy(3, 3));

      //---- labelConTimeout ----
      labelConTimeout.setText("Connection Timeout:");
      panelIOTCP.add(labelConTimeout, CC.xy(1, 5));

      //---- ftfConnectionTimeout ----
      ftfConnectionTimeout.setColumns(8);
      panelIOTCP.add(ftfConnectionTimeout, CC.xy(3, 5));
    }

    //======== panelSettings ========
    {
      panelSettings.setLayout(new FormLayout(
          "2*(default, $lcgap), default",
          "2*(default, $lgap), default"));

      //---- maxQueueSizeLabel ----
      maxQueueSizeLabel.setText("Max Queue Size:");
      panelSettings.add(maxQueueSizeLabel, CC.xy(1, 1, CC.RIGHT, CC.DEFAULT));

      //---- ftfMaxQueueSize ----
      ftfMaxQueueSize.setColumns(15);
      ftfMaxQueueSize.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
      panelSettings.add(ftfMaxQueueSize, CC.xy(3, 1));

      //---- lblReceiveFrameTimeout ----
      lblReceiveFrameTimeout.setText("Receive Frame Timeout:");
      panelSettings.add(lblReceiveFrameTimeout, CC.xy(1, 3, CC.RIGHT, CC.DEFAULT));

      //---- ftfReceiveFrameTimeout ----
      ftfReceiveFrameTimeout.setColumns(15);
      panelSettings.add(ftfReceiveFrameTimeout, CC.xy(3, 3));

      //---- label5 ----
      label5.setText("ms");
      panelSettings.add(label5, CC.xy(5, 3));

      //---- lblChannelResponseTimeout ----
      lblChannelResponseTimeout.setText("Channel Response Timeout:");
      panelSettings.add(lblChannelResponseTimeout, CC.xy(1, 5, CC.RIGHT, CC.DEFAULT));

      //---- ftfResponseTimeout ----
      ftfResponseTimeout.setColumns(15);
      panelSettings.add(ftfResponseTimeout, CC.xy(3, 5));

      //---- label9 ----
      label9.setText("ms");
      panelSettings.add(label9, CC.xy(5, 5));
    }

    //---- panelSerial ----
    panelSerial.setTitle("Physical Layer");
    // //GEN-END:initComponents
  }

  private void setBoundary(JFormattedTextField ftf, String prefix, String property) {
    ftf.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(
            MMBConstraints.INSTANCE.getMin(prefix, property),
            MMBConstraints.INSTANCE.getMax(prefix, property))));
  }

  private void setBoundaries() {
    setBoundary(ftfPort, MMBConstraints.PREFIX_CHANNEL_IO, "ipPort");
    setBoundary(ftfConnectionTimeout, MMBConstraints.PREFIX_CHANNEL_IO, "ipConnectTimeout");

    setBoundary(ftfMaxQueueSize, MMBConstraints.PREFIX_CHANNEL, MMBChannelConf.PROPERTY_MAX_QUEUESIZE);
    setBoundary(ftfResponseTimeout, MMBConstraints.PREFIX_CHANNEL, MMBChannelConf.PROPERTY_CHNL_RES_TIMEOUT);
    setBoundary(ftfReceiveFrameTimeout, MMBConstraints.PREFIX_CHANNEL, MMBChannelConf.PROPERTY_RX_FRAMETIMEOUT);
  }

  private void installCommitOnType() {
    FormattedTextFieldSupport.installCommitOnType(tfChannelName);

    FormattedTextFieldSupport.installCommitOnType(ftfIPAddress);

    FormattedTextFieldSupport.installCommitOnType(ftfPort);
    FormattedTextFieldSupport.installCommitOnType(ftfConnectionTimeout);
    FormattedTextFieldSupport.installCommitOnType(ftfMaxQueueSize);
    FormattedTextFieldSupport.installCommitOnType(ftfReceiveFrameTimeout);
    FormattedTextFieldSupport.installCommitOnType(ftfResponseTimeout);
  }

  @Override
  protected void init() throws Exception {
    super.init();
    initComponents();
    setBoundaries();
    initComponentsBinding();
    installCommitOnType();
    updateNetworkPanelVisibility();
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JXTaskPaneContainer ioConfigPane;
  private JScrollPane scrollPane2;
  private JXTaskPaneContainer dataLinkPane;
  private JPanel sessionListPanel;
  private JPanel panelGeneral;
  private JLabel labelChnName;
  private JFormattedTextField tfChannelName;
  private JLabel labelNetworktype;
  private JComboBox<LU_MBLINK_TYPE> comboNetworkType;
  private JPanel panelIOTCP;
  private JLabel labelIPAdd;
  private JFormattedTextField ftfIPAddress;
  private JLabel labelPort;
  private JFormattedTextField ftfPort;
  private JLabel labelConTimeout;
  private JFormattedTextField ftfConnectionTimeout;
  private JPanel panelSettings;
  private JLabel maxQueueSizeLabel;
  private JFormattedTextField ftfMaxQueueSize;
  private JLabel lblReceiveFrameTimeout;
  private JFormattedTextField ftfReceiveFrameTimeout;
  private JLabel label5;
  private JLabel lblChannelResponseTimeout;
  private JFormattedTextField ftfResponseTimeout;
  private JLabel label9;
  private SerialPortSelectPane panelSerial;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private void updateNetworkPanelVisibility() {
    LU_MBLINK_TYPE selectNetworkType = (LU_MBLINK_TYPE) comboNetworkType.getSelectedItem();

    if (selectNetworkType == LU_MBLINK_TYPE.LU_MBLINK_TYPE_TCP) {
      panelIOTCP.setVisible(true);
      panelSerial.setVisible(false);
    } else {
      panelSerial.setVisible(true);
      panelIOTCP.setVisible(false);
    }
  }

  @SuppressWarnings("unchecked")
  private void initComponentsBinding() {

    Bindings.bind(tfChannelName,
        mbmChnlConfModel.getModel(MMBChannelConf.PROPERTY_CHANNEL_NAME));

    ComboBoxModel<LU_MBLINK_TYPE> modbusTypeCombo = new ComboBoxAdapter<LU_MBLINK_TYPE>(
        LU_MBLINK_TYPE.values(),
        mbmChnlConfModel.getModel(MMBChannelConf.PROPERTY_MBTYPE));
    comboNetworkType.setModel(modbusTypeCombo);

    // ====== TCP/IP Config Binding ======
    Bindings.bind(ftfPort,
        tcpConfigModel.getModel(ProtocolChannelTCPConf.PROPERTY_IP_PORT));
    Bindings.bind(ftfConnectionTimeout, tcpConfigModel
        .getModel(ProtocolChannelTCPConf.PROPERTY_IP_CONNECT_TIMEOUT));

    // ================== Channel Settings Binding ===================
    Bindings.bind(ftfMaxQueueSize, mbmChnlConfModel.getModel(MMBChannelConf.PROPERTY_MAX_QUEUESIZE));
    Bindings.bind(ftfReceiveFrameTimeout, mbmChnlConfModel.getModel(MMBChannelConf.PROPERTY_RX_FRAMETIMEOUT));
    Bindings.bind(ftfResponseTimeout, mbmChnlConfModel.getModel(MMBChannelConf.PROPERTY_CHNL_RES_TIMEOUT));
  }

  @Override
  public ListModel<MMBSession> getSelsectionListModel() {
    return channel.getItemListModel();
  }

  @Override
  public Action[] getContextActions() {
    return sessionMgrModel.getNodeActions();
  }

  @Override
  protected SelectionInList<?> getSelectionInList() {
    return sessionMgrModel.getSelectionInList();
  }

}
