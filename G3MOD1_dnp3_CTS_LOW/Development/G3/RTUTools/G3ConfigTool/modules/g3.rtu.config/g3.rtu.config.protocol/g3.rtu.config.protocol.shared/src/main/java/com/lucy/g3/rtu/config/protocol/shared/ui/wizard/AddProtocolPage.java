/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
class AddProtocolPage extends WizardPage {
  static final String KEY_SELECTED_PROTOCOL_TYPE=  "selectedProtocolType";
  static final String KEY_ADD_CHANNEL =  "addChannel";
  
  private ButtonGroup group;
  private final IProtocolManager<?> manager;
  private final JLabel lblAlreadyExistMsg;
  
  private final ProtocolType[] types;
  
  public AddProtocolPage(IProtocolManager<?> manager, ProtocolType[] types) {
    super("Select a protocol");
    this.types = Preconditions.checkNotNull(types,"types must not be null");
    this.manager = manager;
    this.lblAlreadyExistMsg = new JLabel();
    this.lblAlreadyExistMsg.setForeground(Color.gray);
    
    initComponents();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    /* Validate existing*/
    if(component != null && !KEY_ADD_CHANNEL.equals(component.getName())) {
      ProtocolType type = ProtocolType.valueOf(component.getName()); 
      if(manager != null && manager.hasProtocol(type))
        lblAlreadyExistMsg.setText("The selected protocol \"" + type.getName() + "\" already exists!");
      else
        lblAlreadyExistMsg.setText("");
    } 
    
    /* Validate protocol selected*/
    if(group.getSelection() == null) {
      return "Please select a protocol type!";
    }
    
    /* Validate Create Channel*/
    if(getWizardData(KEY_ADD_CHANNEL) == Boolean.TRUE) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    } else {
      setForwardNavigationMode(WizardController.MODE_CAN_FINISH);
    }
    
    return null;
  }

  private void initComponents() {
    DefaultFormBuilder builder = ProtocolAddingWizards.createPanelBuilder(this);
    
    group = new ButtonGroup();
    JRadioButton radio;
    
    int i = 0;
    for (ProtocolType type : types) {
      if(type == null)
        continue;
      
      radio = new JRadioButton(type.getName());
      builder.append(i == 0 ? "Protocol:" : "", radio);
      builder.nextLine();
      group.add(radio);
      radio.setName(type.name());
      radio.addActionListener(new ActionListener() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          JRadioButton r = (JRadioButton) e.getSource();
          if(r.isSelected()) {
            putWizardData(KEY_SELECTED_PROTOCOL_TYPE, ProtocolType.valueOf(r.getName()));
          }
        }
      });
      i ++;
    }
    
    builder.append(lblAlreadyExistMsg, 4);
    builder.nextLine();
    
    builder.appendGlueRow();
    builder.nextLine();
    
    
    JCheckBox cbox = new JCheckBox("Add a channel");
    cbox.setName(KEY_ADD_CHANNEL);
    builder.append("",cbox);
    builder.nextLine();
    cbox.setSelected(true);
  }
}
