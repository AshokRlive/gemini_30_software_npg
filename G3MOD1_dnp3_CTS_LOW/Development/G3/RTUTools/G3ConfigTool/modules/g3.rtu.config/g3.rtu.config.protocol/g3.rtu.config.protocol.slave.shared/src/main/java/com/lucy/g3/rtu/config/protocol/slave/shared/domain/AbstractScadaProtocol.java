/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocol;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;

/**
 * The basic implementation of {@linkplain IScadaProtocol}.
 *
 * @param <ChannelT>
 *          the type of protocol stack channel
 */
public abstract class AbstractScadaProtocol<ChannelT extends IProtocolChannel<?>>
    extends AbstractProtocol<ChannelT>
    implements IScadaProtocol<ChannelT> {


  protected AbstractScadaProtocol(ProtocolType type) {
    super(type);
  }

  @Override
  public final Collection<ScadaPoint> getAllInputPoints() {
    ArrayList<ScadaPoint> allPoints = new ArrayList<ScadaPoint>();

    Collection<IScadaIoMap<ScadaPoint>> iomaps = getAllIoMaps();
    for (IScadaIoMap<ScadaPoint> iomap : iomaps) {
      allPoints.addAll(iomap.getAllInputPoints());
    }
    return allPoints;
  }

  @Override
  public Collection<ScadaPoint> getAllPoints() {
    ArrayList<ScadaPoint> allPoints = new ArrayList<ScadaPoint>();

    Collection<IScadaIoMap<ScadaPoint>> iomaps = getAllIoMaps();
    for (IScadaIoMap<ScadaPoint> iomap : iomaps) {
      allPoints.addAll(iomap.getAllPoints());
    }

    return allPoints;
  }

  @Override
  public void removeAllPoints(Collection<ScadaPoint> scadapoints) {
    Collection<IScadaIoMap<ScadaPoint>> iomaps = getAllIoMaps();

    for (IScadaIoMap<ScadaPoint> iomap : iomaps) {
      iomap.removeAllPoints(scadapoints);
    }

  }

  private Collection<IScadaIoMap<ScadaPoint>> getAllIoMaps() {
    ArrayList<IScadaIoMap<ScadaPoint>> foundIoMaps = new ArrayList<IScadaIoMap<ScadaPoint>>();

    Collection<ChannelT> channels = getAllChannels();
    for (ChannelT chl : channels) {
      Collection<? extends IProtocolSession> sessions = chl.getAllSessions();
      for (IProtocolSession sesn : sessions) {
        IIoMap iomap = sesn.getIomap();
        if (iomap != null && iomap instanceof IScadaIoMap) {
          @SuppressWarnings("unchecked")
          IScadaIoMap<ScadaPoint> scadaIoMap = (IScadaIoMap<ScadaPoint>) iomap;
          foundIoMaps.add(scadaIoMap);
        }
      }
    }
    return foundIoMaps;
  }

  @Override
  public void setManager(IProtocolManager<?> manager) {
    if (manager != null) {
      Preconditions.checkArgument(manager instanceof ScadaProtocolManager, "manager is not scada type!");
    }

    super.setManager(manager);
  }

  @Override
  public ScadaProtocolManager getManager() {
    return (ScadaProtocolManager) super.getManager();
  }

}
