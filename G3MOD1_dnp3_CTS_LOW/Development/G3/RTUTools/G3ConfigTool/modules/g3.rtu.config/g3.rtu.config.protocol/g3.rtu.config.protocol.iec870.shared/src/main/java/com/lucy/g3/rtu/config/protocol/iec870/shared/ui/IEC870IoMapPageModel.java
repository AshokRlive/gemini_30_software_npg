/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui;

import java.util.ArrayList;
import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ObservableList;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870MappingResources;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870PointFactory;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points.IEC870PointDialogs;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.AbstractIoMapPageModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.ScadaPointTableModel;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;

/**
 * Presentation model for DNP3 points manager, which adapts a list of DNP3
 * points to table model and provides related operation actions.
 */
public class IEC870IoMapPageModel extends AbstractIoMapPageModel<IEC870Point> {
  
  public static final int COLUMN_ENABLE = ScadaPointTableModel.COLUMN_ENABLE;
  public static final int COLUMN_IOA    = ScadaPointTableModel.COLUMN_ID;

  private Logger log = Logger.getLogger(IEC870IoMapPageModel.class);

  private final ScadaPointSourceRepository sourceRepo;


  IEC870IoMapPageModel(IScadaIoMap<IEC870Point> iomap, IEC870PointType type, ScadaPointSourceRepository sourceRepo) {
    super(iomap, type);
    this.sourceRepo = sourceRepo;
  }


  @Override
  protected IEC870Point createPoint(IScadaIoMap<IEC870Point> iomap, ScadaPointType type, long protocolID) {
    return IEC870PointFactory.create(iomap, (IEC870PointType) type, protocolID);
  }

  @Override
  protected ArrayList<IEC870Point> showBulkAdd(ArrayList<ScadaPointSource> res, ScadaPointType type,
      IScadaIoMap<IEC870Point> iomap) {
    return IEC870PointDialogs.showBulkAdd(parent, res, (IEC870PointType)type, iomap);
  }

  @Override
  protected boolean showBulkEditor(ScadaPointType type, 
      IScadaIoMap<IEC870Point> iomap, ObservableList<IEC870Point> selection) {
    return IEC870PointDialogs.showBulkEditor(parent, (IEC870PointType) type, iomap, selection);
  }

  @Override
  protected boolean showEditor(IEC870Point point) {
    return IEC870PointDialogs.showEditor(parent, point, sourceRepo);
  }

  @Override
  protected void showEditor(ScadaPointType type, IScadaPointEditorDialogInvoker<IEC870Point> invoker) {
    IEC870PointDialogs.showEditorDialog(parent, (IEC870PointType)type, invoker, sourceRepo);
  }

  @Override
  protected ArrayList<ScadaPointSource> getMappingRes(ScadaPointType type, IScadaIoMap<IEC870Point> iomap,
      boolean spareOnly) {
    return IEC870MappingResources.getMappingRes(sourceRepo, (IEC870PointType) type, iomap, spareOnly);
  }
  
}
