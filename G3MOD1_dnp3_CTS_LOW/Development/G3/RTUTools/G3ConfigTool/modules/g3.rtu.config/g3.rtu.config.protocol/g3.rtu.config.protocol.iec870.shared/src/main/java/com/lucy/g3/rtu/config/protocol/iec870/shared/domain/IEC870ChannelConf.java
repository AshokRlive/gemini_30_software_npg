/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolChannelConf;



/**
 *
 */
public abstract class IEC870ChannelConf extends AbstractProtocolChannelConf {

  // ====== General ======
  public static final String PROPERTY_INCREMENTAL_TIMEOUT = "incrementalTimeout";
  
  // ====== Link Layer ======
  public static final String PROPERTY_TX_FRAME_SIZE = "txFrameSize";
  public static final String PROPERTY_RX_FRAME_SIZE = "rxFrameSize";
  
  private long incrementalTimeout = getDefault(PROPERTY_INCREMENTAL_TIMEOUT);
  
  private long txFrameSize        = getDefault(PROPERTY_TX_FRAME_SIZE);
  private long rxFrameSize        = getDefault(PROPERTY_RX_FRAME_SIZE);
  
  public IEC870ChannelConf(IProtocolChannel<?> channel, String channelName) {
    super(channel, channelName);
  }
  
  public long getTxFrameSize() {
    return txFrameSize;
  }

  public void setTxFrameSize(long txFrameSize) {
    Object oldValue = this.txFrameSize;
    this.txFrameSize = txFrameSize;
    firePropertyChange(PROPERTY_TX_FRAME_SIZE, oldValue, txFrameSize);
  }

  public long getRxFrameSize() {
    return rxFrameSize;
  }

  public void setRxFrameSize(long rxFrameSize) {
    Object oldValue = this.rxFrameSize;
    this.rxFrameSize = rxFrameSize;
    firePropertyChange(PROPERTY_RX_FRAME_SIZE, oldValue, rxFrameSize);
  }

  
  public long getIncrementalTimeout() {
    return incrementalTimeout;
  }

  public void setIncrementalTimeout(long incrementalTimeout) {
    Object oldValue = this.incrementalTimeout;
    this.incrementalTimeout = incrementalTimeout;
    firePropertyChange(PROPERTY_INCREMENTAL_TIMEOUT, oldValue, incrementalTimeout);
  }
  
  protected long getDefault(String property) {
    return getConstraints().getDefault(IEC870Constraints.PREFIX_CHANNEL, property);
  }
  
  public abstract IEC870Constraints getConstraints();
}

