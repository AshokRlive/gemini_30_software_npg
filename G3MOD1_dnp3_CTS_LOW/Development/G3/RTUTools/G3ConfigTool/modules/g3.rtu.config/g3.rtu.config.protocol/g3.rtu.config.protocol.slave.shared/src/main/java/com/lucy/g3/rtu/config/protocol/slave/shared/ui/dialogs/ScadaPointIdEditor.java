/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;

import javax.swing.Action;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.text.JTextComponent;

import org.jdesktop.swingx.table.NumberEditorExt;

import com.jgoodies.common.base.Preconditions;

/**
 * The cell editor for SCADA Point ID.
 */
public class ScadaPointIdEditor extends NumberEditorExt {

  private final Border red = new LineBorder(Color.red);
  private final Border black = new LineBorder(Color.black);

  private final IIdChecker idChecker;

  private static final NumberFormat format; // The format used by this editor.
  static {
    format = NumberFormat.getIntegerInstance();
    format.setParseIntegerOnly(true);
  }


  public ScadaPointIdEditor(IIdChecker idChecker) {
    super(format);
    this.idChecker = Preconditions.checkNotNull(idChecker, "idChecker must not be null");
    setClickCountToStart(1);
  }

  @Override
  protected boolean isValid() {
    String error = null;

    try {
      Number newValue = getNumber();
      if (newValue != null) {
        long id = newValue.longValue();

        if (idChecker.isOutOfRange(id)) {
          error = "<html><font color=\"red\">The ID is out of range: "
              + id + "</font></html>";
        }

        if (idChecker.isExists(id)) {
          error = "<html><font color=\"red\">The ID: "
              + id + " already exists!</font></html>";
        }
      }
    } catch (Exception e) {
      error = "Invalid protocol id";
    }

    if (error != null) {
      getComponent().setBorder(red);
      showTooltip(error);
      return false;
    } else {
      return true;
    }
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column) {
    getComponent().setBorder(black);
    Component comp = super.getTableCellEditorComponent(table, value, isSelected, row,
        column);
    // Clear editor when start editing
    if (comp instanceof JTextComponent) {
      ((JTextComponent) comp).setText("");
    }

    return comp;
  }

  private void showTooltip(String tooltip) {
    final JFormattedTextField comp = getComponent();
    comp.setToolTipText(tooltip);
    final Action toolTipAction = comp.getActionMap().get("postTip");
    if (toolTipAction != null) {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          ActionEvent postTip = new ActionEvent(comp,
              ActionEvent.ACTION_PERFORMED, "");
          toolTipAction.actionPerformed(postTip);
          comp.setToolTipText(null);
        }
      });
    }
  }

}
