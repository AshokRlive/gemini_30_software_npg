/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolConstraints;

/**
 * The constraints of IEC870 configuration.
 */
public final class IEC870Constraints extends ProtocolConstraints {

  // ====== Constraints Prefixes ======
  public static final String PREFIX_SESSION = "session";
  public static final String PREFIX_SESSION_EVENT = "session.event";
  public static final String PREFIX_CHANNEL = "channel";
  public static final String PREFIX_CHANNEL_TCP = "channel.tcp";


  private IEC870Constraints(String contraintPropertyFileName) {
    super(contraintPropertyFileName);
  }


  public static final IEC870Constraints INSTANCE_101 = new IEC870Constraints("IEC870-101-Constraints.properties");
  public static final IEC870Constraints INSTANCE_104 = new IEC870Constraints("IEC870-104-Constraints.properties");
}
