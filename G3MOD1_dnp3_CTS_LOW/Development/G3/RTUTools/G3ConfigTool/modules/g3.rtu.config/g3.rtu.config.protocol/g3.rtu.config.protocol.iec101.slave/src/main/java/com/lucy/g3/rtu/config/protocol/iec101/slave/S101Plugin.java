/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Session;
import com.lucy.g3.rtu.config.protocol.iec101.slave.ui.S101ChannelManagerPage;
import com.lucy.g3.rtu.config.protocol.iec101.slave.ui.S101ChannelPage;
import com.lucy.g3.rtu.config.protocol.iec101.slave.ui.S101SessionPage;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.ui.IEC870IoMapPage;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class S101Plugin implements IConfigPlugin, IPageFactory, IProtocolFactory{
  private S101Plugin () {}
  private final static S101Plugin  INSTANCE = new S101Plugin();
  
  public static void init() {
    PageFactories.registerFactory(PageFactories.KEY_S101, INSTANCE);
    ProtocolFactories.registerFactory(ProtocolType.S101, INSTANCE);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof S101) {
      return new S101ChannelManagerPage((S101) data);

    } else if (data instanceof S101Channel) {
      
      IConfig config = ((S101Channel)data).getProtocol().getManager().getOwner();
      PortsManager ports = config.getConfigModule(PortsManager.CONFIG_MODULE_ID);
      
      return new S101ChannelPage((S101Channel) data, ports);

    } else if (data instanceof S101Session) {
      return new S101SessionPage((S101Session) data);

    } else if (data instanceof IEC870IoMap) {
      IConfig config = ((IEC870IoMap)data).getSession().getProtocolChannel().getProtocol().getManager().getOwner();
      ScadaProtocolManager scada = config.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
      return new IEC870IoMapPage((IEC870IoMap) data, scada.getSourceRepository());
    }
    
    return null;
  }

  @Override
  public IScadaProtocol createProtocol() {
    return new S101();
  }
  
}

