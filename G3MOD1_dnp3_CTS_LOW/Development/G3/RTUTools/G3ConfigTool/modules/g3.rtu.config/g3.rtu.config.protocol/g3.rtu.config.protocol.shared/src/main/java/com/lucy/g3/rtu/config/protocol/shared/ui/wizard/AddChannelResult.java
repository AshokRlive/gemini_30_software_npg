/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;

class AddChannelResult implements WizardResultProducer {

  private final IProtocol protocol;


  public AddChannelResult(IProtocol protocol) {
    super();
    this.protocol = Preconditions.checkNotNull(protocol, "protocol must not be null");
  }

  @Override
  public IProtocolChannel finish(Map data) throws WizardException {
    String chnlName = (String) data.get(AddChannelPage.KEY_CHANNEL_NAME);
    IProtocolChannel channel = protocol.addChannel(chnlName);
    
    if(Boolean.TRUE == data.get(AddChannelPage.KEY_ADD_SESSION)) {
      new AddSessionResult(channel).finish(data);
    }
    
    return channel;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}
