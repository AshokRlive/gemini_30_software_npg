/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.EnabledHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.ext.swing.table.HighlightPredicateAdapter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractIoMapPage;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * <p>
 * The panel that consists of a points table and control panel which manages
 * point list. The content of table depends on the given
 * {@code PointManageModel}.
 * </p>
 *
 * @param <ScadaPointT>
 *          the generic type
 */
public abstract class AbstractScadaIoMapPage<ScadaPointT extends ScadaPoint> extends AbstractIoMapPage {

  private JXTable[] pointTables;

  private JTabbedPane tabbedPane;

  private final IScadaIoMap<ScadaPointT> iomap;

  private final ScadaPointSourceRepository resoure;
  
  private final ScadaPointType[] pointTypes;
  
  public AbstractScadaIoMapPage(String title, IScadaIoMap<ScadaPointT> iomap, ScadaPointSourceRepository resoure, 
      ScadaPointType[] pointTypes) {
    super(iomap, title);
    this.iomap = Preconditions.checkNotNull(iomap, "iomap must not be null");
    this.pointTypes = Preconditions.checkNotNull(pointTypes, "pointTypes must not be null");
    this.resoure = resoure;
  }

  @Override
  protected final void init() {
    initComponent();
  }

  private void initComponent() {
    /* Create required presentation models */
    AbstractIoMapPageModel<ScadaPointT>[] models = createModels(iomap, pointTypes, resoure);

    /* Create components */
    pointTables = new JXTable[pointTypes.length];
    tabbedPane = new JTabbedPane();
    String compId;
    for (int i = 0; i < pointTables.length; i++) {
      pointTables[i] = buildPointTable(models[i]);
      compId = pointTypes[i].getDescription() + "panel" + i;
      JPanel tabContent = new JPanel(new BorderLayout());
      
      tabContent.setName("tab" + compId);
      JScrollPane scroll = new JScrollPane(pointTables[i]);
      scroll.setName("scroll" + compId);
      scroll.setBorder(BorderFactory.createEmptyBorder());
      tabContent.add(scroll, BorderLayout.CENTER);
      tabContent.add(buildControlPanel(models[i]), BorderLayout.EAST);
      tabbedPane.add(pointTypes[i].getDescription(), tabContent);
    }

    tabbedPane.setName("IoMapPage.TabbedPane");
    add(tabbedPane);
  }
  
  protected abstract AbstractIoMapPageModel<ScadaPointT>[] createModels(
      IScadaIoMap<ScadaPointT> iomap,
      ScadaPointType[] pointTypes,
      ScadaPointSourceRepository resoure);

  protected JXTable buildPointTable(final AbstractIoMapPageModel<ScadaPointT> pm) {
    final TableModel tm = pm.getTableModel();
    final JXTable table = new JXTable(tm) {

      /* Aligning column in this way is for avoiding breaking highlighter */
      @Override
      public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);

        // Align ID column to centre
        if (comp instanceof JLabel) {
          JLabel render = (JLabel) comp;
          if (column == 0) {
            render.setHorizontalAlignment(SwingConstants.CENTER);
          } else {
            render.setHorizontalAlignment(SwingConstants.LEADING);
          }
        }

        return comp;
      }
    };
    
    // Set default configuration
    UIUtils.setDefaultConfig(table, true, true, true);

    // Turn off automatic editing of JTable
    table.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);

    // Set enable/disable highlighter
    table.setHighlighters(
        new ColorHighlighter(
            HighlightPredicate.ROLLOVER_ROW, UIThemeResources.COLOUR_TABLE_ROLLOVER, null),
        new EnabledHighlighter(new TableCellEnablePredicate(pm)));

    // Set invalid cell highlighter
    table.addHighlighter(new ColorHighlighter(
        new HighlightPredicateAdapter((ISupportHighlight) tm),
        UIThemeResources.COLOUR_ERROR_BACKGROUND, Color.black));

    table.setHorizontalScrollEnabled(true);
    table.setRolloverEnabled(true);
    
    // Keyboard binding
    table.getActionMap().put("Delete", pm.getAction(AbstractIoMapPageModel.ACTION_KEY_REMOVE));
    table.getActionMap().put("Edit", pm.getAction(AbstractIoMapPageModel.ACTION_KEY_EDIT));
    table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Edit");
    table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");

    // Create pop-up menu
    final JPopupMenu popupMenu = new JPopupMenu();
    popupMenu.add(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_ADD));
    popupMenu.add(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_BULK_ADD));
    popupMenu.add(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_EDIT));
    popupMenu.add(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_REMOVE));
    popupMenu.setFocusable(false);
    table.addMouseListener(new PointsTableMouseHandler(pm, popupMenu));

    // Binding table
    Bindings.bind(table, pm.getListModel(), pm.getSelectionModel());

    // Scroll to new added point automatically
    table.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(TableModelEvent e) {
        table.scrollRowToVisible(e.getFirstRow());
      }
    });

    
    // Hide columns that are not needed
    table.getColumnExt(AbstractIoMapPageModel.COLUMN_INDEX_STORE_EVENT).setVisible(false);
    return table;
  }

  protected JPanel buildControlPanel(final AbstractIoMapPageModel<ScadaPointT> pm) {

    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    panelBuilder.setDefaultDialogBorder();
    panelBuilder.addGridded(new JButton(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_ADD)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_BULK_ADD)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_EDIT)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(AbstractIoMapPageModel.ACTION_KEY_REMOVE)));
    
    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(getActionCopy()));

    panelBuilder.addGlue();
    JPanel panel = panelBuilder.getPanel();
    panel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        pm.clearSelection();
      }
    });
    return panel;
  }


  private class PointsTableMouseHandler extends MouseAdapter {

    private final AbstractIoMapPageModel<ScadaPointT> pm;
    private final JPopupMenu popupmenu;


    PointsTableMouseHandler(AbstractIoMapPageModel<ScadaPointT> pm, JPopupMenu popupmenu) {
      this.pm = pm;
      this.popupmenu = popupmenu;
    }

    @Override
    public void mousePressed(MouseEvent e) {
      mayshowPopupMenu(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      mayshowPopupMenu(e);
    }

    // Double click to edit
    @Override
    public void mouseClicked(MouseEvent e) {
      JTable table = (JTable) e.getSource();
      JTable source = (JTable) e.getSource();
      int column = source.columnAtPoint(e.getPoint());
      int row = source.rowAtPoint(e.getPoint());
      
      boolean editable = false;
      if(row >=0 && column >=0)
        editable = table.isCellEditable(row, column);
      
      if (UIUtils.isDoubleClick(e) && !editable) {
        ActionEvent event = new ActionEvent(e.getSource(), e.getID(), "edit");
        Action editAction = table.getActionMap().get("Edit");
        if (editAction != null) {
          editAction.actionPerformed(event);
        }
      }
    }

    private void mayshowPopupMenu(MouseEvent e) {
      if (e.isPopupTrigger()) {

        // Re-select point if no multi points have been selected.
        if (pm.getSelectedItems().size() <= 1) {
          doSelect(e);
        }

        JTable table = (JTable) e.getSource();
        if (popupmenu != null) {
          popupmenu.show(table, e.getX(), e.getY());
        }
      }
    }

    private void doSelect(MouseEvent e) {
      JTable pointsTable = (JTable) e.getSource();
      int rowInView = pointsTable.rowAtPoint(e.getPoint());
      if (rowInView >= 0) {
        int rowInMode = pointsTable.convertRowIndexToModel(rowInView);
        pm.getSelectionModel().setSelectionInterval(rowInMode, rowInMode);
      }
    }

  }

  private class TableCellEnablePredicate implements HighlightPredicate {

    private final AbstractIoMapPageModel<ScadaPointT> pm;


    TableCellEnablePredicate(AbstractIoMapPageModel<ScadaPointT> pm) {
      this.pm = pm;
    }

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
      int row = adapter.convertRowIndexToModel(adapter.row);
      ScadaPointT point = pm.getPointAt(row);
      return point != null && point.isEnabled() == false;
    }
  }
}
