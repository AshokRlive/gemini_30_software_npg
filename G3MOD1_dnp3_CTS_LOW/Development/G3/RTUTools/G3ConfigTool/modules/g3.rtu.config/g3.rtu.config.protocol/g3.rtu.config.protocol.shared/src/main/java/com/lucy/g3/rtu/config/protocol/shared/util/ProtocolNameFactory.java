/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.util;

import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;

/**
 *
 */
public class ProtocolNameFactory {

  private ProtocolNameFactory() {
  }

  public static String getAbsoluteName(IProtocol<?> target) {
    return String.format("%s", target.getProtocolName());
  }

  public static String getAbsoluteName(IProtocolChannel<?> target) {
    IProtocol<?> p = target.getProtocol();
    return String.format("[%s] %s", p.getProtocolName(), target.getChannelName());
  }

  public static String getAbsoluteName(IProtocolSession target) {
    IProtocolChannel<? extends IProtocolSession> c = target.getProtocolChannel();
    IProtocol<?> p = c.getProtocol();
    return String.format("[%s - %s] %s", p.getProtocolName(), c.getChannelName(), target.getSessionName());
  }
  
  public static String getAbsoluteName(IIoMap target) {
    IProtocolSession s = target.getSession();
    IProtocolChannel<? extends IProtocolSession> c = s.getProtocolChannel();
    IProtocol<?> p = c.getProtocol();
    return String.format("[%s - %s] %s - IOMap", p.getProtocolName(), c.getChannelName(), s.getSessionName());
  }
  
}
