/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import com.lucy.g3.xml.gen.api.IXmlEnum;

public class VariationsRenderer extends DefaultListCellRenderer {

  @Override
  public Component getListCellRendererComponent(JList<?> list, Object value,
      int index, boolean isSelected, boolean cellHasFocus) {
    Component comp = super.getListCellRendererComponent(list, value, index,
        isSelected, cellHasFocus);

    if (value != null && value instanceof IXmlEnum) {
      IXmlEnum e = (IXmlEnum) value;
      if (e.getValue() == 0) {
        setText("Unspecified");
      } else {
        setText(String.format("%s (%s)", e.getValue(), e.getDescription()));
      }
    } else if (value == null) {
      setText("(No change)");
    }

    return comp;
  }
}
