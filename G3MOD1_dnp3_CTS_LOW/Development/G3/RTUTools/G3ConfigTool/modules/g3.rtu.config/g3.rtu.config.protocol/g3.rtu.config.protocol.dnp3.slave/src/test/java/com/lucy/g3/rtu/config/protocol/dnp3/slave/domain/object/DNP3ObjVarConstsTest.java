/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object;

import org.junit.Assert;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjVarConsts;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Enums;
import com.lucy.g3.xml.gen.api.IXmlEnum;


/**
 * The Class DNP3ObjectVariationEnumsTest.
 */
public class DNP3ObjVarConstsTest {

  final private DNP3ObjGroup[] groups = DNP3ObjGroup.values();
  
  @Test
  public void testEventVaration() {
    for (int i = 0; i < groups.length; i++) {
      IXmlEnum[] e = DNP3ObjVarConsts.getEventVariationEnums(groups[i]);
        Assert.assertNotNull(e);
    }
  }

  @Test
  public void testStaticVaration() {
    for (int i = 0; i < groups.length; i++) {
      IXmlEnum[] e = SDNP3Enums.getStaticVariationEnums(groups[i]);
        Assert.assertNotNull(e);
    }
  }

}

