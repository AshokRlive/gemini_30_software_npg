/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.renderer.BooleanListRenderer;
import com.lucy.g3.gui.common.widgets.utils.KeyBindingUtil;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjGroup;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Enums;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointIdChecker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointIdEditor;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

class SDNP3PointBulkEditDialog extends JDialog {

  private final SDNP3PointType type;

  private final DNP3PointTableModel pointsTableModel;

  private boolean cancelled = false;
  private IScadaIoMap<SDNP3Point> iomap;


  public SDNP3PointBulkEditDialog(Frame owner, List<? extends SDNP3Point> editingPoints,
      SDNP3PointType type, IScadaIoMap<SDNP3Point> iomap) {
    super(owner, true);
    this.iomap = iomap;
    this.type = Preconditions.checkNotNull(type, "type must not be null");

    Preconditions.checkNotNull(editingPoints, "editingPoints must not be null");
    editingPoints.removeAll(Collections.singleton(null));// Remove null points.
    SDNP3Point[] pointsArray = editingPoints.toArray(new SDNP3Point[editingPoints.size()]);
    this.pointsTableModel = new DNP3PointTableModel(pointsArray, type, iomap);

    initComponents();
    initVisibility(pointsArray.length > 0 ? pointsArray[0] : null);
    initEventHandling();
    initComponentStates();
    setTitle(getTitle() + " - " + type.getDescription());

    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());

  }

  /**
   * Initialises default states of components.
   */
  private void initComponentStates() {
    pointsTableModel.setSequentialID(checkBoxSeqID.isSelected());
    
    final BooleanListRenderer boolRender = new BooleanListRenderer("Yes", "No", "(No change)");
    final Boolean[] BOOL_ITEMS = {Boolean.TRUE, Boolean.FALSE };
    
    /* Create parameter components */
    DNP3ObjGroup group = DNP3ObjGroup.convertFrom(type);
    if (group != null) {
      buildComboBox(comboDefVar,       SDNP3Enums.getStaticVariationEnums(group), new VariationsRenderer());
      buildComboBox(comboEventDefVar,  SDNP3Enums.getEventVariationEnums(group) , new VariationsRenderer());
      buildComboBox(comboEventClass,   DNP3ObjConfig.getEventClassItems()       , new EventClassRenderer());
      buildComboBox(comboEnableClass0, BOOL_ITEMS, boolRender);
    }

    buildComboBox(comboDisable           , BOOL_ITEMS, boolRender);
    buildComboBox(comboEnableStoreEvent  , BOOL_ITEMS, boolRender);
    buildComboBox(comboEventOnlyConnected, BOOL_ITEMS, boolRender);
  }

  private void initVisibility(SDNP3Point point) {
    if(point == null) 
      return;
    
    boolean hasEvtVar = point.getEventConf().isHasEventVariation();
    boolean hasStaticVar = point.getEventConf().isHasStaticVariation();
    
    labelEventDefVar      .setVisible(hasEvtVar);
    comboEventDefVar      .setVisible(hasEvtVar);
    labelEventClass       .setVisible(hasEvtVar);
    comboEventClass       .setVisible(hasEvtVar);
    lblEvtOnlyConnect     .setVisible(hasEvtVar);
    comboEventOnlyConnected.setVisible(hasEvtVar);
    
    labelDefVar.setVisible(hasStaticVar);
    comboDefVar.setVisible(hasStaticVar);
  }

  
  private static void buildComboBox(JComboBox<Object> combo, Object[] items, ListCellRenderer<Object> renderer) {
    DefaultComboBoxModel<Object> model = new DefaultComboBoxModel<>(items);
    model.insertElementAt(null, 0); // insert NULL item for de-selecting
    combo.setModel(model);
    combo.setRenderer(renderer);
    combo.setSelectedIndex(0);
  }

  private void applyChanges() {
    // ====== Apply parameters in the table ======
    TableCellEditor editor = pointsTable.getCellEditor();
    if (editor != null) {
      editor.stopCellEditing();
    }
    pointsTableModel.saveChanges();

    // ====== Apply other parameters ======
    HashMap<String, Object> params = new HashMap<>();
    
    params.put(SDNP3Point.PARAMETER_DEFAULT_VARIATION, comboDefVar.getSelectedItem());
    params.put(SDNP3Point.PARAMETER_EVENT_DEF_VARIATION, comboEventDefVar.getSelectedItem());
    params.put(SDNP3Point.PARAMETER_EVENT_CLASS, comboEventClass.getSelectedItem());
    params.put(SDNP3Point.PARAMETER_ENABLE_CLASS0, comboEnableClass0.getSelectedItem());
    params.put(SDNP3Point.PARAMETER_DISABLED, comboDisable.getSelectedItem());
    params.put(SDNP3Point.PARAMETER_ENABLE_STORE_EVENT, comboEnableStoreEvent.getSelectedItem());
    params.put(SDNP3Point.PARAMETER_EVENT_ONLY_CONNECTED, comboEventOnlyConnected.getSelectedItem());

    /*
     * Set customVariation to be true if either "DefaultVariation" or
     * "EventDefaultVariation" has been set by user.
     */
    Boolean customVariation = null;
    if(comboDefVar.getSelectedItem() != null)
      customVariation = Boolean.TRUE;
    
    if(comboEventDefVar.getSelectedItem() != null)
      customVariation = Boolean.TRUE;
    
    params.put(SDNP3Point.PARAMETER_CUSTOM_VARIATION_ENABLED, customVariation);
    
    if(customVariation == Boolean.TRUE) {
      if(params.get(SDNP3Point.PARAMETER_DEFAULT_VARIATION) == null) {
        params.put(SDNP3Point.PARAMETER_DEFAULT_VARIATION, SDNP3Enums.EMPTY_VARIATION);
      }
      
      if(params.get(SDNP3Point.PARAMETER_EVENT_DEF_VARIATION) == null) {
        params.put(SDNP3Point.PARAMETER_EVENT_DEF_VARIATION, SDNP3Enums.EMPTY_VARIATION);
      }
    }

    // Remove null values
    params.values().removeAll(Collections.singleton(null));
    
    // Apply all parameters to all points.
    for (int i = 0; i < pointsTableModel.pointNum; i++) {
      pointsTableModel.points[i].applyParameters(params);
    }
  }

  private void createUIComponents() {
    pointsTable = new JXTable(pointsTableModel);
    pointsTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

    TableColumnModel columnModel = pointsTable.getColumnModel();

    /* Set editor for protocol ID column */
    NumberFormat format = NumberFormat.getIntegerInstance();
    format.setParseIntegerOnly(true);
    TableCellEditor idEditor = new ScadaPointIdEditor(new ScadaPointIdChecker(iomap, type));
    columnModel.getColumn(DNP3PointTableModel.COLUMN_PROTOCOL_ID).setCellEditor(idEditor);

    /* Customise column width */
    TableColumn pidCol = columnModel.getColumn(DNP3PointTableModel.COLUMN_PROTOCOL_ID);
    pidCol.setMinWidth(100);
    columnModel.getColumn(DNP3PointTableModel.COLUMN_PROTOCOL_ID).setMaxWidth(50);

    /* Set protocol ID column centre alignment */
    DefaultTableCellRenderer render = new DefaultTableCellRenderer();
    render.setHorizontalAlignment(SwingConstants.CENTER);
    pidCol.setCellRenderer(render);

  }

  private void initEventHandling() {
    ActionListener okAL = new OKActionListener();
    ActionListener cancelAL = new CancelActionListener();
    cancelButton.addActionListener(cancelAL);
    okButton.addActionListener(okAL);

    KeyBindingUtil.registerEscapeEnterKey(getRootPane(), okAL, cancelAL);
  }

  private void checkBoxSeqIDActionPerformed(ActionEvent e) {
    pointsTableModel.setSequentialID(checkBoxSeqID.isSelected());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    JXTitledSeparator separator2 = new JXTitledSeparator();
    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    checkBoxSeqID = new JCheckBox();
    label2 = new JLabel();
    comboDisable = new JComboBox<>();
    inputPointParamPane = new JPanel();
    JXTitledSeparator separator1 = new JXTitledSeparator();
    label4 = new JLabel();
    comboEnableStoreEvent = new JComboBox<>();
    panel2 = new JPanel();
    labelDefVar = new JLabel();
    comboDefVar = new JComboBox<>();
    labelEventDefVar = new JLabel();
    comboEventDefVar = new JComboBox<>();
    labelEventClass = new JLabel();
    comboEventClass = new JComboBox<>();
    label1 = new JLabel();
    comboEnableClass0 = new JComboBox<>();
    lblEvtOnlyConnect = new JLabel();
    comboEventOnlyConnected = new JComboBox<>();
    buttonBar = new JPanel();
    btnHelp = new JButton();
    okButton = new JButton();
    cancelButton = new JButton();

    //======== this ========
    setTitle("Editing DNP3 Points");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("dnpbulkedit");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "default:grow",
            "default, $lgap, fill:[60dlu,default]:grow, $ugap, default, $pgap, default"));

        //---- separator2 ----
        separator2.setTitle("1. DNP3 Points ");
        contentPanel.add(separator2, CC.xy(1, 1));

        //======== scrollPane1 ========
        {
          scrollPane1.setPreferredSize(new Dimension(650, 200));

          //---- pointsTable ----
          pointsTable.setRowSorter(null);
          pointsTable.setFillsViewportHeight(true);
          pointsTable.setGridColor(Color.lightGray);
          pointsTable.setName("pointsTable");
          scrollPane1.setViewportView(pointsTable);
        }
        contentPanel.add(scrollPane1, CC.xy(1, 3));

        //======== panel1 ========
        {
          panel1.setLayout(new FormLayout(
              "default, $lcgap, default:grow, $lcgap, right:[50dlu,default], $lcgap, [50dlu,default]",
              "default"));

          //---- checkBoxSeqID ----
          checkBoxSeqID.setText("Sequential Protocol ID");
          checkBoxSeqID.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              checkBoxSeqIDActionPerformed(e);
            }
          });
          panel1.add(checkBoxSeqID, CC.xy(1, 1));

          //---- label2 ----
          label2.setText("Disabled All Points:");
          panel1.add(label2, CC.xy(5, 1));
          panel1.add(comboDisable, CC.xy(7, 1));
        }
        contentPanel.add(panel1, CC.xy(1, 5));

        //======== inputPointParamPane ========
        {
          inputPointParamPane.setLayout(new FormLayout(
              "2*(default, $lcgap), [50dlu,default]:grow",
              "2*(default, $ugap), default, $lgap, [12dlu,default]"));

          //---- separator1 ----
          separator1.setTitle("2. Parameters");
          inputPointParamPane.add(separator1, CC.xywh(1, 1, 5, 1));

          //---- label4 ----
          label4.setText("Enable storing event in FRAM:");
          label4.setHorizontalAlignment(SwingConstants.RIGHT);
          inputPointParamPane.add(label4, CC.xy(1, 3));
          inputPointParamPane.add(comboEnableStoreEvent, CC.xy(3, 3));

          //======== panel2 ========
          {
            panel2.setBorder(new CompoundBorder(
                new TitledBorder("DNP3 Object Settings"),
                Borders.DLU2_BORDER));
            panel2.setLayout(new FormLayout(
                "right:default, $lcgap, [50dlu,default]",
                "4*(default, $ugap), default"));

            //---- labelDefVar ----
            labelDefVar.setText("Default Variation:");
            panel2.add(labelDefVar, CC.xy(1, 1));
            panel2.add(comboDefVar, CC.xy(3, 1));

            //---- labelEventDefVar ----
            labelEventDefVar.setText("Event Default Variation:");
            panel2.add(labelEventDefVar, CC.xy(1, 3));
            panel2.add(comboEventDefVar, CC.xy(3, 3));

            //---- labelEventClass ----
            labelEventClass.setText("Event Class:");
            panel2.add(labelEventClass, CC.xy(1, 5));
            panel2.add(comboEventClass, CC.xy(3, 5));

            //---- label1 ----
            label1.setText("Enable Class0:");
            panel2.add(label1, CC.xy(1, 7));
            panel2.add(comboEnableClass0, CC.xy(3, 7));

            //---- lblEvtOnlyConnect ----
            lblEvtOnlyConnect.setText("Event Only When Connected to SCADA:");
            lblEvtOnlyConnect.setHorizontalAlignment(SwingConstants.RIGHT);
            panel2.add(lblEvtOnlyConnect, CC.xy(1, 9));
            panel2.add(comboEventOnlyConnected, CC.xy(3, 9));
          }
          inputPointParamPane.add(panel2, CC.xywh(1, 5, 5, 1));
        }
        contentPanel.add(inputPointParamPane, CC.xy(1, 7));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$lcgap, default, $glue, $button, $rgap, $button",
            "pref"));

        //---- btnHelp ----
        btnHelp.setText("Help");
        buttonBar.add(btnHelp, CC.xy(2, 1));

        //---- okButton ----
        okButton.setText("OK");
        okButton.setToolTipText("Create and add DNP3 protocol points from selected resources");
        buttonBar.add(okButton, CC.xy(4, 1));

        //---- cancelButton ----
        cancelButton.setText("Close");
        buttonBar.add(cancelButton, CC.xy(6, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JTable pointsTable;
  private JPanel panel1;
  private JCheckBox checkBoxSeqID;
  private JLabel label2;
  private JComboBox<Object> comboDisable;
  private JPanel inputPointParamPane;
  private JLabel label4;
  private JComboBox<Object> comboEnableStoreEvent;
  private JPanel panel2;
  private JLabel labelDefVar;
  private JComboBox<Object> comboDefVar;
  private JLabel labelEventDefVar;
  private JComboBox<Object> comboEventDefVar;
  private JLabel labelEventClass;
  private JComboBox<Object> comboEventClass;
  private JLabel label1;
  private JComboBox<Object> comboEnableClass0;
  private JLabel lblEvtOnlyConnect;
  private JComboBox<Object> comboEventOnlyConnected;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class OKActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      // Validate
      Long duplicatedID = pointsTableModel.checkDuplicatedProtocolID();
      if (duplicatedID != null) {
        JOptionPane.showMessageDialog(SDNP3PointBulkEditDialog.this,
            "Duplicated Protocol ID: " + duplicatedID,
            "Error", JOptionPane.ERROR_MESSAGE);
      } else {

        applyChanges();
        dispose();
      }
    }
  }

  private class CancelActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      dispose();
      cancelled = true;
    }
  }


  public boolean isCancelled() {
    return cancelled;
  }


  private static class DNP3PointTableModel extends AbstractTableModel {

    static final int COLUMN_RESOURCE_NAME = 0;
    static final int COLUMN_PROTOCOL_ID = 1;
    static final int COLUMN_DESCRIPTION = 2;

    private static final String[] COLUMN_NAMES = { "Source", "Point ID", "Description (Optional)" };

    private final SDNP3Point[] points;

    private final SDNP3PointType type;

    private final IScadaIoMap<SDNP3Point> iomap;

    private final int pointNum;

    private boolean sequentialID;

    private String[] mappedRes;
    private Long[] protocolID;
    private String[] description;


    public DNP3PointTableModel(SDNP3Point[] points, SDNP3PointType type, IScadaIoMap<SDNP3Point> iomap) {
      this.type = type;
      this.iomap = iomap;
      this.points = points;

      pointNum = points.length;
      mappedRes = new String[pointNum];
      protocolID = new Long[pointNum];
      description = new String[pointNum];

      ScadaPointSource res;
      for (int i = 0; i < pointNum; i++) {
        if (points[i] != null) {
          res = points[i].getSource();
          mappedRes[i] = res == null ? "- Not Configured -" : res.getName();
          protocolID[i] = points[i].getPointID();
          description[i] = points[i].getDescription();
        }
      }

    }

    public void setSequentialID(boolean sequentialID) {
      this.sequentialID = sequentialID;
    }

    @Override
    public int getRowCount() {
      return pointNum;
    }

    @Override
    public int getColumnCount() {
      return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
      return COLUMN_NAMES[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      switch (columnIndex) {
      case COLUMN_RESOURCE_NAME:
        return mappedRes[rowIndex];

      case COLUMN_PROTOCOL_ID:
        return protocolID[rowIndex];

      case COLUMN_DESCRIPTION:
        return description[rowIndex];

      default:
        return null;
      }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex == COLUMN_PROTOCOL_ID) {
        return Long.class;
      } else {
        return super.getColumnClass(columnIndex);
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex != COLUMN_RESOURCE_NAME;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      /* Set Protocol ID */
      if (columnIndex == COLUMN_PROTOCOL_ID) {
        if (iomap.checkPointID((Long) aValue, type) == false)
        {
          return; // Invalid protocol ID value
        }

        long value = (Long) aValue;
        protocolID[rowIndex] = value;

        /* Update subsequent points with sequential ID */
        if (sequentialID) {
          int rows = getRowCount();
          for (int i = rowIndex + 1; i < rows; i++) {
            protocolID[i] = findNextProtocolID(value + 1, type);
            value = protocolID[i];
          }

          // notify table to update
          if (rowIndex + 1 < rows) {
            fireTableRowsUpdated(rowIndex + 1, rows - 1);
          }
        }
      }

      /* Set protocol point description */
      else if (columnIndex == COLUMN_DESCRIPTION) {
        if (aValue != null) {
          description[rowIndex] = aValue.toString();
        }
      }

    }

    private long findNextProtocolID(long startingPoint, SDNP3PointType type) {
      long id = startingPoint;

      while (id < type.getMaximumPointID()) {

        if (isProtocolIDValid(type, id)) {
          return id;
        }

        id++;
      }

      return 0;// Fail to allocate a id, just return default 0
    }

    /**
     * Checks if all input protocol ID are valid.
     *
     * @return duplicated protocol ID. Null if not found.
     */
    private Long checkDuplicatedProtocolID() {
      for (int i = 0; i < pointNum; i++) {
        for (int j = i + 1; j < pointNum; j++) {
          if (protocolID[i].equals(protocolID[j])) {
            return protocolID[i];
          }
        }
      }

      return null;
    }

    public void saveChanges() {
      for (int i = 0; i < points.length; i++) {
        try {
          points[i].setPointID(protocolID[i]);
        } catch (DuplicatedException e) {
          e.printStackTrace();
        }
        points[i].setCustomDescription(description[i]);
      }
    }

    /**
     * Checks if protocol ID is valid for assigning to a editing point. A
     * protocol point id is valid if it is not used in DNP3 or it is in current
     * editing list.
     */
    private boolean isProtocolIDValid(SDNP3PointType type, long id) {

      if (iomap.exists(type, id)) {
        boolean inEditinglist = false;
        for (int i = 0; i < points.length; i++) {
          if (points[i].getPointID() == id) {
            inEditinglist = true;
            break;
          }
        }

        return inEditinglist;

      } else {

        return true;
      }
    }
  }

}
