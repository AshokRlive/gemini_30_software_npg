/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialog;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Enums;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870InputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870MappingResources;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870OutputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.panels.ScadaPointBasicPanel;
import com.lucy.g3.rtu.config.shared.base.math.BitMask;
import com.lucy.g3.rtu.config.shared.base.math.BitMaskEditor;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;

/**
 * The editor dialog for IEC870 point.
 */
class IEC870PointEditDialog extends AbstractEditorDialog<IEC870Point> {

  /** A list of resource that can be mapped to IEC870 point. */
  private SelectionInList<ScadaPointSource> resSelectionModel;

  private final ScadaPointSourceRepository sourceRepo;

  private final PresentationModel<IEC870Point> pm;
  private final PresentationModel<BitMask> pmGroupMask;


  /**
   * Construct a dialog for editing a IEC870 point.
   */
  IEC870PointEditDialog(Frame parent, String title, IEC870Point point, ScadaPointSourceRepository sourceRepo) {
    super(parent, point);
    this.sourceRepo = sourceRepo;
    this.pm = super.getModel();
    this.pmGroupMask = createPresentationModel(null);

    init(point.getType(), title);
  }

  public IEC870PointEditDialog(Frame parent, String title, IEC870PointType type,
      IScadaPointEditorDialogInvoker<IEC870Point> invoker, 
      ScadaPointSourceRepository sourceRepo) {
    super(parent, invoker);
    this.sourceRepo = sourceRepo;
    this.pm = super.getModel();
    this.pmGroupMask = createPresentationModel(null);
    
    init(type, title);
  }
  
  private void init(IEC870PointType type,String dialogTitle) {
    setTitle(dialogTitle);
    setName("S104InputPointEditDialog"); // For saving dialog states
    
    pmGroupMask.setBean(getEditItem().getGroupMask());
    
    
    initComponents();
    initComponentsBinding();
    if (type.isInput()) {
      initForInputPoint();
    } else if (type.isOutput()) {
      initForOutputPoint();
    }
  }
  
  private void initEventHandlingForInputPoint(){
    /*Disable "TimeFormat" and set the value to be session's "TimeFormat" if the "customise" is not checked.*/
    checkBoxCustomiseTimeFormat.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        comboBoxTimeFormat.setEnabled(checkBoxCustomiseTimeFormat.isSelected());
        if(!checkBoxCustomiseTimeFormat.isSelected()) {
          pm.setBufferedValue(IEC870InputPoint.PROPERTY_TIME_FORMAT, getSessionTimeFormat((IEC870InputPoint) pm.getBean()));
        }
      }
    });
    
    if(!checkBoxCustomiseTimeFormat.isSelected())
      pm.setBufferedValue(IEC870InputPoint.PROPERTY_TIME_FORMAT, getSessionTimeFormat((IEC870InputPoint) pm.getBean()));
    comboBoxTimeFormat.setEnabled(checkBoxCustomiseTimeFormat.isSelected());
  }
  
  private LU_TIME_FORMAT getSessionTimeFormat(IEC870InputPoint point) {
    IEC870Session sesn = (IEC870Session) point.getIoMap().getSession();
    return sesn.getEventConfig(point.getType()).getTimeFormat();
  }

  private void initForInputPoint() {
    // @formatter:off
    Bindings.bind(checkBoxCyclic,         pm.getBufferedModel(IEC870InputPoint.PROPERTY_ENABLE_CYCLIC));
    Bindings.bind(checkBoxEnableBg,       pm.getBufferedModel(IEC870InputPoint.PROPERTY_ENABLE_BACKGROUND));
    Bindings.bind(checkBoxGeneralInterog, pm.getBufferedModel(IEC870InputPoint.PROPERTY_ENABLE_GENERAL_INTEROGATION));
    Bindings.bind(checkBoxEvtConnected,   pm.getBufferedModel(IEC870InputPoint.PROPERTY_EVENT_ONLY_WHEN_CONNECTED));
    Bindings.bind(checkBoxSpontaneous,    pm.getBufferedModel(IEC870InputPoint.PROPERTY_SPONTANEOUS_EVENT_ENABLED));
    // @formatter:on

    // Time format
    Bindings.bind(comboBoxTimeFormat,
        new ComboBoxAdapter<LU_TIME_FORMAT>(IEC870Enums.getTimeFormatEnums(),
            pm.getBufferedModel(IEC870InputPoint.PROPERTY_TIME_FORMAT)));
    
    Bindings.bind(checkBoxCustomiseTimeFormat, pm.getBufferedModel(IEC870InputPoint.PROPERTY_CUSTOMISE_TIME_FORMAT));

    // Add to container
    panelParamContainer.add(panelInputPointParamPanel, BorderLayout.CENTER);
    
    initEventHandlingForInputPoint();
  }

  private void initForOutputPoint() {
    // Add to container
    panelParamContainer.add(panelOutputPointParamPanel, BorderLayout.CENTER);
    Bindings.bind(checkBoxSelectRequired, pm.getBufferedModel(IEC870OutputPoint.PROPERTY_SELECTREQUIRED));
  }

  /**
   * Load available resources that can be mapped to the given point.
   */
  private ArrayList<ScadaPointSource> getMappingResource(ScadaPointSourceRepository resource, IEC870Point point) {
    if (point == null) {
      return new ArrayList<ScadaPointSource>();
    } else {
      ArrayList<ScadaPointSource> list =
          IEC870MappingResources.getMappingRes(resource, point.getType(), point.getIoMap(), false);

      // Add current resource.
      ScadaPointSource res = point.getSource();
      if (res != null) {
        list.add(res);
      }
      return list;
    }
  }
  

  private void initComponentsBinding() {
    ValueModel source = pm.getBufferedModel(IEC870Point.PROPERTY_ENABLED);
    ValueModel converter = ConverterFactory.createBooleanNegator(source);
    Bindings.bind(checkBoxDisableProtocolPoint, converter);
  }

  private void createUIComponents() {
    BufferedValueModel vm = pm.getBufferedModel(IEC870Point.PROPERTY_SOURCE);
    resSelectionModel = new SelectionInList<ScadaPointSource>(getMappingResource(sourceRepo, getEditItem()), vm);

    protocolPointBasicPanel1 = new ScadaPointBasicPanel(pm, resSelectionModel, new IOAEditor());

    groupMaskEditor = new BitMaskEditor(pmGroupMask, true);
    
    // Hide bit 0 because it is duplicated with general interogation
    groupMaskEditor.setVisible(0,false);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    JXTitledSeparator separator1 = new JXTitledSeparator();
    panelParamContainer = new JPanel();
    checkBoxDisableProtocolPoint = new JCheckBox();
    panelInputPointParamPanel = new JPanel();
    label2 = new JLabel();
    comboBoxTimeFormat = new JComboBox();
    checkBoxCustomiseTimeFormat = new JCheckBox();
    checkBoxCyclic = new JCheckBox();
    checkBoxEnableBg = new JCheckBox();
    checkBoxSpontaneous = new JCheckBox();
    checkBoxEvtConnected = new JCheckBox();
    label1 = new JLabel();
    checkBoxGeneralInterog = new JCheckBox();
    panelOutputPointParamPanel = new JPanel();
    checkBoxSelectRequired = new JCheckBox();

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "default:grow",
          "default, $lgap, fill:default, $lgap, fill:default:grow, 2*($lgap, default)"));

      //---- protocolPointBasicPanel1 ----
      protocolPointBasicPanel1.setProtocolIDText("IOA:");
      contentPanel.add(protocolPointBasicPanel1, CC.xy(1, 1));

      //---- separator1 ----
      separator1.setTitle("Parameters");
      contentPanel.add(separator1, CC.xy(1, 3));

      //======== panelParamContainer ========
      {
        panelParamContainer.setLayout(new BorderLayout());
      }
      contentPanel.add(panelParamContainer, CC.xy(1, 5));

      //---- checkBoxDisableProtocolPoint ----
      checkBoxDisableProtocolPoint.setText("Disable this protocol point");
      contentPanel.add(checkBoxDisableProtocolPoint, CC.xy(1, 7, CC.LEFT, CC.DEFAULT));
    }

    //======== panelInputPointParamPanel ========
    {
      panelInputPointParamPanel.setLayout(new FormLayout(
          "default, $lcgap, [80dlu,default], $lcgap, default, $lcgap, default:grow, $lcgap, default",
          "default, $rgap, 3*(default, $lgap), 2*(default, $rgap), top:default:grow"));

      //---- label2 ----
      label2.setText("Time Format:");
      panelInputPointParamPanel.add(label2, CC.xy(1, 1));
      panelInputPointParamPanel.add(comboBoxTimeFormat, CC.xy(3, 1));

      //---- checkBoxCustomiseTimeFormat ----
      checkBoxCustomiseTimeFormat.setText("Customise");
      panelInputPointParamPanel.add(checkBoxCustomiseTimeFormat, CC.xy(5, 1));

      //---- checkBoxCyclic ----
      checkBoxCyclic.setText("Cyclic");
      panelInputPointParamPanel.add(checkBoxCyclic, CC.xywh(1, 3, 3, 1));

      //---- checkBoxEnableBg ----
      checkBoxEnableBg.setText("Background");
      panelInputPointParamPanel.add(checkBoxEnableBg, CC.xywh(1, 5, 3, 1));

      //---- checkBoxSpontaneous ----
      checkBoxSpontaneous.setText("Spontaneous Event");
      panelInputPointParamPanel.add(checkBoxSpontaneous, CC.xywh(1, 7, 3, 1));

      //---- checkBoxEvtConnected ----
      checkBoxEvtConnected.setText("Event Only When Connected");
      panelInputPointParamPanel.add(checkBoxEvtConnected, CC.xywh(1, 9, 3, 1));

      //---- label1 ----
      label1.setText("Group Mask:");
      panelInputPointParamPanel.add(label1, CC.xy(1, 11));

      //---- checkBoxGeneralInterog ----
      checkBoxGeneralInterog.setText("General Interrogation");
      panelInputPointParamPanel.add(checkBoxGeneralInterog, CC.xy(3, 11));

      //---- groupMaskEditor ----
      groupMaskEditor.setRequestFocusEnabled(false);
      groupMaskEditor.setMinimumSize(new Dimension(200, 33));
      groupMaskEditor.setPreferredSize(new Dimension(200, 150));
      panelInputPointParamPanel.add(groupMaskEditor, CC.xywh(3, 13, 5, 1));
    }

    //======== panelOutputPointParamPanel ========
    {
      panelOutputPointParamPanel.setLayout(new FormLayout(
          "default",
          "2*(default, $lgap), default"));

      //---- checkBoxSelectRequired ----
      checkBoxSelectRequired.setText("Select Before Operate Required.");
      panelOutputPointParamPanel.add(checkBoxSelectRequired, CC.xy(1, 1));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  @Override
  protected BufferValidator createValidator(PresentationModel<IEC870Point> target) {
    return new IEC870PointBufferValidator(target);
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private ScadaPointBasicPanel protocolPointBasicPanel1;
  private JPanel panelParamContainer;
  private JCheckBox checkBoxDisableProtocolPoint;
  private JPanel panelInputPointParamPanel;
  private JLabel label2;
  private JComboBox comboBoxTimeFormat;
  private JCheckBox checkBoxCustomiseTimeFormat;
  private JCheckBox checkBoxCyclic;
  private JCheckBox checkBoxEnableBg;
  private JCheckBox checkBoxSpontaneous;
  private JCheckBox checkBoxEvtConnected;
  private JLabel label1;
  private JCheckBox checkBoxGeneralInterog;
  private BitMaskEditor groupMaskEditor;
  private JPanel panelOutputPointParamPanel;
  private JCheckBox checkBoxSelectRequired;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected void editingItemChanged(IEC870Point newItem) {
    pmGroupMask.setBean(newItem.getGroupMask());
  }
}
