/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.JOptionPane;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.dialogs.SelectionDialog;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.channel.ui.ChannelManagerModel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio.MMBIOChannelAddingWizard;

/**
 * Presentation model for managing field device IO map.
 */
public class MMBIoMapPageModel extends ChannelManagerModel {

  public static final String ACTION_KEY_ADD_CHANNEL = "addChannel";
  public static final String ACTION_KEY_REMOVE_CHANNEL = "removeChannel";

  protected final ChannelType type;
  private final MMBIoMap iomap;


  public MMBIoMapPageModel(MMBIoMap iomap, ChannelType type) {
    super(iomap.getChannelListModel(type), iomap.getChannelParamNames(type));
    this.type = Preconditions.checkNotNull(type, "type must not be null");
    this.iomap = iomap;

  }

  @org.jdesktop.application.Action
  public void addChannel() {
    MMBIOChannelAddingWizard.showWizard(iomap, type);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_SELECTED)
  public void removeChannel() {
    IChannel[] allChnls = iomap.getChannels(type);
    IChannel sel = getSelectedChannel();
    Collection<IChannel> selected = SelectionDialog.showRemoveDialog(WindowUtils.getMainFrame(), allChnls, new IChannel[]{sel});
    if(selected == null)
      return; // User cancelled
    
    Collection<IChannel> removable = findRemovableChannels(selected);
    Collection<IChannel> unremoved = new ArrayList<>(selected);
    unremoved.removeAll(removable); 
    
    iomap.removeAllChannel(removable);
    
    if(!unremoved.isEmpty())
      MessageDialogs.list(WindowUtils.getMainFrame(),JOptionPane.ERROR_MESSAGE, 
          "Failure", "Cannot remove the following mandatory channels:", unremoved);
  }

  private Collection<IChannel> findRemovableChannels(Collection<IChannel> allChnls) {
    ArrayList<IChannel> removeable = new ArrayList<>();
    for (IChannel c : allChnls) {
      if(!c.isReadOnly())
        removeable.add(c);
    }
    
    return removeable;
  }

  /**
   * Gets the action for adding a device channel.
   *
   * @return non-null action.
   */
  public Action getActionAdd() {
    return getAction(ACTION_KEY_ADD_CHANNEL);
  }

  /**
   * Gets the action for removing a selected device channel.
   *
   * @return non-null action.
   */
  public Action getActionRemove() {
    return getAction(ACTION_KEY_REMOVE_CHANNEL);
  }

}
