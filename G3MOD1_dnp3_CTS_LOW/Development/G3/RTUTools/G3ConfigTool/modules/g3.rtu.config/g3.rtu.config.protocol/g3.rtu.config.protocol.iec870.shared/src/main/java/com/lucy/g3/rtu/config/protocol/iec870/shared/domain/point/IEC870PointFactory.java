/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.IScadaPointFactory;

/**
 * A factory for creating IEC870 Point objects.
 */
public final class IEC870PointFactory implements IScadaPointFactory<IEC870Point> {

  private Logger log = Logger.getLogger(IEC870PointFactory.class);


  @Override
  public IEC870Point createPoint(ScadaPointSource source, IScadaIoMap<IEC870Point> iomap, ScadaPointType type,
      long id, String description) {
    Preconditions.checkNotNull(type, "type is null");
    Preconditions.checkArgument(type instanceof IEC870PointType, "expected IEC870PointType");

    IEC870Point point = null;
    try {
      point = create(iomap, (IEC870PointType) type, id);

      if (!Strings.isBlank(description)) {
        point.setCustomDescription(description);
      }

      iomap.addPoint(point);
      point.mapTo(source);

    } catch (Throwable e) {
      log.fatal("Fail to create IEC870 point", e);
    }

    return point;
  }

  public static IEC870Point create(IScadaIoMap<IEC870Point> iomap, IEC870PointType type, long id) {
    Preconditions.checkNotNull(type, "type is null");
    switch (type) {

    case MMEA:
    case MMEB:
    case MMEC:
    case MSP:
    case MDP:
      return new IEC870InputPoint(iomap, type, id);
    case CSC:
      return createSingleCommandPoint(iomap, id);
    case CDC:
      return createDoubleCommandPoint(iomap, id);
    case MIT:
      return createCounterPoint(iomap, id);

    default:
      throw new UnsupportedOperationException("Unsupported type: " + type);
    }
  }

  public static IEC870InputPoint createInputPoint(IScadaIoMap<IEC870Point> ioMap, IEC870PointType type, long id) {
    Preconditions.checkNotNull(type, "type is null");
    Preconditions.checkArgument(type != IEC870PointType.CDC, "Invalid input type:" + type);
    Preconditions.checkArgument(type != IEC870PointType.CSC, "Invalid input type:" + type);

    return new IEC870InputPoint(ioMap, type, id);
  }

  public static IEC870OutputPoint createSingleCommandPoint(IScadaIoMap<IEC870Point> ioMap, long id) {
    return new IEC870OutputPoint(ioMap, id, IEC870PointType.CSC);
  }

  public static IEC870OutputPoint createDoubleCommandPoint(IScadaIoMap<IEC870Point> ioMap, long id) {
    return new IEC870OutputPoint(ioMap, id, IEC870PointType.CDC);
  }

  public static IEC870InputPoint createCounterPoint(IScadaIoMap<IEC870Point> iomap, long id) {
    return new IEC870InputPoint(iomap, IEC870PointType.MIT, id);
  }

}
