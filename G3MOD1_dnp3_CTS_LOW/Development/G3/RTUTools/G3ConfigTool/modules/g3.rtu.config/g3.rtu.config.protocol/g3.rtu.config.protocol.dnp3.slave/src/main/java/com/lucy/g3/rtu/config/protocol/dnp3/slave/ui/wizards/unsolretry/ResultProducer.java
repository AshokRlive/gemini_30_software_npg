/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.wizards.unsolretry;

import java.beans.PropertyVetoException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.UnsolRetry;
import com.lucy.g3.xml.gen.common.DNP3Enum.UNSOLICITED_RETRY_MODE;


/**
 * For applying the wizard settings.
 */
class ResultProducer implements WizardResultProducer {

  private Logger log = Logger.getLogger(ResultProducer.class);
  
  private final UnsolRetry bean;
  private Map wizardData;
  
  ResultProducer(UnsolRetry bean) {
    this.bean = bean;
  }

  @Override
  public Object finish(Map wizardData) throws WizardException {
    UNSOLICITED_RETRY_MODE mode = (UNSOLICITED_RETRY_MODE) wizardData.get(StepInitial.KEY_RETRY_MODE);
    this.wizardData = wizardData;
    
    if (mode == null) {
      log.error("No mode found");
      return null;
    }
    
    bean.setMode(mode);
    try {
      Long value;
      
      value = getLong(StepInitial.KEY_MAX_RETRY);
      if (value != null)
        bean.setUnsolMaxRetries(value);
      
      value = getLong(StepInitial.KEY_OFFLINE_DELAY);
      if (value != null)
        bean.setUnsolOfflineRetryDelay(value);
      
      value = getLong(StepInitial.KEY_RETRY_DELAY);
      if (value != null)
        bean.setUnsolRetryDelay(getLong(StepInitial.KEY_RETRY_DELAY));
      
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    
    return null;
  }
  
  private Long getLong(String key) {
    String input = (String)wizardData.get(key);
    return input == null ? null : Long.valueOf(input);
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}

