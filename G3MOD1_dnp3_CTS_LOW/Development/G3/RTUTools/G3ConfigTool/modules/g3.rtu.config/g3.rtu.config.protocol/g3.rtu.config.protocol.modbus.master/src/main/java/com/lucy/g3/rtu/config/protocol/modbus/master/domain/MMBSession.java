/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

import org.apache.log4j.Logger;

import com.g3schema.ns_mmb.MMBSesnConfT;
import com.g3schema.ns_mmb.MMBSesnT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.master.shared.factory.IMasterProtocolDeviceFactory;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolSession;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The implementation of ModBus Master Session.
 */
public class MMBSession extends AbstractProtocolSession implements IMasterProtocolSession {

  // @formatter:off
  public static final String PROPERTY_SLAVEADDRESS        = "slaveAddress";
  public static final String PROPERTY_DEF_RESP_TIMEOUT    = "defRespTimeout";
  public static final String PROPERTY_NUM_RETRIES         = "numberOfRetries";
  public static final String PROPERTY_OFFLINE_POLL_PERIOD = "offlinePollPeriod";
  public static final String PROPERTY_BYTE_SWAP           = "byteswap";
  public static final String PROPERTY_WORD_SWAP           = "wordswap";

  public static final String PROPERTY_DEVICE_ID           = "deviceId";   //Read-only
  public static final String PROPERTY_DEVICE_TYPE         = "deviceType"; //Read-only
  public static final String PROPERTY_DEVICE_MODEL        = "deviceModel";
  public static final String PROPERTY_DEVICE_NAME         = "deviceName";
  public static final String PROPERTY_CLASSIFIER          = "classifier";

  private final String prefix = MMBConstraints.PREFIX_SESSION;
  private final MMBConstraints contraint = MMBConstraints.INSTANCE;

  private long slaveAddress       = contraint.getDefault(prefix, "slaveAddress");
  private long defRespTimeout     = contraint.getDefault(prefix, "defRespTimeout");
  private long offlinePollPeriod  = contraint.getDefault(prefix, "offlinePollPeriod");
  private long numberOfRetries    = contraint.getDefault(prefix, "numberOfRetries");
  // @formatter:on

  private boolean byteswap;
  private boolean wordswap;

  private final MMBChannel channel;
  private final MMBIoMap iomap;

  private final IMasterProtocolModule device;
  private String deviceId;
  private String deviceType;
  private String deviceModel;
  private String deviceName;
  private String classifier;

  private final boolean isTemplate;

  MMBSession(MMBChannel channel, String sessionName) {
    this(channel, sessionName, false);
  }
  
  /**
   * Construct a session.
   */
  protected MMBSession(MMBChannel channel, String sessionName, boolean isTemplate) {
    super(channel, sessionName);
    this.isTemplate = isTemplate;
    this.channel = channel;
    this.iomap = new MMBIoMap(this);

    if (isTemplate) {
      this.device = null;
    } else {
      IMasterProtocolDeviceFactory factory = channel.getProtocol().getDeviceFactory();
      device = factory.createMasterDevice(MODULE_ID.MODULE_ID_0, this);
      if (device == null) {
        throw new RuntimeException("Failed to create Modbus device!");
      }
    }

    init();
  }

  private void init() {
    // Device name is set same as session name
    setDeviceName(getSessionName());

    setDeviceType(device == null ? "Modbus Device" : device.getType().getDescription());

    if (device != null) {
      MODULE_ID id = device.getId();
      if (id != null) {
        setDeviceId(id.getDescription());
      }
      device.addPropertyChangeListener(new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          String property = evt.getPropertyName();
          if (IMasterProtocolModule.PROPERTY_MODULE_ID.equals(property)) {
            MODULE_ID id = (MODULE_ID) evt.getNewValue();
            if (id != null) {
              setDeviceId(id.getDescription());
            }
          }
        }
      });
    }

    /* Add mandatory channel "online" */
    boolean readOnly = true;
    MMBIoChannel channel = new MMBIoChannel(ChannelType.DIGITAL_INPUT, 0, device, "Online", readOnly);
    channel.setParameter(MMBIoChannel.PARAM_REG_ADDRESS, 0XFFFFL);
    iomap.addChannel(channel);
  }

  @Override
  public IMasterProtocolModule getDevice() {
    return device;
  }

  @Override
  public MMBIoMap getIomap() {
    return iomap;
  }

  @Override
  public void delete() {
    iomap.clearIoMap();
    Logger.getLogger(getClass()).info("Session deleted: " + getSessionName());
  }

  @Override
  public long getSlaveAddress() {
    return slaveAddress;
  }

  @Override
  public void setSlaveAddress(long slaveAddress) throws PropertyVetoException {
    long oldValue = getSlaveAddress();
    checkBoundary(PROPERTY_SLAVEADDRESS, oldValue, slaveAddress);
    this.slaveAddress = slaveAddress;
    firePropertyChange(PROPERTY_SLAVEADDRESS, oldValue, slaveAddress);
  }

  public long getDefRespTimeout() {
    return defRespTimeout;
  }

  public void setDefRespTimeout(long defRespTimeout) throws PropertyVetoException {
    long oldValue = getDefRespTimeout();
    checkBoundary(PROPERTY_DEF_RESP_TIMEOUT, oldValue, defRespTimeout);

    this.defRespTimeout = defRespTimeout;
    firePropertyChange(PROPERTY_DEF_RESP_TIMEOUT, oldValue, defRespTimeout);
  }

  public long getOfflinePollPeriod() {
    return offlinePollPeriod;
  }

  public void setOfflinePollPeriod(long offlinePollPeriod) throws PropertyVetoException {
    long oldValue = getOfflinePollPeriod();
    checkBoundary(PROPERTY_OFFLINE_POLL_PERIOD, offlinePollPeriod, oldValue);
    this.offlinePollPeriod = offlinePollPeriod;
    firePropertyChange(PROPERTY_OFFLINE_POLL_PERIOD, oldValue, offlinePollPeriod);
  }

  private void checkBoundary(String property, long oldValue, long newValue) throws PropertyVetoException {
    contraint.checkPropertyBoundary(this, oldValue, newValue, prefix, property);
  }

  public long getNumberOfRetries() {
    return numberOfRetries;
  }

  public void setNumberOfRetries(long numberOfRetries) throws PropertyVetoException {
    long oldValue = getNumberOfRetries();
    checkBoundary(PROPERTY_NUM_RETRIES, oldValue, numberOfRetries);
    this.numberOfRetries = numberOfRetries;
    firePropertyChange(PROPERTY_NUM_RETRIES, oldValue, numberOfRetries);
  }

  @Override
  public MMBChannel getProtocolChannel() {
    return channel;
  }

  public boolean isByteswap() {
    return byteswap;
  }

  public void setByteswap(boolean byteswap) {
    Object oldValue = isByteswap();
    this.byteswap = byteswap;
    firePropertyChange(PROPERTY_BYTE_SWAP, oldValue, byteswap);
  }

  public boolean isWordswap() {
    return wordswap;
  }

  public void setWordswap(boolean wordswap) {
    Object oldValue = isWordswap();
    this.wordswap = wordswap;
    firePropertyChange(PROPERTY_WORD_SWAP, oldValue, wordswap);
  }

  public String getDeviceId() {
    return deviceId;
  }

  private void setDeviceId(String deviceId) {
    Object oldValue = this.deviceId;
    this.deviceId = deviceId;
    firePropertyChange(PROPERTY_DEVICE_ID, oldValue, deviceId);
  }

  public String getDeviceModel() {
    return deviceModel;
  }

  public void setDeviceModel(String deviceModel) {
    Object oldValue = this.deviceModel;
    this.deviceModel = deviceModel;
    firePropertyChange(PROPERTY_DEVICE_MODEL, oldValue, deviceModel);
  }

  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceDescription) {
    Object oldValue = this.deviceName;
    this.deviceName = deviceDescription;
    firePropertyChange(PROPERTY_DEVICE_NAME, oldValue, deviceDescription);

    setSessionName(deviceDescription);
  }

  public String getDeviceType() {
    return deviceType;
  }

  private void setDeviceType(String deviceType) {
    Object oldValue = this.deviceType;
    this.deviceType = deviceType;
    firePropertyChange("deviceType", oldValue, deviceType);
  }
  
  

  
  public String getClassifier() {
    return classifier;
  }

  
  public void setClassifier(String classifier) {
    Object oldValue = this.classifier;
    this.classifier = classifier;
    firePropertyChange(PROPERTY_CLASSIFIER, oldValue, classifier);
  }

  @Override
  public String toString() {
    String deviceName = getDeviceName();
    String model = getDeviceModel();
    if (!Strings.isBlank(model) && !Strings.isBlank(model)) {
      return String.format("[%s] %s", model, deviceName);
    } else if(!Strings.isBlank(model)){
      return model;
    } else if(!Strings.isBlank(deviceName)) {
      return deviceName;
    } else {
      return getSessionName();
    }
  }

  public boolean isTemplate() {
    return isTemplate;
  }

  public void writeToXML(MMBSesnT xml) {
    xml.enabled.setValue(isEnabled());
    
    MMBSesnConfT xml_sesn_conf = xml.config.append();
    xml.deviceName.setValue(getDeviceName());
    IMasterProtocolModule dev = getDevice();
    if (dev != null) {
      xml.deviceId.setValue(dev.getDeviceId().getValue());
    }
    
    xml.classifier.setValue(getClassifier());
    xml.deviceModel.setValue(getDeviceModel());
    xml.byteswap.setValue(isByteswap());
    xml.wordswap.setValue(isWordswap());
    xml.uuid.setValue(getUUID());

    xml_sesn_conf.sessionName.setValue(getSessionName());
    xml_sesn_conf.slaveAddress.setValue(getSlaveAddress());
    xml_sesn_conf.defaultResponseTimeout.setValue(getDefRespTimeout());
    xml.numberOfRetries.setValue(getNumberOfRetries());
    xml.offlinePollPeriod.setValue((int) getOfflinePollPeriod());

    getIomap().writeToXML(xml.iomap.append());
  }

  public void readFromXML(MMBSesnT xml) {
    if(xml.enabled.exists())
      setEnabled(xml.enabled.getValue());
    
    // Device ID
    MODULE_ID devId = null;
    IMasterProtocolModule device = getDevice();
    if (xml.deviceId.exists() && device != null) {
      devId = MODULE_ID.forValue((int) xml.deviceId.getValue());
      if (devId != null) {
        device.setDeviceId(devId);
      }
    }
    
    if (xml.classifier.exists())
      setClassifier(xml.classifier.getValue());
    
    if (xml.uuid.exists())
      setUUID(xml.uuid.getValue());
    
    // Device name
    setDeviceName(xml.deviceName.getValue());

    // Device model
    if (xml.deviceModel.exists()) {
      setDeviceModel(xml.deviceModel.getValue());
    }

    // Byte order
    setByteswap(xml.byteswap.getValue());
    setWordswap(xml.wordswap.getValue());

    /* Configure session */
    MMBSesnConfT xml_sesn_conf = xml.config.first();
    try {
      setDefRespTimeout(xml_sesn_conf.defaultResponseTimeout.getValue());
      setSessionName(xml_sesn_conf.sessionName.getValue());
      setSlaveAddress(xml_sesn_conf.slaveAddress.getValue());
      setNumberOfRetries(xml.numberOfRetries.getValue());
      setOfflinePollPeriod(xml.offlinePollPeriod.getValue());

    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }

    getIomap().readFromXML(xml.iomap.first());
  }

}
