/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3MappingResources;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object.DNP3ObjConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3CounterPoint;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3OutputPoint.SDNP3BinaryOutputPoint;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.panels.ScadaPointBasicPanel;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialog;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.xml.gen.common.DNP3Enum;

/**
 * Dialog for editing the properties of <code>DNP3InputPoint</code>.
 */
final class SDNP3PointEditDialog extends AbstractEditorDialog<SDNP3Point> {

  private Logger log = Logger.getLogger(SDNP3PointEditDialog.class);

  private final SDNP3PointType type;

  private final ScadaPointSourceRepository sourcesRepository;
  private final ArrayList<ScadaPointSource> sourcesList = new ArrayList<>();
  
  private final PresentationModel<DNP3ObjConfig> pmEvtconf = createPresentationModel(null);
  private final PresentationModel<DNP3ObjConfig> pmFcntrEvtconf = createPresentationModel(null);

  SDNP3PointEditDialog(Frame parent, String dialogTitle, SDNP3Point point, 
      ScadaPointSourceRepository sourcesRepository) {
    super(parent, point);
    this.type = point.getType();
    this.sourcesRepository = Preconditions.checkNotNull(sourcesRepository, "sourcesRepository must not be null");
    
    init(dialogTitle);
  }

  SDNP3PointEditDialog(Frame parent, String dialogTitle, SDNP3PointType type,
      IScadaPointEditorDialogInvoker<SDNP3Point> invoker,
      ScadaPointSourceRepository sourcesRepository) {
    super(parent, invoker);
    this.type = Preconditions.checkNotNull(type, "type must not be null");
    this.sourcesRepository = Preconditions.checkNotNull(sourcesRepository, "sourcesRepository must not be null");

    init(dialogTitle);
  }

  private void init(String dialogTitle) {
    setTitle(dialogTitle);
    setName(type.getDescription());
    
    initModel(getEditItem());
    initComponents();
    initComponentsBinding();
    initEventHandling();
  }
  
  private void initModel(SDNP3Point editingPint) {
    this.pmEvtconf.setBean(editingPint.getEventConf());
    
    if(editingPint instanceof SDNP3CounterPoint) {
      this.pmFcntrEvtconf.setBean(((SDNP3CounterPoint)editingPint).getFrozenCounterConf());
    }
    
    sourcesList.clear();
    sourcesList.addAll(getMappingResource(sourcesRepository, editingPint));
  }

  /**
   * Load available resources that can be mapped to the given point.
   */
  private static ArrayList<ScadaPointSource> getMappingResource(ScadaPointSourceRepository repo, SDNP3Point point) {
    if (point == null) {
      return new ArrayList<ScadaPointSource>();
    } else {
      ArrayList<ScadaPointSource> allRes =
          SDNP3MappingResources.getMappingRes(repo,
              point.getType(),
              point.getIoMap(),
              false);

      // Add current resource.
      ScadaPointSource res = point.getSource();
      if (res != null && !allRes.contains(res)) {
        allRes.add(res);
      }
      return allRes;
    }
  }

  private void initComponentsBinding() {
    PresentationModel<SDNP3Point> pm = getModel();
    
    // Bind Enable
    ValueModel vm = pm.getBufferedModel(SDNP3Point.PROPERTY_ENABLED);
    vm = ConverterFactory.createBooleanNegator(vm);
    Bindings.bind(checkBoxDisable, vm);
    
    // Bind enable storing event
    vm = pm.getBufferedModel(SDNP3Point.PROPERTY_ENABLE_STORE_EVENT);
    Bindings.bind(checkBoxEnableStoreEvents, vm);
    
    // Bind acceptedCommand combobox
    if(pm.getBean() != null && pm.getBean() instanceof SDNP3BinaryOutputPoint) {
      vm = pm.getBufferedModel(SDNP3BinaryOutputPoint.PROPERTY_ACCEPTED_CMD);
      Bindings.bind(comboAcceptedCommand, new SelectionInList<>(DNP3Enum.ACCEPTED_COMMAND.values(), vm));
      lblAcceptedCommand.setVisible(true);
      comboAcceptedCommand.setVisible(true);
    } else {
      lblAcceptedCommand.setVisible(false);
      comboAcceptedCommand.setVisible(false);
    }
    
    // Bind event variation panel, hide it if failed to bind.
    if(panelEvtConf.bind(pmEvtconf) == false)
      panelEvtConf.setCollapsed(true);
    
    
    // Bind counter point components
    if (pm.getBean() instanceof SDNP3CounterPoint) {
      Bindings.bind(checkBoxRolloverflag, pm.getBufferedModel(SDNP3CounterPoint.PROPERTY_USE_ROLLOVER_FLAG));
      Bindings.bind(checkBoxFrozenFlag, pm.getBufferedModel(SDNP3CounterPoint.PROPERTY_IS_FROZEN_COUNTER_ENABLED));
      
      if(panelFcntrEvtConf.bind(pmFcntrEvtconf) == false){
        panelFcntrEvtConf.setCollapsed(true);
      }else {
        // Show/Hide frozen variation settings
        vm = pm.getBufferedModel(SDNP3CounterPoint.PROPERTY_IS_FROZEN_COUNTER_ENABLED);
        PropertyConnector.connectAndUpdate(
            ConverterFactory.createBooleanNegator(vm),
            panelFcntrEvtConf, "collapsed");
      }

    }else{
      panelCounter.setVisible(false);
    }
    
  }

  /* Override to disable horizontal scroll*/
  @Override
  public void pack() {
    super.pack();
    ((JScrollPane)getContentPanel()).setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
  }

  private void createUIComponents() {
    PresentationModel<SDNP3Point> pm = getModel();

    panelBasic = new ScadaPointBasicPanel(pm,
        new SelectionInList<ScadaPointSource>(
            sourcesList,
            pm.getBufferedModel(SDNP3Point.PROPERTY_SOURCE)));
    
  }

  private void initEventHandling() {
    /* refs #2958 only one enableclass0 is allowed to be selected at the same time.*/
    final JCheckBox cbox0 = panelEvtConf.getCheckBoxEnableClass0();
    final JCheckBox cbox1 = panelFcntrEvtConf.getCheckBoxEnableClass0();
    
    cbox0.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if(cbox0.isSelected()) {
          cbox1.setSelected(false);
        }
      }
    });
    
    cbox1.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if(cbox1.isSelected()) {
          cbox0.setSelected(false);
        }
      }
    });
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    separator1 = new JXTitledSeparator();
    checkBoxDisable = new JCheckBox();
    checkBoxEnableStoreEvents = new JCheckBox();
    lblAcceptedCommand = new JLabel();
    comboAcceptedCommand = new JComboBox();
    panelEvtConf = new SDNP3PointEventConfigPanel();
    panelCounter = new JPanel();
    separator2 = new JXTitledSeparator();
    checkBoxRolloverflag = new JCheckBox();
    checkBoxFrozenFlag = new JCheckBox();
    panelFcntrEvtConf = new SDNP3PointEventConfigPanel();

    //======== contentPanel ========
    {
      contentPanel.setBorder(new EmptyBorder(0, 0, 0, 10));
      contentPanel.setLayout(new FormLayout(
        "$ugap, $lcgap, [70dlu,default], $rgap, default:grow, $lcgap, 10dlu",
        "default, $ugap, default, $lgap, default, $ugap, default, $lgap, default, $ugap, default, $lgap, default, $pgap, default, $rgap, default:grow"));

      //---- panelBasic ----
      panelBasic.setProtocolIDText("Point ID:");
      contentPanel.add(panelBasic, CC.xywh(1, 1, 7, 1));

      //---- separator1 ----
      separator1.setTitle("Parameters");
      contentPanel.add(separator1, CC.xywh(1, 3, 7, 1));

      //---- checkBoxDisable ----
      checkBoxDisable.setText("Disable this protocol point");
      contentPanel.add(checkBoxDisable, CC.xywh(3, 5, 5, 1));

      //---- checkBoxEnableStoreEvents ----
      checkBoxEnableStoreEvents.setText("Enable storing events in FRAM");
      contentPanel.add(checkBoxEnableStoreEvents, CC.xywh(3, 7, 3, 1));

      //---- lblAcceptedCommand ----
      lblAcceptedCommand.setText("Accepted Command:");
      contentPanel.add(lblAcceptedCommand, CC.xy(3, 9));
      contentPanel.add(comboAcceptedCommand, CC.xy(5, 9));
      contentPanel.add(panelEvtConf, CC.xywh(3, 11, 5, 1));

      //======== panelCounter ========
      {
        panelCounter.setLayout(new FormLayout(
          "$ugap, $lcgap, [50dlu,default], $rgap, [50dlu,default], $lcgap, default:grow",
          "3*(default, $lgap), default"));

        //---- separator2 ----
        separator2.setTitle("Counter Point Parameters");
        panelCounter.add(separator2, CC.xywh(1, 1, 7, 1));

        //---- checkBoxRolloverflag ----
        checkBoxRolloverflag.setText("Use Rollover Flag");
        panelCounter.add(checkBoxRolloverflag, CC.xy(3, 3));

        //---- checkBoxFrozenFlag ----
        checkBoxFrozenFlag.setText("Is Frozen Counter");
        panelCounter.add(checkBoxFrozenFlag, CC.xy(3, 5));
        panelCounter.add(panelFcntrEvtConf, CC.xywh(3, 7, 5, 1));
      }
      contentPanel.add(panelCounter, CC.xywh(1, 15, 7, 1));
    }
    // //GEN-END:initComponents

    UIUtils.setAllTextFieldsCaretPositionOnClick(contentPanel);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel contentPanel;
  private ScadaPointBasicPanel panelBasic;
  private JXTitledSeparator separator1;
  private JCheckBox checkBoxDisable;
  private JCheckBox checkBoxEnableStoreEvents;
  private JLabel lblAcceptedCommand;
  private JComboBox comboAcceptedCommand;
  private SDNP3PointEventConfigPanel panelEvtConf;
  private JPanel panelCounter;
  private JXTitledSeparator separator2;
  private JCheckBox checkBoxRolloverflag;
  private JCheckBox checkBoxFrozenFlag;
  private SDNP3PointEventConfigPanel panelFcntrEvtConf;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected BufferValidator createValidator(PresentationModel<SDNP3Point> target) {
    return new SDNP3PointBufferValidator(target);
  }

  @Override
  protected void editingItemChanged(SDNP3Point newItem) {
    initModel(getEditItem());
  }
}
