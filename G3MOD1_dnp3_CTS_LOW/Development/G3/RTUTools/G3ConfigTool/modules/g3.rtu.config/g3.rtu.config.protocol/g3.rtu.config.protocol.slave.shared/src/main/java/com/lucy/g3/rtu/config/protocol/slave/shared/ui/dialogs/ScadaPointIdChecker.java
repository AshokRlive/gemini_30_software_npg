/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;

/**
 * The Class ScadaPointIdChecker.
 *
 * @param <ScadaPointT>
 *          the generic type
 */
public class ScadaPointIdChecker<ScadaPointT extends ScadaPoint> implements IIdChecker {

  private final IScadaIoMap<? extends ScadaPoint> iomap;
  private final ScadaPointType type;


  public ScadaPointIdChecker(IScadaIoMap<ScadaPointT> iomap, ScadaPointType type) {
    this.iomap = Preconditions.checkNotNull(iomap, "iomap must not be null");
    this.type = Preconditions.checkNotNull(type, "type must not be null");
  }

  @Override
  public boolean isExists(Long id) {
    return id != null && iomap.exists(type, id);
  }

  @Override
  public boolean isOutOfRange(Long id) {
    return id == null || id < type.getMinimumPointID() || id > type.getMaximumPointID();
  }

}
