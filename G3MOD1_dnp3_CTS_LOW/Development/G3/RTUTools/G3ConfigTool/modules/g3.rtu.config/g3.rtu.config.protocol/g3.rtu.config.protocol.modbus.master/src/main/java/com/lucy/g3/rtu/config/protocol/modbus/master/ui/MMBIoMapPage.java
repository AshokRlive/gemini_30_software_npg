/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.EnabledHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.FormatStringValue;
import org.jdesktop.swingx.table.NumberEditorExt;
import org.jdesktop.swingx.text.NumberFormatExt;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.lucy.g3.gui.common.widgets.ext.swing.table.AutoFillJTable;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractIoMapPage;

public class MMBIoMapPage extends AbstractIoMapPage {

  private JTabbedPane tabbedPane;

  private final MMBIoMap iomap;


  public MMBIoMapPage(MMBIoMap iomap) {
    super(iomap, "Field Device Channels");
    this.iomap = Preconditions.checkNotNull(iomap, "iomap is null");
  }

  @Override
  protected void init() {
    initComponents();
  }

  private void initComponents() {
    tabbedPane = new JTabbedPane();
    this.add(tabbedPane, BorderLayout.CENTER);
    this.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));

    // Added channel table for all available types
    ChannelType[] chTypes = iomap.getAvailableChannelTypes();
    for (int i = 0; i < chTypes.length; i++) {
      // Validate all channels
      IChannel[] channels = iomap.getChannels(chTypes[i]);
      for (IChannel ch : channels) {
        ch.getValidator().validate();
      }

      MMBIoMapPageModel pm = new MMBIoMapPageModel(iomap, chTypes[i]);
      JXTable channelTable = buildChannelTable(pm, chTypes[i]);
      JComponent tableComp = buildChannelTablePane(channelTable);
      JComponent ctlPanel = buildControlPanel(pm);

      JPanel container = new JPanel(new BorderLayout());
      container.add(tableComp, BorderLayout.CENTER);
      container.add(ctlPanel, BorderLayout.EAST);
      tabbedPane.addTab(chTypes[i].getDescription(), container);
      
      if(chTypes[i] == ChannelType.DIGITAL_INPUT)
        disableOnlineChannel(channelTable);
    }
  }
  
  private JComponent buildControlPanel(MMBIoMapPageModel pm) {
    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    panelBuilder.setDefaultDialogBorder();

    panelBuilder.addGridded(new JButton(pm.getActionAdd()));
    panelBuilder.addRelatedGap();

    panelBuilder.addGridded(new JButton(pm.getActionRemove()));
    panelBuilder.addRelatedGap();

    panelBuilder.addGridded(new JButton(pm.getActionViewDetails()));
    panelBuilder.addRelatedGap();

    return panelBuilder.getPanel();
  }
  
  private JXTable buildChannelTable(MMBIoMapPageModel pm, ChannelType type) {
  
    JXTable table = pm.createChannelTable();

    // Binding
    Bindings.bind(table, pm.getChannelSelectList());

    // Set Pop-up menu
    JPopupMenu popmenu = new JPopupMenu();
    popmenu.add(pm.getActionAdd());
    popmenu.add(pm.getActionRemove());
    popmenu.add(pm.getActionViewDetails());
    table.setComponentPopupMenu(popmenu);

    // Set cell editor
    table.setDefaultEditor(MMBIoChannel.RegisterType.class,
        new DefaultCellEditor(new JComboBox<>(MMBIoChannel.getAvailabelRegisterTypes(type))));

    table.setDefaultEditor(MMBIoChannel.RegisterDataType.class,
        new DefaultCellEditor(new JComboBox<>(MMBIoChannel.getAvailableDataTypes(type))));

    table.setDefaultEditor(MMBIoChannel.MMBIoBitMask.class,
        new DefaultCellEditor(new JComboBox<>(MMBIoChannel.MMBIoBitMask.values())));

    
    /*Default Double editor*/
    NumberFormat decimalFormat = NumberFormat.getInstance();
    decimalFormat.setMaximumFractionDigits(10);
    
    table.setDefaultEditor(Double.class, new NumberEditorExt(new NumberFormatExt(decimalFormat)));
    table.setDefaultRenderer(Double.class, new DefaultTableRenderer(new FormatStringValue(decimalFormat)));
    
    
    // Set keyboard action
    getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "View");
    getActionMap().put("View", pm.getAction(MMBIoMapPageModel.ACTION_KEY_VIEW_DETAILS));

    return table;
  }

  private JComponent buildChannelTablePane(JTable channelTable) {
    // Add to container
    JScrollPane scrollpane = AutoFillJTable.createNoBorderScrollPane(channelTable);
    scrollpane.setViewportBorder(BorderFactory.createEmptyBorder());
    scrollpane.setBorder(BorderFactory.createEmptyBorder());

    return scrollpane;
  }

  /*
   * Disable "online" channel to indicate it is reserved. 
   */
  private void disableOnlineChannel(JXTable table) {
    EnabledHighlighter enableRenderer = new EnabledHighlighter(new HighlightPredicate() {
      
      @Override
      public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
          return adapter.row == 0 
              && (adapter.column != MMBIoMapPageModel.COLUMN_ID
                  && adapter.column != MMBIoMapPageModel.COLUMN_DESCRIPTION);
      }
    }, false);
    
    table.setHighlighters(enableRenderer);
  }
}
