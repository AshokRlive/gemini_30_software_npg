/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.beans.PropertyVetoException;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.g3schema.ns_sdnp3.SDNP3ChnlConfT;
import com.g3schema.ns_sdnp3.SDNP3ChnlT;
import com.g3schema.ns_sdnp3.SDNP3SesnT;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolChannel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

public class SDNP3Channel extends AbstractProtocolChannel<SDNP3Session> {

  private Logger log = Logger.getLogger(SDNP3Channel.class);

  private final SDNP3ChannelConf channelConf;

  public SDNP3Channel(SDNP3 sdnp3) {
    this(sdnp3, null);
  }

  public SDNP3Channel(SDNP3 sdnp3, String channelName) {
    super(sdnp3);
    channelConf = new SDNP3ChannelConf(this, channelName);
    getValidator().addAdditionalValidation(channelConf.getSerialConf(), channelConf.getCommsConf());
  }

  @Override
  public SDNP3ChannelConf getChannelConfig() {
    return channelConf;
  }

  @Override
  public SDNP3Session addSession(String sessionName) {
    if (allowMultiSession() || getSessionNum() == 0) {
      SDNP3Session newSesn = new SDNP3Session(this, sessionName);
      super.add(newSesn);
      return newSesn;
    } else {
      log.error("Cannot add another session");
      return null;
    }
  }

  @Override
  public SDNP3 getProtocol() {
    return (SDNP3) super.getProtocol();
  }
  
  public void writeToXML(SDNP3ChnlT xml, VirtualPointWriteSupport support) {
    // Export dnp3 channel config
    getChannelConfig().writeToXML(xml.config.append(), support);

    // Export dnp3 sessions
    Collection<SDNP3Session> sessions = getAllSessions();
    for (SDNP3Session session : sessions) {
      session.writeToXML(xml.session.append());
    }
  }

  public void readFromXML(SDNP3ChnlT xml_chnl,PortsManager portManager, CommsDeviceManager commsDevMgr,
      VirtualPointReadSupport support,  ScadaPointSourceRepository resource) throws PropertyVetoException {
    SDNP3ChnlConfT xml_chnl_conf = xml_chnl.config.first();
    SDNP3ChannelConf chnlconf = getChannelConfig();
    
    chnlconf.readFromXML(xml_chnl_conf, portManager,commsDevMgr, support);

    // Read sessions
    int sesnNum = xml_chnl.session.count();
    for (int j = 0; j < sesnNum; j++) {
      SDNP3SesnT xml_sesn = xml_chnl.session.at(j);
      SDNP3Session session = addSession(xml_sesn.config.first().sessionName.getValue());
      session.readFromXML(xml_sesn,  resource);
    }
  }

}
