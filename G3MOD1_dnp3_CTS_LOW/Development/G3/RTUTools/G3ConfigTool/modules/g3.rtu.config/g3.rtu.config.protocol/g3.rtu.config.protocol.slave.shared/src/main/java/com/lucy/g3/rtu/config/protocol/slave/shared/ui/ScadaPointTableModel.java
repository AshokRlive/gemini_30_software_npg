/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui;

import javax.swing.JOptionPane;
import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IIdChecker;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

/**
 * The table model of SCADA point.
 *
 * @param <T>
 *          the type of SCADA point
 */
public class ScadaPointTableModel<T extends ScadaPoint> extends AbstractTableAdapter<T>
    implements ISupportHighlight {

  public static final int COLUMN_ENABLE = 0;
  public static final int COLUMN_ID = 1;
  public static final int COLUMN_DESCRIPTION = 2;
  public static final int COLUMN_SOURCE = 3;
  public static final int COLUMN_ENABLE_STORE_EVENT = 4;

  private static final String NOT_CONFIGURED_TEXT = "- Not Configured -";

  protected static final String[] COLUMNS = { "Enabled", "Point ID", "Description", "Source", "Store event" };

  private final IIdChecker idChecker;

  private final boolean isOutput;
  
  public ScadaPointTableModel(ListModel<T> listModel, IIdChecker idChecker, boolean isOutput) {
    super(listModel);
    this.idChecker = idChecker;
    this.isOutput = isOutput;
  }
  
  @Override
  public int getColumnCount() {
    return COLUMNS.length;
  }

  @Override
  public String getColumnName(int columnIndex) {
    if (isOutput && columnIndex == COLUMN_SOURCE)
      return "Target";
    
    return COLUMNS[columnIndex];
  }

  protected void setColumnTitle(int column, String title) {
    if (column >= 0 && column < COLUMNS.length)
      COLUMNS[column] = title;
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    switch (columnIndex) {
    case COLUMN_DESCRIPTION:
    case COLUMN_ID:
    case COLUMN_ENABLE:
      return true;

    default:
      return super.isCellEditable(rowIndex, columnIndex);
    }
  }

  private boolean validateId(long oldId, long newId) {
    String err = null;

    if (idChecker != null) {
      if (idChecker.isOutOfRange(newId)) {
        err = String.format("The value:%s is out of range!", newId);
      } else if (idChecker.isExists(newId) && newId != oldId) {
        err = String.format("The value:%s alreay exist!", newId);
      } 

      if (err == null) {
        return true;
      } else {
        JOptionPane.showMessageDialog(null, err, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        return false;
      }
    }
    return true;
  }

  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    T point = getRow(rowIndex);
    switch (columnIndex) {
    case COLUMN_DESCRIPTION:
      point.setCustomDescription(aValue.toString());
      break;
    case COLUMN_ENABLE:
      point.setEnabled((boolean) aValue);
      break;
    case COLUMN_ID:
      if (aValue != null) {
        try {
          long newId = Long.valueOf(aValue.toString());
          if (newId != point.getPointID()) {
            if (validateId((long) getValueAt(rowIndex, columnIndex), newId)) {
              point.setPointID(newId);
            }
          }
        } catch (DuplicatedException ex) {
          JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
      }
      break;
    default:
      break;
    }
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
    case COLUMN_ENABLE:
      return Boolean.class;
    case COLUMN_ID:
      return Long.class;
    default:
      return super.getColumnClass(columnIndex);
    }
  }

  @Override
  public boolean shouldHightlight(int rowIndex, int columnIndex) {
    if (columnIndex == COLUMN_SOURCE
        && getValueAt(rowIndex, columnIndex).equals(NOT_CONFIGURED_TEXT)) {
      return true;
    }

    return false;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    T point = getRow(rowIndex);
    switch (columnIndex) {
    case COLUMN_ID:
      return point.getPointID();
    case COLUMN_DESCRIPTION:
      String descript = point.getDescription();
      return descript == null ? "" : descript;
    case COLUMN_SOURCE:
      ScadaPointSource mappedRes = point.getSource();
      return mappedRes != null ? mappedRes.getName() : NOT_CONFIGURED_TEXT;
    case COLUMN_ENABLE:
      return point.isEnabled();
    case COLUMN_ENABLE_STORE_EVENT:
      return point.isEnableStoreEvent() ? "Yes" : "No";
    default:
      return null;
    }
  }
}