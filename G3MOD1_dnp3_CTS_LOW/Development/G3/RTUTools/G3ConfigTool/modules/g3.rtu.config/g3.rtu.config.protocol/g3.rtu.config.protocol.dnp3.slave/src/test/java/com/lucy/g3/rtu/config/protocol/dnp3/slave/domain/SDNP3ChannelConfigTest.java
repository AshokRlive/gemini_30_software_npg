/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import static org.junit.Assert.fail;

import java.beans.PropertyVetoException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelConf;
import com.lucy.g3.test.support.utilities.BeanTestUtil.VetoAllListener;

/**
 *
 *
 */
public class SDNP3ChannelConfigTest {

  private SDNP3ChannelConf fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new SDNP3ChannelConf(new SDNP3Channel(new SDNP3()), "");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstraintProperty() {
    fixture.addVetoableChangeListener(new VetoAllListener());

    try {
      // Set invalid value.
      fixture.setRxFrameSize(-1);
      fail("PropertyVetoException expected");
    } catch (PropertyVetoException e) {
    }
  }

}
