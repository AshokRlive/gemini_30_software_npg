/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.stub;

import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocol;

/**
 * The Class ProtocolStub.
 */
@SuppressWarnings("rawtypes")
public class ProtocolStub extends AbstractProtocol {

  public ProtocolStub() {
    this(ProtocolType.SDNP3);
  }

  public ProtocolStub(ProtocolType type) {
    super(type);
  }

  @Override
  public IProtocolChannel<?> addChannel(String channelName) {
    return null;
  }

  @Override
  public Collection getAllSessions() {
    return null;
  }

}
