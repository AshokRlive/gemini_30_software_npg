/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.common.bean.Bean;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.validation.ScadaPointValidator;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidResourceException;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * The basic implementation of {@linkplain ScadaPoint}.
 */
public abstract class AbstractScadaPoint extends Bean implements ScadaPoint, PropertyChangeListener {
  
  private Logger log = Logger.getLogger(AbstractScadaPoint.class);

  private long pointID;
  private String customDescription = "";
  private ScadaPointSource source;
  private boolean enabled = true;
  private boolean enableStoreEvent = true;

  private String name;

  private final ScadaPointType type;
  private final IValidator validator;
  private final IScadaIoMap<? extends ScadaPoint> ioMap;
  private final IProtocolSession session;


  protected AbstractScadaPoint(IScadaIoMap<? extends ScadaPoint> ioMap, ScadaPointType type, long pointID) {
    super();
    this.ioMap = ioMap;
    this.type = Preconditions.checkNotNull(type, "Type must not be null");
    this.pointID = pointID;
    this.validator = new ScadaPointValidator(this);
    this.session = ioMap == null ? null : ioMap.getSession();
    if (session != null) {
      session.addPropertyChangeListener(IProtocolSession.PROPERTY_SESSION_NAME, this);
    }

    updateName();
  }

  @Override
  public final String getSessionName() {
    return session == null ? "No Session" : session.getSessionName();
  }

  @Override
  public final String toString() {
    return getFullName();
  }

  protected final void setPointID(long newPointID, boolean allowDuplicated) throws DuplicatedException {
    if (ioMap != null && allowDuplicated == false) {
      ScadaPoint point = ioMap.getPoint(getType(), newPointID);
      if (point != null && point != this) {
        throw new DuplicatedException("Duplicated protocol ID: " + newPointID);
      }
    }
    setPointId(newPointID);
  }

  @Override
  public final void setPointID(long newPointID) throws DuplicatedException {
    setPointID(newPointID, false);
  }

  private void setPointId(long newPointID) {
    Object oldValue = getPointID();
    this.pointID = newPointID;
    firePropertyChange(PROPERTY_POINT_ID, oldValue, newPointID);
    updateName();
  }

  @Override
  public final long getPointID() {
    return pointID;
  }

  @Override
  public ScadaPointType getType() {
    return type;
  }

  @Override
  public String getDescription() {
    if (!Strings.isBlank(customDescription)) {
      return customDescription;
    } else if (source != null) {
      return getDefaultDescription();
    } else {
      return null;
    }
  }

  @Override
  public final IScadaIoMap<? extends ScadaPoint> getIoMap() {
    return ioMap;
  }

  @Override
  public final String getCustomDescription() {
    return customDescription;
  }
  
  private String getDefaultDescription() {
    return source == null ? null : source.getDescription();
  }
  
  @Override
  public final void setCustomDescription(String customDescription) {
    /*
     * Custom description shouldn't be the same as default description.
     */
    if(customDescription != null && customDescription.equals(getDefaultDescription())) {
      customDescription = null;
    }
    
    Object oldValue = getCustomDescription();
    this.customDescription = customDescription;
    firePropertyChange(PROPERTY_CUSTOM_DESCRIPTION, oldValue, customDescription);

    updateName();
  }

  @Override
  public void delete() {
    mapTo(null);
    // Remove all listeners
    PropertyChangeListener[] pcls = this.getPropertyChangeListeners();
    for (PropertyChangeListener pcl : pcls) {
      this.removePropertyChangeListener(pcl);
    }
  }

  @Override
  public final ScadaPointSource getSource() {
    return source;
  }

  /**
   * <p>
   * Set the mapped resource of this point. Same behaviour as
   * {@link #mapTo(ScadaPointSource)}.
   * </p>
   * <em>This is a property getter method.</em>
   *
   * @param newSource
   *          new resource
   */
  @Override
  public final void setSource(ScadaPointSource newSource) {
    if (source != null) {
      source.deregisterPoint(this);
      source.removePropertyChangeListener(ScadaPointSource.PROPERTY_NAME, this);
      this.source = null;
    }

    if (newSource != null) {
      try {
        newSource.registerPoint(this);
        newSource.addPropertyChangeListener(ScadaPointSource.PROPERTY_NAME, this);
      } catch (InvalidResourceException e) {
        log.error(String.format("Cannot register \"%s\" to virtual resource cause:%s", this, e.getMessage()));
        newSource = null;
      }
    }

    Object oldValue = getSource();
    this.source = newSource;
    firePropertyChange(PROPERTY_SOURCE, oldValue, source);
  }

  public final void mapTo(ScadaPointSource source) {
    setSource(source);
  }

  @Override
  public final boolean isEnabled() {
    return enabled;
  }

  @Override
  public final void setEnabled(boolean enabled) {
    Object oldValue = isEnabled();
    this.enabled = enabled;
    firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
  }

  @Override
  public boolean isEnableStoreEvent() {
    return enableStoreEvent;
  }
  
  public void setEnableStoreEvent(boolean enableStoreEvent) {
    Object oldValue = this.enableStoreEvent;
    this.enableStoreEvent = enableStoreEvent;
    firePropertyChange(PROPERTY_ENABLE_STORE_EVENT, oldValue, enableStoreEvent);
  }

  @Override
  public final String getName() {
    return name;
  }

  @Override
  public final String getProtocolName() {
    return session == null ? "No Protocol" : session.getProtocolChannel().getProtocol().getProtocolName();
  }

  private void updateName() {
    Object oldValue = getName();
    String desp = getDescription();
    if(desp == null)
      desp = "";
    
    if (!Strings.isBlank(desp)) {
      desp = "\"" + desp + "\"";
    }

    this.name = String.format("[%s \"%s\"] %s [%s] %s",
        getProtocolName(),
        getSessionName(),
        getType().getDescription(),
        getPointID(),
        desp
        );

    firePropertyChange(PROPERTY_NAME, oldValue, this.name);
  }

  @Override
  public final String getFullName() {
    return getName();
  }

  @Override
  public final IValidator getValidator() {
    return validator;
  }

  @Override
  public final void propertyChange(PropertyChangeEvent evt) {
    updateName();
  }

  public abstract void applyParameters(HashMap<String, Object> parameters);
}
