/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.utils.CANModuleUtility;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.CommsPowerCycle;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.CommsPowerCycle.IncomingCheck;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.CommsPowerCycle.IpConnectivityCheck;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.CommsPowerCycle.OutGoingCheck;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.ConnectionManager;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.ConnectionSettings;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.Failover;
import com.lucy.g3.rtu.config.shared.base.misc.ComponentGroup;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointUtility;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;

/**
 * The panel for configuring {@linkplain connectionManager}.
 */
public class ConnectionManagerPanel extends JXTaskPane {

  public static final String ACTION_ADD_FAILOVER = "addFailover";
  public static final String ACTION_REMOVE_FAILOVER = "removeFailover";

  private final ConnectionManager connectionManager;

  private final PresentationModel<ConnectionManager> connManagerModel = new PresentationModel<>();
  private final PresentationModel<ConnectionSettings> connSettingsModel = new PresentationModel<>();
  private final PresentationModel<CommsPowerCycle> pwrCycleModel = new PresentationModel<>();

  private final PresentationModel<IpConnectivityCheck> connectivityCheckModel = new PresentationModel<>();
  private final PresentationModel<OutGoingCheck> outgoingCheckModel = new PresentationModel<>();
  private final PresentationModel<IncomingCheck> incomingCheckModel = new PresentationModel<>();

  private final IConfig config;


  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private ConnectionManagerPanel() {
    this.connectionManager = null;
    this.config = null;
    initComponents();
  }

  public ConnectionManagerPanel(ConnectionManager connMgr, IConfig config) {
    this.connectionManager = Preconditions.checkNotNull(connMgr, "connMgr is null");
    this.config = config;

    setTitle("Connection Manager");

    connManagerModel.setBean(connMgr);
    connSettingsModel.setBean(connMgr.getConnectionSettings());

    CommsPowerCycle powerCycle = connMgr.getCommsPowerCycle();
    pwrCycleModel.setBean(powerCycle);

    connectivityCheckModel.setBean(powerCycle.connectivityCheck);
    outgoingCheckModel.setBean(powerCycle.outgoingCheck);
    incomingCheckModel.setBean(powerCycle.incomingCheck);

    initComponents();
    initComponentsBinding();
    setBounaries();
  }

  @org.jdesktop.application.Action
  public void addFailover() {

    Failover failover = FailoverDialog.showDialog(WindowUtils.getMainFrame());
    if (failover != null) {
      connectionManager.getFailoverGroup().addFailover(failover);
    }
  }

  @org.jdesktop.application.Action
  public void removeFailover() {
    int[] selectedRow = tableServers.getSelectedRows();
    if (selectedRow.length > 0) {
      connectionManager.getFailoverGroup().removeFailover(selectedRow);
    } else {
      JOptionPane.showMessageDialog(WindowUtils.getMainFrame(),
          "Please select an entry to remove", "No Selection", JOptionPane.ERROR_MESSAGE);
    }
  }

  private void initComponentsBinding() {
    ValueModel vm;

    /* Connection Manager */
    Bindings.bind(checkBoxConnMgr, connManagerModel.getModel(ConnectionManager.PROPERTY_ENABLED));

    /* Connection Settings */
    Bindings.bind(chkClass1, connSettingsModel.getModel(ConnectionSettings.PROPERTY_CONNECT_ON_CLASS1));
    Bindings.bind(chkClass2, connSettingsModel.getModel(ConnectionSettings.PROPERTY_CONNECT_ON_CLASS2));
    Bindings.bind(chkClass3, connSettingsModel.getModel(ConnectionSettings.PROPERTY_CONNECT_ON_CLASS3));

    Bindings.bind(ftfActivityTimeout, connSettingsModel.getModel(ConnectionSettings.PROPERTY_ACTIVITY_TIMEOUT));
    Bindings.bind(ftfConnRetryDelay, connSettingsModel.getModel(ConnectionSettings.PROPERTY_CONNECT_RETRY_DELAY));

    /* Comms Device Power Cycle */
    Bindings.bind(checkBoxPowerCycle, pwrCycleModel.getModel(CommsPowerCycle.PROPERTY_ENABLED));

    /* Power Cycle Channel */
    bindPowerChannel();
    bindInhibit();
    Bindings.bind(ftfPowerCycleDuration, pwrCycleModel.getModel(CommsPowerCycle.PROPERTY_COMMS_POWER_CYCLE_DURATION));
    Bindings.bind(ftfInitTime, pwrCycleModel.getModel(CommsPowerCycle.PROPERTY_COMMS_INITTIME));

    /* IP Connectivity Check */
    Bindings.bind(checkBoxConnectivityCheck, connectivityCheckModel.getModel(IpConnectivityCheck.PROPERTY_ENABLED));
    Bindings.bind(ftfPeriod, connectivityCheckModel.getModel(IpConnectivityCheck.PROPERTY_PERIOD));
    Bindings.bind(ftfPort, connectivityCheckModel.getModel(IpConnectivityCheck.PROPERTY_PORT));
    Bindings.bind(ftfConnectivityRetries,
        connectivityCheckModel.getModel(IpConnectivityCheck.PROPERTY_CONNECTIVITY_RETRIES));

    /* Outgoing Connections */
    Bindings.bind(checkBoxOutoing, outgoingCheckModel.getModel(OutGoingCheck.PROPERTY_ENABLED));
    Bindings.bind(ftfOutingRetries, outgoingCheckModel.getModel(OutGoingCheck.PROPERTY_OUTGOING_RETRIES));

    /* Incoming Connections */
    Bindings.bind(checkBoxIncoming, incomingCheckModel.getModel(IncomingCheck.PROPERTY_ENABLED));
    Bindings.bind(comboBoxIncoming, new SelectionInList<Long>(IncomingCheck.PERIOD_MINS_OPTIONS,
        incomingCheckModel.getModel(IncomingCheck.PROPERTY_NOT_RECEIVE_PERIOD)));
    comboBoxIncoming.setRenderer(new NotReceiveComboRenderer());

    /* Binding visibility of components */
    vm = connManagerModel.getModel(ConnectionManager.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, panelCenter, "visible");

    vm = pwrCycleModel.getModel(CommsPowerCycle.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, panelPowerSupply, "visible");
    PropertyConnector.connectAndUpdate(vm, panelPowerCycle, "visible");

    ComponentGroup group;

    /* Binding enable state of components */
    vm = connectivityCheckModel.getModel(IpConnectivityCheck.PROPERTY_ENABLED);
    group = new ComponentGroup(ftfPeriod, ftfPort, ftfConnectivityRetries);
    PropertyConnector.connectAndUpdate(vm, group, ComponentGroup.PROPERTY_ENABLED);

    vm = outgoingCheckModel.getModel(OutGoingCheck.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, ftfOutingRetries, "enabled");

    vm = incomingCheckModel.getModel(IncomingCheck.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, comboBoxIncoming, "enabled");

    /* Binding Actions */
    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(this);
    btnAddServer.setAction(actions.get(ACTION_ADD_FAILOVER));
    btnRemoveServer.setAction(actions.get(ACTION_REMOVE_FAILOVER));
  }

  private void bindInhibit() {
    ValueModel vm;
    ListModel<VirtualPoint> binaryInputs = VirtualPointUtility.getBinaryInputsListModel(config);
    

    vm = pwrCycleModel.getModel(CommsPowerCycle.PROPERTY_COMMS_POWER_CYCLE_INHIBIT);
    Bindings.bind(comboxInhibit, new SelectionInList<>(binaryInputs, vm));
    ComboBoxUtil.makeAboveAndWider(comboxInhibit);
  }

  private void bindPowerChannel() {
    ListModel<IChannel> powerChannels = CANModuleUtility.getPowerChannelListModel(config);

    ValueModel vm = pwrCycleModel.getModel(CommsPowerCycle.PROPERTY_COMMS_POWER_SUPPLY);
    Bindings.bind(comboxCommsPowerSupply, new SelectionInList<IChannel>(powerChannels, vm));

    // Select a channel by default
    if (vm.getValue() == null && powerChannels.getSize() > 0) {
      vm.setValue(powerChannels.getElementAt(0));
    }
  }

  private void setBounaries() {
    FormattedTextFieldSupport support = new FormattedTextFieldSupport(SDNP3Constraints.INSTANCE);
    final String prefix = SDNP3Constraints.PREFIX_CHANNEL_CONNECTION;

    support.setBoundary(ftfPort, prefix, IpConnectivityCheck.PROPERTY_PORT);
    support.setBoundary(ftfConnectivityRetries, prefix, IpConnectivityCheck.PROPERTY_CONNECTIVITY_RETRIES);
    support.setBoundary(ftfPeriod, prefix, IpConnectivityCheck.PROPERTY_PERIOD);

    support.setBoundary(ftfPowerCycleDuration, prefix, CommsPowerCycle.PROPERTY_COMMS_POWER_CYCLE_DURATION);
    support.setBoundary(ftfInitTime, prefix, CommsPowerCycle.PROPERTY_COMMS_INITTIME);

    support.setBoundary(ftfActivityTimeout, prefix, ConnectionSettings.PROPERTY_ACTIVITY_TIMEOUT);
    support.setBoundary(ftfConnRetryDelay, prefix, ConnectionSettings.PROPERTY_CONNECT_RETRY_DELAY);

    support.setBoundary(ftfOutingRetries, prefix, OutGoingCheck.PROPERTY_OUTGOING_RETRIES);
  }

  private void createUIComponents() {
    ListModel<Failover> failoverListModel;
    if (connectionManager != null) {
      failoverListModel = connectionManager.getFailoverGroup().getFailoverListModel();
    } else {
      failoverListModel = new DefaultListModel<>();
    }

    tableServers = new JTable(new FailoverTableModel(failoverListModel));

    UIUtils.scrollToAddedRow(tableServers);
  }

  private void btnSelectInhibitActionPerformed() {
    CommsPowerCycle powerCycle = connectionManager.getCommsPowerCycle();
    VirtualPoint curSelection = powerCycle.getCommsPowerCycleInhibit();

    // Get all binary points
    ListModel<VirtualPoint> vpointsModel = VirtualPointUtility.getBinaryInputsListModel(config);
    ArrayList<VirtualPoint> vpoints = new ArrayList<>();
    for (int i = 0; i < vpointsModel.getSize(); i++) {
      vpoints.add(vpointsModel.getElementAt(i));
    }

    // Show selector dialog
    VirtualPointSelector selector = new VirtualPointSelector(WindowUtils.getMainFrame(), curSelection, vpoints);
    selector.setSubtitle("Select an inhibit binary point");
    selector.showDialog();

    if (selector.hasBeenAffirmed()) {
      VirtualPoint newSelection = selector.getSelectedPoint();
      if (newSelection != curSelection) {
        powerCycle.setCommsPowerCycleInhibit(newSelection);
      }
    }
  }

 

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    panel2 = new JPanel();
    checkBoxConnMgr = new JCheckBox();
    panelCenter = new JPanel();
    JPanel panelFailover = new JPanel();
    scrollPane2 = new JScrollPane();
    panel6 = new JPanel();
    btnAddServer = new JButton();
    btnRemoveServer = new JButton();
    JPanel panelConnSettings = new JPanel();
    JLabel lblConnectOnClass = new JLabel();
    panelClass = new JPanel();
    chkClass1 = new JCheckBox();
    chkClass2 = new JCheckBox();
    chkClass3 = new JCheckBox();
    JLabel lblActivityTimeout = new JLabel();
    ftfActivityTimeout = new JFormattedTextField();
    label8 = new JLabel();
    JLabel label1 = new JLabel();
    ftfConnRetryDelay = new JFormattedTextField();
    label2 = new JLabel();
    JPanel panel7 = new JPanel();
    panel1 = new JPanel();
    checkBoxPowerCycle = new JCheckBox();
    panelPowerSupply = new JPanel();
    JLabel lblCommsPowerSupply = new JLabel();
    comboxCommsPowerSupply = new JComboBox<>();
    JLabel lblCommsPowerSupply2 = new JLabel();
    comboxInhibit = new JComboBox();
    btnSelectInhibit = new JButton();
    JLabel lblPowerCycleLength = new JLabel();
    ftfPowerCycleDuration = new JFormattedTextField();
    JLabel lblPowerCycleLengthSeconds = new JLabel();
    JLabel label6 = new JLabel();
    ftfInitTime = new JFormattedTextField();
    JLabel label7 = new JLabel();
    panelPowerCycle = new JPanel();
    label3 = new JLabel();
    JPanel panel4 = new JPanel();
    checkBoxConnectivityCheck = new JCheckBox();
    lblPeriod = new JLabel();
    ftfPeriod = new JFormattedTextField();
    lblSeconds = new JLabel();
    lblPort = new JLabel();
    ftfPort = new JFormattedTextField();
    lblRetries = new JLabel();
    ftfConnectivityRetries = new JFormattedTextField();
    JPanel panel8 = new JPanel();
    checkBoxOutoing = new JCheckBox();
    label4 = new JLabel();
    ftfOutingRetries = new JFormattedTextField();
    JPanel panel9 = new JPanel();
    checkBoxIncoming = new JCheckBox();
    label5 = new JLabel();
    comboBoxIncoming = new JComboBox<>();

    // ======== this ========
    setLayout(new BorderLayout(0, 10));

    // ======== panel2 ========
    {
      panel2.setOpaque(false);
      panel2.setLayout(new FlowLayout(FlowLayout.LEADING));

      // ---- checkBoxConnMgr ----
      checkBoxConnMgr.setText("Enabled");
      checkBoxConnMgr.setOpaque(false);
      panel2.add(checkBoxConnMgr);
    }
    add(panel2, BorderLayout.NORTH);

    // ======== panelCenter ========
    {
      panelCenter.setOpaque(false);
      panelCenter.setLayout(new FormLayout(
          "default, $lcgap, default:grow",
          "2*(fill:default, $ugap), fill:default"));

      // ======== panelFailover ========
      {
        panelFailover.setBorder(new TitledBorder("Failover Group"));
        panelFailover.setOpaque(false);
        panelFailover.setLayout(new FormLayout(
            "default, $lcgap, default",
            "fill:default"));

        // ======== scrollPane2 ========
        {
          scrollPane2.setPreferredSize(new Dimension(302, 90));

          // ---- tableServers ----
          tableServers.setFillsViewportHeight(true);
          scrollPane2.setViewportView(tableServers);
        }
        panelFailover.add(scrollPane2, CC.xy(1, 1));

        // ======== panel6 ========
        {
          panel6.setOpaque(false);
          panel6.setLayout(new FormLayout(
              "default",
              "default, $lgap, default"));

          // ---- btnAddServer ----
          btnAddServer.setText("Add");
          panel6.add(btnAddServer, CC.xy(1, 1));

          // ---- btnRemoveServer ----
          btnRemoveServer.setText("Remove");
          panel6.add(btnRemoveServer, CC.xy(1, 3));
        }
        panelFailover.add(panel6, CC.xy(3, 1));
      }
      panelCenter.add(panelFailover, CC.xy(1, 1));

      // ======== panelConnSettings ========
      {
        panelConnSettings.setBorder(new TitledBorder("Connection Settings"));
        panelConnSettings.setOpaque(false);
        panelConnSettings.setLayout(new FormLayout(
            "3*(default, $lcgap), default:grow",
            "2*(default, $lgap), default"));

        // ---- lblConnectOnClass ----
        lblConnectOnClass.setText("Connect on Event:");
        panelConnSettings.add(lblConnectOnClass, CC.xy(1, 1));

        // ======== panelClass ========
        {
          panelClass.setOpaque(false);
          panelClass.setLayout(new FormLayout(
              "2*(default, $lcgap), default",
              "default"));

          // ---- chkClass1 ----
          chkClass1.setText("Class 1");
          chkClass1.setOpaque(false);
          panelClass.add(chkClass1, CC.xy(1, 1));

          // ---- chkClass2 ----
          chkClass2.setText("Class 2");
          chkClass2.setOpaque(false);
          panelClass.add(chkClass2, CC.xy(3, 1));

          // ---- chkClass3 ----
          chkClass3.setText("Class 3");
          chkClass3.setOpaque(false);
          panelClass.add(chkClass3, CC.xy(5, 1));
        }
        panelConnSettings.add(panelClass, CC.xywh(3, 1, 5, 1));

        // ---- lblActivityTimeout ----
        lblActivityTimeout.setText("Disconnect on Inactivity:");
        panelConnSettings.add(lblActivityTimeout, CC.xy(1, 3));

        // ---- ftfActivityTimeout ----
        ftfActivityTimeout.setPreferredSize(new Dimension(70, 20));
        ftfActivityTimeout.setColumns(8);
        panelConnSettings.add(ftfActivityTimeout, CC.xy(3, 3));

        // ---- label8 ----
        label8.setText("Seconds");
        panelConnSettings.add(label8, CC.xy(5, 3));

        // ---- label1 ----
        label1.setText("Connect Retry Delay:");
        panelConnSettings.add(label1, CC.xy(1, 5));
        panelConnSettings.add(ftfConnRetryDelay, CC.xy(3, 5));

        // ---- label2 ----
        label2.setText("Seconds");
        panelConnSettings.add(label2, CC.xy(5, 5));
      }
      panelCenter.add(panelConnSettings, CC.xy(1, 3));

      // ======== panel7 ========
      {
        panel7.setBorder(new TitledBorder("Comms Device Power Cycle"));
        panel7.setOpaque(false);
        panel7.setLayout(new BorderLayout());

        // ======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setLayout(new FlowLayout(FlowLayout.LEADING));

          // ---- checkBoxPowerCycle ----
          checkBoxPowerCycle.setText("Enabled");
          checkBoxPowerCycle.setOpaque(false);
          panel1.add(checkBoxPowerCycle);
        }
        panel7.add(panel1, BorderLayout.NORTH);

        // ======== panelPowerSupply ========
        {
          panelPowerSupply.setBorder(Borders.DLU4_BORDER);
          panelPowerSupply.setOpaque(false);
          panelPowerSupply.setLayout(new FormLayout(
              "3*(default, $lcgap), 60dlu, $lcgap, 10dlu, $lcgap, default",
              "3*(default, $lgap), default"));

          // ---- lblCommsPowerSupply ----
          lblCommsPowerSupply.setText("Comms Power Supply:");
          panelPowerSupply.add(lblCommsPowerSupply, CC.xy(1, 1));
          panelPowerSupply.add(comboxCommsPowerSupply, CC.xywh(3, 1, 5, 1));

          // ---- lblCommsPowerSupply2 ----
          lblCommsPowerSupply2.setText("Power Cycle Inhibit:");
          panelPowerSupply.add(lblCommsPowerSupply2, CC.xy(1, 3));

          // ---- comboxInhibit ----
          comboxInhibit.setEditable(false);
          panelPowerSupply.add(comboxInhibit, CC.xywh(3, 3, 5, 1));

          // ---- btnSelectInhibit ----
          btnSelectInhibit.setText("...");
          btnSelectInhibit.setToolTipText("Select an inhibit point");
          btnSelectInhibit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              btnSelectInhibitActionPerformed();
            }
          });
          panelPowerSupply.add(btnSelectInhibit, CC.xy(9, 3));

          // ---- lblPowerCycleLength ----
          lblPowerCycleLength.setText("Power Cycle Duration:");
          panelPowerSupply.add(lblPowerCycleLength, CC.xy(1, 5));

          // ---- ftfPowerCycleDuration ----
          ftfPowerCycleDuration.setColumns(8);
          panelPowerSupply.add(ftfPowerCycleDuration, CC.xy(3, 5));

          // ---- lblPowerCycleLengthSeconds ----
          lblPowerCycleLengthSeconds.setText("Seconds");
          panelPowerSupply.add(lblPowerCycleLengthSeconds, CC.xy(5, 5));

          // ---- label6 ----
          label6.setText("Initialisation Time:");
          panelPowerSupply.add(label6, CC.xy(1, 7));
          panelPowerSupply.add(ftfInitTime, CC.xy(3, 7));

          // ---- label7 ----
          label7.setText("Seconds");
          panelPowerSupply.add(label7, CC.xy(5, 7));
        }
        panel7.add(panelPowerSupply, BorderLayout.CENTER);

        // ======== panelPowerCycle ========
        {
          panelPowerCycle.setBorder(Borders.DLU4_BORDER);
          panelPowerCycle.setOpaque(false);
          panelPowerCycle.setLayout(new FormLayout(
              "2*(default, $lcgap), default",
              "default, $lgap, fill:default"));
          ((FormLayout) panelPowerCycle.getLayout()).setColumnGroups(new int[][] { { 1, 3, 5 } });

          // ---- label3 ----
          label3.setText("- Power Cycle Conditions -");
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 2f));
          panelPowerCycle.add(label3, CC.xywh(1, 1, 5, 1));

          // ======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setBorder(new TitledBorder("Ip Connectivity Check"));
            panel4.setLayout(new FormLayout(
                "2*(default, $lcgap), default",
                "3*(default, $lgap), default"));

            // ---- checkBoxConnectivityCheck ----
            checkBoxConnectivityCheck.setText("Enabled");
            checkBoxConnectivityCheck.setOpaque(false);
            panel4.add(checkBoxConnectivityCheck, CC.xywh(1, 1, 3, 1));

            // ---- lblPeriod ----
            lblPeriod.setText("Period:");
            panel4.add(lblPeriod, CC.xy(1, 3));

            // ---- ftfPeriod ----
            ftfPeriod.setPreferredSize(new Dimension(210, 20));
            ftfPeriod.setColumns(8);
            panel4.add(ftfPeriod, CC.xy(3, 3));

            // ---- lblSeconds ----
            lblSeconds.setText("Minutes");
            panel4.add(lblSeconds, CC.xy(5, 3));

            // ---- lblPort ----
            lblPort.setText("Port:");
            panel4.add(lblPort, CC.xy(1, 5));

            // ---- ftfPort ----
            ftfPort.setPreferredSize(new Dimension(70, 20));
            ftfPort.setColumns(8);
            ftfPort.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            panel4.add(ftfPort, CC.xy(3, 5));

            // ---- lblRetries ----
            lblRetries.setText("Retries:");
            panel4.add(lblRetries, CC.xy(1, 7));

            // ---- ftfConnectivityRetries ----
            ftfConnectivityRetries.setColumns(8);
            panel4.add(ftfConnectivityRetries, CC.xy(3, 7));
          }
          panelPowerCycle.add(panel4, CC.xy(1, 3));

          // ======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setBorder(new TitledBorder("Outgoing Connections"));
            panel8.setLayout(new FormLayout(
                "default, $lcgap, default",
                "2*(default, $lgap), default"));

            // ---- checkBoxOutoing ----
            checkBoxOutoing.setText("Enabled");
            checkBoxOutoing.setOpaque(false);
            panel8.add(checkBoxOutoing, CC.xywh(1, 1, 3, 1));

            // ---- label4 ----
            label4.setText("Retries:");
            panel8.add(label4, CC.xy(1, 3));

            // ---- ftfOutingRetries ----
            ftfOutingRetries.setColumns(8);
            panel8.add(ftfOutingRetries, CC.xy(3, 3));
          }
          panelPowerCycle.add(panel8, CC.xy(3, 3));

          // ======== panel9 ========
          {
            panel9.setOpaque(false);
            panel9.setBorder(new TitledBorder("Incoming Connections"));
            panel9.setLayout(new FormLayout(
                "default, $lcgap, [50dlu,default]",
                "2*(default, $lgap), default"));

            // ---- checkBoxIncoming ----
            checkBoxIncoming.setText("Enabled");
            checkBoxIncoming.setOpaque(false);
            panel9.add(checkBoxIncoming, CC.xywh(1, 1, 3, 1));

            // ---- label5 ----
            label5.setText("Not Received for:");
            panel9.add(label5, CC.xy(1, 3));
            panel9.add(comboBoxIncoming, CC.xy(3, 3));
          }
          panelPowerCycle.add(panel9, CC.xy(5, 3));
        }
        panel7.add(panelPowerCycle, BorderLayout.SOUTH);
      }
      panelCenter.add(panel7, CC.xywh(1, 5, 3, 1));
    }
    add(panelCenter, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel2;
  private JCheckBox checkBoxConnMgr;
  private JPanel panelCenter;
  private JScrollPane scrollPane2;
  private JTable tableServers;
  private JPanel panel6;
  private JButton btnAddServer;
  private JButton btnRemoveServer;
  private JPanel panelClass;
  private JCheckBox chkClass1;
  private JCheckBox chkClass2;
  private JCheckBox chkClass3;
  private JFormattedTextField ftfActivityTimeout;
  private JLabel label8;
  private JFormattedTextField ftfConnRetryDelay;
  private JLabel label2;
  private JPanel panel1;
  private JCheckBox checkBoxPowerCycle;
  private JPanel panelPowerSupply;
  private JComboBox<Object> comboxCommsPowerSupply;
  private JComboBox comboxInhibit;
  private JButton btnSelectInhibit;
  private JFormattedTextField ftfPowerCycleDuration;
  private JFormattedTextField ftfInitTime;
  private JPanel panelPowerCycle;
  private JLabel label3;
  private JCheckBox checkBoxConnectivityCheck;
  private JLabel lblPeriod;
  private JFormattedTextField ftfPeriod;
  private JLabel lblSeconds;
  private JLabel lblPort;
  private JFormattedTextField ftfPort;
  private JLabel lblRetries;
  private JFormattedTextField ftfConnectivityRetries;
  private JCheckBox checkBoxOutoing;
  private JLabel label4;
  private JFormattedTextField ftfOutingRetries;
  private JCheckBox checkBoxIncoming;
  private JLabel label5;
  private JComboBox<Object> comboBoxIncoming;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static class NotReceiveComboRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list,
        Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected,
          cellHasFocus);
      if (value != null) {
        try {
          int hours = Integer.valueOf(value.toString());
          String displayText;
          switch (hours) {
          case 1:
            displayText = "1 Hour";
            break;
          case 24:
            displayText = "1 Day";
            break;
          case 48:
            displayText = "2 Days";
            break;
          case 72:
            displayText = "3 Days";
            break;
          case 168:
            displayText = "1 Week";
            break;
          default:
            displayText = hours + " Hours";
            break;
          }

          setText(displayText);

        } catch (Exception e) {
          Logger.getLogger(getClass()).error(e);
        }
      }

      return comp;
    }

  }
}
