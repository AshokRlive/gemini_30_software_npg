/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import java.util.ArrayList;

/**
 * The Enum Protocol Stack Type.
 */
public enum ProtocolType {
  MMB("Modbus Master", false),
  SDNP3("DNP3 Slave",  true),
  S104("IEC104 Slave", true),
  S101("IEC101 Slave", true);

  ProtocolType(String name, boolean isSlave) {
    this.name = name;
    this.isSlave = isSlave;
    
    if(isSlave)
      Constants.slaves.add(this);
    else
      Constants.masters.add(this);
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return getName();
  }


  
  public boolean isSlave() {
    return isSlave;
  }


  private final String name;
  private final boolean isSlave;
  
  public static ProtocolType[] getAllSlaveProtocolTypes() {
    return Constants.slaves.toArray(new ProtocolType[Constants.slaves.size()]);
  }
  
  public static ProtocolType[] getAllMasterProtocolTypes() {
    return Constants.masters.toArray(new ProtocolType[Constants.masters.size()]);
  }
  
  private static class Constants {
    private static final ArrayList<ProtocolType> slaves = new ArrayList<>();
    private static final ArrayList<ProtocolType> masters = new ArrayList<>();

  }
}
