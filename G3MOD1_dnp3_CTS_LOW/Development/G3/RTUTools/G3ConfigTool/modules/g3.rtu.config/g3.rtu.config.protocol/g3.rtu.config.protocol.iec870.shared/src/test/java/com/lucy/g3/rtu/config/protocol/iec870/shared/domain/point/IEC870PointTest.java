/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870PointFactory;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.stub.ScadaIoMapStub;

/**
 * The Class IEC870PointTest.
 */
public class IEC870PointTest {

  private IScadaIoMap<IEC870Point> iomap;
  private final IEC870PointType[] allTypes = IEC870PointType.values();
  

  @Before
  public void setUp() throws Exception {
    iomap = new ScadaIoMapStub<IEC870Point>();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCreatePoint() {
    for (int i = 0; i < allTypes.length; i++) {
      IEC870PointFactory.create(iomap, allTypes[i], 0);
    }
  }

}
