/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import com.g3schema.ns_iec870.IEC870PointsT;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.iec870.shared.xml.IEC870IoMapXmlProcessor;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * The IO map of IEC870.
 */
public final class IEC870IoMap extends AbstractScadaIoMap<IEC870Point> {

  public IEC870IoMap(IProtocolSession session) {
    super(session, IEC870PointType.values());
  }

  
  /**
   * Override to return non-zero IOA for IEC104.
   */
  @Override
  public long findAvailablePointID(ScadaPointType type) {
    long id = super.findAvailablePointID(type);
    if(id == 0) {
      id = super.findAvailablePointID(id+1, type);
    }
    return id;
  }
  
  @Override
  public void readFromXML(Object xmlObj, ScadaPointSourceRepository resource) {
    IEC870PointsT xml = (IEC870PointsT) xmlObj;
    new IEC870IoMapXmlProcessor(this).readFromXML(xml, resource);
  }

  @Override
  public void writeToXML(Object xmlObj) {
    IEC870PointsT xml = (IEC870PointsT) xmlObj;
    new IEC870IoMapXmlProcessor(this).writeToXML(xml);
  }

}
