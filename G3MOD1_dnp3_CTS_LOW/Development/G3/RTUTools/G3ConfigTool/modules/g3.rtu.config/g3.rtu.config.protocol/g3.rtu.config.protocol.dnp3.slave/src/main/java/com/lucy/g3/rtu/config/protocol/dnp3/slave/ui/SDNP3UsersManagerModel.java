/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;

import org.jdesktop.application.Action;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionSingleSelection;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3User;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthUsersConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.SDNP3UserEditorDialog.SDNP3UserEditorDialogCallback;


/**
 * The presentation model for managing DNP3 users.
 */
public class SDNP3UsersManagerModel implements SDNP3UserEditorDialogCallback {
  public final static String ACTION_ADD = "addUser";
  public final static String ACTION_EDIT = "editUser";
  public final static String ACTION_REMOVE = "removeUser";

  private final MultiSelectionInList<SDNP3User> selctionList;
  private final MultiSelectionSingleSelection singleSelection;
  private final MultiListSelectionAdapter<SDNP3User> selectionModel;
  
  private final TableModel usersTableModel;
  private final SDNPAuthUsersConfig usersConfig;
  
  public SDNP3UsersManagerModel(SDNPAuthUsersConfig usersConfig) {
    this.usersConfig = usersConfig;
    selctionList = new MultiSelectionInList<SDNP3User>(usersConfig.getItemListModel());
    selectionModel = new MultiListSelectionAdapter<SDNP3User>(selctionList);
    singleSelection = new MultiSelectionSingleSelection(selctionList.getSelection());
    usersTableModel = new SDNP3UsersTableModel(usersConfig.getItemListModel());
  }

  public TableModel getUsersTableModel() {
    return usersTableModel;
  }

  public MultiSelectionInList<SDNP3User> getSelctionList() {
    return selctionList;
  }
  
  public ListModel<SDNP3User> getUsersListModel() {
    return selctionList.getList();
  }


  public MultiSelectionSingleSelection getSingleSelection() {
    return singleSelection;
  }

  public MultiListSelectionAdapter<SDNP3User> getSelectionModel() {
    return selectionModel;
  }
  
  @Action
  public void addUser() {
    SDNP3User newUser = new SDNP3User();
    int num = findAvailableUserNumber();
    if(num >= SDNP3User.MIN_USER_NUMBER && num <= SDNP3User.MAX_USER_NUMBER) {
      newUser.setUserNumber(num);
      newUser.setUserName("New User");
      
//      // Select new created user
//      int size = selctionList.getList().getSize();
//      getSelectionModel().setSelectionInterval(size -1, size - 1);
      
      if(editUser(newUser))
        usersConfig.add(newUser);
      
    } else {
      JOptionPane.showMessageDialog(null, "Cannot add more user!");
    }
  }
  
  /**
   * @return
   */
  private int findAvailableUserNumber() {
    Collection<SDNP3User> existingUsers = usersConfig.getAllItems();
    boolean unused;
    for (int i = SDNP3User.MIN_USER_NUMBER; i < SDNP3User.MAX_USER_NUMBER; i++) {
      unused = true;
      
      for (SDNP3User u : existingUsers) {
        if(u.getUserNumber() == i) {
          unused = false;
          break;
        }
      }
      
      if(unused)
        return i;
    }
    return SDNP3User.MIN_USER_NUMBER - 1; // return invalid user number
  }

  private boolean editUser(SDNP3User user) {
    SDNP3UserEditorDialog dialog = new SDNP3UserEditorDialog(WindowUtils.getMainFrame(),  user, this);
    dialog.pack();
    dialog.setVisible(true);
    return dialog.hasBeenAffirmed();
  }

  @Action
  public void editUser() {
    SDNP3User selectedUser = (SDNP3User)singleSelection.getValue();
    if(selectedUser != null) {
      editUser(selectedUser);
    }
  }
  
  @Action
  public void removeUser(ActionEvent e) {
    int rc = JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor((Component) e.getSource()),
        "Do you want to remove the selection?",
        "Remove",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    if (rc == JOptionPane.YES_OPTION) {
      ArrayList<SDNP3User> selections = new ArrayList<>(selctionList.getSelection());
      usersConfig.removeAll(selections);
    }
  }
  
  private static final class SDNP3UsersTableModel extends AbstractTableAdapter<SDNP3User> {

    private static final String[] COLUMNS =
    { "User Number", "User Name" , "Role",  "Key Wrap Algorithm", "Update Key"};

    private static final int COLUMN_NUMBER = 0;
    private static final int COLUMN_NAME = 1;
    private static final int COLUMN_ROLE = 2;
    private static final int COLUMN_KEY_ALOG = 3;
    private static final int COLUMN_KEY = 4;


    public SDNP3UsersTableModel(ListModel<SDNP3User> listModel) {
      super(listModel, COLUMNS);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

      SDNP3User user = getRow(rowIndex);

      switch (columnIndex) {
      case COLUMN_KEY_ALOG:
        return user.getKeyWrapAlgorithm();
        
      case COLUMN_KEY:
        return user.getUpdateKey();

      case COLUMN_NAME:
        return user.getUserName();

      case COLUMN_NUMBER:
        return user.getUserNumber();

      case COLUMN_ROLE:
        return user.getUserRole();

      default:
        throw new IllegalStateException("Unknown column");
      }
    }

  }

  @Override
  public Collection<SDNP3User> getExistingUsers() {
    return usersConfig.getAllItems();
  }

}

