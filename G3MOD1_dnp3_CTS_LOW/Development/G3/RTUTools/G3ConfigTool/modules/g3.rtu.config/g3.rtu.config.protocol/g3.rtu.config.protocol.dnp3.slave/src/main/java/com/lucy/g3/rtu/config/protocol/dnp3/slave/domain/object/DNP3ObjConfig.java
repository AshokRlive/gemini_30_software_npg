/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.object;

import java.util.Arrays;

import org.jdesktop.application.AbstractBean;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session.GlobalVariationsConfig;

/**
 * DNP3 object event configuration.
 */
public class DNP3ObjConfig extends AbstractBean {

  public static final String PROPERTY_ENABLECLASS0 = "enableClass0";

  public static final String PROPERTY_STATIC_VARIATION = "staticVariation";

  public static final String PROPERTY_EVENT_VARIATION = "eventVariation";

  //public static final String PROPERTY_CMD_EVENT_VARIATION = "cmdEventVariation";

  public static final String PROPERTY_EVENT_CLASS = "eventClass";

  public static final String PROPERTY_EVENT_ONLY_WHEN_CONNECTED = "eventOnlyWhenConnected";

  /**
   * The name of property {@value} , which indicates whether or not applying
   * custom variation to this point. <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_CUSTOM_VARIATION_ENABLED = "customVariationEnabled";

  private long staticVariation;
  private long eventVariation;
  private boolean customVariationEnabled;
  //private long cmdEventVariation = 0;

  private long eventClass = 1;
  private boolean enableClass0 = true;
  private boolean eventOnlyWhenConnected = false;

  private final GlobalVariationsConfig globalVariation;
  private final DNP3ObjGroup group;

  private final boolean hasStaticVariation;
  private final boolean hasEventVariation;
//  private final boolean hasCmdVariation;


  public DNP3ObjConfig(GlobalVariationsConfig globalVariation, DNP3ObjGroup objGroup) {
    this(globalVariation, objGroup, true, true);
  }

  public DNP3ObjConfig(GlobalVariationsConfig globalVariation, DNP3ObjGroup objGroup, 
      boolean hasStaticVariation, boolean hasEventVariation) {
    super();
    this.globalVariation = globalVariation;
    this.group = objGroup;
    this.hasEventVariation = hasEventVariation;
    this.hasStaticVariation = hasStaticVariation;
    
    this.customVariationEnabled = false;
    this.staticVariation = getStaticVariation();
    this.eventVariation = getEventVariation();
  }

  public boolean isHasStaticVariation() {
    return hasStaticVariation;
  }

  public boolean isHasEventVariation() {
    return hasEventVariation;
  }

  public DNP3ObjGroup getGroup() {
    return group;
  }

  public Long getGlobalStaticVariation() {
    return globalVariation == null ? 0L  : DNP3ObjVarConsts
        .getGlobalStaticVariation(globalVariation, group);
  }

  public Long getGlobalEventVariation() {
    return globalVariation == null ? 0L : DNP3ObjVarConsts
        .getGlobalEventVariation(globalVariation, group);
  }

  public long getStaticVariation() {
    if (!isCustomVariationEnabled()) {
      return getGlobalStaticVariation();
    }

    return staticVariation;
  }

  public void setStaticVariation(long staticVariation) {
    Object oldValue = getStaticVariation();
    this.staticVariation = staticVariation;
    firePropertyChange(PROPERTY_STATIC_VARIATION, oldValue, staticVariation);
  }

  public long getEventVariation() {
    if (!isCustomVariationEnabled()) {
      return getGlobalEventVariation();
    }

    return eventVariation;
  }

  public void setEventVariation(long eventVariation) {
    Object oldValue = getEventVariation();
    this.eventVariation = eventVariation;
    firePropertyChange(PROPERTY_EVENT_VARIATION, oldValue, eventVariation);
  }

//  public long getCmdEventVariation() {
//    return cmdEventVariation;
//  }
//
//  public void setCmdEventVariation(long cmdEventVariation) {
//    Object oldValue = this.cmdEventVariation;
//    this.cmdEventVariation = cmdEventVariation;
//    firePropertyChange(PROPERTY_CMD_EVENT_VARIATION, oldValue, cmdEventVariation);
//  }

  public long getEventClass() {
    return eventClass;
  }

  public void setEventClass(long eventClass) {
    Object oldValue = getEventClass();
    this.eventClass = eventClass;
    firePropertyChange(PROPERTY_EVENT_CLASS, oldValue, eventClass);
  }

  public boolean isEnableClass0() {
    return enableClass0;
  }

  public void setEnableClass0(boolean enableClass0) {
    Object oldValue = isEnableClass0();
    this.enableClass0 = enableClass0;
    firePropertyChange(PROPERTY_ENABLECLASS0, oldValue, enableClass0);
  }

  public boolean isCustomVariationEnabled() {
    return customVariationEnabled;
  }

  public void setCustomVariationEnabled(boolean enabled) {
    Object oldValue = isCustomVariationEnabled();
    this.customVariationEnabled = enabled;
    firePropertyChange(PROPERTY_CUSTOM_VARIATION_ENABLED, oldValue, enabled);
  }

  public boolean isEventOnlyWhenConnected() {
    return eventOnlyWhenConnected;
  }

  public void setEventOnlyWhenConnected(boolean eventOnlyWhenConnected) {
    Object oldValue = this.eventOnlyWhenConnected;
    this.eventOnlyWhenConnected = eventOnlyWhenConnected;
    firePropertyChange(PROPERTY_EVENT_ONLY_WHEN_CONNECTED, oldValue,
        eventOnlyWhenConnected);
  }

  /** Gets the event classes for selection. */
  public static final Long[] getEventClassItems() {
    return Arrays.copyOf(EVENT_CLASS_ITEMS, EVENT_CLASS_ITEMS.length);
  }

  /** Gets the description of event classes for selection. */
  public static final String[] getEventClassDescriptItems() {
    return Arrays.copyOf(EVENT_CLASS_ITEMS_DESCRIPTION, EVENT_CLASS_ITEMS_DESCRIPTION.length);
  }


  private static final Long[] EVENT_CLASS_ITEMS = {
      new Long(0),
      new Long(1),
      new Long(2),
      new Long(3)
  };

  private static final String[] EVENT_CLASS_ITEMS_DESCRIPTION = {
      "Class None",
      "Class 1",
      "Class 2",
      "Class 3",
  };
}
