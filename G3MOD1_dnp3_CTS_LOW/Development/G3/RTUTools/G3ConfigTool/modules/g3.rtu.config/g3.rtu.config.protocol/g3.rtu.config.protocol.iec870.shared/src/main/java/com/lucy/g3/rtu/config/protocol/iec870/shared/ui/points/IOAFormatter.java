package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import java.text.ParseException;

import javax.swing.text.DefaultFormatter;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_IOA_FORMAT;

/**
 * The Class IOAFormatter.
 */
public final class IOAFormatter extends DefaultFormatter {
    private static IOAFormatter INSTANCE;
    
    private static long IOA_MAX = IEC870PointType.MAX_ID;
    private static long IOA_MIN = IEC870PointType.MIN_ID;
    
    public static final String REGEX_IOA_8_8_8 =
        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    public static final String REGEX_IOA_8_16 =
        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." 
            + "([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$";
    
    public static final String REGEX_IOA_16_8 =
        "^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])\\." 
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    private LU_IOA_FORMAT ioaFormat = LU_IOA_FORMAT.LU_IOA_FORMAT_NONE;
    
    IOAFormatter() {
      super();
      setCommitsOnValidEdit(true);
      setAllowsInvalid(true);
      setOverwriteMode(false);
    }
    
    public void setIoaFormat(LU_IOA_FORMAT ioaFormat) {
      if (ioaFormat != null)
        this.ioaFormat = ioaFormat;
    }

    @Override
    public String valueToString(Object value) throws ParseException {
      if (value == null)
        return null;
      
      long ioaValue = Long.valueOf(value.toString());
      String ioaStr;
      
      /* Parse formatted IOA*/ 
      switch (ioaFormat) {
      case LU_IOA_FORMAT_16_8:
        ioaStr = ioaToString_16_8(ioaValue);
        break;
      case LU_IOA_FORMAT_8_16:
        ioaStr = ioaToString_8_16(ioaValue);
        break;
      case LU_IOA_FORMAT_8_8_8:
        ioaStr = ioaToString_8_8_8(ioaValue);
        break;
      default:
        ioaStr = Long.toString(ioaValue);
        break;
      }
      
      return ioaStr;
    }
    
    private String ioaToString_8_16(long ioaValue) {
      return String.format("%d.%d", ioaValue >> 16, ioaValue & 0x00FFFF); 
    }

    private String ioaToString_16_8(long ioaValue) {
      return String.format("%d.%d", ioaValue >> 8, ioaValue & 0x00FF); 
    }

    private String ioaToString_8_8_8(long ioaValue) {
      return String.format("%d.%d.%d", ioaValue >> 16, (ioaValue >> 8) & 0x00FF, ioaValue & 0x0000FF);
    }


    @Override
    public Long stringToValue(String text) throws ParseException {
      if (Strings.isBlank(text))
        throw new ParseException("Cannot parse blank text", 0);
      
      Long ioaValue = null;
      
      /* Parse integer IOA*/
      if (!text.contains(".")) {
        try {
          ioaValue = Long .parseLong(text);
          checkBoundary(ioaValue);
          return ioaValue;
        } catch (Throwable e) {
          throw new ParseException("Invalid text:" + text, 0);
        }
      }
      
      /* Parse formatted IOA*/ 
      switch (ioaFormat) {
      case LU_IOA_FORMAT_16_8:
        ioaValue = stringToIOA_16_8(text);
        break;
      case LU_IOA_FORMAT_8_16:
        ioaValue = stringToIOA_8_16(text);
        break;
      case LU_IOA_FORMAT_8_8_8:
        ioaValue = stringToIOA_8_8_8(text);
        break;
      default:
        break;
      }
      
      if (ioaValue != null)
        checkBoundary(ioaValue);
      else
        throw new ParseException("Invalid text:" + text, 0);
      
      return ioaValue;
     
    }

    private void checkBoundary(long  ioaValue) throws ParseException {
      if (ioaValue < IOA_MIN || ioaValue > IOA_MAX)
        throw new ParseException(String.format("IOA:%d is out of boundary:[%d,%d]", ioaValue, IOA_MIN, IOA_MAX), 0);
    }

    private void checkRegex(String ioaText, String ioaRegex) throws ParseException {
      if (!ioaText.matches(ioaRegex)) {
        throw new ParseException("Invalid IOA:" + ioaText, 0);
      }
    }

    private long stringToIOA_16_8(String text)  throws ParseException {
      checkRegex(text, REGEX_IOA_16_8);
      String[] tokens = text.split("\\.");
      int ioa16 = Integer.parseInt(tokens[0]) << 8;
      int ioa8  = Integer.parseInt(tokens[1]);
      
      return ioa16 | ioa8;
    }
    
    private long stringToIOA_8_16(String text)  throws ParseException {
      checkRegex(text, REGEX_IOA_8_16);
      
      String[] tokens = text.split("\\.");
      int ioa8 = Integer.parseInt(tokens[0]) << 16;
      int ioa16  = Integer.parseInt(tokens[1]);
      
      return ioa8 | ioa16;
    }
    
    private long stringToIOA_8_8_8(String text) throws ParseException {
      checkRegex(text, REGEX_IOA_8_8_8);
      String[] tokens = text.split("\\.");
      int ioa8_2  = Integer.parseInt(tokens[0]) << 16;
      int ioa8_1  = Integer.parseInt(tokens[1]) << 8;
      int ioa8_0  = Integer.parseInt(tokens[2]);
      
      return ioa8_2 | ioa8_1 | ioa8_0;
    }
    
    public static IOAFormatter getInstance() {
      if(INSTANCE == null) {
        INSTANCE = new IOAFormatter();
      }
      
      return INSTANCE;
  }
  }