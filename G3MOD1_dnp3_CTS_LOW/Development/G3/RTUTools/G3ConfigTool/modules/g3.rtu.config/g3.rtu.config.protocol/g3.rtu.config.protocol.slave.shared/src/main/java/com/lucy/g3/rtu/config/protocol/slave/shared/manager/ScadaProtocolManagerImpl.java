/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.manager;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ListModel;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.clogic.CLogicRepository;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.manager.AbstractProtocolManager;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointRepository;

/**
 * Manages the configured slave and master protocol. The change of the protocol
 * can be observed via property {@code PROPERTY_MASTER} and
 * {@code PROPERTY_SLAVE}.
 */
public final class ScadaProtocolManagerImpl extends AbstractProtocolManager<IScadaProtocol<?>>
    implements ScadaProtocolManager {

  private ScadaPointSourceRepository sourceRepository;


  public ScadaProtocolManagerImpl(IConfig owner) {
    super(owner);
    
    
  }

  /**
   * Gets all Input SCADA points.
   *
   * @return
   */
  @Override
  public Collection<ScadaPoint> getAllPoints() {
    ArrayList<ScadaPoint> allPoints = new ArrayList<ScadaPoint>();

    Collection<IScadaProtocol<?>> protocols = getAllItems();
    for (IScadaProtocol<?> proto : protocols) {
      allPoints.addAll(proto.getAllPoints());
    }

    return allPoints;
  }

  @Override
  public void removeAllPoints(Collection<ScadaPoint> ppoints) {
    Collection<IScadaProtocol<?>> protocols = getAllItems();
    for (IScadaProtocol<?> proto : protocols) {
      proto.removeAllPoints(ppoints);
    }
  }

  @Override
  public Collection<ScadaPoint> getAllInputPoints() {
    ArrayList<ScadaPoint> allPoints = new ArrayList<ScadaPoint>();

    Collection<IScadaProtocol<?>> protocols = getAllItems();
    for (IScadaProtocol<?> proto : protocols) {
      allPoints.addAll(proto.getAllInputPoints());
    }
    return allPoints;
  }


  @SuppressWarnings("unchecked")
  @Override
  public ScadaPointSourceRepository getSourceRepository() {
    if(sourceRepository == null) {
      IConfig config = getOwner();
      
      ListModel<VirtualPoint> pointsModel = null;
      ListModel<ICLogic> clogicModel = null;
      
      if(config != null) {
        VirtualPointRepository repo1 
          =  config.getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);
        pointsModel = repo1 == null ? null : repo1.getAllPointsModel();
        
        CLogicRepository repo2 =  config.getConfigModule(CLogicRepository.CONFIG_MODULE_ID);
        clogicModel = repo2 == null ? null : repo2.getAllClogicListModel();
      } 
      
      if(pointsModel == null)
        pointsModel = new ArrayListModel<VirtualPoint>(0);
      
      if(clogicModel == null)
        clogicModel = new ArrayListModel<ICLogic>(0);
      
      sourceRepository = new ScadaPointSourceRepository(pointsModel, clogicModel);
    }
    
    return sourceRepository;
  }

  @Override
  public IScadaProtocol<?> getProtocol(ProtocolType type) {
    Collection<IScadaProtocol<?>> protocols = getAllItems();
    for (IScadaProtocol<?> proto : protocols) {
      if (proto.getProtocolType() == type) {
        return proto;
      }
    }
    return null;
  }

  @Override
  public String getName() {
    return "SCADA Protocol Manager";
  }

}
