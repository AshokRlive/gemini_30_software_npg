/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.panels;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.EventsConfig;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
public class EventPanelDI extends JXTaskPane {
  private EventsConfig bi, dbi, ct;
  
  @SuppressWarnings("unused")
  private EventPanelDI(){
    super("Digital Point Events");
    this.bi = new EventsConfig(IEC870Constraints.INSTANCE_101);
    this.dbi = new EventsConfig(IEC870Constraints.INSTANCE_101);
    this.ct = new EventsConfig(IEC870Constraints.INSTANCE_101);
    initComponents();
  }
  
  public EventPanelDI(EventsConfig bi, EventsConfig dbi, EventsConfig ct) {
    super("Digital Point Events");
    this.bi = bi;
    this.dbi = dbi;
    this.ct = ct;
    initComponents();
  }

  private void createUIComponents() {
    panelEventBI = new EventConfigPanel(bi);
    panelEventDBI = new EventConfigPanel(dbi);
    panelEventCT = new EventConfigPanel(ct);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label18 = new JLabel();
    label21 = new JLabel();
    label24 = new JLabel();
    panel3 = new JPanel();
    label27 = new JLabel();
    label28 = new JLabel();
    label3 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
      "3*(default, $ugap), default",
      "default, $lgap, fill:default"));

    //---- label18 ----
    label18.setText("- Single Point-");
    label18.setHorizontalAlignment(SwingConstants.CENTER);
    add(label18, CC.xy(3, 1));

    //---- label21 ----
    label21.setText("-  Double Point -");
    label21.setHorizontalAlignment(SwingConstants.CENTER);
    add(label21, CC.xy(5, 1));

    //---- label24 ----
    label24.setText("- Integrated Total -");
    label24.setHorizontalAlignment(SwingConstants.CENTER);
    add(label24, CC.xy(7, 1));
    add(panelEventBI, CC.xy(3, 3));
    add(panelEventDBI, CC.xy(5, 3));

    //======== panel3 ========
    {
      panel3.setOpaque(false);
      panel3.setLayout(new FormLayout(
        "default",
        "2*(fill:default:grow, $lgap), bottom:default:grow"));

      //---- label27 ----
      label27.setText("Max Events:");
      panel3.add(label27, CC.xy(1, 1));

      //---- label28 ----
      label28.setText("Event Mode:");
      panel3.add(label28, CC.xy(1, 3));

      //---- label3 ----
      label3.setText("Time Format:");
      panel3.add(label3, CC.xy(1, 5));
    }
    add(panel3, CC.xy(1, 3));
    add(panelEventCT, CC.xy(7, 3));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label18;
  private JLabel label21;
  private JLabel label24;
  private EventConfigPanel panelEventBI;
  private EventConfigPanel panelEventDBI;
  private JPanel panel3;
  private JLabel label27;
  private JLabel label28;
  private JLabel label3;
  private EventConfigPanel panelEventCT;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
