/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.table.TableColumnExt;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.IScadaPointFactory;

/**
 * The dialog for adding a bulk of Scada Points.
 *
 * @param <PointT>
 *          Scada Point type
 */
public final class ScadaPointBulkAddDialog<PointT extends ScadaPoint> extends AbstractDialog {

  public static final int COLUMN_SELECT        = ScadaPointSourceTableModel.COLUMN_SELECT;
  public static final int COLUMN_RESOURCE_NAME = ScadaPointSourceTableModel.COLUMN_RESOURCE_NAME;
  public static final int COLUMN_POINT_ID      = ScadaPointSourceTableModel.COLUMN_POINT_ID;
  public static final int COLUMN_DESCRIPTION   = ScadaPointSourceTableModel.COLUMN_DESCRIPTION;   
  
  private Logger log = Logger.getLogger(ScadaPointBulkAddDialog.class);
  
  private final ScadaPointType type;
  private final IScadaIoMap<PointT> iomap;
  private final ScadaPointSourceTableModel sourceTableModel;
  private final IScadaPointFactory<PointT> factory;
  private final IIdChecker idChecker;

  private final ArrayList<PointT> createdPoints = new ArrayList<PointT>();

  
  public ScadaPointBulkAddDialog(Frame owner, List<ScadaPointSource> sources, ScadaPointType type,
      IScadaIoMap<PointT> iomap, IScadaPointFactory<PointT> factory) {
    super(owner, true);
    this.factory = Preconditions.checkNotNull(factory, "factory must not be null");
    this.type = Preconditions.checkNotNull(type, "type must not be null");
    this.iomap = iomap;
    this.idChecker = new ScadaPointIdChecker(iomap, type);
    this.sourceTableModel = new ScadaPointSourceTableModel(type, sources, idChecker);

    initComponents();
    this.sourceTableModel.setSequentialIdEnabled(checkBoxSeqID.isSelected());

    // Set title
    String protocolName = iomap == null ? "" : iomap.getSession().getProtocolType().getName();
    if (!Strings.isBlank(protocolName)) {
      protocolName = "[" + protocolName + "]";
    }
    setTitle(String.format("%s Bulk Add SCADA Points - %s", protocolName, type.getDescription()));

    pack();
  }

  public void setColumnTitle(int column, String title) {
    sourceTableModel.setColumnTitle(column, title);
    
    if (column >= 0 && column < sourceTable.getColumnCount()) {
      sourceTable.getColumn(column).setHeaderValue(title);
      sourceTable.getTableHeader().repaint();
    }
  }
  
  @Override
  public void ok() {
    if (createAndAddProtocolPoints()) {
      super.ok();
    }
  }

  public ArrayList<PointT> getCreatedPoints() {
    return createdPoints;
  }

  /**
   * Create and add Scada points from the selected mapping resource.
   */
  private boolean createAndAddProtocolPoints() {
    // Stop editing
    TableCellEditor editor = sourceTable.getCellEditor();
    if (editor != null) {
      editor.stopCellEditing();
    }

    // Check selection
    if (sourceTableModel.hasSelections() == false) {
      JOptionPane.showMessageDialog(this, "No virtual point has been selected!",
          "No Selection", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // Check protocol ID not empty
    if (sourceTableModel.checkPointIDNotEmpty() == false) {
      JOptionPane.showMessageDialog(this, "Please input protocol ID!",
          "No Protocol ID", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // ====== Create New SCADA Points ======
    ArrayList<Long> failedIds = new ArrayList<Long>();

    for (int i = 0; i < sourceTableModel.getRowCount(); i++) {
      Long pointID = (Long) sourceTableModel.getValueAt(i, ScadaPointSourceTableModel.COLUMN_POINT_ID);
      String description = (String) sourceTableModel.getValueAt(i, ScadaPointSourceTableModel.COLUMN_DESCRIPTION);
      if (pointID != null) {
        PointT point = factory.createPoint(sourceTableModel.getResource(i), iomap, type, pointID, description);
        if (point == null) {
          failedIds.add(pointID);
          log.warn("No point created for ID:" + pointID);
        } else {
          createdPoints.add(point);
        }

      }
    }

    if (!failedIds.isEmpty()) {
      showFailureMessag(failedIds);
    } else if (createdPoints.isEmpty()) {
      JOptionPane.showMessageDialog(this, "No point has been added!",
          "Warning", JOptionPane.WARNING_MESSAGE);
    }

    return true;
  }

  private void showFailureMessag(ArrayList<Long> confictPoints) {
    StringBuilder sb = new StringBuilder();
    sb.append("<html><p>The following points have not been created successfully. Those ID may already exist.</p>");
    sb.append("<ul>");
    int idx = 0;
    for (idx = 0; idx < confictPoints.size() && idx < 5; idx++) {
      sb.append("<li>");
      sb.append(confictPoints.get(idx));
      sb.append("</li>");
    }

    if (idx < confictPoints.size()) {
      sb.append("<li>......</li>");
    }
    sb.append("</ul>");
    JOptionPane.showMessageDialog(this, sb.toString(), "Warning", JOptionPane.WARNING_MESSAGE);
  }

  private void selectAllResource(boolean select) {
    for (int i = 0; i < sourceTableModel.getRowCount(); i++) {
      sourceTableModel.setSelection(i, select);
    }
  }

  private void createUIComponents() {
    sourceTable = new JXTable(sourceTableModel);
    sourceTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    
    TableColumnExt col;
    /* Customise high lighter */
    sourceTable.setHighlighters(new SourceTableHighlighter());

    /* Get "ID" column */
    col = sourceTable.getColumnExt(ScadaPointSourceTableModel.COLUMN_POINT_ID);

    /* Customise "ID" column editor */
    col.setCellEditor(new ScadaPointIdEditor(idChecker));

    /* Customise "ID" column width */
    col.setMinWidth(100);

    /* Customise "ID" column alignment */
    DefaultTableCellRenderer render = new DefaultTableCellRenderer();
    render.setHorizontalAlignment(SwingConstants.CENTER);
    col.setCellRenderer(render);

    /* Customise "Select" column width */
    col = sourceTable.getColumnExt(ScadaPointSourceTableModel.COLUMN_SELECT);
    col.setMaxWidth(50);
    
  }

  private void checkBoxSeqIDActionPerformed(ActionEvent e) {
    sourceTableModel.setSequentialIdEnabled(checkBoxSeqID.isSelected());
  }


  private void menuItemSelAllActionPerformed() {
    selectAllResource(true);
  }

  private void menuItemUnselAllActionPerformed() {
    selectAllResource(false);
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return dialogPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createHelpOKCancelButtonPanel(Helper.createHelpButton(getClass()));
  }

  private void checkBoxSelAllActionPerformed(ActionEvent e) {
    selectAllResource(checkBoxSelAll.isSelected());
  }


  private static class SourceTableHighlighter extends ColorHighlighter {

    public SourceTableHighlighter() {
      super(
        new HighlightPredicate() {

          @Override
          public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
            return adapter.getValue() == null
                && adapter.column == ScadaPointSourceTableModel.COLUMN_POINT_ID
                && adapter.getValue(ScadaPointSourceTableModel.COLUMN_SELECT) == Boolean.TRUE;
          }

        },
        UIThemeResources.COLOUR_ERROR_BACKGROUND,
        Color.black,
        UIThemeResources.COLOUR_ERROR_BACKGROUND,
        Color.black);
    }
  }


  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    JXTitledSeparator separator2 = new JXTitledSeparator();
    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    checkBoxSelAll = new JCheckBox();
    checkBoxSeqID = new JCheckBox();
    popupMenu = new JPopupMenu();
    menuItemSelAll = new JMenuItem();
    menuItemUnselAll = new JMenuItem();

    //======== this ========
    setName("bulkAddDialog");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "default:grow",
            "default, $lgap, fill:default:grow, $lgap, default, $pgap, default"));

        //---- separator2 ----
        separator2.setTitle("* Create SCADA points from the selected Virtual Points/Control Logic ");
        contentPanel.add(separator2, CC.xy(1, 1));

        //======== scrollPane1 ========
        {

          //---- sourceTable ----
          sourceTable.setPreferredScrollableViewportSize(new Dimension(550, 200));
          sourceTable.setRowSorter(null);
          sourceTable.setFillsViewportHeight(true);
          sourceTable.setGridColor(Color.lightGray);
          sourceTable.setComponentPopupMenu(popupMenu);
          sourceTable.setName("resourceTable");
          scrollPane1.setViewportView(sourceTable);
        }
        contentPanel.add(scrollPane1, CC.xy(1, 3));

        //======== panel1 ========
        {
          panel1.setLayout(new FormLayout(
              "default, $lcgap, default",
              "default"));

          //---- checkBoxSelAll ----
          checkBoxSelAll.setText("Select/Deselect All");
          checkBoxSelAll.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              checkBoxSelAllActionPerformed(e);
            }
          });
          panel1.add(checkBoxSelAll, CC.xy(1, 1));

          //---- checkBoxSeqID ----
          checkBoxSeqID.setText("Sequential Point ID");
          checkBoxSeqID.setSelected(true);
          checkBoxSeqID.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              checkBoxSeqIDActionPerformed(e);
            }
          });
          panel1.add(checkBoxSeqID, CC.xy(3, 1));
        }
        contentPanel.add(panel1, CC.xy(1, 5));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    //======== popupMenu ========
    {

      //---- menuItemSelAll ----
      menuItemSelAll.setText("Select All");
      menuItemSelAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemSelAllActionPerformed();
        }
      });
      popupMenu.add(menuItemSelAll);

      //---- menuItemUnselAll ----
      menuItemUnselAll.setText("Deselect All");
      menuItemUnselAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemUnselAllActionPerformed();
        }
      });
      popupMenu.add(menuItemUnselAll);
    }

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JXTable sourceTable;
  private JPanel panel1;
  private JCheckBox checkBoxSelAll;
  private JCheckBox checkBoxSeqID;
  private JPopupMenu popupMenu;
  private JMenuItem menuItemSelAll;
  private JMenuItem menuItemUnselAll;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
