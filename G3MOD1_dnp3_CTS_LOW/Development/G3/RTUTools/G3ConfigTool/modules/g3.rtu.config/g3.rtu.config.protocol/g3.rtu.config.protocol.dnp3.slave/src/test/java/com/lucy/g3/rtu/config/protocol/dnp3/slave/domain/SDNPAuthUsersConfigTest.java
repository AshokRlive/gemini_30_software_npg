/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.util.Arrays;

import org.junit.Assert;

import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthUsersConfig;


/**
 *
 */
public class SDNPAuthUsersConfigTest {


  @Test
  public void testConvertKeyHex2Xml() {
    String keyInHex = "61 62 63";
    byte[] keyInXml = SDNPAuthUsersConfig.convertKeyFromHexToXml(keyInHex.getBytes());
    System.out.println(String.format("Convert hex: %s, result XML as array:%s, String:%s",
        keyInHex,
        Arrays.toString(keyInXml),
        new String(keyInXml)));
    Assert.assertTrue(Arrays.equals(new byte[]{0x61, 0x62, 0x63}, keyInXml));
  }
  
  @Test
  public void testConvertKeyXml2Hex() {
    byte[] hex = SDNPAuthUsersConfig.convertKeyFromXmlToHex("abcd".getBytes());
    Assert.assertEquals("Result: "+ hex, "61 62 63 64 00 00 00 00 00 00 00 00 00 00 00 00", new String(hex));
  }

}

