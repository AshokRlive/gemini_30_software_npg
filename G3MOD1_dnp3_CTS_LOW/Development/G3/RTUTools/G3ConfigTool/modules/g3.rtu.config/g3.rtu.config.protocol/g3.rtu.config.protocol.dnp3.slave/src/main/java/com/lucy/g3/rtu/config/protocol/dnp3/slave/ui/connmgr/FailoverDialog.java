/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.connmgr;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.*;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.IPAddressField;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.Failover;

/**
 * The dialog for creating a failover.
 */
class FailoverDialog extends AbstractDialog {

  private Failover failover = null;


  public FailoverDialog(Frame owner) {
    super(owner, true);
    initComponents();
  }

  public FailoverDialog(Dialog owner) {
    super(owner, true);
    initComponents();
  }

  @Override
  public void ok() {
    if (createFailover()) {
      super.ok();
    }
  }

  public Failover getFailover() {
    return failover;
  }

  public void setFailover(Failover failover) {
    Object oldValue = this.failover;
    this.failover = failover;
    firePropertyChange("failover", oldValue, failover);
  }

  private boolean createFailover() {
    String port = tfPort.getText();
    String masterAddress = tfMasterAddress.getText();
    String ipAddress = tfIPAddress.getText();

    /* Validate */
    String err = null;
    if (Strings.isBlank(ipAddress)) {
      err = "IP address cannot be blank!";
    } else if (Strings.isBlank(masterAddress)) {
      err = "Master address cannot be blank!";
    } else if (Strings.isBlank(port)) {
      err = "IP port cannot be blank!";
    }
    if (err != null) {
      MessageDialogs.warning(err);
      return false;
    }

    failover = new Failover(ipAddress, Integer.parseInt(port), Integer.parseInt(masterAddress));
    return true;
  }

  private void createUIComponents() {
    tfIPAddress = new IPAddressField();
    tfIPAddress.setValue("0.0.0.0");
    tfMasterAddress = new JFormattedTextField(0);
    tfPort = new JFormattedTextField(0);
    FormattedTextFieldSupport.installCommitOnType(tfMasterAddress);
    FormattedTextFieldSupport.installCommitOnType(tfPort);
    FormattedTextFieldSupport.setBoundary(tfMasterAddress, 0, Integer.MAX_VALUE);
    FormattedTextFieldSupport.setBoundary(tfPort, 0, Integer.MAX_VALUE);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();

    //======== this ========
    setTitle("Add Failover Server");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
          "default, $lcgap, 185dlu",
          "3*(default, $lgap), default"));

        //---- label1 ----
        label1.setText("IP Address:");
        contentPanel.add(label1, CC.xy(1, 1));
        contentPanel.add(tfIPAddress, CC.xy(3, 1));

        //---- label2 ----
        label2.setText("Port:");
        contentPanel.add(label2, CC.xy(1, 3));
        contentPanel.add(tfPort, CC.xy(3, 3));

        //---- label3 ----
        label3.setText("Master Address:");
        contentPanel.add(label3, CC.xy(1, 5));
        contentPanel.add(tfMasterAddress, CC.xy(3, 5));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JLabel label1;
  private IPAddressField tfIPAddress;
  private JLabel label2;
  private JFormattedTextField tfPort;
  private JLabel label3;
  private JFormattedTextField tfMasterAddress;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {

    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return createOKCancelButtonPanel();
  }

  public static Failover showDialog(Frame parent) {
    FailoverDialog dlg = new FailoverDialog(parent);
    dlg.setVisible(true);
    return dlg.getFailover();
  }
}
