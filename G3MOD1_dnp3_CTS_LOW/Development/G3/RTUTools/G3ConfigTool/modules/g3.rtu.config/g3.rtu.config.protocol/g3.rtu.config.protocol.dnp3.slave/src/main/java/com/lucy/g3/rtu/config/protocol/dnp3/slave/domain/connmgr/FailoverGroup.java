/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ListModel;

import com.g3schema.ns_sdnp3.ConnectionManagerT;
import com.g3schema.ns_sdnp3.FailoverGroupT;
import com.jgoodies.common.collect.ArrayListModel;

public class FailoverGroup {

  private final ArrayListModel<Failover> failoverList = new ArrayListModel<>(100);


  public void addFailover(Failover failover) {
    if (failover != null) {
      this.failoverList.add(failover);
    }
  }

  public void removeFailover(int[] indexes) {
    if (indexes == null || indexes.length == 0) {
      return;
    }

    ArrayListModel<Failover> removeList = new ArrayListModel<>(indexes.length);
    for (int i = 0; i < indexes.length; i++) {
      if (indexes[i] >= 0 && indexes[i] < failoverList.size()) {
        removeList.add(failoverList.get(indexes[i]));
      }
    }

    failoverList.removeAll(removeList);
  }

  @SuppressWarnings("unchecked")
  public ListModel<Failover> getFailoverListModel() {
    return failoverList;
  }

  public Collection<Failover> getAllFailOvers() {
    return new ArrayList<Failover>(failoverList);
  }

  void readFromXML(ConnectionManagerT xml) {
    failoverList.clear();

    for (int i = 0; i < xml.failoverGroup.count(); i++) {
      FailoverGroupT xml_failover = xml.failoverGroup.at(i);
      addFailover(new Failover(
          xml_failover.ipAddress.getValue(),
          (int) xml_failover.port.getValue(),
          (int) xml_failover.masterAddress.getValue()));
    }
  }

  void writeToXML(ConnectionManagerT xml) {
    Collection<Failover> failovers = getAllFailOvers();
    for (Failover failover : failovers) {
      FailoverGroupT xml_failover = xml.failoverGroup.append();
      xml_failover.port.setValue(failover.getPort());
      xml_failover.ipAddress.setValue(failover.getIpAddress());
      xml_failover.masterAddress.setValue(failover.getMasterAddress());
    }
  }
}
