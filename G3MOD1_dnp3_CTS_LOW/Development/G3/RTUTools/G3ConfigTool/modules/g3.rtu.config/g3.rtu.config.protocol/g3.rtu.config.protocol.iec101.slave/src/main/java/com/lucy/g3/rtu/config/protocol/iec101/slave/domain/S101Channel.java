/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.domain;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.g3schema.ns_s101.S101ChannelT;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Channel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * The channel of S104.
 */
public class S101Channel extends IEC870Channel<S101Session> {

  private Logger log = Logger.getLogger(S101Channel.class);

  private final S101ChannelConf channelSettings;

  public S101Channel(S101 iec104, String channelName) {
    super(iec104);
    channelSettings = new S101ChannelConf(this, channelName);
    getValidator().addAdditionalValidation(channelSettings.getSerialConf());
  }

  @Override
  public S101ChannelConf getChannelConfig() {
    return channelSettings;
  }
  
  @Override
  public S101Session addSession(String sessionName) {
    S101Session newSession = new S101Session(this, sessionName);
    if (super.add(newSession)) {
      return newSession;
    } else {
      log.error("Failed to add a new session:" + sessionName);
      return null;
    }
  }

  public void writeToXML(S101ChannelT xml_channel) {
    getChannelConfig().writeToXML(xml_channel.config.append());
    Collection<S101Session> sesns = getAllSessions();
    for (S101Session session : sesns) {
      session.writeToXML(xml_channel.session.append());
    }    
  }
  
  public void readFromXML(S101ChannelT xmlChannel, ScadaPointSourceRepository sourceRepository, PortsManager ports) {
    getChannelConfig().readFromXML(xmlChannel.config.first(), ports);

    /* Read session */
    int sessionNum = xmlChannel.session.count();
    for (int j = 0; j < sessionNum; j++) {
      addSession("New Session").readFromXML(xmlChannel.session.at(j), sourceRepository);
    }
    
  }

}
