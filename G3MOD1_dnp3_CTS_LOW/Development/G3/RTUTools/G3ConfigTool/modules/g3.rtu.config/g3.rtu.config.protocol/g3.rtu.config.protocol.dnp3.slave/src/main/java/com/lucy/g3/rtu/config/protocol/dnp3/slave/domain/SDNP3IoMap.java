/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import org.apache.log4j.Logger;

import com.g3schema.ns_sdnp3.SDNP3PointsT;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.xml.SDNP3IoMapXmlProcessor;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

public class SDNP3IoMap extends AbstractScadaIoMap<SDNP3Point> {

  private Logger log = Logger.getLogger(SDNP3IoMap.class);

  SDNP3IoMap(SDNP3Session session) {
    super(session, SDNP3PointType.values());
  }


  @Override
  public void readFromXML(Object xmlObj, ScadaPointSourceRepository sourceRepository) {
    SDNP3PointsT xml = (SDNP3PointsT)xmlObj; 
    try {
      new SDNP3IoMapXmlProcessor(this).readSDNP3SessionIoMap(xml, sourceRepository);
    } catch (DuplicatedException e) {
      log.error("Failed to read from XML", e);
    }
  }

  @Override
  public void writeToXML(Object xmlObj) {
    SDNP3PointsT xml = (SDNP3PointsT)xmlObj;
    new SDNP3IoMapXmlProcessor(this).writeToXML(xml);    
  }

}
