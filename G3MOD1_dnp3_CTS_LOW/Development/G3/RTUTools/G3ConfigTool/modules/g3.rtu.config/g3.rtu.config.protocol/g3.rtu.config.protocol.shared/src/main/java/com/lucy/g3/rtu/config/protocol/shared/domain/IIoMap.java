/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import java.util.Collection;

import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;


/**
 * Input/Output mapping of protocol stack.
 * <p>
 * To allow RTU to read/write IO values, <code>IIoMap</code> has to be defined
 * to map external IO to some virtual IO inside RTU, such as Virtual Points,
 * Module Channels.
 * </p>
 * <p>
 * For example, to read the value from a slave module SCM, we have to create a
 * virtual point and then map this point to a channel of SCM we want to read.
 * </p>
 */
public interface IIoMap extends IContainerValidation{

  /**
   * Default name of IO Map tree node.
   */
  String DEFAULT_NAME = "IO Map";


  /**
   * <p>
   * Delete all IO mappings, points, etc.
   * </p>
   * This method is usually called when deleting a protocol.
   */
  void clearIoMap();
  
  
  void copyFrom(IIoMap iomap);
  
  /**
   * Get the session this iomap belongs to.
   *
   * @return non-null session instance.
   */
  IProtocolSession getSession();

  Collection<?> getAllItems();
}
