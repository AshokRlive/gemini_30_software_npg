/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point;

import com.g3schema.ns_pstack.PointT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.AbstractScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.base.math.BitMask;

/**
 * IEC870 Protocol Point.
 */
public abstract class IEC870Point extends AbstractScadaPoint {

  public static final String PROTOCOL_ID_DISPLAY_NAME = "IOA";


  IEC870Point(IScadaIoMap<IEC870Point> ioMap, IEC870PointType type, long id) {
    super(ioMap, type, id);
  }
  
  public abstract BitMask getGroupMask();

  @Override
  public IEC870PointType getType() {
    return (IEC870PointType) super.getType();
  }

  protected void readFromXML(PointT xml) {
    try {
      setPointID(xml.protocolID.getValue(), true);
    } catch (DuplicatedException e) {
      e.printStackTrace();
    }

    setEnabled(xml.enable.getValue());
    setEnableStoreEvent(xml.enableStoreEvent.getValue());

    if (xml.description.exists()) {
      setCustomDescription(xml.description.getValue());
    }
  }

  protected void writeToXML(PointT xml) {
    xml.protocolID.setValue(getPointID());
    xml.enable.setValue(isEnabled());
    xml.enableStoreEvent.setValue(isEnableStoreEvent());
    String desp = getCustomDescription();
    if (!Strings.isBlank(desp)) {
      xml.description.setValue(desp);
    }

  }

}
