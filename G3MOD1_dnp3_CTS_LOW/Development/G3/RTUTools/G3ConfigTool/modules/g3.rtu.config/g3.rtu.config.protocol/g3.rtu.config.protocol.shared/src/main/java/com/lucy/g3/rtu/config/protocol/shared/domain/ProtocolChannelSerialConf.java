/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.domain;

import java.beans.PropertyVetoException;

import org.apache.log4j.Logger;

import com.g3schema.ns_pstack.SerialConfT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.AbstractSerialPortUser;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPortUser;
import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolNameFactory;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The configuration for serial protocol channel.
 */
public class ProtocolChannelSerialConf extends AbstractSerialPortUser implements ISerialPortUser, IValidation {

  private Logger log = Logger.getLogger(ProtocolChannelSerialConf.class);

  private final IProtocolChannel<?> channel;

  private boolean active = true;
  
  private final Validator validator;
  
  public ProtocolChannelSerialConf(IProtocolChannel<?> channel) {
    super();
    this.channel = channel;
    this.validator = new Validator(this);
  }
  
  protected boolean isActive() {
    return active;
  }
  
  protected void setActive(boolean active) {
    this.active = active;
  }

  @Override
  public String getPortUserName() {
    return channel != null ? String.format("%s[%s]",
        channel.getProtocol().getProtocolName(),
        channel.getChannelName()) : null;
  }

  public void writeToXML(SerialConfT xml, ValidationResult result) {
    ISerialPort port = getSerialPort();
    if (port == null) {
//      if (result != null) {
//        result.addError(String.format("The serial port is not configured for %s channel:\"%s\"",
//            channel.getProtocol().getProtocolName(), channel.getChannelName()));
//      }
      // jgeorge - schema does not allow this on scada protocol
      xml.serialPortName.setValue("");

    } else {
      xml.serialPortName.setValue(port.getPortName());
    }
  }

  public void readFromXML(SerialConfT xml_serial, PortsManager manager) {
    if (xml_serial.serialPortName.exists()) {
      String portName = xml_serial.serialPortName.getValue();
      ISerialPort port = manager.getSerialPort(portName);
      if (port != null) {
        try {
          setSerialPort(port);
        } catch (PropertyVetoException e) {
          log.error("Fail to set serial port for: " + this, e);
        }
      } else {
        log.warn("Serial port not found:" + portName);
      }
    } else {
      log.warn("Serial port not configured for:" + this);
    }
  }
  
  private static class Validator extends AbstractValidator<ProtocolChannelSerialConf> {
    public Validator(ProtocolChannelSerialConf target) {
      super(target);
    }
    @Override
    public String getTargetName() {
      return ProtocolNameFactory.getAbsoluteName(target.channel);
    }
    
    @Override
    protected void validate(ValidationResultExt result) {
      if(target.isActive()) {
        if(target.getSerialPort() == null ) {
          result.addError("The port is not configured!");
        }
      }
    }
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }
  
  public final void releaseSerialPort() {
    try {
      setSerialPort(null);
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
  }
}
