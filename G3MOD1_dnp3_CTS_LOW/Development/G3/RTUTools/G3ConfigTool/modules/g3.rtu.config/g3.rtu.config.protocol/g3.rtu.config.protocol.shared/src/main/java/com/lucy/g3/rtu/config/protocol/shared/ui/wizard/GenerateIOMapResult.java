/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.factory.IIOMapGeneratorFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.IOMapGeneratorFactories;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

class GenerateIOMapResult implements WizardResultProducer {

  private final IProtocolSession session;

  public GenerateIOMapResult(IProtocolSession session) {
    super();
    this.session = Preconditions.checkNotNull(session, "session must not be null");
  }

  @Override
  public IProtocolSession finish(Map data) throws WizardException {
    IIOMapGeneratorFactory genFactory = IOMapGeneratorFactories.get(session.getProtocolType());
    IConfig config = session.getProtocolChannel().getProtocol().getManager().getOwner();
    IIOMapGenerator gen = genFactory.getGenerator(config);
    SelectionTableModel<?> m = (SelectionTableModel<?>) data.get(GenerateIOMapPage.KEY_OPTIONS_TABLE_MODEL);
    Object option = m.getSelection();
    
    boolean createNew = (boolean) data.get(GenerateIOMapPage.KEY_CREATE_NEW_IO);
    if(createNew) 
      gen.generate(session.getIomap(), option);
    else
      gen.update(session.getIomap(), option);
      

    return session;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}
