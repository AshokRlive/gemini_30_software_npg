/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.domain;

import java.util.Collection;

import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolNameFactory;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;


class S101ChannelValidator extends AbstractContainerValidator<S101Channel> {
  
  public S101ChannelValidator(S101Channel target) {
    super(target);
  }

  @Override
  public String getTargetName() {
    return ProtocolNameFactory.getAbsoluteName(target);
  }


  @Override
  protected void validate(ValidationResultExt result) {
    // Validate serial port
    if(target.getChannelConfig().getSerialConf().getSerialPort() == null )
      result.addError("The port is not configured!");
    
    Collection<S101Session> sesns = target.getAllItems();
    
    // Validate ASDU address against its size
    for (S101Session sesn : sesns) {
      long maxASDU = calcMaxSize(sesn.getAsduAddrSize());
      if(sesn.getAsduAddress() > maxASDU) {
        result.addError(String.format("[%s] Invalid ASDU adress: %d, valid range[%d,%d]",
            sesn.getSessionName(),sesn.getAsduAddress(),0,maxASDU));
      }
    }
    
    // Validate the size of link address of sessions
    long maxlinkAddress = calcMaxSize(target.getChannelConfig().getLinkAddressSize());
    for (S101Session sesn : sesns) {
      long address = sesn.getLinkAddress();
      
      if(address > maxlinkAddress && maxlinkAddress > 0)
        result.addError(String.format("[%s] Invalid link adress: %d, valid range[%d,%d]",sesn.getSessionName(),address,0,maxlinkAddress));
    }
    
    // Validate IOA of each point against configured IOA in session.
    for (S101Session sesn : sesns) {
      long maxIOA = calcMaxSize(sesn.getInfoObjAddrSize());
      Collection<IEC870Point> points = sesn.getIomap().getAllPoints();
      for (IEC870Point p : points) {
        if(p.getPointID() > maxIOA || p.getPointID() < 0) {
          result.addError(String.format("[%s] Invalid IOA:%d of point:\"%s\", valid range[%d,%d]",
              sesn.getSessionName(),p.getPointID(), p.getDescription(), 0, maxIOA));
        }
      }
    }
    
  }

  private static long calcMaxSize(long bytes) {
    return (long) Math.pow(2, bytes*8) - 1;
  }

  @Override
  protected Collection<IValidation> getValidatableChildrenItems() {
    // TODO Auto-generated method stub
    return null;
  }

}

