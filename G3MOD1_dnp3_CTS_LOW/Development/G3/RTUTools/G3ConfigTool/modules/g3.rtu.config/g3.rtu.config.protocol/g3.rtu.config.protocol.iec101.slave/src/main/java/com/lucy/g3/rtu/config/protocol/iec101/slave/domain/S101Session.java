/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec101.slave.domain;

import org.apache.log4j.Logger;

import com.g3schema.ns_s101.S101SesnConfT;
import com.g3schema.ns_s101.S101SessionT;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Constraints;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870Session;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;

/**
 * The Session of S104.
 */
public class S101Session extends IEC870Session {

  private Logger log = Logger.getLogger(S101Session.class);

  // @formatter:off
  public static final String PROPERTY_COT_SIZE                   = "cotSize";
  public static final String PROPERTY_MAX_POLL_DELAY             = "maxPollDelay";
  public static final String PROPERTY_LINK_ADDRESS               = "linkAddress";
  public static final String PROPERTY_MAX_FRAME_SIZE             = "maxFrameSize";
  public static final String PROPERTY_ASDU_ADDR_SIZE             = "asduAddrSize";
  public static final String PROPERTY_INFO_OBJ_ADDR_SIZE         = "infoObjAddrSize";

  private long cotSize         = (int) getDefault("cotSize");;
  private long maxPollDelay   = getDefault("maxPollDelay");
  private long linkAddress     = (int) getDefault("linkAddress");
  private long maxFrameSize    = (int) getDefault("maxFrameSize");
  private long asduAddrSize    = (int) getDefault("asduAddrSize");
  private long infoObjAddrSize = (int) getDefault("infoObjAddrSize");
  
  // @formatter:on


  public S101Session(S101Channel channel, String sessionName) {
    super(channel, sessionName);

    LoggingUtil.logPropertyChanges(this);
  }

  private static long getDefault(String property) {
    return IEC870Constraints.INSTANCE_101.getDefault(IEC870Constraints.PREFIX_SESSION, property);
  }


  private void readFromXML(S101SesnConfT xml) {
    super.readIEC870FromXML(xml);
    
    setCotSize((int) xml.cotSize.getValue());
    
    if(xml.maxPollDelay.exists())
      setMaxPollDelay(xml.maxPollDelay.getValue());
    
    if(xml.linkAddress.exists())
      setLinkAddress(xml.linkAddress.getValue());
    
    if(xml.maxFrameSize.exists())
      setMaxFrameSize(xml.maxFrameSize.getValue());
    
    if(xml.asduAddrSize.exists())
      setAsduAddrSize(xml.asduAddrSize.getValue());
    
    if(xml.infoObjAddrSize.exists())
      setInfoObjAddrSize(xml.infoObjAddrSize.getValue());
  }

  public void readFromXML(S101SessionT xml, ScadaPointSourceRepository resource) {
    super.readIEC870FromXML(xml.config.first());
    readFromXML(xml.config.first());
    getIomap().readFromXML(xml.iomap.first(), resource);
  }

  private void writeToXML(S101SesnConfT xml) {
    super.writeIEC870ToXML(xml);
    
    xml.cotSize.setValue(getCotSize());
    xml.maxPollDelay.setValue(getMaxPollDelay());
    xml.linkAddress.setValue((int) getLinkAddress());
    xml.maxFrameSize.setValue((int) getMaxFrameSize());
    xml.asduAddrSize.setValue((int) getAsduAddrSize());
    xml.infoObjAddrSize.setValue((int) getInfoObjAddrSize());
  }

  public void writeToXML(S101SessionT xml) {
    writeToXML(xml.config.append());
    getIomap().writeToXML(xml.iomap.append());
  }


  public long getCotSize() {
    return cotSize;
  }

  public void setCotSize(long cotSize) {
    Object oldValue = this.cotSize;
    this.cotSize = cotSize;
    firePropertyChange(PROPERTY_COT_SIZE, oldValue, cotSize);
  }

  
  public long getMaxPollDelay() {
    return maxPollDelay;
  }

  
  public void setMaxPollDelay(long maxPollDelay) {
    Object oldValue = this.maxPollDelay;
    this.maxPollDelay = maxPollDelay;
    firePropertyChange(PROPERTY_MAX_POLL_DELAY, oldValue, maxPollDelay);
  }

  
  public long getLinkAddress() {
    return linkAddress;
  }

  
  public void setLinkAddress(long linkAddress) {
    Object oldValue = this.linkAddress;
    this.linkAddress = linkAddress;
    firePropertyChange(PROPERTY_LINK_ADDRESS, oldValue, linkAddress);
  }

  
  public long getMaxFrameSize() {
    return maxFrameSize;
  }

  
  public void setMaxFrameSize(long maxFrameSize) {
    Object oldValue = this.maxFrameSize;
    this.maxFrameSize = maxFrameSize;
    firePropertyChange(PROPERTY_MAX_FRAME_SIZE, oldValue, maxFrameSize);
  }

  
  public long getAsduAddrSize() {
    return asduAddrSize;
  }

  
  public void setAsduAddrSize(long asduAddrSize) {
    Object oldValue = this.asduAddrSize;
    this.asduAddrSize = asduAddrSize;
    firePropertyChange(PROPERTY_ASDU_ADDR_SIZE, oldValue, asduAddrSize);
  }

  
  public long getInfoObjAddrSize() {
    return infoObjAddrSize;
  }

  
  public void setInfoObjAddrSize(long infoObjAddrSize) {
    Object oldValue = this.infoObjAddrSize;
    this.infoObjAddrSize = infoObjAddrSize;
    firePropertyChange(PROPERTY_INFO_OBJ_ADDR_SIZE, oldValue, infoObjAddrSize);
  }

  @Override
  public IEC870Constraints getConstraints() {
    return IEC870Constraints.INSTANCE_101;
  }

}
