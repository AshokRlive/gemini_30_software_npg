/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs;

import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;

/**
 * The Interface IScadaPointEditorDialogInvoker.
 *
 * @param <PointT>
 *          the generic type
 */
public interface IScadaPointEditorDialogInvoker<PointT extends ScadaPoint> extends IEditorDialogInvoker<PointT> {

}