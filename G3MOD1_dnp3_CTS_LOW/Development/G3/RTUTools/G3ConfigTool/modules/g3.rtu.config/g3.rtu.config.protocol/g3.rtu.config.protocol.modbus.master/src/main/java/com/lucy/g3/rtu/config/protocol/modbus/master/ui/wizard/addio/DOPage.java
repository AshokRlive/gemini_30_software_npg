/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.ui.wizard.addio;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
public class DOPage extends AbstractPage {
  public static final String KEY_REGISTER_TYPE   = AbstractPage.IKEY_REGISTER_TYPE   ;
  public static final String KEY_CHANNEL_NAME    = AbstractPage.IKEY_CHANNEL_NAME    ;
  public static final String KEY_NUMBER_OF_CHNL  = AbstractPage.IKEY_NUMBER_OF_CHNL  ;
  public static final String KEY_START_REG       = AbstractPage.IKEY_START_REG       ;
  
  public DOPage() {
    super(ChannelType.DIGITAL_OUTPUT);
    initComponents();
  }

  private void initComponents() {
    DefaultFormBuilder builder = createPanelBuilder(this);
    
    builder.append("Register Type:", createRegTypeCombobox(type), true);
    builder.append("Base Channel Name:", createChannelNameField(), true);
    builder.append("Number Of Channels:", createNumField(), true);
    builder.append("Starting Register:", createAddressField(), true);
  }
}
