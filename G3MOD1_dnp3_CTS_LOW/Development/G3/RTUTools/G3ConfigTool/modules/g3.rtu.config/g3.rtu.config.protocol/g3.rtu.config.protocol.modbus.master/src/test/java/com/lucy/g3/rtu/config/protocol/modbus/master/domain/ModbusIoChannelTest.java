/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.domain;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.MMBIoBitMask;

public class ModbusIoChannelTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testBitMaskToValue() {
    assertEquals((short) 1, MMBIoBitMask.BIT0.getMask());
    assertEquals((short) 2, MMBIoBitMask.BIT1.getMask());
    assertEquals((short) 4, MMBIoBitMask.BIT2.getMask());
    assertEquals((short) 8, MMBIoBitMask.BIT3.getMask());
  }

  @Test
  public void testValueToBitMask() {
    assertEquals(MMBIoBitMask.BIT0, MMBIoBitMask.forMask(1));
    assertEquals(MMBIoBitMask.BIT1, MMBIoBitMask.forMask(2));
    assertEquals(MMBIoBitMask.BIT2, MMBIoBitMask.forMask(4));
    assertEquals(MMBIoBitMask.BIT3, MMBIoBitMask.forMask(8));

  }

}
