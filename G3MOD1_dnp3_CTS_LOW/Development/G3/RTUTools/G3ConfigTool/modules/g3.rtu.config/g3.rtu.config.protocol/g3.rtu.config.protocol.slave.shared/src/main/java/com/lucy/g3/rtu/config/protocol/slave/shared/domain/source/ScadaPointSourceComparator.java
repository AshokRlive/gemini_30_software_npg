
package com.lucy.g3.rtu.config.protocol.slave.shared.domain.source;

import java.util.Comparator;

public final class ScadaPointSourceComparator implements Comparator<ScadaPointSource> {

  public static final ScadaPointSourceComparator INSTANCE = new ScadaPointSourceComparator();


  @Override
  public int compare(ScadaPointSource o1, ScadaPointSource o2) {
    if (o1 != null && o2 != null) {
      if (o1.getGroup() != o2.getGroup()) {
        return o1.getGroup() - o2.getGroup();
      } else {
        return o1.getId() - o2.getId();
      }
    } else if (o1 == null) {
      return -1;
    } else if (o2 == null) {
      return 1;
    } else {
      return 0;
    }

  }

}