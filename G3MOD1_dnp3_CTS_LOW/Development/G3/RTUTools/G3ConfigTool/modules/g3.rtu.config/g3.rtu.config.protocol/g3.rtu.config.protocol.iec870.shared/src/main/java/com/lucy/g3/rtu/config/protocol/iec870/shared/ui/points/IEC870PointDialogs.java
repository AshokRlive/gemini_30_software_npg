/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.ui.points;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870PointFactory;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.IScadaPointEditorDialogInvoker;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointBulkAddDialog;

public class IEC870PointDialogs {
  
  public static final String IOA_TEXT = "IOA";
  
  private static String createTitle(ScadaPointType type) {
    String title = "IEC104 Point ";

    if (type != null) {
      title = title + " - " + type.getDescription();
    }

    return title;
  }

  private static void showDialog(JDialog dialog) {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      ((SingleFrameApplication) app).show(dialog);
    } else {
      dialog.setVisible(true);
    }
  }
  

  /**
   * Shows a editor dialog which has navigation buttons such as "Next", "Prev".
   */
  public static void showEditorDialog(Frame parent, IEC870PointType type, IScadaPointEditorDialogInvoker<IEC870Point> invoker,
      ScadaPointSourceRepository sources) {
    Preconditions.checkNotNull(invoker, "provider is null");
    String title = createTitle(type);
    IEC870PointEditDialog dlg = new IEC870PointEditDialog(parent, title, type, invoker, sources);
    dlg.showDialog();
  }

  public static boolean showEditor(Frame parent, IEC870Point point, ScadaPointSourceRepository resource) {
    String title = createTitle(point.getType());

    IEC870PointEditDialog dlg = null;
    dlg = new IEC870PointEditDialog(parent, title, point, resource);
    dlg.showDialog();
    return dlg.hasBeenAffirmed();
  }
  
  public static boolean showBulkEditor(Frame parent, IEC870PointType type, IScadaIoMap<IEC870Point> iomap,
      List<IEC870Point> selections) {

    /* Edit multiple selections */
    IEC870PointBulkEditDialog editor = new IEC870PointBulkEditDialog(parent, selections, type, iomap);
    showDialog(editor);

    return !editor.isCancelled();
  }

  public static ArrayList<IEC870Point> showBulkAdd(Frame parent, List<ScadaPointSource> mappingRes,
      IEC870PointType type, IScadaIoMap<IEC870Point> iomap) {

    ScadaPointBulkAddDialog<IEC870Point> dialog;
    dialog = new ScadaPointBulkAddDialog<IEC870Point>(parent, mappingRes, type, iomap, new IEC870PointFactory());
    dialog.setColumnTitle(ScadaPointBulkAddDialog.COLUMN_POINT_ID, IOA_TEXT);
    showDialog(dialog);

    return dialog.getCreatedPoints();
  }

}
