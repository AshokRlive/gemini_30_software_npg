/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.factory;

import java.util.HashMap;

import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;


/**
 * The Class IOMapGenerators.
 */
public class IOMapGeneratorFactories {
  private IOMapGeneratorFactories() {}
  private static final HashMap<ProtocolType, IIOMapGeneratorFactory> factories = new HashMap<>();
  
  public static void register(ProtocolType type, IIOMapGeneratorFactory factory) {
    factories.put(type, factory);
  }
  
  public static IIOMapGeneratorFactory get(ProtocolType type) {
    return factories.get(type);
  }
}

