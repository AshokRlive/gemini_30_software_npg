/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.slave.shared.validation.ScadaPointBufferValidator;

/**
 * Validate the buffer value in DNP3 point presentation model.
 */
class SDNP3PointBufferValidator extends ScadaPointBufferValidator {

  public SDNP3PointBufferValidator(PresentationModel<SDNP3Point> target) {
    super(target);
  }
}
