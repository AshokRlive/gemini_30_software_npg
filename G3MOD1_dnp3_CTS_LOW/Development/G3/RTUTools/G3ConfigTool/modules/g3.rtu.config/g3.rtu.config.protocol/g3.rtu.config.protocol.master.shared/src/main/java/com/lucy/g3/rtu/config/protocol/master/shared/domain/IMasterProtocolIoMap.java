/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.master.shared.domain;

import java.util.Collection;
import java.util.List;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;

/**
 * Interface of Field Device IO Map.
 *
 * @param <ChannelT>
 *          the type of IO Channel.
 */
public interface IMasterProtocolIoMap<ChannelT extends IChannel> extends IIoMap {

  /**
   * Add a new field device channel.
   *
   * @param channel
   */
  boolean addChannel(ChannelT channel);

  ChannelT addChannel(ChannelType type, String description);

  void removeChannel(ChannelT channel);
  
  void removeAllChannel(Collection<IChannel> channels);

  void removeChannel(ChannelType type, int channelIndex);

  ChannelType[] getAvailableChannelTypes();

  List<ChannelT> getAllChannels();

  ChannelT[] getChannels(ChannelType type);

  ListModel<ChannelT> getChannelListModel(ChannelType type);

  String[] getChannelParamNames(ChannelType type);

  String getDeviceName();

  ChannelT getChannel(ChannelType type, int index);

}
