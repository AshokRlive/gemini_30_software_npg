/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.slave.shared.ui.panels;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.apache.commons.lang3.ArrayUtils;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;

class ScadaSourceSelectorDialog extends AbstractDialog {
  private final static String FILTER_ITEM_ALL = "All";
  
  private final FilterModel filterListModel;
  private ScadaPointSource selected;
  
  public ScadaSourceSelectorDialog(Window owner,List<ScadaPointSource> items, ScadaPointSource initialSelection) {
    super(owner);
    setModal(true);
    setTitle("Select SCADA point Source");
    sortList(items);
    this.filterListModel = new FilterModel(items);
    
    initComponents();
    filterList();
    listSources.setSelectedValue(initialSelection, true);
  }

  private void sortList(List<ScadaPointSource> sourceList) {
    sourceList.removeAll(Collections.singleton(null)); // remove null;
    
    Collections.sort(sourceList, new Comparator<Object>(){
      @Override
      public int compare(Object o1, Object o2) {
        ScadaPointSource s1 = (ScadaPointSource) o1;
        ScadaPointSource s2 = (ScadaPointSource) o2;
        
        if(s1.getGroup() == s2.getGroup())
          return s1.getId() - s2.getId();
        else
          return s1.getGroup() - s2.getGroup();
      }
      
    });    
  }


  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  public ScadaPointSource getSelected() {
    return selected;
  }
  
  @Override
  public void cancel() {
    selected = null;
    super.cancel();
  }

  @Override
  public void ok() {
    selected = listSources.getSelectedValue();
    super.ok();
  }


  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
  
  public static ScadaPointSource showDialog(Window owner, List<ScadaPointSource> selections, ScadaPointSource initial) {
    ScadaSourceSelectorDialog dialog = new ScadaSourceSelectorDialog(owner, selections, initial);
    
    dialog.pack();
    dialog.setVisible(true);
    
    return dialog.getSelected();
  }

  private void createUIComponents() {
    listSources = new JList<>(filterListModel);
    combFilter = new JComboBox<>(ArrayUtils.add(getAllSourceTypes(),0, FILTER_ITEM_ALL));
  }

  private String[]getAllSourceTypes() {
    HashSet<String> types = new HashSet<>();
    ArrayList<ScadaPointSource> items = filterListModel.items;
    for (ScadaPointSource s : items) {
      if(s == null)
        continue;
      types.add(s.getType());
    }
    return types.toArray(new String[types.size()]);
  }

  private void combFilterActionPerformed(ActionEvent e) {
    filterList();
  }


  private void filterList() {
    filterListModel.refilter((String)combFilter.getSelectedItem());
  }

  private void listSourcesMouseClicked(MouseEvent e) {
    if(e.getClickCount() > 1) {
      ok();
    }
  }


  private static class FilterModel extends AbstractListModel<ScadaPointSource> {

    private final ArrayList<ScadaPointSource> items;
    private final ArrayList<ScadaPointSource> filterItems;


    public FilterModel(Collection<ScadaPointSource> items) {
      super();
      this.items = new ArrayList<>(items);
      this.filterItems = new ArrayList<>();
    }

    @Override
    public ScadaPointSource getElementAt(int index) {
      if (index < filterItems.size())
        return filterItems.get(index);
      else
        return null;
    }

    @Override
    public int getSize() {
      return filterItems.size();
    }

    public void refilter(String term) {
      filterItems.clear();
      for (int i = 0; i < items.size(); i++)
        if (FILTER_ITEM_ALL.equals(term) || items.get(i).getType().equals(term))
          filterItems.add(items.get(i));
      fireContentsChanged(this, 0, getSize());
    }

  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    label1 = new JLabel();
    scrollPane1 = new JScrollPane();

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "default, $lcgap, [150dlu,default]:grow",
          "default, $ugap, fill:[250dlu,default]:grow"));

      //---- label1 ----
      label1.setText("Filter:");
      contentPanel.add(label1, CC.xy(1, 1));

      //---- combFilter ----
      combFilter.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          combFilterActionPerformed(e);
        }
      });
      contentPanel.add(combFilter, CC.xy(3, 1));

      //======== scrollPane1 ========
      {

        //---- listSources ----
        listSources.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listSources.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            listSourcesMouseClicked(e);
          }
        });
        scrollPane1.setViewportView(listSources);
      }
      contentPanel.add(scrollPane1, CC.xywh(1, 3, 3, 1));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel label1;
  private JComboBox combFilter;
  private JScrollPane scrollPane1;
  private JList<ScadaPointSource> listSources;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
