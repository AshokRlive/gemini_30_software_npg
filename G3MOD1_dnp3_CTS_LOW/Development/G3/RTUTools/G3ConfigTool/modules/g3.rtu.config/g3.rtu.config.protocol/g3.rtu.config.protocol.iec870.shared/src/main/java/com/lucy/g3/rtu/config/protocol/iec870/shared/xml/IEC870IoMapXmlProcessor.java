/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.xml;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Logger;

import com.g3schema.ns_iec870.IEC870CounterPointT;
import com.g3schema.ns_iec870.IEC870InputPointT;
import com.g3schema.ns_iec870.IEC870OutputPointT;
import com.g3schema.ns_iec870.IEC870PointsT;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870InputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870OutputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870Point;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870PointFactory;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.util.ScadaPointComparator;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;

/**
 * The XML reader/writer for IEC870 IO map.
 */
public class IEC870IoMapXmlProcessor {

  private Logger log = Logger.getLogger(IEC870IoMapXmlProcessor.class);

  private final IEC870IoMap iomap;


  public IEC870IoMapXmlProcessor(IEC870IoMap iomap) {
    this.iomap = Preconditions.checkNotNull(iomap, "iomap must not be null");
  }

  private ArrayList<IEC870Point> getSortedPoints(IEC870PointType type) {
    ArrayList<IEC870Point> pointlist;

    pointlist = new ArrayList<IEC870Point>(iomap.getPoints(type));
    Collections.sort(pointlist, new ScadaPointComparator());
    return pointlist;
  }

  public void writeToXML(IEC870PointsT xml) {
    ArrayList<IEC870Point> points;

    // mmea
    points = getSortedPoints(IEC870PointType.MMEA);
    for (IEC870Point p : points) {
      ((IEC870InputPoint) p).writeToXML(xml.mmea.append());
    }

    // mmeb
    points = getSortedPoints(IEC870PointType.MMEB);
    for (IEC870Point p : points) {
      ((IEC870InputPoint) p).writeToXML(xml.mmeb.append());
    }

    // mmec
    points = getSortedPoints(IEC870PointType.MMEC);
    for (IEC870Point p : points) {
      ((IEC870InputPoint) p).writeToXML(xml.mmec.append());
    }

    // msp
    points = getSortedPoints(IEC870PointType.MSP);
    for (IEC870Point p : points) {
      ((IEC870InputPoint) p).writeToXML(xml.msp.append());
    }

    // mdp
    points = getSortedPoints(IEC870PointType.MDP);
    for (IEC870Point p : points) {
      ((IEC870InputPoint) p).writeToXML(xml.mdp.append());
    }

    // csc
    points = getSortedPoints(IEC870PointType.CSC);
    for (IEC870Point p : points) {
      ((IEC870OutputPoint) p).writeToXML(xml.csc.append());
    }

    // cdc
    points = getSortedPoints(IEC870PointType.CDC);
    for (IEC870Point p : points) {
      ((IEC870OutputPoint) p).writeToXML(xml.cdc.append());
    }

    // mit
    points = getSortedPoints(IEC870PointType.MIT);
    for (IEC870Point p : points) {
      ((IEC870InputPoint) p).writeToXML(xml.mit.append());
    }
  }

  public void readFromXML(IEC870PointsT xml, ScadaPointSourceRepository resource) {
    // mmea
    for (int i = 0; i < xml.mmea.count(); i++) {
      readInputPoint(IEC870PointType.MMEA, xml.mmea.at(i), resource);
    }

    // mmeb
    for (int i = 0; i < xml.mmeb.count(); i++) {
      readInputPoint(IEC870PointType.MMEB, xml.mmeb.at(i), resource);
    }

    // mmec
    for (int i = 0; i < xml.mmec.count(); i++) {
      readInputPoint(IEC870PointType.MMEC, xml.mmec.at(i), resource);
    }

    // msp
    for (int i = 0; i < xml.msp.count(); i++) {
      readInputPoint(IEC870PointType.MSP, xml.msp.at(i), resource);
    }

    // mdp
    for (int i = 0; i < xml.mdp.count(); i++) {
      readInputPoint(IEC870PointType.MDP, xml.mdp.at(i), resource);
    }

    // csc
    for (int i = 0; i < xml.csc.count(); i++) {
      readOutputPoint(xml.csc.at(i), resource, false);
    }

    // cdc
    for (int i = 0; i < xml.cdc.count(); i++) {
      readOutputPoint(xml.cdc.at(i), resource, true);
    }

    // mit
    for (int i = 0; i < xml.mit.count(); i++) {
      readCounterPoint(xml.mit.at(i), resource);
    }
  }

  private void readOutputPoint(IEC870OutputPointT xml, ScadaPointSourceRepository resource, boolean isDouble) {
    IEC870OutputPoint point;

    if (isDouble) {
      point = IEC870PointFactory.createDoubleCommandPoint(iomap, xml.protocolID.getValue());
    } else {
      point = IEC870PointFactory.createSingleCommandPoint(iomap, xml.protocolID.getValue());
    }

    point.readFromXML(xml);

    try {
      iomap.addPoint(point);

      // Configure mapped control logic after adding successfully
      if (xml.controlLogicGroup.exists()) {
        //TODO analogue?
        point.mapTo(resource.getDigitalOutputSource((int) xml.controlLogicGroup.getValue()));
      }
      checkResourceMapped(point);

    } catch (DuplicatedException e) {
      log.error("Fail to add point:" + point, e);
    }

  }

  private void readCounterPoint(IEC870CounterPointT xml, ScadaPointSourceRepository resource) {
    IEC870InputPoint point;

    point = IEC870PointFactory.createCounterPoint(iomap, xml.protocolID.getValue());
    point.readFromXML(xml);

    try {
      iomap.addPoint(point);

      // Configure mapped virtual point after adding successfully
      if (xml.vpoint.exists()) {
        point.mapTo(resource.getInputSourceByRef(xml.vpoint.first()));
      }
      checkResourceMapped(point);

    } catch (DuplicatedException e) {
      log.error("Fail to add point:" + point, e);
    }

  }

  private void readInputPoint(IEC870PointType type, IEC870InputPointT xml, ScadaPointSourceRepository resource) {
    IEC870InputPoint point;
    point = IEC870PointFactory.createInputPoint(iomap, type, xml.protocolID.getValue());
    point.readFromXML(xml);

    try {
      iomap.addPoint(point);

      // Configure mapped virtual point after adding successfully
      if (xml.vpoint.exists()) {
        ScadaPointSource res = resource.getInputSourceByRef(xml.vpoint.first());
        point.setSource(res);
      }

      checkResourceMapped(point);

    } catch (DuplicatedException e) {
      log.error("Fail to add point:" + point, e);
    }
  }

  private void checkResourceMapped(IEC870Point point) {
    if (point.getSource() == null) {
      log.error(String.format("Point \"%s\" is not mapped to any resource", point));
    }
  }

}
