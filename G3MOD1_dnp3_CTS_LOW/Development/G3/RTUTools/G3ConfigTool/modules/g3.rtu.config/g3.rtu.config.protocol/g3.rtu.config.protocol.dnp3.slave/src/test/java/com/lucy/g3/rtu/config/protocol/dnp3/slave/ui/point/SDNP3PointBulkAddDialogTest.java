/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.ui.point;

import java.awt.Frame;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointInput;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.protocol.slave.shared.ui.dialogs.ScadaPointBulkAddDialog;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointStub;

public class SDNP3PointBulkAddDialogTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private static ScadaPointSource createSource(int id) {
    return new ScadaPointInput(new VirtualPointStub(0, id, VirtualPointType.BINARY_INPUT));
  }

  // Run Bulk Add dialog
  public static void main(String[] args) {
    ArrayList<ScadaPointSource> targets = new ArrayList<ScadaPointSource>();
    targets.add(createSource(0));
    targets.add(createSource(1));
    targets.add(createSource(2));
    targets.add(createSource(3));

    SDNP3Channel channel = new SDNP3Channel(new SDNP3());
    SDNP3Session dnp3Session = channel.addSession("session");
    try {
      dnp3Session.getIomap().addPoint(
          SDNP3PointFactory.create(dnp3Session.getIomap(),
              SDNP3PointType.BinaryInput, 0));
      dnp3Session.getIomap().addPoint(
          SDNP3PointFactory.create(dnp3Session.getIomap(),
              SDNP3PointType.BinaryInput, 3));
    } catch (DuplicatedException e) {
      e.printStackTrace();
    }

    ScadaPointBulkAddDialog dialog = new ScadaPointBulkAddDialog(
        (Frame) null, targets, SDNP3PointType.BinaryInput,
        dnp3Session.getIomap(), new SDNP3PointFactory());
    dialog.setVisible(true);
  }
}
