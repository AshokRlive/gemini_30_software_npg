/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.shared.ui.wizard;

import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IProtocolFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.ProtocolFactories;
import com.lucy.g3.rtu.config.protocol.shared.manager.IProtocolManager;


class AddProtocolResult implements WizardResultProducer{
  private final IProtocolManager manager;
  
  public AddProtocolResult(IProtocolManager manager) {
    super();
    this.manager = Preconditions.checkNotNull(manager, "manager must not be null");
  }

  @Override
  public IProtocol finish(Map data) throws WizardException {
    ProtocolType type = (ProtocolType) data.get(AddProtocolPage.KEY_SELECTED_PROTOCOL_TYPE);
    if(type == null) {
      throw new WizardException("The protocol type is not specified!");
    }

    IProtocol protocol = manager.getProtocolByType(type);
    if(protocol == null) {
      IProtocolFactory factory = ProtocolFactories.getFactory(type);
      if (factory == null)
        throw new WizardException("The factory is not found for protocol:" + type);
      
      protocol = factory.createProtocol();
      
      manager.add(protocol);
    }
    
    if(Boolean.TRUE == data.get(AddProtocolPage.KEY_ADD_CHANNEL)) {
      new AddChannelResult(protocol).finish(data);
    }
    
    return protocol;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}

