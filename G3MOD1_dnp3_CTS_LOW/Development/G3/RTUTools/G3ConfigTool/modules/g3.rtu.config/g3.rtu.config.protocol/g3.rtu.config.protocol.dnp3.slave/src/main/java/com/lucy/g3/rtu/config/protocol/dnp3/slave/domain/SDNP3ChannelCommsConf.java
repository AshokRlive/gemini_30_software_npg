/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import java.beans.PropertyVetoException;

import com.g3schema.ns_sdnp3.SDNP3CommsDeviceConfT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceUser;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.protocol.shared.util.ProtocolNameFactory;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.xml.gen.common.DNP3Enum.DNPLINK_NETWORK_TYPE;

/**
 * The Comms Device configuration of SDNP3 channel.
 */
public class SDNP3ChannelCommsConf extends ConnectOnClass implements IValidation  {

  private final CommsDeviceUser comms = new CommsDeviceUser();

  private final SDNP3ChannelConf channelConf;


  SDNP3ChannelCommsConf(SDNP3ChannelConf channelConf) {
    super();
    this.channelConf = channelConf;
  }

  public CommsDeviceUser getCommsDeviceSelection() {
    return comms;
  }

  /** Write Comms Device to XML. */
  void writeToXML(ValidationResult result, SDNP3CommsDeviceConfT xmlComms) {
    ICommsDevice commsDevice = comms.getCommsDevice();
    if (commsDevice != null) {
      xmlComms.commsDeviceID.setValue(commsDevice.getDeviceId());
    } else {
      result.addError(String.format(
          "The comms device is not configured for:\"%s\"", channelConf.getChannel()));
    }
    xmlComms.connectionClass.setValue(getConnectOnClassMask());
  }

  /**
   * Read Comms Device from XML.
   */
  void readFromXML(CommsDeviceManager commsDevMgr, SDNP3CommsDeviceConfT xml)
      throws PropertyVetoException {
    if(xml.commsDeviceID.exists()) {
      long commsDeviceID = xml.commsDeviceID.getValue();
      ICommsDevice device = commsDevMgr.getDevice((int) commsDeviceID);
      comms.setCommsDevice(device);
    }
    setConnectOnClassMask(xml.connectionClass.getValue());
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }
  
  private final Validator validator = new Validator(this);
  
  private static class Validator extends AbstractValidator<SDNP3ChannelCommsConf> {

    public Validator(SDNP3ChannelCommsConf target) {
      super(target);
    }

    @Override
    public String getTargetName() {
      return ProtocolNameFactory.getAbsoluteName(target.channelConf.getChannel());
    }

    @Override
    protected void validate(ValidationResultExt result) {
      if(target.getCommsDeviceSelection().getCommsDevice() == null
          && target.channelConf.getNetworkType() == DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_COMMS_DEVICE) {
        result.addError("No comms device selected!");
      }
    }
    
  }
}
