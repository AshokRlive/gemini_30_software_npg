/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.dnp3.slave.domain;

import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNPAuthConfig;
import com.lucy.g3.test.support.utilities.TestUtil;


/**
 *
 */
public class SDNPAuthConfigTest {


  @Test
  public void testProperties() {
    SDNPAuthConfig bean = new SDNPAuthConfig();
    TestUtil.testReadWriteProperties(bean, new String[]{
        SDNPAuthConfig.PROPERTY_AUTHENTICATION_ENABLED,
        SDNPAuthConfig.PROPERTY_AUTHENTICATION_ENABLED    ,
        SDNPAuthConfig.PROPERTY_ASSOCID                   ,
        SDNPAuthConfig.PROPERTY_MACALGORITHM              ,
        SDNPAuthConfig.PROPERTY_REPLYTIMEOUTMS            ,
        SDNPAuthConfig.PROPERTY_KEYCHANGEINTERVALMS       ,
        SDNPAuthConfig.PROPERTY_MAXAPPLTIMEOUTCOUNT       ,
        SDNPAuthConfig.PROPERTY_MAXKEYCHANGECOUNT         ,
        SDNPAuthConfig.PROPERTY_AGGRESSIVEMODESUPPORT     ,
        SDNPAuthConfig.PROPERTY_DISALLOWSHA1              ,
        SDNPAuthConfig.PROPERTY_EXTRADIAGS                ,
        SDNPAuthConfig.PROPERTY_OPERATEINV2MODE           ,
        SDNPAuthConfig.PROPERTY_MAXERRORCOUNT             ,
        SDNPAuthConfig.PROPERTY_RANDOMCHALLENGEDATALENGTH ,
        SDNPAuthConfig.PROPERTY_MAXAUTHENTICATIONFAILURES ,
        SDNPAuthConfig.PROPERTY_MAXSESSIONKEYSTATUSCOUNT  ,
        SDNPAuthConfig.PROPERTY_MAXREPLYTIMEOUTS          ,
        SDNPAuthConfig.PROPERTY_MAXAUTHENTICATIONREKEYS   ,
        SDNPAuthConfig.PROPERTY_MAXERRORMESSAGESSENT      ,
    });
  }

}

