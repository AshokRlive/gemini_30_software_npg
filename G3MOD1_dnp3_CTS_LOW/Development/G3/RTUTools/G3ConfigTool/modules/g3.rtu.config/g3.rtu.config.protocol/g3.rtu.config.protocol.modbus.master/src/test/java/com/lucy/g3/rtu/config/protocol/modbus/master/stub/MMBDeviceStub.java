/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.modbus.master.stub;

import java.util.List;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.IModuleSettings;
import com.lucy.g3.rtu.config.module.shared.domain.ModuleIcon;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolSession;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

public class MMBDeviceStub extends AbstractNode implements IMasterProtocolModule {

  public MMBDeviceStub() {
    super(NodeType.MODULE);
  }

  @Override
  public MODULE_ID getId() {

    return null;
  }

  @Override
  public String getShortName() {

    return null;
  }

  @Override
  public String getFullName() {

    return null;
  }

  @Override
  public MODULE getType() {

    return null;
  }

  @Override
  public ChannelType[] getAvailableChannelTypes() {

    return null;
  }

  @Override
  public List<? extends IChannel> getAllChannels() {

    return null;
  }

  @Override
  public IChannel[] getChannels(ChannelType type) {

    return null;
  }

  @Override
  public IChannel getChByTyeAndID(int id, ChannelType type) {

    return null;
  }

  @Override
  public IChannel getChByEnum(IChannelEnum channelEnum) {

    return null;
  }

  @Override
  public ModuleIcon getIcons() {

    return null;
  }

  @Override
  public MMBIoMap getIomap() {

    return null;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {

  }

  @Override
  public IProtocolChannel<IMasterProtocolSession> getChannel() {
    return null;
  }

  @Override
  public IMasterProtocolSession getSession() {
    return null;
  }

  @Override
  public IValidator getValidator() {
    return null;
  }

  @Override
  public String getDeviceName() {
    return null;
  }

  @Override
  public String getDeviceModel() {
    return null;
  }

  @Override
  public MODULE_ID getDeviceId() {
    return null;
  }

  @Override
  public void setDeviceId(MODULE_ID id) {

  }

  @Override
  public void setDeviceName(String description) {
  }

  @Override
  public IModuleSettings getSettings() {
    return null;
  }

}
