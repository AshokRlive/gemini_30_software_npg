/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.protocol.iec870.shared.domain;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.g3schema.ns_iec870.IEC870EventConfigT;
import com.g3schema.ns_iec870.IEC870SesnConfT;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_TIMETGA_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_TIME_FORMAT;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolChannel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.shared.impl.AbstractProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocolSession;

/**
 * The Session of S104.
 */
public abstract class IEC870Session extends AbstractProtocolSession implements IScadaProtocolSession {

  private Logger log = Logger.getLogger(IEC870Session.class);
  
  // @formatter:off
  public static final String PROPERTY_ASDU_ADDRESS             = "asduAddress";
  public static final String PROPERTY_CYCLIC_PERIOD            = "cyclicPeriod";
  public static final String PROPERTY_CYCLIC_FIRST_PERIOD      = "cyclicFirstPeriod";
  public static final String PROPERTY_BACKGROUND_PERIOD        = "backgroundPeriod";
  public static final String PROPERTY_SELECT_TIMEOUT           = "selectTimeout";
  public static final String PROPERTY_DEFAULT_RESPONSE_TIMEOUT = "defaultResponseTimeout";
  public static final String PROPERTY_SEND_CLOCK_SYNC_EVENTS   = "sendClockSyncEvents";
  public static final String PROPERTY_DELETE_OLDEST_EVENT      = "deleteOldestEvent";
  public static final String PROPERTY_CSEUSEACTTERM            = "cseUseActTerm";
  public static final String PROPERTY_CMDUSEACTTERM            = "cmdUseActTerm";
  public static final String PROPERTY_MULTIPLE_PMEN            = "multiplePMEN";
  public static final String PROPERTY_USE_DAYOFWEEK            = "useDayOfWeek";
  public static final String PROPERTY_CMD_TIMETAG_MODE         = "cmdTimetagMode";
  public static final String PROPERTY_CICNA_TIMEFORMAT         = "cicnaTimeFormat";
  public static final String PROPERTY_READ_TIMEFORMAT          = "readTimeFormat";
  public static final String PROPERTY_READMSRND_TIMEFORMAT     = "readMsrndTimeFormat";
  public static final String PROPERTY_EVENTCONFIG_BI           = "eventConfigBI";
  public static final String PROPERTY_EVENTCONFIG_DBI          = "eventConfigDBI";
  public static final String PROPERTY_EVENTCONFIG_CT           = "eventConfigCounter";
  public static final String PROPERTY_EVENTCONFIG_AIN          = "eventConfigAINorminalised";
  public static final String PROPERTY_EVENTCONFIG_AIS          = "eventConfigAIScaled";
  public static final String PROPERTY_EVENTCONFIG_AIF          = "eventConfigAIFloat";

  private long        asduAddress            = getDefault(PROPERTY_ASDU_ADDRESS);
  private long        cyclicPeriod           = getDefault(PROPERTY_CYCLIC_PERIOD);// ms
  private long        cyclicFirstPeriod      = getDefault(PROPERTY_CYCLIC_FIRST_PERIOD);// ms
  private long        backgroundPeriod       = getDefault(PROPERTY_BACKGROUND_PERIOD);// ms
  private long        selectTimeout          = getDefault(PROPERTY_SELECT_TIMEOUT);// ms
  private long        defaultResponseTimeout = getDefault(PROPERTY_DEFAULT_RESPONSE_TIMEOUT);// ms
  private boolean     cseUseActTerm          = true;
  private boolean     cmdUseActTerm          = true;
  private boolean     sendClockSyncEvents    = false;
  private boolean     deleteOldestEvent      = true;
  private boolean     multiplePMEN           = true;
  private boolean     useDayOfWeek           = true;
  private LU_TIMETGA_MODE cmdTimetagMode     = LU_TIMETGA_MODE.LU_TIMETGA_MODE_WITHOUT_TIMETAG;
  private LU_TIME_FORMAT cicnaTimeFormat     = LU_TIME_FORMAT.LU_TIME_FORMAT_NONE; 
  private LU_TIME_FORMAT readTimeFormat      = null; // Use default
  private LU_TIME_FORMAT readMsrndTimeFormat = null; // Use default


  private final HashMap<IEC870PointType,EventsConfig> eventConfig = new HashMap<>(); 
  // @formatter:on

  private final IEC870IoMap iomap = new IEC870IoMap(this);

  private final IEC870Channel channel;


  public IEC870Session(IEC870Channel channel, String sessionName) {
    super(channel, sessionName);
    this.channel = channel;

    LoggingUtil.logPropertyChanges(this);
  }

  private long getDefault(String property) {
    return getConstraints().getDefault(IEC870Constraints.PREFIX_SESSION, property);
  }
  
  public abstract IEC870Constraints getConstraints();
  
  @Override
  public IProtocolChannel<? extends IProtocolSession> getProtocolChannel() {
    return channel;
  }

  @Override
  public void delete() {
    iomap.clearIoMap();

  }

  @Override
  public IEC870IoMap getIomap() {
    return iomap;
  }

  public EventsConfig getEventConfig(IEC870PointType key) {
    EventsConfig ec = eventConfig.get(key);
    if(ec == null) {
      ec = new EventsConfig(getConstraints());
      eventConfig.put(key, ec);
    }
    
    return ec;
  }
  
  public EventsConfig getEventConfigBI() {
    return getEventConfig(IEC870PointType.MSP);
  }

  public EventsConfig getEventConfigDBI() {
    return getEventConfig(IEC870PointType.MDP);
  }

  public EventsConfig getEventConfigAINorminalised() {
    return getEventConfig(IEC870PointType.MMEA);
  }

  public EventsConfig getEventConfigAIScaled() {
    return getEventConfig(IEC870PointType.MMEB);
  }

  public EventsConfig getEventConfigAIFloat() {
    return getEventConfig(IEC870PointType.MMEC);
  }

  public EventsConfig getEventConfigCounter() {
    return getEventConfig(IEC870PointType.MIT);
  }

  
  public LU_TIME_FORMAT getCicnaTimeFormat() {
    return cicnaTimeFormat;
  }

  
  public void setCicnaTimeFormat(LU_TIME_FORMAT cicnaTimeFormat) {
    Object oldValue = this.cicnaTimeFormat;
    this.cicnaTimeFormat = cicnaTimeFormat;
    firePropertyChange(PROPERTY_CICNA_TIMEFORMAT, oldValue, cicnaTimeFormat);
  }

  
  public LU_TIME_FORMAT getReadTimeFormat() {
    return readTimeFormat;
  }

  
  public void setReadTimeFormat(LU_TIME_FORMAT readTimeFormat) {
    Object oldValue = this.readTimeFormat;
    this.readTimeFormat = readTimeFormat;
    firePropertyChange(PROPERTY_READ_TIMEFORMAT, oldValue, readTimeFormat);
  }

  
  public LU_TIME_FORMAT getReadMsrndTimeFormat() {
    return readMsrndTimeFormat;
  }

  
  public void setReadMsrndTimeFormat(LU_TIME_FORMAT readMsrndTimeFormat) {
    Object oldValue = this.readMsrndTimeFormat;
    this.readMsrndTimeFormat = readMsrndTimeFormat;
    firePropertyChange(PROPERTY_READMSRND_TIMEFORMAT, oldValue, readMsrndTimeFormat);
  }

  public long getAsduAddress() {
    return asduAddress;
  }

  public void setAsduAddress(long asduAddress) {
    Object oldValue = this.asduAddress;
    this.asduAddress = asduAddress;
    firePropertyChange(PROPERTY_ASDU_ADDRESS, oldValue, asduAddress);
  }

  public long getCyclicPeriod() {
    return cyclicPeriod;
  }

  public void setCyclicPeriod(long cyclicPeriod) {
    Object oldValue = this.cyclicPeriod;
    this.cyclicPeriod = cyclicPeriod;
    firePropertyChange(PROPERTY_CYCLIC_PERIOD, oldValue, cyclicPeriod);
  }

  public long getBackgroundPeriod() {
    return backgroundPeriod;
  }

  public void setBackgroundPeriod(long backgroundPeriod) {
    Object oldValue = this.backgroundPeriod;
    this.backgroundPeriod = backgroundPeriod;
    firePropertyChange(PROPERTY_BACKGROUND_PERIOD, oldValue, backgroundPeriod);
  }

  public long getSelectTimeout() {
    return selectTimeout;
  }

  public void setSelectTimeout(long selectTimeout) {
    Object oldValue = this.selectTimeout;
    this.selectTimeout = selectTimeout;
    firePropertyChange(PROPERTY_SELECT_TIMEOUT, oldValue, selectTimeout);
  }

  public long getDefaultResponseTimeout() {
    return defaultResponseTimeout;
  }

  public void setDefaultResponseTimeout(long defaultResponseTimeout) {
    Object oldValue = this.defaultResponseTimeout;
    this.defaultResponseTimeout = defaultResponseTimeout;
    firePropertyChange("defaultResponseTimeout", oldValue,
        defaultResponseTimeout);
  }


  public LU_TIMETGA_MODE getCmdTimetagMode() {
    return cmdTimetagMode;
  }

  
  public void setCmdTimetagMode(LU_TIMETGA_MODE cmdTimetagMode) {
    if(cmdTimetagMode != null) {
      Object oldValue = this.cmdTimetagMode;
      this.cmdTimetagMode = cmdTimetagMode;
      firePropertyChange(PROPERTY_CMD_TIMETAG_MODE, oldValue, cmdTimetagMode);
    }
  }

  public boolean isSendClockSyncEvents() {
    return sendClockSyncEvents;
  }

  public void setSendClockSyncEvents(boolean sendClockSyncEvents) {
    Object oldValue = this.sendClockSyncEvents;
    this.sendClockSyncEvents = sendClockSyncEvents;
    firePropertyChange("sendClockSyncEvents", oldValue, sendClockSyncEvents);
  }

  public boolean isDeleteOldestEvent() {
    return deleteOldestEvent;
  }

  public void setDeleteOldestEvent(boolean deleteOldestEvent) {
    Object oldValue = this.deleteOldestEvent;
    this.deleteOldestEvent = deleteOldestEvent;
    firePropertyChange("deleteOldestEvent", oldValue, deleteOldestEvent);
  }
  
  public long getCyclicFirstPeriod() {
    return cyclicFirstPeriod;
  }

  public void setCyclicFirstPeriod(long cyclicFirstPeriod) {
    Object oldValue = this.cyclicFirstPeriod;
    this.cyclicFirstPeriod = cyclicFirstPeriod;
    firePropertyChange(PROPERTY_CYCLIC_FIRST_PERIOD, oldValue, cyclicFirstPeriod);
  }
  

  public boolean isCseUseActTerm() {
    return cseUseActTerm;
  }

  public void setCseUseActTerm(boolean cseUseActTerm) {
    Object oldValue = this.cseUseActTerm;
    this.cseUseActTerm = cseUseActTerm;
    firePropertyChange(PROPERTY_CSEUSEACTTERM, oldValue, cseUseActTerm);
  }

  public boolean isCmdUseActTerm() {
    return cmdUseActTerm;
  }

  public void setCmdUseActTerm(boolean cmdUseActTerm) {
    Object oldValue = this.cmdUseActTerm;
    this.cmdUseActTerm = cmdUseActTerm;
    firePropertyChange(PROPERTY_CMDUSEACTTERM, oldValue, cmdUseActTerm);
  }

  
  public boolean isMultiplePMEN() {
    return multiplePMEN;
  }

  public void setMultiplePMEN(boolean multiplePMEN) {
    Object oldValue = this.multiplePMEN;
    this.multiplePMEN = multiplePMEN;
    firePropertyChange(PROPERTY_MULTIPLE_PMEN, oldValue, multiplePMEN);
  }

  public boolean isUseDayOfWeek() {
    return useDayOfWeek;
  }
  
  public void setUseDayOfWeek(boolean useDayOfWeek) {
    Object oldValue = this.useDayOfWeek;
    this.useDayOfWeek = useDayOfWeek;
    firePropertyChange(PROPERTY_USE_DAYOFWEEK, oldValue, useDayOfWeek);
  }

  protected final void writeIEC870ToXML(IEC870SesnConfT xml) {
    xml.sessionName.setValue(getSessionName());
    xml.enabled.setValue(isEnabled());
    xml.asduAddress.setValue(getAsduAddress());
    xml.cyclicPeriodMs.setValue(getCyclicPeriod());
    xml.cyclicFirstPeriodMs.setValue(getCyclicFirstPeriod());
    xml.backgroundPeriodMs.setValue(getBackgroundPeriod());
    xml.selectTimeoutMs.setValue(getSelectTimeout());
    xml.defResponseTimeoutMs.setValue(getDefaultResponseTimeout());
    xml.cseUseActTerm.setValue(isCseUseActTerm());
    xml.cmdUseActTerm.setValue(isCmdUseActTerm());
    xml.sendClockSyncEvents.setValue(isSendClockSyncEvents());
    xml.deleteOldestEvent.setValue(isDeleteOldestEvent());
    xml.multiplePMEN.setValue(isMultiplePMEN());
    xml.useDayOfWeek.setValue(isUseDayOfWeek());
    xml.cmdTimetagMode.setValue(getCmdTimetagMode().getValue());
    
    if(getCicnaTimeFormat() != null ) {
      xml.cicnaTimeFormat.setValue(getCicnaTimeFormat().name());
    }
    
    if(getReadTimeFormat() != null ) {
      xml.readTimeFormat.setValue(getReadTimeFormat().name());
    }
    
    if(getReadMsrndTimeFormat() != null ) {
      xml.readMsrndTimeFormat.setValue(getReadMsrndTimeFormat().name());
    }

    /* Events */
    IEC870EventConfigT xmlEvent = xml.eventConfig.append();
    getEventConfigAINorminalised().writeToXML(xmlEvent.mmea.append());
    getEventConfigAIScaled().writeToXML(xmlEvent.mmeb.append());
    getEventConfigAIFloat().writeToXML(xmlEvent.mmec.append());
    getEventConfigBI().writeToXML(xmlEvent.msp.append());
    getEventConfigDBI().writeToXML(xmlEvent.mdp.append());
    getEventConfigCounter().writeToXML(xmlEvent.mit.append());
  }
  
  protected final void readIEC870FromXML(IEC870SesnConfT xml) {
    if (xml.enabled.exists())
      setEnabled(xml.enabled.getValue());
    setSessionName(xml.sessionName.getValue());
    setAsduAddress(xml.asduAddress.getValue());
    setCyclicPeriod(xml.cyclicPeriodMs.getValue());
    if(xml.cyclicFirstPeriodMs.exists())
      setCyclicFirstPeriod(xml.cyclicFirstPeriodMs.getValue());
    setBackgroundPeriod(xml.backgroundPeriodMs.getValue());
    setSelectTimeout(xml.selectTimeoutMs.getValue());
    setDefaultResponseTimeout(xml.defResponseTimeoutMs.getValue());
    setSendClockSyncEvents(xml.sendClockSyncEvents.getValue());
    setDeleteOldestEvent(xml.deleteOldestEvent.getValue());
    setCseUseActTerm(xml.cseUseActTerm.getValue());
    setCmdUseActTerm(xml.cmdUseActTerm.getValue());
    setDeleteOldestEvent(xml.deleteOldestEvent.getValue());
    
    if(xml.cicnaTimeFormat.exists())
      setCicnaTimeFormat(LU_TIME_FORMAT.valueOf(xml.cicnaTimeFormat.getValue()));
    else
      setCicnaTimeFormat(null);
    
    if(xml.readTimeFormat.exists())
      setReadTimeFormat(LU_TIME_FORMAT.valueOf(xml.readTimeFormat.getValue()));
    else
      setReadTimeFormat(null);
    
    if(xml.readMsrndTimeFormat.exists())
      setReadMsrndTimeFormat(LU_TIME_FORMAT.valueOf(xml.readMsrndTimeFormat.getValue()));
    else
      setReadMsrndTimeFormat(null);
    
    if(xml.multiplePMEN.exists())
      setMultiplePMEN(xml.multiplePMEN.getValue());
    
    if(xml.useDayOfWeek.exists())
      setUseDayOfWeek(xml.useDayOfWeek.getValue());
    
    if(xml.cmdTimetagMode.exists()) {
      LU_TIMETGA_MODE mode = LU_TIMETGA_MODE.forValue((int) xml.cmdTimetagMode.getValue());
      if(mode != null)
        setCmdTimetagMode(mode);
      else
        log.error("Invalid cmdTimetagMode value:" + xml.cmdTimetagMode.getValue());
    }

    /* Events */
    IEC870EventConfigT xmlEvent = xml.eventConfig.first();
    getEventConfigAINorminalised().readFromXML(xmlEvent.mmea.first());
    getEventConfigAIScaled().readFromXML(xmlEvent.mmeb.first());
    getEventConfigAIFloat().readFromXML(xmlEvent.mmec.first());
    getEventConfigBI().readFromXML(xmlEvent.msp.first());
    getEventConfigDBI().readFromXML(xmlEvent.mdp.first());
    getEventConfigCounter().readFromXML(xmlEvent.mit.first());
  
  }
  
}
