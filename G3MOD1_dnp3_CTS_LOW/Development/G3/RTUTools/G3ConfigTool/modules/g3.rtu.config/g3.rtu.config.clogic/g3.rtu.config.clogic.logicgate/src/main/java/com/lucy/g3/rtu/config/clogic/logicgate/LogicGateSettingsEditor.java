/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.logicgate;

import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.xml.gen.common.ControlLogicDef.LOGIC_GATE_OPERATE;


class LogicGateSettingsEditor extends AbstractCLogicSettingsEditor {

  public LogicGateSettingsEditor(LogicGateSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    // Nothing to validate
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    SelectionInList<LOGIC_GATE_OPERATE> efhOpSelection = new SelectionInList<LOGIC_GATE_OPERATE>(
        Arrays.asList(LOGIC_GATE_OPERATE.values()),
        pm.getBufferedModel(LogicGateSettings.PROPERTY_OPER));

    JComponent comp = BasicComponentFactory.createComboBox(efhOpSelection);
    builder.append("Operator:", comp);
    
    return builder.getPanel();
  }

}

