
package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic;

import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;

public class ControlLogicAddingResult implements WizardResultProducer {
  private Logger log = Logger.getLogger(ControlLogicAddingResult.class);
  
  private final CLogicManager manager;
  private final ICLogicGenerator configurator;
  private final IControlLogicAddingWizardContributor contributor;
  
  public ControlLogicAddingResult(CLogicManager manager, ICLogicGenerator configurator, 
      IControlLogicAddingWizardContributor contributor) {
    super();
    this.manager = manager;
    this.configurator = configurator;
    this.contributor = contributor;
  }


  @Override
  public Object finish(Map data) throws WizardException {
    ICLogic newLogic = null;
    final ICLogicType type = (ICLogicType) data.get(LogicTypeSelectionPage.KEY_CLOGIC_TYPE);
    
    if(contributor != null) {
      try {
        newLogic = contributor.createLogic(type, data);
      } catch (Exception e) {
        log.error("Failed to create the new control logic",e);
      }
    } else {
      newLogic = CLogicFactories.getFactory(type.getId()).createLogic();
    }
    
    if (newLogic == null)
      throw new WizardException("The clogic was not created succesfully: " + type);
    
    if (!manager.contains(newLogic) && manager.add(newLogic) == false)
      throw new WizardException("Failed to add the created logic to manager!");
    
    // Configure OLR
    if (data.get(OLRConfigPage.KEY_REUIQRE_OLR) == Boolean.TRUE) {
      if (newLogic instanceof ISupportOffLocalRemote) {
        ((ISupportOffLocalRemote) newLogic).setInputOLR(manager.getOLRStatus());
      } else {
        log.error("Cannot configure OLR cause ISupportOffLocalRemote not implemented by clogic:" + newLogic);
      }
    }

    // Apply configuration from configurator.
    if (configurator != null)
      configurator.configure(newLogic);
    else
      log.error("The created logic:\"" + type + "\" is not configured cause configurator is missing.");

    return newLogic;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}
