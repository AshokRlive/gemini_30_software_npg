/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.sectionaliser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.SectionaliserParamT;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SECTIONALISER_INPUT;


/**
 *
 */
public class SectionaliserSettings  extends AbstractCLogicSettings<Sectionaliser>{

  public static final String PROPERTY_DEFAULT_ENABLED = "defaultEnabled";

  public static final String PROPERTY_SHORT_RECLAIMTIME = "shortReclaimTime";

  public static final String PROPERTY_LONG_RECLAIMTIME = "longReclaimTime";

  public static final String PROPERTY_FAULTCOUNTER_THRESHOLD = "faultCounterThreshold";

  /**
   * The option vaue for input type. With this type the sectionaliser has two
   * inputs MV1 and MV2 as voltage sensor.
   */
  public static final int INPUT_TYPE_MV = 0;

  /**
   * The option vaue for input type. With this type the sectionaliser uses input
   * LV as voltage sensor.
   */
  public static final int INPUT_TYPE_LV = 1;

  private boolean defaultEnabled;

  private long shortReclaimTime = 5; // seconds

  private long longReclaimTime = 30; // seconds

  private long faultCounterThreshold = 4;// Times

  private int inputType;

  
  /**
   * @param owner
   */
  public SectionaliserSettings(Sectionaliser owner) {
    super(owner);
    // TODO Auto-generated constructor stub
  }


  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    SectionaliserParamT xml = xml_param.sectionaliserParam.first();
    setDefaultEnabled(xml.defaultEnable.getValue());
    setFaultCounterThreshold(xml.faultCounterThreshold.getValue());
    setLongReclaimTime(xml.longReclaimTime.getValue());
    setShortReclaimTime(xml.shortReclaimTime.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    SectionaliserParamT xml_sect = xml.sectionaliserParam.append();
    xml_sect.defaultEnable.setValue(isDefaultEnabled());
    xml_sect.shortReclaimTime.setValue(getShortReclaimTime());
    xml_sect.longReclaimTime.setValue(getLongReclaimTime());
    xml_sect.faultCounterThreshold.setValue(getFaultCounterThreshold());

  }
  
  @Override
  public IValidator getValidator() {
    return null;
  }
  

  public int getInputType() {
    return inputType;
  }

  /**
   * Modify the input type.
   *
   * @param inputType
   *          {@code INPUT_TYPE_MV} or {@code INPUT_TYPE_LV}
   */
  public void setInputType(int inputType) {
    if (inputType != INPUT_TYPE_LV && inputType != INPUT_TYPE_MV) {
      log.error("Invalid input type: " + inputType);
      return;
    }

    Object oldValue = getInputType();
    this.inputType = inputType;

    Sectionaliser owner = getOwnerLogic();
    // Update inputs
    List<CLogicIO<VirtualPoint>> inputlist = new ArrayList<>(Arrays.asList(owner.getOriginalInputs()));
    switch (inputType) {
    case INPUT_TYPE_LV:
      inputlist.remove(owner.getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_MV0));
      inputlist.remove(owner.getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_MV1));
      break;

    case INPUT_TYPE_MV:
      inputlist.remove(owner.getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_LV));
      break;
    default:
      throw new RuntimeException("Unsupported input type:" + inputType);
    }
    CLogicIOPoint[] intputs = inputlist.toArray(new CLogicIOPoint[inputlist.size()]);
    owner.setInputs(intputs);

    firePropertyChange(PROPERTY_INPUT_TYPE, oldValue, inputType);
  }

  public boolean isDefaultEnabled() {
    return defaultEnabled;
  }

  public void setDefaultEnabled(boolean defaultEnabled) {
    Object oldValue = isDefaultEnabled();
    this.defaultEnabled = defaultEnabled;
    firePropertyChange(PROPERTY_DEFAULT_ENABLED, oldValue, defaultEnabled);
  }

  public long getShortReclaimTime() {
    return shortReclaimTime;
  }

  public void setShortReclaimTime(long shortReclaimTime) {
    if (shortReclaimTime < 0) {
      log.warn("Cannot set shortReclaimTime to: " + shortReclaimTime);
      return;
    }
    Object oldValue = getShortReclaimTime();
    this.shortReclaimTime = shortReclaimTime;
    firePropertyChange(PROPERTY_SHORT_RECLAIMTIME, oldValue, shortReclaimTime);
  }

  public long getLongReclaimTime() {
    return longReclaimTime;
  }

  public void setLongReclaimTime(long longReclaimTime) {
    if (longReclaimTime < 0) {
      log.warn("Cannot set longReclaimTime to: " + longReclaimTime);
      return;
    }

    Object oldValue = getLongReclaimTime();
    this.longReclaimTime = longReclaimTime;
    firePropertyChange(PROPERTY_LONG_RECLAIMTIME, oldValue, longReclaimTime);
  }

  public long getFaultCounterThreshold() {
    return faultCounterThreshold;
  }

  public void setFaultCounterThreshold(long faultCounterThreshold) {
    if (faultCounterThreshold < 0) {
      log.warn("Cannot set faultCounterThreshold to: " + faultCounterThreshold);
      return;
    }

    Object oldValue = getFaultCounterThreshold();
    this.faultCounterThreshold = faultCounterThreshold;
    firePropertyChange(PROPERTY_FAULTCOUNTER_THRESHOLD, oldValue, faultCounterThreshold);
  }

}

