
package com.lucy.g3.rtu.config.clogic.digitaloutputcontrol;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;


public class DigitalOutputControlLogicFactory extends AbstractClogicFactory {
  public final static DigitalOutputControlLogicFactory INSTANCE = new DigitalOutputControlLogicFactory();
  private DigitalOutputControlLogicFactory(){}
  

  @Override
  public ICLogic createLogic() {
    return new DigitalOutputControl();
  }

  @Override
  public ICLogicType getLogicType() {
    return DigitalOutputControl.TYPE;
  }
  

  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return IControlLogicAddingWizardContributor.Factory.createOutputChannelContributor(
        ChannelType.DIGITAL_OUTPUT,  "Select a Digital Output Channel");
  }
}

