
package com.lucy.g3.rtu.config.clogic.pltu;

import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.OutputLogicPage;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PLTU_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_POINT;


public class PLTULogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor {
  public final static PLTULogicFactory INSTANCE = new PLTULogicFactory();
  private PLTULogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new PLTULogicSettingsEditor((PLTULogicSettings) settings);
  }

  @Override
  public PLTULogic createLogic() {
    return new PLTULogic();
  }

  @Override
  public ICLogicType getLogicType() {
    return PLTULogic.TYPE;
  }
  
  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    CLogicManager manager = config.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    return new WizardPage[]{
        new OutputLogicPage(manager.getLogicByType(ICLogicType.SGL), "Select a Switchgear Logic")
    };
  }
  
  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }
  
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    PLTULogic newLogic = createLogic();
    
    // Configure output 
    OutputLogicPage logicPage = (OutputLogicPage)wizardData.get(OutputLogicPage.PAGE_ID);
    IOperableLogic sw = (IOperableLogic)logicPage.getSelectedLogic();
    newLogic.setOutputSwitch(sw);

    // Configure PLTU inputs 
    PseudoDoubleBinaryPoint cbstatus = 
        CLogicUtility.findDoubleBinaryPoint(sw.getAllPoints(), SGL_POINT.SGL_POINT_STATUS);
    if (cbstatus != null) {
      newLogic.getInput(PLTU_INPUT.PLTU_INPUT_POSITION).setValue(cbstatus);
    } else {
      Logger.getLogger(getClass()).error("CB status point is not found in switchgear logic");
    }
    
    return newLogic;
  }

}

