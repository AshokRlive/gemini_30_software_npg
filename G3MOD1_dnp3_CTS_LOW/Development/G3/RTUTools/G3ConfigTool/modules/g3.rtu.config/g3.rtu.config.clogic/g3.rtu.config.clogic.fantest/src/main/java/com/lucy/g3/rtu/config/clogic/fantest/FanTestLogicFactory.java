
package com.lucy.g3.rtu.config.clogic.fantest;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;


public class FanTestLogicFactory extends AbstractClogicFactory{
  public final static FanTestLogicFactory INSTANCE = new FanTestLogicFactory();
  private FanTestLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new FanTestSettingsEditor((FanTestSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new FanTest();
  }
  
  @Override
  public ICLogicType getLogicType() {
    return FanTest.TYPE;
  }
  
  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return IControlLogicAddingWizardContributor.Factory.createOutputChannelContributor(
        ChannelType.PSM_CH_FAN,  "Select a Fan Test Channel");
  }
}

