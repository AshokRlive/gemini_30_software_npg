
package com.lucy.g3.rtu.config.clogic.countercommand;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class CounterCommandLogicFactory extends AbstractClogicFactory {
  public final static CounterCommandLogicFactory INSTANCE = new CounterCommandLogicFactory();
  private CounterCommandLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new CounterCommandSettingsEditor((CounterCommandSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new CounterCommand();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return CounterCommand.TYPE;
  }
 
  
}

