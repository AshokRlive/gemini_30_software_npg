/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import java.util.Collection;

/**
 * The interface of Logic Outputs Manager.
 *
 * @param <T>
 *          the generic type of output value.
 */
public interface IOutputsManager<T> extends IIOManager<T> {

  int MAX_SIZE = 20;

  void addOutput(int number, boolean userCreated, String...label);

  void setOutputs(Collection<CLogicIO<ICLogic>> outputs);

}
