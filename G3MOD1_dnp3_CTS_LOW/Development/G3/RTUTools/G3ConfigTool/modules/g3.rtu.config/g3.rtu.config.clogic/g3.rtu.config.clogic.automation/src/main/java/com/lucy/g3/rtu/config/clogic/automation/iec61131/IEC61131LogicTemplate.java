/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61131;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.AutomationSchemeTemplate.AutomationLogicTemplateT;
import com.AutomationSchemeTemplate.LogicConstantT;
import com.AutomationSchemeTemplate.LogicInputT;
import com.AutomationSchemeTemplate.LogicPointT;
import com.lucy.g3.automation.scheme.templates.IEC61131Res;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutoSchemeType;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutomationConstantType;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutomationPointType;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogic;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicConstantManager;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.constants.VirtualPointType;

/**
 * The Class AutomationLogicTemplate.
 */
public class IEC61131LogicTemplate {
  
  private Logger log = Logger.getLogger(IEC61131LogicTemplate.class);
  
  private final AutoSchemeType schemeType;

  public IEC61131LogicTemplate(AutoSchemeType schemeType) {
    this.schemeType = schemeType;
  }

  public void apply(AutomationLogic autoLogic) {
    if(schemeType == null)
      return;
    
    if(schemeType == AutoSchemeType.CUSTOM) {
      resetIO(autoLogic);
      autoLogic.setCustomName(autoLogic.getType().getInstanceName());
    } else {
      applyXmlTemplate(autoLogic);
    }
  }

  private void applyXmlTemplate(AutomationLogic autoLogic) {
    IInputsManager inputMgr = autoLogic.getInputsManager();
    IOutputsManager<ICLogic> outputMgr = autoLogic.getOutputsManager();
    ICLogicPointsManager pointsMgr = autoLogic.getPointsManager();
    AutomationLogicConstantManager constantMgr = autoLogic.getSettings().getConstantsManager();
    
    // Read template from XML
    AutomationLogicTemplateT xmlRoot;
    try {
      String templFile = IEC61131Res.getTemplateXmlFilePath(schemeType);
      if(templFile == null) {
        log.error("Template file not found for:"+schemeType);
        return;
      }
      xmlRoot = readXml(readFromJARFile(templFile));
    } catch (IOException e) {
      MessageDialogs.error("Failed to read Automation Configuration Template", e);
      return;
    }
    
    // Reset IO
    resetIO(autoLogic);
    
    // Initialise Inputs
    int inputNum = xmlRoot.input.count();
    for (int i = 0; i < inputNum; i++) {
      LogicInputT xmlInput = xmlRoot.input.at(i);
      VirtualPointType type = convertPointTypeEnum(xmlInput.type.getValue());
      inputMgr.addInputs(new VirtualPointType[]{type}, 
          1, xmlInput.isMandatory.getValue(), false, false, xmlInput.name.getValue());
    }
    
    // Initialise points
    int pointNum = xmlRoot.point.count();
    for (int i = 0; i < pointNum; i++) {
      LogicPointT xmlPoint = xmlRoot.point.at(i);
      pointsMgr.addPoint(convertPointTypeEnum(xmlPoint.type.getValue()), false, 1, xmlPoint.name.getValue());
    }
    
    // Initialise output
    int outputNum = xmlRoot.output.count();
    for (int i = 0; i < outputNum; i++) {
      outputMgr.addOutput(1, false, xmlRoot.output.at(i).name.getValue());
    }
    
    // Initialise constants
    int constNum = xmlRoot.constant.count();
    for (int i = 0; i < constNum; i++) {
      LogicConstantT xml = xmlRoot.constant.at(i);
      AutomationConstantType type = AutomationConstantType.valueOf(xml.type.getValue());
      String unit = null;
      if(xml.unit.exists())
       unit = xml.unit.getValue();
      
      constantMgr.add(xml.name.getValue(), type, xml.defaultValue.getValue(), unit);
    }
    
    // Set version
    BasicVersion version = new BasicVersion(xmlRoot.versionMajor.getValue(), xmlRoot.versionMinor.getValue());
    autoLogic.getSettings().setAutoSchemeVersion(version);
    
    // Set custom name
    if(xmlRoot.logicName.exists()) {
      autoLogic.setCustomName(xmlRoot.logicName.getValue());
    }
  }

  /**
   * Remove all existing IOs and add default IO.
   * @param autoLogic
   */
  private void resetIO(AutomationLogic autoLogic) {
    IInputsManager inputMgr = autoLogic.getInputsManager();
    IOutputsManager<ICLogic> outputMgr = autoLogic.getOutputsManager();
    ICLogicPointsManager pointsMgr = autoLogic.getPointsManager();
    AutomationLogicConstantManager constantMgr = autoLogic.getSettings().getConstantsManager();
    
    inputMgr.removeAll();
    pointsMgr.removeAll();
    outputMgr.removeAll();
    constantMgr.removeAll();
    
    // Add predefined IOs
    inputMgr.addInputs(new VirtualPointType[]{VirtualPointType.BINARY_INPUT},
        1, false, false, false, "Auto Disabled");
    pointsMgr.addPoint(VirtualPointType.BINARY_INPUT, false, 1, "Auto Running");
    pointsMgr.addPoint(VirtualPointType.BINARY_INPUT, false, 1, "Auto Fail");
  }
  
  
  private VirtualPointType convertPointTypeEnum(String pointTypeEnum) {
    AutomationPointType type = AutomationPointType .valueOf(pointTypeEnum);
    if(type == null) {
      log.error("Unsupported point enum: " + pointTypeEnum);
      return VirtualPointType.DOUBLE_BINARY_INPUT;
    }
    
    switch (type) {
    case analogue:
      return VirtualPointType.ANALOGUE_INPUT;
    case binary:
      return VirtualPointType.BINARY_INPUT;
    case counter:
      return VirtualPointType.COUNTER;
    case doubleBinary:
      return VirtualPointType.DOUBLE_BINARY_INPUT;
    default:
      break;
    }
    
    return VirtualPointType.DOUBLE_BINARY_INPUT;
  }


  private static AutomationLogicTemplateT readXml(String xmlString) {
    com.AutomationSchemeTemplate.AutomationSchemeTemplate2 doc;
    try {
      doc = com.AutomationSchemeTemplate.AutomationSchemeTemplate2.loadFromString(xmlString);
      return doc.automationLogicTemplate.first();
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  private static String readFromJARFile(String filename) throws IOException {
    InputStream is = IEC61131LogicTemplate.class.getResourceAsStream(filename);
    String result = IOUtils.toString(is,"UTF-8");
    IOUtils.closeQuietly(is);
    return result;
  }

  public static void applyTemplate(AutoSchemeType schemeType, AutomationLogic autoLogic) {
      new IEC61131LogicTemplate(schemeType).apply(autoLogic);
  }
}

