/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.pseudoclock;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.pseudoclock.PseudoClockSettings.OutputMode;
import com.lucy.g3.rtu.config.clogic.pseudoclock.PseudoClockSettings.TimingMode;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.FreezeClockTime;

class PseudoClockSettingsEditor extends AbstractCLogicSettingsEditor{

  public PseudoClockSettingsEditor(PseudoClockSettings settings) {
    super(settings);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    ValueModel vm;
    
    vm = pm.getBufferedModel(PseudoClockSettings.PROPERTY_AUTOSTART_ENABLED);
    JCheckBox autoStartCheckBox = ComponentsFactory.createCheckBox(vm, "");
    autoStartCheckBox.setToolTipText("If enabled, this pseudo clock will be started once the system starts up, "
        + "otherwise it won't be started until recving a START command from SCADA");
    builder.append("Auto Start:", autoStartCheckBox);
    builder.nextLine();
    
    builder.appendSeparator("Output Mode");
    builder.nextLine();
    
    // Create all components
    vm = pm.getBufferedModel(PseudoClockSettings.PROPERTY_OUTPUT_MODE);
    JRadioButton radio0 = ComponentsFactory.createRadioButton(vm, OutputMode.TOGGLING, "Toggle Output Value");
    JRadioButton radio1 = ComponentsFactory.createRadioButton(vm, OutputMode.PULSE,    "Generate Pulse");
    
    vm = pm.getBufferedComponentModel(PseudoClockSettings.PROPERTY_OUTPUTPULSE_WIDTH_MS);
    JFormattedTextField pulseField = BasicComponentFactory.createFormattedTextField(vm, 
                    createRoundingFormatter(PseudoClockSettings.MIN_PULSE_MS, Integer.MAX_VALUE));
    FormattedTextFieldSupport.installCommitOnType(pulseField);
    
    // Append components
    builder.append("",radio0);
    builder.nextLine();
    
    builder.append("",radio1,pulseField, new JLabel("ms"));
    builder.nextLine();
    
    builder.appendSeparator("Timing Mode");
    builder.nextLine();
   
    
    // Create all components
    vm = pm.getBufferedModel(PseudoClockSettings.PROPERTY_TIMING_MODE);
    radio0 = ComponentsFactory.createRadioButton(vm, TimingMode.PERIODIC, "Interval:");
    radio1 = ComponentsFactory.createRadioButton(vm, TimingMode.OCLOCK,   "From O'Clock:");
    
    vm = pm.getBufferedModel(PseudoClockSettings.PROPERTY_PERIODIC_INTERVAL_SECS);
    JFormattedTextField intervalField = ComponentsFactory.createNumberField(vm, 
        PseudoClockSettings.MIN_INTERVAL_SEC, Integer.MAX_VALUE);
    
    vm = pm.getBufferedModel(PseudoClockSettings.PROPERTY_OCLOCKTIME);
    JComboBox oclockSelection = ComponentsFactory.createComboBox(new SelectionInList<>(FreezeClockTime.values(), vm));
    
    // Append components
    builder.append("",radio0,intervalField, new JLabel("secs"));
    builder.nextLine();
    
    builder.append("",radio1,oclockSelection);
    builder.nextLine();

    
    // Update components' enable state upon radio button selection
    {
      BindingConverter<OutputMode,Boolean> enumToBooleanConverter = new BindingConverter<PseudoClockSettings.OutputMode, Boolean>() {
        @Override
        public Boolean targetValue(OutputMode sourceValue) {
          return sourceValue == OutputMode.PULSE;
        }

        @Override
        public OutputMode sourceValue(Boolean targetValue) {
          return null;
        }
      };
      ConverterValueModel booleanVM = new ConverterValueModel(pm.getBufferedModel(PseudoClockSettings.PROPERTY_OUTPUT_MODE), enumToBooleanConverter);
      PropertyConnector.connectAndUpdate(booleanVM, pulseField, "enabled");
    }
     
    {
      BindingConverter<TimingMode,Boolean> enumToBooleanConverter2 = new BindingConverter<PseudoClockSettings.TimingMode, Boolean>() {
        @Override
        public Boolean targetValue(TimingMode sourceValue) {
          return sourceValue == TimingMode.PERIODIC;
        }

        @Override
        public TimingMode sourceValue(Boolean targetValue) {
          return null;
        }
      };
      ConverterValueModel booleanVM = new ConverterValueModel(pm.getBufferedModel(PseudoClockSettings.PROPERTY_TIMING_MODE), enumToBooleanConverter2);
      PropertyConnector.connectAndUpdate(booleanVM, intervalField, "enabled");
      PropertyConnector.connectAndUpdate(ConverterFactory.createBooleanNegator(booleanVM), oclockSelection, "enabled");
    }
    return builder.getPanel();
  }

  private long getLongValue(String property) {
    return ((Number)pm.getBufferedValue(property)).longValue();
  }
  
  @Override
  public void validate(ValidationResult result) {
    OutputMode outputMode = (OutputMode) pm.getBufferedValue(PseudoClockSettings.PROPERTY_OUTPUT_MODE);
    // Validate pulse value
    if(outputMode  == OutputMode.PULSE) {
      long pulseMs = getLongValue(PseudoClockSettings.PROPERTY_OUTPUTPULSE_WIDTH_MS);
      long intervalMs;
      TimingMode timingmode = (TimingMode) pm.getBufferedValue(PseudoClockSettings.PROPERTY_TIMING_MODE);
      if(timingmode == TimingMode.PERIODIC) {
        long secs = getLongValue(PseudoClockSettings.PROPERTY_PERIODIC_INTERVAL_SECS);
        intervalMs = secs * 1000;
      } else {
        Long mins = ((FreezeClockTime)pm.getBufferedValue(PseudoClockSettings.PROPERTY_OCLOCKTIME)).getValue();
        intervalMs = mins * 60000;
      }
      
      if (pulseMs > intervalMs) {
        result.addError("The pulse length value is too big. Please set it to be below: "+intervalMs,
            PseudoClockSettings.PROPERTY_OUTPUTPULSE_WIDTH_MS);
      } 
    }
  }

}

