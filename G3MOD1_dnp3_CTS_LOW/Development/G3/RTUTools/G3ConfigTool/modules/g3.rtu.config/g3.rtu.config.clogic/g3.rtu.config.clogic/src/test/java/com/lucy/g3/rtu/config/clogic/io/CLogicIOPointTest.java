/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.io;

import static org.junit.Assert.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointStub;

public class CLogicIOPointTest {

  private CLogicIOPoint fixture;
  private VirtualPoint point;


  @BeforeClass
  public static void setUpBeforeClass() {
    Logger.getRootLogger().setLevel(Level.DEBUG);
  }

  @Before
  public void setUp() throws Exception {
    fixture = new CLogicIOPoint(null, "test", VirtualPointType.binaryTypes(), false);
    point = new VirtualPointStub(0, 0, VirtualPointType.BINARY_INPUT);
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
    point = null;
  }

  @Test
  public void testSetPoint() {
    final String initialStr = fixture.getValueString();
    assertNull(fixture.getValue());

    fixture.setValue(point);
    assertEquals(point, fixture.getValue());
    assertNotSame("Expect string changed", initialStr, fixture.getValueString());

    fixture.setValue(null);
    assertNull(fixture.getValue());
    assertEquals("Expect value string recovered", initialStr, fixture.getValueString());
  }

  @Test
  public void testRemovePoint() {
    final String initialStr = fixture.getValueString();
    fixture.setValue(point);

    point.delete();
    assertEquals("Expect value string recovered", initialStr, fixture.getValueString());

    assertNull(fixture.getValue());
  }

  @Test
  public void testPropertyChangeEvent(){
    PCL pcl = new PCL();
    fixture.addPropertyChangeListener(CLogicIOPoint.PROPERTY_VALUE, pcl);
    fixture.setValue(point);
    assertTrue(pcl.changed);
  }
  
  private static class PCL implements PropertyChangeListener {
    boolean changed = false;
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      changed = true;
    }
    
  }
}
