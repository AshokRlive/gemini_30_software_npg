/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fantest;

import java.util.Arrays;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_FAN;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FAN_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FAN_OUTPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Fan Test Control Logic.
 */
public final class FanTest extends PredefinedCLogic<FanTestSettings> implements ISupportOffLocalRemote, IOperableLogic {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.FAN, "Fan Test")
      .desp("The logic for controlling FAN test.")
      .maxAmount(1)
      .opMode(OperationMode.GENERIC_OPERATE)
      .requireOLR(true)
      .get();
  
  private final CLogicIOChannel[] outputs;


  public FanTest() {
    this(null);
  }

  public FanTest(IChannel fanTestChannel) {
    super(TYPE, FAN_INPUT.values(), null);
    outputs = new CLogicIOChannel[FAN_OUTPUT.values().length];
    initOutputs();

    /* Set fanTest channel */
    if (fanTestChannel != null) {
      if (fanTestChannel.getType() == ChannelType.PSM_CH_FAN) {
        setOutputChannel(fanTestChannel);
      } else {
        log.error("Unacceptable Fan Test channel. Invalid type:"
            + fanTestChannel.getType());
      }
    }

    /* Bind logic name to output. */
    bindDescriptionToIO(outputs[0]);
  }

  private void initOutputs() {

    FAN_OUTPUT[] outputEnums = FAN_OUTPUT.values();
    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new CLogicIOChannel(this,
          outputEnums[i].getDescription(),
          false,
          true,
          new ChannelType[] { ChannelType.PSM_CH_FAN },
          new MODULE[] { MODULE.MODULE_PSM },
          new Enum[] { PSM_CH_FAN.PSM_CH_FAN_FAN_TEST });
    }
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(FAN_INPUT.FAN_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  public CLogicIO<IChannel> getOutputChannel() {
    int index = FAN_OUTPUT.FAN_OUTPUT_OUTPUT.ordinal();
    return outputs[index];
  }

  public void setOutputChannel(IChannel ch) {
    if (ch != null && ch.getType() != ChannelType.PSM_CH_FAN) {
      log.error("Unacceptable Fan Test channel. Invalid type:"
          + ch.getType());
      return;
    }

    int index = FAN_OUTPUT.FAN_OUTPUT_OUTPUT.ordinal();
    outputs[index].setValue(ch);
  }

  @Override
  public CLogicIO<IChannel>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public Module getSourceModule() {
    IChannel ch = getOutputChannel().getValue();
    if (ch != null) {
      return ch.getOwnerModule();
    } else {
      return null;
    }
  }

  @Override
  public boolean isSwitchLogic() {
    return false;
  }

  @Override
  protected FanTestSettings createSettings() {
    return new FanTestSettings(this);
  }
}
