/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The interface of control logic.
 */
public interface ICLogic extends INode, IPseudoPointSource, IValidation {

  /**
   * The name of property {@value} . <li>Value type: {@link Integer}.</li>
   */
  String PROPERTY_GROUP = "group";

  /**
   * The name of read-only property {@value} . <li>Value type: {@link String}.</li>
   */
  String PROPERTY_ENABLED = "enabled";
  
  String PROPERTY_OPERABLE = "operable";

  /**
   * The name of property {@value} . <li>Value type: {@link String}.</li>
   */
  String PROPERTY_CUSTOM_NAME = "customName";


  /**
   * Gets the custom name of this control logic. Getter method of property
   * {@linkplain PROPERTY_CUSTOM_NAME}.
   */
  String getCustomName();

  /**
   * Change the custom name of this control logic. Setter method of property
   * {@linkplain PROPERTY_CUSTOM_NAME}.
   *
   * @param name
   *          new name.
   */
  void setCustomName(String name);

  /**
   * Gets the type of this control logic.
   */
  ICLogicType getType();

  /**
   * Getter method of property {@linkplain PROPERTY_GROUP}.
   */
  @Override
  int getGroup();

  /**
   * Setter method of property {@linkplain PROPERTY_GROUP}.
   */
  void setGroup(int group);

  /**
   * Gets all inputs of this control logic.
   */
  CLogicIO<VirtualPoint>[] getInputs();
  
  IInputsManager getInputsManager();
  
  /**
   * Gets all outputs of this control logic.
   */
  CLogicIO<?>[] getOutputs();
  
  IOutputsManager<?> getOutputsManager();

  boolean isEnabled();

  void setEnabled(boolean enabled);

  ICLogicPointsManager getPointsManager();

  /**
   * Gets a <code>PseudoPoint</code> of this control logic by specifying ID.
   */
  IPseudoPoint getPointByID(int id);

  /**
   * Gets all <code>PseudoPoint</code> of this control logic.
   */
  IPseudoPoint[] getAllPoints();

  /**
   * Gets the number of all <code>PseudoPoint</code> of this control logic.
   *
   * @return The number of <code>PseudoPoint</code>.
   */
  int getAllPointsNum();

  /**
   * Gets the number of inputs.
   *
   * @return the number of inputs.
   */
  int getInputNum();

  /**
   * Gets the number of outputs.
   *
   * @return The number of outputs.
   */
  int getOutputNum();

  /**
   * Gets the summary information of this control logic in HTML format.
   *
   * @return HTML content of this control logic.
   */
  String getHTMLSummary();

  /**
   * Reads all parameters from XML.
   */
  void readParamFromXML(CLogicParametersT xml);

  /**
   * Writes all parameters to XML.
   */
  void writeParamToXML(CLogicParametersT xml);

  String getUUID();

  void setUUID(String uuid);
  
  ICLogicSettings getSettings();

}
