/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.psupply;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ComponentModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DCTL_LOGIC_TRIGGER;


class PowerSupplyControlSettingsEditor extends AbstractCLogicSettingsEditor {

  /**
   * 
   */
  private static final String LABEL_DURATION = "Disconnection duration";

  public PowerSupplyControlSettingsEditor(PowerSupplyControlSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result, pm, PowerSupplyControlSettings.PROPERTY_DURATION, LABEL_DURATION);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    
    DefaultFormBuilder builder = getDefaultBuilder();


    createIntField(LABEL_DURATION, PowerSupplyControlSettings.PROPERTY_DURATION, Units.SECS);
    
    
    return builder.getPanel();
  }

}

