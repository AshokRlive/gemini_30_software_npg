/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogic;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class AbstractCLogicTest {

  private AbstractCLogic<?> fixture;

  private String[] readWriteProperties = {
//      Fixture.PROPERTY_CUSTOM_NAME,
  };

  private String[] readOnlyProperties = {
      Fixture.PROPERTY_NAME,
  };


  @Before
  public void setUp() throws Exception {
    fixture = new Fixture(CLogicType.builder(123, "StubLogicType").get());
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

//  @Test(expected = NullPointerException.class)
//  public void testConstructWithNullArg() {
//    new Fixture(null);
//  }

  @Test
  public void testGetName() {
    assertTrue(Strings.isNotBlank(fixture.getName()));
  }

  @Test
  public void testGetFullName() {
    String oldName, newName;

    assertNotNull(fixture.getName());

    /* The full name will be updated if control logic's group has changed */
    oldName = fixture.getName();
    fixture.setGroup(fixture.getGroup() + 1);
    newName = fixture.getName();
    assertNotSame(oldName, newName);

    /* The full name will be updated if control logic's name has changed */
    oldName = fixture.getName();
    fixture.setCustomName(fixture.getName() + " test");
    newName = fixture.getName();
    assertNotSame(oldName, newName);
  }

  /*
   * The group of control logic must be > 0 cause group 0 is used by standard
   * point
   */
  @Test
  public void testSetGroup() {
    assertTrue(fixture.getGroup() > 0);

    fixture.setGroup(1);
    assertEquals(1, fixture.getGroup());

    // Invalid group value is supposed to be ignored.
    fixture.setGroup(0);
    assertEquals(1, fixture.getGroup());
    fixture.setGroup(-1);
    assertEquals(1, fixture.getGroup());

    // Valid group value will be accepted
    fixture.setGroup(2);
    assertEquals(2, fixture.getGroup());

    fixture.setGroup(1000);
    assertEquals(1000, fixture.getGroup());
  }

  @Test
  public void testReadOnlyProperties() {
    BeanTestUtil.testReadOnlyProperties(fixture, readOnlyProperties);
  }

  @Test
  public void testReadWriteProperties() {
    BeanTestUtil.testReadWriteProperties(fixture, readWriteProperties);
  }


  // The group value of pseudo point should be the same as its owner's group
  // @Test
  // public void testPointGroupUpdating(){
  // for (IPseudoPoint point :points) {
  // assertEquals(point.getGroup(), owner.getGroup());
  // owner.setGroup(owner.getGroup() + 10);
  // assertEquals(point.getGroup(), owner.getGroup());
  // }
  // }

  // @Test
  // public void testUpdatePseudoPointReference(){
  // // Owner group change will make point reference text also changed
  // ref_old= point.getReferenceText();
  // assertNotNull(ref_old);
  // stub.setGroup(1);
  // ref_new = point.getRefernceText();
  // assertNotSame(ref_old, ref_new);
  // }

  private static class Fixture extends AbstractCLogic<ICLogicSettings> {

    Fixture(ICLogicType type) {
      super(type);
    }

    @Override
    public CLogicIO<?>[] getOutputs() {

      return null;
    }

    @Override
    public Module getSourceModule() {
      return null;
    }

    @Override
    public CLogicIO<VirtualPoint>[] getInputs() {
      return null;
    }

    @Override
    public IPseudoPoint[] getAllPoints() {
      return null;
    }

    @Override
    public IInputsManager getInputsManager() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public IOutputsManager<?> getOutputsManager() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public ICLogicPointsManager getPointsManager() {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    protected ICLogicSettings createSettings() {
      // TODO Auto-generated method stub
      return null;
    }

  }
}
