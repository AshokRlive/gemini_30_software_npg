/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.binarytoanalog;


import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.InputsManagerImpl;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.BTA_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Equipment Fault Handler Control Logic, which has
 * configurable amount of inputs.
 */
public final class BinaryToAnalogue extends PredefinedCLogic<BinaryToAnalogueSettings>  {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.BTA, "Binary to Analogue Converter")
      .desp("The control logic for converting a list of binaries to an analogue.")
      .get();
  
  private final BTAInputsManager inputs = new BTAInputsManager(this, VirtualPointType.binaryTypes());

  public BinaryToAnalogue() {
    super(TYPE, null, BTA_POINT.values());
    inputs.setAllowUserChange(true);
    inputs.setMaximumSize(16);
  }


  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return inputs.getAll();
  }

  @Override
  public BTAInputsManager getInputsManager() {
    return inputs;
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

//  @Override
//  public boolean isCustomLogic() {
//    return true;
//  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }

  @Override
  protected BinaryToAnalogueSettings createSettings() {
    return new BinaryToAnalogueSettings(this);
  }
  
  public static class BTAInputsManager extends InputsManagerImpl {

    public BTAInputsManager(ICLogic owner, VirtualPointType[] supportedInputTypes) {
      super(owner, supportedInputTypes);
    }
    
    @Override
    protected CLogicIOPoint createNewInput(boolean isMandatory, boolean allowUserChangeLabel, boolean allowUserDelete,
        VirtualPointType[] types, String labelStr) {
      CLogicIOPoint p = super.createNewInput(isMandatory, allowUserChangeLabel, allowUserDelete, types, labelStr);
      p.setAllowChangeLabel(false);
      return p;
    }
    
    @Override
    public void addIO(Collection<CLogicIO<VirtualPoint>> newIO) {
      super.addIO(newIO);
      updateAllLabels();
    }

    @Override
    protected void addIO(CLogicIO<VirtualPoint> newIO) {
      super.addIO(newIO);
      updateAllLabels();
    }

    @Override
    public void removeAll(Collection<CLogicIO<?>> removeItems) {
      super.removeAll(removeItems);
      updateAllLabels();
    }

    @Override
    public void remove(CLogicIO<?> removeItem) {
      super.remove(removeItem);
      updateAllLabels();
    }

    @Override
    public void removeAll() {
      super.removeAll();
      updateAllLabels();
    }

    private void updateAllLabels() {
      CLogicIO<VirtualPoint>[] all = getAll();
      for (int i = 0; i < all.length; i++) {
        all[i].setLabel("Bit "+i);
      }
    }
  }

}
