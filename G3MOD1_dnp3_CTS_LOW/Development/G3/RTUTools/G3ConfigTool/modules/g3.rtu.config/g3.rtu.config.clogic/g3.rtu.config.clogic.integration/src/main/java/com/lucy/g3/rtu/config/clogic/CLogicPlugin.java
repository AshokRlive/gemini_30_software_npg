/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.clogic.action.ActionLogicFactory;
import com.lucy.g3.rtu.config.clogic.analoguecontrol.AnaloguePointControlLogicFactory;
import com.lucy.g3.rtu.config.clogic.automation.iec61131.IEC61131LogicFactory;
import com.lucy.g3.rtu.config.clogic.automation.iec61499.IEC61499LogicFactory;
import com.lucy.g3.rtu.config.clogic.batterytest.BatteryTestLogicFactory;
import com.lucy.g3.rtu.config.clogic.binarytoanalog.BinaryToAnalogueLogicFactory;
import com.lucy.g3.rtu.config.clogic.changeover.ChangeOverLogicFactory;
import com.lucy.g3.rtu.config.clogic.countercommand.CounterCommandLogicFactory;
import com.lucy.g3.rtu.config.clogic.digitaloutputcontrol.DigitalOutputControlLogicFactory;
import com.lucy.g3.rtu.config.clogic.digitalpointcontrol.DigitalPointControlLogicFactory;
import com.lucy.g3.rtu.config.clogic.doubletosingleconverter.DoubleToSingleLogicFactory;
import com.lucy.g3.rtu.config.clogic.dummyswitch.DummySwitchLogicFactory;
import com.lucy.g3.rtu.config.clogic.externalfpi.ExternalFPILogicFactory;
import com.lucy.g3.rtu.config.clogic.fantest.FanTestLogicFactory;
import com.lucy.g3.rtu.config.clogic.fpireset.FPIResetLogicFactory;
import com.lucy.g3.rtu.config.clogic.fpitest.FPITestLogicFactory;
import com.lucy.g3.rtu.config.clogic.impl.CLogicManagerImpl;
import com.lucy.g3.rtu.config.clogic.logicgate.LogicGateLogicFactory;
import com.lucy.g3.rtu.config.clogic.minmaxavg.MinMaxAvgLogicFactory;
import com.lucy.g3.rtu.config.clogic.motorsupply.MotorSupplyLogicFactory;
import com.lucy.g3.rtu.config.clogic.offlocalremote.OffLocalRemoteFactory;
import com.lucy.g3.rtu.config.clogic.pltu.PLTULogicFactory;
import com.lucy.g3.rtu.config.clogic.pseudoclock.PseudoClockLogicFactory;
import com.lucy.g3.rtu.config.clogic.psupply.PowerSupplyControlLogicFactory;
import com.lucy.g3.rtu.config.clogic.sectionaliser.SectionaliserLogicFactory;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogicFactory;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingLogicFactory;
import com.lucy.g3.rtu.config.clogic.tcl.TCLTrippingLogicFactory;
import com.lucy.g3.rtu.config.clogic.threshold.ThresholdLogicLogicFactory;
import com.lucy.g3.rtu.config.clogic.ui.CLogicManagerPage;
import com.lucy.g3.rtu.config.clogic.ui.CLogicPage;
import com.lucy.g3.rtu.config.clogic.voltagemismatch.VoltageMismatchLogicFactory;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;

/**
 * The Class VirtualPoint Plugin.
 */
public class CLogicPlugin implements IConfigPlugin{
  private CLogicPlugin () {}
  
  public static void init() {
    // Register control logic to configFactory
    ConfigFactory.getInstance().registerFactory(CLogicManager.CONFIG_MODULE_ID,new CLogicConfigFactory());
    
    // Register control logic to pageFactory
    PageFactories.registerFactory(PageFactories.KEY_LOGIC, new CLogicPageFactory());
    
    // Register control logic to logicFactory
    CLogicFactories.registerFactory(ICLogicType.ACTION         , ActionLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.OLR            , OffLocalRemoteFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.CHANGE_OVER    , ChangeOverLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.FAN            , FanTestLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.SGL            , SwitchGearLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.LOGIC_GATE     , LogicGateLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.DOL            , DigitalOutputControlLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.DSL            , DummySwitchLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.BAT_CHARGER    , BatteryTestLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.SECTIONALISER  , SectionaliserLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.EXT_FPI        , ExternalFPILogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.PLTU           , PLTULogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.VOLTAGEMISMATCH, VoltageMismatchLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.MMAVG          , MinMaxAvgLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.PCLK           , PseudoClockLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.DIGCTRL        , DigitalPointControlLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.ANALOGCTRL     , AnaloguePointControlLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.FPI_TEST       , FPITestLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.FPI_RESET      , FPIResetLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.THRESHOLD      , ThresholdLogicLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.IEC61131       , IEC61131LogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.IEC61499       , IEC61499LogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.COUNTER_COMMAND, CounterCommandLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.BTA            , BinaryToAnalogueLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.DTS            , DoubleToSingleLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.TCL_SENSING    , TCLSensingLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.TCL_TRIPPING   , TCLTrippingLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.POWER_SUPPLY   , PowerSupplyControlLogicFactory.INSTANCE);
    CLogicFactories.registerFactory(ICLogicType.MOTOR_SUPPLY   , MotorSupplyLogicFactory.INSTANCE);
  }
  
  
  private static class CLogicConfigFactory implements IConfigModuleFactory {
    @Override
    public CLogicManager create(IConfig owner) {
      return new CLogicManagerImpl(owner);
    }

  }
  
  private static class CLogicPageFactory implements IPageFactory {
    @Override
    public Page createPage(Object data) {
      if (data instanceof ICLogic) {
        return new CLogicPage((ICLogic) data);
        
      } else if (data instanceof CLogicManager) {
        return new CLogicManagerPage((CLogicManager) data);
        
      } 
      
      return null;
    }
  }

  public static void destroy() {
    ConfigFactory.getInstance().deregisterFactory(CLogicManager.CONFIG_MODULE_ID);
    PageFactories.deregisterFactory(PageFactories.KEY_LOGIC);
    CLogicFactories.clear();
  }
}
