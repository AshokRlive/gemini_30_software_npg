/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.util.IteratorIterable;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutomationConstantType;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.automation.AutomationConstant;
import com.lucy.g3.rtu.config.clogic.automation.iec61499.FORTESysFileParser.G3FBConfig;
import com.lucy.g3.rtu.config.clogic.automation.iec61499.FORTESysFileParser.G3FBConfig.G3FB_DATATYPE;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

class IEC61499LogicAddingWizard extends WizardPage {
  private static final Color ENGINE_POINT_COLOR = UIThemeResources.COLOUR_LOGIC_BLOCK_BORDER_COLOR;

  public final static String PAGE_ID = "IEC61499LogicAddingWizard";
  
  private final static Preferences PREF = Preferences.userRoot();
  private final static String DIR_KEY = "4DIACFilePath";
  private final FORTESysFileParser sysParser= new FORTESysFileParser();

  private CLogicManager manager;

  public IEC61499LogicAddingWizard(IConfig config) {
    super("Configuration");
    this.manager = config.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    initComponents();
    
    combResource.setRenderer(resourceRender);
    BusyCursorAction.apply(btnSelectBoot, this);
    BusyCursorAction.apply(btnSelectSys, this);
    
    updateVisiblity();
    putWizardData(PAGE_ID, this);
  }
  
  private String getSelectedResource() {
    Element res = (Element) combResource.getSelectedItem();
    return res == null ? null : res.getAttributeValue("Name");
  }
  
  @Override
  public WizardPanelNavResult allowNext(String stepName, Map settings, Wizard wizard) {
    if(!cboxImport.isSelected()) 
      return WizardPanelNavResult.PROCEED;

    try {
      sysParser.parseResource((Element) combResource.getSelectedItem());
      
      String resName = getSelectedResource();
      
      /* Check if the resource name conflicts*/
      IEC61499Logic logic = findLogicWithResource(manager, resName);
      if(logic != null ) {
          int ret = JOptionPane.showConfirmDialog(this, 
              String.format("The IEC61499 logic \"%s\" already exists.\nDo you want to overwrite it?", resName),
              "Overwrite", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
          return (ret == JOptionPane.YES_OPTION) ? 
                          WizardPanelNavResult.PROCEED
                        :WizardPanelNavResult.REMAIN_ON_PAGE;
      }
      
      return WizardPanelNavResult.PROCEED;
      
    } catch (Exception e) {
      Logger.getLogger(getClass()).error("Failed to parse the selected resource!", e);
      MessageDialogs.error(this, "Failed to parse the selected resource!", e);
      return WizardPanelNavResult.REMAIN_ON_PAGE;
    }
  }
  
  private static IEC61499Logic findLogicWithResource(CLogicManager manager, String resName) {
    Collection<IEC61499Logic> existing61499 = manager.getAllControlLogics(IEC61499Logic.class);

    for (IEC61499Logic logic : existing61499) {
      IEC61499LogicSettings logicSettings = (IEC61499LogicSettings) logic.getSettings();
      if(logicSettings.getResName().equals(resName)) {
        return logic;
      }
    }
    
    return null;
  }

  private void updateVisiblity() {
    centerPanel.setVisible(cboxImport.isSelected());
  }
  
  public IEC61499Logic createLogic() {
    IEC61499Logic logic = findLogicWithResource(manager, getSelectedResource());
    if(logic == null)
      logic = IEC61499LogicFactory.INSTANCE.createLogic();
    
    applyConfiguration(logic);
    
    return logic;
  }
  
  private void applyConfiguration(IEC61499Logic logic) {
    if(cboxImport.isSelected()) {
      try {
        logic.getInputsManager().setInputs(createInputs(logic, sysParser.getG3inputs()));
        logic.getPointsManager().setPoints(createPoints(logic, sysParser.getG3outputs()));
        logic.getOutputsManager().setOutputs(createOutputs(logic, sysParser.getG3controls()));
        logic.getSettings().getConstantsManager().setConstants(createConstants(logic, sysParser.getG3parameters()));
        
        // Do not allow user change logic imported from system.
        logic.setImported(true);
        
        // Set resource name
        IEC61499LogicSettings settings = (IEC61499LogicSettings) logic.getSettings();
        settings.setResName(sysParser.getResourceName());
        logic.setCustomName(sysParser.getResourceName());
        
        settings.setScheme(FileUtils.readFileToString(new File(tfBootPath.getText()), "UTF-8"));
        
      }catch(Exception e) {
        Logger.getLogger(getClass()).error("Failed to parse the selected resource!", e);
        MessageDialogs.error(this, "Failed to parse the selected resource!", e);
      }
    }
  }

  private AutomationConstant[] createConstants(IEC61499Logic logic, ArrayList<G3FBConfig> g3parameters) {
    sort(g3parameters);
    int num = g3parameters.isEmpty() ? 0 : (g3parameters.get(g3parameters.size() - 1).id + 1);
    AutomationConstant[] params = new AutomationConstant[num];
    for (G3FBConfig conf : g3parameters) {
      params[conf.id] = new AutomationConstant(conf.name,convert2ConstantType(conf.getType()));
    }
    // Fill in null inputs
    for (int i = 0; i < params.length; i++) {
      if (params[i] == null) {
        params[i] = new AutomationConstant("Unused", AutomationConstantType.Boolean);
      }
    }
    return params;
  }

  private static Collection<CLogicIO<VirtualPoint>> createInputs(IEC61499Logic logic, ArrayList<G3FBConfig> inputs) {
    sort(inputs);
    int inputNum = inputs.isEmpty() ? 0 : (inputs.get(inputs.size() - 1).id + 1);
    @SuppressWarnings("unchecked")
    CLogicIO<VirtualPoint>[] logicInputs = new CLogicIO[inputNum];
    for (G3FBConfig conf : inputs) {
      VirtualPointType[] types = convert2VirtualPointType(conf.getType());
      logicInputs[conf.id] = new CLogicIOPoint(logic, conf.name, types, false, false, conf.isOptional(), true);
      if(conf.isReserved()) {
        logicInputs[conf.id].putProperty(VirtualPoint.CLIENT_PROPERTY_FG_COLOR, ENGINE_POINT_COLOR);
      }
    }
    // Fill in null inputs
    for (int i = 0 ; i < logicInputs.length; i++) {
      if(logicInputs[i] == null) {
        logicInputs[i] = new CLogicIOPoint(logic, "Unused", VirtualPointType.values(), false, false, true, false);
        logicInputs[i].putProperty(VirtualPoint.CLIENT_PROPERTY_FG_COLOR, ENGINE_POINT_COLOR);
      }
    }
    return Arrays.asList(logicInputs);
  }
  
  private static AutomationConstantType convert2ConstantType(G3FB_DATATYPE fbType) {
    switch(fbType){
    case BOOL:
      return AutomationConstantType.Boolean;
    case INT:
      return AutomationConstantType.Integer;
    case REAL:
      return AutomationConstantType.Float;
    default:
      throw new IllegalArgumentException("Unsupported type: " + fbType);
    }
  }
  
  private static VirtualPointType[] convert2VirtualPointType(G3FB_DATATYPE fbType) {
    switch(fbType){
    case ANALOGUE:
      return VirtualPointType.analoygTypes();
    case BINARY:
      return VirtualPointType.binaryTypes();
    case DOUBLEBINARY:
      return VirtualPointType.doubleBinaryTypes();
    case COUNTER:
      return new VirtualPointType[]{VirtualPointType.COUNTER};
    default:
      throw new IllegalArgumentException("Unsupported type: " + fbType);
    }
  }
  
  private static Collection<IPseudoPoint> createPoints(IEC61499Logic logic, ArrayList<G3FBConfig> pointsConf) {
    sort(pointsConf);
    int pointNum = pointsConf.isEmpty() ? 0: (pointsConf.get(pointsConf.size() - 1).id + 1);
    IPseudoPoint[] logicPoints = new IPseudoPoint[pointNum];
    for (G3FBConfig conf : pointsConf) {
      switch(conf.getType()){
      case ANALOGUE:
        logicPoints[conf.id] = PseudoPointFactory.createPseudoAnalogPoint(conf.name, logic, conf.id);
        break;
      case BINARY:
        logicPoints[conf.id] = PseudoPointFactory.createPseudoBinaryPoint(conf.name, logic, conf.id);
        break;
      case DOUBLEBINARY:
        logicPoints[conf.id] = PseudoPointFactory.createPseudoDBinaryPoint(conf.name, logic, conf.id);
        break;
      case COUNTER:
        logicPoints[conf.id] = PseudoPointFactory.createPseudoCounterPoint(conf.name, logic, conf.id);
        break;
      default:
        throw new IllegalArgumentException("Unsupported point type: " + conf.getType());
      }
      
      if(conf.isReserved()) {
        logicPoints[conf.id].putProperty(VirtualPoint.CLIENT_PROPERTY_FG_COLOR, ENGINE_POINT_COLOR);
      }
    }
    
    
    // Fill in null inputs
    for (int i = 0 ; i < logicPoints.length; i++) {
      if(logicPoints[i] == null) {
        logicPoints[i] = PseudoPointFactory.createPseudoBinaryPoint("Unused", logic, i);
        logicPoints[i].putProperty(VirtualPoint.CLIENT_PROPERTY_FG_COLOR, ENGINE_POINT_COLOR);
      }
    }
    return Arrays.asList(logicPoints);
  }
  
  private static Collection<CLogicIO<ICLogic>> createOutputs(IEC61499Logic logic, ArrayList<G3FBConfig> outputsConf) {
    sort(outputsConf);
    int outputNum = outputsConf.isEmpty() ? 0 : (outputsConf.get(outputsConf.size() - 1).id + 1);
    @SuppressWarnings("unchecked")
    CLogicIO<ICLogic>[] outputs = new CLogicIO[outputNum];
    
    for (G3FBConfig conf : outputsConf) {
        outputs[conf.id] = new CLogicIOLogic(logic, conf.name,
            CLogicFactories.getAllOperatibleTypes(), false, true, false, false);
    }
    // Fill in null inputs
    for (int i = 0 ; i < outputs.length; i++) {
      if(outputs[i] == null) {
        outputs[i] = new CLogicIOLogic(logic, "Unused",
            CLogicFactories.getAllOperatibleTypes(), false, false, false, false);
      }
    }
    return Arrays.asList(outputs);
  }
  
  private static ArrayList<G3FBConfig> sort(ArrayList<G3FBConfig> items) {
    Collections.sort(items, new Comparator<G3FBConfig>() {
      @Override
      public int compare(G3FBConfig o1, G3FBConfig o2) {
        return o1.id - o2.id;
      }
    });
    
    return items;
  }
  
  public static File chooseFile(Component parent, String desc, String extensions) {
    String curPath = PREF.get(DIR_KEY, "");
    JFileChooser fc = new JFileChooser(curPath);
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setAcceptAllFileFilterUsed(true);
    fc.setFileFilter(new FileNameExtensionFilter(desc, extensions));
    int ret = fc.showOpenDialog(parent);
    File file = fc.getSelectedFile();
    if(file != null)
      PREF.put(DIR_KEY, file.getParent());
      
    return ret == JFileChooser.APPROVE_OPTION ? file : null;
  }
  
  private void btnSelectSysActionPerformed() {
    File file = chooseFile(this, "Choose a 61499 system file(*.sys)", "sys");
    if(file != null) {
      tfSysPath.setText(file.getAbsolutePath());
      combResource.removeAllItems();
      try {
        sysParser.openFile(file);
        
        // Add resources to combobox for user selection.
        IteratorIterable<Element> res = sysParser.getAllResources();
        for (Element r : res ) {
          combResource.addItem(r);
        }
        combResource.setSelectedItem(null);
      } catch (JDOMException | IOException e) {
        Logger.getLogger(getClass()).error("Failed to parse the selected system file!", e);
        MessageDialogs.error(this, "Failed to parse the selected system file!", e);
      }
    }
  }

  private void btnSelectBootActionPerformed() {
    File file = chooseFile(this, "Choose a 61499 boot file(*.fboot)", "fboot");
    if(file != null) {
      tfBootPath.setText(file.getAbsolutePath());
    }
  }
  
  private void cboxImportActionPerformed() {
    updateVisiblity();
  }
  

  @Override
  protected String validateContents(Component component, Object event) {
    if(cboxImport.isSelected()) {
      if(tfSysPath.getText().isEmpty())
        return "System file not selected!";
      
      if(tfBootPath.getText().isEmpty())
        return "Boot file not selected!";
      
      if(combResource.getSelectedItem() == null) {
        return "Resource not selected!";
      }
    }
    
    return null;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    centerPanel = new JPanel();
    label1 = new JLabel();
    tfSysPath = new JTextField();
    btnSelectSys = new JButton();
    label2 = new JLabel();
    tfBootPath = new JTextField();
    btnSelectBoot = new JButton();
    label3 = new JLabel();
    combResource = new JComboBox<>();
    panel2 = new JPanel();
    cboxImport = new JCheckBox();

    //======== this ========
    setLayout(new BorderLayout(0, 10));

    //======== centerPanel ========
    {
      centerPanel.setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "default, $lgap, default, $ugap, default, $lgap, default, $pgap, default"));

      //---- label1 ----
      label1.setText("Select a *.sys file:");
      centerPanel.add(label1, CC.xy(1, 1));

      //---- tfSysPath ----
      tfSysPath.setEditable(false);
      centerPanel.add(tfSysPath, CC.xywh(1, 3, 3, 1));

      //---- btnSelectSys ----
      btnSelectSys.setText("Select");
      btnSelectSys.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btnSelectSysActionPerformed();
        }
      });
      centerPanel.add(btnSelectSys, CC.xy(5, 3));

      //---- label2 ----
      label2.setText("Select a FORTE boot-file :");
      centerPanel.add(label2, CC.xy(1, 5));

      //---- tfBootPath ----
      tfBootPath.setEditable(false);
      centerPanel.add(tfBootPath, CC.xywh(1, 7, 3, 1));

      //---- btnSelectBoot ----
      btnSelectBoot.setText("Select");
      btnSelectBoot.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btnSelectBootActionPerformed();
        }
      });
      centerPanel.add(btnSelectBoot, CC.xy(5, 7));

      //---- label3 ----
      label3.setText("Select a resource to import:");
      centerPanel.add(label3, CC.xy(1, 9));
      centerPanel.add(combResource, CC.xy(3, 9));
    }
    add(centerPanel, BorderLayout.CENTER);

    //======== panel2 ========
    {
      panel2.setLayout(new FormLayout(
        "default, $lcgap, default",
        "default"));

      //---- cboxImport ----
      cboxImport.setText("Import configuration from 4DIAC files");
      cboxImport.setSelected(true);
      cboxImport.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          cboxImportActionPerformed();
        }
      });
      panel2.add(cboxImport, CC.xy(1, 1));
    }
    add(panel2, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel centerPanel;
  private JLabel label1;
  private JTextField tfSysPath;
  private JButton btnSelectSys;
  private JLabel label2;
  private JTextField tfBootPath;
  private JButton btnSelectBoot;
  private JLabel label3;
  private JComboBox<Object> combResource;
  private JPanel panel2;
  private JCheckBox cboxImport;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  final private DefaultListCellRenderer resourceRender = new DefaultListCellRenderer() {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      Element e = (Element) value;
      if(e != null)
        setText(e.getAttributeValue("Name"));
      
      return comp;
    }
  };
}
