
package com.lucy.g3.rtu.config.clogic;

import javax.swing.JPanel;

import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;


public abstract class AbstractClogicFactory implements IClogicFactory {

  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return null;
  }

  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return null;
  }

  @Override
  public JPanel[] createConfigTabs(ICLogic clogic) {
    return null;
  }
  
  

}

