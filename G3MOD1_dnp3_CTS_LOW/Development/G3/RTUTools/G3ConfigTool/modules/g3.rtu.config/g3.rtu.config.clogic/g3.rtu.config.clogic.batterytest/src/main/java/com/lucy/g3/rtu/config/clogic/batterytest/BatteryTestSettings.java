/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.batterytest;

import com.g3schema.ns_clogic.BatteryTestParamT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

public class BatteryTestSettings extends AbstractCLogicSettings<BatteryTest> {
  public static final int BATTERY_TEST_DURATION_MAX = Integer.MAX_VALUE;// mins
  
  public static final int BATTERY_TEST_DURATION_MIN = 1; // mins
  
  public static final String PROPERTY_DURATION_SECS = "duration";

  private int duration = 120; // seconds

  public BatteryTestSettings(BatteryTest owner) {
    super(owner);
  }

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    if (duration <BATTERY_TEST_DURATION_MIN || duration > BATTERY_TEST_DURATION_MAX) {
      return;
    }
    Object oldValue = getDuration();
    this.duration = duration;
    firePropertyChange(PROPERTY_DURATION_SECS, oldValue, duration);
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    BatteryTestParamT xml = xml_param.batteryTestParam.first();
    setDuration((int) xml.duration.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    BatteryTestParamT xml_bat = xml.batteryTestParam.append();
    xml_bat.duration.setValue(getDuration());
  }


  @Override
  public IValidator getValidator() {
    return null;
  }

}

