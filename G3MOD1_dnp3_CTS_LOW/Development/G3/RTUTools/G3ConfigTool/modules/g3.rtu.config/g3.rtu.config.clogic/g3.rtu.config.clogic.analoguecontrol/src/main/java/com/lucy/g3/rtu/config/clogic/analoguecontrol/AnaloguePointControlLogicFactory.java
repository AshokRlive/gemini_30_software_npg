
package com.lucy.g3.rtu.config.clogic.analoguecontrol;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;


public class AnaloguePointControlLogicFactory extends AbstractClogicFactory {
  public final static AnaloguePointControlLogicFactory INSTANCE = new AnaloguePointControlLogicFactory();
  private AnaloguePointControlLogicFactory(){}
  
  @Override
  public ICLogic createLogic() {
    return new AnaloguePointControlLogic();
  }

  @Override
  public ICLogicType getLogicType() {
    return AnaloguePointControlLogic.TYPE;
  }
}

