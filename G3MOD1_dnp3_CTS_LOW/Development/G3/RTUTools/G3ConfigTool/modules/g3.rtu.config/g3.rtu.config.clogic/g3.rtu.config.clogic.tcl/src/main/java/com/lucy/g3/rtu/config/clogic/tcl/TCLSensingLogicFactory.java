
package com.lucy.g3.rtu.config.clogic.tcl;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class TCLSensingLogicFactory extends AbstractClogicFactory {
  public final static TCLSensingLogicFactory INSTANCE = new TCLSensingLogicFactory();
  private TCLSensingLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new TCLSensingSettingsEditor((TCLSensingSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new TCLSensingLogic();
  }
  
  @Override
  public ICLogicType getLogicType() {
    return TCLSensingLogic.TYPE;
  }
  
  
}

