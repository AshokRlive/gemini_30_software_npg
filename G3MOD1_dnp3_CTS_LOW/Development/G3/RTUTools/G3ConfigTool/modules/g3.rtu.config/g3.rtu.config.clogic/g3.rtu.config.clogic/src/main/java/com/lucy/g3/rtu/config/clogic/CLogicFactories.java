
package com.lucy.g3.rtu.config.clogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang3.ArrayUtils;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CONTROL_LOGIC_TYPE;

public class CLogicFactories {
  private final static HashMap<Integer, IClogicFactory> factoryMap = new HashMap<>();
  private static ICLogicType[] types = null;
  private static String[] typeNames = null;
  private static int[] operatibleTypes = null;

  public static ICLogicType[] getAllLogicTypes() {
    if (types == null) {
      Object[] keys = factoryMap.keySet().toArray();
      ArrayList<ICLogicType> names = new ArrayList<>();
      for (int i = 0; i < keys.length; i++) {
        names.add(getLogicType((int)keys[i]));
      }
      
      types = names.toArray(new ICLogicType[names.size()]);
    }

    return Arrays.copyOf(types, types.length);
  }
  
  public static int[] getAllLogicTypeIds() {
    Integer[] keys = factoryMap.keySet().toArray(new Integer[factoryMap.size()]);
    return ArrayUtils.toPrimitive(keys);
  }

  public static String[] getAllLogicTypeNames() {
    if (typeNames == null) {
      Object[] keys = factoryMap.keySet().toArray();
      ArrayList<String> names = new ArrayList<>();
      for (int i = 0; i < keys.length; i++) {
        names.add(getLogicType((int)keys[i]).getTypeName());
      }
      
      typeNames = names.toArray(new String[names.size()]);
    }

    return Arrays.copyOf(typeNames, typeNames.length);
  }
  
  public static ICLogicType getLogicType(int logicTypeId) {
    return getFactory(logicTypeId).getLogicType();
  }
  
  public static IClogicFactory getFactory(int logicTypeId) {
    IClogicFactory factory = factoryMap.get(logicTypeId);
    if(factory == null) {
      throw new IllegalStateException("Unsupported logic type: " + convertIdToStr(logicTypeId));
    }
    return factory;
  }
  
  static void registerFactory(int logicTypeId, IClogicFactory factory) {
    Preconditions.checkArgument(logicTypeId >= 0, "logicType must not be negative");
    Preconditions.checkNotNull(factory,   "factory must not be null");
    
    if(factory.getLogicType().getId() != logicTypeId)
      throw new IllegalArgumentException("Mismatched logic type id, expected:"+logicTypeId
          +" but factory gives:"+factory.getLogicType().getId());
    
    if(factoryMap.containsKey(logicTypeId))
      throw new IllegalStateException("Factory already exists for: "+ convertIdToStr(logicTypeId));
    
    factoryMap.put(logicTypeId, factory);
  }
  
  static void clear(){
    factoryMap.clear();
  }
  
  public static int[] getAllOperatibleTypes() {
    if(operatibleTypes == null) {
      ArrayList<Integer> foundTypes = new ArrayList<>();
      Object[] keys = factoryMap.keySet().toArray();
      for (int i = 0; i < keys.length; i++) {
        ICLogicType type = getLogicType((int)keys[i]);
        if(type.isOperable()) {
          foundTypes.add(type.getId());
        }
      }
      
      operatibleTypes = ArrayUtils.toPrimitive(foundTypes.toArray(new Integer[foundTypes.size()]));
    }
    
    return Arrays.copyOf(operatibleTypes,operatibleTypes.length);
  }
  
  private static String convertIdToStr(int logicTypeId) {
    CONTROL_LOGIC_TYPE e = CONTROL_LOGIC_TYPE.forValue(logicTypeId);
    return e == null ? String.valueOf(logicTypeId) : e.getDescription();
  }
}

