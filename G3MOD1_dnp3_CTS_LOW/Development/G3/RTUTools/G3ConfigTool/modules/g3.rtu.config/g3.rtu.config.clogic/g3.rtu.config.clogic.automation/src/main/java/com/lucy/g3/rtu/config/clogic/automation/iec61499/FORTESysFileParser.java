
package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.util.IteratorIterable;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.clogic.automation.iec61499.FORTESysFileParser.G3FBConfig.G3FB_DATATYPE;
import com.lucy.g3.rtu.config.clogic.automation.iec61499.IEC61499Logic.ENGINE_INPUT;
import com.lucy.g3.rtu.config.clogic.automation.iec61499.IEC61499Logic.ENGINE_POINT;

public class FORTESysFileParser {
  
  private static final String FB_NAME_G3DINPUT = "G3DInput";
  private static final String FB_NAME_G3AINPUT = "G3AInput";
  private static final String FB_NAME_G3DOUTPUT = "G3DOutput";
  private static final String FB_NAME_G3OUTPUTD = "G3OutputD";
  private static final String FB_NAME_G3AOUTPUT = "G3AOutput";
  private static final String FB_NAME_G3PARAM = "G3Parameter";
  private static final String FB_NAME_G3CONTROL = "G3Control";
  
  private ArrayList<G3FBConfig> g3inputs;
  private ArrayList<G3FBConfig> g3outputs;
  private ArrayList<G3FBConfig> g3controls;
  private ArrayList<G3FBConfig> g3parameters;

  private Element root;
  private String resName;

  public void openFile(File sysFile) throws JDOMException, IOException {
    SAXBuilder jdomBuilder = new SAXBuilder();
    
    /** Turn off the attempt to load the DTD.*/
    jdomBuilder.setEntityResolver(new EntityResolver()
    {
       public InputSource resolveEntity(String publicId, String systemId)
       {
         InputStream is = FORTESysFileParser.class.getResourceAsStream("LibraryElement.dtd");
          return new InputSource(is);
       }
    });
    
    Document jdomDocument = jdomBuilder.build(sysFile);
    root = jdomDocument.getRootElement();
  }
  
  public IteratorIterable<Element> getAllResources() {
    return root.getDescendants(Filters.element("Resource"));
  }

  
  public void parseResource(Element resource) {
    String id;
    String name;
    G3FB_DATATYPE type = G3FB_DATATYPE.UNKNOWN;
    
    String fbType;
    String fbName;
    String resName = resource.getAttributeValue("Name");
    String fbRef;
    
    g3inputs = new ArrayList<>();
    g3outputs = new ArrayList<>();
    g3controls = new ArrayList<>();
    g3parameters = new ArrayList<>();
    
    /* Add engine inputs*/
    final ENGINE_INPUT[] engineInputs = ENGINE_INPUT.values();
    for (int i = 0; i < engineInputs.length; i++) {
      addToList(g3inputs, new G3FBConfig(engineInputs[i].getName(), i, G3FB_DATATYPE.BINARY, true, true));      
    }
    
    /* Add engine outputs*/
    final ENGINE_POINT[] engineOutputs = ENGINE_POINT.values();
    for (int i = 0; i < engineOutputs.length; i++) {
      addToList(g3outputs, new G3FBConfig(engineOutputs[i].getName(), i, G3FB_DATATYPE.BINARY, false, true));      
    }
    
    IteratorIterable<Element> fbs = resource.getDescendants(Filters.element("FB"));
    for (Element fb : fbs) {
      fbName = fb.getAttributeValue("Name");
      fbType = fb.getAttributeValue("Type");
      fbRef = "resource:\""+resName+"\", function block:\""+fbName+"\", type:\""+fbType+"\"";
      
      if(isG3FunctionBlocks(fbType) == false)
        continue;
      
      id = getId(fb);
      
      /* Parse name*/
      name = getName(fb);
      if(name == null)
        name = fbName;
        
      /* Parse type*/
      if(fbType.equals("G3AInput") || fbType.equals("G3AOutput")) {
        type =  G3FB_DATATYPE.ANALOGUE;
      } else if(fbType.equals("G3Control")) {
        type =  G3FB_DATATYPE.UNKNOWN;
      }else {
        type =  getType(fb);
      }
      
      /* Validate */
      Preconditions.checkNotBlank(id, "ID value is missing in "+fbRef);
      Preconditions.checkNotNull(name, "NAME value is missing in "+fbRef);
      Preconditions.checkNotNull(type, "TYPE value is missing in "+fbRef);
      
      try {
      if(FB_NAME_G3DINPUT.equals(fbType) || FB_NAME_G3AINPUT.equals(fbType)) {
        addToList(g3inputs, new G3FBConfig(name, Integer.valueOf(id) + engineInputs.length, type));
      } else if(FB_NAME_G3DOUTPUT.equals(fbType) || FB_NAME_G3AOUTPUT.equals(fbType) ||FB_NAME_G3OUTPUTD.equals(fbType)){
        addToList(g3outputs, new G3FBConfig(name, Integer.valueOf(id) + engineOutputs.length, type));
      } else if(FB_NAME_G3CONTROL.equals(fbType)) {
        addToList(g3controls,new G3FBConfig(name, Integer.valueOf(id), type));
      } else if(FB_NAME_G3PARAM.equals(fbType)){
        addToList(g3parameters,new G3FBConfig(name, Integer.valueOf(id), type));
      }
      } catch(RuntimeException e) {
        throw new RuntimeException(String.format("Invalid function block:\"%s\" in resource:\"%s\" cause: %s", 
            fbName, resName, e.getMessage()));
      }
    }
    
    this.resName = resName;
  }
  
  private boolean isG3FunctionBlocks(String fbType) {
    return fbType != null && !fbType.isEmpty() && fbType.startsWith("G3");
  }

  public String getResourceName() {
    return this.resName;
  }

  private void addToList(ArrayList<G3FBConfig> list, G3FBConfig newItem) {
    for (G3FBConfig item: list) {
      if(item.id == newItem.id && item.type == newItem.type) {
        return;// Do not add to list if it already exists.
      } else if(item.id == newItem.id && item.type != newItem.type) {
        throw new RuntimeException("duplicated ID \"" + item.id+"\"");
      }
    }
    
    list.add(newItem);
  }
  
  
//  public String getID(Element FB) {
//    List<Element> params = FB.getChildren("Parameter");
//    for (Element p : params) {
//      if(isParameter(p))
//        return p.getAttributeValue("Value");
//    }
//    
//    return null;
//  }
  
  
  public ArrayList<G3FBConfig> getG3inputs() {
    return g3inputs;
  }

  
  public ArrayList<G3FBConfig> getG3outputs() {
    return g3outputs;
  }

  
  public ArrayList<G3FBConfig> getG3controls() {
    return g3controls;
  }

  
  public ArrayList<G3FBConfig> getG3parameters() {
    return g3parameters;
  }

  private static String getId(Element fb) {
    IteratorIterable<Element> params = fb.getDescendants(Filters.element("Parameter"));
    for (Element p: params) {
      if(isParameter(p, "ID"))
        return getParameterValue(p);
    }
    
    return null;
  }
  
  private static String getName(Element fb) {
    IteratorIterable<Element> params = fb.getDescendants(Filters.element("Parameter"));
    for (Element p: params) {
      if(isParameter(p, "NAME"))
        return getParameterValue(p);
    }
    
    return null;
  }
  
  private static G3FB_DATATYPE getType(Element fb) {
    IteratorIterable<Element> params = fb.getDescendants(Filters.element("Parameter"));
    for (Element p: params) {
      if(isParameter(p, "TYPE"))
        return G3FB_DATATYPE.valueOf(getParameterValue(p).toUpperCase());
    }
    
    return null;
  }

  private static boolean isParameter(Element p, String name) {
    return name.equals(p.getAttributeValue("Name"));
  }
  
  private static String getParameterValue(Element p) {
    return p.getAttributeValue("Value");
  }
  
//  public IteratorIterable<Element> getID(Element FB) {
//    return FB.getDescendants(Filters.element("Parameter"));
//  }
  
//  public String importForteBootFile(File bootfile) throws IOException {
//    return FileUtils.readFileToString(bootfile, "UTF-8");
//  }
  
  
  public static class G3FBConfig{
    public static enum G3FB_DATATYPE{
      UNKNOWN,
      
      /*Types for point*/
      ANALOGUE,
      BINARY,
      DOUBLEBINARY,
      COUNTER,
      
      /*Types for constants*/
      INT,
      BOOL,
      REAL
    }
    
    public final String name;
    public final int  id;
    public final G3FB_DATATYPE  type;
    private final boolean optional;
    private final boolean reserved;
    
    public G3FBConfig(String name, int id, G3FB_DATATYPE type) {
      this(name, id, type, false, false);
    }
    
    public G3FBConfig(String name, int id, G3FB_DATATYPE type, boolean optional, boolean reserved) {
      super();
      this.name = name;
      this.id = id;
      this.type = type;
      this.optional = optional;
      this.reserved = reserved;
    }
    public boolean isOptional() {
      return optional;
    }
    
    public boolean isReserved(){
      return reserved;
    }
    
    @Override
    public String toString() {
      return String.format("[%d]%s (type:%s)", id, name, type);
    }
    
    public G3FB_DATATYPE getType() {
      return type;
    }

  }
}

