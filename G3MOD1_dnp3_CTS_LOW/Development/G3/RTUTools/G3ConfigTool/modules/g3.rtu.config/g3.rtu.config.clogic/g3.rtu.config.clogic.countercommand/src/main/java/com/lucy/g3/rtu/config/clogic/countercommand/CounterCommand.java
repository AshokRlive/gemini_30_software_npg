/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.countercommand;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.common.ControlLogicDef.COUNTER_COMMAND_OUTPUT;


/**
 * The implementation of Counter Command Logic.
 */
public class CounterCommand extends PredefinedCLogic<CounterCommandSettings> implements IOperableLogic{
  
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.COUNTER_COMMAND, "Counter Command")
      .desp("The control logic for controlling counter points.")
      .opMode(OperationMode.GENERIC_OPERATE)
      .get();
  
  private final CLogicIOPoint[] outputs;
  
  public CounterCommand() {
    super(TYPE, null, null);
    
    
    COUNTER_COMMAND_OUTPUT [] enums = COUNTER_COMMAND_OUTPUT.values();
    outputs = new CLogicIOPoint[enums.length];
    for (int i = 0; i < enums.length; i++) {
      outputs[i] = new CLogicIOPoint(this, "Output", new VirtualPointType[]{VirtualPointType.COUNTER},false);
      outputs[i].setOptional(false);
    }
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return Arrays.copyOf(outputs,outputs.length);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  protected CounterCommandSettings createSettings() {
    return new CounterCommandSettings(this);
  }
}

