/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.externalfpi;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.common.ControlLogicDef.EXTERNAL_FPI_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.EXTERNAL_FPI_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.EXTERNAL_FPI_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of External FPI Control Logic.
 */
public final class ExternalFPI extends PredefinedCLogic<ICLogicSettings> implements IOperableLogic {

  private final CLogicIOChannel[] outputs;
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.EXT_FPI, "External FPI")
      .desp("The logic for controlling external FPI.")
      .opMode(OperationMode.OPEN_CLOSE)
      .allowUserAdd(false)
      .get();


  public ExternalFPI() {
    super(TYPE, EXTERNAL_FPI_INPUT.values(), EXTERNAL_FPI_POINT.values());

    // Outputs
    outputs = new CLogicIOChannel[EXTERNAL_FPI_OUTPUT.values().length];
    initOutputs();

    // Bind logic name.
    bindDescriptionToIO(outputs[0]);
  }

  private void initOutputs() {
    boolean isOptional = false;
    boolean enabled = true;
    String name;
    isOptional = false;
    enabled = true;
    EXTERNAL_FPI_OUTPUT[] outputEnums = EXTERNAL_FPI_OUTPUT.values();
    for (int i = 0; i < outputEnums.length; i++) {
      name = outputEnums[i].getDescription();
      outputs[i] = new CLogicIOChannel(
          this,
          name,
          isOptional,
          enabled,
          new ChannelType[] { ChannelType.ANALOG_OUTPUT, ChannelType.DIGITAL_OUTPUT },
          null,
          null);
    }
  }

  public CLogicIO<IChannel> getOutputCh() {
    int index = EXTERNAL_FPI_OUTPUT.EXTERNAL_FPI_OUTPUT_CHANNEL.ordinal();
    return outputs[index];
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return outputs;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }

  @Override
  public boolean isSwitchLogic() {
    return false;
  }

  @Override
  protected ICLogicSettings createSettings() {
    return null;
  }
}
