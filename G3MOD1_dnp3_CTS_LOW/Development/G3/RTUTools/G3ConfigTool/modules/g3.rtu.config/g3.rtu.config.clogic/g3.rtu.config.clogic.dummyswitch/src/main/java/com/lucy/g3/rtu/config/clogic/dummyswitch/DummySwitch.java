/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.dummyswitch;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DSL_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DSL_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Dummy Switch Control Logic.
 */
public final class DummySwitch extends PredefinedCLogic<DummySwitchSettings> implements ISupportOffLocalRemote, IOperableLogic {

  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.DSL, "Dummy Switch")
      .desp("The control logic for controlling Dummy Switch.")
      .opMode(OperationMode.OPEN_CLOSE)
      .maxAmount(1)
      .requireOLR(true)
      .get();
  
  public DummySwitch() {
    super(TYPE, DSL_INPUT.values(), DSL_POINT.values());
  }
  
  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(DSL_INPUT.DSL_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }

  @Override
  public boolean isSwitchLogic() {
    return true;
  }

  @Override
  protected DummySwitchSettings createSettings() {
    return new DummySwitchSettings(this);
  }
}
