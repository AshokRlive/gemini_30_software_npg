
package com.lucy.g3.rtu.config.clogic.minmaxavg;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


public class MinMaxAvgLogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor {
  public final static MinMaxAvgLogicFactory INSTANCE = new MinMaxAvgLogicFactory();
  private MinMaxAvgLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new MinMaxAvgLogicSettingsEditor((MinMaxAvgLogicSettings) settings);
  }

  @Override
  public  MinMaxAvgLogic createLogic() {
    return new MinMaxAvgLogic();
  }

  @Override
  public ICLogicType getLogicType() {
    return MinMaxAvgLogic.TYPE;
  }

  
  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }
  
  
  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    return new WizardPage[]{
        new MinMaxAvgLogicOptions()
    };
  }
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    MinMaxAvgLogic newLogic = createLogic();
    Boolean supportCounter = (Boolean) wizardData.get(MinMaxAvgLogicOptions.KEY_SUPPORT_COUNTER);
    newLogic.getSettings().setCounterEnabled(supportCounter);
    return newLogic;
  }
}

