/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.analoguecontrol;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.ANALOGCTRL_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.ANALOGCTRL_POINT;

/**
 * Implementation of Analogue Control Control Logic.
 */
public class AnaloguePointControlLogic extends PredefinedCLogic<ICLogicSettings> implements ISupportOffLocalRemote, IOperableLogic {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.ANALOGCTRL, "Analogue Control Point")
      .desp("Control Logic for controlling Analogue Point")
      .opMode(OperationMode.OPEN_CLOSE)
      .requireOLR(true)
      .get();
  
  public AnaloguePointControlLogic() {
    super(TYPE, ANALOGCTRL_INPUT.values(), ANALOGCTRL_POINT.values());

  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(ANALOGCTRL_INPUT.ANALOGCTRL_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }


  @Override
  public boolean isSwitchLogic() {
    return false;
  }

  @Override
  protected ICLogicSettings createSettings() {
    return null;
  }
}
