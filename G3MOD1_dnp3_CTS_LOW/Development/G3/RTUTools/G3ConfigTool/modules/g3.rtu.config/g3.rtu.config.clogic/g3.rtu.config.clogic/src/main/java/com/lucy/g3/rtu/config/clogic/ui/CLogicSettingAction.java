/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.clogic.ICLogic;

/**
 * Action for triggering CLogic Parameter dialog.
 */
public class CLogicSettingAction extends AbstractAction {

  private ICLogic clogic;


  public CLogicSettingAction(ICLogic cLogic) {
    super();
    putValue(NAME, "Settings");
    this.clogic = cLogic;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    AbstractDialog dialog = new CLogicSettingsDialog(WindowUtils.getMainFrame(), clogic);
    dialog.setVisible(true);
  }
}
