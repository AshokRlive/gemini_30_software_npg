/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.minmaxavg;

import javax.swing.JCheckBox;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * Wizard page for configuring MinMax Logic options.
 */
class MinMaxAvgLogicOptions extends WizardPage {

  /**
   * The key for the value:{@value} . <li>Value type: {@link Boolean}.</li>
   */
  static final String KEY_SUPPORT_COUNTER = "Support Counter";


  public MinMaxAvgLogicOptions() {
    super("Configuration");
    initComponents();

    cbCounterSupport.setName(KEY_SUPPORT_COUNTER);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    cbCounterSupport = new JCheckBox();

    // ======== this ========
    setLayout(new FormLayout(
        "default",
        "default"));

    // ---- cbCounterSupport ----
    cbCounterSupport.setText("Counter Point Support");
    add(cbCounterSupport, CC.xy(1, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox cbCounterSupport;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
