
package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogic;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicSettings;

public class IEC61499Logic extends AutomationLogic {
  static enum ENGINE_POINT {
    AUTO_RUNNING("Auto Running"),
    AUTO_FAIL("Auto Fail");
    ENGINE_POINT(String name){
      this.name = name;
    }
    
    public String getName() {
      return name;
    }
    
    private final String name;
  }
  
  static enum ENGINE_INPUT{
    AUTO_INHIBIT("Inhibit");
    ENGINE_INPUT(String name){
      this.name = name;
    }
    
    public String getName() {
      return name;
    }
    
    private final String name;
  }
  
  static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.IEC61499, "IEC-61499")
      .desp("Control Logic for supporting IEC-61499")
      .opMode(OperationMode.START_STOP)
      .maxAmount(255)
      .get();
  
  private boolean imported;
  
  protected IEC61499Logic() {
    super(TYPE);
  }

  @Override
  protected IEC61499LogicSettings createSettings() {
    return new IEC61499LogicSettings(this);
  }

  
  public boolean isImported(){
    return imported;
  }
  
  public void setImported(boolean imported) {
    this.imported = imported;
    
    boolean changable = !imported;
    
    getInputsManager().setAllowUserChange(changable);
    getPointsManager().setAllowUserChange(changable);
    getOutputsManager().setAllowUserChange(changable);
    getSettings().getConstantsManager().setAllowChange(changable);    
  }

}

