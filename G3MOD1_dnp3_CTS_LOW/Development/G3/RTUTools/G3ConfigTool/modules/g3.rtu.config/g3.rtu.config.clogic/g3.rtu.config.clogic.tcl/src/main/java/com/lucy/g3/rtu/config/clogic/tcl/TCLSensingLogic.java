
package com.lucy.g3.rtu.config.clogic.tcl;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_SENSING_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_SENSING_POINT;


public class TCLSensingLogic extends PredefinedCLogic<TCLSensingSettings>  {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.TCL_SENSING, "TCL Sensing Logic")
      .desp("Timed Connection Sensing Logic")
      .maxAmount(1)
      .get();
  
  public TCLSensingLogic() {
    super(TYPE, TCL_SENSING_INPUT.values(), TCL_SENSING_POINT.values());
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  protected TCLSensingSettings createSettings() {
    return new TCLSensingSettings(this);
  }

}

