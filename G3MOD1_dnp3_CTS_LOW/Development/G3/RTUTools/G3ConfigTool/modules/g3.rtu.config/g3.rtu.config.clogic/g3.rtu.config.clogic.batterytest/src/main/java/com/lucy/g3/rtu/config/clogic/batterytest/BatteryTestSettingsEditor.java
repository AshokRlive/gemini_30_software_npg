/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.batterytest;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;


class BatteryTestSettingsEditor extends AbstractCLogicSettingsEditor {
  private final BatteryTestSettings settings;
  public BatteryTestSettingsEditor(BatteryTestSettings settings) {
    super(settings);
    this.settings = settings;
  }

  @Override
  public void validate(ValidationResult result) {
    Number duration = (Number) pm.getBufferedValue(BatteryTestSettings.PROPERTY_DURATION_SECS);
    if(duration.intValue() < BatteryTestSettings.BATTERY_TEST_DURATION_MIN 
        || duration.intValue() > BatteryTestSettings.BATTERY_TEST_DURATION_MAX) 
    {
      result.addError(String.format("Duration:%d is out of range: [%d,%d]",
          duration.intValue(),
          BatteryTestSettings.BATTERY_TEST_DURATION_MIN,
          BatteryTestSettings.BATTERY_TEST_DURATION_MAX
          ));
    }
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    ValueModel vm = pm.getBufferedModel(BatteryTestSettings.PROPERTY_DURATION_SECS);
    String unit = "seconds";

    SpinnerNumberModel spinnerModel;

    // Convert Unit to minutes for Battery Test.
    boolean isBatTest = settings.getOwnerLogic().isConfiguredAsBatteryTest();
    if (isBatTest) {
      vm = new ConverterValueModel(vm, new SecsToMinsConverter());
      unit = "minutes";
      spinnerModel = SpinnerAdapterFactory.createNumberAdapter(
          vm,
          10, // default
          BatteryTestSettings.BATTERY_TEST_DURATION_MIN, // min
          BatteryTestSettings.BATTERY_TEST_DURATION_MAX, // max
          1);// step
    } else {
      spinnerModel = SpinnerAdapterFactory.createNumberAdapter(vm, 10, 0, Integer.MAX_VALUE, 1);
    }

    JSpinner durationSpinner = new JSpinner(spinnerModel);
    SpinnerWheelSupport.installMouseWheelSupport(durationSpinner);
    builder.append("Maximum Duration:", durationSpinner, new JLabel(unit));
    return builder.getPanel();
  }
  private static class SecsToMinsConverter implements BindingConverter<Integer, Integer> {

    @Override
    public Integer sourceValue(Integer mins) {
      return mins * 60;
    }

    @Override
    public Integer targetValue(Integer secs) {
      return secs / 60;
    }
  }
}

