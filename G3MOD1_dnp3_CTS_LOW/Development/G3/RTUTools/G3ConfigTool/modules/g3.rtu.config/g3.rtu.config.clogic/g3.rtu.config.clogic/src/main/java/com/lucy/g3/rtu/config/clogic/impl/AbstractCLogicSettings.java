/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import org.apache.log4j.Logger;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;


/**
 *
 */
public abstract class AbstractCLogicSettings<CLogicT extends ICLogic> extends Model implements ICLogicSettings{
  
  private final CLogicT owner;
  
  protected Logger log = Logger.getLogger(getClass());
  
  public AbstractCLogicSettings (CLogicT owner) {
    this.owner = owner;
  }
  
  @Override
  public final CLogicT getOwnerLogic() {
    return owner;
  }

  @Override
  public void copyFrom(ICLogicSettings from) {
    throw new UnsupportedOperationException("Not implemented!");
  }
}

