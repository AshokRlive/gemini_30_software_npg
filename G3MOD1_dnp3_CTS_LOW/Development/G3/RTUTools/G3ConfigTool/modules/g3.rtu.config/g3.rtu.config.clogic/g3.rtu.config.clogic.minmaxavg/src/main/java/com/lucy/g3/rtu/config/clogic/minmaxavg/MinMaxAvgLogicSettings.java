/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.minmaxavg;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.MinMaxAvgParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.clogic.minmaxavg.MinMaxAvgLogic.ClockTime;
import com.lucy.g3.rtu.config.clogic.minmaxavg.MinMaxAvgLogic.EventMode;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;


public class MinMaxAvgLogicSettings extends AbstractCLogicSettings<MinMaxAvgLogic> {
  public static final String PROPERTY_EVENT_MODE = "eventMode";
  public static final String PROPERTY_PERIOD = "period";
  public static final String PROPERTY_CLOCKTIME = "clocktime";
  public static final String PROPERTY_START_DELAY = "startDelay";
  public static final String PROPERTY_COUNTER_SCALINGFACTOR = "counterScalingFactor";

  /**
   * The name of read-only property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_COUNTER_ENABLED = "counterEnabled";

  private long period = 30;// mins
  private ClockTime clocktime = ClockTime.ONE_MIN;// mins
  private EventMode eventMode = EventMode.BY_CLOCKTIME;
  private int startDelay = 5;// seconds
  private boolean counterEnabled;
  private double counterScalingFactor = 1.0D;
  
  
  MinMaxAvgLogicSettings(MinMaxAvgLogic owner) {
    super(owner);
  }

  public long getPeriod() {
    return period;
  }

  public void setPeriod(long period) {
    Object oldValue = this.period;
    this.period = period;
    firePropertyChange(PROPERTY_PERIOD, oldValue, period);
  }

  public int getStartDelay() {
    return startDelay;
  }

  public void setStartDelay(int startDelay) {
    Object oldValue = this.startDelay;
    this.startDelay = startDelay;
    firePropertyChange(PROPERTY_START_DELAY, oldValue, startDelay);
  }

  
  public void setCounterEnabled(boolean counterEnabled) {
    Object oldValue = this.counterEnabled;
    this.counterEnabled = counterEnabled;
    firePropertyChange(PROPERTY_COUNTER_ENABLED, oldValue, counterEnabled);
    getOwnerLogic().init(counterEnabled);
  }


  public boolean isCounterEnabled() {
    return counterEnabled;
  }

  public double getCounterScalingFactor() {
    return counterScalingFactor;
  }

  public void setCounterScalingFactor(double counterScalingFactor) throws PropertyVetoException {
    Object oldValue = this.counterScalingFactor;

    if (counterScalingFactor == 0) {
      throw new PropertyVetoException("Invalid scaling factor: 0",
          new PropertyChangeEvent(this, PROPERTY_COUNTER_SCALINGFACTOR, oldValue, counterScalingFactor));
    }

    this.counterScalingFactor = counterScalingFactor;
    firePropertyChange(PROPERTY_COUNTER_SCALINGFACTOR, oldValue, counterScalingFactor);
  }

  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    MinMaxAvgParamT xml = xml_param.minMaxAvgParam.first();
    
    setCounterEnabled(xml.enableCounter.getValue());

    try {
      setCounterScalingFactor(xml.counterScalingFactor.getValue());
    } catch (PropertyVetoException e) {
      log.error(this + "=>" + e.getMessage());
    }
    setPeriod(xml.period.getValue());
    setStartDelay(xml.startDelay.getValue());

    if (xml.clocktime.exists()) {
      setClocktime(ClockTime.forValue(xml.clocktime.getValue()));
      setEventMode(EventMode.BY_CLOCKTIME);
    } else {
      setEventMode(EventMode.BY_INTERVAL);
    }
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    MinMaxAvgParamT xml_mma = xml.minMaxAvgParam.append();
    xml_mma.counterScalingFactor.setValue(getCounterScalingFactor());
    xml_mma.enableCounter.setValue(isCounterEnabled());
    xml_mma.period.setValue(getPeriod());
    xml_mma.startDelay.setValue(getStartDelay());

    if (getEventMode() == EventMode.BY_CLOCKTIME) {
      xml_mma.clocktime.setValue(getClocktime().value);
    }
  }

  public ClockTime getClocktime() {
    return clocktime;
  }

  public void setClocktime(ClockTime clocktime) {
    Object oldValue = this.clocktime;
    this.clocktime = clocktime;
    firePropertyChange(PROPERTY_CLOCKTIME, oldValue, clocktime);
  }

  public EventMode getEventMode() {
    return eventMode;
  }

  public void setEventMode(EventMode eventMode) {
    Object oldValue = this.eventMode;
    this.eventMode = eventMode;
    firePropertyChange(PROPERTY_EVENT_MODE, oldValue, eventMode);
  }


  @Override
  public IValidator getValidator() {
    return null;
  }

}

