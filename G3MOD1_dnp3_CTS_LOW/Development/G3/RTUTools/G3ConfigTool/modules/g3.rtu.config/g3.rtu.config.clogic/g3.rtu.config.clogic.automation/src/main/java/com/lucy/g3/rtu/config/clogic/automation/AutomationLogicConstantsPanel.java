/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SortOrder;
import javax.swing.table.TableCellRenderer;

import org.jdesktop.swingx.JXTable;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutomationConstantType;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.clogic.automation.AutomationConstant.IllegalConstantException;

/**
 * A panel for configuring the constants of Automation Logic.
 */
public class AutomationLogicConstantsPanel extends JPanel {

  private final AutomationLogicConstantManager manager;
  
  private final SelectionInList<AutomationConstant> selections;
  
  /**
   * Required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private AutomationLogicConstantsPanel() {
    this.manager = null;
    this.selections = null;
    initComponents();
  }
  
  /**
   * Instantiates a new automation logic constants panel.
   *
   * @param manager
   *          the manager
   */
  public AutomationLogicConstantsPanel(AutomationLogicConstantManager manager) {
    this.manager = manager;
    if (manager == null)
      this.selections = new SelectionInList<>();
    else
      this.selections = new SelectionInList<>(manager.getItemListModel());
    
    initComponents();
    initComponentsBinding();
    initEventHanding();
    setActionEnabled(manager == null ? false: manager.isAllowChange());
    
    // Bind delete"
    UIUtils.registerKeyAction(tableConstants, KeyEvent.VK_DELETE, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnAddActionPerformed();
      }
    });

    // Set table sorting
    tableConstants.setSortOrderCycle(SortOrder.ASCENDING, SortOrder.DESCENDING, SortOrder.UNSORTED);
    tableConstants.setColumnControlVisible(true);
    //UIUtil.packTableColumns(tableConstants);
    
  }
  
  private void initEventHanding() {
    if(manager != null) {
    manager.addPropertyChangeListener(AutomationLogicConstantManager.PROPERTY_ALLOW_CHANGE, new PropertyChangeListener() {
      
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setActionEnabled((Boolean)evt.getNewValue());
      }
    });
    }
    
  }

  private void setActionEnabled(Boolean enabled) {
    btnAdd.setEnabled(enabled);
    btnRemove.setEnabled(enabled);
    menuItemAdd.setEnabled(enabled);
    menuItemRemove.setEnabled(enabled);
  }
  
  public boolean shouldBeDisplayed() {
    return manager != null;
  }
  
  private void initComponentsBinding() {
    if (manager == null)
      return;
    
    tableConstants.setModel(new AutomationConstantTableModel(manager.getItemListModel(),manager));
    Bindings.bind(tableConstants, selections);
  }

  private void btnAddActionPerformed() {
//    Object selectType = JOptionPane.showInputDialog(this, "Please select a type for the new constant:",
//        "Select Constant Type", 
//        JOptionPane.QUESTION_MESSAGE, null, 
//        AutomationConstantType.values(), AutomationConstantType.String);
    
    Object input = JOptionPane.showInputDialog(this, "Please give a name for the new constant:",
        "New Constant", 
        JOptionPane.QUESTION_MESSAGE);
    
    if(input != null) {
      AutomationConstant constant = new AutomationConstant((String)input,AutomationConstantType.String);
      constant.setValue("0");
      manager.add(constant);
    }
  }

  private void btnRemoveActionPerformed() {
    manager.remove(selections.getSelection());
  }

  private void createUIComponents() {
    tableConstants = new JXTable() {
      /* Set render component disabled/enabled state */
      @Override
      public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component c = super.prepareRenderer(renderer, row, column);

        // Set editable cell different colour
        if (isCellEditable(row, column) == true) {
          if (isRowSelected(row)) {
            c.setForeground(Color.yellow);
          } else {
            c.setForeground(Color.blue);
          }
        }
        
        return c;
      }
    };
  }

  private void menuItemAddActionPerformed(ActionEvent e) {
    btnAddActionPerformed();
  }

  private void menuItemRemoveActionPerformed(ActionEvent e) {
    btnRemoveActionPerformed();
  }
  
  private static class AutomationConstantTableModel extends AbstractTableAdapter<AutomationConstant> {

    private static final String[] COLUMN_NAMES = {
        "Index",
        "Name",
        "Type",
        "Value",
        "Unit"
    };

    private final static int COLUMN_INDEX = 0;
    private final static int COLUMN_NAME = 1;
    private final static int COLUMN_TYPE = 2;
    private final static int COLUMN_VALUE = 3;
    private final static int COLUMN_UNIT = 4;
    
    private final AutomationLogicConstantManager manager;

    public AutomationConstantTableModel(ListModel<AutomationConstant> listModel,  AutomationLogicConstantManager manager) {
      super(listModel, COLUMN_NAMES);
      this.manager = manager;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      AutomationConstant constant = getRow(rowIndex);
      switch(columnIndex) {
      case COLUMN_INDEX:
        return rowIndex;
      case COLUMN_NAME:
        return constant.getName();
      case COLUMN_VALUE:
        return constant.getValue();
      case COLUMN_UNIT:
        return constant.getUnit();
      case COLUMN_TYPE:
        return constant.getValueType();
      default:
        return null;
      }
    }


    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      switch (columnIndex) {
      case COLUMN_TYPE:
      case COLUMN_INDEX:
      case COLUMN_UNIT:
        return false;
      case COLUMN_VALUE:
        return true;

      default:
        return manager.isAllowChange();
      }
    }


    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      AutomationConstant constant = getRow(rowIndex);
      switch(columnIndex) {
      case COLUMN_NAME:
        constant.setName(aValue.toString());
        break;
      case COLUMN_VALUE:
        try{
          constant.setValue(aValue.toString());
        }catch (IllegalConstantException e) {
          MessageDialogs.error("Invalid input value:"+aValue, e);
        }
        break;
      case COLUMN_UNIT:
        constant.setUnit(aValue == null ? null : aValue.toString());
        break;
      default:
        break;
      }
    }
    
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    btnAdd = new JButton();
    btnRemove = new JButton();
    popupMenu1 = new JPopupMenu();
    menuItemAdd = new JMenuItem();
    menuItemRemove = new JMenuItem();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {

      //---- tableConstants ----
      tableConstants.setComponentPopupMenu(popupMenu1);
      scrollPane1.setViewportView(tableConstants);
    }
    add(scrollPane1, BorderLayout.CENTER);

    //======== panel1 ========
    {
      panel1.setBorder(Borders.DIALOG_BORDER);
      panel1.setLayout(new FormLayout(
          "[50dlu,default]",
          "2*(default, $lgap), default"));

      //---- btnAdd ----
      btnAdd.setText("Add Constant");
      btnAdd.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnAddActionPerformed();
        }
      });
      panel1.add(btnAdd, CC.xy(1, 1));

      //---- btnRemove ----
      btnRemove.setText("Remove Constant");
      btnRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnRemoveActionPerformed();
        }
      });
      panel1.add(btnRemove, CC.xy(1, 3));
    }
    add(panel1, BorderLayout.EAST);

    //======== popupMenu1 ========
    {

      //---- menuItemAdd ----
      menuItemAdd.setText("Add Constant");
      menuItemAdd.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemAddActionPerformed(e);
        }
      });
      popupMenu1.add(menuItemAdd);

      //---- menuItemRemove ----
      menuItemRemove.setText("Remove Constant");
      menuItemRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemRemoveActionPerformed(e);
        }
      });
      popupMenu1.add(menuItemRemove);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JXTable tableConstants;
  private JPanel panel1;
  private JButton btnAdd;
  private JButton btnRemove;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItemAdd;
  private JMenuItem menuItemRemove;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
