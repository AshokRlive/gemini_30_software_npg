
package com.lucy.g3.rtu.config.clogic.externalfpi;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class ExternalFPILogicFactory extends AbstractClogicFactory {
  public final static ExternalFPILogicFactory INSTANCE = new ExternalFPILogicFactory();
  private ExternalFPILogicFactory(){}
  

  @Override
  public ICLogic createLogic() {
    return new ExternalFPI();
  }

  @Override
  public ICLogicType getLogicType() {
    return ExternalFPI.TYPE;
  }

}

