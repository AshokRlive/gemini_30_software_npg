/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.offlocalremote;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Off-Local-Remote Control Logic,based on generated Enum from
 * control logic XML.
 */
public final class OffLocalRemote extends PredefinedCLogic<ICLogicSettings> {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.OLR, "OffLocalRemote")
      .desp("The control logic for controlling the Off/Local/Remote state of the RTU.")
      .maxAmount(1)
      .allowUserRemove(false)
      .get();
  
  public OffLocalRemote() {
    super(TYPE, null, OLR_POINT.values());
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return new CLogicIO[0];
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return new CLogicIO[0];
  }
  
  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }


  @Override
  protected ICLogicSettings createSettings() {
    return null;
  }

  private PseudoDoubleBinaryPoint olrStatus;
  public PseudoDoubleBinaryPoint getOLRStatus(){
    if(olrStatus == null)
      olrStatus = CLogicUtility.findDoubleBinaryPoint(getAllPoints(), OLR_POINT.OLR_POINT_OFFLOCALREMOTE);
    return olrStatus;
  }
}
