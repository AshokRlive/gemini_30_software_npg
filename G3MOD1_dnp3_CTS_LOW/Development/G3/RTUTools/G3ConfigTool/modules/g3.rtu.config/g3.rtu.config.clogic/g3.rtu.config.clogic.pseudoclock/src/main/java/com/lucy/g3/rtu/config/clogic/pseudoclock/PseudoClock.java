/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.pseudoclock;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PCLK_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PCLK_POINT;

/**
 * Implementation of PseudoClock Logic.
 */
public class PseudoClock extends PredefinedCLogic<PseudoClockSettings> implements ISupportOffLocalRemote, IOperableLogic {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.PCLK, "Pseudo Clock")
      .desp("Pseudo Digital Alternating Clock")
      .opMode(OperationMode.START_STOP)
      .requireOLR(true)
      .get();
  
  PseudoClock() {
    super(TYPE, PCLK_INPUT.values(), PCLK_POINT.values());
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(PCLK_INPUT.PCLK_INPUT_OLR).setValue(offLocalRemotePoint);
  }


  @Override
  protected PseudoClockSettings createSettings() {
    return new PseudoClockSettings(this);
  }
  
  @Override
  public boolean isSwitchLogic() {
    return false;
  }
}
