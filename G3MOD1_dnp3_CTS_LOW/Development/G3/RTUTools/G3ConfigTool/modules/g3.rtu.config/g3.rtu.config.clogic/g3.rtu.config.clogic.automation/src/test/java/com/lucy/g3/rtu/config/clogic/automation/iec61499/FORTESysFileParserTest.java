
package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.util.IteratorIterable;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.automation.iec61499.FORTESysFileParser.G3FBConfig;


public class FORTESysFileParserTest {
  private FORTESysFileParser fixture;
  
  @Before
  public void setUp() throws Exception {
    fixture = new FORTESysFileParser();
  }
  
  @Test //@Ignore// Manual test
  public void testImportSysfile() throws IOException, JDOMException {
    fixture.openFile(new File("src/test/resources/G3Automation.sys"));
    IteratorIterable<Element> resources = fixture.getAllResources();
    for(Element res: resources) {
      fixture.parseResource(res);
      String resName = res.getAttributeValue("Name");
      
      print(resName, "INPUTS", fixture.getG3inputs());
      print(resName, "OUTPUTS", fixture.getG3outputs());
      print(resName, "PARAMETERS", fixture.getG3parameters());
      print(resName, "CONTROLS", fixture.getG3controls());
    }
  }

  private static void print(String resName, String confName, ArrayList<G3FBConfig> confs) {
    if(confs.isEmpty())
      return;
    System.out.println(String.format("\n=== [%s] %s==== ", resName,confName));
    for (G3FBConfig conf: confs) {
      System.out.println(conf);
    }
  }
  

//  @Test
//  public void testImportForteBootFile() throws IOException {
//    String fboot = fixture.importForteBootFile(new File("src/test/resources/forte.fboot"));
//    //System.out.println(fboot);
//  }

}

