/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.sectionaliser;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRadioButton;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;


public class SectionaliserSettingsEditor extends AbstractCLogicSettingsEditor{
  private final static String LABEL_LONG_RECLAIMTIME       ="Long Reclaim Time"      ;
  private final static String LABEL_SHORT_RECLAIMTIME      ="Short Reclaim Time"     ;
  private final static String LABEL_FAULTCOUNTER_THRESHOLD ="Fault Counter Threshold";
  
  
  
  SectionaliserSettingsEditor(SectionaliserSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result, pm, SectionaliserSettings.PROPERTY_LONG_RECLAIMTIME       ,LABEL_LONG_RECLAIMTIME         );
    checkNotNegative(result, pm, SectionaliserSettings.PROPERTY_SHORT_RECLAIMTIME      ,LABEL_SHORT_RECLAIMTIME        );
    checkNotNegative(result, pm, SectionaliserSettings.PROPERTY_FAULTCOUNTER_THRESHOLD ,LABEL_FAULTCOUNTER_THRESHOLD   );

  }

  @Override
  public JComponent getComponent(JDialog parent) {

     DefaultFormBuilder builder = getDefaultBuilder();

     ValueModel inputTypeVM = pm.getBufferedModel(SectionaliserSettings.PROPERTY_INPUT_TYPE);
     JRadioButton rb_inputLV = new JRadioButton("Low");
     JRadioButton rb_inputMV = new JRadioButton("Medium");
     Bindings.bind(rb_inputLV, inputTypeVM, SectionaliserSettings.INPUT_TYPE_LV);
     Bindings.bind(rb_inputMV, inputTypeVM, SectionaliserSettings.INPUT_TYPE_MV);
     builder.append("Voltage Sensor:", rb_inputLV);
     builder.nextLine();
     builder.append("", rb_inputMV);
     builder.nextLine();

     createLongField(LABEL_LONG_RECLAIMTIME      , SectionaliserSettings.PROPERTY_LONG_RECLAIMTIME,       Units.SECS);
     createLongField(LABEL_SHORT_RECLAIMTIME     , SectionaliserSettings.PROPERTY_SHORT_RECLAIMTIME,      Units.SECS);
     createLongField(LABEL_FAULTCOUNTER_THRESHOLD, SectionaliserSettings.PROPERTY_FAULTCOUNTER_THRESHOLD, Units.NONE);

     JCheckBox comp0 = BasicComponentFactory.createCheckBox(
         pm.getBufferedModel(SectionaliserSettings.PROPERTY_DEFAULT_ENABLED),
         "Default Enabled");
     builder.append("", comp0);
   
     return builder.getPanel();
  }

}

