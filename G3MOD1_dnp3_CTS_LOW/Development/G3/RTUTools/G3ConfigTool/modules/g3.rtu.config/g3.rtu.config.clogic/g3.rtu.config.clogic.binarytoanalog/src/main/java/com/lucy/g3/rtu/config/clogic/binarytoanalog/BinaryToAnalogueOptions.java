/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.binarytoanalog;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
class BinaryToAnalogueOptions extends WizardPage {
  public final static String KEY_NUM = "btaNum";
  public final static String KEY_UNIT = "btaUnit";
  
  public BinaryToAnalogueOptions() {
    super("Configuration");
    initComponents();
    
    spinnerNum.setName(KEY_NUM);
    spinnerNum.setValue(16);
    tfUnit.setName(KEY_UNIT);
    SpinnerWheelSupport.installMouseWheelSupport(spinnerNum);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    label1 = new JLabel();
    spinnerNum = new JSpinner();
    label4 = new JLabel();
    label2 = new JLabel();
    tfUnit = new JTextField();
    label3 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, 30dlu, $ugap, default:grow",
        "2*(default, $lgap), default"));

    //---- label1 ----
    label1.setText("Number of Binaries:");
    add(label1, CC.xy(1, 1));

    //---- spinnerNum ----
    spinnerNum.setModel(new SpinnerNumberModel(1, 1, 16, 1));
    add(spinnerNum, CC.xy(3, 1));

    //---- label4 ----
    label4.setText("(Bits of the analogue value)");
    label4.setForeground(Color.gray);
    add(label4, CC.xy(5, 1));

    //---- label2 ----
    label2.setText("Analogue Value Unit:");
    add(label2, CC.xy(1, 3));
    add(tfUnit, CC.xy(3, 3));

    //---- label3 ----
    label3.setText("(Optional, e.g. mV, A, etc)");
    label3.setForeground(Color.gray);
    add(label3, CC.xy(5, 3));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JSpinner spinnerNum;
  private JLabel label4;
  private JLabel label2;
  private JTextField tfUnit;
  private JLabel label3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
