
package com.lucy.g3.rtu.config.clogic.automation.iec61131;

import javax.swing.JPanel;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IClogicFactory;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogic;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicConstantsPanel;


public class IEC61131LogicFactory extends AbstractClogicFactory {
  public final static IEC61131LogicFactory INSTANCE = new IEC61131LogicFactory();
  private IEC61131LogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new IEC61131LogicSettingsEditor((IEC61131LogicSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new IEC61131Logic();
  }

  @Override
  public ICLogicType getLogicType() {
    return IEC61131Logic.TYPE;
  }

  @Override
  public JPanel[] createConfigTabs(ICLogic clogic) {
    AutomationLogic autologic = (AutomationLogic) clogic;
    AutomationLogicConstantsPanel constPanel = new AutomationLogicConstantsPanel(autologic.getSettings().getConstantsManager());
    constPanel.putClientProperty(IClogicFactory.PROPERTY_TAB_TITLE, "Constants");
    return new JPanel[]{constPanel};
  }
}

