/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.RoundRectangle2D;
import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.UIThemeResources;

/**
 * The view of control logic.
 */
class Canvas extends JPanel {

  private Logger log = Logger.getLogger(Canvas.class);

  private static final int TITLE_WIDTH = 400;

  /* Logic Block width */
  private static final int BLOCK_WIDTH = 250;

  /* The minimum vertical space between pins */
  private static final int PIN_GAP_V = 30;
  /* The length of IO Pin line */
  private static final int PINT_LEN = 200;
  /* Horizontal gap between IO components */
  static final int PIN_GAP_H = 8;

  /* The length of value label */
  private static final int VALUE_LBL_LEN = 300;

  /* The total length of pin plus label */
  private static final int PIN_TOTAL_LEN = PINT_LEN + VALUE_LBL_LEN;

  /* The length of button */
  private static final int BTN_LEN = 20;
  /* The width of button */
  private static final int BTN_HEIGHT = 20;

  private static final int INTER_ITEM_GAP = 5;
  private static final int INTERN_ITEM_HEIGHT = 20;
  private static final int INTERN_ITEM_WIDTH = BLOCK_WIDTH - 2 * INTER_ITEM_GAP;

  /* The height of title label */
  private static final int TITLE_HEIGHT = 30;

  private static final int CTL_BTN_LEN = 110;
  private static final int CTL_BTN_HEIGHT = 25;
  private static final int CTL_BTN_GAP = 10;

  // Block colours
  static final Color CANVAS_BG = UIManager.getColor("Panel.background");
  static final Color BLOCK_FG = UIThemeResources.COLOUR_LOGIC_BLOCK_BORDER_COLOR;
  static final Color BLOCK_BG = UIThemeResources.COLOUR_LOGIC_BLOCK_BG_COLOR;

  // Drawing stroke
  private static final BasicStroke STROKE = new BasicStroke(2.0f, BasicStroke.CAP_BUTT,
      BasicStroke.JOIN_BEVEL);

  /* Logic Block height */
  private int blockHeight;
  private int rectX;
  private int rectY;
  private int rectLen;
  private int rectHeight;

  private int[][] leftPinCoordinates;
  private int[][] rightPinCoordinates;

  private JComponent titleCentre;
  private JComponent titleLeft;
  private JComponent titleRight;
  
  private IOCompGroup[] inputs;
  private IOCompGroup[] outputs;
  private JComponent[] interns;
  private JComponent[] controllers;

  private JPanel infoPanel;


  public Canvas() {
    super(null);
  }

  private int inputNum() {
    return inputs == null ? 0 : inputs.length;
  }

  private int outputNum() {
    return outputs == null ? 0 : outputs.length;
  }

  private int internNum() {
    return interns == null ? 0 : interns.length;
  }

  /**
   * Update the preferred canvas size and the size of control logic block.
   */
  public void updatePreferredSize() {
    int inputNum = inputNum();
    int outputNum = outputNum();
    int internNum = internNum();

    // ---- compute block size ----
    blockHeight = Math.max((inputNum > outputNum ? inputNum : outputNum) * PIN_GAP_V, 80);
    int heightOfPseudoPoints = internNum * 30 + 20;
    heightOfPseudoPoints += 30; // keep extra space for setting button.
    blockHeight = Math.max(blockHeight, heightOfPseudoPoints);

    // ---- compute panel size ----
    int partNum = 2;
    if (inputNum == 0 && outputNum == 0) {
      partNum = 0;
    }
    int panelWidth = BLOCK_WIDTH + PIN_TOTAL_LEN * partNum + 80;
    int panelHeight = blockHeight + 100;// panel is bigger than block.
    setPreferredSize(new Dimension(panelWidth, panelHeight));
    
    log.debug("Updated Canvas preferred size:" + panelWidth + "*" + panelHeight);
  }

  /**
   * Calculate the coordinates of all graphic parts.
   */
  private void calculateCoordinates() {
    int inputNum = inputNum();
    int outputNum = outputNum();

    // ====== Calculate the centre of block ======
    Dimension dim = getSize();
    int pointX;
    int pointY;
    pointX = dim.width / 2;
    pointY = dim.height / 2;
    // Horizontal centre adjust
    if (inputNum <= 0 && outputNum > 0) {
      pointX = pointX - PIN_TOTAL_LEN / 2;
    }
    // System.out.println("Calculated coordinate. BLOCK x:"+x +" block Y:"+y);

    // ====== Calculate rectangle position ======
    rectX = pointX - BLOCK_WIDTH / 2;
    rectY = pointY - blockHeight / 2;
    rectLen = BLOCK_WIDTH;
    rectHeight = blockHeight;

    // ====== Calculate the position of left pins ======
    leftPinCoordinates = new int[inputNum][4];
    {
      final int strokeX = pointX - PINT_LEN - BLOCK_WIDTH / 2;
      int strokeY = pointY - blockHeight / 2;
      final int offsetY = blockHeight / (inputNum + 1);

      if (titleLeft != null) {
        titleLeft.setBounds(strokeX, strokeY - 20, PINT_LEN, TITLE_HEIGHT);
      }

      for (int i = 0; i < inputNum; i++) {
        strokeY += offsetY;

        leftPinCoordinates[i][0] = strokeX; // X0
        leftPinCoordinates[i][1] = strokeY; // Y0
        leftPinCoordinates[i][2] = strokeX + PINT_LEN; // X1
        leftPinCoordinates[i][3] = leftPinCoordinates[i][1]; // Y1

        if (inputs != null && inputs[i] != null) {
          // input value label
          inputs[i].valueLabel.setBounds(strokeX - VALUE_LBL_LEN - PIN_GAP_H,// X
              strokeY - BTN_HEIGHT / 2,// Y
              VALUE_LBL_LEN - BTN_LEN,// W
              BTN_HEIGHT);// H
          // input button
          inputs[i].button.setBounds(strokeX - BTN_LEN,// X
              strokeY - BTN_HEIGHT / 2,// Y
              BTN_LEN, // W
              BTN_HEIGHT);// H
          // input name label
          inputs[i].nameLabel.setBounds(strokeX + PIN_GAP_H,// X
              strokeY - BTN_HEIGHT,// Y
              PINT_LEN - 2 * PIN_GAP_H,// W
              BTN_HEIGHT);// H
        }
      }
    }

    // ====== Calculate the position of right pins ======
    rightPinCoordinates = new int[outputNum][4];
    {
      final int strokeX = pointX + BLOCK_WIDTH / 2;
      int strokeY = pointY - blockHeight / 2;
      final int offsetY = blockHeight / (outputNum + 1);

      if (titleRight != null) {
        titleRight.setBounds(strokeX, strokeY - 20, PINT_LEN, TITLE_HEIGHT);
      }

      for (int i = 0; i < outputNum; i++) {
        strokeY += offsetY;
        rightPinCoordinates[i][0] = strokeX; // X0
        rightPinCoordinates[i][1] = strokeY; // Y0
        rightPinCoordinates[i][2] = strokeX + PINT_LEN; // X1
        rightPinCoordinates[i][3] = rightPinCoordinates[i][1]; // Y1

        if (outputs != null && outputs[i] != null) {
          // output button
          outputs[i].button.setBounds(strokeX + PINT_LEN, // X
              strokeY - BTN_HEIGHT / 2,// Y
              BTN_LEN, // W
              BTN_HEIGHT);// H

          // output value label
          outputs[i].valueLabel.setBounds(strokeX + PINT_LEN + BTN_LEN + PIN_GAP_H, // X
              strokeY - BTN_HEIGHT / 2, // Y
              VALUE_LBL_LEN - BTN_LEN,// W
              BTN_HEIGHT);// H

          // output name label
          outputs[i].nameLabel.setBounds(strokeX + PIN_GAP_H,// X
              strokeY - BTN_HEIGHT,// Y
              PINT_LEN - 2 * PIN_GAP_H,// W
              BTN_HEIGHT);// H
        }
      }

    }

    // ====== Calculate centre title bounds ======
    int indent = 40;
    int titleX = pointX - TITLE_WIDTH / 2;
    int titleY = pointY - blockHeight / 2 - indent;
    if (titleCentre != null) {
      titleCentre.setBounds(titleX, titleY, TITLE_WIDTH, TITLE_HEIGHT);
    }

    // ====== Calculate internal points bounds ======
    if (interns != null && interns.length > 0) {
      final int internX = pointX - BLOCK_WIDTH / 2 + INTER_ITEM_GAP;
      int internY = pointY + blockHeight / 2 - INTER_ITEM_GAP - INTERN_ITEM_HEIGHT;

      // Set points positions
      for (int i = interns.length - 1; i >= 1; i--) {
        interns[i].setBounds(internX, internY, INTERN_ITEM_WIDTH, INTERN_ITEM_HEIGHT);
        internY = internY - INTERN_ITEM_HEIGHT - INTER_ITEM_GAP;
      }

      // Set "settings" button to up right corner
      interns[0].setBounds(internX,
          pointY - blockHeight / 2 + INTER_ITEM_GAP,
          INTERN_ITEM_WIDTH,
          INTERN_ITEM_HEIGHT + 10);
    }

    // ====== Calculate controller button bounds ======
    if (controllers != null) {
      int ctlX = pointX - controllers.length * (CTL_BTN_LEN + CTL_BTN_GAP) / 2;
      int ctlY = pointY + blockHeight / 2 + 40;
      for (int i = 0; i < controllers.length; i++) {
        controllers[i].setBounds(ctlX + i * (CTL_BTN_LEN + CTL_BTN_GAP),
            ctlY, CTL_BTN_LEN, CTL_BTN_HEIGHT);
      }
    }

  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    Graphics2D g2 = (Graphics2D) g;

    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    // ====== Draw Block ======
    g2.setPaint(BLOCK_BG);
    g2.fillRoundRect(rectX, rectY, rectLen, rectHeight, 10, 10);
    g2.setPaint(BLOCK_FG);
    g2.setStroke(STROKE);
    g2.draw(new RoundRectangle2D.Double(rectX, rectY, rectLen, rectHeight, 10, 10));

    // ====== Draw Left Pins ======

    for (int i = 0; i < leftPinCoordinates.length; i++) {
      g2.draw(new Line2D.Double(
          leftPinCoordinates[i][0],
          leftPinCoordinates[i][1],
          leftPinCoordinates[i][2],
          leftPinCoordinates[i][3]));
    }

    // ====== Draw Right Pins ======
    for (int i = 0; i < rightPinCoordinates.length; i++) {
      g2.draw(new Line2D.Double(
          rightPinCoordinates[i][0],
          rightPinCoordinates[i][1],
          rightPinCoordinates[i][2],
          rightPinCoordinates[i][3]));
    }
  }

  @Override
  public void repaint() {
    calculateCoordinates();
    super.repaint();
  }

  public void setInfoPanel(JPanel infoPanel) {
    if (this.infoPanel != null) {
      removeComps(infoPanel);
    }

    this.infoPanel = infoPanel;

    if (this.infoPanel != null) {
      addComps(infoPanel);
      infoPanel.setBounds(20, 20, 300, 200);
    }
  }

  public void setInputComps(IOCompGroup[] inputs) {
    Preconditions.checkNotNull(inputs, "inputs is null");

    // Remove old comps
    if (this.inputs != null) {
      for (int i = 0; i < this.inputs.length; i++) {
        removeComps(this.inputs[i].button, this.inputs[i].nameLabel, this.inputs[i].valueLabel);
      }
    }

    this.inputs = inputs;
    for (int i = 0; i < inputs.length; i++) {
      addComps(inputs[i].button, inputs[i].nameLabel, inputs[i].valueLabel);
    }
  }

  public void setOutputComps(IOCompGroup[] outputs) {
    Preconditions.checkNotNull(outputs, "outputs is null");

    // Remove old comps
    if (this.outputs != null) {
      for (int i = 0; i < this.outputs.length; i++) {
        removeComps(this.outputs[i].button, this.outputs[i].nameLabel, this.outputs[i].valueLabel);
      }
    }

    this.outputs = outputs;
    for (int i = 0; i < outputs.length; i++) {
      addComps(outputs[i].button, outputs[i].nameLabel, outputs[i].valueLabel);
    }
  }

  public void setInternComps(JComponent... interns) {
    Preconditions.checkNotNull(interns, "interns is null");
    // Remove old comps
    removeComps(this.interns);

    log.debug("Set internal components. size:" + interns.length);
    this.interns = interns;
    addComps(interns);
  }

  public void setTitleComps(JComponent left, JComponent centre, JComponent right) {
    removeComps(this.titleLeft, this.titleCentre, this.titleRight);

    titleLeft = left;
    titleCentre = centre;
    titleRight = right;
    log.debug("Set title components.");
    addComps(titleLeft, titleCentre, titleRight);
  }

  // TODO UNUSED
  public void setControllerComps(JComponent... comps) {
    removeComps(this.controllers);

    log.debug("Set controller components. size:" + comps.length);
    controllers = comps;
    addComps(comps);
  }

  private void addComps(JComponent... comps) {
    if (comps != null) {
      for (int i = 0; i < comps.length; i++) {
        if (comps[i] != null) {
          add(comps[i]);
        }
      }
    }
  }

  private void removeComps(JComponent... comps) {

    if (comps != null) {
      log.debug("Remove components: " + Arrays.toString(comps));
      for (int i = 0; i < comps.length; i++) {
        if (comps[i] != null) {
          remove(comps[i]);
        }
      }
    }
  }


  /**
   * IO Component Group.
   */
  public static class IOCompGroup {

    protected JComponent button;
    protected JComponent nameLabel;
    protected JComponent valueLabel;
  }

}
