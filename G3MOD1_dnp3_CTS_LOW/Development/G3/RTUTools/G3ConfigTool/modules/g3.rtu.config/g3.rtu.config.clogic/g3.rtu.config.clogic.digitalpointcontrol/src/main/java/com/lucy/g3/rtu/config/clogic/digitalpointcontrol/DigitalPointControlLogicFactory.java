
package com.lucy.g3.rtu.config.clogic.digitalpointcontrol;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;


public class DigitalPointControlLogicFactory extends AbstractClogicFactory {
  public final static DigitalPointControlLogicFactory INSTANCE = new DigitalPointControlLogicFactory();
  private DigitalPointControlLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new DigitalPointControlSettingsEditor((DigitalPointControlSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new DigitalPointControl();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return DigitalPointControl.TYPE;
  }
}

