/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.psupply;

import com.g3schema.ns_clogic.CLogicParametersBaseT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.DigitalControlParamT;
import com.g3schema.ns_common.ParameterT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DCTL_LOGIC_TRIGGER;


public class PowerSupplyControlSettings extends AbstractCLogicSettings<PowerSupplyControl> {
  public static final String PROPERTY_DURATION = "duration";

  private int duration;
  
  PowerSupplyControlSettings(PowerSupplyControl owner) {
    super(owner);
  }

  @Override
  public IValidator getValidator() {
    return null;
  }
  

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    Object oldValue = this.duration;
    this.duration = duration;
    firePropertyChange(PROPERTY_DURATION, oldValue, duration);
  }

  
  @Override
  public void readParamFromXML(CLogicParametersT _xml) {
     if(_xml.generic.exists()) {
       CLogicParametersBaseT xml= _xml.generic.first();
       int size = xml.parameter.count();
       for (int i = 0; i < size ; i++) {
         ParameterT xmlParam = xml.parameter.at(i);
         if(PROPERTY_DURATION.equals(xmlParam.parameterName.getValue())) {
           setDuration(Integer.valueOf(xmlParam.parameterValue.getValue()));
           break;
         }
       }
     } else {
       log.warn("Parameters not found: "+ getOwnerLogic());
     }

  }

  @Override
  public void writeParamToXML(CLogicParametersT _xml) {
    CLogicParametersBaseT xml= _xml.generic.append();
    ParameterT xmlParam = xml.parameter.append();
    xmlParam.parameterName.setValue(PROPERTY_DURATION);
    xmlParam.parameterValue.setValue(Integer.toString(getDuration()));
  }

}

