/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;


/**
 * The Class DefaultCLogicSettings.
 */
public class DefaultCLogicSettings extends AbstractCLogicSettings<ICLogic>{
  
  public DefaultCLogicSettings(ICLogic owner) {
    super(owner);
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml) {
    // Do nothing
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    // Do nothing
  }

  @Override
  public IValidator getValidator() {
    return null;
  }
}

