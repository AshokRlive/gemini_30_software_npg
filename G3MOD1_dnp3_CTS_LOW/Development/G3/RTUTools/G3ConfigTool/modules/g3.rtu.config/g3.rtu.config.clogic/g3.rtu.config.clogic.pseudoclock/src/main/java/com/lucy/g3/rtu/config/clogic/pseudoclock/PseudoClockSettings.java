/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.pseudoclock;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.PseudoClockT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.FreezeClockTime;


/**
 * The Class PseudoClockSettings.
 */
public class PseudoClockSettings extends AbstractCLogicSettings<PseudoClock>{
  public final static String PROPERTY_AUTOSTART_ENABLED = "autoStartEnabled";
  public final static String PROPERTY_OUTPUTPULSE_WIDTH_MS = "outputPulseWidthMs";
  public final static String PROPERTY_PERIODIC_INTERVAL_SECS = "periodicIntervalSecs";
  public final static String PROPERTY_OCLOCKTIME = "oClockTime";
  public final static String PROPERTY_OUTPUT_MODE = "outputMode";
  public final static String PROPERTY_TIMING_MODE = "timingMode";
  
  public final static int MIN_PULSE_MS = 100;
  public final static int MIN_INTERVAL_SEC= 1;
  
  private boolean autoStartEnabled =  true;
  private long outputPulseWidthMs = MIN_PULSE_MS;
  private long periodicIntervalSecs = MIN_INTERVAL_SEC;
  private FreezeClockTime oClockTime = FreezeClockTime.ONE_MIN;
  private OutputMode outputMode = OutputMode.TOGGLING;
  private TimingMode timingMode = TimingMode.OCLOCK;
  
  PseudoClockSettings(PseudoClock owner){
    super(owner);
  }
  
  public boolean isAutoStartEnabled() {
    return autoStartEnabled;
  }
  
  public void setAutoStartEnabled(boolean autoStartEnabled) {
    Object oldValue = this.autoStartEnabled;
    this.autoStartEnabled = autoStartEnabled;
    firePropertyChange(PROPERTY_AUTOSTART_ENABLED, oldValue, autoStartEnabled);
  }

  
  public long getPeriodicIntervalSecs() {
    return periodicIntervalSecs;
  }

  
  public void setPeriodicIntervalSecs(long periodicIntervalSecs) {
    if(periodicIntervalSecs >= 0) {
      Object oldValue = this.periodicIntervalSecs;
      this.periodicIntervalSecs = periodicIntervalSecs;
      firePropertyChange(PROPERTY_PERIODIC_INTERVAL_SECS, oldValue, periodicIntervalSecs);
    }
  }

  
  public long getOutputPulseWidthMs() {
    return outputPulseWidthMs;
  }

  
  public void setOutputPulseWidthMs(long outputPulseWidthMs) {
    Object oldValue = this.outputPulseWidthMs;
    this.outputPulseWidthMs = outputPulseWidthMs;
    firePropertyChange(PROPERTY_OUTPUTPULSE_WIDTH_MS, oldValue, outputPulseWidthMs);
  }

  
  public FreezeClockTime getoClockTime() {
    return oClockTime;
  }

  
  public void setoClockTime(FreezeClockTime oClockTime) {
    if(oClockTime != null) {
      Object oldValue = this.oClockTime;
      this.oClockTime = oClockTime;
      firePropertyChange(PROPERTY_OCLOCKTIME, oldValue, oClockTime);
    }
  }

  public OutputMode getOutputMode() {
    return outputMode;
  }

  
  public void setOutputMode(OutputMode outputMode) {
    Object oldValue = this.outputMode;
    this.outputMode = outputMode;
    firePropertyChange(PROPERTY_OUTPUT_MODE, oldValue, outputMode);
  }

  
  public TimingMode getTimingMode() {
    return timingMode;
  }

  
  public void setTimingMode(TimingMode timingMode) {
    if(timingMode != null) {
      Object oldValue = this.timingMode;
      this.timingMode = timingMode;
      firePropertyChange(PROPERTY_TIMING_MODE, oldValue, timingMode);
    }
  }

  @Override
  public IValidator getValidator() {
    return null;
  }

  @Override
  public void readParamFromXML(CLogicParametersT xml) {
    if(xml.pseudoClock.exists()) {
      PseudoClockT xmlpc = xml.pseudoClock.first();
      setAutoStartEnabled(xmlpc.autoStartEnabled.getValue());
      setOutputMode(xmlpc.outputToggleEnabled.getValue() ? OutputMode.TOGGLING : OutputMode.PULSE);
      setTimingMode(xmlpc.periodicIntervalEnabled.getValue() ? TimingMode.PERIODIC : TimingMode.OCLOCK);
      setoClockTime(FreezeClockTime.forValue(xmlpc.fromOClockMins.getValue()));
      setOutputPulseWidthMs(xmlpc.outputPulseWidth.getValue());
      setPeriodicIntervalSecs(xmlpc.periodicIntervalMs.getValue()/1000);
    }
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    PseudoClockT xmlpc = xml.pseudoClock.append();
    xmlpc.autoStartEnabled.setValue(isAutoStartEnabled());
    xmlpc.outputToggleEnabled.setValue(getOutputMode() == OutputMode.TOGGLING);
    xmlpc.outputPulseWidth.setValue(getOutputPulseWidthMs());
    xmlpc.periodicIntervalEnabled.setValue(getTimingMode() == TimingMode.PERIODIC);
    xmlpc.periodicIntervalMs.setValue(getPeriodicIntervalSecs()*1000);
    xmlpc.fromOClockMins.setValue(getoClockTime().getValue());
  }

  public static enum OutputMode {
    TOGGLING,
    PULSE
  }
  
  public static enum TimingMode {
    PERIODIC,
    OCLOCK
  }

}

