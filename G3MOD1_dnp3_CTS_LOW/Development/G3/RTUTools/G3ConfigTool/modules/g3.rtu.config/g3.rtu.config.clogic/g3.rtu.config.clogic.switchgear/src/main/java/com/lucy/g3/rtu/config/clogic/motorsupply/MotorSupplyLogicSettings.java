/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.motorsupply;

import com.g3schema.ns_clogic.CLogicParametersBaseT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.SwitchgearParamT;
import com.g3schema.ns_common.ParameterT;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

public class MotorSupplyLogicSettings extends AbstractCLogicSettings<MotorSupplyLogic>{

  /**
   * The name of property {@value} . <li>Value type: {@link int}.</li>
   */
  public static final String PROPERTY_TIMEOUT_SECS = "timeout";


  private long timeout = 60;

  protected MotorSupplyLogicSettings () {
    this(null);
  }
  
  protected MotorSupplyLogicSettings (MotorSupplyLogic logic) {
    super(logic);
  }


	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		Object oldValue = getTimeout();
		this.timeout = timeout;
		firePropertyChange(PROPERTY_TIMEOUT_SECS, oldValue, timeout);
	}

@Override
  public void readParamFromXML(CLogicParametersT _xml) {
     if(_xml.generic.exists()) {
       CLogicParametersBaseT xml= _xml.generic.first();
       int size = xml.parameter.count();
       for (int i = 0; i < size ; i++) {
         ParameterT xmlParam = xml.parameter.at(i);
         if(PROPERTY_TIMEOUT_SECS.equals(xmlParam.parameterName.getValue())) {
        	 setTimeout(Integer.valueOf(xmlParam.parameterValue.getValue()));
           break;
         }
       }
     } else {
       log.warn("Parameters not found: "+ getOwnerLogic());
     }
  }

  @Override
  public void writeParamToXML(CLogicParametersT _xml) {
    CLogicParametersBaseT xml= _xml.generic.append();
    ParameterT xmlParam = xml.parameter.append();
    xmlParam.parameterName.setValue(PROPERTY_TIMEOUT_SECS);
    xmlParam.parameterValue.setValue(Integer.toString((int)getTimeout()));
  }

  
  @Override
  public IValidator getValidator() {
    return null;
  }
}
