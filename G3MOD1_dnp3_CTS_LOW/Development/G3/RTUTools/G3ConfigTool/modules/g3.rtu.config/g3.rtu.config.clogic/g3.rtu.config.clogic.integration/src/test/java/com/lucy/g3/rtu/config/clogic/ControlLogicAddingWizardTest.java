
package com.lucy.g3.rtu.config.clogic;

import com.lucy.g3.rtu.config.clogic.impl.CLogicManagerImpl;
import com.lucy.g3.rtu.config.clogic.ui.CLogicGraphPanel;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.ControlLogicAddingWizard;
import com.lucy.g3.test.support.utilities.TestUtil;

public class ControlLogicAddingWizardTest {

  public static void main(String[] args) {
    CLogicPlugin.init();
    ICLogic newLogic = ControlLogicAddingWizard.showDialog(new CLogicManagerImpl(null), null);

    if (newLogic != null) {
      CLogicGraphPanel panel = new CLogicGraphPanel(newLogic);
      panel.init();
      TestUtil.showFrame(panel);
    }
  }

}
