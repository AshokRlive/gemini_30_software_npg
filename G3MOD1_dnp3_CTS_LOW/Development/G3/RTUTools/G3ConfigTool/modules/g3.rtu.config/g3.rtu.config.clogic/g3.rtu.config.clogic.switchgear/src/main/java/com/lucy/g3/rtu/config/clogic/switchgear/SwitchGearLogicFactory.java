
package com.lucy.g3.rtu.config.clogic.switchgear;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


public class SwitchGearLogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor{
  public final static SwitchGearLogicFactory INSTANCE = new SwitchGearLogicFactory();
  private SwitchGearLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new SwitchGearLogicSettingsEditor((SwitchGearLogicSettings) settings);
  }

  @Override
  public SwitchGearLogic createLogic() {
    return new SwitchGearLogic();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return SwitchGearLogic.TYPE;
  }
  
  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }

  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    return new WizardPage[]{
        new SwitchGearLogicOptions(config)
    };
  }
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    SwitchGearLogic newLogic = createLogic();
    SwitchGearLogicOptions page = (SwitchGearLogicOptions) wizardData.get(SwitchGearLogicOptions.PAGE_ID);
    SwitchModuleOutput output = page.getSelectedSCM();
    newLogic.setOutputSCM(output);
    return newLogic;
  }

}

