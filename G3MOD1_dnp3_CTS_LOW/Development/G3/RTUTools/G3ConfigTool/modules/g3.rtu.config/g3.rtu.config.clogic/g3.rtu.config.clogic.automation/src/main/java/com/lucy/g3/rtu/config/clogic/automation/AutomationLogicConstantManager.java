/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation;

import java.util.Collection;

import com.g3schema.ns_clogic.AutomationConstantT;
import com.g3schema.ns_clogic.AutomationConstantsT;
import com.g3schema.ns_clogic.AutomationT;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutomationConstantType;
import com.lucy.g3.itemlist.ItemListManager;


/**
 * This class stores Automation Logic constants.
 */
public class AutomationLogicConstantManager extends ItemListManager<AutomationConstant> {
  public final static String PROPERTY_ALLOW_CHANGE = "allowChange";
  
  private boolean allowChange = true;
  
  
  public boolean isAllowChange() {
    return allowChange;
  }
  
  public void setAllowChange(boolean allowChange) {
    Object oldValue = this.allowChange;
    this.allowChange = allowChange;
    firePropertyChange("allowChange", oldValue, allowChange);
  }

  public void add(String name, AutomationConstantType type, String value){
    add(name,type, value,null);
  }
  
  public void setConstants(AutomationConstant[] constants) {
    /*Copy constants value from old to new*/
    for (int i = 0; i < constants.length; i++) {
      AutomationConstant existing = get(i);
      if(existing != null) {
        try {
          constants[i].setValue(existing.getValue());
        } catch(Exception e){
        }
      }
    }
    
    removeAll();
    for (int i = 0; i < constants.length; i++) {
      add(constants[i]);
    }
  }
  public void add(String name, AutomationConstantType type, String value, String unit){
    AutomationConstant newItem = new AutomationConstant(name, type);
    newItem.setValue(value);
    newItem.setUnit(unit);
    add(newItem);
    
    if(unit != null) {
      newItem.setUnit(unit);
    }
  }
  
  public void removeAll() {
    this.removeAll(getAllItems());
  }
  
  public void readParamFromXML(AutomationT xmlSettings) {
    AutomationConstantsT xmlConstants = xmlSettings.constants.first();
    int count = xmlConstants.constant.count();

    AutomationConstantType valueType; 
    for (int i = 0; i < count; i++) {
      AutomationConstantT xmlConstant = xmlConstants.constant.at(i);
      if(xmlConstant.valueType.exists())
        valueType = AutomationConstantType .valueOf(xmlConstant.valueType.getValue());
      else
        valueType = AutomationConstantType.String;
      
      AutomationConstant constant = new AutomationConstant(xmlConstant.name.getValue(), valueType);
      constant.setValue( xmlConstant.value2.getValue());
      if(xmlConstant.unit.exists())
        constant.setUnit(xmlConstant.unit.getValue());
      add(constant);
    }
  }

  public void writeParamToXML(AutomationT xmlSettings) {
    Collection<AutomationConstant> all = getAllItems();
    AutomationConstantsT xmlConstants = xmlSettings.constants.append();
    AutomationConstantT xmlConstant;
    for (AutomationConstant c : all) {
      xmlConstant = xmlConstants.constant.append();
      xmlConstant.name.setValue(c.getName());
      xmlConstant.value2.setValue(c.getValue().toString());
      xmlConstant.valueType.setValue(c.getValueType().name());
      xmlConstant.unit.setValue(c.getUnit());
    }
  }
}

