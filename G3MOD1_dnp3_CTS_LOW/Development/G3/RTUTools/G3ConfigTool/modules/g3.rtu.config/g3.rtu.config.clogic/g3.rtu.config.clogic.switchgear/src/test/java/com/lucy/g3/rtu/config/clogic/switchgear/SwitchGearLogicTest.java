/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.switchgear;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_SWOUT;

public class SwitchGearLogicTest {

  private ISwitchModule scm;
  private SwitchGearLogic fixture;

  private SwitchModuleOutput outputA;
  private SwitchModuleOutput outputB;


  @Before
  public void setUp() throws Exception {
    fixture = new SwitchGearLogic();
    scm = ModuleStub.createSCM();
    outputA = new SwitchModuleOutput(scm, SwitchIndex.SwitchA,
        SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_OPEN   ,
        SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_CLOSED ,
        SCM_CH_SWOUT.SCM_CH_SWOUT_OPEN_SWITCH     ,
        SCM_CH_SWOUT.SCM_CH_SWOUT_CLOSE_SWITCH     );
    outputB = new SwitchModuleOutput(scm, SwitchIndex.SwitchB,
        SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_OPEN   ,
        SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_CLOSED ,
        SCM_CH_SWOUT.SCM_CH_SWOUT_OPEN_SWITCH     ,
        SCM_CH_SWOUT.SCM_CH_SWOUT_CLOSE_SWITCH     );
  }

  @After
  public void tearDown() throws Exception {

  }

  @Test
  public void testSetOutput() {
    assertEquals(null, fixture.getOutputSCM());

    fixture.setOutputSCM(outputA);
    assertEquals(outputA, fixture.getOutputSCM());

    fixture.setOutputSCM(outputB);
    assertEquals(outputB, fixture.getOutputSCM());
  }

  @Test
  public void testNameUpdate() {
    fixture.setGroup(1);

    final String defaultName = fixture.getName();
    assertNotNull("default name is should  NOT be null ", defaultName);
    System.out.println("Default LOGIC name:" + defaultName);

    // Set output, expect name changed
    fixture.setOutputSCM(outputA);
    assertNotNull(fixture.getName());
    System.out.println("IO name:" + fixture.getOutputs()[0].getValueString());
    System.out.println("LOGIC name:" + fixture.getName());
    assertEquals("Expect logic name equals to output name",
        "[1] " + fixture.getOutputs()[0].getValueString(), fixture.getName());

    outputA.delete();
    assertEquals("Delete output, expect name recovereed", defaultName, fixture.getName());
  }

  @Test
  public void testDelete() {
    fixture.delete();
    assertEquals(0, fixture.getAllConnections().size());
    assertNull(fixture.getOutputSCM());
  }

}
