/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import java.awt.Frame;
import java.util.Collection;

import com.lucy.g3.rtu.config.shared.model.IDeletable;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The interface of control logic IO.
 *
 * @param <SourceT>
 *          the input source type of this IO.
 */
public interface CLogicIO<SourceT> extends INode, IValidation, IDeletable {

  String PROPERTY_VALUE = "value";

  String PROPERTY_VALUE_STRING = "valueString";

  String PROPERTY_VALID = "valid";
  
  String PROPERTY_LABEL = "label";
  
  String PROPERTY_ALLOW_CHANGE_LABEL = "allowChangeLabel";
  
  // ====== Constants ======
  String OPTIONAL_NULL_TEXT = "[None]";

  String MANDATORY_NULL_TEXT = "[Mandatory]";
  
  String CHOOSER_CANCELLED = "Cancelled";
  

  /**
   * Sets the value of this IO.
   *
   * @param newValue
   *          the new value
   * @throws RuntimeException
   *           the runtime exception if the new value is not acceptable
   */
  void setValue(SourceT newValue) throws RuntimeException;

  /**
   * Gets the value.
   * @return the value of this IO. It is normally the object which is assigned
   *         to this IO.
   */
  SourceT getValue();

  String getValueString();

  /**
   * Gets the label of this IO.
   *
   * @return The label text of this IO.
   */
  String getLabel();
  
  void setLabel(String label);
  
  boolean isAllowChangeLabel();

  /**
   * Checks if this IO is optional to be configured.
   *
   * @return true if this IO is not necessary to be configured, false if this IO
   *         is mandatory to be configured.
   */
  boolean isOptional();

  /**
   * Checks if this IO cannot enabled to be configured by user.
   *
   * @return {@code true} if this IO is allowed to be configured by user,
   *         {@code false} otherwise.
   */
  boolean isConfigurable();

  ICLogic getOwner();

  Class<SourceT> getSourceClass();

  /**
   * Shows a chooser dialog to choose a value for this IO.
   */
  void chooseValue(Frame parent);

  /**
   * Shows a chooser dialog to choose a value for this IO.
   * @param parent
   * @return the selected value by user. 
   * {@value #CHOOSER_CANCELLED} will be returned if the chooser is cancelled by user.
   */
  Object showChooser(Frame parent);
  

  /**
   * Checks if an IO value can be set to this Control Logic IO.
   *
   * @param ioValue
   *          the IO value to be validated
   * @return valid or not
   */
  boolean isAcceptable(SourceT ioValue);

  /**
   * Gets the summary information of this control logic IO.
   *
   * @return plain-text information.
   */
  String getSummary();

  // ================== Validation Support ===================
  /**
   * Checks if this IO is already correctly configured.
   *
   * @return true if this IO is configured correctly, false otherwise.
   */
  boolean isValid();

  IODirection getDirection();


  /**
   * The Enum IODirection.
   */
  public enum IODirection {
    INPUT("Input"), OUTPUT("Output");

    IODirection(String description) {
      this.description = description;
    }


    private final String description;


    @Override
    public String toString() {
      return description;
    }
  }


  Collection<SourceT> getAvailableSources();

  void fireValidChangeEvent();
}
