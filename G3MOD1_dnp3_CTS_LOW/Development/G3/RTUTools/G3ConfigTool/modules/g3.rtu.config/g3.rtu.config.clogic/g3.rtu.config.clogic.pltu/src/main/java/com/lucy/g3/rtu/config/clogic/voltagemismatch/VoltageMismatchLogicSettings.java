/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.voltagemismatch;

import com.g3schema.ns_clogic.CLogicParametersBaseT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.PLTUParamT;
import com.g3schema.ns_common.ParameterT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;


public class VoltageMismatchLogicSettings extends AbstractCLogicSettings<VoltageMismatchLogic> {
  public static final String PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC = "voltageMismatchDelaySec";

  private int voltageMismatchDelaySec = 5; // seconds
  
  VoltageMismatchLogicSettings(VoltageMismatchLogic owner) {
    super(owner);
  }

  public void setvoltageMismatchDelaySec(int voltageMismatchDelaySec) {
    if (voltageMismatchDelaySec < 0) {
      log.warn("Cannot set voltageMismatchDelaySec to: " + voltageMismatchDelaySec);
      return;
    }
    Object oldValue = getVoltageMismatchDelaySec();
    this.voltageMismatchDelaySec = voltageMismatchDelaySec;
    firePropertyChange(PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC, oldValue, voltageMismatchDelaySec);
  }

  public int getVoltageMismatchDelaySec() {
    return voltageMismatchDelaySec;
  }
  

  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    CLogicParametersBaseT xml = xml_param.generic.first();
    if(xml.parameter.exists()) {
      int size = xml.parameter.count();
      for (int i = 0; i < size ; i++) {
        ParameterT xmlParam = xml.parameter.at(i);
        if("voltageDelaySecs".equals(xmlParam.parameterName.getValue())) {
          setvoltageMismatchDelaySec(Integer.valueOf(xmlParam.parameterValue.getValue()));
          break;
        }
      }
    } else {
      log.warn("Voltage Mismatch parameters not found");
    }
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    ParameterT xmlParam = xml.generic.append().parameter.append();
    xmlParam.parameterName.setValue("voltageDelaySecs");
    xmlParam.parameterValue.setValue(String.valueOf(getVoltageMismatchDelaySec()));
  }
  @Override
  public IValidator getValidator() {
    return null;
  }

}

