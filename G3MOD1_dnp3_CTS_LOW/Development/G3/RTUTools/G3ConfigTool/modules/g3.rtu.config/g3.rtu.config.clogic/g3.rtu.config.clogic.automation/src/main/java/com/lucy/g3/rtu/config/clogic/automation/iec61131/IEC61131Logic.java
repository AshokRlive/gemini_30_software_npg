
package com.lucy.g3.rtu.config.clogic.automation.iec61131;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutoSchemeType;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogic;

public class IEC61131Logic extends AutomationLogic {
  public final static String INVALID_LIB = "lib{YouSchemeName}.so";
  
  static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.IEC61131, "IEC-61131")
      .desp("Control Logic for supporting IEC-61131")
      .opMode(OperationMode.START_STOP)
      .maxAmount(1)
      .allowUserAdd(false)
      .get();
  
  protected IEC61131Logic() {
    super(TYPE);

    /*Update related properties upon scheme type's change*/
    IEC61131LogicSettings settings = (IEC61131LogicSettings) getSettings();
    
    boolean isCustom = settings.getAutoSchemeType() == AutoSchemeType.CUSTOM;
    //setPointAllowToChange(isCustom);
    setManagerAllowToChange(isCustom);
    applyTemplate(settings.getAutoSchemeType());
    
    
    settings.addPropertyChangeListener(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_TYPE, 
        new PropertyChangeListener() {
          
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            boolean isCustom = evt.getNewValue() == AutoSchemeType.CUSTOM;
            //setPointAllowToChange(isCustom);
            setManagerAllowToChange(isCustom);
            applyTemplate((AutoSchemeType) evt.getNewValue());
          }
        });
    
    // Set initials
    settings.setAutoSchemeLib(INVALID_LIB);
    settings.setAutoSchemeType(AutoSchemeType.CUSTOM);
  }
  
  @Override
  protected IEC61131LogicSettings createSettings() {
    return new IEC61131LogicSettings(this);
  }
  
  private void applyTemplate(AutoSchemeType autoSchemeType) {
    IEC61131LogicTemplate.applyTemplate(autoSchemeType, this);
  }
  
  private void setManagerAllowToChange(boolean allowToChange){
    outputManager.setAllowUserChange(allowToChange);
    pointsManager.setAllowUserChange(allowToChange);
    inputManager.setAllowUserChange(allowToChange);
    getSettings().getConstantsManager().setAllowChange(allowToChange);
  }
  

}

