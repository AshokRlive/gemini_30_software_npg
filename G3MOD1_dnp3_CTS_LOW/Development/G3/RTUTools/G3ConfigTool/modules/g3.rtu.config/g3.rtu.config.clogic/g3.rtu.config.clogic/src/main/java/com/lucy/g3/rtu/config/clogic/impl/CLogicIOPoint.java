/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointRepository;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;

/**
 * The Class CLogicIOPoint.
 */
public final class CLogicIOPoint extends AbstractCLogicIO<VirtualPoint> {

  private final VirtualPointType[] supportedTypes;


  public CLogicIOPoint(ICLogic owner, String name, VirtualPointType[] supportedTypes, boolean allowUserChange) {
    this(owner, name, supportedTypes, allowUserChange, true, true, true);
  }

  public CLogicIOPoint(ICLogic owner, VirtualPointType[] supportedTypes, 
      boolean allowUserChangeLabel, 
      boolean allowUserDelete, 
      boolean optional, 
      boolean enabled) {
    super(owner, IODirection.INPUT, "Input", allowUserChangeLabel, allowUserDelete, optional, enabled);
    
    this.supportedTypes = supportedTypes;
  }
  
  public CLogicIOPoint(ICLogic owner, String label, VirtualPointType[] supportedTypes, 
      boolean allowUserChangeLabel, 
      boolean allowUserDelete,  
      boolean optional, 
      boolean enabled) {
    super(owner, IODirection.INPUT, label, allowUserChangeLabel, allowUserDelete, optional, enabled);

    this.supportedTypes = supportedTypes;
  }
  

  @Override
  public void chooseValue(Frame parent) {
     Object value = showChooser(parent);
     if(value == null || value instanceof VirtualPoint)
       setValue((VirtualPoint) value);
  }
  
  @Override
  public Object showChooser(Frame parent) {
    Collection<VirtualPoint> sources = getAvailableSources();
    VirtualPointSelector selector = new VirtualPointSelector(getParentFrame(), getValue(),
        sources,
        getOwner().getGroup());
    
    // Compile subtitle from the supported types
    if (supportedTypes != null) {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < supportedTypes.length; i++) {
        if (supportedTypes[i] != null) {
          builder.append(supportedTypes[i].getName());
          if (i != supportedTypes.length - 1) {
            builder.append(", ");
          }
        }
      }
      selector.setSubtitle(builder.toString());
    }

    selector.showDialog();

    if (!selector.hasBeenCanceled()) {
      return selector.getSelectedPoint();
    } else {
      return CHOOSER_CANCELLED;
    }
  }

  @Override
  public boolean isAcceptable(VirtualPoint ioValue) {
    if (ioValue == null) {
      return true; // Null value always acceptable

    }

    if (supportedTypes != null) {
      boolean validType = false;
      for (VirtualPointType t : supportedTypes) {
        if (t == ioValue.getType()) {
          validType = true;
          break;
        }
      }

      if (validType == false) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String getSummary() {
    VirtualPoint vpoint = getValue();
    return "Output Point: "
        + (vpoint == null ? "None" : vpoint.getName());
  }

  @Override
  public Class<VirtualPoint> getSourceClass() {
    return VirtualPoint.class;
  }
  
  public VirtualPointType getSupportedType(){
    return supportedTypes[0];//Arrays.copyOf(supportedTypes, supportedTypes.length);
  }

  @Override
  public Collection<VirtualPoint> getAvailableSources() {
    IConfig config = ConfigManager.getInstance().getLocalConfig();
    VirtualPointRepository repo = config == null ? null
            :(VirtualPointRepository) config.getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);
    
    return repo == null ? new ArrayList<VirtualPoint>(0) : repo.getAllPointsByType(supportedTypes);
  }
}
