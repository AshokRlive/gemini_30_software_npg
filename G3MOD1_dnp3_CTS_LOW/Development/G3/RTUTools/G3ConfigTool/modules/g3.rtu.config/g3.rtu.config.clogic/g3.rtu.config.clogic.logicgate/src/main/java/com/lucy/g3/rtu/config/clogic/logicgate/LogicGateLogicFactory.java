
package com.lucy.g3.rtu.config.clogic.logicgate;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class LogicGateLogicFactory extends AbstractClogicFactory{
  public final static LogicGateLogicFactory INSTANCE = new LogicGateLogicFactory();
  private LogicGateLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new LogicGateSettingsEditor((LogicGateSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new LogicGate();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return LogicGate.TYPE;
  }
  
}

