/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61131;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.automation.iec61131.IEC61131Logic;


public class IEC61131LogicTest {
  private IEC61131Logic fixture;
  
  @Before
  public void setUp() throws Exception {
    fixture = new IEC61131Logic();
  }

  @Test
  public void testManagerChangeable() {
    assertTrue(fixture.getInputsManager().isAllowUserChange());
    assertTrue(fixture.getOutputsManager().isAllowUserChange());
    assertTrue(fixture.getPointsManager().isAllowUserChange());
    
//    assertFalse(fixture.getInputsManager().isAllowUserChange());
//    assertFalse(fixture.getOutputsManager().isAllowUserChange());
//    assertFalse(fixture.getPointsManager().isAllowUserChange());
  }
  
}

