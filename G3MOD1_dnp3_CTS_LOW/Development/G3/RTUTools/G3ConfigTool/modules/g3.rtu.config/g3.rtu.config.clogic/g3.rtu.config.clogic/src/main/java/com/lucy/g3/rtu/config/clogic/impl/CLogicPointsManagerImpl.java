/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.ListModel;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;

/**
 * The Class CLogicPointsManagerImpl.
 */
public class CLogicPointsManagerImpl extends Model implements ICLogicPointsManager {

  private final ArrayListModel<IPseudoPoint> pointsList = new ArrayListModel<>();
  
  private final ArrayList<ICLogicPointsManagerObserver> observers = new ArrayList<>();
  
  private final ICLogic owner;
  private boolean allowUserChange = true;

  
  public CLogicPointsManagerImpl(ICLogic owner) {
    super();
    this.owner = owner;
  }

  @SuppressWarnings("unchecked")
  @Override
  public ListModel<IPseudoPoint> getListModel() {
    return pointsList;
  }

  @Override
  public Collection<IPseudoPoint> addPoint(VirtualPointType type, boolean userCreated, int num,String ... label) {
	  ArrayList<IPseudoPoint> newPoints = new ArrayList<>();
	  if (type != null) {

      IPseudoPoint newPoint; 
      for (int i = 0; i < num; i++) {
        newPoint = PseudoPointFactory.createPseudoPoint(type, 0, owner, i < label.length ? label[i] : null);
        newPoint.setAllowChangeDescription(userCreated);
        newPoint.setAllowDelete(userCreated);
        newPoints.add(newPoint);
      }

      pointsList.addAll(newPoints);
      updateAllPointID();
      
      for (ICLogicPointsManagerObserver ob : observers) {
        ob.pointsAdded(newPoints);
      }
    }
	  return newPoints;
  }
  
  @Override
  public void setPoints(Collection<IPseudoPoint> points) {
    ArrayList<IPseudoPoint> plist = new ArrayList<>(points);
    Collections.sort(plist, PointComparator.INSTANCE);
    
    removeAll();
    pointsList.addAll(plist);
    
    for (ICLogicPointsManagerObserver ob : observers) {
      ob.pointsAdded(points);
    }
  }

  private void updateAllPointID() {
    IPseudoPoint point;
    
    // Set id 
    for (int i = 0; i < pointsList.size(); i++) {
      point = pointsList.get(i);
      point.setId(i);
    }
  }

  @Override
  public IPseudoPoint[] getAllPoints() {
    return pointsList.toArray(new IPseudoPoint[pointsList.size()]);
  }

  @Override
  public void removeAll() {
    removeAll(new ArrayList<>(pointsList));
  }
  
  @Override
  public void removeAll(Collection<IPseudoPoint> removeItems) {
    pointsList.removeAll(removeItems);
    updateAllPointID();
    
    for (ICLogicPointsManagerObserver ob : observers) {
      ob.pointsRemoved(removeItems);
    }
    
    for (IPseudoPoint point : removeItems) {
      point.delete();
    }
  }

  @Override
  public int getSize() {
    return pointsList.size();
  }

  @Override
  public void addObserver(ICLogicPointsManagerObserver observer) {
    observers.add(observer);
  }

  @Override
  public void removeObserver(ICLogicPointsManagerObserver observer) {
    observers.remove(observer);
  }
  
  @Override
  public boolean isAllowUserChange() {
    return allowUserChange;
  }
  
  @Override
  public void setAllowUserChange(boolean allowUserChange) {
    Object oldValue = this.allowUserChange;
    this.allowUserChange = allowUserChange;
    firePropertyChange(PROPERTY_ALLOW_USER_CHANGE, oldValue, allowUserChange);
  }


  private static class PointComparator implements Comparator<IPseudoPoint> {
    private static PointComparator INSTANCE = new PointComparator();
    
    @Override
    public int compare(IPseudoPoint o1, IPseudoPoint o2) {
      if (o1 == null)
        return 1;
      if (o2 == null)
        return -1;
      
//      VirtualPointType t1 = o1.getType();
//      VirtualPointType t2 = o2.getType();
//      
//      return t1.ordinal() - t2.ordinal();
      int t1 = o1.getId();
      int t2 = o2.getId();
      
      return t1 - t2;
    }
  }


  @Override
  public IPseudoPoint getByID(int id) {
    for (IPseudoPoint p : pointsList) {
      if(p.getId() == id)
        return p;
    }
    
    return null;
  }

//  @Override
//  public void addPoints(IPseudoPoint... points) {
//    for (IPseudoPoint p : points) {
//      if(findPoint(p.getId(), p.getType()) == null) {
//        pointsList.add(p);
//      }
//    }
//    
//    sortPointsList();
//  }
//  
//  private IPseudoPoint findPoint(int id, VirtualPointType type) {
//    for (IPseudoPoint p : pointsList) {
//      if (p.getId() == id && p.getType() == type) {
//        return p;
//      }
//    }
//    return null;
//  }

}
