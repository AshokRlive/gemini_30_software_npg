/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation;

import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutomationConstantType;

/**
 * The Class AutomationConstant.
 *
 * @param <T>
 *          the generic type
 */
public class AutomationConstant {

  private String name;
  private String unit;
  private String value;
  private final AutomationConstantType valueType;


  public AutomationConstant(String name, AutomationConstantType valueType) {
    this.name = name;
    this.valueType = valueType;
    
    // Init value
    switch(valueType) {
    case Boolean:
      value = "false";
      break;
    case Float:
      value = "0.0";
      break;
    case Integer:
    case Long:
    case Short:
      value = "0";
      break;
    case String:
      value = "";
      break;
    default:
      break;
    }
  }

  public String getName() {
    return name;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) throws IllegalConstantException {
    this.value = validate(value);
  }

  private String validate(String value) throws IllegalConstantException {
    try {
      switch (valueType) {
      case Boolean:
        return Boolean.valueOf(value).toString();
      case Float:
        return Float.valueOf(value).toString();
      case Integer:
        return Integer.valueOf(value).toString();
      case Long:
        return Long.valueOf(value).toString();
      case Short:
        return Short.valueOf(value).toString();
      case String:
        return Integer.valueOf(value).toString();
      default:
        return value;
      }
    }catch(Throwable e) {
      throw new IllegalConstantException("Illegal constant value, the valid type is:"+valueType);
    }
  }

  public AutomationConstantType getValueType() {
    return valueType;
  }
  
  public static class IllegalConstantException extends RuntimeException {

    public IllegalConstantException(String string) {
      super(string);
    }
    
  }

}