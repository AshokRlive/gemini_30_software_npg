
package com.lucy.g3.rtu.config.clogic.tcl;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOLogic;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_TRIPPING_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_TRIPPING_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_TRIPPING_POINT;


public class TCLTrippingLogic extends PredefinedCLogic<TCLTrippingSettings>  implements ISupportOffLocalRemote{
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.TCL_TRIPPING, "TCL Tripping Logic")
      .desp("Timed Connection Tripping Logic")
      .maxAmount(1)
      .requireOLR(true)
      .get();
  
  private final CLogicIO<ICLogic>[] outputs;

  public TCLTrippingLogic() {
    super(TYPE, TCL_TRIPPING_INPUT.values(), TCL_TRIPPING_POINT.values());
    this.outputs = initOutputs();
  }

  private CLogicIO<ICLogic>[] initOutputs() {
    TCL_TRIPPING_OUTPUT[] outputEnums = TCL_TRIPPING_OUTPUT.values();
    CLogicIO<ICLogic>[] outputs = new CLogicIO[outputEnums.length];

    for (int i = 0; i < outputs.length; i++) {
      if(i == TCL_TRIPPING_OUTPUT.TCL_TRIPPING_EXT_LATCH_RELAY.ordinal())
      {
        outputs[i] = new CLogicIOLogic(this, outputEnums[i].getDescription(), CLogicFactories.getAllOperatibleTypes(), true,true, false,false);
      }else {
        outputs[i] = new CLogicIOLogic(this, outputEnums[i].getDescription(), new int[]{ICLogicType.SGL}, false,true, false,false);}
    }
    return outputs;
  }
  
  @Override
  public CLogicIO<?>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }
  
  public CLogicIO<ICLogic> getCircuitBreakerOutput() {
    return outputs[TCL_TRIPPING_OUTPUT.TCL_TRIPPING_CB.ordinal()];
  }

  @Override
  public Module getSourceModule() {
    return null;
  }


  public CLogicIO<VirtualPoint> getInput(TCL_TRIPPING_INPUT inputEnum) {
    return getInputByEnum(inputEnum);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInput(TCL_TRIPPING_INPUT.TCL_TRIPPING_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  protected TCLTrippingSettings createSettings() {
    return new TCLTrippingSettings(this);
  }

}

