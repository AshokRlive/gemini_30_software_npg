/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.utils;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicRepository;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;


/**
 *
 */
public class CLogicUtility extends CLogicFinder implements IConfigUtility {
  private CLogicUtility () {}
  
  public static Collection<ICLogic> getAllCLogic(IConfig config) {
    CLogicRepository repo = getRepository(config);
    if (repo == null) {
      Logger.getLogger(CLogicUtility.class).error("ICLogic repository not found!");
      return new ArrayList<ICLogic>(0);
    } else {
      return repo.getAllClogic();
    }
  }

  private static CLogicRepository getRepository(IConfig config) {
    return config.getConfigModule(CLogicRepository.CONFIG_MODULE_ID);
  }
  
  private static CLogicManager getManager(IConfig config) {
    return config.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
  }

  public static void removeLogic(IConfig config, ICLogic clogic) {
    getManager(config).remove(clogic);
  }
}

