
package com.lucy.g3.rtu.config.clogic.offlocalremote;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class OffLocalRemoteFactory extends AbstractClogicFactory {
  public final static OffLocalRemoteFactory INSTANCE = new OffLocalRemoteFactory();
  private OffLocalRemoteFactory(){}
  

  @Override
  public ICLogic createLogic() {
    return new OffLocalRemote();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return OffLocalRemote.TYPE;
  }

}

