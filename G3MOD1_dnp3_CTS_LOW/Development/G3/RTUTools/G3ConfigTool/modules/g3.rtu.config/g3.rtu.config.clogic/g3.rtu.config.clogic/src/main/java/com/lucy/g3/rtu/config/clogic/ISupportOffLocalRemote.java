/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;

/**
 * This interface must be implemented if a control logic has an input which
 * should be mapped to OffLocalRemote.
 */
public interface ISupportOffLocalRemote {

  void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint);
}
