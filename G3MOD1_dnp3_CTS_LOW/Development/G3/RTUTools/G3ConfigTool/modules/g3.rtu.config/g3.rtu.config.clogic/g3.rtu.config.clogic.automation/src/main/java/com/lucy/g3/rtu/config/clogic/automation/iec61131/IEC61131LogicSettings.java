/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61131;

import com.g3schema.ns_clogic.AutomationT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutoSchemeLib;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutoSchemeType;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogic;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * This class stores Automation Logic settings.
 */
public class IEC61131LogicSettings extends AutomationLogicSettings{

  public static final String PROPERTY_AUTO_SCHEME_TYPE = "autoSchemeType";
  public static final String PROPERTY_AUTO_SCHEME_LIB = "autoSchemeLib";

  private String autoSchemeLib;
  private AutoSchemeType autoSchemeType = null;
  private final IValidator validator = new SetttingsValidator(this);

  IEC61131LogicSettings(AutomationLogic owner){
    super(owner);
  }
  
  public String getAutoSchemeLib() {
    return autoSchemeLib;
  }

  public void setAutoSchemeLib(String autoSchemeLib) {
    Object oldValue = this.autoSchemeLib;
    this.autoSchemeLib = autoSchemeLib;
    firePropertyChange(PROPERTY_AUTO_SCHEME_LIB, oldValue, autoSchemeLib);
  }

  public AutoSchemeType getAutoSchemeType() {
    return autoSchemeType;
  }

  public void setAutoSchemeType(AutoSchemeType autoSchemeType) {
    if(autoSchemeType != null) {
      Object oldValue = this.autoSchemeType;
      this.autoSchemeType = autoSchemeType;
      firePropertyChange(PROPERTY_AUTO_SCHEME_TYPE, oldValue, autoSchemeType);
    }
  }

  public static String getDefaultAutoSchemeLibName(AutoSchemeType autoSchemeType) {
    if (autoSchemeType == null)
      return "";
    AutoSchemeLib libEnum = AutoSchemeLib.forValue(autoSchemeType.getValue());
    if (libEnum == null)
      return "";
    else {
      return libEnum.getDescription();
    }
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xmlParam) {
    AutomationT xml = xmlParam.automation.first();
    
    setAutoStart(xml.autoStart.getValue());
    if(xml.autoSchemeVersionMajor.exists() && xml.autoSchemeVersionMinor.exists())
      setAutoSchemeVersion(new BasicVersion(xml.autoSchemeVersionMajor.getValue(), xml.autoSchemeVersionMinor.getValue()));
    
    AutoSchemeType type;
    try {
    if(xml.autoSchemeType.exists())
      type = AutoSchemeType.forValue(xml.autoSchemeType.getValue());
    else
      type = AutoSchemeType.CUSTOM;
    }catch(Exception e){
      type = AutoSchemeType.CUSTOM;
    }
    
    setAutoSchemeType(type);
    
    if(type == AutoSchemeType.CUSTOM) {
      setAutoSchemeLib(xml.sharedLibraryFile.getValue());
    } else {
      setAutoSchemeLib(getDefaultAutoSchemeLibName(type));
    }
    
    getConstantsManager().readParamFromXML(xml);
  }

  
  @Override
  public void writeParamToXML(CLogicParametersT xmlParams) {
    AutomationT xml = xmlParams.automation.exists() ? 
        xmlParams.automation.first() : xmlParams.automation.append();
        
    xml.autoStart.setValue(isAutoStart());
    xml.sharedLibraryFile.setValue(autoSchemeLib);
    xml.autoSchemeType.setValue(getAutoSchemeType().getValue());
    xml.autoSchemeVersionMajor.setValue(getAutoSchemeVersion().major());
    xml.autoSchemeVersionMinor.setValue(getAutoSchemeVersion().minor());
    
    getConstantsManager().writeParamToXML(xml);
  }

  private class SetttingsValidator extends AbstractValidator<IEC61131LogicSettings> implements IValidator{
    SetttingsValidator(IEC61131LogicSettings target){
      super(target);
    }
    
    @Override
    public String getTargetName() {
      AutoSchemeType type = IEC61131LogicSettings.this.getAutoSchemeType();
      return type == null ? "AutomationLogic" :type.getDescription();
    }

    @Override
    protected void validate(ValidationResultExt result) {
      if(getAutoSchemeType() == null) {
        result.addError("No automation scheme selected!");
      }
      
      if(Strings.isBlank(getAutoSchemeLib())) {
        result.addError("No automation library configured!");
      }
    }

  }
  
  @Override
  public IValidator getValidator() {
    return validator;
  }
}
