/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.threshold;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel.HighHighLowLowPanel;
import com.lucy.g3.rtu.config.virtualpoint.shared.validation.HighHighLowLowValidator;

class ThresholdLogicSettingsEditor extends AbstractCLogicSettingsEditor {
  private final ThresholdLogicSettings settings;
  private HighHighLowLowValidator validator;
  ThresholdLogicSettingsEditor(ThresholdLogicSettings settings) {
    super(settings);
    this.settings = settings;
  }

  @Override
  public void validate(ValidationResult result) {
    if(validator != null)
      validator.validate(result);
  }

  @Override
  public JComponent getComponent(JDialog parent) {

    DefaultFormBuilder builder = getDefaultBuilder();

      builder.append("Scaled Max:", new JLabel(Double.toString(settings.getScaledMaxValue())));
      builder.nextLine();
      builder.append("Scaled Min:", new JLabel(Double.toString(settings.getScaledMinValue())));
      builder.nextLine();

      PresentationModel<HighHighLowLow> hihiloloPm = new PresentationModel<HighHighLowLow>(settings.getHihilolo());
      HighHighLowLowPanel panel = new HighHighLowLowPanel(settings.getFullRangeValue(), hihiloloPm);
      builder.append(panel, 5);

      addPM(hihiloloPm);
      validator = new HighHighLowLowValidator(hihiloloPm);

    return builder.getPanel();
  }

}
