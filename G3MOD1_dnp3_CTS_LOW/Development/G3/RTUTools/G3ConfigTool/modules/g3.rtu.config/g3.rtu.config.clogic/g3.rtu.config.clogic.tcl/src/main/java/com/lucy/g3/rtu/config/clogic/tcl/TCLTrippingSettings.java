
package com.lucy.g3.rtu.config.clogic.tcl;

import javax.swing.JComponent;

import org.apache.log4j.Logger;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_tcl.TCLTrippingT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

public class TCLTrippingSettings extends AbstractCLogicSettings<TCLTrippingLogic> implements ICLogicSettings {
  private boolean cbTripEnable = true;
  private boolean commsFailEnable = true;
  private long tripSignalTimer = 3600;
  private long commsFailTimer = 3600;
  private long cbCurrentThreshold = 10;
  
  public TCLTrippingSettings(TCLTrippingLogic logic) {
    super(logic);
  }
  
  public boolean isCbTripEnable() {
    return cbTripEnable;
  }
  
  public void setCbTripEnable(boolean cbTripEnable) {
    Object oldValue = this.cbTripEnable;
    this.cbTripEnable = cbTripEnable;
    firePropertyChange("cbTripEnable", oldValue, cbTripEnable);
  }

  
  public boolean isCommsFailEnable() {
    return commsFailEnable;
  }

  
  public void setCommsFailEnable(boolean commsFailEnable) {
    this.commsFailEnable = commsFailEnable;
  }

  
  public long getTripSignalTimer() {
    return tripSignalTimer;
  }

  
  public void setTripSignalTimer(long tripSignalTimer) {
    this.tripSignalTimer = tripSignalTimer;
  }

  
  public long getCommsFailTimer() {
    return commsFailTimer;
  }

  
  public void setCommsFailTimer(long commsFailTimer) {
    this.commsFailTimer = commsFailTimer;
  }

  @Override
  public IValidator getValidator() {
    return null;
  }

  public long getCbCurrentThreshold() {
    return cbCurrentThreshold;
  }

  
  public void setCbCurrentThreshold(long cbCurrentThreshold) {
    Object oldValue = this.cbCurrentThreshold;
    this.cbCurrentThreshold = cbCurrentThreshold;
    firePropertyChange("cbCurrentThreshold", oldValue, cbCurrentThreshold);
  }

  @Override
  public void readParamFromXML(CLogicParametersT _xml) {
    if(!_xml.tclTripping.exists()) {
      Logger.getLogger(getClass()).error("TCLTripping settings not found in XML");
      return;
    }
      
    TCLTrippingT xml = _xml.tclTripping.first();
    setCbTripEnable(xml.cbTripEnable.getValue());
    setCommsFailEnable(xml.commsFailMonitorEnable.getValue());
    setTripSignalTimer(xml.tripSignalTimer.getValue());
    setCommsFailTimer(xml.commsFailTimer.getValue());
    
    if(xml.cbCurrentThreshold.exists()) {
      setCbCurrentThreshold(xml.cbCurrentThreshold.getValue());
    }
  }

  @Override
  public void writeParamToXML(CLogicParametersT _xml) {
    TCLTrippingT xml = _xml.tclTripping.append();
    xml.cbTripEnable.setValue(isCbTripEnable());
    xml.commsFailMonitorEnable.setValue(isCommsFailEnable());
    xml.tripSignalTimer.setValue(getTripSignalTimer());
    xml.commsFailTimer.setValue(getCommsFailTimer());
    xml.cbCurrentThreshold.setValue((int) getCbCurrentThreshold());
  }
}

