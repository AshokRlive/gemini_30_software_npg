/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.switchgear;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.SwitchgearParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * Implementation of Switchgear Logic. 
 */
public class SwitchGearLogicSettings extends AbstractCLogicSettings<SwitchGearLogic>{

  /**
   * The name of property {@value} . <li>Value type: {@link long}.</li>
   */
  public static final String PROPERTY_OVER_CUR_THRESHOLD = "motoCurrentThreshold";

  /**
   * The name of property {@value} . <li>Value type: {@link long}.</li>
   */
  public static final String PROPERTY_OVER_CUR_TIME_MS = "motoCurrentTime";

  /**
   * The name of property {@value} . <li>Value type: {@link int}.</li>
   */
  public static final String PROPERTY_LOCALDELAY_SEC = "localDelay";

  /**
   * The name of property {@value} . <li>Value type: {@link int}.</li>
   */
  /**
   * The name of property {@value} . <li>Value type: {@link int}.</li>
   */
  public static final String PROPERTY_REMOTEDELAY_SEC = "remoteDelay";

  /**
   * The name of property {@value} . <li>Value type: {@link int}.</li>
   */
  public static final String PROPERTY_PRE_OPERATION_DELAY_MS = "preOpDelay";

  /**
   * The name of property {@value} . <li>Value type: {@link int}.</li>
   */
  public static final String PROPERTY_POST_OPERATION_DELAY_MS = "postOpDelay";

  public static final String PROPERTY_CIRCUIT_BREAKER = "circuitBreaker";
  
  public static final String PROPERTY_OPEN_WHEN_EARTHED = "openWhenEarthed";

  private long motoCurrentThreshold = 0;

  private long motoCurrentTime = 0;

  private int localDelay = 0;

  private int remoteDelay = 0;

  private int preOpDelay = 0;

  private int postOpDelay = 0;

  private boolean circuitBreaker;
  
  private boolean openWhenEarthed;

  private SwitchGearLogicSettingsValidator validator;
  
  protected SwitchGearLogicSettings () {
    this(null);
  }
  
  protected SwitchGearLogicSettings (SwitchGearLogic logic) {
    super(logic);
  }

  public long getMotoCurrentThreshold() {
    return motoCurrentThreshold;
  }

  public void setMotoCurrentThreshold(long motoCurrentThreshold) {
    Object oldValue = getMotoCurrentThreshold();
    this.motoCurrentThreshold = motoCurrentThreshold;
    firePropertyChange(PROPERTY_OVER_CUR_THRESHOLD, oldValue, motoCurrentThreshold);
  }

  public long getMotoCurrentTime() {
    return motoCurrentTime;
  }

  public void setMotoCurrentTime(long motoCurrentTime) {
    Object oldValue = getMotoCurrentTime();
    this.motoCurrentTime = motoCurrentTime;
    firePropertyChange(PROPERTY_OVER_CUR_TIME_MS, oldValue, motoCurrentTime);
  }

  public int getLocalDelay() {
    return localDelay;
  }

  public void setLocalDelay(int localDelay) {
    Object oldValue = getLocalDelay();
    this.localDelay = localDelay;
    firePropertyChange(PROPERTY_LOCALDELAY_SEC, oldValue, localDelay);
  }

  public int getRemoteDelay() {
    return remoteDelay;
  }

  public void setRemoteDelay(int remoteDelay) {
    Object oldValue = getRemoteDelay();
    this.remoteDelay = remoteDelay;
    firePropertyChange(PROPERTY_REMOTEDELAY_SEC, oldValue, remoteDelay);
  }

  public int getPreOpDelay() {
    return preOpDelay;
  }

  public void setPreOpDelay(int preOpDelay) {
    Object oldValue = this.preOpDelay;
    this.preOpDelay = preOpDelay;
    firePropertyChange(PROPERTY_PRE_OPERATION_DELAY_MS, oldValue, preOpDelay);
  }

  public int getPostOpDelay() {
    return postOpDelay;
  }

  public void setPostOpDelay(int postOpDelay) {
    Object oldValue = this.postOpDelay;
    this.postOpDelay = postOpDelay;
    firePropertyChange(PROPERTY_POST_OPERATION_DELAY_MS, oldValue, postOpDelay);
  }

  public boolean isCircuitBreaker() {
    return circuitBreaker;
  }

  public void setCircuitBreaker(boolean circuitBreaker) {
    Object oldValue = this.circuitBreaker;
    this.circuitBreaker = circuitBreaker;
    firePropertyChange(PROPERTY_CIRCUIT_BREAKER, oldValue, circuitBreaker);
  }

  
  public boolean isOpenWhenEarthed() {
    return openWhenEarthed;
  }
  
  public void setOpenWhenEarthed(boolean openWhenEarthed) {
    Object oldValue = this.openWhenEarthed;
    this.openWhenEarthed = openWhenEarthed;
    firePropertyChange(PROPERTY_OPEN_WHEN_EARTHED, oldValue, openWhenEarthed);
  }
  
  public void copyFrom(SwitchGearLogicSettings from) {
    this.circuitBreaker = from.circuitBreaker;
    this.localDelay = from.localDelay;
    this.motoCurrentThreshold = from.motoCurrentThreshold;
    this.motoCurrentTime = from.motoCurrentTime;
    this.postOpDelay = from.postOpDelay;
    this.preOpDelay = from.preOpDelay;
    this.remoteDelay = from.remoteDelay;
  }

  public void writeParamToXML(SwitchgearParamT xml) {
    xml.motorCurrentThreshold.setValue(getMotoCurrentThreshold());
    xml.motorOvercurrentTime.setValue(getMotoCurrentTime());
    xml.localDelay.setValue(getLocalDelay());
    xml.remoteDelay.setValue(getRemoteDelay());
    xml.preOperationTime.setValue(getPreOpDelay());
    xml.postOperationTime.setValue(getPostOpDelay());
    xml.isCircuitBreaker.setValue(isCircuitBreaker());
    xml.showOpenWhenEarthed.setValue(isOpenWhenEarthed());
  }

  public void readParamFromXML(SwitchgearParamT xml) {
    setMotoCurrentThreshold(xml.motorCurrentThreshold.getValue());
    setMotoCurrentTime(xml.motorOvercurrentTime.getValue());
    setLocalDelay((int) xml.localDelay.getValue());
    setRemoteDelay((int) xml.remoteDelay.getValue());
    setPreOpDelay(xml.preOperationTime.getValue());
    setPostOpDelay(xml.postOperationTime.getValue());
    if (xml.isCircuitBreaker.exists()) {
      setCircuitBreaker(xml.isCircuitBreaker.getValue());
    }
    
    if (xml.showOpenWhenEarthed.exists()) {
      setOpenWhenEarthed(xml.showOpenWhenEarthed.getValue());
    }
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml) {
    readParamFromXML(xml.switchgearParam.first());    
  }
  
  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    writeParamToXML(xml.switchgearParam.exists() ? 
        xml.switchgearParam.first():xml.switchgearParam.append());
  }

  @Override
  public IValidator getValidator() {
    if(validator == null)
      validator = new SwitchGearLogicSettingsValidator(getOwnerLogic());
    
    return validator;
  }
}
