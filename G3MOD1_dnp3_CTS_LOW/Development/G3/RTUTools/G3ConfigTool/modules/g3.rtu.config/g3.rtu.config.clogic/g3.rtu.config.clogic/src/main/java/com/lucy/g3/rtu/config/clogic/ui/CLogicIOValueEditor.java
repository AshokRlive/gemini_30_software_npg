/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.clogic.CLogicIO;

/**
 * The Class CLogicIOValueEditor.
 */
class CLogicIOValueEditor extends AbstractCellEditor
    implements TableCellEditor,
    ActionListener {

  public static final String NULL_TEXT = "- None -";
//  private Collection<Object> sources;
  private Object currentValue;
  private CLogicIO<?> logicIO;
  private JButton button;
  protected static final String EDIT = "edit";


  public CLogicIOValueEditor() {
    button = new JButton();
    button.setActionCommand(EDIT);
    button.setHorizontalAlignment(SwingConstants.LEADING);
    button.addActionListener(this);
    button.setBorderPainted(false);
  }

  /**
   * Handles events from the editor button and from the dialog's OK button.
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    if (EDIT.equals(e.getActionCommand())) {
      button.setText(currentValue == null ? NULL_TEXT : currentValue.toString());
      Object value = logicIO.showChooser(WindowUtils.getMainFrame());
      if(!CLogicIO.CHOOSER_CANCELLED.equals(value)) {
        currentValue = value;
        stopCellEditing();
      }else {
        cancelCellEditing();
      }
//      CLogicIOValueChooser valueChooser = new CLogicIOValueChooser(WindowUtils.getMainFrame(), sources, currentValue);
//      valueChooser.pack();
//      valueChooser.setVisible(true);
//      
//      
//      if(valueChooser.hasBeenAffirmed()) {
//        currentValue = valueChooser.getSelected();
//        // Make the renderer reappear.
//        stopCellEditing();
//      }else{
//        cancelCellEditing();
//      }
    } 
  }

  @Override
  public Object getCellEditorValue() {
    return currentValue;
  }

  @Override
  public Component getTableCellEditorComponent(JTable table,
      Object value,
      boolean isSelected,
      int row,
      int column) {
    AbstractTableAdapter<?> model = (AbstractTableAdapter<?>) table.getModel();
    logicIO = (CLogicIO<?>) model.getRow(row);
    //sources = new ArrayList<Object>(logicIO.getAvailableSources());
    currentValue = value;
    this.button.setText(currentValue == null ? NULL_TEXT : currentValue.toString());
    return button;
  }
}