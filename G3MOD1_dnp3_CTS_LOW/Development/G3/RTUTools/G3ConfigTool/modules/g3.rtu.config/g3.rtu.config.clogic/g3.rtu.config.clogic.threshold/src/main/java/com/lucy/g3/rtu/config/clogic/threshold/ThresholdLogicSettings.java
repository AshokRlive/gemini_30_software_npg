/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.threshold;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;


public class ThresholdLogicSettings extends AbstractCLogicSettings<ThresholdLogic> {
  
  private final HighHighLowLow hihilolo = new HighHighLowLow();

  private double fullRangeValue = 0;

  
  ThresholdLogicSettings(ThresholdLogic owner) {
    super(owner);
  }
  
  void setFullRangeValue(double newFullRangeValue) {
    hihilolo.setHihiHysteresis(calculateHys(this.fullRangeValue, newFullRangeValue, hihilolo.getHihiHysteresis()));
    hihilolo.setHiHysteresis(calculateHys(this.fullRangeValue, newFullRangeValue, hihilolo.getHiHysteresis()));
    hihilolo.setLoHysteresis(calculateHys(this.fullRangeValue, newFullRangeValue, hihilolo.getLoHysteresis()));
    hihilolo.setLoloHysteresis(calculateHys(this.fullRangeValue, newFullRangeValue, hihilolo.getLoloHysteresis()));
    this.fullRangeValue = newFullRangeValue;
  }

  private static double calculateHys(double oldFullRangeValue,
      double newFullRangeValue, double hysteresis) {
    if (oldFullRangeValue == 0 || newFullRangeValue == 0) {
      return hysteresis;
    }

    return newFullRangeValue * hysteresis / oldFullRangeValue;
  }

  public HighHighLowLow getHihilolo() {
    return hihilolo;
  }
  
  @Override
  public IValidator getValidator() {
    return null;
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml) {
    Preconditions.checkState(xml.thresholdParam.exists(), "threshold parameter not found");
    hihilolo.readParamFromXML(xml.thresholdParam.first());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    hihilolo.writeParamToXML(xml.thresholdParam.append());
  }

  public double getFullRangeValue() {
    return fullRangeValue;
  }

  public double getScaledMaxValue() {
    VirtualPoint vp = getOwnerLogic().getValueInput().getValue();
    if (vp != null && vp instanceof AnaloguePoint) {
      return ((AnaloguePoint) vp).getScaledMax();
    }

    return 0;
  }

  public double getScaledMinValue() {
    VirtualPoint vp = getOwnerLogic().getValueInput().getValue();
    if (vp != null && vp instanceof AnaloguePoint) {
      return ((AnaloguePoint) vp).getScaledMin();
    }

    return 0;
  }

}

