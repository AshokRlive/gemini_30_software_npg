/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;

/**
 * Wizard for adding a control logic.
 */
public class ControlLogicAddingWizard extends WizardBranchController {

  private static final String WIZARD_TITLE = "Add Control Logic Wizard";

  private final HashMap<ICLogicType, Wizard> subWizards = new HashMap<>();
  private final CLogicManager m;
  private final ICLogicGenerator c;


  private ControlLogicAddingWizard(CLogicManager manager, ICLogicGenerator configurator) {
    super(WIZARD_TITLE, new LogicTypeSelectionPage(manager)); // Initial page.
    this.m = manager;
    this.c = configurator;
  }

  @Override
  protected Wizard getWizardForStep(String step, Map settings) {
    if (LogicTypeSelectionPage.ID.equals(step)) {
      ICLogicType type = (ICLogicType) settings.get(LogicTypeSelectionPage.KEY_CLOGIC_TYPE);
      if(type == null)
        return null;
      
      Wizard wiz = subWizards.get(type);

      if (wiz == null) {
        wiz = createSubWizard(type);
        subWizards.put(type, wiz);
      }

      return wiz;
    }

    return null;
  }

  private Wizard createSubWizard(ICLogicType type) {
    ArrayList<WizardPage> subPages = new ArrayList<>();
//    AbstractOptionPage contributors =
//        CLogicFactories.getFactory(type.getId()).createWizardPageForAddingLogic(m, c);
//    if(contributors != null) {
//      subPages.add(contributors);
//    }

    // Add contributor pages
    IControlLogicAddingWizardContributor contr = CLogicFactories.getFactory(type.getId()).getWizardContributor();
    if(contr != null) {
      WizardPage[] pages = contr.getWizardPages(m.getOwner());
      if(pages != null) {
      for (int i = 0; i < pages.length; i++) {
        if(pages[i] != null)
          subPages.add(pages[i]);
      }
      }
    }
    
    // Add OLR page
    if(checkAllowConfigOLR(type)) {
      subPages.add(new OLRConfigPage());
    }
    
    // Add finish page
    FinishPage finish = new FinishPage(type, m, c);
    subPages.add(finish);
    
    return WizardPage.createWizard(subPages.toArray(new WizardPage[subPages.size()]), 
        new ControlLogicAddingResult(m, c, contr));
  }
  
  public static ICLogic showDialog(CLogicManager manager, ICLogicGenerator configurator) {
    Preconditions.checkNotNull(manager, "manager must not be null");

    // create subsequent pages
    WizardBranchController wbc = new ControlLogicAddingWizard(manager, configurator);
    return (ICLogic) WizardDisplayer.showWizard(
        wbc.createWizard(),
        WizardUtils.createRect(WindowUtils.getMainFrame(), 600, 400),
        Helper.createHelpAction(ControlLogicAddingWizard.class),
        null);
  }
  

  private static boolean checkAllowConfigOLR(ICLogicType type){
    return type.requireOLR();
  }
}
