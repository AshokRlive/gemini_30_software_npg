/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.sectionaliser;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SECTIONALISER_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SECTIONALISER_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SECTIONALISER_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Sectionaliser Control Logic.
 */
public final class Sectionaliser extends PredefinedCLogic<SectionaliserSettings> implements ISupportOffLocalRemote {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.SECTIONALISER, "Sectionaliser")
      .desp("Automation - Sectionaliser")
      .allowUserAdd(false)
      .requireOLR(true)
      .get();
  
  private CLogicIOPoint[] inputs;
  private final CLogicIOLogic[] outputs;

  public Sectionaliser() {
    super(TYPE, SECTIONALISER_INPUT.values(), SECTIONALISER_POINT.values());
    SECTIONALISER_OUTPUT[] outputEnums = SECTIONALISER_OUTPUT.values();

    outputs = new CLogicIOLogic[outputEnums.length];
    initOutputs(outputEnums);

    getSettings().setInputType(SectionaliserSettings.INPUT_TYPE_MV);
  }

  private void initOutputs(SECTIONALISER_OUTPUT[] outputEnums) {
    boolean isOptional = false;
    int[] types;
    for (int i = 0; i < outputs.length; i++) {
      if (outputEnums[i] == SECTIONALISER_OUTPUT.SECTIONALISER_OUTPUT_FPI) {
        types = new int[] { ICLogicType.EXT_FPI };
      } else if (outputEnums[i] == SECTIONALISER_OUTPUT.SECTIONALISER_OUTPUT_SW) {
        types = new int[] { ICLogicType.SGL };
      } else {
        types = null;
      }

      outputs[i] = new CLogicIOLogic(this,
          outputEnums[i].getDescription(),
          types, isOptional, true, false, false);
    }
  }
  
  public void setOutputSwitch(IOperableLogic switchLogic) {
    int index = SECTIONALISER_OUTPUT.SECTIONALISER_OUTPUT_SW.ordinal();
    outputs[index].setValue(switchLogic);
  }

  public void setOutputFPI(IOperableLogic fpi) {
    int index = SECTIONALISER_OUTPUT.SECTIONALISER_OUTPUT_FPI.ordinal();
    outputs[index].setValue(fpi);
  }

  public ICLogic getOutputSwitch() {
    int index = SECTIONALISER_OUTPUT.SECTIONALISER_OUTPUT_SW.ordinal();
    return outputs[index].getValue();
  }

  public ICLogic getOutputFPI() {
    int index = SECTIONALISER_OUTPUT.SECTIONALISER_OUTPUT_FPI.ordinal();
    return outputs[index].getValue();
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return Arrays.copyOf(inputs, inputs.length);
  }

  @Override
  public CLogicIO<ICLogic>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }

 void setInputs(CLogicIOPoint[] inputs) {
   this.inputs = inputs;
  }

@Override
protected SectionaliserSettings createSettings() {
  return new SectionaliserSettings(this);
}

CLogicIO<VirtualPoint>[] getOriginalInputs() {
  return super.getInputs();
}
}
