
package com.lucy.g3.rtu.config.clogic.fpitest;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;


public class FPITestLogicFactory extends AbstractClogicFactory {
  public final static FPITestLogicFactory INSTANCE = new FPITestLogicFactory();
  private FPITestLogicFactory(){}
  

  @Override
  public ICLogic createLogic() {
    return new FPITest();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return FPITest.TYPE;
  }
  
 
}

