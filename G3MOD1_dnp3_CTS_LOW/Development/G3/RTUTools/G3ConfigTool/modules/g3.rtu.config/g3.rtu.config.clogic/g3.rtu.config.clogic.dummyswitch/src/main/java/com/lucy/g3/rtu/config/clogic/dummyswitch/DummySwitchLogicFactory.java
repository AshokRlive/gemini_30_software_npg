
package com.lucy.g3.rtu.config.clogic.dummyswitch;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;


public class DummySwitchLogicFactory extends AbstractClogicFactory{
  public final static DummySwitchLogicFactory INSTANCE = new DummySwitchLogicFactory();
  private DummySwitchLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new DummySwitchSettingsEditor((DummySwitchSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new DummySwitch();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return DummySwitch.TYPE;
  }
  
}

