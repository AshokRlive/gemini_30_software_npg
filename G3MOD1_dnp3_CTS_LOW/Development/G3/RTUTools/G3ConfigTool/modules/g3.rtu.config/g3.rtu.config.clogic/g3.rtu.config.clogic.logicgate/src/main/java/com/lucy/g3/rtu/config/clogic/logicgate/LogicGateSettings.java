/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.logicgate;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.LogicGateParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CLOGIC_OFFLINE_BEHAVIOUR;
import com.lucy.g3.xml.gen.common.ControlLogicDef.LOGIC_GATE_OPERATE;

public class LogicGateSettings extends AbstractCLogicSettings<LogicGate> {

  public static final String PROPERTY_OPER = "operator";
  public static final String PROPERTY_OFFLINE_BEHAVIOUR = "offlineBehaviour";

  private LOGIC_GATE_OPERATE operator = LOGIC_GATE_OPERATE.LOGIC_GATE_OPERATE_AND;

  private CLOGIC_OFFLINE_BEHAVIOUR offlineBehaviour = CLOGIC_OFFLINE_BEHAVIOUR.CLOGIC_OFFLINE_BEHAVIOUR_A;


  LogicGateSettings(LogicGate owner) {
    super(owner);
  }

  public LOGIC_GATE_OPERATE getOperator() {
    return operator;
  }

  public void setOperator(LOGIC_GATE_OPERATE operator) {
    if (operator == null) {
      log.error("Operator cannot be set to null");
      return;
    }

    Object oldValue = getOperator();
    this.operator = operator;
    firePropertyChange(PROPERTY_OPER, oldValue, operator);
  }
  
  public void setOfflineBehaviour(CLOGIC_OFFLINE_BEHAVIOUR offlineBehaviour) {
    if (offlineBehaviour == null) {
      log.error("OFFLINE_BEHAVIOUR cannot be set to null");
      return;
    }

    Object oldValue = getOfflineBehaviour();
    this.offlineBehaviour = offlineBehaviour;
    firePropertyChange(PROPERTY_OFFLINE_BEHAVIOUR, oldValue, offlineBehaviour);
  }

  public CLOGIC_OFFLINE_BEHAVIOUR getOfflineBehaviour() {
    return offlineBehaviour;
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    LogicGateParamT xml = xml_param.logicGateParam.first();
    LOGIC_GATE_OPERATE op = LOGIC_GATE_OPERATE.forValue(xml.logicOperate.getEnumerationValue());
    setOperator(op);// operator
    CLOGIC_OFFLINE_BEHAVIOUR offlineBehaviour = CLOGIC_OFFLINE_BEHAVIOUR.forValue(xml.offlineBehaviour.getValue());
    setOfflineBehaviour(offlineBehaviour);
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    LogicGateParamT xml_logicGate = xml.logicGateParam.append();
    xml_logicGate.logicOperate.setEnumerationValue(getOperator().getValue());
    xml_logicGate.offlineBehaviour.setValue(getOfflineBehaviour().getValue());
  }
  

  @Override
  public IValidator getValidator() {
    return null;
  }

}
