/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.util.UUID;

import org.apache.log4j.Logger;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.DefaultCLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.validate.CLogicValidator;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * This abstract class implements the common fields and methods of a control
 * logic.
 */
public abstract class AbstractCLogic<SettingsT extends ICLogicSettings> extends AbstractNode implements ICLogic {

  protected Logger log = Logger.getLogger(getClass());
  
  private final ICLogicType type;
  private final SettingsT settings;
  private final CLogicValidator validator;
  private final CLogicNameUpdater nameUpdater;
  
  private int group = 1;
  private boolean enabled = true;
  private String customName;
  private String uuid = UUID.randomUUID().toString();

  public AbstractCLogic(ICLogicType type) {
    super(NodeType.CLOGIC);
    this.type = type;
    SettingsT s = createSettings();
    this.settings = (SettingsT) (s == null ? new DefaultCLogicSettings(this) : s); 
    this.validator = new CLogicValidator(this);
    this.nameUpdater = new CLogicNameUpdater(this);
    
    setDescription(null);
    
    // Set instance name
    String instanceName = type.getInstanceName();
    if( !Strings.isBlank(instanceName)) {
      setCustomName(instanceName);
    }
    
    // Validate type against class type.
    if (this instanceof IOperableLogic) {
      if (type.isOperable() == false)
        throw new IllegalArgumentException(
            String.format("Invalid type:%s. Expected operable type.", type));
    } else {
      if (type.isOperable() == true)
        throw new IllegalArgumentException(
            String.format("Invalid type:%s. Expected non-operable type.", type));
    }
  }
  
  /** 
   * Creates a settings object for this logic. if null, the default settings will be created.
   * @return 
   */
  protected abstract SettingsT createSettings();

  @Override
  protected final void setName(String name) {
    super.setName(name);
  }

  @Override
  public final String getCustomName() {
    return customName;
  }

  @Override
  public final void setCustomName(String customName) {
    Object oldValue = this.customName;
    this.customName = customName;
    firePropertyChange(PROPERTY_CUSTOM_NAME, oldValue, customName);
  }

  
  @Override
  public final SettingsT getSettings() {
    return settings;
  }
  
  @Override
  public final void readParamFromXML(CLogicParametersT xml) {
    settings.readParamFromXML(xml);
  }

  @Override
  public final void writeParamToXML(CLogicParametersT xml) {
    settings.writeParamToXML(xml);
  }

  @Override
  protected /*final*/ void setDescription(String description) {
    if (Strings.isBlank(description)) {
      description = type.getTypeName();
    }
    super.setDescription(description);
  }
  
  @Override
  public final int getInputNum() {
    return (getInputs() != null) ? getInputs().length : 0;
  }

  @Override
  public int getOutputNum() {
    return (getOutputs() != null) ? getOutputs().length : 0;
  }

  @Override
  public final int getAllPointsNum() {
    return getAllPoints() == null ? 0 : getAllPoints().length;
  }

  @Override
  public String getSourceName() {
    return nameUpdater.getNameAsSource();
  }

  @Override
  public IPseudoPoint getPointByID(int id) {
    IPseudoPoint[] allPoints = getAllPoints();

    for (int i = 0; i < allPoints.length; i++) {
      if (allPoints[i].getId() == id) {
        return allPoints[i];
      }
    }

    return null;
  }

  @Override
  public final ICLogicType getType() {
    return type;
  }

  @Override
  public int getGroup() {
    return group;
  }

  public boolean isOperable() {
    return getType().isOperable();
  }
  
  @Override
  protected final void setAllowDelete(boolean allowDelete) {
    throw new UnsupportedOperationException("Unsupported operation. "
        + "Delete attribute should be set in CLogicType.");
  }
  
  @Override
  public final boolean isAllowDelete() {
    return getType().allowUserRemove();
  }
  
  @Override
  public void setGroup(int group) {
    if (group < 1) {
      log.error("Cannot set group to: " + group + ".  Group must be >= 1 ");
      return;
    }

    int oldValue = getGroup();
    this.group = group;
    firePropertyChange(PROPERTY_GROUP, oldValue, group);

    // Update full name
    updatePointGroup();
  }

  private void updatePointGroup() {
    IPseudoPoint[] points = getAllPoints();
    if (points != null) {
      for (IPseudoPoint p : points) {
        if (p != null)
          p.updateGroup();
      }
    }
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    Module module = getSourceModule();

    if (module != null) {
      return module.getType() == moduleType;
    } else {
      return false;
    }
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return getSourceModule() == module;
  }

  @Override
  public String getHTMLSummary() {

    StringBuffer html = new StringBuffer();
    html.append("<html>");
    html.append("<b>ControlLogic: ");
    html.append(getName());
    html.append("</b><ul>");
    html.append("<li>Group: ");
    html.append(getGroup());
    html.append("</li><li>Type : ");
    html.append(getType().getTypeName());
    html.append("</li><br>");

    html.append("</ul>");
    html.append("</html>");
    return html.toString();

  }

  @Override
  public void delete() {
    // Clear inputs
    if (getInputs() != null) {
      for (CLogicIO<?> input : getInputs()) {
        input.setValue(null);
      }
    }

    // Clear outputs
    if (getOutputs() != null) {
      for (CLogicIO<?> output : getOutputs()) {
        output.setValue(null);
      }
    }

    IPseudoPoint[] points = getAllPoints();
    for (IPseudoPoint p : points) {
      p.delete();
    }

    super.delete();

  }

  @Override
  public final IValidator getValidator() {
    return validator;
  }

  protected final void bindDescriptionToIO(CLogicIO<?> io) {
    nameUpdater.bindDescriptionToIO(io);
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
    // No handler
  }

  @Override
  public boolean isEnabled() {
    return enabled;
  }

  @Override
  public void setEnabled(boolean enabled) {
    Object oldValue = this.enabled;
    this.enabled = enabled;
    firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
  }

  @Override
  public final String getUUID() {
    return uuid;
  }
  
  @Override
  public final void setUUID(String uuid) {
    this.uuid = uuid;
  }
  
  public boolean isSwitchLogic() {
    return type.getOpMode() == OperationMode.OPEN_CLOSE;
  }
}