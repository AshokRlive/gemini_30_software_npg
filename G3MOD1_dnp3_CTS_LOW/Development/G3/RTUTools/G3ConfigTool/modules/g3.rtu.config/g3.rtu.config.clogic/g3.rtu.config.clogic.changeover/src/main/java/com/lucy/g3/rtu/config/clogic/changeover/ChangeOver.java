/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.changeover;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CHANGE_OVER_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CHANGE_OVER_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CHANGE_OVER_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Change Over control logic.
 */
public final class ChangeOver extends PredefinedCLogic<ChangeOverSettings>
    implements ISupportOffLocalRemote {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.CHANGE_OVER, "Change Over")
      .desp("Automation - Change Over")
      .allowUserAdd(false)
      .requireOLR(true)
      .get();

  
  private CLogicIOPoint[] inputs;
  private final CLogicIOLogic[] outputs;


  public ChangeOver() {
    super(TYPE, CHANGE_OVER_INPUT.values(), CHANGE_OVER_POINT.values());
    CHANGE_OVER_OUTPUT[] outputEnums = CHANGE_OVER_OUTPUT.values();

    outputs = new CLogicIOLogic[outputEnums.length];
    initOutputs(outputEnums);
    
    getSettings().setInputType(ChangeOverSettings.INPUT_TYPE_MV);
  }

  private void initOutputs(CHANGE_OVER_OUTPUT[] outputEnums) {
    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new CLogicIOLogic(this,
          outputEnums[i].getDescription(),
          new int[] { ICLogicType.SGL }, // Supported types
          false,
          false,
          true, // Optional
          false);// Enabled
    }
  }

  public CLogicIO<VirtualPoint> getInput(CHANGE_OVER_INPUT inputEnum) {
    return getInputByEnum(inputEnum);
  }

  public void setOutputSwitch1(IOperableLogic switchLogic) {
    int index = CHANGE_OVER_OUTPUT.CHANGE_OVER_OUTPUT_SW1.ordinal();
    outputs[index].setValue(switchLogic);
  }

  public void setOutputSwitch2(IOperableLogic switchLogic) {
    int index = CHANGE_OVER_OUTPUT.CHANGE_OVER_OUTPUT_SW2.ordinal();
    outputs[index].setValue(switchLogic);
  }

  public ICLogic getOutputSwitch1() {
    int index = CHANGE_OVER_OUTPUT.CHANGE_OVER_OUTPUT_SW1.ordinal();
    return outputs[index].getValue();
  }

  public ICLogic getOutputSwitch2() {
    int index = CHANGE_OVER_OUTPUT.CHANGE_OVER_OUTPUT_SW2.ordinal();
    return outputs[index].getValue();
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return Arrays.copyOf(inputs, inputs.length);
  }

  @Override
  public CLogicIO<ICLogic>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInput(CHANGE_OVER_INPUT.CHANGE_OVER_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }

  void setInputs(CLogicIOPoint[] inputs) {
    this.inputs = inputs; 
  }

  @Override
  protected ChangeOverSettings createSettings() {
    return new ChangeOverSettings(this);
  }

  CLogicIO<VirtualPoint>[] getOriginalInputs() {
    return super.getInputs();
  }

}
