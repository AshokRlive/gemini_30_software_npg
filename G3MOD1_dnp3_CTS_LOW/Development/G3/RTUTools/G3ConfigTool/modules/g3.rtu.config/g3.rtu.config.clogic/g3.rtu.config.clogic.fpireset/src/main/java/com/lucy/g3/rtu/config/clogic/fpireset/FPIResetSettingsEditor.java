/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fpireset;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;


class FPIResetSettingsEditor extends AbstractCLogicSettingsEditor {

  public FPIResetSettingsEditor(FPIResetSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    // Nothing to validate
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    JCheckBox comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(FPIResetSettings.PROPERTY_RST_ON_LV_RESTORED),
        "Reset FPI when LV is restored");
    builder.append("", comp);
    builder.nextLine();
    
    comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(FPIResetSettings.PROPERTY_RST_ON_LV_LOST),
        "Reset FPI when LV is lost");
    builder.append("", comp);
    builder.nextLine();
    
    return builder.getPanel();
  }

}

