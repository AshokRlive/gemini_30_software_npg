/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.prompt.PromptSupport;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.Sizes;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.util.DefaultValidationResultModel;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;


/**
 * A dialog for configuring CLogic settings.
 */
public class CLogicSettingsDialog extends AbstractDialog{
  private final PresentationModel<ICLogic> pm;
  private final ICLogicSettingsEditor editor;
  private Window parent;
  public CLogicSettingsDialog(Window parent, ICLogic logic) {
    super(parent);
    Preconditions.checkNotNull(logic, "logic must not be null!");
    this.pm = new PresentationModel<>(logic);
    this.parent = parent;
    this.editor = CLogicFactories.getFactory(logic.getType().getId()).createEditor(logic.getSettings());
    
    setName(pm.getBean().getClass().getName());
    setTitle(String.format("[%d] %s Settings", logic.getGroup(), logic.getType().getTypeName()));
    setMinimumSize(new Dimension(400, 120));


    setInitFocusedComponent(this);
    pack();
    ValidationComponentUtils.updateComponentTreeMandatoryBackground(this);
  }

  @Override
  public void ok() {
    ValidationResult result = doValidate();
    
    DefaultValidationResultModel model = new DefaultValidationResultModel();
    model.setResult(result);
    if (model.hasErrors()) {
      BufferValidator.showErrorReport(parent, model);
      return;
    } 
    
    commit();
    release();
    super.ok();
    
    if(editor != null)
    editor.postCommitAction();
  }

  @Override
  public void cancel() {
    triggerflush();
    release();
    super.cancel();
  }
  
  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    final JPanel settingsPanel = new JPanel(new BorderLayout(0,10));
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout(
        "right:[80dlu,default], $lcgap, [50dlu,default]:grow, $lcgap,default, $lcgap,default",
        "" // dynamic rows
        )
    );
    
    builder.setLineGapSize(Sizes.DLUY5);
    
    // Name
    JTextField nameField = new JTextField();
    Bindings.bind(nameField, pm.getBufferedModel(ICLogic.PROPERTY_CUSTOM_NAME));
    builder.append("Logic Name:", nameField);
    PromptSupport.setPrompt("Optional", nameField);
    builder.nextLine();

    // Group
//    JFormattedTextField groupField = BasicComponentFactory.createFormattedTextField(
//        pm.getBufferedModel(ICLogic.PROPERTY_GROUP),
//        UIUtils.createUnsigIntFactory());
//    groupField.setEnabled(false); // group cannot be changed.
//    builder.append("Group:", groupField);
//    builder.nextLine();

    settingsPanel.add(builder.getPanel(), BorderLayout.NORTH);
    if(editor != null)
    settingsPanel.add(editor.getComponent(CLogicSettingsDialog.this), BorderLayout.CENTER);
    
    // Enable option
    final JCheckBox enable = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(ICLogic.PROPERTY_ENABLED),"Enable Logic");
    enable.setHorizontalAlignment(SwingConstants.CENTER);
    enable.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        Point location = CLogicSettingsDialog.this.getLocation();
        settingsPanel.setVisible(enable.isSelected());
        CLogicSettingsDialog.this.pack();
        CLogicSettingsDialog.this.revalidate();
        CLogicSettingsDialog.this.setLocation(location);
        CLogicSettingsDialog.this.repaint();
      }
    });
    settingsPanel.setVisible(enable.isSelected());

    // Create content panel
    JPanel contentPanel = new JPanel(new BorderLayout(10,10));
    contentPanel.add(enable, BorderLayout.NORTH);
    contentPanel.add(settingsPanel, BorderLayout.CENTER);
    return contentPanel;
  }
  
  @Override
  protected JPanel createButtonPanel() {
    return createOKCancelButtonPanel();
  }
  
  private void commit() {
    pm.triggerCommit();
    
    if(editor != null)
      editor.triggerCommit();
  }

  private ValidationResult doValidate() {
    ValidationResult result = new ValidationResult();
    
    if(editor != null)
      editor.validate(result);
    
    ValidationComponentUtils.updateComponentTreeSeverityBackground(this, result);
    return result;
  }

  private void triggerflush() {
    pm.triggerFlush();
    if(editor != null)
      editor.triggerFlush();
  }

  private void release() {
    pm.release();
    if(editor != null)
      editor.release();
  }
}

