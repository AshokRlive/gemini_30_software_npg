/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.pltu;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.PLTUParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;


public class PLTULogicSettings extends AbstractCLogicSettings<PLTULogic> {
  public static final String PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC = "voltageMismatchDelaySec";

  private int voltageMismatchDelaySec = 5; // seconds
  
  PLTULogicSettings(PLTULogic owner) {
    super(owner);
    // TODO Auto-generated constructor stub
  }
  public void setvoltageMismatchDelaySec(int voltageMismatchDelaySec) {
    if (voltageMismatchDelaySec < 0) {
      log.warn("Cannot set voltageMismatchDelaySec to: " + voltageMismatchDelaySec);
      return;
    }
    Object oldValue = getVoltageMismatchDelaySec();
    this.voltageMismatchDelaySec = voltageMismatchDelaySec;
    firePropertyChange(PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC, oldValue, voltageMismatchDelaySec);
  }

  public int getVoltageMismatchDelaySec() {
    return voltageMismatchDelaySec;
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    PLTUParamT xml = xml_param.pltuParam.first();
    setvoltageMismatchDelaySec((int) xml.voltageMismatchDelaySec.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    PLTUParamT xml_pltu = xml.pltuParam.append();
    xml_pltu.voltageMismatchDelaySec.setValue(getVoltageMismatchDelaySec());
  }


  @Override
  public IValidator getValidator() {
    return null;
  }

}

