/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.commons.io.FileUtils;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.dialogs.InfoDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;

public class IEC61499LogicSettingsEditor extends AbstractCLogicSettingsEditor {


  public IEC61499LogicSettingsEditor(IEC61499LogicSettings settings) {
    super(settings);
  }

  @Override
  public JComponent getComponent(final JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();

    JComponent comp;

    // Append "Auto Start"
    comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(IEC61499LogicSettings.PROPERTY_AUTO_START), "Auto Start");
    builder.append("", comp);
    builder.nextLine();

    // Append "Resource name" field
    final BufferedValueModel libResVM = pm.getBufferedModel(IEC61499LogicSettings.PROPERTY_RES_NAME);
    comp = BasicComponentFactory.createTextField(libResVM);
    // Read only resource name if it is imported from 4DIAC system file
    comp.setEnabled(!((IEC61499Logic)pm.getBean().getOwnerLogic()).isImported());
    builder.append("Resource Name:", comp);
    builder.nextLine();

    // Append "Scheme Version" field
    ValueModel ValueModel = pm.getBufferedModel(IEC61499LogicSettings.PROPERTY_AUTO_SCHEME_VERSION);
    comp = BasicComponentFactory.createLabel(ValueModel, IEC61499LogicSettings.getVersionFormat());
    builder.append("Scheme Version:", comp);
    builder.nextLine();
    
    builder.appendParagraphGapRow();
    builder.nextLine();
    builder.appendSeparator("Automation Boot File");
    comp = new JButton("Import fboot...");
    builder.append("", comp);
    builder.nextLine();
    ((JButton)comp).addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        File fbootFile = IEC61499LogicAddingWizard.chooseFile(parent,
            "Choose a 61499 boot file(*.fboot)", "fboot");
        if(fbootFile != null) {
          try {
            pm.setBufferedValue(IEC61499LogicSettings.PROPERTY_SCHEME, FileUtils.readFileToString(fbootFile));
            MessageDialogs.finish(parent, "The fboot has been imported.");
            
          } catch (IOException e1) {
            MessageDialogs.error(parent, "Cannot import fboot file", e1);
          }
        }
      }
      
    });
    
    // Append view scheme button
    comp = new JButton("View fboot...");
    builder.append("", comp);
    builder.nextLine();
    ((JButton)comp).addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String scheme = pm.getBufferedValue(IEC61499LogicSettings.PROPERTY_SCHEME).toString();
        JTextArea ta = new JTextArea(scheme);
        ta.setWrapStyleWord(true);
        ta.setEditable(false);
        JScrollPane sc = new JScrollPane(ta);
        sc.setPreferredSize(new Dimension(500, 400));
        InfoDialog dialog = new InfoDialog(parent,true,
            "IEC61499 Boot File", sc, null);
        dialog.pack();
        dialog.setVisible(true);
      }
    });

    return builder.getPanel();
  }

  @Override
  public void validate(ValidationResult result) {
    // Nothing to validate
  }

}
