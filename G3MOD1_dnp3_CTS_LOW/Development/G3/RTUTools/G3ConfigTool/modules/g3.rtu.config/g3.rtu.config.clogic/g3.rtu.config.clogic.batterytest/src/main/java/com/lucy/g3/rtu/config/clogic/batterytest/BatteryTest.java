/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.batterytest;

import java.util.Arrays;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_BCHARGER;
import com.lucy.g3.xml.gen.common.ControlLogicDef.BATTERY_CHARGER_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.BATTERY_CHARGER_OUTPUT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Battery charger control logic.
 */
public final class BatteryTest extends PredefinedCLogic<BatteryTestSettings> implements ISupportOffLocalRemote, IOperableLogic {

  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.BAT_CHARGER, "Battery Charger Test")
      .desp("The control logic for battery charger test")
      .opMode(OperationMode.START_STOP)
      .maxAmount(5)
      .requireOLR(true)
      .get();

  private final CLogicIOChannel[] outputs;

  public BatteryTest() {
    this(null);
  }

  public BatteryTest(IChannel batteryTestChannel) {
    super(TYPE, BATTERY_CHARGER_INPUT.values(), null);
    outputs = new CLogicIOChannel[BATTERY_CHARGER_OUTPUT.values().length];

    initOutputs();

    setOutputChannel(batteryTestChannel);

    bindDescriptionToIO(outputs[0]);
  }

  private void initOutputs() {
    String name;
    BATTERY_CHARGER_OUTPUT[] outputEnums = BATTERY_CHARGER_OUTPUT.values();
    for (int i = 0; i < outputs.length; i++) {
      name = outputEnums[i].getDescription();
      outputs[i] = new CLogicIOChannel(this,
          name,
          false,
          true,
          new ChannelType[] { ChannelType.PSM_CH_BCHARGER },
          new MODULE[] { MODULE.MODULE_PSM },
          null);
    }
  }

  /**
   * Get the output channel of FanLogic.
   */
  public CLogicIO<IChannel> getOutputChannel() {
    int index = BATTERY_CHARGER_OUTPUT.BATTERY_CHARGER_OUTPUT_CHANNEL.ordinal();
    return outputs[index];
  }

  public void setOutputChannel(IChannel ch) {
    if (ch != null && ch.getType() != ChannelType.PSM_CH_BCHARGER) {
      log.error("Unacceptable Battery Charger channel. Invalid type:"
          + ch.getType());
      return;
    }

    int index = BATTERY_CHARGER_OUTPUT.BATTERY_CHARGER_OUTPUT_CHANNEL.ordinal();
    outputs[index].setValue(ch);
  }

  @Override
  public CLogicIO<IChannel>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInput(BATTERY_CHARGER_INPUT.BATTERY_CHARGER_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  public CLogicIO<VirtualPoint> getInput(BATTERY_CHARGER_INPUT inputItem) {
    return super.getInputByEnum(inputItem);
  }

  @Override
  public Module getSourceModule() {
    IChannel ch = getOutputChannel().getValue();
    if (ch != null) {
      return ch.getOwnerModule();
    } else {
      return null;
    }
  }

  public boolean isConfiguredAsBatteryTest() {
    IChannel output = getOutputChannel().getValue();
    if (output != null && output instanceof IChannel) {
      return output.getEnum() == PSM_CH_BCHARGER.PSM_CH_BCHARGER_BATTERY_TEST;
    }
    return false;
  }

  @Override
  public boolean isSwitchLogic() {
    return isConfiguredAsBatteryTest();
  }

  @Override
  protected BatteryTestSettings createSettings() {
    return new BatteryTestSettings(this);
  }
}
