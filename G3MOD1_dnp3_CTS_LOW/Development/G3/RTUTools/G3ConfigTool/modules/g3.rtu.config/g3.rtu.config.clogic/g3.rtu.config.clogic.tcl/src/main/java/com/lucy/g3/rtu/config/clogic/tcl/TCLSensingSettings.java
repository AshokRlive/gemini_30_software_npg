
package com.lucy.g3.rtu.config.clogic.tcl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.JComponent;

import org.apache.log4j.Logger;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_tcl.AnalogueMonitoringModeT;
import com.g3schema.ns_tcl.TCLSensingT;
import com.g3schema.ns_tcl.TimeMonitoringModeT;
import com.g3schema.ns_tcl.TimeSlotT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_POWER_DIRECTION;
import com.lucy.g3.xml.gen.common.ControlLogicDef.TCL_SENSING_TRIPPING_MODE;

public class TCLSensingSettings extends AbstractCLogicSettings<TCLSensingLogic> implements ICLogicSettings {

  private long triggerTimer = 3600;
  private long badioTimer = 3600;
  private TrippingMode trippingMode = TrippingMode.TIME_MOINTORING;

  private final TCLPowerSettings cbTripSettings = new TCLPowerSettings();

  private Date summerStart;
  private Date winterStart;

  private final TimeSlot[][] timeslots;
  private ICLogic logic;
  
  private final Logger log = Logger.getLogger(TCLSensingLogic.class);
  
  private final static SimpleDateFormat DAYMONTH_FORMAT = new SimpleDateFormat("dd/MM");
  private final static SimpleDateFormat HOURMINUTE_FORMAT = new SimpleDateFormat("HH:mm");

  static {
    DAYMONTH_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    HOURMINUTE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
  }
  
  public TCLSensingSettings(TCLSensingLogic logic) {
    super(logic);
    this.logic = logic;
    timeslots = new TimeSlot[TimeSlotId.values().length][TIME_SLOT_NUM];
    for (int i = 0; i < timeslots.length; i++) {
      for (int j = 0; j < timeslots[i].length; j++) {
        timeslots[i][j] = new TimeSlot();
      }
    }

    try {
      summerStart = DAYMONTH_FORMAT.parse("26/03");
      winterStart = DAYMONTH_FORMAT.parse("29/10");
    } catch (ParseException e) {
      summerStart = new Date();
      winterStart = new Date();
      e.printStackTrace();
    }
  }

  public ICLogic getLogic(){
    return logic;
  }
  
  
  public TCLPowerSettings getCbTripSettings() {
    return cbTripSettings;
  }

  public TimeSlot getTimeSlot(TimeSlotId id, int index) {
    return timeslots[id.ordinal()][index];
  }

  public TimeSlot[] getTimeSlot(TimeSlotId id) {
    return timeslots[id.ordinal()];
  }

  public long getTriggerTimer() {
    return triggerTimer;
  }

  public void setTriggerTimer(long triggerTimer) {
    Object oldValue = this.triggerTimer;
    this.triggerTimer = triggerTimer;
    firePropertyChange("triggerTimer", oldValue, triggerTimer);
  }

  public long getBadioTimer() {
    return badioTimer;
  }

  public void setBadioTimer(long badioTimer) {
    Object oldValue = this.badioTimer;
    this.badioTimer = badioTimer;
    firePropertyChange("badioTimer", oldValue, badioTimer);
  }

  public TrippingMode getTrippingMode() {
    return trippingMode;
  }
  
  public void setTrippingMode(TrippingMode trippingMode) {
    Object oldValue = this.trippingMode;
    this.trippingMode = trippingMode;
    firePropertyChange("trippingMode", oldValue, trippingMode);
  }

  

  public Date getSummerStart() {
    return summerStart;
  }
  

  public void setSummerStart(Date summerStart) {
    Object oldValue = this.summerStart;
    this.summerStart = summerStart;
    firePropertyChange("summerStart", oldValue, summerStart);
  }

  public Date getWinterStart() {
    return winterStart;
  }
  
  public void setWinterStart(Date winterStart) {
    Object oldValue = this.winterStart;
    this.winterStart = winterStart;
    firePropertyChange("winterStart", oldValue, winterStart);
  }

  @Override
  public void readParamFromXML(CLogicParametersT _xml) {
    if(!_xml.tclSensing.exists()) {
      log.error("TCLSensing settings not found in XML");
      return;
    }
    
    TCLSensingT xml = _xml.tclSensing.first();
    
    setTriggerTimer(xml.triggerTimer.getValue());
    setBadioTimer(xml.badIoTimer.getValue());
    
    TrippingMode mode = TrippingMode.forValue((int)xml.trippingMode.getValue());
    if(mode != null)
      setTrippingMode(mode);
    else
      log.error("Failed to pase tripping mode");
    
    AnalogueMonitoringModeT xmlAMode = xml.analogueMonitoringMode.first();
    cbTripSettings.setPowerDirection(PowerFlowDirection.forValue((int)xmlAMode.cbAnalogueTripDirection.getValue()));
    cbTripSettings.setPowerValue(xmlAMode.cbAnalogueTripValue.getValue());
    
    TimeMonitoringModeT xmlTMode = xml.timeMonitoringMode.first();
    try {
      setSummerStart(DAYMONTH_FORMAT.parse(xmlTMode.summerStartDate.getValue()));
    } catch (ParseException e) {
      log.error("Failed to parse start date: " + e.getMessage());
    }
    
    try {
      setWinterStart(DAYMONTH_FORMAT.parse(xmlTMode.winterStartDate.getValue()));
    } catch (ParseException e) {
      log.error("Failed to parse end date: " + e.getMessage());
    }
    
    
    TimeSlot[] slots= getTimeSlot(TimeSlotId.SummerWeekdays);
    for (int i = 0; i < slots.length && i <xmlTMode.summerWeekdays.count(); i++) {
      readTimeSlotToXML(slots[i], xmlTMode.summerWeekdays.at(i));
    }
    
    slots= getTimeSlot(TimeSlotId.SummerWeekends);
    for (int i = 0; i < slots.length && i <xmlTMode.summerWeekends.count(); i++) {
      readTimeSlotToXML(slots[i], xmlTMode.summerWeekends.at(i));
    }
    
    slots= getTimeSlot(TimeSlotId.WinterWeekdays);
    for (int i = 0; i < slots.length && i <xmlTMode.winterWeekdays.count(); i++) {
      readTimeSlotToXML(slots[i], xmlTMode.winterWeekdays.at(i));
    }
    
    slots= getTimeSlot(TimeSlotId.WinterWeekends);
    for (int i = 0; i < slots.length && i <xmlTMode.winterWeekends.count(); i++) {
      readTimeSlotToXML(slots[i], xmlTMode.winterWeekends.at(i));
    }
  }

  private void readTimeSlotToXML(TimeSlot timeSlot, TimeSlotT xml) {
    timeSlot.setEnabled(xml.enabled.getValue());
    
    try {
      timeSlot.setStartTime(HOURMINUTE_FORMAT.parse(xml.startTime.getValue()));
    } catch (ParseException e) {
      log.error("Failed to parse start time:"+e.getMessage());
    }
    
    try {
      timeSlot.setEndTime(HOURMINUTE_FORMAT.parse(xml.endTime.getValue()));
    } catch (ParseException e) {
      log.error("Failed to parse end time:"+e.getMessage());
    }
    
    PowerFlowDirection dir = PowerFlowDirection.forValue((int) xml.powerDirection.getValue());
    if(dir != null)
      timeSlot.setPowerFlowDirection(dir);
    else
      log.error("Failed to pase power flow direction");
    timeSlot.setPowerFlowValue(xml.powerValue.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {

    TCLSensingT xmlSensing = xml.tclSensing.append();
    xmlSensing.triggerTimer.setValue(getTriggerTimer());
    xmlSensing.badIoTimer.setValue(getBadioTimer());
    xmlSensing.trippingMode.setValue(getTrippingMode().getValue());
    
    AnalogueMonitoringModeT xmlAMode = xmlSensing.analogueMonitoringMode.append();
    xmlAMode.cbAnalogueTripDirection.setValue(cbTripSettings.getPowerDirection().getValue());
    xmlAMode.cbAnalogueTripValue.setValue(cbTripSettings.getPowerValueAsDouble());
    
    TimeMonitoringModeT xmlTMode = xmlSensing.timeMonitoringMode.append();
    xmlTMode.summerStartDate.setValue(DAYMONTH_FORMAT.format(getSummerStart()));
    xmlTMode.winterStartDate.setValue(DAYMONTH_FORMAT.format(getWinterStart()));
    
    TimeSlot[] slots= getTimeSlot(TimeSlotId.SummerWeekdays);
    for (int i = 0; i < slots.length; i++) {
      writeTimeSlotToXML(slots[i], xmlTMode.summerWeekdays.append());
    }
    
    slots= getTimeSlot(TimeSlotId.SummerWeekends);
    for (int i = 0; i < slots.length; i++) {
      writeTimeSlotToXML(slots[i], xmlTMode.summerWeekends.append());
    }
    
    slots= getTimeSlot(TimeSlotId.WinterWeekdays);
    for (int i = 0; i < slots.length; i++) {
      writeTimeSlotToXML(slots[i], xmlTMode.winterWeekdays.append());
    }
    
    slots= getTimeSlot(TimeSlotId.WinterWeekends);
    for (int i = 0; i < slots.length; i++) {
      writeTimeSlotToXML(slots[i], xmlTMode.winterWeekends.append());
    }
  }


  private void writeTimeSlotToXML(TimeSlot timeSlot, TimeSlotT xml) {
    xml.enabled.setValue(timeSlot.isEnabled());
    xml.startTime.setValue(HOURMINUTE_FORMAT.format(timeSlot.getStartTime()));
    xml.endTime.setValue(HOURMINUTE_FORMAT.format(timeSlot.getEndTime()));
    xml.powerDirection.setValue(timeSlot.getPowerFlowDirection().getValue());
    xml.powerValue.setValue(timeSlot.getPowerValueAsDouble());
  }


  public static enum TrippingMode {
    CURRENT_MONITORING(TCL_SENSING_TRIPPING_MODE.TCL_SENSING_TRIPPING_MODE_ANALOGUE_MOINTOR), 
    TIME_MOINTORING(TCL_SENSING_TRIPPING_MODE.TCL_SENSING_TRIPPING_MODE_TIME_MONITOR);

    TrippingMode(TCL_SENSING_TRIPPING_MODE source) {
      this.source = source;
    }
    
    public int getValue() {
      return source.getValue();
    }
    

    public String toString() {
      return source.getDescription();
    }

    public static TrippingMode forValue(int value){
      TCL_SENSING_TRIPPING_MODE source = TCL_SENSING_TRIPPING_MODE.forValue(value);
      TrippingMode[]values =TrippingMode.values();
      for (int i = 0; i < values.length; i++) {
        if(values[i].source == source)
          return values[i];
      }
      return null;
    }

    private final TCL_SENSING_TRIPPING_MODE source;
  }

  public static enum PowerFlowDirection {
    LOAD("+ve", "Load",TCL_POWER_DIRECTION.TCL_POWER_DIRECTION_LOAD), 
    GENERATION("-ve", "Generation",TCL_POWER_DIRECTION.TCL_POWER_DIRECTION_GENERATION), 
    STORAGE("±ve", "Storage",TCL_POWER_DIRECTION.TCL_POWER_DIRECTION_STORAGE);

    PowerFlowDirection(String symbol, String description, TCL_POWER_DIRECTION source) {
      this.symbol = symbol;
      this.description = description;
      this.source = source;
    }

    public String toString() {
      return getSymbol();
    }

    public String getSymbol() {
      return symbol;
    }

    public String getDescription() {
      return description;
    }


    private final String symbol;
    private final String description;
    private final TCL_POWER_DIRECTION source;
    
    
    public int getValue() {
      return source.getValue();
    }
    
    public static PowerFlowDirection forValue(int value){
      TCL_POWER_DIRECTION source = TCL_POWER_DIRECTION.forValue(value);
      PowerFlowDirection[]values =PowerFlowDirection.values();
      for (int i = 0; i < values.length; i++) {
        if(values[i].source == source)
          return values[i];
      }
      return null;
    }
  }


  public static final int TIME_SLOT_NUM = 4;


  public static enum TimeSlotId {
    SummerWeekdays, SummerWeekends, WinterWeekdays, WinterWeekends
  }

  public static class TimeSlot {
    private boolean enabled;
    private Date startTime;
    private Date endTime;

    private final TCLPowerSettings power  = new TCLPowerSettings();

    public TimeSlot() {
      try {
        startTime = HOURMINUTE_FORMAT.parse("00:00");
        endTime = HOURMINUTE_FORMAT.parse("01:59");
      } catch (Exception e) {
        startTime = new Date();
        endTime = new Date();
      }
    }
    
    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }

    public Date getStartTime() {
      return startTime;
    }
    
    public void setStartTime(Date startTime) {
      this.startTime = startTime;
    }

    public Date getEndTime() {
      return endTime;
    }

    public void setEndTime(Date endTime) {
      this.endTime = endTime;
    }

    public float getPowerValueAsFloat() {
      return power.getPowerValueAsFloat();
    }
    
    public double getPowerValueAsDouble() {
      return power.getPowerValueAsDouble();
    }

    public void setPowerFlowValue(double powerFlowValue) {
      power.setPowerValue(powerFlowValue);
    }
    
    public void setPowerFlowValue(float powerFlowValue) {
      power.setPowerValue(powerFlowValue);
    }

    public PowerFlowDirection getPowerFlowDirection() {
      return power.getPowerDirection();
    }

    public void setPowerFlowDirection(PowerFlowDirection powerFlowDirection) {
      power.setPowerDirection(powerFlowDirection);
    }
  }

  @Override
  public IValidator getValidator() {
    // TODO Auto-generated method stub
    return null;
  }
}