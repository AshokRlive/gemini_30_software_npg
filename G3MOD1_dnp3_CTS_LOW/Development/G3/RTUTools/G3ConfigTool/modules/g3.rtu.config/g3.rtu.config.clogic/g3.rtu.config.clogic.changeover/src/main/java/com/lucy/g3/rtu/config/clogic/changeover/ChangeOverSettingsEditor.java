/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.changeover;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRadioButton;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;


class ChangeOverSettingsEditor extends AbstractCLogicSettingsEditor {

  /**
   * 
   */
  private static final String LABEL_SWITCH_OFF_DELAY = "Switch Off Delay";
  /**
   * 
   */
  private static final String LABEL_SWITCH_CHANGE_DELAY = "Switch Change Delay";

  public ChangeOverSettingsEditor(ChangeOverSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result, pm, ChangeOverSettings.PROPERTY_SWITCHCHANGE_DELAY, LABEL_SWITCH_CHANGE_DELAY);
    checkNotNegative(result, pm, ChangeOverSettings.PROPERTY_SWITCHOFF_DELAY, LABEL_SWITCH_OFF_DELAY);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    ValueModel inputTypeVM = pm.getBufferedModel(ChangeOverSettings.PROPERTY_INPUT_TYPE);
    JRadioButton rbInputLV = new JRadioButton("Low");
    JRadioButton rbInputMV = new JRadioButton("Medium");
    Bindings.bind(rbInputLV, inputTypeVM, ChangeOverSettings.INPUT_TYPE_LV);
    Bindings.bind(rbInputMV, inputTypeVM, ChangeOverSettings.INPUT_TYPE_MV);
    builder.append("Voltage Sensor:", rbInputLV);
    builder.nextLine();
    builder.append("", rbInputMV);
    builder.nextLine();

    createLongField(LABEL_SWITCH_CHANGE_DELAY, ChangeOverSettings.PROPERTY_SWITCHCHANGE_DELAY, Units.SECS);
    createLongField(LABEL_SWITCH_OFF_DELAY, ChangeOverSettings.PROPERTY_SWITCHOFF_DELAY, Units.SECS);

    JCheckBox comp0 = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(ChangeOverSettings.PROPERTY_DEFAULT_ENABLED),
        "Default Enabled");
    builder.append("", comp0);
    
    return builder.getPanel();
  }

}

