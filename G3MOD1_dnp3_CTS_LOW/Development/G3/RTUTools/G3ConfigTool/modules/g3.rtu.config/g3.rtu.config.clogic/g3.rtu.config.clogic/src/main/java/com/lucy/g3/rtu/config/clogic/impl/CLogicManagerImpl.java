/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.g3schema.ns_clogic.ControlLogicsT;
import com.jgoodies.common.base.Strings;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicFinder;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.clogic.validate.CLogicManagerValidator;
import com.lucy.g3.rtu.config.clogic.xml.CLogicXmlReader;
import com.lucy.g3.rtu.config.clogic.xml.CLogicXmlWriter;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.manager.AbstractListConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_POINT;

/**
 * <p>
 * Implementation of Control Logic Manager.
 * </p>
 * <p>
 * All control logic is managed in a list model so they can be observed by GUI
 * layer. The list model can be obtained using {@linkplain #getItemListModel()}.
 * </p>
 * <p>
 * For all control logic in this manager, their group value is supposed to be
 * unique and sequential, so when a control logic is added or removed, all other
 * control logic's group value will be updated to keep them consistent.
 * </p>
 * <p>
 * The points inside a control logic will be added to the existing
 * {@code PointsManager} so they can be accessed via the {@code PointsManager}'s
 * common API. {@code PointsManager}.
 * </p>
 */
public final class CLogicManagerImpl extends AbstractListConfigModule<ICLogic> implements CLogicManager {

  private final IContainerValidator validator;

  private final LogicPointListModelListener logicPointListener = new LogicPointListModelListener();
  
  /**
   * Constructor.
   *
   * @param pointsManager
   *          the existing points manager, must not be null.
   */
  public CLogicManagerImpl(IConfig owner/*VirtualPointManager pointsManager*/) {
    super(owner);

    validator = new CLogicManagerValidator(this);
  }


  @Override
  public boolean allowToAdd(ICLogic clogic) throws AddItemException {
    int maxNum = clogic.getType() == null ? 0 : clogic.getType().getMaxAmount();
    int currentNum = getLogicByType(clogic.getType().getId()).size();
    if (currentNum >= maxNum) {
      throw new AddItemException("Not allowed to add more "
          + clogic.getType().getTypeName() + ".");
    }

    return true;
  }

  @Override
  public boolean allowToRemove(ICLogic item) {
    return item == null || item.isAllowDelete(); // not allow to remove OLR
  }

  @Override
  protected void postAddAction(ICLogic clogic) {
    updateGroup();
    addLogicPointsToPointManager(clogic);
  }

  @Override
  protected void postRemoveAction(ICLogic clogic) {
    clogic.delete();
    updateGroup();
    removeLogicPointsFromPointManager(clogic);
  }

  @Override
  public boolean checkCustomNameExisting(String customName) {
    if (Strings.isBlank(customName)) {
      return false;
    }

    Collection<ICLogic> clogics = getAllItems();
    for (ICLogic cl : clogics) {
      if (EqualsUtil.areEqual(cl.getCustomName(), customName)) {
        return true;
      }
    }

    return false;
  }

  @Override
  public <T extends ICLogic> Collection<T> getAllControlLogics(Class<T> logicClass) {
    ArrayList<T> foundLogic = new ArrayList<T>();

    Collection<ICLogic> clogics = getAllItems();
    for (ICLogic logic : clogics) {
      if (logic.getClass() == logicClass) {
        foundLogic.add(logicClass.cast(logic));
      }
    }

    return foundLogic;
  }

  /**
   * Get local remote logic.
   *
   * @return local remote logic. Null if there is no local remote logic being
   *         configured.
   */
  private PseudoDoubleBinaryPoint olrStatus;
  @Override
//  public OffLocalRemote getOLR() {
//    Iterator<ICLogic> it = CLogicUtility.findCLogicByType(getAllClogic(), ICLogicType.OLR).iterator();
//    return (OffLocalRemote)(it.hasNext() ? it.next() : null);
//  }
  public PseudoDoubleBinaryPoint getOLRStatus() {
    if(olrStatus == null) {
      List<ICLogic> olrs = getLogicByType(ICLogicType.OLR);
      if(!olrs.isEmpty()) {
        ICLogic olr = olrs.get(0);
        olrStatus = CLogicUtility
          .findDoubleBinaryPoint(olr.getAllPoints(), OLR_POINT.OLR_POINT_OFFLOCALREMOTE);
      }
    }
    return olrStatus;
  }

  @Override
  public ICLogic getLogicByGroup(int group) {
    Collection<ICLogic> items = getAllItems();
    for (ICLogic clogic : items) {
      if (clogic.getGroup() == group) {
        return clogic;
      }
    }
    return null;
  }

  @Override
  public List<IOperableLogic> getOperableCLogic() {
    ArrayList<IOperableLogic> logicsInType = new ArrayList<IOperableLogic>();
    Collection<ICLogic> items = getAllItems();
    for (ICLogic logic : items) {
      if (logic != null && logic instanceof IOperableLogic) {
        logicsInType.add((IOperableLogic) logic);
      }
    }
    return logicsInType;
  }

  @Override
  public List<ICLogic> getLogicByType(int... typeIds) {
    return new ArrayList<ICLogic>(CLogicFinder.findCLogicByType(getAllItems(), typeIds));
  }

  /**
   * Add all pseudo points of this control logic into point manager.
   */
  private void addLogicPointsToPointManager(ICLogic clogic) {
    VirtualPointManager pointsManager = getVirtualPointsManager();
    
    // add new logic's points to points manager
    IPseudoPoint[] points = clogic.getAllPoints();
    if (points != null && points.length > 0 && pointsManager != null) {
      pointsManager.addAllPseudoPoints(Arrays.asList(points));
    }
    
    ICLogicPointsManager logicPointMgr = clogic.getPointsManager();
    if (logicPointMgr != null) {
      logicPointMgr.addObserver(logicPointListener);
    }
  }
  
  @Override
  public VirtualPointManager getVirtualPointsManager() {
    IConfig owner = getOwner();
    return (VirtualPointManager) (owner == null ? null : owner.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID));
  }
  /**
   * Delete all pseudo points of this control logic from point manager.
   */
  private void removeLogicPointsFromPointManager(ICLogic clogic) {
    VirtualPointManager pointsManager = getVirtualPointsManager();

    IPseudoPoint[] points = clogic.getAllPoints();
    if (points != null && points.length > 0 && pointsManager != null) {
      pointsManager.removeAllPseudoPoints(Arrays.asList(points));
    }
    
    ICLogicPointsManager logicPointMgr = clogic.getPointsManager();
    if (logicPointMgr != null) {
      logicPointMgr.removeObserver(logicPointListener);
    }
  }

  private class LogicPointListModelListener implements ICLogicPointsManager.ICLogicPointsManagerObserver {
    private Logger log = Logger.getLogger(CLogicManagerImpl.LogicPointListModelListener.class);
    
  

    @Override
    public void pointsAdded(Collection<IPseudoPoint> addedItems) {
      VirtualPointManager pointsManager = getVirtualPointsManager();

      if (pointsManager == null)
        return;
      
      pointsManager.addAllPseudoPoints(addedItems);
    }

    @Override
    public void pointsRemoved(Collection<IPseudoPoint> removedItems) {
      VirtualPointManager pointsManager = getVirtualPointsManager();

      if (pointsManager == null)
        return;
      
      pointsManager.removeAllPseudoPoints(removedItems);    
    }
  }

  /**
   * Refresh all control logics' group value with their index in the Control
   * Logic List. The starting group is 1 cause group 0 is used by virtual point.
   */
  private void updateGroup() {
    Iterator<ICLogic> it = getAllItems().iterator();

    int group = 1;
    while (it.hasNext()) {
      it.next().setGroup(group++);
    }
  }

  @Override
  public IContainerValidator getValidator() {
    return validator;
  }

  @Override
  public void writeToXML(ControlLogicsT xml, ValidationResult result) {
    new CLogicXmlWriter(result).write(xml, getAllItems());
  }

  @Override
  public void read(ControlLogicsT xml, VirtualPointReadSupport support) throws InvalidConfException {
    new CLogicXmlReader(support).read(this, xml);
  }
  
  @Override
  public void configureIO(ControlLogicsT xml, VirtualPointReadSupport support) throws InvalidConfException {
    new CLogicXmlReader(support).configureIO(this, xml);
  }

  @Override
  public Collection<ICLogic> getAllClogic() {
    return getAllItems();
  }

  @Override
  public ListModel<ICLogic> getAllClogicListModel() {
    return getItemListModel();
  }
  
  @Override
  public boolean swap(ICLogic p0, ICLogic p1) {
    if (p0 == null || p1 == null) {
      log.error("Cannot swap null clogic!");
      return false;
    }

    if (!contains(p0) || !contains(p1)) {
      log.error("Cannot swap clogics as they are not found in manager!");
      return false;
    }

    if(super.swap(p0, p1)){
      int group0 = p0.getGroup();
      int group1 = p1.getGroup();
      p0.setGroup(group1);
      p1.setGroup(group0);
      return true;
    }else {
      return false;
    }
 
  }
}