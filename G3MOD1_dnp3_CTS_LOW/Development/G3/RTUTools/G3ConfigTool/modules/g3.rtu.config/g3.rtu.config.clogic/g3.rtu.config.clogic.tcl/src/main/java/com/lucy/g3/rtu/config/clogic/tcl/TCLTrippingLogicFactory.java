
package com.lucy.g3.rtu.config.clogic.tcl;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class TCLTrippingLogicFactory extends AbstractClogicFactory {
  public final static TCLTrippingLogicFactory INSTANCE = new TCLTrippingLogicFactory();
  private TCLTrippingLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new TCLTrippingSettingsEditor((TCLTrippingSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new TCLTrippingLogic();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return TCLTrippingLogic.TYPE;
  }
  
 
}

