/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.action;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.NumberFormatter;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.action.ActionLogic.InputSetting;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.InputSignal;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.InputMode;
import com.lucy.g3.xml.gen.common.ControlLogicDef.ACTION_CLOGIC_CMD;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;


class ActionLogicSettingsEditor extends AbstractCLogicSettingsEditor {
  private final ActionLogicSettings settings;
  
  public ActionLogicSettingsEditor(ActionLogicSettings settings) {
    super(settings);
    this.settings = settings;
  }

  @Override
  public void validate(ValidationResult result) {
    // Nothing to validate
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    JComponent comp0 = null;
    JComponent comp1 = null;
    
    DefaultFormBuilder builder = getDefaultBuilder();

    // Append Delay
    createLongField("Delay", ActionLogicSettings.PROPERTY_DELAY, Units.SECS);

    InputSetting[] inputSetting = settings.getInputSettings();
    NumberFormatter formatter = new NumberFormatter();
    formatter.setValueClass(Long.class);

    for (int i = 0; i < inputSetting.length; i++) {
      PresentationModel<InputSetting> inputPM = new PresentationModel<InputSetting>(inputSetting[i]);
      addPM(inputPM);

      builder.appendSeparator("\"Input " + i + "\" Settings");

      /* Append action command components */
      comp0 = BasicComponentFactory.createComboBox(new SelectionInList<ACTION_CLOGIC_CMD>(ACTION_CLOGIC_CMD.values(),
          inputPM.getBufferedModel(InputSetting.PROPERTY_COMMAND)));
      builder.append("Action Command:", comp0);
      builder.nextLine();

      /* Append pulse driven components */
      comp0 = BasicComponentFactory.createRadioButton(
          inputPM.getBufferedModel(InputSetting.PROPERTY_INPUT_MODE),
          InputMode.PULSE_DRIVEN,
          InputMode.PULSE_DRIVEN.getDescription());

      comp1 = BasicComponentFactory.createFormattedTextField(
          inputPM.getBufferedComponentModel(InputSignal.PROPERTY_PULSE_WIDTH),
          formatter);

      // Update enable state
      ((JRadioButton) comp0).addChangeListener(new EnableStateHandler((JRadioButton) comp0, comp1));

      builder.append(comp0, comp1);
      builder.nextLine();

      /* Append edge driven components */
      comp0 = BasicComponentFactory.createRadioButton(
          inputPM.getBufferedModel(InputSignal.PROPERTY_INPUT_MODE),
          InputMode.EDGE_DRIVEN,
          InputMode.EDGE_DRIVEN.getDescription());

      comp1 = BasicComponentFactory.createComboBox(new SelectionInList<EDGE_DRIVEN_MODE>(
          EDGE_DRIVEN_MODE.values(),
          inputPM.getBufferedComponentModel(InputSignal.PROPERTY_EDGE_DRIVEN_MODE)
          ));

      // Update enable state
      ((JRadioButton) comp0).addChangeListener(new EnableStateHandler((JRadioButton) comp0, comp1));

      builder.append(comp0, comp1);
      builder.nextLine();
    }
    
    return builder.getPanel();

  }


  private static class EnableStateHandler implements ChangeListener {

    private final JComponent enabledComp;
    private final JRadioButton button;


    public EnableStateHandler(JRadioButton button, JComponent enabledComp) {
      this.enabledComp = enabledComp;
      this.enabledComp.setEnabled(button.isSelected());

      this.button = button;
    }

    @Override
    public void stateChanged(ChangeEvent e) {
      enabledComp.setEnabled(button.isSelected());
    }
  }
}

