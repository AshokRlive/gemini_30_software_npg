
package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute;

import java.awt.Component;
import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.clogic.ICLogic;

public class OutputLogicPage extends WizardPage {

  public static final String PAGE_ID = "OutputLogicPage";


  public OutputLogicPage(Collection<ICLogic> logicSelections, String title) {
    super("Output Logic");
    setLongDescription(title);
    initComponents();

    if (logicSelections != null) {
      cbOutputLogic.setModel(new DefaultComboBoxModel<Object>(logicSelections.toArray()));
    }

    putWizardData(PAGE_ID, this);
  }
  
  public void setLabelText(String label) {
    this.labelOutputLogic.setText(label);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;

    if (getSelectedLogic() == null) {
      result = "";// "No selected switchgear logic";
    }

    return result;
  }

  public ICLogic getSelectedLogic() {
    return (ICLogic) cbOutputLogic.getSelectedItem();
  }

  private void initComponents() {
    labelOutputLogic = new JLabel();
    cbOutputLogic = new JComboBox<>();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, [80dlu,default], $lcgap, default:grow",
        "default, $lgap, fill:default"));

    // ---- label1 ----
    labelOutputLogic.setText("Output Logic:");
    add(labelOutputLogic, CC.xy(1, 1));
    add(cbOutputLogic, CC.xy(3, 1));
  }


  private JComboBox<Object> cbOutputLogic;
  private JLabel labelOutputLogic;
}