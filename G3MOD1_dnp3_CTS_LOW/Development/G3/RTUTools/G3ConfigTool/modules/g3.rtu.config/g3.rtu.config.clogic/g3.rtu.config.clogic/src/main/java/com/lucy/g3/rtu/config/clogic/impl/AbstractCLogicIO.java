/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.validate.IOValidator;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.model.impl.DefaultNode;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Basic implementation of {@code CLogicIO}.
 * <p>
 * An IO of a control logic can be mapped to a virtual
 * point/channel/module/another control logic. Each IO contains some properties:
 * "value" property indicates the name of the mapped item, such as an channel's
 * name or a virtual point's name; "optional" indicates if this IO is mandatory
 * to be configured, and etc.
 * </p>
 *
 * @param <SourceT>
 *          the input type 
 */
abstract class AbstractCLogicIO<SourceT extends INode> extends
    DefaultNode<INode> implements CLogicIO<SourceT> {

  private static String UNACCEPTABLE_IO_CONFIG_MSG_FORMAT = "Unacceptable Logic IO value. Logic:"
      + " \"%s\" IO:%s. Failed to set to new value: %s";

  private Logger log = Logger.getLogger(getClass());

  private final IODirection direction;

  private boolean optional;

  private final boolean enabled;

  private String valueString;

  private final ICLogic owner;

  private String label;

  private final IOValidator validator;

  private boolean allowChangeLabel;

  AbstractCLogicIO(ICLogic owner, IODirection direction, String label, 
      boolean allowUserChangeLabel, 
      boolean allowUserDelete,
      boolean isOptional, 
      boolean enabled) {
    super(NodeType.CLOGIC, INode.class);
    this.label = label;
    setName(label);
    this.allowChangeLabel = allowUserChangeLabel;
    setAllowDelete(allowUserDelete);
    syncNameWtihSource();

    this.direction = direction != null ? direction : IODirection.INPUT;

    this.owner = owner;
    this.optional = isOptional;
    this.enabled = enabled;
    updateValueStr();

    validator = new IOValidator(this);
  }
  
  public void setAllowChangeLabel(boolean allowChangeLabel) {
    this.allowChangeLabel = allowChangeLabel;
  }

  protected final void updateValueStr() {
    SourceT src = getValue();
    String newValueStr = src == null ? null : src.getName();
    if (newValueStr == null) {
      newValueStr = isOptional() ? OPTIONAL_NULL_TEXT
          : MANDATORY_NULL_TEXT;
    }

    Object oldValue = getValueString();
    this.valueString = newValueStr;
    firePropertyChange(PROPERTY_VALUE_STRING, oldValue, newValueStr);
  }

  public Object getBeanUser() {
    return this;
  }

  @Override
  protected void setName(String name) {
    super.setName(name);
    updateValueStr();
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public void setLabel(String label) {
    Object oldValue = getLabel();
    this.label = label;
    setName(label);
    firePropertyChange(PROPERTY_LABEL, oldValue, label);
  }

  // Refactoring required.
  @Override
  public boolean isAllowChangeLabel() {
    return allowChangeLabel; 
  }

  @Override
  public String getValueString() {
    return valueString;
  }

  @Override
  public final boolean isOptional() {
    return optional;
  }

  public void setOptional(boolean optional) {
    this.optional = optional;
    if (getValue() == null) {
      updateValueStr();
    }
  }

  @Override
  public final ICLogic getOwner() {
    return owner;
  }

  @Override
  public final boolean isValid() {
    return getValidator().isValid();
  }

  @Override
  public final boolean isConfigurable() {
    return enabled;
  }

  @Override
  public final IODirection getDirection() {
    return direction;
  }

  /**
   * Help method to provide a common main frame which can be used as a parent of
   * dialogues.
   */
  protected JFrame getParentFrame() {
    Application app = Application.getInstance();
    if (app instanceof SingleFrameApplication) {
      return ((SingleFrameApplication) app).getMainFrame();
    } else {
      return null;
    }
  }

  protected static String createWarningMsg(MODULE[] supportedModuleTypes) {
    StringBuilder sb = new StringBuilder();
    sb.append("No Module found.");
    if (supportedModuleTypes != null) {
      sb.append("Module:");
      for (int i = 0; i < supportedModuleTypes.length; i++) {
        sb.append(ModuleResource.INSTANCE
            .getModuleTypeShortName(supportedModuleTypes[i]));
        if (i != supportedModuleTypes.length - 1) {
          sb.append("/ ");
        }
      }
      sb.append(" is required");
    }

    return sb.toString();
  }

  @Override
  public void setValue(SourceT newValue) {
    Object oldValue = getValue();
    
    if(newValue == oldValue) {
      return;
    }
    
    // check if the value is accepted
    if (newValue != null && !isAcceptable(newValue)) {
      String err = String.format(UNACCEPTABLE_IO_CONFIG_MSG_FORMAT,
          getOwner(), getLabel(), newValue);
      log.error(err);
      throw new RuntimeException(err);
    }

    super.setSource(newValue);
    firePropertyChange(PROPERTY_VALUE, oldValue, newValue);
    
    getValidator().validate();
  }
  
  @Override
  public void fireValidChangeEvent(){
    firePropertyChange(PROPERTY_VALID, !isValid(), isValid());
  }

  @SuppressWarnings("unchecked")
  @Override
  public SourceT getValue() {
    return (SourceT) getSource();
  }

  @Override
  public final IValidator getValidator() {
    return validator;
  }

  /**
   * This exception should be thrown if user is trying to configure multiple
   * control logics for controlling the same output.
   */
  public static class CLogicOutputConflictException extends RuntimeException {

    public CLogicOutputConflictException(String message) {
      super(message);
    }
  }

}
