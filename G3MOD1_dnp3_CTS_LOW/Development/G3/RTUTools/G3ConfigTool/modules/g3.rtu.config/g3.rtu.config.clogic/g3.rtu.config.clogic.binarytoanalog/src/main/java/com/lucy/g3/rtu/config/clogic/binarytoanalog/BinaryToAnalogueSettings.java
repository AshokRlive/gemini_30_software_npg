/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.binarytoanalog;

import com.g3schema.ns_clogic.CLogicParametersBaseT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_common.ParameterT;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

public class BinaryToAnalogueSettings extends AbstractCLogicSettings<BinaryToAnalogue> {

  public static final String PROPERTY_DEBOUNCE= "debounce";
  
  private long debounce=100; //ms


  BinaryToAnalogueSettings(BinaryToAnalogue owner) {
    super(owner);
  }
  
  
  public long getDebounce() {
    return debounce;
  }

  
  public void setDebounce(long debounce) {
    Object oldValue = this.debounce;
    this.debounce = debounce;
    firePropertyChange(PROPERTY_DEBOUNCE, oldValue, debounce);
  }

  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    CLogicParametersBaseT xml = xml_param.generic.first();
    
    for (int i = 0; i < xml.parameter.count(); i++) {
      ParameterT xmlDeb = xml.parameter.at(i);
      if(PROPERTY_DEBOUNCE.equals(xmlDeb.parameterName.getValue())) {
        setDebounce(Long.valueOf(xmlDeb.parameterValue.getValue()));
      }
    }
  }

  @Override
  public void writeParamToXML(CLogicParametersT xmlParam) {
    CLogicParametersBaseT xml = xmlParam.generic.append();
    ParameterT xmlDebounce = xml.parameter.append();
    xmlDebounce.parameterName.setValue(PROPERTY_DEBOUNCE);
    xmlDebounce.parameterValue.setValue(String.valueOf(getDebounce()));
  }

  @Override
  public IValidator getValidator() {
    return null;
  }

}
