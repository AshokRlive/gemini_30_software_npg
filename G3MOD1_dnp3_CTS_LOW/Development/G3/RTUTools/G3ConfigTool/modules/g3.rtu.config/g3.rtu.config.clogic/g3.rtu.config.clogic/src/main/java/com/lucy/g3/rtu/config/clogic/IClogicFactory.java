
package com.lucy.g3.rtu.config.clogic;

import javax.swing.JPanel;

import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;

public interface IClogicFactory {
  String PROPERTY_TAB_TITLE = "tabTitle";

  ICLogicSettingsEditor createEditor(ICLogicSettings settings);
  
  ICLogic createLogic();

  ICLogicType getLogicType();

  IControlLogicAddingWizardContributor getWizardContributor();
  
  /**
   * Creates configuration tabs for the logic. <br>
   * The created tabs will be populated into configuration tabpane of this logic.<br> 
   * The title of the created tab will be accessed via {@code PROPERTY_TAB_TITLE}.<br>
   * @param clogic
   * @return
   */
  JPanel[] createConfigTabs(ICLogic clogic);
}

