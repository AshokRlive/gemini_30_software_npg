
package com.lucy.g3.rtu.config.clogic.pseudoclock;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;


public class PseudoClockLogicFactory extends AbstractClogicFactory {
  public final static PseudoClockLogicFactory INSTANCE = new PseudoClockLogicFactory();
  private PseudoClockLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new PseudoClockSettingsEditor((PseudoClockSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new PseudoClock();
  }

  @Override
  public ICLogicType getLogicType() {
    return PseudoClock.TYPE;
  }

}

