/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.minmaxavg;

import java.awt.Color;
import java.awt.Component;
import java.text.NumberFormat;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.minmaxavg.MinMaxAvgLogic.ClockTime;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
import com.lucy.g3.common.utils.EqualsUtil;


class MinMaxAvgLogicSettingsEditor extends AbstractCLogicSettingsEditor {

  /**
   * 
   */
  private static final String LABEL_START_DELAY = "Start Delay";

  public MinMaxAvgLogicSettingsEditor(ICLogicSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result, pm, MinMaxAvgLogicSettings.PROPERTY_START_DELAY, LABEL_START_DELAY);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    

    JComponent comp = null;

    builder.appendSeparator("Event Mode");

    // Append period time
    @SuppressWarnings("unchecked")
    ComboBoxModel<Long> adapter = new ComboBoxAdapter<Long>(
        MinMaxAvgLogic.getPeriodChoices(), pm.getBufferedModel(MinMaxAvgLogicSettings.PROPERTY_PERIOD));
    JComboBox<Long> periodTimeCombo = new JComboBox<Long>(adapter);
    periodTimeCombo.setRenderer(new PeriodCombRender());
    JRadioButton periodTimeRadio = BasicComponentFactory
        .createRadioButton(pm.getBufferedModel(MinMaxAvgLogicSettings.PROPERTY_EVENT_MODE),
            MinMaxAvgLogic.EventMode.BY_INTERVAL, "Interval:");
    builder.append(periodTimeRadio, periodTimeCombo);
    bindingEnableState(periodTimeRadio, periodTimeCombo);
    builder.nextLine();

    // Append clock time
    @SuppressWarnings("unchecked")
    ComboBoxModel<ClockTime> clockTimeAdapter = new ComboBoxAdapter<ClockTime>(
        ClockTime.values(),
        pm.getBufferedModel(MinMaxAvgLogicSettings.PROPERTY_CLOCKTIME));
    JComboBox<ClockTime> clockTimeCombo = new JComboBox<ClockTime>(clockTimeAdapter);
    clockTimeCombo.setRenderer(new PeriodCombRender());
    JRadioButton clockTimeRadio = BasicComponentFactory
        .createRadioButton(pm.getBufferedModel(MinMaxAvgLogicSettings.PROPERTY_EVENT_MODE),
            MinMaxAvgLogic.EventMode.BY_CLOCKTIME, "From O'Clock:");
    builder.append(clockTimeRadio, clockTimeCombo);
    bindingEnableState(clockTimeRadio, clockTimeCombo);
    builder.nextLine();

    // Append start delay
    createIntField(LABEL_START_DELAY,  MinMaxAvgLogicSettings.PROPERTY_START_DELAY, Units.SECS);

    builder.appendSeparator("Counter Support");
    builder.nextLine();

    final boolean counterEnabled = EqualsUtil.areEqual(Boolean.TRUE,
        pm.getBufferedValue(MinMaxAvgLogicSettings.PROPERTY_COUNTER_ENABLED));

    // Append counter enabled
    builder.append("Convert Analogue to Counter:", new JLabel(counterEnabled ? "YES" : "NO"));
    builder.nextLine();

    // Append counter scaling.
    if (counterEnabled) {
      comp = BasicComponentFactory.createFormattedTextField(
          pm.getBufferedModel(MinMaxAvgLogicSettings.PROPERTY_COUNTER_SCALINGFACTOR),
          NumberFormat.getNumberInstance());
      builder.append("Conversion Scaling Factor:", comp);
      builder.nextLine();
      JLabel hint;
      builder.append("", hint = new JLabel("(counter value = analogue value * scaling factor)"));
      hint.setForeground(Color.gray);
      builder.nextLine();
    }
    
    return builder.getPanel();
  }

  private static class PeriodCombRender extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list,
        Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (value != null && value instanceof Long) {
        String text;
        long period = (Long) value;
        long hrs = period / 60;
        long mins = period % 60;

        String minsTxt = mins > 1 ? "Minutes" : "Minute";
        String hrsTxt = hrs > 1 ? "Hours" : "Hour";

        if (hrs != 0 && mins != 0) {
          text = String.format("%d %s %d %s", hrs, hrsTxt, mins, minsTxt);
        } else if (hrs != 0) {
          text = String.format("%d %s", hrs, hrsTxt);
        } else if (mins != 0) {
          text = String.format("%d %s", mins, minsTxt);
        } else {
          text = String.format("Immediately");
        }

        setText(text);
      }
      return this;
    }
  }
}

