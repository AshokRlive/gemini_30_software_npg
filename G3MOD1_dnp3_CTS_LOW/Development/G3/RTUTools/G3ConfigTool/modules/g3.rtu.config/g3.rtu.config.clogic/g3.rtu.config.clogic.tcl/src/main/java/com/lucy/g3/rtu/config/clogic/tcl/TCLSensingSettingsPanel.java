/*
 * Created by JFormDesigner on Tue Apr 11 20:11:06 BST 2017
 */

package com.lucy.g3.rtu.config.clogic.tcl;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatterFactory;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettings.PowerFlowDirection;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettings.TimeSlotId;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettings.TrippingMode;

/**
 */
public class TCLSensingSettingsPanel extends JPanel{
  private TCLSensingSettings settings;
  public TCLSensingSettingsPanel() {
    initComponents();
    initComponentModels();
    initComponentNames();
    updateCards();
  }
  
  private void initComponentNames() {
    ftfTimerTrigger.setName("tclSensing.ftfTimerTrigger");
    ftfTimerBadIO.setName("tclSensing.ftfTimerBadIO");
    ftfCBTripValue.setName("tclSensing.ftfCBTripValue");
    ftfSummerStartDate.setName("tclSensing.ftfSummerStartDate");
    ftfWinterStartDate.setName("tclSensing.ftfWinterStartDate");
    comboCBTripDirection.setName("tclSensing.comboCBTripDirection");
    comboTrippingMode.setName("tclSensing.comboTrippingMode");
    panelSummerWeekdays.initComponentNames("panelSummerWeekdays");
    panelSummerWeekends.initComponentNames("panelSummerWeekends");
    panelWinterWeekdays.initComponentNames("panelWinterWeekdays");
    panelWinterWeekends.initComponentNames("panelWinterWeekends");
  }

  public void load(ICLogicSettings _settings){
    TCLSensingSettings settings = (TCLSensingSettings) _settings;
    ftfTimerTrigger.setValue(settings.getTriggerTimer());
    ftfTimerBadIO.setValue(settings.getBadioTimer());
    ftfCBTripValue.setValue(settings.getCbTripSettings().getPowerValueAsFloat());
    ftfSummerStartDate.setValue(settings.getSummerStart());
    ftfWinterStartDate.setValue(settings.getWinterStart());
    comboCBTripDirection.setSelectedItem(settings.getCbTripSettings().getPowerDirection());
    comboTrippingMode.setSelectedItem(settings.getTrippingMode());
    
    panelSummerWeekdays.load(settings.getTimeSlot(TimeSlotId.SummerWeekdays));
    panelSummerWeekends.load(settings.getTimeSlot(TimeSlotId.SummerWeekends));
    panelWinterWeekdays.load(settings.getTimeSlot(TimeSlotId.WinterWeekdays));
    panelWinterWeekends.load(settings.getTimeSlot(TimeSlotId.WinterWeekends));
    
    this.settings = settings;
  }
  
  public void save(){
    if(settings != null)
    save(settings);
  }
  
  
  public TCLSensingSettings getSettings() {
    return settings;
  }

  public void save(ICLogicSettings _settings){
    TCLSensingSettings settings = (TCLSensingSettings) _settings;
    settings.setTriggerTimer((long) ftfTimerTrigger.getValue());
    settings.setBadioTimer((long) ftfTimerBadIO.getValue());
    settings.getCbTripSettings().setPowerValue((float) ftfCBTripValue.getValue()) ;
    settings.setSummerStart((Date) ftfSummerStartDate.getValue());
    
    settings.setWinterStart((Date) ftfWinterStartDate.getValue());
    settings.getCbTripSettings().setPowerDirection((PowerFlowDirection) comboCBTripDirection.getSelectedItem());
    settings.setTrippingMode((TrippingMode) comboTrippingMode.getSelectedItem());
    
    panelSummerWeekdays.save(settings.getTimeSlot(TimeSlotId.SummerWeekdays));
    panelSummerWeekends.save(settings.getTimeSlot(TimeSlotId.SummerWeekends));
    panelWinterWeekdays.save(settings.getTimeSlot(TimeSlotId.WinterWeekdays));
    panelWinterWeekends.save(settings.getTimeSlot(TimeSlotId.WinterWeekends));
  }
  
  private static void setDateMonthFormat(JSpinner spinner) {
    spinner.setModel(new SpinnerDateModel());
    DateEditor editor = new JSpinner.DateEditor(spinner, "dd/MM");
    editor.getFormat().setTimeZone(TimeZone.getTimeZone("UTC"));
    spinner.setEditor(editor);
  }
  
  private void initComponentModels() {
    ftfTimerBadIO.setFormatterFactory(new DefaultFormatterFactory(new BoundaryNumberFormatter(0L, Long.MAX_VALUE)));
    ftfTimerTrigger.setFormatterFactory(new DefaultFormatterFactory(new BoundaryNumberFormatter(0L, Long.MAX_VALUE)));
    
    
    ftfCBTripValue.setFormatterFactory(new DefaultFormatterFactory(
        TCLPowerSettings.POWER_VALUE_FORMATTER));
    ftfCBTripValue.setText("0");
    
    setDateMonthFormat(ftfSummerStartDate);
    setDateMonthFormat(ftfWinterStartDate);
    
    comboTrippingMode.setModel(new DefaultComboBoxModel<Object>(TrippingMode.values()));
    comboCBTripDirection.setModel(new DefaultComboBoxModel<Object>(PowerFlowDirection.values()));
    comboCBTripDirection.setRenderer(new DefaultListCellRenderer(){

      @Override
      public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
          boolean cellHasFocus) {
        Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        PowerFlowDirection dir = (PowerFlowDirection) value;
        if(dir != null) {
          setToolTipText(dir.getDescription());
          setText(String.format("%s (%s)", dir.getSymbol(),dir.getDescription()));
        }
        return comp;
      }
      
    });
    
    comboLocale.setModel(new DefaultComboBoxModel<String>(new String[]{
        Locale.UK.getDisplayCountry()
    }));
  }

  private void updateCards() {
    CardLayout cardLayout = (CardLayout) cards.getLayout();
    int cardIndex = comboTrippingMode.getSelectedIndex();
    cardLayout.show(cards, "card"+cardIndex);
  }

  private void comboTrippingModeItemStateChanged(ItemEvent e) {
    updateCards();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    separator1 = compFactory.createSeparator("Timer Settings");
    label1 = new JLabel();
    ftfTimerTrigger = new JFormattedTextField();
    label4 = new JLabel();
    label2 = new JLabel();
    ftfTimerBadIO = new JFormattedTextField();
    label5 = new JLabel();
    separator2 = compFactory.createSeparator("Turn-down Settings");
    label3 = new JLabel();
    comboTrippingMode = new JComboBox<>();
    cards = new JPanel();
    card0 = new JPanel();
    separator3 = compFactory.createSeparator("Analogue Trigger Settings");
    label6 = new JLabel();
    ftfCBTripValue = new JFormattedTextField();
    label8 = new JLabel();
    label7 = new JLabel();
    comboCBTripDirection = new JComboBox<>();
    card1 = new JPanel();
    label9 = new JLabel();
    comboLocale = new JComboBox<>();
    tabbedPane1 = new JTabbedPane();
    panel4 = new JPanel();
    label10 = compFactory.createLabel("");
    ftfSummerStartDate = new JSpinner();
    label11 = compFactory.createLabel("");
    panelSummerWeekdays = new TimeSettingsPanel();
    label12 = compFactory.createLabel("");
    panelSummerWeekends = new TimeSettingsPanel();
    panel7 = new JPanel();
    label13 = compFactory.createLabel("");
    ftfWinterStartDate = new JSpinner();
    label14 = compFactory.createLabel("");
    panelWinterWeekdays = new TimeSettingsPanel();
    label15 = compFactory.createLabel("");
    panelWinterWeekends = new TimeSettingsPanel();

    //======== this ========
    setLayout(new FormLayout(
      "[100dlu,default], $lcgap, default, $lcgap, [50dlu,default], $lcgap, default:grow",
      "2*(default, $lgap), default, $pgap, default, $lgap, default, $ugap, fill:default:grow"));
    add(separator1, CC.xywh(1, 1, 7, 1));

    //---- label1 ----
    label1.setText("Timer for Trigger:");
    label1.setHorizontalAlignment(SwingConstants.RIGHT);
    add(label1, CC.xy(1, 3));
    add(ftfTimerTrigger, CC.xy(3, 3));

    //---- label4 ----
    label4.setText("Seconds");
    add(label4, CC.xy(5, 3));

    //---- label2 ----
    label2.setText("Timer for Bad I/O:");
    label2.setHorizontalAlignment(SwingConstants.RIGHT);
    add(label2, CC.xy(1, 5));
    add(ftfTimerBadIO, CC.xy(3, 5));

    //---- label5 ----
    label5.setText("Seconds");
    add(label5, CC.xy(5, 5));
    add(separator2, CC.xywh(1, 7, 7, 1));

    //---- label3 ----
    label3.setText("Tripping Mode:");
    label3.setHorizontalAlignment(SwingConstants.RIGHT);
    add(label3, CC.xy(1, 9));

    //---- comboTrippingMode ----
    comboTrippingMode.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        comboTrippingModeItemStateChanged(e);
      }
    });
    add(comboTrippingMode, CC.xy(3, 9));

    //======== cards ========
    {
      cards.setLayout(new CardLayout());

      //======== card0 ========
      {
        card0.setLayout(new FormLayout(
          "[100dlu,default], $lcgap, [80dlu,default], $lcgap, [50dlu,default]",
          "2*(default, $lgap), default"));
        card0.add(separator3, CC.xywh(1, 1, 5, 1));

        //---- label6 ----
        label6.setText("Trip Value:");
        label6.setHorizontalAlignment(SwingConstants.RIGHT);
        card0.add(label6, CC.xy(1, 3));
        card0.add(ftfCBTripValue, CC.xy(3, 3));

        //---- label8 ----
        label8.setText("Amps");
        card0.add(label8, CC.xy(5, 3));

        //---- label7 ----
        label7.setText("Trip Direction:");
        label7.setHorizontalAlignment(SwingConstants.RIGHT);
        card0.add(label7, CC.xy(1, 5));
        card0.add(comboCBTripDirection, CC.xy(3, 5));
      }
      cards.add(card0, "card0");

      //======== card1 ========
      {
        card1.setLayout(new FormLayout(
          "[100dlu,default], $lcgap, default, $lcgap, [50dlu,default]:grow",
          "default, $ugap, fill:default:grow"));

        //---- label9 ----
        label9.setText("Locale:");
        label9.setHorizontalAlignment(SwingConstants.RIGHT);
        card1.add(label9, CC.xy(1, 1));

        //---- comboLocale ----
        comboLocale.setEnabled(false);
        card1.add(comboLocale, CC.xy(3, 1));

        //======== tabbedPane1 ========
        {

          //======== panel4 ========
          {
            panel4.setBorder(Borders.TABBED_DIALOG_BORDER);
            panel4.setLayout(new FormLayout(
              "default, $lcgap, [50dlu,default], $lcgap, default:grow",
              "2*(default, $lgap), default, $ugap, default, $lgap, default"));

            //---- label10 ----
            label10.setText("Start Date (Day/Month):");
            panel4.add(label10, CC.xy(1, 1));
            panel4.add(ftfSummerStartDate, CC.xy(3, 1));

            //---- label11 ----
            label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
            label11.setText("Weekdays");
            panel4.add(label11, CC.xywh(1, 3, 3, 1));
            panel4.add(panelSummerWeekdays, CC.xywh(1, 5, 5, 1));

            //---- label12 ----
            label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
            label12.setText("Weekends");
            panel4.add(label12, CC.xywh(1, 7, 3, 1));
            panel4.add(panelSummerWeekends, CC.xywh(1, 9, 5, 1));
          }
          tabbedPane1.addTab("Summer Settings", panel4);

          //======== panel7 ========
          {
            panel7.setBorder(Borders.TABBED_DIALOG_BORDER);
            panel7.setLayout(new FormLayout(
              "default, $lcgap, [50dlu,default], $lcgap, default:grow",
              "2*(default, $lgap), default, $ugap, default, $lgap, default"));

            //---- label13 ----
            label13.setText("Start Date (Day/Month):");
            panel7.add(label13, CC.xy(1, 1));
            panel7.add(ftfWinterStartDate, CC.xy(3, 1));

            //---- label14 ----
            label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
            label14.setText("Weekdays");
            panel7.add(label14, CC.xywh(1, 3, 3, 1));
            panel7.add(panelWinterWeekdays, CC.xywh(1, 5, 5, 1));

            //---- label15 ----
            label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
            label15.setText("Weekends");
            panel7.add(label15, CC.xywh(1, 7, 3, 1));
            panel7.add(panelWinterWeekends, CC.xywh(1, 9, 5, 1));
          }
          tabbedPane1.addTab("Winter Settings", panel7);
        }
        card1.add(tabbedPane1, CC.xywh(1, 3, 5, 1));
      }
      cards.add(card1, "card1");
    }
    add(cards, CC.xywh(1, 11, 7, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JComponent separator1;
  private JLabel label1;
  private JFormattedTextField ftfTimerTrigger;
  private JLabel label4;
  private JLabel label2;
  private JFormattedTextField ftfTimerBadIO;
  private JLabel label5;
  private JComponent separator2;
  private JLabel label3;
  private JComboBox<Object> comboTrippingMode;
  private JPanel cards;
  private JPanel card0;
  private JComponent separator3;
  private JLabel label6;
  private JFormattedTextField ftfCBTripValue;
  private JLabel label8;
  private JLabel label7;
  private JComboBox<Object> comboCBTripDirection;
  private JPanel card1;
  private JLabel label9;
  private JComboBox<String> comboLocale;
  private JTabbedPane tabbedPane1;
  private JPanel panel4;
  private JLabel label10;
  private JSpinner ftfSummerStartDate;
  private JLabel label11;
  private TimeSettingsPanel panelSummerWeekdays;
  private JLabel label12;
  private TimeSettingsPanel panelSummerWeekends;
  private JPanel panel7;
  private JLabel label13;
  private JSpinner ftfWinterStartDate;
  private JLabel label14;
  private TimeSettingsPanel panelWinterWeekdays;
  private JLabel label15;
  private TimeSettingsPanel panelWinterWeekends;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
  
  public boolean validateSettings() {
    return panelSummerWeekdays.validateSettings()
        && panelSummerWeekends.validateSettings()
        && panelWinterWeekdays.validateSettings()
        && panelWinterWeekends.validateSettings();
  }
}
