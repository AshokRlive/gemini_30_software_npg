/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IClogicFactory;
//import com.lucy.g3.rtu.config.clogic.automation.AutomationLogic;
//import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicConstantManager;
//import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicConstantsPanel;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * Page for configuring Control Logic.
 */
public class CLogicPage extends AbstractConfigPage {


  private final ICLogic clogic;

  private final CLogicGraphPanel graph;
  
  private final CLogicPageModel model;


  /**
   * Constructor a graphic logic page based on give logic.
   *
   * @param cl
   *          the control logic object
   */
  public CLogicPage(ICLogic cl) {
    super(cl);
    this.model = new CLogicPageModel(cl);
    this.clogic = Preconditions.checkNotNull(cl, "logic must not be null");
    this.graph = new CLogicGraphPanel(clogic, 
        model.getAction(CLogicPageModel.ACTION_ADD_INPUT),
        model.getAction(CLogicPageModel.ACTION_ADD_OUTPUT),
        model.getAction(CLogicPageModel.ACTION_ADD_POINT));
    
    PropertyConnector.connect(cl, ICLogic.PROPERTY_NAME, this, PROPERTY_NODE_NAME).updateProperty2();
    PropertyConnector.connect(cl, ICLogic.PROPERTY_NAME, this, PROPERTY_TITLE).updateProperty2();
  }

  @Override
  protected void init() {
    graph.init();
    initComponents();
    IClogicFactory factory = CLogicFactories.getFactory(clogic.getType().getId());
    JPanel[] tabs = factory.createConfigTabs(clogic);
    if(tabs != null) {
      for (int i = 0; i < tabs.length; i++) {
        String tabtitle = (String) tabs[i].getClientProperty(IClogicFactory.PROPERTY_TAB_TITLE);
        if(tabtitle == null) {
          throw new IllegalStateException("title property not found in:"+tabs[i]);
        }
        tabbedPane1.insertTab(tabtitle, null, tabs[i], "", tabbedPane1.getTabCount() - 1);
      }
    }
  }


  @Override
  public String toString() {
    return clogic.toString();
  }


  @Override
  public String getContextTips() {
    return clogic.getHTMLSummary();
  }

  @Override
  public Action[] getContextActions() {
    return graph.getContextActions();
  }

  public void repaintCanvas() {
    graph.repaintCanvas();
  }

  private void createUIComponents() {
    panelOverview = graph;
    
    tableInputs = new JXTable(model.getInputsTableModel());
    btnAddInputs = new JButton(model.getAction(CLogicPageModel.ACTION_ADD_INPUT));
    btnRemoveInputs = new JButton(model.getAction(CLogicPageModel.ACTION_REMOVE_INPUT));
    
    tableOutputs = new JXTable(model.getOutputsTableModel());
    btnAddOutputs = new JButton(model.getAction(CLogicPageModel.ACTION_ADD_OUTPUT));
    btnRemoveOutputs = new JButton(model.getAction(CLogicPageModel.ACTION_REMOVE_OUTPUT));
    
    tablePoints = new JXTable(model.getPointsTableModel());
    btnAddPoint = new JButton(model.getAction(CLogicPageModel.ACTION_ADD_POINT));
    btnRemovePoint = new JButton(model.getAction(CLogicPageModel.ACTION_REMOVE_POINT));
    btnEditPoint = new JButton(model.getAction(CLogicPageModel.ACTION_EDIT_POINT));

//    UIUtil.packTableColumns(tableInputs);
//    UIUtil.packTableColumns(tableOutputs);
//    UIUtil.packTableColumns(tablePoints);
    /*
     * Bug: we must NOT set "packed" to true, otherwise the table editor becomes sluggish.
     * No clue to this bug. 
     */
    // Set default configuration
    UIUtils.setDefaultConfig((JXTable) tableInputs, true, false, true);
    UIUtils.setDefaultConfig((JXTable) tableOutputs, true, false, true);
    UIUtils.setDefaultConfig((JXTable) tablePoints, true, false, true);
    
    
    // Bind table model
    Bindings.bind(tableInputs, model.getInputsSelection().getList(), model.getInputsSelectionModel());
    Bindings.bind(tableOutputs, model.getOutputsSelection().getList(), model.getOutputsSelectionModel());
    Bindings.bind(tablePoints, model.getPointsSelection().getList(), model.getPointsSelectionModel());
    
    // Bind key stroke
    UIUtils.registerKeyAction(tableInputs, KeyEvent.VK_DELETE, model.getAction(CLogicPageModel.ACTION_REMOVE_INPUT));
    UIUtils.registerKeyAction(tableOutputs, KeyEvent.VK_DELETE, model.getAction(CLogicPageModel.ACTION_REMOVE_OUTPUT));
    UIUtils.registerKeyAction(tablePoints, KeyEvent.VK_DELETE, model.getAction(CLogicPageModel.ACTION_REMOVE_POINT));
    
    // Set editor
    tableInputs.getColumnModel().getColumn(CLogicPageModel.COLUMN_INDEX_INPUT_VALUE)
      .setCellEditor(new CLogicIOValueEditor());
    tableOutputs.getColumnModel().getColumn(CLogicPageModel.COLUMN_INDEX_OUTPUT_VALUE)
      .setCellEditor(new CLogicIOValueEditor());
    
    // Set renderer
    tableInputs.getColumnModel().getColumn(CLogicPageModel.COLUMN_INDEX_INPUT_VALUE)
    .setCellRenderer(new CLogicIOValueRenderer());
    tableOutputs.getColumnModel().getColumn(CLogicPageModel.COLUMN_INDEX_OUTPUT_VALUE)
    .setCellRenderer(new CLogicIOValueRenderer());
    
    // Set highilighter
    Highlighter hl = new ColorHighlighter(new EditibleCellPredicator(),Color.WHITE, Color.BLUE);
    ((JXTable)tableInputs).setHighlighters(hl);
    ((JXTable)tableOutputs).setHighlighters(hl);
    ((JXTable)tablePoints).setHighlighters(hl);
    
    // Create Popup menu
    JPopupMenu menu = new JPopupMenu();
    menu.add(model.getAction(CLogicPageModel.ACTION_ADD_INPUT));
    menu.add(model.getAction(CLogicPageModel.ACTION_REMOVE_INPUT));
    tableInputs.setComponentPopupMenu(menu);
    
    menu = new JPopupMenu();
    menu.add(model.getAction(CLogicPageModel.ACTION_ADD_OUTPUT));
    menu.add(model.getAction(CLogicPageModel.ACTION_REMOVE_OUTPUT));
    tableOutputs.setComponentPopupMenu(menu);
    
    menu = new JPopupMenu();
    menu.add(model.getAction(CLogicPageModel.ACTION_ADD_POINT));
    menu.add(model.getAction(CLogicPageModel.ACTION_REMOVE_POINT));
    tablePoints.setComponentPopupMenu(menu);
    
    
    btnChangeSettings = new JButton(new CLogicSettingAction(clogic));
    
    
    // Create constants panel.
//    AutomationLogicConstantManager constant = null;
//    if (clogic instanceof AutomationLogic)
//      constant = ((AutomationLogic)clogic).getSettings().getConstantsManager();
//    panelConstants = new AutomationLogicConstantsPanel(constant); 
  }

  private void tablePointsMouseClicked(MouseEvent e) {
    if (e.getClickCount() > 1) {
      model.editPoint();
    }
  }
  

  private static class ComboBoxCellEditor extends DefaultCellEditor {

    private final JComboBox<Object> comboBox;


    public ComboBoxCellEditor(JComboBox<Object> comboBox) {
      super(comboBox);
      this.comboBox = comboBox;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      AbstractTableAdapter<?> model = (AbstractTableAdapter<?>) table.getModel();
      CLogicIO<?> input = (CLogicIO<?>) model.getRow(row);
      Collection<?> sources = input.getAvailableSources();
      if (sources != null) {
        DefaultComboBoxModel<Object> comboMode = new DefaultComboBoxModel<>(sources.toArray());
        comboMode.addElement(null);
        comboBox.setModel(comboMode);
      }
      return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }
  }
  
  
  private class EditibleCellPredicator implements HighlightPredicate {

    @Override
    public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
      return adapter.isCellEditable(adapter.row, adapter.column);
    }
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    scrollPane0 = new JScrollPane();
    panelInputs = new JPanel();
    scrollPane1 = new JScrollPane();
    controlPanelInputs = new JPanel();
    panelOutputs = new JPanel();
    scrollPane3 = new JScrollPane();
    controlPanelPoints = new JPanel();
    panelControls = new JPanel();
    scrollPane2 = new JScrollPane();
    controlPanelOutputs = new JPanel();
    panelSettings = new JPanel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane1 ========
    {

      //======== scrollPane0 ========
      {
        scrollPane0.setBorder(BorderFactory.createEmptyBorder());
        scrollPane0.setViewportView(panelOverview);
      }
      tabbedPane1.addTab("Overview", scrollPane0);

      //======== panelInputs ========
      {
        panelInputs.setLayout(new FormLayout(
          "default:grow, $lcgap, default",
          "fill:default:grow"));

        //======== scrollPane1 ========
        {

          //---- tableInputs ----
          tableInputs.setFillsViewportHeight(true);
          scrollPane1.setViewportView(tableInputs);
        }
        panelInputs.add(scrollPane1, CC.xy(1, 1));

        //======== controlPanelInputs ========
        {
          controlPanelInputs.setBorder(Borders.TABBED_DIALOG_BORDER);
          controlPanelInputs.setLayout(new FormLayout(
            "[80dlu,default]",
            "2*(default, $lgap), default"));

          //---- btnAddInputs ----
          btnAddInputs.setText("Add Input");
          controlPanelInputs.add(btnAddInputs, CC.xy(1, 1));

          //---- btnRemoveInputs ----
          btnRemoveInputs.setText("Remove Input");
          controlPanelInputs.add(btnRemoveInputs, CC.xy(1, 3));
        }
        panelInputs.add(controlPanelInputs, CC.xy(3, 1));
      }
      tabbedPane1.addTab("Inputs", panelInputs);

      //======== panelOutputs ========
      {
        panelOutputs.setLayout(new FormLayout(
          "default:grow, $lcgap, default",
          "fill:default:grow"));

        //======== scrollPane3 ========
        {

          //---- tablePoints ----
          tablePoints.setFillsViewportHeight(true);
          tablePoints.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tablePointsMouseClicked(e);
            }
          });
          scrollPane3.setViewportView(tablePoints);
        }
        panelOutputs.add(scrollPane3, CC.xy(1, 1));

        //======== controlPanelPoints ========
        {
          controlPanelPoints.setBorder(Borders.TABBED_DIALOG_BORDER);
          controlPanelPoints.setLayout(new FormLayout(
            "[80dlu,default]",
            "2*(default, $lgap), default"));

          //---- btnAddPoint ----
          btnAddPoint.setText("Add Point");
          controlPanelPoints.add(btnAddPoint, CC.xy(1, 1));

          //---- btnRemovePoint ----
          btnRemovePoint.setText("Remove Point");
          controlPanelPoints.add(btnRemovePoint, CC.xy(1, 3));

          //---- btnEditPoint ----
          btnEditPoint.setText("Edit Point");
          controlPanelPoints.add(btnEditPoint, CC.xy(1, 5));
        }
        panelOutputs.add(controlPanelPoints, CC.xy(3, 1));
      }
      tabbedPane1.addTab("Outputs", panelOutputs);

      //======== panelControls ========
      {
        panelControls.setLayout(new FormLayout(
          "default:grow, $lcgap, default",
          "fill:default:grow"));

        //======== scrollPane2 ========
        {

          //---- tableOutputs ----
          tableOutputs.setFillsViewportHeight(true);
          scrollPane2.setViewportView(tableOutputs);
        }
        panelControls.add(scrollPane2, CC.xy(1, 1));

        //======== controlPanelOutputs ========
        {
          controlPanelOutputs.setBorder(Borders.TABBED_DIALOG_BORDER);
          controlPanelOutputs.setLayout(new FormLayout(
            "[80dlu,default]",
            "2*(default, $lgap), default"));

          //---- btnAddOutputs ----
          btnAddOutputs.setText("Add Output");
          controlPanelOutputs.add(btnAddOutputs, CC.xy(1, 1));

          //---- btnRemoveOutputs ----
          btnRemoveOutputs.setText("Remove Output");
          controlPanelOutputs.add(btnRemoveOutputs, CC.xy(1, 3));
        }
        panelControls.add(controlPanelOutputs, CC.xy(3, 1));
      }
      tabbedPane1.addTab("Controls", panelControls);

      //======== panelSettings ========
      {
        panelSettings.setBorder(Borders.DIALOG_BORDER);
        panelSettings.setLayout(new FormLayout(
          "default, $lcgap, default",
          "2*(default, $lgap), default"));

        //---- btnChangeSettings ----
        btnChangeSettings.setText("Change Settings...");
        panelSettings.add(btnChangeSettings, CC.xy(1, 1));
      }
      tabbedPane1.addTab("Settings", panelSettings);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
    
    // ====== Custom Code ======
    UIUtils.increaseScrollSpeed(scrollPane0);
    UIUtils.increaseScrollSpeed(scrollPane1);
    UIUtils.increaseScrollSpeed(scrollPane2);
    UIUtils.increaseScrollSpeed(scrollPane3);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JScrollPane scrollPane0;
  private JPanel panelOverview;
  private JPanel panelInputs;
  private JScrollPane scrollPane1;
  private JTable tableInputs;
  private JPanel controlPanelInputs;
  private JButton btnAddInputs;
  private JButton btnRemoveInputs;
  private JPanel panelOutputs;
  private JScrollPane scrollPane3;
  private JTable tablePoints;
  private JPanel controlPanelPoints;
  private JButton btnAddPoint;
  private JButton btnRemovePoint;
  private JButton btnEditPoint;
  private JPanel panelControls;
  private JScrollPane scrollPane2;
  private JTable tableOutputs;
  private JPanel controlPanelOutputs;
  private JButton btnAddOutputs;
  private JButton btnRemoveOutputs;
  private JPanel panelSettings;
  private JButton btnChangeSettings;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


  private static class CLogicIOValueRenderer extends DefaultTableRenderer {
    
    public CLogicIOValueRenderer(){
      super(new StringValue() {
        
        @Override
        public String getString(Object value) {
          return value == null ? CLogicIOValueEditor.NULL_TEXT : value.toString();
        }
      });
    }
  }
}
