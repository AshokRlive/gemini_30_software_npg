package com.lucy.g3.rtu.config.clogic.voltagemismatch;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.VMISMATCH_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.VMISMATCH_POINT;

/**
 * Implementation of PLTU Logic.
 */
public class VoltageMismatchLogic extends PredefinedCLogic<VoltageMismatchLogicSettings> /* implements ISupportOffLocalRemote */{


  public static final String PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC = "voltageMismatchDelaySec";

  private int voltageMismatchDelaySec = 5; // seconds
  
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.VOLTAGEMISMATCH, "Voltage Mismatch Logic")
      .maxAmount(10)
      //.requireOLR(true)
      .get();

  VoltageMismatchLogic() {
    super(TYPE, VMISMATCH_INPUT.values(), VMISMATCH_POINT.values());
  }

  public void setvoltageMismatchDelaySec(int voltageMismatchDelaySec) {
    if (voltageMismatchDelaySec < 0) {
      log.warn("Cannot set voltageMismatchDelaySec to: " + voltageMismatchDelaySec);
      return;
    }
    Object oldValue = getVoltageMismatchDelaySec();
    this.voltageMismatchDelaySec = voltageMismatchDelaySec;
    firePropertyChange(PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC, oldValue, voltageMismatchDelaySec);
  }

  public int getVoltageMismatchDelaySec() {
    return voltageMismatchDelaySec;
  }


  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

//  @Override
//  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
//    getInput(VMISMATCH_INPUT.VMISMATCH_INPUT_OLR).setValue(offLocalRemotePoint);
//  }

  public CLogicIO<VirtualPoint> getInput(VMISMATCH_INPUT inputItem) {
    return getInputByEnum(inputItem);
  }


  @Override
  protected VoltageMismatchLogicSettings createSettings() {
    return new VoltageMismatchLogicSettings(this);
  }
}
