/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.validate;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * Validator for Control Logic.
 */
public class CLogicValidator extends AbstractValidator<ICLogic> {


  public CLogicValidator(ICLogic target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {
    CLogicIO<VirtualPoint>[] inputs = target.getInputs();
    if (inputs != null) {
      for (CLogicIO<VirtualPoint> io : inputs) {
        io.getValidator().validate();
        result.addAllFrom(io.getValidator().getResult());
      }
    }

    CLogicIO<?>[] outputs = target.getOutputs();
    if (outputs != null) {
      for (CLogicIO<?> io : outputs) {
        io.getValidator().validate();
        result.addAllFrom(io.getValidator().getResult());
      }
    }
    
    ICLogicSettings settings = target.getSettings();
    if (settings != null) {
      IValidator validator = settings.getValidator();
      if (validator != null) {
        result.addAllFrom(validator.validate());
      }
    }
  }

  @Override
  public String getTargetName() {
    return String.format("Control Logic:\"%s\"", target.getName());
  }
}
