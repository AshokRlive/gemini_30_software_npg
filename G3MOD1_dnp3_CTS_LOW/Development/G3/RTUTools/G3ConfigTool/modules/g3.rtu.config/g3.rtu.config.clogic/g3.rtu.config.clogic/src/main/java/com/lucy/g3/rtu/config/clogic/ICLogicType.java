/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;


import com.lucy.g3.xml.gen.common.ControlLogicDef.CONTROL_LOGIC_TYPE;

/**
 * The interface for getting the attribute of a logic type.
 */
public interface ICLogicType {
  /*ID of supported logic type*/
  int OLR             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_OLR            .getValue();
  int FAN             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_FAN_TEST       .getValue();
  int SGL             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_SGL            .getValue();
  int LOGIC_GATE      = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_LGT            .getValue();
  int DOL             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_DOL            .getValue();
  int DSL             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_DUMMYSW        .getValue();
  int BAT_CHARGER     = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_BATTERY        .getValue();
  int CHANGE_OVER     = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_CHANGE_OVER    .getValue();                  
  int SECTIONALISER   = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_SECTIONALISER  .getValue();                  
  int EXT_FPI         = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_EXT_FPI        .getValue();
  int PLTU            = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_PLTU           .getValue();                         
  int VOLTAGEMISMATCH = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_VMISMATCH      .getValue();                         
  int MMAVG           = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_MMAVG          .getValue();                        
  int PCLK            = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_PCLK           .getValue();
  int ACTION          = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_ACTION         .getValue();                         
  int DIGCTRL         = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_DIGCTRLPOINT   .getValue();
  int ANALOGCTRL      = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_ANALOGCTRLPOINT.getValue();
  int FPI_TEST        = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_FPI_TEST       .getValue();
  int FPI_RESET       = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_FPI_RESET      .getValue();
  int THRESHOLD       = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_THR            .getValue();                         
  int IEC61131        = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_IEC61131       .getValue(); 
  int IEC61499        = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_IEC61499       .getValue(); 
  int COUNTER_COMMAND = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_COUNTER_COMMAND.getValue();
  int BTA             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_BTA            .getValue();
  int DTS             = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_DTS            .getValue();
  int TCL_SENSING     = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_TCL_SENSING    .getValue();
  int TCL_TRIPPING    = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_TCL_TRIPPING   .getValue();
  int POWER_SUPPLY    = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_POWER_SUPPLY   .getValue();
  int MOTOR_SUPPLY    = CONTROL_LOGIC_TYPE.CONTROL_LOGIC_TYPE_MOTOR_SUPPLY   .getValue();

  int    getId();
  /**
   * Gets the name of this type.
   * @return
   */
  String getTypeName();
  /**
   * Gets the description of this type.
   * @return
   */
  String getTypeDescription();
  
  /**
   * Gets the default name of the instance in this type.
   * @return
   */
  String getInstanceName();
  int getMaxAmount();
  String toString();
  boolean isOperable();
  OperationMode getOpMode();
  
  /*
   * For some logic such as threshold logic, it is required to show setting dialog once user
   * changes the input value. This is for forcing user to review logic
   * settings as hysteresis replies on the full range value of input.
   */
  boolean requireShowingSettingWhenChangeIO();
  boolean requireDurationWhenTest();
  boolean requireOLR();
  
  /**
   * Indicates if users are allowed to add this logic
   * @return
   */
  boolean allowUserAdd();
  boolean allowUserRemove();
}
  
