/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.dummyswitch;

import java.awt.Component;
import java.util.Arrays;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JList;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;


class DummySwitchSettingsEditor extends AbstractCLogicSettingsEditor {

  /**
   * 
   */
  private static final String LABEL_OPERATION_INTERVAL = "Operation Interval";

  DummySwitchSettingsEditor(DummySwitchSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result,pm, DummySwitchSettings.PROPERTY_INTERVAL,LABEL_OPERATION_INTERVAL);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    SelectionInList<LED_COLOUR> ledColourSelection = new SelectionInList<LED_COLOUR>(
        Arrays.asList(LED_COLOUR.values()),
        pm.getBufferedModel(DummySwitchSettings.PROPERTY_OPEN_COLOUR));
    JComponent comp = BasicComponentFactory.createComboBox(ledColourSelection, new LEDColourCombRender());
    builder.append("Switch Open Colour:", comp);
    builder.nextLine();

    createLongField(LABEL_OPERATION_INTERVAL, DummySwitchSettings.PROPERTY_INTERVAL, Units.MS);

    comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(DummySwitchSettings.PROPERTY_ALLOW_FORCED_OPERATION),
        DummySwitchSettings.FORCED_OPERATION_TEXT);
    comp.setToolTipText(DummySwitchSettings.FORCE_OPERATION_HINT);
    builder.append("", comp);
    builder.nextLine();
    
    return builder.getPanel();
  }

  private static class LEDColourCombRender extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list,
        Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      
      if (value != null && value instanceof LED_COLOUR) {
        setText(((LED_COLOUR) value).getDescription());
      }
      return this;
    }
  }
}

