/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;

/**
 * This class stores Automation Logic settings.
 */
public abstract class AutomationLogicSettings extends AbstractCLogicSettings<AutomationLogic>{

  public static final String PROPERTY_AUTO_START = "autoStart";
  public static final String PROPERTY_AUTO_SCHEME_VERSION = "autoSchemeVersion";

  private boolean       autoStart;
  private BasicVersion  autoSchemeVersion = new BasicVersion(0, 0);
  
  private final AutomationLogicConstantManager constantsManager;

  protected AutomationLogicSettings(AutomationLogic owner){
    super(owner);
    constantsManager = new AutomationLogicConstantManager();
  }
  
  public boolean isAutoStart() {
    return autoStart;
  }

  public void setAutoStart(boolean autoStart) {
    Object oldValue = this.autoStart;
    this.autoStart = autoStart;
    firePropertyChange(PROPERTY_AUTO_START, oldValue, autoStart);
  }
  
  public BasicVersion  getAutoSchemeVersion() {
    return autoSchemeVersion;
  }
  
  public void setAutoSchemeVersion(BasicVersion autoSchemeVersion) {
    if(autoSchemeVersion != null) {
      Object oldValue = this.autoSchemeVersion;
      this.autoSchemeVersion = autoSchemeVersion;
      firePropertyChange(PROPERTY_AUTO_SCHEME_VERSION, oldValue, autoSchemeVersion);
    }
  }

  public AutomationLogicConstantManager getConstantsManager() {
    return constantsManager;
  }
  
  public static Format getVersionFormat() {
      return VersionFormat.INSTANCE;
  }
  
  private static class VersionFormat {
    public final static Format INSTANCE = new Format() {
      @Override
      public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
        BasicVersion version = (BasicVersion) obj;
        toAppendTo.append(version == null ? "N/A" : version.toString());
        return toAppendTo;
      }

      @Override
      public Object parseObject(String source, ParsePosition pos) {
        throw new UnsupportedOperationException("unsupported");
      }
    };
  }

}
