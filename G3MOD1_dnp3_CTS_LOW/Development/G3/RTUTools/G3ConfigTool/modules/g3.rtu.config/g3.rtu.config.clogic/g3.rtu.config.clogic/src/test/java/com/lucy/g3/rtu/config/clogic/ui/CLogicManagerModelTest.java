/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.impl.CLogicManagerImpl;
import com.lucy.g3.rtu.config.clogic.ui.CLogicManagerModel;

public class CLogicManagerModelTest {

  private CLogicManagerModel pm;


  @Before
  public void setUp() throws Exception {
    pm = new CLogicManagerModel(null, new CLogicManagerImpl(null));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAction() {
    String[] actionKeys = {
        CLogicManagerModel.ACTION_KEY_ADD,
        CLogicManagerModel.ACTION_KEY_REMOVE,
        CLogicManagerModel.ACTION_KEY_SETTING,
    };

    for (String key : actionKeys) {
      assertNotNull(pm.getAction(key));
    }
  }
}
