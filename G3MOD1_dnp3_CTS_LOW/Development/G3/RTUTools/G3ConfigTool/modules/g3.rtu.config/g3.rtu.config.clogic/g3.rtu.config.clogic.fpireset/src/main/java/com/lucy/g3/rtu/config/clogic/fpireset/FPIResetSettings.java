/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fpireset;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.FPIResetParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FPIRESET_INPUT;

public class FPIResetSettings extends AbstractCLogicSettings<FPIReset> {
  FPIResetSettings(FPIReset owner) {
    super(owner);
  }
  public static final String PROPERTY_RST_ON_LV_RESTORED = "resetOnLVRestored";
  public static final String PROPERTY_RST_ON_LV_LOST = "resetOnLVLost";

  private boolean resetOnLVRestored = false;
  private boolean resetOnLVLost = false;
  
  public boolean isResetOnLVRestored() {
    return resetOnLVRestored;
  }

  public void setResetOnLVRestored(boolean rstOnLVRestored) {
    Object oldValue = isResetOnLVRestored();
    this.resetOnLVRestored = rstOnLVRestored;
    getOwnerLogic().getInputByEnum(FPIRESET_INPUT.FPIRESET_INPUT_LVFAIL).setOptional(!rstOnLVRestored);
    firePropertyChange(PROPERTY_RST_ON_LV_RESTORED, oldValue, rstOnLVRestored);
  }

  public boolean isResetOnLVLost() {
    return resetOnLVLost;
  }

  public void setResetOnLVLost(boolean resetOnLVLost) {
    Object oldValue = isResetOnLVLost();
    this.resetOnLVLost = resetOnLVLost;
    getOwnerLogic().getInputByEnum(FPIRESET_INPUT.FPIRESET_INPUT_LVFAIL).setOptional(!resetOnLVLost);
    firePropertyChange(PROPERTY_RST_ON_LV_LOST, oldValue, resetOnLVLost);
  }
  
  @Override
  public IValidator getValidator() {
    return null;
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml) {
    FPIResetParamT params = xml.fpiResetParam.first();
    setResetOnLVLost(params.resetFPIOnLVLost.getValue());
    setResetOnLVRestored(params.resetFPIOnLVRestored.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    FPIResetParamT params = xml.fpiResetParam.append();
    params.resetFPIOnLVLost.setValue(isResetOnLVLost());
    params.resetFPIOnLVRestored.setValue(isResetOnLVRestored());
  }
}

