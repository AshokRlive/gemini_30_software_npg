/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.log4j.Logger;

import com.g3schema.ns_clogic.CLogicAnalogPointT;
import com.g3schema.ns_clogic.CLogicBinaryPointT;
import com.g3schema.ns_clogic.CLogicCounterPointT;
import com.g3schema.ns_clogic.CLogicDoubleBinaryPointT;
import com.g3schema.ns_clogic.CLogicInputsT;
import com.g3schema.ns_clogic.CLogicOutputsT;
import com.g3schema.ns_clogic.ControlLogicT;
import com.g3schema.ns_clogic.ControlLogicsT;
import com.g3schema.ns_common.OutputChannelRefT;
import com.g3schema.ns_common.OutputModuleRefT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.xml.gen.common.ControlLogicDef.CONTROL_LOGIC_TYPE;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.OUTPUT_CHANNEL_TYPE;

/**
 * XmlReader for control logic.
 */
public class CLogicXmlReader {

  private Logger log = Logger.getLogger(CLogicXmlReader.class);
  private ValidationResult result;
  private VirtualPointReadSupport support;


  public CLogicXmlReader(VirtualPointReadSupport support) {
    this.support = Preconditions.checkNotNull(support, "support must not be null");
    this.result = support.getResult();
  }

  /**
   * Reads all control logic from XMl to control logic manager.
   */
  public void read(CLogicManager logicManager, ControlLogicsT xmlLogics) throws InvalidConfException {
    //this.logicManager = Preconditions.checkNotNull(logicManager, "logicManager must not be null");
    ArrayList<ICLogic> clogicList = new ArrayList<ICLogic>(20);
    short group;
    String customName;

    /* Create all logic */
    final int logicNum = xmlLogics.clogic.count();
    for (int i = 0; i < logicNum; i++) {
      ControlLogicT xmlLogic = xmlLogics.clogic.at(i);
      CONTROL_LOGIC_TYPE rawType = CONTROL_LOGIC_TYPE.forValue((int) xmlLogic.logicType.getValue());
      //ICLogicType type = ICLogicType.convertEnum(rawType);
      customName = xmlLogic.name.getValue();
      group = (short) xmlLogic.group.getValue();
      if(rawType == null) 
        throw new InvalidConfException("Unrecognized logic type enum:"+xmlLogic.logicType.getValue());
      ICLogic logic = CLogicFactories.getFactory(rawType.getValue()).createLogic();
      if(customName != null && !customName.isEmpty())
        logic.setCustomName(customName);
      
      // Read UUID
      if(xmlLogic.uuid.exists())
        logic.setUUID(xmlLogic.uuid.getValue());
      
      /* Create all logic IOs*/
      createIO(logic,xmlLogic);
      
      clogicList.add(logic);
      logic.setEnabled(xmlLogic.enabled.getValue());

      /* Read logic settings */
      if (xmlLogic.parameters.exists()) {
        try {
          logic.readParamFromXML(xmlLogic.parameters.first());
        } catch (Exception e) {
          String err = "Fail to load parameters for logic:\""
              + logic + "\" cause:" + e.getMessage();
          log.error(err);
          result.addError(err);
        }
      }
      logic.setGroup(group);
      
      /* Workaround: -reinitialise Automation Logic IO*/
//      if(logic instanceof AutomationLogic) {
//        AutomationLogic autoLogic = (AutomationLogic) logic;
//        AutomationLogicTemplate.applyTemplate(autoLogic.getSettings().getAutoSchemeType(), autoLogic);
//        autoLogic.setCustomName(customName); // Bug fix: custom name was changed by "template", we need to change it back to the name in XML. 
//      }
    }

    // Sort by group
    Collections.sort(clogicList, new CLogicComparable());

    /* Add all logic to manager */
    logicManager.addAll(clogicList);
    

    /* Configure logic IO */
    for (int i = 0; i < logicNum; i++) {
      ControlLogicT xmlLogic = xmlLogics.clogic.at(i);
      group = (short) xmlLogic.group.getValue();
      ICLogic logic = logicManager.getLogicByGroup(group);

      CONTROL_LOGIC_TYPE rawType = CONTROL_LOGIC_TYPE.forValue((int) xmlLogic.logicType.getValue());
//      ICLogicType type = ICLogicType.convertEnum(rawType);
      if (logic == null || rawType.name().equals(logic.getType())) {
        log.error("Logic not found in manager. Type: " + rawType + " group: " + group);
        continue;
      }

      /* Configure binary points */
      {
        final int pointInXML = xmlLogic.binaryPoints.count();
        final PseudoBinaryPoint[] points = CLogicUtility.findBinaryPoints(logic.getAllPoints());
        checksPointNumInXMl(logic, points, pointInXML);
        if (points != null) {
          for (int j = 0; j < points.length; j++) {
            if (j < pointInXML) {
              points[j].readFromXML(xmlLogic.binaryPoints.at(j), support);
            }
          }
        }
      }

      /* Configure double binary points */
      {
        final int pointInXML = xmlLogic.dbinaryPoints.count();
        final PseudoDoubleBinaryPoint[] points = CLogicUtility.findDoubleBinaryPoints(logic.getAllPoints());
        checksPointNumInXMl(logic, points, pointInXML);
        if (points != null) {
          for (int j = 0; j < points.length; j++) {
            if (j < pointInXML) {
              points[j].readFromXML(xmlLogic.dbinaryPoints.at(j), support);
            }
          }
        }
      }

      /* Configure analogue points */
      {
        final int pointInXML = xmlLogic.analoguePoints.count();
        final PseudoAnaloguePoint[] points = CLogicUtility.findAnalogPoints(logic.getAllPoints());
        checksPointNumInXMl(logic, points, pointInXML);
        if (points != null) {
          for (int j = 0; j < points.length; j++) {
            if (j < pointInXML) {
              points[j].readFromXML(xmlLogic.analoguePoints.at(j), support);
            }
          }
        }
      }

      /* Configure counter points */
      {
        final int pointInXML = xmlLogic.counterPoints.count();
        final PseudoCounterPoint[] points = CLogicUtility.findCounterPoints(logic.getAllPoints());
        checksPointNumInXMl(logic, points, pointInXML);
        if (points != null) {
          for (int j = 0; j < points.length; j++) {
            if (j < pointInXML) {
              points[j].readFromXML(xmlLogic.counterPoints.at(j), support);
            }
          }
        }
      }
    }
  }
  
  public void configureIO(CLogicManager logicManager, ControlLogicsT xmlLogics) {
    final int logicNum = xmlLogics.clogic.count();
    for (int i = 0; i < logicNum; i++) {

      ControlLogicT xmlLogic = xmlLogics.clogic.at(i);
      short group = (short) xmlLogic.group.getValue();
      ICLogic logic = logicManager.getLogicByGroup(group);

      /* Create inputs. TODO IMPROVE THIS */
      int num = xmlLogic.inputs.count();

      /* Configure inputs */
      CLogicIO<VirtualPoint>[] inputs = logic.getInputs();
      if(inputs == null ) {
        log.fatal(logic.getType().getTypeName() + " not initialised!");
      }
      
      for (int j = 0; j < num; j++) {
        if (j < inputs.length) {
          configureInput(inputs[j], xmlLogic.inputs.at(j));
        } else {
          log.fatal("Input not found at:" + j + " in logic:" + logic);
        }
      }

      /* Configure outputs */
      num = xmlLogic.outputs.count();
      CLogicIO<?>[] outputs = logic.getOutputs();
      for (int j = 0; j < num; j++) {
        if (outputs != null && j < outputs.length) {
          configureOutput(logicManager, outputs[j], xmlLogic.outputs.at(j));
        } else {
          log.fatal("Input not found at:" + j + " in logic:" + logic);
        }
      }
    }
  }

  private void createIO(ICLogic logic, ControlLogicT xml_logic) {
    // Create Inputs
    IInputsManager inputMgr = logic.getInputsManager();
    int num = xml_logic.inputs.count();
    
    if (inputMgr != null) {
      VirtualPointType inputType = null; 
      for (int j = 0; j < num; j++) {
        CLogicInputsT xml_input = xml_logic.inputs.at(j);
        if (xml_input.type.exists())
          inputType = VirtualPointType.values()[xml_input.type.getValue()];

        if (inputType == null)
          inputType = VirtualPointType.BINARY_INPUT;
        
        CLogicIO<?> input = inputMgr.getByIndex(j);
        if(input == null)
          inputMgr.addInputs(new VirtualPointType[]{inputType}, 1, false, true, true);// FIXME cannot read "mandatory" cause it is not stored in config xml
        else {
          //TODO  check type identical
        }
          
      }
    }
    
    // Create Outputs
    IOutputsManager<?> outputMgr = logic.getOutputsManager();
    num = xml_logic.outputs.count();
    if (outputMgr != null) {
      outputMgr.addOutput(num - outputMgr.getSize(), true); 
    }
    
    // Create Points
    ICLogicPointsManager pointsMgr = logic.getPointsManager();
    ArrayList<IPseudoPoint> points = new ArrayList<>();
    if (pointsMgr != null) {
      int count = xml_logic.binaryPoints.count();
      for (int j = 0; j < count; j++) {
        CLogicBinaryPointT xmlPoint = xml_logic.binaryPoints.at(j);
        String desp = xmlPoint.description.exists() ? xmlPoint.description.getValue() :"Unnamed Virtual Point";
        int id = (int) xmlPoint.id.getValue();
        IPseudoPoint newPoint = pointsMgr.getByID(id);
        if(newPoint == null)
        newPoint = PseudoPointFactory.createPseudoPoint(VirtualPointType.BINARY_INPUT,
            id, logic, desp);
        points.add(newPoint);
      }
      
      count = xml_logic.dbinaryPoints.count();
      for (int j = 0; j < count; j++) {
        CLogicDoubleBinaryPointT xmlPoint = xml_logic.dbinaryPoints.at(j);
        String desp = xmlPoint.description.exists() ? xmlPoint.description.getValue() :"Unnamed Virtual Point";
        int id = (int) xmlPoint.id.getValue();
        IPseudoPoint newPoint = pointsMgr.getByID(id);
        if(newPoint == null)
          newPoint = PseudoPointFactory.createPseudoPoint(VirtualPointType.DOUBLE_BINARY_INPUT,
            (int)xmlPoint.id.getValue(), logic, desp);
        points.add(newPoint);
      }
      
      count = xml_logic.analoguePoints.count();
      for (int j = 0; j < count; j++) {
        CLogicAnalogPointT xmlPoint = xml_logic.analoguePoints.at(j);
        String desp = xmlPoint.description.exists() ? xmlPoint.description.getValue() :"Unnamed Virtual Point";
        int id = (int) xmlPoint.id.getValue();
        IPseudoPoint newPoint = pointsMgr.getByID(id);
        if(newPoint == null)
          newPoint = PseudoPointFactory.createPseudoPoint(VirtualPointType.ANALOGUE_INPUT,
            (int)xmlPoint.id.getValue(), logic, desp);
        points.add(newPoint);
      }

      count = xml_logic.counterPoints.count();
      for (int j = 0; j < count; j++) {
        CLogicCounterPointT xmlPoint = xml_logic.counterPoints.at(j);
        String desp = xmlPoint.description.exists() ? xmlPoint.description.getValue() :"Unnamed Virtual Point";
        int id = (int) xmlPoint.id.getValue();
        IPseudoPoint newPoint = pointsMgr.getByID(id);
        if(newPoint == null)
          newPoint = PseudoPointFactory.createPseudoPoint(VirtualPointType.COUNTER,
            (int)xmlPoint.id.getValue(), logic, desp);
        points.add(newPoint);
      }

      pointsMgr.setPoints(points);
    }
  }

  @SuppressWarnings("unchecked")
  private void configureOutput(CLogicManager logicManager, @SuppressWarnings("rawtypes") CLogicIO output, CLogicOutputsT xml_output) {
    
    if (output.isAllowChangeLabel() && xml_output.name.exists())
      output.setLabel(xml_output.name.getValue());
    
    
    if (xml_output.outputChannel.exists()) {
      IChannel value = getOutputChannelByRef(xml_output.outputChannel.first());
      output.setValue(value);

    } else if (xml_output.outputModule.exists()) {
      OutputModuleRefT xml_module = xml_output.outputModule.first();
      Module module = support.getModuleByRef(xml_module);

      if (xml_module.switchID.exists() && module instanceof ISwitchModule) {
        long swid = xml_module.switchID.getValue();
        SwitchIndex index = SwitchIndex.forValue((int) swid);
        ISwitchModule switchModule = (ISwitchModule) module;
        output.setValue(switchModule.getSwitchOutput(index));
      } else {
        output.setValue(module);
      }

    } else if (xml_output.outputLogic.exists()) {
      ICLogic logic = logicManager.getLogicByGroup((short) xml_output.outputLogic.first().getValue());
      output.setValue(logic);
    } else if(xml_output.outputPoint.exists()) {
      int group = (int)xml_output.outputPoint.first().pointGroup.getValue();
      int id = (int)xml_output.outputPoint.first().pointID.getValue();
      VirtualPoint vpoint = support.getVirtualPoint(group,id);
      output.setValue(vpoint);
    }

  }

  private void configureInput(CLogicIO<VirtualPoint> input, CLogicInputsT xml_input) {
    if (/*input.isAllowChangeLabel() && */xml_input.name.exists())
      if(input.isAllowChangeLabel())
        input.setLabel(xml_input.name.getValue());

    if (xml_input.inputPoint.exists()) {
      try {
        input.setValue(support.getVirtualPointByRef(xml_input.inputPoint.first()));
      } catch (Exception e) {
        result.addError("Fail to configure Logic input:" + input + " cause:" + e.getMessage());
      }
    }
  }

  private IChannel getOutputChannelByRef(OutputChannelRefT xml_ref) {
    OUTPUT_CHANNEL_TYPE rawType = OUTPUT_CHANNEL_TYPE.forValue((int) xml_ref.channelType.getValue());
    ChannelType type = ChannelType.convertEnum(rawType);

    return support.getChannelByRef(xml_ref, type);
  }

  private void checksPointNumInXMl(ICLogic owner, IPseudoPoint[] points, int actualNumInXML) {
    int expected = 0;
    if (points != null) {
      expected = points.length;
    }

    if (expected != actualNumInXML) {
      log.warn("invalid points number in:" + owner + " expected points number:" + expected + " but actual:"
          + actualNumInXML);
    }
  }


  private static class CLogicComparable implements Comparator<ICLogic> {

    @Override
    public int compare(ICLogic c1, ICLogic c2) {
      int g1 = c1.getGroup();
      int g2 = c2.getGroup();
      return (g1 - g2);
    }
  }
}
