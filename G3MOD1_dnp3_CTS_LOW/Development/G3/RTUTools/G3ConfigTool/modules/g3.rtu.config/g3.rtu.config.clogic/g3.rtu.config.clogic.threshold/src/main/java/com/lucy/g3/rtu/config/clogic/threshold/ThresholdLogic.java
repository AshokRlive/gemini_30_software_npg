/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.threshold;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.THR_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.THR_POINT;

/**
 * Implementation of Threshold Logic.
 */
public class ThresholdLogic extends PredefinedCLogic<ThresholdLogicSettings> {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.THRESHOLD, "Threshold Logic")
      .get();

  public ThresholdLogic() {
    super(TYPE, THR_INPUT.values(), THR_POINT.values());

    // Observe input change for updating full range value
    final CLogicIOPoint valueInput = getValueInput();
    valueInput.addPropertyChangeListener(CLogicIO.PROPERTY_VALUE, new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        VirtualPoint vp = valueInput.getValue();
        if (vp != null && vp instanceof AnaloguePoint) {
          ThresholdLogic.this.getSettings().setFullRangeValue(((AnaloguePoint) vp).getFullRangeValue());
        }
      }
    });
  }

  CLogicIOPoint getValueInput() {
    return getInputByEnum(THR_INPUT.THR_INPUT_VALUE);
  }

  public void setInput(VirtualPoint point) {
    getValueInput().setValue(point);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  protected ThresholdLogicSettings createSettings() {
    return new ThresholdLogicSettings(this);
  }

  
}
