/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.dummyswitch;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.DummySwitchParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModuleSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;

public class DummySwitchSettings extends AbstractCLogicSettings<DummySwitch> {
  DummySwitchSettings(DummySwitch owner) {
    super(owner);
  }

  public static final String PROPERTY_OPEN_COLOUR = "openColour";
  public static final String PROPERTY_INTERVAL = "interval";
  public static final String PROPERTY_ALLOW_FORCED_OPERATION = "allowForcedOperation";

  public static final String FORCE_OPERATION_HINT = ISwitchModuleSettings.FORCE_OPERATION_HINT;

  public static final String FORCED_OPERATION_TEXT = ISwitchModuleSettings.FORCED_OPERATION_TEXT;

  private LED_COLOUR openColour = LED_COLOUR.LED_COLOUR_GREEN;

  /* Allowed switch operating interval (ms) */
  private long interval = 10;

  private boolean allowForcedOperation;

  
  public LED_COLOUR getOpenColour() {
    return openColour;
  }

  public void setOpenColour(LED_COLOUR openColour) {
    if (openColour == null) {
      return;
    }

    Object oldValue = getOpenColour();
    this.openColour = openColour;
    firePropertyChange(PROPERTY_OPEN_COLOUR, oldValue, openColour);
  }


  public long getInterval() {
    return interval;
  }

  public void setInterval(long interval) {
    if (interval < 0) {
      log.error("Cannot set interval value: " + interval);
      return;
    }

    Object oldValue = getInterval();
    this.interval = interval;
    firePropertyChange(PROPERTY_INTERVAL, oldValue, interval);
  }

  public boolean isAllowForcedOperation() {
    return allowForcedOperation;
  }

  public void setAllowForcedOperation(boolean allowForcedOperation) {
    Object oldValue = this.allowForcedOperation;
    this.allowForcedOperation = allowForcedOperation;
    firePropertyChange(PROPERTY_ALLOW_FORCED_OPERATION, oldValue, allowForcedOperation);
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    DummySwitchParamT xml = xml_param.dummySwitchParam.first();
    setInterval(xml.interval.getValue());
    setOpenColour(LED_COLOUR.valueOf(xml.openColour.getValue()));
    setAllowForcedOperation(xml.allowForcedOperation.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    DummySwitchParamT xml_DSL = xml.dummySwitchParam.append();
    xml_DSL.openColour.setValue(getOpenColour().name());
    xml_DSL.interval.setValue(getInterval());
    xml_DSL.allowForcedOperation.setValue(isAllowForcedOperation());
  }


  @Override
  public IValidator getValidator() {
    // TODO Auto-generated method stub
    return null;
  }
}

