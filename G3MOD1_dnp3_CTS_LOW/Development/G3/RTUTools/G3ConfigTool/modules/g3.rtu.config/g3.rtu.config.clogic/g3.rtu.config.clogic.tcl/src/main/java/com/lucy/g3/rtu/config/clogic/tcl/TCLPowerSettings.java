
package com.lucy.g3.rtu.config.clogic.tcl;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettings.PowerFlowDirection;

public class TCLPowerSettings {
  public static final Class<? extends Number> POWER_VALUE_TYPE = Float.class;
  
  private String powerValueStr = "10";
  
  private PowerFlowDirection powerDirection = PowerFlowDirection.LOAD;
  
  public float getPowerValueAsFloat() {
    try {
      return POWER_VALUE_FORMAT.parse(powerValueStr).floatValue();
    } catch (ParseException e) {
      e.printStackTrace();
      return 0.0F;
    }
    
  }
  
  public double getPowerValueAsDouble() {
    try {
      return POWER_VALUE_FORMAT.parse(powerValueStr).doubleValue();
    } catch (ParseException e) {
      e.printStackTrace();
      return 0.0D;
    }
  }
  
  public void setPowerValue(float powerValue) {
    this.powerValueStr = POWER_VALUE_FORMAT.format(powerValue);
  }
  
  public void setPowerValue(double powerValue) {
    this.powerValueStr = POWER_VALUE_FORMAT.format(powerValue);
  }
  
  public PowerFlowDirection getPowerDirection() {
    return powerDirection;
  }
  
  public void setPowerDirection(PowerFlowDirection powerDirection) {
    this.powerDirection = powerDirection;
  }
  
  
  private final static DecimalFormat POWER_VALUE_FORMAT = new DecimalFormat("###.######");
  public  final static NumberFormatter POWER_VALUE_FORMATTER;
  
  static {
    POWER_VALUE_FORMAT.setRoundingMode(RoundingMode.HALF_UP);
    POWER_VALUE_FORMAT.setMinimumFractionDigits(2);
    
    NumberFormatter formatter = new NumberFormatter(POWER_VALUE_FORMAT);
    formatter.setValueClass(POWER_VALUE_TYPE);
    formatter.setMinimum(0.0F);
    formatter.setMaximum(Float.MAX_VALUE);
    POWER_VALUE_FORMATTER = formatter;
  }
}

