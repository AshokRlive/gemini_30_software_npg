/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.channel.util.ChannelFinder;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.module.iomodule.manager.ChannelRepository;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.rtu.config.virtualpointsource.ui.VirtualPointSourceSelector;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * The implementation of control logic IO which is for configuring a mapped
 * channel.
 */
public final class CLogicIOChannel extends AbstractCLogicIO<IChannel> {

  private final ChannelType[] supportedChlTypes;

  private final MODULE[] supportedModuleTypes;

  private final Enum<?>[] supportedEnum;


  public CLogicIOChannel(ICLogic owner, String name, boolean optional, boolean enabled) {
    this(owner, name, optional, enabled, null, null, null);
  }

  /**
   * Create an logic IO that is associated with a channel.
   *
   * @param owner
   *          The owner control logic of this IO
   * @param label
   *          The name of this IO
   * @param supportedTypes
   *          the type of channel which is allowed to be mapped to this IO.
   */
  public CLogicIOChannel(ICLogic owner, String label, boolean optional, boolean enabled,
      ChannelType[] supportedTypes, MODULE[] supportedModuleTypes, Enum<?>[] supportedEnum) {
    super(owner, IODirection.OUTPUT, label, false, false, optional, enabled);
    this.supportedChlTypes = supportedTypes;
    this.supportedModuleTypes = supportedModuleTypes;
    this.supportedEnum = supportedEnum;
  }

  @Override
  public void setValue(final IChannel newChannel) throws CLogicOutputConflictException {
    if(newChannel == getValue()) {
      return;
    }
    
    // Check if the channel has already been used by other objects
//    if (newChannel != null) {
//      if (newChannel.isConnectedToTarget(NodeType.CLOGIC)) { 
//        throw new CLogicOutputConflictException("The output channel \""
//            + newChannel.getDescription()
//            + "\" has been used by other control logic");
//      }
//    }

    super.setValue(newChannel);
  }

  @Override
  public void chooseValue(Frame parent) {
    Object selection = showChooser(parent);
    if(selection == null || selection instanceof IChannel) {
      try {
        setValue((IChannel) selection);
      }catch(Exception e) {
        MessageDialogs.error(e.getMessage(), null);
      }
    }
  }
  
  @Override
  public Object showChooser(Frame parent) {
    Collection<IChannel> chs = getAvailableSources();

    String descript = "";
    if (supportedChlTypes != null) {
      descript = Arrays.toString(supportedChlTypes);
    }

    VirtualPointSourceSelector chooser = new VirtualPointSourceSelector(parent, getValue(), 
        new ArrayList<IVirtualPointSource>(chs), null, descript);
    chooser.showDialog();
    
    if(chooser.hasBeenAffirmed()) {
      return chooser.getSelection();
    }else {
      return CHOOSER_CANCELLED;
    }
  }

  @Override
  public Collection<IChannel> getAvailableSources() {
     IConfig config = ConfigManager.getInstance().getLocalConfig();
     ChannelRepository repo = config == null ? null
             :(ChannelRepository) config.getConfigModule(ChannelRepository.CONFIG_MODULE_ID);
     
    if (repo != null) {
      Collection<IChannel> sources = repo.getAllChannels(supportedModuleTypes, supportedChlTypes);
      return supportedEnum != null 
          ? ChannelFinder.findChannelsByEnum(sources, supportedEnum)
          : sources;
    }else
      return new ArrayList<IChannel>(0);
  }


  @Override
  public boolean isAcceptable(IChannel ioValue) {
    if (ioValue == null) {
      return true;
    }

    // Check against supportedTypes
    if (supportedChlTypes != null) {
      boolean validType = false;
      for (int i = 0; i < supportedChlTypes.length; i++) {
        if (supportedChlTypes[i] == ioValue.getType()) {
          validType = true;
          break;
        }
      }
      if (validType == false) {
        return false;
      }
    }

    // Check against channel's enum
    if (supportedEnum != null) {
      boolean validEnum = false;
      for (int i = 0; i < supportedEnum.length; i++) {
        if (ioValue instanceof IChannel
            && supportedEnum[i] == ioValue.getEnum()) {
          validEnum = true;
          break;
        }
      }
      if (validEnum == false) {
        return false;
      }
    }

    // Check against owner module type
    if (supportedModuleTypes != null) {
      boolean validModuleType = false;
      for (int i = 0; i < supportedModuleTypes.length; i++) {
        if (supportedModuleTypes[i] == ioValue.getOwnerModule().getType()) {
          validModuleType = true;
          break;
        }
      }
      if (validModuleType == false) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String getSummary() {
    IChannel channel = getValue();
    return "Output Channel: "
        + (channel == null ? "None" : channel.getName());
  }

  @Override
  public Class<IChannel> getSourceClass() {
    return IChannel.class;
  }

}
