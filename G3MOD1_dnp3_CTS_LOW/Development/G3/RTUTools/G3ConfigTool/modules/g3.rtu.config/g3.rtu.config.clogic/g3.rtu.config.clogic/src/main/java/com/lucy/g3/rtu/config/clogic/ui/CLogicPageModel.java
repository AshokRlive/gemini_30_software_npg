/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.common.collect.ObservableList;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.model.IDeletable;
import com.lucy.g3.rtu.config.shared.model.impl.DeletableItem;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.VirtualPointDialog;

/**
 * Presentation model of CLogicPage.
 */
public class CLogicPageModel extends Model {

  public static final String ACTION_ADD_INPUT = "addInput";
  public static final String ACTION_REMOVE_INPUT = "removeInput";
  public static final String ACTION_ADD_OUTPUT = "addOutput";
  public static final String ACTION_REMOVE_OUTPUT = "removeOutput";
  public static final String ACTION_ADD_POINT = "addPoint";
  public static final String ACTION_REMOVE_POINT = "removePoint";
  public static final String ACTION_EDIT_POINT = "editPoint";

  public static final String PROPERTY_ALLOW_CHANGE_INPUTS = "allowChangeInputs";
  public static final String PROPERTY_ALLOW_CHANGE_OUTPUTS = "allowChangeOutputs";
  public static final String PROPERTY_ALLOW_CHANGE_POINTS = "allowChangePoints";
  
  public final static int COLUMN_INDEX_INPUT_VALUE = InputsTableModel.COLUMN_VALUE;
  public final static int COLUMN_INDEX_OUTPUT_VALUE = OutputsTableModel.COLUMN_VALUE;
  
  private static Logger log = Logger.getLogger(CLogicPageModel.class);
  
  private final ICLogic clogic;

  private final IOutputsManager<?> outputsManager;
  private final IInputsManager inputsManager;
  private final ICLogicPointsManager pointsManager;

  private final ListModel<?> outputsListModel;
  private final ListModel<?> inputsListModel;
  private final ListModel<?> pointsListModel;
  
  private TableModel outputsTableModel;
  private TableModel inputsTableModel;
  private TableModel pointsTableModel;

  private final MultiSelectionInList<?> outputsSelection;
  private final MultiSelectionInList<?> inputsSelection;
  private final MultiSelectionInList<?> pointsSelection;

  private final MultiListSelectionAdapter<?> outputsSelectionModel;
  private final MultiListSelectionAdapter<?> inputsSelectionModel;
  private final MultiListSelectionAdapter<?> pointsSelectionModel;

  private ApplicationActionMap actions;

  private JFrame parent = WindowUtils.getMainFrame();


  public CLogicPageModel(ICLogic clogic) {
    this.clogic = Preconditions.checkNotNull(clogic, "logic must not be null");
    outputsManager = clogic.getOutputsManager();
    inputsManager = clogic.getInputsManager();
    pointsManager = clogic.getPointsManager();
    
    // Update action state when settings changes.
    ICLogicSettings settings = clogic.getSettings();
    if(settings != null) {
      settings.addPropertyChangeListener(new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          updateActionEnableState();
        }
      });
    }
    
    if (outputsManager != null)
      outputsListModel = outputsManager.getListModel();
    else
      outputsListModel = createListModel(clogic.getOutputs());
    
    if (inputsManager != null)
      inputsListModel = inputsManager.getListModel();
    else {
      inputsListModel = createListModel(clogic.getInputs());
    }
    
    if (pointsManager != null)
      pointsListModel = pointsManager.getListModel();
    else
      pointsListModel = createListModel(clogic.getAllPoints());

    outputsSelection = new MultiSelectionInList<>(outputsListModel);
    inputsSelection = new MultiSelectionInList<>(inputsListModel);
    pointsSelection = new MultiSelectionInList<>(pointsListModel);

    outputsSelectionModel = new MultiListSelectionAdapter(outputsSelection);
    inputsSelectionModel = new MultiListSelectionAdapter(inputsSelection);
    pointsSelectionModel = new MultiListSelectionAdapter(pointsSelection);
  }

  private void updateActionEnableState() {
    boolean value;
    
    value = isAllowChangeInputs();
    firePropertyChange(PROPERTY_ALLOW_CHANGE_INPUTS, !value, value);
    
    value = isAllowChangeOutputs();
    firePropertyChange(PROPERTY_ALLOW_CHANGE_OUTPUTS,!value, value);
    
    value = isAllowChangePoints();
    firePropertyChange(PROPERTY_ALLOW_CHANGE_POINTS, !value, value);
  }
  
  
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ALLOW_CHANGE_INPUTS)
  public void addInput() {
    if (inputsManager == null)
      return;
    
    final int maxNum = inputsManager.getMaximumSize() - inputsManager.getSize();
    
    if(maxNum <= 0) {
      JOptionPane.showMessageDialog(WindowUtils.getMainFrame(), 
          "Cannot add more inputs!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    

    VirtualPointType selectType = (VirtualPointType) JOptionPane.showInputDialog(WindowUtils.getMainFrame(),
        "Please select a type for the new input", "Input Type", JOptionPane.QUESTION_MESSAGE, null,
        inputsManager.getSupportedInputTypes(), null);
    
    if (selectType != null) {
      int addNum = askUserInputNumber("add", maxNum);
  
      if (addNum > 0) {
        inputsManager.addInputs(new VirtualPointType[]{selectType}, addNum, false, true, true);
      }
    }
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ALLOW_CHANGE_INPUTS)
  public void removeInput() {
    if (inputsManager == null || checkHasSelection(inputsSelection) == false || confirmRemove() == false)
      return;
    
    Collection<?> sels = new ArrayList<>(inputsSelection.getSelection());
    Collection<?> undeleted = new ArrayList<>(sels);
    sels = DeletableItem.findDeleteable(sels);
    undeleted.removeAll(sels);
    inputsManager.removeAll((Collection<CLogicIO<?>>) sels);
    showUndeletedItems(parent, undeleted);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ALLOW_CHANGE_OUTPUTS)
  public void addOutput() {
    if (outputsManager == null)
      return;

    int maxNum = IOutputsManager.MAX_SIZE - outputsManager.getSize();
    int addNum = askUserInputNumber("add", maxNum);
    outputsManager.addOutput(addNum, true);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ALLOW_CHANGE_OUTPUTS)
  public void removeOutput() {
    if (outputsManager == null || checkHasSelection(outputsSelection) == false ||confirmRemove() == false)
      return;
    
    Collection<?> sels = new ArrayList<>(outputsSelection.getSelection());
    Collection<?> undeleted = new ArrayList<>(sels);
    sels = DeletableItem.findDeleteable(sels);
    undeleted.removeAll(sels);
    outputsManager.removeAll((Collection<CLogicIO<?>>) sels);
    showUndeletedItems(parent, undeleted);
  }

  private static void showUndeletedItems(JFrame parent, Collection<?> items) {
    StringBuilder undeletedStr = new StringBuilder();
    
    for (Object item : items) {
      if (item instanceof IDeletable && ((IDeletable)item).isDeleted() == false) {
        undeletedStr.append("\"");
        undeletedStr.append(((IDeletable)item).getName());
        undeletedStr.append("\"");
        undeletedStr.append("\n");
      }
    }
    
    if (undeletedStr.length() > 0) {
      JOptionPane.showMessageDialog(parent,
          "The following items cannot be removed:\n" + undeletedStr);
    }
  }

  
  private boolean confirmRemove() {
    int rc = JOptionPane.showConfirmDialog(parent, 
        "Do you want to remove the selections?", 
        "Remove", JOptionPane.YES_NO_CANCEL_OPTION);
    return rc == JOptionPane.YES_OPTION;
  }
  
  private boolean checkHasSelection(MultiSelectionInList<?> selectionModel) {
    if (selectionModel.getSelection().isEmpty()) {
      JOptionPane.showMessageDialog(parent, "Please select some items first!", "No selection",
          JOptionPane.INFORMATION_MESSAGE);
      return false;
    }
    return true;
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ALLOW_CHANGE_POINTS)
  public void addPoint() {
    if (pointsManager == null)
      return;
    
    VirtualPointType selectType = (VirtualPointType) JOptionPane.showInputDialog(WindowUtils.getMainFrame(),
        "Please select a type for the new point", "Point Type", JOptionPane.QUESTION_MESSAGE, null,
        VirtualPointType.values(), null);
    
    if (selectType != null) {
      int maxNum = ICLogicPointsManager.MAX_SIZE - pointsManager.getSize();
      int addNum = askUserInputNumber("add", maxNum);
      
      pointsManager.addPoint(selectType, true, addNum);
    }
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ALLOW_CHANGE_POINTS)
  public void removePoint() {
    if (pointsManager == null || checkHasSelection(pointsSelection) == false ||  confirmRemove() == false)
      return;

    Collection<?> sels = new ArrayList<>(pointsSelection.getSelection());
    Collection<?> undeleted = new ArrayList<>(sels);
    sels = DeletableItem.findDeleteable(sels);
    undeleted.removeAll(sels);
    pointsManager.removeAll((Collection<IPseudoPoint>) sels);
    showUndeletedItems(parent, undeleted);
  }

  @org.jdesktop.application.Action
  public void editPoint() {
    ObservableList<?> selections = pointsSelection.getSelection();
    if (selections.size() > 0)
      VirtualPointDialog.edit(WindowUtils.getMainFrame(), null, (VirtualPoint) selections.get(0));
  }

  public TableModel getOutputsTableModel() {
    if (outputsTableModel == null)
      outputsTableModel = new OutputsTableModel(outputsListModel);
    return outputsTableModel;
  }

  public TableModel getInputsTableModel() {
    if (inputsTableModel == null) {
      inputsTableModel = new InputsTableModel(inputsListModel);
    }
    return inputsTableModel;
  }

  public TableModel getPointsTableModel() {
    if (pointsTableModel == null) {
      pointsTableModel = new PointsTableModel(pointsListModel);
    }
    return pointsTableModel;
  }
  
  public MultiSelectionInList<?> getOutputsSelection() {
    return outputsSelection;
  }

  
  public MultiSelectionInList<?> getInputsSelection() {
    return inputsSelection;
  }

  public MultiSelectionInList<?> getPointsSelection() {
    return pointsSelection;
  }

  public MultiListSelectionAdapter<?> getOutputsSelectionModel() {
    return outputsSelectionModel;
  }

  public MultiListSelectionAdapter<?> getInputsSelectionModel() {
    return inputsSelectionModel;
  }

  public MultiListSelectionAdapter<?> getPointsSelectionModel() {
    return pointsSelectionModel;
  }

  public void setOutputsTableModel(TableModel outputsTableModel) {
    Object oldValue = this.outputsTableModel;
    this.outputsTableModel = outputsTableModel;
    firePropertyChange("outputsTableModel", oldValue, outputsTableModel);
  }

  public Action getAction(String actionKey) {
    if (actions == null)
      actions = Application.getInstance().getContext().getActionMap(this);
    return actions.get(actionKey);
  }


  @SuppressWarnings("unchecked")
  private static <T> ListModel<T> createListModel(T[] ioArray) {
    ArrayListModel<T> listModel = new ArrayListModel<>();
    if (ioArray != null)
      listModel.addAll(Arrays.asList(ioArray));
    return listModel;
  }

  private int askUserInputNumber(String actionName, int maxInput) {
    int number = -1;

    /* Ask user for the amount of inputs to be added */
    do {
      String ret = JOptionPane.showInputDialog(parent,
          "How many do you want to " + actionName + "?",
          "Select Quantity",
          JOptionPane.QUESTION_MESSAGE);

      if (ret == null) {
        break; // User cancelled
      }

      try {
        number = Integer.parseInt(ret);
      } catch (Exception e1) {
        // Nothing to do
      }

      if (number <= 0 || number > maxInput) {
        JOptionPane.showMessageDialog(parent,
            String.format("Please input a valid number [%d,%d]!", 1, maxInput),
            "Invalid",
            JOptionPane.ERROR_MESSAGE);
      }

    } while (number <= 0 || number > maxInput);
    return number;
  }
  
  public boolean isAllowChangeInputs() {
    return inputsManager != null && inputsManager.isAllowUserChange();
  }

  public boolean isAllowChangeOutputs() {
    return outputsManager != null && outputsManager.isAllowUserChange();
  }

  public boolean isAllowChangePoints() {
    return pointsManager != null&& pointsManager.isAllowUserChange();
  }

  private static class OutputsTableModel extends AbstractTableAdapter<CLogicIO<Object>> {

    private static final String[] COLUMNS = { "Index", "Label", "Value" };
    
    public final static int COLUMN_INDEX = 0;
    public final static int COLUMN_LABEL = 1;
    public final static int COLUMN_VALUE = 2;


    public OutputsTableModel(ListModel<?> outputsListModel) {
      super(outputsListModel, COLUMNS);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      CLogicIO<?> output = getRow(rowIndex);
      switch (columnIndex) {
      case COLUMN_INDEX:
        return rowIndex;
      
      case COLUMN_LABEL:
        return output.getLabel();
      case COLUMN_VALUE:
        return output.getValue();

      default:
        break;
      }
      return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      CLogicIO<Object> item = getRow(rowIndex);
      
      if (columnIndex == COLUMN_LABEL) {
        item.setLabel((String)aValue);
      } else if (columnIndex == COLUMN_VALUE) {
        try{
          item.setValue(aValue);
        }catch (Throwable e){
          MessageDialogs.error("Cannot set value. See details.", e);
        }
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      CLogicIO<?> item = getRow(rowIndex);
      
      if (columnIndex == COLUMN_LABEL)
        return item.isAllowChangeLabel();
      
      else if (columnIndex == COLUMN_VALUE)
        return true;
      
      else
        return false;
    }

  }

  private static class InputsTableModel extends AbstractTableAdapter<CLogicIO<VirtualPoint>> {

    private static final String[] COLUMNS = {"Index", "Label", "Type", "Value"};
    public final static int COLUMN_INDEX = 0;
    public final static int COLUMN_LABEL = 1;
    public final static int COLUMN_TYPE = 2;
    public final static int COLUMN_VALUE = 3;

    private InputsTableModel(ListModel<?> inputsListModel) {
      super(inputsListModel, COLUMNS);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      CLogicIOPoint input = (CLogicIOPoint) getRow(rowIndex);
      switch (columnIndex) {
      case COLUMN_INDEX:
        return rowIndex;
      case COLUMN_LABEL:
        return input.getLabel();
      case COLUMN_VALUE:
        return input.getValue();
      case COLUMN_TYPE:
        return input.getSupportedType(); //Arrays.toString(input.getSupportedType());
      default:
        break;
      }
      return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      CLogicIO<VirtualPoint> item = getRow(rowIndex);
      
      if (columnIndex == COLUMN_LABEL) {
        item.setLabel((String)aValue);
      } else if (columnIndex == COLUMN_VALUE) {
        try{
          item.setValue((VirtualPoint)aValue);
        }catch (Throwable e){
          log.error(e);
          
          
          MessageDialogs.error("Cannot set value. See details.", e);
        }
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      CLogicIO<?> item = getRow(rowIndex);
      
      if (columnIndex == COLUMN_LABEL)
        return item.isAllowChangeLabel();
      
      else if (columnIndex == COLUMN_VALUE)
        return true;
      
      else
        return false;
    }
  }

  private static class PointsTableModel extends AbstractTableAdapter<IPseudoPoint> {

    private static final String[] COLUMNS = { "ID","Type","Description",  };
    private final static int COLUMN_ID = 0;
    private final static int COLUMN_TYPE = 1;
    private final static int COLUMN_DESCRIPT = 2;

    public PointsTableModel(ListModel<?> outputsListModel) {
      super(outputsListModel, COLUMNS);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      IPseudoPoint point = getRow(rowIndex);
      switch (columnIndex) {
      case COLUMN_ID:
        return point.getGroupID();
      case COLUMN_TYPE:
        return point.getType().getName();
      case COLUMN_DESCRIPT:
        return point.getDescription();
      default:
        break;
      }
      return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      IPseudoPoint item = getRow(rowIndex);
      
      if (columnIndex == COLUMN_DESCRIPT) {
        item.setDescription((String)aValue);
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      IPseudoPoint item = getRow(rowIndex);
      
      return columnIndex == COLUMN_DESCRIPT && item.isAllowChangeDescription();
    }
  }
 
}
