/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;

/**
 * Utility for finding control logic from a collection of control logic.
 */
public class CLogicFinder {

  public static <LogicT>Collection<LogicT> findCLogicByClass(Collection<ICLogic> clogics, Class<LogicT> logicClass) {
    if (clogics == null) {
      return null;
    }

    ArrayList<LogicT> foundLogic = new ArrayList<>();
    for (ICLogic logic : clogics) {
      if (logic != null && logic.getClass() == logicClass) {
        foundLogic.add((LogicT)logic);
      }
    }
    return foundLogic;
    
  }
  
  public static Collection<ICLogic> findCLogicByType(Collection<ICLogic> clogics, int... typeIds) {
    if (clogics == null) {
      return null;
    }

    ArrayList<ICLogic> foundLogic = new ArrayList<ICLogic>();
    if (typeIds != null) {
      for (ICLogic logic : clogics) {
        for (int typeId : typeIds) {
          if (logic != null && logic.getType().getId() == typeId) {
            foundLogic.add(logic);
          }
        }
      }
    }
    return foundLogic;
  }
  

  static <T extends VirtualPoint> T findPointByEnum(T[] points, ILogicPointEnum enumItem) {
    if (enumItem == null) {
      return null;
    }

    if (points != null) {
      for (T p : points) {
        if (p != null && p.getId() == enumItem.getValue()) {
          return p;
        }
      }
    }

    return null;
  }

  
  public static PseudoBinaryPoint[] findBinaryPoints(IPseudoPoint[] allPoints) {
    return findPoints(allPoints, PseudoBinaryPoint.class);
  }

  
  public static PseudoDoubleBinaryPoint[] findDoubleBinaryPoints(IPseudoPoint[] allPoints) {
    return findPoints(allPoints, PseudoDoubleBinaryPoint.class);
  }

  
  public static PseudoAnaloguePoint[] findAnalogPoints(IPseudoPoint[] allPoints) {
    return findPoints(allPoints, PseudoAnaloguePoint.class);
  }

  
  public static PseudoCounterPoint[] findCounterPoints(IPseudoPoint[] allPoints) {
    return findPoints(allPoints, PseudoCounterPoint.class);
  }

  
  public static PseudoBinaryPoint findBinaryPoint(IPseudoPoint[] allPoints, ILogicPointEnum enumItem) {
    return findPointByEnum(findBinaryPoints(allPoints), enumItem);
  }
  
  
  public static PseudoDoubleBinaryPoint findDoubleBinaryPoint(IPseudoPoint[] allPoints,ILogicPointEnum enumItem) {
    return findPointByEnum(findDoubleBinaryPoints(allPoints), enumItem);
  }

  
  public static PseudoAnaloguePoint findAnalogPoint(IPseudoPoint[] allPoints,ILogicPointEnum enumItem) {
    return findPointByEnum(findAnalogPoints(allPoints), enumItem);
  }

  
  public static PseudoCounterPoint findCounterPoint(IPseudoPoint[] allPoints,ILogicPointEnum enumItem) {
    return findPointByEnum(findCounterPoints(allPoints), enumItem);
  }

  static <T extends VirtualPoint> T []findPoints(VirtualPoint[] points, Class<T> pointClass) {
    ArrayList<T> foundPoints = new ArrayList<>();
    
    if (points != null) {
      for (int i = 0; i < points.length; i++) {
        if (pointClass.isInstance(points[i])) {
          foundPoints.add((T) points[i]);
        }
      }
    }
    
    T[] buf =  (T[]) Array.newInstance(pointClass, foundPoints.size());
    return foundPoints.toArray(buf);
  }
  
}
