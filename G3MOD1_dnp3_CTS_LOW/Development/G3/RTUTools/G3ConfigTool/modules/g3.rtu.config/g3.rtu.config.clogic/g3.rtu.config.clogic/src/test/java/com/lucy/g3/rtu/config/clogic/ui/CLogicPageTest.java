/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.fest.swing.annotation.GUITest;
import org.fest.swing.testing.FestSwingTestCaseTemplate;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ui.CLogicPage;
import com.lucy.g3.test.support.utilities.TestUtil;

@GUITest
@Ignore
// Skipped GUI test cause it failes sometimes in linux
public class CLogicPageTest extends FestSwingTestCaseTemplate {

  @Before
  public void setup() {
    setUpRobot();

  }

  @Test
  public void testCreatePage_MMAveg() {
    // final ControlLogicPage page = new
    // ControlLogicPage(CLogicFactory.createControlLogic(CLogicType.MMAVG));
    //
    // JFrame frame = GuiActionRunner.execute(new GuiQuery<JFrame>() {
    //
    // @Override
    // protected JFrame executeInEDT() throws Throwable {
    // return TestUtil.createFrame("",page.getContent());
    // }
    // });
    //
    // assertNotNull(frame);
    // FrameFixture fixture = new FrameFixture(robot(), frame);
    // page.updateCanvas();
    // fixture.show();
    //
    //
    // fixture.target.dispose();
    // fixture.cleanUp();
  }

   public static void main(String[] args) throws InterruptedException {
   Logger.getRootLogger().setLevel(Level.DEBUG);
   ICLogic logic = CLogicFactories.getFactory(ICLogicType.IEC61131).createLogic();
   final CLogicPage page = new CLogicPage(logic);
   TestUtil.showFrame(page.getContent());
   page.repaintCanvas();
  
  
   Thread.sleep(3000);
  
   System.out.println("group changed ");
   logic.setGroup(30);
  
   }

}
