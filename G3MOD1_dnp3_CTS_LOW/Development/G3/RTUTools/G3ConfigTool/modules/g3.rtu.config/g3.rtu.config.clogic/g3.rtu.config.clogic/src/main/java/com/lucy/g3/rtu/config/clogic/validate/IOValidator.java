/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.validate;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicIO.IODirection;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * Validator for Control Logic IO.
 */
public class IOValidator extends AbstractValidator<CLogicIO<Object>> {


  @SuppressWarnings("unchecked")
  public IOValidator(CLogicIO<?> target) {
    super((CLogicIO<Object>) target);
  }

  @Override
  protected void validate(ValidationResultExt result) {
    if (target.getValue() == null) {
      String message = String.format(" %s is not configured", getTargetName());
      if (target.isOptional()) {
        result.addWarning(message);
      } else {
        result.addError(message);
      }
    }

    if (target.isAcceptable(target.getValue()) == false) {
      String message = String.format(" %s is configured incorrectly", getTargetName());
      result.addError(message);
    }
    
    target.fireValidChangeEvent();
  }

  @Override
  public String getTargetName() {
    IODirection dir = target.getDirection();
    String label = target.getLabel();
    return String.format("%s \"%s\"", dir, label);
  }

}
