/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.impl.CLogicIOSwitch;
import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCM.SCM_CH_SWOUT;

/**
 *
 *
 */
public class CLogicIOSwitchTest {

  private SwitchModuleOutput output;
  private CLogicIOSwitch fixture;


  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    fixture = new CLogicIOSwitch(null, "");

    output = new SwitchModuleOutput(ModuleStub.createDSM(), SwitchIndex.SwitchA, 
        SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_OPEN   ,
        SCM_CH_DINPUT.SCM_CH_DINPUT_SWITCH_CLOSED ,
        SCM_CH_SWOUT.SCM_CH_SWOUT_OPEN_SWITCH     ,
        SCM_CH_SWOUT.SCM_CH_SWOUT_CLOSE_SWITCH     );
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    fixture = null;
    output = null;
  }

  @Test
  public void testSetOutput() {
    assertNull(fixture.getValue());
    fixture.setValue(output);
    assertTrue(fixture.getValue() == output);

    fixture.setValue(null);
    assertEquals(null, fixture.getValue());
  }

  @Test
  public void testValueStringUpdate() {
    final String initialStr = fixture.getValueString();
    assertNull(fixture.getValue());

    fixture.setValue(output);
    assertNotSame("Expect string changed", initialStr, fixture.getValueString());

    fixture.setValue(null);
    assertEquals("Expect value string recovered", initialStr, fixture.getValueString());

    fixture.setValue(output);

    output.delete();
    assertNull("Logic's output is disconnected to module once the module is deleted",
        fixture.getValue());
    assertEquals("Expect value string recovered", initialStr, fixture.getValueString());

  }

  @Test
  public void testSetValueNull() {
    Assert.assertEquals(0, output.getAllConnections().size());
    fixture.setValue(output);
    Assert.assertEquals(1, output.getAllConnections().size());
    fixture.setValue(null);
    Assert.assertEquals(0, output.getAllConnections().size());
  }

  @Test
  public void testDeleteOutput() {
    fixture.setValue(output);
    output.delete();
    assertDeleted();
  }

  private void assertDeleted() {
    assertNull(fixture.getValue());
    assertEquals(0, fixture.getAllConnections().size());
    assertEquals(0, output.getAllConnections().size());
  }

  @Test
  public void testDeleteIO() {
    fixture.setValue(output);
    fixture.delete();
    assertDeleted();
  }

}
