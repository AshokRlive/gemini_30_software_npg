/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Iterator;

import com.g3schema.ns_clogic.AutomationT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_common.ParameterT;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * This class stores Automation Logic settings.
 */
public class IEC61499LogicSettings extends AutomationLogicSettings{

  public static final String PROPERTY_RES_NAME = "resName";
  public static final String PROPERTY_SCHEME = "scheme";


  private String        resName="Unknown"; // Resource name of IEC61499
  private String        scheme ="";        // Content of automation scheme
  
  IEC61499LogicSettings(IEC61499Logic owner){
    super(owner);
  }
  
  public String getResName() {
    return resName;
  }
  
  public void setResName(String resName) {
    Object oldValue = this.resName;
    this.resName = resName;
    firePropertyChange(PROPERTY_RES_NAME, oldValue, resName);
  }
  
  public String getScheme() {
    return scheme;
  }

  
  public void setScheme(String scheme) {
    Object oldValue = this.scheme;
    this.scheme = scheme;
    firePropertyChange(PROPERTY_SCHEME, oldValue, scheme);
  }

  private static String getParameter(String name, Iterator<ParameterT> it) {
    while(it.hasNext()) {
      ParameterT p = it.next(); 
      if(p.parameterName.equals(name))
        return p.parameterValue.getValue();
    }
    
    return null;
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xmlParam) {
    AutomationT xml = xmlParam.automation.first();
    
    boolean imported = true;
    if(xml.parameter.exists()) {
      String importedStr = getParameter("imported", xml.parameter.iterator());
      if(importedStr != null)
        imported = Boolean.parseBoolean(importedStr);
    }
    
    ((IEC61499Logic)getOwnerLogic()).setImported(imported); 
    
    
    // Auto start
    setAutoStart(xml.autoStart.getValue());
    
    // Version
    if(xml.autoSchemeVersionMajor.exists() && xml.autoSchemeVersionMinor.exists())
      setAutoSchemeVersion(new BasicVersion(xml.autoSchemeVersionMajor.getValue(), xml.autoSchemeVersionMinor.getValue()));
    
    // Resource name
    if(xml.autoResName.exists())
      setResName(xml.autoResName.getValue());  
    
    // Scheme content
    if(xml.sharedLibraryFile.exists()) { 
      String schemeStr = xml.sharedLibraryFile.getValue();
      schemeStr = new String(Base64.getDecoder().decode(schemeStr));
      setScheme(schemeStr);
    }
    
    getConstantsManager().readParamFromXML(xml);
  }

  
  @Override
  public void writeParamToXML(CLogicParametersT xmlParams) {
    AutomationT xml = xmlParams.automation.exists() ? 
        xmlParams.automation.first() : xmlParams.automation.append();
    ParameterT xmlImported = xml.parameter.append();
    xmlImported.parameterName.setValue("imported");
    xmlImported.parameterValue.setValue(Boolean.toString(((IEC61499Logic)getOwnerLogic()).isImported()));
    // Auto start
    xml.autoStart.setValue(isAutoStart());
    
    // Resource name
    xml.autoResName.setValue(getResName());
    
    // Scheme
    try {
      xml.sharedLibraryFile.setValue(Base64.getEncoder().encodeToString(getScheme().getBytes("UTF-8")));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    
    // version
    xml.autoSchemeVersionMajor.setValue(getAutoSchemeVersion().major());
    xml.autoSchemeVersionMinor.setValue(getAutoSchemeVersion().minor());
    
    getConstantsManager().writeParamToXML(xml);
    
    // Unused
    xml.autoSchemeType.setValue(-1);
  }

  @Override
  public IValidator getValidator() {
    return null;
  }
}
