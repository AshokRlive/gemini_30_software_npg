
package com.lucy.g3.rtu.config.clogic.doubletosingleconverter;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class DoubleToSingleLogicFactory extends AbstractClogicFactory{
  public final static DoubleToSingleLogicFactory INSTANCE = new DoubleToSingleLogicFactory();
  private DoubleToSingleLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return null;
  }

  @Override
  public ICLogic createLogic() {
    return new DoubleToSingle();
  }

  @Override
  public ICLogicType getLogicType() {
    return DoubleToSingle.TYPE;
  }
  
}

