/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;


/**
 * The Interface ICLogicSettings.
 */
public interface ICLogicSettings extends IValidation, ObservableBean2{
  public static final String PROPERTY_INPUT_TYPE = "inputType";
  
  ICLogic getOwnerLogic();
  
  void readParamFromXML(CLogicParametersT xml);

  void writeParamToXML(CLogicParametersT xml);

  void copyFrom(ICLogicSettings from);
  
}

