/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.jgoodies.common.collect.ObservableList;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.dialogs.SelectionDialog;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.ControlLogicAddingWizard;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;

/**
 * The Presentation Model of control logic manager page.
 */
public class CLogicManagerModel extends Bean {

  public static final int COLUMN_ENABLED = CLogicTableModel.COL_ENABLED;
  
  public static final String ACTION_KEY_ADD = "addCLogic";
  public static final String ACTION_KEY_SETTING = "modifySelection";
  public static final String ACTION_KEY_REMOVE = "removeSelection";
  public static final String ACTION_KEY_ENABLE = "enableLogic";
  public static final String ACTION_KEY_DISABLE = "disableLogic";
  public static final String ACTION_KEY_MOVE_UP = "moveSelectionUp";
  public static final String ACTION_KEY_MOVE_DOWN = "moveSelectionDown";


  public static final String PROPERTY_EDITITABLE = "editable";
  public static final String PROPERTY_REMOVABLE = "removeable";

  private Logger log = Logger.getLogger(CLogicManagerModel.class);

  private final MultiSelectionInList<ICLogic> selectionList;
  private final MultiListSelectionAdapter<ICLogic> selectionModel;

  private final CLogicManager manager;

  private final Frame parent;

  private CLogicTableModel tableModel;
  
  private final CLogicManagerPage page;


  /**
   * Construct a control logic manager model.
   * @param cLogicManagerPage 
   *
   * @param manager
   *          Control logic manager
   */
  CLogicManagerModel(CLogicManagerPage page, CLogicManager manager) {
    this.page = page;
    parent = WindowUtils.getMainFrame();
    this.manager = manager;
    this.selectionList = new MultiSelectionInList<ICLogic>(manager.getItemListModel());
    this.selectionModel = new MultiListSelectionAdapter<ICLogic>(selectionList);
    this.selectionModel.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

    selectionList.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        new CLogicSelectionHandler());
  }

  public TableModel getTableModel() {
    if (tableModel == null) {
      tableModel = new CLogicTableModel(selectionList.getList());
    }

    return tableModel;
  }

  public ListModel<ICLogic> getListModel() {
    return selectionList.getList();
  }

  public ListSelectionModel getSelectionModel() {
    return selectionModel;
  }

  public ICLogic getSingleSelection() {
    ObservableList<ICLogic> clogic = selectionList.getSelection();

    if (clogic != null && clogic.getSize() == 1) {
      return clogic.get(0);
    }
    return null;
  }

  /**
   * Bean getter method. Check if the selected control logic can be removed.
   */
  public boolean isRemoveable() {
    ObservableList<ICLogic> selLogic = selectionList.getSelection();
    return selLogic != null && !selLogic.isEmpty();
  }

  /**
   * Bean getter method. Check if the selected control logic can be edited.
   */
  public boolean isEditable() {
    return getSingleSelection() != null;
  }

  public void clearSelection() {
    selectionList.getSelection().clear();
  }

  public javax.swing.Action getAction(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");

    ApplicationActionMap actionMap = Application.getInstance().getContext().getActionMap(this);
    javax.swing.Action action = actionMap.get(actionKey);

    if (action != null) {
      action = new BusyCursorAction(action, parent);
    } else {
      throw new IllegalArgumentException("Action is not available for the key:" + actionKey);
    }
    return action;
  }

  @Action
  public void addCLogic() {
    ICLogicGenerator gen = ConfigGeneratorRegistry.getInstance().getGenerator(manager.getOwner(), ICLogicGenerator.class);
    ICLogic clogic = ControlLogicAddingWizard.showDialog(manager, gen);
    if(clogic != null) {
      page.navigateTo(clogic);
      showSettingsDialog(clogic); // Show settings dialog.
    }
    
  }

  @Action(enabledProperty = PROPERTY_REMOVABLE)
  public void removeSelection() {
    ObservableList<ICLogic> initialSelections = selectionList.getSelection();
    Collection<ICLogic> all = manager.getAllClogic();
    Collection<ICLogic> selected = SelectionDialog.showRemoveDialog(parent, 
        all.toArray(new ICLogic[all.size()]), 
        initialSelections.toArray(new ICLogic[initialSelections.size()]));
    if(selected == null)
      return; //user cancelled.
    
    Collection<ICLogic> removable = findRemovableCLogic(selected);
    Collection<ICLogic> unremoved = new ArrayList<ICLogic>(selected);
    unremoved.removeAll(removable);
    manager.removeAll(removable);
    if(!unremoved.isEmpty())
      MessageDialogs.list(parent, JOptionPane.ERROR_MESSAGE,
          "Failure", "Not allowed to remove the following mandatory control logic:", unremoved);
  }

  private Collection<ICLogic> findRemovableCLogic(Collection<ICLogic> selLogic) {
    ArrayList<ICLogic> removeable = new ArrayList<>();
    for (ICLogic sel : selLogic) {
      if(sel.isAllowDelete()) {
        removeable.add(sel);
      }
    }
    
    return removeable;
  }

  @Action(enabledProperty = PROPERTY_EDITITABLE)
  public void modifySelection() {
    showSettingsDialog(getSingleSelection());
  }

  private void showSettingsDialog(ICLogic clogic) {
    if (clogic != null) {
      new CLogicSettingsDialog(parent, clogic).setVisible(true);
    }
  }
  
  @Action(enabledProperty = PROPERTY_REMOVABLE)
  public void enableLogic() {
    setSelectedLogicEnabled(true);
  }
  
  @Action(enabledProperty = PROPERTY_REMOVABLE)
  public void disableLogic() {
    setSelectedLogicEnabled(false);
  }

  @Action(enabledProperty = PROPERTY_REMOVABLE)
  public void moveSelectionUp() {
    final int idxMin = selectionModel.getMinSelectionIndex();
    final int idxMax = selectionModel.getMaxSelectionIndex();
    if (idxMin < 1) {
      // Cannot move the first element up
      return;
    }
    
    for (int i = idxMin; i <= idxMax; i++) {
      Object p0 = getListModel().getElementAt(i);
      Object p1 = getListModel().getElementAt(i - 1);
  
      manager.swap((ICLogic) p0, (ICLogic) p1);
    }
    selectionModel.setSelectionInterval(idxMin-1, idxMax-1);
    log.info("Moved control logic up");
  }

  @Action(enabledProperty = PROPERTY_REMOVABLE)
  public void moveSelectionDown() {
    final int idxMin = selectionModel.getMinSelectionIndex();
    final int idxMax = selectionModel.getMaxSelectionIndex();
    if (idxMax >= getItemsCount() - 1) {
      // Cannot move the last element down
      return;
    }
    for (int i = idxMax; i >= idxMin; i--) {
      Object p0 = getListModel().getElementAt(i);
      Object p1 = getListModel().getElementAt(i+1);
      
      manager.swap((ICLogic) p0, (ICLogic) p1);
    }
    selectionModel.setSelectionInterval(idxMin+1, idxMax+1);
    log.info("Moved control logic down");
  }
  
  public int getItemsCount() {
    return selectionList.getList().getSize();
  }

  private void setSelectedLogicEnabled(final boolean enabled) {
    ObservableList<ICLogic> selLogic = selectionList.getSelection();
    if (selLogic == null || selLogic.isEmpty()) {
      log.warn("No selected control logic");
      return;
    }
    
    for (ICLogic clogic : selLogic) {
      clogic.setEnabled(enabled);
    }
    
    // For refreshing GUI
    if (tableModel != null) {
      tableModel.fireTableRowsUpdated(0, selectionList.getList().getSize() - 1);
    }
    
    // For repainting configuration tree to update renderer colour
    WindowUtils.getMainFrame().repaint();
  }


  private class CLogicSelectionHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      /* Fire event to update the "enable" property of actions */
      CLogicManagerModel.this.firePropertyChange(PROPERTY_EDITITABLE, null, isEditable());
      CLogicManagerModel.this.firePropertyChange(PROPERTY_REMOVABLE, null, isRemoveable());
    }
  }

  private static class CLogicTableModel extends AbstractTableAdapter<ICLogic> {

    private static final String[] COL_NAMES = { "Enabled", "Logic ID", "Logic Type", "Logic Name" };

    private static final int COL_ENABLED = 0;
    private static final int COL_ID = 1;
    private static final int COL_TYPE = 2;
    private static final int COL_NAME = 3;


    public CLogicTableModel(ListModel<ICLogic> listModel) {
      super(listModel, COL_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      ICLogic clogic = getRow(rowIndex);
      switch (columnIndex) {
      case COL_ID:
        return clogic.getGroup();
      case COL_TYPE:
        return clogic.getType().getTypeName();
      case COL_NAME:
        return clogic.getName();
      case COL_ENABLED:
        return clogic.isEnabled();
      default:
        break;
      }
      return null;
    }

//    @Override
//    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//      CLogic clogic = getRow(rowIndex);
//      switch (columnIndex) {
//      case COL_ENABLED:
//        clogic.setEnabled((boolean) aValue);
//        break;
//      default:
//        break;
//      }
//    }

//    @Override
//    public boolean isCellEditable(int rowIndex, int columnIndex) {
//      return columnIndex == COL_ENABLED;
//    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
      if (columnIndex == COL_ENABLED) {
        return Boolean.class;
      }

      return super.getColumnClass(columnIndex);
    }


  }

}
