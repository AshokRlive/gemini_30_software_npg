/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.voltagemismatch;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;


class VoltageMismatchLogicSettingsEditor extends AbstractCLogicSettingsEditor {
  private final static String LABEL_VOLTAGE_MISMATCH_DELAY= "Voltage Mismatch Delay";
  public VoltageMismatchLogicSettingsEditor(VoltageMismatchLogicSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result, pm, VoltageMismatchLogicSettings.PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC, LABEL_VOLTAGE_MISMATCH_DELAY);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();

    createIntField(LABEL_VOLTAGE_MISMATCH_DELAY, VoltageMismatchLogicSettings.PROPERTY_VOLTAGE_MISMATCH_DELAY_SEC, Units.SECS);
  
    return builder.getPanel();
  }

}

