/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import com.lucy.g3.common.utils.CollectionUtils;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.api.ILogicInputEnum;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;

/**
 * The abstract Control Logic initialised from XML enum.
 */
public abstract class PredefinedCLogic<SettingsT extends ICLogicSettings> extends AbstractCLogic<SettingsT> {


  private PseudoBinaryPoint[] binaryPoints = null;
  private PseudoDoubleBinaryPoint[] dbinaryPoints = null;
  private PseudoAnaloguePoint[] analogPoints = null;
  private PseudoCounterPoint[] counterPoints = null;
  private IPseudoPoint[] allPoints = null;

  private CLogicIOPoint[] inputs;


  public PredefinedCLogic(ICLogicType type, ILogicInputEnum[] inputEnums, ILogicPointEnum[] pointEnums) {
    super(type);
    init(inputEnums, pointEnums);
  }
  
  /**
   * Construct a logic without initialising inputs/outputs.
   * init() msut be called afterwards.
   * @param type
   */
  protected PredefinedCLogic(ICLogicType type){
    super(type);
  }
  
  protected void init(ILogicInputEnum[] inputEnums, ILogicPointEnum[] pointEnums) {
    if(inputs == null) {
      this.inputs = initInputs(inputEnums);
      initPoints(pointEnums);
    } else {
      throw new RuntimeException(getType().getTypeName() + " already initialised");
    }
  }

  private void initPoints(ILogicPointEnum[] pointEnums) {
    ArrayList<PseudoBinaryPoint> bpointsList = new ArrayList<>();
    ArrayList<PseudoDoubleBinaryPoint> dbpointsList = new ArrayList<>();
    ArrayList<PseudoAnaloguePoint> apointsList = new ArrayList<>();
    ArrayList<PseudoCounterPoint> cpointsList = new ArrayList<>();

    if (pointEnums != null) {
      for (ILogicPointEnum pe : pointEnums) {
        if (pe == null) {
          throw new IllegalArgumentException("Logic point enum must not be null");
        }

        switch (pe.getType()) {
        case analogue:
          apointsList.add(PseudoPointFactory.createPseudoAnalogPoint(this, pe));
          break;
        case binary:
          bpointsList.add(PseudoPointFactory.createPseudoBinaryPoint(this, pe));
          break;
        case counter:
          cpointsList.add(PseudoPointFactory.createPseudoCounterPoint(this, pe));
          break;
        case doubleBinary:
          dbpointsList.add(PseudoPointFactory.createPseudoDBinaryPoint(this, pe));
          break;
        default:
          break;
        }
      }
    }

    binaryPoints = bpointsList.toArray(new PseudoBinaryPoint[bpointsList.size()]);
    dbinaryPoints = dbpointsList.toArray(new PseudoDoubleBinaryPoint[dbpointsList.size()]);
    analogPoints = apointsList.toArray(new PseudoAnaloguePoint[apointsList.size()]);
    counterPoints = cpointsList.toArray(new PseudoCounterPoint[cpointsList.size()]);
    
    allPoints = new IPseudoPoint[0];
    allPoints = ArrayUtils.addAll(allPoints, binaryPoints);
    allPoints = ArrayUtils.addAll(allPoints, dbinaryPoints);
    allPoints = ArrayUtils.addAll(allPoints, analogPoints);
    allPoints = ArrayUtils.addAll(allPoints, counterPoints);
    
    /* Predefined logic doesn't allow user to change points. */
    for (IPseudoPoint point : allPoints) {
      point.setAllowChangeDescription(false);
      point.setAllowDelete(false);
    }
  }

  private CLogicIOPoint[] initInputs(ILogicInputEnum[] inputEnums) {
    if (inputEnums == null || inputEnums.length == 0) {
      return null;
    }

    ArrayList<CLogicIOPoint> inputsList = new ArrayList<>();
    VirtualPointType[] pointType;
    boolean isMandatory;

    for (int i = 0; i < inputEnums.length; i++) {
      if (inputEnums[i] == null) {
        throw new IllegalArgumentException("Logic Input enum must not be null");
      }

      isMandatory = inputEnums[i].isMandatory();
      switch (inputEnums[i].getType()) {
      case analogue:
        pointType = VirtualPointType.analoygTypes();
        break;
      case binary:
        pointType = VirtualPointType.binaryTypes();
        break;
      case doubleBinary:
        pointType = VirtualPointType.doubleBinaryTypes();
        break;
      default:
        throw new IllegalStateException("Unsupported type:" + inputEnums[i].getType());
      }

      inputsList.add(new CLogicIOPoint(this, inputEnums[i].getDescription(), pointType, false, false, !isMandatory, true));

    }

    return inputsList.toArray(new CLogicIOPoint[inputsList.size()]);
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return CollectionUtils.copy(inputs);
  }

  public PseudoBinaryPoint[] getBinaryPoints() {
    return CollectionUtils.copy(binaryPoints);
  }

  public PseudoDoubleBinaryPoint[] getDoubleBinaryPoints() {
    return CollectionUtils.copy(dbinaryPoints);
  }

  public PseudoAnaloguePoint[] getAnalogPoints() {
    return CollectionUtils.copy(analogPoints);
  }

  public PseudoCounterPoint[] getCounterPoints() {
    return CollectionUtils.copy(counterPoints);
  }

  @Override
  public final IPseudoPoint[] getAllPoints() {
    return CollectionUtils.copy(allPoints);
  }
  
  @Override
  public IInputsManager getInputsManager() {
    return null;
  }

  @Override
  public IOutputsManager<?> getOutputsManager() {
    return null;
  }

  @Override
  public ICLogicPointsManager getPointsManager() {
    return null;
  }

  public final CLogicIOPoint getInputByEnum(Enum<? extends ILogicInputEnum> enumItem) {
    if (enumItem != null && inputs != null) {
      int index = enumItem.ordinal();
      if (index >= 0 && index < inputs.length) {
        return inputs[index];
      }
    }

    return null;
  }

}
