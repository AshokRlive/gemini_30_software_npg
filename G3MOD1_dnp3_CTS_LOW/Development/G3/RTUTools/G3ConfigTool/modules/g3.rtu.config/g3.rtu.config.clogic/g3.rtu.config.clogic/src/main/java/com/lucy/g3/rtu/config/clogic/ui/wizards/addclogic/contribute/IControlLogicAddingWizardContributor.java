
package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

public interface IControlLogicAddingWizardContributor {
  
  WizardPage[] getWizardPages(IConfig config);
  
  /**
   * Creates a new control logic from wizard data.
   * @param newLogic
   */
  ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception;
  
  class Factory {
    private Factory (){}
    public static IControlLogicAddingWizardContributor createOutputChannelContributor(
        ChannelType chtype, String description)  {
      return new OutputChannelContributor(chtype, description);
    }
    
  }
}

