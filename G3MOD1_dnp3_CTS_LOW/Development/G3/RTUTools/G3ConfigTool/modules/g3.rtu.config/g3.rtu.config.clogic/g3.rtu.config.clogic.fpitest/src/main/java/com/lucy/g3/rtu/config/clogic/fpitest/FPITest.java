/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fpitest;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_FPI;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FAN_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FPITEST_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FPITEST_OUTPUT;

/**
 * Implementation of FPI Test Logic.
 */
public class FPITest extends PredefinedCLogic<ICLogicSettings> implements ISupportOffLocalRemote, IOperableLogic {

  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.FPI_TEST, "FPI Test")
      .opMode(OperationMode.GENERIC_OPERATE)
      .requireOLR(true)
      .get();

  
  private final CLogicIOChannel[] outputs;


  public FPITest() {
    super(TYPE, FPITEST_INPUT.values(), null);
    outputs = new CLogicIOChannel[FPITEST_OUTPUT.values().length];
    initOutputs();

    bindDescriptionToIO(outputs[0]);
  }

  private void initOutputs() {
    FPITEST_OUTPUT[] outputEnums = FPITEST_OUTPUT.values();
    boolean isOptional = false;
    boolean enabled = true;
    for (int i = 0; i < outputEnums.length; i++) {
      String name = outputEnums[i].getDescription();
      outputs[i] = new CLogicIOChannel(
          this,
          name,
          isOptional,
          enabled,
          new ChannelType[] { ChannelType.FPI },
          null,
          new Enum<?>[] { FPM_CH_FPI.FPM_CH_FPI_1_TEST, FPM_CH_FPI.FPM_CH_FPI_2_TEST });
    }
  }

  public void setOutputChannel(IChannel outputChannel) {
    if (outputChannel != null) {
      outputs[0].setValue(outputChannel);
    }
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return outputs;
  }


  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(FAN_INPUT.FAN_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public Module getSourceModule() {
    IChannel ch = outputs[0].getValue();
    return ch == null ? null : ch.getOwnerModule();
  }
  
  @Override
  public boolean isSwitchLogic() {
    return false;
  }

  @Override
  protected ICLogicSettings createSettings() {
    return null;
  }
}
