/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic;

import java.awt.Font;
import javax.swing.*;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicType;

class FinishPage extends WizardPage {
  
  public FinishPage(ICLogicType type, CLogicManager manager, ICLogicGenerator configurator) {
    super("Finish"/*, "Finish", type, manager, configurator*/);
    initComponents();

    lblName.setText(type.getTypeName());
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    label1 = new JLabel();
    lblName = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
      "default:grow",
      "default, $pgap, fill:default"));

    //---- label1 ----
    label1.setText("Press finish to create a new control logic:");
    add(label1, CC.xy(1, 1));

    //---- lblName ----
    lblName.setFont(lblName.getFont().deriveFont(lblName.getFont().getStyle() | Font.BOLD, lblName.getFont().getSize() + 2f));
    lblName.setHorizontalAlignment(SwingConstants.CENTER);
    add(lblName, CC.xy(1, 3));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JLabel lblName;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
