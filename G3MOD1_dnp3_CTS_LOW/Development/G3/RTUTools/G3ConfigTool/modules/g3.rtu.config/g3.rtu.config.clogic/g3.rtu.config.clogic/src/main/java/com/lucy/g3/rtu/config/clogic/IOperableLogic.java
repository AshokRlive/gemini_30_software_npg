/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

/**
 * The logic that implements this interface can be operated.
 */
public interface IOperableLogic extends ICLogic {

  /**
   * Checks if this logic is a switch logic. A switch logic supports
   * "open/close" action, while a none-switch logic only supports "operate"
   * action.
   */
  boolean isSwitchLogic();
}
