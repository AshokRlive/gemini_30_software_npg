/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.action;

import java.util.Arrays;

import com.g3schema.ns_clogic.ActionInputSettingT;
import com.g3schema.ns_clogic.ActionParamT;
import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_vpoint.InputModeT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.action.ActionLogic.InputSetting;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.InputMode;
import com.lucy.g3.xml.gen.common.ControlLogicDef.ACTION_CLOGIC_CMD;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

public class ActionLogicSettings extends AbstractCLogicSettings<ActionLogic> {
  public static final String PROPERTY_DELAY = "delay";

  private long delay; // seconds
  
  private InputSetting[] inputSettings = new InputSetting[0];

  public ActionLogicSettings(ActionLogic owner) {
    super(owner);
  }

  public long getDelay() {
    return delay;
  }

  public void setDelay(long delay) {
    Object oldValue = this.delay;
    this.delay = delay;
    firePropertyChange(PROPERTY_DELAY, oldValue, delay);
  }
  
  void setInputsNum(int inputNum){
    inputSettings = new InputSetting[inputNum];
    for (int i = 0; i < inputNum; i++) {
      inputSettings[i] = new InputSetting();
    }
  }
  
  public InputSetting[] getInputSettings() {
    return inputSettings == null ? null : Arrays.copyOf(inputSettings, inputSettings.length);
  }
  

  @Override
  public IValidator getValidator() {
    return null;
  }

  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    ActionParamT xml = xml_param.actionParam.first();
    setDelay(xml.delay.getValue());

    int inputNum = xml.inputSetting.count();
    getOwnerLogic().init(inputNum);
    InputSetting[] settings = getInputSettings();
    assert settings.length == inputNum;
    for (int i = 0; i < inputNum; i++) {
      ActionInputSettingT xml_setting = xml.inputSetting.at(i);
      InputModeT xml_input = xml_setting.inputMode.first();

      settings[i].setCommand(ACTION_CLOGIC_CMD.forValue(
          (int) xml_setting.command.getValue()));

      if (xml_input.edgeDriven.exists()) {
        settings[i].setInputMode(InputMode.EDGE_DRIVEN);
        settings[i].setEdgeDrivenMode(EDGE_DRIVEN_MODE.forValue(
            (int) xml_input.edgeDriven.first().edgeDrivenMode.getValue()));
      } else if (xml_input.pulseDriven.exists()) {
        settings[i].setInputMode(InputMode.PULSE_DRIVEN);
        settings[i].setPulseWidth(xml_input.pulseDriven.first().pulseWidth.getValue());
      }
    }

  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    ActionParamT xml_action = xml.actionParam.append();

    xml_action.delay.setValue(getDelay());
    InputSetting[] actionSettings = getInputSettings();

    if (actionSettings != null) {
      for (int i = 0; i < actionSettings.length; i++) {
        ActionInputSettingT xml_setting = xml_action.inputSetting.append();
        InputModeT xml_inputmode = xml_setting.inputMode.append();

        // Append input mode
        switch (actionSettings[i].getInputMode()) {
        case EDGE_DRIVEN:
          xml_inputmode.edgeDriven.append().edgeDrivenMode.setValue(
              actionSettings[i].getEdgeDrivenMode().getValue());
          break;

        case PULSE_DRIVEN:
          xml_inputmode.pulseDriven.append().pulseWidth.setValue(
              actionSettings[i].getPulseWidth());
          break;
        default:
          break;
        }

        // Append command
        xml_setting.command.setValue(
            actionSettings[i].getCommand().getValue());
      }
    }
  }
}

