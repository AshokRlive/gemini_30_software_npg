package com.lucy.g3.rtu.config.clogic.tcl;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;

class TCLSensingSettingsEditor implements ICLogicSettingsEditor {
    private TCLSensingSettingsPanel comp;
    private final ICLogicSettings settings;
    
    public TCLSensingSettingsEditor(ICLogicSettings settings) {
      this.settings = settings;
    }

    @Override
    public void validate(ValidationResult result) {
      if(comp != null) {
        if(comp.validateSettings() == false) {
          result.addError(ERR_MSG_INVALID_LOGIC_SETTINGS);
        };
      }
        
    }

    @Override
    public JComponent getComponent(JDialog parent) {
      if(comp == null) {
        comp = new TCLSensingSettingsPanel();
        comp.load(settings);
      }
      
      return comp;
    }

    @Override
    public void triggerCommit() {
      if(comp != null) {
        comp.save();
      }
    }

    @Override
    public void triggerFlush() {
      // Do nothing
    }

    @Override
    public void release() {
      // Do nothing
    }

    @Override
    public void postCommitAction() {
      // Do nothing
    }
  }
 