/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.doubletosingleconverter;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.clogic.threshold.ThresholdLogicSettings;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetRef;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DTS_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DTS_POINT;

/**
 * Implementation of Threshold Logic.
 */
public class DoubleToSingle extends PredefinedCLogic<ThresholdLogicSettings> {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.DTS, "Double to Single Converter")
      .desp("The control logic for converting a double binary to 4 single binaries.")
      .get();

  public DoubleToSingle() {
    super(TYPE, DTS_INPUT.values(), DTS_POINT.values());

    // Observe the label settings of input and update the description of binaries.
    final CLogicIOPoint valueInput = getValueInput();
    valueInput.addPropertyChangeListener(CLogicIOPoint.PROPERTY_VALUE, new LabelChangeHandler());
  }

  CLogicIOPoint getValueInput() {
    return getInputByEnum(DTS_INPUT.DTS_INPUT_VALUE);
  }

  public void setInput(VirtualPoint point) {
    getValueInput().setValue(point);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  protected ThresholdLogicSettings createSettings() {
    return null;
  }

  private void updatePointDescription(ValueLabelSet label) {
    for (int i = 0; i < getAllPointsNum(); i++) {
      updatePointDescription(i, label == null ? null : label.getLabel((double)i));
    }
  }
  
  private void updatePointDescription(int index, String label){
    IPseudoPoint point = getPointByID(index);
    point.setAllowChangeDescription(true);
    point.setDescription(Strings.isBlank(label)? "Binary "+index : label);
    point.setAllowChangeDescription(false);
  }
  
  private class LabelChangeHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if(CLogicIOPoint.PROPERTY_VALUE.equals(evt.getPropertyName())) {
        VirtualPoint oldPoint = (VirtualPoint) evt.getOldValue();
        VirtualPoint newPoint = (VirtualPoint) evt.getNewValue();
        
        ValueLabelSet label = null;
        
        if(oldPoint != null)
          oldPoint.getLabelSetRef().removePropertyChangeListener(ValueLabelSetRef.PROPERTY_LABEL_SET_CHANGED, this);
        
        if(newPoint != null) {
          newPoint.getLabelSetRef().addPropertyChangeListener(ValueLabelSetRef.PROPERTY_LABEL_SET_CHANGED, this);
          label = newPoint.getLabelSetRef().getLabelSet();
        }
        
        updatePointDescription(label);
        
        
      } else if(ValueLabelSetRef.PROPERTY_LABEL_SET_CHANGED.equals(evt.getPropertyName())) {
        updatePointDescription((ValueLabelSet) evt.getNewValue());
      }
    }

  }
}
