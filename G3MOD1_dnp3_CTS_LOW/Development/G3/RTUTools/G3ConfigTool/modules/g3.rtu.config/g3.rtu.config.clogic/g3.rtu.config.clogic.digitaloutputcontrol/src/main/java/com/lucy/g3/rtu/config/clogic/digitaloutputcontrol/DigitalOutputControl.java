/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.digitaloutputcontrol;

import java.util.Arrays;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DOL_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DOL_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DOL_POINT;

/**
 * Implementation of Digital Output Control Logic.
 */
public final class DigitalOutputControl extends PredefinedCLogic<ICLogicSettings> implements ISupportOffLocalRemote, IOperableLogic {

  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.DOL, "Digital Output")
      .desp("The control logic for controlling an IO Module output.")
      .opMode(OperationMode.OPEN_CLOSE)
      .requireOLR(true)
      .get();
  
  private final CLogicIOChannel[] outputs;


  public DigitalOutputControl() {
    this(null);
  }

  /**
   * Constructor of Local Remote Control Logic.
   */
  public DigitalOutputControl(IChannel digitalOutputChannel) {
    super(TYPE, DOL_INPUT.values(), DOL_POINT.values());

    outputs = new CLogicIOChannel[DOL_OUTPUT.values().length];
    initOutputs();

    setOutputChannel(digitalOutputChannel);

    // Bind logic name.
    bindDescriptionToIO(outputs[0]);
  }

  private void initOutputs() {
    {
      DOL_OUTPUT[] outputEnums = DOL_OUTPUT.values();
      boolean isOptional;
      for (int i = 0; i < outputs.length; i++) {
        DOL_OUTPUT output = outputEnums[i];
        isOptional = (output != DOL_OUTPUT.DOL_OUTPUT_CHANNEL);
        outputs[i] = new CLogicIOChannel(
            this,
            outputEnums[i].getDescription(),
            isOptional,
            true,
            new ChannelType[] { ChannelType.DIGITAL_OUTPUT },
            null,// Any module
            null);
      }
    }
  }

  public void setOutputChannel(IChannel ch) {
    if (ch != null && ch.getType() != ChannelType.DIGITAL_OUTPUT) {
      log.error("Unacceptable digital output channel. Invalid type:"
          + ch.getType());
      return;
    }

    int index = DOL_OUTPUT.DOL_OUTPUT_CHANNEL.ordinal();
    outputs[index].setValue(ch);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(DOL_INPUT.DOL_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  public CLogicIO<IChannel> getOutput(DOL_OUTPUT outputItem) {
    if (outputItem == null) {
      throw new IllegalArgumentException("Null Ouput Enum");
    }

    return outputs[outputItem.ordinal()];
  }

  @Override
  public CLogicIO<IChannel>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public Module getSourceModule() {
    IChannel ch = getOutput(DOL_OUTPUT.DOL_OUTPUT_CHANNEL).getValue();
    if (ch != null) {
      return ch.getOwnerModule();
    } else {
      return null;
    }
  }

  @Override
  public boolean isSwitchLogic() {
    return true;
  }

  @Override
  protected ICLogicSettings createSettings() {
    return null;
  }
}
