/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.text.NumberFormatExt;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.BooleanToStringConverter;
import com.lucy.g3.rtu.config.clogic.ICLogic;

/**
 * The information of Control Logic.
 */
/*public*/ class CLogicInfoPanel extends JPanel {

  private final ICLogic clogic;
  private final PresentationModel<ICLogic> pm;


  public CLogicInfoPanel(ICLogic cLogic) {
    this.clogic = Preconditions.checkNotNull(cLogic, "cLogic is null");
    this.pm = new PresentationModel<ICLogic>(cLogic);

    initComponents();
    initComponentsBinding();
  }

  private void initComponentsBinding() {
    lblType.setText(clogic.getType().getTypeName());

    ValueModel vm;

    vm = pm.getModel(ICLogic.PROPERTY_GROUP);
    vm = ConverterFactory.createStringConverter(vm, new NumberFormatExt());
    Bindings.bind(lblID, vm);

    vm = pm.getModel(ICLogic.PROPERTY_NAME);
    Bindings.bind(lblName, vm);
    
    vm = pm.getModel(ICLogic.PROPERTY_ENABLED);
    Bindings.bind(lblEnabled, new ConverterValueModel(vm, new BooleanToStringConverter()));
    
    vm = pm.getModel(ICLogic.PROPERTY_OPERABLE);
    Bindings.bind(lblOperable, new ConverterValueModel(vm, new BooleanToStringConverter()));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    label1 = new JLabel();
    lblType = new JLabel();
    label2 = new JLabel();
    lblName = new JLabel();
    label4 = new JLabel();
    lblID = new JLabel();
    label5 = new JLabel();
    lblOperable = new JLabel();
    label3 = new JLabel();
    lblEnabled = new JLabel();

    //======== this ========
    setOpaque(false);
    setLayout(new FormLayout(
      "left:default, $lcgap, default:grow",
      "4*(default, $lgap), fill:default"));

    //---- label1 ----
    label1.setText("Logic Type:");
    add(label1, CC.xy(1, 1));
    add(lblType, CC.xy(3, 1));

    //---- label2 ----
    label2.setText("Logic Name:");
    add(label2, CC.xy(1, 3));
    add(lblName, CC.xy(3, 3));

    //---- label4 ----
    label4.setText("Logic ID:");
    add(label4, CC.xy(1, 5));
    add(lblID, CC.xy(3, 5));

    //---- label5 ----
    label5.setText("Operable:");
    add(label5, CC.xy(1, 7));
    add(lblOperable, CC.xy(3, 7));

    //---- label3 ----
    label3.setText("Enabled:");
    add(label3, CC.xy(1, 9));
    add(lblEnabled, CC.xy(3, 9));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JLabel lblType;
  private JLabel label2;
  private JLabel lblName;
  private JLabel label4;
  private JLabel lblID;
  private JLabel label5;
  private JLabel lblOperable;
  private JLabel label3;
  private JLabel lblEnabled;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
