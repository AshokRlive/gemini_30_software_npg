package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute;

import java.awt.Component;
import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.channel.domain.IChannel;

public class OutputChannelPage extends WizardPage {
    final static String PAGE_ID = "OutputChannelPage";
    private JComboBox<Object> combDoutCh;

    public OutputChannelPage( Collection<IChannel> channels,String description/*int typeId, CLogicManager manager, ICLogicGenerator configurator, 
        Collection<IChannel> channels, IChannel initialSelection, String hint*/) {
      super("Output Channel");
      setLongDescription(description);
      setLayout(new FormLayout(
          "right:[30dlu,default], $lcgap, default:grow",
          "default, $rgap, default, $lgap, default, $ugap, top:default"));

      combDoutCh = new JComboBox<Object>(channels == null ? new Object[0] : channels.toArray());

      JLabel label3 = new JLabel();

      // ---- label3 ----
      label3.setText("Output Channel:");
      add(label3, CC.xy(1, 1));

      add(combDoutCh, CC.xy(3, 1));

      // ---- labelDescription3 ----
//      if (!Strings.isBlank(hint)) {
//        JXLabel labelDescription3 = new JXLabel();
//        labelDescription3.setLineWrap(true);
//        labelDescription3.setText("* " + hint);
//        labelDescription3.setForeground(Color.gray);
//        add(labelDescription3, CC.xy(3, 3));
//      }
      
      putWizardData(PAGE_ID, this);
    }

    @Override
    protected String validateContents(Component component, Object event) {
      String result = null;

      if (getSelectedCh() == null) {
        result = "";// "Output channel is not selected";
      }

      return result;
    }

    public IChannel getSelectedCh() {
      return (IChannel) combDoutCh.getSelectedItem();
    }
  }