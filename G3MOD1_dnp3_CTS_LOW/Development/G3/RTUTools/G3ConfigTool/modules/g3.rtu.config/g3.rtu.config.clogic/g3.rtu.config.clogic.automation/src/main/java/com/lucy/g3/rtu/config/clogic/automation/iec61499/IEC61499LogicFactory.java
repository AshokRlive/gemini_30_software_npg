
package com.lucy.g3.rtu.config.clogic.automation.iec61499;

import java.util.Map;

import javax.swing.JPanel;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IClogicFactory;
import com.lucy.g3.rtu.config.clogic.automation.AutomationLogicConstantsPanel;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


public class IEC61499LogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor {
  public final static IEC61499LogicFactory INSTANCE = new IEC61499LogicFactory();
  private IEC61499LogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new IEC61499LogicSettingsEditor((IEC61499LogicSettings) settings);
  }

  @Override
  public IEC61499Logic createLogic() {
    return new IEC61499Logic();
  }

  @Override
  public ICLogicType getLogicType() {
    return IEC61499Logic.TYPE;
  }

  @Override
  public JPanel[] createConfigTabs(ICLogic clogic) {
    IEC61499Logic autologic = (IEC61499Logic) clogic;
    AutomationLogicConstantsPanel constPanel = new AutomationLogicConstantsPanel(autologic.getSettings().getConstantsManager());
    constPanel.putClientProperty(IClogicFactory.PROPERTY_TAB_TITLE, "Parameters");
    return new JPanel[]{constPanel};
  }

  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }

  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    return new WizardPage[] {
        new IEC61499LogicAddingWizard(config)
    };
  }

  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    IEC61499LogicAddingWizard wiz = (IEC61499LogicAddingWizard) wizardData.get(IEC61499LogicAddingWizard.PAGE_ID);
    return wiz.createLogic();
  }
}

