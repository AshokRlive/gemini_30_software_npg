/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.threshold;

import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointRepository;

/**
 * Wizard page for configuring Threshold Logic options.
 */
class ThresholdLogicOptions extends WizardPage {

  static final String KEY_ACTION_INPUT_POINT = "ThresholdInputPoint";


  public ThresholdLogicOptions(IConfig config) {
    super("Configuration");
    
    initComponents();
    
    comboBoxInput.setModel(new DefaultComboBoxModel<Object>(findInputs(config).toArray()));
    ComboBoxUtil.makeWider(comboBoxInput);
    comboBoxInput.setName(KEY_ACTION_INPUT_POINT);
  }

  private static Collection<VirtualPoint> findInputs(IConfig config) {
    VirtualPointRepository repo = config.getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);
    Collection<VirtualPoint> allPoints = repo.getAllPointsByType(VirtualPointType.ANALOGUE_INPUT);
    return allPoints;
  }
  
  public VirtualPoint getInput() {
    return (VirtualPoint) comboBoxInput.getSelectedItem();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    cLogicType = new JLabel();
    comboBoxInput = new JComboBox<>();

    //======== this ========
    setLayout(new FormLayout(
      "43dlu, $lcgap, default:grow",
      "default"));

    //---- cLogicType ----
    cLogicType.setText("Input Value:");
    add(cLogicType, CC.xy(1, 1));
    add(comboBoxInput, CC.xy(3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel cLogicType;
  private JComboBox<Object> comboBoxInput;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
