/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fpireset;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumFPM.FPM_CH_FPI;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FPIRESET_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.FPIRESET_OUTPUT;

/**
 * Implementation of FPI Reset Logic.
 */
public class FPIReset extends PredefinedCLogic<FPIResetSettings> implements ISupportOffLocalRemote, IOperableLogic {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.FPI_RESET, "FPI Reset")
      .desp("The logic for resetting FPI.")
      .opMode(OperationMode.GENERIC_OPERATE)
      .requireOLR(true)
      .get();
  
  private final CLogicIOChannel[] outputs;


  public FPIReset() {
    super(TYPE, FPIRESET_INPUT.values(), null);
    outputs = new CLogicIOChannel[FPIRESET_OUTPUT.values().length];
    initOutputs();

    bindDescriptionToIO(outputs[0]);
  }

  private void initOutputs() {
    FPIRESET_OUTPUT[] outputEnums = FPIRESET_OUTPUT.values();
    for (int i = 0; i < outputEnums.length; i++) {
      String name = outputEnums[i].getDescription();
      outputs[i] = new CLogicIOChannel(
          this,
          name,
          false,
          true,
          new ChannelType[] { ChannelType.FPI },
          null,
          new Enum<?>[] { FPM_CH_FPI.FPM_CH_FPI_1_RESET, FPM_CH_FPI.FPM_CH_FPI_2_RESET });
    }
  }

  public void setInputVPoint(IStandardPoint point) {
    getInputByEnum(FPIRESET_INPUT.FPIRESET_INPUT_LVFAIL).setValue(point);
  }

  public void setOutputChannel(IChannel outputChannel) {
    if (outputChannel != null) {
      outputs[0].setValue(outputChannel);
    }
  }

  public IChannel getOutputChannel() {
    return outputs[0].getValue();
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return outputs;
  }


  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(FPIRESET_INPUT.FPIRESET_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public boolean isSwitchLogic() {
    return false;
  }

  @Override
  protected FPIResetSettings createSettings() {
    return new FPIResetSettings(this);
  }
}
