/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.tcl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatterFactory;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettings.PowerFlowDirection;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettings.TimeSlot;

public class TimeSettingsPanel extends JPanel {
  private final static TimeZone TZ = TimeZone.getTimeZone("UTC");
  
  private final JComponent[][] groups;
  private final JCheckBox[] enables;
  
  
  public TimeSettingsPanel() {
    initComponents();
    
    JComponent[][] compGroups = {
        { ftfStart0, ftfEnd0, ftfValue0, comboDirection0 },
        { ftfStart1, ftfEnd1, ftfValue1, comboDirection1 },
        { ftfStart2, ftfEnd2, ftfValue2, comboDirection2 },
        { ftfStart3, ftfEnd3, ftfValue3, comboDirection3 },
    };
    enables = new  JCheckBox[] {
        checkBoxEnable0,
        checkBoxEnable1,
        checkBoxEnable2,
        checkBoxEnable3
    };
    this.groups = compGroups;
    
    initEventHandling();
    customiseFocusTraversalPolicy(compGroups);
  }
  
  public boolean validateSettings(){
    Date startA = null;
    Date endA = null;
    Date startB = null;
    Date endB = null;
    
    clearErrorMessage();
    
    // Check End > Start
    for (int i = 0; i < enables.length; i++) {
      if(!enables[i].isSelected())
        continue;
      
      startA = (Date)((JSpinner)groups[i][0]).getValue();
      endA = (Date)((JSpinner)groups[i][1]).getValue();

      if(compareHourMin(startA, endA) >= 0) {
        setErrorBackground((JSpinner)groups[i][1]);
        setErrorMessage("End time must be greater than start time!");
        return false;
      }
    }
    
    
      
    // Check time overlapped 
    for (int i = 0; i < enables.length; i++) {
      if(!enables[i].isSelected())
        continue;
      startA = (Date)((JSpinner)groups[i][0]).getValue();
      endA = (Date)((JSpinner)groups[i][1]).getValue();

      for (int j = i+1; j < enables.length; j++) {
        if(!enables[j].isSelected())
          continue;
        
        startB = (Date)((JSpinner)groups[j][0]).getValue();
        endB = (Date)((JSpinner)groups[j][1]).getValue();
        
        if(checkTimeOverlapped(startA, endA, startB, endB)) {
          setErrorBackground((JSpinner)groups[i][0]);
          setErrorBackground((JSpinner)groups[i][1]);
          setErrorBackground((JSpinner)groups[j][0]);
          setErrorBackground((JSpinner)groups[j][1]);
          setErrorMessage("Time overlapped!");
          return false;
        }
      } 
    }

    return true;
  }
  
  private static boolean checkTimeOverlapped(Date startA,Date endA, Date startB, Date endB){
    return !(compareHourMin(startA, endB) >= 0  || compareHourMin(endA, startB) <=0);
  }
  
  /**
   * Compares the date at the field of hour and minute.
   * @param date0
   * @param date1
   * @return 1 if date0 > date1, 0 if equals, -1 if date0 < date1
   */
  private static int compareHourMin(Date date0, Date date1){
    final Calendar cal0 = Calendar.getInstance(TZ);
    final Calendar cal1 = Calendar.getInstance(TZ);
    
    cal0.setTime(date0);
    cal1.setTime(date1);
    
    int field0 = cal0.get(Calendar.HOUR_OF_DAY);
    int field1 = cal1.get(Calendar.HOUR_OF_DAY);
    
    if(field0 > field1) {
      return 1;
    } else if(field0 == field1) {
      field0 = cal0.get(Calendar.MINUTE);
      field1 = cal1.get(Calendar.MINUTE);
      if(field0 > field1) {
        return 1; 
      } else if(field0 == field1) {
        return 0;
      } else {
        return -1;
      }
    } else {
      return -1;
    }
  }
  
  private void setErrorMessage(String errorMessage) {
    lblError.setText(errorMessage);
  }
  
//  public boolean isHasError() {
//    return !lblError.getText().trim().isEmpty();
//  }

  private static void setErrorBackground(JSpinner spinner) {
    Component c = spinner.getEditor().getComponent(0);
    c.setBackground(ValidationComponentUtils.getErrorBackground());    
  }
  
  private void clearErrorMessage() {
    setErrorMessage(" "); 
    
    for (int i = 0; i < groups.length; i++) {
      clearErrorBackground((JSpinner) groups[i][0]);
      clearErrorBackground((JSpinner) groups[i][1]);
    }
  }
  
  private static void clearErrorBackground(JSpinner spinner) {
    Component c = spinner.getEditor().getComponent(0);
    c.setBackground(Color.white);    
  }

  void initComponentNames(String prefix) {
    for (int i = 0; i < enables.length; i++) {
      enables[i].setName(prefix+".enable"+i);
    }
    
    for (int i = 0; i < groups.length; i++) {
      groups[i][0].setName(prefix + ".ftfStart" + i);
      groups[i][1].setName(prefix + ".ftfEnd" + i);
      groups[i][2].setName(prefix + ".ftfValue" + i);
      groups[i][3].setName(prefix + ".comboDirection" + i);
    }
  }
  

  public void load(TimeSlot[] timeSlot) {
    for (int i = 0; i < timeSlot.length; i++) {
      enables[i].setSelected(timeSlot[i].isEnabled());
      ((JSpinner)groups[i][0]).setValue(timeSlot[i].getStartTime());
      ((JSpinner)groups[i][1]).setValue(timeSlot[i].getEndTime());
      ((JFormattedTextField)groups[i][2]).setValue(timeSlot[i].getPowerValueAsFloat());
      ((JComboBox<?>)groups[i][3]).setSelectedItem(timeSlot[i].getPowerFlowDirection());
    }
  }
  
  public void save(TimeSlot[] timeSlot) {
    for (int i = 0; i < timeSlot.length; i++) {
      timeSlot[i].setEnabled(enables[i].isSelected());
      timeSlot[i].setStartTime((Date) ((JSpinner)groups[i][0]).getValue());
      timeSlot[i].setEndTime((Date) ((JSpinner)groups[i][1]).getValue());
      timeSlot[i].setPowerFlowValue((float)((JFormattedTextField)groups[i][2]).getValue());
      timeSlot[i].setPowerFlowDirection((PowerFlowDirection) ((JComboBox<?>)groups[i][3]).getSelectedItem());
    }    
  }

  
  private void customiseFocusTraversalPolicy(JComponent[][] compGroups) {
    Vector<JComponent> order = new Vector<>();
    for (int i = 0; i < compGroups.length; i++) {
      for (int j = 0; j < compGroups[i].length; j++) {
        if(compGroups[i][j] instanceof JSpinner) {
          JSpinner jsp = (JSpinner) compGroups[i][j];
          order.add(((JSpinner.DefaultEditor) jsp.getEditor()).getTextField());
        }else
          order.add(compGroups[i][j]);
      }
    }
    this.setFocusTraversalPolicyProvider(true);
    this.setFocusTraversalPolicy(new CustomFocusTraversalPolicy(order));
  }
  
  private void initEventHandling() {
    ChangeListener timeChgListener = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        validateSettings();
      }
    };
    
    for (int i = 0; i < enables.length; i++) {
      enables[i].addItemListener(new EnableActionListener(i));
      setComponentsEnabled(i, enables[i].isSelected());
      
      ((JSpinner)groups[i][0]).addChangeListener(timeChgListener);
      ((JSpinner)groups[i][1]).addChangeListener(timeChgListener);
    }
    
    
  }

  private void setComponentsEnabled(int group, boolean enabled) {
    int len = groups[group].length;
    for (int i = 0; i < len; i++) {
      groups[group][i].setEnabled(enabled);
    }
    
    validateSettings();
  }
  
  private JFormattedTextField createValueField() {
    JFormattedTextField tf = new JFormattedTextField();
    tf.setFormatterFactory(new DefaultFormatterFactory(TCLPowerSettings.POWER_VALUE_FORMATTER));
    tf.setText("0");
    return tf;
  }
  
  private JSpinner createTimeField() {
    JSpinner timeField = new JSpinner(new SpinnerDateModel());
    DateEditor editor = new JSpinner.DateEditor(timeField, "HH:mm");
    editor.getFormat().setTimeZone(TZ);
    timeField.setEditor(editor);
    
    // Set default time value
    Calendar cal = Calendar.getInstance(TZ);
    cal.setTimeInMillis(0);
    timeField.setValue(cal.getTime());
    
    return timeField;
  }

  private JComboBox<Object> createDirectionComobox() {
    JComboBox<Object> combo = new JComboBox<>(
        new DefaultComboBoxModel<Object>(PowerFlowDirection.values()));
    combo.setRenderer(new DefaultListCellRenderer() {

      @Override
      public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
          boolean cellHasFocus) {
        Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        PowerFlowDirection dir = (PowerFlowDirection) value;
        if(dir != null) {
          setToolTipText(dir.getDescription());
          setText(String.format("%s (%s)", dir.getSymbol(),dir.getDescription()));
        }
        return comp;
      }
      
    });
    ComboBoxUtil.makeWider(combo);
    return combo;
  }

  private class EnableActionListener implements ItemListener {

    private final int group;

    EnableActionListener(int group) {
      this.group = group;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
      JCheckBox cbox = (JCheckBox) e.getSource();
      setComponentsEnabled(group, cbox.isSelected());
    }
  }
  
  private static class CustomFocusTraversalPolicy
      extends FocusTraversalPolicy {

    private Vector<Component> order;


    public CustomFocusTraversalPolicy(Vector<JComponent> order) {
      this.order = new Vector<Component>(order.size());
      this.order.addAll(order);
    }

    public Component getComponentAfter(Container focusCycleRoot,
        Component aComponent) {
      int idx = (order.indexOf(aComponent) + 1) % order.size();
      Component focus = order.get(idx);
      
      if(focus.isEnabled() == false) {
        return getComponentAfter(focusCycleRoot, focus);
      } else {
        return focus;
      }
    }

    public Component getComponentBefore(Container focusCycleRoot,
        Component aComponent) {
      int idx = order.indexOf(aComponent) - 1;
      if (idx < 0) {
        idx = order.size() - 1;
      }
      Component component = order.get(idx);
      if(component.isEnabled() == false) {
        return getComponentBefore(focusCycleRoot, component);
      } else {
        return component;
      }
    }

    public Component getDefaultComponent(Container focusCycleRoot) {
      return order.get(0);
    }

    public Component getLastComponent(Container focusCycleRoot) {
      return order.lastElement();
    }

    public Component getFirstComponent(Container focusCycleRoot) {
      return order.get(0);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    checkBoxEnable0 = new JCheckBox();
    ftfStart0 = createTimeField();
    ftfEnd0 = createTimeField();
    ftfValue0 = createValueField();
    comboDirection0 = createDirectionComobox();
    checkBoxEnable1 = new JCheckBox();
    ftfStart1 = createTimeField();
    ftfEnd1 = createTimeField();
    ftfValue1 = createValueField();
    comboDirection1 = createDirectionComobox();
    checkBoxEnable2 = new JCheckBox();
    ftfStart2 = createTimeField();
    ftfEnd2 = createTimeField();
    ftfValue2 = createValueField();
    comboDirection2 = createDirectionComobox();
    checkBoxEnable3 = new JCheckBox();
    ftfStart3 = createTimeField();
    ftfEnd3 = createTimeField();
    ftfValue3 = createValueField();
    comboDirection3 = createDirectionComobox();
    lblError = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
      "right:default, 4*($ugap, 40dlu)",
      "5*(default, $lgap), fill:default"));

    //---- label1 ----
    label1.setText("Enabled:");
    add(label1, CC.xy(1, 1));

    //---- label2 ----
    label2.setText("Start Time:");
    add(label2, CC.xy(1, 3));

    //---- label3 ----
    label3.setText("End Time:");
    add(label3, CC.xy(1, 5));

    //---- label4 ----
    label4.setText("Value (A):");
    add(label4, CC.xy(1, 7));

    //---- label5 ----
    label5.setText("Power Flow Direction:");
    add(label5, CC.xy(1, 9));

    //---- checkBoxEnable0 ----
    checkBoxEnable0.setHorizontalAlignment(SwingConstants.CENTER);
    add(checkBoxEnable0, CC.xy(3, 1));
    add(ftfStart0, CC.xy(3, 3));
    add(ftfEnd0, CC.xy(3, 5));
    add(ftfValue0, CC.xy(3, 7));
    add(comboDirection0, CC.xy(3, 9));

    //---- checkBoxEnable1 ----
    checkBoxEnable1.setHorizontalAlignment(SwingConstants.CENTER);
    add(checkBoxEnable1, CC.xy(5, 1));
    add(ftfStart1, CC.xy(5, 3));
    add(ftfEnd1, CC.xy(5, 5));
    add(ftfValue1, CC.xy(5, 7));
    add(comboDirection1, CC.xy(5, 9));

    //---- checkBoxEnable2 ----
    checkBoxEnable2.setHorizontalAlignment(SwingConstants.CENTER);
    add(checkBoxEnable2, CC.xy(7, 1));
    add(ftfStart2, CC.xy(7, 3));
    add(ftfEnd2, CC.xy(7, 5));
    add(ftfValue2, CC.xy(7, 7));
    add(comboDirection2, CC.xy(7, 9));

    //---- checkBoxEnable3 ----
    checkBoxEnable3.setHorizontalAlignment(SwingConstants.CENTER);
    add(checkBoxEnable3, CC.xy(9, 1));
    add(ftfStart3, CC.xy(9, 3));
    add(ftfEnd3, CC.xy(9, 5));
    add(ftfValue3, CC.xy(9, 7));
    add(comboDirection3, CC.xy(9, 9));

    //---- lblError ----
    lblError.setForeground(Color.red);
    add(lblError, CC.xywh(1, 11, 9, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JCheckBox checkBoxEnable0;
  private JSpinner ftfStart0;
  private JSpinner ftfEnd0;
  private JFormattedTextField ftfValue0;
  private JComboBox<Object> comboDirection0;
  private JCheckBox checkBoxEnable1;
  private JSpinner ftfStart1;
  private JSpinner ftfEnd1;
  private JFormattedTextField ftfValue1;
  private JComboBox<Object> comboDirection1;
  private JCheckBox checkBoxEnable2;
  private JSpinner ftfStart2;
  private JSpinner ftfEnd2;
  private JFormattedTextField ftfValue2;
  private JComboBox<Object> comboDirection2;
  private JCheckBox checkBoxEnable3;
  private JSpinner ftfStart3;
  private JSpinner ftfEnd3;
  private JFormattedTextField ftfValue3;
  private JComboBox<Object> comboDirection3;
  private JLabel lblError;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
