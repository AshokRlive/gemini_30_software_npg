/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.motorsupply;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.LabelledFTFSupport;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;

/**
 * The Class MotorSupplyLogicSettingsEditor.
 */
class MotorSupplyLogicSettingsEditor extends AbstractCLogicSettingsEditor {
  private static final String LABEL_TIMEOUT      = "Time out";
  
  public MotorSupplyLogicSettingsEditor(MotorSupplyLogicSettings settings) {
    super(settings);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
	 DefaultFormBuilder builder = getDefaultBuilder();

     JFormattedTextField tf = createLongField(LABEL_TIMEOUT, MotorSupplyLogicSettings.PROPERTY_TIMEOUT_SECS, Units.SECS);
     //LabelledFTFSupport.decorateWithLabel(tf, 0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
     
     return builder.getPanel();
  }

  @Override
  public void validate(ValidationResult result) {
    checkRange(result, pm, 1, 255, MotorSupplyLogicSettings.PROPERTY_TIMEOUT_SECS, LABEL_TIMEOUT);
  }

}
