/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.binarytoanalog;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;


class BinaryToAnalogueSettingsEditor extends AbstractCLogicSettingsEditor {
  private static final String LABEL_DEBOUNCE = "Debounce";
  
  private final static long DEBOUNCE_MIN = 10; //ms
  private final static long DEBOUNCE_MAX = 10000; //ms
  public BinaryToAnalogueSettingsEditor(BinaryToAnalogueSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkRange(result, pm, DEBOUNCE_MIN, DEBOUNCE_MAX,
        BinaryToAnalogueSettings.PROPERTY_DEBOUNCE, LABEL_DEBOUNCE);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    createLongField(LABEL_DEBOUNCE, BinaryToAnalogueSettings.PROPERTY_DEBOUNCE, Units.MS);
    
    return builder.getPanel();
  }

}

