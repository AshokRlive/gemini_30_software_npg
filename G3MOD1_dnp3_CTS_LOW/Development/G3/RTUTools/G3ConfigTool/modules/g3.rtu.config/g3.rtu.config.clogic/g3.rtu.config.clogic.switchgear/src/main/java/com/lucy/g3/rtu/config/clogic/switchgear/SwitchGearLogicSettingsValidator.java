/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.switchgear;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.xml.gen.common.MainAppEnum;


/**
 * The validator for SwitchGearLogicSettings.
 */
public class SwitchGearLogicSettingsValidator extends AbstractValidator<SwitchGearLogic>{
  private Logger log = Logger.getLogger(SwitchGearLogicSettingsValidator.class);
  
  public SwitchGearLogicSettingsValidator(SwitchGearLogic target) {
    super(target);
  }

  /**
   * @see <a href="http://10.11.253.18/work_packages/2847">#2847</a>.
   */
  public static String validateOperationDelay(int preOpDelayMs, SwitchModuleOutput output) {
    if(output == null)
      return null;
    
    String error = null;
    long MAX_DELAY_MS = MainAppEnum.SGL_MSUPPLY_MAX_OVERALL_TIME_MS;
    long SAFETY_THRESHOLD_MS = MainAppEnum.SGL_MSUPPLY_SECURITY_THR_MS;
    
    try {
    IOModule module = (IOModule) output.getModule();
    IChannel openChannel = module.getChByEnum(output.getOpenEnumForOutput());
    IChannel closeChannel = module.getChByEnum(output.getCloseEnumForOutput());
    long overrunMs0 = (long) openChannel.getParameter(ICANChannel.PARAM_OVERRUN_MS);
    long overrunMs1 = (long) closeChannel.getParameter(ICANChannel.PARAM_OVERRUN_MS);
    long opTimeoutSec0 = (long) openChannel.getParameter(ICANChannel.PARAM_OPTIMEOUT_SEC);
    long opTimeoutSec1 = (long) closeChannel.getParameter(ICANChannel.PARAM_OPTIMEOUT_SEC);
    long overrunMs = Math.max(overrunMs0, overrunMs1);
    long opTimeoutMs = Math.max(opTimeoutSec0, opTimeoutSec1) * 1000;
    
    if(preOpDelayMs + opTimeoutMs + overrunMs + SAFETY_THRESHOLD_MS > MAX_DELAY_MS )
      error = String.format("Motor supply pre-operation time is too big!");
    
    } catch (Exception e) {
      Logger.getLogger(SwitchGearLogicSettingsValidator.class).error("Failed to validat switch settings.",e);
    }
    
    return error;
  }

  @Override
  public String getTargetName() {
    return target.getName();
  }

  @Override
  protected void validate(ValidationResultExt result) {
    if (target == null) {
      log.error("Cannot validate. Owner clogic is null");
      return;
    }
    
    String err = validateOperationDelay(target.getSettings().getPreOpDelay(), target.getOutputSCM());
    if (err != null) {
      result.addError(err);
    }
  }
}

