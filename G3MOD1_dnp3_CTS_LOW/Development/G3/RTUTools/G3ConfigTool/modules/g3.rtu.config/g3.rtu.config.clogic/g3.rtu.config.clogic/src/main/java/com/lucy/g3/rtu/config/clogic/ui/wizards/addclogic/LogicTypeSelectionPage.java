/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogicType;

/**
 * Wizard page for selecting logic type.
 */
class LogicTypeSelectionPage extends WizardPage {
  
  public static final String ID = "LogicTypeSelection"; 

  private final CLogicManager manager;

  /**
   * The key for accessing current selected logic type from Settings Map. <li>
   * Value type: {@link ChannelType}.</li>
   */
  static final String KEY_CLOGIC_TYPE = "selectedClogicType";

  public LogicTypeSelectionPage(CLogicManager manager) {
    super(ID, "Select a type");
    this.manager = manager;

    initComponents();

    /*
     * Note: Name is necessarily needed for storing this component value to
     * Settings Map.
     */
    combLogicType.setName(KEY_CLOGIC_TYPE);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    ICLogicType curType = (ICLogicType) combLogicType.getSelectedItem();

    if (curType == null) {
      return "Please select a control logic type";
    }

    int curAmount = manager.getLogicByType(curType.getId()).size();
    if (curAmount >= curType.getMaxAmount()) {
      return "Not allowed to add more \"" + curType.getTypeName()+"\"";
    }

    return null; // Valid
  }

  private void createUIComponents() {
    ICLogicType[] alltypes = CLogicFactories.getAllLogicTypes();
    alltypes = findAllowToAdd(alltypes);
    Arrays.sort(alltypes, 0, alltypes.length, new TypeComparator());
    combLogicType = new JComboBox<Object>(alltypes);
    combLogicType.addItem(null);
    combLogicType.setRenderer(new CombLogicTypeRenderer());
    combLogicType.setSelectedItem(null); // Initial selection.
    combLogicType.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ICLogicType type = (ICLogicType) combLogicType.getSelectedItem();
        labelTips.setText(type == null ? null : "* "+type.getTypeDescription());
      }
    });
    AutoCompleteDecorator.decorate(combLogicType, new ObjectToStringConverter() {
      @Override
      public String getPreferredStringForItem(Object arg0) {
        if(arg0 != null) {
          ICLogicType type = (ICLogicType) arg0;
          return type.getTypeName();
        }else {
          return null;
        }
      }
    });
    
    labelTips = new JXLabel();
    ((JXLabel)labelTips).setLineWrap(true);
  }
  
  private static ICLogicType[] findAllowToAdd(ICLogicType[] alltypes) {
    ArrayList<ICLogicType> allowToAdd = new ArrayList<>();
    for (int i = 0; i < alltypes.length; i++) {
      if(alltypes[i].allowUserAdd()) {
        allowToAdd.add(alltypes[i]);
      }
    }
    
    return allowToAdd.toArray(new ICLogicType[allowToAdd.size()]);
  }

  private class TypeComparator implements Comparator<ICLogicType> {

    @Override
    public int compare(ICLogicType o1, ICLogicType o2) {
      if (o1 == null)
        return 1;
      else if (o2 == null)
        return -1;
      else
        return String.CASE_INSENSITIVE_ORDER.compare(o1.getTypeName(), o2.getTypeName());
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    labelLogicType = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "right:default, $lcgap, [50dlu,default]:grow",
        "default, $ugap, bottom:default:grow"));
    add(combLogicType, CC.xy(3, 1));

    //---- labelLogicType ----
    labelLogicType.setText("Control Logic Type:");
    add(labelLogicType, CC.xy(1, 1));

    //---- labelTips ----
    labelTips.setForeground(Color.gray);
    add(labelTips, CC.xywh(1, 3, 3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JComboBox<Object> combLogicType;
  private JLabel labelLogicType;
  private JLabel labelTips;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static class CombLogicTypeRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
        int index, boolean isSelected, boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected,
          cellHasFocus);
      ICLogicType type = (ICLogicType) value;
      if(index >= 0 && type != null)
        this.setText(String.format("[%d] %s", index, type == null ? "" : type.getTypeName()));
      else
        this.setText(type == null ? "  " : type.getTypeName());
      return this;
    }
  }
}
