package com.lucy.g3.rtu.config.clogic.tcl;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;

class TCLTrippingSettingsEditor implements ICLogicSettingsEditor {
    private TCLTrippingSettingsPanel comp ;
    private final ICLogicSettings settings;

    public TCLTrippingSettingsEditor(ICLogicSettings settings){
      this.settings = settings;
    }
    
    @Override
    public void triggerCommit() {
      if(comp != null) {
        comp.save();
      }
    }

    @Override
    public void triggerFlush() {
      // TODO Auto-generated method stub
      
    }

    @Override
    public void release() {
      // TODO Auto-generated method stub
      
    }

    @Override
    public void validate(ValidationResult result) {
      if(comp != null) {
        if(comp.validateSettings() == false)
          result.addError(ERR_MSG_INVALID_LOGIC_SETTINGS);
      }
        
    }

    @Override
    public void postCommitAction() {
      // TODO Auto-generated method stub
      
    }

    @Override
    public JComponent getComponent(JDialog parent) {
      if(comp == null) {
        comp = new TCLTrippingSettingsPanel();
        comp.load(settings);
      }
      return comp;
    }
    
  }