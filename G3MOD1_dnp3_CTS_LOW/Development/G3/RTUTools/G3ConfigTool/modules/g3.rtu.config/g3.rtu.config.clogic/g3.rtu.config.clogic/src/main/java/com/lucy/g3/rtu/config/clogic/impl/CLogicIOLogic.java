/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicRepository;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * The implementation of control logic IO which is for configured a mapped
 * Control Logic.
 */
public final class CLogicIOLogic extends AbstractCLogicIO<ICLogic> {

  private final int[] supportedTypes;
  private Logger log = Logger.getLogger(CLogicIOLogic.class);

  /**
   * Create an logic IO that is associated with a control logic.
   *
   * @param owner
   *          The owner control logic of this IO
   * @param label
   *          The name of this IO
   * @param optional
   *          Indicates if this IO is optionally configured.
   */
  public CLogicIOLogic(ICLogic owner, String label, int[] supportedTypeIds,
      boolean optional, boolean enabled, boolean allowUserChangeLabel, boolean allowUserDelete) {
    super(owner, IODirection.OUTPUT, label, allowUserChangeLabel, allowUserDelete, optional, enabled);
    this.supportedTypes = supportedTypeIds;

  }
  
  private static String createWarningMsg(int[] supportedLogicTypes) {
    StringBuilder sb = new StringBuilder();
    sb.append("No Control Logic found.");
    if (supportedLogicTypes != null) {
      sb.append("Control Logic: ");
      for (int i = 0; i < supportedLogicTypes.length; i++) {
        sb.append(CLogicFactories.getFactory(supportedLogicTypes[i]).getLogicType().getTypeName());
        if (i != supportedLogicTypes.length - 1) {
          sb.append("/ ");
        }
      }
      sb.append(" is required");
    }

    return sb.toString();
  }
  
  @Override
  public void chooseValue(Frame parent) {
    Object selection = showChooser(parent);
    if(selection == null || selection instanceof IOperableLogic) {
      try {
        setValue((IOperableLogic) selection); 
      } catch (CLogicOutputConflictException e1) {
        JOptionPane.showMessageDialog(parent, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }
  
  @Override
  public Object showChooser(Frame parent) {

    Collection<ICLogic> sources = getAvailableSources();

    if (sources == null || sources.size() == 0) {
      JOptionPane.showMessageDialog(parent, createWarningMsg(supportedTypes),
          "Control Logic Required", JOptionPane.WARNING_MESSAGE);
      return CHOOSER_CANCELLED;
      
    } else {
      Object sel = JOptionPane.showInputDialog(parent,
          "Select a output control logic:", // Message
          "Select",// Title
          JOptionPane.QUESTION_MESSAGE,
          null,
          sources.toArray(),
          getValue());// initially selected clogic
      
      if (sel != null) {
        return sel;
      }else {
        return CHOOSER_CANCELLED;
      }
    }

  }

  @Override
  public boolean isAcceptable(ICLogic ioValue) {
    if (ioValue == null) {
      return true;
    }

    if (!(ioValue instanceof IOperableLogic)) {
      log.error("This control logic cannot be operated by control logic:" + ioValue);
      return false;
    }

    // Validate new value
    if (supportedTypes != null) {
      boolean validType = false;
      for (int t : supportedTypes) {
        if (t == ioValue.getType().getId()) {
          validType = true;
          break;
        }
      }

      if (validType == false) {
        return false;
      }
    }

    return true;
  }

  @Override
  public String getSummary() {
    ICLogic clogic = getValue();
    return "Output Control Logic: "
        + (clogic == null ? "None" : clogic.getName());
  }

  @Override
  public Class<ICLogic> getSourceClass() {
    return ICLogic.class;
  }

  @Override
  public Collection<ICLogic> getAvailableSources() {
    IConfig config = ConfigManager.getInstance().getLocalConfig();
    CLogicRepository repo = config == null ? null
            :(CLogicRepository) config.getConfigModule(CLogicRepository.CONFIG_MODULE_ID);
    
    return repo == null ? new ArrayList<ICLogic>(0) : repo.getLogicByType(supportedTypes);
  }

 

}
