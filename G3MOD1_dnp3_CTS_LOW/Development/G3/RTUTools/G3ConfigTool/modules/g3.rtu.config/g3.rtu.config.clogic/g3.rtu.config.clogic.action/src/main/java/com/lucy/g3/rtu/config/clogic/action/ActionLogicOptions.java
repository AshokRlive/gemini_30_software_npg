/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.action;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * The Class ActionLogicOptions.
 */
class ActionLogicOptions extends WizardPage {

  /**
   * The key for the value:{@value} . <li>Value type: {@link Boolean}.</li>
   */
  static final String KEY_ACTION_INPUT_NUM = "Action Input Number";


  public ActionLogicOptions() {
    super("Configuration");
    initComponents();

    /*
     * Note: Name is necessarily needed for storing this component value to
     * Settings Map.
     */
    spinnerActionInputNum.setName(KEY_ACTION_INPUT_NUM);
  }
  

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    label4 = new JLabel();
    spinnerActionInputNum = new JSpinner();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, [50dlu,default]",
        "default"));

    //---- label4 ----
    label4.setText("Number of Inputs:");
    add(label4, CC.xy(1, 1));

    //---- spinnerActionInputNum ----
    spinnerActionInputNum.setModel(new SpinnerNumberModel(1, 1, 100, 1));
    add(spinnerActionInputNum, CC.xy(3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label4;
  private JSpinner spinnerActionInputNum;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
