/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fantest;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;


/**
 *
 */
public class FanTestSettingsEditor extends AbstractCLogicSettingsEditor {

  public FanTestSettingsEditor(ICLogicSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {

  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    ValueModel vm = pm.getBufferedModel(FanTestSettings.PROPERTY_DURATION_SECS);

    SpinnerNumberModel spinnerModel = SpinnerAdapterFactory.createNumberAdapter(vm, 10, 1, Integer.MAX_VALUE, 1);
    JSpinner durationSpinner = new JSpinner(spinnerModel);
    SpinnerWheelSupport.installMouseWheelSupport(durationSpinner);
    builder.append("Duration:", durationSpinner, new JLabel("seconds"));
  
    return builder.getPanel();
  }

}

