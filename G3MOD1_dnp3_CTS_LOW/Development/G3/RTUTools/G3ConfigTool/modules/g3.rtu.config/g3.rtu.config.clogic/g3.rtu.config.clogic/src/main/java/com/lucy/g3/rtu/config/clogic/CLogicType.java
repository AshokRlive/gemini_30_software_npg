
package com.lucy.g3.rtu.config.clogic;

import com.jgoodies.common.base.Preconditions;

/**
 * The default implementation of ICLogicType   
 */
public class CLogicType implements ICLogicType {
  private static final int DEFAULT_MAX_LOGIC_AMOUNT = 128;
  
  private int logicTypeId;
  private int maxAmount = DEFAULT_MAX_LOGIC_AMOUNT;
  private OperationMode opMode = OperationMode.NOT_OPERATBLE;
  private boolean requireShowingSettingWhenChangeIO = false;
  private boolean requireDurationWhenTest = false;
  private boolean requireOLR = false;
  private boolean allowUserAdd = true;
  private boolean allowUserRemove = true;

  private final String typeName;
  private String typeDescription;
  private String instanceName;
  
  
  private CLogicType(int logicTypeId, String typeName) {
    Preconditions.checkArgument(logicTypeId >= 0, "logicTypeId must not be negative");
    this.logicTypeId = logicTypeId;
    this.typeName = Preconditions.checkNotBlank(typeName, "fullname must not be blank");
    this.typeDescription = typeName;
  }

  @Override
  public final int getId() {
    return logicTypeId;
  }

  @Override
  public int getMaxAmount() {
    return maxAmount;
  }

  @Override
  public boolean isOperable() {
    return opMode != OperationMode.NOT_OPERATBLE;
  }

  @Override
  public OperationMode getOpMode() {
    return opMode;
  }

  @Override
  public boolean requireShowingSettingWhenChangeIO() {
    return requireShowingSettingWhenChangeIO;
  }

  @Override
  public boolean requireDurationWhenTest() {
    return requireDurationWhenTest;
  }

  @Override
  public boolean requireOLR() {
    return requireOLR;
  }
  
  @Override
  public boolean allowUserAdd() {
    return allowUserAdd;
  }
  
  @Override
  public boolean allowUserRemove() {
    return allowUserRemove;
  }
  
  @Override
  public String getTypeName() {
    return typeName;
  }

//  @Override
//  public String getShortName() {
//    return shortName;
//  }

  @Override
  public String getTypeDescription() {
    return typeDescription;
  }
  
  @Override
  public String getInstanceName() {
    return instanceName;
  }
  
  public static CLogicTypeBuilder builder(int logicTypeId, String name) {
    return new CLogicTypeBuilder(logicTypeId, name);
  }
  public static class CLogicTypeBuilder {
    private final CLogicType type;
    public CLogicTypeBuilder(int logicTypeId, String name) {
      type = new CLogicType(logicTypeId, name);
    }
    
    public CLogicTypeBuilder desp(String desp) {
      type.typeDescription = desp;
      return this;
    }
    
    public CLogicTypeBuilder instanceName(String instanceName){
      type.instanceName = instanceName;
      return this;
    }
    
    public CLogicTypeBuilder allowUserAdd(boolean allowUserAdd) {
      type.allowUserAdd = allowUserAdd;
      return this;
    }
    
    public CLogicTypeBuilder allowUserRemove(boolean allowUserRemove) {
      type.allowUserRemove = allowUserRemove;
      return this;
    }
    
    public CLogicTypeBuilder requireOLR(boolean requireOLR) {
      type.requireOLR = requireOLR;
      return this;
    }
    
    public CLogicTypeBuilder requireDurationWhenTest(boolean requireDurationWhenTest) {
      type.requireDurationWhenTest = requireDurationWhenTest;
      return this;
    }
    
    public CLogicTypeBuilder requireShowingSettingWhenChangeIO(boolean requireShowingSettingWhenChangeIO) {
      type.requireShowingSettingWhenChangeIO = requireShowingSettingWhenChangeIO;
      return this;
    }
    
    public CLogicTypeBuilder opMode(OperationMode opMode) {
      type.opMode = opMode;
      return this;
    }
    
    public CLogicTypeBuilder maxAmount(int maxAmount) {
      type.maxAmount = maxAmount;
      return this;
    }
    
    public CLogicType get() {
      return type;
    }
  }

  
}

