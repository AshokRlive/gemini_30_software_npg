/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.switchgear;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOSwitch;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.FILTER;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_POINT;

/**
 * <p>
 * Implementation of Switchgear control logic.
 * </p>
 * <i>inputs:</i>
 * <ul>
 * <li>Status[Mandatory]</li>
 * <li>LowInsulation</li>
 * <li>ActuatorDisabled[Mandatory]</li>
 * <li>OffLocalRemote</li>
 * <li>Earthed</li>
 * <li>InterLock</li>
 * </ul>
 * <i>outputs:</i> <li>OutputSwitchgear</li>
 * <ul>
 * </ul>
 * Note switchgear logic has both analogue and digital internal points, their
 * IDs are consistent when they are initialised.
 */
public final class SwitchGearLogic extends PredefinedCLogic<SwitchGearLogicSettings> implements ISupportOffLocalRemote, IOperableLogic {

  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.SGL, "Switchgear")
      .desp("The control logic for operating a switch.")
      .opMode(OperationMode.OPEN_CLOSE)
      .requireOLR(true)
      .get();
  
  private final CLogicIOSwitch[] outputs;


  public SwitchGearLogic(SwitchModuleOutput output) {
    this();
    setOutputSCM(output);
  }

  public SwitchGearLogic() {
    super(TYPE, SGL_INPUT.values(), SGL_POINT.values());

    outputs = new CLogicIOSwitch[SGL_OUTPUT.values().length];
    initOutputs();

    bindDescriptionToIO(outputs[0]);
    
    /*Initialise filter*/
    PseudoAnaloguePoint peakMotorCurrent = (PseudoAnaloguePoint) getPointByID(SGL_POINT.SGL_POINT_PEAKMOTORCURRENT.getValue());
    peakMotorCurrent.setFilterType(FILTER.None);
  }

  
  @Override
  protected SwitchGearLogicSettings createSettings() {
    return new SwitchGearLogicSettings(this);
  }
  
  private void initOutputs() {
    SGL_OUTPUT[] outputEnums = SGL_OUTPUT.values();

    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new CLogicIOSwitch(this, outputEnums[i].getDescription());
    }
  }

  // ================== End of Attributes Accessors ===================

  public CLogicIO<VirtualPoint> getInput(SGL_INPUT inputItem) {
    return getInputByEnum(inputItem);
  }

  public void setOutputSCM(SwitchModuleOutput output) {
    outputs[SGL_OUTPUT.SGL_OUTPUT_SWITCHGEAR.ordinal()].setValue(output);
  }

  public SwitchModuleOutput getOutputSCM() {
    int index = SGL_OUTPUT.SGL_OUTPUT_SWITCHGEAR.ordinal();
    return outputs[index].getValue();
  }

  @Override
  public CLogicIO<SwitchModuleOutput>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInput(SGL_INPUT.SGL_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public Module getSourceModule() {
    SwitchModuleOutput output = getOutputSCM();
    return output == null ? null : output.getModule();
  }

  @Override
  public boolean isSwitchLogic() {
    return true;
  }
  
}
