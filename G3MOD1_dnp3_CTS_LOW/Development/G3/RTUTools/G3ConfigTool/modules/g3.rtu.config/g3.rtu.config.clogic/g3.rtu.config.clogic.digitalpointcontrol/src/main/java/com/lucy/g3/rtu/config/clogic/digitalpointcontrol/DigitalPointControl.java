/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.digitalpointcontrol;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DIGCTRL_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DIGCTRL_POINT;

/**
 * Implementation of Digital Control Control Logic.
 */
public class DigitalPointControl extends PredefinedCLogic<DigitalPointControlSettings> implements ISupportOffLocalRemote, IOperableLogic {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.DIGCTRL, "Digital Control Point")
      .desp("Control Logic for controlling Digital Point ")
      .opMode(OperationMode.OPEN_CLOSE)
      .requireOLR(true)
      .get();
  
  public DigitalPointControl() {
    super(TYPE, DIGCTRL_INPUT.values(), DIGCTRL_POINT.values());

  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInputByEnum(DIGCTRL_INPUT.DIGCTRL_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  protected DigitalPointControlSettings createSettings() {
    return new DigitalPointControlSettings(this);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public boolean isSwitchLogic() {
    return true;
  }
}
