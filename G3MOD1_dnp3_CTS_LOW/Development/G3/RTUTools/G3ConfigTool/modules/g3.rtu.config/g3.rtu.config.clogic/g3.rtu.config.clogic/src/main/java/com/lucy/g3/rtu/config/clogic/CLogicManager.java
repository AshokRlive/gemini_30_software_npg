/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import java.util.Collection;

import com.g3schema.ns_clogic.ControlLogicsT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.manager.IListConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;

/**
 * Interface for managing all Control Logic instances.
 */
public interface CLogicManager extends IListConfigModule<ICLogic>, CLogicRepository, IContainerValidation {

  String CONFIG_MODULE_ID = CLogicRepository.CONFIG_MODULE_ID;
  
  /**
   * Checks if this manager contains a control logic with a specific name.
   *
   * @param name
   *          the control logic name to be checked
   * @return <code>true</code> if the control logic with the given name already
   *         exists, <code>false</code> otherwise.
   */
  boolean checkCustomNameExisting(String name);

  /**
   * Gets a logic in specific index in logic list model.
   *
   * @param index
   *          the index of logic to be retrieved.
   * @return a found logic or <code>null</code> if not found.
   */
  // CLogic getLogicByIndex(int index);

  /**
   * Gets all control logics by specifying control logic class.
   */
  <T extends ICLogic> Collection<T> getAllControlLogics(Class<T> logicClass);

  /**
   * Gets fixture control logic "OffLocalRemote".
   *
   * @return non-null OffLocalRemote control logic.
   */
  //OffLocalRemote getOLR();
  PseudoDoubleBinaryPoint getOLRStatus();


  @Override
  IContainerValidator getValidator();

  void writeToXML(ControlLogicsT append, ValidationResult result);

  void read(ControlLogicsT first, VirtualPointReadSupport support) throws InvalidConfException;

  void configureIO(ControlLogicsT xml, VirtualPointReadSupport support) throws InvalidConfException;

  VirtualPointManager getVirtualPointsManager();
  
  boolean swap(ICLogic p0, ICLogic p1);
}