/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.countercommand;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.xml.gen.common.ControlLogicDef.COUNTER_COMMAND_ACTION;


class CounterCommandSettingsEditor extends AbstractCLogicSettingsEditor{

  public CounterCommandSettingsEditor(CounterCommandSettings settings) {
    super(settings);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    
    COUNTER_COMMAND_ACTION[] actions = COUNTER_COMMAND_ACTION.values();
    JComponent comp = BasicComponentFactory.createComboBox(new SelectionInList<>(actions,
        pm.getBufferedModel(CounterCommandSettings.PROPERTY_ACTION)));
    builder.append("Action", comp);
    return builder.getPanel();
  }

  @Override
  public void validate(ValidationResult result) {
  }

}

