/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;


import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The Class OutputsIOManagerImpl.
 */
public final class OutputsManagerImpl extends AbstractIOManager<ICLogic>
    implements IOutputsManager<ICLogic> {

  private final ICLogic owner;


  public OutputsManagerImpl(ICLogic owner) {
    this.owner = owner;
  }

  @Override
  public void addOutput(int number, boolean userCreated, String...label) {
    for (int i = 0; i < number; i++) {
      addIO(new CLogicIOLogic(owner, i < label.length ? label[i] : null,
          CLogicFactories.getAllOperatibleTypes(), false, true, userCreated, userCreated));
    }
  }
  
  @Override
  public void setOutputs(Collection<CLogicIO<ICLogic>> outputs) {
    /* Coping mapping from existing outputs to the new outputs*/
    int index = 0;
    for (CLogicIO<ICLogic> output: outputs) {
      CLogicIO<ICLogic> existingOutput= get(index);
      if(existingOutput != null){
        try {
          output.setValue(existingOutput.getValue());
        }catch (Exception e) {
        }
      }
      index ++;
    }
    
    removeAll();
    addIO(outputs);
  }
}
