/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * The implementation of control logic IO which is for configuring a mapped
 * switch module.
 */
public final class CLogicIOSwitch extends AbstractCLogicIO<SwitchModuleOutput> {

  private final MODULE[] supportedTypes = { MODULE.MODULE_DSM, MODULE.MODULE_SCM,MODULE.MODULE_SCM_MK2 };

  private final PropertyChangeListener valueNamePCL = new SwitchOutputPCL();


  public CLogicIOSwitch(ICLogic owner, String label) {
    super(owner, IODirection.OUTPUT, label, false, false, false, true);
  }

  @Override
  public void setValue(SwitchModuleOutput newValue) {
    SwitchModuleOutput oldValue = getValue();
    if (oldValue != null) {
      oldValue.removePropertyChangeListener(SwitchModuleOutput.PROPERTY_NAME, valueNamePCL);
    }

    super.setValue(newValue);

    if (newValue != null) {
      newValue.addPropertyChangeListener(SwitchModuleOutput.PROPERTY_NAME, valueNamePCL);
    }
  }

  @Override
  public void chooseValue(Frame parent) {
    Object selection = showChooser(parent);
    if(selection == null || selection instanceof SwitchModuleOutput) {
      try {
        setValue((SwitchModuleOutput) selection);
      } catch (CLogicOutputConflictException e1) {
        // Fail to set new module value
        JOptionPane.showMessageDialog(parent, e1.getMessage(), "Error",
            JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  @Override
  public Object showChooser(Frame parent) {
    Collection<SwitchModuleOutput> selections = getAvailableSources();

    // Show warning dialog
    if (selections.isEmpty()) {
      JOptionPane.showMessageDialog(parent,
          createWarningMsg(supportedTypes),
          "Module Required", JOptionPane.ERROR_MESSAGE);
      return CHOOSER_CANCELLED;
      
    } else {
      // Show module selection dialog
      ImageIcon icon = null;
      if (supportedTypes != null && supportedTypes.length > 0) {
        icon = ModuleResource.INSTANCE.getLargeModuleIcon(supportedTypes[0]);
      }

      // Select switch output
      SwitchModuleOutput selectedOutput = (SwitchModuleOutput) JOptionPane.showInputDialog(parent,
          "Select Switch Module:", "Select",
          JOptionPane.QUESTION_MESSAGE, icon,
          selections.toArray(new SwitchModuleOutput[selections.size()]), // switch
          // outputs
          getValue());// initially selected switch module

      if (selectedOutput != null) {
        return selectedOutput;
      }else {
        return CHOOSER_CANCELLED;
      }
    }
  }

  @Override
  public boolean isAcceptable(SwitchModuleOutput ioValue) {
    if (ioValue == null) {
      return true;
    }

    // Validate new value
    if (this.supportedTypes != null) {
      boolean validType = false;
      for (MODULE t : this.supportedTypes) {
        if (t == ioValue.getModule().getType()) {
          validType = true;
          break;
        }
      }

      if (validType == false) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String getSummary() {
    SwitchModuleOutput output = getValue();
    return "Output Module: "
        + (output == null ? "None" : output.getName());
  }

  @Override
  public Class<SwitchModuleOutput> getSourceClass() {
    return SwitchModuleOutput.class;
  }


  private class SwitchOutputPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateValueStr();
    }
  }


  @Override
  public Collection<SwitchModuleOutput> getAvailableSources() {
    IConfig config = ConfigManager.getInstance().getLocalConfig();
    ModuleRepository repo = config == null ? null
        : (ModuleRepository) config.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);

    if (repo == null) {
      return new ArrayList<SwitchModuleOutput>(0);
    }

    Collection<SwitchModuleOutput> sources = repo.getAllSwitchModuleOutputs();
    ArrayList<SwitchModuleOutput> availableSources = new ArrayList<SwitchModuleOutput>();


    // Add usable outputs
    for (SwitchModuleOutput s : sources) {
      if (s.isUsable()) {
        availableSources.add(s);
      }
    }
    
    // Add current value to selections
    SwitchModuleOutput curVal = getValue();
    if(!availableSources.contains(curVal))
      availableSources.add(0, curVal);
    

    return availableSources;
  }
   
}
