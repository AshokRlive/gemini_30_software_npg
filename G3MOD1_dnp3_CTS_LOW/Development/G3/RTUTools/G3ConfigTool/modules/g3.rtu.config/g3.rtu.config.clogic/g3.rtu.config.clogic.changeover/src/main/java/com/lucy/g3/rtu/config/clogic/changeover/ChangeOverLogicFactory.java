
package com.lucy.g3.rtu.config.clogic.changeover;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class ChangeOverLogicFactory extends AbstractClogicFactory{
  public final static ChangeOverLogicFactory INSTANCE = new ChangeOverLogicFactory();
  private ChangeOverLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new ChangeOverSettingsEditor((ChangeOverSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new ChangeOver();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return ChangeOver.TYPE;
  }
}

