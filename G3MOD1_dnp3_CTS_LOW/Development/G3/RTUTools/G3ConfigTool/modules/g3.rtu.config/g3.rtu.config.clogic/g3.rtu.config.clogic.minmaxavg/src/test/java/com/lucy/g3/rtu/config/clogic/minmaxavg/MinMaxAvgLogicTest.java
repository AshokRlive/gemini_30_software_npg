/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.minmaxavg;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;

/**
 * The Class MinMaxAvgLogicTest.
 */
public class MinMaxAvgLogicTest {

  private MinMaxAvgLogic fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new MinMaxAvgLogic();
    fixture.init(true);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNameBindings() {
    final String newDecription = "NewDescription";
    final String customName = "customName";
    final String oldDescription = fixture.getDescription();

    IPseudoPoint[] points = fixture.getAllPoints();
    for (int i = 0; i < points.length; i++) {
      System.out.println("Old Point Name:" + points[i].getName());
    }
    fixture.setDescription(newDecription);
    assertNameContains("Expect name contains description", newDecription);

    fixture.setCustomName(customName);
    assertNameContains("Expect name contains customName", customName);

    fixture.setDescription(newDecription);
    assertNameContains("Expect name contains customName if custom name is set", customName);

    fixture.setCustomName("");
    assertNameContains("Expect name contains description if custom name is not set", newDecription);

    fixture.setDescription("");
    assertNameContains("Expect name is set to default(if neither custom name or description has not been set)",
        oldDescription);
  }

  private void assertNameContains(String message, String str) {
    System.out.println("\n" + message + ":" + str);
    assertTrue(fixture.getName().contains(str));

    IPseudoPoint[] points = fixture.getAllPoints();
    for (int i = 0; i < points.length; i++) {
      assertTrue("expect point name contains:" + str + " but actual:" + points[i].getName(),
          points[i].getName().contains(str));
    }
  }

}
