/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.xml;

import com.lucy.g3.rtu.config.clogic.IOperableLogic;

/**
 * This interface specifies the resources required by CLogicXMLReader.
 */
public interface ICLogicXmlReadSupport {

  IOperableLogic getOutputCLogic(int logicGroup);
}
