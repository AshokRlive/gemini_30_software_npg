/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import java.util.Collection;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * A Control Logic which implements this interface will have variable number of
 * inputs, which means its inputs may be added/removed by users.
 */
public interface IInputsManager extends IIOManager<VirtualPoint> {
  
  void addInputs(VirtualPointType[] inputTypes, int inputNum,
      boolean isMandatory, boolean allowUserChangeLabel, boolean allowUserDelete, String... label);
  
  void setInputs(Collection<CLogicIO<VirtualPoint>> inputs);
  
  VirtualPointType[] getSupportedInputTypes();
}
