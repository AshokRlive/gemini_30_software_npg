/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.generator.IConfigGenerator;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;

/**
 * Interface of Configurator for configuring Control Logic.
 */
public interface ICLogicGenerator extends IConfigGenerator {


  void createSwitchLogic(SwitchModuleOutput scmOutput);

  void createBatteryChargerLogic(IChannel selCh);

  void createDigitalOutputLogic(IChannel selCh);

  void createFanTestLogic(IChannel selCh);

  void createScreens(IOperableLogic newCreated);

  void configure(ICLogic clogic);
}
