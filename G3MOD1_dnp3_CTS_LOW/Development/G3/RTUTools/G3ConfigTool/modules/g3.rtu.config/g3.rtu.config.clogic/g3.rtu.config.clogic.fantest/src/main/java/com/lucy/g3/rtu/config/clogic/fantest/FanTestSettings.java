/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.fantest;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.FanTestParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;


/**
 *
 */
public class FanTestSettings extends AbstractCLogicSettings<FanTest> {
  
  public static final String PROPERTY_DURATION_SECS = "duration";
  
  private int duration = 10; // seconds
  
  public FanTestSettings(FanTest owner) {
    super(owner);
  }
  

  public int getDuration() {
    return duration;
  }

  public void setDuration(int duration) {
    if (duration <= 0) {
      return;
    }
    Object oldValue = getDuration();
    this.duration = duration;
    firePropertyChange(PROPERTY_DURATION_SECS, oldValue, duration);
  }

  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    FanTestParamT xml = xml_param.fanTestParam.first();
    setDuration((int) xml.duration.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    FanTestParamT xml_Fan = xml.fanTestParam.append();
    xml_Fan.duration.setValue(getDuration());
  }


  @Override
  public IValidator getValidator() {
    return null;
  }

}

