/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;


public class InputsManagerImpl extends AbstractIOManager<VirtualPoint>
      implements IInputsManager {

  private final ICLogic owner;
  private final VirtualPointType[] supportedInputTypes;
  
  public InputsManagerImpl(ICLogic owner,VirtualPointType[] supportedInputTypes) {
    super();
    this.owner = owner;
    this.supportedInputTypes = supportedInputTypes;
  }

  @Override
  public void addInputs(VirtualPointType[] inputTypes, int inputNum,
      boolean isMandatory, boolean allowUserChangeLabel, boolean allowUserDelete, String... label) {
    if (inputNum <= 0 || inputTypes == null) {
      return;
    }
    
    ArrayList<CLogicIO<VirtualPoint>> newInputs = new ArrayList<>();
    CLogicIOPoint newInput;
    for (int i = 0; i < inputNum; i++) {
      String labelStr = i < label.length ? label[i] : null;
      newInput = createNewInput(isMandatory, allowUserChangeLabel, allowUserDelete, inputTypes, labelStr);
      newInputs.add(newInput);
    }
    
    addIO(newInputs);
  }
  
  @Override
  public void setInputs(Collection<CLogicIO<VirtualPoint>> inputs) {
    /* Coping mapping from existing inputs to the new inputs*/
    int index = 0;
    for (CLogicIO<VirtualPoint> input: inputs) {
      CLogicIO<VirtualPoint> existingInput= get(index);
      if(existingInput != null){
        try {
          input.setValue(existingInput.getValue());
        }catch (Exception e) {
        }
      }
      index ++;
    }
    
    removeAll();
    addIO(inputs);
  }

  protected CLogicIOPoint createNewInput(boolean isMandatory, boolean allowUserChangeLabel, boolean allowUserDelete,
      VirtualPointType[] types, String labelStr) {
    CLogicIOPoint newInput;
    newInput = new CLogicIOPoint(owner, labelStr, types, allowUserChangeLabel, allowUserDelete, !isMandatory, true);
    return newInput;
  }

  @Override
  public VirtualPointType[] getSupportedInputTypes() {
    return Arrays.copyOf(supportedInputTypes, supportedInputTypes.length);
  }


}

