/*
 * Created by JFormDesigner on Tue Apr 11 20:11:22 BST 2017
 */

package com.lucy.g3.rtu.config.clogic.tcl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.DefaultFormatterFactory;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;

/**
 */
public class TCLTrippingSettingsPanel extends JPanel {
  private TCLTrippingSettings settings;
  public TCLTrippingSettingsPanel() {
    initComponents();
    initComponentNames(); //Required when this panel is used in a wizard
    initComponentsModel();
    initEventHandling();
    updateComponentsState();
  }
  
  private void initComponentNames() {
    ftfCBCurrentThreshold.setName("TCLTrippingSettingsPanel.ftfCBCurrentThreshold");
    ftfTimerComms.setName("TCLTrippingSettingsPanel.ftfTimerComms");
    ftfTimerTripSignal.setName("TCLTrippingSettingsPanel.ftfTimerTripSignal");
    checkboxCBTripEnable.setName("TCLTrippingSettingsPanel.checkboxCBTripEnable");
    checkboxCommsFailEnable.setName("TCLTrippingSettingsPanel.checkboxCommsFailEnable");
  }
  
  public void load(ICLogicSettings _settings) {
    TCLTrippingSettings settings = (TCLTrippingSettings) _settings;
    
    ftfCBCurrentThreshold.setValue(settings.getCbCurrentThreshold());
    ftfTimerComms.setValue(settings.getCommsFailTimer());
    ftfTimerTripSignal.setValue(settings.getTripSignalTimer());
    checkboxCBTripEnable.setSelected(settings.isCbTripEnable());
    checkboxCommsFailEnable.setSelected(settings.isCommsFailEnable());
    this.settings = settings;
    updateComponentsState();
  }

  public void save(){
    if(settings != null)
      save(settings);
  }

  
  public TCLTrippingSettings getSettings() {
    return settings;
  }
  
  public void save(ICLogicSettings _settings) {
    TCLTrippingSettings settings = (TCLTrippingSettings) _settings;
    settings.setCbTripEnable(checkboxCBTripEnable.isSelected() == Boolean.TRUE);
    settings.setCommsFailEnable(checkboxCommsFailEnable.isSelected() == Boolean.TRUE);
    settings.setCommsFailTimer((long) ftfTimerComms.getValue());
    settings.setTripSignalTimer((long) ftfTimerTripSignal.getValue());
    settings.setCbCurrentThreshold((long)ftfCBCurrentThreshold.getValue());
  }
  
  private void initEventHandling() {
    checkboxCBTripEnable.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        updateComponentsState();
      }
    });

    checkboxCommsFailEnable.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        updateComponentsState();
      }
    });
  }
  
  private void updateComponentsState() {
    boolean enabled = checkboxCBTripEnable.isSelected() == Boolean.TRUE;
    ftfTimerTripSignal.setEnabled(enabled);
    lblTimerTripSignal.setEnabled(enabled);
    lblTimerTripSignalUnit.setEnabled(enabled);
    lblCBCurrentThreshold.setEnabled(enabled);
	  lblCBCurrentThresholdUnit.setEnabled(enabled);
    ftfCBCurrentThreshold.setEnabled(enabled);
    
    enabled = checkboxCommsFailEnable.isSelected() == Boolean.TRUE;
    ftfTimerComms.setEnabled(enabled);
    lblTimerComms.setEnabled(enabled);
    lblTimerCommsUnit.setEnabled(enabled);
  }

//  private JComboBox<Object> createEnableDisableCombo(){
//    JComboBox<Object> comb = new JComboBox<>(new DefaultComboBoxModel<Object>(
//        new Boolean[]{Boolean.TRUE, Boolean.FALSE}));
//    comb.setRenderer(new DefaultListCellRenderer() {
//
//      @Override
//      public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
//          boolean cellHasFocus) {
//        Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
//        setText(value == Boolean.TRUE ? "Enabled" : "Disabled");
//        return comp;
//      }
//    });
//    return comb;
//  }
  
  private void initComponentsModel() {
    ftfTimerComms.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(0L, Long.MAX_VALUE)));
    ftfTimerTripSignal.setFormatterFactory(new DefaultFormatterFactory(
        new BoundaryNumberFormatter(0L, Long.MAX_VALUE)));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    label1 = new JLabel();
    checkboxCBTripEnable = new JCheckBox();
    label2 = new JLabel();
    checkboxCommsFailEnable = new JCheckBox();
    lblCBCurrentThreshold = new JLabel();
    ftfCBCurrentThreshold = new JFormattedTextField();
    lblCBCurrentThresholdUnit = new JLabel();
    separator1 = compFactory.createSeparator("Timer Settings");
    lblTimerTripSignal = new JLabel();
    ftfTimerTripSignal = new JFormattedTextField();
    lblTimerTripSignalUnit = new JLabel();
    lblTimerComms = new JLabel();
    ftfTimerComms = new JFormattedTextField();
    lblTimerCommsUnit = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
      "right:default, $lcgap, [50dlu,default], $lcgap, default, $lcgap, default:grow",
      "2*(default, $lgap), default, $ugap, 2*(default, $lgap), default"));

    //---- label1 ----
    label1.setText("CB Trip Enabled:");
    add(label1, CC.xy(1, 1));
    add(checkboxCBTripEnable, CC.xy(3, 1));

    //---- label2 ----
    label2.setText("Comms Fail Monitor Enabled:");
    add(label2, CC.xy(1, 3));
    add(checkboxCommsFailEnable, CC.xy(3, 3));

    //---- lblCBCurrentThreshold ----
    lblCBCurrentThreshold.setText("CB Current Threshold:");
    add(lblCBCurrentThreshold, CC.xy(1, 5));
    add(ftfCBCurrentThreshold, CC.xy(3, 5));

    //---- lblCBCurrentThresholdUnit ----
    lblCBCurrentThresholdUnit.setText("Amps");
    add(lblCBCurrentThresholdUnit, CC.xy(5, 5));
    add(separator1, CC.xywh(1, 7, 7, 1));

    //---- lblTimerTripSignal ----
    lblTimerTripSignal.setText("Timer for CB Trip Signal:");
    add(lblTimerTripSignal, CC.xy(1, 9));
    add(ftfTimerTripSignal, CC.xy(3, 9));

    //---- lblTimerTripSignalUnit ----
    lblTimerTripSignalUnit.setText("Seconds");
    add(lblTimerTripSignalUnit, CC.xy(5, 9));

    //---- lblTimerComms ----
    lblTimerComms.setText("Timer for Comms Fail:");
    add(lblTimerComms, CC.xy(1, 11));
    add(ftfTimerComms, CC.xy(3, 11));

    //---- lblTimerCommsUnit ----
    lblTimerCommsUnit.setText("Seconds");
    add(lblTimerCommsUnit, CC.xy(5, 11));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JCheckBox checkboxCBTripEnable;
  private JLabel label2;
  private JCheckBox checkboxCommsFailEnable;
  private JLabel lblCBCurrentThreshold;
  private JFormattedTextField ftfCBCurrentThreshold;
  private JLabel lblCBCurrentThresholdUnit;
  private JComponent separator1;
  private JLabel lblTimerTripSignal;
  private JFormattedTextField ftfTimerTripSignal;
  private JLabel lblTimerTripSignalUnit;
  private JLabel lblTimerComms;
  private JFormattedTextField ftfTimerComms;
  private JLabel lblTimerCommsUnit;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  public boolean validateSettings() {
    return true; // Nothing to validate
  }
}
