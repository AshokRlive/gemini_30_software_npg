
package com.lucy.g3.rtu.config.clogic.sectionaliser;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class SectionaliserLogicFactory extends AbstractClogicFactory {
  public final static SectionaliserLogicFactory INSTANCE = new SectionaliserLogicFactory();
  private SectionaliserLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new SectionaliserSettingsEditor((SectionaliserSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new Sectionaliser();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return Sectionaliser.TYPE;
  }
  
  
}

