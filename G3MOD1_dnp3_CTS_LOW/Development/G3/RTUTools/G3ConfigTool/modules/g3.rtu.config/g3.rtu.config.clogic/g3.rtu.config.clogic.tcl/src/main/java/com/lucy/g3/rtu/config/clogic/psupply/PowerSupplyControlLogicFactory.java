
package com.lucy.g3.rtu.config.clogic.psupply;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;


public class PowerSupplyControlLogicFactory extends AbstractClogicFactory{
  public final static PowerSupplyControlLogicFactory INSTANCE = new PowerSupplyControlLogicFactory();
  private PowerSupplyControlLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new PowerSupplyControlSettingsEditor((PowerSupplyControlSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new PowerSupplyControl();
  }

  @Override
  public ICLogicType getLogicType() {
    return PowerSupplyControl.TYPE;
  }
  
}

