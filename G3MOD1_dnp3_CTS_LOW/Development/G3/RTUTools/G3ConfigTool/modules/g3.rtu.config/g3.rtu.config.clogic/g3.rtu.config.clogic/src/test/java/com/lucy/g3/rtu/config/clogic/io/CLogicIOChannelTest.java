/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelStub;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOChannel;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;

public class CLogicIOChannelTest {

  private CLogicIOChannel fixture;
  private IChannel channel;
  @SuppressWarnings("unused")
  private Module module;


  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    fixture = new CLogicIOChannel(null, "testOutput", true, true);
    module = new ModuleStub();
    channel = new ChannelStub();
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    fixture = null;
    channel = null;
    module = null;
  }

  @Test
  public void testSetChannel() {
    final String initialStr = fixture.getValueString();
    assertNull(fixture.getValue());

    fixture.setValue(channel);
    assertEquals(fixture.getValue(), channel);
    System.out.println("old valueStr:" + initialStr);
    System.out.println("valueStr:" + fixture.getValueString());
    assertNotSame("Expect string changed:" + initialStr, initialStr, fixture.getValueString());

    fixture.setValue(null);
    assertEquals(null, fixture.getValue());
    assertEquals("Expect value string recovered", initialStr, fixture.getValueString());
  }

  @Test
  public void testChannelDisabled() {
    final String initialStr = fixture.getValueString();
    fixture.setValue(channel);

    channel.delete();
    assertEquals(initialStr, fixture.getValueString());
    assertEquals("Expect value string recovered", initialStr, fixture.getValueString());

    assertNull(fixture.getValue());
  }

}
