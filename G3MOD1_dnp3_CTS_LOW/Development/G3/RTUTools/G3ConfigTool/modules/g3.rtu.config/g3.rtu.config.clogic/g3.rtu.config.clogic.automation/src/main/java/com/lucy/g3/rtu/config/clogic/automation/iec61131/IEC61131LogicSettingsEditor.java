/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation.iec61131;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.automation.scheme.templates.AutomationEnum.AutoSchemeType;
import com.lucy.g3.common.version.BasicVersion;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;

public class IEC61131LogicSettingsEditor extends AbstractCLogicSettingsEditor {

  private IEC61131LogicSettings settings;

  private final AutoSchemeType  previousScheme;

  public IEC61131LogicSettingsEditor(IEC61131LogicSettings settings) {
    super(settings);
    this.settings = settings;
    previousScheme = settings.getAutoSchemeType(); 
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();

    JComponent comp;

    // Append "Auto Start"
    comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(IEC61131LogicSettings.PROPERTY_AUTO_START), "Auto Start");

    builder.append("", comp);
    builder.nextLine();

    // Append "AutoScheme Type"
    AutoSchemeType[] autoSchemeTypes = AutoSchemeType.values();
    // autoSchemeTypes = ArrayUtils.removeElement(autoSchemeTypes ,
    // AutoSchemeType.CUSTOM);
    comp = BasicComponentFactory.createComboBox(new SelectionInList<>(autoSchemeTypes,
        pm.getBufferedModel(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_TYPE)));
    builder.append("Automation Scheme:", comp);
    ValidationComponentUtils.setMessageKey(comp, IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_TYPE);
    ValidationComponentUtils.setMandatory(comp, true);
    builder.nextLine();

    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();

    // Append "AutoScheme Library" field
    final BufferedValueModel libNameVM = pm.getBufferedModel(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_LIB);
    comp = BasicComponentFactory.createTextField(libNameVM);
    builder.append("Automation Scheme Library:", comp);
    ValidationComponentUtils.setMessageKey(comp, IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_LIB);
    final JTextField libField = (JTextField) comp;
    if (libField.getText().isEmpty())
      ValidationComponentUtils.setErrorBackground((JTextField) comp);
    else
      ValidationComponentUtils.setMandatory(comp, true);

    builder.nextLine();

    // Append "AutoScheme Version" field
    ValueModel ValueModel = pm.getBufferedModel(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_VERSION);
    comp = BasicComponentFactory.createLabel(ValueModel, IEC61131LogicSettings.getVersionFormat());
    builder.append("Automation Scheme Version:", comp);

    // Set lib field editable when custom scheme is selected
    final BufferedValueModel valueModel = pm.getBufferedModel(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_TYPE);
    valueModel.addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        boolean isCustom = valueModel.getValue() == AutoSchemeType.CUSTOM;
        libField.setEditable(isCustom);

        // Update library name
        AutoSchemeType autoSchemeType = (AutoSchemeType) valueModel.getValue();
        libNameVM.setValue(isCustom ? IEC61131Logic.INVALID_LIB : IEC61131LogicSettings.getDefaultAutoSchemeLibName(autoSchemeType));
      }

    });
    boolean isCustom = valueModel.getValue() == AutoSchemeType.CUSTOM;
    libField.setEditable(isCustom);

    return builder.getPanel();
  }

  @Override
  public void validate(ValidationResult result) {
    AutoSchemeType type = (AutoSchemeType) pm.getBufferedValue(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_TYPE);
    String lib = (String) pm.getBufferedValue(IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_LIB);

    if (type == null) {
      result.addError("Automation Scheme type is not selected!",
          IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_LIB);
    } else if (lib == null || Strings.isBlank(lib)) {
      result.addError("Automation Scheme Library must not be empty!",
          IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_LIB);
    } else if(IEC61131Logic.INVALID_LIB.equals(lib)) {
      result.addError("Automation Scheme Library is not specified!",
          IEC61131LogicSettings.PROPERTY_AUTO_SCHEME_LIB);
    }
  }

  @Override
  public void postCommitAction() {
    AutoSchemeType currentScheme = settings.getAutoSchemeType();
    
    if(previousScheme!= currentScheme && currentScheme != null) {
      // Template must be applied if the scheme type changes
      //AutomationLogicTemplate.applyTemplate(currentScheme, settings.getOwnerLogic());
      
      if(previousScheme != null)
        JOptionPane.showMessageDialog(null, "The type of Automation Scheme has been changed! \n"
          + "Please review the configuration of this Automation Logic.");
    }
  }
  
  

}
