
package com.lucy.g3.rtu.config.clogic.fpireset;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.OperationMode;


public class FPIResetLogicFactory extends AbstractClogicFactory {
  public final static FPIResetLogicFactory INSTANCE = new FPIResetLogicFactory();
  private FPIResetLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new FPIResetSettingsEditor((FPIResetSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new FPIReset();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return FPIReset.TYPE;
  }
}

