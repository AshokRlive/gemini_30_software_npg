/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.switchgear;

import java.awt.Component;
import java.util.Collection;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * Wizard page for configuring Switchgear Logic options.
 */
class SwitchGearLogicOptions extends WizardPage {
  public final static String PAGE_ID = "SwitchGearLogicOptions";
  private final Collection<SwitchModuleOutput> outputs;
  
  public SwitchGearLogicOptions(IConfig config) {
    super("Select Switch");
    outputs = findUnusedOutputs(config);
    initComponents();
    putWizardData(PAGE_ID, this);
  }
  
  private static Vector<SwitchModuleOutput>  findUnusedOutputs(IConfig config) {
    Vector<SwitchModuleOutput> unusedSCMs = new Vector<SwitchModuleOutput>();
    ModuleRepository repo = config.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    Collection<SwitchModuleOutput> alloutputs = repo.getAllSwitchModuleOutputs();

    // Find usable switch output
    for (SwitchModuleOutput output : alloutputs) {
      if (output.isUsable()) {
        unusedSCMs.add(output);
      }
    }
    return unusedSCMs;
  }
  

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;

    if (getSelectedSCM() == null) {
      result = "";// "No selected switch output";
    }

    return result;
  }

  public SwitchModuleOutput getSelectedSCM() {
    Object sel = combSCM.getSelectedItem();
    return sel instanceof SwitchModuleOutput ? (SwitchModuleOutput) sel : null;
  }

  public boolean getCheckBoxAutoCreate() {
    return checkBoxAutoCreate.isSelected();
  }

  private void createUIComponents() {
    combSCM = new JComboBox<Object>(outputs.toArray());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    labelSCM = new JLabel();
    checkBoxAutoCreate = new JCheckBox();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, [50dlu,default]:grow",
        "2*(default, $lgap), default:grow, $lgap, default"));

    //---- labelSCM ----
    labelSCM.setText("Switch Output:");
    add(labelSCM, CC.xy(1, 1));

    //---- combSCM ----
    combSCM.setToolTipText("Controlled Switchgear by this ControlLogic");
    add(combSCM, CC.xy(3, 1));

    //---- checkBoxAutoCreate ----
    checkBoxAutoCreate.setText("Auto Create Required Virtual Points");
    checkBoxAutoCreate.setToolTipText("Create required virtual points for this Swicthgear Control Logic");
    checkBoxAutoCreate.setSelected(true);
    add(checkBoxAutoCreate, CC.xywh(1, 7, 3, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel labelSCM;
  private JComboBox<Object> combSCM;
  private JCheckBox checkBoxAutoCreate;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
