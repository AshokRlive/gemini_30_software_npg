
package com.lucy.g3.rtu.config.clogic.voltagemismatch;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.OutputLogicPage;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


public class VoltageMismatchLogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor {
  public final static VoltageMismatchLogicFactory INSTANCE = new VoltageMismatchLogicFactory();
  private VoltageMismatchLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new VoltageMismatchLogicSettingsEditor((VoltageMismatchLogicSettings) settings);
  }

  @Override
  public VoltageMismatchLogic createLogic() {
    return new VoltageMismatchLogic();
  }

  @Override
  public ICLogicType getLogicType() {
    return VoltageMismatchLogic.TYPE;
  }
  
  @Override
  public WizardPage[] getWizardPages(IConfig config) {
//    CLogicManager manager = config.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
//    return new WizardPage[]{
//        new OutputLogicPage(manager.getLogicByType(ICLogicType.SGL), "Select a Switchgear Logic")
//    };
    return null;
  }
  
  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }
  
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    VoltageMismatchLogic newLogic = createLogic();
    
    return newLogic;
  }

}

