
package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.module.iomodule.manager.ChannelRepository;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


class OutputChannelContributor implements IControlLogicAddingWizardContributor {
  private final ChannelType chtype;
  private final String description;
  
  public OutputChannelContributor(ChannelType chtype, String description) {
    super();
    this.chtype = chtype;
    this.description = description;
  }

  
  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    Collection<IChannel> chnls = findUnusedChs(config, chtype);
    return new WizardPage[]{
        new OutputChannelPage(chnls, description)
        };
  }

  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
      ICLogic newLogic = CLogicFactories.getFactory(type.getId()).createLogic();
    
      Method setter = newLogic.getClass().getMethod("setOutputChannel", IChannel.class);
      IChannel ch = ((OutputChannelPage)wizardData.get(OutputChannelPage.PAGE_ID)).getSelectedCh();
      setter.invoke(newLogic, ch);
      
      return newLogic;
  }
  
  
  private static Collection<IChannel> findUnusedChs(IConfig config, ChannelType t) {
    if(config == null)
      return new ArrayList<IChannel>(0);
    
    ChannelRepository repo = config.getConfigModule(ChannelRepository.CONFIG_MODULE_ID);
    Collection<IChannel> allchs = repo.getAllChannels(t);
    return findUnusedChs(allchs);
  }
  
  /**
   * Help method for finding channels that are not controlled by any logic.
   */
  private static Collection<IChannel> findUnusedChs(Collection<IChannel> chs) {
    if(chs == null)
        return new ArrayList<IChannel>(0);
      
    ArrayList<IChannel> unusedChs = new ArrayList<IChannel>();
    for (IChannel ch : chs) {
      if (!ch.isConnectedToAnyTarget()) {
        unusedChs.add(ch);
      }
    }

    return unusedChs;
  }

}

