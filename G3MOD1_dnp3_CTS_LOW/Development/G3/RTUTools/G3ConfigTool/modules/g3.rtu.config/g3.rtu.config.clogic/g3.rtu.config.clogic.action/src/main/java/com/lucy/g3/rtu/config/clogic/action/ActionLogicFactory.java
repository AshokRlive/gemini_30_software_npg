
package com.lucy.g3.rtu.config.clogic.action;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


public class ActionLogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor{
  public final static ActionLogicFactory INSTANCE = new ActionLogicFactory();
  private ActionLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new ActionLogicSettingsEditor((ActionLogicSettings) settings);
  }

  @Override
  public ActionLogic createLogic() {
    return new ActionLogic();
  }

  @Override
  public ICLogicType getLogicType() {return ActionLogic.TYPE;}

  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }
  
  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    return new WizardPage[]{
        new ActionLogicOptions()
    };
  }
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    assert type == getLogicType();
    
    ActionLogic newLogic = createLogic();
    int num = (Integer) wizardData.get(ActionLogicOptions.KEY_ACTION_INPUT_NUM);
    newLogic.init(num);
    return newLogic;
  }

}

