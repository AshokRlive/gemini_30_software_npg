/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.digitalpointcontrol;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.DigitalControlParamT;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DCTL_LOGIC_TRIGGER;


public class DigitalPointControlSettings extends AbstractCLogicSettings<DigitalPointControl> {
  public static final String PROPERTY_PULSEENABLED = "pulseEnabled";
  public static final String PROPERTY_PULSEDURATION = "pulseDuration";
  public static final String PROPERTY_TRIGGER = "trigger";

  private boolean pulseEnabled;
  private int pulseDuration;
  private DCTL_LOGIC_TRIGGER trigger = DCTL_LOGIC_TRIGGER.DCTL_LOGIC_TRIGGER_BOTH;
  
  DigitalPointControlSettings(DigitalPointControl owner) {
    super(owner);
  }

  @Override
  public IValidator getValidator() {
    return null;
  }
  
  public boolean isPulseEnabled() {
    return pulseEnabled;
  }

  public void setPulseEnabled(boolean pulseEnabled) {
    Object oldValue = this.pulseEnabled;
    this.pulseEnabled = pulseEnabled;
    firePropertyChange(PROPERTY_PULSEENABLED, oldValue, pulseEnabled);
  }

  public int getPulseDuration() {
    return pulseDuration;
  }

  public void setPulseDuration(int pulseDuration) {
    Object oldValue = this.pulseDuration;
    this.pulseDuration = pulseDuration;
    firePropertyChange(PROPERTY_PULSEDURATION, oldValue, pulseDuration);
  }

  public DCTL_LOGIC_TRIGGER getTrigger() {
    return trigger;
  }

  public void setTrigger(DCTL_LOGIC_TRIGGER trigger) {
    if (trigger == null) {
      return;
    }

    Object oldValue = this.trigger;
    this.trigger = trigger;
    firePropertyChange(PROPERTY_TRIGGER, oldValue, trigger);
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml) {
    DigitalControlParamT params = xml.digCtrlParam.first();

    setPulseDuration((int) params.pulseDuration.getValue());
    setPulseEnabled(params.pulseEnabled.getValue());

    int triggerValue = params.trigger.getValue();
    DCTL_LOGIC_TRIGGER[] triggerEnums = DCTL_LOGIC_TRIGGER.values();
    if (triggerValue >= 0 && triggerValue <= triggerEnums.length) {
      setTrigger(triggerEnums[triggerValue]);
    }
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    DigitalControlParamT params = xml.digCtrlParam.append();
    params.pulseDuration.setValue(pulseDuration);
    params.pulseEnabled.setValue(pulseEnabled);
    params.trigger.setValue(trigger.getValue());
  }


}

