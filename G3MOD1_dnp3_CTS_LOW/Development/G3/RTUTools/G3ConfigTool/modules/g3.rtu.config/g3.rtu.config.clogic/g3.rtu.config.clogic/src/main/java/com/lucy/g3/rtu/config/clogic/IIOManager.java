/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic;

import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IUserChangeable;

/**
 * The Interface IIOManager.
 *
 * @param <T>
 *          the generic type of IO value.
 */
public interface IIOManager<T> extends IUserChangeable {
  
  int DEFAULT_MAX_SIZE = 50;
  
  int getMaximumSize();
  
  void removeAll();
  
  void removeAll(Collection<CLogicIO<?>> removeItems);
  
  void remove(CLogicIO<?> removeItem);
  
  int getSize();
  
  CLogicIO<T>[] getAll();
  
  CLogicIO<?> getByIndex(int index);

  ListModel<CLogicIO<T>> getListModel();
  
}
