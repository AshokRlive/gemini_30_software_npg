/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.pltu;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOLogic;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PLTU_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PLTU_OUTPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PLTU_POINT;

/**
 * Implementation of PLTU Logic.
 */
public class PLTULogic extends PredefinedCLogic<PLTULogicSettings> implements ISupportOffLocalRemote {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.PLTU, "PLTU Logic")
      .desp("Parasitic Load Trip Unit.")
      .maxAmount(10)
      .requireOLR(true)
      .get();
  
  private final CLogicIOLogic[] outputs;


  public PLTULogic() {
    this(null);
  }
  
  public PLTULogic(IOperableLogic switchLogic) {
    super(TYPE, PLTU_INPUT.values(), PLTU_POINT.values());

    PLTU_OUTPUT[] outputEnums = PLTU_OUTPUT.values();
    outputs = new CLogicIOLogic[outputEnums.length];
    initOutputs(outputEnums);
    
    if(switchLogic != null)
      setOutputSwitch(switchLogic);
  }


  public ICLogic getOutputSwitch() {
    int index = PLTU_OUTPUT.PLTU_OUTPUT_SW.ordinal();
    return outputs[index].getValue();
  }

  private void initOutputs(PLTU_OUTPUT[] outputEnums) {
    boolean isOptional = false;
    int[] types;
    for (int i = 0; i < outputs.length; i++) {
      if (outputEnums[i] == PLTU_OUTPUT.PLTU_OUTPUT_SW) {
        types = new int[] { ICLogicType.SGL };
      } else {
        types = null;
      }

      outputs[i] = new CLogicIOLogic(this,
          outputEnums[i].getDescription(),
          types, 
          isOptional, true, false, false);
    }

  }

  public void setOutputSwitch(IOperableLogic switchLogic) {
    int index = PLTU_OUTPUT.PLTU_OUTPUT_SW.ordinal();
    outputs[index].setValue(switchLogic);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInput(PLTU_INPUT.PLTU_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  public CLogicIO<VirtualPoint> getInput(PLTU_INPUT inputItem) {
    return getInputByEnum(inputItem);
  }

  @Override
  protected PLTULogicSettings createSettings() {
    return new PLTULogicSettings(this);
  }

}
