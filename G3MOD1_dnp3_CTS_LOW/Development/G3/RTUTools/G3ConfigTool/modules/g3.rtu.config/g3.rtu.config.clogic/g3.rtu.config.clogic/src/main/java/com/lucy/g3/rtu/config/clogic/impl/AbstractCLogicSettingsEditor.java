/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.NumberFormatter;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.BoundaryNumberFormatter;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;


/**
 * The Class AbstractCLogicSettingsEditor.
 */
public abstract class AbstractCLogicSettingsEditor implements ICLogicSettingsEditor{
  protected final PresentationModel<ICLogicSettings> pm;
  private final ArrayList<PresentationModel<?>> otherPMs = new ArrayList<>();
  
  private final DefaultFormBuilder builder =
      new DefaultFormBuilder(new FormLayout(
          "right:[80dlu,default], $lcgap, [50dlu,default]:grow, $lcgap,default, $lcgap,default",
          "" // dynamic rows
      )
      );

  public AbstractCLogicSettingsEditor(ICLogicSettings settings) {
    this.pm = new PresentationModel<>(settings);
  }
  
  protected ICLogicSettings getSettings() {
    return pm.getBean();
  }
  
  @Override
  public void triggerCommit() {
    pm.triggerCommit();
    
    for(PresentationModel<?> p : otherPMs) {
      p.triggerCommit();
    }
  }
  
  /**
   * Adds a presentation model to be involved for commit/flush/release.
   */
  protected void addPM(PresentationModel<?> otherPM) { 
    otherPMs.add(otherPM);
  }

  @Override
  public void triggerFlush() {
    pm.triggerFlush();
    
    for(PresentationModel<?> p : otherPMs) {
      p.triggerFlush();
    }
  }

  @Override
  public void release() {
    pm.release();
    for(PresentationModel<?> p : otherPMs) {
      p.release();
    }
  }
  

  /**
   * Override this method if need to do something after commit.
   */
  @Override
  public void postCommitAction() {
    // Nothing to do
  }
  
  
  protected JFormattedTextField createIntField(String label, String property, String unit) {
    JFormattedTextField comp = BasicComponentFactory.createIntegerField(pm.getBufferedModel(property));
    builder.append(label+":", comp, new JLabel(unit == null ? "" : unit));
    builder.nextLine();
    setMessageKey(comp, property);
    return comp;
  }
  
  protected JFormattedTextField createLongField(String label, String property, String unit) {
	  JFormattedTextField comp = BasicComponentFactory.createLongField(pm.getBufferedModel(property));
    builder.append(label+":", comp, new JLabel(unit == null ? "" : unit));
    setMessageKey(comp, property);
    builder.nextLine();
    return comp;
  }

  protected static void setMessageKey(JComponent comp, Object messageKey) {
    com.jgoodies.validation.view.ValidationComponentUtils.setMessageKey(comp, messageKey);
  }
  
  protected final ICLogic getOwner() {
    return pm.getBean().getOwnerLogic();
  }
  
  protected static void bindingEnableState(final JToggleButton controller, final JComponent... targets) {
    boolean enabled = controller.isSelected();
    for (int i = 0; i < targets.length; i++) {
      targets[i].setEnabled(enabled);
    }

    controller.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        boolean enabled = controller.isSelected();
        for (int i = 0; i < targets.length; i++) {
          targets[i].setEnabled(enabled);
        }
      }
    });
  }
  
  protected final DefaultFormBuilder getDefaultBuilder() {
    return builder;
  }
  
  protected static NumberFormatter createRoundingFormatter(Comparable<?> min, Comparable<?> max) {
    return new RoundingFormatter(min, max);
  }
  
  protected static void checkNotNegative(ValidationResult result, PresentationModel<?> pm, String property, String propertyDisplayName) {
    Number val = (Number) pm.getBufferedValue(property);
    if(val.longValue() < 0) {
      result.addError(String.format("The value of \"%s\" must not be negative! Current:%s",
          Strings.isBlank(propertyDisplayName)? property: propertyDisplayName, val), 
          property);
    }
  }
  
  protected static void checkRange(ValidationResult result, PresentationModel<?> pm, long min, long max, 
      String property, String propertyDisplayName) {
    long val = (long) pm.getBufferedValue(property);
    if(val > max|| val < min) {
      result.addError(String.format("The value of \"%s\" is out of range:[%s, %s]. Current: %s",
          Strings.isBlank(propertyDisplayName)? property: propertyDisplayName,
              min, max, val), 
          property);
    }
  }


  private static class RoundingFormatter extends BoundaryNumberFormatter{
    
    public RoundingFormatter(Comparable<?> min, Comparable<?> max) {
     super(min,max);
    }
    
    @Override
    public Object stringToValue(final String text) throws ParseException {
      final Object parsed = super.stringToValue(text);

      long value = ((Number) parsed).longValue();
      return roundToClosestFive(value);
    }

    private long roundToClosestFive(long num) {
      return Math.round(num / 100.0) * 100;
    }
  }
}

