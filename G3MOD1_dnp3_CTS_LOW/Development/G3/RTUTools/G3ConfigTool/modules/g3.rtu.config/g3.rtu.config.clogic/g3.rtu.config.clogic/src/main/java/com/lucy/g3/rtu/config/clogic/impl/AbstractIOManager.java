/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.lang.reflect.Array;
import java.util.Collection;

import javax.swing.ListModel;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.IIOManager;

/**
 * Manager of CLogic IO.
 *
 * @param <T>
 *          the generic type of IO
 */
public class AbstractIOManager<T> extends Model implements IIOManager<T> {

  private final ArrayListModel<CLogicIO<T>> ioList = new ArrayListModel<>();
  
  private boolean allowUserChange = true;

  private int maximumSize = DEFAULT_MAX_SIZE;
  
  
  public AbstractIOManager() {
    super();
  }

  protected void addIO(CLogicIO<T> newIO) {
    ioList.add(newIO);
  }
  
  protected void setIO(CLogicIO<T> newIO, int index) {
    ioList.set(index, newIO);
  }

  public void addIO(Collection<CLogicIO<T>> newIO) {
    ioList.addAll(newIO);
  }

  @Override
  public int getSize() {
    return ioList.size();
  }

  @Override
  public CLogicIO<T>[] getAll() {
    CLogicIO<T>[] buf = (CLogicIO<T>[]) Array.newInstance(CLogicIO.class, ioList.size());
    return ioList.toArray(buf);
  }

  @Override
  @SuppressWarnings("unchecked")
  public ListModel<CLogicIO<T>> getListModel() {
    return ioList;
  }

  @Override
  public void removeAll(Collection<CLogicIO<?>> removeItems) {
    ioList.removeAll(removeItems);    
    for (CLogicIO<?> item : removeItems) {
      item.setValue(null);
    }
  }

  @Override
  public void remove(CLogicIO<?> removeItem) {
    ioList.remove(removeItem);    
    removeItem.setValue(null);
  }

  @Override
  public void removeAll() {
    ioList.clear();    
    for (CLogicIO<?> io: ioList) {
      io.setValue(null);
    }
  }

  protected CLogicIO<T> get(int i) {
    return i < ioList.size() ? ioList.get(i) : null;
  }

  @Override
  public boolean isAllowUserChange() {
    return allowUserChange;
  }
  
  @Override
  public void setAllowUserChange(boolean allowUserChange) {
    Object oldValue = this.allowUserChange;
    this.allowUserChange = allowUserChange;
    firePropertyChange(PROPERTY_ALLOW_USER_CHANGE, oldValue, allowUserChange);
  }

  @Override
  public CLogicIO<?> getByIndex(int index) {
    if(index >=0 && index < ioList.getSize())
      return ioList.get(index);
    return null;
  }

  
  @Override
  public int getMaximumSize() {
    return maximumSize;
  }

  
  public void setMaximumSize(int maximumSize) {
    this.maximumSize = maximumSize;
  }
  
}
