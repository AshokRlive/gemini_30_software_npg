
package com.lucy.g3.rtu.config.clogic.threshold;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;


public class ThresholdLogicLogicFactory extends AbstractClogicFactory 
      implements IControlLogicAddingWizardContributor
{
  public final static ThresholdLogicLogicFactory INSTANCE = new ThresholdLogicLogicFactory();
  private ThresholdLogicLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new ThresholdLogicSettingsEditor((ThresholdLogicSettings) settings);
  }

  @Override
  public ThresholdLogic createLogic() {
    return new ThresholdLogic();
  }

  
  @Override
  public ICLogicType getLogicType() {
    return ThresholdLogic.TYPE;
  }
  
  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }
  

  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    return new WizardPage[]{
        new ThresholdLogicOptions(config)
    };
  }
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    ThresholdLogic newLogic = createLogic();
    VirtualPoint input = (VirtualPoint) wizardData.get(ThresholdLogicOptions.KEY_ACTION_INPUT_POINT);
    newLogic.setInput(input);
    return newLogic;
  }
}

