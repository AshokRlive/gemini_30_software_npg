
package com.lucy.g3.rtu.config.clogic;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CLogicPluginTest {
  
  @Before
  public void setup() {
    CLogicPlugin.init();
  }
  
  @After
  public void tearDown() {
    CLogicPlugin.destroy();
  }
  
  
  @Test
  public void testCLogicFactories() {
    
    
    int[] typeIds = CLogicFactories.getAllLogicTypeIds();
    
    // Checksize
//    if(typeIds.length != ICLogicType.TOTAL_NUMBER) {
//      Logger.getLogger(getClass()).warn(
//          "The number of factory is incorrect! Expect registered factory: "
//          + ICLogicType.TOTAL_NUMBER+" but actual:"+typeIds.length);
//    }
    
    IClogicFactory factory;
    for (int i = 0; i < typeIds.length; i++) {
      factory = CLogicFactories.getFactory(typeIds[i]);
      assertNotNull(factory);
      assertNotNull(factory.createLogic());
      assertNotNull(factory.getLogicType());
      
      assertTrue(factory.createLogic().getType() == CLogicFactories.getLogicType(typeIds[i]));
      assertEquals(typeIds[i], factory.createLogic().getType().getId());
    }
  }

}

