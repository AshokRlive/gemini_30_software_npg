/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicIO.IODirection;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IIOManager;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.clogic.ui.Canvas.IOCompGroup;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.IVirtualPointDialogResource;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.VirtualPointDialog;

/**
 * Panel for displaying Control Logic in graph.
 */
public class CLogicGraphPanel extends JPanel {

  private Logger log = Logger.getLogger(CLogicGraphPanel.class);

  private static final String ICON_KEY_POINT = "pseudo.icon";
  private static final String ICON_KEY_POINT_HIGHLIGHT = "pseudo.icon.highlight";
  private static final String ICON_KEY_SETTING = "setting.icon";
  private static final String ICON_KEY_SETTING_ROLLOVER = "settingRollover.icon";

  private static final HashMap<String, ImageIcon> ICONS = new HashMap<String, ImageIcon>();

  // Font resources
  private static final Font TITLE_FONT = new JLabel().getFont().deriveFont(Font.BOLD, 17);

  private static final Color COLOR_INVALID = Color.red;
  private static final Color COLOR_VALID = Color.black;

  // Data Model
  private final ICLogic clogic;

  // Drawn components
  private JLabel[] lblInputValues;
  private JLabel[] lblOutputValues;
  
  private JLabel[] lblInputPins;
  private JLabel[] lblOutputPins;
  
  private JButton[] btnInputs;
  private JButton[] btnOutputs;
  
  private JButton[] btnPseudoPoints;
  
  private JLabel lblBlockTitle;
  private JLabel lblLeftTitle;
  private JLabel lblRightTitle;
  private JButton btnSetting;

  private Canvas canvas;

  private final JFrame parent = WindowUtils.getMainFrame();


  private final Action[] popupActions;
  private final Action settingsAction;
  /**
   * Constructor a graphic logic page based on give logic.
   *
   * @param cl
   *          the control logic object
   */
  public CLogicGraphPanel(ICLogic clogic, Action... popupActions) {
    super(new BorderLayout());
    this.popupActions = popupActions;
    this.clogic = Preconditions.checkNotNull(clogic, "ControlLogic must not be null");
    settingsAction = new BusyCursorAction(new CLogicSettingAction(clogic), this);
  }
  
  public void init() {
    int inputNum = clogic.getInputNum();
    int outputNum = clogic.getOutputNum();

    // Set canvas
    canvas = new Canvas();
    this.add(BorderLayout.CENTER, canvas);

    // "Input" title on the left
    if (inputNum > 0 || clogic instanceof IInputsManager) {
      lblLeftTitle = new JLabel("Inputs");
      lblLeftTitle.setFont(TITLE_FONT);
      lblLeftTitle.setForeground(Color.gray);
      lblLeftTitle.setHorizontalAlignment(SwingConstants.CENTER);
    }
    // "Output" title on the right
    if (outputNum > 0) {
      lblRightTitle = new JLabel("Controls");
      lblRightTitle.setHorizontalAlignment(SwingConstants.CENTER);
      lblRightTitle.setFont(TITLE_FONT);
      lblRightTitle.setForeground(Color.gray);
    }
    // Title label
    lblBlockTitle = new JLabel();
    lblBlockTitle.setFont(UIManager.getFont("Label.font").deriveFont(Font.BOLD, 14.0f));
    lblBlockTitle.setHorizontalAlignment(SwingConstants.CENTER);
    PropertyAdapter<ICLogic> vm = new PropertyAdapter<ICLogic>(clogic,
        ICLogic.PROPERTY_NAME, true);
    Bindings.bind(lblBlockTitle, vm);
    canvas.setTitleComps(lblLeftTitle, lblBlockTitle, lblRightTitle);

    canvas.setInfoPanel(new CLogicInfoPanel(clogic));

    // Create internal "setting" button
    btnSetting = createInternalButton(settingsAction);
    btnSetting.setIcon(getIcon(ICON_KEY_SETTING));
    btnSetting.setRolloverIcon(getIcon(ICON_KEY_SETTING_ROLLOVER));
    btnSetting.setToolTipText("Setting parameters");
    btnSetting.setText(""); // Hide "settings" button text

    initInputComps();
    initOutputComps();
    initInternalComps();
    canvas.updatePreferredSize();

    canvas.setComponentPopupMenu(createPopupMenu());

    initEventHandling();
  }

  private void initEventHandling() {
    // Observe the change of control logic settings.
    clogic.addPropertyChangeListener(new CLogicSettingsPCL());
    
    // Observe input change
    IInputsManager inputMgr = clogic.getInputsManager();
    if (inputMgr != null) {
      inputMgr.getListModel().addListDataListener(new ListDataListener() {
        @Override
        public void intervalRemoved(ListDataEvent e) {
          rebuildInputs();
        }
        
        @Override
        public void intervalAdded(ListDataEvent e) {
          rebuildInputs();
        }
        
        @Override
        public void contentsChanged(ListDataEvent e) {
        }
      });
    }
    
    // Observe output change
    IOutputsManager<?> outputMgr = clogic.getOutputsManager();
    if (outputMgr != null) {
      outputMgr.getListModel().addListDataListener(new ListDataListener() {
        @Override
        public void intervalRemoved(ListDataEvent e) {
          rebuildOutputs();
        }
        
        @Override
        public void intervalAdded(ListDataEvent e) {
          rebuildOutputs();
        }
        
        @Override
        public void contentsChanged(ListDataEvent e) {
        }
      });
    }
    
    // Observe points change
    ICLogicPointsManager pointsMgr = clogic.getPointsManager();
    if (pointsMgr != null) {
      pointsMgr.getListModel().addListDataListener(new ListDataListener() {
        @Override
        public void intervalRemoved(ListDataEvent e) {
          rebuildPoints();
        }
        
        @Override
        public void intervalAdded(ListDataEvent e) {
          rebuildPoints();
        }
        
        @Override
        public void contentsChanged(ListDataEvent e) {
        }
      });
    }
  }

  private JPopupMenu createPopupMenu() {
    // Build pop-up menu
    JPopupMenu popupMenu = new JPopupMenu();
    
    // Add refresh button
    JMenuItem btnRefresh = new JMenuItem("Refresh");
    btnRefresh.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        canvas.repaint();
      }
    });
    popupMenu.add(btnRefresh);
    popupMenu.add(settingsAction);
    
    for (int i = 0; i < popupActions.length; i++) {
      popupMenu.add(popupActions[i]);
    }
    
    return popupMenu;
  }

  private void rebuildInputs() {
    initInputComps();

    // update canvas size since input number changed
    updateCanvas();
  }
  
  private void rebuildOutputs() {
    initOutputComps();
    
    // update canvas size since input number changed
    updateCanvas();
  }
  
  private void rebuildPoints() {
    initInternalComps();
    
    // update canvas size since input number changed
    updateCanvas();
  }

  /*
   * Initialise inputs.
   */
  private IOCompGroup[] initInputComps() {
    int inputNum = clogic.getInputNum();
    IOCompGroup[] inputsComps = new IOCompGroup[inputNum];

    lblInputValues = new JLabel[inputNum];
    btnInputs = new JButton[inputNum];
    lblInputPins = new JLabel[inputNum];

    int index = 0;
    CLogicIO<?>[] inputs = clogic.getInputs();
    if (inputs != null) {
      for (CLogicIO<?> input : inputs) {
        // Create input value label
        lblInputValues[index] = createIOLabel(input);
        lblInputValues[index].setHorizontalAlignment(SwingConstants.RIGHT);

        // Create input button
        btnInputs[index] = createIOButton(index, input);

        // PropertyConnector.connect(input, CLogicIO<?>.PROPERTY_ENABLE,
        // btnInputs[i], "enabled").updateProperty2();
        btnInputs[index].setEnabled(input.isConfigurable());

        // Create input pin label
        lblInputPins[index] = BasicComponentFactory.createLabel(
            new PropertyAdapter<CLogicIO<?>>(input, CLogicIO.PROPERTY_LABEL, true));
        lblInputPins[index].setHorizontalAlignment(SwingConstants.LEFT);
        // Set color
        Color fg = (Color) input.getProperty(VirtualPoint.CLIENT_PROPERTY_FG_COLOR);
        if(fg != null) {
          lblInputPins[index].setForeground(fg);
        }

        inputsComps[index] = new IOCompGroup();
        inputsComps[index].button = btnInputs[index];
        inputsComps[index].nameLabel = lblInputPins[index];
        inputsComps[index].valueLabel = lblInputValues[index];
        index++;
      }

      canvas.setInputComps(inputsComps);
    }

    return inputsComps;
  }

  private void initInternalComps() {
    IPseudoPoint[] pseudoPoints = clogic.getAllPoints();
    btnPseudoPoints = new JButton[pseudoPoints.length];
    for (int i = 0; i < btnPseudoPoints.length; i++) {
      Action action = new BusyCursorAction(new PseudoPointAction(pseudoPoints[i]), parent);
      btnPseudoPoints[i] = createInternalButton(action);
      PropertyConnector.connectAndUpdate(
          new PropertyAdapter<IPseudoPoint>(pseudoPoints[i], IPseudoPoint.PROPERTY_DESCRIPTION,true),
          btnPseudoPoints[i],"text");
      
      // Set color
      Color fg = (Color) pseudoPoints[i].getProperty(VirtualPoint.CLIENT_PROPERTY_FG_COLOR);
      if(fg != null) {
        btnPseudoPoints[i].setForeground(fg);
      }
    }

    // Insert "setting" button to internal buttons
    JComponent[] interns = new JComponent[btnPseudoPoints.length + 1];
    System.arraycopy(btnPseudoPoints, 0, interns, 1, btnPseudoPoints.length);
    interns[0] = btnSetting;
    canvas.setInternComps(interns);
  }

  private void initOutputComps() {
    int outputNum = clogic.getOutputNum();
    lblOutputValues = new JLabel[outputNum];
    btnOutputs = new JButton[outputNum];
    lblOutputPins = new JLabel[outputNum];

    IOCompGroup[] outputs = new IOCompGroup[outputNum];

    int index = 0;

    if (clogic.getOutputs() != null) {
      for (CLogicIO<?> output : clogic.getOutputs()) {
        lblOutputValues[index] = createIOLabel(output);
        btnOutputs[index] = createIOButton(index, output);
        lblOutputPins[index] = 
            BasicComponentFactory.createLabel(
                new PropertyAdapter<CLogicIO<?>>(output, CLogicIO.PROPERTY_LABEL, true));
        lblOutputPins[index].setHorizontalAlignment(SwingConstants.RIGHT);

        outputs[index] = new IOCompGroup();
        outputs[index].button = btnOutputs[index];
        outputs[index].nameLabel = lblOutputPins[index];
        outputs[index].valueLabel = lblOutputValues[index];

        index++;
      }

      canvas.setOutputComps(outputs);
    }
  }

  private JButton createInternalButton(Action action) {
    JButton button = new JButton(action);
    button.setIconTextGap(0);
    button.setContentAreaFilled(false);
    button.setBackground(Canvas.BLOCK_BG);
    button.setBorder(null);
    button.setHorizontalAlignment(SwingConstants.RIGHT);
    button.setHorizontalTextPosition(SwingConstants.LEFT);
    button.setOpaque(false);
    button.setIcon(getIcon(ICON_KEY_POINT));
    button.setRolloverIcon(getIcon(ICON_KEY_POINT_HIGHLIGHT));
    button.setFocusable(false);
    // Mouse roll over font effect
    button.addMouseListener(MOUSE_ROLLOVER_HANDER);
    button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    return button;
  }

  private JLabel createIOLabel(CLogicIO<?> clogicIO) {
    JLabel label = new JLabel();

    label.setText(clogicIO.getValueString());
    label.setToolTipText(clogicIO.getValueString());
    label.setForeground(clogicIO.isValid() ? COLOR_VALID : COLOR_INVALID);

    clogicIO.addPropertyChangeListener(new ClogicIoPCL(label));

    return label;
  }

  private static ImageIcon getIcon(String key) {
    ImageIcon icon = ICONS.get(key);
    if (icon == null) {
      ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(CLogicPage.class);
      icon = resourceMap.getImageIcon(key);
      ICONS.put(key, icon);
    }

    return icon;
  }

  private JButton createIOButton(int index, CLogicIO<?> clogicIO) {
    JButton button = new JButton(new IOAction(index, clogicIO));
    button.setSize(30, 20);
    button.setFocusable(false);
    button.setIcon(getIcon(ICON_KEY_POINT));
    button.setRolloverIcon(getIcon(ICON_KEY_POINT_HIGHLIGHT));
    // pop-up menu
    JPopupMenu popup = new JPopupMenu();
    popup.add(new ClearAction(clogicIO));
    
    IInputsManager inputMgr = clogic.getInputsManager();
    IOutputsManager<?> outputMgr = clogic.getOutputsManager();
    if (inputMgr != null && clogicIO.getDirection() == IODirection.INPUT)
      popup.add(new RemoveIOAction(inputMgr, clogicIO));
    else if (outputMgr != null && clogicIO.getDirection() == IODirection.OUTPUT)
      popup.add(new RemoveIOAction(inputMgr, clogicIO));
    button.setComponentPopupMenu(popup);
    button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    return button;
  }

  private void updateCanvas() {
    canvas.updatePreferredSize();
    canvas.repaint();
  }

  @Override
  public String toString() {
    return clogic.toString();
  }

  Action[] getContextActions() {
    return new Action[] {
        settingsAction,
    };
  }


  /**
   * Remove an input of control logic by index.
   */
  private static class RemoveIOAction extends AbstractAction {

    private IIOManager<?> manager;
    private CLogicIO<?> io;


    public RemoveIOAction(IIOManager<?> manager, CLogicIO<?> io) {
      super("Remove");
      this.io = io;
      this.manager = manager;
      setEnabled(io.isAllowDelete());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      manager.remove(io);
    }

  }

  /**
   * Clear control logic input by index.
   */
  private class ClearAction extends AbstractAction {

    private CLogicIO<?> clogicIO;


    public ClearAction(CLogicIO<?> clogicIO) {
      super("Clear");
      this.clogicIO = clogicIO;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (clogicIO.isConfigurable()) {
        clogicIO.setValue(null);
      } else {
        log.warn("Control Logic IO is not configurable:" + clogicIO.getLabel());
      }
    }
  }

  /**
   * The pseudo point inside control logic block. It observe the change of
   * pseudo point's property and reflect the changes in ToolTips box.
   */
  private class PseudoPointAction extends AbstractAction implements IVirtualPointDialogResource {

    private final IPseudoPoint point;


    public PseudoPointAction(final IPseudoPoint p) {
      super();
      point = p;
      putValue(Action.NAME, point.getDescription());
      // Set dynamic ToolTips
      updateToolTip();
    }

    private void updateToolTip() {
      putValue(Action.SHORT_DESCRIPTION, point.getHTMLSummary());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      /* Open a dialog to set pseudo point's parameters */
      boolean affirmed = VirtualPointDialog.edit(parent, this, point);

      if (affirmed) {
        updateToolTip();
      }
    }

    @Override
    public Collection<IChannel> getAllChannels(ChannelType... type) {
      return new ArrayList<IChannel>(0);
    }

    @Override
    public Collection<VirtualPoint> getAllVirtualPoints(VirtualPointType... type) {
      return new ArrayList<VirtualPoint>(0);
    }

    @Override
    public ValueLabelSetManager getLabelManager() {
      IConfig config = ConfigManager.getInstance().getLocalConfig();
      VirtualPointManager manager = config == null ? null
              :(VirtualPointManager) config.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
      
      return manager == null ? null : manager.getValueLabelManager();
    }
  }

  private class IOAction extends AbstractAction {

    private CLogicIO<?> clogicIO;


    public IOAction(int index, CLogicIO<?> clogicIO) {
      super();
      this.clogicIO = clogicIO;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (clogicIO.isConfigurable()) {
        clogicIO.chooseValue(parent);

        /*
         * For threshold logic, it is required to show setting dialog once user
         * changes the input value. This is for forcing user to review logic
         * settings as hysteresis replies on the full range value of input.
         */
        if (clogicIO.getOwner().getType().requireShowingSettingWhenChangeIO()) {
          AbstractDialog dialog = new CLogicSettingsDialog(parent, clogic);
          dialog.setVisible(true);
        }
      } else {
        log.warn("Control Logic IO is not configurable:" + clogicIO.getLabel());
      }
    }

  }

  private class CLogicSettingsPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {

      // Rebuild internal points since they may have changed.
      // if(evt.getPropertyName().equals(MMAvgLogic.PROPERTY_COUNTER_ENABLED)){
      // rebuildInternals();
      // }

      // Rebuild inputs since they may have changed.
      if (evt.getPropertyName().equals(ICLogicSettings.PROPERTY_INPUT_TYPE)) {
        rebuildInputs();

      // Rebuild inputs since they may have changed.
      } else if (evt.getPropertyName().equals(ICLogicSettings.PROPERTY_INPUT_TYPE)) {
        rebuildInputs();
      }
    }
  }
  
  private static class ClogicIoPCL implements PropertyChangeListener {

    private JLabel ioLabel;


    public ClogicIoPCL(JLabel ioLabel) {
      this.ioLabel = ioLabel;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      CLogicIO<?> io = (CLogicIO<?>) evt.getSource();
      if (evt.getPropertyName().equals(CLogicIO.PROPERTY_VALUE_STRING)) {
        ioLabel.setText(io.getValueString());
        ioLabel.setToolTipText(io.getValueString());

      } else if (evt.getPropertyName().equals(CLogicIO.PROPERTY_VALID)) {
        ioLabel.setForeground(io.isValid() ? COLOR_VALID : COLOR_INVALID);
      }
    }
  }


  private static final MouseAdapter MOUSE_ROLLOVER_HANDER = new MouseAdapter() {

    @Override
    public void mouseEntered(MouseEvent e) {
      ((JButton) e.getSource()).setFont(BUTTON_FONT_BOLD);
    }

    @Override
    public void mouseExited(MouseEvent e) {
      ((JButton) e.getSource()).setFont(BUTTON_FONT_REGULAR);
    }
  };

  private static final Font BUTTON_FONT_REGULAR = UIManager.getFont("Button.font");
  private static final Font BUTTON_FONT_BOLD = BUTTON_FONT_REGULAR.deriveFont(Font.BOLD, BUTTON_FONT_REGULAR.getSize());


  public void repaintCanvas() {
    if (canvas != null)
      canvas.repaint();
  }

}
