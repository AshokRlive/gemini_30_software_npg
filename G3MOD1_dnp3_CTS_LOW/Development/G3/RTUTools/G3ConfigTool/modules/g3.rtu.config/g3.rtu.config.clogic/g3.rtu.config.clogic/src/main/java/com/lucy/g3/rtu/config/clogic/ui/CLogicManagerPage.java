/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListModel;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.EnabledHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.jgoodies.forms.factories.Borders;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.NavigationEvent;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * Page of Control Logic Manager.
 */
public class CLogicManagerPage extends AbstractConfigPage {

  private final CLogicManagerModel pm;

  private AbstractAction navAction;

  private JPopupMenu popMenu;

  private JScrollPane scrollPane1;

  private JXTable clogicList;

  private CLogicManager manager;


  public CLogicManagerPage(CLogicManager manager) {
    super(manager);
    pm = new CLogicManagerModel(this, manager);
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
    
    setNodeName("Control Logic");
  }

  @Override
  protected void init() {
    initComponents();
  }

  private JPopupMenu getPopupMenu() {
    if (popMenu == null) {
      popMenu = new JPopupMenu();
      popMenu.add(pm.getAction(CLogicManagerModel.ACTION_KEY_ADD));
      popMenu.add(pm.getAction(CLogicManagerModel.ACTION_KEY_REMOVE));
      popMenu.add(pm.getAction(CLogicManagerModel.ACTION_KEY_SETTING));
      popMenu.addSeparator();
      popMenu.add(pm.getAction(CLogicManagerModel.ACTION_KEY_ENABLE));
      popMenu.add(pm.getAction(CLogicManagerModel.ACTION_KEY_DISABLE));
      popMenu.addSeparator();
      popMenu.add(getNavAction());
    }

    return popMenu;
  }

  private void initComponents() {
    clogicList = new JXTable(pm.getTableModel());
    UIUtils.setDefaultConfig(clogicList,  true,  true,  true);
    
    Bindings.bind(clogicList, pm.getListModel(), pm.getSelectionModel());

    clogicList.addMouseListener(new CLogicListMouseHandler());
    clogicList.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Edit");
    clogicList.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
    clogicList.getActionMap().put("Edit", pm.getAction(CLogicManagerModel.ACTION_KEY_SETTING));
    clogicList.getActionMap().put("Delete", pm.getAction(CLogicManagerModel.ACTION_KEY_REMOVE));
    clogicList.setGridColor(Color.gray);
    clogicList.setFillsViewportHeight(true);
    clogicList.addHighlighter(new EnabledHighlighter(new HighlightPredicate() {
      @Override
      public boolean isHighlighted(Component renderer, ComponentAdapter adapter) {
        return adapter.getValue(CLogicManagerModel.COLUMN_ENABLED) == Boolean.FALSE;
      }
    }, false));
    

    scrollPane1 = new JScrollPane(clogicList);

    setLayout(new BorderLayout(5, 5));
    setBorder(Borders.DIALOG_BORDER);

    add(scrollPane1, BorderLayout.CENTER);
    add(createControlPanel(), BorderLayout.EAST);
  }

  private JPanel createControlPanel() {
    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    panelBuilder.setDefaultDialogBorder();
    panelBuilder.addGridded(new JButton(pm.getAction(CLogicManagerModel.ACTION_KEY_ADD)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(CLogicManagerModel.ACTION_KEY_REMOVE)));

    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(CLogicManagerModel.ACTION_KEY_SETTING)));

    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(getNavAction()));
    
    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(CLogicManagerModel.ACTION_KEY_MOVE_UP)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(CLogicManagerModel.ACTION_KEY_MOVE_DOWN)));
    

    JPanel panel = panelBuilder.getPanel();
    panel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        pm.clearSelection();
      }
    });

    return panel;
  }

  @SuppressWarnings("static-access")
  private Action getNavAction() {
    if (navAction == null) {
      navAction = new NavigationAction();
      // Sync enable property of navAction.
      PropertyConnector.connect(pm, pm.PROPERTY_EDITITABLE, navAction, "enabled").updateProperty2();
    }
    return navAction;
  }
  
  void navigateTo(ICLogic clogic) {
    if (clogic != null) {
      fireNavigationEvent(new NavigationEvent(clogic));
    }
  }

  private class NavigationAction extends AbstractAction {

    public NavigationAction() {
      super("View Control Logic");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      navigateTo(pm.getSingleSelection());
    }

  }

  private class CLogicListMouseHandler extends MouseAdapter {

    // Double click to go to configuration page of the selection
    @Override
    public void mouseClicked(MouseEvent e) {
      if (UIUtils.isDoubleClick(e)) {
        getNavAction().actionPerformed(null);
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
      mayShowPopupMenu(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      mayShowPopupMenu(e);
    }

    private void mayShowPopupMenu(MouseEvent e) {
      if (e.isPopupTrigger()) {
        getPopupMenu().show(e.getComponent(), e.getX(), e.getY());
      }
    }
  }


  @Override
  public ListModel<ICLogic> getChildrenDataList() {
    return manager.getItemListModel();
  }

  @Override
  public Action[] getContextActions() {
    return new Action[] {
        pm.getAction(CLogicManagerModel.ACTION_KEY_ADD)
    };
  }

}
