/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.digitalpointcontrol;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ComponentModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
import com.lucy.g3.xml.gen.common.ControlLogicDef.DCTL_LOGIC_TRIGGER;


class DigitalPointControlSettingsEditor extends AbstractCLogicSettingsEditor {

  /**
   * 
   */
  private static final String LABEL_PULSE_DURATION = "Pulse Duration";

  public DigitalPointControlSettingsEditor(DigitalPointControlSettings settings) {
    super(settings);
  }

  @Override
  public void validate(ValidationResult result) {
    checkNotNegative(result, pm, DigitalPointControlSettings.PROPERTY_PULSEDURATION, LABEL_PULSE_DURATION);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    JComponent comp0;
    
    DefaultFormBuilder builder = getDefaultBuilder();

    final BufferedValueModel enableVM = pm.getBufferedModel(DigitalPointControlSettings.PROPERTY_PULSEENABLED);
    comp0 = BasicComponentFactory.createCheckBox(
        enableVM,
        "Pulse Enabled");
    builder.append("", comp0);
    builder.nextLine();

    createIntField(LABEL_PULSE_DURATION, DigitalPointControlSettings.PROPERTY_PULSEDURATION, Units.MS);
    
    SelectionInList<DCTL_LOGIC_TRIGGER> list = new SelectionInList<DCTL_LOGIC_TRIGGER>(
        DCTL_LOGIC_TRIGGER.values(),
        pm.getBufferedComponentModel(DigitalPointControlSettings.PROPERTY_TRIGGER));
    comp0 = BasicComponentFactory.createComboBox(list);
    builder.append("Trigger:", comp0);
    builder.nextLine();

    // Bind enable property
    PropertyConnector.connectAndUpdate(enableVM,
        pm.getBufferedComponentModel(DigitalPointControlSettings.PROPERTY_PULSEDURATION),
        ComponentModel.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(enableVM,
        pm.getBufferedComponentModel(DigitalPointControlSettings.PROPERTY_TRIGGER),
        ComponentModel.PROPERTY_ENABLED);
    
    return builder.getPanel();
  }

}

