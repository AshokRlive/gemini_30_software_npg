
package com.lucy.g3.rtu.config.clogic.batterytest;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;


public class BatteryTestLogicFactory extends AbstractClogicFactory{
  public final static BatteryTestLogicFactory INSTANCE = new BatteryTestLogicFactory();
  private BatteryTestLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new BatteryTestSettingsEditor((BatteryTestSettings) settings);
  }

  @Override
  public ICLogic createLogic() {
    return new BatteryTest();
  }

  @Override
  public ICLogicType getLogicType() {
    return BatteryTest.TYPE;
  }

  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return IControlLogicAddingWizardContributor.Factory.createOutputChannelContributor(
        ChannelType.PSM_CH_BCHARGER,"Select a Battery Charger Channel");
  }
  
}

