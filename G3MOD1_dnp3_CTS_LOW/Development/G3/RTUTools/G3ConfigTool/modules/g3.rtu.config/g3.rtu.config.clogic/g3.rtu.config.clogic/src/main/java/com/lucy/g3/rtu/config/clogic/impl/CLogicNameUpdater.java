/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogic;

/**
 * Component for updating Control Logic name.
 */
final public class CLogicNameUpdater implements PropertyChangeListener {

  private final AbstractCLogic<?> logic;
  private CLogicIO<?> io;


  public CLogicNameUpdater(AbstractCLogic<?> logic) {
    this.logic = Preconditions.checkNotNull(logic, "logic must not be null");
    updateName();

    this.logic.addPropertyChangeListener(ICLogic.PROPERTY_CUSTOM_NAME, this);
    this.logic.addPropertyChangeListener(ICLogic.PROPERTY_DESCRIPTION, this);
    this.logic.addPropertyChangeListener(ICLogic.PROPERTY_GROUP, this);
  }

  private void updateName() {
    String name = null;

    /* create name from description */
    if (name == null) {
      String cname = logic.getCustomName();
      String description = logic.getDescription();

      if (Strings.isNotBlank(cname)) {
        name = cname;
      } else if (Strings.isNotBlank(description)) {
        name = description;
      } else {
        name = createDefaultDescription();
      }

      name = "[" + logic.getGroup() + "] " + name;
    }

    logic.setName(name);
  }

  private void updateDescription() {
    if (io != null) {
      String valueStr = io.getValueString();
      if (!CLogicIO.MANDATORY_NULL_TEXT.equals(valueStr)
          && !CLogicIO.OPTIONAL_NULL_TEXT.equals(valueStr)
          && !Strings.isBlank(valueStr)) {
        logic.setDescription(valueStr);
      } else {
        logic.setDescription(createDefaultDescription());
      }
    } else {
      logic.setDescription(createDefaultDescription());
    }
  }

  private String createDefaultDescription() {
    return logic.getType().getTypeName();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getSource() == io && CLogicIO.PROPERTY_VALUE_STRING.equals(evt.getPropertyName())) {
      updateDescription();
    } else {
      updateName();
    }
  }

  /**
   * Gets the name of control logic when it is used as a source of another
   * items.
   */
  public String getNameAsSource() {
    String customName = logic.getCustomName();
    String description = logic.getDescription();
    return Strings.isBlank(customName) ? description : customName;
  }

  public void bindDescriptionToIO(CLogicIO<?> io) {
    if (this.io != null) {
      this.io.removePropertyChangeListener(CLogicIO.PROPERTY_VALUE_STRING, this);
    }

    this.io = io;
    updateDescription();

    if (io != null) {
      io.addPropertyChangeListener(CLogicIO.PROPERTY_VALUE_STRING, this);
    }
  }
}