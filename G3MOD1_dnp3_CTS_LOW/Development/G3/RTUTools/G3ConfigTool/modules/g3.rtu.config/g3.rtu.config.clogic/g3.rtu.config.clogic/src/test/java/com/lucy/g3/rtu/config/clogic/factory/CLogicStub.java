/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.factory;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogic;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

public class CLogicStub extends AbstractCLogic<ICLogicSettings> {

  public CLogicStub(ICLogicType type) {
    super(type);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {

    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return null;
  }

  @Override
  public IPseudoPoint[] getAllPoints() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IInputsManager getInputsManager() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public IOutputsManager<?> getOutputsManager() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public ICLogicPointsManager getPointsManager() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  protected ICLogicSettings createSettings() {
    // TODO Auto-generated method stub
    return null;
  }

}
