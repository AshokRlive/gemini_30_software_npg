/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.minmaxavg;

import java.util.Arrays;
import java.util.HashMap;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.MMAVG_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.MMAVG_POINT;

/**
 * Implementation of MinMaxAverage Logic.
 */
public class MinMaxAvgLogic extends PredefinedCLogic<MinMaxAvgLogicSettings>{
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.MMAVG, "Max/Min/Average")
      .desp("The logic for measuring Max/Min/Average value")
      .get();
  
  /**
   * The Enum EventMode.
   */
  public static enum EventMode {
    BY_INTERVAL,
    BY_CLOCKTIME
  }


  private static final MMAVG_POINT[] POINTS_ENUM_WITHOUT_COUNTER = {
      MMAVG_POINT.MMAVG_POINT_A_AVG,
      MMAVG_POINT.MMAVG_POINT_A_MIN,
      MMAVG_POINT.MMAVG_POINT_A_MAX,
  };


  public MinMaxAvgLogic() {
    super(TYPE);
  }
  
  void init(boolean counterEnabled) {
    super.init(MMAVG_INPUT.values(), counterEnabled ? MMAVG_POINT.values() : POINTS_ENUM_WITHOUT_COUNTER);
  }
  
  @Override
  protected final void setDescription(String description) {
    super.setDescription(description);
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }


  // ================== INSTANTS ===================

  private static Long[] PERIOD_CHOICES;


  public static Long[] getPeriodChoices() {
    if (PERIOD_CHOICES == null) {
      PERIOD_CHOICES = new Long[] {
          1L,// 1 minute
          2L,// 2 minutes
          5L,// 5 minutes
          10L,// 10 minutes
          15L,// 15 minutes
          20L,// 20 minutes
          30L,// 30 minutes
          60L,// 1 hour
          120L,// 2 hours
          180L,// 3 hours
          360L,// 6 hours
          720L,// 12 hours
          1440L,// 24 hours
      };
    }

    return Arrays.copyOf(PERIOD_CHOICES, PERIOD_CHOICES.length);
  }


  private static Long[] CLOCK_CHOICES;


  public static Long[] getClockChoices() {
    if (CLOCK_CHOICES == null) {
      CLOCK_CHOICES = new Long[] {
          1L,// 1 minute
          2L,// 2 minutes
          5L,// 5 minutes
          10L,// 10 minutes
          15L,// 15 minutes
          20L,// 20 minutes
          30L,// 30 minutes
          60L,// 1 hour
          120L,// 2 hours
          180L,// 3 hours
          360L,// 6 hours
          720L,// 12 hours
          1440L,// 24 hours
      };
    }

    return Arrays.copyOf(CLOCK_CHOICES, CLOCK_CHOICES.length);
  }


  /** time from o'clock. */
  public enum ClockTime {
    ONE_MIN(1L, "1 Minute"),
    TWO_MIN(2L, "2 Minutes"),
    FIVE_MIN(5L, "5 Minutes"),
    TEN_MIN(10L, "10 Minutes"),
    TWENTY_MIN(20L, "20 Minutes"),
    THRITY_MIN(30L, "30 Minutes"),
    ONE_HOUR(60L, "1 Hour"),
    TWO_HOUR(120L, "2 Hours"),
    THREE_HOUR(180L, "3 Hours"),
    SIX_HOUR(360L, "6 Hours"),
    TWELVE_HOUR(720L, "12 Hours"),
    ONE_DAY(1440L, "24 Hours");

    private static final HashMap<Long, ClockTime> TYPE_BY_VALUE = new HashMap<Long, ClockTime>();

    static {
      for (ClockTime type : ClockTime.values()) {
        TYPE_BY_VALUE.put(type.value, type);
      }
    }


    ClockTime(Long value, String valueStr) {
      this.value = value;
      this.valueStr = valueStr;
    }

    @Override
    public String toString() {
      return valueStr;
    }

    public static ClockTime forValue(long value) {
      return TYPE_BY_VALUE.get(value);
    }


    protected final Long value;
    protected final String valueStr;
  }


  @Override
  protected MinMaxAvgLogicSettings createSettings() {
    return new MinMaxAvgLogicSettings(this);
  }
}
