/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic;

import java.awt.Color;
import javax.swing.JCheckBox;
import org.jdesktop.swingx.JXLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
class OLRConfigPage extends WizardPage{
  public static final String KEY_REUIQRE_OLR = "requireOLR";

  public OLRConfigPage() {
    super("Configure OLR");
    initComponents();
    cboxOLR.setName(KEY_REUIQRE_OLR);
    cboxOLR.setSelected(true);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    cboxOLR = new JCheckBox();
    label1 = new JXLabel();

    //======== this ========
    setLayout(new FormLayout(
        "10dlu, default:grow",
        "default, $lgap, default"));

    //---- cboxOLR ----
    cboxOLR.setText("Allow Off/Local/Remote controlling");
    add(cboxOLR, CC.xywh(1, 1, 2, 1));

    //---- label1 ----
    label1.setText("* If enabled, the control logic will be configured with an input \"Off/Local/Remote\"");
    label1.setForeground(Color.gray);
    label1.setLineWrap(true);
    add(label1, CC.xy(2, 3));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JCheckBox cboxOLR;
  private JXLabel label1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
