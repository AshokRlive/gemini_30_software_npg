/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.logicgate;


import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.InputsManagerImpl;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.LOGIC_GATE_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of Equipment Fault Handler Control Logic, which has
 * configurable amount of inputs.
 */
public final class LogicGate extends PredefinedCLogic<LogicGateSettings>  {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.LOGIC_GATE, "Logic Gate")
      .get();
  
  private final IInputsManager inputs = new InputsManagerImpl(this, VirtualPointType.binaryTypes()) {

    @Override
    protected CLogicIOPoint createNewInput(boolean isMandatory, boolean allowUserChangeLabel, boolean allowUserDelete,
        VirtualPointType[] types, String labelStr) {
      return super.createNewInput(true, allowUserChangeLabel, allowUserDelete, types, labelStr);
    }
  
  };


  public LogicGate() {
    super(TYPE, null, LOGIC_GATE_POINT.values());
  }


  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return inputs.getAll();
  }

  @Override
  public IInputsManager getInputsManager() {
    return inputs;
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return null;
  }

  @Override
  public Module getSourceModule() {
    return null;
  }

//  @Override
//  public boolean isCustomLogic() {
//    return true;
//  }

  @Override
  public boolean checkSourceModule(MODULE moduleType) {
    return moduleType == MODULE.MODULE_MCM;
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }


  @Override
  protected LogicGateSettings createSettings() {
    return new LogicGateSettings(this);
  }

}
