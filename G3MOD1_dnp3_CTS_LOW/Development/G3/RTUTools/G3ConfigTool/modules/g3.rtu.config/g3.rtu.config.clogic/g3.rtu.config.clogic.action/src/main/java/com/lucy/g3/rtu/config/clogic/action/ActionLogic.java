/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.action;

import java.util.Arrays;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.InputSignal;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.ACTION_CLOGIC_CMD;
import com.lucy.g3.xml.gen.common.ControlLogicDef.ACTION_CLOGIC_OUTPUT;

/**
 * Implementation of Action Logic.
 */
public class ActionLogic extends AbstractCLogic<ActionLogicSettings> {
  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.ACTION, "Action Logic")
      .get();
  
  private CLogicIOPoint[] inputs;
  private final CLogicIOLogic[] outputs;
  private boolean initialised = false;

  public ActionLogic() {
    super(TYPE);

    int[] outputTypes = new int[] {
        ICLogicType.DOL,// Digital Output
        ICLogicType.FAN,// Fan Test
        ICLogicType.BAT_CHARGER,// Battery Charger
        ICLogicType.FPI_RESET,// FPI Test/Reset
        ICLogicType.PCLK, // Pseudo Clock
        ICLogicType.DIGCTRL, 
        ICLogicType.SGL,
        ICLogicType.POWER_SUPPLY,
    };

    ACTION_CLOGIC_OUTPUT[] outputEnum = ACTION_CLOGIC_OUTPUT.values();
    outputs = new CLogicIOLogic[outputEnum.length];
    for (int i = 0; i < outputEnum.length; i++) {
      outputs[i] = new CLogicIOLogic(this,
          outputEnum[i].getDescription(),
          outputTypes,
          false, true, false, false);
    }
  }

  /**
   * Initialise the number of input of this logic. This function can only be
   * called once.
   */
  public void init(int inputNum) {
    if (initialised) {
      log.warn("Already initialised!");
      return;
    }

    Preconditions.checkArgument(inputNum > 0, "inputNum must be > 0");

    
    inputs = new CLogicIOPoint[inputNum];

    VirtualPointType[] inpointType = { VirtualPointType.BINARY_INPUT };

    for (int i = 0; i < inputNum; i++) {
      inputs[i] = new CLogicIOPoint(this, "Input " + i, inpointType, false, false, false, true);
    }
    
    getSettings().setInputsNum(inputNum);

    initialised = true;
  }

  private void checkInitialised() throws IllegalStateException {
    if (initialised == false) {
      throw new IllegalStateException("Not initialised:" + this);
    }
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    checkInitialised();
    return inputs == null ? null : Arrays.copyOf(inputs, inputs.length);
  }


  @Override
  public CLogicIO<?>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public Module getSourceModule() {
    return null;
  }


  /**
   * InputSetting for Action Logic.
   */
  public static final class InputSetting extends InputSignal {

    /**
     * The name of property {@value} . <li>Value type: {@link ACTION_CLOGIC_CMD}
     * .</li>
     */
    public static final String PROPERTY_COMMAND = "command";

    private ACTION_CLOGIC_CMD command = ACTION_CLOGIC_CMD.ACTION_CLOGIC_CMD_OPERATE;


    public ACTION_CLOGIC_CMD getCommand() {
      return command;
    }

    public void setCommand(ACTION_CLOGIC_CMD command) {
      Object oldValue = this.command;
      this.command = command;
      firePropertyChange("command", oldValue, command);
    }

  }



  @Override
  public IPseudoPoint[] getAllPoints() {
    return new IPseudoPoint[0];
  }

  @Override
  public IInputsManager getInputsManager() {
    return null;
  }

  @Override
  public IOutputsManager<?> getOutputsManager() {
    return null;
  }

  @Override
  public ICLogicPointsManager getPointsManager() {
    return null;
  }

  @Override
  protected ActionLogicSettings createSettings() {
    return new ActionLogicSettings(this);
  }
}
