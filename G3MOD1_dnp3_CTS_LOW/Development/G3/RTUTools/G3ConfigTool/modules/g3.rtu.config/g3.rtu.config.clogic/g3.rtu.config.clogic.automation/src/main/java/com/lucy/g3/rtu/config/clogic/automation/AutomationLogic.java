/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.automation;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IInputsManager;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.IOutputsManager;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogic;
import com.lucy.g3.rtu.config.clogic.impl.CLogicPointsManagerImpl;
import com.lucy.g3.rtu.config.clogic.impl.ICLogicPointsManager;
import com.lucy.g3.rtu.config.clogic.impl.InputsManagerImpl;
import com.lucy.g3.rtu.config.clogic.impl.OutputsManagerImpl;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * Implementation of Automation control logic.
 */
public abstract class AutomationLogic extends AbstractCLogic<AutomationLogicSettings> implements IOperableLogic{
  protected final IOutputsManager<ICLogic> outputManager = new OutputsManagerImpl(this);
  protected final ICLogicPointsManager pointsManager = new CLogicPointsManagerImpl(this);
  protected final IInputsManager  inputManager = new InputsManagerImpl(this,VirtualPointType.values());

  protected AutomationLogic(CLogicType TYPE) {
    super(TYPE);
  }


  @Override
  public Module getSourceModule() {
    return null;
  }

  @Override
  public IInputsManager getInputsManager() {
    return inputManager;
  }

  @Override
  public IOutputsManager<ICLogic> getOutputsManager() {
    return outputManager;
  }

  @Override
  public ICLogicPointsManager getPointsManager() {
    return pointsManager;
  }

  @Override
  public CLogicIO<VirtualPoint>[] getInputs() {
    return inputManager.getAll();
  }

  @Override
  public CLogicIO<?>[] getOutputs() {
    return outputManager.getAll();
  }

  @Override
  public IPseudoPoint[] getAllPoints() {
    return pointsManager.getAllPoints();
  }

  @Override
  public boolean isSwitchLogic() {
    return true;
  }
}
