/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.changeover;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.g3schema.ns_clogic.ChangeOverParamT;
import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettings;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOPoint;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SECTIONALISER_INPUT;


public class ChangeOverSettings extends AbstractCLogicSettings<ChangeOver> {

  public static final String PROPERTY_DEFAULT_ENABLED = "defaultEnabled";

  public static final String PROPERTY_SWITCHOFF_DELAY = "switchOffDelay";

  public static final String PROPERTY_SWITCHCHANGE_DELAY = "switchChangeDelay";

  /**
   * The option vaue for input type. With this type the sectionaliser has two
   * inputs MV1 and MV2 as voltage sensor.
   */
  public static final int INPUT_TYPE_MV = 0;

  /**
   * The option vaue for input type. With this type the sectionaliser uses input
   * LV as voltage sensor.
   */
  public static final int INPUT_TYPE_LV = 1;

  private boolean defaultEnabled;

  private long switchOffDelay = 30; // seconds

  private long switchChangeDelay = 5; // seconds

  private int inputType;

  ChangeOverSettings(ChangeOver owner) {
    super(owner);
}
  
  public int getInputType() {
    return inputType;
  }

  /**
   * Modify the input type.
   *
   * @param inputType
   *          {@code INPUT_TYPE_MV} or {@code INPUT_TYPE_LV}
   */
  void setInputType(int inputType) {
    if (inputType != INPUT_TYPE_LV && inputType != INPUT_TYPE_MV) {
      log.error("Invalid input type: " + inputType);
      return;
    }

    Object oldValue = getInputType();
    this.inputType = inputType;
    
    ChangeOver owner = getOwnerLogic();
    
    // Update inputs
    List<CLogicIO<VirtualPoint>> list = new ArrayList<>(Arrays.asList(owner.getOriginalInputs()));
    switch (inputType) {
    case INPUT_TYPE_LV:
      list.remove(owner.getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_MV0));
      list.remove(owner.getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_MV1));
      break;

    case INPUT_TYPE_MV:
      list.remove(owner.getInputByEnum(SECTIONALISER_INPUT.SECTIONALISER_INPUT_LV));
      break;
    default:
      break;
    }
    CLogicIOPoint[] inputs = list.toArray(new CLogicIOPoint[list.size()]);
    owner.setInputs(inputs);

    firePropertyChange(PROPERTY_INPUT_TYPE, oldValue, inputType);
  }

  public boolean isDefaultEnabled() {
    return defaultEnabled;
  }

  public void setDefaultEnabled(boolean defaultEnabled) {
    Object oldValue = isDefaultEnabled();
    this.defaultEnabled = defaultEnabled;
    firePropertyChange(PROPERTY_DEFAULT_ENABLED, oldValue, defaultEnabled);
  }

  public long getSwitchOffDelay() {
    return switchOffDelay;
  }

  public void setSwitchOffDelay(long switchOffDelay) {
    if (switchOffDelay < 0) {
      log.warn("Cannot set Switch off delay to: " + switchOffDelay);
      return;
    }

    Object oldValue = getSwitchChangeDelay();
    this.switchOffDelay = switchOffDelay;
    firePropertyChange(PROPERTY_SWITCHOFF_DELAY, oldValue, switchOffDelay);
  }

  public long getSwitchChangeDelay() {
    return switchChangeDelay;
  }

  public void setSwitchChangeDelay(long switchChangeDelay) {
    if (switchChangeDelay < 0) {
      log.warn("Cannot set Switch change delay to: " + switchChangeDelay);
      return;
    }

    Object oldValue = getSwitchChangeDelay();
    this.switchChangeDelay = switchChangeDelay;
    firePropertyChange(PROPERTY_SWITCHCHANGE_DELAY, oldValue, switchChangeDelay);
  }

  @Override
  public IValidator getValidator() {
    return null;
  }
  
  @Override
  public void readParamFromXML(CLogicParametersT xml_param) {
    ChangeOverParamT xml = xml_param.changeOverParam.first();
    setDefaultEnabled(xml.defaultEnable.getValue());
    setSwitchChangeDelay(xml.switchChangeDelay.getValue());
    setSwitchOffDelay(xml.swithcOffDelay.getValue());
  }

  @Override
  public void writeParamToXML(CLogicParametersT xml) {
    ChangeOverParamT xml_changeover = xml.changeOverParam.append();
    xml_changeover.defaultEnable.setValue(isDefaultEnabled());
    xml_changeover.switchChangeDelay.setValue(getSwitchChangeDelay());
    xml_changeover.swithcOffDelay.setValue(getSwitchOffDelay());
  }
}

