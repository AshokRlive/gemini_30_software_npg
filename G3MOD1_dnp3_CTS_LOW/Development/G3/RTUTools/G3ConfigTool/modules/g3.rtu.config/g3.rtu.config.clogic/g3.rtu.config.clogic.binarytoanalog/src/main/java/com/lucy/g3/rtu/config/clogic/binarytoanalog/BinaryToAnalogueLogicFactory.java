
package com.lucy.g3.rtu.config.clogic.binarytoanalog;

import java.util.Map;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.clogic.AbstractClogicFactory;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicSettings;
import com.lucy.g3.rtu.config.clogic.ICLogicSettingsEditor;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ui.wizards.addclogic.contribute.IControlLogicAddingWizardContributor;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.FILTER;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractAnaloguePoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.BTA_POINT;


public class BinaryToAnalogueLogicFactory extends AbstractClogicFactory implements IControlLogicAddingWizardContributor{
  public final static BinaryToAnalogueLogicFactory INSTANCE = new BinaryToAnalogueLogicFactory();
  private BinaryToAnalogueLogicFactory(){}
  
  @Override
  public ICLogicSettingsEditor createEditor(ICLogicSettings settings) {
    return new BinaryToAnalogueSettingsEditor((BinaryToAnalogueSettings) settings);
  }

  @Override
  public BinaryToAnalogue createLogic() {
    return new BinaryToAnalogue();
  }

  @Override
  public ICLogicType getLogicType() {
    return BinaryToAnalogue.TYPE;
  }
  

  @Override
  public IControlLogicAddingWizardContributor getWizardContributor() {
    return this;
  }
  
  
  @Override
  public WizardPage[] getWizardPages(IConfig config) {
    return new WizardPage[]{
        new BinaryToAnalogueOptions()
    };
  }
  
  @Override
  public ICLogic createLogic(ICLogicType type, Map wizardData) throws Exception {
    BinaryToAnalogue newLogic = createLogic();
    BinaryToAnalogue bta = new BinaryToAnalogue();
    int bitsNum = (int) wizardData.get(BinaryToAnalogueOptions.KEY_NUM);
    bta.getInputsManager().addInputs(new VirtualPointType[]{VirtualPointType.BINARY_INPUT},
        bitsNum, false, false, true);
    
    String unit = (String) wizardData.get(BinaryToAnalogueOptions.KEY_UNIT);
    if(Strings.isBlank(unit))
      unit = null;
    
    PseudoAnaloguePoint apoint = (PseudoAnaloguePoint) bta.getPointByID(BTA_POINT.BTA_POINT_ANALOGUE.getValue());
    apoint.setFilterType(FILTER.None);
    apoint.setOverflowEnabled(false);
    apoint.setUnderflowEnabled(false);
    double max = Math.pow(2D, bitsNum) -1;
    AbstractAnaloguePoint.calculateScaling(apoint, 0, max, 1.0, 0, unit);
    
    return newLogic;
  }
}

