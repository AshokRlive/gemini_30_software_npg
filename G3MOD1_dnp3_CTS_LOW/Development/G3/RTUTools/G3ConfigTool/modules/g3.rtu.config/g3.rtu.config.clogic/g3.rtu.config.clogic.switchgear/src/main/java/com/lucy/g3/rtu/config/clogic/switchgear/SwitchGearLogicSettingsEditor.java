/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.switchgear;

import javax.swing.JComponent;
import javax.swing.JDialog;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.clogic.impl.AbstractCLogicSettingsEditor;
import com.lucy.g3.rtu.config.shared.base.labels.Units;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_AINPUT;

/**
 * The Class SwitchGearLogicSettingsEditor.
 */
class SwitchGearLogicSettingsEditor extends AbstractCLogicSettingsEditor {
  private static final String LABEL_OVER_CUR_THRESHOLD      = "Motor Current Threshold";
  private static final String LABEL_OVER_CUR_TIME_MS        = "Motor Overcurrent Measurement Delay";
  private static final String LABEL_LOCALDELAY_SEC          = "Delay for Local Operation";
  private static final String LABEL_REMOTEDELAY_SEC         = "Delay for Remote Operation";
  private static final String LABEL_PRE_OPERATION_DELAY_MS  = "Motor Supply Pre Operation Time";
  private static final String LABEL_POST_OPERATION_DELAY_MS = "Post Operation Inhibit Time";
  public SwitchGearLogicSettingsEditor(SwitchGearLogicSettings settings) {
    super(settings);
  }

  @Override
  public JComponent getComponent(JDialog parent) {
    DefaultFormBuilder builder = getDefaultBuilder();
    JComponent comp = null;

    // Append is Circuit Breaker
    comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(SwitchGearLogicSettings.PROPERTY_CIRCUIT_BREAKER),
        "Is Circuit Breaker");
    builder.append("", comp);
    builder.nextLine();
    
    // Append Open when earthed
    comp = BasicComponentFactory.createCheckBox(
        pm.getBufferedModel(SwitchGearLogicSettings.PROPERTY_OPEN_WHEN_EARTHED),
        "Show Open when Earthed");
    builder.append("", comp);
    builder.nextLine();

    // Append Motor over current threshold
    String unit = PSM_CH_AINPUT.PSM_CH_AINPUT_MS_CURRENT.getUnitForScaleFactor(1D);
    createLongField(LABEL_OVER_CUR_THRESHOLD, SwitchGearLogicSettings.PROPERTY_OVER_CUR_THRESHOLD, unit);

    // Append Motor over current time
    createLongField(LABEL_OVER_CUR_TIME_MS, SwitchGearLogicSettings.PROPERTY_OVER_CUR_TIME_MS, Units.MS);
    
    // Append local delay
    createIntField(LABEL_LOCALDELAY_SEC, SwitchGearLogicSettings.PROPERTY_LOCALDELAY_SEC, Units.SECS);

    // Append remote delay
    createIntField(LABEL_REMOTEDELAY_SEC, SwitchGearLogicSettings.PROPERTY_REMOTEDELAY_SEC, Units.SECS);

    // Append pre operation delay
    createIntField(LABEL_PRE_OPERATION_DELAY_MS, SwitchGearLogicSettings.PROPERTY_PRE_OPERATION_DELAY_MS, Units.MS);

    // Append post operation delay
    createIntField(LABEL_POST_OPERATION_DELAY_MS, SwitchGearLogicSettings.PROPERTY_POST_OPERATION_DELAY_MS, Units.MS);

    return builder.getPanel();
  }

  @Override
  public void validate(ValidationResult result) {
    String key = SwitchGearLogicSettings.PROPERTY_PRE_OPERATION_DELAY_MS;
    int preOpDelayMs = (int) pm.getBufferedValue(key);
    String err = SwitchGearLogicSettingsValidator.validateOperationDelay(preOpDelayMs, ((SwitchGearLogic)getOwner()).getOutputSCM());
    if (err != null)
      result.addError(err, key);
    
    checkNotNegative(result, pm, SwitchGearLogicSettings.PROPERTY_OVER_CUR_THRESHOLD      ,LABEL_OVER_CUR_THRESHOLD      );
    checkNotNegative(result, pm, SwitchGearLogicSettings.PROPERTY_OVER_CUR_TIME_MS        ,LABEL_OVER_CUR_TIME_MS        );
    checkNotNegative(result, pm, SwitchGearLogicSettings.PROPERTY_LOCALDELAY_SEC          ,LABEL_LOCALDELAY_SEC          );
    checkNotNegative(result, pm, SwitchGearLogicSettings.PROPERTY_REMOTEDELAY_SEC         ,LABEL_REMOTEDELAY_SEC         );
    checkNotNegative(result, pm, SwitchGearLogicSettings.PROPERTY_PRE_OPERATION_DELAY_MS  ,LABEL_PRE_OPERATION_DELAY_MS  );
    checkNotNegative(result, pm, SwitchGearLogicSettings.PROPERTY_POST_OPERATION_DELAY_MS ,LABEL_POST_OPERATION_DELAY_MS );

  }

}
