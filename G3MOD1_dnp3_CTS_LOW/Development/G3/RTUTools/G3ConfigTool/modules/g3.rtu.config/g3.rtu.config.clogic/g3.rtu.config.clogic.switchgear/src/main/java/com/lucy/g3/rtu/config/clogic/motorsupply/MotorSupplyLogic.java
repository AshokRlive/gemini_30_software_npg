/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.clogic.motorsupply;

import java.util.Arrays;

import com.lucy.g3.rtu.config.clogic.CLogicIO;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.ISupportOffLocalRemote;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.clogic.impl.CLogicIOSwitch;
import com.lucy.g3.rtu.config.clogic.impl.PredefinedCLogic;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.MOTOR_SUPPLY_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.MOTOR_SUPPLY_OUTPUT;

public final class MotorSupplyLogic extends PredefinedCLogic<MotorSupplyLogicSettings> implements ISupportOffLocalRemote, IOperableLogic {

  public static final CLogicType TYPE = CLogicType
      .builder(ICLogicType.MOTOR_SUPPLY, "Motor Supply")
      .desp("The control logic for controlling a motor supply.")
      .opMode(OperationMode.OPEN_CLOSE)
      .requireOLR(true)
      .get();
  
  private final CLogicIOSwitch[] outputs;


  public MotorSupplyLogic(SwitchModuleOutput output) {
    this();
    setOutputSCM(output);
  }

  public MotorSupplyLogic() {
    super(TYPE, MOTOR_SUPPLY_INPUT.values(), null);

    outputs = new CLogicIOSwitch[MOTOR_SUPPLY_OUTPUT.values().length];
    initOutputs();

    // bindDescriptionToIO(outputs[0]);
  }

  
  @Override
  protected MotorSupplyLogicSettings createSettings() {
    return new MotorSupplyLogicSettings(this);
  }
  
  private void initOutputs() {
    MOTOR_SUPPLY_OUTPUT[] outputEnums = MOTOR_SUPPLY_OUTPUT.values();

    for (int i = 0; i < outputs.length; i++) {
      outputs[i] = new CLogicIOSwitch(this, outputEnums[i].getDescription());
    }
  }

  // ================== End of Attributes Accessors ===================

  public CLogicIO<VirtualPoint> getInput(MOTOR_SUPPLY_INPUT inputItem) {
    return getInputByEnum(inputItem);
  }

  public void setOutputSCM(SwitchModuleOutput output) {
    outputs[MOTOR_SUPPLY_OUTPUT.MOTOR_SUPPLY_OUTPUT_CHANNEL.ordinal()].setValue(output);
  }

  public SwitchModuleOutput getOutputSCM() {
    int index = MOTOR_SUPPLY_OUTPUT.MOTOR_SUPPLY_OUTPUT_CHANNEL.ordinal();
    return outputs[index].getValue();
  }

  @Override
  public CLogicIO<SwitchModuleOutput>[] getOutputs() {
    return Arrays.copyOf(outputs, outputs.length);
  }

  @Override
  public void setInputOLR(PseudoDoubleBinaryPoint offLocalRemotePoint) {
    getInput(MOTOR_SUPPLY_INPUT.MOTOR_SUPPLY_INPUT_OLR).setValue(offLocalRemotePoint);
  }

  @Override
  public Module getSourceModule() {
    SwitchModuleOutput output = getOutputSCM();
    return output == null ? null : output.getModule();
  }

  @Override
  public boolean isSwitchLogic() {
    return true;
  }
  
}
