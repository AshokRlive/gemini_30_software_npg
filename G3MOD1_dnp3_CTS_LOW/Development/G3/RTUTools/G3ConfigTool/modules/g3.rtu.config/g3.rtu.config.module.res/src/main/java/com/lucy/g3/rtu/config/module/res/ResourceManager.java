/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.res;

import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This class is for managing instances of resource class, such as
 * {@link IModuleNameResource}.
 */
final class ResourceManager {

  private static IModuleNameResource moduleNameRes;
  private static IModuleIconResource moduleIconRes;
  private static java.util.logging.Logger logger = Logger.getLogger("ResourceManager");


  private ResourceManager() {
  }

  public static void registerModuleNameResource(IModuleNameResource resource) {
    if (resource != null) {
      moduleNameRes = resource;
    }
  }

  public static void registerModuleIconResource(IModuleIconResource resource) {
    if (resource != null) {
      moduleIconRes = resource;
    }
  }

  public static IModuleNameResource getModuleNameResource() {
    if (moduleNameRes == null) {
      logger.log(Level.SEVERE, "moduleNameRes not registered");
      return DefaultModuleResource.INSTANCE;
    }

    return moduleNameRes;
  }

  public static IModuleIconResource getModuleIconResource() {
    if (moduleIconRes == null) {
      logger.log(Level.SEVERE, "moduleIconRes not registered");
      return DefaultModuleResource.INSTANCE;
    }

    return moduleIconRes;
  }


  private static final class DefaultModuleResource implements IModuleIconResource, IModuleNameResource {

    private static DefaultModuleResource INSTANCE = new DefaultModuleResource();
    private static final ImageIcon EMPTY_ICON = new ImageIcon(new BufferedImage(24, 24, BufferedImage.TYPE_INT_RGB));


    @Override
    public ImageIcon getModuleIconThumbnail(MODULE type) {
      return EMPTY_ICON;
    }

    @Override
    public ImageIcon getModuleIcon(MODULE type) {
      return EMPTY_ICON;
    }

    @Override
    public ImageIcon getLargeModuleIcon(MODULE type) {
      return EMPTY_ICON;
    }

    @Override
    public String getModuleShortName(MODULE type, MODULE_ID id) {
      if(id == null)
        throw new IllegalArgumentException("id must not be null");
      if(type == null)
        throw new IllegalArgumentException("type must not be null");

      return String.format("%s %s", type.getDescription(), id.getDescription());
    }

    @Override
    public String getModuleFullName(MODULE type, MODULE_ID id) {
      return getModuleShortName(type, id);
    }

    @Override
    public String getModuleTypeShortName(MODULE type) {
      return type.getDescription();
    }
  }

}
