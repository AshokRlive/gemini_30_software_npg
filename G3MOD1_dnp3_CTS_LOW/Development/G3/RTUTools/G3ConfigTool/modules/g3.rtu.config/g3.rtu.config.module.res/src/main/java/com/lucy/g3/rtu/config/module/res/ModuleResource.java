/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.res;


import java.util.HashMap;

import javax.swing.ImageIcon;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * This class is for accessing the icon/string resource of Module.
 */
public final class ModuleResource implements IModuleNameResource, IModuleIconResource {

  private final ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(ModuleResource.class);

  private final HashMap<MODULE, ImageIcon> thumbIconsMap = new HashMap<MODULE, ImageIcon>();
  private final HashMap<MODULE, ImageIcon> largeIconsMap = new HashMap<MODULE, ImageIcon>();
  private final HashMap<MODULE, ImageIcon> iconsMap = new HashMap<MODULE, ImageIcon>();


  @Override
  public ImageIcon getModuleIcon(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");
    
    ImageIcon icon = this.iconsMap.get(type);
    if (icon == null) {
      icon = this.resourceMap.getImageIcon(type.name() + ".icon");
      this.iconsMap.put(type, icon);
    }
    return icon;
  }

  @Override
  public ImageIcon getLargeModuleIcon(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");

    ImageIcon icon = this.largeIconsMap.get(type);
    if (icon == null) {
      icon = this.resourceMap.getImageIcon(type.name() + ".icon.large");
      this.largeIconsMap.put(type, icon);
    }
    return icon;
  }

  @Override
  public ImageIcon getModuleIconThumbnail(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");

    ImageIcon icon = this.thumbIconsMap.get(type);
    if (icon == null) {
      icon = this.resourceMap.getImageIcon(type.name() + ".icon.small");
      this.thumbIconsMap.put(type, icon);
    }
    return icon;
  }

  @Override
  public String getModuleShortName(MODULE type, MODULE_ID id) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");
    if(id == null)
      throw new IllegalArgumentException("id must not be null");
    
    String name = getModuleTypeShortName(type);
    if (ModuleEnums.getMaxModuleNumber(type) <= 1) {
      return name;
    } else {
      return String.format("%s %s", name, id.getDescription());
    }
  }

  @Override
  public String getModuleFullName(MODULE type, MODULE_ID id) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");
    if(id == null)
      throw new IllegalArgumentException("id must not be null");

    String sname = getModuleTypeFullName(type);
    if (ModuleEnums.getMaxModuleNumber(type) <= 1) {
      return sname;
    } else {
      return String.format("%s %s", sname, id.getDescription());
    }
  }

  @Override
  public String getModuleTypeShortName(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");

    // return this.resourceMap.getString(type.name() + ".short");
    return type.getDescription();
  }

  public String getModuleTypeFullName(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");

    return this.resourceMap.getString(type.name() + ".name");
  }

  public String getModuleTypeDescription(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");

    return this.resourceMap.getString(type.name() + ".desp");
  }


  public static final ModuleResource INSTANCE = new ModuleResource();

  private ModuleResource() {
  }

}
