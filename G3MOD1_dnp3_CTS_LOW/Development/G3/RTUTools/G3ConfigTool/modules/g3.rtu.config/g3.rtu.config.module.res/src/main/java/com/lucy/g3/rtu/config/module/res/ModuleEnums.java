/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.res;


import java.util.Arrays;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The Class contains enum constants used by Module.
 */
public final class ModuleEnums {

  private ModuleEnums() {
  }


  /** The supported module IDs. */
  private static final MODULE_ID[] SUPPPORTED_MODULE_IDS = {
      MODULE_ID.MODULE_ID_0,
      MODULE_ID.MODULE_ID_1,
      MODULE_ID.MODULE_ID_2,
      MODULE_ID.MODULE_ID_3,
      MODULE_ID.MODULE_ID_4,
      MODULE_ID.MODULE_ID_5,
      MODULE_ID.MODULE_ID_6
  };

  /** The supported module types. */
  private static final MODULE[] SUPPORTED_MODULE_TYPES = {
      MODULE.MODULE_MCM,
      MODULE.MODULE_PSM,
      MODULE.MODULE_HMI,
      MODULE.MODULE_SCM,
      MODULE.MODULE_SCM_MK2,
      MODULE.MODULE_DSM,
      MODULE.MODULE_IOM,
      //MODULE.MODULE_FDM,
      MODULE.MODULE_FPM,
  };

  private static int[] maxModuleNum;


  /**
   * Gets Max Module Number.
   */
  public static int getMaxModuleNumber(MODULE type) {
    if(type == null)
      throw new IllegalArgumentException("type must not be null");

    // Initialise max number for each type of module
    if (maxModuleNum == null) {
      maxModuleNum = new int[MODULE.values().length];
      maxModuleNum[MODULE.MODULE_MCM.ordinal()] = 1;
      maxModuleNum[MODULE.MODULE_PSM.ordinal()] = 1;
      maxModuleNum[MODULE.MODULE_SCM.ordinal()] = 7;
      maxModuleNum[MODULE.MODULE_SCM_MK2.ordinal()] = 7;
      maxModuleNum[MODULE.MODULE_DSM.ordinal()] = 7;
      maxModuleNum[MODULE.MODULE_FDM.ordinal()] = 7;
      maxModuleNum[MODULE.MODULE_FPM.ordinal()] = 7;
      maxModuleNum[MODULE.MODULE_HMI.ordinal()] = 1;
      maxModuleNum[MODULE.MODULE_IOM.ordinal()] = 7;
      maxModuleNum[MODULE.MODULE_MBDEVICE.ordinal()] = 20;
    }

    return maxModuleNum[type.ordinal()];
  }

  /**
   * Gets CAN module types that are supported by application.
   *
   * @return an array of <code>MODULE</code> items.
   */
  public static MODULE[] getSupportedTypes() {
    return Arrays.copyOf(SUPPORTED_MODULE_TYPES, SUPPORTED_MODULE_TYPES.length);
  }

  /**
   * Gets module IDs that are supported by application.
   *
   * @return an array of <code>MODULE_ID</code> items.
   */
  public static MODULE_ID[] getSupportedIDs() {
    return Arrays.copyOf(SUPPPORTED_MODULE_IDS, SUPPPORTED_MODULE_IDS.length);
  }


  public static boolean isDefaultModule(MODULE type, MODULE_ID id) {
    return type == MODULE.MODULE_MCM;
  }

}
