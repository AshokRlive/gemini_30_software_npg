/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.res;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

public class ModuleResourceTest {

  private MODULE[] types;


  @Before
  public void setUp() throws Exception {
    this.types = ModuleEnums.getSupportedTypes();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetModuleIcon() {
    for (MODULE t : this.types) {
      assertNotNull(ModuleResource.INSTANCE.getModuleIcon(t));
      assertNotNull(ModuleResource.INSTANCE.getLargeModuleIcon(t));
      assertNotNull(ModuleResource.INSTANCE.getModuleIconThumbnail(t));
    }
  }

  @Test
  public void testTypeName() {
    for (MODULE t : this.types) {
      assertFalse("Short name is null: " + t.name(), isBlank(ModuleResource.INSTANCE.getModuleTypeShortName(t)));
      assertFalse("Description name is null: " + t.name(),
          isBlank(ModuleResource.INSTANCE.getModuleTypeDescription(t)));
      assertFalse("Full name is null: " + t.name(), isBlank(ModuleResource.INSTANCE.getModuleTypeFullName(t)));
    }
  }

  private boolean isBlank(String str) {
    return str == null || str.trim().isEmpty();
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testGetModuleTypeShortNameNullArg() {
    ModuleResource.INSTANCE.getModuleTypeShortName(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetModuleTypeFullNameNullArg() {
    ModuleResource.INSTANCE.getModuleTypeFullName(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetModuleTypeDescriptionNullArg() {
    ModuleResource.INSTANCE.getModuleTypeDescription(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetModuleIconNullArg() {
    ModuleResource.INSTANCE.getModuleIcon(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetLargeModuleIconNullArg() {
    ModuleResource.INSTANCE.getLargeModuleIcon(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetModuleIconThumbnailNullArg() {
    ModuleResource.INSTANCE.getModuleIconThumbnail(null);
  }

}
