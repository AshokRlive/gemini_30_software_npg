/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.res;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The object that implements this interface generates names for G3 module. 
 */
interface IModuleNameResource {

  String getModuleShortName(MODULE type, MODULE_ID id);

  String getModuleFullName(MODULE type, MODULE_ID id);

  String getModuleTypeShortName(MODULE type);
}
