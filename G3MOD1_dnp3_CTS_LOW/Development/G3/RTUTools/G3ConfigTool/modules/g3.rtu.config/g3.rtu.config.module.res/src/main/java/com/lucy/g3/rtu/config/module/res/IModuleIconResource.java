/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.module.res;

import javax.swing.ImageIcon;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * The object that implements this interface supplies the icons for G3 module. 
 */
interface IModuleIconResource {

  ImageIcon getModuleIconThumbnail(MODULE type);

  ImageIcon getModuleIcon(MODULE type);

  ImageIcon getLargeModuleIcon(MODULE type);
}
