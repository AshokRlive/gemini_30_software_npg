/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.shared;

import com.lucy.g3.itemlist.IItemListManager;

/**
 * The Interface of manager for managing cutsom template.
 *
 * @param <TemplateT>
 *          the type of template
 */
public interface ICustomTemplateManager <TemplateT extends ICustomTemplate> extends IItemListManager<TemplateT>{
  ICustomTemplateOwner getOwner();
}

