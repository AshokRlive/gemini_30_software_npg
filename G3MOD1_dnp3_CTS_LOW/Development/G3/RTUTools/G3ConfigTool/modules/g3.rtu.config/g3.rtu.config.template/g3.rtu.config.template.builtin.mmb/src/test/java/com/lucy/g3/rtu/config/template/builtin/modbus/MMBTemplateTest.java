
package com.lucy.g3.rtu.config.template.builtin.modbus;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.template.builtin.modbus.MMBTemplate;
import com.lucy.g3.rtu.config.template.builtin.modbus.PredefinedModbusDevices;


public class MMBTemplateTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testChannelNum() {
    PredefinedModbusDevices dev = PredefinedModbusDevices.ARGUS7SR;
    MMBTemplate templ = new MMBTemplate(MMBChannel.createDummy(), dev);
    assertEquals(dev.getChannels().length + 1/*Online channel*/, templ.getIomap().getAllChannels().size());
  }

}

