/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import java.beans.PropertyVetoException;

import org.apache.log4j.Logger;

import com.g3schema.ns_sdnp3.SDNP3CommsDeviceConfT;
import com.g3schema.ns_template.CommsDeviceConfigT;
import com.g3schema.ns_template.ScadaTemplateCommsDevT;
import com.lucy.g3.rtu.config.device.comms.domain.AbstractCommsDevice;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceUser;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.sdnp3.serial.GPRSTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.serial.ISerialTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.serial.PAKNETTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.serial.PSTNTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;

/**
 * The template of PAKET for SCADA.
 */
public class ScadaTemplateCommsDev implements ICustomTemplate {

  
  private Logger log = Logger.getLogger(ScadaTemplateCommsDev.class);
  
  private final ICustomTemplateManager<?> manager;
  
  private final PAKNETTemplate paknet;
  private final GPRSTemplate gprs;
  private final PSTNTemplate pstn;
  
  private CommsDeviceType type;

  ScadaTemplateCommsDev(ICustomTemplateManager<?> manager) {
    super();
    this.type = CommsDeviceType.PAKNET_MODEM;
    this.manager = manager;
    
    this.paknet = new PAKNETTemplate(new CommsDeviceSelection());
    this.pstn = new PSTNTemplate(new CommsDeviceSelection());
    this.gprs = new GPRSTemplate(new CommsDeviceSelection());
  }
  

  public CommsDeviceType getType() {
    return type;
  }

  
  void setType(CommsDeviceType type) {
    this.type = type;
  }

  public AbstractCommsDevice getDevByType (CommsDeviceType type) {
    switch (type) {
    case GPRS_MODEM:
      return gprs;
    case PAKNET_MODEM:
      return paknet;
    case PSTN_MODEM:
      return pstn;
      
    default:
      throw new IllegalStateException("Unsupported type:"+type);
    }
  }
  
  
  public CommsDeviceSelection getCommsDeviceSelection() {
    return getCommsDeivceConfig().getCommsDeviceSelection();
  }

  
  @Override
  public ICustomTemplateManager<?> getManager() {
    return manager;
  }


  public static final class CommsDeviceSelection extends CommsDeviceUser {
    public final static String PROPERTY_CONNECTION_CLASS = "connectionClass";
    
    private int connectionClass;

    public int getConnectionClass() {
      return connectionClass;
    }
    
    public void setConnectionClass(int connectionClass) {
      Object oldValue = this.connectionClass;
      this.connectionClass = connectionClass;
      firePropertyChange(PROPERTY_CONNECTION_CLASS, oldValue, connectionClass);
    }

    @Override
    public String toString() {
      ICommsDevice dev = getCommsDevice() ;
      return "";//dev == null ? "Not Selected" : dev.getDisplayName();
    }

    @Override
    public void setCommsDevice(ICommsDevice commsDevice) throws PropertyVetoException {
      super.setCommsDevice(commsDevice);
      
      // this is a template, not a comms device  
      if(commsDevice != null)
        commsDevice.removeUser(this);
    }
    
  }

  private ICommsDevice findCommsDevice(int commsDeviceId) {
    if(manager != null) {
      IConfig conf = manager.getOwner().getOwner();
      CommsDeviceManager manager = (CommsDeviceManager) (conf == null 
              ? null 
              : conf.getConfigModule(CommsDeviceManager.CONFIG_MODULE_ID));
        if(manager != null) {
          return manager.getDevice(commsDeviceId);
      }
    }
    
    log.warn("Comms Device not found for id:" + commsDeviceId);
    return null;
  }

  public void readFromXML(ScadaTemplateCommsDevT xml) {
    String masterAddress = null;
    if(xml.masterAddress.exists())
      masterAddress = xml.masterAddress.getValue(); 
      
    // Read comms device config
    if(xml.commsDeviceConfig.exists()) {
      CommsDeviceConfigT xmlConf = xml.commsDeviceConfig.first();
      
      if(xmlConf.PAKNETModem.exists()) {
        paknet.readFromXML(xmlConf.PAKNETModem.first(), null);
        paknet.getScadaAddress().setMasterAddress(masterAddress);
        setType(CommsDeviceType.PAKNET_MODEM);
      }
      
      else if(xmlConf.GPRSModem.exists()) {
        gprs.readFromXML(xmlConf.GPRSModem.first(), null);
        gprs.getScadaAddress().setMasterAddress(masterAddress);
        setType(CommsDeviceType.GPRS_MODEM);
      }
      
      else if(xmlConf.PSTNModem.exists()) {
        pstn.readFromXML(xmlConf.PSTNModem.first(), null);
        pstn.getScadaAddress().setMasterAddress(masterAddress);
        setType(CommsDeviceType.PSTN_MODEM);
      }
    }
    
    // Read comms device reference
    if(xml.commsDevice.exists()) {
      CommsDeviceSelection commDevSel = getCommsDeivceConfig().getCommsDeviceSelection();

      if(xml.commsDevice.first().commsDeviceID.exists()) {
        try {
          commDevSel.setCommsDevice(findCommsDevice((int) xml.commsDevice.first().commsDeviceID.getValue()));
        } catch (PropertyVetoException e) {
          e.printStackTrace();
        }
      }
      
      // Read device connection class
      commDevSel.setConnectionClass(xml.commsDevice.first().connectionClass.getValue());
    }    
  }


  public void writeToXML(ScadaTemplateCommsDevT xml) {
    final CommsDeviceSelection comDevSel = getCommsDeivceConfig().getCommsDeviceSelection();
    final ICommsDevice device = comDevSel.getCommsDevice();
    
    if(device != null) {
      SDNP3CommsDeviceConfT xmlDev = xml.commsDevice.append();
      xmlDev.commsDeviceID.setValue(device.getDeviceId());
      xmlDev.connectionClass.setValue(comDevSel.getConnectionClass());
    }
    
    CommsDeviceConfigT xmlConf = xml.commsDeviceConfig.append();
    
    switch(type) {
    case GPRS_MODEM:
      gprs.writeToXML(xmlConf.GPRSModem.append());
      xml.masterAddress.setValue(gprs.getScadaAddress().getMasterAddress());
      break;
      
    case PAKNET_MODEM:
      paknet.writeToXML(xmlConf.PAKNETModem.append());
      xml.masterAddress.setValue(paknet.getScadaAddress().getMasterAddress());
      break;
      
    case PSTN_MODEM:
      pstn.writeToXML(xmlConf.PSTNModem.append());
      xml.masterAddress.setValue(pstn.getScadaAddress().getMasterAddress());
      break;
      
    default:
      log.error("Nothing written to XML. Unsupported type:" + type);
      break;
    }
  }

  public String getMasterAddress() {
    ISerialTemplate dev = getCommsDeivceConfig();
    return dev == null ? null : dev.getScadaAddress().getMasterAddress();
  }

  public ISerialTemplate getCommsDeivceConfig() {
    switch(type) {
    case GPRS_MODEM:
      return gprs;
    case PAKNET_MODEM:
      return paknet;
    case PSTN_MODEM:
      return pstn;
    default:
      throw new IllegalStateException("unsupported type:"+type);
    
    }
  }
  
}
