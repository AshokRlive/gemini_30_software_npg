/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import com.lucy.g3.network.support.IPAddress;


/**
 *
 */
public class TcpScadaAddress extends ScadaAddress {
  private String ip = "0.0.0.0";
  
  public String getIp() {
    return ip;
  }

  
  public void setIp(String ip) {
    if(IPAddress.validateIPAddress(ip) == false)
      return;
    
    this.ip = ip;
  }
  
  @Override
  public String toString() {
    return String.format("IP: %s     Master Address: %s",ip, getMasterAddress());
  }
}

