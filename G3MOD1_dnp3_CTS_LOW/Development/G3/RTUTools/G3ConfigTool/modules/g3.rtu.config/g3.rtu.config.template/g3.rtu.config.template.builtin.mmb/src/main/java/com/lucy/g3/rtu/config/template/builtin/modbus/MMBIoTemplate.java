/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.builtin.modbus;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.MMBIoBitMask;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterDataType;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel.RegisterType;


class MMBIoTemplate {
  private final String            name;
  private final ChannelType       type;
  private final RegisterType      regType;
  private final RegisterDataType  regDataType;
  private final MMBIoBitMask      bitMask;
  private final long              regAddr;
  private final long              pollingRate;
  
  
  
  public MMBIoTemplate(String name, ChannelType type, RegisterType regType, RegisterDataType regDataType,
      MMBIoBitMask bitMask, long regAddr, long pollingRate) {
    super();
    this.name = name;
    this.type = type;
    this.regType = regType;
    this.regDataType = regDataType;
    this.bitMask = bitMask;
    this.regAddr = regAddr;
    this.pollingRate = pollingRate;
  }

  public ChannelType getType() {
    return type;
  }

  /**
   * Creates a channel with this template.
   * @param owner the owner of the channel
   * @param id the id of the channel.
   * @return
   */
  public MMBIoChannel createChannel(Module owner, int id) {
    MMBIoChannel channel = new MMBIoChannel(type, id, owner, name);
    
    if (channel.hasParameter(MMBIoChannel.PARAM_REG_TYPE)) {
      channel.setParameter(MMBIoChannel.PARAM_REG_TYPE, regType);
    }
    
    if (channel.hasParameter(MMBIoChannel.PARAM_REG_ADDRESS)) {
      channel.setParameter(MMBIoChannel.PARAM_REG_ADDRESS, regAddr);
    }
    
    if (channel.hasParameter(MMBIoChannel.PARAM_DATA_TYPE)) {
      channel.setParameter(MMBIoChannel.PARAM_DATA_TYPE, regDataType);
    }
    
    if (channel.hasParameter(MMBIoChannel.PARAM_POLL_RATE)) {
      channel.setParameter(MMBIoChannel.PARAM_POLL_RATE, pollingRate);
    }
    
    if (channel.hasParameter(MMBIoChannel.PARAM_BIT_MASK) && bitMask != null) {
      channel.setParameter(MMBIoChannel.PARAM_BIT_MASK, bitMask);
    }
    
    return channel;
  }


  static MMBIoTemplate createIO(String name, int regAddr, ChannelType type, RegisterType regType,
      RegisterDataType regDataType, MMBIoBitMask bitMask, int pollingRate) {
    return new MMBIoTemplate(name, type, regType, regDataType, bitMask, regAddr, pollingRate);
  }
}

