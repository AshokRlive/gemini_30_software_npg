/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3.serial;

import com.l2fprod.common.beans.ExtendedPropertyDescriptor;
import com.lucy.g3.rtu.config.shared.base.bean.BeanInfo;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaAddress;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateCommsDev.CommsDeviceSelection;

/**
 * BeanInfo class required by Switch Template Property Editor.
 */
public class PAKNETTemplateBeanInfo extends BeanInfo {

  public PAKNETTemplateBeanInfo() {
    super(PAKNETTemplate.class);
    
    ExtendedPropertyDescriptor address = addProperty(GPRSTemplate.PROPERTY_SCADA_ADDRESS);
    addSubProperty(address, ScadaAddress.PROPERTY_MASTER_ADDRESS);
    
    ExtendedPropertyDescriptor commsDeviceSel = addProperty(PAKNETTemplate.PROPERTY_COMMS_DEVICE_SElECTION);
    addSubProperty(commsDeviceSel, CommsDeviceSelection.PROPERTY_COMMS_DEVICE);
    addSubProperty(commsDeviceSel, CommsDeviceSelection.PROPERTY_CONNECTION_CLASS);
    
    addProperty(PAKNETTemplate.PROPERTY_MAIN_NUA);
    addProperty(PAKNETTemplate.PROPERTY_STANDBY_NUA);
    
    addProperty(PAKNETTemplate.PROPERTY_CONNECTT_IMEOUT);
    addProperty(PAKNETTemplate.PROPERTY_REGULAR_CALL_INTERVAL);
    addProperty(PAKNETTemplate.PROPERTY_MODEM_INACTIVITY_TIMEOUT);
    addProperty(PAKNETTemplate.PROPERTY_CONNECTION_INACTIVITYT_IMEOUT);
    addProperty(PAKNETTemplate.PROPERTY_DIAL_RETRY_DELAYS);
  }
    
}