/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.switchgear;

import java.awt.Window;

import com.lucy.g3.gui.common.dialogs.PropertyEditorDialog;
import com.lucy.g3.rtu.config.template.custom.shared.ui.AbstractTemplatesPage;

/**
 * Page for managing Switch Templates.
 */
public class SwitchTemplatesPage extends AbstractTemplatesPage<SwitchTemplate, SwitchTemplates> {

  public SwitchTemplatesPage(final SwitchTemplates templates) {
    super(templates, "Switch");
  }

  @Override
  public void showEditor(Window parent, SwitchTemplate selection) {
    PropertyEditorDialog.createAndShowDialog(parent, selection, selection.getSwitchName());
  }

  @Override
  protected SwitchTemplate createNewTempate(String name, SwitchTemplates manager) {
    return new SwitchTemplate(manager, name);
  }

}
