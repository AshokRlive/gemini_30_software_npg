/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.fpi;

import java.util.UUID;

import com.g3schema.ns_template.FPIChannelTemplateT;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ISupportCache;


public class FPIChannelTemplate extends FPIConfig implements ISupportCache, ICustomTemplate{
  
  public final static String PROPERTY_PROFILE_NAME = "profileName";
  public static final String PROPERTY_RST_ON_LV_RESTORED = "resetOnLVRestored";
  public static final String PROPERTY_RST_ON_LV_LOST = "resetOnLVLost";
  
  private String profileName;
  private final ICustomTemplateManager<?> manager;
  private String uuid = UUID.randomUUID().toString();
  

  private boolean resetOnLVRestored = false;
  private boolean resetOnLVLost = false;

  public FPIChannelTemplate(ICustomTemplateManager<?> manager) {
    this(manager, "Unnamed");
  }
  
  public FPIChannelTemplate(ICustomTemplateManager<?> manager, String profileName) {
    super(null);
    this.manager = manager;
    this.profileName = profileName;
  }
  
  public String getProfileName() {
    return profileName;
  }

  @Override
  public String toString() {
    return getProfileName();
  }

  /*
   * A template must be enabled always.
   */
  @Override
  public boolean isEnabled() {
    return true;
  }

  public void setProfileName(String profileName) {
    Object oldValue = this.profileName;
    this.profileName = profileName;
    firePropertyChange(PROPERTY_PROFILE_NAME, oldValue, profileName);
  }
  
  public boolean isResetOnLVRestored() {
    return resetOnLVRestored;
  }

  public void setResetOnLVRestored(boolean rstOnLVRestored) {
    Object oldValue = isResetOnLVRestored();
    this.resetOnLVRestored = rstOnLVRestored;
    firePropertyChange(PROPERTY_RST_ON_LV_RESTORED, oldValue, rstOnLVRestored);
  }

  public boolean isResetOnLVLost() {
    return resetOnLVLost;
  }

  public void setResetOnLVLost(boolean resetOnLVLost) {
    Object oldValue = isResetOnLVLost();
    this.resetOnLVLost = resetOnLVLost;
    firePropertyChange(PROPERTY_RST_ON_LV_LOST, oldValue, resetOnLVLost);
  }

  public String getUuid() {
    return uuid;
  }

  private void setUuid(String uuid) {
    Object oldValue = this.uuid;
    this.uuid = uuid;
    firePropertyChange("uuid", oldValue, uuid);
  }

  public void writeToXML(FPIChannelTemplateT xml) {
    super.writeToXML(xml);
    xml.profileName.setValue(getProfileName());
    xml.uuid.setValue(uuid);
    xml.resetFPIOnLVLost.setValue(isResetOnLVLost());
    xml.resetFPIOnLVRestored.setValue(isResetOnLVRestored());
  }


  public void readFromXML(FPIChannelTemplateT xml) {
    super.readFromXML(xml);
    setProfileName(xml.profileName.getValue());
    
    if(xml.resetFPIOnLVLost.exists()) {
      setResetOnLVLost(xml.resetFPIOnLVLost.getValue());
    }
    
    if(xml.resetFPIOnLVRestored.exists()) {
      setResetOnLVRestored(xml.resetFPIOnLVRestored.getValue());
    }
    
    if(xml.uuid.exists())
      setUuid(xml.uuid.getValue());
  }


  @Override
  public ICustomTemplateManager<?> getManager() {
    return manager;
  }

  @Override
  public String getUUID() {
    return uuid;
  }
  
}

