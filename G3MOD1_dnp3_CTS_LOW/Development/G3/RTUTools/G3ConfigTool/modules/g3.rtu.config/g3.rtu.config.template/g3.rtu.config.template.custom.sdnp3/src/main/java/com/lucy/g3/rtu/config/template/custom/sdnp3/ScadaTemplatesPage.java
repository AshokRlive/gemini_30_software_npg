/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import java.awt.Window;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.template.custom.shared.ui.AbstractTemplatesPage;

public class ScadaTemplatesPage extends AbstractTemplatesPage<ScadaTemplate, ScadaTemplates> {

  public ScadaTemplatesPage(final ScadaTemplates template) {
    super(template, "SCADA");
  }
  
  
  @Override
  public void showEditor(Window parent, ScadaTemplate selection) {
    WindowUtils.showDialog(new ScadaTemplateEditorDialog(parent, selection));
  }


  @Override
  protected ScadaTemplate createNewTempate(String name, ScadaTemplates manager) {
    return new ScadaTemplate(manager, name);
  }
}
