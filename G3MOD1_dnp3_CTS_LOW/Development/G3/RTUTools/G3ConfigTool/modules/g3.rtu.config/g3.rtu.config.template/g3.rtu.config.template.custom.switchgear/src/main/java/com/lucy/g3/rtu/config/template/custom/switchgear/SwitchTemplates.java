/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.switchgear;

import java.util.Collection;

import com.g3schema.ns_template.SwitchTemplateT;
import com.g3schema.ns_template.TemplateT;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateOwner;

/**
 * The manager of Switch Templates.
 */
public class SwitchTemplates extends ItemListManager<SwitchTemplate> 
                              implements ICustomTemplateManager<SwitchTemplate>{
  
  private final ICustomTemplateOwner owner;
  
  public SwitchTemplates(ICustomTemplateOwner owner) {
    this.owner = owner;
  }


  public Collection<SwitchTemplate> getSwitchTemplates() {
    return getAllItems();
  }


  public void readFromXML(TemplateT xml) {
    int count = xml.switchTemplate.count();
    for (int i = 0; i < count; i++) {
      SwitchTemplateT xmlTempl = xml.switchTemplate.at(i);
      SwitchTemplate newTemplate = new SwitchTemplate(this, null);
      newTemplate.readFromXML(xmlTempl);
      add(newTemplate);
    }
  }

  public void writeToXML(TemplateT xml) {
    Collection<SwitchTemplate> all = getAllItems();
    for (SwitchTemplate templ : all) {
      templ.writeToXML(xml.switchTemplate.append());
    }
  }

  @Override
  public ICustomTemplateOwner getOwner() {
    return owner;
  }

}
