/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.integrated;

import com.g3schema.ns_template.TemplateT;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.fpi.FPIChannelTemplates;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplates;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplates;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateOwner;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplates;

/**
 * Configuration template.
 */
public class CustomTemplates extends AbstractConfigModule implements ICustomTemplateOwner{

  public static final String CONFIG_MODULE_ID = "CommissioningTemplatesModule";

  private final MMBTemplates mmbTemplates = new MMBTemplates(this);

  private final SwitchTemplates switchTemplates = new SwitchTemplates(this);

  private final ScadaTemplates scadaTemplates = new ScadaTemplates(this);
  
  private final FPIChannelTemplates fpiTemplates = new FPIChannelTemplates(this);

  public CustomTemplates(IConfig owner) {
    super(owner);
  }
  
  public MMBTemplates getMMBTemplates() {
    return mmbTemplates;
  }

  public SwitchTemplates getSwitchTemplates() {
    return switchTemplates;
  }
  
  public ScadaTemplates getScadaTemplates() {
    return scadaTemplates;
  }
  
  public FPIChannelTemplates getFpiTemplates() {
    return fpiTemplates;
  }

  public void readFromXML(TemplateT xml) {
    switchTemplates.readFromXML(xml);
    mmbTemplates.readFromXML(xml);
    scadaTemplates.readFromXML(xml);
    fpiTemplates.readFromXML(xml);
  }

  public void writeToXML(TemplateT xml) {
    switchTemplates.writeToXML(xml);
    mmbTemplates.writeToXML(xml);
    scadaTemplates.writeToXML(xml);
    fpiTemplates.writeToXML(xml);
  }
}
