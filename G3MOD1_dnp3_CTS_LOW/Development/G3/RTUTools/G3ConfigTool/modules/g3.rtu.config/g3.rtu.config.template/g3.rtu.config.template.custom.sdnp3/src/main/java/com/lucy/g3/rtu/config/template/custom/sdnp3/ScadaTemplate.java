/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import java.util.UUID;

import org.apache.log4j.Logger;

import com.g3schema.ns_template.ScadaTemplateT;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ISupportCache;


public class ScadaTemplate extends Model implements ISupportCache, ICustomTemplate{

  final public static String PROPERTY_PROFILENAME = "profileName";
  final public static String PROPERTY_CLASSIFIER  = "classifier";
  final public static String PROPERTY_TYPE= "type";
  
  private Logger log = Logger.getLogger(ScadaTemplate.class);
  
  private String uuid = UUID.randomUUID().toString();

  private String profileName = "Unnamed Profile";
  private String classifier = "";
  
  private ScadaTemplateType type = ScadaTemplateType.TCPIP;
  private final ScadaTemplateTcp tcp;
  private final ScadaTemplateCommsDev comms;
  private final ScadaTemplateSerial serial;
  private final ICustomTemplateManager<?> manager;
  
  public ScadaTemplate(ICustomTemplateManager<?> manager){
    this(manager, null);
  }
  
  public ScadaTemplate(ICustomTemplateManager<?> manager, String profileName){
    super();
    this.manager = manager;
    if(profileName != null)
      this.profileName = profileName;
    this.tcp = new ScadaTemplateTcp(manager);
    this.comms = new ScadaTemplateCommsDev(manager);
    this.serial = new ScadaTemplateSerial();
  }

  public String getProfileName() {
    return profileName;
  }
  
  public void setProfileName(String profileName) {
    Object oldValue = this.profileName;
    this.profileName = profileName;
    firePropertyChange(PROPERTY_PROFILENAME, oldValue, profileName);
  }
  
  
  public void setType(ScadaTemplateType type) {
    Preconditions.checkNotNull(type, "type must not be null");
    Object oldValue = this.type;
    this.type = type;
    firePropertyChange(PROPERTY_TYPE, oldValue, type);
  }

  public ScadaTemplateType getType() {
    return type;
  }
  
  public String getClassifier() {
    return classifier;
  }

  
  public void setClassifier(String classifier) {
    if(classifier != null) {
      classifier = classifier.trim();
      Object oldValue = this.classifier;
      this.classifier = classifier;
      firePropertyChange(PROPERTY_CLASSIFIER, oldValue, classifier);
    }
  }

  
  @Override
  public String toString() {
    String typeStr = "Unknown Type";
    if(type == ScadaTemplateType.TCPIP) {
      typeStr  = "TCP/IP";
    } else if(type == ScadaTemplateType.COMMS) {
      typeStr = String.valueOf(getCommsDev().getType());
    } else if(type == ScadaTemplateType.SERIAL) 
      typeStr = "Serial";
    
    return String.format("[%s] %s", typeStr, getProfileName());
  }
  
  public ScadaTemplateTcp getTcp() {
    return tcp;
  }
  
  public ScadaTemplateCommsDev getCommsDev() {
    return comms;
  }

  
  public ScadaTemplateSerial getSerial() {
    return serial;
  }

  public void readFromXML(ScadaTemplateT xml) {
    
    
    setProfileName(xml.profileName.getValue());
    if(xml.classifier.exists())
      setClassifier(xml.classifier.getValue());
    
    if(xml.uuid.exists())
      uuid = xml.uuid.getValue();
    
    
    // Read Serial
    if(xml.commsDevice.exists()) {
      getCommsDev().readFromXML(xml.commsDevice.first());
      setType(ScadaTemplateType.COMMS);
    }
    
    // Read TCP
    if(xml.tcpip.exists()) {
      getTcp().readFromXML(xml.tcpip.first());
      setType(ScadaTemplateType.TCPIP);
    }
    
    // Read Serial
    if(xml.serial.exists()) {
      getSerial().readFromXML(xml.serial.first());
      setType(ScadaTemplateType.SERIAL);
    }
  }


  public void writeToXML(ScadaTemplateT xml) {
    xml.profileName.setValue(getProfileName());
    xml.classifier.setValue(getClassifier());
    xml.uuid.setValue(uuid);

    switch (type) {
    case TCPIP:
      getTcp().writeToXML(xml.tcpip.append());
      break;
      
    case COMMS:
      getCommsDev().writeToXML(xml.commsDevice.append());
      break;
      
    case SERIAL:
      getSerial().writeToXML(xml.serial.append());
      break;
      
      default:
        log.error("Unsupported type:"+type);
      
    }
  }

  public static enum ScadaTemplateType {
    TCPIP,
    COMMS,
    SERIAL,
  }

  @Override
  public String getUUID() {
    return uuid;
  }

  @Override
  public ICustomTemplateManager<?> getManager() {
    return manager;
  }
  
}

