/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.fpi;

import java.util.Collection;

import com.g3schema.ns_template.TemplateT;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateOwner;


public class FPIChannelTemplates extends ItemListManager<FPIChannelTemplate> implements ICustomTemplateManager<FPIChannelTemplate> {

  private final ICustomTemplateOwner owner;
  
  public FPIChannelTemplates(ICustomTemplateOwner owner) {
    this.owner = owner;
  }

  public void readFromXML(TemplateT xml) {
    int count = xml.fpiTemplate.count();
    for (int i = 0; i < count; i++) {
      FPIChannelTemplate templ = new FPIChannelTemplate(this);
      templ.readFromXML(xml.fpiTemplate.at(i));
      add(templ);
    }
    
  }

  public void writeToXML(TemplateT xml) {
    Collection<FPIChannelTemplate> all = getAllItems();
    for (FPIChannelTemplate templ : all) {
      templ.writeToXML(xml.fpiTemplate.append());
    }
  
  }

  @Override
  public ICustomTemplateOwner getOwner() {
    return owner;
  }
}

