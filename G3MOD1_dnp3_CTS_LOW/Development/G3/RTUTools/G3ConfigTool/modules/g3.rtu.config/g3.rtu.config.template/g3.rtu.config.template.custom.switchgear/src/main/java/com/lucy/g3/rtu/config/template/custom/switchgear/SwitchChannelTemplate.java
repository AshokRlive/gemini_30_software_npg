/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.switchgear;

import com.g3schema.ns_template.ParameterT;
import com.g3schema.ns_template.SwitchChannelTemplateT;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;


/**
 * The template of a switch channel.
 */
public class SwitchChannelTemplate extends Model{
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PROPERTY_PULSELENGTH = "pulseLength";
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PROPERTY_OVERRUN = "overrun";
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PROPERTY_MOTOR_OVER_DRIVE = "motoOverDrive";
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PROPERTY_MOTOR_REVERSE_DRIVE = "motoReverseDrive";
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PROPERTY_OPERATIONTIMEOUT = "operationTimeout";

  /**
   * The name of property {@value} . <li>Value type: {@link Polarity}.</li>
   */
  public static final String PROPERTY_POLARITY = "polarity";
  

  
  private Polarity polarity = Polarity.SWITCH_POLARITY_DOWN;
  private long pulseLength = 1000;//ms
  private long overrun = 3000;//ms
  private long operationTimeout = 30;//ms
  private long motoOverDrive = 0;//ms
  private long motoReverseDrive = 0;//ms


  public Polarity getPolarity() {
    return polarity;
  }

  public void setPolarity(Polarity polarity) {
    Object oldValue = this.polarity;
    this.polarity = polarity;
    firePropertyChange(PROPERTY_POLARITY, oldValue, polarity);
  }

  public long getPulseLength() {
    return pulseLength;
  }

  public void setPulseLength(long pulseLength) {
    Object oldValue = this.pulseLength;
    this.pulseLength = pulseLength;
    firePropertyChange(PROPERTY_PULSELENGTH, oldValue, pulseLength);
  }

  public long getOverrun() {
    return overrun;
  }

  public void setOverrun(long overrun) {
    Object oldValue = this.overrun;
    this.overrun = overrun;
    firePropertyChange(PROPERTY_OVERRUN, oldValue, overrun);
  }

  public long getOperationTimeout() {
    return operationTimeout;
  }

  public void setOperationTimeout(long operationTimeout) {
    Object oldValue = this.operationTimeout;
    this.operationTimeout = operationTimeout;
    firePropertyChange(PROPERTY_OPERATIONTIMEOUT, oldValue, operationTimeout);
  }

  public void writeToXML(SwitchChannelTemplateT xml) {
    xml.polarity.setValue(polarity.getValue());
    xml.pulseLength.setValue(pulseLength);
    xml.overrunMS.setValue(overrun);
    xml.opTimeoutS.setValue(operationTimeout);
    
    // Motor over drive
    ParameterT xmlPara = xml.parameter.append();
    xmlPara.name.setValue(PROPERTY_MOTOR_OVER_DRIVE);
    xmlPara.value2.setValue(String.valueOf(getMotoOverDrive()));
    
    // Motor reverse drive
    xmlPara = xml.parameter.append();
    xmlPara.name.setValue(PROPERTY_MOTOR_REVERSE_DRIVE);
    xmlPara.value2.setValue(String.valueOf(getMotoReverseDrive()));
  }

  public void readFromXML(SwitchChannelTemplateT xml) {
    polarity = Polarity.forValue((int) xml.polarity.getValue());
    pulseLength = xml.pulseLength.getValue();
    overrun = xml.overrunMS.getValue();
    operationTimeout = xml.opTimeoutS.getValue();
    
    for (int i = 0; i < xml.parameter.count(); i++) {
      ParameterT xmlPara = xml.parameter.at(i);
      
      if(PROPERTY_MOTOR_OVER_DRIVE.equals(xmlPara.name.getValue()))
          setMotoOverDrive(Long.valueOf(xmlPara.value2.getValue()));
      else if(PROPERTY_MOTOR_REVERSE_DRIVE.equals(xmlPara.name.getValue()))
          setMotoReverseDrive(Long.valueOf(xmlPara.value2.getValue()));
    }
  }
  
  
  public long getMotoOverDrive() {
    return motoOverDrive;
  }

  
  public void setMotoOverDrive(long motoOverDrive) {
    if(motoOverDrive < 0)
      return;
    
    Object oldValue = this.motoOverDrive;
    this.motoOverDrive = motoOverDrive;
    firePropertyChange(PROPERTY_MOTOR_OVER_DRIVE, oldValue, motoOverDrive);
  }

  
  public long getMotoReverseDrive() {
    return motoReverseDrive;
  }

  
  public void setMotoReverseDrive(long motoReverseDrive) {
    if(motoReverseDrive < 0)
      return;
    
    Object oldValue = this.motoReverseDrive;
    this.motoReverseDrive = motoReverseDrive;
    firePropertyChange(PROPERTY_MOTOR_REVERSE_DRIVE, oldValue, motoReverseDrive);
  }

  @Override
  public String toString() {
    return "Switch Output Channel Parameters";
  }
  
}

