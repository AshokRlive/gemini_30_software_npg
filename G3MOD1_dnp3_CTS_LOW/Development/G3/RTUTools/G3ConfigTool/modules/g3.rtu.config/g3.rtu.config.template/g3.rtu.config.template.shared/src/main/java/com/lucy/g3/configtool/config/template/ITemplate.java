/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.config.template;



public interface ITemplate {

  /**
   * Gets the name of the template.
   * @return
   */
  String getName();
  
  /**
   * Gets the text of this template for displaying on GUI.
   * @return
   */
  String getDisplayText();

  /**
   * Applies this template to an exiting target object.
   * @param target an exiting target object which is applicable by the template.
   * @param overwrite if true, overwrite the content of the existing object, otherwise update the content as must as it can.
   */
  void applyTo(Object target, boolean overwrite);

}

