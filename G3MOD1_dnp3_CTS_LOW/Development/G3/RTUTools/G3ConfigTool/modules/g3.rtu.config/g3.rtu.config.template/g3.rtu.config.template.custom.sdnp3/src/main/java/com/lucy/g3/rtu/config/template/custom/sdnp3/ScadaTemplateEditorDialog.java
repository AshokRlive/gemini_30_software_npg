/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.*;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.beans.editor.ComboBoxPropertyEditor;
import com.l2fprod.common.propertysheet.PropertyEditorRegistry;
import com.l2fprod.common.propertysheet.PropertySheet;
import com.l2fprod.common.propertysheet.PropertySheetPanel;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.PropertyEditorDialog;
import com.lucy.g3.gui.common.widgets.propertysheet.PropertyEditorBinder;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate.ScadaTemplateType;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateOwner;
import com.lucy.g3.rtu.config.template.custom.shared.ui.TemplatesManagerPanel;
import com.lucy.g3.rtu.config.template.custom.shared.ui.TemplatesManagerPanel.ItemListManagerPanelInvoker;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;

/**
 * Editor dialog of Switch Template.
 */
class ScadaTemplateEditorDialog extends AbstractDialog {

  private final ScadaTemplate template;
  private PropertyEditorBinder binder;


  public ScadaTemplateEditorDialog(Window parent, ScadaTemplate template) {
    super(parent);
    this.template = Preconditions.checkNotNull(template, "template must not be null");
    setTitle("SCADA Template Editor");
    setName("SCADATemplateEditor");
    
    initComponents();
    load();
    updateVisibility();
    pack();
  }

  
  private void updateVisibility() {
    CardLayout cl = (CardLayout)(panelCard.getLayout());
    
    String cardName = null;
    if(radioBtnGPRS.isSelected())
        cardName = "cardGPRS";
    else if(radioBtnPak.isSelected())
        cardName = "cardPAKNET";
    else if(radioBtnPSTN.isSelected())
        cardName = "cardPSTN";
    else if(radioBtnTcp.isSelected())
        cardName = "cardTCP";
    else if(radioBtnSerial.isSelected())
      cardName = "cardSerial";
    
    cl.show(panelCard, cardName);
  }
  
  @Override
  public void dispose() {
    save();
    super.dispose();
  }


  private void save() {
    if(binder != null) {
      binder.unbind();
    }
    
    template.setProfileName(tfProfileName.getText());
    template.setClassifier(tfClassifier.getText());
    
    if (radioBtnTcp.isSelected()) {
      template.setType(ScadaTemplateType.TCPIP);
      
    } else if(radioBtnSerial.isSelected()) { 
      template.setType(ScadaTemplateType.SERIAL);
      template.getSerial().setPort((SERIAL_PORT) combSerialPort.getSelectedItem());
      
    } else {
      template.setType(ScadaTemplateType.COMMS);
      
      CommsDeviceType ctype = null;
      if(radioBtnGPRS.isSelected())
        ctype = CommsDeviceType.GPRS_MODEM; 
      else if(radioBtnPak.isSelected())
        ctype = CommsDeviceType.PAKNET_MODEM; 
      else if(radioBtnPSTN.isSelected())
        ctype = CommsDeviceType.PSTN_MODEM;
      
      template.getCommsDev().setType(ctype);
    }
  }

  private void load() {
    tfProfileName.setText(template.getProfileName());
    tfClassifier.setText(template.getClassifier());
    
    ScadaTemplateType t = template.getType();
    if (t == ScadaTemplateType.TCPIP) {
      radioBtnTcp.setSelected(true);
    } else if (t == ScadaTemplateType.COMMS) {
      CommsDeviceType ctype = template.getCommsDev().getType();
      if (ctype != null)
        switch (ctype) {
        case GPRS_MODEM:
          radioBtnGPRS.setSelected(true);
          break;
        case PAKNET_MODEM:
          radioBtnPak.setSelected(true);
          break;
        case PSTN_MODEM:
          radioBtnPSTN.setSelected(true);
          break;
        default:
          break;
        }
      
    } else if(t == ScadaTemplateType.SERIAL) {
      radioBtnSerial.setSelected(true);
      combSerialPort.setSelectedItem(template.getSerial().getPort());
    } 
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createCloseButtonPanel();
  }

  private void createUIComponents() {
    combSerialPort = new JComboBox<>(getAvailablePorts().toArray());
    panelScadaAddress= new TemplatesManagerPanel<>(template.getTcp().getItemListModel(), 
        new ItemListManagerPanelInvoker<TcpScadaAddress>(){
          @Override
          public void showEditor(Window parent, TcpScadaAddress selection) {
            DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout("default,$lcgap,fill:default:grow",""));
            JTextField ipField = new JTextField(selection.getIp());
            JTextField masterAddressField = new JTextField(selection.getMasterAddress());
            builder.append("SCADA IP Address:", ipField);
            builder.append("SCADA Master Address:", masterAddressField);
            
            int result = JOptionPane.showConfirmDialog(parent, builder.getPanel(),
                "Edit Master Address", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if(result == JOptionPane.OK_OPTION) {
              selection.setIp(ipField.getText());
              selection.setMasterAddress(masterAddressField.getText());
            }
          }

          @Override
          public TcpScadaAddress createNew() {
            return new TcpScadaAddress();
          }
      
    });
    panelPakConfig = createSerialPropertyPanel(template.getCommsDev(),CommsDeviceType.PAKNET_MODEM);
    panelGPRSConfig = createSerialPropertyPanel(template.getCommsDev(),CommsDeviceType.GPRS_MODEM);
    panelPSTNConfig = createSerialPropertyPanel(template.getCommsDev(),CommsDeviceType.PSTN_MODEM);
  }


  private ArrayList<SERIAL_PORT> getAvailablePorts() {
    ICustomTemplateOwner owner = template.getManager().getOwner();
    IConfig conf = owner == null? null: owner.getOwner();
    PortsManager portsMgr = (PortsManager) (conf== null ? null : conf.getConfigModule(PortsManager.CONFIG_MODULE_ID));
    Collection<ISerialPort> ports = portsMgr == null ? null : portsMgr.getSerialManager().getSerialPorts();
    ArrayList<SERIAL_PORT> portNames = new ArrayList<>();
    if (ports != null) {
      for (ISerialPort p : ports) {
        portNames.add(p.getPortEnum());
      }
    }
    return portNames;
  }

  private PropertySheetPanel createSerialPropertyPanel(ScadaTemplateCommsDev serial, CommsDeviceType type) {
    PropertySheetPanel panel = new PropertySheetPanel();
    
    // Configure property panel
    PropertyEditorDialog.configurePropertyPanel(panel);
    panel.setToolBarVisible(false);
    panel.setMode(PropertySheet.VIEW_AS_FLAT_LIST);
    panel.setPreferredSize(new Dimension(300,100));
    
    // Set property editor
    PropertyEditorRegistry factory = (PropertyEditorRegistry) panel.getTable().getEditorFactory();
    factory.registerEditor(ICommsDevice.class, new CommsDeviceEditor(findCommsDeviceManager(serial), type));
    
    binder = new PropertyEditorBinder(template.getCommsDev().getDevByType(type), panel);
    binder.readFromObject();
  
    // Toggle sub properties
    try{
      panel.getTable().getSheetModel().getPropertySheetElement(0).toggle();
      panel.getTable().getSheetModel().getPropertySheetElement(2).toggle();
    }catch (Throwable e) {
      Logger.getLogger(getClass()).error("failed to toggle property sheet");
    }
    
    
    return panel;
  }

  private CommsDeviceManager findCommsDeviceManager(ScadaTemplateCommsDev serial) {
    IConfig conf = serial.getManager().getOwner().getOwner();
    return (CommsDeviceManager) (conf == null ? null : conf.getConfigModule(CommsDeviceManager.CONFIG_MODULE_ID));
  }


  private void radioBtnActionPerformed(ActionEvent e) {
    updateVisibility();
  }
  
  private static class CommsDeviceEditor extends ComboBoxPropertyEditor{
    private final CommsDeviceManager manager;
    private final CommsDeviceType type;

    public CommsDeviceEditor(CommsDeviceManager manager, CommsDeviceType type) {
      super();
      this.manager = manager;
      this.type = type;
    }

    @Override
    public Component getCustomEditor() {
      if(manager != null)
        setAvailableValues(getAllDevices().toArray());
      return super.getCustomEditor();
    }

    private Collection<ICommsDevice> getAllDevices() {
      Collection<ICommsDevice> allDevices = manager.getAllItems();
      ArrayList<ICommsDevice>  devicesInType = new ArrayList<>();
      for (ICommsDevice dev : allDevices) {
        if(type == dev.getDeviceType())
          devicesInType.add(dev);
      }
      
      return devicesInType;
    }
  }


  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    panel2 = new JPanel();
    label1 = new JLabel();
    tfProfileName = new JTextField();
    label3 = new JLabel();
    tfClassifier = new JTextField();
    panel1 = new JPanel();
    radioBtnTcp = new JRadioButton();
    radioBtnSerial = new JRadioButton();
    radioBtnPak = new JRadioButton();
    radioBtnGPRS = new JRadioButton();
    radioBtnPSTN = new JRadioButton();
    separator = new JXTitledSeparator();
    panelCard = new JPanel();
    cardPAKNET = new JPanel();
    cardTCP = new JPanel();
    cardGPRS = new JPanel();
    cardPSTN = new JPanel();
    cardSerial = new JPanel();
    label2 = new JLabel();

    //======== this ========
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
        "default, $lcgap, default:grow",
        "default, $lgap, default, $pgap, default, $lgap, fill:default:grow"));

      //======== panel2 ========
      {
        panel2.setLayout(new FormLayout(
          "default, $lcgap, [80dlu,default]:grow, $ugap, default, $lcgap, [50dlu,default]",
          "default"));

        //---- label1 ----
        label1.setText("Profile Name:");
        panel2.add(label1, CC.xy(1, 1));
        panel2.add(tfProfileName, CC.xy(3, 1));

        //---- label3 ----
        label3.setText("Classifier:");
        panel2.add(label3, CC.xy(5, 1));
        panel2.add(tfClassifier, CC.xy(7, 1));
      }
      contentPanel.add(panel2, CC.xy(1, 1));

      //======== panel1 ========
      {
        panel1.setLayout(new FormLayout(
          "default, $lcgap, default, $rgap, 2*(default, $lcgap), default",
          "default"));

        //---- radioBtnTcp ----
        radioBtnTcp.setText("TCP/IP");
        radioBtnTcp.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            radioBtnActionPerformed(e);
          }
        });
        panel1.add(radioBtnTcp, CC.xy(1, 1));

        //---- radioBtnSerial ----
        radioBtnSerial.setText("Serial");
        radioBtnSerial.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            radioBtnActionPerformed(e);
          }
        });
        panel1.add(radioBtnSerial, CC.xy(3, 1));

        //---- radioBtnPak ----
        radioBtnPak.setText("PAKNET");
        radioBtnPak.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            radioBtnActionPerformed(e);
          }
        });
        panel1.add(radioBtnPak, CC.xy(5, 1));

        //---- radioBtnGPRS ----
        radioBtnGPRS.setText("GPRS Modem");
        radioBtnGPRS.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            radioBtnActionPerformed(e);
          }
        });
        panel1.add(radioBtnGPRS, CC.xy(7, 1));

        //---- radioBtnPSTN ----
        radioBtnPSTN.setText("PSTN Modem");
        radioBtnPSTN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            radioBtnActionPerformed(e);
          }
        });
        panel1.add(radioBtnPSTN, CC.xy(9, 1));
      }
      contentPanel.add(panel1, CC.xywh(1, 3, 3, 1));

      //---- separator ----
      separator.setTitle("Configuration");
      contentPanel.add(separator, CC.xywh(1, 5, 3, 1));

      //======== panelCard ========
      {
        panelCard.setLayout(new CardLayout());

        //======== cardPAKNET ========
        {
          cardPAKNET.setLayout(new BorderLayout());
          cardPAKNET.add(panelPakConfig, BorderLayout.CENTER);
        }
        panelCard.add(cardPAKNET, "cardPAKNET");

        //======== cardTCP ========
        {
          cardTCP.setLayout(new BorderLayout());

          //---- panelScadaAddress ----
          panelScadaAddress.setBorder(null);
          cardTCP.add(panelScadaAddress, BorderLayout.CENTER);
        }
        panelCard.add(cardTCP, "cardTCP");

        //======== cardGPRS ========
        {
          cardGPRS.setLayout(new BorderLayout());
          cardGPRS.add(panelGPRSConfig, BorderLayout.CENTER);
        }
        panelCard.add(cardGPRS, "cardGPRS");

        //======== cardPSTN ========
        {
          cardPSTN.setLayout(new BorderLayout());
          cardPSTN.add(panelPSTNConfig, BorderLayout.CENTER);
        }
        panelCard.add(cardPSTN, "cardPSTN");

        //======== cardSerial ========
        {
          cardSerial.setLayout(new FormLayout(
            "2*(default, $lcgap), [80dlu,default]",
            "default"));

          //---- label2 ----
          label2.setText("Serial Port:");
          cardSerial.add(label2, CC.xy(3, 1));
          cardSerial.add(combSerialPort, CC.xy(5, 1));
        }
        panelCard.add(cardSerial, "cardSerial");
      }
      contentPanel.add(panelCard, CC.xywh(1, 7, 3, 1));
    }
    contentPane.add(contentPanel, BorderLayout.CENTER);

    //---- buttonGroup ----
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(radioBtnTcp);
    buttonGroup.add(radioBtnSerial);
    buttonGroup.add(radioBtnPak);
    buttonGroup.add(radioBtnGPRS);
    buttonGroup.add(radioBtnPSTN);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JPanel panel2;
  private JLabel label1;
  private JTextField tfProfileName;
  private JLabel label3;
  private JTextField tfClassifier;
  private JPanel panel1;
  private JRadioButton radioBtnTcp;
  private JRadioButton radioBtnSerial;
  private JRadioButton radioBtnPak;
  private JRadioButton radioBtnGPRS;
  private JRadioButton radioBtnPSTN;
  private JXTitledSeparator separator;
  private JPanel panelCard;
  private JPanel cardPAKNET;
  private JPanel panelPakConfig;
  private JPanel cardTCP;
  private TemplatesManagerPanel<TcpScadaAddress> panelScadaAddress;
  private JPanel cardGPRS;
  private JPanel panelGPRSConfig;
  private JPanel cardPSTN;
  private JPanel panelPSTNConfig;
  private JPanel cardSerial;
  private JLabel label2;
  private JComboBox combSerialPort;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
