/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.builtin.modbus;

import java.beans.PropertyVetoException;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.configtool.config.template.ITemplate;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.protocol.master.shared.domain.IMasterProtocolModule;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoMap;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;


/**
 * Built-in Modbus Master session template.
 */
public class MMBTemplate extends MMBSession implements ITemplate{
  private static Logger log = Logger.getLogger(MMBTemplate.class);
  
  protected static final String DISPLAY_FORMAT="[%s] %s";
  
  /**
   * Constructs a built-in MMB template.
   * @param channel
   * @param sessionName
   */
  protected MMBTemplate(MMBChannel channel, String sessionName) {
    super(channel, sessionName, true);
  }
  
  /**
   * Constructs a built-in MMB template from a predefined device.
   * @param device non-null device enum.
   */
  protected  MMBTemplate(MMBChannel channel, PredefinedModbusDevices device){ 
    this(channel, device.getDeviceModel());
    setDeviceModel(device.getDeviceModel());
    setByteswap(device.isByteswap());
    setWordswap(device.isWordswap());
    
    MMBIoMap ioMap = getIomap();
    MMBIoTemplate[] chnls = device.getChannels();
    IMasterProtocolModule dev = getDevice();
    
    
    int id;
    for (int i = 0; i < chnls.length; i++) {
      id = ioMap.getChannelNum(chnls[i].getType());
      
      if(ioMap.addChannel(chnls[i].createChannel(dev, id)) == false)
        log.error("Failed to add channel:" + i);
    }
  }
  

  @Override
  public String toString() {
    return getDisplayText();
  }
  
  @Override
  public String getDisplayText() {
    String name = getName();
    name = Strings.isBlank(name) ? " Unknown" : name;
    return String.format(DISPLAY_FORMAT, "Built-in", name);
  }

  
  @Override
  public final String getName() {
    return getDeviceModel();
  }

  /**
   * Applies session template by copying session settings and IoMap from template to target. 
   * 
   * @param target must be an object of MMBSession.
   * @param overwrite
   *          if true, new channel will be created if it is not found,
   *          otherwise, it will only update existing channels.
   */
  @Override
  public void applyTo(Object target, boolean overwrite) {
    if(target != null && target instanceof MMBSession) {
      applyTemplate((MMBSession)target, this, overwrite);
    }else{
      log.error("Cannot apply template cause illegal argument: "+ target
          +". Expected class: " + MMBSession.class);
    }
  }

  /**
   * Applies session template by copying session settings and IoMap from template to target.
   * 
   * @param overwrite
   *          if true, new channel will be created if it is not found,
   *          otherwise, it will only update existing channels.
   */
  public static void applyTemplate(MMBSession target, MMBTemplate template, boolean overwrite) {
    Preconditions.checkNotNull(target, "target must not be null");
    Preconditions.checkNotNull(template, "template must not be null");
  
    copySession(target, template);
    copyIoMap(template.getIomap(), target.getIomap(), overwrite);
  }

  private static void copyIoMap(MMBIoMap templateIoMap, MMBIoMap targetIoMap, boolean overwrite) {
    ChannelType[] types = targetIoMap.getAvailableChannelTypes();
    for (int i = 0; i < types.length; i++) {
      MMBIoChannel[] targetChs = targetIoMap.getChannels(types[i]);
      MMBIoChannel[] templateChs = templateIoMap.getChannels(types[i]);
  
      if (overwrite) {
        for (int j = 0; j < templateChs.length; j++) {
          if (j < targetChs.length) {
            targetChs[j].setDescription(templateChs[j].getDescription());
            copyChannel(targetChs[j], templateChs[j]);
          } else {
            // Create new channel
            MMBIoChannel newCh = targetIoMap.addChannel(types[i], templateChs[j].getDescription());
            copyChannel(newCh, templateChs[j]);
          }
        }
  
      } else {
        // Update existing channels content
        if (targetChs.length != templateChs.length) {
          log.warn(String.format("IO Map may be updated particially "
              + "cause channel size in template and existings session is different! "
              + "session:%s, channel type:%s, template:%s",
              targetIoMap.getSession().getSessionName(),
              types[i].getDescription(),
              templateIoMap.getSession()));
          
          for (int j = 0; j < templateChs.length && j < targetChs.length; j++) {
            copyChannel(targetChs[j], templateChs[j]);
          }
        }
      }
    }
  }

  /**
   * Copies session. Slave address and device description are preserved.
   */
  private static void copySession(MMBSession target, MMBTemplate template) {
    // Device Model
    target.setDeviceModel(template.getDeviceModel());
    // Byte Swap
    target.setByteswap(template.isByteswap());
    // Word Swap
    target.setWordswap(template.isWordswap());
  
    // Default Response Timeout
    try {
      target.setDefRespTimeout(template.getDefRespTimeout());
    } catch (PropertyVetoException e) {
      log.error(e);
    }
  
    // Offline Poll Period
    try {
      target.setOfflinePollPeriod(template.getOfflinePollPeriod());
    } catch (PropertyVetoException e) {
      log.error(e);
    }
  
    // Number Of Retries
    try {
      target.setNumberOfRetries(template.getNumberOfRetries());
    } catch (PropertyVetoException e) {
      log.error(e);
    }
  }

  /*
   * Copy channel parameters from template to target. Channel description is preserved.
   */
  private static void copyChannel(MMBIoChannel target, MMBIoChannel template) {
    if (target == null || template == null) {
      log.error("Failed to aplly channel template: target or template Modbus IO Channel is null!");
      return;
    }
  
    if (target.getId() != template.getId()) {
      log.error("Failed to apply channel template: mismatched channel id!");
      return;
    }
  
    String[] params = target.getParameterNames();
  
    for (int i = 0; i < params.length; i++) {
      try {
        target.setParameter(params[i], template.getParameter(params[i]));
      } catch (Throwable e) {
        log.fatal("Failed to apply channel parameter from template", e);
      }
    }
  }
}

