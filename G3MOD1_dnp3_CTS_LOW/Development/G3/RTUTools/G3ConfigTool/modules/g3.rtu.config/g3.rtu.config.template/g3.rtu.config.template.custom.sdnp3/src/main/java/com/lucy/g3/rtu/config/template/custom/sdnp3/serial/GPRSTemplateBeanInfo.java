/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3.serial;

import com.l2fprod.common.beans.ExtendedPropertyDescriptor;
import com.lucy.g3.rtu.config.shared.base.bean.BeanInfo;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaAddress;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateCommsDev.CommsDeviceSelection;

/**
 * BeanInfo class required by Switch Template Property Editor.
 */
public class GPRSTemplateBeanInfo extends BeanInfo {

  public GPRSTemplateBeanInfo() {
    super(GPRSTemplate.class);
    
    ExtendedPropertyDescriptor address = addProperty(GPRSTemplate.PROPERTY_SCADA_ADDRESS);
    addSubProperty(address, ScadaAddress.PROPERTY_MASTER_ADDRESS);
    
    ExtendedPropertyDescriptor commsDeviceSel = addProperty(GPRSTemplate.PROPERTY_COMMS_DEVICE_SElECTION);
    addSubProperty(commsDeviceSel, CommsDeviceSelection.PROPERTY_COMMS_DEVICE);
    addSubProperty(commsDeviceSel, CommsDeviceSelection.PROPERTY_CONNECTION_CLASS);
    
    
    addProperty(GPRSTemplate.PROPERTY_HOST);
    addProperty(GPRSTemplate.PROPERTY_PASS);
    addProperty(GPRSTemplate.PROPERTY_USER);
  }
    
}