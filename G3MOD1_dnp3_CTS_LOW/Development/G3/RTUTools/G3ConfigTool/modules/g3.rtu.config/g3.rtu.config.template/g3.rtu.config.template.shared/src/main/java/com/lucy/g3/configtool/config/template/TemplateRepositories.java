/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.config.template;

import java.util.HashMap;

import com.lucy.g3.rtu.config.shared.manager.IConfig;



/**
 * The Class IOMapGenerators.
 */
public class TemplateRepositories {
  public static final int KEY_MMB_BUILTIN = 0;
  public static final int KEY_MMB_CUSTOM = 1;
  
  private TemplateRepositories() {}
  private static final HashMap<Integer, ITemplateRepository> factories = new HashMap<>();
  
  public static void register(int type, ITemplateRepository factory) {
    factories.put(type, factory);
  }
  
  public static ITemplateRepository get(int key) {
    return factories.get(key);
  }
  
  public static ITemplate[] getAvailabelTemplates(IConfig config, int key) {
    ITemplateRepository repo = TemplateRepositories.get(key);
    return repo == null ? new ITemplate[0] : repo.getTemplates(config);
  }
}

