/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.builtin.modbus;

import java.util.ArrayList;

import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * Built-in Modbus session templates manager.
 */
public class MMBTemplates extends AbstractConfigModule {

  public static final String CONFIG_MODULE_ID = "mmbTemplates";

  private ArrayList<MMBTemplate> templateList;
  
  private final MMBChannel dummyMMBChannel = MMBChannel.createDummy();

  public MMBTemplates(IConfig owner) {
    super(owner);
  }

  private ArrayList<MMBTemplate> createBuiltinTemplates() {
    ArrayList<MMBTemplate> templates = new ArrayList<>();
    PredefinedModbusDevices[] devs = PredefinedModbusDevices .values();
    for (int i = 0; i < devs.length; i++) {
      MMBTemplate templ = new MMBTemplate(dummyMMBChannel, devs[i]);
      templates.add(templ);
      dummyMMBChannel.add(templ);
    }
    return templates;
  }

  public MMBTemplate[] getAll() {
    if(templateList == null)
      templateList = createBuiltinTemplates();
    
    return templateList.toArray(new MMBTemplate[templateList.size()]);
  }
}
