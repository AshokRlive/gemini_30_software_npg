/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.switchgear;

import java.util.UUID;

import com.g3schema.ns_template.ParameterT;
import com.g3schema.ns_template.SwitchTemplateParamT;
import com.g3schema.ns_template.SwitchTemplateT;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogicSettings;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ISupportCache;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.MOTOR_MODE;

/**
 * This bean contains the template configuration for Switch module & logic.
 */
public class SwitchTemplate extends SwitchGearLogicSettings implements ISupportCache, ICustomTemplate{


  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_SWITCHNAME = "switchName";
  
  public static final String PROPERTY_ALLOW_FORCED_OPERATION = "allowForcedOperation";
  public static final String PROPERTY_MOTOR_MODE = "motorMode";
  
  
  public static final String PROPERTY_OPEN  = "open";
  public static final String PROPERTY_CLOSE = "close";

  private String switchName = "Unnamed";
  
  private final SwitchChannelTemplate open = new SwitchChannelTemplate();
  private final SwitchChannelTemplate close = new SwitchChannelTemplate();

  private MOTOR_MODE motorMode = MOTOR_MODE.MOTOR_MODE_1;
  private boolean allowForcedOperation;
  
  private String uuid = UUID.randomUUID().toString();
  private final ICustomTemplateManager<?> manager;

  public SwitchTemplate(ICustomTemplateManager<?> manager, String switchName) {
    super();
    this.manager = manager;
    setSwitchName(switchName);
  }
  
  public MOTOR_MODE getMotorMode() {
    return motorMode;
  }

  
  public void setMotorMode(MOTOR_MODE motorMode) {
    if(motorMode != null) {
    Object oldValue = this.motorMode;
    this.motorMode = motorMode;
    firePropertyChange(PROPERTY_MOTOR_MODE, oldValue, motorMode);
    }
  }
  
  public String getSwitchName() {
    return switchName;
  }

  public void setSwitchName(String switchName) {
    if (Strings.isBlank(switchName)) {
      return;
    }

    Object oldValue = this.switchName;
    this.switchName = switchName;
    firePropertyChange(PROPERTY_SWITCHNAME, oldValue, switchName);
  }

  public boolean isAllowForcedOperation() {
    return allowForcedOperation;
  }

  public void setAllowForcedOperation(boolean allowForcedOperation) {
    Object oldValue = this.allowForcedOperation;
    this.allowForcedOperation = allowForcedOperation;
    firePropertyChange(PROPERTY_ALLOW_FORCED_OPERATION, oldValue, allowForcedOperation);
  }

  public SwitchChannelTemplate getOpen() {
    return open;
  }
  
  public SwitchChannelTemplate getClose() {
    return close;
  }
  
  public void setOpen(SwitchChannelTemplate open) {
    throw new UnsupportedOperationException("Read only property");
  }
  
  public void setClose(SwitchChannelTemplate close) {
    throw new UnsupportedOperationException("Read only property");
  }

  @Override
  public String toString() {
    return getSwitchName();
  }

  public void writeToXML(SwitchTemplateT xml) {
    xml.switchName.setValue(switchName);
    xml.allowForcedOperation.setValue(allowForcedOperation);
    super.writeParamToXML(xml);

    open.writeToXML(xml.open.append());
    close.writeToXML(xml.close.append());
  
    xml.uuid.setValue(uuid);
    
    SwitchTemplateParamT xmlParams = xml.parameters.append();
    ParameterT xmlParam = xmlParams.parameter.append();
    xmlParam.name.setValue(PROPERTY_MOTOR_MODE);
    xmlParam.value2.setValue(getMotorMode().name());
  }

  public void readFromXML(SwitchTemplateT xml) {
    super.readParamFromXML(xml);

    if(xml.open.exists())
      open.readFromXML(xml.open.first());
    
    if(xml.close.exists())
      close.readFromXML(xml.close.first());
    
    switchName = xml.switchName.getValue();
    
    if(xml.allowForcedOperation.exists())
      allowForcedOperation = xml.allowForcedOperation.getValue();
    
    if (xml.uuid.exists())
      uuid = xml.uuid.getValue();
    
    if(xml.parameters.exists()) {
      SwitchTemplateParamT xmlParams = xml.parameters.first();
      
      int count = xmlParams.parameter.count();
      
      for (int i = 0; i < count; i++) {
        ParameterT xmlParam = xmlParams.parameter.at(i);
        String motorMode = null;
        try {
        if(PROPERTY_MOTOR_MODE.equals(xmlParam.name.getValue())) {
            motorMode = xmlParam.value2.getValue();
           setMotorMode(MOTOR_MODE.valueOf(motorMode));
        }
        }catch(Throwable e){
          log.error("Invalid motor mode:" + motorMode);
        }
      }
    }
  }

  @Override
  public String getUUID() {
    return uuid;
  }

  @Override
  public ICustomTemplateManager getManager() {
    return manager;
  }
  
  @Override
  public IValidator getValidator() {
    return null;
  }
  
}
