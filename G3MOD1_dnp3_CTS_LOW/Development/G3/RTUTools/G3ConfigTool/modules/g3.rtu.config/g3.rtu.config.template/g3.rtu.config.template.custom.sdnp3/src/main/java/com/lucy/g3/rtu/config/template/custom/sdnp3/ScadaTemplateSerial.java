/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import com.g3schema.ns_pstack.SerialConfT;
import com.jgoodies.binding.beans.Model;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;


/**
 *
 */
public class ScadaTemplateSerial extends Model {
  final public static String PROPERTY_PORT = "port";
  
  private SERIAL_PORT port;
  
  private final ScadaAddress scadaAddress = new ScadaAddress(); 

  
  public SERIAL_PORT getPort() {
    return port;
  }

  
  public ScadaAddress getScadaAddress() {
    return scadaAddress;
  }


  public void setPort(SERIAL_PORT port) {
    Object oldValue = this.port;
    this.port = port;
    firePropertyChange(PROPERTY_PORT, oldValue, port);
  }


  public void writeToXML(SerialConfT xml) {
    xml.serialPortName.setValue(getPort() == null ? "" : getPort().name());
  }


  public void readFromXML(SerialConfT xml) {
    String portName = xml.serialPortName.getValue();
    if(portName.isEmpty())
      setPort(null);
    else
      setPort(SERIAL_PORT.valueOf(portName));
  }
  
  

}

