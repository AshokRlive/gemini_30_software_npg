/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.shared.ui;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.common.collect.ObservableList;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionSingleSelection;

public class TemplatesManagerPanel<ItemT> extends JPanel {

  private Logger log = Logger.getLogger(TemplatesManagerPanel.class);

  private final MultiSelectionInList<ItemT> selectionList;
  private final MultiListSelectionAdapter<ItemT> selectionModel;
  private final MultiSelectionSingleSelection singleSelection;

  private final ItemListManagerPanelInvoker<ItemT> invoker;
  
  /**
   * Required by JFromDesigner.
   */
  @SuppressWarnings("unused")
  private TemplatesManagerPanel() {
    this.selectionModel = null;
    this.selectionList = null;
    this.singleSelection = null;
    this.invoker = null;
    initComponents();
  }
  
  public TemplatesManagerPanel(ListModel<ItemT> listModel, ItemListManagerPanelInvoker<ItemT> invoker) {
    this.selectionList = new MultiSelectionInList<ItemT>(listModel);
    this.selectionModel = new MultiListSelectionAdapter<ItemT>(selectionList);
    this.singleSelection = new MultiSelectionSingleSelection(selectionList.getSelection());
    
    this.invoker = invoker;

    initComponents();
    initComponentsBinding();
  }

  void setAddButtonText(String addText) {
      btnAdd.setText(addText);
  }

  private void initComponentsBinding() {
//    Bindings.bind(list, selectionModel);
    list.setModel(selectionList.getList());
    list.setSelectionModel(selectionModel);;
    Bindings.addComponentPropertyHandler(list, singleSelection);

    /* Bind button enable state to selection */
    PropertyAdapter<MultiSelectionInList<ItemT>> hasSelctionVM 
      = new PropertyAdapter<>(selectionList, MultiSelectionInList.PROPERTY_SELECTION, true);
    PropertyConnector.connectAndUpdate(hasSelctionVM, btnEdit, "enabled");
    PropertyConnector.connectAndUpdate(hasSelctionVM, btnRemove, "enabled");
  }

  private void editSelection() {
    ItemT selection = (ItemT) singleSelection.getValue();
    
    if(selection == null){
      ObservableList<ItemT> selections = selectionList.getSelection();
      if(!selections.isEmpty())
        selection = selections.get(0);
    }
    
    if(selection != null) {
      invoker.showEditor(SwingUtilities.getWindowAncestor(this), selection);
      
      // Refresh the list after editing
      list.repaint();
      
    } else {
      log.error("Cannot edit cause there is no selected item!");
    }
  }

  private void controlPanelMouseClicked() {
    selectionModel.clearSelection();
  }

  private void btnEditActionPerformed() {
    editSelection();
  }

  private void btnAddActionPerformed() {
      ItemT newItem = invoker.createNew();
      
      if(newItem != null) {
        ArrayListModel<ItemT> itemlist = (ArrayListModel<ItemT>) selectionList.getList();
        itemlist.add(newItem);
        int index = itemlist.getSize()-1;
        
        // Select new created item
        //selectionList.getSelection().clear();
        selectionModel.setSelectionInterval(index, index);
        
        // Edit new created item
        editSelection();
      }
  }

  private void btnRemoveActionPerformed() {
    int rc = JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(this),
        "Do you want to remove the selection?",
        "Remove",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE);
    if (rc == JOptionPane.YES_OPTION) {
      ArrayListModel<ItemT> itemlist = (ArrayListModel<ItemT>) selectionList.getList();
      itemlist.removeAll(new ArrayList<ItemT>(selectionList.getSelection()));
    }
  }

  private void listTemplateMouseClicked(MouseEvent e) {
    if (e.getClickCount() >= 2) {
      editSelection();
    }
  }
  
  public interface ItemListManagerPanelInvoker<ItemT> {
    void showEditor(Window parent, ItemT selection);
    ItemT createNew();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scrollPane1 = new JScrollPane();
    list = new JList<>();
    controlPanel = new JPanel();
    btnAdd = new JButton();
    btnRemove = new JButton();
    btnEdit = new JButton();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
        "[120dlu,default]:grow, $lcgap, default",
        "fill:default:grow"));

    //======== scrollPane1 ========
    {

      //---- list ----
      list.setFixedCellHeight(30);
      list.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          listTemplateMouseClicked(e);
        }
      });
      scrollPane1.setViewportView(list);
    }
    add(scrollPane1, CC.xy(1, 1));

    //======== controlPanel ========
    {
      controlPanel.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          controlPanelMouseClicked();
        }
      });
      controlPanel.setLayout(new FormLayout(
          "[60dlu,default]",
          "2*(default, $lgap), default"));

      //---- btnAdd ----
      btnAdd.setText("Add");
      btnAdd.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnAddActionPerformed();
        }
      });
      controlPanel.add(btnAdd, CC.xy(1, 1));

      //---- btnRemove ----
      btnRemove.setText("Remove");
      btnRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnRemoveActionPerformed();
        }
      });
      controlPanel.add(btnRemove, CC.xy(1, 3));

      //---- btnEdit ----
      btnEdit.setText("Edit");
      btnEdit.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnEditActionPerformed();
        }
      });
      controlPanel.add(btnEdit, CC.xy(1, 5));
    }
    add(controlPanel, CC.xy(3, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JList<ItemT> list;
  private JPanel controlPanel;
  private JButton btnAdd;
  private JButton btnRemove;
  private JButton btnEdit;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
