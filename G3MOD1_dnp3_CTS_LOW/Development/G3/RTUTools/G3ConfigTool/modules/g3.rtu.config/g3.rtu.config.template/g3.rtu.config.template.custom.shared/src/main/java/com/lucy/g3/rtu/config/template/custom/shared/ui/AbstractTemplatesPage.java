/*******************************************************************************
 * ****************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.shared.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ui.TemplatesManagerPanel.ItemListManagerPanelInvoker;

public abstract class AbstractTemplatesPage<TemplateT extends ICustomTemplate, ManagerT extends ICustomTemplateManager<TemplateT>>
    extends AbstractConfigPage
    implements ItemListManagerPanelInvoker<TemplateT> {

  private Logger log = Logger.getLogger(AbstractTemplatesPage.class);
  
  private final ManagerT manager;
  private AbstractAction addAction;
  
  private TemplatesManagerPanel<TemplateT> contentPanel;
  
  private final String templateName;


  public AbstractTemplatesPage(ManagerT _manager, String templateName) {
    super(_manager);
    this.templateName = Preconditions.checkNotBlank(templateName, "templateName must not be blank");
    this.manager = _manager;
    String nodeName = templateName + " Templates";
    setNodeName(nodeName);
    setTitle(nodeName + " Manager");

    this.addAction = new AbstractAction("Add " + templateName+" Template") {

      @Override
      public void actionPerformed(ActionEvent e) {
        TemplateT newItem = createNew();
        if (newItem != null) {
          if(manager.add(newItem) == false )
            log.error("Failed to add new template to manager!");
          
          showEditor(WindowUtils.getMainFrame(), newItem);
          if(contentPanel != null)
            contentPanel.repaint();
        }
      }
    };
  }
  
  @Override
  protected final void init() throws Exception {
    initComponents();    
  }
  
  private void initComponents() {
    contentPanel = new TemplatesManagerPanel<>(manager.getItemListModel(), this);
    contentPanel.setAddButtonText((String) addAction.getValue(Action.NAME));
    setLayout(new BorderLayout());
    add(contentPanel, BorderLayout.CENTER);    
  }

  @Override
  public TemplateT createNew() {
    String name = null; 
    JFrame parent = WindowUtils.getMainFrame();
    while(true) {
      name = JOptionPane.showInputDialog(parent,
        "Please input the name of the template:", "New "+ templateName + " Template", JOptionPane.QUESTION_MESSAGE);

      if (name == null) {
        /* user cancelled*/
        return null;
        
      } else if(Strings.isBlank(name) == false) {
        return createNewTempate(name, manager);
        
      } else {
        JOptionPane.showMessageDialog(parent, 
            "The name must not be blank!", "Invalid Name", JOptionPane.ERROR_MESSAGE);
      }
    }
    
    
  }
  
  protected abstract TemplateT createNewTempate(String name, ManagerT manager);

    
  @Override
  public final Action[] getContextActions() {
    return new Action[]{addAction};
  }
  
}
