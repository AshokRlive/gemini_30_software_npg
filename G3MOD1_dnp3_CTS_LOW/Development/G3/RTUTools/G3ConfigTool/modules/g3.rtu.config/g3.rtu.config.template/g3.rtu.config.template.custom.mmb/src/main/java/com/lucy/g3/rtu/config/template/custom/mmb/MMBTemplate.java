/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.mmb;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.template.builtin.modbus.PredefinedModbusDevices;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ISupportCache;


/**
 * User created MMBTemplate.
 */
public class MMBTemplate extends com.lucy.g3.rtu.config.template.builtin.modbus.MMBTemplate 
              implements ICustomTemplate, ISupportCache{
  
  private final MMBTemplates manager;

  /**
   * Construct a custom MMB template.
   * @param owner
   * @param model
   */
  MMBTemplate(MMBTemplates manager, String model) {
    super(manager.getMMBChannel(), model);
    setDeviceModel(model);
    this.manager = manager;
  }
  
  /**
   * Constructs a custom MMB template from predefined template.
   * @param owner
   * @param device
   */
  public MMBTemplate(MMBTemplates manager, PredefinedModbusDevices device) {
    super(manager.getMMBChannel(), device);
    this.manager = manager;
  }
  
  @Override
  public ICustomTemplateManager<?> getManager() {
    return manager;
  }
  
  @Override
  public String getDisplayText() {
    String name = getName();
    name = Strings.isBlank(name) ? " Unknown" : name;
    return String.format(DISPLAY_FORMAT, "Custom", name);
  }

}

