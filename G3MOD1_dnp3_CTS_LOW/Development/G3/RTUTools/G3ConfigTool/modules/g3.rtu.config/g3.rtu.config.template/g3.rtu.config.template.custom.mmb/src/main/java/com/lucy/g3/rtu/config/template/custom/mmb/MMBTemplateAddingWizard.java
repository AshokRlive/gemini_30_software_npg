
package com.lucy.g3.rtu.config.template.custom.mmb;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.jdesktop.swingx.prompt.PromptSupport;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.config.shared.base.wizard.WizardPageExt;
import com.lucy.g3.rtu.config.template.builtin.modbus.PredefinedModbusDevices;

class MMBTemplateAddingWizard{
    
  private static final String WIZARD_TITLE = "Add Modbus Template";

  
  public static MMBTemplate showWizard(MMBTemplates manager) {
    
    PredefinedModbusDevices[] templs = PredefinedModbusDevices.values();
    
    Wizard wiz = WizardPage.createWizard(WIZARD_TITLE, new WizardPage[]{
       new SelectTemplatePage(templs),
    }, new Result(manager));
    
    return (MMBTemplate) wiz.show(wiz, WizardUtils.createRect(WindowUtils.getMainFrame(),500, 300));
  }

  
  private static class Result implements WizardResultProducer {
    private final MMBTemplates manager;
    
    public Result(MMBTemplates manager) {
      this.manager = manager;
    }

    @Override
    public Object finish(Map data) throws WizardException {
      PredefinedModbusDevices tmpl = (PredefinedModbusDevices) data.get(KEY_DEVICE_TEMPLATE);
      String model = (String) data.get(KEY_DEVICE_MODEL);
      
      MMBTemplate result;
      
      if(tmpl == null) {
        result = new MMBTemplate(manager, model);
      } else {
        result = new MMBTemplate(manager, tmpl);
      }
      
      return result;
    }
  
    @Override
    public boolean cancel(Map settings) {
      return true;
    }
  }
 
  private static final String KEY_DEVICE_TEMPLATE  = "deviceTemplate"; 
  private static final String KEY_DEVICE_MODEL     = "deviceModel"; 
  
  private static class SelectTemplatePage extends WizardPageExt {
    private static final String TEXT_CREATE_A_NEW_DEVICE = "Create a new model...";
    
    private final PredefinedModbusDevices[] templates;
    

    private JComboBox<PredefinedModbusDevices> combTemplate;

    private JTextField tfDeviceModel;

    private JLabel lblNewDeviceModel;
    
    SelectTemplatePage(PredefinedModbusDevices[] templates) {
      super("Select a device");
      
      this.templates = templates;
      initComponents();
      updateVisibility();
    }
    
    private void updateVisibility() {
      PredefinedModbusDevices template = (PredefinedModbusDevices) combTemplate.getSelectedItem();
      boolean visiable = template == null;
      tfDeviceModel.setVisible(visiable);
      lblNewDeviceModel.setVisible(visiable);
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {
      DefaultFormBuilder builder = createPanelBuilder(this);
       
      combTemplate = new JComboBox<>(templates);
      combTemplate.insertItemAt(null, templates.length);
      combTemplate.setName(KEY_DEVICE_TEMPLATE);
      combTemplate.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          updateVisibility();        
        }
      });
      combTemplate.setRenderer(new DefaultListRenderer(new StringValue() {
        @Override
        public String getString(Object value) {
          if(value == null)
            return TEXT_CREATE_A_NEW_DEVICE ;
          
          return ((PredefinedModbusDevices)value).getDeviceModel();
        }
      }));
      builder.append("Choose a Model:", combTemplate);
      builder.nextLine();
      
      tfDeviceModel = new JTextField();
      tfDeviceModel.setName(KEY_DEVICE_MODEL);
      tfDeviceModel.setText("New Model");
      PromptSupport.setPrompt("e.g. Modbus Device", tfDeviceModel);
      lblNewDeviceModel = builder.append("New Device Model:", tfDeviceModel);
      builder.nextLine();
    }

    @Override
    protected String validateContents(Component component, Object event) {
      error = null;
      
      if(tfDeviceModel.isVisible())
        checkNotBlank(tfDeviceModel, "Device Model");
      
      return error;
    }
  }
}