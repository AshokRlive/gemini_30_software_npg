/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.gui.common.dialogs.PropertyEditorDialog;

/**
 *
 */
public class ScadaConfigPaknetEditorTest {

  public static void main(String[] args) {
    PropertyEditorDialog.createAndShowDialog(null,
        (Model) new ScadaTemplateCommsDev(null).getCommsDeivceConfig(), 
        "PAKNET Template Editor");
  }
}
