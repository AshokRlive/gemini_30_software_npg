/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.mmb;

import java.util.Collection;

import com.g3schema.ns_template.TemplateT;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateOwner;

/**
 * The manager for managing custom ModBus session templates.
 */
public final class MMBTemplates extends ItemListManager<MMBTemplate> implements ICustomTemplateManager<MMBTemplate>{
  private final ICustomTemplateOwner owner;
  
  private final MMBChannel mmbChannel = MMBChannel.createDummy();

  
  public MMBTemplates(ICustomTemplateOwner owner) {
    super();
    this.owner = owner;
  }
  
  @Override
  public ICustomTemplateOwner getOwner() {
    return owner;
  }

  @Override
  protected void postRemoveAction(MMBTemplate removedItem) {
    mmbChannel.remove(removedItem);
  }

  @Override
  protected void postAddAction(MMBTemplate addedItem) {
    mmbChannel.add(addedItem);
  }
  
  MMBChannel getMMBChannel(){
    return mmbChannel;
  }

  public void readFromXML(TemplateT xml) {
    int count = xml.modbusTemplate.count();
    for (int i = 0; i < count; i++) {
      MMBTemplate template = new MMBTemplate(this, "");
      template.readFromXML(xml.modbusTemplate.at(i));
      add(template);
    }
  }

  public void writeToXML(TemplateT xml) {
    Collection<MMBTemplate> templates = getAllItems();
    for (MMBTemplate template : templates) {
      template.writeToXML(xml.modbusTemplate.append());
    }
  }
}
