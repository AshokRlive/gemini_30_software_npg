/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.fpi;

import java.awt.Window;

import com.lucy.g3.gui.common.dialogs.PropertyEditorDialog;
import com.lucy.g3.rtu.config.template.custom.shared.ui.AbstractTemplatesPage;

public class FPIChannelTemplatesPage extends AbstractTemplatesPage<FPIChannelTemplate, FPIChannelTemplates> {
  public FPIChannelTemplatesPage(FPIChannelTemplates _template) {
    super(_template, "FPI");
  }
  
  @Override
  public void showEditor(Window parent, FPIChannelTemplate selection) {
    PropertyEditorDialog.createAndShowDialog(parent, selection, selection.getProfileName());
  }

  @Override
  protected FPIChannelTemplate createNewTempate(String name, FPIChannelTemplates manager) {
      return new FPIChannelTemplate(manager, name);
  }
}
