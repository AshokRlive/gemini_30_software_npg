/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.mmb;

import com.jgoodies.binding.list.SelectionInList;
import com.lucy.g3.rtu.config.protocol.shared.ui.AbstractProtocolItemManagerModel;
import com.lucy.g3.rtu.config.protocol.shared.ui.ProtocolStringFormatter;
import com.lucy.g3.rtu.config.shared.base.page.ItemListManagerPage;

/**
 * The page for managing ModBus Session templates.
 */
public final class MMBTemplatesPage extends ItemListManagerPage {

  public MMBTemplatesPage(MMBTemplates templates) {
    super(templates,
        "Modbus Templates",
        null,
        new MMBSessionTemplateManagerModel(templates),
        new ProtocolStringFormatter());
    setTitle("Modbus Templates Manager");
  }


  private static class MMBSessionTemplateManagerModel extends AbstractProtocolItemManagerModel {

    private final MMBTemplates manager;

    private final SelectionInList<MMBTemplate> selectionList;


    public MMBSessionTemplateManagerModel(MMBTemplates manager) {
      super();
      this.manager = manager;
      this.selectionList = new SelectionInList<>(manager.getItemListModel());
    }

    @Override
    public SelectionInList<?> getSelectionInList() {
      return selectionList;
    }

    @Override
    protected void addActionPerformed() {
      MMBTemplate newTempl = MMBTemplateAddingWizard.showWizard(manager);
      if(newTempl != null) {
        manager.add(newTempl);
        selectionList.setSelection(newTempl);  
      }
    }

    @Override
    protected void removeActionPerformed() {
      MMBTemplate selection = selectionList.getSelection();
      if (selection != null) {
        if (showConfirmRemoveDialog(selection.getDeviceName())) {
          manager.remove(selection);
        }
      }
    }

  }
}
