/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import java.util.Collection;

import com.g3schema.ns_template.TemplateT;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateOwner;


public class ScadaTemplates extends ItemListManager<ScadaTemplate> implements ICustomTemplateManager<ScadaTemplate>{

  private final ICustomTemplateOwner owner;
  
  public ScadaTemplates(ICustomTemplateOwner owner) {
    this.owner = owner;
  }

  public void readFromXML(TemplateT xml) {
    int count = xml.scadaTemplate.count();
    for (int i = 0; i < count; i++) {
      ScadaTemplate templ = new ScadaTemplate(this, "");
      templ.readFromXML(xml.scadaTemplate.at(i));
      add(templ);
    }
    
  }

  public void writeToXML(TemplateT xml) {
    Collection<ScadaTemplate> all = getAllItems();
    for (ScadaTemplate templ : all) {
      templ.writeToXML(xml.scadaTemplate.append());
    }
  }

  @Override
  public ICustomTemplateOwner getOwner() {
    return owner;
  }

}

