
package com.lucy.g3.rtu.config.template.custom.sdnp3.serial;

import com.lucy.g3.rtu.config.device.comms.domain.ICommsDevice;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaAddress;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateCommsDev.CommsDeviceSelection;

public interface ISerialTemplate extends ICommsDevice{
  String PROPERTY_COMMS_DEVICE_SElECTION = "commsDeviceSelection";
  String PROPERTY_SCADA_ADDRESS = "scadaAddress";
  
  int INVALID_ID = 65536;
  
  //String PROPERTY_MASTER_ADDRESS   = "masterAddress";
  
  ScadaAddress getScadaAddress();
  
  CommsDeviceSelection getCommsDeviceSelection(); 
  
}

