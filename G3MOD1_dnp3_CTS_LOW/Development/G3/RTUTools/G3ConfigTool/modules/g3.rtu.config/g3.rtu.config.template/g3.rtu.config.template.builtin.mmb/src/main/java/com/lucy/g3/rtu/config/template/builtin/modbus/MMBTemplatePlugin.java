/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.builtin.modbus;

import com.lucy.g3.configtool.config.template.TemplateRepositories;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


public class MMBTemplatePlugin implements IConfigPlugin{
  
  private MMBTemplatePlugin () {}
  private final static MMBTemplatePlugin INSTANCE = new MMBTemplatePlugin();
  
  
  public static MMBTemplatePlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    // Register config module
    ConfigFactory.getInstance().registerFactory(MMBTemplates.CONFIG_MODULE_ID, 
        new IConfigModuleFactory() {
      @Override
      public IConfigModule create(IConfig owner) {
        return new MMBTemplates(owner);
      }
    });

    // Register template repository
    TemplateRepositories.register(TemplateRepositories.KEY_MMB_BUILTIN, MMBTemplatesRepository.INSTANCE);
  }

}

