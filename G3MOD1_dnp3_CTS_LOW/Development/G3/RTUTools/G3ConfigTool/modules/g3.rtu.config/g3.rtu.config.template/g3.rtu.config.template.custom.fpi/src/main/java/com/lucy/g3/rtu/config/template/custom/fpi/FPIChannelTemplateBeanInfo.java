/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.fpi;

import java.text.ParseException;

import com.l2fprod.common.beans.ExtendedPropertyDescriptor;
import com.l2fprod.common.beans.editor.StringConverterPropertyEditor;
import com.l2fprod.common.util.converter.Converter;
import com.l2fprod.common.util.converter.ConverterRegistry;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigEarthFault;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigPhaseFault;
import com.lucy.g3.rtu.config.shared.base.bean.BeanInfo;

/**
 * BeanInfo class required by FPIChannelTemplate Property Editor.
 */
public class FPIChannelTemplateBeanInfo extends BeanInfo {

  static {
    new CTRatioConverter().register(ConverterRegistry.instance());
    new FaultConverter().register(ConverterRegistry.instance());
  }
  
  public FPIChannelTemplateBeanInfo() {
    super(FPIChannelTemplate.class);
    
    addProperty(FPIChannelTemplate.PROPERTY_PROFILE_NAME);
    addProperty(FPIChannelTemplate.PROPERTY_CTRATIO).setPropertyEditorClass(CTRatioPropertyEditor.class);
    addProperty(FPIChannelTemplate.PROPERTY_SELF_RESET_MIN);
    addProperty(FPIChannelTemplate.PROPERTY_RST_ON_LV_LOST);
    addProperty(FPIChannelTemplate.PROPERTY_RST_ON_LV_RESTORED);
    
    ExtendedPropertyDescriptor earthFault = addProperty(FPIChannelTemplate.PROPERTY_EARCHFAULT);
    addSubProperty(earthFault, FPIConfigEarthFault.PROPERTY_MIN_FAULT_DURATION_MS);
    addSubProperty(earthFault, FPIConfigEarthFault.PROPERTY_TIMED_FAULT_CURRENT);
    addSubProperty(earthFault, FPIConfigEarthFault.PROPERTY_INSTANT_FAULT_CURRENT);
    
    earthFault = addProperty(FPIChannelTemplate.PROPERTY_SENSITIVE_EARCHFAULT);
    addSubProperty(earthFault, FPIConfigEarthFault.PROPERTY_MIN_FAULT_DURATION_MS);
    addSubProperty(earthFault, FPIConfigEarthFault.PROPERTY_TIMED_FAULT_CURRENT);
    //addSubProperty(earthFault, FPIConfigEarthFault.PROPERTY_INSTANT_FAULT_CURRENT);
    
    
    ExtendedPropertyDescriptor phaseFault = addProperty(FPIChannelTemplate.PROPERTY_PHASEFAULT);
    addSubProperty(phaseFault, FPIConfigPhaseFault.PROPERTY_MIN_FAULT_DURATION_MS);
    addSubProperty(phaseFault, FPIConfigPhaseFault.PROPERTY_TIMED_FAULT_CURRENT);
    addSubProperty(phaseFault, FPIConfigPhaseFault.PROPERTY_INSTANT_FAULT_CURRENT);
  }

  public static class CTRatioPropertyEditor extends StringConverterPropertyEditor {

    @Override
    protected Object convertFromString(String text) {
      return ConverterRegistry.instance().convert(CTRatio.class, text);
    }
  }

  public static class CTRatioConverter implements Converter {
    public void register(ConverterRegistry registry) {
      registry.addConverter(String.class, CTRatio.class, this);
      registry.addConverter(CTRatio.class, String.class, this);
    }
    
    @Override
    public Object convert(@SuppressWarnings("rawtypes") Class type, Object value) {
      if (String.class.equals(type) && CTRatio.class.equals(value.getClass())) {
        return ((CTRatio)value).getText();
      } else if (CTRatio.class.equals(type)) {
        try {
          return CTRatio.parseFromString(String.valueOf(value));
        } catch (ParseException e) {
          throw new IllegalArgumentException("Can't convert " + value + " to "+ type.getName());
        }
      } else {
        throw new IllegalArgumentException("Can't convert " + value + " to "
          + type.getName());
      }
    }
    
  }
  
  public static class FaultConverter implements Converter {
    public void register(ConverterRegistry registry) {
      registry.addConverter(FPIConfigEarthFault.class, String.class, this);
      registry.addConverter(FPIConfigPhaseFault.class, String.class, this);
    }
    
    @Override
    public Object convert(@SuppressWarnings("rawtypes") Class type, Object value) {
      return "";
    }
    
  }
//
//  public static class CTRatioPropertyEditor extends ComboBoxPropertyEditor {
//
//    public CTRatioPropertyEditor() {
//      super();
//      setAvailableValues(FPIConfigChannel.getDefaultCTRatio());
//      JComboBox combo = (JComboBox) getCustomEditor();
//      combo.setEditable(true);
//      combo.setFocusable(true);
//
//      CTRatioEditor ratioEditor = new CTRatioEditorCombox.CTRatioEditor();
//      combo.setEditor(ratioEditor);
//    }
//  }
}