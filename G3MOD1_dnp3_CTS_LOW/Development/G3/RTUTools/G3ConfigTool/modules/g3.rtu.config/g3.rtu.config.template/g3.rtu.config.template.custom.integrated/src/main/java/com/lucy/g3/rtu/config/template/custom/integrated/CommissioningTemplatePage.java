/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.integrated;

import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.rtu.config.template.custom.fpi.FPIChannelTemplatesPage;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplatesPage;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplatesPage;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplatesPage;

/**
 * The page of ConfigTemplate.
 */
public class CommissioningTemplatePage extends AbstractConfigPage {

  public CommissioningTemplatePage(CustomTemplates data) {
    super(data);
    setNodeName("Templates");
    addChildPage(new SwitchTemplatesPage(data.getSwitchTemplates()));
    addChildPage(new ScadaTemplatesPage(data.getScadaTemplates()));
    addChildPage(new MMBTemplatesPage(data.getMMBTemplates()));
    addChildPage(new FPIChannelTemplatesPage(data.getFpiTemplates()));
  }

  @Override
  protected void init() throws Exception {
    // Nothing to do
  }

}
