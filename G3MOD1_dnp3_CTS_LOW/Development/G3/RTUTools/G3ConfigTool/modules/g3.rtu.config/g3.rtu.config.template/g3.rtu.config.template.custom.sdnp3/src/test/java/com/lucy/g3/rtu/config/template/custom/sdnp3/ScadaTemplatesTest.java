/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplates;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplatesPage;
import com.lucy.g3.test.support.utilities.TestUtil;


/**
 *
 */
public class ScadaTemplatesTest {

  public static void main(String[] args) {

    ScadaTemplatesPage page = new ScadaTemplatesPage(new ScadaTemplates(null));

    TestUtil.showFrame(page.getContent());
  
  }
}

