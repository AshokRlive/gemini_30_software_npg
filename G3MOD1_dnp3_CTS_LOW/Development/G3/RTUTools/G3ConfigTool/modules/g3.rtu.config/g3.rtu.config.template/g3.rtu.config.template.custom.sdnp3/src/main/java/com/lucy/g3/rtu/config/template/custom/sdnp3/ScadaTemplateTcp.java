/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import java.util.Collection;

import com.g3schema.ns_template.ScadaAddressT;
import com.g3schema.ns_template.ScadaTemplateTcpT;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplate;
import com.lucy.g3.rtu.config.template.custom.shared.ICustomTemplateManager;


public class ScadaTemplateTcp extends ItemListManager<TcpScadaAddress> implements ICustomTemplate{

  private final ICustomTemplateManager<?> manager;

  public ScadaTemplateTcp(ICustomTemplateManager<?> manager) {
    super();
    this.manager = manager;
  }
  

  public void readFromXML(ScadaTemplateTcpT xml) {
    int count = xml.scadaAddress.count();
    TcpScadaAddress address;
    for (int i = 0; i < count; i++) {
      address = new TcpScadaAddress();
      address.setIp(xml.scadaAddress.at(i).ipaddress.getValue());
      address.setMasterAddress(xml.scadaAddress.at(i).masterAddress.getValue());
      add(address);
    }    
  }

  public void writeToXML(ScadaTemplateTcpT xml) {
    Collection<TcpScadaAddress> all = getAllItems();
    ScadaAddressT xmlScada;
    for (TcpScadaAddress sa : all) {
      xmlScada = xml.scadaAddress.append();
      xmlScada.ipaddress.setValue(sa.getIp());
      xmlScada.masterAddress.setValue(sa.getMasterAddress());
    }
    
  }


  @Override
  public ICustomTemplateManager<?> getManager() {
    return manager;
  }

}

