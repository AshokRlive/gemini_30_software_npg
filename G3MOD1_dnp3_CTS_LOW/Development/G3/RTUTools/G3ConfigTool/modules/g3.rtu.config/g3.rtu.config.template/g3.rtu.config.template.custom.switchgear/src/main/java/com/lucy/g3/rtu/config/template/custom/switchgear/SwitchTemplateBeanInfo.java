/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.switchgear;

import com.l2fprod.common.beans.ExtendedPropertyDescriptor;
import com.l2fprod.common.beans.editor.ComboBoxPropertyEditor;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.rtu.config.shared.base.bean.BeanInfo;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.MOTOR_MODE;

/**
 * BeanInfo class required by Switch Template Property Editor.
 */
public class SwitchTemplateBeanInfo extends BeanInfo {

  public SwitchTemplateBeanInfo() {
    super(SwitchTemplate.class);

    addProperty(SwitchTemplate.PROPERTY_SWITCHNAME);
    addProperty(SwitchTemplate.PROPERTY_ALLOW_FORCED_OPERATION);
    addProperty(SwitchTemplate.PROPERTY_MOTOR_MODE).setPropertyEditorClass(MotorModeEditor.class);
    
    ExtendedPropertyDescriptor open  = addProperty(SwitchTemplate.PROPERTY_OPEN).setReadOnly();
    ExtendedPropertyDescriptor close = addProperty(SwitchTemplate.PROPERTY_CLOSE).setReadOnly();
        
    addSubProperty(open, SwitchChannelTemplate.PROPERTY_PULSELENGTH);
    addSubProperty(open, SwitchChannelTemplate.PROPERTY_OVERRUN);
    addSubProperty(open, SwitchChannelTemplate.PROPERTY_MOTOR_OVER_DRIVE);
    addSubProperty(open, SwitchChannelTemplate.PROPERTY_MOTOR_REVERSE_DRIVE);
    addSubProperty(open, SwitchChannelTemplate.PROPERTY_OPERATIONTIMEOUT);
    addSubProperty(open, SwitchChannelTemplate.PROPERTY_POLARITY).setPropertyEditorClass(PolarityEditor.class);
    addSubProperty(close, SwitchChannelTemplate.PROPERTY_PULSELENGTH);
    addSubProperty(close, SwitchChannelTemplate.PROPERTY_OVERRUN);
    addSubProperty(close, SwitchChannelTemplate.PROPERTY_MOTOR_OVER_DRIVE);
    addSubProperty(close, SwitchChannelTemplate.PROPERTY_MOTOR_REVERSE_DRIVE);
    addSubProperty(close, SwitchChannelTemplate.PROPERTY_OPERATIONTIMEOUT);
    addSubProperty(close, SwitchChannelTemplate.PROPERTY_POLARITY).setPropertyEditorClass(PolarityEditor.class);
    
    addProperty(SwitchTemplate.PROPERTY_CIRCUIT_BREAKER);
    addProperty(SwitchTemplate.PROPERTY_OVER_CUR_THRESHOLD);
    addProperty(SwitchTemplate.PROPERTY_OVER_CUR_TIME_MS);
    addProperty(SwitchTemplate.PROPERTY_LOCALDELAY_SEC);
    addProperty(SwitchTemplate.PROPERTY_REMOTEDELAY_SEC);
    addProperty(SwitchTemplate.PROPERTY_PRE_OPERATION_DELAY_MS);
    addProperty(SwitchTemplate.PROPERTY_POST_OPERATION_DELAY_MS);
  }


  /**
   * Property editor for Polarity used by PropertySheet.
   */
  public static class PolarityEditor extends ComboBoxPropertyEditor {

    public PolarityEditor() {
      super();
      setAvailableValues(Polarity.values());
    }
  }
  
  /**
   * Property editor for Polarity used by PropertySheet.
   */
  public static class MotorModeEditor extends ComboBoxPropertyEditor {
    
    public MotorModeEditor() {
      super();
      setAvailableValues(MOTOR_MODE.values());
    }
  }
}