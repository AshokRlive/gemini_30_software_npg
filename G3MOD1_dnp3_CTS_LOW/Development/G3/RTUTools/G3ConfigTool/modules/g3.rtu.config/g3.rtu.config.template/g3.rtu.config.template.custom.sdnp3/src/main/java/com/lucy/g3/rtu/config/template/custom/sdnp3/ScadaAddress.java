/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import org.apache.log4j.Logger;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Strings;


/**
 * The Class ScadaAddress.
 */
public class ScadaAddress extends Model {
  final public static String PROPERTY_MASTER_ADDRESS = "masterAddress";
  
  private String masterAddress = "0";
  
  public long getMasterAddressValue() {
    try {
      return Long.valueOf(masterAddress);
    }catch(Throwable e) {
      return -1;
    }
  }
  
  public String getMasterAddress() {
    return masterAddress;
  }
  
  public void setMasterAddress(String masterAddress) {
    Object oldValue = getMasterAddress();
    
    if(Strings.isBlank(masterAddress))  
      masterAddress = "";
    
    try{
      Long.valueOf(masterAddress);
      this.masterAddress = masterAddress;
      firePropertyChange(PROPERTY_MASTER_ADDRESS, oldValue, masterAddress);
    }catch (Throwable e) {
      Logger.getLogger(ScadaAddress.class).error(e);
    }
  }


  @Override
  public String toString() {
    return "";
  }
  
}

