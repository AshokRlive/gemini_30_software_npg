/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.sdnp3;

import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceType;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateEditorDialog;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate.ScadaTemplateType;



public class ScadaTemplateEditorDialogTest {


  public static void main(String[] args) {
    ScadaTemplate template = new ScadaTemplate(null);
    template .setType(ScadaTemplateType.COMMS);
    template.getCommsDev().setType(CommsDeviceType.PAKNET_MODEM);
    ScadaTemplateEditorDialog dialog = new ScadaTemplateEditorDialog(null,template);
    dialog.setVisible(true);
  }
}

