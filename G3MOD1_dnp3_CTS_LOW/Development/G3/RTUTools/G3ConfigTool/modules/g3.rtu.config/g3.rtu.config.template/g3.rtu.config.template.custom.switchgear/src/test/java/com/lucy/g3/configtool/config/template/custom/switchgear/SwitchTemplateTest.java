/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.config.template.custom.switchgear;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplate;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplates;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplatesPage;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class SwitchTemplateTest.
 */
public class SwitchTemplateTest {
  private SwitchTemplate fixture; 
  private final static String[] PARAMETERS = {
    SwitchTemplate.PROPERTY_ALLOW_FORCED_OPERATION,
    SwitchTemplate.PROPERTY_MOTOR_MODE
  };
  @Before
  public void init(){
    fixture = new SwitchTemplate(null, null);
  }
  

  @Test
  public void testBean() {
    TestUtil.testReadWriteProperties(new SwitchTemplate(null, null), PARAMETERS);
  }
  
  
  // GUI test 
  public static void main(String[] args) {
    SwitchTemplatesPage page = new SwitchTemplatesPage(new SwitchTemplates(null));

    TestUtil.showFrame(page.getContent());
  }
}
