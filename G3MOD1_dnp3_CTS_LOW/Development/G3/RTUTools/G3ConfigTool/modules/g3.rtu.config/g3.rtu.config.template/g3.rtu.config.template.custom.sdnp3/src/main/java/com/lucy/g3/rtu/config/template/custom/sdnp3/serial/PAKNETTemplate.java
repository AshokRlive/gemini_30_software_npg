
package com.lucy.g3.rtu.config.template.custom.sdnp3.serial;

import com.lucy.g3.rtu.config.device.comms.domain.PAKNETModem;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaAddress;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateCommsDev.CommsDeviceSelection;


public class PAKNETTemplate extends PAKNETModem implements ISerialTemplate{

  private final CommsDeviceSelection commsDeviceSelection;
  private final ScadaAddress address = new ScadaAddress(); 
  
  
  public PAKNETTemplate(CommsDeviceSelection commsSelection) {
    super(INVALID_ID);
    this.commsDeviceSelection = commsSelection;
  }
  
  @Override
  public void setDeviceId(int deviceId){
    // No effect since the ID template mustn't be modified 
  }
  
  
  @Override
  public ScadaAddress getScadaAddress() {
    return address;
  }

  public void setScadaAddress(ScadaAddress address) {
    throw new UnsupportedOperationException("Unsupported operation");
  }
  
  public void setCommsDeviceSelection(CommsDeviceSelection selection) {
    throw new UnsupportedOperationException("Unsupported operation");
  }

  @Override
  public CommsDeviceSelection getCommsDeviceSelection() {
    return commsDeviceSelection;
  }

  @Override
  public IValidator getValidator() {
    return null;
  }
  
}

