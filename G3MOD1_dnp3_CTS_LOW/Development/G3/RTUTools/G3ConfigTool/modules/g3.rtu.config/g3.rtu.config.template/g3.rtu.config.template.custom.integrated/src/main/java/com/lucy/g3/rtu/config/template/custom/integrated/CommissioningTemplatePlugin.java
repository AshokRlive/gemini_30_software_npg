/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.template.custom.integrated;

import java.util.Collection;

import com.lucy.g3.configtool.config.template.ITemplate;
import com.lucy.g3.configtool.config.template.ITemplateRepository;
import com.lucy.g3.configtool.config.template.TemplateRepositories;
import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplate;


public class CommissioningTemplatePlugin implements IConfigPlugin, IConfigModuleFactory, IPageFactory{
  
  private CommissioningTemplatePlugin () {}
  private final static CommissioningTemplatePlugin  INSTANCE = new CommissioningTemplatePlugin();
  
  
  public static CommissioningTemplatePlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory.getInstance().registerFactory(CustomTemplates.CONFIG_MODULE_ID, INSTANCE);
    
    PageFactories.registerFactory(PageFactories.KEY_TEMPLATE, INSTANCE);
    
    TemplateRepositories.register(TemplateRepositories.KEY_MMB_CUSTOM, new ITemplateRepository() {
      
      @Override
      public ITemplate[] getTemplates(IConfig config) {
        CustomTemplates templates = config.getConfigModule(CustomTemplates.CONFIG_MODULE_ID);
        Collection<MMBTemplate> customTempls = templates.getMMBTemplates().getAllItems();
        return customTempls.toArray(new ITemplate[customTempls.size()]);
      }
    });
  }
  
  @Override
  public CustomTemplates create(IConfig owner) {
    return new CustomTemplates(owner);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof CustomTemplates) {
      return new CommissioningTemplatePage((CustomTemplates) data);
    } 
    
    return null;
  }
 
}

