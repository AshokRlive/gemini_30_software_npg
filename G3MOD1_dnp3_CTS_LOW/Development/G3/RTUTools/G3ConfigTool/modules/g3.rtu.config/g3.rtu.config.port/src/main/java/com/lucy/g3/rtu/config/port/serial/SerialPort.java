/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT_PATH;

/**
 * The serial port created from Port enum.
 */
public final class SerialPort extends AbstractSerialPort implements ISerialPort {

  private Logger log = Logger.getLogger(SerialPort.class);

  private final SERIAL_PORT portEnum;


  SerialPort(SERIAL_PORT portEnum) {
    super(portEnum.name(), 1);
    this.portEnum = Preconditions.checkNotNull(portEnum, "portEnum is null");
  }

  @Override
  public SERIAL_PORT getPortEnum() {
    return portEnum;
  }

  @Override
  public String getPortPath() {
    SERIAL_PORT_PATH pathEnum = SERIAL_PORT_PATH.forValue(portEnum.getValue());

    if (pathEnum == null) {
      log.fatal("No port path enum found for port: " + portEnum);
      return "";
    } else {
      return pathEnum.getDescription();
    }
  }

  @Override
  public String getDisplayName() {
    return portEnum.getDescription();
  }

}
