/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import java.util.Collection;

import com.lucy.g3.rtu.config.port.eth.EthPortManager;
import com.lucy.g3.rtu.config.port.eth.IEthPortManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPortManager;
import com.lucy.g3.rtu.config.port.serial.SerialPortManager;
import com.lucy.g3.rtu.config.port.validation.PortsManagerValidator;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * Implementation of <code>PortsManager</code>.
 */
public class PortsManagerImpl extends AbstractConfigModule implements PortsManager {

  public PortsManagerImpl(IConfig owner) {
    super(owner);
  }

  private final SerialPortManager serialMgr = new SerialPortManager();
  private final EthPortManager ethMgr = new EthPortManager();
  private final IContainerValidator validator = new PortsManagerValidator(this);


  @Override
  public ISerialPortManager getSerialManager() {
    return serialMgr;
  }

  @Override
  public IEthPortManager getEthManager() {
    return ethMgr;
  }

  @Override
  public IContainerValidator getValidator() {
    return validator;
  }

  // ================== Delegate Methods ===================
  @Override
  public Collection<IEthernetPort> getEthPorts() {
    return ethMgr.getEthPorts();
  }

  @Override
  public IEthernetPort getEthPort(String portName) {
    return ethMgr.getEthPort(portName);
  }

  @Override
  public IEthernetPort getEthPort(ETHERNET_PORT portEnum) {
    return ethMgr.getEthPort(portEnum);
  }

  @Override
  public Collection<ISerialPort> getSerialPorts() {
    return serialMgr.getSerialPorts();
  }

  @Override
  public ISerialPort getSerialPort(String portName) {
    return serialMgr.getSerialPort(portName);
  }

}
