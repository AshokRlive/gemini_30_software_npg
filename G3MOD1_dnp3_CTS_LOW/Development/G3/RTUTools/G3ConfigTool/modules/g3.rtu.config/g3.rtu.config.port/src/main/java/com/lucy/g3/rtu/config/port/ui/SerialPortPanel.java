/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_BAUD_RATE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_DATA_BITS;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PARITY;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PORT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_STOP_BITS;

public class SerialPortPanel extends JPanel {

  private final PresentationModel<ISerialPort> pm = new PresentationModel<>();


  public SerialPortPanel(ISerialPort serialPort) {
    pm.setBean(serialPort);
    initComponents();
    initComponentsBindings();

    // Temporarily hide this cause MCM doesn't support it.
    cboxDisable.setVisible(false);
  }

  @SuppressWarnings("unchecked")
  private void initComponentsBindings() {
    ValueModel vm;

    // Disable port
    vm = ConverterFactory.createBooleanNegator(pm.getModel(ISerialPort.PROPERTY_ENABLED));
    Bindings.bind(cboxDisable, vm);

    // Baud Rate
    vm = pm.getModel(ISerialPort.PROPERTY_BAUDRATE);
    cboxBaudRate.setModel(new ComboBoxAdapter<>(LU_LIN232_BAUD_RATE.values(), vm));

    // Data Bits
    vm = pm.getModel(ISerialPort.PROPERTY_NUMDATABITS);
    cboxDatabits.setModel(new ComboBoxAdapter<>(LU_LIN232_DATA_BITS.values(), vm));

    // Stop Bits
    vm = pm.getModel(ISerialPort.PROPERTY_NUMSTOPBITS);
    cboxStopbits.setModel(new ComboBoxAdapter<>(LU_LIN232_STOP_BITS.values(), vm));

    // Parity
    vm = pm.getModel(ISerialPort.PROPERTY_PARITY);
    Bindings.bind(radioBtnParityNone, vm, LU_LIN232_PARITY.LU_LIN232_PARITY_NONE);
    Bindings.bind(radioBtnParityEven, vm, LU_LIN232_PARITY.LU_LIN232_PARITY_EVEN);
    Bindings.bind(radioBtnParityOdd, vm, LU_LIN232_PARITY.LU_LIN232_PARITY_ODD);

    // Data Bits
    vm = pm.getModel(ISerialPort.PROPERTY_PORTMODE);
    Bindings.bind(radioBtnModeHardware, vm, LU_LIN232_PORT_MODE.LU_LIN232_MODE_HARDWARE);
    Bindings.bind(radioBtnModeNone, vm, LU_LIN232_PORT_MODE.LU_LIN232_MODE_NONE);

    // In Use
    vm = pm.getModel(ISerialPort.PROPERTY_IN_USE);
    vm = ConverterFactory.createBooleanToStringConverter(vm, "Yes", "No", "");
    Bindings.bind(lblInUse, vm);

    bindComponentsStates();
  }

  private void bindComponentsStates() {
    if (pm.getBean() == null) {
      return;
    }

    // Enable port
    ValueModel vm = pm.getModel(ISerialPort.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, panelSerial, "visible");
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    cboxDisable = new JCheckBox();
    panelSerial = new JPanel();
    JLabel label5 = new JLabel();
    cboxBaudRate = new JComboBox<>();
    JLabel label6 = new JLabel();
    cboxDatabits = new JComboBox<>();
    JLabel label7 = new JLabel();
    cboxStopbits = new JComboBox<>();
    JLabel label8 = new JLabel();
    radioBtnParityNone = new JRadioButton();
    radioBtnParityEven = new JRadioButton();
    radioBtnParityOdd = new JRadioButton();
    JLabel label9 = new JLabel();
    radioBtnModeNone = new JRadioButton();
    radioBtnModeHardware = new JRadioButton();
    panel1 = new JPanel();
    label1 = new JLabel();
    lblInUse = new JLabel();

    // ======== this ========
    setLayout(new FormLayout(
        "default:grow",
        "default, $pgap, default, $ugap, default"));

    // ---- cboxDisable ----
    cboxDisable.setText("Disable this port");
    add(cboxDisable, CC.xy(1, 1));

    // ======== panelSerial ========
    {
      panelSerial.setBorder(new CompoundBorder(
          new TitledBorder("Serial Port Settings"),
          Borders.DLU2_BORDER));
      panelSerial.setLayout(new FormLayout(
          "right:[50dlu,default], $lcgap, [80dlu,default]",
          "3*(fill:default, $ugap), fill:default, 2*($lgap, default), $ugap, fill:default, $lgap, default"));

      // ---- label5 ----
      label5.setText("Baud Rate:");
      panelSerial.add(label5, CC.xy(1, 1));
      panelSerial.add(cboxBaudRate, CC.xy(3, 1));

      // ---- label6 ----
      label6.setText("Data Bits:");
      panelSerial.add(label6, CC.xy(1, 3));
      panelSerial.add(cboxDatabits, CC.xy(3, 3));

      // ---- label7 ----
      label7.setText("Stop Bits:");
      panelSerial.add(label7, CC.xy(1, 5));
      panelSerial.add(cboxStopbits, CC.xy(3, 5));

      // ---- label8 ----
      label8.setText("Parity");
      panelSerial.add(label8, CC.xy(1, 7));

      // ---- radioBtnParityNone ----
      radioBtnParityNone.setText("None");
      panelSerial.add(radioBtnParityNone, CC.xy(3, 7));

      // ---- radioBtnParityEven ----
      radioBtnParityEven.setText("Even");
      panelSerial.add(radioBtnParityEven, CC.xy(3, 9));

      // ---- radioBtnParityOdd ----
      radioBtnParityOdd.setText("Odd");
      panelSerial.add(radioBtnParityOdd, CC.xy(3, 11));

      // ---- label9 ----
      label9.setText("Port Mode");
      panelSerial.add(label9, CC.xy(1, 13));

      // ---- radioBtnModeNone ----
      radioBtnModeNone.setText("None");
      panelSerial.add(radioBtnModeNone, CC.xy(3, 13));

      // ---- radioBtnModeHardware ----
      radioBtnModeHardware.setText("Hardware");
      panelSerial.add(radioBtnModeHardware, CC.xy(3, 15));
    }
    add(panelSerial, CC.xy(1, 3));

    // ======== panel1 ========
    {
      panel1.setLayout(new FormLayout(
          "default, $lcgap, default",
          "default"));

      // ---- label1 ----
      label1.setText("In Use:");
      panel1.add(label1, CC.xy(1, 1));
      panel1.add(lblInUse, CC.xy(3, 1));
    }
    add(panel1, CC.xy(1, 5));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox cboxDisable;
  private JPanel panelSerial;
  private JComboBox<Object> cboxBaudRate;
  private JComboBox<Object> cboxDatabits;
  private JComboBox<Object> cboxStopbits;
  private JRadioButton radioBtnParityNone;
  private JRadioButton radioBtnParityEven;
  private JRadioButton radioBtnParityOdd;
  private JRadioButton radioBtnModeNone;
  private JRadioButton radioBtnModeHardware;
  private JPanel panel1;
  private JLabel label1;
  private JLabel lblInUse;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
