/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.validation;

import java.text.ParseException;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.port.eth.DHCPConfig;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.eth.IPConfig;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The validator of Ethernet Port.
 */
public class EthernetPortValidator extends AbstractValidator<IEthernetPort> {

  private Logger log = Logger.getLogger(EthernetPortValidator.class);


  public EthernetPortValidator(IEthernetPort target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {

    if (target.isConfigEnabled()) {
      validateStaticIP(target.getIpConfig(), result);
      validateDHCP(target.getDhcpConfig(), result);
    }
  }

  private void validateDHCP(DHCPConfig dhcpConfig, ValidationResultExt result) {
    if (dhcpConfig == null) {
      return;
    }

    // TODO validate DHCP...
  }

  private void validateStaticIP(IPConfig ipConfig, ValidationResultExt result) {
    if (ipConfig == null || ipConfig.isDynamicIP()) {
      return;
    }

    String ip = ipConfig.getIpAddress();
    String submask = ipConfig.getSubmask();
    String gateway = ipConfig.getGateway();

    if (!IPAddress.validateIPAddress(ip)) {
      result.addError("Invalid IP:" + ip);
    }

    if (!IPAddress.validateIPAddress(submask)) {
      result.addError("Invalid Submask:" + submask);
    }

    if (!IPAddress.validateIPAddress(gateway, true)) {
      result.addError("Invalid Gateway:" + gateway);
    }

    if(!Strings.isBlank(gateway)) {
      // Checks gateway and IP address are in the same sub net.
      try {
        IPAddress ipAddr = IPAddress.valueOf(ip);
        IPAddress maskAddr = IPAddress.valueOf(submask);
        IPAddress gatewayAddr = IPAddress.valueOf(gateway, true);
        ipAddr = IPAddress.applySubMask(ipAddr, maskAddr);
        
        gatewayAddr = IPAddress.applySubMask(gatewayAddr, maskAddr);
        if (ipAddr.equals(gatewayAddr) == false) {
          result.addError(String.format("Gateway\"%s\" and IP \"%s\" are not in the same sub net\"%s\"",
              gateway, ip, submask));
        }
      } catch (ParseException e) {
        log.error("Failed to parse IP address:" + ip, e);
      }
    }

  }

  @Override
  public String getTargetName() {
    return target.getDisplayName();
  }
}
