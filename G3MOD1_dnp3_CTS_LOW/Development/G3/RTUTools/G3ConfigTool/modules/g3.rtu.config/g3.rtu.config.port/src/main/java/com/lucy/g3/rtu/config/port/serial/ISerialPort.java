/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import com.lucy.g3.rtu.config.port.Port;
import com.lucy.g3.rtu.config.port.PortUnusableException;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_BAUD_RATE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_DATA_BITS;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PARITY;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PORT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_STOP_BITS;

/**
 * The Interface ISerialPort.
 */
public interface ISerialPort extends Port {

  /**
   * The name of read-only property {@value} . <li>Value type: {@link String}.</li>
   */
  String PROPERTY_PORT_NAME = "portName";
  /**
   * The name of read-only property {@value} . <li>Value type: {@link String}.</li>
   */
  String PROPERTY_PORT_PATH = "portPath";

  String PROPERTY_BAUDRATE = "baudRate";
  String PROPERTY_NUMDATABITS = "dataBit";
  String PROPERTY_NUMSTOPBITS = "stopBit";
  String PROPERTY_PARITY = "parity";
  String PROPERTY_PORTMODE = "portMode";
  String PROPERTY_DISCARDMSG_CTLOW = "discardMessageCTLow";

  /**
   * The name of read-only property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  String PROPERTY_IN_USE = "inUse";


  /**
   * Gets Linux path of this port. e.g. "/dev/ttymxc0"
   *
   * @return the port path
   */
  String getPortPath();

  SERIAL_PORT getPortEnum();

  /**
   * Try to connect a device to this port.
   *
   * @param device
   *          a device to be connected to this port.
   * @throws PortUnusableException
   *           If this port not available for connecting to a given device.
   */
  void connectTo(ISerialPortUser user) throws PortUnusableException;

  /**
   * Disconnect a device that was connected to this port.
   *
   * @param user
   *          the user to be disconnected
   */
  void disconnectFrom(ISerialPortUser user);

  /**
   * Checks if this port is connected to a device.
   */
  boolean isUsedBy(ISerialPortUser user);

  /**
   * Checks if this port is connected to any device.
   */
  boolean isInUse();

  LU_LIN232_BAUD_RATE getBaudRate();

  LU_LIN232_DATA_BITS getDataBit();

  LU_LIN232_PARITY getParity();

  LU_LIN232_PORT_MODE getPortMode();

  LU_LIN232_STOP_BITS getStopBit();
  
  boolean getdiscardMessageCTLow();

  void setBaudRate(LU_LIN232_BAUD_RATE baudRate);

  void setDataBit(LU_LIN232_DATA_BITS dataBit);

  void setParity(LU_LIN232_PARITY parity);

  void setPortMode(LU_LIN232_PORT_MODE portMode);

  void setStopBit(LU_LIN232_STOP_BITS stopBit);

  void checkUsable() throws PortUnusableException;
  
  void setdiscardMessageCTLow(boolean param);
  

  /**
   * @return
   */
  boolean allowMutipleUser();
}
