/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.port.ui.PortsManagerPage;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;

/**
 *
 */
public class PortPlugin implements IConfigPlugin, IPageFactory{

  private PortPlugin() {
  }


  private final static PortPlugin INSTANCE = new PortPlugin();


  public static PortPlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory factory = ConfigFactory.getInstance();
    
    factory.registerFactory(PortsManager.CONFIG_MODULE_ID,
        new IConfigModuleFactory() {

          @Override
          public IConfigModule create(IConfig owner) {
            return new PortsManagerImpl(owner);

          }
        });
    
    PageFactories.registerFactory(PageFactories.KEY_PORTS , INSTANCE);
    
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof PortsManager) {
      /* Port manager page */
      return new PortsManagerPage((PortsManager) data);
      // PageView view = PortsView.get(man);
      // page = GeneratorPage.get(view, man);

    }
    
    return null;
  }

}
