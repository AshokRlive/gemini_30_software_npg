/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.support;

import javax.swing.JOptionPane;

import com.jgoodies.binding.value.AbstractVetoableValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.rtu.config.port.PortUnusableException;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;

/**
 * A ValueModel that allows to check if a serial port can be used before user
 * setting serial port.
 */
public class SerialPortVetoValueModel extends AbstractVetoableValueModel {

  public SerialPortVetoValueModel(ValueModel subject) {
    super(subject);
  }

  @Override
  public boolean proposedChange(Object oldValue, Object proposedNewValue) {
    ISerialPort newValue = (ISerialPort) proposedNewValue;
    if (newValue != null) {
      try {
        newValue.checkUsable();
      } catch (PortUnusableException e) {
        JOptionPane.showMessageDialog(null, e.getMessage(), "Failure",
            JOptionPane.ERROR_MESSAGE);
        return false;
      }
    }

    return true;
  }
}
