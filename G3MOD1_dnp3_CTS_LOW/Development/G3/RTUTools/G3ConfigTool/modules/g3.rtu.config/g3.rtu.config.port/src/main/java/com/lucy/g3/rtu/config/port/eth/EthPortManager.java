/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import java.util.Collection;

import javax.swing.ListModel;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.port.PortsFinder;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 *
 */
public class EthPortManager extends ItemListManager<IEthernetPort> implements IEthPortManager {

  public EthPortManager() {
    super();

    // Create default Ethernet ports
    add(new EthernetPort(ETHERNET_PORT.ETHERNET_PORT_CONTROL, false));
    add(new EthernetPort(ETHERNET_PORT.ETHERNET_PORT_CONFIG, true));

    // Null element is needed by UI to select non port.
    @SuppressWarnings("unchecked")
    ArrayListModel<IEthernetPort> lm = (ArrayListModel<IEthernetPort>) getItemListModel();
    lm.add(null);
  }

  /** Overload to not allow remove. */
  @Override
  public boolean allowToRemove(IEthernetPort item) {
    return false;
  }

  @Override
  public Collection<IEthernetPort> getEthPorts() {
    return getAllItems();
  }

  @Override
  public ListModel<IEthernetPort> getEthPortsListModel() {
    return getItemListModel();
  }

  @Override
  public IEthernetPort getEthPort(String portName) {
    return PortsFinder.findPort(getEthPorts(), portName);
  }

  @Override
  public IEthernetPort getEthPort(ETHERNET_PORT portEnum) {
    return portEnum == null ? null : getEthPort(portEnum.name());
  }

  /** Overload to allow only custom port to be added. */
  @Override
  public boolean allowToAdd(IEthernetPort item) throws AddItemException {
    if (getEthPort(item.getPortName()) != null) {
      throw new AddItemException(
          String.format("Ethernet port:%s already exists!",
              item.getPortName()));
    }

    return super.allowToAdd(item);
  }
}
