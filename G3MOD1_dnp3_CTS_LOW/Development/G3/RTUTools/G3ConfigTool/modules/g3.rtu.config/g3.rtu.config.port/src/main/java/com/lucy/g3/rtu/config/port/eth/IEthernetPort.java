/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import com.g3schema.ns_port.EthernetPortConfT;
import com.lucy.g3.rtu.config.port.Port;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;

/**
 * The interface of Ethernet port.
 */
public interface IEthernetPort extends Port, IValidation {

  String PROPERTY_CONFIG_ENABLED = "configEnabled";


  boolean isConfigEnabled();

  void setConfigEnabled(boolean configEnabled);

  /**
   * Gets the DHCP config.
   *
   * @return null if DHCP is not supported by this port.
   */
  DHCPConfig getDhcpConfig();

  /**
   * Gets the IP config.
   *
   * @return non-null IPConfig bean.
   */
  IPConfig getIpConfig();

  void readFromXML(EthernetPortConfT xml);

  void writeToXML(EthernetPortConfT xml);
}
