/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import org.apache.log4j.Logger;

import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;

/**
 * The java bean for configuring the DHCP server of Ethernet port.
 */
public final class DHCPConfig extends BeanWithEnable {

  public static final String PROPERTY_STARTIP = "startIP";
  public static final String PROPERTY_ENDIP = "endIP";
  public static final String PROPERTY_SUBMASK = "submask";
  public static final String PROPERTY_ROUTER = "router";

  private Logger log = Logger.getLogger(DHCPConfig.class);

  private String startIP = "0.0.0.0"; // IP Address
  private String endIP = "0.0.0.0"; // IP Address
  private String submask = "0.0.0.0"; // IP Mask
  private String router = "0.0.0.0"; // IP Address


  public String getStartIP() {
    return startIP;
  }

  public void setStartIP(String startIP) {
    if (IPAddress.validateIPAddress(startIP) == false) {
      log.error("Fail to set startIP. Invalid startIP: " + startIP);
      return;
    }

    Object oldValue = this.startIP;
    this.startIP = startIP;
    firePropertyChange(PROPERTY_STARTIP, oldValue, startIP);
  }

  public void setEndIP(String endIP) {
    if (IPAddress.validateIPAddress(endIP) == false) {
      log.error("Fail to set endIP. Invalid endIP: " + endIP);
      return;
    }

    Object oldValue = this.endIP;
    this.endIP = endIP;
    firePropertyChange(PROPERTY_ENDIP, oldValue, endIP);
  }

  public void setSubmask(String submask) {
    if (IPAddress.validateIPAddress(submask) == false) {
      log.error("Fail to set submask. Invalid submask: " + submask);
      return;
    }

    Object oldValue = this.submask;
    this.submask = submask;
    firePropertyChange(PROPERTY_SUBMASK, oldValue, submask);
  }

  public void setRouter(String router) {
    if (IPAddress.validateIPAddress(router, true) == false) {
      log.error("Fail to set router. Invalid router: " + router);
      return;
    }

    Object oldValue = this.router;
    this.router = router;
    firePropertyChange(PROPERTY_ROUTER, oldValue, router);
  }

  public String getEndIP() {
    return endIP;
  }

  public String getSubmask() {
    return submask;
  }

  public String getRouter() {
    return router;
  }

}
