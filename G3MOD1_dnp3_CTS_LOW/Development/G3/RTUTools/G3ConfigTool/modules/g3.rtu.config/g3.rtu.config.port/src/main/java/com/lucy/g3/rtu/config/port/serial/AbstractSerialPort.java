/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import java.util.ArrayList;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.port.AbstractPort;
import com.lucy.g3.rtu.config.port.PortUnusableException;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_BAUD_RATE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_DATA_BITS;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PARITY;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PORT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_STOP_BITS;

/**
 * The basic implementation of serial port.
 */
public abstract class AbstractSerialPort extends AbstractPort implements ISerialPort {

  private Logger log = Logger.getLogger(AbstractSerialPort.class);

  private final String portName;

  private final int maxUserNum;

  private final ArrayListModel<ISerialPortUser> portUsers = new ArrayListModel<>();

  // ====== Configuration Fields ======
  private LU_LIN232_BAUD_RATE baudRate = LU_LIN232_BAUD_RATE.LU_LIN232_BAUD_19200;
  private LU_LIN232_DATA_BITS dataBit = LU_LIN232_DATA_BITS.LU_LIN232_DATA_BITS_8;
  private LU_LIN232_STOP_BITS stopBit = LU_LIN232_STOP_BITS.LU_LIN232_STOP_BITS_1;
  private LU_LIN232_PARITY parity = LU_LIN232_PARITY.LU_LIN232_PARITY_NONE;
  private LU_LIN232_PORT_MODE portMode = LU_LIN232_PORT_MODE.LU_LIN232_MODE_NONE;

  private boolean inUse,ctlowdiscardmsg;


  /**
   * Construct a port.
   *
   * @param portName
   *          the unique name of this port.
   * @param maxUserNum
   *          the maximum number of users that can be connected to this port.
   */
  AbstractSerialPort(String portName, int maxUserNum) {
    Preconditions.checkArgument(maxUserNum >= 0, "limitedUserNum must be >= 0");
    this.portName = Preconditions.checkNotNull(portName, "portEnum is null");
    this.maxUserNum = maxUserNum;

    updateInUse();
    portUsers.addListDataListener(new ListDataListener() {

      @Override
      public void intervalRemoved(ListDataEvent e) {
        updateInUse();
      }

      @Override
      public void intervalAdded(ListDataEvent e) {
        updateInUse();
      }

      @Override
      public void contentsChanged(ListDataEvent e) {
      }
    });
  }

  @Override
  public LU_LIN232_BAUD_RATE getBaudRate() {
    return baudRate;
  }

  @Override
  public void setBaudRate(LU_LIN232_BAUD_RATE baudRate) {
    if (baudRate == null) {
      return;
    }

    Object oldValue = getBaudRate();
    this.baudRate = baudRate;
    firePropertyChange(PROPERTY_BAUDRATE, oldValue, baudRate);
  }

  @Override
  public LU_LIN232_DATA_BITS getDataBit() {
    return dataBit;
  }

  @Override
  public LU_LIN232_STOP_BITS getStopBit() {
    return stopBit;
  }

  @Override
  public void setStopBit(LU_LIN232_STOP_BITS stopBit) {
    if (stopBit == null) {
      return;
    }

    Object oldValue = this.stopBit;
    this.stopBit = stopBit;
    firePropertyChange(PROPERTY_NUMSTOPBITS, oldValue, stopBit);
  }

  @Override
  public LU_LIN232_PARITY getParity() {
    return parity;
  }

  @Override
  public void setParity(LU_LIN232_PARITY parity) {
    if (parity == null) {
      return;
    }
    Object oldValue = this.parity;
    this.parity = parity;
    firePropertyChange(PROPERTY_PARITY, oldValue, parity);
  }

  @Override
  public LU_LIN232_PORT_MODE getPortMode() {
    return portMode;
  }

  @Override
  public void setPortMode(LU_LIN232_PORT_MODE portMode) {
    if (portMode == null) {
      return;
    }
    Object oldValue = this.portMode;
    this.portMode = portMode;
    firePropertyChange(PROPERTY_PORTMODE, oldValue, portMode);
  }

  @Override
  public void setDataBit(LU_LIN232_DATA_BITS dataBit) {
    if (dataBit == null) {
      return;
    }
    Object oldValue = this.dataBit;
    this.dataBit = dataBit;
    firePropertyChange(PROPERTY_NUMDATABITS, oldValue, dataBit);
  }

  @Override
  public String getPortName() {
    return portName;
  }

  private void updateInUse() {
    Object oldValue = isInUse();
    this.inUse = !portUsers.isEmpty();
    firePropertyChange(PROPERTY_IN_USE, oldValue, inUse);

    log.info(String.format("\"%s\" is in use:%s", this.portName, inUse));
  }

  @Override
  public final boolean isInUse() {
    return inUse;
  }

  @Override
  public void connectTo(ISerialPortUser portuser) throws PortUnusableException {
    if (portuser == null) {
      return;
    }

    checkUsable();

    if (!portUsers.contains(portuser)) {
      portUsers.add(portuser);
    }
  }

  @Override
  public void checkUsable() throws PortUnusableException {
    String exceptMsg = null;
    if (portUsers.size() >= maxUserNum && !allowMutipleUser()) {
      exceptMsg = String.format(
          "Cannot use serial port \"%s\".\nIt is currently being used by \"%s\"!",
          getDisplayName(), portUsers.get(0).getPortUserName());
    }

    if (!isEnabled()) {
      exceptMsg = String.format("Cannot use serial port \"%s\".\n It is currently disabled!", getDisplayName());
    }

    if (exceptMsg != null) {
      throw new PortUnusableException(exceptMsg);
    }
  }

  @Override
  public boolean allowMutipleUser() {
    return getPortEnum() == SERIAL_PORT.SERIAL_PORT_RS485;
  }

  @Override
  public void disconnectFrom(ISerialPortUser portuser) {
    portUsers.remove(portuser);
  }

  @Override
  public boolean isUsedBy(ISerialPortUser user) {
    return portUsers.contains(user);
  }

  @Override
  public void setEnabled(boolean enabled) {
    /* Disconnect all port users from this port. */
    if (enabled == false) {
      ArrayList<ISerialPortUser> allUsers = new ArrayList<ISerialPortUser>(portUsers);

      for (ISerialPortUser user : allUsers) {
        try {
          user.setSerialPort(null);
        } catch (Exception e) {
          // No exception expected
          e.printStackTrace();
        }
      }
    }

    super.setEnabled(enabled);
  }
  
  @Override
  public boolean getdiscardMessageCTLow(){
	  return ctlowdiscardmsg;
  }
  
  @Override
	public void setdiscardMessageCTLow(boolean param) {
		// TODO Auto-generated method stub
	    Object oldValue = this.ctlowdiscardmsg;
	    this.ctlowdiscardmsg = param;
	    firePropertyChange(PROPERTY_DISCARDMSG_CTLOW, oldValue, param);
	  
	}

}
