/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import com.lucy.g3.rtu.config.shared.model.BeanWithEnable;

/**
 * The basic implementation of Port.
 */
public abstract class AbstractPort extends BeanWithEnable implements Port {

  public AbstractPort() {
    super.setEnabled(true);// Enable port by default
  }

  @Override
  public final String toString() {
    return getDisplayName();
  }

}
