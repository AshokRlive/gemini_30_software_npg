/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import org.apache.log4j.Logger;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.network.support.IPAddress;

/**
 * The java bean for configuring the IP of Ethernet port.
 */
public final class IPConfig extends Model {

  // @formatter:off
  public static final String PROPERTY_DYNAMIC_IP  = "dynamicIP";
  public static final String PROPERTY_IP_ADDRESS  = "ipAddress";
  public static final String PROPERTY_SUBMASK     = "submask";
  public static final String PROPERTY_GATEWAY     = "gateway";
  public static final String PROPERTY_BROADCAST   = "broadcast"; // Not supported yet.
  public static final String PROPERTY_DNSSERVER   = "dnsServer"; // Not supported yet.
  public static final String PROPERTY_DNSSERVER2  = "dnsServer2";// Not supported yet.
  // @formatter:on

  private Logger log = Logger.getLogger(IPConfig.class);

  private final String portName;

  // ====== Dynamic IP ======
  private boolean dynamicIP = false;

  // ====== Static IP ======
  private String ipAddress = "0.0.0.0";
  private String submask = "255.255.0.0";
  private String broadcast = "255.255.255.255";
  private String gateway = "";
  private String dnsServer = "";
  private String dnsServer2 = "";


  public IPConfig(String portName) {
    this.portName = portName;
  }

  public boolean isDynamicIP() {
    return dynamicIP;
  }

  public void setDynamicIP(boolean dynamicIP) {
    Object oldValue = isDynamicIP();
    this.dynamicIP = dynamicIP;
    firePropertyChange(PROPERTY_DYNAMIC_IP, oldValue, dynamicIP);
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    if (IPAddress.validateIPAddress(ipAddress) == false) {
      log.error("Fail to set IP address. Invalid IP address: " + ipAddress);
      return;
    }

    Object oldValue = getIpAddress();
    this.ipAddress = ipAddress.trim();
    firePropertyChange(PROPERTY_IP_ADDRESS, oldValue, this.ipAddress);

  }

  public String getSubmask() {
    return submask;
  }

  public void setSubmask(String submask) {
    if (IPAddress.validateIPAddress(submask) == false) {
      log.error("Fail to set subMask. Invalid submask: " + submask);
      return;
    }

    Object oldValue = getSubmask();
    this.submask = submask.trim();
    firePropertyChange(PROPERTY_SUBMASK, oldValue, this.submask);
  }

  public String getBroadcast() {
    return broadcast;
  }

  public void setBroadcast(String broadcast) {
    if (IPAddress.validateIPAddress(broadcast) == false) {
      log.error("Fail to set broadcast. Invalid broadcast: " + broadcast);
      return;
    }

    Object oldValue = getBroadcast();
    this.broadcast = broadcast.trim();
    firePropertyChange(PROPERTY_BROADCAST, oldValue, this.broadcast);
  }

  public String getGateway() {
    return gateway;
  }

  public void setGateway(String gateway) {
    if (IPAddress.validateIPAddress(gateway, true) == false) {
      log.error("Fail to set gateway. Invalid gateway: " + gateway);
      return;
    }

    Object oldValue = getGateway();
    this.gateway = gateway.trim();
    firePropertyChange(PROPERTY_GATEWAY, oldValue, this.gateway);
  }

  public String getDnsServer() {
    return dnsServer;
  }

  public void setDnsServer(String dnsServer) {
    if (IPAddress.validateIPAddress(dnsServer, true) == false) {
      log.error("Fail to set dnsServer. Invalid dnsServer: " + dnsServer);
      return;
    }

    Object oldValue = getDnsServer();
    this.dnsServer = dnsServer.trim();
    firePropertyChange(PROPERTY_DNSSERVER, oldValue, this.dnsServer);
  }

  public String getDnsServer2() {
    return dnsServer2;
  }

  public void setDnsServer2(String dnsServer2) {
    if (IPAddress.validateIPAddress(dnsServer2, true) == false) {
      log.error("Fail to set dnsServer2. Invalid dnsServer2: " + dnsServer2);
      return;
    }

    Object oldValue = getDnsServer2();
    this.dnsServer2 = dnsServer2.trim();
    firePropertyChange(PROPERTY_DNSSERVER2, oldValue, this.dnsServer2);
  }

  @Override
  public String toString() {
    return portName;
  }
}
