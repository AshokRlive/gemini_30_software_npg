/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.swing.ListModel;

import com.lucy.g3.itemlist.ItemListManager;
import com.lucy.g3.rtu.config.port.PortsFinder;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;

/**
 *
 */
public class SerialPortManager extends ItemListManager<ISerialPort> implements ISerialPortManager {

  public SerialPortManager() {
    super();

    // Create default Serial ports
    SERIAL_PORT[] portEnums = SERIAL_PORT.values();
    for (int i = 0; i < portEnums.length; i++) {
      add(new SerialPort(portEnums[i]));
    }

//    // Null element is needed by UI to select non port.
//    @SuppressWarnings("unchecked")
//    ArrayListModel<IEthernetPort> lm = (ArrayListModel<IEthernetPort>) getItemListModel();
//    lm.add(null);
  }

  @Override
  public boolean allowToAdd(ISerialPort item) throws AddItemException {
    if (getSerialPort(item.getPortName()) != null) {
      throw new AddItemException(
          String.format("Serial port:%s already exists!",
              item.getPortName()));
    }
    return super.allowToAdd(item);
  }

  /** Overload to allow only custom port to be removed. */
  @Override
  public boolean allowToRemove(ISerialPort item) {
    return item != null && item instanceof CustomSerialPort;
  }

  @Override
  public ISerialPort getSerialPort(String portName) {
    return PortsFinder.findPort(getSerialPorts(), portName);
  }

  @Override
  public Collection<ISerialPort> getSerialPorts() {
    Collection<ISerialPort> ret = getAllItems();
    ret.removeAll(Collections.singleton(null)); // remove null previously added
    return ret;
  }
  
  @Override
  public Collection<ISerialPort> getSerialPortsNotInUse() {
    Collection<ISerialPort> ret = getAllItems();
    ret.removeAll(Collections.singleton(null)); // remove null previously added
    
    ArrayList<ISerialPort> inUse = new ArrayList<>();  
    for (ISerialPort s : ret) {
      if(s.isInUse())
        inUse.add(s);
    }
    
    ret.removeAll(inUse);
    
    return ret;
  }

  @Override
  public ListModel<ISerialPort> getSerialPortsListModel() {
    return getItemListModel();
  }

  @Override
  public void addCustomSerialPort(CustomSerialPort customPort) throws AddItemException {
    add(customPort);
  }

  @Override
  public boolean removeCustomSerialPort(CustomSerialPort customPort) {
    return remove(customPort);
  }

}
