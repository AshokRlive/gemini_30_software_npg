/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import java.beans.PropertyVetoException;

/**
 * The Interface SerialPort User. The configuration item that needs to use
 * serial port should implement this interface.
 */
public interface ISerialPortUser {

  /**
   * The name of read-only property {@value} . Type: <code>ISerialPort</code>.
   */
  String PROPERTY_SERIAL_PORT = "serialPort";


  ISerialPort getSerialPort();

  void setSerialPort(ISerialPort port) throws PropertyVetoException;

  /**
   * Gets the name of this serial port user.
   *
   * @return
   */
  String getPortUserName();
}
