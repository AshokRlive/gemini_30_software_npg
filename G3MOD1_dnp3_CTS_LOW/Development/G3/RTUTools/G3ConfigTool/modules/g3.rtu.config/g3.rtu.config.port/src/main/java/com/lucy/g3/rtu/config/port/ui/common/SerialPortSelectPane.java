/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui.common;

import java.awt.BorderLayout;

import javax.swing.ListModel;

import org.jdesktop.swingx.JXTaskPane;

import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
public class SerialPortSelectPane extends JXTaskPane {
  
  public SerialPortSelectPane() {
    panelSerial = new SerialPortSelectPanel();
    initComponents();
  }
  
  public SerialPortSelectPane(ValueModel serialPortValueHolder, ListModel<ISerialPort> serialPortList) {
    panelSerial = new SerialPortSelectPanel(serialPortValueHolder, serialPortList);
    initComponents();
  }


  private void createUIComponents() {
    // Nothing to do.
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    //======== this ========
    setTitle("Serial Port");
    setLayout(new BorderLayout());

    //---- panelSerial ----
    panelSerial.setOpaque(false);
    add(panelSerial, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private SerialPortSelectPanel panelSerial;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
