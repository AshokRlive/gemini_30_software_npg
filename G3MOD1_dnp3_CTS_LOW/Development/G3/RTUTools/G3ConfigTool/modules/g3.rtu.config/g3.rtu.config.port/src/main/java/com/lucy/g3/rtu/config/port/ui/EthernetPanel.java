/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.IPAddressField;
import com.lucy.g3.rtu.config.port.eth.DHCPConfig;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.eth.IPConfig;

public class EthernetPanel extends JPanel {

  // @formatter:off
  private final PresentationModel<IEthernetPort> pmEthPort   = new PresentationModel<>();
  private final PresentationModel<IPConfig>     pmIPConfig   = new PresentationModel<>();
  private final PresentationModel<DHCPConfig>   pmDHCPConfig = new PresentationModel<>();
  // @formatter:on

  private final boolean supportDHCP;


  public EthernetPanel(IEthernetPort port) {
    if (port != null) {
      pmEthPort.setBean(port);
      pmIPConfig.setBean(port.getIpConfig());
      pmDHCPConfig.setBean(port.getDhcpConfig());
    }

    supportDHCP = pmDHCPConfig.getBean() != null;

    initComponents();
    initComponentsBinding();

    // Hide DHCP panel if not supported
    panelDHCP.setVisible(supportDHCP);

    // Temporarily hide unsupported configuration by MCM
    cboxDisablePort.setVisible(false);
  }

  private void bindComponentsStates() {
    if (pmEthPort.getBean() == null)
    {
      return; // State cannot be bound cause bean is null.
    }

    // @formatter:off
    ValueModel vm;

    // Configure
    vm = pmEthPort.getModel(IEthernetPort.PROPERTY_CONFIG_ENABLED);
    PropertyConnector.connectAndUpdate(vm, panelConfig, "visible");

    // Enable port
    vm = pmEthPort.getModel(IEthernetPort.PROPERTY_ENABLED);
    PropertyConnector.connectAndUpdate(vm, panelIP,   "visible");

    // Static IP
    vm = ConverterFactory.createBooleanNegator(pmIPConfig.getModel(IPConfig.PROPERTY_DYNAMIC_IP));
    PropertyConnector.connectAndUpdate(vm, ftfIPConfigAddress, "enabled");
    PropertyConnector.connectAndUpdate(vm, ftfIPConfigSubmask, "enabled");
    PropertyConnector.connectAndUpdate(vm, ftfIPConfigGateway, "enabled");

    // DHCP
    if(supportDHCP){
      PropertyConnector.connectAndUpdate(vm, panelDHCP, "visible");

      vm = pmDHCPConfig.getModel(DHCPConfig.PROPERTY_ENABLED);
      PropertyConnector.connectAndUpdate(vm, ftfDHCPStartIP, "enabled");
      PropertyConnector.connectAndUpdate(vm, ftfDHCPEndIP  , "enabled");
      PropertyConnector.connectAndUpdate(vm, ftfDHCPSubmask, "enabled");
      PropertyConnector.connectAndUpdate(vm, ftfDHCPRouter , "enabled");
    }

    // @formatter:on
  }

  private void initComponentsBinding() {
    ValueModel vm;
    // Configure
    Bindings.bind(cboxEnableConfig, pmEthPort.getModel(IEthernetPort.PROPERTY_CONFIG_ENABLED));

    // Disable port
    vm = ConverterFactory.createBooleanNegator(pmEthPort.getModel(IEthernetPort.PROPERTY_ENABLED));
    Bindings.bind(cboxDisablePort, vm);

    // Dynamic IP
    vm = pmIPConfig.getModel(IPConfig.PROPERTY_DYNAMIC_IP);
    Bindings.bind(radioBtnDynamic, vm, true, false);

    // Static IP
    Bindings.bind(radioBtnStatic, vm, false, true);
    ftfIPConfigAddress.bindTo(pmIPConfig.getModel(IPConfig.PROPERTY_IP_ADDRESS));
    ftfIPConfigSubmask.bindTo(pmIPConfig.getModel(IPConfig.PROPERTY_SUBMASK));
    ftfIPConfigGateway.bindTo(pmIPConfig.getModel(IPConfig.PROPERTY_GATEWAY));

    // DHCP
    if (supportDHCP) {
      Bindings.bind(cboxEnableDHCP, pmDHCPConfig.getModel(DHCPConfig.PROPERTY_ENABLED));
      ftfDHCPStartIP.bindTo(pmDHCPConfig.getModel(DHCPConfig.PROPERTY_STARTIP));
      ftfDHCPEndIP.bindTo(pmDHCPConfig.getModel(DHCPConfig.PROPERTY_ENDIP));
      ftfDHCPSubmask.bindTo(pmDHCPConfig.getModel(DHCPConfig.PROPERTY_SUBMASK));
      ftfDHCPRouter.bindTo(pmDHCPConfig.getModel(DHCPConfig.PROPERTY_ROUTER));
    }

    bindComponentsStates();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    cboxEnableConfig = new JCheckBox();
    panelConfig = new JPanel();
    cboxDisablePort = new JCheckBox();
    panelIP = new JPanel();
    radioBtnDynamic = new JRadioButton();
    radioBtnStatic = new JRadioButton();
    JLabel label1 = new JLabel();
    ftfIPConfigAddress = new IPAddressField();
    JLabel label2 = new JLabel();
    ftfIPConfigSubmask = new IPAddressField();
    JLabel label3 = new JLabel();
    ftfIPConfigGateway = new IPAddressField();
    panelDHCP = new JPanel();
    cboxEnableDHCP = new JCheckBox();
    JLabel label4 = new JLabel();
    ftfDHCPStartIP = new IPAddressField();
    JLabel label7 = new JLabel();
    ftfDHCPEndIP = new IPAddressField();
    JLabel label8 = new JLabel();
    ftfDHCPSubmask = new IPAddressField();
    JLabel label5 = new JLabel();
    ftfDHCPRouter = new IPAddressField();

    // ======== this ========
    setLayout(new FormLayout(
        "default:grow",
        "default, $lgap, default"));

    // ---- cboxEnableConfig ----
    cboxEnableConfig.setText("Configure");
    add(cboxEnableConfig, CC.xy(1, 1));

    // ======== panelConfig ========
    {
      panelConfig.setLayout(new FormLayout(
          "default:grow",
          "2*(default, $pgap), default"));

      // ---- cboxDisablePort ----
      cboxDisablePort.setText("Disable this port");
      panelConfig.add(cboxDisablePort, CC.xy(1, 1));

      // ======== panelIP ========
      {
        panelIP.setBorder(new CompoundBorder(
            new TitledBorder("IP Settings"),
            Borders.DLU2_BORDER));
        panelIP.setLayout(new FormLayout(
            "right:default, $lcgap, default, $lcgap, [80dlu,default]",
            "default, $rgap, 3*(default, $lgap), default"));

        // ---- radioBtnDynamic ----
        radioBtnDynamic.setText("Dynamic IP");
        panelIP.add(radioBtnDynamic, CC.xywh(1, 1, 5, 1));

        // ---- radioBtnStatic ----
        radioBtnStatic.setText("Static IP");
        panelIP.add(radioBtnStatic, CC.xywh(1, 3, 5, 1));

        // ---- label1 ----
        label1.setText("IP Address:");
        panelIP.add(label1, CC.xy(3, 5));
        panelIP.add(ftfIPConfigAddress, CC.xy(5, 5));

        // ---- label2 ----
        label2.setText("Submask:");
        panelIP.add(label2, CC.xy(3, 7));
        panelIP.add(ftfIPConfigSubmask, CC.xy(5, 7));

        // ---- label3 ----
        label3.setText("Gateway:");
        panelIP.add(label3, CC.xy(3, 9));

        // ---- ftfIPConfigGateway ----
        ftfIPConfigGateway.setAllowBlank(true);
        panelIP.add(ftfIPConfigGateway, CC.xy(5, 9));
      }
      panelConfig.add(panelIP, CC.xy(1, 3));

      // ======== panelDHCP ========
      {
        panelDHCP.setBorder(new CompoundBorder(
            new TitledBorder("DHCP Server"),
            Borders.DLU2_BORDER));
        panelDHCP.setLayout(new FormLayout(
            "right:default, 2*($lcgap, default)",
            "default, $ugap, 3*(default, $lgap), default"));

        // ---- cboxEnableDHCP ----
        cboxEnableDHCP.setText("Enable DHCP Server");
        panelDHCP.add(cboxEnableDHCP, CC.xywh(1, 1, 5, 1));

        // ---- label4 ----
        label4.setText("Start IP:");
        panelDHCP.add(label4, CC.xy(3, 3));
        panelDHCP.add(ftfDHCPStartIP, CC.xy(5, 3));

        // ---- label7 ----
        label7.setText("End IP:");
        panelDHCP.add(label7, CC.xy(3, 5));
        panelDHCP.add(ftfDHCPEndIP, CC.xy(5, 5));

        // ---- label8 ----
        label8.setText("Submask:");
        panelDHCP.add(label8, CC.xy(3, 7));
        panelDHCP.add(ftfDHCPSubmask, CC.xy(5, 7));

        // ---- label5 ----
        label5.setText("Router:");
        panelDHCP.add(label5, CC.xy(3, 9));

        // ---- ftfDHCPRouter ----
        ftfDHCPRouter.setAllowBlank(true);
        panelDHCP.add(ftfDHCPRouter, CC.xy(5, 9));
      }
      panelConfig.add(panelDHCP, CC.xy(1, 5));
    }
    add(panelConfig, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox cboxEnableConfig;
  private JPanel panelConfig;
  private JCheckBox cboxDisablePort;
  private JPanel panelIP;
  private JRadioButton radioBtnDynamic;
  private JRadioButton radioBtnStatic;
  private IPAddressField ftfIPConfigAddress;
  private IPAddressField ftfIPConfigSubmask;
  private IPAddressField ftfIPConfigGateway;
  private JPanel panelDHCP;
  private JCheckBox cboxEnableDHCP;
  private IPAddressField ftfDHCPStartIP;
  private IPAddressField ftfDHCPEndIP;
  private IPAddressField ftfDHCPSubmask;
  private IPAddressField ftfDHCPRouter;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
