/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

/**
 * This exception will be thrown when trying to connect a device to a port which
 * is not allowed to be connected.
 */
public class PortUnusableException extends Exception {

  public PortUnusableException(String message) {
    super(message);
  }

}
