/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.port.serial.CustomSerialPort;
import com.lucy.g3.rtu.config.port.serial.SerialPortManager;

/**
 * The Class PortsManagerImplTest.
 */
public class SerialPortManagerTest {

  private SerialPortManager fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new SerialPortManager();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddRemoveCustomPort() {
    final CustomSerialPort custom = new CustomSerialPort("", "");

    assertFalse(fixture.contains(custom));

    fixture.add(custom);
    assertTrue(fixture.contains(custom));

    fixture.remove(custom);
    assertFalse(fixture.contains(custom));
  }

}
