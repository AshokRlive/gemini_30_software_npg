/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import java.util.Collection;

import com.lucy.g3.rtu.config.port.eth.IEthPortManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPortManager;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * Manages a list of ports and their configuration.
 */
public interface PortsManager extends IConfigModule, IContainerValidation {

  String CONFIG_MODULE_ID = "PortsManager";

  IEthPortManager getEthManager();

  ISerialPortManager getSerialManager();

  ISerialPort getSerialPort(String name);

  IEthernetPort getEthPort(String name);

  Collection<IEthernetPort> getEthPorts();

  Collection<ISerialPort> getSerialPorts();

  IEthernetPort getEthPort(ETHERNET_PORT ethEnum);

  @Override
  IContainerValidator getValidator();
}
