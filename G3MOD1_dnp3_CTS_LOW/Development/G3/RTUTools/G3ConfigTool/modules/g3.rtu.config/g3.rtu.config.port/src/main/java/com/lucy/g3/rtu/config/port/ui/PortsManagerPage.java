/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui;

import java.awt.BorderLayout;
import java.util.Collection;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.jgoodies.forms.factories.Borders;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * The page for configuring RTU Ports.
 */
public class PortsManagerPage extends AbstractConfigPage {

  private PortsManager manager;
  private JTabbedPane tabbedPane;


  public PortsManagerPage(PortsManager manager) {
    super(manager);
    setNodeName("Ports");
    this.manager = manager;
  }

  @Override
  protected void init() throws Exception {
    tabbedPane = new JTabbedPane();
    add(tabbedPane, BorderLayout.CENTER);

    // Ethernet Ports
    Collection<IEthernetPort> eths = manager.getEthPorts();
    for (IEthernetPort eth : eths) {
      tabbedPane.addTab(eth.getDisplayName(), decoratePanel(new EthernetPanel(eth)));
    }

    // Serial Ports
    Collection<ISerialPort> serials = manager.getSerialPorts();
    for (ISerialPort serial : serials) {
      tabbedPane.addTab(serial.getDisplayName(), decoratePanel(new SerialPortPanel(serial)));
    }
  }

  private JPanel decoratePanel(JPanel panel) {
    panel.setBorder(Borders.DIALOG_BORDER);
    return panel;
  }

}
