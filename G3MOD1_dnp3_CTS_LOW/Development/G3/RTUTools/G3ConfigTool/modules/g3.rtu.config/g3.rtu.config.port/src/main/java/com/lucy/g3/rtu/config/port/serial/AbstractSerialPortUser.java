/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.port.PortUnusableException;

/**
 * The basic implementation of serial port user.
 */
public abstract class AbstractSerialPortUser extends Model implements ISerialPortUser {

  private ISerialPort serialport;


  @Override
  public ISerialPort getSerialPort() {
    return serialport;
  }

  @Override
  public void setSerialPort(ISerialPort newPort) throws PropertyVetoException {
    ISerialPort oldPort = getSerialPort();
    fireVetoableChange(PROPERTY_SERIAL_PORT, oldPort, newPort);

    // Try to connect to a new port
    if (newPort != null) {
      try {
        newPort.connectTo(this);
      } catch (PortUnusableException e) {
        // Change port to null if connect action fails
        throw new PropertyVetoException(e.getMessage(),
            new PropertyChangeEvent(this, PROPERTY_SERIAL_PORT, oldPort, newPort));
      }
    }

    // Disconnect from old port
    if (oldPort != null) {
      oldPort.disconnectFrom(this);
    }

    // Set value & fire event.
    this.serialport = newPort;
    firePropertyChange(PROPERTY_SERIAL_PORT, oldPort, newPort);
  }
}
