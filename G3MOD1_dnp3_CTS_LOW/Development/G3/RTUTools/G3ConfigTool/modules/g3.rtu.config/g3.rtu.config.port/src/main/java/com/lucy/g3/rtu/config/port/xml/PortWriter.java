/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.xml;

import java.util.Collection;

import com.g3schema.ns_port.PortsT;
import com.g3schema.ns_port.SerialPortConfT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.common.utils.EnumUtils;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;

/**
 *
 *
 */
public class PortWriter {

  private final PortsManager portMgr;


  public PortWriter(PortsManager portMgr, ValidationResult result) {
    // super(data, result);
    this.portMgr = portMgr;
  }

  public void write(PortsT xml) {
    // Export Ethernet ports
    Collection<IEthernetPort> eths = portMgr.getEthPorts();
    for (IEthernetPort eth : eths) {
      if (eth.isConfigEnabled()) {
        eth.writeToXML(xml.ethernet.append());
      }
    }

    // Export serial ports
    Collection<ISerialPort> serials = portMgr.getSerialPorts();
    for (ISerialPort serial : serials) {
      exportSerialPort(serial, xml.serial.append());
    }
  }

  private void exportSerialPort(ISerialPort serial, SerialPortConfT xml_serial) {
    xml_serial.enabled.setValue(serial.isEnabled());
    xml_serial.portName.setValue(serial.getPortName());
    xml_serial.portPath.setValue(serial.getPortPath());
    xml_serial.baudRate.setValue(EnumUtils.getStringFromEnum(serial.getBaudRate()));
    xml_serial.numDataBits.setValue(EnumUtils.getStringFromEnum(serial.getDataBit()));
    xml_serial.numStopBits.setValue(EnumUtils.getStringFromEnum(serial.getStopBit()));
    xml_serial.parity.setValue(EnumUtils.getStringFromEnum(serial.getParity()));
    xml_serial.portMode.setValue(EnumUtils.getStringFromEnum(serial.getPortMode()));
    xml_serial.ctLowDiscardMsg.setValue(serial.getdiscardMessageCTLow());
  }

}
