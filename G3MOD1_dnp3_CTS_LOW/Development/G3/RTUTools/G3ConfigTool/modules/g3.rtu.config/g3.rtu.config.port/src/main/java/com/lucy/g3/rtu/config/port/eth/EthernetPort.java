/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import org.apache.log4j.Logger;

import com.g3schema.ns_port.DhcpServerT;
import com.g3schema.ns_port.EthernetPortConfT;
import com.g3schema.ns_port.Ipv4SettingT;
import com.g3schema.ns_port.StaticIpConfT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.rtu.config.port.AbstractPort;
import com.lucy.g3.rtu.config.port.validation.EthernetPortValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * The Class Ethernet Port.
 */
public final class EthernetPort extends AbstractPort implements IEthernetPort {

  private Logger log = Logger.getLogger(EthernetPort.class);

  private final DHCPConfig dhcpConfig;
  private final IPConfig ipConfig;

  private final ETHERNET_PORT portEnum;

  private boolean configEnabled;

  private final IValidator validator;


  EthernetPort(ETHERNET_PORT portEnum, boolean supportDHCP) {
    this.portEnum = Preconditions.checkNotNull(portEnum, "port enum is null");
    this.validator = new EthernetPortValidator(this);
    this.ipConfig = new IPConfig(getDisplayName());
    this.dhcpConfig = supportDHCP ? new DHCPConfig() : null;

    LoggingUtil.logPropertyChanges(ipConfig, log);
  }

  @Override
  public String getPortName() {
    return portEnum.name();
  }

  @Override
  public String getDisplayName() {
    return portEnum.getDescription();
  }

  @Override
  public DHCPConfig getDhcpConfig() {
    return dhcpConfig;
  }

  @Override
  public IPConfig getIpConfig() {
    return ipConfig;
  }

  @Override
  public boolean isConfigEnabled() {
    return configEnabled;
  }

  @Override
  public void setConfigEnabled(boolean configEnabled) {
    Object oldValue = this.configEnabled;
    this.configEnabled = configEnabled;
    firePropertyChange(PROPERTY_CONFIG_ENABLED, oldValue, configEnabled);
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    log.info(getDisplayName() + "-> set ethernet port enable state:" + enabled);
  }

  @Override
  public void readFromXML(EthernetPortConfT xml) {
    setEnabled(xml.enabled.getValue());
    setConfigEnabled(true);

    if (xml.ipv4Setting.exists()) {
      Ipv4SettingT xml_ipv4 = xml.ipv4Setting.first();
      IPConfig ipconf = getIpConfig();
      if (xml_ipv4.staticIP.exists()) {
        ipconf.setDynamicIP(false);

        StaticIpConfT xml_static = xml_ipv4.staticIP.first();
        ipconf.setIpAddress(xml_static.ipaddress.getValue());
        ipconf.setSubmask(xml_static.submask.getValue());
        ipconf.setBroadcast(xml_static.broadcast.getValue());
        ipconf.setGateway(xml_static.gateway.getValue());
        ipconf.setDnsServer(xml_static.DNSServer.getValue());
        if (xml_static.DNSServer2.exists()) {
          ipconf.setDnsServer2(xml_static.DNSServer2.getValue());
        }
      } else {
        ipconf.setDynamicIP(true);
      }

      // DHCP
      DHCPConfig dhcpconf = getDhcpConfig();
      if (dhcpconf != null) {
        if (xml_ipv4.dhcpServer.exists()) {
          dhcpconf.setEnabled(true);
          DhcpServerT xml_dhcp = xml_ipv4.dhcpServer.first();
          dhcpconf.setStartIP(xml_dhcp.startIP.getValue());
          dhcpconf.setEndIP(xml_dhcp.endIP.getValue());
          dhcpconf.setRouter(xml_dhcp.router.getValue());
          dhcpconf.setSubmask(xml_dhcp.submask.getValue());
        } else {
          dhcpconf.setEnabled(false);
        }
      }
    } else {
      log.warn("Fail to read DHCP configuration. DHCP not supported by: " + this);
    }

  }

  @Override
  public void writeToXML(EthernetPortConfT xml) {
    // Attributes
    xml.enabled.setValue(isEnabled());
    xml.portName.setValue(getPortName());

    Ipv4SettingT xml_ipv4 = xml.ipv4Setting.append();
    IPConfig ipconf = getIpConfig();

    if (ipconf.isDynamicIP()) {
      // Dynamic IP
      xml_ipv4.dynamicIP.append();

    } else {
      // Static IP
      StaticIpConfT xml_static = xml_ipv4.staticIP.append();
      xml_static.ipaddress.setValue(ipconf.getIpAddress());
      xml_static.submask.setValue(ipconf.getSubmask());
      xml_static.broadcast.setValue(ipconf.getBroadcast());
      xml_static.gateway.setValue(ipconf.getGateway());
      xml_static.DNSServer.setValue(ipconf.getDnsServer());

      // dns2
      String dns2 = ipconf.getDnsServer2();
      if (!Strings.isBlank(dns2)) {
        xml_static.DNSServer2.setValue(dns2);
      }
    }

    // DHCP server
    DHCPConfig dhcpconf = getDhcpConfig();
    if (dhcpconf != null && getDhcpConfig().isEnabled()) {
      DhcpServerT xml_dhcp = xml_ipv4.dhcpServer.append();
      xml_dhcp.startIP.setValue(dhcpconf.getStartIP());
      xml_dhcp.endIP.setValue(dhcpconf.getEndIP());
      xml_dhcp.router.setValue(dhcpconf.getRouter());
      xml_dhcp.submask.setValue(dhcpconf.getSubmask());
    }

  }

  @Override
  public IValidator getValidator() {
    return validator;
  }

}
