/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui.common;

import javax.swing.*;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.binder.Binders;
import com.jgoodies.binding.binder.PresentationModelBinder;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.ObjectToStringConverter;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.port.support.SerialPortVetoValueModel;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PORT_MODE;

/**
 * A panel for selecting serial port.
 */
public class SerialPortSelectPanel extends JPanel {

  private final ListModel<ISerialPort> serialPortList;

  private final PresentationModel<ISerialPort> serialPortModel = new PresentationModel<>();
  private final ValueModel serialPortValueHolder;


  /**
   * Private constructor required by JFormDesigner.
   */
  SerialPortSelectPanel() {
    super();
    this.serialPortList = new DefaultListModel<>();
    this.serialPortValueHolder = new ValueHolder();
    initComponents();
  }

  public SerialPortSelectPanel(ValueModel serialPortValueHolder, ListModel<ISerialPort> serialPortList) {
    super();
    this.serialPortList = serialPortList;
    this.serialPortValueHolder = serialPortValueHolder;

    initComponents();
    initComponentsBinding();
  }

  private void initComponentsBinding() {
    // Bind serial port model to serial port value holder
    PropertyConnector.connectAndUpdate(serialPortValueHolder, serialPortModel, PresentationModel.PROPERTY_BEAN);

    // Bind combobox
    @SuppressWarnings("unchecked")
    ComboBoxModel<Object> comboModel = new ComboBoxAdapter<ISerialPort>(serialPortList,
        new SerialPortVetoValueModel(serialPortValueHolder));
    //comboSerilPort.setModel(comboModel);
    Bindings.bind(comboSerilPort, comboModel, "Not selected");

    // Bind labels
    ObjectToStringConverter converter = new ObjectToStringConverter("");
    PresentationModelBinder binder = Binders.binderFor(serialPortModel);

    binder.bindBeanProperty(ISerialPort.PROPERTY_BAUDRATE).converted(converter).to(lblBaudRate);
    binder.bindBeanProperty(ISerialPort.PROPERTY_NUMDATABITS).converted(converter).to(lblDatabits);
    binder.bindBeanProperty(ISerialPort.PROPERTY_PARITY).converted(converter).to(lblParity);
    binder.bindBeanProperty(ISerialPort.PROPERTY_PORTMODE).converted(converter).to(lblPortmode);
    binder.bindBeanProperty(ISerialPort.PROPERTY_NUMSTOPBITS).converted(converter).to(lblStopbits);
    binder.bindBeanProperty(ISerialPort.PROPERTY_BAUDRATE).converted(converter).to(lblBaudRate);
   
    Bindings.bind(enabledisablectslow, "visible", new ConverterValueModel(serialPortModel.getModel(ISerialPort.PROPERTY_PORTMODE), new PortConverter()));
    
   /* AbstractValueModel vm = serialPortModel.getModel(ISerialPort.PROPERTY_DISCARDMSG_CTLOW);
    Bindings.bind(enabledisablectslow, vm);
    PropertyConnector.connectAndUpdate(vm, serialPortModel, "enabled");*/
    
    Bindings.bind(enabledisablectslow, serialPortModel.getModel(ISerialPort.PROPERTY_DISCARDMSG_CTLOW));
    
  }
  
  private static class PortConverter implements BindingConverter<LU_LIN232_PORT_MODE, Boolean> {

	    @Override
	    public LU_LIN232_PORT_MODE sourceValue(Boolean arg0) {
	      return null;
	    }

	    @Override
	    public Boolean targetValue(LU_LIN232_PORT_MODE sourceValue) {
	    	// TODO Auto-generated method stub
			 if (sourceValue == null) {
			      return false;
			    }

			    return sourceValue.getDescription() == "Hardware" ? true : false;
	    }
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
	JLabel label7 = new JLabel();
	comboSerilPort = new JComboBox<>();
	JLabel label1 = new JLabel();
	lblBaudRate = new JLabel();
	JLabel label2 = new JLabel();
	lblDatabits = new JLabel();
	JLabel label3 = new JLabel();
	lblStopbits = new JLabel();
	JLabel label4 = new JLabel();
	lblParity = new JLabel();
	JLabel label6 = new JLabel();
	lblPortmode = new JLabel();
	enabledisablectslow = new JCheckBox();

	//======== this ========
	setLayout(new FormLayout(
		"right:[50dlu,default], $lcgap, [100dlu,default]",
		"default, 5*($ugap, fill:default), $lgap, default"));

	//---- label7 ----
	label7.setText("Serial Port:");
	add(label7, CC.xy(1, 1));
	add(comboSerilPort, CC.xy(3, 1));

	//---- label1 ----
	label1.setText("Baud Rate:");
	add(label1, CC.xy(1, 3));

	//---- lblBaudRate ----
	lblBaudRate.setEnabled(false);
	add(lblBaudRate, CC.xy(3, 3));

	//---- label2 ----
	label2.setText("Data Bits:");
	add(label2, CC.xy(1, 5));

	//---- lblDatabits ----
	lblDatabits.setEnabled(false);
	add(lblDatabits, CC.xy(3, 5));

	//---- label3 ----
	label3.setText("Stop Bits:");
	add(label3, CC.xy(1, 7));

	//---- lblStopbits ----
	lblStopbits.setEnabled(false);
	add(lblStopbits, CC.xy(3, 7));

	//---- label4 ----
	label4.setText("Parity:");
	add(label4, CC.xy(1, 9));

	//---- lblParity ----
	lblParity.setEnabled(false);
	add(lblParity, CC.xy(3, 9));

	//---- label6 ----
	label6.setText("Port Mode:");
	add(label6, CC.xy(1, 11));

	//---- lblPortmode ----
	lblPortmode.setEnabled(false);
	add(lblPortmode, CC.xy(3, 11));

	//---- enabledisablectslow ----
	enabledisablectslow.setText("Discard messages when CTS is low");
	enabledisablectslow.setOpaque(false);
	enabledisablectslow.setVisible(false);
	add(enabledisablectslow, CC.xy(3, 13));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JComboBox<Object> comboSerilPort;
  private JLabel lblBaudRate;
  private JLabel lblDatabits;
  private JLabel lblStopbits;
  private JLabel lblParity;
  private JLabel lblPortmode;
  private JCheckBox enabledisablectslow;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
