/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.eth;

import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.itemlist.IItemListManager;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 *
 */
public interface IEthPortManager extends IItemListManager<IEthernetPort> {

  Collection<IEthernetPort> getEthPorts();

  IEthernetPort getEthPort(String portName);

  IEthernetPort getEthPort(ETHERNET_PORT portEnum);

  ListModel<IEthernetPort> getEthPortsListModel();
}
