/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui;

import com.lucy.g3.rtu.config.port.eth.EthPortManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.ui.EthernetPanel;
import com.lucy.g3.test.support.utilities.TestUtil;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * The Class EthernetPanelTest.
 */
public class EthernetPanelTest {

  public static void main(String[] args) {
    IEthernetPort port = new EthPortManager().getEthPort(ETHERNET_PORT.ETHERNET_PORT_CONFIG);
    TestUtil.showFrame(new EthernetPanel(port));
  }
}
