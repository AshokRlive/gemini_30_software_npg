/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.ui;

import com.lucy.g3.rtu.config.port.serial.CustomSerialPort;
import com.lucy.g3.rtu.config.port.ui.SerialPortPanel;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class SerialPortPanelTest.
 */
public class SerialPortPanelTest {

  public static void main(String[] args) {
    TestUtil.showFrame(new SerialPortPanel(new CustomSerialPort("custom", "")));
  }
}
