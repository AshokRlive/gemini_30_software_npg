/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;

/**
 * Custom SerialPort is serial port defined by user.
 */
public class CustomSerialPort extends AbstractSerialPort implements ISerialPort {

  private String portPath;


  public CustomSerialPort(String portName, String portPath) {
    super(portName, 1);

    this.portPath = Preconditions.checkNotNull(portPath, "portPath is null");
  }

  public void setPortPath(String port) {
    this.portPath = Preconditions.checkNotNull(port, "port is null");
  }

  @Override
  public String getPortPath() {
    return portPath;
  }

  @Override
  public String getDisplayName() {
    return getPortName();
  }

  @Override
  public SERIAL_PORT getPortEnum() {
    return null;
  }

}
