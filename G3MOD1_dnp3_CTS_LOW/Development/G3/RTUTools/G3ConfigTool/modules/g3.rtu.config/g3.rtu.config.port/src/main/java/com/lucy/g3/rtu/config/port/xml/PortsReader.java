/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.xml;

import org.apache.log4j.Logger;

import com.g3schema.ns_port.EthernetPortConfT;
import com.g3schema.ns_port.PortsT;
import com.g3schema.ns_port.SerialPortConfT;
import com.lucy.g3.common.utils.EnumUtils;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IEthPortManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.CustomSerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPortManager;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_BAUD_RATE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_DATA_BITS;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PARITY;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_PORT_MODE;
import com.lucy.g3.xml.gen.common.ProtocolStackCommonEnum.LU_LIN232_STOP_BITS;

public class PortsReader {

  private Logger log = Logger.getLogger(PortsReader.class);

  public void read(PortsManager portMgr, PortsT xml_ports) {
    IEthPortManager ethMgr = portMgr.getEthManager();
    readEthPorts(xml_ports, ethMgr);

    ISerialPortManager serialMgr = portMgr.getSerialManager();
    readSerialPort(xml_ports, serialMgr);
  }

  public void readEthPorts(PortsT xml_ports, IEthPortManager ethMgr) {
    int ethNum = xml_ports.ethernet.count();
    for (int i = 0; i < ethNum; i++) {
      EthernetPortConfT xmlEth = xml_ports.ethernet.at(i);
      String portName = xmlEth.portName.getValue();
      IEthernetPort eth = ethMgr.getEthPort(portName);
  
      if (eth == null) {
        log.error("Ethernet port not found: " + portName);
        continue;
      }
      eth.readFromXML(xmlEth);
    }
  }

  public void readSerialPorts(SerialPortConfT xml_serial, ISerialPort serial) {
    if (xml_serial.enabled.exists()) {
      serial.setEnabled(xml_serial.enabled.getValue());
    }
  
    serial.setBaudRate(EnumUtils.getEnumFromString(LU_LIN232_BAUD_RATE.class, xml_serial.baudRate.getValue()));
    serial.setDataBit(EnumUtils.getEnumFromString(LU_LIN232_DATA_BITS.class, xml_serial.numDataBits.getValue()));
    serial.setParity(EnumUtils.getEnumFromString(LU_LIN232_PARITY.class, xml_serial.parity.getValue()));
    serial.setPortMode(EnumUtils.getEnumFromString(LU_LIN232_PORT_MODE.class, xml_serial.portMode.getValue()));
    serial.setStopBit(EnumUtils.getEnumFromString(LU_LIN232_STOP_BITS.class, xml_serial.numStopBits.getValue()));
    
    if(xml_serial.ctLowDiscardMsg.exists())
    	serial.setdiscardMessageCTLow(xml_serial.ctLowDiscardMsg.getValue());
  }

  private void readSerialPort(PortsT xml_ports, ISerialPortManager serialMgr) {
    int serialNum = xml_ports.serial.count();
    for (int i = 0; i < serialNum; i++) {
      SerialPortConfT xml_serial = xml_ports.serial.at(i);
      String portName = xml_serial.portName.getValue();
      String portPath = xml_serial.portPath.getValue();
      ISerialPort serial = serialMgr.getSerialPort(portName);
      

      if (serial == null) {
        serial = new CustomSerialPort(portName, portPath);
        serialMgr.addCustomSerialPort((CustomSerialPort) serial);
      }

      readSerialPorts(xml_serial, serial);
    }
  }

}
