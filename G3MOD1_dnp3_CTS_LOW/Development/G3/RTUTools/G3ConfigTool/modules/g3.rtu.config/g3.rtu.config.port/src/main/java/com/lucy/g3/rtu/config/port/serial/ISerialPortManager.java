/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.itemlist.IItemListManager;

/**
 *
 */
public interface ISerialPortManager extends IItemListManager<ISerialPort> {

  Collection<ISerialPort> getSerialPorts();
  
  Collection<ISerialPort> getSerialPortsNotInUse();

  ISerialPort getSerialPort(String portName);

  void addCustomSerialPort(CustomSerialPort customPort) throws AddItemException;

  boolean removeCustomSerialPort(CustomSerialPort customPort);

  ListModel<ISerialPort> getSerialPortsListModel();
}
