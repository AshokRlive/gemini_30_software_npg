/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port.serial;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.beans.PropertyVetoException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.port.PortUnusableException;
import com.lucy.g3.rtu.config.port.serial.AbstractSerialPortUser;
import com.lucy.g3.rtu.config.port.serial.CustomSerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPortUser;
import com.lucy.g3.rtu.config.port.serial.SerialPort;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;

/**
 * The Class SerialPortTest.
 */
public class SerialPortTest {

  private ISerialPort port0;
  private ISerialPort port1;
  private ISerialPortUser user;


  @Before
  public void setUp() throws Exception {
    user = new AbstractSerialPortUser() {

      @Override
      public String getPortUserName() {
        return "test user";
      }
    };

    port0 = new SerialPort(SERIAL_PORT.SERIAL_PORT_CONTROL);
    port1 = new SerialPort(SERIAL_PORT.SERIAL_PORT_CONFIG);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCheckUsable() throws PortUnusableException, PropertyVetoException {
    port0.checkUsable();
    port1.checkUsable();

    // Connect user to port0
    user.setSerialPort(port0);
    port1.checkUsable();
    try {
      port0.checkUsable();
      fail("Expect port0 usuable");
    } catch (Exception e) {
      // Nothing to handle
    }

    // Connect user to port1
    user.setSerialPort(port1);
    port0.checkUsable();
    try {
      port1.checkUsable();
      fail("Expect port1 usuable");
    } catch (PortUnusableException e) {
      // Exception expected
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }

  @Test
  public void testToString() {
    String name = "Abc";

    ISerialPort port = new CustomSerialPort(name, "");
    assertEquals(name, port.toString());

    name = SERIAL_PORT.SERIAL_PORT_CONFIG.getDescription();
    port = new SerialPort(SERIAL_PORT.SERIAL_PORT_CONFIG);
    assertEquals(name, port.toString());
  }

}
