/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import java.util.Collection;

/**
 * Utility for finding ports from a collection of ports.
 */
public final class PortsFinder {

  public static <T extends Port> T findPort(Collection<T> ports, String portName) {
    for (T port : ports) {
      if (port != null && port.getPortName().equals(portName)) {
        return port;
      }
    }

    return null;
  }
}
