/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.port;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.port.eth.IEthPortManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;
import com.lucy.g3.common.utils.EqualsUtil;


/**
 *
 */
public class PortsUtility implements IConfigUtility{
  private PortsUtility() {}
  
  public static ListModel<ISerialPort> getSerialPortsListModel(PortsManager portManager) {
    ListModel<ISerialPort> listModel = null;
    listModel = portManager.getSerialManager().getSerialPortsListModel();
    
    if(listModel == null) {
      Logger.getLogger(PortsUtility.class).error("Ports List Model not found!");
      listModel = new DefaultListModel<ISerialPort>();
    }
    
    return listModel;
  }
  
  public static ListModel<ISerialPort> getSerialPortsListModel(IConfig config) {
    
    PortsManager manager = config == null ? null :
      (PortsManager)config.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    
    return getSerialPortsListModel(manager);
  }
  
  public static ETHERNET_PORT findConnectedEthPort(String[] existingRtuIPs, String currentConnectedIP) {
    for (int id = 0; existingRtuIPs != null && id < existingRtuIPs.length; id++) {
      if (EqualsUtil.areEqual(existingRtuIPs[id], currentConnectedIP)) {
        return ETHERNET_PORT.forValue(id);
      }
    }
    
    return null;
  }
  
  public static String findNewIP(String[] existingRtuIPs, IEthPortManager portsManager, String currentConnectedIP) {
    ETHERNET_PORT connectedPort = findConnectedEthPort(existingRtuIPs, currentConnectedIP);
    
    if(connectedPort == null)
      return null;
    
    IEthernetPort newPortConfig = portsManager.getEthPort(connectedPort);
    
    if (newPortConfig != null && newPortConfig.isConfigEnabled()) {
      return newPortConfig.getIpConfig().getIpAddress();
    }
    
    return null;
  }
}

