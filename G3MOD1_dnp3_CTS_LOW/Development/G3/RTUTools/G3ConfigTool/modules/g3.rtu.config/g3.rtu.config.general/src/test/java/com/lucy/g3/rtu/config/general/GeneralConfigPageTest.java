/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.fest.swing.annotation.GUITest;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiQuery;
import org.fest.swing.edt.GuiTask;
import org.fest.swing.fixture.FrameFixture;
import org.fest.swing.testing.FestSwingTestCaseTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.test.support.utilities.TestUtil;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.general.GeneralConfigPage;

/**
 * The Class GeneralConfigPageTest.
 */
@GUITest
@Ignore
// Skipped GUI test cause it failes sometimes in linux
public class GeneralConfigPageTest extends FestSwingTestCaseTemplate {

  private FrameFixture window;
  private GeneralConfig config;


  @BeforeClass
  public static void setUpOnce() {
    // FailOnThreadViolationRepaintManager.install();
  }

  @Before
  public void setup() {
    setUpRobot();

    config = new GeneralConfig(null);

    // Logging property changes
    Logger log = Logger.getLogger(getClass());
    log.setLevel(Level.DEBUG);
    LoggingUtil.logPropertyChanges(config, log);

    // Create window
    JFrame frame = GuiActionRunner.execute(
        new GuiQuery<JFrame>() {

          @Override
          protected JFrame executeInEDT() {
            return TestUtil.showFrame(new GeneralConfigPage(config).getContent());
          }
        });

    // IMPORTANT: note the call to 'robot()'
    window = new FrameFixture(robot(), frame);
    window.show(); // shows the frame to test
  }

  @After
  public void teardown() {
    window.cleanUp();
  }

  @Test
  public void testInitialValue() throws InterruptedException {
    assertEquals(config.getSiteName(), window.textBox("sitenamefield").text());

    if (config.getConfigDescription() == null) {
      assertTrue(Strings.isBlank(window.textBox("descriptionfield").text()));
    } else {
      assertEquals(config.getConfigDescription(), window.textBox("descriptionfield").text());
    }

    if (config.getConfigVersion() == null) {
      assertTrue(Strings.isBlank(window.textBox("versionfield").text()));
    } else {
      assertEquals(config.getConfigVersion(), window.textBox("versionfield").text());
    }
  }

  @Test
  public void testBinding_SiteName() {
    window.textBox("sitenamefield").deleteText();
    window.textBox("sitenamefield").enterText("");
    assertFalse("Site name should never be blank", Strings.isBlank(config.getSiteName()));

    // View -> model
    window.textBox("sitenamefield").enterText("newsitename");
    window.textBox("sitenamefield").pressKey(KeyEvent.VK_ENTER);
    assertEquals("newsitename", config.getSiteName());

    // Model -> view
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {
        config.setSiteName("newnewsitename");
      }
    });
    assertEquals("newnewsitename", window.textBox("sitenamefield").text());
  }

  @Test
  public void testBinding_Version() {
    window.textBox("versionfield").deleteText();
    window.textBox("versionfield").enterText("");
    assertTrue("Site name should be blank but it is " + config.getConfigVersion(),
        Strings.isBlank(config.getConfigVersion()));

    // View -> model
    window.textBox("versionfield").enterText("1.01234");
    assertEquals("1.01234", config.getConfigVersion());

    // Model -> view
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {
        config.setConfigVersion("2.31234");
      }
    });
    assertEquals("2.31234", window.textBox("versionfield").text());
  }

  @Test
  public void testBinding_Version_with_invalidinput() {

    // Valid input
    window.textBox("versionfield").enterText("1.01234");
    assertEquals("1.01234", config.getConfigVersion());

    // Invalid input should have no effect
    window.textBox("versionfield").enterText("2.ad1");
    assertEquals("1.01234", config.getConfigVersion());
  }

  @Ignore
  // Unstable test case, sometimes fails
  @Test
  public void testBinding_Description() {
    // View -> model
    window.textBox("descriptionfield").enterText("");
    assertTrue(Strings.isBlank(config.getConfigDescription()));

    // View -> model
    window.textBox("descriptionfield").enterText("description");
    assertEquals("description", config.getConfigDescription());

    // Model -> view
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {
        config.setConfigDescription("newDescript");
      }
    });
    assertEquals("newDescript", window.textBox("descriptionfield").text());
  }

  // @Test
  public void manualTest() throws InterruptedException {
    Thread.sleep(100000);
  }

  public static void main(String[] args) {
    Logger log = Logger.getLogger(GeneralConfigPage.class);
    log.setLevel(Level.DEBUG);
    GeneralConfig bean = new GeneralConfig(null);
    LoggingUtil.logPropertyChanges(bean, log);
    TestUtil.showFrame(new GeneralConfigPage(bean).getContent());
  }
}
