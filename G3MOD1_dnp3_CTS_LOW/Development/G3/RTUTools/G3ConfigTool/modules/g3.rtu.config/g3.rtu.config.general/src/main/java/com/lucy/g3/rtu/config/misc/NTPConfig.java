/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.util.Collection;

import javax.swing.ListModel;

import com.jgoodies.common.bean.Bean;
import com.jgoodies.common.collect.ArrayListModel;


/**
 * The bean of NTP Config.
 */
public class NTPConfig extends Bean {

  public static final String PROPERTY_NTP_ENABLED = "ntpEnabled";
  public static final String PROPERTY_NTP_SCRIPT  = "ntpScript";
  private boolean ntpEnabled;
  private String  ntpScript = "";
  
  public boolean isNtpEnabled() {
    return ntpEnabled;
  }

  
  public void setNtpEnabled(boolean ntpEnabled) {
    Object oldValue = this.ntpEnabled;
    this.ntpEnabled = ntpEnabled;
    firePropertyChange(PROPERTY_NTP_ENABLED, oldValue, ntpEnabled);
  }
  
  public String getNtpScript() {
    return ntpScript;
  }
  
  public void setNtpScript(String ntpScript) {
    if (ntpScript != null) {
      Object oldValue = this.ntpScript;
      this.ntpScript = ntpScript;
      firePropertyChange(PROPERTY_NTP_SCRIPT, oldValue, ntpScript);
    }
  }
  
}

