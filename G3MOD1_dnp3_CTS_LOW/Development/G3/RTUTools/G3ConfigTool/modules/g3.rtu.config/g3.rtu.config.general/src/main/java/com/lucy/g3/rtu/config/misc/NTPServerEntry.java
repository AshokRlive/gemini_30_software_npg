package com.lucy.g3.rtu.config.misc;

import com.jgoodies.common.bean.Bean;

class NTPServerEntry extends Bean {
    private String ipAddr;
    private String options;
    
    public NTPServerEntry() {
      this("0.0.0.0");
    }
    
    public NTPServerEntry(String ip, String... options) {
      this.ipAddr = ip;
      
      StringBuilder sb = new StringBuilder();
      if(options != null) {
        for (int i = 0; i < options.length; i++) {
          sb.append(options[i]);
          sb.append(" ");
        }
      }
      this.options = sb.toString();
    }
    
    public String getIpAddr() {
      return ipAddr;
    }
    
    public void setIpAddr(String ipAddr) {
      this.ipAddr = ipAddr;
    }
    
    public String getOptions() {
      return options;
    }
    
    public void setOptions(String options) {
      this.options = options;
    }
    
    public String convertToString() {
      return String.format("server %s %s", ipAddr, options);
    }
  }