/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.general;

import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;


/**
 *
 */
public class GeneralConfigUtility implements IConfigUtility {
  private GeneralConfigUtility () {}
  
  public static String getFilePath(IConfig config) { 
    GeneralConfig conf = getGeneralConfig(config);
    return conf == null ? null : conf.getFilePath();
  }

  private static GeneralConfig getGeneralConfig(IConfig config) {
    return config == null ? null : (GeneralConfig) config.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
  }

  public static void setFilePath(IConfig config, String filePath) {
    GeneralConfig conf = getGeneralConfig(config);
    if(conf != null) {
      conf.setFilePath(filePath);
    }
  }
  
}

