/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TimeZone;
import javax.swing.*;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SpinnerNumberModel;

import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.zoneinfo.gen.RTUTimeZone;

/**
 * The page for configuring miscellaneous configuration.
 */
public class MiscConfigPage extends AbstractConfigPage {

  private final PresentationModel<MiscConfig> pm;
  private final PresentationModel<TimePeriod> pmTimeSync;
  private final PresentationModel<TimePeriod> pmTimePowerSavingDelay;
  private final PresentationModel<NTPConfig> pmNTP;

  public MiscConfigPage(MiscConfig miscBean) {
    super(miscBean);
    Preconditions.checkNotNull(miscBean, "miscBean is null");
    this.pm = new PresentationModel<MiscConfig>(miscBean);
    this.pmNTP = new PresentationModel<>(miscBean.getNtp());
    this.pmTimeSync = new PresentationModel<>(miscBean.getTimeSync());
    this.pmTimePowerSavingDelay = new PresentationModel<>(miscBean.getPowerSavingDelay());
    setNodeName("Misc");
  }

  @Override
  public boolean isScrollable() {
    return true;
  }

  private void createUIComponents() {
    labelPowerSavingDelay = BasicComponentFactory.createLabel(pmTimePowerSavingDelay.getModel(TimePeriod.PROPERTY_TIME_AS_TEXT));
    labelSyncTimeout = BasicComponentFactory.createLabel(pmTimeSync.getModel(TimePeriod.PROPERTY_TIME_AS_TEXT));

    cboxInvalidEventTimeCorrection =
        BasicComponentFactory.createCheckBox(pm.getModel(MiscConfig.PROPERTY_INVALIDEVENTTIMECORRECTION), "");
    
    ftfLRToggleTime = ComponentsFactory.createNumberField(pm.getModel(MiscConfig.PROPERTY_LRTOGGLETIME), 
        MiscConfig.MIN_LOCAL_REMOTE_TOGGLE_TIME, 60000);
    
    ftfOffToggleTime = ComponentsFactory.createNumberField(pm.getModel(MiscConfig.PROPERTY_OFFTOGGLETIME), 
        MiscConfig.MIN_OFF_TOGGLE_TIME, 60000);
    
    ftfSlaveProtocolDelay = ComponentsFactory.createNumberField(pm.getModel(MiscConfig.PROPERTY_SLAVE_PROTOCOL_DELAY_SECS), 
        0, 1000);
    
    TimeZone[] tz = RTUTimeZone.getAvailableRTUTimeZone();
    tz = ArrayUtils.add(tz, 0, null);
    combTimezone = ComponentsFactory.createComboBox(new SelectionInList<>(
        tz, 
        pm.getModel(MiscConfig.PROPERTY_TIMEZONE)), 
        new TimeZoneCellRenderer());
  }
  
  private void initComponentsBinding() {
    AbstractValueModel vm = pmNTP.getModel(NTPConfig.PROPERTY_NTP_ENABLED);
    Bindings.bind(cboxNTP, vm);
    PropertyConnector.connectAndUpdate(vm, btnNTP, "enabled");
    
    vm = pm.getModel(MiscConfig.PROPERTY_SYSLOG_ENABLE);
    Bindings.bind(cboxSyslogEnable, vm);
    PropertyConnector.connectAndUpdate(vm, btnSyslogConfig, "enabled");
  }


//  private SpinnerNumberModel createSpinnerModel(String property) {
//    return SpinnerAdapterFactory.createNumberAdapter(
//        pm.getModel(property),
//        0, 0, Integer.MAX_VALUE, 1);
//  }

  private void btnModifyPowerSavingActionPerformed(ActionEvent e) {
    new TimePeriodSettingDialog(
        WindowUtils.getMainFrame(), 
        "Power Saving Delay", 
        pmTimePowerSavingDelay).setVisible(true);
  }

  private void btnModifyTimeSyncActionPerformed(ActionEvent e) {
    new TimePeriodSettingDialog(
        WindowUtils.getMainFrame(), 
        "Time Sync Delay", 
        pmTimeSync).setVisible(true);
  }

  private void btnNTPActionPerformed(ActionEvent e) {
    new NTPScriptEditorDialog(WindowUtils.getMainFrame(), pmNTP.getBean()).setVisible(true);
  }

  private void btnSyslogConfigActionPerformed() {
    SyslogConfEditorDialog editor = new SyslogConfEditorDialog(WindowUtils.getMainFrame(), true);
    editor.setSysConf(pm.getBean().getSyslogConf());
    editor.setVisible(true);
    if(editor.hasBeenAffirmed()) {
      pm.getBean().setSyslogConf(editor.getSysConf());
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    xTitledSeparator3 = new JXTitledSeparator();
    JLabel label1 = new JLabel();
    btnModifyPowerSaving = new JButton();
    xTitledSeparator2 = new JXTitledSeparator();
    JLabel label2 = new JLabel();
    btnModifyTimeSync = new JButton();
    cboxNTP = new JCheckBox();
    btnNTP = new JButton();
    label3 = new JLabel();
    xTitledSeparator4 = new JXTitledSeparator();
    labelLRToggleTime2 = new JLabel();
    labelMs2 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    labelOffToggleTime = new JLabel();
    labelLRToggleTime = new JLabel();
    labelMs0 = new JLabel();
    labelMs1 = new JLabel();
    xTitledSeparator5 = new JXTitledSeparator();
    cboxSyslogEnable = new JCheckBox();
    btnSyslogConfig = new JButton();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
      "right:default, $lcgap, [50dlu,default], $rgap, [100dlu,default], $lcgap, default",
      "default, $lgap, fill:default, $pgap, default, $lgap, fill:default, $rgap, default, $lgap, 2*(default, $pgap), default, $lgap, default, $pgap, 2*(default, $lgap), default, $pgap, default, $lgap, default"));

    //---- xTitledSeparator3 ----
    xTitledSeparator3.setTitle("Power Saving");
    add(xTitledSeparator3, CC.xywh(1, 1, 7, 1));

    //---- label1 ----
    label1.setText("Power Saving Delay:");
    add(label1, CC.xy(1, 3));
    add(labelPowerSavingDelay, CC.xywh(3, 3, 3, 1));

    //---- btnModifyPowerSaving ----
    btnModifyPowerSaving.setText("Modify");
    btnModifyPowerSaving.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnModifyPowerSavingActionPerformed(e);
      }
    });
    add(btnModifyPowerSaving, CC.xy(7, 3));

    //---- xTitledSeparator2 ----
    xTitledSeparator2.setTitle("Time Sync");
    add(xTitledSeparator2, CC.xywh(1, 5, 7, 1));

    //---- label2 ----
    label2.setText("Time Sync Delay:");
    add(label2, CC.xy(1, 7));
    add(labelSyncTimeout, CC.xywh(3, 7, 3, 1));

    //---- btnModifyTimeSync ----
    btnModifyTimeSync.setText("Modify");
    btnModifyTimeSync.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnModifyTimeSyncActionPerformed(e);
      }
    });
    add(btnModifyTimeSync, CC.xy(7, 7));

    //---- cboxInvalidEventTimeCorrection ----
    cboxInvalidEventTimeCorrection.setText("Invalid Event Time Correction");
    add(cboxInvalidEventTimeCorrection, CC.xywh(3, 9, 3, 1));

    //---- cboxNTP ----
    cboxNTP.setText("NTP Enabled");
    add(cboxNTP, CC.xywh(3, 11, 3, 1));

    //---- btnNTP ----
    btnNTP.setText("Configure...");
    btnNTP.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnNTPActionPerformed(e);
      }
    });
    add(btnNTP, CC.xy(7, 11));

    //---- label3 ----
    label3.setText("RTU Time Zone:");
    add(label3, CC.xy(1, 13));
    add(combTimezone, CC.xywh(3, 13, 3, 1));

    //---- xTitledSeparator4 ----
    xTitledSeparator4.setTitle("Protocol Stack");
    add(xTitledSeparator4, CC.xywh(1, 15, 7, 1));

    //---- labelLRToggleTime2 ----
    labelLRToggleTime2.setText("Slave Protocol Initialisation Delay:");
    add(labelLRToggleTime2, CC.xy(1, 17));
    add(ftfSlaveProtocolDelay, CC.xy(3, 17));

    //---- labelMs2 ----
    labelMs2.setText("secs");
    add(labelMs2, CC.xy(5, 17));

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Off Local Remote Button");
    add(xTitledSeparator1, CC.xywh(1, 19, 7, 1));

    //---- labelOffToggleTime ----
    labelOffToggleTime.setText("Off Toggle Time:");
    add(labelOffToggleTime, CC.xy(1, 23));

    //---- labelLRToggleTime ----
    labelLRToggleTime.setText("Local Remote Toggle Time:");
    add(labelLRToggleTime, CC.xy(1, 21));
    add(ftfLRToggleTime, CC.xy(3, 21));

    //---- labelMs0 ----
    labelMs0.setText("ms");
    add(labelMs0, CC.xy(5, 21));
    add(ftfOffToggleTime, CC.xy(3, 23));

    //---- labelMs1 ----
    labelMs1.setText("ms");
    add(labelMs1, CC.xy(5, 23));

    //---- xTitledSeparator5 ----
    xTitledSeparator5.setTitle("Syslog");
    add(xTitledSeparator5, CC.xywh(1, 25, 7, 1));

    //---- cboxSyslogEnable ----
    cboxSyslogEnable.setText("Allow to configure Syslog");
    add(cboxSyslogEnable, CC.xywh(3, 27, 3, 1));

    //---- btnSyslogConfig ----
    btnSyslogConfig.setText("Configure...");
    btnSyslogConfig.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnSyslogConfigActionPerformed();
      }
    });
    add(btnSyslogConfig, CC.xy(7, 27));
    // JFormDesigner - End of component initialization //GEN-END:initComponents


    labelMs0.setText(String.format("ms [%-4d ~ %d]",
        MiscConfig.MIN_LOCAL_REMOTE_TOGGLE_TIME,
        MiscConfig.MAX_LOCAL_REMOTE_TOGGLE_TIME));
    labelMs1.setText(String.format("ms [%-4d ~ %d]",
        MiscConfig.MIN_OFF_TOGGLE_TIME,
        MiscConfig.MAX_OFF_TOGGLE_TIME));
  }

  @Override
  protected void init() throws Exception {
    initComponents();
    initComponentsBinding();
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTitledSeparator xTitledSeparator3;
  private JLabel labelPowerSavingDelay;
  private JButton btnModifyPowerSaving;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel labelSyncTimeout;
  private JButton btnModifyTimeSync;
  private JCheckBox cboxInvalidEventTimeCorrection;
  private JCheckBox cboxNTP;
  private JButton btnNTP;
  private JLabel label3;
  private JComboBox combTimezone;
  private JXTitledSeparator xTitledSeparator4;
  private JLabel labelLRToggleTime2;
  private JFormattedTextField ftfSlaveProtocolDelay;
  private JLabel labelMs2;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel labelOffToggleTime;
  private JLabel labelLRToggleTime;
  private JFormattedTextField ftfLRToggleTime;
  private JLabel labelMs0;
  private JFormattedTextField ftfOffToggleTime;
  private JLabel labelMs1;
  private JXTitledSeparator xTitledSeparator5;
  private JCheckBox cboxSyslogEnable;
  private JButton btnSyslogConfig;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  private static class TimeZoneCellRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      TimeZone tz = (TimeZone) value;
      setText(tz == null ? "Do not change" : tz.getDisplayName(tz.useDaylightTime(), TimeZone.SHORT));
      return comp;
    }
    
  }
}
