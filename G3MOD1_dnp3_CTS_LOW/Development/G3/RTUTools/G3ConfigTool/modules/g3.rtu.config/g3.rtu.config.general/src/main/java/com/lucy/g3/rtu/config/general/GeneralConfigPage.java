/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.general;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.prompt.PromptSupport;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.RegexFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

/**
 * The page for configuring {@link GeneralConfig}.
 */
public final class GeneralConfigPage extends AbstractConfigPage {

  private final GeneralConfig config;


  public GeneralConfigPage(GeneralConfig config) {
    super(config);
    this.config = config;
    setNodeName("General");
  }

  @Override
  protected void init() {
    initComponents();
    initComponentsBinding();
  }

  private void initComponentsBinding() {
    PresentationModel<GeneralConfig> pm = new PresentationModel<GeneralConfig>(config);
    Bindings.bind(ftfSite, pm.getModel(GeneralConfig.PROPERTY_SITENAME));
    Bindings.bind(tfDescription, pm.getModel(GeneralConfig.PROPERTY_CONFIG_DESCRIPTION));
    Bindings.bind(ftfConfigVersion, pm.getModel(GeneralConfig.PROPERTY_CONFIG_VERSION));
    Bindings.bind(lblModifyDate, pm.getModel(GeneralConfig.PROPERTY_MODIFY_DATE));
    
    // Binding release type
    ComboBoxAdapter<String> model = new ComboBoxAdapter<>(
        new String[]{"Pre-release", "Release", "Debug", "Test"},
        pm.getModel(GeneralConfig.PROPERTY_CONFIG_RELEASE_TYPE));
    Bindings.bind(comboReleaseType, model);
    
    String ver = (String) pm.getValue(GeneralConfig.PROPERTY_TOOL_VERSION);
    String rev = (String) pm.getValue(GeneralConfig.PROPERTY_TOOL_REVEISION);
    if(ver != null && rev != null) {
      lblToolVer.setText(String.format("Version: %s (Revision:%s)", ver, rev));
    }
  }

  private void createUIComponents() {
    // Create version field
    RegexFormatter cfgVersionformatter = new RegexFormatter(GeneralConfig.CONFIG_VERSION_PATTERN);
    cfgVersionformatter.setCommitsOnValidEdit(true);
    ftfConfigVersion = new JFormattedTextField(cfgVersionformatter);
    PromptSupport.setPrompt("e.g. \"1.0.0\"", ftfConfigVersion);
    FormattedTextFieldSupport.installCommitOnType(ftfConfigVersion);

    // Create site name field
    RegexFormatter nameFormatter = new RegexFormatter(GeneralConfig.RTU_NAME_PATTERN);
    ftfSite = new JFormattedTextField(nameFormatter);
    FormattedTextFieldSupport.installCommitOnType(ftfSite);
    PromptSupport.setPrompt("1~64 characters", ftfSite);
    UIUtils.setTextFieldsCaretPositionOnClick(ftfSite);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    separator2 = new JXTitledSeparator();
    lblSite = new JLabel();
    separator1 = new JXTitledSeparator();
    label1 = new JLabel();
    comboReleaseType = new JComboBox();
    lblConfigVersion = new JLabel();
    lblVersion = new JLabel();
    tfDescription = new JTextArea();
    lblModify = new JLabel();
    lblModifyDate = new JLabel();
    label2 = new JLabel();
    lblToolVer = new JLabel();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
        "right:default, $lcgap, 100dlu, 2*($lcgap, default), $lcgap, [100dlu,default]",
        "default, $lgap, default, $pgap, default, $lgap, 2*(default, $ugap), fill:40dlu, $ugap, fill:default, $lgap, fill:default"));

    //---- separator2 ----
    separator2.setTitle("Site Information");
    add(separator2, CC.xywh(1, 1, 9, 1));

    //---- lblSite ----
    lblSite.setText("Site Name:");
    add(lblSite, CC.xy(1, 3));

    //---- ftfSite ----
    ftfSite.setColumns(64);
    ftfSite.setName("sitenamefield");
    add(ftfSite, CC.xywh(3, 3, 7, 1));

    //---- separator1 ----
    separator1.setTitle("Configuration File");
    add(separator1, CC.xywh(1, 5, 9, 1));

    //---- label1 ----
    label1.setText("Release Type:");
    add(label1, CC.xy(1, 7));

    //---- comboReleaseType ----
    comboReleaseType.setEditable(true);
    add(comboReleaseType, CC.xy(3, 7));

    //---- lblConfigVersion ----
    lblConfigVersion.setText("Version:");
    add(lblConfigVersion, CC.xy(1, 9));

    //---- ftfConfigVersion ----
    ftfConfigVersion.setName("versionfield");
    add(ftfConfigVersion, CC.xy(3, 9));

    //---- lblVersion ----
    lblVersion.setText("Description:");
    lblVersion.setVerticalAlignment(SwingConstants.TOP);
    add(lblVersion, CC.xy(1, 11));

    //---- tfDescription ----
    tfDescription.setLineWrap(true);
    tfDescription.setBorder(new LineBorder(UIManager.getColor("List.dropLineColor")));
    tfDescription.setName("descriptionfield");
    tfDescription.setFont(UIManager.getFont("TextField.font"));
    add(tfDescription, CC.xywh(3, 11, 7, 1));

    //---- lblModify ----
    lblModify.setText("Last Modification:");
    add(lblModify, CC.xy(1, 13));
    add(lblModifyDate, CC.xywh(3, 13, 7, 1));

    //---- label2 ----
    label2.setText("Created by Tool:");
    add(label2, CC.xy(1, 15));
    add(lblToolVer, CC.xywh(3, 15, 7, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTitledSeparator separator2;
  private JLabel lblSite;
  private JFormattedTextField ftfSite;
  private JXTitledSeparator separator1;
  private JLabel label1;
  private JComboBox comboReleaseType;
  private JLabel lblConfigVersion;
  private JFormattedTextField ftfConfigVersion;
  private JLabel lblVersion;
  private JTextArea tfDescription;
  private JLabel lblModify;
  private JLabel lblModifyDate;
  private JLabel label2;
  private JLabel lblToolVer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
