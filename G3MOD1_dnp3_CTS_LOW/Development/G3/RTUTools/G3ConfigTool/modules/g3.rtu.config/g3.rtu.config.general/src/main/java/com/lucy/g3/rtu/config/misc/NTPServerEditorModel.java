
package com.lucy.g3.rtu.config.misc;

import java.util.Collection;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;

class NTPServerEditorModel {
  private static final String[] COLUMN_NAMES = { "Server Address", "Options"};

  private final ArrayListModel<NTPServerEntry> severList = new ArrayListModel<>();

  private final MultiSelectionInList<NTPServerEntry> multiselectionList;
  private final MultiListSelectionAdapter<NTPServerEntry> multiSelectionModel;
  
  private final NTPServerTableModel serverListTableModel;
  
  @SuppressWarnings("unchecked")
  public NTPServerEditorModel() {
    super();
    this.multiselectionList = new MultiSelectionInList<NTPServerEntry>(severList);
    this.multiSelectionModel = new MultiListSelectionAdapter<NTPServerEntry>(multiselectionList);
    this.multiSelectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    this.serverListTableModel = new NTPServerTableModel(severList);
  }

  public void addServer(NTPServerEntry newEntry) {
    severList.add(newEntry);
  }
  
  public void removeServer(Collection<NTPServerEntry> selection) {
    severList.removeAll(selection);
  }
  
  @SuppressWarnings("rawtypes")
  public ListModel getServerListModel() {
    return severList;
  }
  
  public List<NTPServerEntry> getServerList(){
    return severList;
  }
  
  public TableModel getServerListTableModel() {
    return serverListTableModel;
  }
  
  public ListSelectionModel getTableSelectionModel(){
    return multiSelectionModel;
  }
  
  public boolean hasSelection() {
    return multiselectionList.hasSelection();
  }
  
  public List<NTPServerEntry> getSelection() {
    return multiselectionList.getSelection();
  }

  private class NTPServerTableModel extends AbstractTableAdapter<NTPServerEntry> {

    public NTPServerTableModel(ListModel<NTPServerEntry> severListModel) {
      super(severListModel, COLUMN_NAMES);
    }

    @Override
    public int getColumnCount() {
      return COLUMN_NAMES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      NTPServerEntry serverEntry = getRow(rowIndex);

      switch (columnIndex) {
      case 0:
        return serverEntry.getIpAddr();
      case 1:
        return serverEntry.getOptions();
      default:
        throw new IllegalStateException("column not found");
      }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
      return true;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
      if (value == null) {
        return;
      }

      NTPServerEntry serverEntry = getRow(rowIndex);

      switch (columnIndex) {
      case 0:
        serverEntry.setIpAddr(value.toString());
        break;
      case 1:
        serverEntry.setOptions(value.toString());
        break;
      default:
        throw new IllegalStateException("column not found");
      }
      
      fireTableCellUpdated(rowIndex, columnIndex);
    }
  }
  
}

