
package com.lucy.g3.rtu.config.misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import javax.swing.text.JTextComponent;

class NTPSynch {
  private NTPSynch() {}
  
  static Pattern PATTERN_SERVER_ENTRY = Pattern.compile("server\\s+\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}");

  static void sync(JTextComponent source, List<NTPServerEntry> target) {
      /* Find all server entries in the document*/
      String[] lines = source.getText().split("\n");
      ArrayList<String> serverlines = new ArrayList<>();
      for (String line : lines) {
        if(PATTERN_SERVER_ENTRY.matcher(line).find()) {
          serverlines.add(line);
        }
      }
      
      target.clear();
      for (String entry : serverlines) {
        String[] tokens = entry.split("\\s+");
        String[] options = null;
        if(tokens.length > 2)
          options = Arrays.copyOfRange(tokens, 2, tokens.length);
        
        target.add(new NTPServerEntry(tokens[1], options));
      }
      
  }
  
  static void sync(List<NTPServerEntry> source, JTextComponent target) {
      /* Remove existing server list*/
      StringBuilder sb = new StringBuilder();
    if (!target.getText().isEmpty()) {
      String[] lines = target.getText().split("\n");
      for (String line : lines) {
        if (PATTERN_SERVER_ENTRY.matcher(line).find() == false) {
          sb.append(line);
          sb.append("\n");
        }
      }
    }
      
      // Append server list
      for (NTPServerEntry entry : source) {
        sb.append(entry.convertToString());
        sb.append("\n");
      }
      
      target.setText(sb.toString());
  } 
  
  
}

