/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.general;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * Bean for storing general configuration.
 */
public class GeneralConfig extends AbstractConfigModule{
  public static final String CONFIG_MODULE_ID = "GeneralConfig";
  
  /** Regex of site name,length:1-64. Illegal character:\,. */
  public static final String RTU_NAME_PATTERN = "[^\\\\/]{1,64}";

  /** Regex of configuration version. */
  public static final String CONFIG_VERSION_PATTERN = "^$|\\d{1,3}\\.\\d{1,5}\\.\\d{1,5}";

  public static final String DEFAULT_RTU_NAME = "Gemini3 RTU";

  public static final String PROPERTY_SITENAME = "siteName";

  public static final String PROPERTY_FILEPATH = "filePath";

  public static final String PROPERTY_CONFIG_DESCRIPTION = "configDescription";

  public static final String PROPERTY_CONFIG_VERSION = "configVersion";
  public static final String PROPERTY_CONFIG_RELEASE_TYPE = "configReleaseType";

  public static final String PROPERTY_MODIFY_DATE = "modifyDate";
  
  public static final String PROPERTY_TOOL_REVEISION = "toolReveision";
  public static final String PROPERTY_TOOL_VERSION = "toolVersion";


  private Logger log = Logger.getLogger(GeneralConfig.class);

  /**
   * The file path where this configData is saved.
   */
  private String filePath;

  /**
   * RTU site name, mandatory property, must not be empty.
   */
  private String siteName = DEFAULT_RTU_NAME;

  private String configReleaseType;
  
  private String configVersion;
  
  private long configVersionMajor;
  private long configVersionMinor;
  private long configVersionPatch;

  /** Configuration file description customised by user. */
  private String configDescription;

  /**
   * Configuration file modification date. This date is read from XML and
   * unmodifiable.
   */
  private String modifyDate;
  
  private String toolVersion;
  private String toolReveision;

  public GeneralConfig(IConfig owner) {
    super(owner);
  }
  
  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    String oldValue = getFilePath();
    this.filePath = filePath;
    firePropertyChange(PROPERTY_FILEPATH, oldValue, filePath);
  }

  public String getSiteName() {
    return siteName;
  }

  public void setSiteName(String siteName) {
    if (Strings.isBlank(siteName)) {
      return;
    }

    if (!siteName.matches(RTU_NAME_PATTERN)) {
      return;
    }

    Object oldValue = getSiteName();
    this.siteName = siteName;
    firePropertyChange(PROPERTY_SITENAME, oldValue, siteName);
  }

  public String getConfigDescription() {
    return configDescription;
  }

  public void setConfigDescription(String configDescription) {
    Object oldValue = getConfigDescription();
    this.configDescription = configDescription;
    firePropertyChange(PROPERTY_CONFIG_DESCRIPTION, oldValue, configDescription);
  }

  
  public String getConfigReleaseType() {
    return configReleaseType;
  }

  
  public void setConfigReleaseType(String configReleaseType) {
    Object oldValue = this.configReleaseType;
    this.configReleaseType = configReleaseType;
    firePropertyChange(PROPERTY_CONFIG_RELEASE_TYPE, oldValue, configReleaseType);
  }

  /**
   * Gets the configuration file version number in string format "major.minor".
   *
   * @return a string that represents the version of configuration file;
   *         <code>null</code> or empty string if the version is not set.
   */
  public String getConfigVersion() {
    return configVersion;
  }

  public void setConfigVersion(String configVersion) {
    if (configVersion == null) {
      log.warn("Cannot set config version to null");
      return;
    }

    if (!configVersion.matches(CONFIG_VERSION_PATTERN)) {
      log.warn("Cannot set config version. Unsupport version format:" + configVersion);
      return;
    }

    Object oldValue = getConfigVersion();
    this.configVersion = configVersion;
    firePropertyChange(PROPERTY_CONFIG_VERSION, oldValue, configVersion);

    // Update version number
    String[] tokens = configVersion.split("\\.");
    if (tokens.length >= 1) {
      configVersionMajor = Integer.valueOf(tokens[0]);
    }
    
    if (tokens.length >= 2) {
      configVersionMinor = Integer.valueOf(tokens[1]);
    }
    
    if (tokens.length >= 3) {
      configVersionPatch = Integer.valueOf(tokens[2]);
    }
  }

  
  public long getConfigVersionMajor() {
    return configVersionMajor;
  }

  public long getConfigVersionMinor() {
    return configVersionMinor;
  }

  public long getConfigVersionPatch() {
    return configVersionPatch;
  }

  public String getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(String modifyDate) {
    Object oldValue = getModifyDate();
    this.modifyDate = modifyDate;
    firePropertyChange(PROPERTY_MODIFY_DATE, oldValue, modifyDate);
  }

  
  public String getToolVersion() {
    return toolVersion;
  }

  
  public void setToolVersion(String toolVersion) {
    Object oldValue = this.toolVersion;
    this.toolVersion = toolVersion;
    firePropertyChange(PROPERTY_TOOL_VERSION, oldValue, toolVersion);
  }

  
  public String getToolReveision() {
    return toolReveision;
  }

  
  public void setToolReveision(String toolReveision) {
    Object oldValue = this.toolReveision;
    this.toolReveision = toolReveision;
    firePropertyChange(PROPERTY_TOOL_REVEISION, oldValue, toolReveision);
  }
  
  
}
