/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.util.TimeZone;

import com.g3schema.GlobalProtocolstackSettingsT;
import com.g3schema.MiscellaneousT;
import com.g3schema.NTPConfigT;
import com.g3schema.OffLocalButtonSettingT;
import com.g3schema.SyslogT;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * The bean for storing miscellaneous configuration.
 */
public class MiscConfig extends AbstractConfigModule {
  public static final String CONFIG_MODULE_ID = "MiscConfig";

  public static final long DEFAULT_POWER_SAVE_DELAY = 3; // seconds
  public static final long DEFAULT_TIMESYNC_TIMEOUT = 36000; // seconds

  public static final String PROPERTY_INVALIDEVENTTIMECORRECTION = "invalidEventTimeCorrection";
  public static final String PROPERTY_LRTOGGLETIME = "localRemoteToggleTime";
  public static final String PROPERTY_OFFTOGGLETIME = "offToggleTime";
  public static final String PROPERTY_SLAVE_PROTOCOL_DELAY_SECS = "slaveProtocolDelaySecs";
  public static final String PROPERTY_TIMEZONE= "tz";
  public static final String PROPERTY_SYSLOG_ENABLE = "syslogEnable";
  public static final String PROPERTY_SYSLOG_CONF= "syslogConf";

  public static final int MAX_LOCAL_REMOTE_TOGGLE_TIME = 60000;
  public static final int MIN_LOCAL_REMOTE_TOGGLE_TIME = 100;
  public static final int MAX_OFF_TOGGLE_TIME = 60000;
  public static final int MIN_OFF_TOGGLE_TIME = 1000;
  
  private boolean invalidEventTimeCorrection;
  private long localRemoteToggleTime = 500;
  private long offToggleTime = 3000;

  private final TimePeriod timeSync = new TimePeriod(TimePeriod.UNSIGN_INT_MAX);
  private final TimePeriod powerSavingDelay= new TimePeriod(TimePeriod.UNSIGN_SHORT_MAX);
  
  private final NTPConfig ntp = new NTPConfig();
  
  private int slaveProtocolDelaySecs;
  
  private TimeZone tz;
  
  private boolean syslogEnable;
  private String  syslogConf = "";
  
  public MiscConfig(IConfig owner) {
    super(owner);
    
    timeSync.setTotalSecs(DEFAULT_TIMESYNC_TIMEOUT);
    powerSavingDelay.setTotalSecs(DEFAULT_POWER_SAVE_DELAY);
  }
  
  
  public TimePeriod getTimeSync() {
    return timeSync;
  }
  
  public TimePeriod getPowerSavingDelay() {
    return powerSavingDelay;
  }

  public NTPConfig getNtp() {
    return ntp;
  }


//  public void setPowerSavingDelay(long powerSavingDelay) {
//    if (powerSavingDelay < 0) {
//      return;
//    }
//
//    this.powerSavingDelay = powerSavingDelay;
//
//    long seconds = powerSavingDelay;
//
//    int hours = (int) TimeUnit.SECONDS.toHours(seconds);
//    seconds = seconds - TimeUnit.HOURS.toSeconds(hours);
//
//    int minutes = (int) TimeUnit.SECONDS.toMinutes(seconds);
//    seconds = seconds - TimeUnit.MINUTES.toSeconds(minutes);
//
//    setPowerSavingDelayHours(hours);
//    setPowerSavingDelayMins(minutes);
//    setPowerSavingDelaySecs((int) seconds);
//  }

  

  // ==================Property Getter/Setters ===================

//  public int getPowerSavingDelayHours() {
//    return powerSavingDelayHours;
//  }
//
//  public void setPowerSavingDelayHours(int powerSavingDelay_hours) {
//    if (powerSavingDelay_hours < 0) {
//      return;
//    }
//
//    Object oldValue = getPowerSavingDelayHours();
//    this.powerSavingDelayHours = powerSavingDelay_hours;
//    firePropertyChange(PROPERTY_POWERSAVINGDELAY_HOURS, oldValue, powerSavingDelay_hours);
//    updatePowerSavingDelay();
//  }
//
//  public int getPowerSavingDelayMins() {
//    return powerSavingDelayMins;
//  }
//
//  public void setPowerSavingDelayMins(int powerSavingDelay_mins) {
//    if (powerSavingDelay_mins < 0) {
//      return;
//    }
//    Object oldValue = getPowerSavingDelayMins();
//    this.powerSavingDelayMins = powerSavingDelay_mins;
//    firePropertyChange(PROPERTY_POWERSAVINGDELAY_MINS, oldValue, powerSavingDelay_mins);
//    updatePowerSavingDelay();
//  }
//
//  public int getPowerSavingDelaySecs() {
//    return powerSavingDelaySecs;
//  }
//
//  public void setPowerSavingDelaySecs(int powerSavingDelay_secs) {
//    if (powerSavingDelay_secs < 0) {
//      return;
//    }
//    Object oldValue = getPowerSavingDelaySecs();
//    this.powerSavingDelaySecs = powerSavingDelay_secs;
//    firePropertyChange(PROPERTY_POWERSAVINGDELAY_SECS, oldValue, powerSavingDelay_secs);
//    updatePowerSavingDelay();
//  }
//
// 
//
//  public String getPowerSavingDelayText() {
//    return powerSavingDelayText;
//  }
//
//  private void setPowerSavingDelayText(String powerSavingDelayText) {
//    Object oldValue = getPowerSavingDelayText();
//    this.powerSavingDelayText = powerSavingDelayText;
//    firePropertyChange(PROPERTY_POWERSAVINGDELAY_TEXT, oldValue, powerSavingDelayText);
//  }

  public boolean isInvalidEventTimeCorrection() {
    return invalidEventTimeCorrection;
  }

  public void setInvalidEventTimeCorrection(boolean invalidEventTimeCorrection) {
    Object oldValue = this.invalidEventTimeCorrection;
    this.invalidEventTimeCorrection = invalidEventTimeCorrection;
    firePropertyChange("invalidEventTimeCorrection", oldValue,
        invalidEventTimeCorrection);
  }

  public long getLocalRemoteToggleTime() {
    return localRemoteToggleTime;
  }

  public void setLocalRemoteToggleTime(long lRToggleTime) {
    if (lRToggleTime < MIN_LOCAL_REMOTE_TOGGLE_TIME || lRToggleTime > MAX_LOCAL_REMOTE_TOGGLE_TIME)
      return;
    
    Object oldValue = getLocalRemoteToggleTime();
    this.localRemoteToggleTime = lRToggleTime;
    firePropertyChange(PROPERTY_LRTOGGLETIME, oldValue, localRemoteToggleTime);
  }

  public long getOffToggleTime() {
    return offToggleTime;
  }

  public void setOffToggleTime(long offToggleTime) {
    if (offToggleTime < MIN_OFF_TOGGLE_TIME || offToggleTime > MAX_OFF_TOGGLE_TIME)
      return;

    Object oldValue = getOffToggleTime();
    this.offToggleTime = offToggleTime;
    firePropertyChange(PROPERTY_OFFTOGGLETIME, oldValue, offToggleTime);
  }
  
  public int getSlaveProtocolDelaySecs() {
    return slaveProtocolDelaySecs;
  }
  
  public void setSlaveProtocolDelaySecs(int slaveProtocolDelaySecs) {
    Object oldValue = this.slaveProtocolDelaySecs;
    this.slaveProtocolDelaySecs = slaveProtocolDelaySecs;
    firePropertyChange(PROPERTY_SLAVE_PROTOCOL_DELAY_SECS, oldValue, slaveProtocolDelaySecs);
  }


  
  public TimeZone getTz() {
    return tz;
  }

  public void setTz(TimeZone tz) {
    Object oldValue = this.tz;
    this.tz = tz;
    firePropertyChange(PROPERTY_TIMEZONE, oldValue, tz);
  }
  
  
  public boolean isSyslogEnable() {
    return syslogEnable;
  }
  
  public void setSyslogEnable(boolean syslogEnable) {
    Object oldValue = this.syslogEnable;
    this.syslogEnable = syslogEnable;
    firePropertyChange(PROPERTY_SYSLOG_ENABLE, oldValue, syslogEnable);
  }
  
  public String getSyslogConf() {
    return syslogConf;
  }
  
  public void setSyslogConf(String syslogConf) {
    Object oldValue = this.syslogConf;
    this.syslogConf = syslogConf;
    firePropertyChange(PROPERTY_SYSLOG_CONF, oldValue, syslogConf);
  }


  public void writeToXML(MiscellaneousT xml) {
    xml.powerSavingDelaySec.setValue(this.getPowerSavingDelay().getTotalSecs());
    xml.timeSyncTimeoutSec.setValue(this.getTimeSync().getTotalSecs());
    xml.invalidEventTimeCorrection.setValue(this.isInvalidEventTimeCorrection());
    
    if(tz != null)
    xml.timezone.setValue(tz.getID());

    OffLocalButtonSettingT xml_orbtn = xml.offLocalButtonSetting.append();
    xml_orbtn.localRemoteToggleTime.setValue(this.getLocalRemoteToggleTime());
    xml_orbtn.offToggleTime.setValue(this.getOffToggleTime());

    GlobalProtocolstackSettingsT xml_ps = xml.protocolstack.append();
    xml_ps.slaveProtocolStackDelay.setValue(this.getSlaveProtocolDelaySecs());

    NTPConfigT xml_ntp = xml.ntp.append();
    xml_ntp.enabled.setValue(this.getNtp().isNtpEnabled());
    xml_ntp.script.setValue(this.getNtp().getNtpScript());
    
    SyslogT xmlSyslog = xml.syslog.append();
    xmlSyslog.enabled.setValue(isSyslogEnable());
    xmlSyslog.syslogconf.setValue(getSyslogConf());
  }
  
  public void readFromXML(MiscellaneousT xml) {
    this.getPowerSavingDelay().setTotalSecs(xml.powerSavingDelaySec.getValue());
    this.getTimeSync().setTotalSecs(xml.timeSyncTimeoutSec.getValue());
    this.setInvalidEventTimeCorrection(xml.invalidEventTimeCorrection.getValue());

    OffLocalButtonSettingT xml_olbtn = xml.offLocalButtonSetting.first();
    this.setLocalRemoteToggleTime((int) xml_olbtn.localRemoteToggleTime.getValue());
    this.setOffToggleTime((int) xml_olbtn.offToggleTime.getValue());

    if (xml.protocolstack.exists()) {
      this.setSlaveProtocolDelaySecs((int) xml.protocolstack.first().slaveProtocolStackDelay.getValue());
    }

    if (xml.ntp.exists()) {
      this.getNtp().setNtpEnabled(xml.ntp.first().enabled.getValue());
      this.getNtp().setNtpScript(xml.ntp.first().script.getValue());
    } else {
      this.getNtp().setNtpEnabled(false);
    }
    
    if(xml.timezone.exists()) {
      this.setTz(TimeZone.getTimeZone(xml.timezone.getValue()));
    }
    
    if(xml.syslog.exists()) {
      SyslogT xmlSyslog = xml.syslog.first();
      setSyslogEnable(xmlSyslog.enabled.getValue());
      if(xmlSyslog.syslogconf.exists())
        setSyslogConf(xmlSyslog.syslogconf.getValue());
    }
  }
}
