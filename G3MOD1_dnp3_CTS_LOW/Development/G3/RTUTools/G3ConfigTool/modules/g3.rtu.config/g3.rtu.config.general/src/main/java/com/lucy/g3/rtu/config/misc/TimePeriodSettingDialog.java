/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
public class TimePeriodSettingDialog extends AbstractDialog {

  private final PresentationModel<TimePeriod> pm;
  
  public TimePeriodSettingDialog(Frame owner,String title, PresentationModel<TimePeriod> pm) {
    super(owner, title);
    
    this.pm = pm;
    initComponents();
    pack();
  }
  
  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  public void cancel() {
    pm.triggerFlush();
    super.cancel();
  }

  @Override
  public void ok() {
    if(validateInputs()) {
      pm.triggerCommit();
      super.ok();
    }
  }

  private boolean validateInputs() {
    int days  = (int) pm.getBufferedValue(TimePeriod.PROPERTY_DAYS);
    int hours = (int) pm.getBufferedValue(TimePeriod.PROPERTY_HOURS);
    int mins  = (int) pm.getBufferedValue(TimePeriod.PROPERTY_MINS);
    int secs  = (int) pm.getBufferedValue(TimePeriod.PROPERTY_SECS);
    
    long totalSecs = TimePeriod.calculateTotalSecs(days, hours, mins, secs);
    long max = pm.getBean().getTotalSecsMax();
    if(totalSecs > max) {
      JOptionPane.showMessageDialog(this, 
          String.format("The total time period (%d seconds) is out of range: [%d,%d]! "
              + "\nPlease reduce the input time value and try again.",
              totalSecs, 0, max),
          "Out of Range Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

  private void createUIComponents() {
    spinnerDays = new JSpinner(createSpinnerModel(TimePeriod.PROPERTY_DAYS));
    spinnerHours = new JSpinner(createSpinnerModel(TimePeriod.PROPERTY_HOURS));
    spinnerMins = new JSpinner(createSpinnerModel(TimePeriod.PROPERTY_MINS));
    spinnerSecs = new JSpinner(createSpinnerModel(TimePeriod.PROPERTY_SECS));
  }
  
  private SpinnerNumberModel createSpinnerModel(String property) {
    return SpinnerAdapterFactory.createNumberAdapter(
        pm.getBufferedModel(property),
        0, 0, Integer.MAX_VALUE, 1);
  }
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    JLabel label4 = new JLabel();
    JLabel label5 = new JLabel();
    JLabel label6 = new JLabel();
    JLabel label7 = new JLabel();

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "[80dlu,default]:grow, $lcgap, default",
          "3*(default, $ugap), default, $lgap, default"));
      contentPanel.add(spinnerDays, CC.xy(1, 1));

      //---- label4 ----
      label4.setText("days");
      label4.setHorizontalTextPosition(SwingConstants.RIGHT);
      contentPanel.add(label4, CC.xy(3, 1));
      contentPanel.add(spinnerHours, CC.xy(1, 3));

      //---- label5 ----
      label5.setText("hours");
      label5.setHorizontalTextPosition(SwingConstants.RIGHT);
      contentPanel.add(label5, CC.xy(3, 3));
      contentPanel.add(spinnerMins, CC.xy(1, 5));

      //---- label6 ----
      label6.setText("minutes");
      label6.setHorizontalTextPosition(SwingConstants.RIGHT);
      contentPanel.add(label6, CC.xy(3, 5));
      contentPanel.add(spinnerSecs, CC.xy(1, 7));

      //---- label7 ----
      label7.setText("seconds");
      label7.setHorizontalTextPosition(SwingConstants.RIGHT);
      contentPanel.add(label7, CC.xy(3, 7));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  
    SpinnerWheelSupport.installMouseWheelSupport(spinnerDays);
    SpinnerWheelSupport.installMouseWheelSupport(spinnerHours);
    SpinnerWheelSupport.installMouseWheelSupport(spinnerMins);
    SpinnerWheelSupport.installMouseWheelSupport(spinnerSecs);
    
    // Add focus listeners for updating background
    FocusListener focusListener = new FocusListener() {

      @Override
      public void focusGained(FocusEvent e) {
        JSpinner spinner = (JSpinner) e.getSource();
        spinner.getEditor().setBackground(Color.blue);
      }

      @Override
      public void focusLost(FocusEvent e) {
        JSpinner spinner = (JSpinner) e.getSource();
        spinner.getEditor().setBackground(Color.lightGray);
      }
    };
    spinnerDays.addFocusListener(focusListener);
    spinnerHours.addFocusListener(focusListener);
    spinnerMins.addFocusListener(focusListener);
    spinnerSecs.addFocusListener(focusListener);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JSpinner spinnerDays;
  private JSpinner spinnerHours;
  private JSpinner spinnerMins;
  private JSpinner spinnerSecs;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
