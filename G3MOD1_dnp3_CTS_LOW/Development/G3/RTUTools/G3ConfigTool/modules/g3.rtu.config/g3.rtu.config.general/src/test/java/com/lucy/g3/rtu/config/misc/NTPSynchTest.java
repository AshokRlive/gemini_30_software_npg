
package com.lucy.g3.rtu.config.misc;

import static org.junit.Assert.*;

import org.junit.Test;


public class NTPSynchTest {

  @Test
  public void testRegex() {
    assertTrue(NTPSynch.PATTERN_SERVER_ENTRY.matcher("server 1.2.3.4 df").find());
    assertTrue(NTPSynch.PATTERN_SERVER_ENTRY.matcher("server  1.2.3.4  df").find());
    assertTrue(NTPSynch.PATTERN_SERVER_ENTRY.matcher("server 1.2.3.4 ").find());
    assertTrue(NTPSynch.PATTERN_SERVER_ENTRY.matcher("server 1.2.3.4").find());
    assertTrue(NTPSynch.PATTERN_SERVER_ENTRY.matcher("  server 1.2.3.4 ").find());
    
    assertFalse(NTPSynch.PATTERN_SERVER_ENTRY.matcher("s1erver 1.2.3.4 ").find());
    assertFalse(NTPSynch.PATTERN_SERVER_ENTRY.matcher("server 1.a.3.4").find());
  }

}

