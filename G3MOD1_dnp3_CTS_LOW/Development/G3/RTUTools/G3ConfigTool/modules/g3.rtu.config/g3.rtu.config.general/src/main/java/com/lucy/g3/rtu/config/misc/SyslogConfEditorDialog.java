/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;

public class SyslogConfEditorDialog extends AbstractDialog {

  public SyslogConfEditorDialog(Dialog owner, boolean modal) throws HeadlessException {
    super(owner, modal);
    init();
  }

  public SyslogConfEditorDialog(Frame owner, boolean modal) throws HeadlessException {
    super(owner, modal);
    init();
  }
  
  public SyslogConfEditorDialog(Window parent, String title, boolean modal) {
    super(parent, title, modal);
    init();
    setTitle(title);
  }

  private void init() {
    setTitle("Syslog Configuration");
    initComponents();
    pack();
  }
  
  public String getSysConf(){
    return taSysConf.getText();
  }
  
  public void setSysConf(String conf) {
    taSysConf.setText(conf);
  }

  @Override
  public void ok() {
    super.ok();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

  
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    scrollPane1 = new JScrollPane();
    taSysConf = new JTextArea();

    //======== contentPanel ========
    {
      contentPanel.setLayout(new BorderLayout());

      //======== scrollPane1 ========
      {
        scrollPane1.setBorder(new TitledBorder("syslog.conf"));
        scrollPane1.setPreferredSize(new Dimension(400, 300));
        scrollPane1.setViewportView(taSysConf);
      }
      contentPanel.add(scrollPane1, BorderLayout.CENTER);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JTextArea taSysConf;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
