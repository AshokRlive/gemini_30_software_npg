/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.util.concurrent.TimeUnit;

import com.jgoodies.binding.beans.Model;


/**
 *
 */
public class TimePeriod extends Model{
  public static final long UNSIGN_SHORT_MAX = (long) Math.pow(2, 16) - 1; 
  public static final long UNSIGN_INT_MAX = (long) Math.pow(2, 32) - 1; 
  
  public static final String PROPERTY_TIME_AS_TEXT = "timeAsText";

  public static final String PROPERTY_DAYS  = "days";
  public static final String PROPERTY_HOURS = "hours";
  public static final String PROPERTY_MINS  = "mins";
  public static final String PROPERTY_SECS  = "secs";
  
  private int days;
  private int hours;
  private int mins;
  private int secs;
  
  private long totalSecs; // seconds in total
  private String timeAsText;
  private final long totalSecsMax;

  public TimePeriod(long totalSecsMax) {
    this.totalSecsMax = totalSecsMax;
  }
  
  public long getTotalSecsMax() {
    return totalSecsMax;
  }

  private long calculateTotalSecs() {
    return calculateTotalSecs(days, hours, mins, secs);
  }
  
  public static long calculateTotalSecs(int days,int hours,int mins, int secs) {
    return TimeUnit.DAYS.toSeconds(days)
        + TimeUnit.HOURS.toSeconds(hours)
        + TimeUnit.MINUTES.toSeconds(mins)
        + secs;
  }
  
  public long getTotalSecs() {
    return totalSecs;
  }

  public void setTotalSecs(long totalSecs) {
    setTotalSecs(totalSecs, true);
  }
  
  private void setTotalSecs(long totalSecs, boolean updateAll) {
    if (totalSecs < 0 ) {
      return;
    }
    
    if(totalSecs > totalSecsMax) {
      totalSecs = totalSecsMax;
    }

    this.totalSecs = totalSecs;
    
    if(updateAll) {
      long secsTemp = totalSecs;
  
      int days = (int) TimeUnit.SECONDS.toDays(secsTemp);
      secsTemp = secsTemp - TimeUnit.DAYS.toSeconds(days);
  
      int hours = (int) TimeUnit.SECONDS.toHours(secsTemp);
      secsTemp = secsTemp - TimeUnit.HOURS.toSeconds(hours);
  
      int minutes = (int) TimeUnit.SECONDS.toMinutes(secsTemp);
      secsTemp = secsTemp - TimeUnit.MINUTES.toSeconds(minutes);
  
      setDays(days);
      setHours(hours);
      setMins(minutes);
      setSecs((int) secsTemp);
    }
    
    // Update time text
    setTimeAsText(String.format("%d days, %d hours, %d minutes, %d seconds",
        getDays(),
        getHours(),
        getMins(),
        getSecs()));
    
  }
  
  public String getTimeAsText() {
    return timeAsText;
  }

  private void setTimeAsText(String timeAsText) {
    Object oldValue = getTimeAsText();
    this.timeAsText = timeAsText;
    firePropertyChange(PROPERTY_TIME_AS_TEXT, oldValue, timeAsText);
  }

  public int getDays() {
    return days;
  }

  public void setDays(int days) {
    if (days < 0) {
      return;
    }

    Object oldValue = getDays();
    this.days = days;
    firePropertyChange(PROPERTY_DAYS, oldValue, days);

    setTotalSecs(calculateTotalSecs(), false);
  }

  public int getHours() {
    return hours;
  }

  public void setHours(int hours) {
    if (hours < 0) {
      return;
    }

    Object oldValue = getHours();
    this.hours = hours;
    firePropertyChange(PROPERTY_HOURS, oldValue, hours);

    setTotalSecs(calculateTotalSecs(), false);
  }

  public int getMins() {
    return mins;
  }

  public void setMins(int mins) {
    if (mins < 0) {
      return;
    }

    Object oldValue = getMins();
    this.mins = mins;
    firePropertyChange(PROPERTY_MINS, oldValue, mins);

    setTotalSecs(calculateTotalSecs(), false);
  }

  public int getSecs() {
    return secs;
  }

  public void setSecs(int secs) {
    if (secs < 0) {
      return;
    }

    Object oldValue = getSecs();
    this.secs = secs;
    firePropertyChange(PROPERTY_SECS, oldValue, secs);

    setTotalSecs(calculateTotalSecs(), false);
  }  
  
}

