/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.general;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.misc.MiscConfig;
import com.lucy.g3.rtu.config.misc.MiscConfigPage;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class GeneralConfigPlugin implements IConfigPlugin, IPageFactory {

  private GeneralConfigPlugin() {
  }


  private final static GeneralConfigPlugin INSTANCE = new GeneralConfigPlugin();


  public static GeneralConfigPlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory factory = ConfigFactory.getInstance();
    
    factory.registerFactory(MiscConfig.CONFIG_MODULE_ID,
        new IConfigModuleFactory() {

          @Override
          public IConfigModule create(IConfig owner) {
            return new MiscConfig(owner);
          }
        });
    
    factory.registerFactory(GeneralConfig.CONFIG_MODULE_ID,
        new IConfigModuleFactory() {

          @Override
          public IConfigModule create(IConfig owner) {
            return new GeneralConfig (owner);
          }
        });
    
    PageFactories.registerFactory(PageFactories.KEY_GENERAL, INSTANCE);
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof GeneralConfig) {
      return new GeneralConfigPage((GeneralConfig) data);

    } else if (data instanceof MiscConfig) {
      return new MiscConfigPage((MiscConfig) data);

    }
    
    return null;
  }

}
