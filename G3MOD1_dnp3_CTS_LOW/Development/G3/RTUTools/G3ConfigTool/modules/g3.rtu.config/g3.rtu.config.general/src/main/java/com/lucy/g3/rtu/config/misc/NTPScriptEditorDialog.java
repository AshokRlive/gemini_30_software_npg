/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.misc;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.table.IPAddressTableCellEditor;

/**
 * The dialog for editing ntp script.
 */
public class NTPScriptEditorDialog extends AbstractDialog {
  private Logger log = Logger.getLogger(NTPServerEditorModel.class);
  
  private final NTPConfig config;
  private final NTPServerEditorModel model;
  private final DocumentListener docListener;
  private final TableModelListener tableListener;
  private final Timer syncTimer;
  
  
  public NTPScriptEditorDialog(Frame owner, NTPConfig config) {
    super(owner);
    this.config = config;
    this.model = new NTPServerEditorModel();
    this.syncTimer = createSyncTimer();
    this.docListener = createDocumentLister();
    this.tableListener = createTableLister();
    
    setTitle("NTP Configuration");
    initComponents();
    initComponentsBinding();
    initEventHandling();
    pack();
    
    // Sync txt to list initially
    syncTimer.getActionListeners()[0].actionPerformed(null);
  }

  
  private TableModelListener createTableLister() {
    return new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        taNtpConf.getDocument().removeDocumentListener(docListener);
        
        log.info("Synchroise server list to doc");
        NTPSynch.sync(model.getServerList(),taNtpConf);
        
        taNtpConf.getDocument().addDocumentListener(docListener);
      }
    };
  }


  private Timer createSyncTimer() {
    Timer timer = new Timer(1000, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tableServer.getModel().removeTableModelListener(tableListener);
        
        log.info("Synchroise doc to server list");
        NTPSynch.sync(taNtpConf, model.getServerList());
        
        tableServer.getModel().addTableModelListener(tableListener);
      }
    });
    timer.setRepeats(false);
    return timer;
  }


  private DocumentListener createDocumentLister() {
    return new DocumentListener() {
      @Override
      public void removeUpdate(DocumentEvent e) {
        syncTimer.restart();
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        syncTimer.restart();
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        syncTimer.restart();
      }
    };    
  }


  private void initEventHandling() {
    tableServer.getModel().addTableModelListener(tableListener);
    taNtpConf.getDocument().addDocumentListener(docListener);
  }


  private void initComponentsBinding() {
    tableServer.setModel(model.getServerListTableModel());
    Bindings.bind(tableServer, model.getServerListModel(), model.getTableSelectionModel());
    
    // Set IP editor
    TableColumn col = tableServer.getColumnModel().getColumn(0);
    col.setCellEditor(new IPAddressTableCellEditor());
    
    taNtpConf.setText(config.getNtpScript());
  }


  @Override
  public void ok() {
    super.ok();
    config.setNtpScript(taNtpConf.getText());
  }

  private void cboxAdvancedActionPerformed(ActionEvent e) {
    int idx = tabbedPane.indexOfComponent(tabConf);
    if(((JCheckBox)e.getSource()).isSelected()) {
      if(idx < 0) {
        tabbedPane.addTab("Advanced", tabConf);
      }
      tabbedPane.setSelectedComponent(tabConf);
    } else {
      if(idx >= 0)
      tabbedPane.removeTabAt(idx);
    }
  }

  private void btnAddActionPerformed() {
    model.addServer(new NTPServerEntry());
  }

  private void btnRemoveActionPerformed() {
    if(model.hasSelection()) {
      if(MessageDialogs.confirmRemoveAll(this))
        model.removeServer(model.getSelection());
    } else {
      MessageDialogs.warning(this, "No selection!");
    }
  }



  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    tabConf = new JScrollPane();
    taNtpConf = new JTextArea();
    contentPanel = new JPanel();
    tabbedPane = new JTabbedPane();
    panel2 = new JPanel();
    scrollPane1 = new JScrollPane();
    tableServer = new JTable();
    controlPanel = new JPanel();
    btnAdd = new JButton();
    btnRemove = new JButton();
    cboxAdvanced = new JCheckBox();

    //======== tabConf ========
    {
      tabConf.setBorder(new TitledBorder("ntf.conf"));
      tabConf.setViewportView(taNtpConf);
    }

    //======== contentPanel ========
    {
      contentPanel.setPreferredSize(new Dimension(400, 300));
      contentPanel.setLayout(new BorderLayout());

      //======== tabbedPane ========
      {

        //======== panel2 ========
        {
          panel2.setLayout(new BorderLayout());

          //======== scrollPane1 ========
          {
            scrollPane1.setBorder(null);
            scrollPane1.setPreferredSize(new Dimension(0, 0));

            //---- tableServer ----
            tableServer.setFillsViewportHeight(true);
            tableServer.setRowHeight(20);
            scrollPane1.setViewportView(tableServer);
          }
          panel2.add(scrollPane1, BorderLayout.CENTER);
        }
        tabbedPane.addTab("NTP Servers", panel2);
      }
      contentPanel.add(tabbedPane, BorderLayout.CENTER);

      //======== controlPanel ========
      {
        controlPanel.setBorder(Borders.TABBED_DIALOG_BORDER);
        controlPanel.setLayout(new FormLayout(
          "default",
          "10dlu, 2*($lgap, default), $lgap, default:grow, $lgap, fill:default"));

        //---- btnAdd ----
        btnAdd.setText("Add ");
        btnAdd.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btnAddActionPerformed();
          }
        });
        controlPanel.add(btnAdd, CC.xy(1, 3));

        //---- btnRemove ----
        btnRemove.setText("Remove");
        btnRemove.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btnRemoveActionPerformed();
          }
        });
        controlPanel.add(btnRemove, CC.xy(1, 5));

        //---- cboxAdvanced ----
        cboxAdvanced.setText("Advanced");
        cboxAdvanced.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            cboxAdvancedActionPerformed(e);
          }
        });
        controlPanel.add(cboxAdvanced, CC.xy(1, 9));
      }
      contentPanel.add(controlPanel, BorderLayout.EAST);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane tabConf;
  private JTextArea taNtpConf;
  private JPanel contentPanel;
  private JTabbedPane tabbedPane;
  private JPanel panel2;
  private JScrollPane scrollPane1;
  private JTable tableServer;
  private JPanel controlPanel;
  private JButton btnAdd;
  private JButton btnRemove;
  private JCheckBox cboxAdvanced;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }


  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }


  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
}
