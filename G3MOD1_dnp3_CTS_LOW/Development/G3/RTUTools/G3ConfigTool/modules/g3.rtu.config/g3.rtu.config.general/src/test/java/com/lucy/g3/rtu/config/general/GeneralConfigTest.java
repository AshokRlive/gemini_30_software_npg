/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.general;

import static org.junit.Assert.assertEquals;

import java.util.regex.Pattern;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.general.GeneralConfig;


/**
 *
 */
public class GeneralConfigTest {
  private GeneralConfig fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new GeneralConfig(null);
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetSiteName() {
    String name = "NewSiteName";
    fixture.setSiteName(name);
    assertEquals(name, fixture.getSiteName());

    // Set empty
    fixture.setSiteName(" ");
    assertEquals(name, fixture.getSiteName());

    // Set null
    fixture.setSiteName(null);
    assertEquals(name, fixture.getSiteName());

    // Set over 64 characters
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 65; i++) {
      sb.append("a");
    }
    fixture.setSiteName(sb.toString());
    assertEquals(name, fixture.getSiteName());
  }

  @Test
  public void testVersionPattern() {
    String[] validVersions = {"","0.0.0","1.1.1"};
    
    String[] invalidVersions = {"0.0","0.0.","12121212"};
    
    String pattern = GeneralConfig.CONFIG_VERSION_PATTERN;
    Pattern pat = Pattern.compile(pattern);
    
    for (int i = 0; i < validVersions.length; i++) {
      Assert.assertTrue("Mimsatched version : " + validVersions[i],pat.matcher(validVersions[i]).matches());
    }
    
    for (int i = 0; i < invalidVersions.length; i++) {
      Assert.assertFalse("Mimsatched version : " + invalidVersions[i],pat.matcher(invalidVersions[i]).matches());
    }
  }

}

