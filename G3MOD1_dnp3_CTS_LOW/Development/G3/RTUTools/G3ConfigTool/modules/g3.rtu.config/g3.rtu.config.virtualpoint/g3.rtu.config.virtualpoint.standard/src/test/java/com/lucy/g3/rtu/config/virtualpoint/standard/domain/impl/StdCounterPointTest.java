/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointStub;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.StdCounterPointImpl;

/**
 * The Class StdCounterPointTest.
 */
public class StdCounterPointTest {

  private StdCounterPointImpl fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new StdCounterPointImpl(0, "");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetSourcePoint() {
    VirtualPointStub source = new VirtualPointStub(VirtualPointType.BINARY_INPUT);

    assertNull(fixture.getSource());
    fixture.setSource(source);
    assertTrue(source == fixture.getSource());

    source.delete();
    assertNull(fixture.getSource());
  }

}
