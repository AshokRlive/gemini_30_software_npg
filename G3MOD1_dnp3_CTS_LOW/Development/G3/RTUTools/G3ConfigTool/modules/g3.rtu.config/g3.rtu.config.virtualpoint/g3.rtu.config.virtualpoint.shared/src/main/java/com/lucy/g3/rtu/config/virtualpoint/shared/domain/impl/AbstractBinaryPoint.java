/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;


import com.g3schema.ns_vpoint.BaseBPointT;
import com.g3schema.ns_vpoint.DelayedEventEntryT;
import com.g3schema.ns_vpoint.DelayedReportT;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

/**
 * Basic implementation of {@linkplain BinaryPoint}.
 */
public abstract class AbstractBinaryPoint extends AbstractDigitalPoint implements BinaryPoint {

  private boolean invert;


  public AbstractBinaryPoint(int group, int id, String description, VirtualPointType type) {
    super(group, id, description, type);
  }

  @Override
  public boolean isInvert() {
    return invert;
  }

  @Override
  public void setInvert(boolean invert) {
    Object oldValue = isInvert();
    this.invert = invert;
    firePropertyChange(PROPERTY_INVERT, oldValue, invert);
  }

  protected void readFromXML(BaseBPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);

    // Delay report
    if (xml.delayedReport.exists()) {
      DelayedReportT xmlDelay = xml.delayedReport.first();
      setDelayEnabled(xmlDelay.enabled.getValue());

      // Read legacy delay settings
      if(xmlDelay.delay.exists())
        setDelay(xmlDelay.delay.getValue());
      if(xmlDelay.edgeDrivenMode.exists())
        setDrivenMode(EDGE_DRIVEN_MODE.forValue((int) xmlDelay.edgeDrivenMode.getValue()));
      
      // Read delay settings
      DelayedEventEntryT xmlEntry;
      for (int i = 0; i < xmlDelay.entry.count(); i++) {
        xmlEntry = xmlDelay.entry.at(i);
        int value = xmlEntry.eventValue.getValue();
        setDelay(value, toSecs(xmlEntry.delayPeriodMs.getValue()));
      }
    }
    
    setInvert(xml.invert.getValue());
  }

  protected void writeToXML(BaseBPointT xml) {
    super.writeToXML(xml);
    
    // Delay report
    DelayedReportT xmlDelay = xml.delayedReport.append();
    xmlDelay.enabled.setValue(isDelayEnabled());
    
    DelayedEventEntryT xmlEntry = xmlDelay.entry.append();
    xmlEntry.eventValue.setValue(0);
    xmlEntry.delayPeriodMs.setValue(toMs(getDelay0()));
    
    xmlEntry = xmlDelay.entry.append();
    xmlEntry.eventValue.setValue(1);
    xmlEntry.delayPeriodMs.setValue(toMs(getDelay1()));
    
    // Store legacy delay settings.
    xmlDelay.delay.setValue(getDelay());
    xmlDelay.edgeDrivenMode.setValue(getDrivenMode().getValue());
    
    xml.invert.setValue(isInvert());
  }

  @Override
  public long getDelay2() {
    throw new IllegalAccessError("Unsupported field");
  }

  @Override
  public long getDelay3() {
    throw new IllegalAccessError("Unsupported field");
  }

  @Override
  public void setDelay2(long delay) {
    throw new IllegalAccessError("Unsupported field");
  }

  @Override
  public void setDelay3(long delay) {
    throw new IllegalAccessError("Unsupported field");
  }

  
  
}
