/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.page;

import javax.swing.BorderFactory;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import com.lucy.g3.gui.framework.page.NavigationEvent;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;

/**
 * The page that that manage all kinds of virtual point to manage different type of points.
 *
 * @see {@link VirtualPointTab}
 */
public class VirtualPointPage extends AbstractConfigPage {

  private final VirtualPointTab[] tabs;

  private JTabbedPane tabbedPane;

  private VirtualPointManager manager;

  public VirtualPointPage(VirtualPointManager manager) {
    super(manager);
    setNodeName("Virtual Points");
    this.manager = manager;
    VirtualPointType[] types = VirtualPointType.values();
    tabs = new VirtualPointTab[types.length];
  }

  @Override
  protected void init() {
    tabbedPane = new JTabbedPane(SwingConstants.TOP);
    tabbedPane.setBorder(BorderFactory.createEmptyBorder(0, 0, -1, -2));
    tabbedPane.setName("VirtualPointsPage.tabbedPane");
    tabbedPane.setBorder(null);

    VirtualPointType[] types = VirtualPointType.values();

    for (int i = 0; i < types.length; i++) {
      VirtualPointPageModel model = new VirtualPointPageModel(manager, types[i]);
      tabs[i] = new VirtualPointTab(model, this);
      tabbedPane.addTab(types[i].getName(), null, tabs[i], types[i].getDescription());
      tabs[i].packColumns();
    }

    add(tabbedPane);
    this.setBorder(BorderFactory.createEmptyBorder(4, 0, 0, 0));
  }

  void fireNavEvent(NavigationEvent e) {
    fireNavigationEvent(e);
  }

}
