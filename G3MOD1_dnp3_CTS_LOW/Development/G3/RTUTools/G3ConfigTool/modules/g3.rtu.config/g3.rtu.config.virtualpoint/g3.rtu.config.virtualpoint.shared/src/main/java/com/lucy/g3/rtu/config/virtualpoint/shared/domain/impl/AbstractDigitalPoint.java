/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.g3schema.ns_vpoint.BaseDigitalPointT;
import com.g3schema.ns_vpoint.DigitalPointChatterT;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DigitalPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

/**
 * Basic implementation of {@linkplain DigitalPoint}.
 */
public abstract class AbstractDigitalPoint extends AbstractVirtualPoint implements DigitalPoint {

  private Logger log = Logger.getLogger(AbstractDigitalPoint.class);
  
  private int chatterNo = 0;
  private int chatterTime = 1;
  private boolean delayEnabled; // DelayReport Enabled
  private long delay = 60; // seconds
  private EDGE_DRIVEN_MODE drivenMode = EDGE_DRIVEN_MODE.EDGE_DRIVEN_MODE_RISE;

  private long delay0 = 0; // seconds
  private long delay1 = 0; // seconds
  private long delay2 = 0; // seconds
  private long delay3 = 0; // seconds

  public AbstractDigitalPoint(int group, int id, String description, VirtualPointType type) {
    super(group, id, description, type);
  }

  // ================== Properties Getters/Setters ================

  @Override
  public int getChatterNo() {
    return chatterNo;
  }

  @Override
  public void setChatterNo(int chatterNo) /* throws PropertyVetoException */{
    // if (chatterNo < 0 ) {
    // throw new PropertyVetoException("Negative Value",
    // new PropertyChangeEvent(this, "chatterNo",
    // this.chatterNo, chatterNo));
    // }

    Object oldValue = getChatterNo();
    // fireVetoableChange("chatterNo", this.chatterNo, chatterNo);
    this.chatterNo = chatterNo;
    firePropertyChange(PROPERTY_CHATTERNO, oldValue, chatterNo);

    if (chatterNo == 0) {
      setChatterTime(0);
    }
  }

  @Override
  public int getChatterTime() {
    return chatterTime;
  }

  @Override
  public void setChatterTime(int chatterTime)/* throws PropertyVetoException */{
    // if (chatterNo < 0 ) {
    // throw new PropertyVetoException("Negative Value",
    // new PropertyChangeEvent(this, "chatterTime",
    // this.chatterTime, chatterTime));
    // }

    Object oldValue = getChatterTime();
    // fireVetoableChange("chatterTime", this.chatterTime, chatterTime);
    this.chatterTime = chatterTime;
    firePropertyChange(PROPERTY_CHATTERTIME, oldValue, chatterTime);
  }

  @Override
  public boolean isDelayEnabled() {
    return delayEnabled;
  }

  @Override
  public void setDelayEnabled(boolean delayEnabled) {
    Object oldValue = this.delayEnabled;
    this.delayEnabled = delayEnabled;
    firePropertyChange(PROPERTY_DELAY_ENABLED, oldValue, delayEnabled);
  }

  @Override
  public long getDelay() {
    return delay;
  }

  @Override
  public void setDelay(long delay) {
    Object oldValue = this.delay;
    this.delay = delay;
    firePropertyChange(PROPERTY_DELAY, oldValue, delay);
  }
  
  @Override
  public long getDelay0() {
    return delay0;
  }

  @Override
  public void setDelay0(long delay) {
    Object oldValue = this.delay0;
    this.delay0 = delay;
    firePropertyChange(PROPERTY_DELAY0, oldValue, delay);
  }
  
  @Override
  public long getDelay1() {
    return delay1;
  }

  @Override
  public void setDelay1(long delay) {
    Object oldValue = this.delay1;
    this.delay1 = delay;
    firePropertyChange(PROPERTY_DELAY1, oldValue, delay);
    
    setDelay(delay);
  }
  
  @Override
  public long getDelay2() {
    return delay2;
  }

  @Override
  public void setDelay2(long delay) {
    Object oldValue = this.delay2;
    this.delay2 = delay;
    firePropertyChange(PROPERTY_DELAY2, oldValue, delay);
  }
  @Override
  public long getDelay3() {
    return delay3;
  }

  @Override
  public void setDelay3(long delay) {
    Object oldValue = this.delay3;
    this.delay3 = delay;
    firePropertyChange(PROPERTY_DELAY3, oldValue, delay);
  }

  protected void setDelay(int value, long periodSecs) {
    switch (value) {
    case 0:
      setDelay0(periodSecs);
      break;
      
    case 1:
      setDelay1(periodSecs);
      break;
      
    case 2:
      setDelay2(periodSecs);
      break;
      
    case 3:
      setDelay3(periodSecs);
      break;
      
    default:
      log.error("Unsupported delay value: " + value);
      break;
    }
  }
  
  @Override
  public EDGE_DRIVEN_MODE getDrivenMode() {
    return drivenMode;
  }

  @Override
  public void setDrivenMode(EDGE_DRIVEN_MODE drivenMode) {
    if (drivenMode == EDGE_DRIVEN_MODE.EDGE_DRIVEN_MODE_BOTH) {
      Logger.getLogger(getClass()).error("Unsupported driven mode:" + drivenMode);
      return;
    }

    Object oldValue = this.drivenMode;
    this.drivenMode = drivenMode;
    firePropertyChange("drivenMode", oldValue, drivenMode);
  }

  @Override
  public String getHTMLSummary() {
    StringBuffer html = new StringBuffer();

    html.append("<html>");
    html.append("<b>Point: " + getName() + "</b>");
    html.append("<ul>");
    html.append("<li>Type: " + type.getName() + "</li>");
    html.append("<li>ChatterTime: " + getChatterTime() + " ms</li>");
    html.append("<li>ChatterNo: " + getChatterNo() + "</li>");
    html.append("</ul>");
    html.append("</html>");
    return html.toString();
  }

  protected void writeToXML(BaseDigitalPointT xml) {
    super.writeToXML(xml);

    // Chatter
    DigitalPointChatterT xml_chatter = xml.chatter.append();
    xml_chatter.chatterNo.setValue(BigInteger.valueOf(getChatterNo()));
    xml_chatter.chatterTime.setValue(BigInteger.valueOf(getChatterTime()));
  }

  protected void readFromXML(BaseDigitalPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);

    // Chatter
    setChatterNo(xml.chatter.first().chatterNo.getValue().intValue());
    setChatterTime(xml.chatter.first().chatterTime.getValue().intValue());
  }
  
  
  protected static long toMs(long seconds){
    return TimeUnit.SECONDS.toMillis(seconds);
  }
  
  protected static long toSecs(long ms){
    return TimeUnit.MILLISECONDS.toSeconds(ms);
  }
}