/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain;

import com.g3schema.ns_vpoint.BinaryPointT;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * Interface of standard binary virtual point.
 *
 * @see {@link IStandardPoint}
 * @see {@link BinaryPoint}
 */
public interface StdBinaryPoint extends IMapToChannel, IStandardPoint, BinaryPoint {

  void writeToXML(BinaryPointT xml, VirtualPointWriteSupport support);

  void readFromXML(BinaryPointT xml, VirtualPointReadSupport support);
}