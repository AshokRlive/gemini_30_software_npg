/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

/**
 * Interface of Double Binary Virtual Point.
 */
public interface DoubleBinaryPoint extends DigitalPoint {

  String SOURCE_NAME_A = "Source[0]";

  String SOURCE_NAME_B = "Source[1]";

}
