/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.stub;

import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

public class VirtualPointSourceStub extends AbstractNode implements IVirtualPointSource {

  final private Module module = new ModuleStub();


  public VirtualPointSourceStub() {
    super(NodeType.CHANNEL);
    setName("StubSource");
  }

  public VirtualPointSourceStub(NodeType type) {
    super(type);
    setName("StubSource");
  }

  @Override
  public boolean checkSourceModule(Module module) {
    return false;
  }

  @Override
  public boolean checkSourceModule(MODULE module) {
    return false;
  }

  @Override
  public Module getSourceModule() {
    return module;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {

  }

  @Override
  public String getSourceName() {
    return null;
  }

}
