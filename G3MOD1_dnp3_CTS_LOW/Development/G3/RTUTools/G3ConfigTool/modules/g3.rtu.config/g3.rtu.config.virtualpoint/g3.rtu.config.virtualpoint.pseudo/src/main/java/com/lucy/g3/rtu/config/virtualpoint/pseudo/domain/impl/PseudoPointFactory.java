/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractAnaloguePoint;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;

/**
 * Factory for creating virtual point objects in different types.
 */
public class PseudoPointFactory {
  private static Logger log = Logger.getLogger(PseudoPointFactory.class);
  
  public static PseudoBinaryPoint createPseudoBinaryPoint(String name, IPseudoPointSource source, int id) {
    return new PseudoBinaryPointImpl(name, source, id);
  }

  public static PseudoBinaryPoint createPseudoBinaryPoint(IPseudoPointSource source, ILogicPointEnum e) {
    PseudoBinaryPoint point = new PseudoBinaryPointImpl(source, e);
    return point;
  }

  public static PseudoDoubleBinaryPoint createPseudoDBinaryPoint(String name, IPseudoPointSource source, int id) {
    return new PseudoDoubleBinaryPointImpl(name, source, id);
  }

  public static PseudoDoubleBinaryPoint createPseudoDBinaryPoint(IPseudoPointSource source, ILogicPointEnum e) {
    return new PseudoDoubleBinaryPointImpl(source, e);
  }

  public static PseudoAnaloguePoint createPseudoAnalogPoint(String name, IPseudoPointSource source, int id) {
    PseudoAnaloguePointImp point = new PseudoAnaloguePointImp(name, source, id);
    return point;
  }

  public static PseudoAnaloguePoint createPseudoAnalogPoint(IPseudoPointSource source, ILogicPointEnum e) {
    PseudoAnaloguePointImp point = new PseudoAnaloguePointImp(source, e);

    if (e.getDefaultScalingFactor() != null) {
      
      try {
        double rawMin = point.getRawMin();
        double rawMax = point.getRawMax();
        double scaling = e.getDefaultScalingFactor();
        double offset = point.getOffset();
        String unit = e.getUnit();
        
        AbstractAnaloguePoint.calculateScaling(point, rawMin, rawMax, scaling, offset, unit);

      } catch (Exception e1) {
        log.error(e1);
      }
    }

    

    return point;
  }

  public static PseudoCounterPoint createPseudoCounterPoint(String description,
      IPseudoPointSource source, int id) {
    return new PseudoCounterPointImpl(description, source, id);
  }

  public static PseudoCounterPoint createPseudoCounterPoint(
      IPseudoPointSource source, ILogicPointEnum e) {
    return new PseudoCounterPointImpl(source, e);
  }

  public static IPseudoPoint createPseudoPoint(VirtualPointType type, int id, IPseudoPointSource source) {
    return createPseudoPoint(type, id, source, null);
  }
  
  public static IPseudoPoint createPseudoPoint(VirtualPointType type, int id, IPseudoPointSource source, String label) {
    Preconditions.checkNotNull(type, "Virtual Point type must not be null");

    IPseudoPoint point = null;
    if(Strings.isBlank(label)) {
      label = type.getName();
    }
    
    switch (type) {
    case ANALOGUE_INPUT:
      point = createPseudoAnalogPoint(label, source, id);
      break;

    case BINARY_INPUT:
      point = createPseudoBinaryPoint(label, source, id);
      break;

    case DOUBLE_BINARY_INPUT:
      point = createPseudoDBinaryPoint(label, source, id);
      break;

    case COUNTER:
      point = createPseudoCounterPoint(label, source, id);
      break;

    default:
      throw new IllegalArgumentException("Unsupported point type: " + type);
    }

    return point;
  }
}
