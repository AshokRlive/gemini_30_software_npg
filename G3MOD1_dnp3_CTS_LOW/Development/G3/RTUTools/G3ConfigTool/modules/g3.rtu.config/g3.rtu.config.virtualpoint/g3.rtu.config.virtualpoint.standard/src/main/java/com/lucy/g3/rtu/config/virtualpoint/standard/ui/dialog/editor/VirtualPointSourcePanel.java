/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.AbstractWrappedValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.rtu.config.virtualpointsource.ui.VirtualPointSourceSelector;

/**
 * The panel for configuring the source of a virtual point.
 */
class VirtualPointSourcePanel extends JPanel {

  private PresentationModel<? extends VirtualPoint> pm;
  private List<IVirtualPointSource> sourcesList;

  private final boolean isStandard;// The group of point


  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private VirtualPointSourcePanel() {
    this.isStandard = true;
    initComponents();
  }

  public VirtualPointSourcePanel(PresentationModel<? extends VirtualPoint> pm, List<IVirtualPointSource> sourceList) {
    this.pm = pm;
    this.sourcesList = sourceList;
    this.isStandard = pm.getBean() instanceof IStandardPoint;

    initComponents();
    initComponentsBinding();
    initComponentsState();
    initEventHandling();
  }

  private void initComponentsState() {
    /* Only standard point support changing source and descriptions */
    comboSource.setEnabled(isStandard);
    btnChooseSource.setVisible(isStandard);
    checkboxCutomise.setVisible(isStandard);
  }

  private void initComponentsBinding() {
    // Name
    Bindings.bind(lblName, pm.getBufferedModel(VirtualPoint.PROPERTY_NAME));

    // Description
    Bindings.bind(tfDescription, pm.getBufferedComponentModel(VirtualPoint.PROPERTY_DESCRIPTION));

    // Customise checkBox
    Bindings.bind(checkboxCutomise, pm.getBufferedModel(VirtualPoint.PROPERTY_CUSTOM_DESCRIPTION_ENABLED));

    // Source
    Bindings.bind(comboSource, new ComboBoxAdapter<IVirtualPointSource>(sourcesList,
        pm.getBufferedModel(VirtualPoint.PROPERTY_SOURCE)));

    // Seconds source
    if (pm.getBean() instanceof StdDoubleBinaryPoint) {
      Bindings.bind(comboSource2, new ComboBoxAdapter<IVirtualPointSource>(sourcesList,
          pm.getBufferedModel(StdDoubleBinaryPoint.PROPERTY_SECOND_CHANNEL)));
    }

  }

  private void initEventHandling() {
    btnChooseSource.addActionListener(new SourceChoosingHandler(sourcesList,
        pm.getBufferedModel(VirtualPoint.PROPERTY_SOURCE)));

    if (pm.getBean() instanceof StdDoubleBinaryPoint) {
      btnChooseSource2.addActionListener(new SourceChoosingHandler(sourcesList,
          pm.getBufferedModel(StdDoubleBinaryPoint.PROPERTY_SECOND_CHANNEL)));
    }

    VirtualPoint point = pm.getBean();
    
    // Sync the enable state of description field
    PropertyConnector.connectAndUpdate(
        pm.getBufferedModel(VirtualPoint.PROPERTY_CUSTOM_DESCRIPTION_ENABLED),
        pm.getBufferedComponentModel(VirtualPoint.PROPERTY_DESCRIPTION),
        AbstractWrappedValueModel.PROPERTY_EDITABLE);

    FormattedTextFieldSupport.installCommitOnType(tfDescription);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    label2 = new JLabel();
    lblName = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    lblSource = new JLabel();
    comboSource = new JComboBox<>();
    btnChooseSource = new JButton();
    lblSource2 = new JLabel();
    comboSource2 = new JComboBox<>();
    btnChooseSource2 = new JButton();
    label1 = new JLabel();
    tfDescription = new JFormattedTextField();
    checkboxCutomise = new JCheckBox();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "fill:default, $ugap, fill:default, 3*($lgap, default)"));

    //---- label2 ----
    label2.setText("Name:");
    add(label2, CC.xy(1, 1));
    add(lblName, CC.xywh(3, 1, 3, 1));

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Basic");
    add(xTitledSeparator1, CC.xywh(1, 3, 5, 1));

    //---- lblSource ----
    lblSource.setText("Source:");
    add(lblSource, CC.xy(1, 5));
    add(comboSource, CC.xy(3, 5));

    //---- btnChooseSource ----
    btnChooseSource.setText("Select...");
    add(btnChooseSource, CC.xy(5, 5));

    //---- lblSource2 ----
    lblSource2.setText("Source[1]:");
    lblSource2.setVisible(false);
    add(lblSource2, CC.xy(1, 7));

    //---- comboSource2 ----
    comboSource2.setVisible(false);
    add(comboSource2, CC.xy(3, 7));

    //---- btnChooseSource2 ----
    btnChooseSource2.setText("Select...");
    btnChooseSource2.setVisible(false);
    add(btnChooseSource2, CC.xy(5, 7));

    //---- label1 ----
    label1.setText("Description:");
    add(label1, CC.xy(1, 9));
    add(tfDescription, CC.xy(3, 9));

    //---- checkboxCutomise ----
    checkboxCutomise.setText("Customise");
    add(checkboxCutomise, CC.xy(5, 9));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label2;
  private JLabel lblName;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel lblSource;
  private JComboBox<IVirtualPointSource> comboSource;
  private JButton btnChooseSource;
  private JLabel lblSource2;
  private JComboBox<IVirtualPointSource> comboSource2;
  private JButton btnChooseSource2;
  private JLabel label1;
  private JFormattedTextField tfDescription;
  private JCheckBox checkboxCutomise;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  public boolean isSecondChannelDisplayed() {
    return comboSource2.isVisible();
  }

  public void setSecondChannelDisplayed(boolean enabled) {
    comboSource2.setVisible(enabled);
    lblSource2.setVisible(enabled);
    btnChooseSource2.setVisible(enabled);

    if (enabled) {
      lblSource.setText("Source[0]:");
    } else {
      lblSource.setText("Source:");
    }
  }


  private class SourceChoosingHandler implements ActionListener {

    private final List<IVirtualPointSource> allSources;
    private final ValueModel channelVM;


    public SourceChoosingHandler(List<IVirtualPointSource> allchs,
        ValueModel channelVM) {
      super();
      this.allSources = allchs;
      this.channelVM = channelVM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      JComponent component = (JComponent) e.getSource();
      JDialog parent = (JDialog) SwingUtilities.getWindowAncestor(component);

      /* Get current channel */
      IVirtualPointSource currentCh = (IVirtualPointSource) channelVM.getValue();

      /* Show channel selector dialog */
      VirtualPointSourceSelector chooser = new VirtualPointSourceSelector(parent, currentCh,
          allSources, "Select a input source for the point");
      IVirtualPointSource ch = chooser.showDialog();
      if (chooser.hasBeenAffirmed()) {
        channelVM.setValue(ch);
      }
    }
  }
}
