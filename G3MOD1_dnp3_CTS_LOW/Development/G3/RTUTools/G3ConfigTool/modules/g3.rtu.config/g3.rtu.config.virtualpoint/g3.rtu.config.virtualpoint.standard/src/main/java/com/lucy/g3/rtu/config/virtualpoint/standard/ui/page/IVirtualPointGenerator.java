/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.page;

import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.generator.IConfigGenerator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;


/**
 *
 */
public interface IVirtualPointGenerator extends IConfigGenerator {

  IStandardPoint createStandardVirtualPoint(IChannel ch);

  IStandardPoint createVirtualPoint(IChannel ch);
  
  void createDefaultLabelSets(ValueLabelSetManager valueLabelManager);

}

