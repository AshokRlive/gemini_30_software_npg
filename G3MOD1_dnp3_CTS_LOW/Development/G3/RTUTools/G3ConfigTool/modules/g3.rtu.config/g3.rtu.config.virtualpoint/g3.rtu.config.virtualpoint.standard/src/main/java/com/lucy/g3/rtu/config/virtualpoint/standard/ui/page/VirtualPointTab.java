/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.page;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.ext.swing.table.HighlightPredicateAdapter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.NavigationEvent;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The tab panel for managing a list of virtual points.
 */
class VirtualPointTab extends JPanel {

  public static final String ACTION_KEY_PACK_COLUMNS = "packColumns";

  private final VirtualPointPage parent;
  private final VirtualPointPageModel pm;

  private Action navAction;


  public VirtualPointTab(VirtualPointPageModel pm, VirtualPointPage parent) {
    super(new BorderLayout());

    this.pm = Preconditions.checkNotNull(pm, "PointManageModel is null");
    this.parent = parent;

    initComponents();
    this.setName("panel" + pm.getModelName());
  }

  @org.jdesktop.application.Action
  public void packColumns() {
    if (pointsTable != null) {
      Action action = pointsTable.getActionMap().get(JXTable.PACKALL_ACTION_COMMAND);
      if (action != null) {
        action.actionPerformed(new ActionEvent(pointsTable, 0, JXTable.PACKALL_ACTION_COMMAND));
      } else {
        Logger.getLogger(getClass()).error("Pack action not found");
      }
    }
  }

  private Action getNavAction() {
    if (navAction == null) {
      navAction = new NavigationAction();
      // Sync enable property
      PropertyConnector.connect(pm, VirtualPointPageModel.PROPERTY_EDITITABLE, navAction, "enabled").updateProperty2();
    }
    return navAction;
  }

  private void createUIComponents() {
    /* Create popup menu */
    popMenuVirtual = new JPopupMenu();
    popMenuVirtual.setFocusable(false);

    popMenuVirtual.add(pm.getAction(VirtualPointPageModel.ACTION_KEY_ADD));
    popMenuVirtual.add(pm.getAction(VirtualPointPageModel.ACTION_KEY_INSERT));
    popMenuVirtual.add(pm.getAction(VirtualPointPageModel.ACTION_KEY_EDIT));
    popMenuVirtual.add(pm.getAction(VirtualPointPageModel.ACTION_KEY_REMOVE));
    popMenuVirtual.add(getNavAction());

    // ====== Create point table ======
    pointsTable = createPointsTable();

    controlPanel = createControlPanel();
  }

  private JPanel createControlPanel() {
    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    panelBuilder.setDefaultDialogBorder();

    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_ADD)));
    panelBuilder.addRelatedGap();

    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_INSERT)));
    panelBuilder.addRelatedGap();

    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_EDIT)));
    panelBuilder.addRelatedGap();

    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_DUPLICATE)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_REMOVE)));

    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_MOVE_UP)));
    panelBuilder.addRelatedGap();
    panelBuilder.addGridded(new JButton(pm.getAction(VirtualPointPageModel.ACTION_KEY_MOVE_DOWN)));

    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(new JButton(getNavAction()));

    panelBuilder.addUnrelatedGap();

    // Add point amount label.
    panelBuilder.addUnrelatedGap();
    panelBuilder.addGridded(BasicComponentFactory.createLabel(
        new PropertyAdapter<VirtualPointPageModel>(pm,
            VirtualPointPageModel.PROPERTY_POINT_NUM_TEXT, true)));

    JPanel panel = panelBuilder.getPanel();

    // Click panel to clear selection.
    panel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        pm.clearSelection();
      }

    });
    return panel;
  }

  private JXTable createPointsTable() {
    final JXTable table = new JXTable(pm.getTableModel());

    // Set default configuration
    UIUtils.setDefaultConfig(table, false, true, true);
    
    table.addMouseListener(new PointsTableMouseHandler());

    /*
     * Turn off automatic editing of JTable as it is not working properly. If it is on, when pressing Enter key, it
     * shows editor dialog, while we expect stop editing
     */
    table.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);

    Bindings.bind(table, pm.getListModel(), pm.getSelectionModel());

    // Add high-lighter
    TableModel tm = table.getModel();
    if (tm instanceof ISupportHighlight) {
      HighlightPredicate predicator = new HighlightPredicateAdapter((ISupportHighlight) tm);
      table.addHighlighter(new ColorHighlighter(predicator, UIThemeResources.COLOUR_ERROR_BACKGROUND, Color.black));
    }
    table.addHighlighter(new ColorHighlighter(
        HighlightPredicate.ROLLOVER_ROW, UIThemeResources.COLOUR_TABLE_ROLLOVER, null));

    // Change column packing action
    table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    table.setHorizontalScrollEnabled(true);

    // Set keyboard action
    table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Edit");
    table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
    table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, InputEvent.ALT_DOWN_MASK), "MoveUp");
    table.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, InputEvent.ALT_DOWN_MASK), "MoveDown");
    table.getActionMap().put("Delete", pm.getAction(VirtualPointPageModel.ACTION_KEY_REMOVE));
    table.getActionMap().put("Edit", pm.getAction(VirtualPointPageModel.ACTION_KEY_EDIT));
    table.getActionMap().put("MoveUp", pm.getAction(VirtualPointPageModel.ACTION_KEY_MOVE_UP));
    table.getActionMap().put("MoveDown", pm.getAction(VirtualPointPageModel.ACTION_KEY_MOVE_DOWN));

    // Scroll to new added point automatically
    table.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(TableModelEvent e) {
        table.scrollRowToVisible(e.getFirstRow());
      }
    });

    return table;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    scrollPane1 = new JScrollPane();

    // ======== this ========
    setLayout(new BorderLayout());
    add(controlPanel, BorderLayout.EAST);

    // ======== contentPanel ========
    {
      contentPanel.setLayout(new BorderLayout());

      // ======== scrollPane1 ========
      {
        scrollPane1.setBorder(BorderFactory.createEmptyBorder());

        // ---- pointsTable ----
        pointsTable.setColumnControlVisible(true);
        scrollPane1.setViewportView(pointsTable);
      }
      contentPanel.add(scrollPane1, BorderLayout.CENTER);
    }
    add(contentPanel, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel controlPanel;
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JXTable pointsTable;
  private JPopupMenu popMenuVirtual;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class PointsTableMouseHandler extends MouseAdapter {

    @Override
    public void mousePressed(MouseEvent e) {
      mayshowPopupMenu(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      mayshowPopupMenu(e);
    }

    // Double click to edit
    @Override
    public void mouseClicked(MouseEvent e) {
      JTable table = (JTable) e.getSource();
      if (UIUtils.isDoubleClick(e)) {
        ActionEvent event = new ActionEvent(e.getSource(), e.getID(), "edit");
        Action editAction = table.getActionMap().get("Edit");
        if (editAction != null) {
          editAction.actionPerformed(event);
        }
      }
    }

    private void mayshowPopupMenu(MouseEvent e) {
      if (e.isPopupTrigger()) {

        // Re-select point if no multi points have been selected.
        if (pm.getSelectedItems().length <= 1) {
          doSelect(e);
        }

        popMenuVirtual.show(pointsTable, e.getX(), e.getY());
      }
    }

    private void doSelect(MouseEvent e) {
      int rowInView = pointsTable.rowAtPoint(e.getPoint());
      int rowInMode = pointsTable.convertRowIndexToModel(rowInView);
      pm.getSelectionModel().setSelectionInterval(rowInMode, rowInMode);
    }

  }

  private final class NavigationAction extends AbstractAction {

    public NavigationAction() {
      super("View Source");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      VirtualPoint selPoint = pm.getSelection();
      Object source = null;

      if (selPoint != null) {
        if (selPoint instanceof IStandardPoint) {
          source = selPoint.getSourceModule();
        } else {
          source = selPoint.getSource();
        }
      }

      if (source != null) {
        parent.fireNavEvent(new NavigationEvent(source));
      }
    }
  }
}
