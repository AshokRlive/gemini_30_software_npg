/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.label;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;

/**
 * The manager of {@linkplain ValueLabelSet}.
 */
public class ValueLabelSetManager {

  private Logger log = Logger.getLogger(ValueLabelSetManager.class);

  private final HashMap<String, ValueLabelSet> labelSets;
  private final ArrayListModel<ValueLabelSet> labelSetsListModel;


  public ValueLabelSetManager() {
    labelSets = new HashMap<>();
    labelSetsListModel = new ArrayListModel<>();
    labelSetsListModel.add(null); // Null element is required for configuring no label set.
  }

  public boolean add(ValueLabelSet labelSet) {
    if (labelSet == null) {
      return false;
    }

    if (labelSets.containsKey(labelSet.getId())) {
      return false;
    }

    labelSets.put(labelSet.getId(), labelSet);
    labelSetsListModel.add(labelSet);
    labelSet.setManager(this);
    return true;
  }

  public void remove(ValueLabelSet labelSet) {
    if (labelSet == null) {
      log.error("Cannot remove null labelSet");
    }

    labelSets.remove(labelSet.getId());
    labelSet.setManager(null);
    labelSetsListModel.remove(labelSet);

    ArrayList<IValueLabelSetUser> users = labelSet.getUsers();
    for (IValueLabelSetUser user : users) {
      user.setLabelSet(null);
    }
  }

  public ValueLabelSet get(String id) {
    return labelSets.get(id);
  }

  public boolean contains(String id) {
    return labelSets.containsKey(id);
  }

  @SuppressWarnings("unchecked")
  public ListModel<ValueLabelSet> getLabelSetsListModel() {
    return labelSetsListModel;
  }

  public Collection<ValueLabelSet> getAllLabelSets() {
    return labelSets.values();
  }

}
