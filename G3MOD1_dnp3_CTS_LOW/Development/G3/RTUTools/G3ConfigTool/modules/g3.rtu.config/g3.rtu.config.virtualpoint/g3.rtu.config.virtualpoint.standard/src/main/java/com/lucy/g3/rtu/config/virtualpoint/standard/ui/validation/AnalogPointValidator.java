/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint.FILTER;
import com.lucy.g3.rtu.config.virtualpoint.shared.validation.HighHighLowLowValidator;

/**
 * This class is for validating the buffer value in AnaloguePoint PresentationModel.
 */
public final class AnalogPointValidator extends BufferValidator {

  private static final double OFFSET_MIN = -Double.MAX_VALUE;
  private static final double OFFSET_MAX = Double.MAX_VALUE;

  private static final double OVERFLOW_MIN = -Double.MAX_VALUE;
  private static final double OVERFLOW_MAX = Double.MAX_VALUE;

  private static final double UNDERFLOW_MIN = -Double.MAX_VALUE;
  private static final double UNDERFLOW_MAX = Double.MAX_VALUE;

  private static final double HYSTERESIS_MIN = -Double.MAX_VALUE;
  private static final double HYSTERESIS_MAX = Double.MAX_VALUE;

  private static final int UNIT_LENGTH_MIN = 0;
  private static final int UNIT_LENGTH_MAX = 50;

  // Filter 0
  private static final int F0DEADBAND_MIN = Integer.MIN_VALUE;
  private static final int F0DEADBAND_MAX = Integer.MAX_VALUE;

  // Filter 1
  private static final double F1LOWERLIMIT_MIN = -Double.MAX_VALUE;
  private static final double F1LOWERLIMIT_MAX = Double.MAX_VALUE;
  private static final double F1UPPERLIMIT_MIN = -Double.MAX_VALUE;
  private static final double F1UPPERLIMIT_MAX = Double.MAX_VALUE;
  private static final double F1HYSTERESIS_MIN = -Double.MAX_VALUE;
  private static final double F1HYSTERESIS_MAX = Double.MAX_VALUE;

  private final HighHighLowLowValidator hihiloloValidator;


  public AnalogPointValidator(PresentationModel<? extends AnaloguePoint> target,
      PresentationModel<HighHighLowLow> hihilolo) {
    super(target);
    hihiloloValidator = new HighHighLowLowValidator(hihilolo);
  }

  private void validateFilter(ValidationResult result) {
    FILTER filter = (FILTER) target.getBufferedValue(AnaloguePoint.PROPERTY_FILTER_TYPE);
    if (filter == null) {
      return;
    }

    switch (filter) {
    case DeadBand:
      BufferValidator.validatePropertyBoundary(result, target,
          AnaloguePoint.PROPERTY_FILTER0_DEADBAND,
          F0DEADBAND_MIN, F0DEADBAND_MAX);
      break;

    case LowerUpperLimit:
      BufferValidator.validatePropertyBoundary(result, target,
          AnaloguePoint.PROPERTY_FILTER1_LOWER_HYSTERESIS,
          F1HYSTERESIS_MIN, F1HYSTERESIS_MAX);

      BufferValidator.validatePropertyBoundary(result, target,
          AnaloguePoint.PROPERTY_FILTER1_UPPER_HYSTERESIS,
          F1HYSTERESIS_MIN, F1HYSTERESIS_MAX);

      BufferValidator.validatePropertyBoundary(result, target,
          AnaloguePoint.PROPERTY_FILTER1_LOWER_LIMIT,
          F1LOWERLIMIT_MIN, F1LOWERLIMIT_MAX);

      BufferValidator.validatePropertyBoundary(result, target,
          AnaloguePoint.PROPERTY_FILTER1_UPPER_LIMIT,
          F1UPPERLIMIT_MIN, F1UPPERLIMIT_MAX);
      break;

    case HighHighLowLow:
      hihiloloValidator.validate(result);
      break;

    case None:
      break;

    default:
      throw new IllegalStateException("Unsupported filter type: " + filter);
    }
  }

  @Override
  public void validate(ValidationResult result) {

    // Validate offset
    validatePropertyBoundary(result, target, AnaloguePoint.PROPERTY_OFFSET,
        OFFSET_MIN, OFFSET_MAX);

    // Validate overflow
    validatePropertyBoundary(result, target, AnaloguePoint.PROPERTY_OVERFLOW,
        OVERFLOW_MIN, OVERFLOW_MAX);

    // Validate underflow
    validatePropertyBoundary(result, target, AnaloguePoint.PROPERTY_UNDERFLOW,
        UNDERFLOW_MIN, UNDERFLOW_MAX);

    // Validate underflow hysteresis
    validatePropertyBoundary(result, target, AnaloguePoint.PROPERTY_UNDERFLOW_HYSTERESIS,
        HYSTERESIS_MIN, HYSTERESIS_MAX);

    // Validate overflow hysteresis
    validatePropertyBoundary(result, target, AnaloguePoint.PROPERTY_OVERFLOW_HYSTERESIS,
        HYSTERESIS_MIN, HYSTERESIS_MAX);

    validatePropertyStringLength(result, target, AnaloguePoint.PROPERTY_UNIT, UNIT_LENGTH_MIN, UNIT_LENGTH_MAX, true);

    // Validate filter
    validateFilter(result);

    // Validate source
    validatePropertyEmpty(result, target, VirtualPoint.PROPERTY_SOURCE);
  }

}
