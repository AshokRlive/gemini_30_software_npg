/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import com.g3schema.ns_vpoint.BinaryPointT;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.validation.StandardPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain StdBinaryPoint}.
 */
public final class StdBinaryPointImpl extends AbstractBinaryPoint implements StdBinaryPoint {

  private final IValidator validator;


  StdBinaryPointImpl(int id, String desp) {
    super(STANDARD_POINT_GROUP, id, desp, VirtualPointType.BINARY_INPUT);
    validator = new StandardPointValidator(this);
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }

  @Override
  public StdBinaryPointImpl duplicate() {
    StdBinaryPointImpl copy = new StdBinaryPointImpl(0, getDescription());
    copy.setInvert(isInvert());
    copy.setChatterNo(getChatterNo());
    copy.setChatterTime(getChatterTime());
    copy.setChannel(getChannel());
    copy.setDelay(getDelay());
    copy.setDelayEnabled(isDelayEnabled());
    copy.setDelay0(getDelay0());
    copy.setDelay1(getDelay1());
    copy.setDrivenMode(getDrivenMode());
    return copy;
  }

  @Override
  public IChannel getChannel() {
    return (IChannel) getSource();
  }

  @Override
  public void setChannel(IChannel channel) {
    setSource(channel);
  }

  @Override
  public void setSource(IVirtualPointSource source)
      throws NotConnectibleException {
    SourceChecker.checkChannel(this, source);
    super.setSource(source);
  }

  // Override to change visibility to public
  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return getChannel() == null ? null : getChannel().predefined().getDefaultCreateOption();
  }

  @Override
  public void writeToXML(BinaryPointT xml, VirtualPointWriteSupport support) {
    super.writeToXML(xml);

    // Channel
    IChannel channel = getChannel();
    if (channel != null) {
      support.writeChannelRef(this.getName(), xml.channel.append(), channel);
    }
  }

  @Override
  public void readFromXML(BinaryPointT xml, VirtualPointReadSupport support) {
    super.readFromXML(xml, support);

    IChannel ch = support.getChannelByRef(xml.channel.first(), ChannelType.DIGITAL_INPUT);
    setChannel(ch);
  }

}