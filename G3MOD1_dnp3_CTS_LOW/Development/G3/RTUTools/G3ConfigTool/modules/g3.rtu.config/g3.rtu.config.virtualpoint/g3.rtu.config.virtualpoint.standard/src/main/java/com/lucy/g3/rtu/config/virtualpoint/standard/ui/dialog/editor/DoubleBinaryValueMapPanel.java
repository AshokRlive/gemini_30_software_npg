/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import static com.jgoodies.validation.view.ValidationComponentUtils.setMessageKey;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap.PROPERTY_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap.PROPERTY_VALUE00;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap.PROPERTY_VALUE01;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap.PROPERTY_VALUE10;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap.PROPERTY_VALUE11;

import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterFactory.BooleanNegator;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.JCollapsiblePane;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;

/**
 * The panel for configuring value map of double binary point.
 */
class DoubleBinaryValueMapPanel extends JPanel {

  private final PresentationModel<DoubleBinaryValueMap> pm;

  private ValueLabelSet labelSet;

  public DoubleBinaryValueMapPanel() {
    pm = new PresentationModel<DoubleBinaryValueMap>();
    initComponents();
    ftf00.setValue(0);
    ftf01.setValue(1);
    ftf10.setValue(2);
    ftf11.setValue(3);
  }

  public DoubleBinaryValueMapPanel(PresentationModel<DoubleBinaryValueMap> pm) {
    this.pm = pm;
    initComponents();
    initComponentAnnotations();
    initComponentsBinding();
    initEventHandling();
  }

  private void initEventHandling() {
    /* Observe value change and update related label strings.*/
    pm.getBufferedComponentModel(PROPERTY_VALUE00).addValueChangeListener(new LabelUpdateHandler(lbl0));
    pm.getBufferedComponentModel(PROPERTY_VALUE01).addValueChangeListener(new LabelUpdateHandler(lbl1));
    pm.getBufferedComponentModel(PROPERTY_VALUE10).addValueChangeListener(new LabelUpdateHandler(lbl2));
    pm.getBufferedComponentModel(PROPERTY_VALUE11).addValueChangeListener(new LabelUpdateHandler(lbl3));    
  }

  public void setLabelSet(ValueLabelSet labelSet) {
    this.labelSet = labelSet;
    
    if(this.labelSet == null) {
      lbl0.setText(null);
      lbl1.setText(null);
      lbl2.setText(null);
      lbl3.setText(null);
    }else {
      // Update all value label strings
      int value ;
      String labelStr;
      
      value = (int) ftf00.getValue();
      labelStr = labelSet.getLabel((double) value); 
      lbl0.setText(labelStr);
      
      value = (int) ftf01.getValue();
      labelStr = labelSet.getLabel((double) value); 
      lbl1.setText(labelStr);
      
      value = (int) ftf10.getValue();
      labelStr = labelSet.getLabel((double) value); 
      lbl2.setText(labelStr);
      
      value = (int) ftf11.getValue();
      labelStr = labelSet.getLabel((double) value); 
      lbl3.setText(labelStr);
    }
  }


  private void initComponentsBinding() {
    ValueModel vm = pm.getBufferedComponentModel(PROPERTY_ENABLED);
    vm = new ConverterValueModel(vm, new BooleanNegator());
    PropertyConnector.connectAndUpdate(vm, panelValues, "collapsed");

    lblSourceA.setText(DoubleBinaryPoint.SOURCE_NAME_A);
    lblSourceB.setText(DoubleBinaryPoint.SOURCE_NAME_B);
  }
  

  private void createUIComponents() {
    NumberFormatter formatter = new NumberFormatter();
    formatter.setValueClass(Integer.class);

    ftf00 = BasicComponentFactory.createFormattedTextField(pm.getBufferedComponentModel(PROPERTY_VALUE00), formatter);
    ftf01 = BasicComponentFactory.createFormattedTextField(pm.getBufferedComponentModel(PROPERTY_VALUE01), formatter);
    ftf10 = BasicComponentFactory.createFormattedTextField(pm.getBufferedComponentModel(PROPERTY_VALUE10), formatter);
    ftf11 = BasicComponentFactory.createFormattedTextField(pm.getBufferedComponentModel(PROPERTY_VALUE11), formatter);

    checkBoxEnabled = BasicComponentFactory.createCheckBox(pm.getBufferedComponentModel(PROPERTY_ENABLED), "");
  }

  private void initComponentAnnotations() {
    setMessageKey(ftf00, PROPERTY_VALUE00);
    setMessageKey(ftf01, PROPERTY_VALUE01);
    setMessageKey(ftf10, PROPERTY_VALUE10);
    setMessageKey(ftf11, PROPERTY_VALUE11);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    createUIComponents();

    separator1 = new JXTitledSeparator();
    panelValues = new JCollapsiblePane();
    lblSourceB = new JLabel();
    lblSourceA = new JLabel();
    label11 = new JLabel();
    label6 = new JLabel();
    label3 = new JLabel();
    label12 = new JLabel();
    lbl0 = new JLabel();
    label7 = new JLabel();
    label10 = new JLabel();
    label13 = new JLabel();
    lbl1 = new JLabel();
    label8 = new JLabel();
    label4 = new JLabel();
    label14 = new JLabel();
    lbl2 = new JLabel();
    label9 = new JLabel();
    label5 = new JLabel();
    label15 = new JLabel();
    lbl3 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "default:grow",
        "default, $lgap, fill:default, $lgap, fill:default:grow"));

    //---- separator1 ----
    separator1.setTitle("Custom Double Binary Values");
    add(separator1, CC.xy(1, 1));

    //======== panelValues ========
    {
      Container panelValuesContentPane = panelValues.getContentPane();
      panelValuesContentPane.setLayout(new FormLayout(
          "2*(center:default, $lcgap), 2*(default, $lcgap), default",
          "4*(default, $lgap), default"));

      //---- lblSourceB ----
      lblSourceB.setText("B");
      panelValuesContentPane.add(lblSourceB, CC.xy(1, 1));

      //---- lblSourceA ----
      lblSourceA.setText("A");
      panelValuesContentPane.add(lblSourceA, CC.xy(3, 1));

      //---- label11 ----
      label11.setText("Custom Value");
      panelValuesContentPane.add(label11, CC.xy(7, 1));

      //---- label6 ----
      label6.setText("0");
      panelValuesContentPane.add(label6, CC.xy(1, 3));

      //---- label3 ----
      label3.setText("0");
      panelValuesContentPane.add(label3, CC.xy(3, 3));

      //---- label12 ----
      label12.setText("---->");
      panelValuesContentPane.add(label12, CC.xy(5, 3));
      panelValuesContentPane.add(ftf00, CC.xy(7, 3));
      panelValuesContentPane.add(lbl0, CC.xy(9, 3));

      //---- label7 ----
      label7.setText("0");
      panelValuesContentPane.add(label7, CC.xy(1, 5));

      //---- label10 ----
      label10.setText("1");
      panelValuesContentPane.add(label10, CC.xy(3, 5));

      //---- label13 ----
      label13.setText("---->");
      panelValuesContentPane.add(label13, CC.xy(5, 5));
      panelValuesContentPane.add(ftf01, CC.xy(7, 5));
      panelValuesContentPane.add(lbl1, CC.xy(9, 5));

      //---- label8 ----
      label8.setText("1");
      panelValuesContentPane.add(label8, CC.xy(1, 7));

      //---- label4 ----
      label4.setText("0");
      panelValuesContentPane.add(label4, CC.xy(3, 7));

      //---- label14 ----
      label14.setText("---->");
      panelValuesContentPane.add(label14, CC.xy(5, 7));
      panelValuesContentPane.add(ftf10, CC.xy(7, 7));
      panelValuesContentPane.add(lbl2, CC.xy(9, 7));

      //---- label9 ----
      label9.setText("1");
      panelValuesContentPane.add(label9, CC.xy(1, 9));

      //---- label5 ----
      label5.setText("1");
      panelValuesContentPane.add(label5, CC.xy(3, 9));

      //---- label15 ----
      label15.setText("---->");
      panelValuesContentPane.add(label15, CC.xy(5, 9));
      panelValuesContentPane.add(ftf11, CC.xy(7, 9));
      panelValuesContentPane.add(lbl3, CC.xy(9, 9));
    }
    add(panelValues, CC.xy(1, 5));

    //---- checkBoxEnabled ----
    checkBoxEnabled.setText("Enabled");
    add(checkBoxEnabled, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTitledSeparator separator1;
  private JCollapsiblePane panelValues;
  private JLabel lblSourceB;
  private JLabel lblSourceA;
  private JLabel label11;
  private JLabel label6;
  private JLabel label3;
  private JLabel label12;
  private JFormattedTextField ftf00;
  private JLabel lbl0;
  private JLabel label7;
  private JLabel label10;
  private JLabel label13;
  private JFormattedTextField ftf01;
  private JLabel lbl1;
  private JLabel label8;
  private JLabel label4;
  private JLabel label14;
  private JFormattedTextField ftf10;
  private JLabel lbl2;
  private JLabel label9;
  private JLabel label5;
  private JLabel label15;
  private JFormattedTextField ftf11;
  private JLabel lbl3;
  private JCheckBox checkBoxEnabled;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  private class LabelUpdateHandler implements PropertyChangeListener {
    private final JLabel label;
    
    LabelUpdateHandler(JLabel label){
      this.label = label; 
    }
    
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      int value = (int) evt.getNewValue();
      String labelStr = labelSet == null ? null : labelSet.getLabel((double) value); 
      label.setText(labelStr);
    }
    
  }
}
