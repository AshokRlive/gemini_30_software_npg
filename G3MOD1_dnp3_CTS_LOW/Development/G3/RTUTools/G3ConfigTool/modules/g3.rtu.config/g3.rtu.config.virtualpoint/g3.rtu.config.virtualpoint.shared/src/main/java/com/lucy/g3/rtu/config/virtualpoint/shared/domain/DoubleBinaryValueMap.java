/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import com.g3schema.ns_vpoint.DoubleBinaryValueMapT;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;

/**
 * Configuration of value mapping for double binary points.
 */
public final class DoubleBinaryValueMap extends Model {

  public static final String PROPERTY_VALUE00 = "value00";
  public static final String PROPERTY_VALUE01 = "value01";
  public static final String PROPERTY_VALUE10 = "value10";
  public static final String PROPERTY_VALUE11 = "value11";

  public static final String PROPERTY_ENABLED = "enabled";

  private boolean enabled = false;

  private int value00 = 0;
  private int value01 = 1;
  private int value10 = 2;
  private int value11 = 3;


  public int getValue00() {
    return value00;
  }

  public void setValue00(int value00) {
    Object oldValue = this.value00;
    this.value00 = value00;
    firePropertyChange("value00", oldValue, value00);
  }

  public int getValue01() {
    return value01;
  }

  public void setValue01(int value01) {
    Object oldValue = this.value01;
    this.value01 = value01;
    firePropertyChange("value01", oldValue, value01);
  }

  public int getValue10() {
    return value10;
  }

  public void setValue10(int value10) {
    Object oldValue = this.value10;
    this.value10 = value10;
    firePropertyChange("value10", oldValue, value10);
  }

  public int getValue11() {
    return value11;
  }

  public void setValue11(int value11) {
    Object oldValue = this.value11;
    this.value11 = value11;
    firePropertyChange("value11", oldValue, value11);
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    Object oldValue = this.enabled;
    this.enabled = enabled;
    firePropertyChange("enabled", oldValue, enabled);
  }

  public void readXML(DoubleBinaryValueMapT xml) {
    Preconditions.checkNotNull(xml, "xml is null");

    value00 = xml.value00.getValue();
    value01 = xml.value01.getValue();
    value10 = xml.value10.getValue();
    value11 = xml.value11.getValue();
    enabled = true;
  }

  public void writeXML(DoubleBinaryValueMapT xml) {
    Preconditions.checkNotNull(xml, "xml is null");
    if (enabled) {
      xml.value00.setValue(value00);
      xml.value01.setValue(value01);
      xml.value10.setValue(value10);
      xml.value11.setValue(value11);
    }
  }

}
