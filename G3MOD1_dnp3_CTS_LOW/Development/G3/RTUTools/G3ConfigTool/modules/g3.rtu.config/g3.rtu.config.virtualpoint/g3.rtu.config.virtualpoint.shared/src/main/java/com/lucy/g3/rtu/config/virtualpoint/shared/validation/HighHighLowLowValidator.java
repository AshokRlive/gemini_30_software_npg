/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.validation;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;

/**
 * Buffer validator for {@linkplain HighHighLowLow}.
 */
public class HighHighLowLowValidator extends BufferValidator {

  public HighHighLowLowValidator(PresentationModel<HighHighLowLow> target) {
    super(target);
  }

  @Override
  public void validate(ValidationResult result) {
    try {
      ValueGroup[] values = new ValueGroup[4];
      for (int i = 0; i < values.length; i++) {
        values[i] = new ValueGroup();
      }

      values[0].value = (Double) target.getBufferedValue(HighHighLowLow.PROPERTY_HIHI_THRESHOLD);
      values[1].value = (Double) target.getBufferedValue(HighHighLowLow.PROPERTY_HI_THRESHOLD);
      values[2].value = (Double) target.getBufferedValue(HighHighLowLow.PROPERTY_LO_THRESHOLD);
      values[3].value = (Double) target.getBufferedValue(HighHighLowLow.PROPERTY_LOLO_THRESHOLD);

      values[0].enabled = (Boolean) target.getBufferedValue(HighHighLowLow.PROPERTY_HIHI_ENABLED);
      values[1].enabled = (Boolean) target.getBufferedValue(HighHighLowLow.PROPERTY_HI_ENABLED);
      values[2].enabled = (Boolean) target.getBufferedValue(HighHighLowLow.PROPERTY_LO_ENABLED);
      values[3].enabled = (Boolean) target.getBufferedValue(HighHighLowLow.PROPERTY_LOLO_ENABLED);

      values[0].key = HighHighLowLow.PROPERTY_HIHI_THRESHOLD;
      values[1].key = HighHighLowLow.PROPERTY_HI_THRESHOLD;
      values[2].key = HighHighLowLow.PROPERTY_LO_THRESHOLD;
      values[3].key = HighHighLowLow.PROPERTY_LOLO_THRESHOLD;

      if (!values[0].enabled
          && !values[1].enabled
          && !values[2].enabled
          && !values[3].enabled) {
        result.addError("Filter \"High High Low Low\" is not configured!");

      } else {
        ValueGroup invalid = findInvalidValueGroup(values);

        if (invalid != null) {
          result.addError("The high value must be equal or greater than the low value", invalid.key);
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      if (e.getMessage() != null) {
        result.addError(e.getMessage());
      }
    }
  }


  private static class ValueGroup {

    private boolean enabled;
    private double value;
    private String key;
  }


  private ValueGroup findInvalidValueGroup(ValueGroup... value) {
    for (int i = 0; i < value.length - 1; i++) {
      if (!value[i].enabled) {
        continue;
      }

      for (int j = i + 1; j < value.length; j++) {
        if (!value[j].enabled) {
          continue;
        }
        if (value[i].value < value[i + 1].value) {
          return value[i];
        }
      }
    }
    return null;
  }

}
