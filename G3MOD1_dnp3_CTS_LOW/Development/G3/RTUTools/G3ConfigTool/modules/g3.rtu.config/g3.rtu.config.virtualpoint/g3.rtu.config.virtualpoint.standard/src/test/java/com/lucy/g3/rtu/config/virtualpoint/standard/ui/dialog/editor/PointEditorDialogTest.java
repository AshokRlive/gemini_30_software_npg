/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.IVirtualPointDialogResource;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.VirtualPointDialog;

/**
 * The Class PointEditorDialogTest.
 */
public class PointEditorDialogTest {
  private static JFrame parent = null;
  
  @Before
  public void setup() {
  }

  @After
  public void tearDown() throws Exception {
  }


  private final static DummyResource RES = new DummyResource();


  private static void testAnalogDialog() {
    VirtualPointDialog.edit(parent, RES, VirtualPointFactory.createAnalogPoint());

  }

  private static void testBinaryDialog() {
    VirtualPointDialog.edit(parent, RES, VirtualPointFactory.createBinaryPoint());
  }

  private static void testDBinaryDialog() {
    VirtualPointDialog.edit(parent, RES, VirtualPointFactory.createDoubleBinaryPoint());
  }

  private static void testCounterDialog() {
    VirtualPointDialog.edit(parent, RES, VirtualPointFactory.createCounterPoint());
  }
  
  /**
   * The main method.
   *
   * @param args
   *          the arguments
   */
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(
          UIManager.getSystemLookAndFeelClassName());
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
      e.printStackTrace();
    }
    //testAnalogDialog();
    testBinaryDialog();
    testDBinaryDialog();
    //testCounterDialog();
  }


  private static class DummyResource implements IVirtualPointDialogResource {

    @Override
    public Collection<IChannel> getAllChannels(ChannelType... type) {
      return new ArrayList<IChannel>(0);
    }

    @Override
    public Collection<VirtualPoint> getAllVirtualPoints(VirtualPointType... type) {
      return new ArrayList<VirtualPoint>(0);
    }

    @Override
    public ValueLabelSetManager getLabelManager() {
      return null;
    }
  }

}
