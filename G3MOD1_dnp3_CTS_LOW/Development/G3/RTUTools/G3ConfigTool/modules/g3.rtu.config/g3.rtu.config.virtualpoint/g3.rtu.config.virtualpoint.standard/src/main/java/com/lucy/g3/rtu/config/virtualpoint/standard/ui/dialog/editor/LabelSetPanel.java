/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.labels.LabelSetEditorDialog;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.labels.LabelSetManagerDialog;

/**
 * The panel for configuring label set.
 */
class LabelSetPanel extends JPanel {

  private Logger log = Logger.getLogger(LabelSetPanel.class);

  private final ValueModel labelSetVM;
  private final ValueLabelSetManager manager;


  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private LabelSetPanel() {
    labelSetVM = new ValueHolder();
    this.manager = null;
    initComponents();
  }

  public LabelSetPanel(ValueLabelSetManager manager, ValueModel labelSetValueModel) {
    this.labelSetVM = Preconditions.checkNotNull(labelSetValueModel, "labelSetValueModel must not be null");
    this.manager = manager;

    initComponents();
    initComponentsBinding();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    xTitledSeparator2 = new JXTitledSeparator();
    panel1 = new JPanel();
    label3 = new JLabel();
    comboBoxLabelSet = new JComboBox<>();
    btnEdit = new JButton();
    btnManage = new JButton();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow",
        "default, $lgap, default"));

    // ---- xTitledSeparator2 ----
    xTitledSeparator2.setTitle("Custom Value Label");
    add(xTitledSeparator2, CC.xywh(1, 1, 3, 1));

    // ======== panel1 ========
    {
      panel1.setLayout(new FormLayout(
          "default, $lcgap, [80dlu,default,200dlu], 2*($lcgap, default)",
          "default"));

      // ---- label3 ----
      label3.setText("Value Label Set:");
      panel1.add(label3, CC.xy(1, 1));
      panel1.add(comboBoxLabelSet, CC.xy(3, 1));

      // ---- btnEdit ----
      btnEdit.setText("Edit...");
      btnEdit.setToolTipText("Edit the selected label set");
      btnEdit.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnEditActionPerformed();
        }
      });
      panel1.add(btnEdit, CC.xy(5, 1));

      // ---- btnManage ----
      btnManage.setText("Manage...");
      btnManage.setToolTipText("Manage existing label sets");
      btnManage.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnManageActionPerformed(e);
        }
      });
      panel1.add(btnManage, CC.xy(7, 1));
    }
    add(panel1, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ====== custom code ======
    comboBoxLabelSet.setRenderer(new LabelSetComboRenderer());
  }

  @SuppressWarnings("unchecked")
  private void initComponentsBinding() {
    ListModel<ValueLabelSet> labelSetList = manager == null
        ? new ArrayListModel<ValueLabelSet>()
        : manager.getLabelSetsListModel();

    ComboBoxModel<ValueLabelSet> comboModel = new ComboBoxAdapter<ValueLabelSet>(
        labelSetList,
        labelSetVM);
    comboBoxLabelSet.setModel(comboModel);

    // Clear value model when its label set has been removed
    labelSetList.addListDataListener(new ListDataListener() {

      @Override
      public void intervalRemoved(ListDataEvent e) {
        ValueLabelSet labelSet = (ValueLabelSet) labelSetVM.getValue();

        // Checks if current label set has been removed
        if (labelSet != null && labelSet.getManager() == null) {
          labelSetVM.setValue(null);
        }
      }

      @Override
      public void intervalAdded(ListDataEvent e) {
      }

      @Override
      public void contentsChanged(ListDataEvent e) {
      }
    });
  }

  private void btnEditActionPerformed() {
    ValueLabelSet labelSet = (ValueLabelSet) labelSetVM.getValue();

    if (labelSet != null) {
      Window parent = SwingUtilities.getWindowAncestor(this);
      if (parent instanceof Dialog) {
        new LabelSetEditorDialog((Dialog) parent, labelSet).setVisible(true);
      } else if (parent instanceof Frame) {
        new LabelSetEditorDialog((Frame) parent, labelSet).setVisible(true);
      } else {
        new LabelSetEditorDialog((Dialog) null, labelSet).setVisible(true);
      }
    } else {
      JOptionPane.showMessageDialog(this, "No label set has been selected!", "Warning", JOptionPane.WARNING_MESSAGE);
    }
  }

  private void btnManageActionPerformed(ActionEvent e) {
    if (manager != null) {
      Window parent = SwingUtilities.getWindowAncestor(this);
      if (parent instanceof Dialog) {
        new LabelSetManagerDialog((Dialog) parent, manager).setVisible(true);
      } else if (parent instanceof Frame) {
        new LabelSetManagerDialog((Frame) parent, manager).setVisible(true);
      } else {
        new LabelSetManagerDialog((Dialog) null, manager).setVisible(true);
      }
    } else {
      log.error("label manager is null");
    }
  }


  private static class LabelSetComboRenderer extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (value == null) {
        setText("- Not Configured -");
      }

      return comp;
    }

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTitledSeparator xTitledSeparator2;
  private JPanel panel1;
  private JLabel label3;
  private JComboBox<ValueLabelSet> comboBoxLabelSet;
  private JButton btnEdit;
  private JButton btnManage;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
