/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.channel.domain.ChannelStub;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointSourceStub;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.StdAnaloguePointImpl;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.StdBinaryPointImpl;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.StdDoubleBinaryPointImpl;

/**
 * The Class StdChannelPointTest.
 */
public class StdChannelPointTest {

  private IStandardPoint[] stdPoints = new IStandardPoint[3];


  @Before
  public void setUp() throws Exception {
    stdPoints[0] = new StdBinaryPointImpl(0, null);
    stdPoints[1] = new StdDoubleBinaryPointImpl(0, null);
    stdPoints[2] = new StdAnaloguePointImpl(0, "");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetGroup() {
    for (IStandardPoint p : stdPoints) {
      assertEquals("The group of standard point should be " + IStandardPoint.STANDARD_POINT_GROUP,
          IStandardPoint.STANDARD_POINT_GROUP, p.getGroup());
    }
  }

  @Test
  public void testSetChannel() {
    for (IStandardPoint p : stdPoints) {
      assertNull(p.getSource());

      if (p.getType().isAnalogue()) {
        p.setSource(new ChannelStub(ChannelType.ANALOG_INPUT));
      } else {
        p.setSource(new ChannelStub(ChannelType.DIGITAL_INPUT));
      }
      assertNotNull(p.getSource());

      p.setSource(null);
      assertNull(p.getSource());
    }
  }

  @Test
  public void testSetNonChannelSource() {
    for (IStandardPoint p : stdPoints) {
      try{
        p.setSource(new VirtualPointSourceStub());
        fail("Non-channel source shouldn't be set succefully");
      }catch(Exception e) {
        // expected
      }
      
    }
  }
  
  @Test
  public void testSetWrongTypeChannel() {
    for (IStandardPoint p : stdPoints) {
      assertNull(p.getSource());

      try {
        if (p.getType().isAnalogue()) {
          p.setSource(new ChannelStub(ChannelType.DIGITAL_INPUT));
        } else {
          p.setSource(new ChannelStub(ChannelType.ANALOG_INPUT));
        }

        fail("IllegalArgumentException expected if trying to set a wrong type channel");
      } catch (IllegalArgumentException e) {
        // Nothing to do
      }

      assertNull("No channel should be null", p.getSource());//
    }
  }

  @Test
  public void testDeleteChannel() {
    for (IStandardPoint p : stdPoints) {
      IChannel ch = createChannelForPoint(p);
      p.setSource(ch);
      assertNotNull(p.getSource());
      assertTrue(p.getSource() == ch);

      ch.delete();
      if (p.getSource() != null) {
        fail("The point should not map to a channel that is deleted:" + p);
      }
    }
  }

  private IChannel createChannelForPoint(IStandardPoint p) {
    if (p.getType().isAnalogue()) {
      return new ChannelStub(ChannelType.ANALOG_INPUT);
    } else {
      return new ChannelStub(ChannelType.DIGITAL_INPUT);
    }
  }

  @Test
  public void testDelete() {
    for (IStandardPoint p : stdPoints) {
      IChannel ch = createChannelForPoint(p);

      assertTrue(ch.getAllConnections().isEmpty());
      p.setSource(ch);
      assertFalse(ch.getAllConnections().isEmpty());// Point observes channel

      // Check PCL num
      p.addPropertyChangeListener(new DummyPCL());
      p.delete();

      // Check PCL num
      assertEquals(0, p.getPropertyChangeListeners().length);// No PCL after
      // delete
      assertTrue(ch.getAllConnections().isEmpty());// Stop observing channel
      // after delete
    }
  }


  private static class DummyPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
    }
  }
}
