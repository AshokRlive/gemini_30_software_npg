/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import com.g3schema.ns_clogic.CLogicBinaryPointT;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain PseudoBinaryPoint}.
 */
public final class PseudoBinaryPointImpl extends AbstractBinaryPoint
    implements PseudoBinaryPoint {

  private ILogicPointEnum pointXmlEnum;
  private final IValidator validator;


  PseudoBinaryPointImpl(String description, IPseudoPointSource source, int id) {
    super(DEF_PSEUDO_POINT_GROUP, id, description, VirtualPointType.BINARY_INPUT);
    validator = new PseudoPointValidator(this);
    setSource(source);
    init();
  }

  PseudoBinaryPointImpl(IPseudoPointSource source, ILogicPointEnum pointXml) {
    super(DEF_PSEUDO_POINT_GROUP, pointXml.getValue(), pointXml.getDescription(),
        VirtualPointType.BINARY_INPUT);
    validator = new PseudoPointValidator(this);
    pointXmlEnum = pointXml;
    setSource(source);
    init();
  }

  private void init() {
    updateGroup();
  }

  @Override
  public void updateGroup() {
    IPseudoPointSource src = (IPseudoPointSource) getSource();
    if (src != null) {
      setGroup(src.getGroup());
    }
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return pointXmlEnum == null ? DefaultCreateOption.DoNotCreatePoint
        : pointXmlEnum.getDefCreateOption();
  }

  @Override
  public void readFromXML(CLogicBinaryPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);
  }

  @Override
  public void writeToXML(CLogicBinaryPointT xml) {
    super.writeToXML(xml);
  }
  
  // ----------------------[ Unsupported operations ]--------------------------

  @Override
  public IValidator getValidator() {
    return validator;
  }

}
