/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.stub;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractVirtualPoint;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

public class VirtualPointStub extends AbstractVirtualPoint {

  public VirtualPointStub(VirtualPointType type) {
    this(0, 0, type);
  }

  public VirtualPointStub(int id, int group, VirtualPointType type) {
    super(id, group, "VirtualPointStub", type);
  }

  @Override
  public String getHTMLSummary() {
    return null;
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return null;
  }

  @Override
  public IValidator getValidator() {
    return null;
  }

}
