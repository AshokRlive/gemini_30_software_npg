/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DigitalPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;

/**
 * This class is for validating the buffer value in DigitalPoint PresentationModel.
 */
abstract class DigitalPointValidator extends BufferValidator {

  private static final int CHATTERNO_MIN = 0;
  private static final int CHATTERNO_MAX = 200000;
  
  private static final int CHATTERTIME_MIN = 1;
  private static final int CHATTERTIME_MAX = 200000;

  private static final int DEBOUNCE_MIN = 0;
  private static final int DEBOUNCE_MAX = 200000;

  private static final int DELAY_MIN = 0;
  private static final int DELAY_MAX = 1000000;
  
  public DigitalPointValidator(PresentationModel<? extends DigitalPoint> target) {
    super(target);
  }

  @Override
  public void validate(ValidationResult result) {
    Object bean = target.getBean();

    validatePropertyBoundary(result, target,
        DigitalPoint.PROPERTY_CHATTERNO,
        CHATTERNO_MIN, CHATTERNO_MAX);

    if ((int) (target.getBufferedModel(DigitalPoint.PROPERTY_CHATTERNO).getValue()) != 0) {
      validatePropertyBoundary(result, target,
          DigitalPoint.PROPERTY_CHATTERTIME,
          CHATTERTIME_MIN, CHATTERTIME_MAX);
    }

    validatePropertyEmpty(result, target,
        VirtualPoint.PROPERTY_SOURCE);

    /*Validate delay report*/
    validatePropertyBoundary(result, target,
        DigitalPoint.PROPERTY_DELAY0,
        DELAY_MIN, DELAY_MAX);
    validatePropertyBoundary(result, target,
        DigitalPoint.PROPERTY_DELAY1,
        DELAY_MIN, DELAY_MAX);
    if(!(bean instanceof BinaryPoint)) {
      validatePropertyBoundary(result, target,
          DigitalPoint.PROPERTY_DELAY2,
          DELAY_MIN, DELAY_MAX);
      validatePropertyBoundary(result, target,
          DigitalPoint.PROPERTY_DELAY3,
          DELAY_MIN, DELAY_MAX);
    }

    if (bean instanceof StdDoubleBinaryPoint) {
      validatePropertyEmpty(result, target,
          StdDoubleBinaryPoint.PROPERTY_SECOND_CHANNEL);

      validatePropertyBoundary(result, target,
          StdDoubleBinaryPoint.PROPERTY_DEBOUNCE,
          DEBOUNCE_MIN, DEBOUNCE_MAX);
      
      // Check two channel not mapping to the same channel.
      Object ch0 = target.getBufferedValue(StdDoubleBinaryPoint.PROPERTY_FIRST_CHANNEL);
      Object ch1 = target.getBufferedValue(StdDoubleBinaryPoint.PROPERTY_SECOND_CHANNEL);
      if (ch0 == ch1) {
        result.addError("Not allowed to select the same channel for double point");
      }
    }
  }

}
