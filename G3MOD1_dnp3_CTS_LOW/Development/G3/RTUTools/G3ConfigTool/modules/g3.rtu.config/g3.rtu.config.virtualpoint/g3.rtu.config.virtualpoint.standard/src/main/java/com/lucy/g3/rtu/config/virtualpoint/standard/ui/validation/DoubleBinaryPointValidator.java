/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation;


import com.jgoodies.binding.PresentationModel;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap;


/**
 *
 */
public class DoubleBinaryPointValidator extends DigitalPointValidator{
  private PresentationModel<DoubleBinaryValueMap> valueMap;
  
  public DoubleBinaryPointValidator(PresentationModel<DoubleBinaryPoint> target,
      PresentationModel<DoubleBinaryValueMap> valueMap) {
    super(target);
    this.valueMap = valueMap;
  }

  @Override
  public void validate(ValidationResult result) {
    super.validate(result);
    
    if(valueMap != null)
      validateValueMap(result);
  }

  private void validateValueMap(ValidationResult result) {
    boolean enabled = (boolean) valueMap.getBufferedValue(DoubleBinaryValueMap.PROPERTY_ENABLED);
    if (enabled) {
      checkDoubleValue(DoubleBinaryValueMap.PROPERTY_VALUE00, result);
      checkDoubleValue(DoubleBinaryValueMap.PROPERTY_VALUE01, result);
      checkDoubleValue(DoubleBinaryValueMap.PROPERTY_VALUE10, result);
      checkDoubleValue(DoubleBinaryValueMap.PROPERTY_VALUE11, result);
    }
  }

  private void checkDoubleValue(String key, ValidationResult result) {
    int value = (int) valueMap.getBufferedValue(key);
    if (value < 0 || value > 3) {
      result.addError("Invalid double binary value:" + value+", valid range:[0,3]", key);
    }
  }

}

