/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.VirtualPointManagerImpl;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;

/**
 * The Class VirtualPointManagerImplTest.
 */
public class VirtualPointManagerImplTest {

  private VirtualPointManagerImpl fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new VirtualPointManagerImpl(null);
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

  /**
   * Test add point.
   *
   * @throws DuplicatedException
   *           the duplicated exception
   */
  @Test
  public void testAddPoint() throws DuplicatedException {
    assertEquals(0, fixture.getAllPointsNum());
    fixture.addPoint(null); // Ignored

    IStandardPoint p0 = VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT);
    fixture.addPoint(p0);

    assertEquals(1, fixture.getAllPointsNum());
    assertTrue(fixture.contains(p0));
    assertTrue(fixture.getPoint(p0.getGroup(), p0.getId()) == p0);
  }

  @Test
  public void testAddDuplicatePoint() {
    IStandardPoint p0 = VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT);
    IStandardPoint p1 = VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT);

    fixture.addPoint(p0);
    fixture.addPoint(p1);
    assertEquals(2, fixture.getPointsNum(VirtualPointType.ANALOGUE_INPUT));

    fixture.addPoint(p0);// No effect to add duplicate point
    assertEquals(2, fixture.getPointsNum(VirtualPointType.ANALOGUE_INPUT));
  }

  /**
   * Test add all.
   *
   * @throws DuplicatedException
   *           the duplicated exception
   */
  @Test
  public void testAddAll() throws DuplicatedException {
    ArrayList<IStandardPoint> points = createStdPointList();

    // Add points to manager
    fixture.addAllPoints(points);

    // Assert the added point can be retrieved using group and id.
    for (VirtualPoint p : points) {
      assertTrue(fixture.contains(p));

      // Retrieve point
      VirtualPoint point = fixture.getPoint(p.getGroup(), p.getId());
      assertNotNull(point);
      assertTrue("Original point:" + p + " and retrieved Point:" + point + " are not equal", point == p);
    }
  }

  @Test
  public void testAddNull() throws DuplicatedException {
    ArrayList<IStandardPoint> points = new ArrayList<IStandardPoint>();
    points.add(null);
    points.add(null);
    points.add(null);
    fixture.addAllPoints(points);
    fixture.addAllPoints((ArrayList<IStandardPoint>) null);
  }

  @Test
  public void testContains() throws DuplicatedException {
    IStandardPoint p0 = VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT);

    assertFalse(fixture.contains(p0));
    assertFalse(fixture.contains(null));

    fixture.addPoint(p0);
    assertTrue(fixture.contains(p0));
  }

  @Test
  public void testRemove() throws DuplicatedException {
    IStandardPoint p0 = VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT);
    assertEquals(0, fixture.getAllPointsNum());

    fixture.addPoint(p0);
    assertEquals(1, fixture.getAllPointsNum());

    fixture.removePoint(p0);
    assertEquals(0, fixture.getAllPointsNum());
  }

  @Test
  public void testRemoveAll() throws DuplicatedException {
    ArrayList<IStandardPoint> points = createStdPointList();
    fixture.removeAllPoints(points);

    // Add points to manager
    fixture.addAllPoints(points);
    assertEquals(points.size(), fixture.getAllPointsNum());

    fixture.removeAllPoints(points);
    assertEquals(0, fixture.getAllPointsNum());

  }

  @Test
  public void testRemoveNull() {
    fixture.removePoint(null);
    fixture.removeAllPoints(null);

    ArrayList<IStandardPoint> points = new ArrayList<IStandardPoint>();
    points.add(null);
    points.add(null);
    points.add(null);
    fixture.removeAllPoints(points);
  }

  @Test
  public void testAddAllPseudo() throws DuplicatedException {
    assertEquals(0, fixture.getAllPointsNum());
    ArrayList<IPseudoPoint> points = createPseudoPointList();
    fixture.addAllPseudoPoints(points);

    assertEquals(points.size(), fixture.getAllPointsNum());

    // Assert the added point can be retrieved using group and id.
    for (IPseudoPoint p : points) {
      assertTrue(fixture.contains(p));

      // Retrieve point
      VirtualPoint point = fixture.getPoint(p.getGroup(), p.getId());
      assertNotNull(point);
      assertTrue("Original point:" + p + " and retrieved Point:" + point + " are not equal", point == p);
    }
  }

  @Test
  public void testRemoveAllPseudo() throws DuplicatedException {
    ArrayList<IPseudoPoint> points = createPseudoPointList();

    // Add points to manager
    fixture.addAllPseudoPoints(points);
    assertEquals(points.size(), fixture.getAllPointsNum());

    fixture.removeAllPseudoPoints(points);
    assertEquals(0, fixture.getAllPointsNum());
  }

  @Test
  public void testPointAmount() throws DuplicatedException {
    final int analogAmount = 32;
    final int binaryAmount = 50;
    final int dbinaryAmount = 63;

    for (int i = 0; i < analogAmount; i++) {
      fixture.addPoint(VirtualPointFactory.createAnalogPoint());
    }

    for (int i = 0; i < binaryAmount; i++) {
      fixture.addPoint(VirtualPointFactory.createBinaryPoint());
    }

    for (int i = 0; i < dbinaryAmount; i++) {
      fixture.addPoint(VirtualPointFactory.createDoubleBinaryPoint());
    }

    assertEquals(analogAmount, fixture.getPointsNum(VirtualPointType.ANALOGUE_INPUT));
    assertEquals(binaryAmount, fixture.getPointsNum(VirtualPointType.BINARY_INPUT));
    assertEquals(dbinaryAmount, fixture.getPointsNum(VirtualPointType.DOUBLE_BINARY_INPUT));
    assertEquals(analogAmount + dbinaryAmount + binaryAmount, fixture.getAllPointsNum());
  }

  @Test
  public void testValidate() {
    fixture.getValidator().validate();
    assertTrue(fixture.getValidator().isValid());

    // Add an invalid point
    IStandardPoint p0 = VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT);
    fixture.addPoint(p0);

    // Validate again
    fixture.getValidator().validate();
    assertFalse(fixture.getValidator().isValid());
  }

  // Prepare a list of points with consistent ID
  private ArrayList<IStandardPoint> createStdPointList() {
    ArrayList<IStandardPoint> points = new ArrayList<IStandardPoint>();
    VirtualPointType[] types = VirtualPointType.values();
    int id = 0;
    IStandardPoint point = null;
    for (VirtualPointType type : types) {
      for (int i = 0; i < 10; i++) {
        point = VirtualPointFactory.createStanardPoint(type);
        points.add(point);
        if (point != null) {
          point.setId(id++);
        }
      }
    }

    return points;
  }

  private ArrayList<IPseudoPoint> createPseudoPointList() {
    ArrayList<IPseudoPoint> points = new ArrayList<IPseudoPoint>();
    VirtualPointType[] types = VirtualPointType.values();
    int pseudoid = 0;
    for (VirtualPointType type : types) {
      for (int i = 0; i < 10; i++) {
        IPseudoPoint point = null;
        point = PseudoPointFactory.createPseudoPoint(type, pseudoid++, null);
        points.add(point);
      }
    }

    return points;
  }

}
