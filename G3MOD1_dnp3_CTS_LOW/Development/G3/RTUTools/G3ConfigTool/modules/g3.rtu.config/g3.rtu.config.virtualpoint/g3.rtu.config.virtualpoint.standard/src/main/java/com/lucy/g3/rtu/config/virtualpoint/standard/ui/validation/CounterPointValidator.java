/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;

/**
 * This class is for validating the buffer value in CounterPoint PresentationModel.
 */
public final class CounterPointValidator extends BufferValidator {

  private static final int RESETVALUE_MIN = 0;
  private static final int RESETVALUE_MAX = Integer.MAX_VALUE;
  private static final int ROLLOVERVAUE_MIN = 0;
  private static final int ROLLOVERVAUE_MAX = Integer.MAX_VALUE;


  public CounterPointValidator(PresentationModel<? extends CounterPoint> target) {
    super(target);
  }

  @Override
  public void validate(ValidationResult result) {
    CounterPoint bean = (CounterPoint) target.getBean();

    BufferValidator.validatePropertyBoundary(result, target,
        CounterPoint.PROPERTY_RESET_VALUE,
        RESETVALUE_MIN, RESETVALUE_MAX);

    BufferValidator.validatePropertyBoundary(result, target,
        CounterPoint.PROPERTY_ROLLOVER_VALUE,
        ROLLOVERVAUE_MIN, ROLLOVERVAUE_MAX);

    if (bean instanceof StdCounterPoint) {
      BufferValidator.validatePropertyEmpty(result, target,
          StdCounterPoint.PROPERTY_SOURCE);
    }

    // Validate the number of persistent points which are supported yet
    // boolean newPersistent = (Boolean)
    // target.getBufferedValue(CounterPoint.PROPERTY_PERSISTENT);

    /* Changing persistent from false to true */
    // if(bean.isPersistent() == false && newPersistent == true && manager !=
    // null){
    // int currentPersistNum = manager.getPersistentPointsNum();
    //
    // if(currentPersistNum >= CounterPoint.MAX_PERSISTENT_POINTS){
    // result.addError("Cannot set persistent!\nThe number of persistent points has reached maxmimum:"
    // +CounterPoint.MAX_PERSISTENT_POINTS);
    // }
    // }

  }

}
