/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.InputMode;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

/**
 * Bean for storing input signal settings.
 */
public class InputSignal extends com.jgoodies.binding.beans.Model {

  /**
   * The name of property {@value} . <li>Value type: {@link InputMode}.</li>
   */
  public static final String PROPERTY_INPUT_MODE = "inputMode";

  /**
   * The name of property {@value} . <li>Value type: {@link EDGE_DRIVEN_MODE}.</li>
   */
  public static final String PROPERTY_EDGE_DRIVEN_MODE = "edgeDrivenMode";

  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li>
   */
  public static final String PROPERTY_PULSE_WIDTH = "pulseWidth";

  private InputMode inputMode = InputMode.EDGE_DRIVEN;
  private EDGE_DRIVEN_MODE edgeDrivenMode = EDGE_DRIVEN_MODE.EDGE_DRIVEN_MODE_RISE;
  private long pulseWidth = 50;


  public InputMode getInputMode() {
    return inputMode;
  }

  public void setInputMode(InputMode inputMode) {
    Object oldValue = this.inputMode;
    this.inputMode = inputMode;
    firePropertyChange("inputMode", oldValue, inputMode);
  }

  public EDGE_DRIVEN_MODE getEdgeDrivenMode() {
    return edgeDrivenMode;
  }

  public void setEdgeDrivenMode(EDGE_DRIVEN_MODE edgeDrivenMode) {
    Object oldValue = this.edgeDrivenMode;
    this.edgeDrivenMode = edgeDrivenMode;
    firePropertyChange("edgeDrivenMode", oldValue, edgeDrivenMode);
  }

  public long getPulseWidth() {
    return pulseWidth;
  }

  public void setPulseWidth(long pulseWidth) {
    Object oldValue = this.pulseWidth;
    this.pulseWidth = pulseWidth;
    firePropertyChange("pulseWidth", oldValue, pulseWidth);
  }

}
