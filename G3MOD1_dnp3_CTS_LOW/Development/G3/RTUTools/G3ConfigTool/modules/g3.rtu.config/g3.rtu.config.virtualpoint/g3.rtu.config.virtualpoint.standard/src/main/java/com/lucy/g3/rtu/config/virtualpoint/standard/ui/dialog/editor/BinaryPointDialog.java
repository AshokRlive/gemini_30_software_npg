/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetRef;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation.BinaryPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * Dialog for editing {@link StdBinaryPoint} and {@link PseudoBinaryPoint}.
 */
class BinaryPointDialog extends VirtualPointDialog<BinaryPoint> {

  public BinaryPointDialog(Frame parent, IVirtualPointDialogResource resource,
      BinaryPoint point) {
    super(parent, resource, point);
  }

  public BinaryPointDialog(Frame parent, IVirtualPointDialogResource resource,
      IEditorDialogInvoker<BinaryPoint> invoker) {
    super(parent, VirtualPointType.BINARY_INPUT, resource, invoker);
  }

  @Override
  void init() {
    initComponents();
    initEventHandling();
  }

  @Override
  void initForStdPoint() {
    // Nothing to do
  }

  @Override
  void initForPseudoPoint() {
    // Nothing to do
  }

  private void initEventHandling() {
    UIUtils.setAllTextFieldsCaretPositionOnClick(contentPanel);
    
    /* Update the value label.*/ 
    ValueLabelSet bufLabelSet = (ValueLabelSet) getLabelModel().getBufferedValue(ValueLabelSetRef.PROPERTY_LABEL_SET);
    panelDelayEvent.setLabelSet(bufLabelSet);
    getLabelModel().getBufferedModel(ValueLabelSetRef.PROPERTY_LABEL_SET).addValueChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        ValueLabelSet bufLabelSet = (ValueLabelSet) evt.getNewValue();
        panelDelayEvent.setLabelSet(bufLabelSet);
      }
    });
  }

  private void createUIComponents() {
    panelTop = new VirtualPointSourcePanel(getModel(),
        new ArrayList<IVirtualPointSource>(getAllChannels(ChannelType.DIGITAL_INPUT)));

    ValueModel vm;

    /* Invert checkBox */
    vm = getModel().getBufferedModel(BinaryPoint.PROPERTY_INVERT);
    checkBoxInvert = BasicComponentFactory.createCheckBox(vm, "");

    panelChatter = new ChatterSettingPanel(getModel());

    // Label Set
    panelLabelSet = createLabelSetPanel();
    
    panelDelayEvent = new DelayedEventReportPanel(
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY_ENABLED),
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY0),
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY1));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    JXTitledSeparator sepParam2 = new JXTitledSeparator();
    JXTitledSeparator sepParam = new JXTitledSeparator();

    //======== contentPanel ========
    {
      contentPanel.setBorder(new EmptyBorder(0, 0, 0, 10));
      contentPanel.setLayout(new FormLayout(
          "[50dlu,default]:grow",
          "fill:default, $ugap, default, $lgap, default, $ugap, default, $lgap, 2*(fill:default, $ugap), default"));
      contentPanel.add(panelTop, CC.xy(1, 1));

      //---- sepParam2 ----
      sepParam2.setTitle("Filter");
      contentPanel.add(sepParam2, CC.xy(1, 3));

      //---- checkBoxInvert ----
      checkBoxInvert.setText("Invert");
      contentPanel.add(checkBoxInvert, CC.xy(1, 5));

      //---- sepParam ----
      sepParam.setTitle("Parameters");
      contentPanel.add(sepParam, CC.xy(1, 7));
      contentPanel.add(panelChatter, CC.xy(1, 9));
      contentPanel.add(panelDelayEvent, CC.xy(1, 11));
      contentPanel.add(panelLabelSet, CC.xy(1, 13));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private VirtualPointSourcePanel panelTop;
  private JCheckBox checkBoxInvert;
  private ChatterSettingPanel panelChatter;
  private DelayedEventReportPanel panelDelayEvent;
  private LabelSetPanel panelLabelSet;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected JPanel createMainPanel() {
    return contentPanel;
  }

  @Override
  protected BufferValidator createValidator(PresentationModel<BinaryPoint> target) {
    return new BinaryPointValidator(target);
  }

  @Override
  protected void editingPointChanged(BinaryPoint newPoint) {
    // Nothing to do
  }

}
