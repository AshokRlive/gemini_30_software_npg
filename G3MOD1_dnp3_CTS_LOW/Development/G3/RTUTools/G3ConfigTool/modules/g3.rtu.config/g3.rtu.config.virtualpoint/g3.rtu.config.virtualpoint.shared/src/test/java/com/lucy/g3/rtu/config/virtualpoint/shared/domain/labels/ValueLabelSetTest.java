/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.labels;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;

/**
 * The Class ValueLabelSetTest.
 */
public class ValueLabelSetTest {

  private ValueLabelSet fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new ValueLabelSet("Set0");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddLabel() {
    ValueLabel sample = new ValueLabel(1.0, "Degree");
    assertTrue(fixture.addLabel(sample));
    assertFalse(fixture.addLabel(sample));
    assertFalse(fixture.addLabel(new ValueLabel(1.0, "Degree")));

    assertEquals(1, fixture.getSize());
  }

  @Test
  public void testGetLabel() {
    ValueLabel sample = new ValueLabel(1.0, "Degree");
    fixture.addLabel(sample);
    assertTrue(sample == fixture.getLabelObj(1D));
    assertTrue(sample == fixture.getLabelObj(1.0D));
  }

}
