/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The Class for checking if a source is valid to be used by a virtual point.
 */
class SourceChecker {

  /**
   * Checks point source.
   *
   * @param point
   *          the point
   * @param pointObj
   *          the point obj
   */
  static void checkPoint(VirtualPoint point, Object pointObj) {
    if (pointObj == null) {
      return;
    }

    if (!(pointObj instanceof VirtualPoint)) {
      throw new NotConnectibleException("Invalid source type: " + pointObj);
    }

    if (((VirtualPoint) pointObj).getType() != VirtualPointType.BINARY_INPUT) {
      throw new NotConnectibleException("Invalid source type: " + pointObj);
    }

  }

  /**
   * Checks channel source.
   *
   * @param point
   *          the point
   * @param channelObj
   *          the channel obj
   */
  static void checkChannel(VirtualPoint point, Object channelObj) {
    if (channelObj == null) {
      return;
    }

    if (!(channelObj instanceof IChannel)) {
      throw new NotConnectibleException("Invalid source type: " + channelObj);
    }

    VirtualPointType pointType = point.getType();
    ChannelType channelType = ((IChannel) channelObj).getType();

    if ((pointType.isAnalogue() != channelType.isAnalogue())) {
      throw new IllegalArgumentException("Cannot set channel. Invalid channel type: " + channelType);
    }
  }

}
