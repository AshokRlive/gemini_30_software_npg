/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.validation;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ICleaner;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;

/**
 * Validator of {@linkplain VirtualPointManager}.
 */
public class VirtualPointManagerValidator extends AbstractContainerValidator implements ICleaner {

  private final VirtualPointManager manager;


  public VirtualPointManagerValidator(VirtualPointManager manager) {
    super(manager);
    this.manager = manager;
  }

  @Override
  public Collection<IValidation> getValidatableChildrenItems() {
    return new ArrayList<IValidation>(manager.getAllPoints());
  }

  @Override
  public String getTargetName() {
    return "Virtual Points Manager";
  }

  @Override
  public ICleaner getCleaner() {
    return this;
  }

  @Override
  public void clean(Collection<ValidationResultExt> results) {
    // TODO Auto-generated method stub
    
  }

  @Override
  protected void validate(ValidationResultExt result) {
  }

//  @Override
//  public void clean(IConfig config, Collection<ValidationResultExt> invalidItems) {
//    for (ValidationResultExt invaliditem: invalidItems) {
//      Object t = invaliditem.getOriginal();
//      if(t instanceof IStandardPoint)
//        manager.removePoint((IStandardPoint) t);
//    }
//  }
}
