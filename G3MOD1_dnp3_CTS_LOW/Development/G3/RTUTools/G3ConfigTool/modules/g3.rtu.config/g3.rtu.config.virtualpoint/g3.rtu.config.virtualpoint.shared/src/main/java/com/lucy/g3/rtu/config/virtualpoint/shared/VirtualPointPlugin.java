/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared;

import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.VirtualPointManagerImpl;

/**
 * The Class VirtualPoint Plugin.
 */
public class VirtualPointPlugin implements IConfigPlugin, IConfigModuleFactory{
  private VirtualPointPlugin () {}
  private final static VirtualPointPlugin  INSTANCE = new VirtualPointPlugin();
  
  public static void init() {
    ConfigFactory.getInstance().registerFactory(VirtualPointManager.CONFIG_MODULE_ID,INSTANCE);
  }
  
  public static void shutdown() {
    ConfigFactory.getInstance().deregisterFactory(VirtualPointManager.CONFIG_MODULE_ID);
  }
  
  @Override
  public VirtualPointManager create(IConfig owner) {
    return new VirtualPointManagerImpl(owner);
  }
}
