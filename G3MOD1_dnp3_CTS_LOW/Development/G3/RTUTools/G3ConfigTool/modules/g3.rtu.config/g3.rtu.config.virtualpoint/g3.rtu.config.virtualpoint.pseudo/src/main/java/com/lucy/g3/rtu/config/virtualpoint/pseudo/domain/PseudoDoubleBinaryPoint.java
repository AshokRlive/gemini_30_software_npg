/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain;

import com.g3schema.ns_clogic.CLogicDoubleBinaryPointT;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;

/**
 * Interface for pseudo double binary virtual point.
 */
public interface PseudoDoubleBinaryPoint extends IPseudoPoint, DoubleBinaryPoint {

  void writeToXML(CLogicDoubleBinaryPointT xml);

  void readFromXML(CLogicDoubleBinaryPointT xml, ValueLabelReadSupport support);
}
