/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.xml;

import com.g3schema.ns_common.ChannelRefT;
import com.g3schema.ns_common.OutputModuleRefT;
import com.g3schema.ns_common.VirtualPointRefT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * This interface specifies the reource required when reading virtual point from
 * XML.
 */
public interface VirtualPointReadSupport extends ValueLabelReadSupport {

  IChannel getChannelByRef(ChannelRefT xml, ChannelType type);

  VirtualPoint getVirtualPointByRef(VirtualPointRefT xml);
  
  VirtualPoint getVirtualPoint(int group, int id);

  Module getModuleByRef(OutputModuleRefT xml_ref);

  ValidationResult getResult();
}
