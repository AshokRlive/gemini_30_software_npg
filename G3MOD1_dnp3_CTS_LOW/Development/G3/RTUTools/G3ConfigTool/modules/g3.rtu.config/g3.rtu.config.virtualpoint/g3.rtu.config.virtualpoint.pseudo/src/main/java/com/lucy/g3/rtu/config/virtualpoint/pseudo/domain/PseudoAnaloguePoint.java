/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain;

import com.g3schema.ns_clogic.CLogicAnalogPointT;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;

/**
 * Interface for pseudo analogue virtual point.
 */
public interface PseudoAnaloguePoint extends IPseudoPoint, AnaloguePoint {

  void writeToXML(CLogicAnalogPointT xml);

  void readFromXML(CLogicAnalogPointT xml, VirtualPointReadSupport support);

}
