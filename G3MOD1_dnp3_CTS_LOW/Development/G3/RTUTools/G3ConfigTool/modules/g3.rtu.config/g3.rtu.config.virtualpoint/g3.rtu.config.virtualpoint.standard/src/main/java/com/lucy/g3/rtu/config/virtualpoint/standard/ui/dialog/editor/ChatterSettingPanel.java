/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DigitalPoint;

/**
 * The panel for configuring chatter number and chatter time.
 */
class ChatterSettingPanel extends JPanel {

  private final PresentationModel<? extends DigitalPoint> pm;


  public ChatterSettingPanel(PresentationModel<? extends DigitalPoint> pm) {
    this.pm = Preconditions.checkNotNull(pm, "pm is null");
    initComponents();
    initComponentAnnotations();
    initCommitOnType();
    initEventHandling();
  }

  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private ChatterSettingPanel() {
    pm = new PresentationModel<DigitalPoint>();
    initComponents();
    initComponentAnnotations();
  }

  private void initCommitOnType() {
    FormattedTextFieldSupport.installCommitOnType(ftfChatterNo);
    FormattedTextFieldSupport.installCommitOnType(ftfChatterTime);
  }

  private void createUIComponents() {
    ValueModel vm;

    /* Chatter No field */
    vm = pm.getBufferedModel(DigitalPoint.PROPERTY_CHATTERNO);
    ftfChatterNo = BasicComponentFactory.createIntegerField(vm);

    /* Chatter Time field */
    vm = pm.getBufferedModel(DigitalPoint.PROPERTY_CHATTERTIME);
    ftfChatterTime = BasicComponentFactory.createIntegerField(vm);
  }

  private void initEventHandling() {

    /* Set Chatter Time to be disabled when chatterNo is 0 */
    int chatterNo = (int) pm.getBufferedValue(DigitalPoint.PROPERTY_CHATTERNO);
    ftfChatterTime.setEnabled(chatterNo != 0);
    pm.getBufferedModel(DigitalPoint.PROPERTY_CHATTERNO).addValueChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getNewValue() == null) {
          return;
        }
        updateChatter((int) evt.getNewValue());
      }
    });
    updateChatter((int) pm.getBufferedModel(DigitalPoint.PROPERTY_CHATTERNO).getValue());
  }

  private void updateChatter(int chatterNo) {
    boolean chatterEnabled = chatterNo != 0;
    ftfChatterTime.setEnabled(chatterEnabled);
    ftfChatterTime.setVisible(chatterEnabled);
    lblChatterTime.setVisible(chatterEnabled);
    label2.setVisible(chatterEnabled);
  }

  private void initComponentAnnotations() {
    ValidationComponentUtils.setMessageKey(ftfChatterNo, DigitalPoint.PROPERTY_CHATTERNO);
    ValidationComponentUtils.setMessageKey(ftfChatterTime, DigitalPoint.PROPERTY_CHATTERTIME);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    createUIComponents();

    JLabel lblChatterNo = new JLabel();
    lblChatterTime = new JLabel();
    label2 = new JLabel();

    // ======== this ========
    setLayout(new FormLayout(
        "right:[50dlu,default], $lcgap, [50dlu,default], $lcgap, default",
        "fill:default, $lgap, default"));

    // ---- lblChatterNo ----
    lblChatterNo.setText("Chatter No:");
    add(lblChatterNo, CC.xy(1, 1));

    // ---- ftfChatterNo ----
    ftfChatterNo.setColumns(10);
    add(ftfChatterNo, CC.xy(3, 1));

    // ---- lblChatterTime ----
    lblChatterTime.setText("Chatter Time:");
    add(lblChatterTime, CC.xy(1, 3));

    // ---- ftfChatterTime ----
    ftfChatterTime.setColumns(10);
    add(ftfChatterTime, CC.xy(3, 3));

    // ---- label2 ----
    label2.setText("seconds");
    add(label2, CC.xy(5, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JFormattedTextField ftfChatterNo;
  private JLabel lblChatterTime;
  private JFormattedTextField ftfChatterTime;
  private JLabel label2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
