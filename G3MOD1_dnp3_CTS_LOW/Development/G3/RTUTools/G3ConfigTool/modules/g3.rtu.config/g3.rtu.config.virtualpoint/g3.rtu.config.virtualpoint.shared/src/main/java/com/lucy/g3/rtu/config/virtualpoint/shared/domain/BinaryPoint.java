/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

/**
 * Interface of Binary Virtual Point.
 */
public interface BinaryPoint extends DigitalPoint {

  /** The name of bound property {@value} . */
  String PROPERTY_INVERT = "invert";


  boolean isInvert();

  void setInvert(boolean invert);

}
