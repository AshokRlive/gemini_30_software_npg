/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;


public class BinaryPointValidator extends DigitalPointValidator {

  public BinaryPointValidator(PresentationModel<BinaryPoint> target) {
    super(target);
  }

}

