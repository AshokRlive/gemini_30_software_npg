/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.labels;

import java.awt.Frame;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.labels.LabelSetManagerDialog;

/**
 * The Class LabelSetDialogTest.
 */
public class LabelSetDialogTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  public static void main(String[] args) {
    ValueLabelSet target = new ValueLabelSet("Test");
    target.addLabel(1.0D, "1.0");
    target.addLabel(1.1D, "1.1");
    target.addLabel(1.123D, "1.23");
    target.addLabel(1.123400D, "1.23400");

    ValueLabelSetManager manager = new ValueLabelSetManager();
    manager.add(target);

    new LabelSetManagerDialog((Frame) null, manager).setVisible(true);
    ;

  }

}
