/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.value.AbstractValueModel;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel.PercentEditorPanel;
import com.lucy.g3.test.support.utilities.TestUtil;

public class HighHighLowLowPanelTest {

  public static void main(String[] args) {
    Logger.getRootLogger().setLevel(Level.DEBUG);

    HighHighLowLow bean = new HighHighLowLow();
    LoggingUtil.logPropertyChanges(bean);

    bean.setHihiHysteresis(123);
    PresentationModel<HighHighLowLow> pm = new PresentationModel<HighHighLowLow>(bean);

    AbstractValueModel vm = pm.getModel(HighHighLowLow.PROPERTY_HI_HYSTERESIS);

    PercentEditorPanel p = new PercentEditorPanel(HighHighLowLow.PROPERTY_HI_HYSTERESIS, 1000, vm);

    TestUtil.showFrame(p);
  }

}
