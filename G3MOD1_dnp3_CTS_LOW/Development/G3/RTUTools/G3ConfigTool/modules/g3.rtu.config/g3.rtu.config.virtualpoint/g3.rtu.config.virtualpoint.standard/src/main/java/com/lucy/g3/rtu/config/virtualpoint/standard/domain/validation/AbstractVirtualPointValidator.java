/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.validation;

import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The Class AbstractVirtualPointValidator.
 */
abstract class AbstractVirtualPointValidator<VointPointT extends VirtualPoint> extends AbstractValidator<VointPointT> {

  public AbstractVirtualPointValidator(VointPointT target) {
    super(target);
  }

  @Override
  public final String getTargetName() {
    return "Virtual Point" + target.getGroupID();
  }

}
