/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain;

import com.g3schema.ns_vpoint.DoubleBinaryPointT;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * Interface of standard double binary virtual point.
 *
 * @see {@link IStandardPoint}
 * @see {@link DoubleBinaryPoint}
 */
public interface StdDoubleBinaryPoint extends IMapToChannel, IStandardPoint, DoubleBinaryPoint {

  String PROPERTY_FIRST_CHANNEL = PROPERTY_SOURCE;
  String PROPERTY_SECOND_CHANNEL = "secondChannel";

  String PROPERTY_DEBOUNCE = "debounce";


  IChannel getSecondChannel();

  void setSecondChannel(IChannel secondChannel);

  int getDebounce();

  void setDebounce(int debounce);

  DoubleBinaryValueMap getValueMap();

  void readFromXML(DoubleBinaryPointT xml, VirtualPointReadSupport support);

  void writeToXML(DoubleBinaryPointT xml, VirtualPointWriteSupport support);

}