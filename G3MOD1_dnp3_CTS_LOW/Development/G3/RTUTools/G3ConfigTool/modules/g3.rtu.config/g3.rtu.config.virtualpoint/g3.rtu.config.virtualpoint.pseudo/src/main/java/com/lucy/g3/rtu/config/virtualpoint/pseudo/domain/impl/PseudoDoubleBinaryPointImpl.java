/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import com.g3schema.ns_clogic.CLogicDoubleBinaryPointT;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain PseudoDoubleBinaryPoint}.
 */
public final class PseudoDoubleBinaryPointImpl extends AbstractDoubleBinaryPoint
    implements PseudoDoubleBinaryPoint {

  private ILogicPointEnum pointXmlEnum;

  private final IValidator validator;


  PseudoDoubleBinaryPointImpl(String description, IPseudoPointSource source, int id) {
    super(DEF_PSEUDO_POINT_GROUP, id, description);
    validator = new PseudoPointValidator(this);
    setSource(source);
    init();
  }

  PseudoDoubleBinaryPointImpl(IPseudoPointSource source, ILogicPointEnum pointXml) {
    this(pointXml.getDescription(), source, pointXml.getValue());
    this.pointXmlEnum = pointXml;
    setSource(source);
    init();
  }

  private void init() {
    updateGroup();
  }

  @Override
  public void updateGroup() {
    IPseudoPointSource src = (IPseudoPointSource) getSource();
    if (src != null) {
      setGroup(src.getGroup());
    }
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return pointXmlEnum == null ? DefaultCreateOption.DoNotCreatePoint
        : pointXmlEnum.getDefCreateOption();
  }

  @Override
  public void writeToXML(CLogicDoubleBinaryPointT xml) {
    super.writeToXML(xml);
  }

  @Override
  public void readFromXML(CLogicDoubleBinaryPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);
  }
  
  // ----------------------[ Unsupported operations ]--------------------------

  @Override
  public IValidator getValidator() {
    return validator;
  }

}
