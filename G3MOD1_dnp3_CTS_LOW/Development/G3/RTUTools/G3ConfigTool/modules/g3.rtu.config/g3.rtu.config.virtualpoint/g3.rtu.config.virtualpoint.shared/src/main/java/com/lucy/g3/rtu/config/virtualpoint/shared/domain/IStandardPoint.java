/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * <p>
 * Interface of standard virtual point that are managed by users. It provides
 * the interface to change description and mapping source.
 * </p>
 * <p>
 * That group of standard point is supposed to be {@value #STANDARD_POINT_GROUP}
 * always.
 * </p>
 */
public interface IStandardPoint extends VirtualPoint {

  /**
   * The group value of standard point.
   */
  int STANDARD_POINT_GROUP = 0;


  void setId(int id);

  void setDescription(String description);

  void setCustomDescriptionEnabled(boolean customDescriptionEnabled);

  void setSource(IVirtualPointSource source);

  /**
   * Create an new standard point instance by cloning this point.
   *
   * @return new instance with the same content as this point.
   */
  IStandardPoint duplicate();
}
