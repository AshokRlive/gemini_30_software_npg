/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;

/**
 * The validator for pseudo point.
 */
public class PseudoPointValidator extends AbstractValidator {

  private final IPseudoPoint point;


  public PseudoPointValidator(IPseudoPoint point) {
    super(point);
    this.point = point;
  }

  @Override
  protected void validate(ValidationResultExt result) {
  }

  @Override
  public String getTargetName() {
    return point.getName();
  }

}
