/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import javax.swing.text.NumberFormatter;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.common.utils.CollectionUtils;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.DoubleNumberFormatter;

/**
 * The editor component of scaling factor.
 */
class ScalingFactorEditor {

  private static final String[] DEFAULT_UNITS = { "°C", "A", "mA", "V", "mV", "%", "Minutes", "Seconds" };
  private static final Double[] DEFAULT_SCALING_FACTORS = { 0.01D, 0.1D, 1.0D, 10D, 100D, 1000D };

  private final ArrayListModel<Double> factorItems = new ArrayListModel<Double>(10);
  private final ArrayListModel<String> unitItems = new ArrayListModel<String>(10);
  private Map<Double, String> factorUnitMap = new HashMap<Double, String>();

  private final ValueModel factorVM;
  private final ValueModel unitVM;

  private JComboBox<Double> factorEditorBox;
  private JComboBox<String> unitEditorBox;


  /**
   * Instantiates a new scaling factor editor.
   *
   * @param factorVM
   *          the factor vm
   * @param unitVM
   *          the unit vm
   */
  public ScalingFactorEditor(ValueModel factorVM, ValueModel unitVM) {
    this.factorVM = Preconditions.checkNotNull(factorVM, "factorVM is null");
    this.unitVM = Preconditions.checkNotNull(unitVM, "unitVM is null");

    unitItems.addAll(Arrays.asList(DEFAULT_UNITS));
    factorItems.addAll(Arrays.asList(DEFAULT_SCALING_FACTORS));

    @SuppressWarnings("unchecked")
    ComboBoxModel<String> m0 = new ComboBoxAdapter<String>((ListModel<String>) unitItems, unitVM);
    unitEditorBox = new JComboBox<String>(m0);
    unitEditorBox.setEditable(true);
    unitEditorBox.addItemListener(new UnitChangeListener());

    @SuppressWarnings("unchecked")
    ComboBoxModel<Double> m1 = new ComboBoxAdapter<Double>((ListModel<Double>) factorItems, factorVM);
    factorEditorBox = new JComboBox<Double>(m1);
    factorEditorBox.setEditable(true);
    factorEditorBox.setEditor(new ScalingFactorComboBoxEditor());
    ScalingFactorListener handler = new ScalingFactorListener();
    factorEditorBox.addItemListener(handler);
    factorEditorBox.getEditor().getEditorComponent().addFocusListener(handler);

  }

  public JComboBox<Double> getScalingFactorEditorComponent() {
    return factorEditorBox;
  }

  public JComboBox<String> getUnitEditorComponent() {
    return unitEditorBox;
  }

  public void setScalingFactorItems(Map<Double, String> factorUnitMap,
      boolean updateUnit) {
    unitItems.clear();
    factorItems.clear();

    this.factorUnitMap = factorUnitMap == null ? new HashMap<Double, String>() : factorUnitMap;

    if (factorUnitMap != null) {
      unitItems.addAll(factorUnitMap.values());
      factorItems.addAll(factorUnitMap.keySet());
    }

    if (factorItems.isEmpty()) {
      factorItems.addAll(Arrays.asList(DEFAULT_SCALING_FACTORS));
    }

    if (unitItems.isEmpty()) {
      unitItems.addAll(Arrays.asList(DEFAULT_UNITS));
    }

    // Update unit against current factor
    if (updateUnit) {
      updateUnit();
    }
  }

  private void updateUnit() {
    Double currentFactor = (Double) factorVM.getValue();
    String currentUnit = (String) unitVM.getValue();
    String predefinedUnit = factorUnitMap.get(currentFactor);
    String newUnit;

    if (factorUnitMap.containsKey(currentFactor)) {
      newUnit = predefinedUnit;
    } else {
      // Use custom key
      if (factorUnitMap.containsValue(currentUnit)) {
        newUnit = null;
      } else {
        newUnit = currentUnit;
      }
    }

    if (!EqualsUtil.areEqual(currentUnit, newUnit)) {
      unitVM.setValue(newUnit);
    }
  }

  private void setFactor(Double newfactor) {
    if (!EqualsUtil.areEqual(factorVM.getValue(), newfactor)) {
      factorVM.setValue(newfactor);
    }
  }


  private class ScalingFactorListener implements ItemListener, FocusListener {

    @Override
    public void itemStateChanged(ItemEvent e) {
      updateUnit();
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    @Override
    public void focusLost(FocusEvent e) {
      updateUnit();
    }
  }

  private class UnitChangeListener implements ItemListener {

    @Override
    public void itemStateChanged(ItemEvent e) {
      String unit = (String) unitVM.getValue();

      if (!factorUnitMap.containsValue(unit)) {
        // Update unit if the unit is not predefined
        updateUnit();
      } else {
        // Update scaling factor if the unit is predefined
        Double factor = (Double) CollectionUtils.getKeyFromValue(factorUnitMap, unit);
        if (factor != null) {
          setFactor(factor);
        }
      }

    }

  }

  private class ScalingFactorComboBoxEditor extends BasicComboBoxEditor {

    @Override
    protected JTextField createEditorComponent() {
      NumberFormatter formatter = DoubleNumberFormatter.create();
      formatter.setCommitsOnValidEdit(true);
      final JFormattedTextField ftf = new JFormattedTextField(formatter);
      ftf.setBorder(BorderFactory.createEmptyBorder());
      // Commit change when press Enter key
      ftf.addKeyListener(new KeyAdapter() {

        @Override
        public void keyPressed(KeyEvent e) {
          if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            factorEditorBox.actionPerformed(new ActionEvent(factorEditorBox, 0, ""));
          }
        }

      });
      return ftf;
    }

    @Override
    public Object getItem() {
      JFormattedTextField ftf = (JFormattedTextField) editor;
      return ftf.getValue();
    }

  }

}
