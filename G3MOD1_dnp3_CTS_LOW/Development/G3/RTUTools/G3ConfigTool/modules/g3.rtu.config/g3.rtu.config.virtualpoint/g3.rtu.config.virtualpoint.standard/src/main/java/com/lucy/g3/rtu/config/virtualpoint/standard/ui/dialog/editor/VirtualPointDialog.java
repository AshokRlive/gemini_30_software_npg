/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialog;
import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetRef;

/**
 * The Class VirtualPointDialog.
 *
 * @param <PointT>
 *          the generic type
 */
public abstract class VirtualPointDialog<PointT extends VirtualPoint> extends AbstractEditorDialog<PointT> {

  private final IVirtualPointDialogResource resource;
  
  private final PresentationModel<ValueLabelSetRef> pmLabel;



  VirtualPointDialog(Frame parent, IVirtualPointDialogResource resource, PointT point) {
    super(parent, point);
    setTitle(point.getType().getDescription());
    this.resource = resource;
    
    PointT p = getEditItem();
    pmLabel = createPresentationModel(p == null ? null : p.getLabelSetRef());
  }

  VirtualPointDialog(Frame parent, VirtualPointType type, IVirtualPointDialogResource resource,
      IEditorDialogInvoker<PointT> invoker) {
    super(parent, invoker);
    setTitle(type.getDescription());
    this.resource = resource;
    
    PointT p = getEditItem();
    pmLabel = createPresentationModel(p == null ? null : p.getLabelSetRef());
  }
  
  protected PresentationModel<ValueLabelSetRef> getLabelModel() {
    return pmLabel;
  }
  
  protected LabelSetPanel createLabelSetPanel(){
    ValueLabelSetManager manager = resource == null ? null : resource.getLabelManager();
    return new LabelSetPanel(manager, pmLabel.getBufferedModel(ValueLabelSetRef.PROPERTY_LABEL_SET));
  }
  
  @Override
  protected final void editingItemChanged(PointT newItem) {
    pmLabel.setBean(newItem == null ? null : newItem.getLabelSetRef());
    editingPointChanged(newItem);
  }
  
  protected abstract void editingPointChanged(PointT newPoint);


  @Override
  public void pack() {
    super.pack();
    setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
  }

  abstract void init();

  abstract void initForPseudoPoint();

  abstract void initForStdPoint();

  abstract JPanel createMainPanel();

  @Override
  protected String getBannerTitle() {
    return null;
  }

  @Override
  protected final JComponent createContentPanel() {
    init();

    PointT point = getEditItem();
    if (point != null) {
      setName(point.getClass().getSimpleName() + ".dialog");

      if (IPseudoPoint.class.isAssignableFrom(point.getClass())) {
        initForPseudoPoint();
      } else if (IStandardPoint.class.isAssignableFrom(point.getClass())) {
        initForStdPoint();
      } else {
        throw new UnsupportedOperationException("Unsupported class: " + point.getClass());
      }
    }
    
    return createMainPanel();
  }

  @Override
  protected final String getBannerSubtitle() {
    return null;
  }

  @Override
  protected final ImageIcon getBannerIcon() {
    return null;
  }

  protected final Collection<IChannel> getAllChannels(ChannelType... type) {
    return resource == null ? new ArrayList<IChannel>(0) : resource.getAllChannels(type);
  }

  protected final Collection<VirtualPoint> getAllVirtualPoints(VirtualPointType... type) {
    return resource == null ? new ArrayList<VirtualPoint>(0) : resource.getAllVirtualPoints(type);
  }

  public static boolean edit(Frame parent, IVirtualPointDialogResource resource, VirtualPoint point) {
    Preconditions.checkNotNull(point, "point must not be null");
    VirtualPointDialog<?> dialog;

    if (point instanceof AnaloguePoint) {
      dialog = new AnaloguePointDialog(parent, resource, ((AnaloguePoint) point));
    } else if (point instanceof BinaryPoint) {
      dialog = new BinaryPointDialog(parent, resource, ((BinaryPoint) point));
    } else if (point instanceof DoubleBinaryPoint) {
      dialog = new DoubleBinaryPointDialog(parent, resource, ((DoubleBinaryPoint) point));
    } else if (point instanceof CounterPoint) {
      dialog = new CounterPointDialog(parent, resource, ((CounterPoint) point));
    } else {
      throw new IllegalArgumentException("No editor found for point: " + point);
    }
    dialog.showDialog();
    return dialog.hasBeenAffirmed();

  }

  public static boolean editAnalog(Frame parent,
      IVirtualPointDialogResource resource,
      IEditorDialogInvoker<AnaloguePoint> invoker) {
    AnaloguePointDialog dialog = new AnaloguePointDialog(parent, resource, invoker);
    showDialog(dialog);
    return dialog.hasBeenAffirmed();
  }

  public static boolean editSingleBinary(Frame parent,
      IVirtualPointDialogResource resource,
      IEditorDialogInvoker<BinaryPoint> invoker) {
    BinaryPointDialog dialog = new BinaryPointDialog(parent, resource, invoker);
    showDialog(dialog);
    return dialog.hasBeenAffirmed();
  }

  public static boolean editDoubleBinary(Frame parent,
      IVirtualPointDialogResource resource,
      IEditorDialogInvoker<DoubleBinaryPoint> invoker) {
    DoubleBinaryPointDialog dialog = new DoubleBinaryPointDialog(parent, resource, invoker);
    showDialog(dialog);
    return dialog.hasBeenAffirmed();
  }

  public static boolean editCounter(Frame parent,
      IVirtualPointDialogResource resource,
      IEditorDialogInvoker<CounterPoint> invoker) {
    CounterPointDialog dialog = new CounterPointDialog(parent, resource, invoker);
    showDialog(dialog);
    return dialog.hasBeenAffirmed();
  }
}
