/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigUtility;

/**
 *
 */
public class VirtualPointUtility implements IConfigUtility {

  private VirtualPointUtility() {
  }

  public static ListModel<VirtualPoint> getBinaryInputsListModel(IConfig config) {
    ListModel<VirtualPoint> binaryInputs = null;
    if (config != null) {
      VirtualPointRepository repo = getRepository(config);
      if (repo != null)
        binaryInputs = repo.getPointsModel(VirtualPointType.BINARY_INPUT);
    }

    if (binaryInputs == null) {
      Logger.getLogger(VirtualPointUtility.class).error("binaryInputs list mode not found!");
      
      binaryInputs = new DefaultListModel<VirtualPoint>();
    }

    return binaryInputs;
  }

  private static VirtualPointRepository getRepository(IConfig config) {
    return config == null ? null
        : (VirtualPointRepository) config.getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);
  }
  
  private static VirtualPointManager getManager(IConfig config) {
    return config == null ? null
        : (VirtualPointManager) config.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
  }

  public static Collection<VirtualPoint> getAllPoints(IConfig config) {
    VirtualPointRepository repo = getRepository(config);
    if (repo == null) {
      Logger.getLogger(VirtualPointUtility.class).error("VirtualPoint repository not found!");
      return new ArrayList<VirtualPoint>(0);
    } else {
      return repo.getAllPoints();
    }
  }

  public static void removePoint(IConfig config, IStandardPoint point) {
    getManager(config).removePoint((IStandardPoint) point);
  }

}
