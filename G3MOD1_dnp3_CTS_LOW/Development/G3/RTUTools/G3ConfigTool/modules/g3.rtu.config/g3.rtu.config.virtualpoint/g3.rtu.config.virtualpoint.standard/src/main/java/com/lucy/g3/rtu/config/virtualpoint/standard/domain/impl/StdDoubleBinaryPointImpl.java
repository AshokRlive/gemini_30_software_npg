/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigInteger;

import com.g3schema.ns_vpoint.DoubleBinaryPointT;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent.ConnectEventType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.validation.StandardPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain StdDoubleBinaryPoint}.
 */
public final class StdDoubleBinaryPointImpl extends AbstractDoubleBinaryPoint implements StdDoubleBinaryPoint {

  private final IValidator validator;

  private int debounce = 50; //ms

  private final DoubleBinaryValueMap valueMap = new DoubleBinaryValueMap();

  private IChannel secondChannel;
  private final PropertyChangeListener secondChannelPCL = new SecondChannelRemovePCL();


  /**
   * Construct a double binary point by giving initial property values.
   */
  StdDoubleBinaryPointImpl(int id, String desp) {
    super(STANDARD_POINT_GROUP, id, desp);
    validator = new StandardPointValidator(this);
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }

  @Override
  public int getDebounce() {
    return debounce;
  }

  @Override
  public void setDebounce(int debounce) {
    Object oldValue = getDebounce();
    this.debounce = debounce;
    firePropertyChange(PROPERTY_DEBOUNCE, oldValue, debounce);
  }

  @Override
  public StdDoubleBinaryPointImpl duplicate() {
    StdDoubleBinaryPointImpl copy = new StdDoubleBinaryPointImpl(0, getDescription());
    copy.setChatterNo(getChatterNo());
    copy.setChatterTime(getChatterTime());
    copy.setChannel(getChannel());    
    copy.setDelay0(getDelay0());
    copy.setDelay1(getDelay1());
    copy.setDelay2(getDelay2());
    copy.setDelay3(getDelay3());
    copy.setSecondChannel(getSecondChannel());
    return copy;
  }

  @Override
  public IChannel getChannel() {
    return (IChannel) getSource();
  }

  @Override
  public void setChannel(IChannel channel) {
    setSource(channel);
  }

  @Override
  public void setSource(IVirtualPointSource source)
      throws NotConnectibleException {
    SourceChecker.checkChannel(this, source);
    super.setSource(source);
  }

  private void _setSecondChannel(IChannel secondChannel) {
    IChannel oldValue = getSecondChannel();
    this.secondChannel = secondChannel;
    firePropertyChange(PROPERTY_SECOND_CHANNEL, oldValue, secondChannel);
  }

  @Override
  public void setSecondChannel(IChannel secondChannel) {
    SourceChecker.checkChannel(this, secondChannel);

    IChannel oldValue = getSecondChannel();
    if (oldValue != null) {
      oldValue.disconnectFromTarget(this);
      oldValue.removePropertyChangeListener(secondChannelPCL);
    }

    _setSecondChannel(secondChannel);

    if (this.secondChannel != null) {
      secondChannel.addPropertyChangeListener(IChannel.PROPERTY_DELETED, secondChannelPCL);
      secondChannel.connectToTarget(this);
    }
  }

  @Override
  public IChannel getSecondChannel() {
    return secondChannel;
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {

    INode source = event.getConnection().getSource();

    // Handle event for the second channel.
    if (IChannel.class.isInstance(source)
        && event.getConnection().getSource() == secondChannel) {

      ConnectEventType etype = event.getEventType();
      switch (etype) {
      case DISCONNECT_FROM_SOURCE:
        _setSecondChannel(null);
        break;

      default:
        break;
      }

    } else {
      super.handleConnectEvent(event);
    }
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    if (getChannel() == null || getSecondChannel() == null) {
      return null;
    }

    DefaultCreateOption op0 = getChannel().predefined().getDefaultCreateOption();
    DefaultCreateOption op1 = getSecondChannel().predefined().getDefaultCreateOption();

    return op0.ordinal() < op1.ordinal() ? op0 : op1;

  }

  @Override
  public DoubleBinaryValueMap getValueMap() {
    return valueMap;
  }

  @Override
  public void readFromXML(DoubleBinaryPointT xml, VirtualPointReadSupport support) {
    super.readFromXML(xml, support);

    // Value map
    if (xml.valueMap.exists()) {
      getValueMap().readXML(xml.valueMap.first());
    } else {
      getValueMap().setEnabled(false);
    }

    // Debounce
    setDebounce(xml.debounce.first().default2.first().debounce.getValue().intValue());// TODO TBD, support custom

    // Channel0
    IChannel ch0 = support.getChannelByRef(xml.channel0.first(), ChannelType.DIGITAL_INPUT);
    setChannel(ch0);

    // Channel1
    IChannel ch1 = support.getChannelByRef(xml.channel1.first(), ChannelType.DIGITAL_INPUT);
    setSecondChannel(ch1);

  }

  @Override
  public void writeToXML(DoubleBinaryPointT xml, VirtualPointWriteSupport support) {
    super.writeToXML(xml);

    // Value map
    if (getValueMap().isEnabled()) {
      getValueMap().writeXML(xml.valueMap.append());
    }

    // Debounce
    xml.debounce.append().default2.append().debounce.setValue(BigInteger.valueOf(getDebounce()));

    // Channel0
    support.writeChannelRef(getName(), xml.channel0.append(), getChannel());

    // Channel1
    support.writeChannelRef(getName(), xml.channel1.append(), getSecondChannel());
  }


  private class SecondChannelRemovePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (IChannel.PROPERTY_DELETED.equals(evt.getPropertyName())) {
        if (evt.getSource() == getSecondChannel()) {
          setSecondChannel(null);
        }
      }
    }

  }
}