/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.stub;

import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointSourceStub;

public class PseudoPointSourceStub extends VirtualPointSourceStub implements
    IPseudoPointSource {

  public PseudoPointSourceStub() {
    super(NodeType.CLOGIC);
  }

  @Override
  public int getGroup() {
    return 0;
  }

  public void updateName(String name) {
    super.setName(name);
  }

}
