/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.JCollapsiblePane;
import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.InputSignal;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.FreezeClockTime;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.FreezeMode;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.FreezeTimeMode;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint.InputMode;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation.CounterPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

/**
 * Dialog for editing {@link StdCounterPoint} and {@link PseudoCounterPoint}.
 */
final class CounterPointDialog extends VirtualPointDialog<CounterPoint> implements ActionListener {

  private PresentationModel<InputSignal> inputSigPM;


  public CounterPointDialog(Frame parent, IVirtualPointDialogResource resource,
      CounterPoint point) {
    super(parent, resource, point);
  }

  public CounterPointDialog(Frame parent, IVirtualPointDialogResource resource,
      IEditorDialogInvoker<CounterPoint> invoker) {
    super(parent, VirtualPointType.COUNTER, resource, invoker);
  }

  @Override
  void init() {
    inputSigPM = createPresentationModel(getEditItem().getInputSignal());
    initComponents();
    initEventHandling();
    initComponentsBinding();
    updateCollapsePane();
    updateEnablement();
  }

  private void updateEnablement() {
    getModel().getBufferedComponentModel(CounterPoint.PROPERTY_FREEZE_INTERVAL).setEnabled(radioInterval.isSelected());
    inputSigPM.getBufferedComponentModel(InputSignal.PROPERTY_PULSE_WIDTH).setEnabled(radioPulse.isSelected());

    combOclock.setEnabled(radioFromOclock.isSelected());
    combEdgeMode.setEnabled(radioEdge.isSelected());
  }

  private void initEventHandling() {
    radioFromOclock.addActionListener(this);
    radioInterval.addActionListener(this);
    radioEdge.addActionListener(this);
    radioPulse.addActionListener(this);
  }

  @Override
  void initForStdPoint() {
  }

  @Override
  void initForPseudoPoint() {
  }

  private void createUIComponents() {
    freezePane = new JCollapsiblePane();
    ArrayList<IVirtualPointSource> sources = new ArrayList<>();
    sources.addAll(getAllVirtualPoints(VirtualPointType.binaryTypes()));
    virtualPointSourcePanel1 = new VirtualPointSourcePanel(getModel(), sources);

    // Label Set
    panelLabelSet = createLabelSetPanel();
  }

  private void initComponentsBinding() {
    PresentationModel<CounterPoint> pm = getModel();

    // Bind input mode
    Bindings.bind(radioEdge, inputSigPM.getBufferedModel(InputSignal.PROPERTY_INPUT_MODE), InputMode.EDGE_DRIVEN);
    Bindings.bind(radioPulse, inputSigPM.getBufferedModel(InputSignal.PROPERTY_INPUT_MODE), InputMode.PULSE_DRIVEN);

    // Bind edge mode
    ValueModel vm = inputSigPM.getBufferedModel(InputSignal.PROPERTY_EDGE_DRIVEN_MODE);
    Bindings.bind(combEdgeMode, new ComboBoxAdapter<EDGE_DRIVEN_MODE>(EDGE_DRIVEN_MODE.values(), vm));

    // Bind pulse width
    Bindings.bind(ftfPulseWidth, inputSigPM.getBufferedComponentModel(InputSignal.PROPERTY_PULSE_WIDTH));

    // Bind parameters
    Bindings.bind(ftfResetValue, pm.getBufferedModel(CounterPoint.PROPERTY_RESET_VALUE));
    Bindings.bind(ftfRolloverValue, pm.getBufferedModel(CounterPoint.PROPERTY_ROLLOVER_VALUE));
    Bindings.bind(ftfEventStep, pm.getBufferedModel(CounterPoint.PROPERTY_EVENT_STEP));
    Bindings.bind(cboxPersistent, pm.getBufferedModel(CounterPoint.PROPERTY_PERSISTENT));
    Bindings.bind(cboxAutoReset, pm.getBufferedModel(CounterPoint.PROPERTY_AUTORESET));

    // Bind freezing
    Bindings.bind(cboxEnableFreeing, pm.getBufferedModel(CounterPoint.PROPERTY_FREEZING_ENABLED));
    Bindings.bind(radioInterval, pm.getBufferedModel(CounterPoint.PROPERTY_FREEZE_TIME_MODE), FreezeTimeMode.INTERVAL);
    Bindings.bind(radioFromOclock, pm.getBufferedModel(CounterPoint.PROPERTY_FREEZE_TIME_MODE),
        FreezeTimeMode.FROM_OCLOCK);
    Bindings.bind(radioFreezeOnly, pm.getBufferedModel(CounterPoint.PROPERTY_FREEZE_MODE), FreezeMode.FREEZE_ONLY);
    Bindings.bind(radioFreezeReset, pm.getBufferedModel(CounterPoint.PROPERTY_FREEZE_MODE),
        FreezeMode.FREEZE_THEN_RESET);

    vm = pm.getBufferedModel(CounterPoint.PROPERTY_FREEZE_CLOCK_TIME);
    Bindings.bind(combOclock, new ComboBoxAdapter<FreezeClockTime>(FreezeClockTime.values(), vm));

    Bindings.bind(ftfInterval, pm.getBufferedComponentModel(CounterPoint.PROPERTY_FREEZE_INTERVAL));

  }

  private void updateCollapsePane() {
    ((JCollapsiblePane) freezePane).setCollapsed(!cboxEnableFreeing.isSelected());
  }

  private void cboxEnableFreeingActionPerformed(ActionEvent e) {
    updateCollapsePane();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    JXTitledSeparator sepParam2 = new JXTitledSeparator();
    panel4 = new JPanel();
    radioEdge = new JRadioButton();
    combEdgeMode = new JComboBox<>();
    radioPulse = new JRadioButton();
    ftfPulseWidth = new JFormattedTextField();
    JLabel label2 = new JLabel();
    JXTitledSeparator sepParam = new JXTitledSeparator();
    panel5 = new JPanel();
    JLabel label3 = new JLabel();
    ftfResetValue = new JFormattedTextField();
    cboxAutoReset = new JCheckBox();
    JLabel label4 = new JLabel();
    ftfRolloverValue = new JFormattedTextField();
    JLabel label5 = new JLabel();
    ftfEventStep = new JFormattedTextField();
    cboxPersistent = new JCheckBox();
    JXTitledSeparator sepParam3 = new JXTitledSeparator();
    panel3 = new JPanel();
    cboxEnableFreeing = new JCheckBox();
    panel1 = new JPanel();
    radioInterval = new JRadioButton();
    ftfInterval = new JFormattedTextField();
    label6 = new JLabel();
    radioFromOclock = new JRadioButton();
    combOclock = new JComboBox<>();
    panel2 = new JPanel();
    radioFreezeOnly = new JRadioButton();
    radioFreezeReset = new JRadioButton();

    //======== contentPanel ========
    {
      contentPanel.setBorder(new EmptyBorder(0, 0, 0, 10));
      contentPanel.setLayout(new FormLayout(
          "$lcgap, [60dlu,default]:grow",
          "default, $ugap, default, $lgap, default, $ugap, default, $lgap, default, $ugap, default, $lgap, default, $ugap, default"));
      contentPanel.add(virtualPointSourcePanel1, CC.xy(2, 1));

      //---- sepParam2 ----
      sepParam2.setTitle("Input Mode");
      contentPanel.add(sepParam2, CC.xy(2, 3));

      //======== panel4 ========
      {
        panel4.setLayout(new FormLayout(
            "$lcgap, right:[60dlu,default], $lcgap, [80dlu,default], $lcgap, default:grow",
            "default, $lgap, default"));

        //---- radioEdge ----
        radioEdge.setText("Edge-driven");
        panel4.add(radioEdge, CC.xywh(1, 1, 2, 1));
        panel4.add(combEdgeMode, CC.xy(4, 1));

        //---- radioPulse ----
        radioPulse.setText("Pulse-driven");
        panel4.add(radioPulse, CC.xywh(1, 3, 2, 1));
        panel4.add(ftfPulseWidth, CC.xy(4, 3));

        //---- label2 ----
        label2.setText("ms");
        panel4.add(label2, CC.xy(6, 3));
      }
      contentPanel.add(panel4, CC.xywh(1, 5, 2, 1));

      //---- sepParam ----
      sepParam.setTitle("Parameters");
      contentPanel.add(sepParam, CC.xy(2, 7));

      //======== panel5 ========
      {
        panel5.setLayout(new FormLayout(
            "right:[60dlu,default], $lcgap, [80dlu,default], $lcgap, default:grow",
            "3*(default, $lgap), default"));

        //---- label3 ----
        label3.setText("Reset Value:");
        panel5.add(label3, CC.xy(1, 1));
        panel5.add(ftfResetValue, CC.xy(3, 1));

        //---- cboxAutoReset ----
        cboxAutoReset.setText("Auto Reset");
        cboxAutoReset.setToolTipText("Reset counter when the input source becomes offline.");
        panel5.add(cboxAutoReset, CC.xy(5, 1));

        //---- label4 ----
        label4.setText("Rollover Value:");
        panel5.add(label4, CC.xy(1, 3));
        panel5.add(ftfRolloverValue, CC.xy(3, 3));

        //---- label5 ----
        label5.setText("Event Step:");
        panel5.add(label5, CC.xy(1, 5));
        panel5.add(ftfEventStep, CC.xy(3, 5));

        //---- cboxPersistent ----
        cboxPersistent.setText("Persistent");
        panel5.add(cboxPersistent, CC.xy(3, 7));
      }
      contentPanel.add(panel5, CC.xy(2, 9));

      //---- sepParam3 ----
      sepParam3.setTitle("Freezing");
      contentPanel.add(sepParam3, CC.xy(2, 11));

      //======== panel3 ========
      {
        panel3.setLayout(new FormLayout(
            "[60dlu,default]:grow",
            "default, $lgap, default"));

        //---- cboxEnableFreeing ----
        cboxEnableFreeing.setText("Timed Freezing Enabled");
        cboxEnableFreeing.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            cboxEnableFreeingActionPerformed(e);
          }
        });
        panel3.add(cboxEnableFreeing, CC.xy(1, 1));

        //======== freezePane ========
        {
          freezePane.setLayout(new FormLayout(
              "default:grow, $lcgap, default",
              "default"));

          //======== panel1 ========
          {
            panel1.setBorder(new CompoundBorder(
                new TitledBorder(" Freezing Time"),
                Borders.DLU2_BORDER));
            panel1.setLayout(new FormLayout(
                "default, $lcgap, [80dlu,default], $lcgap, default",
                "default, $lgap, default"));

            //---- radioInterval ----
            radioInterval.setText("Interval:");
            panel1.add(radioInterval, CC.xy(1, 1));
            panel1.add(ftfInterval, CC.xy(3, 1));

            //---- label6 ----
            label6.setText("mins");
            panel1.add(label6, CC.xy(5, 1));

            //---- radioFromOclock ----
            radioFromOclock.setText("From O'Clock:");
            panel1.add(radioFromOclock, CC.xy(1, 3));
            panel1.add(combOclock, CC.xy(3, 3));
          }
          freezePane.add(panel1, CC.xy(1, 1));

          //======== panel2 ========
          {
            panel2.setBorder(new CompoundBorder(
                new TitledBorder(" Freezing Option"),
                Borders.DLU2_BORDER));
            panel2.setLayout(new FormLayout(
                "default",
                "default, $lgap, default"));

            //---- radioFreezeOnly ----
            radioFreezeOnly.setText("Freeze Only");
            panel2.add(radioFreezeOnly, CC.xy(1, 1));

            //---- radioFreezeReset ----
            radioFreezeReset.setText("Freeze then reset");
            panel2.add(radioFreezeReset, CC.xy(1, 3));
          }
          freezePane.add(panel2, CC.xy(3, 1));
        }
        panel3.add(freezePane, CC.xy(1, 3));
      }
      contentPanel.add(panel3, CC.xy(2, 13));
      contentPanel.add(panelLabelSet, CC.xy(2, 15));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
    combOclock.setEnabled(radioFromOclock.isSelected());
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private VirtualPointSourcePanel virtualPointSourcePanel1;
  private JPanel panel4;
  private JRadioButton radioEdge;
  private JComboBox<EDGE_DRIVEN_MODE> combEdgeMode;
  private JRadioButton radioPulse;
  private JFormattedTextField ftfPulseWidth;
  private JPanel panel5;
  private JFormattedTextField ftfResetValue;
  private JCheckBox cboxAutoReset;
  private JFormattedTextField ftfRolloverValue;
  private JFormattedTextField ftfEventStep;
  private JCheckBox cboxPersistent;
  private JPanel panel3;
  private JCheckBox cboxEnableFreeing;
  private JPanel freezePane;
  private JPanel panel1;
  private JRadioButton radioInterval;
  private JFormattedTextField ftfInterval;
  private JLabel label6;
  private JRadioButton radioFromOclock;
  private JComboBox<FreezeClockTime> combOclock;
  private JPanel panel2;
  private JRadioButton radioFreezeOnly;
  private JRadioButton radioFreezeReset;
  private LabelSetPanel panelLabelSet;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected BufferValidator createValidator(PresentationModel<CounterPoint> target) {
    return new CounterPointValidator(target);
  }

  @Override
  protected JPanel createMainPanel() {
    return contentPanel;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() instanceof JRadioButton) {
      updateEnablement();
    }

  }

  @Override
  protected void editingPointChanged(CounterPoint newItem) {
    inputSigPM.setBean(newItem.getInputSignal());
  }

}
