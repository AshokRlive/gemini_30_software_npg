/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

/**
 * Interface for digital virtual point. This defines all names and setter/getter
 * methods of properties supported by a digital point.
 */
public interface DigitalPoint extends VirtualPoint {

  /** The name of bound property {@value} . type:integer */
  String PROPERTY_CHATTERNO = "chatterNo";

  /** The name of bound property {@value} . type:integer */
  String PROPERTY_CHATTERTIME = "chatterTime";

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  String PROPERTY_DELAY_ENABLED = "delayEnabled";

  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: seconds.</li>
   */
  String PROPERTY_DELAY0 = "delay0";
  
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: seconds.</li>
   */
  String PROPERTY_DELAY1 = "delay1";
  
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: seconds.</li>
   */
  String PROPERTY_DELAY2 = "delay2";
  
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: seconds.</li>
   */
  String PROPERTY_DELAY3 = "delay3";
  
  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: seconds.</li>
   * @deprecated replaced by delay0,1,2,3
   */
  @Deprecated
  String PROPERTY_DELAY = "delay";

  /**
   * The name of property {@value} . <li>Value type: {@link EDGE_DRIVEN_MODE}.</li>
   * @deprecated replaced by delay0,1,2,3
   */
  @Deprecated
  String PROPERTY_DRIVEN_MODE = "drivenMode";


  // ================== Setter/Getter ===================

  int getChatterNo();

  void setChatterNo(int chatterNo)/* throws PropertyVetoException */;

  int getChatterTime();

  void setChatterTime(int chatterTime)/* throws PropertyVetoException */;

  boolean isDelayEnabled();

  void setDelayEnabled(boolean delayEnabled);
  long getDelay0();
  void setDelay0(long delay);
  long getDelay1();
  void setDelay1(long delay);
  long getDelay2();
  void setDelay2(long delay);
  long getDelay3();
  void setDelay3(long delay);
    
  
  /**
   * @deprecated replaced by delay0,1,2,3
   */
  @Deprecated
  long getDelay();

  /**
   * @deprecated replaced by delay0,1,2,3
   */
  @Deprecated
  void setDelay(long delay);

  /**
   * @deprecated replaced by delay0,1,2,3
   */
  @Deprecated
  EDGE_DRIVEN_MODE getDrivenMode();

  /**
   * @deprecated replaced by delay0,1,2,3
   */
  @Deprecated
  void setDrivenMode(EDGE_DRIVEN_MODE drivenMode);
}