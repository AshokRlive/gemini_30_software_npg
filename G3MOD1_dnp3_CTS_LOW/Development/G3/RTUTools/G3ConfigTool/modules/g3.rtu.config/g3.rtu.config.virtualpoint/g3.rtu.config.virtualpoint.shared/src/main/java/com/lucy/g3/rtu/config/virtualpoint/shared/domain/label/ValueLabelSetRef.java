/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.label;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;


public final class ValueLabelSetRef extends Model implements IValueLabelSetUser{
  public final static  String PROPERTY_LABEL_SET = "labelSet";
  public final static  String PROPERTY_LABEL_SET_CHANGED = ValueLabelSet.LABEL_SET_CHANGED;

  private ValueLabelSet labelSet;
  
  private final VirtualPoint point;
  private PropertyChangeListener labelSetChangeHandler = new PropertyChangeListener() {
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      firePropertyChange(PROPERTY_LABEL_SET_CHANGED, evt.getOldValue(), evt.getNewValue());
    }
  };
  
  public ValueLabelSetRef(VirtualPoint point){
    this.point = point;
  }
  
  public final ValueLabelSet getLabelSet() {
    return labelSet;
  }
  
  @Override
  public final void setLabelSet(ValueLabelSet newLabelSet) {
    ValueLabelSet oldValue = this.labelSet;
    if (oldValue != null) {
      oldValue.removePropertyChangeListener(ValueLabelSet.LABEL_SET_CHANGED, labelSetChangeHandler);
      oldValue.removeUser(this);
    }
    
    this.labelSet = newLabelSet;
    firePropertyChange(PROPERTY_LABEL_SET, oldValue, newLabelSet);
    firePropertyChange(PROPERTY_LABEL_SET_CHANGED,  oldValue, newLabelSet);

    if (newLabelSet != null) {
      newLabelSet.addPropertyChangeListener(ValueLabelSet.LABEL_SET_CHANGED, labelSetChangeHandler);
      newLabelSet.addUser(this);
    }
  }

  @Override
  public String getName() {
    return point.getName();
  }
}

