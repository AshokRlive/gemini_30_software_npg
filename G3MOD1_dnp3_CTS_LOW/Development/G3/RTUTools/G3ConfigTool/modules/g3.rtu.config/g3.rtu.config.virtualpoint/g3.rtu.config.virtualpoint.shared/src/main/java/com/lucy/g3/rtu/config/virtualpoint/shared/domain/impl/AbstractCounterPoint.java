/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.g3schema.ns_vpoint.BaseCounterPointT;
import com.g3schema.ns_vpoint.CounterValuesT;
import com.g3schema.ns_vpoint.FreezingConfT;
import com.g3schema.ns_vpoint.InputModeT;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.InputSignal;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.EDGE_DRIVEN_MODE;

/**
 * Basic implementation of {@linkplain CounterPoint}.
 */
public abstract class AbstractCounterPoint extends AbstractVirtualPoint implements CounterPoint {

  private final InputSignal inputSignal = new InputSignal();

  private boolean persistent = false;
  private boolean autoReset = true;
  private boolean freezingEnabled = false;
  private FreezeTimeMode freezeTimeMode = FreezeTimeMode.INTERVAL;
  private FreezeMode freezeMode = FreezeMode.FREEZE_ONLY;
  private long freezeInterval = 30;
  private FreezeClockTime freezeClockTime = FreezeClockTime.ONE_MIN;
  private long rolloverValue = 1000;
  private long eventStep = 1;
  private long resetValue = 0;

  private PersistentCounter persistentCounter;


  public AbstractCounterPoint(int group, int id, String description) {
    super(group, id, description, VirtualPointType.COUNTER);
  }

  @Override
  public InputSignal getInputSignal() {
    return inputSignal;
  }

  @Override
  public boolean isAutoReset() {
    return autoReset;
  }

  @Override
  public boolean isFreezingEnabled() {
    return freezingEnabled;
  }

  @Override
  public void setFreezingEnabled(boolean freezingEnabled) {
    Object oldValue = this.freezingEnabled;
    this.freezingEnabled = freezingEnabled;
    firePropertyChange("freezingEnabled", oldValue, freezingEnabled);
  }

  @Override
  public FreezeTimeMode getFreezeTimeMode() {
    return freezeTimeMode;
  }

  @Override
  public void setFreezeTimeMode(FreezeTimeMode freezeTimeMode) {
    Object oldValue = this.freezeTimeMode;
    this.freezeTimeMode = freezeTimeMode;
    firePropertyChange("freezeTimeMode", oldValue, freezeTimeMode);
  }

  @Override
  public FreezeMode getFreezeMode() {
    return freezeMode;
  }

  @Override
  public void setFreezeMode(FreezeMode freezeMode) {
    Object oldValue = this.freezeMode;
    this.freezeMode = freezeMode;
    firePropertyChange("freezeMode", oldValue, freezeMode);
  }

  @Override
  public long getFreezeInterval() {
    return freezeInterval;
  }

  @Override
  public void setFreezeInterval(long freezeInterval) {
    Object oldValue = this.freezeInterval;
    this.freezeInterval = freezeInterval;
    firePropertyChange("freezeInterval", oldValue, freezeInterval);
  }

  @Override
  public FreezeClockTime getFreezeClockTime() {
    return freezeClockTime;
  }

  @Override
  public void setFreezeClockTime(FreezeClockTime freezeClockTime) {
    Object oldValue = this.freezeClockTime;
    this.freezeClockTime = freezeClockTime;
    firePropertyChange("freezeClockTime", oldValue, freezeClockTime);
  }

  @Override
  public long getRolloverValue() {
    return rolloverValue;
  }

  @Override
  public void setRolloverValue(long rolloverValue) {
    Object oldValue = this.rolloverValue;
    this.rolloverValue = rolloverValue;
    firePropertyChange("rolloverValue", oldValue, rolloverValue);
  }

  @Override
  public long getEventStep() {
    return eventStep;
  }

  @Override
  public void setEventStep(long eventStep) {
    Object oldValue = this.eventStep;
    this.eventStep = eventStep;
    firePropertyChange("eventStep", oldValue, eventStep);
  }

  @Override
  public long getResetValue() {
    return resetValue;
  }

  @Override
  public void setResetValue(long resetValue) {
    Object oldValue = this.resetValue;
    this.resetValue = resetValue;
    firePropertyChange("resetValue", oldValue, resetValue);
  }

  @Override
  public void setAutoReset(boolean autoReset) {
    Object oldValue = this.autoReset;
    this.autoReset = autoReset;
    firePropertyChange("autoReset", oldValue, autoReset);
  }

  @Override
  public boolean isPersistent() {
    return persistent;
  }

  @Override
  public void setPersistent(boolean persistent) {
    boolean oldValue = this.persistent;
    this.persistent = persistent;
    firePropertyChange("persistent", oldValue, persistent);

    if (persistentCounter != null) {
      persistentCounter.updateCount(this);
    }
  }

  @Override
  public void setPersistentCounter(PersistentCounter persistentCounter) {
    // Remove this point from old counter
    if (this.persistentCounter != null) {
      this.persistentCounter.remove(this);
    }

    this.persistentCounter = persistentCounter;

    // Add this point to the new counter
    if (persistentCounter != null) {
      persistentCounter.updateCount(this);
    }
  }

  @Override
  public void delete() {
    super.delete();

    if (persistentCounter != null) {
      persistentCounter.remove(this);
    }
  }


  /**
   * An object for counting the number of the persistent points.
   */
  public static final class PersistentCounter {

    private Logger log = Logger.getLogger(PersistentCounter.class);

    private final ArrayList<CounterPoint> persistentPoints = new ArrayList<CounterPoint>();


    private void updateCount(CounterPoint point) {
      if (point == null) {
        return;
      }

      if (point.isPersistent() && !persistentPoints.contains(point)) {
        persistentPoints.add(point);
        loggingNum();
      } else if (!point.isPersistent()) {
        persistentPoints.remove(point);
      }

    }

    private void loggingNum() {
      log.info("Persistent Points Number:" + getPersistentPointNum());
    }

    private void remove(CounterPoint point) {
      persistentPoints.remove(point);
      loggingNum();
    }

    public int getPersistentPointNum() {
      return persistentPoints.size();
    }
  }


  protected void writeToXML(BaseCounterPointT xml) {
    super.writeToXML(xml);

    xml.autoReset.setValue(isAutoReset());
    xml.persistent.setValue(isPersistent());
    xml.description.setValue(getDescription());
    xml.group.setValue(getGroup());
    xml.id.setValue(getId());

    InputModeT xml_inputMode = xml.inputMode.append();
    InputSignal inputSig = getInputSignal();
    if (inputSig.getInputMode() == InputMode.EDGE_DRIVEN) {
      xml_inputMode.edgeDriven.append().edgeDrivenMode.setValue(inputSig.getEdgeDrivenMode().getValue());
    } else if (inputSig.getInputMode() == InputMode.PULSE_DRIVEN) {
      xml_inputMode.pulseDriven.append().pulseWidth.setValue(inputSig.getPulseWidth());
    }

    FreezingConfT xml_freezing = xml.freezing.append();
    xml_freezing.enabled.setValue(isFreezingEnabled());
    xml_freezing.freezeThenReset.setValue(getFreezeMode() == FreezeMode.FREEZE_THEN_RESET);
    if (getFreezeTimeMode() == FreezeTimeMode.FROM_OCLOCK) {
      xml_freezing.freezingClockTime.append().setValue(getFreezeClockTime().getValue());
    } else if (getFreezeTimeMode() == FreezeTimeMode.INTERVAL) {
      xml_freezing.freezingInterval.append().setValue(getFreezeInterval());
    }

    CounterValuesT xml_values = xml.counterValues.append();
    xml_values.eventStepValue.setValue(getEventStep());
    xml_values.resetValue.setValue(getResetValue());
    xml_values.rolloverValue.setValue(getRolloverValue());
  }

  protected void readFromXML(BaseCounterPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);

    setPersistent(xml.persistent.getValue());
    setAutoReset(xml.autoReset.getValue());

    // input mode
    InputModeT xml_inputmode = xml.inputMode.first();
    if (xml_inputmode.edgeDriven.exists()) {
      getInputSignal().setInputMode(InputMode.EDGE_DRIVEN);
      getInputSignal().setEdgeDrivenMode(EDGE_DRIVEN_MODE.forValue(
          (int) xml_inputmode.edgeDriven.first().edgeDrivenMode.getValue()));

    } else if (xml_inputmode.pulseDriven.exists()) {
      getInputSignal().setInputMode(InputMode.PULSE_DRIVEN);
      getInputSignal().setPulseWidth((int) xml_inputmode.pulseDriven.first().pulseWidth.getValue());
    }

    // freezing
    FreezingConfT xml_freeze = xml.freezing.first();
    setFreezingEnabled(xml_freeze.enabled.getValue());
    setFreezeMode(xml_freeze.freezeThenReset.getValue()
        ? FreezeMode.FREEZE_THEN_RESET
        : FreezeMode.FREEZE_ONLY);
    setFreezeTimeMode(xml_freeze.freezingInterval.exists()
        ? FreezeTimeMode.INTERVAL
        : FreezeTimeMode.FROM_OCLOCK);

    if (xml_freeze.freezingInterval.exists()) {
      setFreezeInterval(xml_freeze.freezingInterval.first().getValue());
    }
    if (xml_freeze.freezingClockTime.exists()) {
      setFreezeClockTime(FreezeClockTime.forValue(
          xml_freeze.freezingClockTime.first().getValue()));
    }

    // counter values
    setResetValue(xml.counterValues.first().resetValue.getValue());
    setRolloverValue(xml.counterValues.first().rolloverValue.getValue());
    setEventStep(xml.counterValues.first().eventStepValue.getValue());

  }

}
