/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

/**
 * Interface of Pseudo Virtual Point which is managed by Control Logic and
 * cannot be deleted or change description by user.
 */
public interface IPseudoPoint extends VirtualPoint{

  int DEF_PSEUDO_POINT_GROUP = -1;


  void updateGroup();
  
  /**
   * Changes the description of this point if it is a custom point.
   */
  void setDescription(String descrption);

  /**
   * Changes the id of this point if it is a custom point.
   */
  void setId(int id);
  
  boolean isAllowChangeDescription();
  
  void setAllowChangeDescription(boolean allowChangeDescription);
  
  @Override
  boolean isAllowDelete();
  
  void setAllowDelete(boolean allowDelete);
}
