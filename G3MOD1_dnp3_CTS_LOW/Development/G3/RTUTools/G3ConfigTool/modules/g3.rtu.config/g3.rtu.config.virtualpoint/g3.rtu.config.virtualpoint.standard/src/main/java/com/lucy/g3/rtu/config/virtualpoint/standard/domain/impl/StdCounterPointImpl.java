/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import com.g3schema.ns_common.VirtualPointRefT;
import com.g3schema.ns_vpoint.CounterPointT;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.validation.StandardPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain StdCounterPoint}.
 */
public final class StdCounterPointImpl extends AbstractCounterPoint implements StdCounterPoint {

  private final IValidator validator;


  StdCounterPointImpl(int id, String description) {
    super(STANDARD_POINT_GROUP, id, description);
    validator = new StandardPointValidator(this);
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }

  @Override
  public void setSource(VirtualPoint source) {
      SourceChecker.checkPoint(this, source);
      super.setSource(source);
  }
  
  @Override
  public void setSource(IVirtualPointSource source)
      throws NotConnectibleException {
    SourceChecker.checkPoint(this, source);
    super.setSource(source);
  }

  @Override
  public VirtualPoint getSource() {
    return (VirtualPoint) super.getSource();
  }

  @Override
  public String getHTMLSummary() {
    return null;
  }

  @Override
  public IStandardPoint duplicate() {
    StdCounterPointImpl copy = new StdCounterPointImpl(getId(), getDescription());
    copy.setEventStep(getEventStep());
    copy.setFreezeClockTime(getFreezeClockTime());
    copy.setFreezeInterval(getFreezeInterval());
    copy.setFreezeMode(getFreezeMode());
    copy.setFreezeTimeMode(getFreezeTimeMode());
    copy.setResetValue(getResetValue());
    copy.setRolloverValue(getRolloverValue());
    copy.setAutoReset(isAutoReset());
    try {
      copy.setSource(getSource());
    } catch (NotConnectibleException e) {
      e.printStackTrace();
    }
    return copy;
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return null;
  }

  @Override
  public void writeToXML(CounterPointT xml, VirtualPointWriteSupport support) {
    super.writeToXML(xml);

    VirtualPoint input = getSource();
    if (input != null) {
      VirtualPointRefT xml_input = xml.inputSource.append();
      xml_input.pointGroup.setValue(input.getGroup());
      xml_input.pointID.setValue(input.getId());
    }
  }

  @Override
  public void readFromXML(CounterPointT xml, VirtualPointReadSupport support) {
    super.readFromXML(xml, support);

    setId((int) xml.id.getValue());

    if (xml.description.exists()) {
      setDescription(xml.description.getValue());
    }

    if (xml.inputSource.exists()) {
      VirtualPoint vp = support.getVirtualPointByRef(xml.inputSource.first());
      setSource(vp);
    }
  }

}
