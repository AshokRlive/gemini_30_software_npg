/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointStub;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;

public class PointSelectorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  public static void main(String[] args) {
    ArrayList<VirtualPoint> allPoints = new ArrayList<VirtualPoint>();
    for (int i = 0; i < 1000; i++) {
      allPoints.add(new VirtualPointStub(i, 0, VirtualPointType.ANALOGUE_INPUT));
      allPoints.add(new VirtualPointStub(i, 0, VirtualPointType.DOUBLE_BINARY_INPUT));
      allPoints.add(new VirtualPointStub(i, 0, VirtualPointType.BINARY_INPUT));
    }

    VirtualPointSelector sel = new VirtualPointSelector(null, null, allPoints, -1);
    sel.setSelectionMode(SelectionMode.SELECTION_MODE_MULTI);
    sel.pack();
    sel.setVisible(true);
  }
}
