/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import java.util.Collection;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;

/**
 * Interface of virtual point manager.
 */
public interface VirtualPointManager extends VirtualPointRepository, IContainerValidation {

  
  /**
   * Inserts a standard virtual point to the existing point list at a specific
   * position. All points' ID will be updated as necessary.
   *
   * @param point
   *          the point to be inserted
   * @param position
   *          the position to be inserted. If the position is out of range, the
   *          point will be inserted at the end of the point list.
   * @throws DuplicatedException
   *           if the point to be added already exists
   */
  void addPoint(IStandardPoint point, int position);

  /**
   * Adds a standard virtual point at the end of the existing points list. All
   * points' ID will be updated as necessary.
   *
   * @param point
   *          new point to add
   */
  void addPoint(IStandardPoint point);

  /**
   * Adds a collection of standard points without updating standard points' ID.
   *
   * @param points
   *          new points to be added
   */
  void addAllPoints(Collection<IStandardPoint> points);

  /**
   * Adds a collection of standard points.
   *
   * @param points
   *          new points to be added
   * @param updateId
   *          if true, all standard points' ID would be updated after adding.
   */
  void addAllPoints(Collection<IStandardPoint> points, boolean updateId);

  /**
   * Removes a standard point and updates all standard points' ID.
   *
   * @param point
   *          the point to be removed.
   */
  void removePoint(IStandardPoint point);

  /**
   * Removes a collection of standard points without updating points' ID.
   *
   * @param points
   *          the points to be removed.
   */
  void removeAllPoints(Collection<IStandardPoint> points);

  

  /**
   * Swaps the position of two points.
   *
   * @param p0
   *          point 0
   * @param p1
   *          point 1
   */
  void swap(IStandardPoint p0, IStandardPoint p1);

  /**
   * Checks if any validation error exists.
   *
   * @return <code>true</code> if there is an error in validation result,
   *         <code>false</code> otherwise.
   */
  boolean hasError();

  int getStandPointNum(VirtualPointType type);

  void removeAllPseudoPoints(Collection<? extends IPseudoPoint> points);

  void addAllPseudoPoints(Collection<? extends IPseudoPoint> points);

  @Override
  IContainerValidator getValidator();

  ValueLabelSetManager getValueLabelManager();
}