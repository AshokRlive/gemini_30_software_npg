/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import java.util.HashMap;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractCounterPoint.PersistentCounter;

/**
 * Interface of Counter Virtual Point.
 */
public interface CounterPoint extends VirtualPoint {

  VirtualPointType SOURCE_TYPE = VirtualPointType.BINARY_INPUT;

  int MAX_PERSISTENT_POINTS = 16;

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  String PROPERTY_PERSISTENT = "persistent";

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  String PROPERTY_AUTORESET = "autoReset";

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  String PROPERTY_FREEZING_ENABLED = "freezingEnabled";

  /**
   * The name of property {@value} . <li>Value type: {@link FreezingTimeMode}.</li>
   */
  String PROPERTY_FREEZE_TIME_MODE = "freezeTimeMode";

  /**
   * The name of property {@value} . <li>Value type: {@link FreezingMode}.</li>
   */
  String PROPERTY_FREEZE_MODE = "freezeMode";

  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: seconds.</li>
   */
  String PROPERTY_FREEZE_INTERVAL = "freezeInterval";

  /**
   * The name of property {@value} . <li>Value type: {@link FreezeClockTime}.</li>
   * <li>Value unit: minutes.</li>
   */
  String PROPERTY_FREEZE_CLOCK_TIME = "freezeClockTime";

  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: minutes.</li>
   */
  String PROPERTY_RESET_VALUE = "resetValue";

  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: minutes.</li>
   */
  String PROPERTY_ROLLOVER_VALUE = "rolloverValue";

  /**
   * The name of property {@value} . <li>Value type: {@link Long}.</li> <li>
   * Value unit: minutes.</li>
   */
  String PROPERTY_EVENT_STEP = "eventStep";


  /** Input counting mode. */
  public enum InputMode {
    EDGE_DRIVEN("Edge Driven"),
    PULSE_DRIVEN("Pulse Driven");

    private InputMode(String description) {
      this.description = description;
    }

    @Override
    public String toString() {
      return description;
    }

    public String getDescription() {
      return description;
    }


    private final String description;
  }

  /** Freezing time mode. */
  public enum FreezeTimeMode {
    INTERVAL("Interval"),
    FROM_OCLOCK("From O'clock");

    private FreezeTimeMode(String description) {
      this.description = description;
    }

    @Override
    public String toString() {
      return description;
    }


    private final String description;
  }

  /** Freezing time mode. */
  public enum FreezeMode {
    FREEZE_ONLY,
    FREEZE_THEN_RESET
  }

  /** Freezing time from o'clock. */
  public enum FreezeClockTime {
    ONE_MIN(1L, "1 Minute"),
    TWO_MIN(2L, "2 Minutes"),
    FIVE_MIN(5L, "5 Minutes"),
    TEN_MIN(10L, "10 Minutes"),
    TWENTY_MIN(20L, "20 Minutes"),
    THRITY_MIN(30L, "30 Minutes"),
    ONE_HOUR(60L, "1 Hour"),
    TWO_HOUR(120L, "2 Hours"),
    THREE_HOUR(180L, "3 Hours"),
    SIX_HOUR(360L, "6 Hours"),
    TWELVE_HOUR(720L, "12 Hours"),
    ONE_DAY(1440L, "24 Hours");

    private static final HashMap<Long, FreezeClockTime> TYPES_BY_VALUE = new HashMap<Long, FreezeClockTime>();

    static {
      for (FreezeClockTime type : FreezeClockTime.values()) {
        TYPES_BY_VALUE.put(type.value, type);
      }
    }


    FreezeClockTime(Long value, String valueStr) {
      this.value = value;
      this.valueStr = valueStr;
    }

    @Override
    public String toString() {
      return valueStr;
    }

    public static FreezeClockTime forValue(long value) {
      return TYPES_BY_VALUE.get(value);
    }

    public Long getValue() {
      return value;
    }

    public String getValueStr() {
      return valueStr;
    }


    private final Long value;
    private final String valueStr;
  }


  boolean isAutoReset();

  boolean isPersistent();

  void setPersistent(boolean persistent);

  boolean isFreezingEnabled();

  void setFreezingEnabled(boolean freezingEnabled);

  FreezeTimeMode getFreezeTimeMode();

  void setFreezeTimeMode(FreezeTimeMode freezeTimeMode);

  FreezeMode getFreezeMode();

  void setFreezeMode(FreezeMode freezeMode);

  long getFreezeInterval();

  void setFreezeInterval(long freezeInterval);

  FreezeClockTime getFreezeClockTime();

  void setFreezeClockTime(FreezeClockTime freezeClockTime);

  long getRolloverValue();

  void setRolloverValue(long rolloverValue);

  long getEventStep();

  void setEventStep(long eventStep);

  long getResetValue();

  void setResetValue(long resetValue);

  void setAutoReset(boolean autoReset);

  /**
   * Gets the input signal.
   *
   * @return non-null InputSignal bean.
   */
  InputSignal getInputSignal();

  void setPersistentCounter(PersistentCounter persistentCounter);
}
