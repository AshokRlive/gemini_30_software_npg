/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.util.Collection;

import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;

/**
 * This interface specifies the resources required by VirtualPointEditorDialog.
 */
public interface IVirtualPointDialogResource {

  /**
   * Gets the channel for mapping to a virtual point.
   *
   * @param type
   *          channel types
   * @return available channels. must not be null.
   */
  Collection<IChannel> getAllChannels(ChannelType... type);

  /**
   * Gets the virtual point for mapping to another virtual point (such as counter point).
   *
   * @param type
   *          virtual point types
   * @return available virtual point. must not be null.
   */
  Collection<VirtualPoint> getAllVirtualPoints(VirtualPointType... type);

  /**
   * Gets the label manager.
   *
   * @return label manager, must not be null.
   */
  ValueLabelSetManager getLabelManager();

}
