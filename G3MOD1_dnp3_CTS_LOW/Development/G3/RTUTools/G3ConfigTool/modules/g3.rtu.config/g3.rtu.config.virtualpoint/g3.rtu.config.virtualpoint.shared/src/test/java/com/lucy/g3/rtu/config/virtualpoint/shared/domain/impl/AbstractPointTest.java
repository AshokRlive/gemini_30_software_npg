/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractVirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointStub;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class AbstractPointTest {

  private String[] readOnlyProperties = {
      AbstractVirtualPoint.PROPERTY_GROUP,
      AbstractVirtualPoint.PROPERTY_NAME,
  };

  private AbstractVirtualPoint testPoint;


  @Before
  public void setUp() throws Exception {
    testPoint = new VirtualPointStub(0, 0, VirtualPointType.DOUBLE_BINARY_INPUT);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstruct() {
    VirtualPointType[] types = VirtualPointType.values();

    for (int i = 0; i < types.length; i++) {
      AbstractVirtualPoint point = new VirtualPointStub(0, i, types[i]);
      assertEquals(types[i], point.getType());
      assertEquals(i, point.getId());
    }
  }

  @Test(expected = NullPointerException.class)
  public void testConstructWithNull() {
    new VirtualPointStub(0, 0, null);
  }

  @Test
  public void testReadOnlyProperties() {
    BeanTestUtil.testReadOnlyProperties(testPoint, readOnlyProperties);
  }

}
