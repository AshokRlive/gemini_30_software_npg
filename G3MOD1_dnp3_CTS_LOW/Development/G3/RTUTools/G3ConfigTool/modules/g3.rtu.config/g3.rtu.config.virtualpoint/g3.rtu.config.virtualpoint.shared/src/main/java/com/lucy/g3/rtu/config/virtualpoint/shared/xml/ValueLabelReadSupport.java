/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.xml;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;

/**
 * This interface specifies the resource required when reading Value Label from
 * XML.
 */
public interface ValueLabelReadSupport {

  ValueLabelSet getLabelSet(String id);
}
