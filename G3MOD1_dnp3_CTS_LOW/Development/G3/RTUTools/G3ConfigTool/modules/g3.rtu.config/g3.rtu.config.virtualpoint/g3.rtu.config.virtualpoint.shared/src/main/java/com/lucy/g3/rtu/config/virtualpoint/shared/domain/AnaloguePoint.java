/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

/**
 * Interface of Analogue Virtual Point.
 */
public interface AnaloguePoint extends VirtualPoint {

  double DEFAULT_HYSTERESIS_PERCENT = 0.01;

  double DEFAULT_DEADBAND_PERCENT = 0.05;

  // ================== Property Names(Parameter) ===================

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_SCALING_FACTOR = "scalingFactor";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_RAW_MIN = "rawMin";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_RAW_MAX = "rawMax";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_SCALED_MIN = "scaledMin";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_SCALED_MAX = "scaledMax";

  /** The name of bound property {@value} . Type: <code>String</code>. */
  String PROPERTY_UNIT = "unit";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_OFFSET = "offset";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_OVERFLOW = "overflow";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_UNDERFLOW = "underflow";

  /** The name of bound property {@value} . Type: <code>boolean</code>. */
  String PROPERTY_OVERFLOW_ENABLED = "overflowEnabled";

  /** The name of bound property {@value} . Type: <code>boolean</code>. */
  String PROPERTY_UNDERFLOW_ENABLED = "underflowEnabled";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_UNDERFLOW_HYSTERESIS = "underflowHysteresis";

  /** The name of bound property {@value} . Type: <code>double</code>. */
  String PROPERTY_OVERFLOW_HYSTERESIS = "overflowHysteresis";

  // ================== Property Names(Filter) ===================

  /**
   * The name of bound property {@value} . <li>Type: {@linkplain FILTER}</li>.
   */
  String PROPERTY_FILTER_TYPE = "filterType";

  /** The name of bound property {@value} . Type: <code>double</code> */
  String PROPERTY_FILTER0_DEADBAND = "f0Deadband";

  /** The name of bound property {@value} . Type: <code>double</code> */
  String PROPERTY_FILTER0_INIT_NOMINAL = "f0InitNominal";

  /** The name of bound property {@value} . Type: <code>boolean</code> */
  String PROPERTY_FILTER0_INIT_NOMINAL_ENABLED = "f0InitNominalEnabled";

  /** The name of bound property {@value} . Type: <code>int</code> */
  String PROPERTY_FILTER1_LOWER_HYSTERESIS = "f1LowerHysteresis";

  /** The name of bound property {@value} . Type: <code>int</code> */
  String PROPERTY_FILTER1_UPPER_HYSTERESIS = "f1UpperHysteresis";

  /** The name of bound property {@value} . Type: <code>double</code> */
  String PROPERTY_FILTER1_LOWER_LIMIT = "f1Lowerlimit";

  /** The name of bound property {@value} . Type: <code>double</code> */
  String PROPERTY_FILTER1_UPPER_LIMIT = "f1Upperlimit";

  /** The name of bound property {@value} . Type: <code>Boolean</code> */
  String PROPERTY_FILTER1_UPPER_ENABLED = "f1UpperEnabled";

  /** The name of bound property {@value} . Type: <code>Boolean</code> */
  String PROPERTY_FILTER1_LOWER_ENABLED = "f1LowerEnabled";


  // ================== Scaling Support for Analogue point configuration
  // ===================

  double getFullRangeValue();

  /**
   * Gets current scaling configuration.
   *
   * @return non-null scaling object.
   */
  double getScalingFactor();

  void setScalingFactor(double scalingFactor);

  double getRawMin();

  void setRawMin(double rawMin);

  double getRawMax();

  void setRawMax(double rawMax);

  double getScaledMin();

  void setScaledMin(double scaledMin);

  double getScaledMax();

  void setScaledMax(double scaledMax);

  String getRawUnit();

  String getUnit();

  void setUnit(String unit);

  // ================== Parameter Getter/Setter ===================

  double getOffset();

  void setOffset(double offset);

  double getOverflow();

  void setOverflow(double overflow);

  double getUnderflow();

  void setUnderflow(double underflow);

  double getUnderflowHysteresis();

  void setUnderflowHysteresis(double underflowHysteresis);

  double getOverflowHysteresis();

  void setOverflowHysteresis(double overflowHysteresis);

  boolean isOverflowEnabled();

  void setOverflowEnabled(boolean enabled);

  boolean isUnderflowEnabled();

  void setUnderflowEnabled(boolean enabled);

  // ================== Filters Getter/Setter ===================

  double getF0Deadband();

  void setF0Deadband(double f0Deadband);

  double getF0InitNominal();

  void setF0InitNominal(double f0InitNominal);

  boolean isF0InitNominalEnabled();

  void setF0InitNominalEnabled(boolean f0InitNominalEnabled);

  double getF1Lowerlimit();

  void setF1Lowerlimit(double f1Lowerlimit);

  double getF1Upperlimit();

  void setF1Upperlimit(double f1Upperlimit);

  double getF1LowerHysteresis();

  void setF1LowerHysteresis(double f1LowerHysteresis);

  void resetAllHysteresis();

  double getF1UpperHysteresis();

  void setF1UpperHysteresis(double f1UpperHysteresis);

  boolean isF1UpperEnabled();

  void setF1UpperEnabled(boolean f1UpperEnabled);

  boolean isF1LowerEnabled();

  void setF1LowerEnabled(boolean f1LowerEnabled);

  HighHighLowLow getF2HiHiLoLo();

  /**
   * Gets the name of the current filter.
   *
   * @return the name of current filter.
   */
  String getFilterName();

  /**
   * Gets the parameter as a string of the current filter.
   *
   * @return formatted text that represents filter parameters.
   */
  String getFilterParam();

  FILTER getFilterType();

  void setFilterType(FILTER filterType);


  /**
   * The analogue point filter type enum.
   */
  public enum FILTER {
    DeadBand("Dead band"),
    LowerUpperLimit("Lower Upper Limit"),
    UpperLimit("Upper Limit"),
    HighHighLowLow("Threshold"),
    None("None");

    private FILTER(String name) {
      this.name = name;
    }

    @Override
    public String toString() {
      return name;
    }

    public String getName() {
      return name;
    }


    private final String name;
  }
}