/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;

import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.list.SelectionInList;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * A presentation model for {@linkplain VirtualPointSelector}.
 */
class PointSelectorModel {

  private static final String ITEM_ALL_POINTS = "All Points";
  private static final String ITEM_ALL_MODULES = "All Modules";
  private static final String ITEM_VIRTUAL_POINTS = "[0] Virtual Points";

  // Column indexes
  public static final int COLUMN_SELECTED = SelectionTableModel.COLUMN_SELECTED;
  public static final int COLUMN_ITEM = SelectionTableModel.COLUMN_ITEM;

  private final SelectionInList<Object> moduleSelectionList = new SelectionInList<Object>();

  private final SelectionInList<Object> groupSelectionList = new SelectionInList<Object>();

  private final PointsTableModel pointsTableModel;

  private final VirtualPoint initialSelect;


  // Constructor
  PointSelectorModel(VirtualPoint initialSelect, Collection<VirtualPoint> points, int excludedGroup) {
    this.initialSelect = initialSelect;

    // Get all valid points
    ArrayList<VirtualPoint> allPoints = new ArrayList<VirtualPoint>();
    if (points != null) {
      for (VirtualPoint p : points) {
        if (p != null && (excludedGroup < 0 || excludedGroup != p.getGroup())) {
          allPoints.add(p);
        }
      }
    }
    
    // Sort all points
    Collections.sort(allPoints, new Comparator<VirtualPoint>() {
      @Override
      public int compare(VirtualPoint p1, VirtualPoint p2) {
        if (p1 == null || p2 == null)
          return 0;

        if (p1.getGroup() != p2.getGroup())
          return p1.getGroup() - p2.getGroup();
        else
          return p1.getId() - p2.getId();
      }
      
    });

    // Gets all available modules
    List<Object> mlist = moduleSelectionList.getList();
    mlist.add(ITEM_ALL_MODULES);
    for (VirtualPoint p : allPoints) {
      Module module = p.getSourceModule();
      if (module != null && !mlist.contains(module)) {
        mlist.add(module);
      }
    }

    // Get all available logic
    List<Object> logicList = groupSelectionList.getList();
    logicList.add(ITEM_ALL_POINTS); // Group all
    logicList.add(ITEM_VIRTUAL_POINTS); // Group 0
    for (VirtualPoint p : allPoints) {
      if (p.getGroup() > 0) {
        IVirtualPointSource groupSource = p.getSource();
        if (groupSource != null && !logicList.contains(groupSource)) {
          logicList.add(groupSource);
        }
      }
    }

    // Set initial selection
    moduleSelectionList.setSelection(ITEM_ALL_MODULES);
    groupSelectionList.setSelection(ITEM_ALL_POINTS);

    // Initialise points table model
    pointsTableModel = new PointsTableModel(allPoints, initialSelect);

    // Initialise event handling
    FilterSelectionHandler pcl = new FilterSelectionHandler();
    moduleSelectionList.addPropertyChangeListener(SelectionInList.PROPERTY_SELECTION, pcl);
    groupSelectionList.addPropertyChangeListener(SelectionInList.PROPERTY_SELECTION, pcl);

  }

  @SuppressWarnings("unchecked")
  public ComboBoxModel<Object> getModuleComboModel() {
    return new ComboBoxAdapter<Object>(moduleSelectionList);
  }

  @SuppressWarnings("unchecked")
  public ComboBoxModel<Object> getGroupComboModel() {
    return new ComboBoxAdapter<Object>(groupSelectionList);
  }

  public TableModel getPointsTableModel() {
    return pointsTableModel;
  }

  public VirtualPoint getInitialSelect() {
    return initialSelect;
  }

  public void clearSelection() {
    pointsTableModel.clearSelection();
  }

  public boolean hasSelection() {
    return pointsTableModel.hasSelection();
  }

  public VirtualPoint getSelection() {
    return pointsTableModel.getSelection();
  }
  
  public int getSelectionIndex() {
    return pointsTableModel.getSelectionIndex();
  }

  public Collection<VirtualPoint> getSelections() {
    return pointsTableModel.getSelections();
  }

  /**
   * Change the mode of selection between single and multiple .
   *
   * @param selectionMode
   *          <li>0: for single selection <li>1: for multiple selection
   */
  public void setSelectionMode(SelectionMode selectionMode) {
    pointsTableModel.setSelectionMode(selectionMode);
  }

  public SelectionMode getSelectionMode() {
    return pointsTableModel.getSelectionMode();
  }

  public void setSelectAll(boolean selectAll) {
    if (selectAll) {
      pointsTableModel.selectAll();
    } else {
      pointsTableModel.clearSelection();
    }
  }

  /**
   * Update channels list by applying the selected filter item.
   */
  private void updatePointsList() {
    Module module = null;
    IVirtualPointSource groupSource = null;

    Object selModuleItem = moduleSelectionList.getSelection();
    Object selGroupItem = groupSelectionList.getSelection();

    if (selModuleItem != null && selModuleItem instanceof Module) {
      module = (Module) selModuleItem;
    }

    if (selGroupItem != null && selGroupItem instanceof IVirtualPointSource) {
      groupSource = (IVirtualPointSource) selGroupItem;
    }

    pointsTableModel.setFilter(module, groupSource);
  }

  public void toggleSelectionAtIndex(int rowInModel) {
    pointsTableModel.toggleSelectionAtIndex(rowInModel);
  }


  private class FilterSelectionHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updatePointsList();
    }
  }

  /**
   * A extended table model for support multi-selection.
   */
  private static final class PointsTableModel extends SelectionTableModel<VirtualPoint> {

    private final Collection<VirtualPoint> allPoints;


    public PointsTableModel(Collection<VirtualPoint> allPoints, VirtualPoint initialSelection) {
      super(allPoints, "Virtual Point");
      setSelectionMode(SelectionMode.SELECTION_MODE_SINGLE);
      this.allPoints = allPoints;

      if (initialSelection != null) {
        setItemSelected(initialSelection, true);
      }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      if (columnIndex == COLUMN_ITEM) {
        VirtualPoint vp = getRow(rowIndex);
        return vp.getName();
      } else {
        return super.getValueAt(rowIndex, columnIndex);
      }
    }

    public void setFilter(Module selectedModule, IVirtualPointSource group) {
      assert SwingUtilities.isEventDispatchThread();

      itemListModel.clear();

      // No selected module, add all points
      if (selectedModule == null && group == null) {
        itemListModel.addAll(allPoints);

        // Add points that are mapped to the selected module
      } else {
        for (VirtualPoint p : allPoints) {
          if (p == null) {
            continue;
          }
          if (selectedModule != null && (p.getSource().checkSourceModule(selectedModule) == false)) {
            continue;
          }
          if (group != null && p.getSource() != group) {
            continue;
          }
          itemListModel.add(p);
        }
      }

      // Clear selectedPoints list
      ArrayList<VirtualPoint> unselect = new ArrayList<VirtualPoint>();
      for (VirtualPoint ch : selectedItems) {
        if (!itemListModel.contains(ch)) {
          unselect.add(ch);
        }
      }
      selectedItems.removeAll(unselect);

      // Fire change event
      fireTableDataChanged();
    }

  }

}
