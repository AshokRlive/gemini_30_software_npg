/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.ListModel;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractCounterPoint.PersistentCounter;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.util.VirtualPointFinder;
import com.lucy.g3.rtu.config.virtualpoint.shared.validation.VirtualPointManagerValidator;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Implementation of {@code VirtualPointsManager}.
 * <p>
 * All virtual points are managed in groups. For the points in group 0, they are
 * standard virtual point, their ID is always unique and sequential. Every time
 * a group 0 point is added/removed, all other points' ID will be updated to
 * keep them consistent.
 * </p>
 * <p>
 * For the points in group [1...*], they belong to control logic, and their ID
 * and group is managed and updated by control logic manager or their owner
 * control logic itself.
 * </p>
 *
 * @see VirtualPointManager
 */
public class VirtualPointManagerImpl extends AbstractConfigModule implements VirtualPointManager {

  // A map for storing points list by type.
  private final HashMap<VirtualPointType, ArrayListModel<VirtualPoint>> pointsMap =
      new HashMap<VirtualPointType, ArrayListModel<VirtualPoint>>();

  private final ArrayListModel<VirtualPoint> allPointsList = new ArrayListModel<VirtualPoint>();

  // The virtual point types supported by this manager.
  private final VirtualPointType[] supportedTypes = VirtualPointType.values();

  private int[] pseudoPointNum = new int[supportedTypes.length];

  private final PersistentCounter persistentCounter = new PersistentCounter();

  private final IContainerValidator validator;

  private final ValueLabelSetManager valueLabelManager = new ValueLabelSetManager();


  public VirtualPointManagerImpl(IConfig owner) {
    super(owner);

    validator = new VirtualPointManagerValidator(this);

    /* Initialise virtual points map */
    ArrayListModel<VirtualPoint> aipoints = new ArrayListModel<VirtualPoint>();
    pointsMap.put(VirtualPointType.ANALOGUE_INPUT, aipoints);

    ArrayListModel<VirtualPoint> bipoints = new ArrayListModel<VirtualPoint>();
    pointsMap.put(VirtualPointType.BINARY_INPUT, bipoints);

    ArrayListModel<VirtualPoint> dipoints = new ArrayListModel<VirtualPoint>();
    pointsMap.put(VirtualPointType.DOUBLE_BINARY_INPUT, dipoints);

    ArrayListModel<VirtualPoint> cpoints = new ArrayListModel<VirtualPoint>();
    pointsMap.put(VirtualPointType.COUNTER, cpoints);
  }

  @Override
  public void addPoint(IStandardPoint point, int position) {
    if (point == null) {
      return;
    }

    int maxPos = getStandPointNum(point.getType());
    if (position < 0 || position > maxPos) {
      position = maxPos;
    }

    if (add(point, position)) {
      updateStdPointsID();
    }
  }

  @Override
  public void addPoint(IStandardPoint point) {
    if (point == null) {
      return;
    }

    if (add(point, getStandPointNum(point.getType()))) {
      updateStdPointsID();
    }
  }

  @Override
  public void addAllPoints(Collection<IStandardPoint> points) {
    addAllPoints(points, false);
  }

  @Override
  public void addAllPoints(Collection<IStandardPoint> points, boolean updateId) {
    if (points == null) {
      return;
    }

    for (VirtualPoint p : points) {
      if (p != null) {
        add(p, getStandPointNum(p.getType()));
      }
    }

    if (updateId) {
      updateStdPointsID();
    }
  }

  @Override
  public void removePoint(IStandardPoint point) {
    if (remove(point)) {
      updateStdPointsID();
    }

  }

  @Override
  public void removeAllPoints(Collection<IStandardPoint> points) {
    if (points == null || points.isEmpty()) {
      return;
    }

    for (IStandardPoint p : points) {
      remove(p);
    }

    updateStdPointsID();
  }

  @Override
  @SuppressWarnings("unchecked")
  public ListModel<VirtualPoint> getPointsModel(VirtualPointType type) {
    return getPointsListModel(type);
  }

  @Override
  public VirtualPoint getPoint(int group, int id) {
    for (VirtualPoint p : allPointsList) {
      if (p.getGroup() == group && p.getId() == id) {
        return p;
      }
    }
    return null;
  }

  @Override
  public ArrayList<VirtualPoint> getAllPoints() {
    return new ArrayList<VirtualPoint>(allPointsList);
  }

  @Override
  public Collection<VirtualPoint> getAllPointsByType(VirtualPointType... type) {
    ArrayList<VirtualPoint> points = new ArrayList<VirtualPoint>(200);
    for (VirtualPointType t : type) {
      if (t != null) {
        points.addAll(getPointsListModel(t));
      }
    }
    return points;
  }

  @Override
  public Collection<VirtualPoint> getPointsByModuleType(MODULE... type) {
    return VirtualPointFinder.findPointsInModuleType(getAllPoints(), type);
  }

  @Override
  @SuppressWarnings("unchecked")
  public ListModel<VirtualPoint> getAllPointsModel() {
    return allPointsList;
  }

  @Override
  public int getPointsNum(VirtualPointType type) {
    return getPointsListModel(type).getSize();
  }

  @Override
  public int getAllPointsNum() {
    return allPointsList.getSize();
  }

  @Override
  public int getPersistentPointsNum() {
    return persistentCounter.getPersistentPointNum();
  }

  @Override
  public boolean contains(VirtualPoint point) {
    if (point == null) {
      return false;
    }

    return allPointsList.contains(point);
  }

  /**
   * Swaps two standard virtual points which belongs to same point type.
   */
  @Override
  public void swap(IStandardPoint p0, IStandardPoint p1) {
    if (p0 == null || p1 == null) {
      log.error("Cannot swap null point!");
      return;
    }

    if (p0.getType() != p1.getType()) {
      log.error("Cannot swap point in different types!");
      return;
    }

    if (!contains(p0) || !contains(p1)) {
      log.error("Cannot swap points as they are not found in point manager!");
      return;
    }

    // Swap position in allList
    int index0 = allPointsList.indexOf(p0);
    int index1 = allPointsList.indexOf(p1);
    allPointsList.set(index0, p1);
    allPointsList.set(index1, p0);

    // Swap position in subList
    ArrayListModel<VirtualPoint> list = getPointsListModel(p0.getType());
    index0 = list.indexOf(p0);
    index1 = list.indexOf(p1);
    list.set(index0, p1);
    list.set(index1, p0);

    // Swap id of two points
    int id0 = p0.getId();
    int id1 = p1.getId();
    p0.setId(id1);
    p1.setId(id0);
  }

  @Override
  public boolean hasError() {
    return !getValidator().isValid();
  }

  @Override
  public int getStandPointNum(VirtualPointType type) {
    int pointNum = getPointsNum(type);
    return pointNum - pseudoPointNum[type.ordinal()];
  }

  @Override
  public ValueLabelSetManager getValueLabelManager() {
    return valueLabelManager;
  }

  /**
   * Updates all standard points' ID to keep them unique and sequential.
   */
  private void updateStdPointsID() {
    int idBase = 0;
    for (int i = 0; i < supportedTypes.length; i++) {
      ArrayListModel<VirtualPoint> pointsList = getPointsListModel(supportedTypes[i]);
      for (VirtualPoint p : pointsList) {
        if (p instanceof IStandardPoint) {
          ((IStandardPoint) p).setId(idBase++);
        }
      }
    }

    log.debug("Standard points' IDs have been updated");
  }

  /**
   * Adds a virtual points to corresponding list.
   */
  private boolean add(VirtualPoint point, int position) {
    if (point == null) {
      log.error("Cannot add a null point.");
      return false;
    }

    if (contains(point)) {
      log.error(point.getType() + ": " + point.getGroupID()
          + " already exists in points manager.");
      return false;
    }

    /* Verify position */
    int num = getPointsNum(point.getType());
    if (position > num) {
      position = num;

    } else if (position < 0) {
      position = 0;
    }

    // Add point to the list model in point's type.
    getPointsListModel(point.getType()).add(position, point);

    if (point instanceof CounterPoint) {
      ((CounterPoint) point).setPersistentCounter(persistentCounter);
    }

    // Add point to the list model of all points.
    return addToAll(point);
  }

  private boolean addToAll(VirtualPoint point) {
    return allPointsList.add(point);
  }

  /**
   * Removes a point from corresponding list.
   *
   * @param point
   *          the point to be removed.
   */
  private boolean remove(VirtualPoint point) {
    if (!contains(point)) {
      log.warn("The point is not contained: " + point);
      return false;
    }

    boolean success = getPointsListModel(point.getType()).remove(point);
    if (success) {
      point.delete();
      removeFromAll(point);

      if (point instanceof CounterPoint) {
        ((CounterPoint) point).setPersistentCounter(null);
      }
    } else {
      log.warn("Cannot remove point:" + point + " cause it is not contained in list model.");
    }

    return success;
  }

  private boolean removeFromAll(VirtualPoint point) {
    return allPointsList.remove(point);
  }

  /**
   * Gets the related point list model for a point type.
   *
   * @return null if the give type is null.
   */
  private ArrayListModel<VirtualPoint> getPointsListModel(VirtualPointType type) {
    Preconditions.checkNotNull(type, "Point type is null");

    ArrayListModel<VirtualPoint> list = pointsMap.get(type);

    /* List not found, create new one */
    if (list == null) {
      list = new ArrayListModel<VirtualPoint>();
      pointsMap.put(type, list);
    }

    return list;
  }

  // ================== Pseudo Points Management ===================

  /**
   * Add pseudo points to the end of the point list.
   */
  @Override
  public void addAllPseudoPoints(Collection<? extends IPseudoPoint> points) {
    if (points == null || points.isEmpty()) {
      return;
    }

    for (VirtualPoint p : points) {
      try {
        if (add(p, getPointsNum(p.getType()))) {
          log.info("Succeeded to add pseudo point: " + p);
          pseudoPointNum[p.getType().ordinal()]++;
        } else {
          log.error("Failed to add pseudo point: " + p);
        }
      } catch (Exception e) {
        log.error("Failed to add fixture point: " + p, e);
      }
    }
  }

  @Override
  public void removeAllPseudoPoints(Collection<? extends IPseudoPoint> points) {
    if (points == null || points.isEmpty()) {
      return;
    }

    for (VirtualPoint p : points) {
      if (remove(p)) {
        pseudoPointNum[p.getType().ordinal()]--;
        log.info("Succeeded to remove fixture point: " + p);
      } else {
        log.error("Failed to remove fixture point:\"" + p + "\"");
      }

    }
  }

  @Override
  public IContainerValidator getValidator() {
    return validator;
  }

}
