/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.VirtualPointPage;

/**
 * The Class VirtualPoint Plugin.
 */
public class StandardPointPlugin implements IConfigPlugin, IPageFactory{
  private StandardPointPlugin () {}
  private final static StandardPointPlugin  INSTANCE = new StandardPointPlugin();
  
  public static void init() {
    PageFactories.registerFactory(PageFactories.KEY_VPOINT, INSTANCE);
  }
  
  public static void shutdown() {
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof VirtualPointManager) {
      return new VirtualPointPage((VirtualPointManager)data);

    } 
    
    return null;
  }
  
}
