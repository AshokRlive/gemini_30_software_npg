/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.g3schema.ns_vpoint.VirtualPointT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.model.impl.DefaultNode;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetRef;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * This abstract class is a full implementation of {@link VirtualPoint}. It
 * implements most common methods and fields required by a virtual point.
 * <p>
 * For the purpose of simplifying the implementation of various types of point,
 * this abstract class should be used as the super class of any other specific
 * implementation. It helps us keep further implementation simple, consistent
 * and easy to maintain.
 * </p>
 * <strong>Note</strong> some properties are implemented as read-only with
 * <code>protected</code> modifier. To change a property to writable, the
 * protected setter should be override to <code>public</code>.
 */
public abstract class AbstractVirtualPoint extends DefaultNode<IVirtualPointSource> implements VirtualPoint {
  public final static String PROPERTY_ALLOW_CHANGE_DESCRIPTION = "allowChangeDescription";
  
  protected final VirtualPointType type;

  private int id;

  private int group = 0;

  private final ValueLabelSetRef labelSetRef = new ValueLabelSetRef(this);

  private boolean customDescriptionEnabled;

  private final DescriptionUpdater descriptionUpdater = new DescriptionUpdater();

  private boolean allowChangeDescription = true; 
  /**
   * Constructor.
   *
   * @param group
   *          The group ID of the point
   * @param id
   *          The ID of the point
   * @param description
   *          The description of the Point
   * @param type
   *          The virtual point type @see {@link pointType}
   */
  public AbstractVirtualPoint(int group, int id, String description, VirtualPointType type) {
    super(NodeType.VIRTUAL_POINT, IVirtualPointSource.class);

    this.type = Preconditions.checkNotNull(type, "Point type must not be null");
    this.id = id;
    this.group = group;
    super.setDescription(description);

    syncNameWtihSource();

    // Update name when property changes
    addPropertyChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateName();
      }
    });

    updateName();
  }

  
  @Override
  public int getId() {
    return id;
  }

  public final void setId(int id) {
    if (getId() == id /*|| !isCustomPoint*/) {
      return;
    }

    Object oldValue = getId();
    this.id = id;
    firePropertyChange(PROPERTY_ID, oldValue, id);
  }

  @Override
  public final void setDescription(String description) {
    if (isAllowChangeDescription()) {
      super.setDescription(description);
      updateName();
    }
  }
  
  
  public boolean isAllowChangeDescription() {
    return allowChangeDescription;
  }

  public void setAllowChangeDescription(boolean allowChangeDescription) {
    Object oldValue = this.allowChangeDescription;
    this.allowChangeDescription = allowChangeDescription;
    firePropertyChange(PROPERTY_ALLOW_CHANGE_DESCRIPTION, oldValue, allowChangeDescription);
  }


  @Override
  public int getGroup() {
    return group;
  }

  protected void setGroup(int group) {
    if (getGroup() == group) {
      return;
    }

    Object oldValue = getGroup();
    this.group = group;
    firePropertyChange(PROPERTY_GROUP, oldValue, group);
  }

  @Override
  public void setAllowDelete(boolean allowDelete) {
    super.setAllowDelete(allowDelete);
  }

  @Override
  public boolean isCustomDescriptionEnabled() {
    return customDescriptionEnabled;
  }

  public void setCustomDescriptionEnabled(boolean customDescriptionEnabled) {
    Object oldValue = this.customDescriptionEnabled;
    this.customDescriptionEnabled = customDescriptionEnabled;
    firePropertyChange(PROPERTY_CUSTOM_DESCRIPTION_ENABLED, oldValue, customDescriptionEnabled);

    if (!customDescriptionEnabled) {
      descriptionUpdater.updateDescription();
    }
  }

  @Override
  public final ValueLabelSetRef getLabelSetRef() {
    return labelSetRef;
  }

  @Override
  protected void setSource(IVirtualPointSource source) throws NotConnectibleException {
    if (getSource() != null) {
      getSource().removePropertyChangeListener(descriptionUpdater);
    }

    super.setSource(source);

    if (source != null) {
      source.addPropertyChangeListener(IVirtualPointSource.PROPERTY_DESCRIPTION, descriptionUpdater);
    }
    descriptionUpdater.updateDescription();
  }

  @Override
  public final String getSourceName() {
    IVirtualPointSource source = getSource();
    return source == null ? null : source.getSourceName();
  }

  @Override
  protected final String generateName() {
    StringBuilder sb = new StringBuilder();
    sb.append(getGroupID());
    sb.append(" ");

    String sourceName = getSourceName();
    if (Strings.isBlank(sourceName) == false) {
      sb.append(sourceName);
      sb.append(" - ");
    }
    String descripton = getDescription();
    if (Strings.isBlank(descripton)) {
      descripton = "No Description";
    }
    sb.append(descripton);

    return sb.toString();
  }

  @Override
  public final VirtualPointType getType() {
    return type;
  }

  @Override
  public final String getGroupID() {
    return String.format("[%1d,%2d]", getGroup(), getId()).replace(' ', '0');
  }

  @Override
  public final String toString() {
    return getName();
  }

  protected void readFromXML(VirtualPointT xml, ValueLabelReadSupport support) {
    setGroup((int) xml.group.getValue());
    setId((int) xml.id.getValue());

    if (xml.description.exists()) {
      setDescription(xml.description.getValue());
    }

    if (xml.customDescriptionEnabled.exists()) {
      setCustomDescriptionEnabled(xml.customDescriptionEnabled.getValue());
    } else {
      setCustomDescriptionEnabled(false);
    }

    if (xml.customLabel.exists()) {
      labelSetRef.setLabelSet(support.getLabelSet(xml.customLabel.first().labelSetName.getValue()));
    }
  }

  protected void writeToXML(VirtualPointT xml) {
    xml.group.setValue(getGroup());
    xml.id.setValue(getId());
    xml.description.setValue(getDescription());
    xml.customDescriptionEnabled.setValue(isCustomDescriptionEnabled());

    ValueLabelSet labelSet = labelSetRef.getLabelSet();
    if (labelSet != null) {
      xml.customLabel.append().labelSetName.setValue(labelSet.getName());
    }
  }

  // ================== ExtendedBean Implementations==========================
  /**
   * Remove this point, clear its relationship to other objects and notify all
   * its observers their observed subject has been removed.
   */
  @Override
  public void delete() {
    super.delete();
    labelSetRef.setLabelSet(null);
  }

  @Override
  public final Module getSourceModule() {
    IVirtualPointSource source = getSource();
    return source == null ? null : source.getSourceModule();
  }

  @Override
  public final boolean checkSourceModule(Module module) {
    IVirtualPointSource source = getSource();
    return source == null ? false : source.checkSourceModule(module);
  }

  @Override
  public final boolean checkSourceModule(MODULE module) {
    IVirtualPointSource source = getSource();
    return source == null ? false : source.checkSourceModule(module);
  }


  private class DescriptionUpdater implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateDescription();
    }

    private void updateDescription() {
      /* The description of point cannot be changed it is pseudo point */
      if (!(AbstractVirtualPoint.this instanceof IPseudoPoint)) {
        if (isCustomDescriptionEnabled() == false) {
          IVirtualPointSource src = getSource();
          if (src != null) {
            setDescription(src.getDescription());
          }
        }
      }
    }
  }

}