/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel;

import static com.jgoodies.validation.view.ValidationComponentUtils.setMessageKey;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_HIHI_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_HIHI_THRESHOLD;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_HI_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_HI_THRESHOLD;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_LOLO_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_LOLO_THRESHOLD;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_LO_ENABLED;
import static com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow.PROPERTY_LO_THRESHOLD;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.shared.base.misc.ComponentGroup;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;

/**
 * Panel for configuring {@linkplain HighHighLowLow}.
 */
public final class HighHighLowLowPanel extends JPanel {

  private final PresentationModel<HighHighLowLow> pm;

  private final ComponentGroup[] groups = new ComponentGroup[4];
  private final PercentEditorPanel[] hysteresisPanels = new PercentEditorPanel[4];

  private final double fullRangeValue;


  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private HighHighLowLowPanel() {
    this(0, new PresentationModel<HighHighLowLow>());
  }

  public HighHighLowLowPanel(double fullRangeVal, PresentationModel<HighHighLowLow> hihilolo) {
    fullRangeValue = fullRangeVal;
    pm = Preconditions.checkNotNull(hihilolo, "hihilolo is null");

    initComponents();
    initComponentAnnotations();

    /* Init component groups */
    groups[0] = new ComponentGroup(lblHihiThreshold, ftfThresholdHighHigh, lblHihiUnitThreshold, hysteresisHihi);
    groups[1] = new ComponentGroup(lblHiThreshold, ftfThresholdHigh, lblHiUnitThreshold, hysteresisHi);
    groups[2] = new ComponentGroup(lblLoThreshold, ftfThresholdLow, lblLoUnitThreshold, hysteresisLo);
    groups[3] = new ComponentGroup(lblLoloThreshold, ftfThresholdLowLow, lblLoloUnitThreshold, hysteresisLolo);

    hysteresisPanels[0] = hysteresisHihi;
    hysteresisPanels[1] = hysteresisHi;
    hysteresisPanels[2] = hysteresisLo;
    hysteresisPanels[3] = hysteresisLolo;

    /* Bind checkBox states */
    groups[0].bindEnableState(cboxHighHigh);
    groups[1].bindEnableState(cboxHigh);
    groups[2].bindEnableState(cboxLow);
    groups[3].bindEnableState(cboxLowLow);
  }

  public void bindUnit(ValueModel unitVM) {
    if (unitVM == null) {
      return;
    }

    Bindings.bind(lblHihiUnitThreshold, unitVM);
    Bindings.bind(lblHiUnitThreshold, unitVM);
    Bindings.bind(lblLoUnitThreshold, unitVM);
    Bindings.bind(lblLoloUnitThreshold, unitVM);

    for (PercentEditorPanel hp : hysteresisPanels) {
      hp.bindUnit(unitVM);
    }
  }

  public void setPercentageVisibility(boolean visible) {
    for (PercentEditorPanel hp : hysteresisPanels) {
      hp.setPercentageVisibility(visible);
    }
  }

  public void setFullRangeValue(double fullrangeVal) {
    for (PercentEditorPanel hp : hysteresisPanels) {
      hp.setFullRangeValue(fullrangeVal);
    }
  }

  private void initComponentAnnotations() {
    setMessageKey(ftfThresholdHighHigh, PROPERTY_HIHI_THRESHOLD);
    setMessageKey(ftfThresholdHigh, PROPERTY_HI_THRESHOLD);
    setMessageKey(ftfThresholdLow, PROPERTY_LO_THRESHOLD);
    setMessageKey(ftfThresholdLowLow, PROPERTY_LOLO_THRESHOLD);
  }

  private void createUIComponents() {
    NumberFormatter doubleFormat = new NumberFormatter();
    doubleFormat.setValueClass(Double.class);

    ftfThresholdHighHigh =
        BasicComponentFactory.createFormattedTextField(pm.getBufferedModel(PROPERTY_HIHI_THRESHOLD), doubleFormat);
    ftfThresholdHigh =
        BasicComponentFactory.createFormattedTextField(pm.getBufferedModel(PROPERTY_HI_THRESHOLD), doubleFormat);
    ftfThresholdLowLow =
        BasicComponentFactory.createFormattedTextField(pm.getBufferedModel(PROPERTY_LOLO_THRESHOLD), doubleFormat);
    ftfThresholdLow =
        BasicComponentFactory.createFormattedTextField(pm.getBufferedModel(PROPERTY_LO_THRESHOLD), doubleFormat);

    FormattedTextFieldSupport.installCommitOnType(ftfThresholdHighHigh);
    FormattedTextFieldSupport.installCommitOnType(ftfThresholdHigh);
    FormattedTextFieldSupport.installCommitOnType(ftfThresholdLowLow);
    FormattedTextFieldSupport.installCommitOnType(ftfThresholdLow);

    cboxHighHigh = BasicComponentFactory.createCheckBox(pm.getBufferedModel(PROPERTY_HIHI_ENABLED), "");
    cboxHigh = BasicComponentFactory.createCheckBox(pm.getBufferedModel(PROPERTY_HI_ENABLED), "");
    cboxLow = BasicComponentFactory.createCheckBox(pm.getBufferedModel(PROPERTY_LO_ENABLED), "");
    cboxLowLow = BasicComponentFactory.createCheckBox(pm.getBufferedModel(PROPERTY_LOLO_ENABLED), "");

    String property;

    property = HighHighLowLow.PROPERTY_HIHI_HYSTERESIS;
    hysteresisHihi = new PercentEditorPanel(property, fullRangeValue, pm.getBufferedModel(property));

    property = HighHighLowLow.PROPERTY_HI_HYSTERESIS;
    hysteresisHi = new PercentEditorPanel(property, fullRangeValue, pm.getBufferedModel(property));

    property = HighHighLowLow.PROPERTY_LO_HYSTERESIS;
    hysteresisLo = new PercentEditorPanel(property, fullRangeValue, pm.getBufferedModel(property));

    property = HighHighLowLow.PROPERTY_LOLO_HYSTERESIS;
    hysteresisLolo = new PercentEditorPanel(property, fullRangeValue, pm.getBufferedModel(property));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    lblHihiThreshold = new JLabel();
    lblHihiUnitThreshold = new JLabel();
    lblHiThreshold = new JLabel();
    lblHiUnitThreshold = new JLabel();
    lblLoThreshold = new JLabel();
    lblLoUnitThreshold = new JLabel();
    lblLoloThreshold = new JLabel();
    lblLoloUnitThreshold = new JLabel();

    // ======== this ========
    setLayout(new FormLayout(
        "left:default, $lcgap, default, $lcgap, 35dlu, $lcgap, left:default, $lcgap, 7dlu, $lcgap, default",
        "3*(fill:default, $lgap), fill:default"));

    // ---- cboxHighHigh ----
    cboxHighHigh.setText("High High");
    add(cboxHighHigh, CC.xy(1, 1));

    // ---- lblHihiThreshold ----
    lblHihiThreshold.setText("Threshold:");
    add(lblHihiThreshold, CC.xy(3, 1, CC.RIGHT, CC.DEFAULT));
    add(ftfThresholdHighHigh, CC.xy(5, 1));
    add(lblHihiUnitThreshold, CC.xy(7, 1, CC.FILL, CC.DEFAULT));

    // ---- hysteresisHihi ----
    hysteresisHihi.setDisplayName("Hysteresis:");
    hysteresisHihi.setFadingEffectEnabled(true);
    add(hysteresisHihi, CC.xy(11, 1));

    // ---- cboxHigh ----
    cboxHigh.setText("High");
    add(cboxHigh, CC.xy(1, 3));

    // ---- lblHiThreshold ----
    lblHiThreshold.setText("Threshold:");
    add(lblHiThreshold, CC.xy(3, 3, CC.RIGHT, CC.DEFAULT));
    add(ftfThresholdHigh, CC.xy(5, 3));
    add(lblHiUnitThreshold, CC.xy(7, 3, CC.FILL, CC.DEFAULT));

    // ---- hysteresisHi ----
    hysteresisHi.setDisplayName("Hysteresis:");
    hysteresisHi.setFadingEffectEnabled(true);
    add(hysteresisHi, CC.xy(11, 3));

    // ---- cboxLow ----
    cboxLow.setText("Low");
    add(cboxLow, CC.xy(1, 5));

    // ---- lblLoThreshold ----
    lblLoThreshold.setText("Threshold:");
    add(lblLoThreshold, CC.xy(3, 5, CC.RIGHT, CC.DEFAULT));
    add(ftfThresholdLow, CC.xy(5, 5));
    add(lblLoUnitThreshold, CC.xy(7, 5, CC.FILL, CC.DEFAULT));

    // ---- hysteresisLo ----
    hysteresisLo.setDisplayName("Hysteresis:");
    hysteresisLo.setFadingEffectEnabled(true);
    add(hysteresisLo, CC.xy(11, 5));

    // ---- cboxLowLow ----
    cboxLowLow.setText("Low Low");
    add(cboxLowLow, CC.xy(1, 7));

    // ---- lblLoloThreshold ----
    lblLoloThreshold.setText("Threshold:");
    add(lblLoloThreshold, CC.xy(3, 7, CC.RIGHT, CC.DEFAULT));
    add(ftfThresholdLowLow, CC.xy(5, 7));
    add(lblLoloUnitThreshold, CC.xy(7, 7, CC.FILL, CC.DEFAULT));

    // ---- hysteresisLolo ----
    hysteresisLolo.setDisplayName("Hysteresis:");
    hysteresisLolo.setFadingEffectEnabled(true);
    add(hysteresisLolo, CC.xy(11, 7));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox cboxHighHigh;
  private JLabel lblHihiThreshold;
  private JFormattedTextField ftfThresholdHighHigh;
  private JLabel lblHihiUnitThreshold;
  private PercentEditorPanel hysteresisHihi;
  private JCheckBox cboxHigh;
  private JLabel lblHiThreshold;
  private JFormattedTextField ftfThresholdHigh;
  private JLabel lblHiUnitThreshold;
  private PercentEditorPanel hysteresisHi;
  private JCheckBox cboxLow;
  private JLabel lblLoThreshold;
  private JFormattedTextField ftfThresholdLow;
  private JLabel lblLoUnitThreshold;
  private PercentEditorPanel hysteresisLo;
  private JCheckBox cboxLowLow;
  private JLabel lblLoloThreshold;
  private JFormattedTextField ftfThresholdLowLow;
  private JLabel lblLoloUnitThreshold;
  private PercentEditorPanel hysteresisLolo;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
