/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain;

import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * The source of pseudo point.
 */
public interface IPseudoPointSource extends IVirtualPointSource {

  String PROPERTY_GROUP = "group";


  int getGroup();
}
