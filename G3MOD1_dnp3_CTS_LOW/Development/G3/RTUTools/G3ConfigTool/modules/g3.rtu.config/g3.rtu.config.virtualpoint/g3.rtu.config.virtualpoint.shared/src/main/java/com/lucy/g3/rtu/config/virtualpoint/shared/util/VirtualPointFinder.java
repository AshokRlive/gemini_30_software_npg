/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.util;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Utility for finding virtual points from a collection of virtual points.
 */
public class VirtualPointFinder {

  public static Collection<VirtualPoint> findPointsInModuleType(Collection<VirtualPoint> points, MODULE... type) {
    Preconditions.checkNotNull(type, "type is null");

    ArrayList<VirtualPoint> foundPoints = new ArrayList<VirtualPoint>();

    for (VirtualPoint p : points) {
      for (int i = 0; i < type.length; i++) {
        IVirtualPointSource source = p.getSource();
        if (source != null && source.checkSourceModule(type[i])) {
          foundPoints.add(p);
          break;
        }
      }
    }
    return foundPoints;
  }

  public static IStandardPoint findStdVirtualPoint(Module module, IChannelEnum chEnum,
      Collection<VirtualPoint> allPoints) {
    Preconditions.checkNotNull(chEnum, "chEnum is null");
    Preconditions.checkNotNull(allPoints, "allPoints is null");

    for (VirtualPoint vp : allPoints) {
      if (vp != null) {
        IVirtualPointSource source = vp.getSource();
        if (source != null
            && source.getSourceModule() == module
            && source instanceof IChannel
            && ((IChannel)source).getEnum() == chEnum) {
          return (IStandardPoint) vp;
        }
      }
    }
    return null;
  }
  
  public static Collection<VirtualPoint> findPointsByType(Collection<VirtualPoint> points, VirtualPointType... type) {
    ArrayList<VirtualPoint> foundPoints = new ArrayList<VirtualPoint>(200);

    if (points != null) {
      for (VirtualPoint p : points) {
        for (VirtualPointType t : type) {
          if (p != null && p.getType() == t) {
            foundPoints.add(p);
          }
        }
      }
    }
    return foundPoints;
  }
  
}
