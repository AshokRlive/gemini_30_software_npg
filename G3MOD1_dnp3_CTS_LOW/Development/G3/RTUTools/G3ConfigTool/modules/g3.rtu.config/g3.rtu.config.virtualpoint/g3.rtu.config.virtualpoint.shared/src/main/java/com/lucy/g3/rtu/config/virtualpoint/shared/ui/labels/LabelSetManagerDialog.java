/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.labels;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.IValueLabelSetUser;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;

/**
 * Dialog for managing {@linkplain ValueLabelSet} in
 * {@linkplain ValueLabelSetManager}.
 */
public final class LabelSetManagerDialog extends AbstractDialog {

  public static final String PROPERTY_SELECTION_NOT_EMPTY = "selectionNotEmpty";

  public static final String ACTION_ADD = "add";
  public static final String ACTION_REMOVE = "remove";
  public static final String ACTION_EDIT = "edit";

  private Logger log = Logger.getLogger(LabelSetManagerDialog.class);

  private final ValueLabelSetManager manager;
  private final SelectionInList<ValueLabelSet> labelSetSelections;


  public LabelSetManagerDialog(Frame owner, ValueLabelSetManager manager) {
    super(owner);
    this.manager = Preconditions.checkNotNull(manager, "manager must not be null");
    labelSetSelections = new SelectionInList<>(manager.getLabelSetsListModel());

    init();
  }

  public LabelSetManagerDialog(Dialog owner, ValueLabelSetManager manager) {
    super(owner, true);
    this.manager = Preconditions.checkNotNull(manager, "manager must not be null");
    labelSetSelections = new SelectionInList<>(manager.getLabelSetsListModel());

    init();
  }

  private void init() {
    setTitle("Label Sets Management");
    initComponents();
    initComponentsBinding();

    pack();
  }

  private void initComponentsBinding() {
    Bindings.bind(labelSetList, labelSetSelections);

    labelSetSelections.addPropertyChangeListener(SelectionInList.PROPERTY_SELECTION_EMPTY,
        new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            LabelSetManagerDialog.this.firePropertyChange(PROPERTY_SELECTION_NOT_EMPTY, !(Boolean) evt.getOldValue(),
                !(Boolean) evt.getNewValue());
          }
        });

    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(this);
    btnAdd.setAction(actions.get(ACTION_ADD));
    btnRemove.setAction(actions.get(ACTION_REMOVE));
    btnEdit.setAction(actions.get(ACTION_EDIT));
  }

  private void labelSetListMouseClicked(MouseEvent e) {
    if (UIUtils.isDoubleClick(e)) {
      edit();
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    scrollPane1 = new JScrollPane();
    labelSetList = new JList<>();
    panel1 = new JPanel();
    btnAdd = new JButton();
    btnRemove = new JButton();
    btnEdit = new JButton();

    // ======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "[200dlu,default]:grow, $lcgap, default",
          "fill:[100dlu,default]"));

      // ======== scrollPane1 ========
      {
        scrollPane1.setBorder(new TitledBorder("All Label Sets"));

        // ---- labelSetList ----
        labelSetList.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            labelSetListMouseClicked(e);
          }
        });
        scrollPane1.setViewportView(labelSetList);
      }
      contentPanel.add(scrollPane1, CC.xy(1, 1));

      // ======== panel1 ========
      {
        panel1.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            clearnSelection();
          }
        });
        panel1.setLayout(new FormLayout(
            "default",
            "3*(default, $lgap), default:grow"));

        // ---- btnAdd ----
        btnAdd.setText("Add");
        panel1.add(btnAdd, CC.xy(1, 1));

        // ---- btnRemove ----
        btnRemove.setText("Remove");
        panel1.add(btnRemove, CC.xy(1, 3));

        // ---- btnEdit ----
        btnEdit.setText("Edit");
        panel1.add(btnEdit, CC.xy(1, 5));
      }
      contentPanel.add(panel1, CC.xy(3, 1));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  private void clearnSelection() {
    labelSetSelections.clearSelection();
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JList<Object> labelSetList;
  private JPanel panel1;
  private JButton btnAdd;
  private JButton btnRemove;
  private JButton btnEdit;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Action
  public void add() {
    String name = JOptionPane.showInputDialog(this, "Please input a name for the new label set", "New Label Set");
    if (Strings.isBlank(name)) {
      // No input or user cancelled
      return;
    }

    if (manager.contains(name)) {
      JOptionPane.showMessageDialog(this, String.format("The label set:\"%s\" already exists", name),
          "Failure", JOptionPane.ERROR_MESSAGE);
      return;
    }

    ValueLabelSet newSet = new ValueLabelSet(name);
    if (manager.add(newSet)) {
      edit(newSet);// Editing the new created set
    } else {
      log.error("Failed to add new set:" + name);
    }
  }

  private void edit(ValueLabelSet newSet) {
    new LabelSetEditorDialog(this, newSet).setVisible(true);
  }

  @Action(enabledProperty = PROPERTY_SELECTION_NOT_EMPTY)
  public void edit() {
    ValueLabelSet selection = labelSetSelections.getSelection();
    if (selection != null) {
      edit(selection);
    }
  }

  @Action(enabledProperty = PROPERTY_SELECTION_NOT_EMPTY)
  public void remove() {
    ValueLabelSet selection = labelSetSelections.getSelection();
    if (selection == null) {
      log.error("The selection is null");
      return;
    }

    String message;
    ArrayList<IValueLabelSetUser> users = selection.getUsers();
    if (!users.isEmpty()) {
      StringBuilder sb = new StringBuilder();
      sb.append("<html><p>Do you really want to remove the label set:\"");
      sb.append(selection.getName());
      sb.append("\"</p>");
      sb.append("<p>which is being used by the following items:</p>");
      sb.append("<ul>");
      for (IValueLabelSetUser user : users) {
        if (user != null) {
          sb.append("<li>");
          sb.append(user.getName());
          sb.append("</li>");
        }
      }
      sb.append("<ul></html>");
      message = sb.toString();
    } else {
      message = "Do you want to remove the selection?";
    }

    int ret = JOptionPane.showConfirmDialog(this,
        message,
        "Remove", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
    if (ret == JOptionPane.YES_OPTION) {
      manager.remove(selection);
    }
  }

  /** The getter method of property {@value #PROPERTY_SELECTION_NOT_EMPTY}. */
  public boolean isSelectionNotEmpty() {
    return labelSetSelections.hasSelection();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createCloseButtonPanel();
  }
}
