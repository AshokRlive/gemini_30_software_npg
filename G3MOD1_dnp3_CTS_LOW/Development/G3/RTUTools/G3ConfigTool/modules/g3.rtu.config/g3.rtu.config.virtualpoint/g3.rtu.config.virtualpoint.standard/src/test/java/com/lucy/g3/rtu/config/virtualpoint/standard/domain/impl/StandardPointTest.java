/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.StdAnaloguePointImpl;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;

/**
 * The Class StandardPointTest.
 */
public class StandardPointTest {

  private IStandardPoint[] fixtures;


  @Before
  public void setUp() throws Exception {
    VirtualPointType[] types = VirtualPointType.values();
    fixtures = new IStandardPoint[types.length];
    for (int i = 0; i < types.length; i++) {
      fixtures[i] = VirtualPointFactory.createStanardPoint(types[i]);
    }
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAssignable() {
    Assert.assertTrue(StdAnaloguePoint.class.isAssignableFrom(StdAnaloguePointImpl.class));
    Assert.assertFalse(StdAnaloguePoint.class.isAssignableFrom(StdBinaryPoint.class));
  }

  @Test
  public void testDuplicate() {
    for (IStandardPoint p : fixtures) {
      String msg = "Fail to duplicate point: " + p.getType();
      IStandardPoint copy = p.duplicate();

      assertNotNull(msg, copy);
      assertEquals(msg, p.getDescription(), copy.getDescription());
      assertEquals(msg, p.getId(), copy.getId());
      assertEquals(msg, p.getDefaultCreateOption(), copy.getDefaultCreateOption());
      assertEquals(msg, p.getGroupID(), copy.getGroupID());
      assertEquals(msg, p.getHTMLSummary(), copy.getHTMLSummary());
      assertEquals(msg, p.getSourceModule(), copy.getSourceModule());
      assertEquals(msg, p.getName(), copy.getName());
      assertEquals(msg, p.getType(), copy.getType());
    }
  }

}
