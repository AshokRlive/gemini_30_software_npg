/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel;

import static com.jgoodies.validation.view.ValidationComponentUtils.setMessageKey;

import java.awt.Component;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.AbstractValueModel;
import com.jgoodies.binding.value.ValueHolder;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.effects.FadingBGValueChangeHandler;

/**
 * Panel for configuring a value model in the form of percentile or number.
 */
public final class PercentEditorPanel extends JPanel {

  public static final String PROPERTY_DISPLAY_NAME = "displayName";
  public static final String PROPERTY_ALLOW_NEGATIVE = "allowNegative";
  public static final String PROPERTY_FADING_EFFECT_ENABLED = "fadingEffectEnabled";

  private final ValueModel valueVM;
  private final PercentValueModel percentVM;

  private final String valuePropertyName;

  private boolean allowNegative;
  private FadingBGValueChangeHandler fadeHandler0;
  private FadingBGValueChangeHandler fadeHandler1;


  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private PercentEditorPanel() {
    valueVM = new ValueHolder(1.0D);
    percentVM = new PercentValueModel(valueVM, 100);
    valuePropertyName = "";

    init();
  }

  public PercentEditorPanel(
      String valuePropertyName,
      double fullRangeValue,
      ValueModel valueVM) {
    this.valuePropertyName = Preconditions.checkNotBlank(valuePropertyName, "valuePropertyName is blank");
    this.valueVM = Preconditions.checkNotNull(valueVM, "valueVM is null");
    percentVM = new PercentValueModel(valueVM, fullRangeValue);

    init();
  }

  private void init() {
    initComponents();
    initCommitOnType();
    initComponentAnnotation();
    initComponentsBinding();
    setAllowNegative(false);
    setFadingEffectEnabled(true);
  }

  public void bindUnit(ValueModel unitVM) {
    if (unitVM != null) {
      Bindings.bind(lblUnit, unitVM);
    }
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);

    Component[] comps = getComponents();
    for (Component comp : comps) {
      comp.setEnabled(enabled);
    }
  }

  public void setPercentageVisibility(boolean visible) {
    lblEquals.setVisible(visible);
    ftfHysteresisPercent.setVisible(visible);
    lblPercent.setVisible(visible);
  }

  public void setFullRangeValue(double fullRangeValue) {
    percentVM.setFullRangeValue(fullRangeValue);
  }

  /** Getter method of {@code PROPERTY_DISPLAY_NAME}. */
  public String getDisplayName() {
    return lblHysteresis.getText();
  }

  /** Setter method of {@code PROPERTY_DISPLAY_NAME}. */
  public void setDisplayName(String displayName) {
    String oldValue = getDisplayName();
    lblHysteresis.setText(displayName);
    firePropertyChange(PROPERTY_DISPLAY_NAME, oldValue, displayName);
  }

  public boolean isAllowNegative() {
    return allowNegative;
  }

  public void setAllowNegative(boolean allowNegative) {
    Object oldValue = this.allowNegative;
    this.allowNegative = allowNegative;

    NumberFormatter nf0 = (NumberFormatter) ftfHysteresis.getFormatter();
    NumberFormatter nf1 = (NumberFormatter) ftfHysteresisPercent.getFormatter();
    if (this.allowNegative) {
      nf0.setMinimum(Double.MIN_VALUE);
      nf1.setMinimum(Double.MIN_VALUE);
    } else {
      nf0.setMinimum(Double.valueOf(0d));
      nf1.setMinimum(Double.valueOf(0d));
    }

    firePropertyChange(PROPERTY_ALLOW_NEGATIVE, oldValue, allowNegative);
  }

  private void initComponentsBinding() {
    Bindings.bind(ftfHysteresis, valueVM);

    /**
     * Caution: ConverterValueModel implements ComponentValueModel which will
     * update the enable state of component when it is binded to that component.
     * It is not what we expected since we like control the "enable" state
     * through "setEnable" method. Solution: keep the old enable state before
     * binding then recover it after binding.
     */
    boolean isEnabled = ftfHysteresisPercent.isEnabled();
    Bindings.bind(ftfHysteresisPercent, percentVM);
    ftfHysteresisPercent.setEnabled(isEnabled);
  }

  public boolean isFadingEffectEnabled() {
    return fadeHandler0 != null;
  }

  public void setFadingEffectEnabled(boolean enabled) {
    if (enabled == isFadingEffectEnabled()) {
      return;
    }

    Object oldValue = isFadingEffectEnabled();

    if (enabled) {
      fadeHandler0 = FadingBGValueChangeHandler.install(valueVM, ftfHysteresis);
      fadeHandler1 = FadingBGValueChangeHandler.install(percentVM, ftfHysteresisPercent);
    } else {
      if (fadeHandler0 != null) {
        fadeHandler0.release();
        fadeHandler0 = null;
      }
      if (fadeHandler1 != null) {
        fadeHandler1.release();
        fadeHandler1 = null;
      }
    }

    Object newValue = isFadingEffectEnabled();
    firePropertyChange(PROPERTY_FADING_EFFECT_ENABLED, oldValue, newValue);
  }

  private void initCommitOnType() {
    FormattedTextFieldSupport.installCommitOnType(ftfHysteresis);
    FormattedTextFieldSupport.installCommitOnType(ftfHysteresisPercent);
  }

  private void initComponentAnnotation() {
    setMessageKey(ftfHysteresis, valuePropertyName);
    setMessageKey(ftfHysteresisPercent, valuePropertyName);
  }

  private void createUIComponents() {
    NumberFormatter doubleFormatter = new NumberFormatter();
    doubleFormatter.setValueClass(Double.class);
    ftfHysteresis = new JFormattedTextField(doubleFormatter);

    doubleFormatter = new NumberFormatter();
    doubleFormatter.setValueClass(Double.class);
    ftfHysteresisPercent = new JFormattedTextField(doubleFormatter);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    lblHysteresis = new JLabel();
    lblUnit = new JLabel();
    lblEquals = new JLabel();
    lblPercent = new JLabel();

    // ======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, 35dlu, $lcgap, center:default, $lcgap, default, $rgap, 35dlu, default",
        "fill:default"));

    // ---- lblHysteresis ----
    lblHysteresis.setText("Property:");
    add(lblHysteresis, CC.xy(1, 1, CC.RIGHT, CC.DEFAULT));
    add(ftfHysteresis, CC.xy(3, 1));
    add(lblUnit, CC.xy(5, 1, CC.FILL, CC.DEFAULT));

    // ---- lblEquals ----
    lblEquals.setText("=");
    add(lblEquals, CC.xy(7, 1));
    add(ftfHysteresisPercent, CC.xy(9, 1, CC.FILL, CC.DEFAULT));

    // ---- lblPercent ----
    lblPercent.setText("%");
    lblPercent.setFont(lblPercent.getFont().deriveFont(lblPercent.getFont().getStyle() | Font.ITALIC));
    add(lblPercent, CC.xy(10, 1, CC.LEFT, CC.DEFAULT));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel lblHysteresis;
  private JFormattedTextField ftfHysteresis;
  private JLabel lblUnit;
  private JLabel lblEquals;
  private JFormattedTextField ftfHysteresisPercent;
  private JLabel lblPercent;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Convert value to percent based on the full range value. If full range value
   * is 0, do not do any conversion and percentage will be equivalent to value.
   */
  public static class PercentValueModel extends AbstractValueModel
      implements PropertyChangeListener {

    private final ValueModel valueVM;
    private double fullRangeValue;

    private double percentage;

    private boolean isSetting; // indicate if public setting method is called


    public PercentValueModel(ValueModel valueVM, double fullRangeValue) {
      this.fullRangeValue = fullRangeValue;
      this.valueVM = valueVM;
      valueVM.addValueChangeListener(this);

      Number val = (Number) valueVM.getValue();
      double currentVal = val == null ? 0D : val.doubleValue();
      percentage = fullRangeValue == 0 ? currentVal : currentVal * 100 / fullRangeValue;
    }

    public void setFullRangeValue(double fullRangeVal) {
      isSetting = true;
      fullRangeValue = fullRangeVal;
      valueVM.setValue(percentage * fullRangeValue / 100);
      isSetting = false;
    }

    @Override
    public Object getValue() {
      return percentage;
    }

    @Override
    public void setValue(Object newValue) {
      isSetting = true;
      percentage = ((Number) newValue).doubleValue();

      valueVM.setValue(percentage * fullRangeValue / 100);
      isSetting = false;
    }

    /**
     * Handling the change of value and update percentage.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == null) {
        return;
      }

      if (fullRangeValue != 0 && isSetting == false) {
        double oldPercentage = percentage;
        double newValue = ((Number) evt.getNewValue()).doubleValue();
        percentage = fullRangeValue == 0 ? newValue : newValue * 100 / fullRangeValue;
        firePropertyChange(PROPERTY_VALUE, oldPercentage, percentage);
      }
    }
  }
}
