/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain;

import com.g3schema.ns_vpoint.CounterPointT;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * Interface of standard counter virtual point.
 *
 * @see {@link IStandardPoint}
 * @see {@link CounterPoint}
 */
public interface StdCounterPoint extends IStandardPoint, CounterPoint {

  void readFromXML(CounterPointT xml, VirtualPointReadSupport support);

  void writeToXML(CounterPointT xml, VirtualPointWriteSupport support);
  
  void setSource(VirtualPoint source);
  
  @Override
  VirtualPoint getSource();
}
