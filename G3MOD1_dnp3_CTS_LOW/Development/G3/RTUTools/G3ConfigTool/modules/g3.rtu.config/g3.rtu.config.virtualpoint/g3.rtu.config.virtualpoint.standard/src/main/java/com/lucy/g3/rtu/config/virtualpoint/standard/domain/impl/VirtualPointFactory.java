/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;

/**
 * A factory for creating Standard Virtual Point objects.
 */
public class VirtualPointFactory {

  protected VirtualPointFactory() {
  }

  public static StdBinaryPoint createBinaryPoint() {
    return createBinaryPoint(0, "");
  }

  public static StdBinaryPoint createBinaryPoint(int id, String description) {
    return new StdBinaryPointImpl(id, description);
  }

  public static StdDoubleBinaryPoint createDoubleBinaryPoint() {
    return createDoubleBinaryPoint(0, "");
  }

  public static StdDoubleBinaryPoint createDoubleBinaryPoint(int id, String desp) {
    return new StdDoubleBinaryPointImpl(id, desp);
  }

  public static StdAnaloguePoint createAnalogPoint() {
    return new StdAnaloguePointImpl(0, "");
  }

  public static StdCounterPoint createCounterPoint() {
    return new StdCounterPointImpl(0, "");
  }

  public static IStandardPoint createStanardPoint(VirtualPointType type) {
    Preconditions.checkNotNull(type, "Virtual Point type must not be null");

    IStandardPoint point = null;

    switch (type) {
    case BINARY_INPUT:
      point = VirtualPointFactory.createBinaryPoint();
      break;

    case DOUBLE_BINARY_INPUT:
      point = VirtualPointFactory.createDoubleBinaryPoint();
      break;

    case ANALOGUE_INPUT:
      point = VirtualPointFactory.createAnalogPoint();
      break;

    case COUNTER:
      point = VirtualPointFactory.createCounterPoint();
      break;

    default:
      throw new IllegalArgumentException("Unsupported standard point type: " + type);
    }

    return point;
  }

}
