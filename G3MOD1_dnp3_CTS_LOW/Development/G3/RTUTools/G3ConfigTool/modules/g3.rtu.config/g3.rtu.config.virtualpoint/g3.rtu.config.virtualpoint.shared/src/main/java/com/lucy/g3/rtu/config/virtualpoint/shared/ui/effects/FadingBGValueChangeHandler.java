/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.effects;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;

import com.jgoodies.animation.AbstractAnimation;
import com.jgoodies.animation.AnimationFunction;
import com.jgoodies.animation.AnimationFunctions;
import com.jgoodies.animation.Animator;
import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.common.utils.EqualsUtil;

/**
 * This class is for creating a highlight effect for a group components once the
 * observed value changes.
 */
public class FadingBGValueChangeHandler extends AbstractAnimation implements PropertyChangeListener {

  private static final long FADING_DURATION = 3000;

  private static final Color DEFAULT_HIGHLIGH_COLOR = Color.yellow;

  private final ValueModel valueModel;
  private final JComponent component;
  private final Color originalColor;

  private final AnimationFunction<Color> colorFadeFunc;

  private Animator animator = null;


  public FadingBGValueChangeHandler(ValueModel valueModel, JComponent component) {
    this(valueModel, DEFAULT_HIGHLIGH_COLOR, component);
  }

  public FadingBGValueChangeHandler(ValueModel valueModel, Color highlightColor, JComponent component) {
    super(FADING_DURATION);
    this.valueModel = valueModel;
    this.component = component;
    this.originalColor = component.getBackground();
    this.colorFadeFunc = AnimationFunctions.linearColors(
        FADING_DURATION,
        new Color[] {
            highlightColor,
            originalColor,
        },
        new float[] { 0.0f, 1.0f });

    this.valueModel.addValueChangeListener(this);
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    assert evt.getSource() == valueModel;

    if (EqualsUtil.areEqual(evt.getNewValue(), evt.getOldValue())) {
      // No animation needed cause value not change
      return;
    }

    if (animator != null) {
      animator.stop();
    }
    this.animator = new Animator(this, 30);
    animator.start();
  }

  @Override
  protected void applyEffect(long time) {
    if (time == 0) {
      resetColor();
    } else {
      component.setBackground(colorFadeFunc.valueAt(time));
    }
  }

  public void release() {
    this.valueModel.removeValueChangeListener(this);
  }

  private void resetColor() {
    component.setBackground(originalColor);
  }

  public static FadingBGValueChangeHandler install(ValueModel valueModel, JComponent component) {
    return new FadingBGValueChangeHandler(valueModel, component);
  }
}
