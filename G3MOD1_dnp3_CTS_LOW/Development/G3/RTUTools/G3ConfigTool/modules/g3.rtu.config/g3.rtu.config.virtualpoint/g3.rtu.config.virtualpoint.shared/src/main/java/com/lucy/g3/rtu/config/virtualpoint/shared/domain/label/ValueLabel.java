/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.label;

import com.jgoodies.common.base.Preconditions;

/**
 * The label of virtual point value.
 */
public class ValueLabel {

  private final Number value;
  private String label;
  private final double hashcode;
  private ValueLabelSet owner;


  public ValueLabel(Double value, String label) {
    Preconditions.checkNotNull(value, "value must not be null");
    if (value % 1 == 0) {
      this.value = value.intValue();
    } else {
      this.value = value.doubleValue();
    }

    this.label = label;
    this.hashcode = value.doubleValue();
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
    if(owner != null)
      owner.fireThisLabelSetChangedEvent();
  }

  public Number getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "ValueLabel [value=" + value + ", label=" + label + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(hashcode);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ValueLabel other = (ValueLabel) obj;
    if (Double.doubleToLongBits(hashcode) != Double.doubleToLongBits(other.hashcode)) {
      return false;
    }
    return true;
  }

  
  void setOwner(ValueLabelSet owner) {
    this.owner = owner;
  }

}
