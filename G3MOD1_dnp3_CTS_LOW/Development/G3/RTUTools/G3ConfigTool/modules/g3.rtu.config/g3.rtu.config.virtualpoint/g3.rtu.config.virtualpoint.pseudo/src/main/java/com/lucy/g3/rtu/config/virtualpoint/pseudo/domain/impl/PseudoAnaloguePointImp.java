/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import com.g3schema.ns_clogic.CLogicAnalogPointT;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain PseudoAnaloguePoint}.
 */
public final class PseudoAnaloguePointImp extends AbstractAnaloguePoint
    implements PseudoAnaloguePoint {

  private ILogicPointEnum pointXmlEnum;

  private final IValidator validator;


  PseudoAnaloguePointImp(IPseudoPointSource owner,
      ILogicPointEnum pointXmlEnum) {
    this(pointXmlEnum.getDescription(), owner, pointXmlEnum.getValue());
    this.pointXmlEnum = pointXmlEnum;
  }

  /**
   * Constructor of Analogue Pseudo point.
   *
   * @param description
   *          The name of point.
   * @param groupID
   *          Group ID
   * @param id
   *          Unique ID in the group
   * @param scale
   *          Parameter scale
   * @param offset
   *          Parameter offset
   * @param overflow
   *          Parameter overflow
   */
  PseudoAnaloguePointImp(String description, IPseudoPointSource source, int id) {
    super(DEF_PSEUDO_POINT_GROUP, id, description);
    validator = new PseudoPointValidator(this);
    setSource(source);
    updateGroup();
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }

  @Override
  public void updateGroup() {
    IPseudoPointSource src = (IPseudoPointSource) getSource();
    if (src != null) {
      setGroup(src.getGroup());
    }
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return pointXmlEnum == null ? DefaultCreateOption.DoNotCreatePoint
        : pointXmlEnum.getDefCreateOption();
  }

  // ----------------------[ Unsupported operations ]--------------------------

  @Override
  public String getRawUnit() {
    return null;
  }

  @Override
  public void writeToXML(CLogicAnalogPointT xml) {
    super.writeToXML(xml);
  }

  @Override
  public void readFromXML(CLogicAnalogPointT xml, VirtualPointReadSupport support) {
    super.readFromXML(xml, support);
  }

}
