/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.label;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;

import javax.swing.ListModel;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.common.utils.EqualsUtil;

/**
 * A class for storing a set of {@link ValueLabel}.
 */
public class ValueLabelSet extends Model{
  final static String LABEL_SET_CHANGED = "labelSetChanged";
  
  private final LinkedHashSet<ValueLabel> valueLabels;
  private final ArrayListModel<ValueLabel> valueLabelsListModel;

  private final String id;

  private final ArrayList<IValueLabelSetUser> users = new ArrayList<>();

  private ValueLabelSetManager manager;


  public ValueLabelSet(String id) {
    this.id = id;
    valueLabels = new LinkedHashSet<>();
    valueLabelsListModel = new ArrayListModel<>();
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return id;
  }

  public boolean addLabel(ValueLabel valueLabel) {
    if (valueLabels.add(valueLabel)) {
      valueLabelsListModel.add(valueLabel);
      valueLabel.setOwner(this);
      fireThisLabelSetChangedEvent();
      return true;
    } else {
      return false;
    }
  }

  public ValueLabel addLabel(Double value, String label) {
    ValueLabel newLabel = new ValueLabel(value, label);
    if (addLabel(newLabel)) {
      return newLabel;
    } else {
      return null;
    }
  }

  public boolean removeLabel(ValueLabel valueLabel) {
    if (valueLabels.remove(valueLabel)) {
      valueLabelsListModel.remove(valueLabel);
      valueLabel.setOwner(null);
      fireThisLabelSetChangedEvent();
      return true;
    } else {
      return false;
    }
  }

  public String getLabel(Double value) {
    ValueLabel obj = getLabelObj(value);
    return obj == null ? null : obj.getLabel();
  }
  
  public ValueLabel getLabelObj(Double value) {
    Iterator<ValueLabel> it = valueLabels.iterator();
    final ValueLabel key = new ValueLabel(value, "");
    ValueLabel label;
    while (it.hasNext()) {
      label = it.next();
      if (EqualsUtil.areEqual(key, label)) {
        return label;
      }
    }

    return null;
  }

  public int getSize() {
    return valueLabels.size();
  }

  @SuppressWarnings("unchecked")
  public ListModel<ValueLabel> getValueLabelsListModel() {
    return valueLabelsListModel;
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ValueLabelSet other = (ValueLabelSet) obj;
    if (id == null) {
      if (other.id != null) {
        return false;
      }
    } else if (!id.equals(other.id)) {
      return false;
    }
    return true;
  }

  public ArrayList<IValueLabelSetUser> getUsers() {
    return new ArrayList<>(users);
  }

  void addUser(IValueLabelSetUser user) {
    users.add(user);
  }

  void removeUser(IValueLabelSetUser user) {
    users.remove(user);
  }

  public boolean hasUsers() {
    return !users.isEmpty();
  }

  public ValueLabelSetManager getManager() {
    return manager;
  }

  void setManager(ValueLabelSetManager manager) {
    this.manager = manager;
  }

  public Collection<ValueLabel> getAllLabelEntries() {
    return new ArrayList<ValueLabel>(valueLabelsListModel);
  }

  public HashMap<Double, String> convertToMap() {
    HashMap<Double, String> map = new HashMap<>();
    Collection<ValueLabel> entries = getAllLabelEntries();
    for (ValueLabel entry : entries) {
      map.put(entry.getValue().doubleValue(), entry.getLabel());
    }
    return map;
  }
  
  void fireThisLabelSetChangedEvent(){
    firePropertyChange(LABEL_SET_CHANGED, null, this);
  }
}
