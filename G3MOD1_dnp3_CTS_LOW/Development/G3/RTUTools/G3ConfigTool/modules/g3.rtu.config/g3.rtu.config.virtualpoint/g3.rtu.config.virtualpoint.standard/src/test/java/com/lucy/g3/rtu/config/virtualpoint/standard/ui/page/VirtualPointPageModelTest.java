/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.page;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.VirtualPointPlugin;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.VirtualPointManagerImpl;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.VirtualPointPageModel;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class VirtualPointPageModelTest.
 */
public class VirtualPointPageModelTest {

  private VirtualPointPageModel pm;

  private VirtualPointManagerImpl manager;

  @Before
  public void setUp() throws Exception {
    VirtualPointPlugin.init();
    manager = new VirtualPointManagerImpl(null);
    pm = new VirtualPointPageModel(manager, VirtualPointType.ANALOGUE_INPUT);
  }

  @After
  public void tearDown() throws Exception {
    VirtualPointPlugin.shutdown();
  }

  @Test(expected = NullPointerException.class)
  public void testConstrator0() {
    new VirtualPointPageModel(null, VirtualPointType.ANALOGUE_INPUT);
  }

  @Test(expected = NullPointerException.class)
  public void testConstrator1() {
    new VirtualPointPageModel(null, null);
  }

  @Test(expected = NullPointerException.class)
  public void testConstrator2() {
    new VirtualPointPageModel(null, VirtualPointType.ANALOGUE_INPUT);
  }

  @Test
  public void testConstrator3() {
    new VirtualPointPageModel(manager, VirtualPointType.ANALOGUE_INPUT);
  }

  @Test
  public void testGetAction() {
    String[] actionKeys = {
        VirtualPointPageModel.ACTION_KEY_ADD,
        VirtualPointPageModel.ACTION_KEY_DUPLICATE,
        VirtualPointPageModel.ACTION_KEY_EDIT,
        VirtualPointPageModel.ACTION_KEY_INSERT,
        VirtualPointPageModel.ACTION_KEY_MOVE_DOWN,
        VirtualPointPageModel.ACTION_KEY_MOVE_UP,
        VirtualPointPageModel.ACTION_KEY_REMOVE,
    };

    for (String key : actionKeys) {
      assertNotNull("Action is null for key:" + key, pm.getAction(key));
    }
  }

  @Test
  public void testBeanProperties() {
    TestUtil.testReadOnlyProperties(pm, new String[] { VirtualPointPageModel.PROPERTY_EDITITABLE });
  }
}
