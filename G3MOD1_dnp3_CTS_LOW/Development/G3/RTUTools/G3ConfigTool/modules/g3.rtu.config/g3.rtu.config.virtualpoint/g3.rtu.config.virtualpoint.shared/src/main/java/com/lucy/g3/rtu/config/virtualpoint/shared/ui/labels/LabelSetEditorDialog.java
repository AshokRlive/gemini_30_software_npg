/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.labels;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellEditor;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;

/**
 * Dialog for editing {@linkplain ValueLabelSet}.
 */
public final class LabelSetEditorDialog extends AbstractDialog {

  public static final String PROPERTY_SELECTION_NOT_EMPTY = "selectionNotEmpty";

  public static final String ACTION_ADD = "add";
  public static final String ACTION_REMOVE = "remove";

  private final ValueLabelSet target;
  private final SelectionInList<ValueLabel> valueLabelSelections;


  public LabelSetEditorDialog(Frame owner, ValueLabelSet target) {
    super(owner);
    this.target = Preconditions.checkNotNull(target, "target must not be null");
    this.valueLabelSelections = new SelectionInList<>(target.getValueLabelsListModel());
    init();
  }

  public LabelSetEditorDialog(Dialog owner, ValueLabelSet target) {
    super(owner, true);
    this.target = Preconditions.checkNotNull(target, "target must not be null");
    this.valueLabelSelections = new SelectionInList<>(target.getValueLabelsListModel());
    init();
  }

  private void init() {
    setTitle("Editing Label Set");
    initComponents();
    initComponentsBinding();
    pack();
  }

  private void clearSelection() {
    TableCellEditor activeEditor = valueLabelTable.getCellEditor();
    if (activeEditor != null) {
      activeEditor.stopCellEditing();
    }
    valueLabelSelections.clearSelection();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    panel2 = new JPanel();
    label2 = new JLabel();
    lblSetName = new JLabel();
    scrollPane1 = new JScrollPane();
    valueLabelTable = new JTable();
    controlPanel = new JPanel();
    btnAdd = new JButton();
    btnRemove = new JButton();

    // ======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "default, $lcgap, default",
          "default, $pgap, fill:default"));

      // ======== panel2 ========
      {
        panel2.setLayout(new FormLayout(
            "default, [100dlu,default]:grow",
            "default"));

        // ---- label2 ----
        label2.setText("Label Set Name:");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        panel2.add(label2, CC.xy(1, 1));
        panel2.add(lblSetName, CC.xy(2, 1));
      }
      contentPanel.add(panel2, CC.xy(1, 1));

      // ======== scrollPane1 ========
      {
        scrollPane1.setBorder(new TitledBorder("All Labels"));
        scrollPane1.setPreferredSize(new Dimension(200, 200));
        scrollPane1.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            clearSelection();
          }
        });
        scrollPane1.setViewportView(valueLabelTable);
      }
      contentPanel.add(scrollPane1, CC.xy(1, 3));

      // ======== controlPanel ========
      {
        controlPanel.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            clearSelection();
          }
        });
        controlPanel.setLayout(new FormLayout(
            "default",
            "2*(default, $lgap), fill:default:grow"));

        // ---- btnAdd ----
        btnAdd.setText("Add");
        controlPanel.add(btnAdd, CC.xy(1, 1));

        // ---- btnRemove ----
        btnRemove.setText("Remove");
        controlPanel.add(btnRemove, CC.xy(1, 3));
      }
      contentPanel.add(controlPanel, CC.xy(3, 3));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  private void initComponentsBinding() {
    lblSetName.setText("\" " + target.getName() + "\" ");

    valueLabelTable.setModel(new ValueLabelTableModel(target.getValueLabelsListModel()));
    Bindings.bind(valueLabelTable, valueLabelSelections);

    // Action binding
    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(this);
    btnAdd.setAction(actions.get(ACTION_ADD));
    btnRemove.setAction(actions.get(ACTION_REMOVE));

    // Observe selection action
    valueLabelSelections.addPropertyChangeListener(SelectionInList.PROPERTY_SELECTION_EMPTY,
        new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            LabelSetEditorDialog.this.firePropertyChange(PROPERTY_SELECTION_NOT_EMPTY,
                !(Boolean) evt.getOldValue(), !(Boolean) evt.getNewValue());
          }
        });

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JPanel panel2;
  private JLabel label2;
  private JLabel lblSetName;
  private JScrollPane scrollPane1;
  private JTable valueLabelTable;
  private JPanel controlPanel;
  private JButton btnAdd;
  private JButton btnRemove;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createCloseButtonPanel();
  }

  @Action
  public void add() {
    final JTextField inputValue = new JTextField(10);
    final JTextField inputLabel = new JTextField(10);
    JLabel tip0 = new JLabel("(Must be a number, e.g. \"0\", \"1\",etc)");
    JLabel tip1 = new JLabel("(Any text, e.g. \"Open\", \"Close\", etc)");
    tip0.setForeground(Color.gray);
    tip1.setForeground(Color.gray);

    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout("default, 2*($lcgap,default)", ""));
    builder.append("Value:", inputValue, tip0);
    builder.nextLine();
    builder.append("Label:", inputLabel, tip1);

    JPanel inputPanel = builder.getPanel();

    while (true) {
      int ret = JOptionPane.showConfirmDialog(this, inputPanel, "New Value Label", JOptionPane.OK_CANCEL_OPTION);
      if (ret != JOptionPane.YES_OPTION) {
        break; // User cancelled
      }

      try {
        double value = Double.parseDouble(inputValue.getText());
        if (target.addLabel(new ValueLabel(value, inputLabel.getText()))) {
          break; // Success
        } else {
          JOptionPane.showMessageDialog(this,
              String.format("Value: \"%s\" already exists!", inputValue.getText()),
              "Failure", JOptionPane.ERROR_MESSAGE);
        }

      } catch (Throwable e) {
        JOptionPane.showMessageDialog(this,
            "Please input a numeric value!",
            "Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  @Action(enabledProperty = PROPERTY_SELECTION_NOT_EMPTY)
  public void remove() {
    int ret = JOptionPane.showConfirmDialog(this,
        "Do you want to remove the selection?",
        "Remove", JOptionPane.YES_NO_OPTION);
    if (ret == JOptionPane.YES_OPTION) {
      target.removeLabel(valueLabelSelections.getSelection());
    }
  }

  /** The getter method of property {@value #PROPERTY_SELECTION_NOT_EMPTY}. */
  public boolean isSelectionNotEmpty() {
    return valueLabelSelections.hasSelection();
  }


  private static class ValueLabelTableModel extends AbstractTableAdapter<ValueLabel> {

    private static final String[] COLUMN_NAMES = { "Value", "Label" };

    private static final int COLUMN_VALUE = 0;
    private static final int COLUMN_LABEL = 1;

    private final DecimalFormat valueFormat;


    public ValueLabelTableModel(ListModel<ValueLabel> listModel) {
      super(listModel, COLUMN_NAMES);
      valueFormat = new DecimalFormat("###.#####");
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      ValueLabel valueLabel = getRow(rowIndex);
      switch (columnIndex) {
      case COLUMN_VALUE:
        return valueFormat.format(valueLabel.getValue());
      case COLUMN_LABEL:
        return valueLabel.getLabel();
      default:
        break;
      }
      return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex == COLUMN_LABEL;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      if (columnIndex == COLUMN_LABEL) {
        getRow(rowIndex).setLabel(String.valueOf(aValue));
      }
    }

  }

}
