/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.xml;

import com.g3schema.ns_common.ChannelRefT;
import com.g3schema.ns_common.VirtualPointRefT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * This interface specifies the resource required when writing Virtual Point
 * into XML.
 */
public interface VirtualPointWriteSupport {

  void writeChannelRef(String source, ChannelRefT xml, IChannel ch);

  void writeVirtualPointRef(String source, VirtualPointRefT xml, VirtualPoint point);

  ValidationResult getResult();
}
