/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import com.g3schema.ns_clogic.CLogicCounterPointT;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.IPseudoPointSource;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.AbstractCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;
import com.lucy.g3.xml.gen.api.ILogicPointEnum;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

/**
 * Implementation of {@linkplain PseudoCounterPoint}.
 */
public final class PseudoCounterPointImpl extends AbstractCounterPoint implements PseudoCounterPoint {

  private ILogicPointEnum pointXmlEnum;

  private final IValidator validator;


  PseudoCounterPointImpl(String description, IPseudoPointSource source, int id) {
    super(DEF_PSEUDO_POINT_GROUP, id, description);
    validator = new PseudoPointValidator(this);
    setSource(source);
    updateGroup();
  }

  PseudoCounterPointImpl(IPseudoPointSource owner, ILogicPointEnum pointXml) {
    this(pointXml.getDescription(), owner, pointXml.getValue());
    pointXmlEnum = pointXml;
  }

  @Override
  public DefaultCreateOption getDefaultCreateOption() {
    return pointXmlEnum == null ? DefaultCreateOption.DoNotCreatePoint
        : pointXmlEnum.getDefCreateOption();
  }

  @Override
  public String getHTMLSummary() {
    StringBuffer html = new StringBuffer();

    html.append("<html>");
    html.append("<b>Point: " + getName() + "</b>");
    html.append("<ul>");
    html.append("<li>Type: " + type.getName() + "</li>");
    // html.append("<li>Invert: "+isInvert()+"</li>");
    // html.append("<li>ChatterTime: "+getChatterTime()+" ms</li>");
    // html.append("<li>ChatterNo: "+getChatterNo()+"</li>");
    html.append("</ul>");
    html.append("</html>");
    return html.toString();
  }

  @Override
  public void updateGroup() {
    IPseudoPointSource src = (IPseudoPointSource) getSource();
    if (src != null) {
      setGroup(src.getGroup());
    }
  }

  @Override
  public void writeToXML(CLogicCounterPointT xml) {
    super.writeToXML(xml);
  }

  @Override
  public void readFromXML(CLogicCounterPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);
  }

  @Override
  public IValidator getValidator() {
    return validator;
  }


}
