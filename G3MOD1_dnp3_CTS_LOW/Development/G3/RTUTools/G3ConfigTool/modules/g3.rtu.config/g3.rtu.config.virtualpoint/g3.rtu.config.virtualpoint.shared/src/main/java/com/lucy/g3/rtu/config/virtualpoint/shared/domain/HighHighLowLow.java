/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import com.g3schema.ns_vpoint.HiHiLoLoT;
import com.jgoodies.binding.beans.Model;

/**
 * The bean for configuring High High Low Low filter.
 */
public final class HighHighLowLow extends Model {

  public static final String PROPERTY_HIHI_ENABLED = "hihiEnabled";
  public static final String PROPERTY_HI_ENABLED = "hiEnabled";
  public static final String PROPERTY_LO_ENABLED = "loEnabled";
  public static final String PROPERTY_LOLO_ENABLED = "loloEnabled";

  public static final String PROPERTY_HIHI_THRESHOLD = "hihiThreshold";
  public static final String PROPERTY_HI_THRESHOLD = "hiThreshold";
  public static final String PROPERTY_LO_THRESHOLD = "loThreshold";
  public static final String PROPERTY_LOLO_THRESHOLD = "loloThreshold";

  public static final String PROPERTY_HIHI_HYSTERESIS = "hihiHysteresis";
  public static final String PROPERTY_HI_HYSTERESIS = "hiHysteresis";
  public static final String PROPERTY_LO_HYSTERESIS = "loHysteresis";
  public static final String PROPERTY_LOLO_HYSTERESIS = "loloHysteresis";

  private boolean hihiEnabled = false;
  private boolean hiEnabled = false;
  private boolean loEnabled = false;
  private boolean loloEnabled = false;

  private double hihiThreshold = 0;
  private double hiThreshold = 0;
  private double loThreshold = 0;
  private double loloThreshold = 0;

  private double hihiHysteresis = 0;
  private double hiHysteresis = 0;
  private double loHysteresis = 0;
  private double loloHysteresis = 0;


  public boolean isHihiEnabled() {
    return hihiEnabled;
  }

  public void setHihiEnabled(boolean hihiEnabled) {
    Object oldValue = isHihiEnabled();
    this.hihiEnabled = hihiEnabled;
    firePropertyChange(PROPERTY_HIHI_ENABLED, oldValue, hihiEnabled);
  }

  public boolean isHiEnabled() {
    return hiEnabled;
  }

  public void setHiEnabled(boolean hiEnabled) {
    Object oldValue = isHiEnabled();
    this.hiEnabled = hiEnabled;
    firePropertyChange(PROPERTY_HI_ENABLED, oldValue, hiEnabled);
  }

  public boolean isLoEnabled() {
    return loEnabled;
  }

  public void setLoEnabled(boolean loEnabled) {
    Object oldValue = isLoEnabled();
    this.loEnabled = loEnabled;
    firePropertyChange(PROPERTY_LO_ENABLED, oldValue, loEnabled);
  }

  public boolean isLoloEnabled() {
    return loloEnabled;
  }

  public void setLoloEnabled(boolean loloEnabled) {
    Object oldValue = isLoloEnabled();
    this.loloEnabled = loloEnabled;
    firePropertyChange(PROPERTY_LOLO_ENABLED, oldValue, loloEnabled);
  }

  public double getHihiThreshold() {
    return hihiThreshold;
  }

  public void setHihiThreshold(double hihiThreshold) {
    Object oldValue = getHihiThreshold();
    this.hihiThreshold = hihiThreshold;
    firePropertyChange(PROPERTY_HIHI_THRESHOLD, oldValue, hihiThreshold);
  }

  public double getHiThreshold() {
    return hiThreshold;
  }

  public void setHiThreshold(double hiThreshold) {
    Object oldValue = getHiThreshold();
    this.hiThreshold = hiThreshold;
    firePropertyChange(PROPERTY_HI_THRESHOLD, oldValue, hiThreshold);
  }

  public double getLoThreshold() {
    return loThreshold;
  }

  public void setLoThreshold(double loThreshold) {
    Object oldValue = getLoThreshold();
    this.loThreshold = loThreshold;
    firePropertyChange(PROPERTY_LO_THRESHOLD, oldValue, loThreshold);
  }

  public double getLoloThreshold() {
    return loloThreshold;
  }

  public void setLoloThreshold(double loloThreshold) {
    Object oldValue = getLoloThreshold();
    this.loloThreshold = loloThreshold;
    firePropertyChange(PROPERTY_LOLO_THRESHOLD, oldValue, loloThreshold);
  }

  public double getHihiHysteresis() {
    return hihiHysteresis;
  }

  public void setHihiHysteresis(double hihiHysteresis) {
    Object oldValue = getHihiHysteresis();
    this.hihiHysteresis = hihiHysteresis;
    firePropertyChange(PROPERTY_HIHI_HYSTERESIS, oldValue, hihiHysteresis);
  }

  public double getHiHysteresis() {
    return hiHysteresis;
  }

  public void setHiHysteresis(double hiHysteresis) {
    Object oldValue = getHiHysteresis();
    this.hiHysteresis = hiHysteresis;
    firePropertyChange(PROPERTY_HI_HYSTERESIS, oldValue, hiHysteresis);
  }

  public double getLoHysteresis() {
    return loHysteresis;
  }

  public void setLoHysteresis(double loHysteresis) {
    Object oldValue = getLoHysteresis();
    this.loHysteresis = loHysteresis;
    firePropertyChange(PROPERTY_LO_HYSTERESIS, oldValue, loHysteresis);
  }

  public double getLoloHysteresis() {
    return loloHysteresis;
  }

  public void setLoloHysteresis(double loloHysteresis) {
    Object oldValue = getLoloHysteresis();
    this.loloHysteresis = loloHysteresis;
    firePropertyChange(PROPERTY_LOLO_HYSTERESIS, oldValue, loloHysteresis);
  }

  public String getFilterParams() {
    StringBuilder filterParams = new StringBuilder();

    boolean isFirst = true;
    if (isHihiEnabled()) {
      if (!isFirst) {
        filterParams.append(", ");
      }
      filterParams.append("hihi: ");
      filterParams.append(getHihiThreshold());
      isFirst = false;

    }
    if (isHiEnabled()) {
      if (!isFirst) {
        filterParams.append(", ");
      }
      filterParams.append("hi: ");
      filterParams.append(getHiThreshold());
      isFirst = false;
    }
    if (isLoEnabled()) {
      if (!isFirst) {
        filterParams.append(", ");
      }
      filterParams.append("lo:");
      filterParams.append(getLoThreshold());
      isFirst = false;
    }
    if (isLoloEnabled()) {
      if (!isFirst) {
        filterParams.append(", ");
      }
      filterParams.append("lolo: ");
      filterParams.append(getLoloThreshold());
      isFirst = false;
    }

    return filterParams.toString();
  }

  public void copyFrom(HighHighLowLow from) {
    setHihiThreshold(from.getHihiThreshold());
    setHihiEnabled(from.isHihiEnabled());

    setHiThreshold(from.getHiThreshold());
    setHiEnabled(from.isHiEnabled());

    setLoThreshold(from.getLoThreshold());
    setLoEnabled(from.isLoEnabled());

    setLoloThreshold(from.getLoloThreshold());
    setLoloEnabled(from.isLoloEnabled());
  }

  public void readParamFromXML(HiHiLoLoT xml) {
    setHihiEnabled(xml.hihiEnabled.getValue());
    setHiEnabled(xml.hiEnabled.getValue());
    setLoEnabled(xml.loEnabled.getValue());
    setLoloEnabled(xml.loloEnabled.getValue());

    setHihiThreshold(xml.hihiThreshold.getValue());
    setHiThreshold(xml.hiThreshold.getValue());
    setLoThreshold(xml.loThreshold.getValue());
    setLoloThreshold(xml.loloThreshold.getValue());

    setHihiHysteresis(xml.hihiHysteresis.getValue());
    setHiHysteresis(xml.hiHysteresis.getValue());
    setLoHysteresis(xml.loHysteresis.getValue());
    setLoloHysteresis(xml.loloHysteresis.getValue());

  }

  public void writeParamToXML(HiHiLoLoT xml) {
    xml.hihiEnabled.setValue(isHihiEnabled());
    xml.hiEnabled.setValue(isHiEnabled());
    xml.loEnabled.setValue(isLoEnabled());
    xml.loloEnabled.setValue(isLoloEnabled());

    xml.hihiThreshold.setValue(getHihiThreshold());
    xml.hiThreshold.setValue(getHiThreshold());
    xml.loThreshold.setValue(getLoThreshold());
    xml.loloThreshold.setValue(getLoloThreshold());

    xml.hihiHysteresis.setValue(getHihiHysteresis());
    xml.hiHysteresis.setValue(getHiHysteresis());
    xml.loHysteresis.setValue(getLoHysteresis());
    xml.loloHysteresis.setValue(getLoloHysteresis());
  }

}
