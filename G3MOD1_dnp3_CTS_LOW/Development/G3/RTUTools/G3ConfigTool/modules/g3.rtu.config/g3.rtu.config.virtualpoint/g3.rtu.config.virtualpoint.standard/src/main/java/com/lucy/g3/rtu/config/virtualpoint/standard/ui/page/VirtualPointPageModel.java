/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.page;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.jgoodies.common.collect.ObservableList;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.dialogs.SelectionDialog;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionSingleSelection;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.module.iomodule.manager.ChannelRepository;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialogInvoker;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.BinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.CounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.IVirtualPointDialogResource;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor.VirtualPointDialog;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.rtu.config.virtualpointsource.ui.VirtualPointSourceSelector;

/**
 * A presentation model that manages a list of virtual points. It provides list model that adapts to Swing JList and
 * JTable components, and also actions to manage the point list, e.g. Add, Remove, Duplicate, etc.
 */
public class VirtualPointPageModel extends Bean {

  public static final String ACTION_KEY_ADD = "addPoints";
  public static final String ACTION_KEY_INSERT = "insertPoints";
  public static final String ACTION_KEY_EDIT = "editSelection";
  public static final String ACTION_KEY_REMOVE = "removeSelection";
  public static final String ACTION_KEY_DUPLICATE = "duplicateSelection";
  public static final String ACTION_KEY_MOVE_UP = "moveSelectionUp";
  public static final String ACTION_KEY_MOVE_DOWN = "moveSelectionDown";

  /** The name of property which indicates if the selected point can be edit. */
  public static final String PROPERTY_EDITITABLE = "editable";

  /** The name of read-only property {@value} . Type: String. */
  public static final String PROPERTY_POINT_NUM_TEXT = "pointNumText";

  public static final String PROPERTY_SELECTION_NAME = "selectionName";

  /**
   * The name of property which indicates if the selected points can be removed.
   */
  private static final String PROPERTY_REMOVEABLE = "removeable";

  private static final String NOT_CONFIGURED_TEXT = "- Not Configured -";
  private static final String UNIT_MS = " ms";
  private static final String UNIT_SEC = " seconds";

  private Logger log = Logger.getLogger(VirtualPointPageModel.class);

  /** The selection list of virtual points,which serves the selection model. */
  private final MultiSelectionInList<VirtualPoint> pointsList;

  /** The holder of the single selected virtual point. */
  private final MultiSelectionSingleSelection singleSelection;

  /** The selection model of virtual points. */
  private final MultiListSelectionAdapter<VirtualPoint> selectionModel;

  /** The virtual point type. */
  private final VirtualPointType type;

  private final AbstractTableAdapter<?> pointsTableModel;

  private final EditorDialogResource resource;

  private final JFrame parent = WindowUtils.getMainFrame();

  private int addedItemIndex0;
  private int addedItemIndex1;

  private final VirtualPointManager manager;
  
  private final IVirtualPointGenerator generator;
  /**
   * Construct a point manage model with provided list model and point type.
   *
   * @param pointsList
   *          the source list to be converted into model
   * @param type
   *          specify point type class.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  VirtualPointPageModel(VirtualPointManager manager, VirtualPointType type) {

    this.type = Preconditions.checkNotNull(type, "Point type  is null");
    this.manager = manager;
    this.generator 
    = ConfigGeneratorRegistry.getInstance().getGenerator(manager.getOwner(), IVirtualPointGenerator.class); 
    
    /* Initialise models */
    ListModel pointsModel = manager.getPointsModel(type);

    pointsList = new MultiSelectionInList<VirtualPoint>(pointsModel);

    selectionModel = new MultiListSelectionAdapter<VirtualPoint>(pointsList);
    selectionModel.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

    singleSelection =
        new MultiSelectionSingleSelection(pointsList.getSelection());

    pointsList.getList().addListDataListener(new ListDataListener() {

      /* Keep an record of new added point index */
      @Override
      public void intervalAdded(ListDataEvent e) {
        addedItemIndex0 =
            addedItemIndex0 >= 0 ? Math.min(addedItemIndex0, e.getIndex0())
                : e.getIndex0();
        addedItemIndex1 =
            addedItemIndex1 >= 0 ? Math.max(addedItemIndex1, e.getIndex1())
                : e.getIndex1();
      }

      @Override
      public void contentsChanged(ListDataEvent e) {
      }

      @Override
      public void intervalRemoved(ListDataEvent e) {
      }
    });

    /* Create virtual point table model */
    switch (type) {
    case BINARY_INPUT:
      pointsTableModel = new BinaryTableAdapter(pointsModel);
      break;

    case DOUBLE_BINARY_INPUT:
      pointsTableModel = new DoubleBinaryTableAdapter(pointsModel);
      break;

    case ANALOGUE_INPUT:
      pointsTableModel = new AnalogueTableAdapter(pointsModel);
      break;

    case COUNTER:
      pointsTableModel = new CounterTableAdapter(pointsModel);
      break;
    default:
      throw new IllegalArgumentException("Unsupported point type: " + type);
    }

    /* Initialise EventHandling */
    pointsList.addPropertyChangeListener(
        MultiSelectionInList.PROPERTY_SELECTION,
        new VirtualPointSelectionHandler());

    pointsList.getList().addListDataListener(new VirtualPointListListener());

    resource = new EditorDialogResource();

  }

  /**
   * Bean getter method. Check if the selected point can be edit.
   */
  public boolean isEditable() {
    // return pointSelectList.getSelection() != null;
    return singleSelection.getValue() != null;
  }

  /**
   * Bean getter method. Checks if there are points that can be removed.
   */
  public boolean isRemoveable() {
    return pointsList.getSelection().size() > 0;
  }

  public VirtualPoint getSelection() {
    return (VirtualPoint) singleSelection.getValue();
  }

  public String getSelectionName() {
    VirtualPoint sel = getSelection();
    return sel == null ? null : sel.getName();
  }

  public VirtualPoint[] getSelectedItems() {
    ObservableList<VirtualPoint> sels = pointsList.getSelection();
    return sels.toArray(new VirtualPoint[sels.size()]);
  }

  public int getItemsCount() {
    return pointsList.getList().getSize();
  }

  // Getter
  public String getPointNumText() {
    return String.format("Points Num:%d", getItemsCount());
  }

  public ListSelectionModel getSelectionModel() {
    return selectionModel;
  }

  public ListModel<VirtualPoint> getListModel() {
    return pointsList.getList();
  }

  public TableModel getTableModel() {
    return pointsTableModel;
  }

  /**
   * Clear the selection.
   */
  public void clearSelection() {
    selectionModel.clearSelection();
  }

  /**
   * Gets the name of this presentation model.
   */
  public String getModelName() {
    return type.getName();
  }

  public javax.swing.Action getAction(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");

    ApplicationActionMap actionMap =
        Application.getInstance().getContext().getActionMap(this);
    javax.swing.Action action = actionMap.get(actionKey);

    if (action != null) {
      action = new BusyCursorAction(action, parent);
    } else {
      throw new IllegalArgumentException("Action is not available for the key:"
          + actionKey);
    }

    return action;
  }

  /**
   * Action method for insert multiple points.
   */
  @Action(enabledProperty = PROPERTY_EDITITABLE)
  public void insertPoints() {
    int index = selectionModel.getAnchorSelectionIndex(); // Get insert position
    createAndAddPoints(index);
  }

  /**
   * Action method for adding multiple points.
   */
  @Action
  public void addPoints() {
    createAndAddPoints(-1); // Append points at the end of point list
  }

  private void createAndAddPoints(int position) {
    // Reset index
    addedItemIndex0 = -1;
    addedItemIndex1 = -1;

    ArrayList<IStandardPoint> createdPoints = new ArrayList<IStandardPoint>();

    // Create counters
    if (type == VirtualPointType.COUNTER) {
      // Select source points for generating counters
      Collection<VirtualPoint> allpoints =
          manager.getAllPointsByType(CounterPoint.SOURCE_TYPE);
      VirtualPointSelector ptsSelector = new VirtualPointSelector(parent, null, allpoints);
      ptsSelector.setSelectionMode(SelectionMode.SELECTION_MODE_MULTI);
      ptsSelector.setSubtitle("Select binary points to create counters for them");
      ptsSelector.showDialog();
      Collection<VirtualPoint> selectedpts = ptsSelector.getSelectedPoints();

      // Generate counters from source points
      StdCounterPoint counter;
      for (VirtualPoint vp : selectedpts) {
        counter = VirtualPointFactory.createCounterPoint();
        counter.setSource(vp);
        counter.setDescription("Counter - " + vp.getDescription());
        createdPoints.add(counter);
      }

      // Create non-counter points
    } else {
      // Get existing channels
      Collection<IChannel> channels;
      if (type == VirtualPointType.ANALOGUE_INPUT) {
        channels = getAllChannels(ChannelType.ANALOG_INPUT);
      } else {
        channels = getAllChannels(ChannelType.DIGITAL_INPUT);
      }

      // Show dialog to select channels
      VirtualPointSourceSelector chlSelector =
          new VirtualPointSourceSelector(parent, null,
              new ArrayList<IVirtualPointSource>(channels), null, "");
      chlSelector.setSelectionMode(SelectionMode.SELECTION_MODE_MULTI);

      if (type == VirtualPointType.DOUBLE_BINARY_INPUT) {
        // Customise dialog title for double points.
        chlSelector.setMaxSelectionNum(2); // Two channels needed for double
        // binary points
        chlSelector.setSubtitle("Select two channels to create a double binary point");
      } else {
        chlSelector.setSubtitle("Select channels to create a list of virtual points");
      }
      chlSelector.showDialog();
      Collection<IVirtualPointSource> selectedChs =
          chlSelector.getSelections();

      // Create points from channels
      switch (type) {
      case ANALOGUE_INPUT:
      case BINARY_INPUT:
        for (IVirtualPointSource ch : selectedChs) {
          createdPoints.add(generator.createStandardVirtualPoint((IChannel) ch));
        }
        break;

      case DOUBLE_BINARY_INPUT:
        if (selectedChs.size() == 2) {
          StdDoubleBinaryPoint doublepoint =
              VirtualPointFactory.createDoubleBinaryPoint();
          doublepoint.setChannel((IChannel) selectedChs.toArray()[0]);
          doublepoint.setSecondChannel((IChannel) selectedChs.toArray()[1]);
          createdPoints.add(doublepoint);

          // Ask user to type point description
          String description = JOptionPane.showInputDialog(null,
              "Please input a description for the new double point:",
              "Input Description",
              JOptionPane.QUESTION_MESSAGE);
          doublepoint.setDescription(description);
        } else if (chlSelector.hasBeenCanceled() == false) {
          JOptionPane.showMessageDialog(null,
              "Please select two channels to create a double point",
              "No selection",
              JOptionPane.ERROR_MESSAGE);
        }
        break;

      default:
        log.error("Unsupported point type:" + type);
        break;
      }
    }

    // Add created points to manager
    for (IStandardPoint p : createdPoints) {
      manager.addPoint(p, position);
      if (position >= 0) {
        position++;
      }
    }

    // Select new created points
    selectionModel.clearSelection();
    if (!createdPoints.isEmpty() && addedItemIndex0 >= 0
        && addedItemIndex1 >= 0) {
      selectionModel.setSelectionInterval(addedItemIndex0, addedItemIndex1);
    }
  }
  
  private Collection<IChannel> getAllChannels(ChannelType... type) {
    ChannelRepository repo = manager.getOwner().getConfigModule(ChannelRepository.CONFIG_MODULE_ID);
    return repo.getAllChannels(type);
  }
  
  @Action(enabledProperty = PROPERTY_EDITITABLE)
  public void editSelection() {
    int selMin = selectionModel.getMinSelectionIndex();
    int selMax = selectionModel.getMaxSelectionIndex();
    if (selMin >= 0 && selMin == selMax) {
      edit(selMin);
    }
  }

  private void edit(int index) {
    try {
      switch (type) {
      case ANALOGUE_INPUT:
        VirtualPointDialog.editAnalog(parent, resource, new EditorDialogInvoker<AnaloguePoint>(index));
        break;
      case BINARY_INPUT:
        VirtualPointDialog.editSingleBinary(parent, resource, new EditorDialogInvoker<BinaryPoint>(index));
        break;
      case DOUBLE_BINARY_INPUT:
        VirtualPointDialog.editDoubleBinary(parent, resource, new EditorDialogInvoker<DoubleBinaryPoint>(index));
        break;
      case COUNTER:
        VirtualPointDialog.editCounter(parent, resource, new EditorDialogInvoker<CounterPoint>(index));
        break;
      default:
        throw new UnsupportedOperationException("Editor dialog not found for: " + type);
      }
    } catch (Exception e) {
      log.error("Fail to open editor dialog", e);
      JOptionPane.showMessageDialog(parent, "Fail to open editor dialog",
          "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  @Action(enabledProperty = PROPERTY_REMOVEABLE)
  public void removeSelection() {
    /* Delete selected item */
    VirtualPoint[] initialSelections = getSelectedItems();
    Collection<VirtualPoint> all = manager.getAllPointsByType(type);
    Collection<VirtualPoint> selected = SelectionDialog.showRemoveDialog(parent, all.toArray(new VirtualPoint[all.size()]), initialSelections);
    if(selected == null)
      return; // User cancelled.
    
    ArrayList<IStandardPoint> removablePoints = findRemovablePoints(selected);
    List<VirtualPoint> unremovedPoints = new ArrayList<>(selected);
    unremovedPoints.removeAll(removablePoints);
    manager.removeAllPoints(removablePoints);
    if (!unremovedPoints.isEmpty()) {
      MessageDialogs.list(parent, JOptionPane.ERROR_MESSAGE, "Failure", 
          "Those points cannot not be removed! You may need to remove related Control Logic.", 
          unremovedPoints);
    }
  }

  private ArrayList<IStandardPoint> findRemovablePoints(Collection<VirtualPoint> selectPoints) {
    ArrayList<IStandardPoint> removablePoints = new ArrayList<>();
    for (VirtualPoint p : selectPoints) {
      if (p != null && p instanceof IStandardPoint) {
        removablePoints.add((IStandardPoint) p);
      }
    }
    
    return removablePoints;
  }

  @Action(enabledProperty = PROPERTY_EDITITABLE)
  public void duplicateSelection() {
    VirtualPoint point = getSelection();
    if (point == null) {
      log.error("Cannot duplicate point, no selection");
      return;
    }

    if (!(point instanceof IStandardPoint)) {
      log.error("Cannot duplicate point, the selection is not a standard point");
      JOptionPane.showMessageDialog(parent,
          "Cannot duplicate control logic point!",
          "Fail", JOptionPane.ERROR_MESSAGE);
      return;
    }

    IStandardPoint stdPoint = (IStandardPoint) point;

    try {
      IStandardPoint copyItem = stdPoint.duplicate();
      manager.addPoint(copyItem);
      selectLastStandPoint();
    } catch (Exception e) {
      log.error("Fail to clone virtual point:" + e.getMessage());
    }
  }


  @Action(enabledProperty = PROPERTY_REMOVEABLE)
  public void moveSelectionUp() {
    final int idxMin = selectionModel.getMinSelectionIndex();
    final int idxMax = selectionModel.getMaxSelectionIndex();
    if (idxMin < 1) {
      // Cannot move the first element up
      return;
    }
    
    for (int i = idxMin; i <= idxMax; i++) {
      Object p0 = getListModel().getElementAt(i);
      Object p1 = getListModel().getElementAt(i - 1);
  
      if (p0 instanceof IStandardPoint && p1 instanceof IStandardPoint) {
        manager.swap((IStandardPoint) p0, (IStandardPoint) p1);
      } else {
        log.warn("Cannot swap non-standard point!");
      }
    }
    selectionModel.setSelectionInterval(idxMin-1, idxMax-1);
    log.info("Moved points up");
  }

  @Action(enabledProperty = PROPERTY_REMOVEABLE)
  public void moveSelectionDown() {
    final int idxMin = selectionModel.getMinSelectionIndex();
    final int idxMax = selectionModel.getMaxSelectionIndex();
    if (idxMax >= getItemsCount() - 1) {
      // Cannot move the last element down
      return;
    }
    for (int i = idxMax; i >= idxMin; i--) {
      Object p0 = getListModel().getElementAt(i);
      Object p1 = getListModel().getElementAt(i+1);
      
      if (p0 instanceof IStandardPoint && p1 instanceof IStandardPoint) {
        manager.swap((IStandardPoint) p0, (IStandardPoint) p1);
      } else {
        log.warn("Cannot swap non-standard point!");
      }
    }
    selectionModel.setSelectionInterval(idxMin+1, idxMax+1);
    log.info("Moved points down");
  }

  private void fireActionStateChangeEvents() {
    firePropertyChange(PROPERTY_EDITITABLE, null,
        isEditable());
    firePropertyChange(PROPERTY_REMOVEABLE, null,
        isRemoveable());
    firePropertyChange(PROPERTY_SELECTION_NAME, null,
        getSelectionName());
  }

  private void selectLastStandPoint() {
    /* Select the added point */
    int lastIndex = manager.getStandPointNum(type) - 1;
    if (lastIndex >= 0) {
      selectionModel.setSelectionInterval(lastIndex, lastIndex);
    }
  }


  private final class VirtualPointSelectionHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      fireActionStateChangeEvents();
    }
  }

  private final class VirtualPointListListener implements ListDataListener {

    private void firePointNumChanged() {
      VirtualPointPageModel.this.firePropertyChange(PROPERTY_POINT_NUM_TEXT,
          null, getPointNumText());
    }

    @Override
    public void intervalAdded(ListDataEvent e) {
      firePointNumChanged();
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      firePointNumChanged();
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
    }

  }

  private class EditorDialogResource implements IVirtualPointDialogResource {

    @Override
    public Collection<IChannel> getAllChannels(ChannelType... type) {
      return VirtualPointPageModel.this.getAllChannels(type);
    }

    @Override
    public Collection<VirtualPoint> getAllVirtualPoints(
        VirtualPointType... type) {
      return manager.getAllPointsByType(type);
    }

    @Override
    public ValueLabelSetManager getLabelManager() {
      return manager.getValueLabelManager();
    }
  }

  private final class EditorDialogInvoker<PointT extends VirtualPoint>
      extends AbstractEditorDialogInvoker<PointT> {

    @SuppressWarnings("unchecked")
    EditorDialogInvoker(int initialSelectionIndex) {
      super((ListModel<PointT>) pointsList.getList(), selectionModel,
          initialSelectionIndex);
    }

    @Override
    public void fireEditingItemChanged() {
      int index = getSelectionIndex();
      pointsTableModel.fireTableRowsUpdated(index, index);
    }
  }

  private abstract static class AbstractVirtualPointTableModel<T extends VirtualPoint>
      extends AbstractTableAdapter<T> {

    private static final int COLUMN_GROUP_ID = 0;
    private static final int COLUMN_DESCRIPTION = 1;
    private static final int COLUMN_CUSTOM_LABEL = 2;
    protected static final int COMMON_COLUMN_COUNT = 3;

    private static final String[] COMMON_COLUMNS_NAMES =
    { "[Group,ID]", "Description", "Custom Label" };


    public AbstractVirtualPointTableModel(ListModel<T> listModel,
        String[] extraColumNames) {
      super(listModel, ArrayUtils.addAll(COMMON_COLUMNS_NAMES, extraColumNames));
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      if (columnIndex == COLUMN_DESCRIPTION) {
        if (getRow(rowIndex).getGroup() == 0) {
          return true;
        }
      }
      return super.isCellEditable(rowIndex, columnIndex);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      VirtualPoint point = getRow(rowIndex);

      switch (columnIndex) {
      case COLUMN_GROUP_ID:
        return point.getGroupID();
      case COLUMN_DESCRIPTION:
        return point.getDescription();
      case COLUMN_CUSTOM_LABEL:
        return point.getLabelSetRef().getLabelSet();
      default:
        return getExtraValues(rowIndex, columnIndex);
      }
    }

    protected abstract Object getExtraValues(int rowIndex, int columnIndex);

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
      T point = getRow(rowIndex);
      if (columnIndex == COLUMN_DESCRIPTION
          && aValue != null && point instanceof IStandardPoint) {
        String oldDescription = (String) getValueAt(rowIndex, columnIndex);
        String newDescription = aValue.toString();
        if (EqualsUtil.areEqual(oldDescription, newDescription) == false) {
          ((IStandardPoint) point).setDescription(newDescription);
          ((IStandardPoint) point).setCustomDescriptionEnabled(true);
        }
      }
    }
  }

  /**
   * Adapt a list of Binary points to table model.<br>
   * Describes how to present an Binary Point in JTable.
   */
  private static final class BinaryTableAdapter extends
      AbstractVirtualPointTableModel<BinaryPoint>
      implements ISupportHighlight {

    private static final String[] COLUMNS =
    { "Source", "Invert", "Chatter No", "Chatter Time" };

    private static final int COLUMN_SOURCE = COMMON_COLUMN_COUNT;
    private static final int COLUMN_INVERT = COMMON_COLUMN_COUNT + 1;
    private static final int COLUMN_CHATNO = COMMON_COLUMN_COUNT + 2;
    private static final int COLUMN_CHATIME = COMMON_COLUMN_COUNT + 3;


    public BinaryTableAdapter(ListModel<BinaryPoint> listModel) {
      super(listModel, COLUMNS);
    }

    @Override
    protected Object getExtraValues(int rowIndex, int columnIndex) {
      BinaryPoint point = getRow(rowIndex);

      switch (columnIndex) {
      case COLUMN_SOURCE:
        IVirtualPointSource source = point.getSource();
        return source == null ? NOT_CONFIGURED_TEXT : source.getName();
      case COLUMN_INVERT:
        return boolToString(point.isInvert());
      case COLUMN_CHATNO:
        return point.getChatterNo();
      case COLUMN_CHATIME:
        return point.getChatterTime() + UNIT_SEC;
      default:
        throw new IllegalStateException("Unknown column");
      }
    }

    private static String boolToString(Boolean bool) {
      return bool ? "Yes" : "No";
    }

    @Override
    public boolean shouldHightlight(int rowIndex, int columnIndex) {
      if (rowIndex >= 0 && rowIndex < getRowCount()) {
        if (columnIndex == COLUMN_SOURCE) {
          return getRow(rowIndex).getSource() == null;
        }
      }
      return false;
    }
  }

  /**
   * Adapt a list of Double binary points to table model.<br>
   * Describes how to present an Double Binary Point in a JTable.
   */
  private static final class DoubleBinaryTableAdapter extends
      AbstractVirtualPointTableModel<DoubleBinaryPoint>
      implements ISupportHighlight {

    private static final String NA = "-";

    private static final String[] COLUMNS = {
        DoubleBinaryPoint.SOURCE_NAME_A,
        DoubleBinaryPoint.SOURCE_NAME_B,
        "Chatter No", "Chatter Time", "Debounce"
    };

    private static final int COLUMN_SOURCE = COMMON_COLUMN_COUNT;
    private static final int COLUMN_SOURCE_2ND = COMMON_COLUMN_COUNT + 1;
    private static final int COLUMN_CHATNO = COMMON_COLUMN_COUNT + 2;
    private static final int COLUMN_CHATIME = COMMON_COLUMN_COUNT + 3;
    private static final int COLUMN_DEBOUNCE = COMMON_COLUMN_COUNT + 4;


    public DoubleBinaryTableAdapter(ListModel<DoubleBinaryPoint> listModel) {
      super(listModel, COLUMNS);
    }

    @Override
    protected Object getExtraValues(int rowIndex, int columnIndex) {
      DoubleBinaryPoint point = getRow(rowIndex);

      StdDoubleBinaryPoint stdpoint = null;
      if (point instanceof StdDoubleBinaryPoint) {
        stdpoint = (StdDoubleBinaryPoint) point;
      }

      IVirtualPointSource source;
      switch (columnIndex) {
      case COLUMN_SOURCE:
        source = point.getSource();
        return source == null ? NOT_CONFIGURED_TEXT : source.getName();

      case COLUMN_SOURCE_2ND:
        if (stdpoint != null) {
          source = stdpoint.getSecondChannel();
          return source == null ? NOT_CONFIGURED_TEXT : source.getName();
        } else {
          return NA; // Not available
        }
      case COLUMN_CHATNO:
        return point.getChatterNo();
      case COLUMN_CHATIME:
        return point.getChatterTime() + UNIT_SEC;
      case COLUMN_DEBOUNCE:
        if (stdpoint != null) {
          return stdpoint.getDebounce() + UNIT_MS;
        } else {
          return NA; // Not available
        }
      default:
        throw new IllegalStateException("Unknown column");
      }
    }

    @Override
    public boolean shouldHightlight(int rowIndex, int columnIndex) {
      if (rowIndex >= 0 && rowIndex < getRowCount()) {
        DoubleBinaryPoint point = getRow(rowIndex);

        if (columnIndex == COLUMN_SOURCE && point.getSource() == null) {
          return true;
        }

        if (point instanceof StdDoubleBinaryPoint) {
          StdDoubleBinaryPoint dpoint = (StdDoubleBinaryPoint) point;
          if (columnIndex == COLUMN_SOURCE_2ND
              && dpoint.getSecondChannel() == null) {
            return true;
          }
        }
      }
      return false;
    }

  }

  /**
   * Adapt a list of Standard analogue points to table model.<br>
   * Describes how to present an StandardAnalogue Point in a JTable.
   */
  private static final class AnalogueTableAdapter extends
      AbstractVirtualPointTableModel<AnaloguePoint>
      implements ISupportHighlight {

    private static final String[] COLUMNS = {
        "Source", "Filter Type", "Filter Parameter", "Scaling Factor", "Unit",
        "Offset", "Overflow",
        "Underflow", "Underflow Hysteresis", "Overflow Hysteresis"
    };

    // @formatter:off
    private static final int COLUMN_SOURCE      = COMMON_COLUMN_COUNT;
    private static final int COLUMN_FILTER      = COMMON_COLUMN_COUNT + 1;
    private static final int COLUMN_FPARAM      = COMMON_COLUMN_COUNT + 2;
    private static final int COLUMN_SCALEFACTOR = COMMON_COLUMN_COUNT + 3;
    private static final int COLUMN_UNIT        = COMMON_COLUMN_COUNT + 4;
    private static final int COLUMN_OFFSET      = COMMON_COLUMN_COUNT + 5;
    private static final int COLUMN_OVERFLOW    = COMMON_COLUMN_COUNT + 6;
    private static final int COLUMN_UNDFLOW     = COMMON_COLUMN_COUNT + 7;
    private static final int COLUMN_UNDFLOW_HYS = COMMON_COLUMN_COUNT + 8;
    private static final int COLUMN_OVERFLOW_HYS = COMMON_COLUMN_COUNT + 9;
    // @formatter:on

    public AnalogueTableAdapter(ListModel<AnaloguePoint> listModel) {
      super(listModel, COLUMNS);
    }

    @Override
    protected Object getExtraValues(int rowIndex, int columnIndex) {
      AnaloguePoint point = getRow(rowIndex);

      switch (columnIndex) {
      case COLUMN_SOURCE:
        IVirtualPointSource source = point.getSource();
        return source == null ? NOT_CONFIGURED_TEXT : source.getName();
      case COLUMN_FILTER:
        return point.getFilterName();
      case COLUMN_FPARAM:
        return point.getFilterParam();
      case COLUMN_SCALEFACTOR:
        return point.getScalingFactor();
      case COLUMN_UNIT:
        return point.getUnit();
      case COLUMN_OFFSET:
        return point.getOffset();
      case COLUMN_OVERFLOW:
        return point.getOverflow();
      case COLUMN_UNDFLOW:
        return point.getUnderflow();
      case COLUMN_UNDFLOW_HYS:
        return point.getUnderflowHysteresis();
      case COLUMN_OVERFLOW_HYS:
        return point.getOverflowHysteresis();
      default:
        throw new IllegalStateException("Unknown column");
      }
    }

    @Override
    public boolean shouldHightlight(int rowIndex, int columnIndex) {
      if (rowIndex >= 0 && rowIndex < getRowCount()) {
        AnaloguePoint point = getRow(rowIndex);
        if (columnIndex == COLUMN_SOURCE) {
          return point.getSource() == null;
        }
      }
      return false;
    }
  }

  /**
   * Adapt a list of counter points to table model.<br>
   * Describes how to present an StandardAnalogue Point in a JTable.
   */
  private static final class CounterTableAdapter extends
      AbstractVirtualPointTableModel<CounterPoint>
      implements ISupportHighlight {

    private static final String[] COLUMNS =
    { "Source" };

    private static final int COLUMN_SOURCE = COMMON_COLUMN_COUNT;


    public CounterTableAdapter(ListModel<CounterPoint> listModel) {
      super(listModel, COLUMNS);
    }

    @Override
    protected Object getExtraValues(int rowIndex, int columnIndex) {
      CounterPoint point = getRow(rowIndex);

      switch (columnIndex) {
      case COLUMN_SOURCE:
        IVirtualPointSource source = point.getSource();
        return (source == null) ? NOT_CONFIGURED_TEXT : source;

      default:
        throw new IllegalStateException("Unknown column");
      }
    }

    @Override
    public boolean shouldHightlight(int rowIndex, int columnIndex) {
      if (rowIndex >= 0 && rowIndex < getRowCount()) {
        if (columnIndex == COLUMN_SOURCE) {
          return getRow(rowIndex).getSource() == null;
        }
      }
      return false;
    }
  }
}
