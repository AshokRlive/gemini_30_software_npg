/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain;

import java.util.Collection;

import javax.swing.ListModel;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;


/**
 *
 */
public interface VirtualPointRepository extends IConfigModule{
  
  String CONFIG_MODULE_ID = "VirtualPointManager";
  
  /**
   * Gets the point list model for a specific point type.
   *
   * @param type
   *          virtual point type
   * @return a <code>ListModel</code> which can be observed by GUI layer.
   */
  ListModel<VirtualPoint> getPointsModel(VirtualPointType type);

  /**
   * Gets the list model for all points.
   *
   * @return a <code>ListModel</code> which can be observed by GUI layer.
   */
  ListModel<VirtualPoint> getAllPointsModel();

  /**
   * Gets all points in specific types.
   *
   * @param types
   *          the point types for finding points.
   * @return a collection of points. The collection is empty if no point in
   *         given types is found.
   */
  Collection<VirtualPoint> getAllPointsByType(VirtualPointType... types);

  /**
   * Gets all points including standard and pseudo type.
   *
   * @return all points
   */
  Collection<VirtualPoint> getAllPoints();

  Collection<VirtualPoint> getPointsByModuleType(MODULE... type);

  /**
   * Gets a point with group and ID.
   *
   * @param group
   *          the point's group value.
   * @param id
   *          the points's ID value.
   * @return a virtual point if it is found. <code>null</code> if is not.
   */
  VirtualPoint getPoint(int group, int id);

  /**
   * Gets the amount of points in a type.
   *
   * @param type
   *          the type of points to be counted.
   * @return the amount of points.
   */
  int getPointsNum(VirtualPointType type);

  int getPersistentPointsNum();

  /**
   * Gets the amount of all points.
   *
   * @return the amount of points.
   */
  int getAllPointsNum();

  /**
   * Checks if this manager contains a point.
   *
   * @param point
   *          the point to be checked.
   * @return <code>true</code> if the point is managed by this manager,
   *         <code>false</code> otherwise.
   */
  boolean contains(VirtualPoint point);
}

