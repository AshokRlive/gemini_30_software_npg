/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.dialog.editor;

import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DigitalPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.DoubleBinaryValueMap;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetRef;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.validation.DoubleBinaryPointValidator;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;

/**
 * Dialog for editing {@link StdDoubleBinaryPoint} and {@link PseudoDoubleBinaryPoint}.
 */
final class DoubleBinaryPointDialog extends VirtualPointDialog<DoubleBinaryPoint> {

  private PresentationModel<DoubleBinaryValueMap> valueMapPM;
  private final PresentationModel<DoubleBinaryPoint> pm;

  {
    pm = getModel();
  }


  public DoubleBinaryPointDialog(Frame parent, IVirtualPointDialogResource resource,
      DoubleBinaryPoint point) {
    super(parent, resource, point);
  }

  public DoubleBinaryPointDialog(Frame parent, IVirtualPointDialogResource resource,
      IEditorDialogInvoker<DoubleBinaryPoint> invoker) {
    super(parent, VirtualPointType.DOUBLE_BINARY_INPUT, resource, invoker);
  }

  @Override
  void init() {
    initComponents();
    initEventHandling();
  }

  @Override
  void initForStdPoint() {
    // Binding Debounce field
    Bindings.bind(tfDebounce, pm.getBufferedModel(StdDoubleBinaryPoint.PROPERTY_DEBOUNCE));

    // ====== INIT ANNOTATIONS ======
    ValidationComponentUtils.setMessageKey(tfDebounce, StdDoubleBinaryPoint.PROPERTY_DEBOUNCE);

    panelTop.setSecondChannelDisplayed(true);
  }

  @Override
  void initForPseudoPoint() {

    // ====== HIDE UNUSED COMPONENTS ======
    tfDebounce.setVisible(false);
    lblDebounce.setVisible(false);
    lblDebounceUnit.setVisible(false);
  }

  private void initEventHandling() {
    UIUtils.setAllTextFieldsCaretPositionOnClick(contentPanel);
    
    /* Update the value label.*/ 
    ValueLabelSet bufLabelSet = (ValueLabelSet) getLabelModel().getBufferedValue(ValueLabelSetRef.PROPERTY_LABEL_SET);
    panelValueMap.setLabelSet(bufLabelSet);
    panelDelayEvent.setLabelSet(bufLabelSet);
    getLabelModel().getBufferedModel(ValueLabelSetRef.PROPERTY_LABEL_SET).addValueChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        ValueLabelSet bufLabelSet = (ValueLabelSet) evt.getNewValue();
        panelValueMap.setLabelSet(bufLabelSet);
        panelDelayEvent.setLabelSet(bufLabelSet);
      }
    });
  }

  private void createUIComponents() {
    panelTop = new VirtualPointSourcePanel(pm,
        new ArrayList<IVirtualPointSource>(getAllChannels(ChannelType.DIGITAL_INPUT)));

    panelChatter = new ChatterSettingPanel(pm);

    DigitalPoint point = getEditItem();

    // Label Set
    panelLabelSet = createLabelSetPanel();
    
    
    /* Create and show value map only for standard double binary point */
    if (point instanceof StdDoubleBinaryPoint) {
      StdDoubleBinaryPoint stdp = (StdDoubleBinaryPoint) point;
      valueMapPM = createPresentationModel(stdp.getValueMap());
      panelValueMap = new DoubleBinaryValueMapPanel(valueMapPM);
      
    } else {
      panelValueMap = new DoubleBinaryValueMapPanel();
      panelValueMap.setVisible(false);
    }

    
    panelDelayEvent = new DelayedEventReportPanel(
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY_ENABLED),
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY0),
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY1),
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY2),
        getModel().getBufferedModel(StdBinaryPoint.PROPERTY_DELAY3));
  }

  private void initComponents() {

    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    JXTitledSeparator sepParam = new JXTitledSeparator();
    panelDebounce = new JPanel();
    lblDebounce = new JLabel();
    tfDebounce = new JFormattedTextField();
    lblDebounceUnit = new JLabel();

    //======== contentPanel ========
    {
      contentPanel.setBorder(new EmptyBorder(0, 0, 0, 10));
      contentPanel.setLayout(new FormLayout(
          "[50dlu,default]:grow",
          "default, $pgap, default, $lgap, fill:default, $ugap, default, $ugap, fill:default, $lgap, default, $ugap, default"));
      contentPanel.add(panelTop, CC.xy(1, 1));

      //---- sepParam ----
      sepParam.setTitle("Parameters");
      contentPanel.add(sepParam, CC.xy(1, 3));
      contentPanel.add(panelChatter, CC.xy(1, 5));

      //======== panelDebounce ========
      {
        panelDebounce.setLayout(new FormLayout(
            "2*([50dlu,default], $lcgap), min:grow",
            "default"));

        //---- lblDebounce ----
        lblDebounce.setText("Debounce:");
        lblDebounce.setHorizontalAlignment(SwingConstants.RIGHT);
        panelDebounce.add(lblDebounce, CC.xy(1, 1));

        //---- tfDebounce ----
        tfDebounce.setColumns(10);
        panelDebounce.add(tfDebounce, CC.xy(3, 1));

        //---- lblDebounceUnit ----
        lblDebounceUnit.setText("milliseconds");
        panelDebounce.add(lblDebounceUnit, CC.xy(5, 1));
      }
      contentPanel.add(panelDebounce, CC.xy(1, 7));
      contentPanel.add(panelValueMap, CC.xy(1, 9));
      contentPanel.add(panelDelayEvent, CC.xy(1, 11));
      contentPanel.add(panelLabelSet, CC.xy(1, 13));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private VirtualPointSourcePanel panelTop;
  private ChatterSettingPanel panelChatter;
  private JPanel panelDebounce;
  private JLabel lblDebounce;
  private JFormattedTextField tfDebounce;
  private JLabel lblDebounceUnit;
  private DoubleBinaryValueMapPanel panelValueMap;
  private DelayedEventReportPanel panelDelayEvent;
  private LabelSetPanel panelLabelSet;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected JPanel createMainPanel() {
    return contentPanel;
  }

  @Override
  protected BufferValidator createValidator(PresentationModel<DoubleBinaryPoint> target) {
    return new DoubleBinaryPointValidator(target, valueMapPM);
  }

  @Override
  protected void editingPointChanged(DoubleBinaryPoint newItem) {
    if (valueMapPM != null && newItem instanceof StdDoubleBinaryPoint) {
      StdDoubleBinaryPoint stdp = (StdDoubleBinaryPoint) newItem;
      valueMapPM = createPresentationModel(stdp.getValueMap());
    }
  }

}
