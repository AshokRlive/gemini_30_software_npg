/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import com.g3schema.ns_vpoint.AnalogueFilterT;
import com.g3schema.ns_vpoint.AnaloguePointScalingT;
import com.g3schema.ns_vpoint.BaseAPointT;
import com.g3schema.ns_vpoint.DeadBandFilterT;
import com.g3schema.ns_vpoint.HighHighLowLowFilterT;
import com.g3schema.ns_vpoint.LowerUpperLimitFilterT;
import com.g3schema.ns_vpoint.OverRangeT;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.utils.NumUtils;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.HighHighLowLow;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;

/**
 * Basic implementation of analogue virtual point.
 */
public abstract class AbstractAnaloguePoint extends AbstractVirtualPoint implements AnaloguePoint {

  private double scalingFactor = 1.0;
  private double offset;
  private String unit; // unit after scaled
  private boolean overflowEnabled = true;
  private boolean underflowEnabled = true;
  private double overflow = 10000;
  private double underflow = -10000;
  private double underflowHysteresis;
  private double overflowHysteresis;
  private double rawMin = 0;
  private double rawMax = 10000;
  private double scaledMin = 0;
  private double scaledMax = 10000;

  /**
   * Current selected filter, one of the following values:
   * <ul>
   * <li>DEAD_BAND</li>
   * <li>LOWER_UPPER_LIMIT</li>
   * <li>UPPER_LIMIT</li>
   * </ul>
   * .
   */
  private FILTER filterType = FILTER.DeadBand;

  private double f0Deadband = 1000;
  private double f0InitNominal;
  private boolean f0InitNominalEnabled;

  private double f1Lowerlimit;
  private double f1Upperlimit;
  private double f1LowerHysteresis;
  private double f1UpperHysteresis;
  private boolean f1UpperEnabled = true;
  private boolean f1LowerEnabled = true;

  private final HighHighLowLow f2hihilolo = new HighHighLowLow();


  public AbstractAnaloguePoint(int group, int id, String description) {
    super(group, id, description, VirtualPointType.ANALOGUE_INPUT);
  }

  @Override
  public double getFullRangeValue() {
    return scaledMax - scaledMin;
  }

  @Override
  public double getScalingFactor() {
    return scalingFactor;
  }

  @Override
  public void setScalingFactor(double scalingFactor) {
    double oldValue = getScalingFactor();
    this.scalingFactor = scalingFactor;
    firePropertyChange(PROPERTY_SCALING_FACTOR, oldValue, scalingFactor);
  }

  @Override
  public String getUnit() {
    return unit;
  }

  @Override
  public void setUnit(String unit) {
    Object oldValue = unit;
    this.unit = unit;
    firePropertyChange(PROPERTY_UNIT, oldValue, unit);
  }

  @Override
  public double getOffset() {
    return offset;
  }

  @Override
  public void setOffset(double offset) {
    double oldValue = getOffset();
    this.offset = offset;
    firePropertyChange(PROPERTY_OFFSET, oldValue, offset);
  }

  @Override
  public double getRawMin() {
    return rawMin;
  }

  @Override
  public void setRawMin(double rawMin) {
    double oldValue = getRawMin();
    this.rawMin = rawMin;
    firePropertyChange(PROPERTY_RAW_MIN, oldValue, rawMin);
  }

  @Override
  public double getRawMax() {
    return rawMax;
  }

  @Override
  public void setRawMax(double rawMax) {
    double oldValue = getRawMax();
    this.rawMax = rawMax;
    firePropertyChange(PROPERTY_RAW_MAX, oldValue, rawMax);
  }

  @Override
  public double getScaledMin() {
    return scaledMin;
  }

  @Override
  public void setScaledMin(double scaledMin) {
    double oldValue = getScaledMin();
    this.scaledMin = scaledMin;
    firePropertyChange(PROPERTY_SCALED_MIN, oldValue, this.scaledMin);
  }

  @Override
  public double getScaledMax() {
    return scaledMax;
  }

  @Override
  public void setScaledMax(double scaledMax) {
    double oldValue = getScaledMax();
    this.scaledMax = scaledMax;
    firePropertyChange(PROPERTY_SCALED_MAX, oldValue, this.scaledMax);
  }

  @Override
  public boolean isOverflowEnabled() {
    return overflowEnabled;
  }

  @Override
  public void setOverflowEnabled(boolean overflowEnabled) {
    Object oldValue = this.overflowEnabled;
    this.overflowEnabled = overflowEnabled;
    firePropertyChange(PROPERTY_OVERFLOW_ENABLED, oldValue, overflowEnabled);
  }

  @Override
  public void setUnderflowEnabled(boolean underflowEnabled) {
    Object oldValue = this.underflowEnabled;
    this.underflowEnabled = underflowEnabled;
    firePropertyChange(PROPERTY_UNDERFLOW_ENABLED, oldValue, underflowEnabled);
  }

  @Override
  public boolean isUnderflowEnabled() {
    return underflowEnabled;
  }

  @Override
  public double getOverflow() {
    return overflow;
  }

  @Override
  public void setOverflow(double overflow) {
    Object oldValue = getOverflow();
    this.overflow = overflow;
    firePropertyChange(PROPERTY_OVERFLOW, oldValue, overflow);
  }

  @Override
  public double getUnderflow() {
    return underflow;
  }

  @Override
  public void setUnderflow(double underflow) {
    Object oldValue = getUnderflow();
    this.underflow = underflow;
    firePropertyChange(PROPERTY_UNDERFLOW, oldValue, underflow);
  }

  @Override
  public double getUnderflowHysteresis() {
    return underflowHysteresis;
  }

  @Override
  public void setUnderflowHysteresis(double underflowHysteresis) {
    Object oldValue = getUnderflowHysteresis();
    this.underflowHysteresis = underflowHysteresis;
    firePropertyChange(PROPERTY_UNDERFLOW_HYSTERESIS, oldValue, underflowHysteresis);
  }

  @Override
  public double getOverflowHysteresis() {
    return overflowHysteresis;
  }

  @Override
  public void setOverflowHysteresis(double overflowHysteresis) {
    Object oldValue = getUnderflowHysteresis();
    this.overflowHysteresis = overflowHysteresis;
    firePropertyChange(PROPERTY_OVERFLOW_HYSTERESIS, oldValue, overflowHysteresis);
  }

  @Override
  public String getHTMLSummary() {
    StringBuffer html = new StringBuffer();
    html.append("<html>");
    html.append("<b>Point: " + getName() + "</b>");
    html.append("<ul>");
    html.append("<li>Type:" + type.getName() + "</li>");
    html.append("<li>Scale: " + getScalingFactor() + "</li>");
    html.append("<li>Scale: " + getUnit() + "</li>");
    html.append("<li>Offset: " + getOffset() + " ms</li>");
    html.append("<li>Overflow: " + getOverflow() + "</li>");
    html.append("<li>Underflow: " + getUnderflow() + "</li>");
    html.append("<li>Overflow Hysteresis: " + getOverflowHysteresis() + "</li>");
    html.append("<li>Underflow Hysteresis: " + getUnderflowHysteresis() + "</li>");
    html.append("</ul>");
    html.append("</html>");
    return html.toString();
  }

  // ----------------------- Getter/Setters [Filters]--------------------------

  @Override
  public FILTER getFilterType() {
    return filterType;
  }

  @Override
  public void setFilterType(FILTER filterType) {
    Preconditions.checkNotNull(filterType, "filter type is null");
    Object oldValue = this.filterType;
    this.filterType = filterType;
    firePropertyChange(PROPERTY_FILTER_TYPE, oldValue, filterType);
  }

  @Override
  public double getF0Deadband() {
    return f0Deadband;
  }

  @Override
  public void setF0Deadband(double f0Deadband) {
    Object oldValue = getF0Deadband();
    this.f0Deadband = f0Deadband;
    firePropertyChange(PROPERTY_FILTER0_DEADBAND, oldValue, f0Deadband);
  }

  @Override
  public double getF0InitNominal() {
    return f0InitNominal;
  }

  @Override
  public void setF0InitNominal(double f0InitNominal) {
    Object oldValue = this.f0InitNominal;
    this.f0InitNominal = f0InitNominal;
    firePropertyChange(PROPERTY_FILTER0_INIT_NOMINAL, oldValue, f0InitNominal);
  }

  @Override
  public boolean isF0InitNominalEnabled() {
    return f0InitNominalEnabled;
  }

  @Override
  public void setF0InitNominalEnabled(boolean f0InitNominalEnabled) {
    Object oldValue = this.f0InitNominalEnabled;
    this.f0InitNominalEnabled = f0InitNominalEnabled;
    firePropertyChange(PROPERTY_FILTER0_INIT_NOMINAL_ENABLED, oldValue, f0InitNominalEnabled);
  }

  @Override
  public double getF1Lowerlimit() {
    return f1Lowerlimit;
  }

  @Override
  public void setF1Lowerlimit(double f1Lowerlimit) {
    Object oldValue = getF1Lowerlimit();
    this.f1Lowerlimit = f1Lowerlimit;
    firePropertyChange(PROPERTY_FILTER1_LOWER_LIMIT, oldValue, f1Lowerlimit);
  }

  @Override
  public double getF1Upperlimit() {
    return f1Upperlimit;
  }

  @Override
  public void setF1Upperlimit(double f1Upperlimit) {
    Object oldValue = getF1Upperlimit();
    this.f1Upperlimit = f1Upperlimit;
    firePropertyChange(PROPERTY_FILTER1_UPPER_LIMIT, oldValue, f1Upperlimit);
  }

  @Override
  public double getF1LowerHysteresis() {
    return f1LowerHysteresis;
  }

  @Override
  public void setF1LowerHysteresis(double f1LowerHysteresis) {
    Object oldValue = getF1LowerHysteresis();
    this.f1LowerHysteresis = f1LowerHysteresis;
    firePropertyChange(PROPERTY_FILTER1_LOWER_HYSTERESIS, oldValue, f1LowerHysteresis);
  }

  @Override
  public double getF1UpperHysteresis() {
    return f1UpperHysteresis;
  }

  @Override
  public void setF1UpperHysteresis(double f1UpperHysteresis) {
    Object oldValue = getF1UpperHysteresis();
    this.f1UpperHysteresis = f1UpperHysteresis;
    firePropertyChange(PROPERTY_FILTER1_UPPER_HYSTERESIS, oldValue, f1UpperHysteresis);
  }

  @Override
  public boolean isF1UpperEnabled() {
    return f1UpperEnabled;
  }

  @Override
  public void setF1UpperEnabled(boolean f1UpperEnabled) {
    Object oldValue = isF1UpperEnabled();
    this.f1UpperEnabled = f1UpperEnabled;
    firePropertyChange(PROPERTY_FILTER1_UPPER_ENABLED, oldValue, f1UpperEnabled);
  }

  @Override
  public boolean isF1LowerEnabled() {
    return f1LowerEnabled;
  }

  @Override
  public void setF1LowerEnabled(boolean f1LowerEnabled) {
    Object oldValue = isF1LowerEnabled();
    this.f1LowerEnabled = f1LowerEnabled;
    firePropertyChange(PROPERTY_FILTER1_LOWER_ENABLED, oldValue, f1LowerEnabled);
  }

  @Override
  public HighHighLowLow getF2HiHiLoLo() {
    return f2hihilolo;
  }

  @Override
  public String getFilterName() {
    return getFilterType().getName();
  }

  @Override
  public String getFilterParam() {
    StringBuilder filterContent = new StringBuilder();
    switch (getFilterType()) {
    case DeadBand:
      filterContent.append("Deadband: ");
      filterContent.append(getF0Deadband());
      break;

    case LowerUpperLimit:
      filterContent.append("Upper Limit:");
      filterContent.append(getF1Upperlimit());
      filterContent.append(", Lower Limit: ");
      filterContent.append(getF1Lowerlimit());
      filterContent.append(", Upper Hysteresis: ");
      filterContent.append(getF1UpperHysteresis());
      filterContent.append(", Lower Hysteresis: ");
      filterContent.append(getF1LowerHysteresis());
      break;

    case HighHighLowLow:
      return f2hihilolo.getFilterParams();

    default:
      break;
    }

    return filterContent.toString();
  }

  @Override
  public void resetAllHysteresis() {
    double defaultHysteresis = getFullRangeValue() * DEFAULT_HYSTERESIS_PERCENT;
    defaultHysteresis = NumUtils.round(defaultHysteresis, 4);

    setF1LowerHysteresis(defaultHysteresis);
    setF1UpperHysteresis(defaultHysteresis);
    setOverflowHysteresis(defaultHysteresis);
    setUnderflowHysteresis(defaultHysteresis);
    f2hihilolo.setHihiHysteresis(defaultHysteresis);
    f2hihilolo.setHiHysteresis(defaultHysteresis);
    f2hihilolo.setLoHysteresis(defaultHysteresis);
    f2hihilolo.setLoloHysteresis(defaultHysteresis);

  }

  protected void readFromXML(BaseAPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);

    // Unit
    String unit = null;
    if (xml.unit.exists()) {
      unit = xml.unit.getValue();
    }
    setUnit(unit);

    // Scaling
    AnaloguePointScalingT scaling = xml.scaling.first();
    double scale = scaling.scalingFactor.getValue();
    double rawMax = scaling.rawMax.getValue();
    double rawMin = scaling.rawMin.getValue();
    double offset = scaling.offset.getValue();
    setRawMax(rawMax);
    setRawMin(rawMin);
    setOffset(offset);
    setScalingFactor(scale);
    setScaledMin(rawMin * scale + offset);
    setScaledMax(rawMax * scale + offset);

    // OverRage
    OverRangeT overRange = xml.overRange.first();
    boolean overflowEnable = overRange.overflowEnabled.getValue();
    double overflow = overRange.overflow.getValue();
    double overflowHys = overRange.overflowHysteresis.getValue();

    boolean underflowEnable = overRange.underflowEnabled.getValue();
    double underflow = overRange.underflow.getValue();
    double underflowHys = overRange.underflowHysteresis.getValue();

    setOverflowHysteresis(overflowHys);
    setOverflow(overflow);
    setOverflowEnabled(overflowEnable);
    setUnderflowHysteresis(underflowHys);
    setUnderflow(underflow);
    setUnderflowEnabled(underflowEnable);

    // Filter
    AnalogueFilterT xml_filter = xml.filter.first();
    if (xml_filter.deadBand.exists()) {
      DeadBandFilterT xml_deadband = xml_filter.deadBand.first();
      setF0Deadband(xml_deadband.deadBand.getValue());
      setF0InitNominal(xml_deadband.initialNominal.getValue());
      setF0InitNominalEnabled(xml_deadband.initialNominalEnabled.getValue());
      setFilterType(FILTER.DeadBand);
    } else if (xml_filter.lowerUpperLimit.exists()) {
      LowerUpperLimitFilterT xml_lowup = xml_filter.lowerUpperLimit.first();
      setF1Lowerlimit(xml_lowup.lowerLimit.getValue());
      setF1Upperlimit(xml_lowup.upperLimit.getValue());
      setF1LowerHysteresis(xml_lowup.lowerHysteresis.getValue());
      setF1UpperHysteresis(xml_lowup.upperHysteresis.getValue());

      setFilterType(FILTER.LowerUpperLimit);
    } else if (xml_filter.highHighLowLow.exists()) {
      HighHighLowLowFilterT xml_highlow = xml_filter.highHighLowLow.first();
      HighHighLowLow hhll = getF2HiHiLoLo();
      hhll.readParamFromXML(xml_highlow);
      setFilterType(FILTER.HighHighLowLow);
    } else {
      setFilterType(FILTER.None);
    }

  }

  protected void writeToXML(BaseAPointT xml) {
    super.writeToXML(xml);

    // Unit
    xml.unit.setValue(getUnit());

    // Scaling
    AnaloguePointScalingT xml_scaling = xml.scaling.append();
    xml_scaling.rawMin.setValue(getRawMin());
    xml_scaling.rawMax.setValue(getRawMax());
    xml_scaling.scalingFactor.setValue(getScalingFactor());
    xml_scaling.offset.setValue(getOffset());

    // OVerRange
    OverRangeT xml_overRange = xml.overRange.append();
    xml_overRange.overflowEnabled.setValue(isOverflowEnabled());
    xml_overRange.overflow.setValue(getOverflow());
    xml_overRange.overflowHysteresis.setValue(getOverflowHysteresis());
    xml_overRange.underflowEnabled.setValue(isUnderflowEnabled());
    xml_overRange.underflow.setValue(getUnderflow());
    xml_overRange.underflowHysteresis.setValue(getUnderflowHysteresis());

    // Filter
    writeToXML(xml.filter.append());
  }

  private void writeToXML(AnalogueFilterT xmlFilter) {
    FILTER filter = getFilterType();
    switch (filter) {
    case DeadBand: {
      DeadBandFilterT xml = xmlFilter.deadBand.append();
      xml.deadBand.setValue(getF0Deadband());
      xml.initialNominal.setValue(getF0InitNominal());
      xml.initialNominalEnabled.setValue(isF0InitNominalEnabled());
    }
      break;

    case LowerUpperLimit: {
      LowerUpperLimitFilterT xml = xmlFilter.lowerUpperLimit.append();
      xml.lowerLimit.setValue(getF1Lowerlimit());
      xml.upperLimit.setValue(getF1Upperlimit());
      xml.lowerHysteresis.setValue(getF1LowerHysteresis());
      xml.upperHysteresis.setValue(getF1UpperHysteresis());
      xml.lowerEnabled.setValue(isF1LowerEnabled());
      xml.upperEnabled.setValue(isF1UpperEnabled());
    }
      break;

    case HighHighLowLow: {
      HighHighLowLowFilterT xml = xmlFilter.highHighLowLow.append();
      HighHighLowLow hhll = getF2HiHiLoLo();
      hhll.writeParamToXML(xml);
    }
      break;

    case None:
      xmlFilter.none.append();
      break;

    default:
      throw new IllegalStateException("Unsupported filter: " + filter);
    }
  }
  
  public static void calculateScaling(AnaloguePoint point, double rawMin, double rawMax, double scaling,
      double offset, String unit) {
    point.setRawMin(rawMin);
    point.setRawMax(rawMax);
    point.setScaledMin(rawMin * scaling + offset);
    point.setScaledMax(rawMax * scaling + offset);
    point.setScalingFactor(scaling);
    point.setOffset(offset);
    point.setOverflow(point.getScaledMax());
    point.setUnderflow(point.getScaledMin());
    if (unit != null) {
      point.setUnit(unit);
    }

    double defaultDeadBand = point.getFullRangeValue() * AnaloguePoint.DEFAULT_DEADBAND_PERCENT;
    if (defaultDeadBand != 0) {
      point.setF0Deadband(defaultDeadBand);
    }
  }

}