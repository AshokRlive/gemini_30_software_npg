/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain.validation;

import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;

/**
 * The Validator of Standard Point.
 */
public class StandardPointValidator extends AbstractVirtualPointValidator<IStandardPoint> {


  public StandardPointValidator(IStandardPoint target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {
    checkMappedResNotNull(result, target.getSource(), IStandardPoint.PROPERTY_SOURCE);

    if (target instanceof StdDoubleBinaryPoint) {
      checkMappedResNotNull(result, ((StdDoubleBinaryPoint) target).getSecondChannel(),
          StdDoubleBinaryPoint.PROPERTY_SECOND_CHANNEL);
    }

  }

  private void checkMappedResNotNull(ValidationResultExt result, Object mapResource, String propertyName) {
    if (mapResource == null) {
      result.addError("\"" + propertyName + "\" is not configured");
    }
  }

}
