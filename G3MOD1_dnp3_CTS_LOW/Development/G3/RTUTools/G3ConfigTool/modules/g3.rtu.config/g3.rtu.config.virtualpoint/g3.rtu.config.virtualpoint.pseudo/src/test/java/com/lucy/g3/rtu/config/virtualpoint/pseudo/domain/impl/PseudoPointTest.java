/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoAnaloguePointImp;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.stub.PseudoPointSourceStub;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;

public class PseudoPointTest {

  final private String description = "TestPoint1";
  final private int id = 1234;
  private IPseudoPoint[] points;

  private PseudoPointSourceStub pointSource;


  @Before
  public void setUp() throws Exception {
    pointSource = new PseudoPointSourceStub();

    VirtualPointType[] types = VirtualPointType.values();
    points = new IPseudoPoint[types.length];
    for (int i = 0; i < types.length; i++) {
      points[i] = PseudoPointFactory.createPseudoPoint(types[i], id, pointSource);
    }
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    PseudoAnaloguePointImp point = new PseudoAnaloguePointImp(description, null, id);
    assertNull(point.getSource());
    assertEquals(description, point.getDescription());
    assertEquals(id, point.getId());
  }

  @Test
  public void testConstructorWithSource() {
    PseudoAnaloguePointImp point = new PseudoAnaloguePointImp(description, pointSource, id);
    System.out.println(point.getDescription());

    assertEquals(pointSource, point.getSource());
    assertEquals(description, point.getDescription());
    assertEquals(id, point.getId());
  }

  @Test
  public void testPseudoPointDescriptionCannotBeChange() {
    PseudoAnaloguePointImp apoint = new PseudoAnaloguePointImp(description, pointSource, id);
    apoint.setAllowChangeDescription(false);
    apoint.setDescription("newdesp");
    assertEquals(description, apoint.getDescription());
  }
  
  @Test
  public void testCustomPointDescriptionCanBeChange() {
    PseudoAnaloguePointImp apoint = new PseudoAnaloguePointImp(description, pointSource, id);
    apoint.setAllowChangeDescription(true);
    apoint.setDescription("newdesp");
    assertEquals("newdesp", apoint.getDescription());
  }

  // The reference text should be updated if the owner's name or group has
  // changed.
  @Test
  public void testNameSynchronisedWithSource() {
    String ref_old, ref_new;

    for (IPseudoPoint point : points) {
      // source name change will make point reference text also changed
      ref_old = point.getName();
      assertNotNull(ref_old);
      pointSource.updateName(pointSource.getName() + "test");
      ref_new = point.getName();
      assertNotSame("Ref: " + ref_new, ref_old, ref_new);

    }
  }

  @Test
  public void testGetSourceName() {
    String name;

    for (IPseudoPoint point : points) {
      name = point.getSource().getName();
      assertNotNull(name);
      assertEquals(name, pointSource.getName());
    }

    PseudoAnaloguePointImp pointWithoutOwner = new PseudoAnaloguePointImp(description, null, id);
    assertNotNull(pointWithoutOwner.getName());
  }

  @Test
  public void testDefaultGroup() {
    VirtualPointType[] types = VirtualPointType.values();
    for (int i = 0; i < types.length; i++) {
      IPseudoPoint vp = PseudoPointFactory.createPseudoPoint(types[i], 0, null);
      assertEquals(IPseudoPoint.DEF_PSEUDO_POINT_GROUP, vp.getGroup());
      System.out.println("Group: " + vp.getGroup());
    }
  }
}
