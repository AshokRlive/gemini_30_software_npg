/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain;

import com.g3schema.ns_vpoint.AnaloguePointT;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * Interface of standard analogue virtual point.
 *
 * @see {@link IStandardPoint}
 * @see {@link AnaloguePoint}
 */
public interface StdAnaloguePoint extends AnaloguePoint, IStandardPoint, IMapToChannel {

  void writeToXML(AnaloguePointT xml, VirtualPointWriteSupport support);

  void readFromXML(AnaloguePointT xml, VirtualPointReadSupport support);
}