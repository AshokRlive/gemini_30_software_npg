/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * A dialog for selecting virtual points.
 */
public class VirtualPointSelector extends AbstractDialog {

  private static final String ACTION_KEY_DESELECT = "deselect";

  private final PointSelectorModel pm;


  public VirtualPointSelector(Window parent, VirtualPoint initialSelect, Collection<VirtualPoint> points) {
    super(parent);
    pm = new PointSelectorModel(initialSelect, points, -1);
    initialise();
  }
  
  public VirtualPointSelector(Frame parent, VirtualPoint initialSelect, Collection<VirtualPoint> points) {
    this(parent, initialSelect, points, -1);
  }

  /**
   * Construct a point selector.
   *
   * @param parent
   *          parent frame
   * @param initialSelected
   *          the default selected point
   * @param points
   *          a list of points for selecting
   * @param excludedGroup
   *          the group of points which need to be excluded.Set to -1 if no
   *          point group will be excluded.
   */
  public VirtualPointSelector(Frame parent, VirtualPoint initialSelect, Collection<VirtualPoint> points,
      int excludedGroup) {
    super(parent);
    pm = new PointSelectorModel(initialSelect, points, excludedGroup);
    initialise();
  }

  public VirtualPointSelector(Dialog parent, VirtualPoint initialSelect, Collection<VirtualPoint> points) {
    super(parent, true);
    pm = new PointSelectorModel(initialSelect, points, -1);
    initialise();
  }

  private void initialise() {
    setName("PointSelector");
    setTitle("Virtual Point Selector");
    initComponents();

    updateEnablement();
    setSelectionMode(SelectionMode.SELECTION_MODE_SINGLE);
    
    setPreferredSize(new Dimension(600, 500));
    pack();
    
    // Scroll to the selection
    int sel = pm.getSelectionIndex();
    if(sel >= 0)
      UIUtils.scrollTable(pointsListTable, sel, 0);
  }

  public void setSelectionMode(SelectionMode selectionMode) {
    pm.setSelectionMode(selectionMode);
    checkBoxSelectAll.setVisible(SelectionMode.SELECTION_MODE_MULTI == selectionMode);

    BannerPanel banner = getBannerPanel();
    if (banner != null) {
      banner.setTitle(getBannerTitle());
    }
  }

  @Override
  public void cancel() {
    pm.clearSelection();
    super.cancel();
  }

  @org.jdesktop.application.Action
  public void deselect() {
    pm.clearSelection();
    super.ok();
  }

  public void setSubtitle(String subTitle) {
    if (getBannerPanel() != null) {
      getBannerPanel().setTitle(subTitle);
    }
  }

  public VirtualPoint showDialog() {
//    Application app = Application.getInstance();
//    if (app instanceof SingleFrameApplication) {
//      ((SingleFrameApplication) app).show(this);
//    } else {
      setVisible(true);
//    }
    return pm.getSelection();
  }

  public VirtualPoint getSelectedPoint() {
    return pm.getSelection();
  }

  public Collection<VirtualPoint> getSelectedPoints() {
    return pm.getSelections();
  }

  private String getBannerTitle() {
    if (SelectionMode.SELECTION_MODE_MULTI == pm.getSelectionMode()) {
      return "Select points";
    } else if (SelectionMode.SELECTION_MODE_SINGLE == pm.getSelectionMode()) {
      return "Select a point";
    } else {
      return "";
    }
  }

  private void updateEnablement() {
    getAction(ACTION_KEY_OK).setEnabled(pm.hasSelection());
  }

  private void createUIComponents() {
    comboModule = new JComboBox<Object>(pm.getModuleComboModel());
    ComboBoxUtil.makeWider(comboModule);
    
    comboGroups = new JComboBox<Object>(pm.getGroupComboModel());
    ComboBoxUtil.makeWider(comboGroups);
    
    pointsListTable = new JTable(pm.getPointsTableModel());

    // Handle mouse event
    pointsListTable.addMouseListener(new PointTableMouseHandler());

    // Handle selection event
    pointsListTable.getModel().addTableModelListener(new PointTableModelHandler());

    // Set column width
    pointsListTable.getColumnModel().getColumn(PointSelectorModel.COLUMN_SELECTED).setMaxWidth(60);
    pointsListTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
  }

  private void checkBoxSelectAllActionPerformed(ActionEvent e) {
    pm.setSelectAll(((JCheckBox) e.getSource()).isSelected());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    contentPanel = new JPanel();
    labelModule = new JLabel();
    labelGroup = new JLabel();
    scrollPane1 = new JScrollPane();
    checkBoxSelectAll = new JCheckBox();

    //======== contentPanel ========
    {
      contentPanel.setBorder(null);
      contentPanel.setLayout(new FormLayout(
          "default, $lcgap, 50dlu:grow, $ugap, default, $lcgap, 50dlu:grow",
          "default, $ugap, fill:default:grow, $lgap, default"));

      //---- labelModule ----
      labelModule.setText("Module:");
      contentPanel.add(labelModule, CC.xy(1, 1));
      contentPanel.add(comboModule, CC.xy(3, 1));

      //---- labelGroup ----
      labelGroup.setText("Group:");
      labelGroup.setHorizontalAlignment(SwingConstants.RIGHT);
      contentPanel.add(labelGroup, CC.xy(5, 1));
      contentPanel.add(comboGroups, CC.xy(7, 1));

      //======== scrollPane1 ========
      {

        //---- pointsListTable ----
        pointsListTable.setPreferredScrollableViewportSize(new Dimension(10, 10));
        pointsListTable.setRowSelectionAllowed(false);
        scrollPane1.setViewportView(pointsListTable);
      }
      contentPanel.add(scrollPane1, CC.xywh(1, 3, 7, 1));

      //---- checkBoxSelectAll ----
      checkBoxSelectAll.setText("Select/Deselect All");
      checkBoxSelectAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          checkBoxSelectAllActionPerformed(e);
        }
      });
      contentPanel.add(checkBoxSelectAll, CC.xywh(1, 5, 3, 1));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel labelModule;
  private JComboBox<Object> comboModule;
  private JLabel labelGroup;
  private JComboBox<Object> comboGroups;
  private JScrollPane scrollPane1;
  private JTable pointsListTable;
  private JCheckBox checkBoxSelectAll;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected BannerPanel createBannerPanel() {
    BannerPanel banner = new BannerPanel();
    banner.setIcon(ResourceUtils.getImg("banner.icon", getClass()));
    banner.setTitle(getBannerTitle());
    return banner;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    Action cancelAction = getAction(ACTION_KEY_CANCEL);
    Action deselectAction = getAction(ACTION_KEY_DESELECT);
    Action okAction = getAction(ACTION_KEY_OK);

    // Set default actions for this dialog.
    setDefaultCancelAction(cancelAction);
    setDefaultAction(okAction);

    // Create cancel button
    JButton btnCancel = new JButton(cancelAction);
    btnCancel.setText("Cancel");
    btnCancel.setIcon(null);

    // Create select button
    JButton btnSelect = new JButton(okAction);
    btnSelect.setText("Select");

    // Help
    JButton btnHelp = Helper.createHelpButton(VirtualPointSelector.class);

    JPanel buttonBar;
    /* Show "Deselect" button if initial selection is available */
    if (pm.getInitialSelect() == null) {
      buttonBar = ButtonBarFactory.buildHelpBar(btnHelp, new JButton[] { btnSelect, btnCancel });
    } else {
      JButton btnDeselect = new JButton(deselectAction);
      buttonBar = ButtonBarFactory.buildHelpBar(btnHelp, new JButton[] { btnDeselect, btnSelect, btnCancel });
    }

    return buttonBar;
  }


  private class PointTableMouseHandler extends MouseAdapter {

    /* Click to toggle selection */
    @Override
    public void mousePressed(MouseEvent e) {
      int col = pointsListTable.columnAtPoint(e.getPoint());
      // No effect for "SELECT" column
      if (pointsListTable.convertColumnIndexToModel(col) == PointSelectorModel.COLUMN_SELECTED) {
        return;
      }

      // Toggle a row selection
      int rowInView = pointsListTable.rowAtPoint(e.getPoint());
      if (rowInView >= 0) {
        int rowInModel = pointsListTable.convertRowIndexToModel(rowInView);
        pm.toggleSelectionAtIndex(rowInModel);
      }
    }
  }

  private class PointTableModelHandler implements TableModelListener {

    @Override
    public void tableChanged(TableModelEvent e) {
      updateEnablement();
    }
  }
}
