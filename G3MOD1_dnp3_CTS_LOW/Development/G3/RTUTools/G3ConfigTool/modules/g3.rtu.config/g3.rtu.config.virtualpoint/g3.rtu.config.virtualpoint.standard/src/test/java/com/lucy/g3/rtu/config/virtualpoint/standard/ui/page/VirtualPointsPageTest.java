/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.ui.page;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl.VirtualPointManagerImpl;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.VirtualPointPage;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class VirtualPointsPageTest.
 */
public class VirtualPointsPageTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() throws InterruptedException {
    new VirtualPointPage(new VirtualPointManagerImpl(null));
  }

  @Ignore
  // Manual test
  @Test
  public void test() throws InterruptedException {

    VirtualPointPage page = new VirtualPointPage(new VirtualPointManagerImpl(null));
    TestUtil.showFrame(page.getContent());
    Thread.sleep(300000);
  }

}
