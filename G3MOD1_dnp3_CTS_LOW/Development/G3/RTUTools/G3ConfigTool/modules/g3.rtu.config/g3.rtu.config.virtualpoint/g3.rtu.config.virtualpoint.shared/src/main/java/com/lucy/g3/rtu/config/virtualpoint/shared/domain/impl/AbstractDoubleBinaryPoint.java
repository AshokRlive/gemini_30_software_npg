/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.impl;

import com.g3schema.ns_vpoint.BaseDBPointT;
import com.g3schema.ns_vpoint.DelayedEventEntryT;
import com.g3schema.ns_vpoint.DelayedReportT;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.ValueLabelReadSupport;

/**
 * Basic implementation of Digital Virtual Point.
 */
public abstract class AbstractDoubleBinaryPoint extends AbstractDigitalPoint {

  public AbstractDoubleBinaryPoint(int group, int id, String description) {
    super(group, id, description, VirtualPointType.DOUBLE_BINARY_INPUT);
  }

  protected void writeToXML(BaseDBPointT xml) {
    super.writeToXML(xml);
    
    // Delay report
    DelayedReportT xmlDelay = xml.delayedReport.append();
    xmlDelay.enabled.setValue(isDelayEnabled());
    
    DelayedEventEntryT xmlEntry = xmlDelay.entry.append();
    xmlEntry.delayPeriodMs.setValue(toMs(getDelay0()));
    xmlEntry.eventValue.setValue(0);
    
    xmlEntry = xmlDelay.entry.append();
    xmlEntry.delayPeriodMs.setValue(toMs(getDelay1()));
    xmlEntry.eventValue.setValue(1);
    
    xmlEntry = xmlDelay.entry.append();
    xmlEntry.delayPeriodMs.setValue(toMs(getDelay2()));
    xmlEntry.eventValue.setValue(2);
    
    xmlEntry = xmlDelay.entry.append();
    xmlEntry.delayPeriodMs.setValue(toMs(getDelay3()));
    xmlEntry.eventValue.setValue(3);
  }
  
  protected void readFromXML(BaseDBPointT xml, ValueLabelReadSupport support) {
    super.readFromXML(xml, support);
    
    // Delay report
    if (xml.delayedReport.exists()) {
      DelayedReportT xmlDelay = xml.delayedReport.first();
      setDelayEnabled(xmlDelay.enabled.getValue());
      
      DelayedEventEntryT xmlEntry;
      for (int i = 0; i < xmlDelay.entry.count(); i++) {
        xmlEntry = xmlDelay.entry.at(i);
        int value = xmlEntry.eventValue.getValue();
        setDelay(value, toSecs(xmlEntry.delayPeriodMs.getValue()));
      };
    }
  }

}
