/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.standard.domain;

import com.lucy.g3.rtu.config.channel.domain.IChannel;


/**
 * The class that implements this interface is mapped to a channel.
 */
interface IMapToChannel {

  /**
   * Gets the channel this point is mapped to.
   *
   * @return current mapped channel.
   */
  IChannel getChannel();

  /**
   * Changes the channel this point is mapped to.
   *
   * @param ch
   *          new channel
   */
  void setChannel(IChannel ch);

}
