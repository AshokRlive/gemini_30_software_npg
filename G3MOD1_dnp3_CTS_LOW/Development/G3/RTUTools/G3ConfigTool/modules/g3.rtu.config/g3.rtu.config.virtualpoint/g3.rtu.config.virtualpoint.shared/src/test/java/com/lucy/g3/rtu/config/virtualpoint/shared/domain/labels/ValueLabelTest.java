/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.domain.labels;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabel;

/**
 * The Class ValueLabelTest.
 */
public class ValueLabelTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    assertEquals(new ValueLabel(1D, ""), new ValueLabel(1D, ""));
    assertEquals(new ValueLabel(123D, ""), new ValueLabel(123.0, ""));
    assertEquals(new ValueLabel(123.1, ""), new ValueLabel(123.1, ""));
    assertNotSame(new ValueLabel(123D, ""), new ValueLabel(123.1, ""));
  }

}
