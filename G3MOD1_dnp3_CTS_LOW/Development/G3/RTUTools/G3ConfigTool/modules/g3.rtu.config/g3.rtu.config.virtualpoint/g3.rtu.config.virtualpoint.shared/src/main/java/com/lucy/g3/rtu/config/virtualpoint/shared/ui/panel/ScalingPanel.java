/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.virtualpoint.shared.ui.panel;

import static com.jgoodies.validation.view.ValidationComponentUtils.setMessageKey;

import java.awt.Color;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.effects.FadingBGValueChangeHandler;
import com.lucy.g3.rtu.config.virtualpointsource.domain.IVirtualPointSource;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.DoubleNumberFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.FormattedTextFieldSupport;

/**
 * Panel for configuring scaling factor and unit.
 */
public class ScalingPanel extends JPanel {

  private Logger log = Logger.getLogger(ScalingPanel.class);

  private final ScalingFactorEditor scalingEditor;
  private final PresentationModel<AnaloguePoint> pm;

  private final ScalingValueChangeHandler scalingChgHandler = new ScalingValueChangeHandler();
  private final RawValueChangeHanlder rawChgHandler = new RawValueChangeHanlder();
  private final ScaledValueChangeHandler scaledChgHandler = new ScaledValueChangeHandler();

  private boolean isCalScaling;
  private boolean isCalScaledValue;

  private final ValueModel fullRangeValueModel;


  public ScalingPanel(PresentationModel<AnaloguePoint> pm, ValueModel fullRangeValueModel) {
    this.pm = Preconditions.checkNotNull(pm, "pm is null");
    this.fullRangeValueModel = fullRangeValueModel;
    this.scalingEditor = new ScalingFactorEditor(
        pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALING_FACTOR),
        pm.getBufferedModel(AnaloguePoint.PROPERTY_UNIT));

    init();
  }

  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private ScalingPanel() {
    fullRangeValueModel = null;
    pm = new PresentationModel<AnaloguePoint>();
    scalingEditor = new ScalingFactorEditor(
        pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALING_FACTOR),
        pm.getBufferedModel(AnaloguePoint.PROPERTY_UNIT));
    init();
  }

  /**
   * Delegate method.
   */
  public void setScalingFactorItems(Map<Double, String> factorUnitMap,
      boolean updateUnit) {
    scalingEditor.setScalingFactorItems(factorUnitMap, updateUnit);
  }

  public void setRawUnit(String unit) {
    unit = Strings.isBlank(unit) ? "" : String.format("%s", unit);
    lblRawUnit1.setText(unit);
    lblRawUnit2.setText(unit);
  }

  public double calculateRangeValue() {
    double scaleMin = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_SCALED_MIN)).doubleValue();
    double scaleMax = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_SCALED_MAX)).doubleValue();
    double fullrangeVal = scaleMax - scaleMin;

    return Math.abs(fullrangeVal);
  }

  private void calculateScaledValues() {
    if (pm.getBean() == null) {
      return;
    }

    log.debug("Calculating scaled values...");
    isCalScaledValue = true;

    double rawMax = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_RAW_MAX)).doubleValue();
    double rawMin = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_RAW_MIN)).doubleValue();
    double factor = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_SCALING_FACTOR)).doubleValue();
    double offset = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_OFFSET)).doubleValue();

    double scaledMax = rawMax * factor + offset;
    double scaledMin = rawMin * factor + offset;

    setBufferedValue(AnaloguePoint.PROPERTY_SCALED_MAX, scaledMax);
    setBufferedValue(AnaloguePoint.PROPERTY_SCALED_MIN, scaledMin);

    log.debug("Result  scaledMax:" + scaledMax + "  scaledMin:" + scaledMin);

    updateFullRangeValue();

    isCalScaledValue = false;
  }

  private void updateFullRangeValue() {
    if (fullRangeValueModel != null) {
      double newFullRangeValue = calculateRangeValue();
      if (!EqualsUtil.areEqual(newFullRangeValue, fullRangeValueModel.getValue())) {
        fullRangeValueModel.setValue(newFullRangeValue);
      }
    }
  }

  private void setBufferedValue(String property, Object value) {
    if (!EqualsUtil.areEqual(value, pm.getBufferedValue(property))) {
      pm.setBufferedValue(property, value);
    }
  }

  private void calculateScaling() {
    if (pm.getBean() == null) {
      return;
    }

    log.debug("Calculating scaling factor and offset...");

    isCalScaling = true;

    double scaledMax = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_SCALED_MAX)).doubleValue();
    double scaledMin = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_SCALED_MIN)).doubleValue();
    double rawMax = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_RAW_MAX)).doubleValue();
    double rawMin = ((Number) pm.getBufferedValue(AnaloguePoint.PROPERTY_RAW_MIN)).doubleValue();
    double offset;

    if (rawMax != rawMin) {
      double factor = (scaledMax - scaledMin) / (rawMax - rawMin);
      offset = scaledMax - rawMax * factor;

      log.debug("Update factor:" + factor + "  offset:" + offset);

      setBufferedValue(AnaloguePoint.PROPERTY_SCALING_FACTOR, factor);
      setBufferedValue(AnaloguePoint.PROPERTY_OFFSET, offset);

    } else {
      log.error("The value of raw high and raw low must be different!");
    }

    updateFullRangeValue();

    isCalScaling = false;
  }

  private void init() {
    /* Load predefined scaling factor and units from analogue channel */
    AnaloguePoint point = pm.getBean();
    if (point != null) {
      IVirtualPointSource source = point.getSource();
      
      if (source != null && source instanceof IChannel) {
        scalingEditor.setScalingFactorItems(((IChannel)source).predefined().getPredefindScalingUnit(), false);
      }
    }

    initComponents();
    initCommitOnType();
    initComponentAnnotations();
    initComponentsBinding();
    initEventHandling();

    /* Init raw unit */
    if (point != null) {
      setRawUnit(point.getRawUnit());
    }
  }

  private void installFadingBG() {
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALED_MAX), tfScaledHigh);
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALED_MIN), tfScaledLow);
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_RAW_MIN), tfRawLow);
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_RAW_MAX), tfRawHigh);
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALING_FACTOR),
        (JComponent) tfScale.getEditor().getEditorComponent());
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_UNIT),
        (JComponent) tfUnit.getEditor().getEditorComponent());
    FadingBGValueChangeHandler.install(pm.getBufferedModel(AnaloguePoint.PROPERTY_OFFSET), tfOffset);
  }

  private void initEventHandling() {
    pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALED_MAX).addValueChangeListener(scaledChgHandler);
    pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALED_MIN).addValueChangeListener(scaledChgHandler);
    pm.getBufferedModel(AnaloguePoint.PROPERTY_RAW_MAX).addValueChangeListener(rawChgHandler);
    pm.getBufferedModel(AnaloguePoint.PROPERTY_RAW_MIN).addValueChangeListener(rawChgHandler);
    pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALING_FACTOR).addValueChangeListener(scalingChgHandler);
    pm.getBufferedModel(AnaloguePoint.PROPERTY_OFFSET).addValueChangeListener(scalingChgHandler);

    installFadingBG();
  }

  private void createUIComponents() {
    tfRawLow = new JFormattedTextField(DoubleNumberFormatter.create());
    tfRawHigh = new JFormattedTextField(DoubleNumberFormatter.create());
    tfScaledLow = new JFormattedTextField(DoubleNumberFormatter.create());
    tfScaledHigh = new JFormattedTextField(DoubleNumberFormatter.create());
    tfOffset = new JFormattedTextField(DoubleNumberFormatter.create());

    /* Scaling Factor editor */
    tfScale = scalingEditor.getScalingFactorEditorComponent();
    tfUnit = scalingEditor.getUnitEditorComponent();
  }

  private void initCommitOnType() {
    FormattedTextFieldSupport.installCommitOnType(tfRawLow);
    FormattedTextFieldSupport.installCommitOnType(tfRawHigh);
    FormattedTextFieldSupport.installCommitOnType(tfScaledLow);
    FormattedTextFieldSupport.installCommitOnType(tfScaledHigh);
    FormattedTextFieldSupport.installCommitOnType(tfOffset);
  }

  private void initComponentAnnotations() {
    setMessageKey(tfOffset, AnaloguePoint.PROPERTY_OFFSET);
    setMessageKey((JComponent) tfScale.getEditor().getEditorComponent(), AnaloguePoint.PROPERTY_SCALING_FACTOR);
    setMessageKey((JComponent) scalingEditor.getUnitEditorComponent().getEditor().getEditorComponent(),
        AnaloguePoint.PROPERTY_UNIT);
  }

  private void initComponentsBinding() {
    Bindings.bind(tfRawLow, pm.getBufferedModel(AnaloguePoint.PROPERTY_RAW_MIN));
    Bindings.bind(tfRawHigh, pm.getBufferedModel(AnaloguePoint.PROPERTY_RAW_MAX));
    Bindings.bind(tfScaledLow, pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALED_MIN));
    Bindings.bind(tfScaledHigh, pm.getBufferedModel(AnaloguePoint.PROPERTY_SCALED_MAX));
    Bindings.bind(tfOffset, pm.getBufferedModel(AnaloguePoint.PROPERTY_OFFSET));

    BufferedValueModel unitVM = pm.getBufferedModel(AnaloguePoint.PROPERTY_UNIT);
    Bindings.bind(lblScaledUnit1, unitVM);
    Bindings.bind(lblScaledUnit2, unitVM);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    label7 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label8 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    panel1 = new JPanel();
    label3 = new JLabel();
    lblRawUnit1 = new JLabel();
    label4 = new JLabel();
    lblRawUnit2 = new JLabel();
    panel3 = new JPanel();
    panel2 = new JPanel();
    lblScaledUnit1 = new JLabel();
    lblScaledUnit2 = new JLabel();

    // ======== this ========
    setLayout(new FormLayout(
        "4*(default, $rgap), 50dlu, 2*($rgap, default)",
        "default, $lgap, default"));

    // ---- label7 ----
    label7.setText("Raw Value");
    label7.setHorizontalAlignment(SwingConstants.CENTER);
    add(label7, CC.xy(1, 1, CC.CENTER, CC.DEFAULT));

    // ---- label10 ----
    label10.setText("Scaling Factor / Unit");
    label10.setHorizontalAlignment(SwingConstants.CENTER);
    add(label10, CC.xy(5, 1));

    // ---- label11 ----
    label11.setText("Offset");
    label11.setHorizontalAlignment(SwingConstants.CENTER);
    add(label11, CC.xy(9, 1));

    // ---- label8 ----
    label8.setText("Scaled Value");
    label8.setHorizontalAlignment(SwingConstants.CENTER);
    add(label8, CC.xy(13, 1, CC.CENTER, CC.DEFAULT));

    // ---- label12 ----
    label12.setText("X");
    label12.setFont(label12.getFont().deriveFont(label12.getFont().getSize() + 5f));
    add(label12, CC.xy(3, 3));

    // ---- label13 ----
    label13.setText("+");
    label13.setFont(label13.getFont().deriveFont(label13.getFont().getSize() + 7f));
    add(label13, CC.xy(7, 3));
    add(tfOffset, CC.xy(9, 3));

    // ---- label14 ----
    label14.setText("=");
    label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD,
        label14.getFont().getSize() + 5f));
    add(label14, CC.xy(11, 3));

    // ======== panel1 ========
    {
      panel1.setBorder(new CompoundBorder(
          new LineBorder(Color.lightGray, 1, true),
          new EmptyBorder(5, 5, 5, 5)));
      panel1.setLayout(new FormLayout(
          "default, $lcgap, 50dlu, $lcgap, default",
          "fill:default, $lgap, fill:default"));

      // ---- label3 ----
      label3.setText("Max:");
      panel1.add(label3, CC.xy(1, 1));
      panel1.add(tfRawHigh, CC.xy(3, 1, CC.FILL, CC.DEFAULT));
      panel1.add(lblRawUnit1, CC.xy(5, 1));

      // ---- label4 ----
      label4.setText("Min:");
      panel1.add(label4, CC.xy(1, 3));
      panel1.add(tfRawLow, CC.xy(3, 3));
      panel1.add(lblRawUnit2, CC.xy(5, 3));
    }
    add(panel1, CC.xy(1, 3));

    // ======== panel3 ========
    {
      panel3.setLayout(new FormLayout(
          "50dlu, $rgap, 35dlu",
          "fill:default"));

      // ---- tfScale ----
      tfScale.setEditable(true);
      panel3.add(tfScale, CC.xy(1, 1));

      // ---- tfUnit ----
      tfUnit.setEditable(true);
      tfUnit.setToolTipText(" unit of the scaled value");
      panel3.add(tfUnit, CC.xy(3, 1));
    }
    add(panel3, CC.xy(5, 3));

    // ======== panel2 ========
    {
      panel2.setBorder(new CompoundBorder(
          new LineBorder(Color.lightGray, 1, true),
          new EmptyBorder(5, 5, 5, 5)));
      panel2.setLayout(new FormLayout(
          "right:50dlu, $lcgap, default",
          "fill:default, $lgap, fill:default"));
      panel2.add(tfScaledHigh, CC.xy(1, 1, CC.FILL, CC.DEFAULT));
      panel2.add(lblScaledUnit1, CC.xy(3, 1));
      panel2.add(tfScaledLow, CC.xy(1, 3, CC.FILL, CC.DEFAULT));
      panel2.add(lblScaledUnit2, CC.xy(3, 3));
    }
    add(panel2, CC.xy(13, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label7;
  private JLabel label10;
  private JLabel label11;
  private JLabel label8;
  private JLabel label12;
  private JLabel label13;
  private JFormattedTextField tfOffset;
  private JLabel label14;
  private JPanel panel1;
  private JLabel label3;
  private JFormattedTextField tfRawHigh;
  private JLabel lblRawUnit1;
  private JLabel label4;
  private JFormattedTextField tfRawLow;
  private JLabel lblRawUnit2;
  private JPanel panel3;
  private JComboBox<Double> tfScale;
  private JComboBox<String> tfUnit;
  private JPanel panel2;
  private JFormattedTextField tfScaledHigh;
  private JLabel lblScaledUnit1;
  private JFormattedTextField tfScaledLow;
  private JLabel lblScaledUnit2;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class ScaledValueChangeHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == null) {
        return;
      }

      if (!isCalScaledValue) {
        calculateScaling();
      }
    }
  }

  private class ScalingValueChangeHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == null) {
        return;
      }

      if (!isCalScaling) {
        calculateScaledValues();
      }
    }
  }

  private class RawValueChangeHanlder implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == null) {
        return;
      }

      calculateScaledValues();
    }
  }
}
