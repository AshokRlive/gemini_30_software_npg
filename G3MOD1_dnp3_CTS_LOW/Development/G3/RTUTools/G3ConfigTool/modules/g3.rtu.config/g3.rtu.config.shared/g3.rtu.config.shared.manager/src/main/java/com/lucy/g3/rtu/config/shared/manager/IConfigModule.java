/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import com.jgoodies.common.bean.ObservableBean2;


/**
 * The object that implements this interface is a configuration module.
 */
public interface IConfigModule extends ObservableBean2{
  // ====== constants ======
  String HMI_SCREEN_MANAGER_ID = "HMIScreenManager";
  
  IConfig getOwner();
}

