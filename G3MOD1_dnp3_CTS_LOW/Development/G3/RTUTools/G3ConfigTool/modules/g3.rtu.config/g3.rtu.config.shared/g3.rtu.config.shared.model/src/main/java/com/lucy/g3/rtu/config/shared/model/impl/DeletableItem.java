/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.config.shared.model.IDeletable;

/**
 * The default implementation of {@link IDeletable}.
 */
public abstract class DeletableItem extends Model implements IDeletable {

  private boolean allowDelete = true;
  private boolean deleted = false;


  @Override
  public boolean isDeleted() {
    return deleted;
  }

  @Override
  public boolean isAllowDelete() {
    return allowDelete;
  }
  
  protected void setAllowDelete(boolean allowDelete) {
    this.allowDelete = allowDelete;
  }

  @Override
  public void delete() {
    firePropertyChange(PROPERTY_DELETED, deleted, deleted = true);
  }

  public static  Collection<?> findDeleteable(Collection<?> items) {
    ArrayList<Object> deleteable = new ArrayList<>();
    for (Object item : items) {
      if(item != null && item instanceof IDeletable) {
        if(((IDeletable)item).isAllowDelete()) {
          deleteable.add(item);
        }
      }
    }
    return deleteable;
  }

}
