/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.page;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.ListModel;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.help.Helper;

/**
 * A page for managing a list of items.
 */
public class ItemListManagerPage extends AbstractConfigListPage {

  private final IItemListManagerModel pm;

  private ItemListManagerPanel content;
  private StringFormatter formatter;


  public ItemListManagerPage(Object nodeData, String nodeName, String pageTitle, IItemListManagerModel pm) {
    this(nodeData, nodeName, pageTitle, pm, null);
  }

  public ItemListManagerPage(Object nodeData, String nodeName, String pageTitle, IItemListManagerModel pm,
      StringFormatter formatter) {
    super(nodeData, pageTitle);
    this.pm = Preconditions.checkNotNull(pm, "pm is null");
    this.formatter = formatter;

    setNodeName(nodeName);

    Helper.register(this, nodeData.getClass());
  }

  @Override
  protected void init() throws Exception {
    super.init();
    initComponents();
  }

  private void initComponents() {
    content = new ItemListManagerPanel(pm.getSelectionInList(), pm.getPageActions(), getViewAction());
    content.setListItemFormatter(formatter);
    add(content, BorderLayout.CENTER);
  }

  @Override
  public ListModel<?> getSelsectionListModel() {
    return pm.getSelectionInList().getListModel();
  }

  @Override
  public Action[] getContextActions() {
    return pm.getNodeActions();
  }

  @Override
  protected SelectionInList<?> getSelectionInList() {
    return pm.getSelectionInList();
  }

}
