/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.itemlist.ItemListManager;



/**
 * Basic implementation of <code>IListConfigModule</code>.
 *
 * @param <T>
 *          the type of managed items.
 */
public abstract class AbstractListConfigModule<T> extends ItemListManager<T> implements IListConfigModule<T> {
  
  private final IConfig owner;

  protected final Logger log = Logger.getLogger(getClass());

  protected AbstractListConfigModule(IConfig owner) {
    this(owner, null);
  }

  protected AbstractListConfigModule(IConfig owner, ArrayListModel<T> items) {
    super(items);
    this.owner = owner;
  }

  @Override
  public final IConfig getOwner() {
    return owner;
  }

}

