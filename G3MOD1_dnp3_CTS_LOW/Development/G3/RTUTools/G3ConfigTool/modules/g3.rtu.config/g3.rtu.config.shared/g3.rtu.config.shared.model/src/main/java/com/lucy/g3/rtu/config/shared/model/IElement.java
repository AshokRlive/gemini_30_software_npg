/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;

import com.jgoodies.common.bean.ObservableBean2;

/**
 * Extended bean that has a list of attributes by default, such as name, description.
 */
public interface IElement extends ObservableBean2 {

  /**
   * The name of read-only property {@value} .
   */
  String PROPERTY_NAME = "name";

  /**
   * The name of read-only property {@value} .
   */
  String PROPERTY_DESCRIPTION = "description";


  String getDescription();

  String getName();
  
  void putProperty(String property, Object value);
  
  Object getProperty(String property);
}
