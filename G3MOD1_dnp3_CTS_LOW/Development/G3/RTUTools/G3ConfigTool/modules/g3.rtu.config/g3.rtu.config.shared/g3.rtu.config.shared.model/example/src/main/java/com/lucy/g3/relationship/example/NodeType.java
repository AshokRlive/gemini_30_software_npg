/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.relationship.example;

import com.lucy.g3.relationship.INodeType;
import com.lucy.g3.relationship.impl.DefaultRule;
import com.lucy.g3.relationship.impl.Rules;

public enum NodeType implements INodeType {
    CHANNEL, VIRUAL_POINT, MODULE, HMI_SCREEN, SCADA_POINT;

    static {
        
        Rules rules = Rules.INSTANCE;
        rules.registerRule(new DefaultRule(CHANNEL, -1, VIRUAL_POINT, 1));
    }
}
