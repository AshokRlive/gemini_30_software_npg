/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent;

/**
 * The Class NodeStub.
 */
class NodeStub extends AbstractNode {

  public NodeStub() {
    this(new INodeType() {
    });
  }

  public NodeStub(INodeType type) {
    super(type);
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
  }

}
