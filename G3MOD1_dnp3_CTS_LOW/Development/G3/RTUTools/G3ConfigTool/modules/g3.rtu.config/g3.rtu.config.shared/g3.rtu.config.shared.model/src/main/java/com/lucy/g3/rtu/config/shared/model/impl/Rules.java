/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import java.util.HashMap;

import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.IRule;

/**
 * The Class Rules.
 */
public final class Rules {

  private final HashMap<RuleType, IRule> rules = new HashMap<RuleType, IRule>();


  public void registerRule(IRule rule) {
    if (rule != null) {
      rules.put(new RuleType(rule.getSourceType(), rule.getTargetType()), rule);
    }
  }

  public IRule getRule(INodeType source, INodeType target) {
    return rules.get(new RuleType(source, target));
  }

  private Rules() {
  }


  public static final Rules INSTANCE = new Rules();


  private static class RuleType {

    private final INodeType source;
    private final INodeType target;


    public RuleType(INodeType source, INodeType target) {
      this.source = source;
      this.target = target;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result
          + ((source == null) ? 0 : source.hashCode());
      result = prime * result
          + ((target == null) ? 0 : target.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      RuleType other = (RuleType) obj;
      if (source == null) {
        if (other.source != null) {
          return false;
        }
      } else if (!source.equals(other.source)) {
        return false;
      }
      if (target == null) {
        if (other.target != null) {
          return false;
        }
      } else if (!target.equals(other.target)) {
        return false;
      }
      return true;
    }

  }
}
