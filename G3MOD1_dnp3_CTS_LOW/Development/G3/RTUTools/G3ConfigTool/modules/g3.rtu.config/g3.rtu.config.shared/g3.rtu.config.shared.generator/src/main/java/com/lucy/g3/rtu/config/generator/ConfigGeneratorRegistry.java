/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * The default implementation of <code>IConfigFactory</code>. Configuration
 * module factories must be registered before creating IConfig.
 */
public final class ConfigGeneratorRegistry  {
  private final HashMap<Class<? extends IConfigGenerator>, IConfigGenerator> generatorsMap = new HashMap<>();

  @SuppressWarnings("unchecked")
  public <T extends IConfigGenerator> T getGenerator(IConfig config, Class<T> generatorClass) {
    T gen = (T) generatorsMap.get(generatorClass);
    
    if(gen != null) {
      gen.setConfig(config);
    } else {
        Logger.getLogger(ConfigGeneratorRegistry.class)
        .error(String.format("Generator \"%s\" not found! "
            + "Please registere it in ConfigGeneratorPlugin.",generatorClass.getSimpleName()));
    }
    
    return gen;
  }
  
  
  public <T extends IConfigGenerator> void registerGenerator(Class<T> generatorClass, T generatorInstance) {
    if (generatorClass == null || generatorInstance == null) {
      throw new IllegalArgumentException("Cannot register null generator!");
    }
    
    if (generatorsMap.containsKey(generatorClass)) {
      throw new IllegalArgumentException("Factory already registered for id: " + generatorClass.getSimpleName());
    }
    
    generatorsMap.put(generatorClass, generatorInstance);
  }
  
  public void deregisterGenerator(Class<? extends IConfigGenerator> generatorClass) {
    generatorsMap.remove(generatorClass);
  }

  
  // ================== Singleton ===================
  private static final ConfigGeneratorRegistry INSTANCE = new ConfigGeneratorRegistry();
  
  
  public static ConfigGeneratorRegistry getInstance() {
    return INSTANCE;
  }
  
  private ConfigGeneratorRegistry() {}

 
}
