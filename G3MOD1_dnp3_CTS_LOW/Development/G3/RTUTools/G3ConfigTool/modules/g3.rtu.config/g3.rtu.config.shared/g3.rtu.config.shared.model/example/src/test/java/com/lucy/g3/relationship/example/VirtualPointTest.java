package com.lucy.g3.relationship.example;

import java.util.Collection;
import java.util.logging.Level;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.util.LoggingUtils;
import com.lucy.g3.relationship.IConnection;
import com.lucy.g3.relationship.NotConnectibleException;


public class VirtualPointTest {
    private VirtualPoint vpoint;
    private Channel channel;

    @Before
    public void setUp() throws Exception {
        vpoint = new VirtualPoint();
        channel = new Channel();
        
        LoggingUtils.setDefaultLevel(Level.INFO);
        LoggingUtils.logPropertyChanges(vpoint);
        LoggingUtils.logPropertyChanges(channel);
    }


    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testSetChannel() throws NotConnectibleException {
        assertDisconnected();
        vpoint.setSource(channel);
        assertConnected();
    }
    
    @Test
    public void testPointConnectToChannel() throws NotConnectibleException {
        assertDisconnected();
        vpoint.connectToSource(channel);
        assertConnected();
    }
    
    @Test
    public void testChannelConnectToPoint() throws NotConnectibleException {
        assertDisconnected();
        channel.connectToTarget(vpoint);
        assertConnected();
    }


    @Test
    public void testDeletePoint() throws NotConnectibleException {
        vpoint.setSource(channel);
        vpoint.delete();
        assertDisconnected();
    }


    @Test
    public void testDeleteChannel() throws NotConnectibleException {
        vpoint.setSource(channel);
        channel.delete();
        Assert.assertNull(vpoint.getSource());
    }
    
    
    @Test
    public void testDeleteConnectionFromPoint() throws NotConnectibleException {
        vpoint.setSource(channel);
        
        Collection<IConnection> conns = vpoint.getAllConnections();
        for (IConnection conn: conns) {
                conn.delete();
        }
        assertDisconnected();
    }
    
    @Test
    public void testDeleteConnectionFromChannel() throws NotConnectibleException {
        vpoint.setSource(channel);
        
        Collection<IConnection> conns = channel.getAllConnections();
        for (IConnection conn: conns) {
            conn.delete();
        }
        assertDisconnected();
    }
    
    @Test
    public void testDisconnectFromChannel() throws NotConnectibleException{
        vpoint.connectToSource(channel);
        vpoint.disconnectFromTarget(channel); // no effect 
        channel.disconnectFromSource(vpoint); // no effect
        assertConnected();
        
        vpoint.disconnectFromSource(channel); // Have effect
        assertDisconnected();
    }
    
    @Test
    public void testDisconnectFromPoint() throws NotConnectibleException{
        vpoint.connectToSource(channel);
        assertConnected();
        
        channel.disconnectFromTarget(vpoint);
        assertDisconnected();
    }
    
    private void assertDisconnected() {
        Assert.assertNull(vpoint.getSource());
        Assert.assertTrue(vpoint.getAllConnections().isEmpty());
        Assert.assertTrue(channel.getAllConnections().isEmpty());
    }
    
    private void assertConnected() {
        Assert.assertEquals(channel, vpoint.getSource());
        Assert.assertTrue(channel.getAllConnections().size() == 1);
        Assert.assertTrue(vpoint.getAllConnections().size() == 1);
    }
    
    
    @Test
    public void testSetDifferentChannel() throws NotConnectibleException{
        vpoint.setSource(channel);
        Assert.assertTrue(channel == vpoint.getSource());
        
        
        Channel newChannel = new Channel();
        vpoint.setSource(newChannel);
        
        Assert.assertTrue(newChannel == vpoint.getSource());
        
        // Checks disconnected from previous channel 
        Assert.assertTrue(channel.getAllConnections().isEmpty());
        
        // Checks only 1 connection exists
        Assert.assertTrue(vpoint.getAllConnections().size() == 1);
    }
    
    @Test(expected = NotConnectibleException.class)
    public void testConnectToTwoChannels() throws NotConnectibleException{
        vpoint.connectToSource(channel);
        Assert.assertTrue(channel == vpoint.getSource());
        
        // exception expected when connecting to two channels
        vpoint.connectToSource(new Channel());
    }
    
    @Test
    public void testBinding() throws NotConnectibleException{
        VirtualPoint vpoint2 = new VirtualPoint();
        
        PropertyConnector.connect(vpoint2, VirtualPoint.PROPERTY_SOURCE, vpoint, VirtualPoint.PROPERTY_SOURCE);
        
        IConnection conn = vpoint.connectToSource(channel);
        Assert.assertTrue(channel == vpoint.getSource());
        Assert.assertTrue(channel == vpoint2.getSource());
        Assert.assertTrue(channel.getAllConnections().size() == 2);
        
        conn.delete();
        Assert.assertNull(vpoint.getSource());
        Assert.assertNull(vpoint2.getSource());
        Assert.assertEquals(0, channel.getAllConnections().size());
    }
    
    
    @Test
    public void testBinding2() throws NotConnectibleException{
        vpoint.setSource(channel);
        
        VirtualPoint vpoint2 = new VirtualPoint();
        PropertyConnector.connect(vpoint2, VirtualPoint.PROPERTY_SOURCE, vpoint, VirtualPoint.PROPERTY_SOURCE);
        
        Channel channel2 = new Channel();
        vpoint2.setSource(channel2);
        Assert.assertEquals(0, channel.getAllConnections().size());
        Assert.assertEquals(1, vpoint.getAllConnections().size());
        Assert.assertEquals(1, vpoint2.getAllConnections().size());
        Assert.assertTrue(channel2 == vpoint.getSource());
        Assert.assertTrue(channel2 == vpoint2.getSource());

        
    }
    
    
}

