/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.shared.validation.domain.ICleaner;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The basic validator implementation for items manager.
 */
public abstract class AbstractContainerValidator<TargetT extends IContainerValidation> 
        extends AbstractValidator<TargetT>
        implements IContainerValidator{

  private Logger log = Logger.getLogger(AbstractContainerValidator.class);

  public AbstractContainerValidator(TargetT validatedItem) {
    super(validatedItem);
  }
  
  @Override
  public final Collection<ValidationResultExt> getResults() {
    ArrayList<ValidationResultExt> results = new ArrayList<>();
    if(!isValid()){
      results.add(getResult());
    }
    
    Collection<IValidation> children = getValidatableChildrenItems();
    if(children != null) {
      for (IValidation child : children) {
        IValidator validator = child.getValidator();
        if(validator != null) {
          if(validator instanceof IContainerValidator)
            results.addAll(((IContainerValidator)validator).getResults());
          else
            results.add(validator.getResult());
        }
      }
    }
    
    return results;
  }
  
  /**
   * Gets the items to be validated.
   * @return
   */
  protected abstract Collection<IValidation> getValidatableChildrenItems();
  
  
  @Override
  public boolean isValid() {
    return super.isValid() && isChildrenValid();
  }

  private boolean isChildrenValid() {
    Collection<IValidation> children = getValidatableChildrenItems();
    
    if(children != null) {
      for (IValidation child : children) {
        IValidator validator = child.getValidator();
        if(validator != null && validator.isValid() == false)
          return false;
      }
    }
    
    return true;
  }

  @Override
  public final ValidationResultExt validate(){
    validateChildren();
    return super.validate();
  }

  private void validateChildren() {
    Collection<IValidation> items = getValidatableChildrenItems();

    if (items != null) {
      for (IValidation item : items) {
        IValidator validator = item.getValidator();
        if (validator != null) {
          validator.validate();
        } 
      }
    }
  }

  /**
   * Override this method if there is anything to be validated for this container itself.
   * By default nothing to be validated for this container. 
   */
  @Override
  protected void validate(ValidationResultExt result) {
    // Nothing to validate
  }

  /**
   * Override this method if this container does support config cleaning.
   */
  @Override
  public ICleaner getCleaner() {
    return null;
  }
  
}
