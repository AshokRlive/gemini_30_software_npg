/**
 * Copyright (c) 2007-2012, JGraph Ltd
 */
package com.lucy.g3.relationship.example.jgrah;

import java.util.EventObject;

import javax.swing.JFrame;

import com.lucy.g3.relationship.IConnection;
import com.lucy.g3.relationship.INode;
import com.lucy.g3.relationship.NotConnectibleException;
import com.lucy.g3.relationship.example.Channel;
import com.lucy.g3.relationship.example.VirtualPoint;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;

public class UserObject extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -708317745824467773L;

	public UserObject()
	{
		super("Hello, World!");

		// Defines the user objects, which are preferrably XML nodes that allow
		// storage of complex values as child nodes and string, number or
		// boolean properties as attributes.
		//
		// When using Java objects as user objects, make sure to add the
		// package name containg the class and register a codec for the user
		// object class as follows:
		//
		// mxCodecRegistry.addPackage("com.example"); 
		// mxCodecRegistry.register(new mxObjectCodec(
		//	new com.example.CustomUserObject()));
		//
		// Note that the object must have an empty constructor and a setter and
		// getter for each property to be persisted. The object must not have
		// a property called ID as this property is reserved for resolving cell
		// references and will cause problems when used inside the user object.
		//
		VirtualPoint vpoint = new VirtualPoint("Point A");
		Channel  channel= new Channel("Channel A");
		IConnection connection = null;
        try {
            connection = vpoint.connectToSource(channel);
        } catch (NotConnectibleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		mxGraph graph = new VirtualPointToChannelGraph();

		Object parent = graph.getDefaultParent();

		graph.getModel().beginUpdate();
		try
		{
			Object v1 = graph.insertVertex(parent, null, vpoint, 20, 20, 80,
					30);
			Object v2 = graph.insertVertex(parent, null, channel, 240, 150, 80,
					30);
			graph.insertEdge(parent, null, connection, v1, v2);
		}
		finally
		{
			graph.getModel().endUpdate();
		}

		// Overrides method to create the editing value
		mxGraphComponent graphComponent = new mxGraphComponent(graph)
		{
			private static final long serialVersionUID = 6824440535661529806L;

			@Override
            public String getEditingValue(Object cell, EventObject trigger)
			{
				if (cell instanceof mxCell)
				{
					Object value = ((mxCell) cell).getValue();

					if (value instanceof INode)
					{
					    INode n = (INode) value;
					    return n.getDescription();
					}
				}

				return super.getEditingValue(cell, trigger);
			}
			

		};
		
		getContentPane().add(graphComponent);
		
		// Stops editing after enter has been pressed instead
		// of adding a newline to the current editing value
		graphComponent.setEnterStopsCellEditing(true);
	}

	public static void main(String[] args)
	{
		UserObject frame = new UserObject();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400, 320);
		frame.setVisible(true);
	}

}
