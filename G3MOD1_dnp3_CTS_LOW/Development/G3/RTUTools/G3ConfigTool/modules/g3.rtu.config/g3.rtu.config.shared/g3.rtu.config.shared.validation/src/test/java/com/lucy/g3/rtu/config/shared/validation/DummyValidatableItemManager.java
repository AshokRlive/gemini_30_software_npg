/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;

class DummyValidatableItemManager implements IContainerValidation {

  private final ArrayList<DummyValidatableItem> validatableItems;

  private final DummyContainerValidator validator;
  
  private String name;

  public DummyValidatableItemManager() {
    this(null);
  }
  public DummyValidatableItemManager(String name) {
    this.name = name;
    validator = new DummyContainerValidator(this);
    validatableItems = new ArrayList<DummyValidatableItem>();
  }

  public void addValidatableItem(DummyValidatableItem validatableItem) {
    validatableItems.add(validatableItem);
  }

  public Collection<DummyValidatableItem> getAllItems() {
    return new ArrayList<DummyValidatableItem>(validatableItems);
  }

  public void setName(String name){
    this.name = name;
  }
  
  public String getName() {
    return name;
  }

  @Override
  public IContainerValidator getValidator() {
    return validator;
  }

  public void clear() {
    validatableItems.clear();
  }
}
