/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import java.util.HashMap;
import java.util.Set;

/**
 * The default implementation of <code>IConfigFactory</code>. Configuration
 * module factories must be registered before creating IConfig.
 */
public final class ConfigFactory implements IConfigFactory {
  private final HashMap<String, IConfigModuleFactory> moduleFactoryMap = new HashMap<>();

  public IConfig create() {
    return new Config(this);
  }
  
  public void registerFactory(String id, IConfigModuleFactory moduleFactory) {
    if (moduleFactory == null || id == null) {
      throw new IllegalArgumentException("Cannot register null factory!");
    }
    
    if (moduleFactoryMap.containsKey(id)) {
      throw new IllegalArgumentException("Factory already registered for id: " + id);
    }
    
    moduleFactoryMap.put(id, moduleFactory);
  }
  
  public void deregisterFactory(String id) {
    moduleFactoryMap.remove(id);
  }

  @Override
  public String[] getAllModuleIDs() {
    Set<String> keys = moduleFactoryMap.keySet();
    return keys.toArray(new String[keys.size()]);
  }

  @Override
  synchronized public IConfigModule createModule(IConfig owner, String id) {
    IConfigModuleFactory factory = moduleFactoryMap.get(id);
    return factory == null ? null : factory.create(owner);
  }

  
  // ================== Singleton ===================
  private static final ConfigFactory INSTANCE = new ConfigFactory();
  
  
  public static ConfigFactory getInstance() {
    return INSTANCE;
  }
  
  private ConfigFactory() {
  }
}
