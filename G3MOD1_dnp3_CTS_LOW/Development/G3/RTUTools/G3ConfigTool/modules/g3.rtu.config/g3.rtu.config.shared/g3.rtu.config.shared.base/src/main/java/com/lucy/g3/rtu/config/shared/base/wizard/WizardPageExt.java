/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.wizard;

import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.ConstantSize;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;

/**
 * Extended WizardPage to provide convenient validation methods.
 */
public class WizardPageExt extends WizardPage {
  protected String error;
  public WizardPageExt() {
    super();

  }

  public WizardPageExt(boolean autoListen) {
    super(autoListen);

  }

  public WizardPageExt(String stepDescription, boolean autoListen) {
    super(stepDescription, autoListen);

  }

  public WizardPageExt(String stepId, String stepDescription, boolean autoListen) {
    super(stepId, stepDescription, autoListen);

  }

  public WizardPageExt(String stepId, String stepDescription) {
    super(stepId, stepDescription);

  }

  public WizardPageExt(String stepDescription) {
    super(stepDescription);

  }

  protected final String checkUnsignedLong(JTextField tf, String name) {
    Long value = null;
    try{
      value = Long.valueOf(tf.getText());
    }catch(Throwable e){}
    
    if(value  == null) { 
      setErrorBackground(tf);
      return error = String.format("\"%s\" is out of range [%d, %s]!", 
          name, 0, Long.MAX_VALUE);
    
    } else if(value < 0) {
      setErrorBackground(tf);
      return error = String.format("Invalid value:%s. \"%s\" must be >= 0!", tf.getText(),name);
      
    } else {
      setNormalBackground(tf);
      return null;
    }
  }
  
  protected final String checkNotBlank(JTextField tf, String name) {
    if(Strings.isBlank(tf.getText())) {
      setErrorBackground(tf);
      return error = String.format("\"%s\" must not be blank!", name);
    } else {
      setNormalBackground(tf);
      return null;
    }
  }
  
  protected final String checkNotNull(JComboBox<?> comb, String name) {
    if(comb.getSelectedItem() == null) {
      return error = String.format("\"%s\" must not be null!", name);
    } else {
      return null;
    }
  }
  
  protected static DefaultFormBuilder createPanelBuilder(JPanel panel){
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout(
        "[50dlu,default], 7dlu, [100dlu,100dlu], 7dlu,default:grow",
        ""), panel);
    builder.setLineGapSize(new ConstantSize(7, ConstantSize.DIALOG_UNITS_Y));
    return builder;
  }
  
  protected static void setErrorBackground(JTextComponent comp){
    ValidationComponentUtils.setErrorBackground(comp);
  }
 
  protected  static void setNormalBackground(JTextComponent comp) {
    comp.setBackground(Color.WHITE);
  }
}
