/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;

/**
 * The Class NotConnectibleException.
 */
public class NotConnectibleException extends RuntimeException {

  public NotConnectibleException() {
    super();

  }

  public NotConnectibleException(String arg0, Throwable arg1, boolean arg2,
      boolean arg3) {
    super(arg0, arg1, arg2, arg3);

  }

  public NotConnectibleException(String arg0, Throwable arg1) {
    super(arg0, arg1);

  }

  public NotConnectibleException(String arg0) {
    super(arg0);

  }

  public NotConnectibleException(Throwable arg0) {
    super(arg0);

  }

}
