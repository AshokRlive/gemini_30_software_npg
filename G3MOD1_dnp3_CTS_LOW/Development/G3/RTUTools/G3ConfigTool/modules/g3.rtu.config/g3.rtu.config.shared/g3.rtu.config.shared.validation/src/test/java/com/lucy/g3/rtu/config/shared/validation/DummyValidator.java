/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

class DummyValidator extends AbstractValidator {

  private DummyValidatableItem point;


  DummyValidator(DummyValidatableItem point) {
    super(point);
    this.point = point;
  }

  @Override
  protected void validate(ValidationResultExt result) {
    if(point.getProperty() == null)
      result.addError(point.getId() + " property is not set");
  }

  @Override
  public String getTargetName() {
    return point.getName();
  }

}
