/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import com.lucy.g3.rtu.config.shared.model.IConnection;

/**
 * The event when make a connection between Nodes.
 */
public class ConnectEvent {

  private final ConnectEventType eventType;

  private final IConnection connection;


  public ConnectEvent(ConnectEventType eventType, IConnection connection) {
    this.eventType = eventType;
    this.connection = connection;
  }

  public ConnectEventType getEventType() {
    return eventType;
  }

  public IConnection getConnection() {
    return connection;
  }


  /**
   * The Enum ConnectEventType.
   */
  public static enum ConnectEventType {
    CONNECT_TO_TARGET,
    DISCONNECT_FROM_TARGET,
    CONNECT_TO_SOURCE,
    DISCONNECT_FROM_SOURCE
  }
}
