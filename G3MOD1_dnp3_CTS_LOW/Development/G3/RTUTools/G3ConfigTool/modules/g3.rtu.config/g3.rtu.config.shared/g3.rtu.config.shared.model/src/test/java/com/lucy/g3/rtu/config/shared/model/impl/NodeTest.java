/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import javax.swing.JLabel;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.beans.PropertyConnector;
import com.lucy.g3.rtu.config.shared.model.impl.AbstractNode;

/**
 * The Class NodeTest.
 */
public class NodeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testBinding() {
    JLabel label = new JLabel();
    AbstractNode node = new NodeStub();
    String description;

    node.setDescription(description = "NodeA");
    PropertyConnector.connect(node, AbstractNode.PROPERTY_DESCRIPTION, label, "text").updateProperty2();
    Assert.assertEquals(description, node.getDescription());
    Assert.assertEquals(description, label.getText());

    node.setDescription(description = "NodeB");
    Assert.assertEquals(description, node.getDescription());
    Assert.assertEquals(description, label.getText());

  }
}
