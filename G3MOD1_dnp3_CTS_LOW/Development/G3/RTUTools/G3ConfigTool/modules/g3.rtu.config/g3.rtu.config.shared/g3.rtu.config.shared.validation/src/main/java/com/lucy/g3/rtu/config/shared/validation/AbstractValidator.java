/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The basic implementation of {@linkplain IValidator}.
 */
public abstract class AbstractValidator<TargetT extends IValidation> /*extends Model */implements IValidator {
  
  private final ValidationResultExt result;
  
  protected final TargetT target;
  
  public AbstractValidator(TargetT target) {
    this.target = Preconditions.checkNotNull(target, "target must not be null");
    result = new ValidationResultExt(target);
  }
  
  @Override
  public final ValidationResultExt getResult() {
    return result;
  }

  @Override
  public ValidationResultExt validate() {
    result.clear();
    validate(result);
    
    return result;
  }

  @Override
  public boolean isValid() {
    return !result.hasErrors();
  }

  protected abstract void validate(ValidationResultExt result);

}
