package com.lucy.g3.relationship.example.jgrah;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;


public class VirtualPointToChannelGraph extends mxGraph{

    // Overrides method to disallow edge label editing
    public boolean isCellEditable(Object cell)
    {
        return !getModel().isEdge(cell);
    }

    // Overrides method to provide a cell label in the display
    public String convertValueToString(Object cell)
    {
        if (cell instanceof mxCell)
        {
            Object value = ((mxCell) cell).getValue();

            if (value instanceof Element)
            {
                Element elt = (Element) value;

                if (elt.getTagName().equalsIgnoreCase("person"))
                {
                    String firstName = elt.getAttribute("firstName");
                    String lastName = elt.getAttribute("lastName");

                    if (lastName != null && lastName.length() > 0)
                    {
                        return lastName + ", " + firstName;
                    }

                    return firstName;
                }
                else if (elt.getTagName().equalsIgnoreCase("knows"))
                {
                    return elt.getTagName() + " (Since "
                            + elt.getAttribute("since") + ")";
                }

            }
        }

        return super.convertValueToString(cell);
    }

    // Overrides method to store a cell label in the model
    public void cellLabelChanged(Object cell, Object newValue,
            boolean autoSize)
    {
        if (cell instanceof mxCell && newValue != null)
        {
            Object value = ((mxCell) cell).getValue();

            if (value instanceof Node)
            {
                String label = newValue.toString();
                Element elt = (Element) value;

                if (elt.getTagName().equalsIgnoreCase("person"))
                {
                    int pos = label.indexOf(' ');

                    String firstName = (pos > 0) ? label.substring(0,
                            pos).trim() : label;
                    String lastName = (pos > 0) ? label.substring(
                            pos + 1, label.length()).trim() : "";

                    // Clones the value for correct undo/redo
                    elt = (Element) elt.cloneNode(true);

                    elt.setAttribute("firstName", firstName);
                    elt.setAttribute("lastName", lastName);

                    newValue = elt;
                }
            }
        }

        super.cellLabelChanged(cell, newValue, autoSize);
    }

}

