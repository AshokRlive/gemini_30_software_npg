/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.page;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.Action;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;

/**
 * The abstract implementation of {@linkplain IItemListManagerModel} for a page
 * that manages a list of items.
 */
public abstract class AbstractItemManagerModel extends Model implements IItemListManagerModel {

  public static final String PROPERTY_SELECTION_NOT_EMPTY = "selectionNotEmpty";


  private Logger log = Logger.getLogger(AbstractItemManagerModel.class);

  private final ApplicationActionMap actionMap;

  private boolean selectionNotEmpty = false;

  private final ArrayList<Action> pageActions = new ArrayList<>(10);
  private final ArrayList<Action> nodeActions = new ArrayList<>(10);

  private PropertyChangeListener selectionPCL;


  protected AbstractItemManagerModel() {
    super();
    this.actionMap = Application.getInstance().getContext().getActionManager()
        .getActionMap(AbstractItemManagerModel.class, this);

    initDefaultActions();
  }

  private void initDefaultActions() {
    Action action = getAction(ACTION_ADD_ITEM);
    if (action != null) {
      pageActions.add(action);
      nodeActions.add(action);
    }
    action = getAction(ACTION_REMOVE_ITEM);
    if (action != null) {
      pageActions.add(action);
    }
  }

  private void ensureListObserved() {
    SelectionInList<?> selInList = getSelectionInList();

    if (selInList != null && selectionPCL == null) {
      selectionPCL = new SelectionListPCL();
      selectionNotEmpty = !selInList.isSelectionEmpty();
      getSelectionInList().addPropertyChangeListener(
          SelectionInList.PROPERTY_SELECTION_EMPTY,
          selectionPCL);
    }
  }

  public final boolean isSelectionNotEmpty() {
    ensureListObserved();
    return selectionNotEmpty;
  }

  private void setSelectionNotEmpty(boolean newValue) {
    Object oldValue = isSelectionNotEmpty();
    selectionNotEmpty = newValue;
    firePropertyChange(PROPERTY_SELECTION_NOT_EMPTY, oldValue, newValue);
  }

  protected final void addPageAction(int index, Action action) {
    index = checkIndex(index, 0, pageActions.size());
    pageActions.add(index, action);
  }

  protected final void addNodeAction(int index, Action action) {
    index = checkIndex(index, 0, nodeActions.size());
    nodeActions.add(index, action);
  }

  private int checkIndex(int index, int min, int max) {
    if (index > max) {
      index = max;
    }

    if (index < min) {
      index = min;
    }

    return index;
  }

  @Override
  public Action[] getPageActions() {
    return pageActions.toArray(new Action[pageActions.size()]);
  }

  @Override
  public Action[] getNodeActions() {
    return nodeActions.toArray(new Action[nodeActions.size()]);
  }

  /**
   * Gets an action instance this presentation model contains.
   *
   * @param actionKey
   *          the key of action
   * @return an instance of of Action. null if the action is not found.
   */
  protected final javax.swing.Action getAction(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");

    javax.swing.Action action = actionMap.get(actionKey);

    if (action == null) {
      log.error("Action is not available for the key:" + actionKey);
    } else {
      action.putValue(ACTION_KEY_NAME, actionKey);
    }

    return action;
  }

  /**
   * Gets an action instance this presentation model contains.
   * <p>
   * Showing busy cursor when action is performed. It is for improving the user
   * experience when action perform takes very long.
   * </p>
   *
   * @param actionKey
   *          the key of action
   * @return an instance of of Action. null if the action is not found.
   */
  protected final javax.swing.Action getActionWithBusyCursor(String actionKey) {
    Preconditions.checkNotNull(actionKey, "actionKey is null");

    javax.swing.Action action = actionMap.get(actionKey);

    if (action != null) {
      action = new BusyCursorAction(action, MessageDialogs.getMainFrame());
    } else {
      Logger.getLogger(getClass()).error("Action is not available for the key:" + actionKey);
    }

    return action;
  }

  /**
   * Adding action perform. Generally it will show user a dialog to add a new
   * item to this manager.
   */
  protected abstract void addActionPerformed();

  /**
   * Removing action perform. Generally it will remove a selected item from this
   * manager.
   */
  protected abstract void removeActionPerformed();

  @Override
  public String getDisplayText(Object item) {
    return item == null ? "N/A" : item.toString();
  }

  @Override
  @org.jdesktop.application.Action
  public final void addItem() {
    addActionPerformed();
  }

  @Override
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_SELECTION_NOT_EMPTY)
  public final void removeItem() {
    removeActionPerformed();
  }

  protected final boolean showConfirmRemoveDialog(String itemName) {
    return MessageDialogs.confirmRemove(MessageDialogs.getMainFrame(), itemName);
  }


  private class SelectionListPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      assert evt.getPropertyName().equals(SelectionInList.PROPERTY_SELECTION_EMPTY);
      setSelectionNotEmpty(!((Boolean) evt.getNewValue()));
    }
  }
}
