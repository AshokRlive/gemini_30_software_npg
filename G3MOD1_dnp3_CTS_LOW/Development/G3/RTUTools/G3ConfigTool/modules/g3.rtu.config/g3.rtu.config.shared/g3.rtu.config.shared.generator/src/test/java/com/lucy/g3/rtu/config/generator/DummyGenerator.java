/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator;

import com.lucy.g3.rtu.config.generator.AbstractConfigGenerator;

/**
 *
 */
public class DummyGenerator extends AbstractConfigGenerator  implements IDummyGenerator {

  @Override
  public void test() {
    System.out.println("This is dummy generator");
  }

}

