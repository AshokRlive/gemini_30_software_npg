/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.math;

import java.awt.FlowLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;

/**
 * The a editor panel for editing {@link BitMask}.
 */
public class BitMaskEditor extends JPanel {

  private final JCheckBox[] comps;

  private final String label;


  /* Constructor used by JFormDesigner. */
  @SuppressWarnings("unused")
  private BitMaskEditor() {
    this(new PresentationModel<BitMask>(new BitMask(BitMask.MAX_BIT_NUMBER)), true);
  }

  public BitMaskEditor(PresentationModel<? extends BitMask> bitMaskModel, boolean buffered) {
    super();
    setLayout(new FlowLayout(FlowLayout.LEFT));
    Preconditions.checkNotNull(bitMaskModel, "bitMaskModel must not be null");

    // Get bit number
    int bitNum = 0;
    BitMask bitMask = bitMaskModel.getBean();
    if (bitMask != null) {
      bitNum = bitMask.getNumberOfBits();
      label = bitMask.getLabel();
    } else {
      label = "Bit";
    }

    // Create editor components
    comps = new JCheckBox[bitNum];
    for (int i = 0; i < comps.length; i++) {
      String property = BitMask.getPropertyName(i);
      ValueModel model = buffered ? bitMaskModel.getBufferedModel(property) : bitMaskModel.getModel(property);
      comps[i] = BasicComponentFactory.createCheckBox(model, label + i);
      add(comps[i]);
    }

  }

  public void setText(String[] text) {
    for (int index = 0; index < text.length; index++) {
      if (index >= 0 && index < comps.length) {
        comps[index].setText(text[index]);
      }
    }
  }

  public String[] getText() {
    String[] text = new String[comps.length];
    for (int i = 0; i < text.length; i++) {
      text[i] = comps[i].getText();
    }

    return text;
  }

  public void setVisible(int i, boolean visible) {
    if (i >= 0 && i < comps.length) {
      comps[i].setVisible(visible);
    }
  }

}
