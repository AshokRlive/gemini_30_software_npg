/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.misc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;

import com.jgoodies.binding.beans.Model;

/**
 * This class is for grouping JComponents together in order to bind their states
 * to a checkBox.
 */
public class ComponentGroup extends Model implements ActionListener {

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_ENABLED = "enabled";
  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_VISIBLE = "visible";

  private final JComponent[] comps;

  private JCheckBox checkBoxEnable;

  private boolean enabled = true;
  private boolean visible = true;


  public ComponentGroup(JComponent... comps) {
    this.comps = comps;
  }

  public void setEnabled(boolean enabled) {
    if (comps != null) {
      for (JComponent comp : comps) {
        if (comp != null) {
          comp.setEnabled(enabled);
        }
      }
    }

    this.enabled = enabled;
  }

  public void setVisible(boolean visible) {
    if (comps != null) {
      for (JComponent comp : comps) {
        if (comp != null) {
          comp.setVisible(visible);
        }
      }
    }

    this.visible = visible;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public boolean isVisible() {
    return visible;
  }

  public void bindEnableState(JCheckBox checkBox) {
    if (checkBox != null) {
      this.checkBoxEnable = checkBox;
      checkBox.addActionListener(this);
      setEnabled(checkBox.isSelected());
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == checkBoxEnable && checkBoxEnable != null) {
      setEnabled(checkBoxEnable.isSelected());
    }
  }
}
