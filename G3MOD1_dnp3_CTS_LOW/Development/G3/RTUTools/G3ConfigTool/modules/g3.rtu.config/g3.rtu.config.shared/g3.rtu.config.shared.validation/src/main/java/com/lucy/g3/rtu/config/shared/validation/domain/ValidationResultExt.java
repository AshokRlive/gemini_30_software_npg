/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation.domain;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;

public final class ValidationResultExt {

  private ValidationResult result = new ValidationResult();
  private final IValidation validatedItem;


  public ValidationResultExt(IValidation validatedItem) {
    this.validatedItem = validatedItem;
  }

  public IValidation getValidatedItem() {
    return validatedItem;
  }

  public void clear() {
    result = new ValidationResult();
  }

  public boolean isValid() {
    return !result.hasErrors();
  }

  public void addAllFrom(ValidationResultExt result2) {
    this.result.addAllFrom(result2.result);
  }

  public String getTargetName() {
    return validatedItem == null ? "Unkown Target" : validatedItem.getValidator().getTargetName();
  }

  public int getErrorsSize() {
    return getErrors().size();
  }

  public int getWarningsSize() {
    return getWarnings().size();
  }

  // ================== Delegate Methods ===================
  public ValidationResult add(ValidationMessage validationMessage) {
    return result.add(validationMessage);
  }

  public ValidationResult addError(String text) {
    return result.addError(text);
  }

  public ValidationResult addError(String text, Object key) {
    return result.addError(text, key);
  }

  public ValidationResult addWarning(String text) {
    return result.addWarning(text);
  }

  public ValidationResult addWarning(String text, Object key) {
    return result.addWarning(text, key);
  }

  public ValidationResult addInfo(String text) {
    return result.addInfo(text);
  }

  public ValidationResult addInfo(String text, Object key) {
    return result.addInfo(text, key);
  }

  public void addAll(List<ValidationMessage> messages) {
    result.addAll(messages);
  }

  public void addAllFrom(ValidationResult validationResult) {
    result.addAllFrom(validationResult);
  }

  public boolean isEmpty() {
    return result.isEmpty();
  }

  public int size() {
    return result.size();
  }

  public boolean contains(ValidationMessage message) {
    return result.contains(message);
  }

  public ValidationMessage get(int index) {
    return result.get(index);
  }

  public Iterator<ValidationMessage> iterator() {
    return result.iterator();
  }

  public ValidationResult subResult(int fromIndex, int toIndex) {
    return result.subResult(fromIndex, toIndex);
  }

  public ValidationResult subResult(Object messageKey) {
    return result.subResult(messageKey);
  }

  public ValidationResult subResult(Object[] messageKeys) {
    return result.subResult(messageKeys);
  }

  public Map<Object, ValidationResult> keyMap() {
    return result.keyMap();
  }

  public Severity getSeverity() {
    return result.getSeverity();
  }

  public boolean hasMessages() {
    return result.hasMessages();
  }

  public boolean hasErrors() {
    return result.hasErrors();
  }

  public boolean hasWarnings() {
    return result.hasWarnings();
  }

  public boolean hasInfos() {
    return result.hasInfos();
  }

  public List<ValidationMessage> getErrors() {
    return result.getErrors();
  }

  public List<ValidationMessage> getWarnings() {
    return result.getWarnings();
  }

  public List<ValidationMessage> getInfos() {
    return result.getInfos();
  }

  public boolean isModifiable() {
    return result.isModifiable();
  }

  public String getMessagesText() {
    return result.getMessagesText();
  }

  @Override
  public String toString() {
    return result.toString();
  }

  public IValidator getValidator() {
    return validatedItem.getValidator();
  }

  public List<ValidationMessage> getMessages() {
    return result.getMessages();
  }
}
