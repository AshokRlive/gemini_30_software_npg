/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.exceptions;

/**
 * This exception should be thrown when trying to allocate a resource which is
 * not valid.
 */
public class InvalidResourceException extends Exception {

  public InvalidResourceException(String message) {
    super(message);
  }
}
