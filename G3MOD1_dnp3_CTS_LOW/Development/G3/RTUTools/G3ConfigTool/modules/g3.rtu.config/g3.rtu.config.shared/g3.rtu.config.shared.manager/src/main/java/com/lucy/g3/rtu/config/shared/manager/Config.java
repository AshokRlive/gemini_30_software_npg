/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * The implementation of <code>IConfig</code>.
 */
public final class Config implements IConfig {

  private final HashMap<String, IConfigModule> modulesMap = new HashMap<>();
  private final IConfigFactory factory;
  private Logger log = Logger.getLogger(Config.class);

  Config(IConfigFactory factory) {
    this.factory = factory;
    String[] moduleIDs = factory.getAllModuleIDs();
    for (int i = 0; i < moduleIDs.length; i++) {
      log.info("Create the module :"+moduleIDs[i]);
      if(modulesMap.get(moduleIDs[i]) == null)
        modulesMap.put(moduleIDs[i], factory.createModule(this, moduleIDs[i]));
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  synchronized public <T extends IConfigModule> T getConfigModule(String id) {
    IConfigModule module = modulesMap.get(id);
    if (module == null) {
      log.warn("Create the module that is not found:"+id);
      module = factory.createModule(this, id);
      modulesMap.put(id, module);
    }
    
    if(module == null) {
      throw new IllegalStateException(String.format("ConfigModule:\"%s\" is not created., "
          + "Pelase make sure ConfigPlugin.init() has been called before this.",id));
    }

    return (T)module;
  }

  @Override
  synchronized public Collection<IConfigModule> getAllConfigModules() {
    return  new ArrayList<IConfigModule>(modulesMap.values());
  }

}
