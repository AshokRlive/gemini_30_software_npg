/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.bean;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

import com.jgoodies.common.base.Strings;
import com.l2fprod.common.beans.BaseBeanInfo;
import com.l2fprod.common.beans.ExtendedPropertyDescriptor;

/**
 * Extended BeanInfo for supporting loading bean info from resource property
 * files.
 */
public class BeanInfo extends BaseBeanInfo {

  private final ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(getClass());


  public BeanInfo(Class<?> type) {
    super(type);
  }

  public ExtendedPropertyDescriptor addProperty(String propertyName, Class<?> tableCellRendererClass) {
    ExtendedPropertyDescriptor descriptor = addProperty(propertyName);
    descriptor.setPropertyTableRendererClass(tableCellRendererClass);
    return descriptor;
  }

  @Override
  public ExtendedPropertyDescriptor addProperty(String propertyName) {
    ExtendedPropertyDescriptor decriptor = super.addProperty(propertyName);
    readInfoFromResource(decriptor, propertyName);
    return decriptor;
  }

  public ExtendedPropertyDescriptor addSubProperty(ExtendedPropertyDescriptor parent, String propertyName) {
    ExtendedPropertyDescriptor decriptor = super.addSubProperty(parent, propertyName);
    readInfoFromResource(decriptor, propertyName);
    return decriptor;
  }

  private void readInfoFromResource(ExtendedPropertyDescriptor decriptor, String propertyName) {
    String cat = resourceMap.getString(propertyName + ".category");
    if (!Strings.isBlank(cat)) {
      decriptor.setCategory(cat);
    }

    String name = resourceMap.getString(propertyName + ".display");
    if (!Strings.isBlank(name)) {
      decriptor.setDisplayName(name);
    }

    String desp = resourceMap.getString(propertyName + ".desp");
    if (!Strings.isBlank(desp)) {
      decriptor.setShortDescription(desp);
    }
  }
}
