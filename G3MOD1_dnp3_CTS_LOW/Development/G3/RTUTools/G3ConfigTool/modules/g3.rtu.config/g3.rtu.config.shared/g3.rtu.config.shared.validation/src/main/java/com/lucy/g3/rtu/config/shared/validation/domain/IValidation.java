/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation.domain;

/**
 * The object that implements this interface will support validation.
 *
 * @param <T>
 *          the type of validation target.
 */

public interface IValidation {

  /**
   * Gets the validator object.
   *
   * @return validator.
   */
  IValidator getValidator();
  
}
