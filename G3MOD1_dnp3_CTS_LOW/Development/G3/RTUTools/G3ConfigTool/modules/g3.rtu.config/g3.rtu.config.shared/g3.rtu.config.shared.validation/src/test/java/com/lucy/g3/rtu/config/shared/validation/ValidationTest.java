/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public class ValidationTest {

  @Test
  public void testValidator() {
    DummyValidatableItem item = new DummyValidatableItem(0);
    assertTrue("Initial state should valid", item.getValidator().isValid());
    
    item.setProperty(null); // Set invalid property
    item.getValidator().validate();
    assertFalse(item.getValidator().isValid());
    
    item.setProperty("sample");
    item.getValidator().validate();
    assertTrue(item.getValidator().isValid());
  }
  
  @Test
  public void testConainterValidator(){
    DummyValidatableItemManager manager = new DummyValidatableItemManager();
    assertTrue("Initial state should valid",manager.getValidator().isValid()); 
    
    manager.setName(null); // Set invalid name
    manager.getValidator().validate();
    assertFalse(manager.getValidator().isValid());
    assertEquals(1, manager.getValidator().getResult().getErrorsSize());
    assertEquals(1, manager.getValidator().getResults().size());
    
    manager.setName("manager"); // Set valid name
    manager.getValidator().validate();
    assertTrue(manager.getValidator().isValid());
    assertEquals(0, manager.getValidator().getResult().getErrorsSize());
  }
  
  @Test
  public void testConainterValidator2(){
    DummyValidatableItemManager manager = new DummyValidatableItemManager("manager");

    // Add one valid item
    manager.addValidatableItem(createValidItem());
    manager.getValidator().validate();
    assertTrue(manager.getValidator().isValid());
    assertEquals(0, manager.getValidator().getResult().getErrorsSize());
    assertEquals(0, getErrorSize(manager.getValidator().getResults()));
    
    // Add one invalid item
    manager.addValidatableItem(createInvalidItem());
    manager.getValidator().validate();
    assertFalse(manager.getValidator().isValid());
    assertEquals(1, getErrorSize(manager.getValidator().getResults()));
    
    
    // Remove invalid item
    manager.clear();
    manager.getValidator().validate();
    assertTrue(manager.getValidator().isValid());
    assertEquals(0, manager.getValidator().getResult().getErrorsSize());
    assertEquals(0, getErrorSize(manager.getValidator().getResults()));
  }

  
  private int getErrorSize(Collection<ValidationResultExt> results) {
    int size = 0;
    for (ValidationResultExt r : results) {
      size += r.getErrorsSize();
    }
    return size;
  }

  private static DummyValidatableItem createInvalidItem() {
    DummyValidatableItem item = new DummyValidatableItem(0);
    item.setProperty(null); // Set invalid property
    return item;
  }
  
  private static DummyValidatableItem createValidItem() {
    DummyValidatableItem item = new DummyValidatableItem(0);
    item.setProperty("item"); // Set valid property
    return item;
  }
  
}
