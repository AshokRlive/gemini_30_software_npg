
package com.lucy.g3.rtu.config.shared.base.dialogs;

import com.jgoodies.binding.beans.Model;

public class TestBean extends Model {

  /**
   * The name of property {@value} .
   * <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_ENABLED = "enabled";

  private boolean enabled;


  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    Object oldValue = this.enabled;
    this.enabled = enabled;
    firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
  }

}