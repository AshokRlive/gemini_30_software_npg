/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import java.util.HashMap;

import com.lucy.g3.rtu.config.shared.model.IElement;

/**
 * The Class AbstractElement.
 */
abstract class AbstractElement extends DeletableItem implements IElement {

  private String description;
  private String name;

  private final HashMap<String, Object> clientProperties = new HashMap<>();

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public String getName() {
    return name;
  }

  protected void setDescription(String description) {
    Object oldValue = this.description;
    this.description = description;
    firePropertyChange(PROPERTY_DESCRIPTION, oldValue, description);
  }

  protected void setName(String name) {
    Object oldValue = this.name;
    this.name = name;
    firePropertyChange(PROPERTY_NAME, oldValue, name);
  }
  
  public void putProperty(String property, Object value){
    this.clientProperties.put(property, value);
  }
  
  public Object getProperty(String property){
    return this.clientProperties.get(property);
  }
}
