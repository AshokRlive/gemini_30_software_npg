/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator;

import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;


/**
 * The class that implements this interface contains options for generator.
 */
public interface IScadaPointsGenOption {
  
  /**
   * Gets the option for creating points.
   */
  GeneratorOption getDefaultCreateOption(); // getGeneratorOption

  
  enum GeneratorOption {
    DoNotGen,
    GenAlways,
    GenOnlyForce;

    public static GeneratorOption convertFrom(DefaultCreateOption defCreateOption) {
      if(defCreateOption == DefaultCreateOption.CreateVirtualPointAndScadaPoint)
        return GenAlways;
      else
        return DoNotGen; 
    }
  }
}

