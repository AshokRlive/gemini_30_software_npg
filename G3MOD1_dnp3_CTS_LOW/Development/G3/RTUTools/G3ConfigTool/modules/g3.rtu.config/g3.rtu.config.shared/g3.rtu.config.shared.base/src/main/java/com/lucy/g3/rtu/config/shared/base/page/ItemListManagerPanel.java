/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.page;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.jgoodies.forms.factories.Borders;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;

/**
 * A panel for managing a list of items. It contains a button panel for
 * adding/removing/viewing items.
 */
public class ItemListManagerPanel extends javax.swing.JPanel {

  private Logger log = Logger.getLogger(ItemListManagerPanel.class);

  private JScrollPane scrollPane1;
  private JComponent itemList;
  private JPopupMenu listPopupMenu;

  private final Action[] pageActions;
  private final Action viewAction;

  private final SelectionInList<?> selectionInList;
  private final AbstractTableAdapter<?> itemListTableModel;

  private StringFormatter formatter;


  protected ItemListManagerPanel(SelectionInList<?> selectionInList,
      Action[] pageActions,
      Action viewAction) {
    this(selectionInList, null, pageActions, viewAction);
  }

  /**
   * Construct an item list panel which contains a list and a control panel.
   *
   * @param selectionInList
   *          the selection model of all items.
   * @param pageActions
   *          actions for controlling items in this page, e.g.
   *          "Add","Remove",etc.
   * @param viewAction
   *          action for viewing the detail of the selected item.
   */
  protected ItemListManagerPanel(SelectionInList<?> selectionInList,
      AbstractTableAdapter<?> listTableModel,
      Action[] pageActions,
      Action viewAction) {

    this.viewAction = viewAction;
    this.selectionInList = selectionInList;
    this.pageActions = pageActions;
    itemListTableModel = listTableModel;

    initComponents();
  }

  public void setListCellRenderer(ListCellRenderer<? super Object> renderer) {
    if (renderer != null) {
      if (itemList != null && itemList instanceof JList) {
        ((JList<?>) itemList).setCellRenderer(renderer);
      } else {
        log.error("No JList found for setting cell renderer!");
      }
    }
  }

  protected void setListItemFormatter(StringFormatter formatter) {
    this.formatter = formatter;
  }

  private void initComponents() {
    // Create item list table
    if (itemListTableModel != null) {
      itemList = BasicComponentFactory.createTable(selectionInList, itemListTableModel);
      ((JTable) itemList).setFillsViewportHeight(true);
      UIUtils.packTableColumns(((JTable) itemList));
    } else {
      itemList = BasicComponentFactory.createList(selectionInList);
      ((JList<?>) itemList).setCellRenderer(new DefaultItemListRenderer());
    }

    itemList.addMouseListener(new ItemListMouseListener());

    // Binding hot key to remove action
    if (pageActions != null) {
      for (int i = 0; i < pageActions.length; i++) {
        if (pageActions[i] != null) {
          Object key = pageActions[i].getValue(IItemListManagerModel.ACTION_KEY_NAME);
          if (IItemListManagerModel.ACTION_REMOVE_ITEM.equals(key)) {
            itemList.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
            itemList.getActionMap().put("Delete", pageActions[i]);
            break;
          }
        }
      }
    }

    scrollPane1 = new JScrollPane(itemList);

    setLayout(new BorderLayout(5, 5));
    setBorder(Borders.DIALOG_BORDER);

    add(scrollPane1, BorderLayout.CENTER);
    add(createControlPanel(), BorderLayout.EAST);
  }

  private JPopupMenu getPopupMenu() {
    if (listPopupMenu == null) {
      listPopupMenu = new JPopupMenu();
      if (pageActions != null) {
        for (int i = 0; i < pageActions.length; i++) {
          listPopupMenu.add(pageActions[i]);
        }
        listPopupMenu.addSeparator();
      }

      listPopupMenu.add(viewAction);
    }
    return listPopupMenu;
  }

  private JPanel createControlPanel() {
    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    // panelBuilder.setDefaultDialogBorder();
    panelBuilder.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

    if (pageActions != null) {
      for (int i = 0; i < pageActions.length; i++) {
        if (pageActions[i] != null) {
          panelBuilder.addGridded(new JButton(pageActions[i]));
          panelBuilder.addRelatedGap();
        } else {
          panelBuilder.addUnrelatedGap();
        }
      }
    }

    if (viewAction != null) {
      panelBuilder.addGridded(new JButton(viewAction));
    }

    JPanel panel = panelBuilder.getPanel();
    panel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        selectionInList.clearSelection();
      }
    });
    return panel;
  }


  private class ItemListMouseListener extends MouseAdapter {

    /* Double click to navigate to the selection item page. */
    @Override
    public void mouseClicked(MouseEvent e) {
      if (UIUtils.isDoubleClick(e)) {
        if (viewAction != null) {
          viewAction.actionPerformed(null);
        }
      }
    }

    /* Select index on mouse press */
    @Override
    public void mousePressed(MouseEvent e) {
      // int s = itemList.locationToIndex(e.getPoint());
      // itemList.setSelectedIndex(s);

      mayShowPopupMenu(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      mayShowPopupMenu(e);
    }

    private void mayShowPopupMenu(MouseEvent e) {
      if (e.isPopupTrigger() && !selectionInList.isSelectionEmpty()) {
        getPopupMenu().show(e.getComponent(), e.getX(), e.getY());
      }
    }
  }

  private class DefaultItemListRenderer extends DefaultListCellRenderer {

    private Border cellBorder = new EmptyBorder(5, 10, 5, 0);


    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value,
        int index, boolean isSelected, boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index,
          isSelected, cellHasFocus);
      if (formatter != null) {
        setText(formatter.format(value));
      }
      setIconTextGap(10);
      setBorder(cellBorder);
      return comp;
    }

  }
}
