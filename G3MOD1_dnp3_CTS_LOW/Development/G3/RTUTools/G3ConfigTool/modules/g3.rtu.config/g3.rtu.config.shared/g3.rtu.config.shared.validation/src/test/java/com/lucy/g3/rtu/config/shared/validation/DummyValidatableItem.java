/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation;

import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

class DummyValidatableItem implements IValidation {

  private int id;
  private String property;
  private DummyValidator validator;


  public DummyValidatableItem(int id) {
    this.id = id;
    this.validator = new DummyValidator(this);
  }

  @Override
  public IValidator getValidator() {
    return this.validator;
  }

  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return "Example";
  }

  @Override
  public String toString() {
    return String.format("hello");
  }

}
