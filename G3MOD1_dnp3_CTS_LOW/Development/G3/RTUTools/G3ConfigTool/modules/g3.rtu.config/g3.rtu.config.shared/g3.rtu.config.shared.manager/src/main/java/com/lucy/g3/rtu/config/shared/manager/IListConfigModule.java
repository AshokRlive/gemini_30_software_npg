/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import com.lucy.g3.itemlist.IItemListManager;


/**
 * Extended <code>IConfigModule</code> for managing a list of configuration items.
 *
 * @param <T>
 *          the type of managed items.
 */
public interface IListConfigModule<T> extends IConfigModule, IItemListManager<T> {

}

