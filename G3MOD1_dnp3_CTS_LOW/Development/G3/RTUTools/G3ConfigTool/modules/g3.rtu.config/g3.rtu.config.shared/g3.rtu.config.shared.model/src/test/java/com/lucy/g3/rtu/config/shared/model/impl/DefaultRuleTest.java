/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.IRule;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.model.impl.DefaultRule;

/**
 * The Class DefaultRuleTest.
 */
public class DefaultRuleTest {

  private INodeType srcType;
  private INodeType targetType;
  private INode src;
  private INode target;


  @Before
  public void setUp() throws Exception {
    srcType = new INodeType() {
    };
    targetType = new INodeType() {
    };
    src = new NodeStub(srcType);
    target = new NodeStub(targetType);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = NullPointerException.class)
  public void testNullArgument() {
    new DefaultRule(null, 1, null, 1);
  }

  @Test(expected = NullPointerException.class)
  public void testNullArgument1() {
    new DefaultRule(null, 1, new INodeType() {
    }, 1);
  }

  @Test(expected = NullPointerException.class)
  public void testNullArgument2() {
    new DefaultRule(new INodeType() {
    }, 1, null, 1);
  }

  @Test
  public void testCheckConnectible0() throws NotConnectibleException {
    IRule rule = new DefaultRule(srcType, 1, targetType, 1);
    rule.checkConnectible(src, target);
  }

  @Test
  public void testCheckConnectible1() throws NotConnectibleException {
    IRule rule = new DefaultRule(srcType, 100, targetType, 100);
    rule.checkConnectible(src, target);
  }

  @Test(expected = NotConnectibleException.class)
  public void testCheckConnectible2() throws NotConnectibleException {
    IRule rule = new DefaultRule(srcType, 0, targetType, 0);
    rule.checkConnectible(src, target);
  }

  @Test(expected = NotConnectibleException.class)
  public void testCheckConnectible3() throws NotConnectibleException {
    IRule rule = new DefaultRule(srcType, 0, targetType, 1);
    rule.checkConnectible(src, target);
  }

  @Test(expected = NotConnectibleException.class)
  public void testCheckConnectible4() throws NotConnectibleException {
    IRule rule = new DefaultRule(srcType, 1, targetType, 0);
    rule.checkConnectible(src, target);
  }

}
