/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.exceptions;

/**
 * This exception should be thrown when trying to created some duplicated item
 * which is not allowed to.
 */
public class DuplicatedException extends Exception {

  public DuplicatedException(String message) {
    super(message);
  }
}
