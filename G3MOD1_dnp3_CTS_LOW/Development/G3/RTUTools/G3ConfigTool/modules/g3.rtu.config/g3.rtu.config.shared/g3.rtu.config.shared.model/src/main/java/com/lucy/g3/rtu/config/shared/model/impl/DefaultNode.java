/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;

/**
 * Default node is supposed to be connect to only one source. It contains a
 * "source" property by default.
 *
 * @param <SourceT>
 *          the source type
 */
public class DefaultNode<SourceT extends INode> extends AbstractNode {

  public static final String PROPERTY_SOURCE = "source";

  private final PropertyChangeListener sourceNamePCL = new SourceNamePCL();

  private SourceT source;

  private final Class<SourceT> sourceclass;

  private boolean synchName;


  public DefaultNode(INodeType type, Class<SourceT> sourceclass) {
    super(type);
    this.sourceclass = Preconditions.checkNotNull(sourceclass, "sourceclass must not be null");
  }

  /**
   * Synchronises the name of this node to the source it is connected to.
   */
  protected final void syncNameWtihSource() {
    synchName = true;

    if (this.source != null) {
      this.source.addPropertyChangeListener(INode.PROPERTY_NAME, sourceNamePCL);
    }
  }

  protected final void updateName() {
    setName(generateName());
  }

  protected String generateName() {
    return source == null ? null : this.source.getName();
  }

  public SourceT getSource() {
    return source;
  }

  protected void setSource(SourceT source) throws NotConnectibleException {
    if (source == this) {
      throw new NotConnectibleException("source cannot be the same object");
    }

    disconnectFromSource(this.source);
    connectToSource(source);
  }

  private void _setSource(SourceT source) {
    SourceT oldValue = this.source;
    this.source = source;
    firePropertyChange(PROPERTY_SOURCE, oldValue, source);

    if (synchName) {
      if (oldValue != null) {
        oldValue.removePropertyChangeListener(INode.PROPERTY_NAME, sourceNamePCL);
      }

      if (this.source != null) {
        this.source.addPropertyChangeListener(INode.PROPERTY_NAME, sourceNamePCL);
      }

      setName(generateName());
    }
  }

  @Override
  protected void handleConnectEvent(ConnectEvent event) {
    INode source = event.getConnection().getSource();

    if (sourceclass.isInstance(source)) {
      switch (event.getEventType()) {
      case CONNECT_TO_SOURCE:
        _setSource(sourceclass.cast(source));
        break;

      case DISCONNECT_FROM_SOURCE:
        _setSource(null);
        break;

      default:
        break;
      }
    }
  }


  private class SourceNamePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setName(generateName());
    }

  }

}
