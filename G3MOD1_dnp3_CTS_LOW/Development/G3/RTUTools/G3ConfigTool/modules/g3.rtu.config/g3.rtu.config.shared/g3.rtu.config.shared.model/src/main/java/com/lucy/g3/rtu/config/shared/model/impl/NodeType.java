/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import com.lucy.g3.rtu.config.shared.model.INodeType;

/**
 * The Node Type enum used by CofigTool application.
 */
public enum NodeType implements INodeType {
  CHANNEL, VIRTUAL_POINT, MODULE, HMI_SCREEN, SWITCH_OUTPUT, CLOGIC;

  @Override
  public String toString() {
    return name();
  }


  static {

    Rules rules = Rules.INSTANCE;
    rules.registerRule(new DefaultRule(CHANNEL, -1, VIRTUAL_POINT, -1));
    rules.registerRule(new DefaultRule(CHANNEL, -1, CLOGIC, 1));
    rules.registerRule(new DefaultRule(MODULE, -1, CLOGIC, 1));
    rules.registerRule(new DefaultRule(CLOGIC, -1, CLOGIC, 1));
    rules.registerRule(new DefaultRule(VIRTUAL_POINT, -1, CLOGIC, 1));
    rules.registerRule(new DefaultRule(SWITCH_OUTPUT, -1, CLOGIC, 1));
    rules.registerRule(new DefaultRule(VIRTUAL_POINT, -1, HMI_SCREEN, 1));
    rules.registerRule(new DefaultRule(VIRTUAL_POINT, -1, VIRTUAL_POINT, 1));
    rules.registerRule(new DefaultRule(CLOGIC, -1, VIRTUAL_POINT, 1));
    rules.registerRule(new DefaultRule(CLOGIC, -1, HMI_SCREEN, 1));
  }
}
