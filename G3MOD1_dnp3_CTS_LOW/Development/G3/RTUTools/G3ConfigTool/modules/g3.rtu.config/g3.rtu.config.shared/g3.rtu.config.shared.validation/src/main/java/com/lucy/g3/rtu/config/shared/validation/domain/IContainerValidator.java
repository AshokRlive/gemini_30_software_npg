/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.validation.domain;

import java.util.Collection;

/**
 * The validator for item manager.
 */
public interface IContainerValidator extends IValidator {

  /**
   * Gets the validation results of this container and its children.
   * @return
   */
  Collection<ValidationResultExt> getResults();
  
  /**
   * Gets the cleaner for cleaning invalid items.
   * @return
   */
  ICleaner getCleaner();
  
}
