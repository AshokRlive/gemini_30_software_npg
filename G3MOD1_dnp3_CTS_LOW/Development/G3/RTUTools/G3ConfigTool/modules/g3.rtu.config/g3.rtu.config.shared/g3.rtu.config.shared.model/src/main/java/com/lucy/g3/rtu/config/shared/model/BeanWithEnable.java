/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;

import com.jgoodies.binding.beans.Model;

/**
 * This bean class contains "enabled" property by default.
 */
public class BeanWithEnable extends Model {

  /**
   * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
   */
  public static final String PROPERTY_ENABLED = "enabled";

  private boolean enabled;


  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    Object oldValue = this.enabled;
    this.enabled = enabled;
    firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
  }

}
