/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.math;

import java.text.ParseException;

import com.jgoodies.common.base.Preconditions;

/**
 * This class represents a ratio number.
 */
public class Ratio {
  // GEN-BEGIN:
  public final long dividend;
  public final long divisor;
  //GEN-END:

  public Ratio(long dividend, long divisor) {
    super();
    Preconditions.checkArgument(divisor > 0, "divisor  must not be  0");
    this.dividend = dividend;
    this.divisor = divisor;
  }



  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (dividend ^ (dividend >>> 32));
    result = prime * result + (int) (divisor ^ (divisor >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Ratio other = (Ratio) obj;
    if (dividend != other.dividend)
      return false;
    if (divisor != other.divisor)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return getText();
  }
  
  public String getText() {
    return String.format("%d:%d", dividend, divisor);
  }
  
  public static Ratio parseFromString(String ratio) throws ParseException {
    Preconditions.checkNotBlank(ratio, "ratio must not be blank");
    long dividend;
    long divisor;
    int columnPos = ratio.indexOf(':');
    Preconditions.checkState(columnPos > 0, "\":\" is not found");
    try {
      String dividentStr = ratio.substring(0, columnPos);
      String divisorStr  = ratio.substring(columnPos + 1, ratio.length());
      
      dividend = Long.parseLong(dividentStr);
      divisor  = Long.parseLong(divisorStr);
      return new Ratio(dividend, divisor);
    } catch (Exception e) {
      throw new ParseException("Invalid ratio: "+ratio,0);
    }
    
  }
}
