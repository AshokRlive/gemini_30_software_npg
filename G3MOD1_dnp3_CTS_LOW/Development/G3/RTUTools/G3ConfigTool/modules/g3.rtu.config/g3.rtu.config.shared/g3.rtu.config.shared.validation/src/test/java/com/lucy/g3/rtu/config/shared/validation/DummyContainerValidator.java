
package com.lucy.g3.rtu.config.shared.validation;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ICleaner;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

class DummyContainerValidator extends AbstractContainerValidator {

  private DummyValidatableItemManager manager;


  DummyContainerValidator(DummyValidatableItemManager manager) {
    super(manager);
    this.manager = manager;
  }

  @Override
  public Collection<IValidation> getValidatableChildrenItems() {
    return new ArrayList<IValidation>(manager.getAllItems());
  }

  @Override
  public String getTargetName() {
    return manager.getName();
  }

  @Override
  public ICleaner getCleaner() {
    return null;
  }

  @Override
  protected void validate(ValidationResultExt result) {
    if(manager.getName() == null)
      result.addError("The name of manager must not be null");
  }

}