/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;

import java.util.Collection;

/**
 * A node is an element that could have relationship with each other.
 */
public interface INode extends IElement, IDeletable {

  Collection<IConnection> getAllConnections();

  INodeType getNodeType();

  IConnection connectToSource(INode source) throws NotConnectibleException;

  IConnection connectToTarget(INode target) throws NotConnectibleException;

  void disconnectFromSource(INode source);

  void disconnectFromTarget(INode target);

  int getNumberOfSources(INodeType sourceType);

  int getNumberOfTargets(INodeType targetType);

  boolean isConnectedToSource(INodeType sourceType);

  boolean isConnectedToTarget(INodeType targetType);

  boolean isConnectedToAnyTarget();

  boolean isConnectedToAnySource();

}
