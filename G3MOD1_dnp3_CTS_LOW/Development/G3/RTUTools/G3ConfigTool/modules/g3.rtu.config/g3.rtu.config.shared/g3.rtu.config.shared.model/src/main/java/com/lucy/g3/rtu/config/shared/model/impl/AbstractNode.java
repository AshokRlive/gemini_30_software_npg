/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.model.IConnection;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.IRule;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.model.impl.ConnectEvent.ConnectEventType;

/**
 * The Class Node.
 */
public abstract class AbstractNode extends AbstractElement implements INode {

  private final ArrayList<IConnection> connections = new ArrayList<IConnection>();

  private final INodeType typeId;


  public AbstractNode(INodeType type) {
    this.typeId = Preconditions.checkNotNull(type, "type must not be null");
  }

  private void addConnection(Connection connection) {
    if (!connections.contains(connection)) {
      connections.add(connection);
    }
  }

  private void removeConnection(Connection connection) {
    connections.remove(connection);
  }

  @Override
  public void delete() {
    super.delete();

    Collection<IConnection> conns = getAllConnections();
    for (IConnection conn : conns) {
      conn.delete();
    }

    PropertyChangeListener[] pcls = getPropertyChangeListeners();
    for (PropertyChangeListener pcl : pcls) {
      removePropertyChangeListener(pcl);
    }

  }

  @Override
  public Collection<IConnection> getAllConnections() {
    return new ArrayList<IConnection>(connections);
  }

  @Override
  public int getNumberOfSources(INodeType sourceType) {
    Collection<IConnection> conns = getAllConnections();
    int num = 0;
    for (IConnection conn : conns) {
      if (conn.getTarget() == this
          && (sourceType == null || conn.getSource().getNodeType() == sourceType)) {
        num++;
      }
    }
    return num;
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public int getNumberOfTargets(INodeType targetType) {
    Collection<IConnection> conns = getAllConnections();
    int num = 0;
    for (IConnection conn : conns) {
      if (conn.getSource() == this
          && (targetType == null || conn.getTarget().getNodeType() == targetType)) {
        num++;
      }
    }
    return num;
  }

  @Override
  public final INodeType getNodeType() {
    return typeId;
  }

  @Override
  public IConnection connectToSource(INode source) throws NotConnectibleException {
    if (source == this) {
      throw new NotConnectibleException("Cannot connect to self");
    }

    if (source != null) {
      return makeConnection((AbstractNode) source, this);
    }
    return null;
  }

  @Override
  public IConnection connectToTarget(INode target) throws NotConnectibleException {
    if (target == this) {
      throw new NotConnectibleException("Cannot connect to self");
    }

    if (target != null) {
      return makeConnection(this, (AbstractNode) target);
    }
    return null;
  }

  @Override
  public void disconnectFromSource(INode source) {
    if (source == null) {
      return;
    }

    Collection<IConnection> conns = getAllConnections();
    for (IConnection conn : conns) {
      if (conn.getSource() == source) {
        conn.delete();
        break;
      }
    }

  }

  @Override
  public void disconnectFromTarget(INode target) {
    if (target == null) {
      return;
    }

    Collection<IConnection> conns = getAllConnections();
    for (IConnection conn : conns) {
      if (conn.getTarget() == target) {
        conn.delete();
        break;
      }
    }
  }

  @Override
  public boolean isConnectedToSource(INodeType sourceType) {
    return getNumberOfSources(sourceType) > 0;
  }

  @Override
  public boolean isConnectedToTarget(INodeType targetType) {
    return getNumberOfTargets(targetType) > 0;
  }

  @Override
  public boolean isConnectedToAnyTarget() {
    return isConnectedToTarget(null);
  }

  @Override
  public boolean isConnectedToAnySource() {
    return isConnectedToSource(null);
  }

  protected abstract void handleConnectEvent(ConnectEvent event);


  private static final class Connection extends AbstractElement implements IConnection {

    private AbstractNode source;
    private AbstractNode target;


    private Connection(AbstractNode source, AbstractNode target) {
      this.source = source;
      this.target = target;
    }

    @Override
    public INode getSource() {
      return source;
    }

    @Override
    public INode getTarget() {
      return target;
    }

    @Override
    public void delete() {
      super.delete();

      source.removeConnection(this);
      target.removeConnection(this);
      source.handleConnectEvent(new ConnectEvent(ConnectEventType.DISCONNECT_FROM_TARGET, this));
      target.handleConnectEvent(new ConnectEvent(ConnectEventType.DISCONNECT_FROM_SOURCE, this));
      source = null;
      target = null;
    }

    @Override
    public String toString() {
      return String.format("Connection[From \"%s\" to \"%s\"]", source, target);
    }

  }


  private static Connection makeConnection(AbstractNode source, AbstractNode target) throws NotConnectibleException {
    Preconditions.checkNotNull(source, "source must not be null");
    Preconditions.checkNotNull(target, "target must not be null");

    /* Checking rules */
    IRule rule = Rules.INSTANCE.getRule(source.getNodeType(), target.getNodeType());
    if (rule == null) {
      throw new NotConnectibleException("No rule defined for nodes. Source:" + source.getNodeType() + " target:"
          + target.getNodeType());
    }
    rule.checkConnectible(source, target);

    /* Making connection */
    Connection conn = new Connection(source, target);
    source.addConnection(conn);
    target.addConnection(conn);
    source.handleConnectEvent(new ConnectEvent(ConnectEventType.CONNECT_TO_TARGET, conn));
    target.handleConnectEvent(new ConnectEvent(ConnectEventType.CONNECT_TO_SOURCE, conn));

    return conn;
  }

}
