/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import com.jgoodies.common.bean.ObservableBean2;


/**
 * For managing the instance of <code>IConfig</code>.
 */
public interface IConfigManager extends ObservableBean2{

  String PROPERTY_LOCAL_CONFIG = "localConfig";
  
  String PROPERTY_REMOTE_CONFIG = "remoteConfig";

  String PROPERTY_LOCAL_CONFIG_EXIST = "localConfigExist";
  
  String PROPERTY_REMOTE_CONFIG_EXIST = "remoteConfigExist";
  

  IConfig getLocalConfig();
  
  IConfig getRemoteConfig();
  
  boolean isLocalConfigExist();
  
  boolean isRemoteConfigExist();
}

