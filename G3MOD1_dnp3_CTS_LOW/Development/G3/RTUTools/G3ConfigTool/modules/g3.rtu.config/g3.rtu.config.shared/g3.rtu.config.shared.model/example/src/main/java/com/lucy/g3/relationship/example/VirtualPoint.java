package com.lucy.g3.relationship.example;

import com.lucy.g3.relationship.NotConnectibleException;
import com.lucy.g3.relationship.impl.DefaultNode;


public class VirtualPoint extends DefaultNode<Channel> {
    
    public VirtualPoint() {
        this("VirtualPoint");
    }
    public VirtualPoint(String descrption) {
        super(NodeType.VIRUAL_POINT, Channel.class);
        setDescription(descrption);
    }
    @Override
    public void setSource(Channel source) throws NotConnectibleException {
        super.setSource(source);
    }

}

