/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.dialogs;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;

/**
 * Basic implementation of {@link IEditorDialogInvoker}.
 *
 * @param <ItemT>
 *          the type of item to be edited
 */
public abstract class AbstractEditorDialogInvoker<ItemT> implements IEditorDialogInvoker<ItemT> {

  private final ItemT initial;
  private final ListModel<ItemT> pointsList;
  private final ListSelectionModel selectionModel;
  private int selectionIndex;


  @SuppressWarnings("unchecked")
  public AbstractEditorDialogInvoker(ItemT editItem) {
    this.initial = Preconditions.checkNotNull(editItem, "editItem must not be null");
    this.pointsList = new ArrayListModel<ItemT>(0);
    selectionModel = null;
  }

  public AbstractEditorDialogInvoker(ListModel<ItemT> pointsList, ListSelectionModel selectionModel,
      int initialSelectionIndex) {
    this.pointsList = Preconditions.checkNotNull(pointsList, "pointsList must not be null");
    this.selectionModel = selectionModel;

    Preconditions.checkArgument(initialSelectionIndex >= 0, "initialSelectionIndex must be >= 0");
    this.selectionIndex = initialSelectionIndex;

    initial = pointsList.getElementAt(initialSelectionIndex);
    Preconditions.checkNotNull(initial, "initial must not be null");
  }

  @Override
  public ItemT initial() {
    return initial;
  }

  protected int getSelectionIndex() {
    return selectionIndex;
  }

  @Override
  public ItemT next() {
    int totalNum = pointsList.getSize();
    int newIdx = selectionIndex + 1;

    if (newIdx >= 0 && newIdx < totalNum) {
      ItemT newSel = pointsList.getElementAt(newIdx);

      if (newSel != null && initial.getClass() == newSel.getClass()) {
        updateSelectionModel(newIdx);
        selectionIndex = newIdx;
        return newSel;
      }
    }

    return null;
  }

  @Override
  public ItemT prev() {
    int totalNum = pointsList.getSize();
    int newIdx = selectionIndex - 1;

    if (newIdx >= 0 && newIdx < totalNum) {
      ItemT newSel = pointsList.getElementAt(newIdx);

      if (newSel != null && initial.getClass() == newSel.getClass()) {
        updateSelectionModel(newIdx);
        selectionIndex = newIdx;
        return newSel;
      }
    }

    return null;
  }

  private void updateSelectionModel(int newIdx) {
    if (selectionModel != null) {
      selectionModel.setSelectionInterval(newIdx, newIdx);
    }
  }
}
