/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.dialogs;

/**
 * This interface should be implemented by the class that invokes editor
 * dialog(such as {@link AbstractDialog}).
 *
 * @param <ItemT>
 *          the type of object to be edited
 */
public interface IEditorDialogInvoker<ItemT> {

  /**
   * Gets the first item to be edited.
   *
   * @return non-null item for editing
   */
  ItemT initial();

  /**
   * Gets the next item to be edited.
   *
   * @return non-null item for editing
   */
  ItemT next();

  /**
   * Gets the previous item to be edited.
   *
   * @return non-null item for editing
   */
  ItemT prev();

  /**
   * This method would be called once the editing item has been changed.
   *
   * @param editingItem
   *          the item that has changes.
   */
  void fireEditingItemChanged();
}
