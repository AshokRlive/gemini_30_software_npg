/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import com.jgoodies.binding.beans.Model;



/**
 * Implementation of IConfigManager.
 */
public final class ConfigManager extends Model implements IConfigManager {
  
  private IConfig localConfig = null;
  private boolean localConfigExist = false;
  
  private IConfig remoteConfig = null;
  private boolean remoteConfigExist = false;
  
  
  @Override
  public boolean isLocalConfigExist() {
    return localConfigExist;
  }

  @Override
  public boolean isRemoteConfigExist() {
    return remoteConfigExist;
  }
  
  @Override
  public IConfig getLocalConfig() {
    return localConfig;
  }

  
  private void setLocalConfigExist(boolean localConfigExist) {
    Object oldValue = this.localConfigExist;
    this.localConfigExist = localConfigExist;
    firePropertyChange(PROPERTY_LOCAL_CONFIG_EXIST, oldValue, localConfigExist);
  }

  
  private void setRemoteConfigExist(boolean remoteConfigExist) {
    Object oldValue = this.remoteConfigExist;
    this.remoteConfigExist = remoteConfigExist;
    firePropertyChange(PROPERTY_REMOTE_CONFIG_EXIST, oldValue, remoteConfigExist);
  }

  public void setLocalConfig(IConfig localConfig) {
    Object oldValue = this.localConfig;
    this.localConfig = localConfig;
    setLocalConfigExist(this.localConfig != null);
    firePropertyChange(PROPERTY_LOCAL_CONFIG, oldValue, localConfig);
  }

  
  @Override
  public IConfig getRemoteConfig() {
    return remoteConfig;
  }
  
  public void setRemoteConfig(IConfig remoteConfig) {
    Object oldValue = this.remoteConfig;
    this.remoteConfig = remoteConfig;
    setRemoteConfigExist(this.remoteConfig != null);
    firePropertyChange(PROPERTY_REMOTE_CONFIG, oldValue, remoteConfig);
  }

  // ================== Singleton ===================
  private ConfigManager() {}
  
  private static ConfigManager INSTANCE = new ConfigManager();
  
  public static ConfigManager getInstance() {
    return INSTANCE;
  }

}

