/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import java.util.Collection;

/**
 * IConfig is intended for holding all configuration modules.
 */
public interface IConfig {

  <T extends IConfigModule> T getConfigModule(String id);
  
  Collection<IConfigModule> getAllConfigModules();
}

