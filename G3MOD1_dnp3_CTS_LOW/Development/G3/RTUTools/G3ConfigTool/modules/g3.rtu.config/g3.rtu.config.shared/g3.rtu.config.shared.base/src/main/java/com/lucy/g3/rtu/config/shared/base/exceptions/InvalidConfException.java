/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.exceptions;

import com.jgoodies.validation.ValidationResult;

/**
 * The exception that indicates configuration to be loaded is invalid.
 */
public class InvalidConfException extends Exception {

  private ValidationResult result;


  /**
   * Constructs a new exception instance with the specified detail message. The
   * cause is not initialised.
   *
   * @param message
   *          the detail message which is saved for later retrieval by the
   *          {@link #getMessage()} method.
   */
  public InvalidConfException(String message) {
    super(message);
  }

  public InvalidConfException(Throwable cause, String message) {
    super(message, cause);
  }

  /**
   * Constructs a new exception instance with the specified detail message and
   * cause.
   *
   * @param message
   *          the detail message which is saved for later retrieval by the
   *          {@link #getMessage()} method.
   * @param cause
   *          the cause which is saved for later retrieval by the
   *          {@link #getCause()} method. A {@code null} value is permitted, and
   *          indicates that the cause is nonexistent or unknown.
   */
  public InvalidConfException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidConfException(ValidationResult result) {
    this.result = result;
  }

  public ValidationResult getResult() {
    return result;
  }

}