/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator;

import org.junit.Test;

import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;

import static org.junit.Assert.*;


/**
 *
 */
public class ConfigGeneratorRegistryTest {

  @Test
  public void test() {
    ConfigGeneratorRegistry reg = ConfigGeneratorRegistry.getInstance();
    
   
    
    reg.registerGenerator(IDummyGenerator.class, new DummyGenerator());
    assertNotNull(reg.getGenerator(null, IDummyGenerator.class));
    
  }

}

