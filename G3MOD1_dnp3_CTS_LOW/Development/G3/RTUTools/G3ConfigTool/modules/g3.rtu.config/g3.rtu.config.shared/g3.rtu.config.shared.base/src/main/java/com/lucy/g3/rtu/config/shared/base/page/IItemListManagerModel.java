/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.page;

import javax.swing.Action;

import com.jgoodies.binding.list.SelectionInList;

/**
 * The interface of presentation model used by {@linkplain ItemListManagerPage}.
 */
public interface IItemListManagerModel {

  String ACTION_KEY_NAME = "actionkeyName";

  /**
   * The key of action {@value} .
   */
  String ACTION_ADD_ITEM = "addItem";

  /**
   * The key of action {@value} .
   */
  String ACTION_REMOVE_ITEM = "removeItem";


  /**
   * Gets selection model.
   *
   * @return non-null selectionInList
   */
  SelectionInList<?> getSelectionInList();

  /**
   * Gets actions for managing item list. e.g. "Add", "Remove".
   *
   * @return available actions for the page.
   */
  Action[] getPageActions();

  /**
   * Gets actions for tree node context menu.
   *
   * @return available actions for the tree node.
   */
  Action[] getNodeActions();

  String getDisplayText(Object item);

  /**
   * Adds a new item to the list.
   */
  @org.jdesktop.application.Action
  void addItem();

  /**
   * Removes current selected item from the list.
   */
  @org.jdesktop.application.Action
  void removeItem();
}
