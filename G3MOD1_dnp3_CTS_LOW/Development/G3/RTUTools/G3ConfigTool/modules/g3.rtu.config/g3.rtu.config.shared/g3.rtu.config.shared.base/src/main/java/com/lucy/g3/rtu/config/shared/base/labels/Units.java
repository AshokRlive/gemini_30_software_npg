/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.labels;


/**
 * String constants of Unit.
 */
public interface Units {
  String MS   = "ms";
  String MINS = "mins";
  String SECS = "secs";
  String NONE = "";
}

