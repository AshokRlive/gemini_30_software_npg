/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.page;

import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

public abstract class AbstractConfigPage extends AbstractPage{
  
  
  public AbstractConfigPage() {
    super();
  }

  public AbstractConfigPage(Object data, String description) {
    super(data, description);
  }

  public AbstractConfigPage(Object data) {
    super(data);
  }

  @Override
  public final boolean hasError() {
    Object data = getData();
    if(data != null && data instanceof IValidation) { 
      IValidator validator = ((IValidation)data).getValidator();
      if(validator != null)
        return !validator.isValid();
    }
    return false;
  }
}

