/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;


/**
 * The Class DisallowDeleteExceptoin.
 */
public class DeleteExceptoin extends RuntimeException {

  public DeleteExceptoin() {
    super();
    
  }

  public DeleteExceptoin(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    
  }

  public DeleteExceptoin(String message, Throwable cause) {
    super(message, cause);
    
  }

  public DeleteExceptoin(String message) {
    super(message);
    
  }

  public DeleteExceptoin(Throwable cause) {
    super(cause);
    
  }

}

