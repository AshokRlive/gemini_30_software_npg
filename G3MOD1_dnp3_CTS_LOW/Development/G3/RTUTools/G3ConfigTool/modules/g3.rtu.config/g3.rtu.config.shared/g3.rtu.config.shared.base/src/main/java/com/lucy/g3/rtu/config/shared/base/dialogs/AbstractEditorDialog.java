/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.dialogs;

import java.awt.Component;
import java.awt.Frame;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.ButtonBarBuilder2;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.jgoodies.validation.ValidationResult;
import com.jgoodies.validation.ValidationResultModel;
import com.jgoodies.validation.util.DefaultValidationResultModel;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator.NullValidator;

/**
 * A super dialog for editing an item.
 *
 * @param <ItemT>
 *          the type of the item to be edited
 */
public abstract class AbstractEditorDialog<ItemT> extends AbstractDialog {

  public static final String ACTION_KEY_RESET = "reset";
  public static final String ACTION_KEY_APPLY = "apply";
  public static final String ACTION_KEY_NEXT = "next";
  public static final String ACTION_KEY_PREV = "prev";
  
  public static final String PROPERTY_BUFFERING = "buffering";

  private Logger log = Logger.getLogger(AbstractEditorDialog.class);

  // Children presentation models
  private final ArrayList<PresentationModel<?>> childrenPms = new ArrayList<PresentationModel<?>>();

  // The main presentation model of the editing item.
  private final PresentationModel<ItemT> mainPm;

  private ItemT editItem;

  private BufferValidator validator;
  private final ValidationResultModel validationModel = new DefaultValidationResultModel();

  private final IEditorDialogInvoker<ItemT> invoker;
  
  private boolean buffering;
  
  
  private final FocusChangeHandler focusHandler = new FocusChangeHandler();
  private final PropertyChangeListener bufferingPCL = new PropertyChangeListener() {
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateBuffering();
    }

  };


  public AbstractEditorDialog(Frame parent, ItemT editItem) {
    super(parent, true);
    this.editItem = Preconditions.checkNotNull(editItem, "editItem must not be null");
    this.mainPm = new PresentationModel<ItemT>(editItem);
    this.invoker = null;
    initEventHandling();
  }

  public AbstractEditorDialog(Frame parent, IEditorDialogInvoker<ItemT> invoker) {
    super(parent, true);
    this.invoker = Preconditions.checkNotNull(invoker, "invoker must not be null");
    this.editItem = Preconditions.checkNotNull(invoker.initial(), "initial item must not be null");
    this.mainPm = new PresentationModel<ItemT>(editItem);
    initEventHandling();
  }

  private boolean checkModelBuffered() {
    if (mainPm.isBuffering())
      return true;
    
    for (PresentationModel<?> childPM : childrenPms) {
      if (childPM.isBuffering()) {
        return true;
      }
    }
    
    return false;
  }
  
  private void updateBuffering() {
    firePropertyChange(PROPERTY_BUFFERING, buffering, buffering = checkModelBuffered());
  }
  
  /**
   * Gets the presentation model of the current editing item.
   *
   * @return non-null presentation model.
   */
  public final PresentationModel<ItemT> getModel() {
    return mainPm;
  }

  /**
   * Gets the editing item.
   *
   * @return non-null item object.
   */
  public final ItemT getEditItem() {
    return mainPm.getBean();
  }

  /**
   * Creates a child presentation model.
   */
  protected final <BeanT> PresentationModel<BeanT> createPresentationModel(BeanT bean) {
    PresentationModel<BeanT> beanPM = new PresentationModel<BeanT>(bean);
    childrenPms.add(beanPM);
    beanPM.addPropertyChangeListener(PresentationModel.PROPERTY_BUFFERING, bufferingPCL);
    return beanPM;
  }

  /**
   * Gets the validator for this dialog.
   */
  protected abstract BufferValidator createValidator(PresentationModel<ItemT> target);

  /**
   * This method would be called when the editing item is set to a new value.
   */
  protected abstract void editingItemChanged(ItemT newItem);

  /**
   * Gets the title for the top banner. If null, the banner will not be created.
   * By default the title is null. Override this method if we need to display
   * the banner.
   */
  protected String getBannerTitle() {
    return null;
  }

  /**
   * Gets the subtitle for the top banner. By default the title is null.
   * Override this method if we need to display the subtitle.
   */
  protected String getBannerSubtitle() {
    return null;
  }

  /**
   * Gets the icon for the top banner. Override this method if we need to
   * display the icon.
   */
  protected ImageIcon getBannerIcon() {
    return null;
  }

  private void initEventHandling() {
    // Observe the focused component and update subtitle hint
    KeyboardFocusManager.getCurrentKeyboardFocusManager()
        .addPropertyChangeListener(focusHandler);

    /* Set default actions */
    setDefaultCancelAction(getAction(ACTION_KEY_CANCEL));
    setDefaultAction(getAction(ACTION_KEY_OK));
    
    mainPm.addPropertyChangeListener(PresentationModel.PROPERTY_BUFFERING, bufferingPCL);
  }
  
  private BufferValidator getValidator() {
    if (validator == null) {
      validator = createValidator(mainPm);

      if (validator == null) {
        validator = new NullValidator();
      }
    }

    return validator;
  }

  private void doValidation() {
    /* Do validation and update results in model */
    ValidationResult newResult = new ValidationResult();
    getValidator().validate(newResult);

    if (newResult != null) {
      validationModel.setResult(newResult);
    }

    /* Update GUI upon validation result model */
    if (getContentPanel() != null) {
      ValidationComponentUtils.updateComponentTreeSeverityBackground(
          getContentPanel(),
          validationModel.getResult());
    }
  }

  @Override
  public void setVisible(boolean visible) {
    if (visible) {
      doValidation();
      updateBuffering();
    }
    super.setVisible(visible);
    
    
    assert !isBuffering();
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_BUFFERING)
  public final boolean apply() {
    doValidation();

    if (validationModel.hasErrors()) {
      BufferValidator.showErrorReport(this, validationModel);
      return false;
    } else {
      commit();
      return true;
    }
  }

  @Override
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_OK_ENABLEMENT)
  public final void ok() {
    doValidation();

    if (validationModel.hasErrors()) {
      BufferValidator.showErrorReport(this, validationModel);
    } else {
      super.ok();// close dialog first
      commit();
      release();
    }
  }

  @org.jdesktop.application.Action
  public final void reset() {
    flush();
    doValidation();
  }

  @Override
  public final void cancel() {
    super.cancel();
    release();
    KeyboardFocusManager.getCurrentKeyboardFocusManager()
        .removePropertyChangeListener(focusHandler);
  }

  @org.jdesktop.application.Action
  public boolean next() {
    /* Save current change */
    askUserSaveChange();

    if (invoker != null) {
      setPoint(invoker.next());
      return true;
    } else {
      log.error("Invoker is null");
      return false;
    }
  }

  @org.jdesktop.application.Action
  public boolean prev() {
    /* Save current change */
    askUserSaveChange();

    if (invoker != null) {
      setPoint(invoker.prev());
      return true;
    } else {
      log.error("Invoker is null");
      return false;
    }
  }

  private void setPoint(ItemT newItem) {
    if (newItem != null) {
      /* Set new item for editing */
      this.editItem = newItem;
      this.mainPm.setBean(this.editItem);
      editingItemChanged(this.editItem);
      flush();
    } else {
      log.error("The new edit item is not allowed to be null!");
    }
  }

  private void commit() {
    for (PresentationModel<?> pm : childrenPms) {
      pm.triggerCommit();
    }
    mainPm.triggerCommit();

    if (invoker != null) {
      invoker.fireEditingItemChanged();
    }

    // Check committed successfully
    boolean success = true;
    for (PresentationModel<?> pm : childrenPms) {
      if (pm.isBuffering()) {
        success = false;
        break;
      }
    }
    if (mainPm.isBuffering()) {
      success = false;
    }

    if (!success) {
      log.fatal("Failed to commit all buffered value");
    }

  }

  /**
   * Removes listeners from the dialog's that are created by the
   * PresentationModel when requesting adapting ValueModels via
   * <code>#getModel</code> or <code>#getBufferedModel</code>. Setting the
   * presentation model's bean to {@code null} removes all these listeners.
   */
  private void release() {
    for (PresentationModel<?> pm : childrenPms) {
      pm.release();
    }

    mainPm.release();
  }

  private void flush() {
    for (PresentationModel<?> pm : childrenPms) {
      pm.triggerFlush();
    }

    mainPm.triggerFlush();
  }

  private void askUserSaveChange() {
    /* Check if there is any change in all models */
    if (isBuffering()) {
      int ret = JOptionPane.showConfirmDialog(this,
          "Do you want to save changes?",
          "Save", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

      if (ret == JOptionPane.YES_OPTION) {
        apply();
        return;
      }
    }

    flush(); // Discard changes
  }
  
  public boolean isBuffering() {
    return buffering;
  }

  @Override
  protected final BannerPanel createBannerPanel() {
    String title = getBannerTitle();
    if (title != null) {
      BannerPanel banner = new BannerPanel();
      banner.setIcon(getBannerIcon());
      banner.setTitle(String.format("<html><h3>%s</h3><html>", getBannerTitle()));
      banner.setSubtitle(title);
      return banner;
    } else {
      return null;
    }
  }

  @Override
  protected final JPanel createButtonPanel() {
    JButton cancelBtn = new JButton(getAction(ACTION_KEY_CANCEL));
    JButton okBtn = new JButton(getAction(ACTION_KEY_OK));
    JButton helpBtn = Helper.createHelpButton(editItem.getClass());

    if (invoker == null) {
      /* Create Button bar :Help|Reset|OK|Cancel */
      return ButtonBarFactory.buildHelpOKCancelApplyBar(
          helpBtn,
          new JButton(getAction(ACTION_KEY_RESET)),
          okBtn,
          cancelBtn);

    } else {
      /* Create Button bar :Help|Prev|Next|Apply|OK|Cancel */
      JButton applyBtn = new JButton(getAction(ACTION_KEY_APPLY));
      JButton nextBtn = new JButton(getAction(ACTION_KEY_NEXT));
      JButton prevBtn = new JButton(getAction(ACTION_KEY_PREV));

      ButtonBarBuilder2 builder = new ButtonBarBuilder2();
      builder.addFixed(helpBtn);

      builder.addRelatedGap();

      builder.addGlue();
      builder.addFixed(prevBtn);
      builder.addRelatedGap();
      builder.addFixed(nextBtn);
      builder.addRelatedGap();
      builder.addButton(applyBtn, okBtn, cancelBtn);

      return builder.getPanel();
    }
  }


  /**
   * Displays an input hint for components that get the focus permanently.
   */
  private final class FocusChangeHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      String propertyName = evt.getPropertyName();
      if (!"permanentFocusOwner".equals(propertyName)) {
        return;
      }

      Component focusOwner = KeyboardFocusManager
          .getCurrentKeyboardFocusManager().getFocusOwner();

      String focusHint = focusOwner instanceof JComponent
          ? (String) ValidationComponentUtils
              .getInputHint((JComponent) focusOwner)
          : null;
      if (focusHint != null && !focusHint.isEmpty()) {
        showHint(focusHint);
      } else {
        showDefault();
      }
    }

    private void showHint(String hint) {
      BannerPanel banner = getBannerPanel();
      if (banner != null) {
        banner.setSubtitle(hint);
        banner.setIconVisible(false);
      }
    }

    private void showDefault() {
      BannerPanel banner = getBannerPanel();
      if (banner != null) {
        banner.setSubtitle(getBannerSubtitle());
        banner.setIconVisible(true);
      }
    }
  }


  public void showDialog() {
    showDialog(this);
  }

  protected static void showDialog(JDialog dialog) {
    // Show dialog by loading dialog sessions from application context.
    // Application app = Application.getInstance();
    // if (app instanceof SingleFrameApplication) {
    // ((SingleFrameApplication) app).show(dialog);
    // } else {
    dialog.pack();
    dialog.setVisible(true);
    // }
  }
}
