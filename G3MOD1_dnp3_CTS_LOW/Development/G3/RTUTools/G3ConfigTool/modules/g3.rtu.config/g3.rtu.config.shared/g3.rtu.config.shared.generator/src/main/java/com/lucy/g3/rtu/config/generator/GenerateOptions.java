/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator;

import java.util.HashMap;

import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;

/**
 * A FactoryOption object contains all user choices for generating default
 * configuration items in {@linkplain ConfigFactory}.
 */
public class GenerateOptions {

  private final HashMap<Target, GenerateOption> options = new HashMap<Target, GenerateOption>();


  public void addOption(Target option, GenerateOption value) {
    options.put(option, value);
  }

  public GenerateOption getOption(Target key) {
    return options.get(key);
  }


  /**
   * The Enum GenerateOption.
   */
  public enum GenerateOption {
    DoNotGenerate("Do not generate"),
    GenerateDefault("Generate default"),
    GenerateFull("Generate full");
    
    GenerateOption(String description) {
      this.description = description;
    }
    
    @Override
    public String toString(){
      return description;
    }

    private final String description;
    public static boolean shouldGenerate(GenerateOption option) {
      return option == GenerateDefault || option == GenerateFull;
    }
  }

  /**
   * The Enum Target.
   */
  public enum Target {
    VirtualPoint,
    ScadaPoint,
  }


  /**
   * Instance Creation Methods.
   */
  public static GenerateOptions genDefaultVPoint() {
    GenerateOptions instance = new GenerateOptions();
    instance.addOption(Target.VirtualPoint, GenerateOption.GenerateDefault);
    instance.addOption(Target.ScadaPoint, GenerateOption.GenerateDefault);
    return instance;
  }

  /**
   * Instance Creation Methods.
   */
  public static GenerateOptions genFullVPoint() {
    GenerateOptions instance = new GenerateOptions();
    instance.addOption(Target.VirtualPoint, GenerateOption.GenerateFull);
    instance.addOption(Target.ScadaPoint, GenerateOption.GenerateFull);
    return instance;
  }

  /**
   * Instance Creation Methods.
   */
  public static GenerateOptions genNoPoint() {
    GenerateOptions instance = new GenerateOptions();
    instance.addOption(Target.VirtualPoint, GenerateOption.DoNotGenerate);
    instance.addOption(Target.ScadaPoint, GenerateOption.DoNotGenerate);
    return instance;
  }

  /**
   * Instance Creation Methods.
   */
  public static GenerateOptions genDefaultScadaPoint() {
    GenerateOptions instance = new GenerateOptions();
    instance.addOption(Target.ScadaPoint, GenerateOption.GenerateDefault);
    return instance;
  }

  /**
   * Instance Creation Methods.
   */
  public static GenerateOptions genFullScadaPoint() {
    GenerateOptions instance = new GenerateOptions();
    instance.addOption(Target.ScadaPoint, GenerateOption.GenerateFull);
    return instance;
  }
}
