/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.IRule;
import com.lucy.g3.rtu.config.shared.model.impl.DefaultRule;
import com.lucy.g3.rtu.config.shared.model.impl.Rules;

/**
 * The Class RulesTest.
 */
public class RulesTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testRegister() {
    INodeType typeA = new INodeType() {
    };
    INodeType typeB = new INodeType() {
    };

    IRule rule = new DefaultRule(typeA, -1, typeB, 1);
    Rules.INSTANCE.registerRule(rule);

    Assert.assertNotNull(Rules.INSTANCE.getRule(typeA, typeB));
  }

  @Test
  public void testRegisterNull() {
    Rules.INSTANCE.registerRule(null);
  }

}
