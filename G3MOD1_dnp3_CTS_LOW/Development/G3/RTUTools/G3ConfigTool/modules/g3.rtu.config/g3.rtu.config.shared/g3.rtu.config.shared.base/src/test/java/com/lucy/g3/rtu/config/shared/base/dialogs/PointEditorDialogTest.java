/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.dialogs;

import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.lucy.g3.rtu.config.shared.base.dialogs.AbstractEditorDialog;
import com.lucy.g3.rtu.config.shared.base.dialogs.IEditorDialogInvoker;
import com.lucy.g3.rtu.config.shared.validation.BufferValidator;

public class PointEditorDialogTest extends AbstractEditorDialog<TestBean> {

  private static Logger log = Logger.getLogger(PointEditorDialogTest.class);


  public PointEditorDialogTest() {
    super(null, new TestInvoker());
  }

  @Override
  protected JComponent createContentPanel() {
    JPanel content = new JPanel();
    content.add(BasicComponentFactory.createCheckBox(getModel().getBufferedModel(TestBean.PROPERTY_ENABLED),
        "Enable"));

    return content;
  }

  @Override
  protected BufferValidator createValidator(PresentationModel<TestBean> target) {
    return null;
  }

  public static void main(String[] args) {

    log.setLevel(Level.DEBUG);

    new PointEditorDialogTest().setVisible(true);
  }

  @Override
  protected void editingItemChanged(TestBean newPoint) {

  }


  private static class TestInvoker implements IEditorDialogInvoker<TestBean> {

    private final ArrayList<TestBean> items = new ArrayList<>();
    private int index = 0;


    public TestInvoker() {
      for (int i = 0; i < 4; i++) {
        TestBean bean = new TestBean();
        items.add(bean);
        if (i % 2 == 0) {
          bean.setEnabled(true);
        }
       //LoggingUtil.logPropertyChanges(bean, log);
      }
    }

    @Override
    public TestBean initial() {
      return items.get(index);
    }

    @Override
    public TestBean next() {
      System.out.println("Current index:" + index);
      if (index < items.size() - 1) {
        return items.get(++index);
      } else {
        return null;
      }
    }

    @Override
    public TestBean prev() {
      System.out.println("Current index:" + index);
      if (index > 0) {
        return items.get(--index);
      } else {
        return null;
      }
    }

    @Override
    public void fireEditingItemChanged() {
      System.out.println("Edit item changed!");
    }
  }


}
