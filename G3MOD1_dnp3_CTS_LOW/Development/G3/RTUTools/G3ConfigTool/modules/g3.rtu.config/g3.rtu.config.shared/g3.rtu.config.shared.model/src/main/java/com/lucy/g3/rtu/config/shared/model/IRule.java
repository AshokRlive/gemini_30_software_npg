/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model;

/**
 * The Interface IRule.
 */
public interface IRule {

  INodeType getSourceType();

  INodeType getTargetType();

  /**
   * Checks if two nodes can be connected.
   *
   * @param source
   * @param target
   * @throws NotConnectibleException
   *           an exception will be thrown if two nodes cannot be connected.
   */
  void checkConnectible(INode source, INode target) throws NotConnectibleException;
}
