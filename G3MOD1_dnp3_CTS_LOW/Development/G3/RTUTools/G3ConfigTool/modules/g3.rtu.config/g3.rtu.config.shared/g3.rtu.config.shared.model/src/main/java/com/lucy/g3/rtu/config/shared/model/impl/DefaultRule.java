/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.model.impl;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.model.INode;
import com.lucy.g3.rtu.config.shared.model.INodeType;
import com.lucy.g3.rtu.config.shared.model.IRule;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;

/**
 * The Class DefaultRule.
 */
public class DefaultRule extends Model implements IRule {

  private final INodeType sourceType;
  private final int allowedTargetNum;

  private final INodeType targetType;
  private final int allowedSourceNum;


  public DefaultRule(INodeType sourceType, int allowedTargetNum, INodeType targetType, int allowedSourceNum) {
    super();
    this.sourceType = Preconditions.checkNotNull(sourceType, "sourceType must not be null");
    this.targetType = Preconditions.checkNotNull(targetType, "target must not be null");
    this.allowedTargetNum = allowedTargetNum;
    this.allowedSourceNum = allowedSourceNum;
  }

  @Override
  public final INodeType getSourceType() {
    return sourceType;
  }

  @Override
  public final INodeType getTargetType() {
    return targetType;
  }

  @Override
  public void checkConnectible(INode src, INode tar) throws NotConnectibleException {
    // TODO refine this
    if (src != null
        && tar != null
        && src.getNodeType() == sourceType
        && tar.getNodeType() == targetType) {

      if ((allowedTargetNum < 0 || src.getNumberOfTargets(tar.getNodeType()) < allowedTargetNum)
          && (allowedSourceNum < 0 || tar.getNumberOfSources(src.getNodeType()) < allowedSourceNum)) {
        return;
      }
    }

    throw new NotConnectibleException("Cannot connect two nodes");
  }

}
