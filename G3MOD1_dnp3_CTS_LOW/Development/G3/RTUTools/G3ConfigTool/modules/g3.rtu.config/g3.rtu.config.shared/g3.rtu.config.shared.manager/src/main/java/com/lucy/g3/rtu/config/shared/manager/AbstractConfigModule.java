/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.manager;

import org.apache.log4j.Logger;

import com.jgoodies.binding.beans.Model;

/**
 * Basic implementation of <code>IConfigModule</code>.
 */
public abstract class AbstractConfigModule extends Model implements IConfigModule {

  private final IConfig owner;

  protected final Logger log = Logger.getLogger(getClass());

  public AbstractConfigModule(IConfig owner) {
    this.owner = owner;
  }

  @Override
  public final IConfig getOwner() {
    return owner;
  }

}
