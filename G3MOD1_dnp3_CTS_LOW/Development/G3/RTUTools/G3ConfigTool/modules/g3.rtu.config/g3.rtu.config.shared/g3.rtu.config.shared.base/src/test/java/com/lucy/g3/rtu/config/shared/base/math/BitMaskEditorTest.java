/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.math;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.rtu.config.shared.base.logger.LoggingUtil;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class BitMaskEditorTest.
 */
public class BitMaskEditorTest {

  public static void main(String[] args) {
    Logger logger = Logger.getLogger(BitMaskEditorTest.class);
    logger.setLevel(Level.DEBUG);

    // Create bean
    BitMask bean = new BitMask(3);
    LoggingUtil.logPropertyChanges(bean, logger);

    // Create GUI
    PresentationModel<BitMask> pm = new PresentationModel<BitMask>(bean);
    TestUtil.showFrame(new BitMaskEditor(pm, true));
  }
}
