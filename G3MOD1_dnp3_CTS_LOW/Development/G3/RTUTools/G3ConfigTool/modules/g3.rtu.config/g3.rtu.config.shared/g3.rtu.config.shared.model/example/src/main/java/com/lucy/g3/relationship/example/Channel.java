package com.lucy.g3.relationship.example;

import com.lucy.g3.relationship.impl.ConnectEvent;
import com.lucy.g3.relationship.impl.AbstractNode;


public class Channel extends AbstractNode {

    public Channel() {
        this("Channel");
    }
    public Channel(String descpriton) {
        super(NodeType.CHANNEL);
        setDescription(descpriton);
    }

    @Override
    protected void handleConnectEvent(ConnectEvent event) {
        
    }


}

