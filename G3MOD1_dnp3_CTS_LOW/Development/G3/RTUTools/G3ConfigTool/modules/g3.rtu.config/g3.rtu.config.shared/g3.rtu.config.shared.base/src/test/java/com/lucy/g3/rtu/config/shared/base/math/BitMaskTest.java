/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.shared.base.math;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.swing.JCheckBox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.lucy.g3.rtu.config.shared.base.math.BitMask;

/**
 * The Class BitMaskTest.
 */
public class BitMaskTest {

  private BitMask fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new BitMask(BitMask.MAX_BIT_NUMBER);
    fixture.setValue(0);// Init
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor0() {
    new BitMask(0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor1() {
    new BitMask(BitMask.MAX_BIT_NUMBER + 1);
  }

  @Test
  public void testSetMask() {
    for (int i = 0; i < 256; i++) {
      fixture.setValue(i);
      assertEquals(i, fixture.getValue());
    }
  }
  
  @Test
  public void testSetMask2() {
    fixture.setValue(0x18000);
    
    for (int j = 0; j < 15; j++) {
      assertFalse(fixture.getBit(j));
    }
    
    assertTrue(fixture.getBit(15));
    assertTrue(fixture.getBit(16));
  }

  @Test
  public void testSetBit0() {
    fixture.setBit0(true);
    assertEquals(1, fixture.getValue());
    assertTrue(fixture.getBit0());
  }
  
  @Test
  public void testSetBit1() {
    fixture.setBit1(true);
    assertEquals(2, fixture.getValue());
    assertTrue(fixture.getBit1());
  }
  
  @Test
  public void testSetBit01() {
    fixture.setBit0(true);
    fixture.setBit1(true);
    assertEquals(3, fixture.getValue());
    assertTrue(fixture.getBit0());
    assertTrue(fixture.getBit1());
    
    fixture.setBit0(false);
    assertEquals(2, fixture.getValue());
    assertFalse(fixture.getBit0());
    assertTrue(fixture.getBit1());
  }
  
  @Test
  public void testBinding() {
    PresentationModel<BitMask> pm = new PresentationModel<>(fixture);

    JCheckBox comp0 = BasicComponentFactory.createCheckBox(pm.getModel(BitMask.PROPERTY_BIT_0), "");
    JCheckBox comp1 = BasicComponentFactory.createCheckBox(pm.getModel(BitMask.PROPERTY_BIT_1), "");
    JCheckBox comp2 = BasicComponentFactory.createCheckBox(pm.getModel(BitMask.PROPERTY_BIT_2), "");

    comp0.setSelected(true);
    comp1.setSelected(false);
    comp2.setSelected(true);
    assertEquals(5, fixture.getValue());

    fixture.setBit0(false);
    fixture.setBit1(false);
    fixture.setBit2(false);
    assertFalse(comp0.isSelected());
    assertFalse(comp1.isSelected());
    assertFalse(comp2.isSelected());

  }

}
