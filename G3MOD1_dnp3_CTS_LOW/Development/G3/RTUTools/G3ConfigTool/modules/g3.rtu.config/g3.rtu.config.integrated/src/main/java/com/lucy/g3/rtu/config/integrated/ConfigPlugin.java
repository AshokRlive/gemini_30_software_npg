/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.integrated;

import com.lucy.g3.rtu.config.clogic.CLogicPlugin;
import com.lucy.g3.rtu.config.device.comms.CommsDevicePlugin;
import com.lucy.g3.rtu.config.device.fields.FieldDevicePlugin;
import com.lucy.g3.rtu.config.general.GeneralConfigPlugin;
import com.lucy.g3.rtu.config.hmi.HMIPlugin;
import com.lucy.g3.rtu.config.module.canmodule.CANModulePlugin;
import com.lucy.g3.rtu.config.module.iomodule.IOModulePlugin;
import com.lucy.g3.rtu.config.module.shared.ModulePlugin;
import com.lucy.g3.rtu.config.port.PortPlugin;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.SDNP3Plugin;
import com.lucy.g3.rtu.config.protocol.iec101.slave.S101Plugin;
import com.lucy.g3.rtu.config.protocol.iec104.slave.S104Plugin;
import com.lucy.g3.rtu.config.protocol.master.shared.MasterProtocolPlugin;
import com.lucy.g3.rtu.config.protocol.modbus.master.ModbusMasterPlugin;
import com.lucy.g3.rtu.config.protocol.slave.shared.ScadaProtocolPlugin;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;
import com.lucy.g3.rtu.config.template.builtin.modbus.MMBTemplatePlugin;
import com.lucy.g3.rtu.config.template.custom.integrated.CommissioningTemplatePlugin;
import com.lucy.g3.rtu.config.user.UserPlugin;
import com.lucy.g3.rtu.config.virtualpoint.shared.VirtualPointPlugin;
import com.lucy.g3.rtu.config.virtualpoint.standard.StandardPointPlugin;

public class ConfigPlugin implements IConfigPlugin {
  private ConfigPlugin() {}
  
  public static void init() {
    CANModulePlugin.init();
    IOModulePlugin.init();
    CLogicPlugin.init();
    CommsDevicePlugin.init();
    CommissioningTemplatePlugin.init();
    GeneralConfigPlugin.init();
    MasterProtocolPlugin.init();
    FieldDevicePlugin.init();  
    ModulePlugin.init();
    PortPlugin.init();
    ScadaProtocolPlugin.init();
    SDNP3Plugin.init();
    S101Plugin.init();
    S104Plugin.init();
    ModbusMasterPlugin.init();
    UserPlugin.init();
    VirtualPointPlugin.init();
    StandardPointPlugin.init();
    HMIPlugin.init();
    MMBTemplatePlugin.init();
  }
}

