/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.integrated;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.CLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.factory.CLogicStub;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.impl.PseudoPointFactory;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;

/**
 * The Class VirtualPointFactoryTest.
 */
public class VirtualPointFactoryTest {

  private ICLogic clogic;


  @Before
  public void setUp() throws Exception {
    clogic = new CLogicStub(CLogicType.builder(123, "StubLogicType").get());
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = NullPointerException.class)
  public void testCreateStdWithNullArg() {
    VirtualPointFactory.createStanardPoint(null);
  }

  @Test(expected = NullPointerException.class)
  public void testCreatePseudoWithNullArg() {
    PseudoPointFactory.createPseudoPoint(null, 0, clogic);
  }

  @Test
  public void testCreateStanardPoint() {
    VirtualPointType[] types = VirtualPointType.values();

    for (VirtualPointType type : types) {
      VirtualPoint point = VirtualPointFactory.createStanardPoint(type);
      assertNotNull(point);
      assertTrue(point.getType() == type);
    }
  }

  @Test
  public void testCreatePseudoPoint() {
    VirtualPointType[] types = VirtualPointType.values();
    clogic.setGroup(2);

    for (VirtualPointType type : types) {
      VirtualPoint point = PseudoPointFactory.createPseudoPoint(type, 0, clogic);
      assertNotNull(point);
      assertTrue(point.getType() == type);
      System.out.println("group: " + point.getGroup());
      if (point.getGroup() != 2) {
        fail("The group of pseudo point must be same as its source group:" + point);
      }
    }
  }

  @Test
  public void testPointTypeMatchClass() {
    VirtualPointType[] types = VirtualPointType.values();

    /* Match map between point type enum and class */
    Object[][] stdMatches = {
        { VirtualPointType.BINARY_INPUT, StdBinaryPoint.class },
        { VirtualPointType.ANALOGUE_INPUT, StdAnaloguePoint.class },
        { VirtualPointType.DOUBLE_BINARY_INPUT, StdDoubleBinaryPoint.class },
    };

    Object[][] pseudoMatches = {
        { VirtualPointType.ANALOGUE_INPUT, PseudoAnaloguePoint.class },
        { VirtualPointType.BINARY_INPUT, PseudoBinaryPoint.class },
        { VirtualPointType.DOUBLE_BINARY_INPUT, PseudoDoubleBinaryPoint.class },
    };

    VirtualPoint stdpoint;
    VirtualPoint pseudopoint;

    for (VirtualPointType type : types) {
      pseudopoint = PseudoPointFactory.createPseudoPoint(type, 0, clogic);
      stdpoint = VirtualPointFactory.createStanardPoint(type);

      /* Check if type matches class */
      for (int i = 0; i < stdMatches.length; i++) {
        if (type == stdMatches[i][0]) {
          String msg = String.format(
              "The enum type \"%s\" of created point  doesn't match"
                  + " with point's class. Expected:%s.class    Actual:%s.class",
              type.name(), ((Class<?>) stdMatches[i][1]).getSimpleName(), stdpoint
                  .getClass().getSimpleName());

          assertTrue(msg, ((Class<?>) stdMatches[i][1]).isInstance(stdpoint));
        }

        if (type == pseudoMatches[i][0]) {
          String msg = String.format(
              "The enum type \"%s\" of created point  doesn't match"
                  + " with point's class. Expected:%s.class    Actual:%s.class",
              type.name(), ((Class<?>) pseudoMatches[i][1]).getSimpleName(), pseudopoint
                  .getClass().getSimpleName());

          assertTrue(msg, ((Class<?>) pseudoMatches[i][1]).isInstance(pseudopoint));
        }
      }

    }

  }

}
