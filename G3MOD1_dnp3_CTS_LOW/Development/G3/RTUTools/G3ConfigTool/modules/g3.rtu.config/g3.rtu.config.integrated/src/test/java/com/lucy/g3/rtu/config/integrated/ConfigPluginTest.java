/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.integrated;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.integrated.ConfigPlugin;


/**
 *
 */
public class ConfigPluginTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testPageFactory() {
    ConfigPlugin.init();
    for (int i = 0; i < PageFactories.KEY_LAST; i++) {
      assertNotNull("page is null at key:" + i, PageFactories.getFactory(i));
    }
  }

}

