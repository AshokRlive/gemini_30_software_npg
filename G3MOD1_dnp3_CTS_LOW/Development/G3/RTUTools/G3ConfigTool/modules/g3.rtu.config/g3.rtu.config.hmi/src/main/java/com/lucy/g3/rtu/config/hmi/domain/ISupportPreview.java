/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.List;

/**
 * The hmi object that support preview should implement this interface.
 */
public interface ISupportPreview {
  String getPreviewText();
  String getPreviewTips();
  
  Screen getScreen();
  
  
  /**
   * Gets the children items to be previewed.
   *
   * @return a list of items to be previewed. null if there is no child.
   */
  List<? extends ISupportPreview> getPreviewChildren();
  
  boolean allowsPreviewChildren();

}

