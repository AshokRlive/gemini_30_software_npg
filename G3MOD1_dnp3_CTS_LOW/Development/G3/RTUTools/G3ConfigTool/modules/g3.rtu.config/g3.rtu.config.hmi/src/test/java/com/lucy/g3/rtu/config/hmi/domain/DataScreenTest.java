/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.virtualpoint.shared.stub.VirtualPointStub;

/**
 *
 *
 */
public class DataScreenTest {

  final private String TITLE = "data screen 0";

  private DataScreen fixture;


  @Before
  public void setUp() throws Exception {
    fixture = new DataScreen(TITLE);
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

  @Test
  public void testDataEntries() {
    assertNotNull("Data entries mustn't be null", fixture.getDataEntries());
    assertEquals("No data entries initially", 0, fixture.getDataEntries().length);

    DataEntry[] entries = new DataEntry[4];
    fixture.setDataEntries(entries);
    assertEquals(0, fixture.getDataEntries().length);

    entries[0] = new DataEntry();
    entries[1] = new DataEntry();
    fixture.setDataEntries(entries);
    assertEquals(2, fixture.getDataEntries().length);
  }

  @Test
  public void testDelete() {
    DataEntry[] entries = new DataEntry[4];
    for (int i = 0; i < entries.length; i++) {
      entries[i] = new DataEntry();
      entries[i].setSource(new VirtualPointStub(VirtualPointType.BINARY_INPUT));
    }
    fixture.setDataEntries(entries);

    fixture.delete();

    assertEquals(0, fixture.getDataEntries().length);

    for (DataEntry e : entries) {
      assertNull(e.getSource());
    }

  }

  @Test
  public void testNotAllowChildren() {
    assertFalse(fixture.getAllowsChildren());
  }

  @Test
  public void testValidate() {
    fixture.getValidator().validate();
    assertTrue("Empty data screen could be valid", fixture.getValidator().isValid());

    DataEntry[] entries = new DataEntry[2];
    entries[0] = new DataEntry();
    entries[1] = new DataEntry();
    // Add two Non-configured data entries
    fixture.setDataEntries(entries);

    fixture.getValidator().validate();
    assertFalse("Data screen should be invalid if it contains Non-configured data entry",
        fixture.getValidator().isValid());
    assertEquals(2, fixture.getValidator().getResult().getErrors().size());
  }

  @Test
  public void testName() {
    assertEquals(Screen.DATA_SCREEN_NAME, fixture.getName());
  }

}
