/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.manager.RootScreen;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * Abstract class for defining the tree node in HMI Menu tree. Each screen has
 * its own title, unique ID, and may or may not contain children screens.
 * <p>
 * <b>Note:</b> {@code Screen} doesn't accept any other type of child node
 * except {@code Screen}.
 * </p>
 */
public abstract class Screen extends DefaultMutableTreeNode implements IValidation, ISupportPreview {

  static final String CONTROL_SCREEN_NAME = "Control Screen";

  static final String DATA_SCREEN_NAME = "Data Screen";

  static final String PHASE_SCREEN_NAME = "Phase Screen";

  static final String MENU_SCREEN_NAME = "Sub Menu";

  static final String INFO_SCREEN_NAME = "Info Screen";

  private static final String DEFAULT_TITLE = "Untitled";

  private int id;

  private String title = DEFAULT_TITLE; // Screen title

  protected final IValidator validator;
  
  public Screen(String title) {
    setTitle(title);
    this.validator = createValidator();
  }

  protected abstract IValidator createValidator();

  @Override
  public final IValidator getValidator() {
    return validator;
  }


  public int getId() {
    return id;
  }

  public void setId(int newVal) {
    id = newVal;
  }

  /**
   * Gets the title of this screen.
   *
   * @return the title String which is displayed on HMI menu.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Changes the title of this screen.
   *
   * @param newVal
   *          new title, must not be null or empty.
   */
  public void setTitle(String newVal) {
    if (newVal != null && !newVal.trim().isEmpty()) {
      title = newVal;
    }
  }

  @Override
  public String toString() {
    return String.format("[%s] %s", getName(), getTitle());
  }
  

  // Override to change the return type to <code>Screen</code>.
  @Override
  public Enumeration<Screen> children() {
    return super.children();
  }

  private void checkNode(MutableTreeNode node) {
    if (node != null && !(node instanceof Screen)) {
      throw new RuntimeException("Unsupported node type: " + node.getClass().getName()
          + " Only support \"Screen\" Node");
    }
  }

  // Override to check new child type.
  @Override
  public void insert(MutableTreeNode newChild, int childIndex) {
    checkNode(newChild);
    super.insert(newChild, childIndex);
  }

  // Override to check parent type.
  @Override
  public void setParent(MutableTreeNode newParent) {
    checkNode(newParent);
    super.setParent(newParent);
  }

  // Override to check new child type.
  @Override
  public void add(MutableTreeNode newChild) {
    checkNode(newChild);
    
    super.add(newChild);
  }

  
  public HMIScreenManager getManager() {
    TreeNode root = getRoot();
    if(root != null && root instanceof RootScreen)
       return ((RootScreen)root).getManager();
    
    return null;
  }

  public String getPathString() {
    TreeNode[] paths = getPath();
    StringBuilder sb = new StringBuilder();
    sb.append("HMI Screen:");
    for (int i = 0; i < paths.length; i++) {
      sb.append(paths[i].toString());
      if (i < paths.length - 1) {
        sb.append(" > ");
      }
    }

    return sb.toString();
  }

  
  @Override
  public String getPreviewText() {
    return getTitle();
  }
  
  @Override
  public String getPreviewTips() {
    return getName();
  }

  @Override
  public Screen getScreen() {
    return this;
  }

  @Override
  public boolean allowsPreviewChildren() {
    return super.getAllowsChildren();
  }

  public abstract IScreenEditor<? extends Screen> getEdtior();
  
  public abstract void delete();

  /**
   * Gets the name of this screen. Unlike the "title" property, the name of a
   * screen is NOT editable by user and mostly used as read-only property of
   * this screen. E.g. for switch screen, its name is "Control Screen" which
   * cannot be changed by user.
   *
   * @return The name of this screen
   */
  public abstract String getName();

  /**
   * Clone this node.
   *
   * @return
   */
  public abstract Screen copy();

}// end Screen
