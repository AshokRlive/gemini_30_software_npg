/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import static org.junit.Assert.*;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.hmi.ui.HMIScreenPreviewer;


public class HMIScreenPreviewerTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test 
  public void testLoadFont(){
    assertNotNull(HMIScreenPreviewer.loadLEDFont());
  }
  
  @Test
  public void testCalculateDimension() {
    JLabel comp = new JLabel();
    Font font = HMIScreenPreviewer.loadLEDFont();
    char c = '0';
    for (int i = 0; i < 10; i++) {
      String text = HMIScreenPreviewer.getSampleStr(c, 20);
      Dimension dim = HMIScreenPreviewer.calculateSize(comp, font, text);
      System.out.println(String.format("character: %s width:%s height:%s", c,dim.getWidth(),dim.getHeight()));
      c ++;
    }
    
  }

}

