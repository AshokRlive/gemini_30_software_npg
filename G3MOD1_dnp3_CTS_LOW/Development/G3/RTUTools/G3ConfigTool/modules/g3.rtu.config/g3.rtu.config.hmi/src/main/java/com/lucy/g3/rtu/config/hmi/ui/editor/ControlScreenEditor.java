/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;

public class ControlScreenEditor extends AbstractScreenEditor<ControlScreen> {

  @Override
  public boolean edit(ControlScreen screen, Object initialSelection) {
    ControlScreenDialog editor = new ControlScreenDialog(parent, screen, getOperableLogic(screen));
    editor.pack();
    editor.setVisible(true);
    
    return editor.hasBeenAffirmed();
  }

  @Override
  public ControlScreen createNew(Screen parentScreen) {
    ControlScreen newScreen = new ControlScreen(null);
    
    ControlScreenDialog editor = new ControlScreenDialog(parent, newScreen, getOperableLogic(parentScreen));
    editor.pack();
    editor.setVisible(true);
    
    
    return editor.hasBeenAffirmed() ? newScreen : null;
  }

}

