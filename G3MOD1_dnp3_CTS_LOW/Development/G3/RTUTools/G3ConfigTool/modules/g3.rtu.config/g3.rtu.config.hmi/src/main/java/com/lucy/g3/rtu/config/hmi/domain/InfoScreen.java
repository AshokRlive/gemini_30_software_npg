/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.List;

import com.lucy.g3.rtu.config.hmi.domain.validation.InfoScreenValidator;
import com.lucy.g3.rtu.config.hmi.ui.editor.InfoScreenEditor;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.HMI_INFO_SCREEN_TYPE;

/**
 *
 * Information screen.
 */
public class InfoScreen extends Screen {

  public static final String PROPERTY_TYPE = "type";

  private HMI_INFO_SCREEN_TYPE type;


  public InfoScreen(String title) {
    this(title, null);
  }

  public InfoScreen(String title, HMI_INFO_SCREEN_TYPE type) {
    super(title);
    if (type == null) {
      type = HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_SOFTWARE;
    }

    this.type = type;
  }
  
  @Override
  protected IValidator createValidator() {
    return new InfoScreenValidator(this);
  }

  public HMI_INFO_SCREEN_TYPE getType() {
    return type;
  }

  public void setType(HMI_INFO_SCREEN_TYPE type) {
    this.type = type;
  }

  @Override
  public String getName() {
    return INFO_SCREEN_NAME;
  }

  @Override
  public Screen copy() {
    InfoScreen copy = new InfoScreen(getTitle());
    copy.type = type;
    return copy;
  }

  @Override
  public List<ISupportPreview> getPreviewChildren() {
    return null;
  }

  @Override
  public boolean getAllowsChildren() {
    return false;
  }

  @Override
  public void delete() {

  }

  @Override
  public InfoScreenEditor getEdtior() {
    return new InfoScreenEditor();
  }

}
