/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.List;


public final class StringPreviewItem implements ISupportPreview {
  private String previewText;
  private final Screen screen;
  
  public StringPreviewItem(Screen screen, String previewText) {
    this.screen = screen;
    this.previewText = previewText;
  }
  
  @Override
  public String getPreviewText() {
    return previewText;
  }

  public static StringPreviewItem create(Screen screen, String previewText){ 
    return new StringPreviewItem(screen, previewText);
  }

  @Override
  public boolean allowsPreviewChildren() {
    return false;
  }
  
  @Override
  public List<ISupportPreview> getPreviewChildren() {
    return null;
  }

  @Override
  public String getPreviewTips() {
    return "";
  }

  @Override
  public Screen getScreen() {
    return screen;
  }
}

