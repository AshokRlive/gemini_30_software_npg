/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.xml;

import java.util.List;

import com.g3schema.ns_common.VirtualPointRefT;
import com.g3schema.ns_hmi.HMIControlScreenT;
import com.g3schema.ns_hmi.HMIDataScreenT;
import com.g3schema.ns_hmi.HMIInfoScreenT;
import com.g3schema.ns_hmi.HMIMenuScreenT;
import com.g3schema.ns_hmi.HMIPhaseScreenT;
import com.g3schema.ns_hmi.HMIVPointDataEntryT;
import com.g3schema.ns_hmi.RootScreenT;
import com.g3schema.ns_hmi.ScreensT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.domain.InfoScreen;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.PhaseScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 *
 *
 */
public class HMIScreenXmlWriter {

  private final IHMIXmlWriteSupport support;

  private final HMIScreenManager manager;

  private ValidationResult result;


  public HMIScreenXmlWriter(HMIScreenManager manager, IHMIXmlWriteSupport support) {
    this.support = Preconditions.checkNotNull(support, "support is null");
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
  }

  // ---- HMI Screen configure ----
  public void write(ScreensT xml_screens, ValidationResult result) {
    manager.updateAllScreensID();
    MenuScreen mainMenu = manager.getMainMenu();

    // Root
    RootScreenT xml_root = xml_screens.rootScreen.append();
    xml_root.rootScreenID.setValue(mainMenu.getId());

    // Export screen
    exportMenuScreen(xml_screens, mainMenu);
  }

  private void exportMenuScreen(ScreensT xml_screens, MenuScreen screen) {
    HMIMenuScreenT xml_screen = xml_screens.menuScreens.append();
    xml_screen.screenID.setValue(screen.getId());
    xml_screen.screenTitle.setValue(screen.getTitle());

    List<Screen> screens = screen.getPreviewChildren();
    for (Screen s : screens) {
      if (s != null) {
        xml_screen.childList.append().screenID.setValue(s.getId());
      }
    }

    for (Screen s : screens) {
      if (s instanceof MenuScreen) {
        exportMenuScreen(xml_screens, (MenuScreen) s);
      }

      else if (s instanceof ControlScreen) {
        exportSwitchScreen(xml_screens, (ControlScreen) s);
      }

      else if (s instanceof PhaseScreen) {
        exportPhaseScreen(xml_screens, (PhaseScreen) s);
      }

      else if (s instanceof DataScreen) {
        exportDataScreen(xml_screens, (DataScreen) s);
      }

      else if (s instanceof InfoScreen) {
        exportInfoScreen(xml_screens, (InfoScreen) s);
      }
    }
  }


  final int InvalidPointID = 999;


  private void exportSwitchScreen(ScreensT xml_screens, ControlScreen screen) {
    HMIControlScreenT xml_switch = xml_screens.controlScreens.append();
    xml_switch.screenID.setValue(screen.getId());
    xml_switch.screenTitle.setValue(screen.getTitle());
    int switchID = InvalidPointID;
    if (screen.getCLogic() != null) {
      switchID = screen.getCLogic().getGroup();
    } else {
      result.addWarning("No switch logic is given to Switch Screen:" + screen);
    }
    xml_switch.controlLogicID.setValue(switchID);
  }

  private void exportInfoScreen(ScreensT xml_screens, InfoScreen screen) {
    HMIInfoScreenT xml_infosc = xml_screens.infoScreens.append();
    xml_infosc.screenID.setValue(screen.getId());
    xml_infosc.screenTitle.setValue(screen.getTitle());
    xml_infosc.type.setValue(screen.getType().getValue());
  }

  private void exportPhaseScreen(ScreensT xml_screens, PhaseScreen screen) {
    HMIPhaseScreenT xml_phase = xml_screens.phaseScreens.append();
    xml_phase.screenID.setValue(screen.getId());
    xml_phase.screenTitle.setValue(screen.getTitle());

    String cfooter, vfooter;
    cfooter = screen.getCurrentFooter();
    vfooter = screen.getVoltageFooter();
    if (cfooter != null && !cfooter.isEmpty()) {
      xml_phase.currentFooter.setValue(cfooter);
    }

    if (vfooter != null && !vfooter.isEmpty()) {
      xml_phase.voltageFooter.setValue(vfooter);
    }

    // Virtual point references
    for (int i = 0; i < screen.getPointsNum(); i++) {
      setPhasePoint(screen.getPoint(i), xml_phase.virtualPointEntry.append());
    }
  }

  private void setPhasePoint(VirtualPoint point, VirtualPointRefT xml_point) {
    if (point == null) {
      xml_point.pointGroup.setValue(InvalidPointID);
      xml_point.pointID.setValue(InvalidPointID);
    } else {
      xml_point.pointGroup.setValue(point.getGroup());
      xml_point.pointID.setValue(point.getId());
    }
  }

  private void exportDataScreen(ScreensT xml_screens, DataScreen screen) {

    HMIDataScreenT xml_data = xml_screens.dataScreens.append();
    xml_data.screenID.setValue(screen.getId());
    xml_data.screenTitle.setValue(screen.getTitle());

    DataEntry[] entries = screen.getDataEntries();
    for (int i = 0; i < entries.length; i++) {
      // Point
      VirtualPoint p = entries[i].getSource();
      if (p == null) {
        continue;
      }

      HMIVPointDataEntryT xml_entry = xml_screens.dataEntries.append();
      // Entry ID
      int entryID = xml_screens.dataEntries.count();
      xml_entry.entryID.setValue(entryID);// use count of entries as entry ID
      xml_data.entries.append().entryID.setValue(entryID);

      // Footer & header
      if (entries[i].getFooter() != null) {
        xml_entry.footer.setValue(entries[i].getFooter());
      }
      if (entries[i].getHeader() != null) {
        xml_entry.header.setValue(entries[i].getHeader());
      }

      // Point
      xml_entry.pointGroup.setValue(p.getGroup());
      xml_entry.pointID.setValue(p.getId());
    }
  }
}
