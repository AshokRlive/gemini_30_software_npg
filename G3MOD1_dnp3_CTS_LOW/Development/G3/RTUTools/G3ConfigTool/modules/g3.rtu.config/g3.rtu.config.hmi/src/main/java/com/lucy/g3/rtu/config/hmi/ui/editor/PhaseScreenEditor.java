/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import com.lucy.g3.rtu.config.hmi.domain.PhaseScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.virtualpoint.shared.util.VirtualPointFinder;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;


public class PhaseScreenEditor extends AbstractScreenEditor<PhaseScreen> {

  @Override
  public boolean edit(PhaseScreen screen, Object initialSelection) {
    PhaseScreenDialog editor = new PhaseScreenDialog(parent,
        screen, VirtualPointFinder.findPointsInModuleType(getAllPoints(screen), MODULE.MODULE_FDM));
    editor.pack();
    editor.setVisible(true);
    
    return editor.hasBeenAffirmed();
  }

  @Override
  public PhaseScreen createNew(Screen parentScreen) {
    PhaseScreen newScreen = new PhaseScreen(null);
    PhaseScreenDialog editor = new PhaseScreenDialog(parent,
        newScreen, VirtualPointFinder.findPointsInModuleType(getAllPoints(parentScreen), MODULE.MODULE_FDM));
    editor.pack();
    editor.setVisible(true);
    
    return editor.hasBeenCanceled() ? null : newScreen;
  }

}

