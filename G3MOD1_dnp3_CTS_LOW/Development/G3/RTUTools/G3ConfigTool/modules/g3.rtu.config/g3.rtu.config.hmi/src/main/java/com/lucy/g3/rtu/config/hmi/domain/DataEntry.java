/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.List;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.rtu.config.shared.model.NotConnectibleException;
import com.lucy.g3.rtu.config.shared.model.impl.DefaultNode;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumHMI;

/**
 * A Data Entry is mapped to a virtual point, and it will be listed in a
 * {@code DataScreen}.
 */
public class DataEntry extends DefaultNode<VirtualPoint> implements ISupportPreview{
  private final static int MAX_PREVIW_LENGTH = ModuleProtocolEnumHMI.HMI_MAX_DISPLAY_COL;
  
  private String header;

  private Screen screen;
  
  private int id;
  
  public DataEntry() {
    super(NodeType.HMI_SCREEN, VirtualPoint.class);
  }
  
  
  @Override
  public Screen getScreen() {
    return screen;
  }
  
  void setScreen(Screen screen) {
    this.screen = screen;
  }

  public String getHeader() {
    return header;
  }

  @Override
  public void setSource(VirtualPoint source)
      throws NotConnectibleException {
    super.setSource(source);
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getFooter() {
    VirtualPoint point = getSource();
    if (point != null && point instanceof AnaloguePoint) {
      String unit = ((AnaloguePoint) point).getUnit();
      if (unit != null) {
        return unit;
      }
    }
    return null;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  /**
   * Deep clone this object.
   *
   * @return
   */
  public DataEntry copy() {
    DataEntry copy = new DataEntry();
    copy.setHeader(header);
    copy.setSource(getSource());
    copy.screen = screen;
    copy.id = id;
    
    return copy;
  }

  @Override
  public String toString() {
    return getPreviewText();
  }

  @Override
  public String getPreviewText() {
    VirtualPoint point = getSource();

    String headerStr = getHeader();
    String footerStr = getFooter();
    
    // Append gap 
    if(!Strings.isBlank(headerStr))
      headerStr += " ";
    if(!Strings.isBlank(footerStr))
      footerStr = " " + footerStr;
    
    // Calculate value string length
    int valueStrLen = MAX_PREVIW_LENGTH;
    if(headerStr != null)
      valueStrLen = valueStrLen - headerStr.length();
    if(footerStr != null)
      valueStrLen = valueStrLen - footerStr.length();
    
    // Building string
    StringBuilder sb = new StringBuilder();
    if(headerStr != null)
    sb.append(headerStr);

    char val = point != null ? 'X' : '?';
    for (int i = 0; i < valueStrLen; i++) {
      sb.append(val);
    }
  
    if(footerStr != null)
      sb.append(footerStr);

    return sb.toString();
  }

  @Override
  public List<ISupportPreview> getPreviewChildren() {
    return null;
  }

  @Override
  public boolean allowsPreviewChildren() {
    return false;
  }
  
  @Override
  public String getPreviewTips() {
    VirtualPoint point = getSource();

    return point == null ? "Not configured" : point.getHTMLSummary();
  }

}
