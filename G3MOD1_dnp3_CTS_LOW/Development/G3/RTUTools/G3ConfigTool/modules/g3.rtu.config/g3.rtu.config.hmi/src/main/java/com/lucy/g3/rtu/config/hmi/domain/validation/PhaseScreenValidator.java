/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import com.lucy.g3.rtu.config.hmi.domain.PhaseNode;
import com.lucy.g3.rtu.config.hmi.domain.PhaseScreen;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public class PhaseScreenValidator extends AbstractScreenValidator {

  private final PhaseScreen target;


  public PhaseScreenValidator(PhaseScreen target) {
    super(target);
    this.target = target;
  }

  @Override
  protected void validate(ValidationResultExt result) {
    PhaseNode[] pointNodes = target.getPointNodes();
    for (int i = 0; i < pointNodes.length; i++) {
      if (pointNodes[i].getSource() == null) {
        result.addError(String.format("\"%s\" NOT configured at: %s",
            pointNodes[i].getName(), target.getPathString()));

      }
    }
  }

}
