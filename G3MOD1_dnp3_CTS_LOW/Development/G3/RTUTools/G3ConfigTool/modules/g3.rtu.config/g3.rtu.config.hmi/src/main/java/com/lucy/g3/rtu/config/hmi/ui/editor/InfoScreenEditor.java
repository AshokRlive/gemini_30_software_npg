/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import com.lucy.g3.rtu.config.hmi.domain.InfoScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;


public class InfoScreenEditor extends AbstractScreenEditor<InfoScreen> {

  @Override
  public boolean edit(InfoScreen screen, Object initialSelection) {
    InfoScreenDialog editor = new InfoScreenDialog(parent, screen);
    editor.pack();
    editor.setVisible(true);

    return editor.hasBeenAffirmed();
  }

  @Override
  public InfoScreen createNew(Screen parentScreen) {
    InfoScreen newScreen = new InfoScreen(null);
    InfoScreenDialog editor = new InfoScreenDialog(parent, newScreen);
    editor.pack();
    editor.setVisible(true);
    
    return editor.hasBeenAffirmed() ? newScreen : null;
  }

}

