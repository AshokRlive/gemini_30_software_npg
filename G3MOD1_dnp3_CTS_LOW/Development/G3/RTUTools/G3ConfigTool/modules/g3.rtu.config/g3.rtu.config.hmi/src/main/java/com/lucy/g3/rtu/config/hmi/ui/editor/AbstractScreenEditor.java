/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.clogic.CLogicRepository;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.hmi.domain.IScreenEditor;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointRepository;


abstract class AbstractScreenEditor<ScreenT extends Screen> implements IScreenEditor<ScreenT> {
  protected JFrame parent = WindowUtils.getMainFrame();

  
  protected  Collection<VirtualPoint> getAllPoints(Screen screen) {
    HMIScreenManager manager = screen.getManager();
    IConfig conf = manager.getOwner();
    if(conf != null) {
      VirtualPointRepository repo = conf 
        .getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);
      if(repo != null)
        return repo.getAllPoints();
    }
    
    return new ArrayList<VirtualPoint>(0);
  }
  
  protected Collection<IOperableLogic> getOperableLogic(Screen screen){
    HMIScreenManager manager = screen.getManager();
    CLogicRepository repo2 = (CLogicRepository)manager.getOwner()
        .getConfigModule(CLogicRepository.CONFIG_MODULE_ID);
    
    return repo2.getOperableCLogic();
  }
  
}

