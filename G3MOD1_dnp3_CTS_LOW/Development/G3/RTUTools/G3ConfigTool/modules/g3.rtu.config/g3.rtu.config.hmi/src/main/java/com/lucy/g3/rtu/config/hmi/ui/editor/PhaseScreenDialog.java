/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.hmi.domain.PhaseScreen;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

class PhaseScreenDialog extends AbstractDialog {

  private final String[] labels = PhaseScreen.POINT_NAMES;

  private final PhaseScreen screen;

  private final Collection<VirtualPoint> points;

  private JTextField titleField;

  private JTextField currentFooterField;

  private JTextField voltageFooterField;

  private JComboBox[] editors = new JComboBox[labels.length];


  public PhaseScreenDialog(JFrame parent, PhaseScreen screen,
      Collection<VirtualPoint> points) {
    super(parent);
    if (screen == null) {
      throw new IllegalArgumentException("screen is null");
    }

    setTitle(screen.getName());

    this.screen = screen;

    this.points = points;

    pack();

    loadData();

  }

  private void loadData() {
    titleField.setText(screen.getTitle());
    voltageFooterField.setText(screen.getVoltageFooter());
    currentFooterField.setText(screen.getCurrentFooter());
    titleField.selectAll();

    if (points != null) {
      for (int i = 0; i < editors.length; i++) {
        editors[i].setModel(new DefaultComboBoxModel(points.toArray()));
        editors[i].setSelectedItem(screen.getPoint(i));
      }
    }
  }

  private void saveData() {
    screen.setTitle(titleField.getText());
    screen.setVoltageFooter(voltageFooterField.getText());
    screen.setCurrentFooter(currentFooterField.getText());

    for (int i = 0; i < editors.length; i++) {
      screen.setPoint((VirtualPoint) editors[i].getSelectedItem(), i);
    }
  }

  @Override
  public void cancel() {
    super.cancel();
  }

  @Override
  public void ok() {
    saveData();
    super.ok();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    FormLayout layout = new FormLayout("right:default, $lcgap,100dlu:grow", "");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout);

    titleField = new JTextField(10);
    currentFooterField = new JTextField(10);
    voltageFooterField = new JTextField(10);

    builder.append("Title: ", titleField, true);
    builder.append("Current Footer: ", currentFooterField, true);
    builder.append("Voltage Footer: ", voltageFooterField, true);
    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();
    for (int i = 0; i < editors.length; i++) {
      editors[i] = new JComboBox();
      builder.append(labels[i] + ": ", editors[i], true);
    }
    builder.setDefaultDialogBorder();
    return builder.getPanel();
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

  public static void main(String[] args) {
    PhaseScreen screen = new PhaseScreen("DigitalData");
    new PhaseScreenDialog(null, screen, null).setVisible(true);

  }
}
