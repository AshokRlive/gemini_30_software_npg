/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.shared.validation.AbstractValidator;

abstract class AbstractScreenValidator<ScreenT extends Screen> extends AbstractValidator<ScreenT> {

  public AbstractScreenValidator(ScreenT target) {
    super(target);
  }

  @Override
  public String getTargetName() {
    return String.format("HMI [%s] \"%s\"", target.getName(), target.getTitle());
  }
}
