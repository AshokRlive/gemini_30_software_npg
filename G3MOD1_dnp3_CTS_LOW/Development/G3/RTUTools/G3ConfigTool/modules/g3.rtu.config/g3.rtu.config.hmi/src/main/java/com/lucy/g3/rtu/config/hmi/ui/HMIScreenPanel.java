/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.border.LineBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.hmi.domain.IScreenEditor;
import com.lucy.g3.rtu.config.hmi.domain.ISupportPreview;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager.HMIScreenChangeListener;
import com.lucy.g3.rtu.config.hmi.ui.editor.ControlScreenEditor;
import com.lucy.g3.rtu.config.hmi.ui.editor.DataScreenEditor;
import com.lucy.g3.rtu.config.hmi.ui.editor.InfoScreenEditor;
import com.lucy.g3.rtu.config.hmi.ui.editor.MenuScreenEditor;
import com.lucy.g3.rtu.config.hmi.ui.editor.PhaseScreenEditor;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * A panel for configuring HMI menu.
 */
public class HMIScreenPanel extends JPanel implements HMIScreenChangeListener {

  public static final String ACTION_KEY_ADD = "add";
  public static final String ACTION_KEY_ADDSUBSCREEN = "addSubScreen";
  public static final String ACTION_KEY_ADDCONTROLSCREEN = "addControlScreen";
  public static final String ACTION_KEY_ADDDATASCREEN = "addDataScreen";
  public static final String ACTION_KEY_ADDPHASESCREEN = "addPhaseScreen";
  public static final String ACTION_KEY_ADDINFOSCREEN = "addInfoScreen";
  public static final String ACTION_KEY_EDIT = "edit";
  public static final String ACTION_KEY_REMOVE = "remove";
  public static final String ACTION_KEY_REBUILD = "rebuild";
  // final static public String ACTION_KEY_VALIDATE= "validateHMIMenu";

  private static final String PROPERTY_REMOVE_ENABLED = "removeEnabled";
  private static final String PROPERTY_ADD_ENABLED = "addEnabled";
  private static final String PROPERTY_EDIT_ENABLED = "editEnabled";


  private Logger log = Logger.getLogger(HMIScreenPanel.class);

  private final JFrame parent = WindowUtils.getMainFrame();

  private JPopupMenu addMenu;

  private final HMIScreenManager manager;

  private DefaultTreeModel hmiTreeModel;

  private final IHMIGenerator generator;


  public HMIScreenPanel(HMIScreenManager manager) {
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
    this.manager.addScreenChangeListener(this);
    setName("HMI Menu");

    generator = ConfigGeneratorRegistry.getInstance().getGenerator(manager.getOwner(), IHMIGenerator.class);

    initComponents();
    initEventHandling();
    hmiScreenTree.setSelectionRow(0);
  }

  /**
   * Bean getter method for the property {@linkplain PROPERTY_REMOVE_ENABLED}
   */
  public boolean isRemoveEnabled() {
    Screen sel = getSelectedScreen();
    return sel != null && !sel.isRoot();
  }

  /**
   * Bean getter method for the property {@linkplain PROPERTY_ADD_ENABLED}
   */
  public boolean isAddEnabled() {
    Screen sel = getSelectedScreen();
    return sel != null && sel.getAllowsChildren();
  }

  /**
   * Bean getter method for the property {@linkplain PROPERTY_EDIT_ENABLED}
   */
  public boolean isEditEnabled() {
    return getSelectedScreen() != null;
  }

  private void initEventHandling() {
    hmiScreenTree.addTreeSelectionListener(new TreeSelectionListener () {
      @Override
      public void valueChanged(TreeSelectionEvent e) {
        selectionPathChanged(e.getPath());
      }
    });
    hmiScreenTree.addMouseListener(new ScreenTreeMouseListener());

    ApplicationActionMap actions = getActions();
    buttonAdd.setAction(actions.get(ACTION_KEY_ADD));
    buttonEdit.setAction(actions.get(ACTION_KEY_EDIT));
    buttonRemove.setAction(actions.get(ACTION_KEY_REMOVE));
    buttonRebuild.setAction(actions.get(ACTION_KEY_REBUILD));
    // btnValidate.setAction(actions.get(ACTION_KEY_VALIDATE));

    // Update action states
    fireActionStatesChangeEvent();
  }

  private Screen getSelectedScreen() {
    Object selectNode = hmiScreenTree.getLastSelectedPathComponent();
    if (selectNode != null && selectNode instanceof Screen) {
      return (Screen) selectNode;
    }

    return null;
  }


  private ApplicationActionMap getActions() {
    return Application.getInstance().getContext().getActionMap(this);
  }

  private JPopupMenu getAddButtonPopupMenu() {
    if (addMenu == null) {
      addMenu = new JPopupMenu();
      ApplicationActionMap actions = getActions();
      addMenu.add(actions.get(ACTION_KEY_ADDSUBSCREEN));
      addMenu.addSeparator();
      addMenu.add(actions.get(ACTION_KEY_ADDCONTROLSCREEN));
      addMenu.add(actions.get(ACTION_KEY_ADDDATASCREEN));
      addMenu.add(actions.get(ACTION_KEY_ADDPHASESCREEN));
      addMenu.add(actions.get(ACTION_KEY_ADDINFOSCREEN));
    }
    return addMenu;
  }

  private JPopupMenu getTreePopupMenu() {
    JPopupMenu popup = new JPopupMenu();
    ApplicationActionMap actions = getActions();
    if (isAddEnabled()) {
      JMenu addMenu = new JMenu(actions.get(ACTION_KEY_ADD));
      addMenu.add(actions.get(ACTION_KEY_ADDSUBSCREEN));
      addMenu.addSeparator();
      addMenu.add(actions.get(ACTION_KEY_ADDCONTROLSCREEN));
      addMenu.add(actions.get(ACTION_KEY_ADDDATASCREEN));
      addMenu.add(actions.get(ACTION_KEY_ADDPHASESCREEN));
      addMenu.add(actions.get(ACTION_KEY_ADDINFOSCREEN));
      popup.add(addMenu);
    }
    popup.add(actions.get(ACTION_KEY_EDIT));
    popup.add(actions.get(ACTION_KEY_REMOVE));
    // popup.add(actions.get(ACTION_KEY_VALIDATE));
    return popup;
  }

  // ================== Actions ===================

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ADD_ENABLED)
  public void add(ActionEvent e) {
    /* Show popup menu at source position */
    JComponent source = (JComponent) e.getSource();
    getAddButtonPopupMenu().show(source, source.getWidth() / 2, source.getHeight() / 2);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ADD_ENABLED)
  public void addSubScreen() {
    creatNew(new MenuScreenEditor());
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ADD_ENABLED)
  public void addControlScreen() {
    creatNew(new ControlScreenEditor());
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ADD_ENABLED)
  public void addDataScreen() {
    creatNew(new DataScreenEditor());
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ADD_ENABLED)
  public void addInfoScreen() {
    creatNew(new InfoScreenEditor());
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_ADD_ENABLED)
  public void addPhaseScreen() {
    creatNew(new PhaseScreenEditor());
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_REMOVE_ENABLED)
  public void remove() {
    TreePath path = hmiScreenTree.getSelectionPath();
    Screen selectedNode = (Screen) hmiScreenTree.getLastSelectedPathComponent();
    if (selectedNode == null)
    {
      return; // No selection
    }

    String msg = String.format("Do you want to remove %s \"%s\"?",
        selectedNode.getName(),
        selectedNode.getTitle());
    int result = JOptionPane.showConfirmDialog(parent,
        msg,
        "Delete",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.WARNING_MESSAGE);
    if (result == JOptionPane.OK_OPTION) {
      TreePath parentPath = path.getParentPath();
      Object parentObject = parentPath.getLastPathComponent();
      Screen deletedObject = (Screen) path.getLastPathComponent();
      int index = hmiScreenTree.getModel().getIndexOfChild(parentObject, deletedObject);

      ((DefaultTreeModel) hmiScreenTree.getModel()).removeNodeFromParent(deletedObject);

      deletedObject.delete();

      TreePath pathToSelect = parentPath;
      if (index >= ((TreeNode) parentObject).getChildCount()) {
        index = ((TreeNode) parentObject).getChildCount() - 1;
      }
      if (index > 0) {
        pathToSelect = parentPath.pathByAddingChild(((TreeNode) parentObject).getChildAt(index));
      }
      hmiScreenTree.setSelectionPath(pathToSelect);
      hmiScreenTree.scrollPathToVisible(pathToSelect);
    }
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_EDIT_ENABLED)
  public void edit() {
    Screen selectedScreen = (Screen) hmiScreenTree.getLastSelectedPathComponent();
    if (selectedScreen != null) {
      editScreen(selectedScreen, null);
    }
  }

  @org.jdesktop.application.Action
  public void rebuild() {
    int ret = JOptionPane.showConfirmDialog(parent,
        "This is going to delete all existing menu configuration and generate a default one."
        + "\nDo you really want to continue?",
        "Generate HMI Menu",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.WARNING_MESSAGE);
    if (ret == JOptionPane.YES_OPTION) {
      generator.generateScreensFromScratch(manager);
    }
  }

  // @org.jdesktop.application.Action
  // public void validateHMIMenu() {
  // List<ValidationResult> results = manager.validate();
  //
  // // Show report
  // JXErrorPane.showDialog(parent,
  // ConfigDataValidationTask.createErrorInfo(results));
  // }

  private void creatNew(IScreenEditor editor) {
    // Check parent selected
    TreePath path = hmiScreenTree.getSelectionPath();
    if (hmiScreenTree.getLastSelectedPathComponent() == null) {
      log.warn("Cannot add new screen, no selected node");
      return;
    }
    
    // Check allow to add
    Screen parentScreen = (Screen) hmiScreenTree.getLastSelectedPathComponent();
    if (!parentScreen.getAllowsChildren()) {
      log.error("Not allowed to add sub menu under " + parentScreen);
      return;
    }
    
    Screen screen = editor.createNew(parentScreen);
    if(screen != null)
      insertTreeNode(path, screen);
  }
  
  private void editScreen(Screen screen, ISupportPreview childItem) {
    @SuppressWarnings("unchecked")
    IScreenEditor<Screen> editor = (IScreenEditor<Screen>) screen.getEdtior();
    if(editor.edit(screen, childItem)) {
      DefaultTreeModel model = (DefaultTreeModel) hmiScreenTree.getModel();
      model.nodeChanged(screen);
      previewer.previewScreen(screen); // Refresh preview screen
    }
  }


  private void insertTreeNode(TreePath path, MutableTreeNode newNode) {
    MutableTreeNode parentNode = ((MutableTreeNode) path.getLastPathComponent());
    int index = parentNode.getChildCount();
    ((DefaultTreeModel) hmiScreenTree.getModel()).insertNodeInto(newNode, parentNode, index);
    TreePath newPath = path.pathByAddingChild(newNode);
    hmiScreenTree.setSelectionPath(newPath);
    hmiScreenTree.scrollPathToVisible(newPath);
  }

  private void selectionPathChanged(TreePath newSelectionPath) {
    // Update action states
    fireActionStatesChangeEvent();

    // Update preview
    previewer.previewScreen(getSelectedScreen());
  }

  private void fireActionStatesChangeEvent() {
    firePropertyChange(PROPERTY_REMOVE_ENABLED, null, isRemoveEnabled());
    firePropertyChange(PROPERTY_ADD_ENABLED, null, isAddEnabled());
    firePropertyChange(PROPERTY_EDIT_ENABLED, null, isEditEnabled());
  }

  private void createUIComponents() {

    // Create HMI Tree
    hmiTreeModel = new DefaultTreeModel(manager.getMainMenu());
    hmiScreenTree = new JXTree(hmiTreeModel);
    DefaultTreeCellRenderer render = new DefaultTreeCellRenderer();
    render.setLeafIcon(ResourceUtils.getImg("leaf.icon", HMIScreenPanel.class));
    render.setOpenIcon(ResourceUtils.getImg("open.icon", HMIScreenPanel.class));
    render.setClosedIcon(ResourceUtils.getImg("close.icon", HMIScreenPanel.class));
    hmiScreenTree.setCellRenderer(render);
    hmiScreenTree.setTransferHandler(new HMITreeTransferHandler());
    hmiScreenTree.setDragEnabled(true);
    hmiScreenTree.setDropMode(DropMode.ON_OR_INSERT);
    hmiScreenTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    // Set HMI tree renderer
    ColorHighlighter invalidNodeHL = new ColorHighlighter(new ErrorTreeNodePredicator(), null, Color.red);
    ColorHighlighter warningNodeHL = new ColorHighlighter(new WarningTreeNodePredicator(), null, Color.orange);
    hmiScreenTree.setHighlighters(warningNodeHL, invalidNodeHL);

    hmiScreenTree.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
    hmiScreenTree.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Edit");
    hmiScreenTree.getActionMap().put("Delete", getActions().get(ACTION_KEY_REMOVE));
    hmiScreenTree.getActionMap().put("Edit", getActions().get(ACTION_KEY_EDIT));
    
    previewer = new HMIScreenPreviewer(new PreviewerCallback());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();
    toolBar1 = new JToolBar();
    label2 = new JLabel();
    hSpacer1 = new JPanel(null);
    buttonAdd = new JButton();
    buttonEdit = new JButton();
    buttonRemove = new JButton();
    buttonRebuild = new JButton();
    scrollPane1 = new JScrollPane();
    lblDescription = new JXLabel();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
        "default:grow, $lcgap, default, $lcgap, default:grow",
        "default, $lgap, fill:pref, 10dlu, default, $lgap, fill:default:grow, $lgap, top:default:grow"));

    //---- label1 ----
    label1.setText("HMI Menu Preview:");
    add(label1, CC.xy(3, 1));
    add(previewer, CC.xy(3, 3));

    //======== toolBar1 ========
    {
      toolBar1.setFloatable(false);
      toolBar1.setRollover(true);

      //---- label2 ----
      label2.setText("HMI Menu Content:");
      toolBar1.add(label2);
      toolBar1.add(hSpacer1);

      //---- buttonAdd ----
      buttonAdd.setText("Add");
      toolBar1.add(buttonAdd);

      //---- buttonEdit ----
      buttonEdit.setText("Edit");
      toolBar1.add(buttonEdit);

      //---- buttonRemove ----
      buttonRemove.setText("Remove");
      toolBar1.add(buttonRemove);
      toolBar1.addSeparator();

      //---- buttonRebuild ----
      buttonRebuild.setText("Rebuild HMI");
      toolBar1.add(buttonRebuild);
    }
    add(toolBar1, CC.xywh(1, 5, 5, 1));

    //======== scrollPane1 ========
    {
      scrollPane1.setPreferredSize(new Dimension(74, 200));
      scrollPane1.setBorder(new LineBorder(Color.lightGray));
      scrollPane1.setViewportView(hmiScreenTree);
    }
    add(scrollPane1, CC.xywh(1, 7, 5, 1));

    //---- lblDescription ----
    lblDescription.setText("* Select a menu to preview or edit");
    lblDescription.setLineWrap(true);
    lblDescription.setFont(lblDescription.getFont().deriveFont(lblDescription.getFont().getStyle() | Font.ITALIC));
    add(lblDescription, CC.xywh(1, 9, 5, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private HMIScreenPreviewer previewer;
  private JToolBar toolBar1;
  private JLabel label2;
  private JPanel hSpacer1;
  private JButton buttonAdd;
  private JButton buttonEdit;
  private JButton buttonRemove;
  private JButton buttonRebuild;
  private JScrollPane scrollPane1;
  private JXTree hmiScreenTree;
  private JXLabel lblDescription;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  private class ScreenTreeMouseListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      if (UIUtils.isDoubleClick(e)) {
        /* Double click to edit leaf node*/
        Object sel = hmiScreenTree.getLastSelectedPathComponent();
        if (sel instanceof Screen && ((Screen) sel).isLeaf()) {
          edit();
        }
        
      } else {
        /* Preview the selected node*/
        previewer.previewScreen(getSelectedScreen());
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
      mayTriggerPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      mayTriggerPopup(e);
    }

    private void mayTriggerPopup(MouseEvent e) {
      if (e.isPopupTrigger()) {
        TreePath path = hmiScreenTree.getPathForLocation(e.getX(), e.getY());
        if (path != null) {
          hmiScreenTree.setSelectionPath(path);
          getTreePopupMenu().show((JComponent) e.getSource(), e.getX(), e.getY());
        }
      }
    }
  }

  private static class ErrorTreeNodePredicator implements HighlightPredicate {

    @Override
    public boolean isHighlighted(Component renderer,
        ComponentAdapter adapter) {
      Object treeNode = adapter.getValue();
      if (treeNode != null && treeNode instanceof Screen) {
        IValidator validator = ((Screen) treeNode).getValidator();
        if (validator != null) {
          ValidationResultExt result = validator.getResult();
          return result != null && result.hasErrors();
        }
      }
      return false;
    }

  }

  private static class WarningTreeNodePredicator implements HighlightPredicate {

    @Override
    public boolean isHighlighted(Component renderer,
        ComponentAdapter adapter) {
      Object treeNode = adapter.getValue();
      if (treeNode != null && treeNode instanceof Screen) {
        IValidator validator = ((Screen) treeNode).getValidator();
        if (validator != null) {
          ValidationResultExt result = validator.getResult();
          return result != null && result.hasWarnings();
        }
      }
      return false;
    }

  }


  @Override
  public void screenRemoved(Screen parent, Screen removedNode) {
    if (hmiTreeModel != null) {
      // Fire event to refresh tree
      hmiTreeModel.nodeStructureChanged(parent);
      selectionPathChanged(hmiScreenTree.getSelectionPath());
    }
  }

  @Override
  public void screenChanged(Screen screen) {
    hmiTreeModel.nodeChanged(screen);
  }

  @Override
  public void rootScreenChanged(MenuScreen newRoot) {
    hmiTreeModel.setRoot(newRoot);
    hmiTreeModel.nodeStructureChanged(newRoot);
  }

  @Override
  public void screenAdded(Screen parent, Screen addedChild) {
    hmiTreeModel.nodeStructureChanged(parent);

  }

  private class PreviewerCallback implements IHMIScreenPreviewerCallback {

    @Override
    public void openEditor(ISupportPreview toEdit) {
      if(toEdit != null) {
        editScreen(toEdit.getScreen(), toEdit);
      }
    }
    
  }
}
