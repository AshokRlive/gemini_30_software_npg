/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.awt.Component;

import javax.swing.JOptionPane;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;


public class MenuScreenEditor extends AbstractScreenEditor<MenuScreen>{
  private Component parent;
  
  public MenuScreenEditor() {
    this(WindowUtils.getMainFrame());
  }
  
  public  MenuScreenEditor(Component parent) {
    this.parent = parent;
  }
  
  @Override
  public boolean edit(MenuScreen screen, Object initialSelection) {
    return editTitle(screen);
  }
  
  @Override
  public MenuScreen createNew(Screen parentScreen) {
    MenuScreen newScreen = new MenuScreen(null);
    if(editTitle(newScreen)) {
      return newScreen;
    } else {
      return null;
    }
  }

  private boolean editTitle(MenuScreen screen) {
    Object result = JOptionPane.showInputDialog(parent, "Menu Title: ",
        screen.getName(),// Dialog title
        JOptionPane.QUESTION_MESSAGE, null, null,
        screen.getTitle()// Dialog content
    );
    
    if (result != null) {
      screen.setTitle(result.toString());
    }
    
    return result != null;
  }
}

