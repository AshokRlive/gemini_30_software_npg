/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import com.lucy.g3.rtu.config.hmi.domain.InfoScreen;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public class InfoScreenValidator extends AbstractScreenValidator<InfoScreen> {

  public InfoScreenValidator(InfoScreen target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {
    // Nothing to validate
  }

}
