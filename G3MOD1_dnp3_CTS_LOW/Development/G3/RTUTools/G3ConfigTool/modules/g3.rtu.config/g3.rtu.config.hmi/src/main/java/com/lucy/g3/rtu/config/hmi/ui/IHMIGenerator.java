/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.generator.IConfigGenerator;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.module.shared.domain.Module;


/**
 *
 */
public interface IHMIGenerator extends IConfigGenerator {
  void generateScreensFromScratch(HMIScreenManager manager);
  void createScreens(HMIScreenManager manager,Module m);
  void generateScreens(HMIScreenManager manager, IOperableLogic clogic); 

}

