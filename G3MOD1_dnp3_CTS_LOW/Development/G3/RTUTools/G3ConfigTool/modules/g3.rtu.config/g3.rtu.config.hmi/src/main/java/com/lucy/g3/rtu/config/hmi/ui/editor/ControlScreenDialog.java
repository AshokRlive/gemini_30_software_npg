/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.awt.Dimension;
import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;

/**
 * A dialog for configuring {@code SwitchScreen}.
 */
class ControlScreenDialog extends AbstractDialog {

  private final IOperableLogic[] controlLogics;

  private final ControlScreen screen;

  private JTextField titleField;

  private JComboBox modulesCombobox;


  public ControlScreenDialog(JFrame parent, ControlScreen screen, Collection<IOperableLogic> clogic) {
    super(parent);
    this.screen = Preconditions.checkNotNull(screen, "screen must not be null");
    this.controlLogics = clogic.toArray(new IOperableLogic[clogic.size()]);
    setTitle(screen.getName());

    pack();
    loadData();
  }

  private void loadData() {
    titleField.setText(screen.getTitle());
    titleField.selectAll();

    if (controlLogics != null) {
      modulesCombobox.setModel(new DefaultComboBoxModel(controlLogics));
    }

    modulesCombobox.setSelectedItem(screen.getCLogic());
  }

  private void saveData() {
    screen.setTitle(titleField.getText());
    screen.setSwitchLogic((IOperableLogic) modulesCombobox.getSelectedItem());
  }

  @Override
  public void cancel() {
    super.cancel();
  }

  @Override
  public void ok() {
    saveData();
    super.ok();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    FormLayout layout = new FormLayout("right:default, $lcgap,default:grow", "");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout);

    titleField = new JTextField();
    titleField.setColumns(10);
    builder.append("Title: ", titleField, true);
    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();
    modulesCombobox = new JComboBox();
    builder.append("Control Logic: ", modulesCombobox, true);
    builder.setDefaultDialogBorder();
    JPanel p = builder.getPanel();
    p.setPreferredSize(new Dimension(300, 200));
    return p;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

}
