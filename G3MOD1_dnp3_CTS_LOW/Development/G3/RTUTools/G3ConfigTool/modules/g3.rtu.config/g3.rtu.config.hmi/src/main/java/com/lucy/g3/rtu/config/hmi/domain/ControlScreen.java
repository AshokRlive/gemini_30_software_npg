/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.ArrayList;
import java.util.List;

import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.hmi.domain.validation.ControlScreenValidator;
import com.lucy.g3.rtu.config.hmi.ui.editor.ControlScreenEditor;
import com.lucy.g3.rtu.config.shared.model.impl.DefaultNode;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * A leaf {@code Screen} which is mapped to a {@code SwitchGearLogic}.
 */
public class ControlScreen extends Screen {

  private final ArrayList<ISupportPreview> switchPreviewItems;
  
  private final DefaultNode<IOperableLogic> switchLogicValueHolder = new DefaultNode<IOperableLogic>(
      NodeType.HMI_SCREEN, IOperableLogic.class);


  public ControlScreen(String title) {
    super(title);
    switchPreviewItems = createSwitchPreviewItems();
  }

  private ArrayList<ISupportPreview> createSwitchPreviewItems() {
    ArrayList<ISupportPreview> items = new ArrayList<>();
    items.add(StringPreviewItem.create(this, "Position: NA"));
    items.add(StringPreviewItem.create(this, "Operation: "));
    items.add(StringPreviewItem.create(this, "Error: "));
    return items;
  }

  @Override
  protected IValidator createValidator() {
    return new ControlScreenValidator(this);
  }

  @Override
  public boolean getAllowsChildren() {
    return false;
  }

  @Override
  public boolean isLeaf() {
    return true;
  }

  @Override
  public void delete() {
    switchLogicValueHolder.delete();
  }

  @Override
  public Screen copy() {
    ControlScreen copyScreen = new ControlScreen(getTitle());
    copyScreen.setSwitchLogic(getCLogic());
    return copyScreen;
  }

  public IOperableLogic getCLogic() {
    return switchLogicValueHolder.getSource();
  }

  public void setSwitchLogic(IOperableLogic scm) {
    switchLogicValueHolder.disconnectFromSource(getCLogic());
    switchLogicValueHolder.connectToSource(scm);
  }

  @Override
  public String getName() {
    return CONTROL_SCREEN_NAME;
  }

  @Override
  public List<ISupportPreview> getPreviewChildren() {
    return switchPreviewItems;
  }

  @Override
  public ControlScreenEditor getEdtior() {
    return new ControlScreenEditor();
  }

}
