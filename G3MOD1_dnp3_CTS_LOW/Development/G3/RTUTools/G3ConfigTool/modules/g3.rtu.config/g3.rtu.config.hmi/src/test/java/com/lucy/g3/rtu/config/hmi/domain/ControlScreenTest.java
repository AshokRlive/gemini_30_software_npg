/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;

public class ControlScreenTest {

  private ControlScreen fixture;
  private IOperableLogic scmLogic;


  @Before
  public void setUp() throws Exception {
    fixture = new ControlScreen("");
    scmLogic = new SwitchGearLogic();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetLogic() {
    assertNull(fixture.getCLogic());
    fixture.setSwitchLogic(scmLogic);
    assertTrue(scmLogic == fixture.getCLogic());

    // Set to a different logic
    scmLogic = new SwitchGearLogic();
    fixture.setSwitchLogic(scmLogic);
    assertTrue(scmLogic == fixture.getCLogic());
  }

  @Test
  public void testDeleteLogic() {
    fixture.setSwitchLogic(scmLogic);

    scmLogic.delete();
    assertNull(fixture.getCLogic());
  }

  @Test
  public void testDeleteScreen() {
    fixture.setSwitchLogic(scmLogic);

    fixture.delete();
    assertNull(fixture.getCLogic());
  }

}
