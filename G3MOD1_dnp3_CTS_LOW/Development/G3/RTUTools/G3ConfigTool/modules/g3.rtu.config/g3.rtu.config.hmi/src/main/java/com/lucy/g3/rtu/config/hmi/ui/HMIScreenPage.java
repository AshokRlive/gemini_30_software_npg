/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import java.awt.BorderLayout;

import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;


/**
 *
 */
public class HMIScreenPage extends AbstractConfigPage {
   private HMIScreenPanel hmiPanel;

  public HMIScreenPage(HMIScreenManager data) {
    super(data);
  }

  @Override
  protected void init() throws Exception {
    hmiPanel = new HMIScreenPanel((HMIScreenManager) getData());
    setName("HMI Screen");
    add(hmiPanel, BorderLayout.CENTER);
  }

}

