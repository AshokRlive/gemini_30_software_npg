/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;


public interface IScreenEditor<ScreenT extends Screen> {
  /**
   * Opens an editor to edit a screen.
   * @param screen
   * @param initialSelection
   * @return false if the screen editing was cancelled by user. 
   */
  boolean edit(ScreenT screen, Object initialSelection);
  
  /**
   * Opens an editor to edit a new created screen.
   * @return false if user called.
   */
  ScreenT createNew(Screen parentScreen);
}

