/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.util.Collection;

import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel.SelectionMode;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;


public class DataScreenEditor extends AbstractScreenEditor<DataScreen> {
      
  @Override
  public boolean edit(DataScreen screen, Object initialSelection) {
    DataScreenDialog editor = new DataScreenDialog(parent, screen, getAllPoints(screen));
    editor.pack();
    editor.selectEntry((DataEntry)initialSelection);
    editor.setVisible(true);
    return !editor.hasBeenCanceled();
  }

  @Override
  public DataScreen createNew(Screen parentScreen) {
    VirtualPointSelector ps = new VirtualPointSelector(parent, null, getAllPoints(parentScreen));
    ps.setSelectionMode(SelectionMode.SELECTION_MODE_MULTI);
    ps.showDialog();
    
    if(ps.hasBeenCanceled())
      return null;
    Collection<VirtualPoint> selectedPoints = ps.getSelectedPoints();
    
    DataScreen newScreen = new DataScreen(null);
    DataScreenDialog editor = new DataScreenDialog(parent, newScreen, getAllPoints(parentScreen));
    editor.pack();
    editor.createEntries(selectedPoints);
    editor.setVisible(true);
    
    return editor.hasBeenCanceled()?null : newScreen;
  }

}

