/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;

import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.ui.editor.DataScreenDialog;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;

public class DataScreenDialogTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  public static void main(String[] args) {
    DataEntry[] entries = new DataEntry[3];
    for (int i = 0; i < entries.length; i++) {
      entries[i] = new DataEntry();
      entries[i].setHeader("header" + i);
    }

    DataScreen screen = new DataScreen("DigitalData");
    screen.setDataEntries(entries);
    ArrayList<VirtualPoint> pointList = new ArrayList<VirtualPoint>();
    pointList.add(VirtualPointFactory.createAnalogPoint());
    pointList.add(VirtualPointFactory.createAnalogPoint());
    pointList.add(VirtualPointFactory.createAnalogPoint());
    ((AnaloguePoint) pointList.get(0)).setScalingFactor(0.1d);
    ((AnaloguePoint) pointList.get(0)).setUnit("aa");

    entries[0].setSource(pointList.get(0));
    new DataScreenDialog(null, screen, pointList).setVisible(true);
    System.out.println(Arrays.toString(screen.getDataEntries()));
  }

}
