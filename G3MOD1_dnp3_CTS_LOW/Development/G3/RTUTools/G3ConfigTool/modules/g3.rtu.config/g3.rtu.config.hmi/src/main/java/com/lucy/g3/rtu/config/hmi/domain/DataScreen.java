/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.lucy.g3.rtu.config.hmi.domain.validation.DataScreenValidator;
import com.lucy.g3.rtu.config.hmi.ui.editor.DataScreenEditor;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * A leaf screen that lists a list of data entries.
 */
public class DataScreen extends Screen {

  private final ArrayList<DataEntry> dataEntries = new ArrayList<DataEntry>(30);

  public DataScreen(String title) {
    this(title, null);
  }

  public DataScreen(String title, DataEntry[] entries) {
    super(title);

    if (entries != null && entries.length > 0) {
      dataEntries.addAll(Arrays.asList(entries));
      
      for (int i = 0; i < entries.length; i++) {
        entries[i].setScreen(this);
      }
    }
  }
  

  @Override
  protected IValidator createValidator() {
    return new DataScreenValidator(this);
  }

  @Override
  public void delete() {
    setDataEntries(null);
  }

  public List<DataEntry> getDataEntryList() {
    return new ArrayList<DataEntry>(dataEntries);
  }
  
  public DataEntry[] getDataEntries() {
    return dataEntries.toArray(new DataEntry[dataEntries.size()]);
  }

  public void setDataEntries(DataEntry[] newEntries) {

    for (DataEntry entry : dataEntries) {
      entry.delete();
      entry.setScreen(null);
    }

    dataEntries.clear();

    if (newEntries != null) {
      dataEntries.addAll(Arrays.asList(newEntries));
      dataEntries.removeAll(Collections.singleton(null));// remove all null
      
      for (DataEntry entry : dataEntries) {
        entry.setScreen(this);
      }
    }
  }

  @Override
  public boolean getAllowsChildren() {
    return false;
  }

  @Override
  public boolean isLeaf() {
    return true;
  }

  @Override
  public Screen copy() {
    return new DataScreen(getTitle(), getDataEntries());
  }

  @Override
  public String getName() {
    return DATA_SCREEN_NAME;
  }

  @Override
  public List<DataEntry> getPreviewChildren() {
    return dataEntries;
  }

  @Override
  public IScreenEditor<?> getEdtior() {
    return new DataScreenEditor();
  }

}
