/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import com.lucy.g3.rtu.config.hmi.domain.ISupportPreview;


interface IHMIScreenPreviewerCallback {

  void openEditor(ISupportPreview toEdit);
}

