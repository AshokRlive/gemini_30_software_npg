/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;

import org.jdesktop.application.Action;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.ext.swing.table.HighlightPredicateAdapter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.ui.dialog.selector.VirtualPointSelector;

public class DataScreenDialog extends AbstractDialog {

  private static final String ACTION_KEY_ADD = "addEntry";
  private static final String ACTION_KEY_REMOVE = "removeEntry";

  private static final String PROPERTY_REMOVE_ENABLED = "removeEnabled";

//  private static final String[] DEFAULT_FOOTERS = { "V", "mV", "A", "mA" };

  private DataScreen screen;

  // Data entries list
  private final ArrayListModel<DataEntry> entryList = new ArrayListModel<>();

  // Data entries table model
  @SuppressWarnings("unchecked")
  private final DataEntriesTableModel dataEntryTableModel = new DataEntriesTableModel(entryList);

  // Points for selecting
  private final Collection<VirtualPoint> availablePoints;

  private JTextField titleField;
  private JTable dataEntryTable;
  private JToolBar toolbar;


  /**
   * Constructor.
   *
   * @param parent
   *          parent frame.
   * @param screen
   *          the screen to be edited.
   * @param selectedPoints
   *          the pre-selected points for the <code>DataScreen</code>
   * @param availablePoints
   *          available points for selection.
   */
  DataScreenDialog(JFrame parent,
      DataScreen screen,
      Collection<VirtualPoint> availablePoints) {
    super(parent);
    if (screen == null) {
      throw new IllegalArgumentException("screen is null");
    }
    setTitle(screen.getName());
    this.screen = screen;
    this.availablePoints = availablePoints;
    
    /*
     * Since this is a StandardDialog, pack is required before next step in
     * order to initialise components.
     */
    pack();
    
    loadData(screen);
  }

  public void createEntries(Collection<VirtualPoint> selectedPoints) {
    if (selectedPoints == null || selectedPoints.isEmpty()) {
      return;
    }

    for (VirtualPoint p : selectedPoints) {
      if (p != null) {
        DataEntry entry = new DataEntry();
        entry.setSource(p);
        entryList.add(0, entry);
      }
    }
  }

  /**
   * Bean getter method for the property PROPERTY_REMOVE_ENABLED.
   */
  public boolean isRemoveEnabled() {
    return dataEntryTable != null && dataEntryTable.getSelectedRow() >= 0;
  }

  private void loadData(DataScreen screen) {
    titleField.setText(screen.getTitle());
    titleField.selectAll();
    
    // Copy& Load existed entries in the screen
    DataEntry[] entriesInScreen = screen.getDataEntries();
    for (int i = 0; i < entriesInScreen.length; i++) {
      entryList.add(entriesInScreen[i].copy());
    }
  }

  private void saveData(DataScreen screen) {
    screen.setTitle(titleField.getText());
    screen.setDataEntries(entryList.toArray(new DataEntry[entryList.size()]));
  }

  private JTable createDataEntryTable() {
    /* Create table */
    JXTable entryTable = new JXTable(dataEntryTableModel);
    entryTable.setRowHeight(20);
    entryTable.setSortable(false);
    entryTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(ListSelectionEvent e) {
        firePropertyChange(PROPERTY_REMOVE_ENABLED, null, isRemoveEnabled());
      }
    });
    entryTable.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

    /* Set column widths */
    TableColumnModel cModel = entryTable.getColumnModel();
    cModel.getColumn(DataEntriesTableModel.COLUMN_FOOTER).setMaxWidth(100);
    cModel.getColumn(DataEntriesTableModel.COLUMN_HEADER).setMaxWidth(100);

    /* Set cell editor for COLUMN_POINT */
    JComboBox<?> combo;
    if (availablePoints != null) {
      combo = new JComboBox<>(availablePoints.toArray());
    } else {
      combo = new JComboBox<>();
    }
    combo.setPreferredSize(new Dimension(500, 300));
    cModel.getColumn(DataEntriesTableModel.COLUMN_FOOTER).setCellRenderer(new FootRenderer());

    /* Set cell editor for COLUMN_FOOTER */
    // combo = new JComboBox(DEFAULT_FOOTERS);
    // combo.setEditable(true);
    // cModel.getColumn(DataEntriesTableModel.COLUMN_FOOTER)
    // .setCellEditor(new DefaultCellEditor(combo));

    /* Set cell editor for COLUMN_BROWSE */
    cModel.getColumn(DataEntriesTableModel.COLUMN_POINT)
        .setCellEditor(new PointSelectEditor());
    cModel.getColumn(DataEntriesTableModel.COLUMN_POINT)
        .setCellRenderer(new PointSelectRenderer());

    /* Set highlighter */
    entryTable.addHighlighter(new ColorHighlighter(
        new HighlightPredicateAdapter(dataEntryTableModel),
        UIThemeResources.COLOUR_ERROR_BACKGROUND, null));
    return entryTable;
  }

  @Override
  public void cancel() {
    super.cancel();
  }

  @Override
  public void ok() {
    saveData(screen);
    super.ok();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    /* Show example information in the banner */
    String title = "Example Data Screen";
    String subTitle = "<html>" +
        "<table align=\"center\" border=\"0\" color=#C0C0C0 bgcolor=#3333FF>" +
        "<caption color=black>[Title] PSM Analogue</caption>" +
        "<tr >" +
        "<th>Header</th>" +
        "<th>Point Value</th>" +
        "<th>Footer</th>" +
        "</tr>" +
        "<tr >" +
        "<td>PSM LS</td>" +
        "<td>2751</td>" +
        "<td>V</td>" +
        "</tr>" +
        "<tr >" +
        "<td>PSM Peak Hold</td>" +
        "<td>0</td>" +
        "<td>A</td>" +
        "</tr>" +
        "</table>" +
        "</html>";
    BannerPanel banner = new BannerPanel();
    banner.setTitle(title);
    banner.setSubtitle(subTitle);
    return banner;
  }

  @Override
  protected JComponent createContentPanel() {
    FormLayout layout = new FormLayout("default, $lcgap,default, $lcgap,default:grow",
        "default, $ugap, default, $rgap, fill:default:grow");
    final int totalColumns = 5;

    DefaultFormBuilder builder = new DefaultFormBuilder(layout);
    titleField = new JTextField();
    titleField.setColumns(10);
    builder.append("Title:", titleField, true);
    builder.nextLine();// Skip Row Gap

    // Toolbar
    toolbar = new JToolBar();
    toolbar.setFloatable(false);
    toolbar.add(new JLabel("Data Entries:"));
    toolbar.add(Box.createHorizontalGlue());
    toolbar.add(new JButton(getAction(ACTION_KEY_ADD)));
    toolbar.add(new JButton(getAction(ACTION_KEY_REMOVE)));
    builder.append(toolbar, totalColumns);
    builder.nextLine();// Next line
    builder.nextLine();// Skip Row Gap

    // Entry Table
    dataEntryTable = createDataEntryTable();
    JScrollPane scrollPane = new JScrollPane(dataEntryTable);
    scrollPane.setPreferredSize(new Dimension(400, 200));
    builder.append(scrollPane, totalColumns);

    return builder.getPanel();
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

  public void selectEntry(DataEntry selection) {
    if(selection == null)
      return;
    int index = screen.getDataEntryList().indexOf(selection);
    if(index >= 0) {
      dataEntryTable.getSelectionModel().setSelectionInterval(index, index);
      UIUtils.scrollTable(dataEntryTable, index, 0);
    }
  }
  
  @Action
  public void addEntry() {
    int index = dataEntryTable.getSelectedRow() + 1;
    entryList.add(index, new DataEntry());
    dataEntryTable.getSelectionModel().setSelectionInterval(index, index);
  }

  @Action(enabledProperty = PROPERTY_REMOVE_ENABLED)
  public void removeEntry() {
    int[] rows = dataEntryTable.getSelectedRows();
    if (rows == null || rows.length == 0) {
      return;
    }
    
    ArrayList<DataEntry> removeItems = new ArrayList<DataEntry>();
    for (int i = 0; i < rows.length; i++) {
      removeItems.add(entryList.get(rows[i]));
    }
    entryList.removeAll(removeItems);
  }

  private static class DataEntriesTableModel extends AbstractTableAdapter<DataEntry> implements ISupportHighlight {

    private static final String[] COLUMNS_NAMES= { "Header", "Footer", "Point"};

    public static final int COLUMN_HEADER = 0;
    public static final int COLUMN_FOOTER = 1;
    public static final int COLUMN_POINT = 2;


    public DataEntriesTableModel(ListModel<DataEntry> model) {
      super(model, COLUMNS_NAMES);
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
      DataEntry entry = getRow(row);
      switch (column) {
      case COLUMN_HEADER:
        if (aValue != null) {
          entry.setHeader(aValue.toString());
        }
        break;

      case COLUMN_FOOTER:
        break; // Footer not editable.

      case COLUMN_POINT:
        if (aValue != null && aValue instanceof VirtualPoint) {
          entry.setSource((VirtualPoint) aValue);
        } else {
          entry.setSource(null);
        }
        fireTableRowsUpdated(row, row);
        break;
      default:
        break;
      }
    }

    @Override
    public Object getValueAt(int row, int column) {
      DataEntry entry = getRow(row);
      switch (column) {
      case COLUMN_HEADER:
        return entry.getHeader();
      case COLUMN_FOOTER:
        return entry.getFooter();
      case COLUMN_POINT:
        return entry.getSource();
      default:
        return "N/A";
      }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
      return columnIndex != COLUMN_FOOTER;
    }

    @Override
    public boolean shouldHightlight(int rowIndex, int columnIndex) {
      if (columnIndex == COLUMN_POINT && getValueAt(rowIndex, columnIndex) == null) {
        return true;
      }
      return false;
    }
  }

  private class PointSelectEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {

    protected static final String EDIT = "edit";
    private JButton button;
    private VirtualPoint curValue;


    public PointSelectEditor() {
      button = new JButton();
      button.setActionCommand(EDIT);
      button.setHorizontalAlignment(SwingConstants.LEFT);
      button.setFocusable(false);
      button.addActionListener(this);
      button.setBorderPainted(false);
      button.setBorder(BorderFactory.createEmptyBorder());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      VirtualPointSelector selector = new VirtualPointSelector(DataScreenDialog.this, curValue, availablePoints);
      VirtualPoint vp = selector.showDialog();
      if (selector.hasBeenCanceled() == false) {
        curValue = vp;
      }

      stopCellEditing();
    }

    @Override
    public Object getCellEditorValue() {
      return curValue;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      curValue = (VirtualPoint) value;
      button.setText(curValue == null ? PointSelectRenderer.TEXT_NOT_CONFIGURED : curValue.getName());
      return button;
    }
  }

  private static class PointSelectRenderer extends DefaultTableRenderer {
    static final String TEXT_NOT_CONFIGURED = "- Not Configured -";

    public PointSelectRenderer() {
      super(new StringValue() {
        
        @Override
        public String getString(Object value) {
          VirtualPoint point = (VirtualPoint) value;
          return point == null ? TEXT_NOT_CONFIGURED : point.getName();
        }
      });
    }
  }
  
  private static class FootRenderer extends DefaultTableRenderer {
    
    public FootRenderer() {
      super(new StringValue() {
        
        @Override
        public String getString(Object value) {
          return value == null ? "-" : String.valueOf(value);
        }
      });
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      comp.setEnabled(false);
      if(comp instanceof JComponent)
        ((JComponent)comp).setToolTipText("Footer is not allowed to edit!");
      
      return comp;
    }
    
    
  }

}
