/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import javax.swing.UIManager;

import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.ui.HMIScreenPanel;
import com.lucy.g3.rtu.config.hmi.ui.HMIScreenPreviewer;
import com.lucy.g3.test.support.utilities.TestUtil;


public class HMIMenuPanelTest {

  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } 
    catch (Throwable e) {
      e.printStackTrace();
    }
    
    
    HMIScreenManager manager = new HMIScreenManager(null);
    manager.getMainMenu().add(new MenuScreen("1 3 5 7 9 1 3 5 7 91"));
    manager.getMainMenu().add(new MenuScreen(HMIScreenPreviewer.getSampleStr('A', HMIScreenPreviewer.MAX_COLUMNS)));
    manager.getMainMenu().add(new MenuScreen(HMIScreenPreviewer.getSampleStr('B', HMIScreenPreviewer.MAX_COLUMNS)));
    manager.getMainMenu().add(new MenuScreen(HMIScreenPreviewer.getSampleStr('1', HMIScreenPreviewer.MAX_COLUMNS)));
    HMIScreenPanel panel = new HMIScreenPanel(manager);
    
    TestUtil.showFrame(panel);
  }

}

