/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.xml;

import com.lucy.g3.rtu.config.clogic.xml.ICLogicXmlReadSupport;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;

/**
 *
 */
public interface IHMIXmlReadSupport extends VirtualPointReadSupport, ICLogicXmlReadSupport {

  /**
   * @param pointGroup
   * @param pointId
   * @return
   * @Deprecated replace with Virtual Point Reference. Schema change required.
   */
  @Deprecated
  VirtualPoint getVirtualPoint(short pointGroup, short pointId);

}
