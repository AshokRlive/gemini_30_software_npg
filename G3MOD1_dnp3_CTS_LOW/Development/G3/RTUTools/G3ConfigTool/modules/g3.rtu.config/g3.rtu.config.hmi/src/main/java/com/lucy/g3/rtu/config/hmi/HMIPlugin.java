/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.ui.HMIScreenPage;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class HMIPlugin implements IConfigPlugin,IConfigModuleFactory, IPageFactory {

  private HMIPlugin() {
  }


  private final static HMIPlugin INSTANCE = new HMIPlugin();


  public static HMIPlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory factory = ConfigFactory.getInstance();
    
    factory.registerFactory(HMIScreenManager.CONFIG_MODULE_ID, INSTANCE);
    
    PageFactories.registerFactory(PageFactories.KEY_HMI, INSTANCE);
  }

  @Override
  public HMIScreenManager create(IConfig owner) {
    return new HMIScreenManager(owner);
  }

  @Override
  public Page createPage(Object data) {
    if(data instanceof HMIScreenManager) {
      return new HMIScreenPage((HMIScreenManager) data);
    }
    
    return null;
  }

}
