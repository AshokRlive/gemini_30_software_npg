/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lucy.g3.rtu.config.hmi.domain.validation.PhaseScreenValidator;
import com.lucy.g3.rtu.config.hmi.ui.editor.PhaseScreenEditor;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * A leaf screen that contains a list of virtual points for phase configuration
 * and footer for HMI displaying.
 */
public class PhaseScreen extends Screen {

  public static final String[] POINT_NAMES = { "I 0", "V 0", "I 1", "V 1",
      "I2", "V2", "In" };

  public static final int POINTS_NUM = POINT_NAMES.length;

  private final PhaseNode[] pointNodes; // Configured virtual points for this
  // phase screen.

  private String currentFooter;

  private String voltageFooter;


  public PhaseScreen(String title) {
    this(title, null);
  }

  public PhaseScreen(String title, VirtualPoint[] points) {
    super(title);
    pointNodes = new PhaseNode[POINTS_NUM];
    for (int i = 0; i < pointNodes.length; i++) {
      pointNodes[i] = new PhaseNode(POINT_NAMES[i]);
    }

    if (points != null) {
      // check array length
      if (points.length != POINTS_NUM) {
        throw new IllegalArgumentException("Invalid length of points: "
            + points.length
            + "  Expected points num in Phase Screen is: "
            + POINTS_NUM);
      }

      for (int i = 0; i < points.length; i++) {
        pointNodes[i].connectToSource(points[i]);
      }

    }

  }

  @Override
  protected IValidator createValidator() {
    return new PhaseScreenValidator(this);
  }
  
  public int getPointsNum() {
    return POINTS_NUM;
  }

  /**
   * Configure the virtual point of this phase screen.
   *
   * @param p
   * @param index
   *          the index of virtual point between 0 and {@code POINTS_NUM-1}.
   */
  public void setPoint(VirtualPoint p, int index) {
    if (index < 0 || index >= POINTS_NUM) {
      throw new IllegalArgumentException("Index " + index
          + " out of boundary: 0~" + (POINTS_NUM - 1));
    }

    pointNodes[index].disconnectFromSource(pointNodes[index].getSource());
    pointNodes[index].connectToSource(p);
  }

  /**
   * Gets the virtual point at specific position for this phase screen.
   *
   * @return one of the following virtual points <li>l1 <li>V1 <li>l2 <li>V2 <li>
   *         l3 <li>V3 <li>ln
   */
  public VirtualPoint getPoint(int index) {
    if (index >= 0 && index < POINTS_NUM) {
      return pointNodes[index].getSource();
    } else {
      return null;
    }
  }

  public PhaseNode[] getPointNodes() {
    return Arrays.copyOf(pointNodes, pointNodes.length);
  }

  @Override
  public boolean getAllowsChildren() {
    return false;
  }

  @Override
  public boolean isLeaf() {
    return true;
  }

  @Override
  public Screen copy() {
    VirtualPoint[] points = new VirtualPoint[pointNodes.length];
    for (int i = 0; i < points.length; i++) {
      points[i] = pointNodes[i].getSource();
    }

    PhaseScreen copyScreen = new PhaseScreen(getTitle(), points);
    return copyScreen;
  }

  public String getCurrentFooter() {
    return currentFooter;
  }

  public void setCurrentFooter(String currentFooter) {
    this.currentFooter = currentFooter;
  }

  public String getVoltageFooter() {
    return voltageFooter;
  }

  public void setVoltageFooter(String voltageFooter) {
    this.voltageFooter = voltageFooter;
  }

  @Override
  public String getName() {
    return PHASE_SCREEN_NAME;
  }

  @Override
  public List<ISupportPreview> getPreviewChildren() {
    
    String NA = "N/A";
    List<ISupportPreview> list = new ArrayList<>();
    // For each line
    for (int i = 0; i < POINTS_NUM; i++) {
      StringBuilder builder = new StringBuilder();

      // Current
      builder.append(POINT_NAMES[i]);
      builder.append(": ");
      if (pointNodes[i].getSource() != null) {
        builder.append(pointNodes[i].getSource().getGroupID());
      } else {
        builder.append(NA);
      }
      if (currentFooter != null) {
        builder.append(currentFooter);
      }

      // Voltage
      if (i < POINTS_NUM - 1) {
        i++;
        builder.append("                   ");
        builder.append(POINT_NAMES[i]);
        builder.append(": ");
        if (pointNodes[i].getSource() != null) {
          builder.append(pointNodes[i].getSource().getGroupID());
        } else {
          builder.append(NA);
        }
      }
      if (voltageFooter != null) {
        builder.append(voltageFooter);
      }
      list.add(StringPreviewItem.create(this, builder.toString()));
    }
    
    return list;
  }

  @Override
  public void delete() {
    for (int i = 0; i < pointNodes.length; i++) {
      pointNodes[i].delete();
    }
  }

  
  @Override
  public PhaseScreenEditor getEdtior() {
    return new PhaseScreenEditor();
  }

}
