/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.apache.log4j.Logger;

import com.g3schema.ns_hmi.HMIControlScreenT;
import com.g3schema.ns_hmi.HMIDataScreenT;
import com.g3schema.ns_hmi.HMIInfoScreenT;
import com.g3schema.ns_hmi.HMIMenuScreenT;
import com.g3schema.ns_hmi.HMIPhaseScreenT;
import com.g3schema.ns_hmi.HMIVPointDataEntryT;
import com.g3schema.ns_hmi.ScreensT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.domain.InfoScreen;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.PhaseScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.manager.RootScreen;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.HMI_INFO_SCREEN_TYPE;

/**
 *
 */
public class HMIScreenXmlReader {

  private Logger log = Logger.getLogger(HMIScreenXmlReader.class);

  private final IHMIXmlReadSupport support;

  private final HMIScreenManager manager;


  public HMIScreenXmlReader(HMIScreenManager manager, IHMIXmlReadSupport support) {
    this.support = Preconditions.checkNotNull(support, "support is null");
    this.manager = Preconditions.checkNotNull(manager, "manager is null");
  }

  public ValidationResult read(ScreensT xml_screens) {
    return read(xml_screens, null);
  }

  public ValidationResult read(ScreensT xml_screens, ValidationResult result) {
    if (result == null) {
      result = new ValidationResult();
    }

    ArrayList<Screen> allScreens = new ArrayList<Screen>();
    int rootID = xml_screens.rootScreen.first().rootScreenID.getValue();

    // Load Menu Screens
    int count = xml_screens.menuScreens.count();
    for (int i = 0; i < count; i++) {
      HMIMenuScreenT xml_menu = xml_screens.menuScreens.at(i);
      int screenID = xml_menu.screenID.getValue();
      String title = xml_menu.screenTitle.getValue();
      MenuScreen screen = (screenID == rootID) ? new RootScreen(title) : new MenuScreen(title);
      screen.setId(screenID);
      allScreens.add(screen);
    }

    // Load Switch Screens
    count = xml_screens.controlScreens.count();
    for (int i = 0; i < count; i++) {
      HMIControlScreenT xml_switch = xml_screens.controlScreens.at(i);
      ControlScreen screen = new ControlScreen(xml_switch.screenTitle.getValue());
      screen.setId(xml_switch.screenID.getValue());
      int group = (int) xml_switch.controlLogicID.getValue();
      screen.setSwitchLogic(support.getOutputCLogic(group));
      allScreens.add(screen);
    }

    // Load Phase Screens
    count = xml_screens.phaseScreens.count();
    for (int i = 0; i < count; i++) {
      HMIPhaseScreenT xml_phase = xml_screens.phaseScreens.at(i);
      PhaseScreen screen = new PhaseScreen(xml_phase.screenTitle.getValue());
      screen.setId(xml_phase.screenID.getValue());
      int num = xml_phase.virtualPointEntry.count();

      for (int j = 0; j < PhaseScreen.POINTS_NUM && j < num; j++) {
        screen.setPoint(support.getVirtualPointByRef(xml_phase.virtualPointEntry.at(j)), j);
      }

      if (xml_phase.currentFooter.exists()) {
        screen.setCurrentFooter(xml_phase.currentFooter.getValue());
      }

      if (xml_phase.voltageFooter.exists()) {
        screen.setVoltageFooter(xml_phase.voltageFooter.getValue());
      }

      allScreens.add(screen);
    }

    // Load All Data Entries
    ArrayList<DataEntry> allEntries = new ArrayList<DataEntry>();
    count = xml_screens.dataEntries.count();
    for (int i = 0; i < count; i++) {
      HMIVPointDataEntryT xml_entry = xml_screens.dataEntries.at(i);
      DataEntry entry = new DataEntry();
      entry.setId((int) xml_entry.entryID.getValue());
      short pointGroup = (short) xml_entry.pointGroup.getValue();
      short pointId = (short) xml_entry.pointID.getValue();
      entry.setSource(support.getVirtualPoint(pointGroup, pointId));

      // Verify Footer
      if (xml_entry.footer.exists()) {
        if (!xml_entry.footer.getValue().equals(entry.getFooter())) {
          log.warn("Mismatched footer in HMI DataScreen:" + entry);
        }
      }

      // Header
      if (xml_entry.header.exists()) {
        entry.setHeader(xml_entry.header.getValue());
      }
      allEntries.add(entry);
    }
    // Sort before binary search
    Collections.sort(allEntries, new DataEntryComparator());

    // Load All Data Screens
    count = xml_screens.dataScreens.count();
    // For each data screen
    for (int i = 0; i < count; i++) {
      // Create data screen
      HMIDataScreenT xml_data = xml_screens.dataScreens.at(i);
      DataScreen screen = new DataScreen(xml_data.screenTitle.getValue());
      screen.setId(xml_data.screenID.getValue());

      int entriesCount = xml_data.entries.count();
      ArrayList<DataEntry> childEntries = new ArrayList<DataEntry>();
      // Search and add child entries
      for (int j = 0; j < entriesCount; j++) {
        DataEntry key = new DataEntry();
        key.setId((int) xml_data.entries.at(j).entryID.getValue());
        int pos = Collections.binarySearch(allEntries, key, new DataEntryComparator());
        if (pos >= 0) {
          childEntries.add(allEntries.get(pos));
        } else {
          result.addError("Data entry : " + key.getId() + " not found for DataScreen: " + screen);
          result.addError("Existed Data Entries: " + allEntries.toString());
        }
      }
      screen.setDataEntries(childEntries.toArray(new DataEntry[childEntries.size()]));

      allScreens.add(screen);
    }

    // Load All Info Screens
    count = xml_screens.infoScreens.count();
    // For each data screen
    for (int i = 0; i < count; i++) {
      // Create data screen
      HMIInfoScreenT xml_data = xml_screens.infoScreens.at(i);
      InfoScreen screen = new InfoScreen(xml_data.screenTitle.getValue());
      screen.setId(xml_data.screenID.getValue());
      screen.setType(HMI_INFO_SCREEN_TYPE.forValue((int) xml_data.type.getValue()));
      allScreens.add(screen);
    }

    Comparator<Screen> comparator = new ScreenComparator();
    Collections.sort(allScreens, comparator);

    // Load Menu Screens' children
    count = xml_screens.menuScreens.count();
    for (int i = 0; i < count; i++) {
      HMIMenuScreenT xml_menu = xml_screens.menuScreens.at(i);
      int childCount = xml_menu.childList.count();
      MenuScreen key = new MenuScreen(null);

      // Search parent
      key.setId(xml_menu.screenID.getValue());
      int pos = Collections.binarySearch(allScreens, key, comparator);
      MenuScreen parent = null;
      if (pos >= 0) {
        Screen p = allScreens.get(pos);
        if (p instanceof MenuScreen) {
          parent = (MenuScreen) p;
        } else {
          log.error("Invalid parent screen: " + p + " ID: " + p.getId());
          continue;
        }
      } else {
        log.error("Fail to load menu screen's children. Menu screen: " + key + " not found.");
        continue;
      }

      // Search children for the parent
      for (int j = 0; j < childCount; j++) {
        long childID = xml_menu.childList.at(j).screenID.getValue();
        key.setId((int) childID);
        pos = Collections.binarySearch(allScreens, key, comparator);
        if (pos >= 0) {
          Screen child = allScreens.get(pos);
          parent.add(child);
        } else {
          result.addError("Child screen: " + key + " not found.");
        }
      }
    }

    // Set root menu
    RootScreen key = new RootScreen(null);
    key.setId(rootID);
    int pos = Collections.binarySearch(allScreens, key, comparator);
    Screen rootScreen = allScreens.get(pos);
    if (pos >= 0 && rootScreen instanceof RootScreen) {
      manager.setMainMenu((RootScreen) rootScreen);
    } else {
      result.addError("Fail to find root menu. ID: " + rootID);
    }

    return result;
  }


  private static class DataEntryComparator implements Comparator<DataEntry> {
    @Override
    public int compare(DataEntry o1, DataEntry o2) {
      if (o2 == null || o1 == null) {
        return 1;
      }
      return o1.getId() - o2.getId();
    }
  }
  private static class ScreenComparator implements Comparator<Screen> {

    @Override
    public int compare(Screen o1, Screen o2) {
      return (o1.getId() - o2.getId());
    }
  }
}
