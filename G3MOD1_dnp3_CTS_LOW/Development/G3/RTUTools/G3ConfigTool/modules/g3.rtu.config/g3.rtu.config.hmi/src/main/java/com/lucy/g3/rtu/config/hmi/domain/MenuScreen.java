/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import com.lucy.g3.rtu.config.hmi.domain.validation.MenuScreenValidator;
import com.lucy.g3.rtu.config.hmi.ui.editor.MenuScreenEditor;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 * A screen that contains sub screens.
 */
public class MenuScreen extends Screen {

  public MenuScreen(String title) {
    super(title);
  }
  
  

  @Override
  protected IValidator createValidator() {
    return new MenuScreenValidator(this);
  }


  @Override
  public boolean getAllowsChildren() {
    return true;
  }

  @Override
  public Screen copy() {
    Screen copy = new MenuScreen(getTitle());
    for (int i = 0; i < getChildCount(); i++) {
      copy.add(((Screen) getChildAt(i)).copy());
    }
    return copy;
  }

  @Override
  public String getName() {
    return MENU_SCREEN_NAME;
  }

  @Override
  public String toString() {
    return getTitle();
  }

  @Override
  public List<Screen> getPreviewChildren() {
    Enumeration<Screen> children = children();
    return Collections.list(children);
  }

  @Override
  public boolean isLeaf() {
    return false;
  }

  @Override
  public void delete() {
    int count = getChildCount();
    for (int i = 0; i < count; i++) {
      Screen child = (Screen) getChildAt(i);
      child.delete();
    }

    removeAllChildren();
  }



  @Override
  public MenuScreenEditor getEdtior() {
    return new MenuScreenEditor();
  }
}
