/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

public class DataScreenValidator extends AbstractScreenValidator<DataScreen> {


  public DataScreenValidator(DataScreen target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {
    DataEntry[] entries = target.getDataEntries();
    final String pathString = target.getPathString();

    if (entries == null || entries.length == 0) {
      result.addWarning("Data Screen",
          " has no data entries configured at: " + pathString);
    }

    // Validate entries
    String error;
    int entryIndex = 0;
    String entryPath;
    for (DataEntry entry : entries) {
      if (entry != null) {

        error = "";

        VirtualPoint point = entry.getSource();

        if (point == null) {
          error = "is not configured with Virtual Point";
        }
        else {
          if (!point.getValidator().isValid()) {
            error = "is configured with an invalid Virtual Point";
          }
        }

        if (error != null && !error.isEmpty()) {
          entryPath = String.format("%s > Data Entry[%s]",
              pathString, entryIndex);
          result.addError(entryPath + error);
        }
      }
      entryIndex++;
    }

  }

}
