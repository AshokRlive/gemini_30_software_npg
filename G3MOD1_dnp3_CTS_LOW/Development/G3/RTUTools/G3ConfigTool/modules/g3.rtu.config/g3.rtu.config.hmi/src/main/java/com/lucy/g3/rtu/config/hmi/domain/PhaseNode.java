/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import com.lucy.g3.rtu.config.shared.model.impl.DefaultNode;
import com.lucy.g3.rtu.config.shared.model.impl.NodeType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

/**
 * The Class PhaseNode.
 */
public class PhaseNode extends DefaultNode<VirtualPoint> {

  public PhaseNode(String name) {
    super(NodeType.HMI_SCREEN, VirtualPoint.class);
    setName(name);
  }
}
