/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.hmi.domain.ISupportPreview;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumHMI;

public class HMIScreenPreviewer extends JPanel {
  public final static int MAX_ROWS = ModuleProtocolEnumHMI.HMI_MAX_DISPLAY_ROW; // lines
  public final static int MAX_COLUMNS = ModuleProtocolEnumHMI.HMI_MAX_DISPLAY_COL; // characters
  
  public final static Color LED_FG_COLOR = Color.lightGray;
  public final static Color LED_BG_COLOR = new Color(51, 51, 255);
  public final static Font  LED_FONT = loadLEDFont();
  
  private final ArrayListModel<Object> previewModel = new ArrayListModel<>(); 
  private ISupportPreview screen;
  
  private JList<Object> previewList;
  private JLabel previewHeader;

  private final IHMIScreenPreviewerCallback callback;
  
  public HMIScreenPreviewer() {
    this(null);
  }
  
  public HMIScreenPreviewer(IHMIScreenPreviewerCallback callback) {
    this.callback = callback;
    initComponents();
  }
  
  public void previewScreen(ISupportPreview screen) {
    this.screen = screen;
    previewModel.clear();
    
    if (screen != null) {
      // Add children 
      List<?> children = screen.getPreviewChildren();
      if (children != null && children.size() > 0) {
        previewModel.addAll(children);
      }

      // Select first line
      previewList.setSelectedIndex(0); 
    }

    updatePreviewerHeader();
  }

  
  /*
   * Update the text of previewer header.
   */
  private void updatePreviewerHeader() {
    String header = "";
    
    if (screen != null) {
      int totalLine = previewList.getModel().getSize();
      int sel = previewList.getSelectedIndex() + 1;
      String num = " " + sel + "/" + totalLine;
      header = trim(screen.getPreviewText(), MAX_COLUMNS - num.length());
      header += num;
    }
    
    previewHeader.setText(header);
  }
  
  @SuppressWarnings("unchecked")
  private void createUIComponents() {
    /* Create preview list */
    previewList = new JList<>(previewModel);
    previewList.setCellRenderer(new PreviewListRender());
    previewList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    previewList.setFixedCellWidth(previewList.getFontMetrics(LED_FONT).stringWidth(getSampleStr('X', MAX_COLUMNS)));
    previewList.setVisibleRowCount(MAX_ROWS - 1/*Title Row*/);
    previewList.setBackground(LED_BG_COLOR);
    previewList.setForeground(LED_FG_COLOR);
    Dimension dim = calculateSize(previewList, LED_FONT, getSampleStr('A', MAX_COLUMNS));
    previewList.setFixedCellHeight((int) dim.getHeight());
    previewList.setFixedCellWidth((int) dim.getWidth());
    previewList.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        updatePreviewerHeader();
      }
    });
    previewList.addMouseListener(new PreviewListMouseListener());
    
    
    /* Create preview header */
    previewHeader = new JLabel();
    previewHeader.setText("header");
    previewHeader.setToolTipText("Menu Title");
    previewHeader.setHorizontalAlignment(SwingConstants.CENTER);
    previewHeader.setOpaque(true);
    previewHeader.setCursor(new Cursor(Cursor.HAND_CURSOR));
    previewHeader.setForeground(Color.lightGray);
    previewHeader.setBackground(new Color(51, 51, 255));
    previewHeader.setFont(LED_FONT);
    previewHeader.setPreferredSize(dim);
    previewHeader.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        if(callback != null && e.getClickCount() > 1) {
          callback.openEditor(screen);
        }
      }
      
    });
  }
  
  private void initComponents() {
    createUIComponents();
    
    setLayout(new BorderLayout());
    
    JScrollPane  scrollPane0 = new JScrollPane();
    scrollPane0.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scrollPane0.setBorder(new BevelBorder(BevelBorder.LOWERED));
    scrollPane0.setViewportView(previewList);
    scrollPane0.setColumnHeaderView(previewHeader);
    add(scrollPane0, BorderLayout.CENTER);
    scrollPane0.getVerticalScrollBar().setBlockIncrement(scrollPane0.getVerticalScrollBar().getVisibleAmount());
  }


  private class PreviewListRender extends DefaultListCellRenderer {

    public PreviewListRender() {
      super();
      setOpaque(false);
      setFont(LED_FONT);
      previewList.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      setForeground(list.getForeground());
      String text;
      ISupportPreview item = (ISupportPreview) value;
      
      text = item == null ? "": item.getPreviewText();
      
      setToolTipText(item == null ? null : item.getPreviewTips());
      
      if(item != null && item.allowsPreviewChildren()) {
        if (isSelected) {
          text = trim(text, MAX_COLUMNS - 4);
          text = "->" + text + "<-";
        }
        setHorizontalAlignment(SwingConstants.CENTER);
      } else {
        setHorizontalAlignment(SwingConstants.LEFT);
      }

      setText(trim(text, MAX_COLUMNS));
      return this;
    }
  }
  
  private class PreviewListMouseListener extends MouseAdapter {
    
    /*Double click to open editor*/
    @Override
    public void mouseClicked(MouseEvent e) {
      if(e.getClickCount() > 1) {
        ISupportPreview selected = (ISupportPreview) previewList.getSelectedValue();
        if(selected != null) {
          callback.openEditor(selected);
        }
      }
    }

    @Override
    public void mousePressed(MouseEvent e) {
      mayShowPopupMen(e);
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
      mayShowPopupMen(e);
    }

    /*
     * Shows context menu to enter into child menu if the event is popup trigger.
     */
    private void mayShowPopupMen(MouseEvent e){

      if (e.isPopupTrigger()) {
        int index = previewList.locationToIndex(e.getPoint());
        if (index >= 0) {
          final ISupportPreview selected = (ISupportPreview) previewList.getModel().getElementAt(index);
          if(selected.allowsPreviewChildren()) {
            JPopupMenu menu = new JPopupMenu();
            JMenuItem menuEnter= new JMenuItem("Enter");
            menuEnter.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                previewScreen(selected);
              }
            });
            menu.add(menuEnter);
            menu.show(previewList, e.getX(), e.getY());
          }
        }
      }
    
    }
  }
  
  private static String trim(String text, int maxLength){
    if(text.length() > maxLength)
      text = text.substring(0, maxLength);
    return text;
  }
  
  static Font loadLEDFont(){
    Logger log = Logger.getLogger(HMIScreenPreviewer.class);
    
    try {
      InputStream ledFontStream = PreviewListRender.class.getClassLoader().getResourceAsStream("fonts/LCD5x8H-edited.ttf");
      Font ledFont = Font.createFont(Font.TRUETYPE_FONT, ledFontStream);
      ledFont = ledFont.deriveFont(24.0f);
      return ledFont;
      
    } catch (FontFormatException e) {
      log.error("Invalid LED font. ", e);
    } catch (IOException e) {
      log.error("LED font not found. ", e);
    }
    
    return null;
  }

  static Dimension calculateSize(JComponent comp, Font font, String text){
      int width = comp.getFontMetrics(font).stringWidth(text)  + 10 /*extra space for avoid overlapping*/;
      int height = comp.getFontMetrics(font).getHeight()  + 16 /*extra space for avoid overlapping*/;
      
      return new Dimension(width, height);
  }
  
  static String getSampleStr(char character, int size){
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < size; i++) {
      sb.append(character);
    }
    return sb.toString();
  }
}
