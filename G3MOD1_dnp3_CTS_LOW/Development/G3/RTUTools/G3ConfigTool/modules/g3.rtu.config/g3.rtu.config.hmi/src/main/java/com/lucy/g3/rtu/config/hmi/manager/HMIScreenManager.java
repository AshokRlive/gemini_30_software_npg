/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.manager;

import java.util.ArrayList;
import java.util.Collection;

import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.domain.validation.HMIScreenManagerValidator;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;

/**
 * The HMIScreenManager is for managing the root node of the HMI menu tree and
 * updating the ID of all children {@code Screen} to keep them consistent and
 * unique.
 */
public class HMIScreenManager extends AbstractConfigModule implements IConfigModule,IContainerValidation{
  public static final String CONFIG_MODULE_ID = HMI_SCREEN_MANAGER_ID;

  private static int id;
  private RootScreen  root;

  private final ArrayList<HMIScreenChangeListener> listeners;

  private final IContainerValidator validator;


  public HMIScreenManager(IConfig owner) {
    super(owner);
    root = new RootScreen("Main Menu");
    listeners = new ArrayList<HMIScreenChangeListener>();
    validator = new HMIScreenManagerValidator(this);
  }

  public void setMainMenu(RootScreen mainMenu) {
    
    if (mainMenu == null) {
      return;
    }
    
    if(this.root != null)
      this.root.setManager(null);

    this.root = mainMenu;
    this.root.setManager(this);
    
    for (HMIScreenChangeListener listener : listeners) {
      listener.rootScreenChanged(mainMenu);
    }
  }

  public void updateAllScreensID() {
    id = 0;
    updateScreenID(root);
  }

  private void updateScreenID(Screen screen) {
    screen.setId(id++);
    for (int i = 0; i < screen.getChildCount(); i++) {
      updateScreenID((Screen) screen.getChildAt(i));
    }
  }

  public RootScreen getMainMenu() {
    return root;
  }

  public void removeAllScreens(Collection<Screen> screens) {
    if (screens != null) {
      for (Screen s : screens) {
        removeScreen(s);
      }
    }
  }

  public void removeScreen(Screen screen) {
    if (screen != null) {
      Screen parent = (Screen) screen.getParent();
      screen.removeFromParent();
      fireScreenRemoved(parent, screen);
    }
  }

  public void fireScreenChanged(Screen screen) {
    for (HMIScreenChangeListener listener : listeners) {
      listener.screenChanged(screen);
    }
  }

  public void fireScreenAdded(Screen parent, Screen addedChild) {
    for (HMIScreenChangeListener listener : listeners) {
      listener.screenAdded(parent, addedChild);
    }
  }

  public void fireScreenRemoved(Screen parent, Screen removedNode) {
    if (parent == null || removedNode == null) {
      return;
    }

    for (HMIScreenChangeListener listener : listeners) {
      listener.screenRemoved(parent, removedNode);
    }
  }

  public void addScreenChangeListener(HMIScreenChangeListener listener) {
    if (listener == null) {
      return;
    }

    if (!listeners.contains(listener)) {
      listeners.add(listener);
    }
  }

  public void removeScreenChangeListener(HMIScreenChangeListener listener) {
    if (listener == null) {
      return;
    }

    if (listeners.contains(listener)) {
      listeners.remove(listener);
    }

  }


  public interface HMIScreenChangeListener {

    void screenRemoved(Screen parent, Screen removedChild);

    void screenAdded(Screen parent, Screen addedChild);

    void screenChanged(Screen changedScreen);

    void rootScreenChanged(MenuScreen newRoot);
  }


  @Override
  public IContainerValidator getValidator() {
    return validator;
  }
}
