/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import static org.junit.Assert.assertEquals;
import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;

/**
 *
 *
 */
public class MenuScreenTest {

  private MenuScreen fixture;
  final private String TITLE = "menu screen 0";


  @Before
  public void setUp() throws Exception {
    fixture = new MenuScreen(TITLE);
  }

  @After
  public void tearDown() throws Exception {
    fixture = null;
  }

  @Test
  public void testAddChild() {
    Screen[] children = addChildren();

    assertEquals(children.length, fixture.getChildCount());
    for (int i = 0; i < children.length; i++) {
      assertEquals(children[i], fixture.getChildAt(i));
    }
  }

  @Test
  public void testDelete() {
    Screen[] children = addChildren();

    assertEquals(children.length, fixture.getChildCount());
    fixture.delete();
    assertEquals(0, fixture.getChildCount());
  }

  private Screen[] addChildren() {
    Screen[] children = new Screen[4];
    for (int i = 0; i < children.length; i++) {
      children[i] = new MenuScreen("child" + i);
      fixture.add(children[i]);
    }
    return children;
  }

  @Test
  public void testCopy() {
    Screen[] children = addChildren();

    Screen copy = fixture.copy();
    assertEquals(TITLE, copy.getTitle());

    assertEquals(children.length, copy.getChildCount());
    for (int i = 0; i < children.length; i++) {
      assertEquals(children[i].getTitle(), ((Screen) fixture.getChildAt(i)).getTitle());
    }
  }

  @Test
  public void testValidate() {
    fixture.getValidator().validate();
    Assert.assertTrue("menu screen must be always valid", fixture.getValidator().isValid());
  }

  @Test
  public void testName() {
    assertEquals(Screen.MENU_SCREEN_NAME, fixture.getName());
  }

}
