/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import java.util.Enumeration;

import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public class MenuScreenValidator extends AbstractScreenValidator<MenuScreen> {

  public MenuScreenValidator(MenuScreen target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {

    Enumeration<Screen> children = target.children();
    while (children.hasMoreElements()) {
      result.addAllFrom(children.nextElement().getValidator().validate());
    }
  }
}
