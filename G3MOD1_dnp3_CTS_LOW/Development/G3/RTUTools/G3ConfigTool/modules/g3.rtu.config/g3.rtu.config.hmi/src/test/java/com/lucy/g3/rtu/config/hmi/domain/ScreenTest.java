/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.lucy.g3.rtu.config.hmi.domain.IScreenEditor;
import com.lucy.g3.rtu.config.hmi.domain.ISupportPreview;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;

/**
 *
 *
 *
 */
public class ScreenTest {

  @Test
  public void testSetTitle() {
    final String newTitle = "NewTitle";
    final String oldTitle = "oldTitle";

    Screen screen = new TestScreen(oldTitle);
    assertEquals(oldTitle, screen.getTitle());
    screen.setTitle(newTitle);
    assertEquals(newTitle, screen.getTitle());
  }


  private class TestScreen extends Screen {

    public TestScreen(String title) {
      super(title);
    }
    
    @Override
    protected IValidator createValidator() {
      return null;
    }
    

    @Override
    public String getName() {
      return null;
    }

    @Override
    public Screen copy() {
      return null;
    }

    @Override
    public List<ISupportPreview> getPreviewChildren() {
      return null;
    }

    @Override
    public void delete() {
      // TODO Auto-generated method stub

    }

    @Override
    public IScreenEditor<?> getEdtior() {
      // TODO Auto-generated method stub
      return null;
    }

  }
}
