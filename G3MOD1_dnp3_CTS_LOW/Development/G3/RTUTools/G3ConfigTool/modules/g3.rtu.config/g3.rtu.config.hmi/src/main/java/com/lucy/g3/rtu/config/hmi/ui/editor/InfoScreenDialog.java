/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui.editor;

import java.awt.Dimension;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.rtu.config.hmi.domain.InfoScreen;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.HMI_INFO_SCREEN_TYPE;

/**
 * A dialog for configuring {@code SwitchScreen}.
 */
class InfoScreenDialog extends AbstractDialog {

  private JTextField tfTitle;
  private JComboBox comboBoxInfoType;

  private final InfoScreen screen;


  public InfoScreenDialog(JFrame parent, InfoScreen screen) {
    super(parent);
    this.screen = Preconditions.checkNotNull(screen, "screen must not be null");
    setTitle(screen.getName());

    pack();
  }

  private void saveData() {
    screen.setTitle(tfTitle.getText());
    screen.setType((HMI_INFO_SCREEN_TYPE) comboBoxInfoType.getSelectedItem());
  }

  @Override
  public void cancel() {
    super.cancel();
  }

  @Override
  public void ok() {
    saveData();
    super.ok();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    FormLayout layout = new FormLayout("right:default, $lcgap,default:grow", "");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout);

    tfTitle = new JTextField(screen.getTitle());
    tfTitle.setColumns(10);
    builder.append("Title: ", tfTitle, true);
    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();
    comboBoxInfoType = new JComboBox(HMI_INFO_SCREEN_TYPE.values());
    comboBoxInfoType.setSelectedItem(screen.getType());
    builder.append("Info Type: ", comboBoxInfoType, true);
    builder.setDefaultDialogBorder();
    JPanel p = builder.getPanel();
    p.setPreferredSize(new Dimension(300, 200));
    return p;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

}
