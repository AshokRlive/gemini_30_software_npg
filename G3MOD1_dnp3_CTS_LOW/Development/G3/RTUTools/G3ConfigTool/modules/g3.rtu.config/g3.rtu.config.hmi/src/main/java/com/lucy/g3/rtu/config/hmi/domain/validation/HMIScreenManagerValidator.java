/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.tree.TreeNode;

import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.shared.validation.AbstractContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 *
 */
public class HMIScreenManagerValidator extends AbstractContainerValidator<HMIScreenManager> {

  public HMIScreenManagerValidator(HMIScreenManager target) {
    super(target);
  }

  @Override
  public Collection<IValidation> getValidatableChildrenItems() {
    if (checkHMIExist()) {
      return new ArrayList<IValidation>(getAllChildren(target.getMainMenu()));
    } else {
      return new ArrayList<IValidation>(0);
    }
  }

  private boolean checkHMIExist() {
    ModuleRepository repo = target.getOwner().getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    
    return repo != null && !repo.getModulesByType(MODULE.MODULE_HMI).isEmpty();
  }

  private static Collection<Screen> getAllChildren(Screen screen) {
    ArrayList<Screen> children = new ArrayList<Screen>();
    int ccount = screen.getChildCount();
    for (int i = 0; i < ccount; i++) {
      TreeNode child = screen.getChildAt(i);
      if (child instanceof Screen) {
        children.add((Screen) child);
        children.addAll(getAllChildren((Screen) child));
      }
    }

    return children;
  }

  @Override
  public String getTargetName() {
    return "HMI Screens";
  }

}
