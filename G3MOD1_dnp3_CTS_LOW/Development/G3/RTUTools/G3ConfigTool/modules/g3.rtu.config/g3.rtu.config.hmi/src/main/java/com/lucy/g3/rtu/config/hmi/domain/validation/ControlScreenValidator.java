/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.domain.validation;

import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The Class ControlScreenValidator.
 */
public class ControlScreenValidator extends AbstractScreenValidator<ControlScreen> {

  public ControlScreenValidator(ControlScreen target) {
    super(target);
  }

  @Override
  protected void validate(ValidationResultExt result) {

    if (target.getCLogic() == null) {
      result.addError("Switch Logic is not configured at: " + target.getPathString());

    } else {
      if (!target.getCLogic().getValidator().isValid()) {
        result.addError("Switch Logic is invalid at: " + target.getPathString());
      }
    }

  }
}
