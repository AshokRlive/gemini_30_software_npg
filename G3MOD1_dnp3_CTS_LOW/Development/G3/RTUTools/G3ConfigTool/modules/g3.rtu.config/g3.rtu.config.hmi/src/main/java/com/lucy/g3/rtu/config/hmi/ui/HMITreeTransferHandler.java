/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.hmi.ui;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.config.hmi.domain.Screen;

class HMITreeTransferHandler extends TransferHandler {

  private Logger log = Logger.getLogger(HMITreeTransferHandler.class);
  DataFlavor nodesFlavor;
  DataFlavor[] flavors = new DataFlavor[1];
  Screen[] nodesToRemove;


  public HMITreeTransferHandler() {
    try {
      String mimeType = DataFlavor.javaJVMLocalObjectMimeType +
          ";class=\"" +
          Screen[].class.getName() +
          "\"";
      nodesFlavor = new DataFlavor(mimeType);
      flavors[0] = nodesFlavor;
    } catch (ClassNotFoundException e) {
      log.debug("ClassNotFound: " + e.getMessage());
    }

  }

  @Override
  public boolean canImport(TransferHandler.TransferSupport support) {
    if (!support.isDrop()) {
      return false;
    }
    support.setShowDropLocation(true);

    if (!support.isDataFlavorSupported(nodesFlavor)) {
      if (log.isDebugEnabled()) {
        log.warn("Unsupported flavor. Expected flavor: " + Arrays.toString(support.getDataFlavors()));
      }
      return false;
    }

    // Do not allow a drop on the drag source selections.
    JTree.DropLocation dl = (JTree.DropLocation) support.getDropLocation();
    JTree tree = (JTree) support.getComponent();
    int dropRow = tree.getRowForPath(dl.getPath());
    int[] selRows = tree.getSelectionRows();
    if (selRows != null) {
      for (int i = 0; i < selRows.length; i++) {
        if (selRows[i] == dropRow) {
          if (log.isDebugEnabled()) {
            log.debug("Do not allow a drop on the drag source selections.");
          }
          return false;
        }
      }
    }

    // Do not allow a non-leaf node to be copied to a level
    // which is less than its source level.
    TreePath dest = dl.getPath();
    if (dest == null) {
      return false;
    }
    Screen target = (Screen) dest.getLastPathComponent();

    return target.getAllowsChildren();
  }

  @Override
  protected Transferable createTransferable(JComponent c) {
    JTree tree = (JTree) c;
    TreePath[] paths = tree.getSelectionPaths();
    if (paths != null) {
      // Make up a node array of copies for transfer and
      // another for/of the nodes that will be removed in
      // exportDone after a successful drop.
      List<Screen> copies = new ArrayList<Screen>();
      List<Screen> toRemove = new ArrayList<Screen>();
      Screen node = (Screen) paths[0].getLastPathComponent();
      Screen copy = node.copy();
      copies.add(copy);
      toRemove.add(node);
      for (int i = 1; i < paths.length; i++) {
        Screen next =
            (Screen) paths[i].getLastPathComponent();
        // Do not allow higher level nodes to be added to list.
        if (next.getLevel() < node.getLevel()) {
          break;
        } else if (next.getLevel() > node.getLevel()) { // child node
          copy.add(next.copy());
          // node already contains child
        } else { // sibling
          copies.add(next.copy());
          toRemove.add(next);
        }
      }
      Screen[] nodes = copies.toArray(new Screen[copies.size()]);
      nodesToRemove = toRemove.toArray(new Screen[toRemove.size()]);
      return new NodesTransferable(nodes);
    }
    return null;
  }

  @Override
  protected void exportDone(JComponent source, Transferable data, int action) {
    if ((action & MOVE) == MOVE) {
      JTree tree = (JTree) source;
      DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
      // Remove nodes saved in nodesToRemove in createTransferable.
      for (int i = 0; i < nodesToRemove.length; i++) {
        model.removeNodeFromParent(nodesToRemove[i]);
      }
    }
  }

  @Override
  public int getSourceActions(JComponent c) {
    return COPY_OR_MOVE;
  }

  @Override
  public boolean importData(TransferHandler.TransferSupport support) {
    if (!canImport(support)) {
      return false;
    }
    // Extract transfer data.
    Screen[] nodes = null;
    try {
      Transferable t = support.getTransferable();
      nodes = (Screen[]) t.getTransferData(nodesFlavor);
    } catch (UnsupportedFlavorException ufe) {
      log.error("UnsupportedFlavor: " + ufe.getMessage());
    } catch (java.io.IOException ioe) {
      log.error("I/O error: " + ioe.getMessage());
    }
    // Get drop location info.
    JTree.DropLocation dl =
        (JTree.DropLocation) support.getDropLocation();
    int childIndex = dl.getChildIndex();
    TreePath dest = dl.getPath();
    Screen parent =
        (Screen) dest.getLastPathComponent();
    JTree tree = (JTree) support.getComponent();
    DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
    // Configure for drop mode.
    int index = childIndex; // DropMode.INSERT
    if (childIndex == -1) { // DropMode.ON
      index = parent.getChildCount();
    }
    // Add data to model.
    for (int i = 0; i < nodes.length; i++) {
      model.insertNodeInto(nodes[i], parent, index++);
    }
    return true;
  }

  @Override
  public String toString() {
    return getClass().getName();
  }


  public class NodesTransferable implements Transferable {

    Screen[] nodes;


    public NodesTransferable(Screen[] nodes) {
      this.nodes = nodes;
    }

    @Override
    public Object getTransferData(DataFlavor flavor)
        throws UnsupportedFlavorException {
      if (!isDataFlavorSupported(flavor)) {
        throw new UnsupportedFlavorException(flavor);
      }
      return nodes;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
      return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
      return nodesFlavor.equals(flavor);
    }
  }
}
