/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import org.junit.Assert;
import org.junit.Test;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 *
 */
public class ChangePassDialogTest {

  @Test
  public void testLoadPolicy(){
    Assert.assertTrue(PasswordPolicy.validatePassword2("Pdfa@_12".toCharArray()) == null);
  }
  
  public static void main(String[] args) {
    UserConfig user = UserConfig.createWithPlainPassword("test", "", USER_LEVEL.USER_LEVEL_ADMIN, UserConfig.MD5);
    new ChangePassDialog(null, null, user).setVisible(true);
  }

}

