/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;

import com.jgoodies.common.base.Strings;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 * The manager of all configured user accounts.
 */
public class UserManager extends AbstractConfigModule {

  private static final String PROPERTY_INVALID_LOGON_ATTEMPTS_THRESHOLD = "invalidLogonAttemptsThreshold";

  private static final String PROPERTY_ACCOUNT_LOCKOUT_DURATION_SECS = "accountLockoutDurationSecs";

  private static final UserConfig DEF_USER 
    = UserConfig.createWithPlainPassword("Admin", "", USER_LEVEL.USER_LEVEL_ADMIN, SecurityUtils.MD_ALGORITHM_SHA512);

  public static final String CONFIG_MODULE_ID = "UserManager";

  private ArrayListModel<UserConfig> userList = new ArrayListModel<UserConfig>();

  private int invalidLogonAttemptsThreshold = 3;
  private int accountLockoutDurationSecs = 300;

  public UserManager(IConfig owner) {
    super(owner);
    add(DEF_USER);
  }
  
  public boolean isAdmin(UserConfig user) {
    return user != null && user.getName().equals(DEF_USER.getName());
  }
  
  @SuppressWarnings("unchecked")
  public ListModel<UserConfig> getUserListModel() {
    return userList;
  }

  public List<UserConfig> getUserList() {
    return new ArrayList<UserConfig>(userList);
  }

  public void add(UserConfig newUser) {
    if (newUser == null) {
      throw new IllegalArgumentException("The new user is empty!");
    }

    if (userList.contains(newUser)) {
      throw new IllegalArgumentException("User name \"" + newUser.getName() + "\" already exists!");
    }
    
    if(checkPinExists(newUser.getPIN())){
      throw new IllegalArgumentException("The PIN already exists!");
    }

    userList.add(newUser);
  }

  public boolean checkPinExists(String PIN) {
    if(!Strings.isBlank(PIN)) {
      for (UserConfig u : userList) {
        if(PIN.equals(u.getPIN()))
          return true;
      }
    }
    return false;
  }

  public void loadAllUser(List<UserConfig> allUsers) {
    if (!allUsers.isEmpty()) {
      userList.clear();
      userList.addAll(allUsers);
      if (userList.contains(DEF_USER) == false) {
        userList.add(0, DEF_USER);
      }
    }
  }

  public void remove(UserConfig user) {
    if (user.getName().equals(DEF_USER.getName())) {
      throw new RuntimeException("Not allowed to remove default user account:" + DEF_USER.getName());
    }
    if (user != null) {
      userList.remove(user);
    }
  }

  public boolean isDefaultUser(UserConfig user) {
    return user == DEF_USER;
  }

  public boolean contains(String userName) {
    for (UserConfig user : userList) {
      if(user.getName().equals(userName))
        return true;
    }
    
    return false;
  }

  
  public int getInvalidLogonAttemptsThreshold() {
    return invalidLogonAttemptsThreshold;
  }

  
  public void setInvalidLogonAttemptsThreshold(int invalidLogonAttemptsThreshold) {
    if(accountLockoutDurationSecs < 0) {
      log.error("Invalid invalidLogonAttemptsThreshold: " + invalidLogonAttemptsThreshold);
      return;
    }
    
    Object oldValue = this.invalidLogonAttemptsThreshold;
    this.invalidLogonAttemptsThreshold = invalidLogonAttemptsThreshold;
    firePropertyChange(PROPERTY_INVALID_LOGON_ATTEMPTS_THRESHOLD, oldValue, invalidLogonAttemptsThreshold);
  }
  
  public int getAccountLockoutDurationSecs() {
    return accountLockoutDurationSecs;
  }
  
  public void setAccountLockoutDurationSecs(int accountLockoutDurationSecs) {
    if(accountLockoutDurationSecs < 0) {
      log.error("Invalid accountLockoutDurationSecs: " +accountLockoutDurationSecs);
      return;
    }
    
    Object oldValue = this.accountLockoutDurationSecs;
    this.accountLockoutDurationSecs = accountLockoutDurationSecs;
    firePropertyChange(PROPERTY_ACCOUNT_LOCKOUT_DURATION_SECS, oldValue, accountLockoutDurationSecs);
  }
  
}
