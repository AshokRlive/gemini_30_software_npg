/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.util.Arrays;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 * User account configuration.
 */
public class UserConfig {
  public final static String PIN_MASK = "####"; 
  public final static int PIN_LENGTH = PIN_MASK.length(); 
  
  public final static String MD5 = SecurityUtils.MD_ALGORITHM_MD5;
  public final static String SHA1 = SecurityUtils.MD_ALGORITHM_SHA1;
  public final static String SHA256= SecurityUtils.MD_ALGORITHM_SHA256;
  public final static String SHA512= SecurityUtils.MD_ALGORITHM_SHA512;
  
  public final static String PIN_MD_ALGORITHM = SecurityUtils.MD_ALGORITHM_SHA512;
  
  private String name;
  private String passwordHash;
  private USER_LEVEL role;
  private final String algorithm;
  private String pin ="";

  /**
   * Construct a user account.
   */
  public UserConfig(String name, String passwordHash, USER_LEVEL role, String mdAlgorithm) {
    this.name = Preconditions.checkNotBlank(name, "The name must not be null, empty or whitespace");
    this.role = Preconditions.checkNotNull(role, "The role must not be null");
    this.algorithm = Preconditions.checkNotNull(mdAlgorithm, "The mdAlgorithm must not be null");
    this.passwordHash = Preconditions.checkNotNull(passwordHash, "The passwordHash must not be null");
  }
  
  public String getName() {
    return name;
  }

  public String getAlgorithm(){
    return algorithm;
  }
  
  public String getPasswordHash(){
    return passwordHash;
  }

  public USER_LEVEL getRole() {
    return role;
  }
  
  public void setRole(USER_LEVEL newRole){
    this.role = Preconditions.checkNotNull(newRole, "newRole must not be null");;
  }

  public void setPassword(String newPassword) {
    if (newPassword == null) {
      throw new IllegalArgumentException("New password must not be null!");
    }

    this.passwordHash = SecurityUtils.calculateMDStr(newPassword.toCharArray(), algorithm);
  }

  public String getPIN() {
    return pin;
  }
  
  /**
   * @param pin pin code cipher
   */
  public void setPIN(String pin) {
    this.pin = pin;
  }
  
  /**
   * @param pinPlain plain pin code typed by users 
   */
  public void setPINAsPlain(String pinPlain) {
    setPIN(Strings.isBlank(pinPlain)?pinPlain:SecurityUtils.calculateMDStr(pinPlain.toCharArray(), PIN_MD_ALGORITHM));
  }

  @Override
  public String toString() {
    return String.format(" [%s] %s", role.getDescription(), name);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    UserConfig other = (UserConfig) obj;
    if (name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!name.equals(other.name)) {
      return false;
    }
    return true;
  }
  
  public static UserConfig createWithPlainPassword(String name, String plainPassword, USER_LEVEL role, String mdAlgorithm) {
    String passwordHash = SecurityUtils.calculateMDStr(plainPassword.toCharArray(), mdAlgorithm);
    return new UserConfig(name, passwordHash, role, mdAlgorithm);
  }

  
  public static String[] getSupportedMD(){ 
    return Arrays.copyOf(SUPPORTED_MD, SUPPORTED_MD.length);
  }
  
  private final static String[] SUPPORTED_MD = {
      SHA512,
      SHA256,
      SHA1,
      MD5,
  };
  
}
 
