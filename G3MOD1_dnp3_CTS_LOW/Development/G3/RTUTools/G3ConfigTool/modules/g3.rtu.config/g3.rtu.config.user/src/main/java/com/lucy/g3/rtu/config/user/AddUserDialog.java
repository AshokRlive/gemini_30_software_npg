/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.awt.BorderLayout;
import java.awt.Window;
import java.text.ParseException;
import java.util.Objects;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.prompt.PromptSupport;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;


public class AddUserDialog
    extends AbstractDialog
{
  static final int PASSWORD_LENGHT = 15;
  
  /** Password entry password field */
	private JPasswordField jpfPass0;
	private JPasswordField jpfPass1;
	private JTextField tfPin;

	
	 private JComboBox<USER_LEVEL> roleField;
	 private JComboBox<String> mdAlgorithmField;
	  private JTextField nameField;

	private JPanel contentPane;
	private UserConfig newUser;
	private final UserManager manager;
	/**
	 * Creates new DGetPassword dialog.
	 * 
	 * @param parent Parent frame
	 * @param manager 
	 * @param sTitle The dialog's title
	 */
	public AddUserDialog(Window parent, UserManager manager)
	{
		super(parent);
		this.manager = manager;
		setTitle("Add A New User");
		setModal(true);
		initComponents();
		pack();
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
	  contentPane = new JPanel(new BorderLayout());

		jpfPass0 = new JPasswordField(PASSWORD_LENGHT);
    jpfPass1 = new JPasswordField(PASSWORD_LENGHT);
    mdAlgorithmField = new JComboBox<String>(UserConfig.getSupportedMD());
    mdAlgorithmField.setSelectedItem(UserConfig.SHA512);
    try {
      tfPin = new JFormattedTextField(new MaskFormatter(UserConfig.PIN_MASK));
    } catch (ParseException e) {
      e.printStackTrace(); 
    }
    tfPin.setToolTipText("Set the PIN code for HMI");
    PromptSupport.setPrompt("Optional", tfPin);
   
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout("right:pref, 4dlu, 100dlu",""));
    builder.append("User Name:", nameField = new JTextField());
    builder.append("User Role:", roleField = new JComboBox<USER_LEVEL>(USER_LEVEL.values()));
    roleField.setSelectedItem(null);
    builder.appendSeparator();
		builder.append("Enter Password:", jpfPass0);
		builder.append("Confirm Password:", jpfPass1);
		builder.append(String.format("PIN(%d Digits):",UserConfig.PIN_LENGTH), tfPin);
		//builder.append("Message Digest:", mdAlgorithmField); Hide from user
		builder.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.add(builder.getPanel(), BorderLayout.CENTER);
	}

	@Override
  protected BannerPanel createBannerPanel() {
	  BannerPanel banner = new BannerPanel();
	  banner.setSubtitle(PasswordPolicy.getAllPolicyDescriptionsAsHtml());
	  banner.setTitle("Password Policy");
    return banner;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
  
  @Override
  public void cancel(){
    newUser = null;
    super.cancel();
  }

  @Override
  public void ok() {
    if(validatePass()) {
      String mdalgorithm = (String)mdAlgorithmField.getSelectedItem();
      newUser = UserConfig.createWithPlainPassword(
          nameField.getText(),
          String.valueOf(jpfPass0.getPassword()),
          (USER_LEVEL) roleField.getSelectedItem(), 
          mdalgorithm
          );
      
      newUser.setPINAsPlain(tfPin.getText());
	 
      try {
        manager.add(newUser);
        super.ok();
      }catch(Exception e){
        String err = e.getMessage();
        if(err == null)
          MessageDialogs.error("Failed to create a new user!", e);
        else
          MessageDialogs.error(e.getMessage(), null);
      }
    }
  }

  private boolean validatePass() {
    char[] p0 = jpfPass0.getPassword();
    char[] p1 = jpfPass0.getPassword();
    String error = null;
    String errorDetails = null;
    
    String userName = nameField.getText();
    if(Strings.isBlank(userName)) {
      error = "The user name must not be blank!";
      
    } else if(manager.contains(userName)){
      error = "The user already exists: "+userName;
      
    } else if(roleField.getSelectedItem() == null){
      error = "The role must be selected!"; 
      
    } else if(mdAlgorithmField.getSelectedItem() == null){
      error = "The Message Digest must be selected!"; 
      
    } else if(Objects.deepEquals(p0, p1) == false) {
      error = "The input passwords don't match!";
      
    } else  if((errorDetails = PasswordPolicy.validatePassword2(p0)) != null) {
      error = "The password does't comply with the password policy!";
    }
    
    if(error != null) {
      MessageDialogs.error(this, error, errorDetails);
    }
    
    return error == null;
  }

}
