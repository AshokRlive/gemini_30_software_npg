/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.awt.*;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.JComponent;
import javax.swing.JPanel;
import com.jgoodies.forms.factories.*;

import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;

public class ChangeUserAccountAdvancedDialog extends AbstractDialog {
  private final  UserManager manager;
  public ChangeUserAccountAdvancedDialog(Frame owner, UserManager manager) {
    super(owner);
    this.manager = manager;
    init();
  }

  public ChangeUserAccountAdvancedDialog(Dialog owner, UserManager manager) {
    super(owner);
    this.manager = manager;
    init();
  }
  
  protected void init() {
    setTitle("User Accounts Advanced Settings");
    initComponents();
    load(manager);
    pack();
  }

  
  private void load(UserManager manager) {
    spinnerDuration.setValue(manager.getAccountLockoutDurationSecs());
    spinnerThreshold.setValue(manager.getInvalidLogonAttemptsThreshold());
    
    boolean lockPolicyEnabled = manager.getInvalidLogonAttemptsThreshold() > 0;
    setLockoutPolicyEnabled(lockPolicyEnabled);
    checkBoxEnableLogckout.setSelected(lockPolicyEnabled);
  }
  
  @Override
  public void ok() {
    manager.setAccountLockoutDurationSecs((int) spinnerDuration.getValue());
    manager.setInvalidLogonAttemptsThreshold((int) spinnerThreshold.getValue());
    
    super.ok();
  }

  private void checkBoxEnableLogckoutActionPerformed() {
    boolean enabled = checkBoxEnableLogckout.isSelected();
    setLockoutPolicyEnabled(enabled);
    spinnerThreshold.setValue(enabled ? 3 : 0);
  }

  private void setLockoutPolicyEnabled(boolean enabled) {
    label1.setEnabled(enabled);
    label2.setEnabled(enabled);
    label3.setEnabled(enabled);
    label4.setEnabled(enabled);
    spinnerDuration.setEnabled(enabled);
    spinnerThreshold.setEnabled(enabled);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    contentPanel = new JPanel();
    separator1 = compFactory.createSeparator("Security Policies");
    checkBoxEnableLogckout = new JCheckBox();
    label1 = new JLabel();
    spinnerThreshold = new JSpinner();
    label3 = new JLabel();
    label2 = new JLabel();
    spinnerDuration = new JSpinner();
    label4 = new JLabel();

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
        "right:default, $lcgap, [50dlu,default], $rgap, default, $lcgap, default:grow",
        "4*(default, $lgap), 100dlu"));
      contentPanel.add(separator1, CC.xywh(1, 1, 7, 1));

      //---- checkBoxEnableLogckout ----
      checkBoxEnableLogckout.setText("Account Lockout Policy");
      checkBoxEnableLogckout.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          checkBoxEnableLogckoutActionPerformed();
        }
      });
      contentPanel.add(checkBoxEnableLogckout, CC.xywh(1, 3, 3, 1));

      //---- label1 ----
      label1.setText("Lockout account after invalid attempts:");
      contentPanel.add(label1, CC.xy(1, 5));

      //---- spinnerThreshold ----
      spinnerThreshold.setToolTipText("The account will lock out after reaching this amount of invalid , range [3,10]");
      spinnerThreshold.setModel(new SpinnerNumberModel(3, 3, 10, 1));
      contentPanel.add(spinnerThreshold, CC.xy(3, 5));

      //---- label3 ----
      label3.setText("times");
      contentPanel.add(label3, CC.xy(5, 5));

      //---- label2 ----
      label2.setText("Account lockout duration:");
      contentPanel.add(label2, CC.xy(1, 7));

      //---- spinnerDuration ----
      spinnerDuration.setToolTipText("The duration that the account will be locked out after reaching attempts threshold, range [0,43200]");
      spinnerDuration.setModel(new SpinnerNumberModel(0, 0, 43200, 1));
      contentPanel.add(spinnerDuration, CC.xy(3, 7));

      //---- label4 ----
      label4.setText("seconds");
      contentPanel.add(label4, CC.xy(5, 7));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JComponent separator1;
  private JCheckBox checkBoxEnableLogckout;
  private JLabel label1;
  private JSpinner spinnerThreshold;
  private JLabel label3;
  private JLabel label2;
  private JSpinner spinnerDuration;
  private JLabel label4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
