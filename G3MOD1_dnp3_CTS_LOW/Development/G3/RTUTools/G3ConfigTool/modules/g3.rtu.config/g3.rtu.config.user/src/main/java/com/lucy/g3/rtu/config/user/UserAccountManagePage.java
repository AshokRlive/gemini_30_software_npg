/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.awt.BorderLayout;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.forms.builder.ButtonStackBuilder;
import com.jgoodies.forms.factories.Borders;
import com.lucy.g3.rtu.config.shared.base.page.AbstractConfigPage;

import org.jdesktop.swingx.JXTable;

/**
 * The page of user accounter manager.
 */
public class UserAccountManagePage extends AbstractConfigPage {

  private final UserManagerModel model;


  public UserAccountManagePage(UserManager manager) {
    super(manager);
    setNodeName("User Accounts");

    this.model = new UserManagerModel(manager);
  }

  private void createUIComponents() {
    userList = new JXTable(new UserTableModel(model.getSelectionList()));
    Bindings.bind(userList, model.getSelectionList());

    ButtonStackBuilder panelBuilder = new ButtonStackBuilder();
    panelBuilder.setDefaultDialogBorder();
    panelBuilder.addButton(
        model.getAction(UserManagerModel.ACTION_KEY_ADD),
        model.getAction(UserManagerModel.ACTION_KEY_REMOVE),
        model.getAction(UserManagerModel.ACTION_KEY_CHANGE_PASSWORD),
        model.getAction(UserManagerModel.ACTION_KEY_CHANGE_ROLE),
        model.getAction(UserManagerModel.ACTION_KEY_CHANGE_ADVANCED)
        );
    controlPanel = panelBuilder.getPanel();
   

    popupMenu = new JPopupMenu();
    popupMenu.add(model.getAction(UserManagerModel.ACTION_KEY_ADD));
    popupMenu.add(model.getAction(UserManagerModel.ACTION_KEY_REMOVE));
    popupMenu.add(model.getAction(UserManagerModel.ACTION_KEY_CHANGE_PASSWORD));
    popupMenu.add(model.getAction(UserManagerModel.ACTION_KEY_CHANGE_ROLE));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    JScrollPane scrollPane = new JScrollPane();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new BorderLayout(5, 5));

    //======== scrollPane ========
    {

      //---- userList ----
      userList.setComponentPopupMenu(popupMenu);
      scrollPane.setViewportView(userList);
    }
    add(scrollPane, BorderLayout.CENTER);
    add(controlPanel, BorderLayout.EAST);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  @Override
  protected void init() {
    initComponents();
    initEventHandling();
  }


  private void initEventHandling() {
    userList.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        if(e.getClickCount() > 1) {
          model.changePassword();
        }
      }
    });
    controlPanel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        model.getSelectionList().clearSelection();
      }
    });
    
    userList.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Edit");
    userList.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "Delete");
    userList.getActionMap().put("Delete", model.getAction(UserManagerModel.ACTION_KEY_REMOVE));
    userList.getActionMap().put("Edit", model.getAction(UserManagerModel.ACTION_KEY_CHANGE_PASSWORD));
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXTable userList;
  private JPanel controlPanel;
  private JPopupMenu popupMenu;
  // JFormDesigner - End of variables declaration //GEN-END:variables


private static class UserTableModel extends AbstractTableAdapter<UserConfig> {
  private final static String[] COL_NAMES = {
      "User Name", 
      "Role", 
      "PIN"
    };
  
    private final static int COL_NAME = 0;
    private final static int COL_ROLE = 1;
    private final static int COL_PIN = 2;
    
    public UserTableModel(ListModel<UserConfig> listModel) {
      super(listModel,COL_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      UserConfig row = getRow(rowIndex);
      
      switch (columnIndex) {
      case COL_NAME:
        return row.getName();
      case COL_ROLE:
        return row.getRole().getDescription();
      case COL_PIN:
        return row.getPIN();
      default:
        break;
      }
      return null;
    }
  } 
}
