/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.gui.framework.page.PageFactories;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;

/**
 *
 */
public class UserPlugin implements IConfigPlugin, IPageFactory {

  private UserPlugin() {
  }


  private final static UserPlugin INSTANCE = new UserPlugin();


  public static UserPlugin getInstance() {
    return INSTANCE;
  }

  public static void init() {
    ConfigFactory factory = ConfigFactory.getInstance();
    
    factory.registerFactory(UserManager.CONFIG_MODULE_ID,
        new IConfigModuleFactory() {

          @Override
          public IConfigModule create(IConfig owner) {
            return new UserManager(owner);

          }
        });
    
    PageFactories.registerFactory(PageFactories.KEY_USERS, INSTANCE);
    
  }

  @Override
  public Page createPage(Object data) {
    if (data instanceof UserManager) {
      return new UserAccountManagePage((UserManager) data);
    }
    
    return null;
  }

}
