/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.Objects;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.prompt.PromptSupport;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;

//import org.bouncycastle.openssl.PasswordFinder;


/**
 * Modal dialog used for entering a masked password.
 */
public class ChangePassDialog
    extends AbstractDialog
{
  private static final int PASSWORD_LENGHT = AddUserDialog.PASSWORD_LENGHT;
  
  /** Password entry password field */
	private JPasswordField jpfPass0;
	private JPasswordField jpfPass1;
	private JCheckBox      cboxNotChgPass;
	private JTextField     tfPin;

	private JPanel contentPane;
	private final UserConfig user;
	private final UserManager manager; 
	/**
	 * Creates new DGetPassword dialog.
	 * 
	 * @param parent Parent frame
	 * @param manager 
	 * @param sTitle The dialog's title
	 */
	public ChangePassDialog(Window parent, UserManager manager, UserConfig user)
	{
		super(parent);
		this.manager = manager;
		this.user = user;
		setTitle("Change Password");
		setModal(true);
		initComponents();
		initEventHandling();
		
		pack();
	}

	private void initEventHandling() {
	  cboxNotChgPass.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        setPasswordFieldsEnable(!cboxNotChgPass.isSelected());
      }
    });
	  setPasswordFieldsEnable(!cboxNotChgPass.isSelected());
  }
	
	private void setPasswordFieldsEnable(boolean enabled) {
	  jpfPass0.setEnabled(enabled);
	  jpfPass1.setEnabled(enabled);
	}

  /**
	 * Get the password set in the dialog.
	 * 
	 * @return The password or null if none was set
	 */
//	private char[] getPassword()
//	{
//		if (password == null)
//		{
//			return null;
//		}
//		char[] copy = new char[password.length];
//		System.arraycopy(password, 0, copy, 0, copy.length);
//		return copy;
//	}
	

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
	  contentPane = new JPanel(new BorderLayout());

		jpfPass0 = new JPasswordField(PASSWORD_LENGHT);
    jpfPass1 = new JPasswordField(PASSWORD_LENGHT);
    cboxNotChgPass = new JCheckBox("Do not change password");
    try {
      tfPin = new JFormattedTextField(new MaskFormatter(UserConfig.PIN_MASK));
    } catch (ParseException e) {
      e.printStackTrace(); 
    }    
    tfPin.setToolTipText("Set the PIN code for HMI");
    tfPin.setText(user.getPIN());
    PromptSupport.setPrompt("Optional", tfPin);
    
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout("right:pref, 4dlu, 100dlu",""));
    builder.append("User Name:", new JLabel(user.getName()));
    builder.append("User Role:", new JLabel(user.getRole().getDescription()));
    builder.appendSeparator();
		builder.append("", cboxNotChgPass);
		builder.append("New Password:", jpfPass0);
		builder.append("Confirm Password:", jpfPass1);
    //builder.append("Message Digest:", new JLabel(user.getAlgorithm()));
		builder.append(String.format("PIN(%d Digits):",UserConfig.PIN_LENGTH), tfPin);
		builder.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.add(builder.getPanel(), BorderLayout.CENTER);
	}

	@Override
  protected BannerPanel createBannerPanel() {
	  BannerPanel banner = new BannerPanel();
	  banner.setSubtitle(PasswordPolicy.getAllPolicyDescriptionsAsHtml());
	  banner.setTitle("Password Policy");
    return banner;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
  
  @Override
  public void ok() {
    if(validatePass()) {
      if(!cboxNotChgPass.isSelected()) {
        char[] password = jpfPass0.getPassword();
        user.setPassword(String.valueOf(password));
      }
      
      user.setPINAsPlain(tfPin.getText());
      
      super.ok();
    }
  }
  
  private boolean validatePass() {
    String error = null;
    String errorDetails = null;
    
    /*Validate password*/
    if(!cboxNotChgPass.isSelected()) {
      char[] p0 = jpfPass0.getPassword();
      char[] p1 = jpfPass1.getPassword();
      
      if(Objects.deepEquals(p0, p1) == false) {
        error = "The input passwords don't match!";
      }
  
      errorDetails = PasswordPolicy.validatePassword2(p0);
      if(errorDetails != null) {
        error = "The new password does't comply with the password policy!";
      }
    }
    
    /* Validate PIN*/
    String newPIN = tfPin.getText().trim();
    if(!Strings.isBlank(newPIN) && !newPIN.equals(user.getPIN())) {
      if(manager != null && manager.checkPinExists(newPIN)){
        error = "The PIN already exists! Please provide a differnt PIN.";
      };
    }
    
    if(error != null) {
      MessageDialogs.error(this, error, errorDetails);
    }
    
    return error == null;
  }

}
