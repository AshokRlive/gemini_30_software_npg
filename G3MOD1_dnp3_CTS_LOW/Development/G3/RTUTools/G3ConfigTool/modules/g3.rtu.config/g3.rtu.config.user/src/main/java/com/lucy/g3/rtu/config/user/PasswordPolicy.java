/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;

public class PasswordPolicy {
  
  private String policyRegex;
  private String policyDescription;
  
  
  public PasswordPolicy(String policyRegex, String policyDescription) {
    super();
    this.policyRegex = Preconditions.checkNotBlank(policyRegex, "the policy must not be blank");
    this.policyDescription = Preconditions.checkNotBlank(policyDescription, "the policy description must not be "
        + "blank for policy:" + policyRegex);
  }
  
  /**
   * Validates the given password.
   * @param password password characters.
   * @return true if the password complies with this policy.
   */
  public boolean validate(char[] password){
    if(Strings.isBlank(policyRegex))
      return true; // Do not validate if the regex is blank.
    String passStr = String.valueOf(password);
    return passStr.matches(policyRegex);
  }
  
  private String getDescription() {
    return policyDescription;
  }
  
  /**
   * Validates the password against all policies.
   * @param password
   * @return error strings. null if the password complies with all policies. 
   */
  public static String [] validatePassword(char[] password){
    ArrayList<String> errors = new ArrayList<>();
    
    Collection<PasswordPolicy> policies = getAllPolicies();

    
    if(policies != null) {
      for (PasswordPolicy policy : policies) {
        if(policy.validate(password) == false) {
          errors.add(policy.getDescription());
        }
      }
    }

    return errors.isEmpty()? null: errors.toArray(new String[errors.size()]);
  }
  
  /**
   * Validate password and returns error in html format.
   * @param password
   * @return
   */
  public static String validatePassword2(char[] password){
    String[] errs = validatePassword(password);
    if(errs == null || errs.length == 0)
      return null;
    
    StringBuilder sb = new StringBuilder();
    

    sb.append("<ul>");
    for (int i = 0; i < errs.length; i++) {
      sb.append("<li>");
      sb.append("<font size=\"4\" color=\"red\">");
      sb.append(errs[i]);
      sb.append("</font>");
      sb.append("</li>");
    }
    sb.append("</ul>");
    
    return sb.toString();
  }
  
  public static String getAllPolicyDescriptionsAsHtml(){
    if(htmlDesc != null)
      return htmlDesc;
    
    Collection<PasswordPolicy> policies = getAllPolicies();
    if(policies != null) {
      StringBuilder sb = new StringBuilder();
      sb.append("<ul>");
      for (PasswordPolicy rule: policies) {
        sb.append("<li>");
        sb.append(rule.getDescription());
        sb.append("</li>");
      }
      htmlDesc = sb.toString();
    }
    
    return htmlDesc;
  }
  
  public static Collection<PasswordPolicy> getAllPolicies(){
    if(allPolicies == null) {
      allPolicies = loadPolicies();
    }
    
    return new ArrayList<>(allPolicies);
  }
  
  private static Collection<PasswordPolicy> allPolicies = null;
  private static String htmlDesc = null;
  /**
   * Read policy from file: UserPasswordPolicy.properties.
   */
  private static Collection<PasswordPolicy> loadPolicies(){
    Logger log = Logger.getLogger(UserConfig.class);
    InputStream is = null;

    ArrayList<PasswordPolicy> policies = new ArrayList<>();
    
    try {
      is = UserConfig.class.getClassLoader().getResourceAsStream("policy/UserPasswordPolicy.properties");
      if(is == null) {
        log.warn("password policy file not found");
        return policies;
      }
        
      Properties prop = new Properties();
      prop.load(is);
      
      int maxNum = Integer.valueOf(prop.getProperty("user.password.policy.maximum"));
      
      String policyRegex = null;
      String policyDesc = null;
      for (int i = 0; i < maxNum; i++) {
        policyRegex = prop.getProperty("user.password.policy"+i);
        policyDesc = prop.getProperty("user.password.policy"+i+".desc");
        if(!Strings.isBlank(policyRegex)) {
          //policyRegex = String.format("^%s$",policyRegex);
          policies.add(new PasswordPolicy(policyRegex, policyDesc));
          log.info("Loaded password policy: " + policyRegex);
        }
      }
    } catch (Exception e) {
      log.error("Failed to load password policy", e);

    } finally {
      if (is != null)
        try {
          is.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
    }
    
    return policies;
  }

}

