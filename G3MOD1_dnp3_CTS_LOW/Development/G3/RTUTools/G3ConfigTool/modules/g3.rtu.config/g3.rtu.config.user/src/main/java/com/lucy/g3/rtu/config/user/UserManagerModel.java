/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;

/**
 * Presentation Model for user manager.
 */
public class UserManagerModel extends Model {

  public static final String ACTION_KEY_ADD = "add";
  public static final String ACTION_KEY_REMOVE = "remove";
  public static final String ACTION_KEY_CHANGE_PASSWORD = "changePassword";
  public static final String ACTION_KEY_CHANGE_ROLE = "changeRole";
  public static final String ACTION_KEY_CHANGE_ADVANCED = "changeAdvanced";

  private Logger log = Logger.getLogger(UserManagerModel.class);

  private final SelectionInList<UserConfig> selectionList;

  private JFrame parent = WindowUtils.getMainFrame();

  private boolean selected;

  private UserManager manager;


  /**
   * Constructs a modules presentation model.
   */
  public UserManagerModel(UserManager manager) {
    this.manager = Preconditions.checkNotNull(manager, "User manager must not be null.");

    selectionList = new SelectionInList<UserConfig>(manager.getUserListModel());
    selectionList.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        new SelectionHandler());
    setSelected(selectionList.hasSelection());
  }

  @org.jdesktop.application.Action
  public void add() {
    new AddUserDialog(parent, manager).setVisible(true);
  }

  @org.jdesktop.application.Action(enabledProperty = "selected")
  public void remove() {
    UserConfig toRemove = selectionList.getSelection();
    
    if(manager.isAdmin(toRemove)) {
      JOptionPane.showMessageDialog(parent, String.format("It is not allowed to remove user %s!",toRemove.getName()), 
          "Forbidden Operation", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    try {
      if(MessageDialogs.confirmRemove(parent, toRemove.getName()))
        manager.remove(toRemove);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(parent,
          e.getMessage(),
          "Error",
          JOptionPane.ERROR_MESSAGE);
    }
  }


  @org.jdesktop.application.Action 
  public void changeAdvanced(){
    new ChangeUserAccountAdvancedDialog(parent, manager).setVisible(true);
  }
  
  @org.jdesktop.application.Action(enabledProperty = "selected")
  public void changePassword() {
    UserConfig user = selectionList.getSelection();
    if(user == null)
      return;
    
    new ChangePassDialog(parent, manager, user).setVisible(true);
  }
  
  @org.jdesktop.application.Action(enabledProperty = "selected")
  public void changeRole() {
    UserConfig user = selectionList.getSelection();
    if(user == null)
      return;
    
    if(manager.isAdmin(user)) {
      JOptionPane.showMessageDialog(parent, String.format("It is not allowed to change the role of %s!",user.getName()), 
          "Forbidden Operation", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    ChangeRoleDialog dlg = new ChangeRoleDialog(parent, user);
    dlg.setVisible(true);
    if (dlg.hasBeenAffirmed()) {
      user.setRole(dlg.getRole());
    }
  }

  public Action getAction(String key) {
    return Application.getInstance().getContext().getActionMap(this).get(key);
  }

  public SelectionInList<UserConfig> getSelectionList() {
    return selectionList;
  }

  public boolean isSelected() {
    return selected;
  }

  protected void setSelected(boolean selected) {
    Object oldValue = isSelected();
    this.selected = selected;
    firePropertyChange("selected", oldValue, selected);
  }


  private class SelectionHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setSelected(selectionList.hasSelection());
    }
  }

}
