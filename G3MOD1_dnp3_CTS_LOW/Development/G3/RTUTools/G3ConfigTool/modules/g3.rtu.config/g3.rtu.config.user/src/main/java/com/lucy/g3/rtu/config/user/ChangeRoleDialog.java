/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.user;

import java.awt.BorderLayout;
import java.awt.Window;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;


public class ChangeRoleDialog
    extends AbstractDialog
{
	private JPanel contentPane;
	private final UserConfig user;
	
	private JComboBox<USER_LEVEL> groupSelection;
	
	private USER_LEVEL newRole;
	
	/**
	 * Creates new DGetPassword dialog.
	 * 
	 * @param parent Parent frame
	 * @param sTitle The dialog's title
	 */
	public ChangeRoleDialog(Window parent, UserConfig user)
	{
		super(parent);
		this.user = user;
		setTitle("Change Role");
		setModal(true);
		initComponents();
		pack();
	}

	/**
	 * Get the password set in the dialog.
	 * 
	 * @return The password or null if none was set
	 */
	public USER_LEVEL getRole()
	{
		return newRole;
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
	  contentPane = new JPanel(new BorderLayout());

    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout("right:pref, 4dlu, 100dlu",""));
    builder.append("User Name:", new JLabel(user.getName()));
    builder.append("User Role:", groupSelection = new JComboBox<USER_LEVEL>(USER_LEVEL.values()));
    groupSelection.setSelectedItem(user.getRole());
		builder.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.add(builder.getPanel(), BorderLayout.CENTER);
	}

	@Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
  
  @Override
  public void cancel(){
    newRole = null;
    super.cancel();
  }

  @Override
  public void ok() {
    if(validatePass()) {
      newRole = (USER_LEVEL) groupSelection.getSelectedItem();
      super.ok();
    }
  }
  
  private boolean validatePass() {
    String error = null;
    
    if(groupSelection.getSelectedItem() == null) {
      error = "The group must be selected!";
    }

    if(error != null) {
      JOptionPane.showMessageDialog(this, error, "Invalid Input", JOptionPane.ERROR_MESSAGE);
    }
    
    return error == null;
  }

}
