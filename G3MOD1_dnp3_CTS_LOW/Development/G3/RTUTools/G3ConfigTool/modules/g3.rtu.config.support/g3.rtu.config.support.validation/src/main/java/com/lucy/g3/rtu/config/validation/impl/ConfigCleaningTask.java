/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.gui.common.widgets.ext.jdesktop.BusyIndicatorInputBlocker;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.validation.impl.ui.CleanerDialog;

/**
 * A task for cleaning invalid configuration items in {@code ConfigData}.
 */
class ConfigCleaningTask extends Task<Void, Void> {

  private Logger log = Logger.getLogger(ConfigCleaningTask.class);

  private final JFrame parent;

  private final IConfigValidator validator;


  public ConfigCleaningTask(Application app, IConfigValidator validator) {
    super(app);

    this.validator = validator;

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    setUserCanCancel(true);
    if (parent != null) {
      setInputBlocker(new BusyIndicatorInputBlocker(this, parent));
    }
  }

  @Override
  protected Void doInBackground() throws Exception {
    validator.validate();
    
    if(validator.isValid()){
      cancel(true);
      showNothingToCleanMessage();
      return null;
    }
      

    while (validator.isValid() == false && !isCancelled()) {
      Collection<ValidationResultExt> items = validator.getResults();
      items = findErrorResults(items);
      
      if (!items.isEmpty()) {
        Collection<ValidationResultExt> cleanItems = CleanerDialog.showDialog(items, parent);
        if (cleanItems != null) {
          new ConfigCleaner(validator.getOwner()).clean(cleanItems);
        } else {
          cancel(true);// user cancelled
        }
      }
      validator.validate();
    }

    return null;
  }


  private void showNothingToCleanMessage() throws InvocationTargetException, InterruptedException {
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        JOptionPane.showMessageDialog(parent, 
            "There is nothing to be cleaned up!", "Done", JOptionPane.INFORMATION_MESSAGE);
      }
    });
  }

  private Collection<ValidationResultExt> findErrorResults(Collection<ValidationResultExt> items) {
    ArrayList<ValidationResultExt> invalidItems = new ArrayList<>();
    for (ValidationResultExt item : items) {
      if(item != null && !item.isValid())
        invalidItems.add(item);
    }
    
    return invalidItems;
  }

  @Override
  protected void succeeded(Void result) {
    String msg = "The configuration has been cleaned up.";
    log.info(msg);
    JOptionPane.showMessageDialog(parent, msg, "Success", JOptionPane.INFORMATION_MESSAGE);
  }

  @Override
  protected void failed(Throwable cause) {
    String error = "Failed to clean up configuration";
    setMessage(error);
    log.error(error, cause);
    // Show error dialog
    ErrorInfo info = new ErrorInfo("Fail", error, cause.getMessage(), null,
        cause, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
  }

}
