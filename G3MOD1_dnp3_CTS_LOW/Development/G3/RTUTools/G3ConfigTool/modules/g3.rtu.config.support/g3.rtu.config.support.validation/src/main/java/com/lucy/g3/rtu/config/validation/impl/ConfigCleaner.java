/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.util.ScadaProtocolUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.ICleaner;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointUtility;


/**
 * Implementation of {@link ICleaner} for cleaning {@link IConfig}.
 */
final class ConfigCleaner implements ICleaner {
  private IConfig data;
  ConfigCleaner(IConfig data) {
    super();
    this.data = data;
  }

  @Override
  public void clean(Collection<ValidationResultExt> invalidItems) {
    for (ValidationResultExt invalidItem : invalidItems) {

      IValidation item = invalidItem.getValidatedItem();
      if (item instanceof IStandardPoint) {
        VirtualPointUtility.removePoint(data,(IStandardPoint) item);
        
      } else if (item instanceof ICLogic) {
        CLogicUtility.removeLogic(data, (ICLogic) item);
        
      } else if (item instanceof ScadaPoint) {
        ScadaProtocolUtility.removeScadaPoint((ScadaPoint)item);
        
      } else if (item instanceof Screen) {
        
        /*
         * Menu Screen shouldn't be cleaned because it may contain valid and
         * invalid children
         */
        if (!(item instanceof MenuScreen)) {
          HMIScreenManager manager = data.getConfigModule(HMIScreenManager.CONFIG_MODULE_ID);
          manager.removeScreen((Screen) item);
        }
      } 
      
    }
  }
}
