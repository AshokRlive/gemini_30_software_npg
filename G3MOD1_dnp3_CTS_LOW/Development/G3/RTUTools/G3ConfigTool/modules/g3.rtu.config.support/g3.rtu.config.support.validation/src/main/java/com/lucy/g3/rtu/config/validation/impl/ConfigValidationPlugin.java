/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModuleFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfigPlugin;


/**
 *
 */
public class ConfigValidationPlugin implements IConfigPlugin, IConfigModuleFactory{
  private ConfigValidationPlugin () {}
  private final static ConfigValidationPlugin  INSTANCE = new ConfigValidationPlugin();
  
  public static void init() {
    ConfigFactory.getInstance().registerFactory(ConfigValidator.CONFIG_MODULE_ID, INSTANCE);
  }
  
  @Override
  public IConfigValidator create(IConfig owner) {
    return new ConfigValidator(owner);
  }
}

