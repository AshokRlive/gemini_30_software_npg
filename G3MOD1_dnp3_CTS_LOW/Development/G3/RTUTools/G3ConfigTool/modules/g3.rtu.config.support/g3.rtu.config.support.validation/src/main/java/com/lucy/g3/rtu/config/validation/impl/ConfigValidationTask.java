/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import java.util.logging.Level;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.gui.common.widgets.ext.jdesktop.BusyIndicatorInputBlocker;
import com.lucy.g3.rtu.config.validation.impl.ui.ValidationResultDialog;

/**
 * A task for validating the content of {@code ConfigData} object, and showing
 * validation result in a dialog.
 */
class ConfigValidationTask extends Task<Void, Void> {

  private Logger log = Logger.getLogger(ConfigValidationTask.class);

  private final JFrame mainFrame;

  private final IConfigValidator  validator;

  public ConfigValidationTask(Application app, IConfigValidator  validator) {
    super(app);
    
    this.validator = validator; 

    if (app instanceof SingleFrameApplication) {
      mainFrame = ((SingleFrameApplication) app).getMainFrame();
    } else {
      mainFrame = null;
    }

    setUserCanCancel(true);
    if (mainFrame != null) {
      setInputBlocker(new BusyIndicatorInputBlocker(this, mainFrame));
    }
  }

  @Override
  protected Void doInBackground() throws Exception {
    validator.validate();
    ValidationResultDialog.showDialog(mainFrame, null, validator.getResults());
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    String error = "Fail to validate configuration";
    setMessage(error);
    log.error(error, cause);
    // Show error dialog
    ErrorInfo info = new ErrorInfo("Validation Failed", error, cause.getMessage(),
        null, cause, Level.SEVERE, null);
    JXErrorPane.showDialog(mainFrame, info);
  }

}
