/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

import com.lucy.g3.gui.common.widgets.ext.swing.table.SelectionTableModel;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The dialog for selecting invalid items to be cleaned.
 */
public final class CleanerDialog {

  private ArrayList<ValidationResultExt> selectedItems;
  private final ArrayList<ValidationResultExt> selections = new ArrayList<>();
  private final Window parent;
  
  private CleanerDialog (Window parent, Collection<ValidationResultExt> items){
    if(items != null)
      this.selections.addAll(items);
    this.parent = parent;
    
    showSelectionDialog();
  }
  
  private void showSelectionDialog() {
    try {
      SwingUtilities.invokeAndWait(new Runnable() {

        @Override
        public void run() {
            final SelectionTableModel<ValidationResultExt> selModel = new SelectionTableModel<ValidationResultExt>(
                selections);
            selModel.selectAll();
            int option = JOptionPane.showConfirmDialog(parent,
                buildCleaningItemsPanel(selModel), "Cleanup",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.PLAIN_MESSAGE);
            if (option == JOptionPane.YES_OPTION) {
              selectedItems = selModel.getSelections();
            }
          }
      });
    } catch (InvocationTargetException | InterruptedException e) {
      e.printStackTrace();
    }
  }

  private JPanel buildCleaningItemsPanel(
      SelectionTableModel<ValidationResultExt> selModel) {
    JScrollPane sc = new JScrollPane(buildSelectionTable(selModel));
    sc.setPreferredSize(new Dimension(450, 300));
    sc.setBorder(BorderFactory
        .createTitledBorder("Invalid Configuration Items"));
    JPanel pane = new JPanel(new BorderLayout(0, 10));
    pane.add(BorderLayout.CENTER, sc);
    pane.add(
        BorderLayout.NORTH,
        new JLabel(
            "<html><font color=\"red\"><b>WARNING:</b></font> The following invalid items will be removed! Do you want to continue?</html>"));
    return pane;
  }

  private JTable buildSelectionTable(SelectionTableModel<ValidationResultExt> selModel) {
    // Build selection table
    JTable selTable = new JTable(selModel);
    selTable.setAutoCreateRowSorter(false);
    selTable.setFillsViewportHeight(true);

    // Set table renderer
    selTable.getColumnModel().getColumn(SelectionTableModel.COLUMN_ITEM)
        .setCellRenderer(new InvalidItemCellRenderer());

    // Adjust column width
    selTable.getColumnModel()
        .getColumn(SelectionTableModel.COLUMN_SELECTED).setMaxWidth(50);

    // Build table pop menu
    JPopupMenu menu = new JPopupMenu();
    menu.add(selModel.getActionSelectAll());
    menu.add(selModel.getActionDeselectAll());
    selTable.setComponentPopupMenu(menu);
    return selTable;
  }


  private static class InvalidItemCellRenderer extends
      DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
        Object value, boolean isSelected, boolean hasFocus, int row,
        int column) {
      Component comp = super.getTableCellRendererComponent(table, value,
          isSelected, hasFocus, row, column);
      if (value instanceof ValidationResultExt) {
        String targetName = ((ValidationResultExt) value).getTargetName();
        setText(targetName);
      }
      return comp;
    }

  }

  public static Collection<ValidationResultExt> showDialog(Collection<ValidationResultExt> items, Window parent) {
    CleanerDialog dlg = new CleanerDialog(parent, items);
    return dlg.selectedItems;
  }
}
