
package com.lucy.g3.rtu.config.validation.impl;

import java.awt.Window;
import java.util.Collection;

import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

public interface IConfigValidator extends IConfigModule{
  
  String CONFIG_MODULE_ID = "IConfigValidationPlugin";
  
  Collection<ValidationResultExt> getResults();
  
  void validate();
  
  boolean isValid();
  
  Task<?,?>  executeCleaning(Application app);

  Task<?,?>  executeValidation(Application app);

  void validateAndShowErrors(Window parent);
  
}

