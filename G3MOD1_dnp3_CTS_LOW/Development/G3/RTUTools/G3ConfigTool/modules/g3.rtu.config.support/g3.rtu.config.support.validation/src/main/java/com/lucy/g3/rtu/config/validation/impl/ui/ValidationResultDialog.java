/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl.ui;

import java.awt.Window;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JDialog;
import javax.swing.UIManager;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.validation.Severity;
import com.jgoodies.validation.ValidationMessage;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;

/**
 * The dialog for showing validation report.
 */
public final class ValidationResultDialog {

  private final Collection<ValidationResultExt> results;
  private final ValidationResultExt result;

  private int errorSize;
  private int warningSize;
  private Level severity;


  ValidationResultDialog(ValidationResultExt result, Collection<ValidationResultExt> invalidItems) {
    this.results = invalidItems;
    this.result = result;

    analyseResults();
  }

  private void analyseResults() {
    errorSize = 0;
    warningSize = 0;

    // The size of warnings&error messages
    if (results != null && results.size() > 0) {
      for (ValidationResultExt result : results) {
        if(result != null) {
          errorSize += result.getErrorsSize();
          warningSize += result.getWarningsSize();
        }
      }

    } else if (result != null) {
      errorSize = result.getErrorsSize();
      warningSize = result.getWarningsSize();
    }

    // Set severity
    if (errorSize > 0) {
      severity = Level.SEVERE;
    } else if (warningSize > 0) {
      severity = Level.WARNING;
    } else {
      severity = Level.INFO;
    }
  }

  private ErrorInfo createErrorInfo() {
    return new ErrorInfo("Validation Result",
        createBasicReport(),
        createDetailReport(),
        "Validation", null, severity, null);
  }

  private String createBasicReport() {
    StringBuilder sb = new StringBuilder();
    sb.append("<html>");

    if (errorSize > 0) {
      sb.append("<p>Invalid configuration!</p>");
      sb.append(errorSize);
      sb.append(" errors ");
      if (warningSize > 0) {
        sb.append("and ");
        sb.append(warningSize);
        sb.append(" warnings");
      }
      sb.append(" were found(See details).");
      sb.append("</p>");
      sb.append("<p>Please fix the invalid configuration or clean up configuration using File Menu-> Clean up.<p>");

    } else if (warningSize > 0) {
      sb.append(warningSize);
      sb.append(" warnings were found.");

    } else {
      sb.append("<p>The configuration is valid!</p>");
    }

    sb.append("</html>");

    return sb.toString();
  }

  private String createDetailReport() {

    StringBuilder sb = new StringBuilder();

    int index = 1;

    sb.append("<html><table border=\"0\" cellpadding=\"5\">");
    sb.append("<tr><th>Index</th><th>Severity</th><th>Target</th><th>Result</th></tr>");

    if (results != null && results.size() > 0) {
      for (ValidationResultExt result : results) {
        if(result != null) {
          List<ValidationMessage> msgs = result.getMessages();
          for (ValidationMessage msg : msgs) {
            appendEntry(sb, index, result.getTargetName(), msg);
            index++;
          }
        }
      }
    } else if (result != null) {
      List<ValidationMessage> msgs = result.getMessages();
      for (ValidationMessage msg : msgs) {
        appendEntry(sb, index, "", msg);
        index++;
      }
    }

    sb.append("</table></html>");
    return sb.toString();
  }

  private void appendEntry(StringBuilder sb, int index, String targetName, ValidationMessage msg) {
    String target;
    String text;
    String severityStr;
    Severity severity;

    sb.append("<tr>");
    severity = msg.severity();

    text = applyColor(msg.formattedText(), severity);
    target = applyColor(targetName, severity);
    severityStr = applyColor(severity.name(), severity);

    sb.append("<td>");
    sb.append(index++);
    sb.append("</td>");

    sb.append("<td>");
    sb.append(severityStr);
    sb.append("</td>");

    sb.append("<td>");
    sb.append(target);
    sb.append("</td>");

    sb.append("<td>");
    sb.append(text);
    sb.append("</td>");

    sb.append("</tr>");
  }

  private String applyColor(String text, Severity severity) {
    switch (severity) {
    case ERROR:
      text = String.format("<font color=\"red\">%s</font>", text);
      break;

    default:
      break;
    }
    return text;
  }

  private void showResultReport(Window parent, ErrorInfo errorInfo) {
    JXErrorPane pane = new JXErrorPane();
    pane.setErrorInfo(errorInfo);

    // Customise icon
    if (severity == Level.INFO) {
      pane.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
    } else if (severity == Level.SEVERE) {
      pane.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
    } else if (severity == Level.WARNING) {
      pane.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
    }

    // Show dialog
    JDialog dlg = JXErrorPane.createDialog(parent, pane);
    dlg.setModal(false);
    dlg.setVisible(true);
  }

  void showResultReport(Window parent) {
    showResultReport(parent, createErrorInfo());
  }

  public static void showDialog(Window parent, ValidationResultExt result, Collection<ValidationResultExt> invalidItems) {
    new ValidationResultDialog(result, invalidItems).showResultReport(parent);
  }
}
