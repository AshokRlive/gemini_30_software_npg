/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import java.util.ArrayList;

import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.validation.impl.ui.ValidationResultDialog;

/**
 *
 */
public class ValidationDialogTest {

  public static void main(String[] args) {
    test1();
    test2();
  }

  private static void test1() {
    ValidationResultExt result = new ValidationResultExt(null);
    result.addError("hee");
    ValidationResultDialog.showDialog(null,result,null);
  }
  
  private static void test2() {
    ArrayList<ValidationResultExt> items = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      ValidationResultExt result = new ValidationResultExt(null);
      result.addError("hee");
      items.add(result);
    }
    
    ValidationResultDialog.showDialog(null,null,items);
  }
  
}
