/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import java.util.ArrayList;

import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.validation.impl.ui.CleanerDialog;


/**
 *
 */
public class CleanerDialogTest {

  /**
   * @param args
   * @throws Exception 
   */
  public static void main(String[] args) throws Exception {
    ArrayList<ValidationResultExt> items = new ArrayList<>(); 
    for (int i = 0; i < 3; i++) {
      items.add(new ValidationResultExt(null));
    }
    
    CleanerDialog.showDialog(items, null);
  }

}

