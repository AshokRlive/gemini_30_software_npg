/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.validation.impl;

import java.awt.Window;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.shared.manager.AbstractConfigModule;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfigModule;
import com.lucy.g3.rtu.config.shared.validation.domain.ICleaner;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidation;
import com.lucy.g3.rtu.config.shared.validation.domain.IValidator;
import com.lucy.g3.rtu.config.shared.validation.domain.ValidationResultExt;
import com.lucy.g3.rtu.config.validation.impl.ui.ValidationResultDialog;

final class ConfigValidator extends AbstractConfigModule implements IConfigValidator{
  

  private final IConfig data;
  
  private final ConfigCleaner cleaner;
  
  ConfigValidator(IConfig data) {
    super(data);
    this.data = Preconditions.checkNotNull(data, "data must not be null");
    this.cleaner = new ConfigCleaner(data);
  }

//  @Override
//  protected void validate(ValidationResult result) {
//
//    ArrayList<IValidator> validators = getValidators();
//
//    ValidationResult res;
//    for (IValidator validator : validators) {
//      if(validator != null) {
//         res = validator.validate();
//         result.addAllFrom(res);
//        
//        if(validator instanceof IManagerValidator)
//          invalidItems.addAll(((IManagerValidator)validator).getInvalidItems());
//      }
//    }
//  }

//  private ArrayList<IValidator> getValidators() {
//    Collection<IConfigModule> allModules = data.getAllConfigModules();
//    ArrayList<IValidator> validators = new ArrayList<>();
//    for (IConfigModule module : allModules) {
//      if(module != null && module != this && module instanceof IValidation) {
//        validators.add(((IValidation)module).getValidator());
//      }
//    }
//    
//    validators.removeAll(Collections.singleton(null)); // remove null validator
//    
//    return validators;
//  }


  @Override
  public Task<?,?> executeCleaning(Application app) {
    ConfigCleaningTask task = new ConfigCleaningTask(app, this);
    app.getContext().getTaskService().execute(task);
    return task;
  }

  @Override
  public Task<?,?> executeValidation(Application app) {
    ConfigValidationTask task = new ConfigValidationTask(app, this);
    app.getContext().getTaskService().execute(task);
    return task;
  }

  private Collection<IValidation> getValidatableChildrenItems() {
    Collection<IConfigModule> allModules = data.getAllConfigModules();
    
    ArrayList<IValidation> validateItems = new ArrayList<>();
    for (IConfigModule module : allModules) {
      if(module != null && module != this && module instanceof IValidation) {
        validateItems.add(((IValidation)module));
      }
    }
    
    return validateItems;
  }

  public ICleaner getCleaner() {
    return cleaner;
  }

  @Override
  public void validateAndShowErrors(Window parent) {
    validate();
    if(isValid() == false) {
      ValidationResultDialog.showDialog(parent, null, getResults());
    }
    
  }


  @Override
  public Collection<ValidationResultExt> getResults() {
    return new ArrayList<ValidationResultExt>(results);
  }

  private final ArrayList<ValidationResultExt> results = new ArrayList<>();
  
  @Override
  public void validate() {
    results.clear();
    Collection<IValidation> children = getValidatableChildrenItems();
    IValidator validator;
    for (IValidation child : children) {
      validator = child.getValidator();
      validator.validate();
      if(validator instanceof IContainerValidator) {
        results.addAll(((IContainerValidator)validator).getResults());
      } else {
        results.add(validator.getResult());
      }
    }
    
    results.removeAll(Collections.singleton(null)); // Remove all null
    
  }


  @Override
  public boolean isValid() {
    Collection<ValidationResultExt> results = getResults();
    for (ValidationResultExt r : results) {
      if(r.isValid() == false)
        return false;
    }
    return true;
  }

}
