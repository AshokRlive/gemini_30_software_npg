/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.validation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * A panel that show messages to user 
 */
public class ConfirmLoadInvalidConfigPanel extends JPanel {
  
  private Throwable error;
  
  public ConfirmLoadInvalidConfigPanel(Throwable error) {
    this.error = error;
    initComponents();
    radioButtonYes.setSelected(true);
    lblDetail.setVisible(error != null);
  }

  public void setMessage(String message) {
    lblMessage.setText(message);
  }
  
  public String getMessage() {
    return lblMessage.getText();
  }
  
  public void setOptionTextYes(String yesText) {
    radioButtonYes.setText(yesText);
  }
  
  public void setOptionTextNo(String noText) {
    radioButtonNo.setText(noText);
  }
  
  public boolean isConfirmed() {
    return radioButtonYes.isSelected();
  }

  private void labelDetailMouseClicked(MouseEvent e) {
    ErrorInfo info = new ErrorInfo("Details", "The XML is not valid against schema!", error.getMessage(),
        null, error, Level.INFO, null);

    JXErrorPane.showDialog(e.getComponent().getParent(), info);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    lblMessage = new JLabel();
    lblDetail = new JLabel();
    radioButtonYes = new JRadioButton();
    radioButtonNo = new JRadioButton();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default",
        "default, $ugap, 2*(default, $lgap), default"));

    //---- lblMessage ----
    lblMessage.setText("You are trying to load an incompatible configuration file.It may fail if you attempt to load it.");
    add(lblMessage, CC.xy(1, 1));

    //---- lblDetail ----
    lblDetail.setText("Details...");
    lblDetail.setForeground(Color.blue);
    lblDetail.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    lblDetail.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        labelDetailMouseClicked(e);
      }
    });
    add(lblDetail, CC.xy(3, 1));

    //---- radioButtonYes ----
    radioButtonYes.setText("Attempt to load this configuration");
    add(radioButtonYes, CC.xy(1, 3));

    //---- radioButtonNo ----
    radioButtonNo.setText("Do not load this configuration");
    add(radioButtonNo, CC.xy(1, 5));

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioButtonYes);
    buttonGroup1.add(radioButtonNo);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel lblMessage;
  private JLabel lblDetail;
  private JRadioButton radioButtonYes;
  private JRadioButton radioButtonNo;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Show this panel in a dialog.
   * @param parent
   * @param title the title of this dialog.
   * @return true if user confirmed the dialog
   */
  public boolean showDialog(Component parent, String title){
    int ret = JOptionPane.showConfirmDialog(parent, 
      this,
      title,
      JOptionPane.OK_CANCEL_OPTION, 
      JOptionPane.WARNING_MESSAGE, null
      );
    
    return ret == JOptionPane.OK_OPTION && this.isConfirmed();
  }
}
