/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.validation;

import java.awt.Frame;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.lucy.g3.common.utils.ZipUtil;

/**
 * Validate XML content against schema.
 */
public class XmlValidator extends com.lucy.g3.xml.validation.XmlValidate {

  public XmlValidator(String schemaFilePath) {
    super(schemaFilePath);
  }

  /**
   * Override to validate zipped XML.
   */
  @Override
  public void validate(String xmlFilePath) throws Exception {
    if (ZipUtil.isGZipFile(xmlFilePath)) {
      InputStream xmlFileStream;
      xmlFileStream = new ByteArrayInputStream(ZipUtil.ungzip(xmlFilePath));
      
      try {
        validate(xmlFileStream);
      } catch (Throwable e) {
        xmlFileStream.close();
        throw e;
      }
      
    } else {
      super.validate(xmlFilePath);
    }
  }

//  /**
//   * Shows an error dialog for invalid configuration.
//   */
//  public static void showInvalidConfigDialog(Frame parent, Throwable e) {
//    ErrorInfo info = new ErrorInfo("Incompatible Configuration Warning",
//        "You are trying to load an incompatible configuration. ",
//        e.getMessage(), null,
//        e, Level.WARNING, null);
//    JXErrorPane.showDialog(parent, info);
//  }
  

  /**
   * Shows a confirm dialog to give user option to continue configuration
   * reading/writing.
   *
   * @param parent
   *          the parent
   * @param e
   *          the e
   * @return confirmed
   * @throws InterruptedException
   *           the interrupted exception
   * @throws InvocationTargetException
   *           the invocation target exception
   */
  public static boolean showConfirmDialog(Frame parent, Throwable e)
      throws InterruptedException, InvocationTargetException {
    ShowConfirmDlg run = new ShowConfirmDlg(parent,e);
    SwingUtilities.invokeAndWait(run);
    return run.confirmed;

  }


  private static class ShowConfirmDlg implements Runnable {

    private boolean confirmed = false;
    private Frame parent;
    private Throwable error;

    ShowConfirmDlg(Frame parent,Throwable error) {
      this.parent = parent;
      this.error = error;
    }

    @Override
    public void run() {
      ConfirmLoadInvalidConfigPanel panel = new ConfirmLoadInvalidConfigPanel(error);
      confirmed = panel.showDialog(parent, "Incompatible Configuration");
    }
  }

}
