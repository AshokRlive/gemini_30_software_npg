/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import org.apache.log4j.Logger;

import com.g3schema.ns_common.ChannelRefT;
import com.g3schema.ns_common.OutputModuleRefT;
import com.g3schema.ns_common.VirtualPointRefT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicRepository;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.hmi.xml.IHMIXmlReadSupport;
import com.lucy.g3.rtu.config.module.iomodule.manager.ChannelRepository;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointRepository;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointReadSupport;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Abstract Xml Reader.
 */
abstract class AbstractXmlReader implements VirtualPointReadSupport, IHMIXmlReadSupport {

  protected final Logger log = Logger.getLogger(getClass());

  protected final ValidationResult result;

  protected final IConfig data;


  AbstractXmlReader(IConfig data, ValidationResult result) {
    this.data = data;
    this.result = result;
  }

  @Override
  public IOperableLogic getOutputCLogic(int logicGroup) {
    CLogicRepository repo = data.getConfigModule(CLogicRepository.CONFIG_MODULE_ID);
    ICLogic clogic = repo.getLogicByGroup(logicGroup);

    if (clogic == null) {
      log.warn("Cannot cast null clogic to operatable logic!");
      return null;
    }

    if (clogic instanceof IOperableLogic == false) {
      log.error("Clogic \"" + clogic + "\" is not operatable!");
      return null;
    } else {
      return (IOperableLogic) clogic;
    }
  }

  @Override
  public ValueLabelSet getLabelSet(String id) {
    VirtualPointManager repo = data.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);

    return repo.getValueLabelManager().get(id);
  }

  @Override
  public VirtualPoint getVirtualPointByRef(VirtualPointRefT xml) {
    VirtualPoint vp = null;
    int pointGroup = -1;
    int pointID = -1;
    if (xml != null && xml.pointGroup.exists() && xml.pointID.exists()) {
      pointGroup = (int) xml.pointGroup.getValue();
      pointID = (int) xml.pointID.getValue();
      vp = getVirtualPoint(pointGroup, pointID);
    }
    return vp;
  }
  
  
  @Override
  public VirtualPoint getVirtualPoint(int group, int id) {
    VirtualPointRepository repo = data.getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);

    return repo.getPoint(group, id);
  }

  @Override
  public IChannel getChannelByRef(ChannelRefT xml_ref, ChannelType type) {
    IChannel ch = null;
    ChannelRepository repo = data.getConfigModule(ChannelRepository.CONFIG_MODULE_ID);

    if (type != null || xml_ref != null) {
      try {
        MODULE moduletype = MODULE.valueOf(xml_ref.moduleType.getValue());
        MODULE_ID moduleID = MODULE_ID.valueOf(xml_ref.moduleID.getValue());
        int channelID = (int) xml_ref.channelID.getValue();
        ch = repo.getChannel(moduletype, moduleID, type, channelID);
      } catch (Exception e) {
        log.error(e);
      }
    }

    if (ch == null) {
      log.error(String.format("Cannot find channel by reference."
          + " module type:%s module ID:%s channel Type: %s channel ID:%d",
          xml_ref.moduleType.getValue(),
          xml_ref.moduleID.getValue(),
          type,
          xml_ref.channelID.getValue()
          ));
    }

    return ch;
  }

  @Override
  public Module getModuleByRef(OutputModuleRefT xml_ref) {
    MODULE type = MODULE.valueOf(xml_ref.moduleType.getValue());
    MODULE_ID id = MODULE_ID.valueOf(xml_ref.moduleID.getValue());
    
    ModuleRepository repo = data.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);

    Module module = repo.getModule(type, id);
    if (module == null) {
      log.error("Module not found by reference. type:" + type + " id:" + id);
    }
    return module;
  }

  @Override
  public ValidationResult getResult() {
    return result;
  }

  @Override
  public VirtualPoint getVirtualPoint(short pointGroup, short pointId) {
    return getVirtualPoint((int)pointGroup, (int)pointId);
  }
}
