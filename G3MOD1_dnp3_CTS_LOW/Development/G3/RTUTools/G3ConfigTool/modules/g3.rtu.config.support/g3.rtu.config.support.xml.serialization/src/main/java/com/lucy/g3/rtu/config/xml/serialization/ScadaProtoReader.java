/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.beans.PropertyVetoException;

import com.g3schema.ScadaProtocolT;
import com.g3schema.ns_s101.S101ChannelT;
import com.g3schema.ns_s101.S101T;
import com.g3schema.ns_s104.S104ChannelT;
import com.g3schema.ns_s104.S104T;
import com.g3schema.ns_sdnp3.SDNP3T;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSourceRepository;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * XML reader for SCADA Protocol.
 */
class ScadaProtoReader extends AbstractXmlReader {

  private final ScadaPointSourceRepository sourceRepository;

  private ScadaProtocolManager manager;
  
  ScadaProtoReader(IConfig data, ValidationResult result) {
    super(data, result);

    manager =  data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    sourceRepository = manager.getSourceRepository();
  }

  public void read(ScadaProtocolT xml_scada) throws PropertyVetoException, DuplicatedException {
    if (xml_scada.sDNP3.exists()) {
      readSDNP3(xml_scada.sDNP3.first());
    }
    
    if (xml_scada.sIEC104.exists()) {
      readIEC104(xml_scada.sIEC104.first());
    }
    
    if (xml_scada.sIEC101.exists()) {
      PortsManager ports = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
      readIEC101(xml_scada.sIEC101.first(),ports);
    }
  }

  private void readSDNP3(SDNP3T xml_dnp3) throws PropertyVetoException, DuplicatedException {
    SDNP3 sdnp3 = new SDNP3();
    manager.add(sdnp3);

    // Load dnp3 channels
    int chnlNum = xml_dnp3.channel.count();
    PortsManager portMgr = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    CommsDeviceManager  commsMgr = (CommsDeviceManager )data.getConfigModule(CommsDeviceManager .CONFIG_MODULE_ID);
    
    for (int i = 0; i < chnlNum; i++) {
      sdnp3.addChannel("New Channel").readFromXML(xml_dnp3.channel.at(i),
          portMgr, commsMgr, this, sourceRepository);
    }
  }
  
  
  private void readIEC104(S104T xml) {
    S104 protocol = new S104();
    manager.add(protocol);

    /* Read Channel */
    int chnlNum = xml.channel.count();
    for (int i = 0; i < chnlNum; i++) {
      S104ChannelT xmlChannel = xml.channel.at(i);
      S104Channel channel = protocol.addChannel("New Channel");
      channel.readFromXML(xmlChannel,sourceRepository);
      }
  }
  
  private void readIEC101(S101T xml, PortsManager ports) {
    S101 protocol = new S101();
    manager.add(protocol);
    
    /* Read Channel */
    int chnlNum = xml.channel.count();
    for (int i = 0; i < chnlNum; i++) {
      S101ChannelT xmlChannel = xml.channel.at(i);
      S101Channel channel = protocol.addChannel("New Channel");
      channel.readFromXML(xmlChannel,sourceRepository, ports);
    }
  }

}
