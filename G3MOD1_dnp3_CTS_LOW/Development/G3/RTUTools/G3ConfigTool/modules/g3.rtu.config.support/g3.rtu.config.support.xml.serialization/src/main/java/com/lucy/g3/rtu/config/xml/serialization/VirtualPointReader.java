/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.util.ArrayList;

import com.g3schema.ns_vpoint.AnaloguePointT;
import com.g3schema.ns_vpoint.BinaryPointT;
import com.g3schema.ns_vpoint.CounterPointT;
import com.g3schema.ns_vpoint.CustomLabelEntryT;
import com.g3schema.ns_vpoint.CustomLabelSetT;
import com.g3schema.ns_vpoint.CustomLabelsT;
import com.g3schema.ns_vpoint.DoubleBinaryPointT;
import com.g3schema.ns_vpoint.VirtualPointsT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IStandardPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;

/**
 * XML reader for standard virtual point.
 */
class VirtualPointReader extends AbstractXmlReader {

  VirtualPointReader(IConfig data, ValidationResult result) {
    super(data, result);
  }

  public void read(VirtualPointsT vpoints) throws InvalidConfException {
    if (vpoints.customLabels.exists()) {
      readCustomLabelSets(vpoints.customLabels.first());
    }

    int apNum = vpoints.analogues.exists() ? vpoints.analogues.first().analogue.count() : 0;
    int bpNum = vpoints.binaries.exists() ? vpoints.binaries.first().binary.count() : 0;
    int dpNum = vpoints.doubleBinaries.exists() ? vpoints.doubleBinaries.first().doubleBinary.count() : 0;

    // Validate all points' ID
    int id_base = 0;
    for (int i = 0; i < apNum; i++) {
      if (vpoints.analogues.first().analogue.at(i).id.getValue() != id_base + i) {
        throw new InvalidConfException("Analogue points' ID are not sequential.");
      }
    }

    id_base = apNum;
    for (int i = 0; i < bpNum; i++) {
      if (vpoints.binaries.first().binary.at(i).id.getValue() != id_base + i) {
        throw new InvalidConfException("Binary points' ID are not sequential.");
      }
    }

    id_base = apNum + bpNum;
    for (int i = 0; i < dpNum; i++) {
      if (vpoints.doubleBinaries.first().doubleBinary.at(i).id.getValue() != id_base + i) {
        throw new InvalidConfException("Double Binary points' ID are not sequential.");
      }
    }

    // load analogue points
    ArrayList<StdAnaloguePoint> apoints = new ArrayList<StdAnaloguePoint>();
    for (int i = 0; i < apNum; i++) {
      AnaloguePointT xmlpoint = vpoints.analogues.first().analogue.at(i);
      StdAnaloguePoint point = VirtualPointFactory.createAnalogPoint();
      point.readFromXML(xmlpoint, this);
      apoints.add(point);
    }

    // load binary points
    ArrayList<StdBinaryPoint> bpoints = new ArrayList<StdBinaryPoint>();
    for (int i = 0; i < bpNum; i++) {
      BinaryPointT xmlpoint = vpoints.binaries.first().binary.at(i);
      StdBinaryPoint point = VirtualPointFactory.createBinaryPoint();
      point.readFromXML(xmlpoint, this);
      bpoints.add(point);
    }

    // load double binary points
    ArrayList<StdDoubleBinaryPoint> dbpoints = new ArrayList<StdDoubleBinaryPoint>();
    for (int i = 0; i < dpNum; i++) {
      DoubleBinaryPointT xmlpoint = vpoints.doubleBinaries.first().doubleBinary.at(i);
      StdDoubleBinaryPoint point = VirtualPointFactory.createDoubleBinaryPoint();
      point.readFromXML(xmlpoint, this);
      dbpoints.add(point);
    }

    addToData(new ArrayList<IStandardPoint>(apoints));
    addToData(new ArrayList<IStandardPoint>(bpoints));
    addToData(new ArrayList<IStandardPoint>(dbpoints));
  }

  private void addToData(ArrayList<IStandardPoint> newPoints) {
    getVirtualPointManager().addAllPoints(newPoints);
  }

  private VirtualPointManager getVirtualPointManager() {
    return data.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
  }

  private void readCustomLabelSets(CustomLabelsT xml) {
    ValueLabelSetManager labelManager = getVirtualPointManager().getValueLabelManager();
    int count = xml.labelSet.count();
    for (int i = 0; i < count; i++) {
      labelManager.add(createLabelSet(xml.labelSet.at(i)));
    }
  }

  private ValueLabelSet createLabelSet(CustomLabelSetT xml) {
    ValueLabelSet labelset = new ValueLabelSet(xml.name.getValue());
    int entriesCount = xml.labelEntry.count();
    for (int i = 0; i < entriesCount; i++) {
      CustomLabelEntryT xmlEntry = xml.labelEntry.at(i);
      labelset.addLabel(new ValueLabel(xmlEntry.value2.getValue().doubleValue(), xmlEntry.label.getValue()));
    }
    return labelset;
  }

  /**
   * Read all counters points and add them to manager.
   */
  public void readCounterPoints(VirtualPointsT vpoints) {
    ArrayList<StdCounterPoint> cpoints = new ArrayList<StdCounterPoint>();
    int cpNum = vpoints.counters.exists() ? vpoints.counters.first().counter.count() : 0;
    for (int i = 0; i < cpNum; i++) {
      CounterPointT xmlpoint = vpoints.counters.first().counter.at(i);
      StdCounterPoint point = VirtualPointFactory.createCounterPoint();
      point.readFromXML(xmlpoint, this);
      cpoints.add(point);
    }

    addToData(new ArrayList<IStandardPoint>(cpoints));
  }

}
