/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.util.Collection;

import com.g3schema.ns_g3module.AnalogueInputChnlT;
import com.g3schema.ns_g3module.CyclicBatteryTestT;
import com.g3schema.ns_g3module.DSMModuleT;
import com.g3schema.ns_g3module.DigitalInputChnlT;
import com.g3schema.ns_g3module.DigitalOutputChnlT;
import com.g3schema.ns_g3module.FDMChannelsT;
import com.g3schema.ns_g3module.FDMModuleT;
import com.g3schema.ns_g3module.FPMChannelsT;
import com.g3schema.ns_g3module.FPMModuleT;
import com.g3schema.ns_g3module.FanSettingT;
import com.g3schema.ns_g3module.HMIChannelsT;
import com.g3schema.ns_g3module.HMIGeneralConfT;
import com.g3schema.ns_g3module.HMIModuleT;
import com.g3schema.ns_g3module.IOMChannelsT;
import com.g3schema.ns_g3module.IOMModuleT;
import com.g3schema.ns_g3module.MCMChannelsT;
import com.g3schema.ns_g3module.MCMModuleT;
import com.g3schema.ns_g3module.ModulesT;
import com.g3schema.ns_g3module.PSMChannelsT;
import com.g3schema.ns_g3module.PSMModuleT;
import com.g3schema.ns_g3module.SCMChannelsT;
import com.g3schema.ns_g3module.SCMMK2ModuleT;
import com.g3schema.ns_g3module.SCMModuleT;
import com.g3schema.ns_g3module.SwitchOutChnlT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.xml.HMIScreenXmlWriter;
import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.Inhibit;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleDSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFDM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleHMI;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleIOM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleMCM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSMSettings;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM_MK2;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * XML writer for CAN Module.
 */
class CANModuleWriter extends AbstractXmlWriter {

  CANModuleWriter(IConfig data, ValidationResult result) {
    super(data, result);
  }

  public void write(ModulesT xml) throws Exception {

    // Attributes
    CANModuleManager manager =  data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
    BackPlaneType bp = manager.getBackplaneType();
    if (bp != BackPlaneType.UNDEFINED) {
      xml.backplaneSlotNum.setValue(bp.getSlotNum());
    }

    // Elements
    Collection<ICANModule> moduleList = manager.getModulesByType(MODULE.MODULE_PSM);
    for (Module module : moduleList) {
      exportPSM((ModulePSM) module, xml.PSM.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_SCM);
    for (Module module : moduleList) {
      exportSCM((ModuleSCM) module, xml.SCM.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_DSM);
    for (Module module : moduleList) {
      exportDSM((ModuleDSM) module, xml.DSM.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_FDM);
    for (Module module : moduleList) {
      exportFDM((ModuleFDM) module, xml.FDM.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_FPM);
    for (Module module : moduleList) {
      exportFPM((ModuleFPM) module, xml.FPM.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_HMI);
    for (Module module : moduleList) {
      exportHMI((ModuleHMI) module, xml.HMI.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_IOM);
    for (Module module : moduleList) {
      exportIOM((ModuleIOM) module, xml.IOM.append());
    }

    moduleList = manager.getModulesByType(MODULE.MODULE_MCM);
    for (Module module : moduleList) {
      exportMCM((ModuleMCM) module, xml.MCM.append());
    }
    
    moduleList = manager.getModulesByType(MODULE.MODULE_SCM_MK2);
    for (Module module : moduleList) {
      exportSCMMK2((ModuleSCM_MK2) module, xml.SCM_MK2.append());
    }

  }

  private void exportSCMMK2(ModuleSCM_MK2 module, SCMMK2ModuleT xml_module) {
      xml_module.moduleID.setValue(module.getId().name());
      SCMChannelsT xml_chs = xml_module.channels.append();

      xml_module.openColour.setValue(module.getSettings().getOpenColour().name());
      xml_module.allowForcedOperation.setValue(module.getSettings().isAllowForcedOperation());
      xml_module.motorMode.setValue(module.getSettings().getMotorMode().getValue());
      
      // append digital input channels
      IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
      if (channels != null) {
        for (IChannel ch : channels) {
          exportDigitalCH(ch, xml_chs.digitalInput.append());
        }
      }

      // append analogue input channels
      channels = module.getChannels(ChannelType.ANALOG_INPUT);
      if (channels != null) {
        for (IChannel ch : channels) {
          exportAnalogCH(ch, xml_chs.analogueInput.append());
        }
      }

      // append digital output channels
      for (IChannel ch : module.getChannels(ChannelType.DIGITAL_OUTPUT)) {
        exportDigitalCH(ch, xml_chs.digitalOutput.append());
      }

      // append switch output channels
      for (IChannel ch : module.getChannels(ChannelType.SWITCH_OUT)) {
        exportSwitchOutCH(ch, xml_chs.switchOut.append());
      }
  }

  private void exportPSM(ModulePSM module, PSMModuleT xml_module) {
    // attributes
    xml_module.moduleID.setValue(module.getId().name());
    FanSettingT xml_fan = xml_module.fanSetting.append();
    ModulePSMSettings settings = module.getSettings();
    xml_fan.fanFitted.setValue(settings.isFanFitted());
    xml_fan.fanSpdSensorFitted.setValue(settings.isFanSpeedSensorFitted());
    xml_fan.fanTempHysteresis.setValue(settings.getFanTempHysteresis());
    xml_fan.fanTempThreshold.setValue(settings.getFanTempThreshold());
    xml_fan.faultHysteresis.setValue(settings.getFaultHysteresis());
    xml_fan.fanOutAsLED.setValue(settings.isFanAsLED());
    xml_fan.fanOutAsDO.setValue(settings.isFanAsDO());
    
    // Cyclic Battery Test
    CyclicBatteryTestT xml_cyclicBatTest = xml_module.cyclicBatteryTest.append();
    xml_cyclicBatTest.enabled.setValue(settings.isBatteryTestEnabled());
    xml_cyclicBatTest.schedulePeriodHrs.setValue(settings.getBatteryTestPeriod() * 24);
    xml_cyclicBatTest.testDurationMins.setValue(settings.getBatteryTestDuration());

    PSMChannelsT xml_chs = xml_module.channels.append();

    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }
    
    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }
    
 // append digital output channels
    channels = module.getChannels(ChannelType.DIGITAL_OUTPUT);
   if (channels != null) {
     for (IChannel ch : channels) {
       exportDigitalCH(ch, xml_chs.digitalOutput.append());
     }
   }
  }

  private void exportSCM(ModuleSCM module, SCMModuleT xml_module) {
    xml_module.moduleID.setValue(module.getId().name());
    SCMChannelsT xml_chs = xml_module.channels.append();

    xml_module.openColour.setValue(module.getSettings().getOpenColour().name());
    xml_module.allowForcedOperation.setValue(module.getSettings().isAllowForcedOperation());
    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }

    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }

    // append digital output channels
    for (IChannel ch : module.getChannels(ChannelType.DIGITAL_OUTPUT)) {
      exportDigitalCH(ch, xml_chs.digitalOutput.append());
    }

    // append switch output channels
    for (IChannel ch : module.getChannels(ChannelType.SWITCH_OUT)) {
      exportSwitchOutCH(ch, xml_chs.switchOut.append());
    }

  }

  private void exportDSM(ModuleDSM module, DSMModuleT xml_module) {
    xml_module.moduleID.setValue(module.getId().name());
    SCMChannelsT xml_chs = xml_module.channels.append();

    xml_module.openColour.setValue(module.getSettings().getOpenColour().name());
    xml_module.polarityA.setValue (module.getSettings().getPolarityA().ordinal());
    xml_module.polarityB.setValue (module.getSettings().getPolarityB().ordinal());
    xml_module.supplyEnable24V.setValue(module.getSettings().isSupplyEnable24V());
    xml_module.allowForcedOperationA.setValue(module.getSettings().isAllowForcedOperationA());
    xml_module.allowForcedOperationB.setValue(module.getSettings().isAllowForcedOperationB());
    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }

    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }

    // append digital output channels
    for (IChannel ch : module.getChannels(ChannelType.DIGITAL_OUTPUT)) {
      exportDigitalCH(ch, xml_chs.digitalOutput.append());
    }

    // append switch output channels
    for (IChannel ch : module.getChannels(ChannelType.SWITCH_OUT)) {
      exportSwitchOutCH(ch, xml_chs.switchOut.append());
    }

  }

  private void exportFDM(ModuleFDM module, FDMModuleT xml_module) {
    xml_module.moduleID.setValue(module.getId().name());

    FDMChannelsT xml_chs = xml_module.channels.append();

    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }

    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }
  }

  private void exportFPM(ModuleFPM module, FPMModuleT xml_module) {
    xml_module.moduleID.setValue(module.getId().name());

    FPMChannelsT xml_chs = xml_module.channels.append();

    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }

    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }

    // append FPIConfig channel
    FPIConfig[] cfgChs = module.getAllFPIs();
    for (int i = 0; i < cfgChs.length; i++) {
      cfgChs[i].writeToXML(xml_chs.fpi.append());
    }
  }


  private void exportHMI(ModuleHMI module, HMIModuleT xml_module) {
    xml_module.moduleID.setValue(module.getId().name());

    HMIChannelsT xml_chs = xml_module.channels.append();

    // Append analogue input channels
    IChannel[] channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }

    // Export attributes
    HMIGeneralConfT xml_hmiGeneral = xml_module.generalConfig.append();
    xml_hmiGeneral.buttonClick.setValue(module.getSettings().isButtonClick());
    xml_hmiGeneral.requestOLR.setValue(module.getSettings().isRequestOLR());
    xml_hmiGeneral.openClosePressTimeMs.setValue(module.getSettings().getOpenclosePressTime());

    
    HMIScreenManager manager = data.getConfigModule(HMIScreenManager.CONFIG_MODULE_ID);
    new HMIScreenXmlWriter(manager, this).write(xml_module.screens.append(), result);
  }

  private void exportIOM(ModuleIOM module, IOMModuleT xml_module) {
    xml_module.moduleID.setValue(module.getId().name());
    IOMChannelsT xml_chs = xml_module.channels.append();

    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }

    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }

    // append digital output channels
    channels = module.getChannels(ChannelType.DIGITAL_OUTPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalOutput.append());
      }
    }
  }

  private void exportMCM(ModuleMCM module, MCMModuleT xml_module) {
    // attributes
    xml_module.moduleID.setValue(module.getId().name());

    MCMChannelsT xml_chs = xml_module.channels.append();

    // append digital input channels
    IChannel[] channels = module.getChannels(ChannelType.DIGITAL_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportDigitalCH(ch, xml_chs.digitalInput.append());
      }
    }

    // append analogue input channels
    channels = module.getChannels(ChannelType.ANALOG_INPUT);
    if (channels != null) {
      for (IChannel ch : channels) {
        exportAnalogCH(ch, xml_chs.analogueInput.append());
      }
    }
  }

  private void exportDigitalCH(IChannel ch, DigitalInputChnlT xml_ch) {
    xml_ch.channelID.setValue(ch.getId());

    if (ch.hasParameter(ICANChannel.PARAM_EXTEQUITPINVERT)) {
      Object invert = ch.getParameter(ICANChannel.PARAM_EXTEQUITPINVERT);
      xml_ch.extEquipInvert.setValue((Boolean) invert);
    } else {
      xml_ch.extEquipInvert.setValue(false);
    }

    xml_ch.eventEnable.setValue((Boolean) ch.getParameter(ICANChannel.PARAM_EVENTENABLED));
    xml_ch.dbHigh2LowMs.setValue((Long) (ch.getParameter(ICANChannel.PARAM_DBHIGH2LOWMS)));
    xml_ch.dbLow2HighMs.setValue((Long) (ch.getParameter(ICANChannel.PARAM_DBLOW2HIGHMS)));
    xml_ch.enable.setValue(true); // TODO remove enable property from schema
  }

  private void exportDigitalCH(IChannel ch, DigitalOutputChnlT xml_ch) {
    xml_ch.channelID.setValue(ch.getId());
    xml_ch.pulseLength.setValue((Long) ch.getParameter(ICANChannel.PARAM_PULSELENGTH));
    xml_ch.enable.setValue(true);
  }

  private void exportAnalogCH(IChannel ch, AnalogueInputChnlT xml_ch) {
    xml_ch.channelID.setValue(ch.getId());
    xml_ch.eventMs.setValue((Long) (ch.getParameter(ICANChannel.PARAM_EVENT_MS)));
    xml_ch.eventEnable.setValue((Boolean) ch.getParameter(ICANChannel.PARAM_EVENTENABLED));
    xml_ch.enable.setValue(true);
  }

  private void exportSwitchOutCH(IChannel ch, SwitchOutChnlT xml_ch) {
    xml_ch.channelID.setValue(ch.getId());

    Polarity polarity = Polarity.SWITCH_POLARITY_UP;
    if (ch.hasParameter(ICANChannel.PARAM_POLARITY)) {
      polarity = (Polarity) ch.getParameter(ICANChannel.PARAM_POLARITY);
    }
    xml_ch.polarity.setValue(polarity.getValue());

    if (ch.hasParameter(ICANChannel.PARAM_PULSELENGTH)) {
      xml_ch.pulseLength.setValue((Long) ch.getParameter(ICANChannel.PARAM_PULSELENGTH));
    }
    
    if (ch.hasParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS)) {
      xml_ch.motorOverDriveMS.setValue((Long) ch.getParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS));
    }
    
    if (ch.hasParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS)) {
      xml_ch.motorReverseDriveMS.setValue((Long) ch.getParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS));
    }
    
    xml_ch.overrunMS.setValue((Long) ch.getParameter(ICANChannel.PARAM_OVERRUN_MS));
    xml_ch.opTimeoutS.setValue((Long) ch.getParameter(ICANChannel.PARAM_OPTIMEOUT_SEC));
    Inhibit inhibit = (Inhibit) ch.getParameter(ICANChannel.PARAM_INHIBI);
    xml_ch.inhibit.setValue(inhibit.getInhibitMask());
    xml_ch.enable.setValue(true);
  }
}
