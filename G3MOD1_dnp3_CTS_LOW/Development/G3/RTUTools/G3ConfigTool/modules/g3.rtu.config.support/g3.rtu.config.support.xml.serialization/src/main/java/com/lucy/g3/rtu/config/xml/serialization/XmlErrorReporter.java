/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.util.List;
import java.util.logging.Level;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.common.base.Strings;
import com.jgoodies.validation.ValidationMessage;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;

/**
 * The Class XmlErrorReport.
 */
class XmlErrorReporter {
  private XmlErrorReporter() {}
  
  public static void showErrorReportDialog(ValidationResult result) {
    if (result.hasErrors()) {
      // Compose error message
      StringBuilder sb = new StringBuilder();
      if (result != null && result.hasErrors()) {
        sb.append("<strong>Details:</strong>\n");
        List<ValidationMessage> errorMsgs = result.getErrors();
        for (ValidationMessage msg : errorMsgs) {
          sb.append("<li>");
          sb.append(msg.formattedText());
          sb.append("</li>");
        }
      }
      String errmsg = sb.toString();

      // Show dialog
      if (!Strings.isBlank(errmsg)) {
        /* Compose and show error info */
        ErrorInfo info = new ErrorInfo("Failure",
            "Errors found in XML file", errmsg, null, null,
            Level.SEVERE, null);
        JXErrorPane.showDialog(WindowUtils.getMainFrame(), info);
      }
    }
  }
}
