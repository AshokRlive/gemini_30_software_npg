/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import com.g3schema.ConfigVersionT;
import com.g3schema.ConfigurationT;
import com.g3schema.HeaderT;
import com.g3schema.MiscellaneousT;
import com.g3schema.OffLocalButtonSettingT;
import com.g3schema.SecurityPolicyT;
import com.g3schema.ToolVersionT;
import com.g3schema.UserAccountT;
import com.g3schema.UserSettingT;
import com.g3schema.g3schema2;
import com.g3schema.ns_g3module.HMIModuleT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.xml.CommsDeviceReader;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.device.fields.xml.FieldsDeviceReader;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.xml.HMIScreenXmlReader;
import com.lucy.g3.rtu.config.misc.MiscConfig;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.xml.PortsReader;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.integrated.CustomTemplates;
import com.lucy.g3.rtu.config.user.UserConfig;
import com.lucy.g3.rtu.config.user.UserManager;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 * Read XML document (based on g3schema) and save all contents into a
 * {@code IConfig} instance.
 */
public class XmlDataReader {

  /** The configuration object that the XML will be read into. */
  private IConfig data;

  private XmlParserInvoker invoker;

  private final ValidationResult result = new ValidationResult();


  public XmlDataReader(XmlParserInvoker invoker) {
    this.invoker = invoker;
  }

  /**
   * Read content from XML file into data.
   */
  public IConfig readFile(File file) throws Exception {
    if (file == null) {
      throw new IllegalArgumentException("Invalid file name: " + file);
    }

    data = ConfigFactory.getInstance().create();
    
    GeneralConfigUtility.setFilePath(data, file.getPath());

    printMsg("Start loading XMl document", 0);
    g3schema2 doc;

    // Decompress gzip file
    if (file.getAbsolutePath().endsWith(G3Files.SUFFFIX_GZIP)) {
      byte[] os = ZipUtil.ungzip(file.getAbsolutePath());
      doc = com.g3schema.g3schema2.loadFromString(new String(os, StandardCharsets.UTF_8));
    } else {
      doc = com.g3schema.g3schema2.loadFromFile(file.getPath());
    }

    loadDocument(doc);
    printMsg("XMl document load completed", 100);

    if (result.hasErrors()) {
      XmlErrorReporter.showErrorReportDialog(result);
    }
    return data;
  }

  public IConfig readFromStream(InputStream is) throws Exception {
    return readFromString(getStringFromInputStream(is));
  }
  
  public IConfig readFromString(String xmlstring) throws Exception {
    Preconditions.checkNotNull(xmlstring, "xmlstring is null");

    data = ConfigFactory.getInstance().create();

    printMsg("Start loading XMl document", 0);
    g3schema2 doc = com.g3schema.g3schema2.loadFromString(new String(xmlstring.getBytes(), StandardCharsets.UTF_8));

    loadDocument(doc);
    printMsg("XMl document load completed", 100);

    if (result.hasErrors()) {
      XmlErrorReporter.showErrorReportDialog(result);
    }

    return data;
  }

  private void printMsg(String msg, int progress) {
    if (invoker != null) {
      invoker.printMessage(msg);
      invoker.setProgressValue(progress);
    }
  }

  private void loadDocument(g3schema2 doc) throws Exception {
    ConfigurationT configRoot = doc.configuration.first();

    GeneralConfig general = data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);

    loadGeneral(configRoot, general);

    printMsg("Loading modules...", 10);
    new CANModuleReader(data, result).read(configRoot.modules.first());

    printMsg("Loading ports...", 13);
    PortsManager portMan = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    new PortsReader().read(portMan, configRoot.ports.first());

    printMsg("Loading fields device...", 18);
    new FieldsDeviceReader(
        (FieldDeviceManager)data.getConfigModule(FieldDeviceManager.CONFIG_MODULE_ID), 
        portMan, result).read(configRoot.fieldDevices.first());

    printMsg("Loading virtual points...", 20);
    VirtualPointReader vpointReader = new VirtualPointReader(data, result);
    vpointReader.read(configRoot.virtualPoints.first());

    printMsg("Loading control logic...", 30);
    CLogicManager clogicMan = data.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    clogicMan.read(configRoot.controlLogic.first(), new XmlReadSupporter());

    /*
     * Caution: counter must be read after all other virtual points have been
     * read cause counter point depends on other virtual point
     */
    vpointReader.readCounterPoints(configRoot.virtualPoints.first());

    clogicMan.configureIO(configRoot.controlLogic.first(), new XmlReadSupporter());
    
    printMsg("Loading comms devices...", 75);
    CommsDeviceManager commsMan = data.getConfigModule(CommsDeviceManager.CONFIG_MODULE_ID); 
    new CommsDeviceReader(commsMan, portMan, result)
        .readCommsDevices(configRoot.commsDevices.first());

    printMsg("Loading Scada protocol...", 40);
    new ScadaProtoReader(data, result).read(configRoot.scadaProtocol.first());

    printMsg("Loading misc...", 50);
    if (configRoot.miscellaneous.exists()) {
      MiscConfig misc = data.getConfigModule(MiscConfig.CONFIG_MODULE_ID);
      misc.readFromXML(configRoot.miscellaneous.first());
    }

    printMsg("Loading user accounts...", 60);
    loadUserSetting(configRoot.userSetting.first());

    printMsg("Loading HMI menu...", 80);
    if (configRoot.modules.first().HMI.count() > 0) {
      HMIModuleT xml_hmi = configRoot.modules.first().HMI.first();
      HMIScreenManager hmi = data.getConfigModule(HMIScreenManager.CONFIG_MODULE_ID);
      new HMIScreenXmlReader(hmi, new XmlReadSupporter()).read(xml_hmi.screens.first(), result);
    }

    if (configRoot.template.exists()) {
      printMsg("Loading template...", 90);
      try {
        CustomTemplates templ = data.getConfigModule(CustomTemplates.CONFIG_MODULE_ID);
        templ.readFromXML(configRoot.template.first());
      } catch (Throwable e) {
        result.addWarning("Failed to load template");
      }
    }
  }

  private void loadGeneral(ConfigurationT configRoot, GeneralConfig general)
      throws UnsupportedEncodingException {
    // Read config file description
    if (configRoot.configFileDescription.exists()) {
      general.setConfigDescription(configRoot.configFileDescription.getValue());
    }

    // Read site name
    if (configRoot.siteName.exists()) {
      String siteName = configRoot.siteName.getValue();

      // If the site name is stored as HEX string, decode it to string
      if (siteName.startsWith("0x") || siteName.startsWith("0X")) {
        siteName = SecurityUtils.decodeHexToString(siteName, XmlDataWriter.STRING_CHARSET);
      }

      general.setSiteName(siteName);
    }

    // Read config file modification date
    if (configRoot.modificationDate.exists()) {
      String modifyDate = configRoot.modificationDate.getValue();
      general.setModifyDate(modifyDate);
    }

    // Read config file version
    long major = 0;
    long minor = 0;
    long patch = 0;
    String releaseType = null;
    if (configRoot.configVersionMajor.exists() 
        || configRoot.configVersionMinor.exists()
        || configRoot.configVersionPatch.exists()) {
      if (configRoot.configVersionMajor.exists()) {
        major = configRoot.configVersionMajor.getValue();
      }
      if (configRoot.configVersionMinor.exists()) {
        minor = configRoot.configVersionMinor.getValue();
      }
      if (configRoot.configVersionPatch.exists()) {
        patch = configRoot.configVersionPatch.getValue();
      }
      if (configRoot.configReleaseType.exists()) {
        releaseType = configRoot.configReleaseType.getValue();
      }
      
      general.setConfigVersion(
          String.format("%s.%s.%s", 
              Long.toString(major), 
              Long.toString(minor),
              Long.toString(patch)));
      
      general.setConfigReleaseType(releaseType);
    }
    
    
    if(configRoot.header.exists()) {
      HeaderT xmlHeader = configRoot.header.first();
      
      ConfigVersionT xmlConfig = xmlHeader.configVersion.first();
      if(xmlConfig.configReleaseType.exists()) {
        releaseType = xmlConfig.configReleaseType.getValue();
        general.setConfigReleaseType(releaseType);
      }
      
      major = xmlConfig.configVersionMajor.getValue();
      minor = xmlConfig.configVersionMinor.getValue();
      patch = xmlConfig.configVersionPatch.getValue();
      general.setConfigVersion(
          String.format("%s.%s.%s", 
              Long.toString(major), 
              Long.toString(minor),
              Long.toString(patch)));
      
      if (xmlConfig.modificationDate.exists()) {
        String modifyDate = xmlConfig.modificationDate.getValue();
        general.setModifyDate(modifyDate);
      }
      
      if (xmlConfig.configFileDescription.exists()) {
        general.setConfigDescription(xmlConfig.configFileDescription.getValue());
      }
      
      if(xmlHeader.toolVersion.exists()) {
        ToolVersionT xmltool = xmlHeader.toolVersion.first();
        if(xmltool.revision.exists())
          general.setToolReveision(xmltool.revision.getValue());
        if(xmltool.version.exists())
          general.setToolVersion(xmltool.version.getValue());
      }
    }
  }

  private void loadUserSetting(UserSettingT xml_userSetting) {
    UserManager userman = data.getConfigModule(UserManager.CONFIG_MODULE_ID);

    if(xml_userSetting.securityPolicy.exists()) {
      SecurityPolicyT xml_security = xml_userSetting.securityPolicy.first();
      if(xml_security.accountLockoutDurationSecs.exists())
        userman.setAccountLockoutDurationSecs((int) xml_security.accountLockoutDurationSecs.getValue());
      
      if(xml_security.invalidLogonAttemptsThreshold.exists())
        userman.setInvalidLogonAttemptsThreshold((int) xml_security.invalidLogonAttemptsThreshold.getValue());
    }
    
    
    int count = xml_userSetting.userAccounts.count();

    ArrayList<UserConfig> usersList = new ArrayList<UserConfig>();
    for (int i = 0; i < count; i++) {
      UserAccountT xml_user = xml_userSetting.userAccounts.at(i);
      String name = xml_user.name.getValue();
      String password = xml_user.password.getValue(); // MD5 password
      USER_LEVEL group = USER_LEVEL.forValue((int) xml_user.group.getValue());
      String mdAlg = null;
      if(xml_user.mdAlgorithm.exists())
        mdAlg = xml_user.mdAlgorithm.getValue();
      if(mdAlg == null) 
        mdAlg = UserConfig.MD5; // Use legacy MD
      
      UserConfig user = new UserConfig(name, password, group, mdAlg);
      if(xml_user.pin.exists()) {
        String pin = xml_user.pin.getValue();
        if(pin.length() == 4) 
        {
          // Pin is plain text, must be converted to hash.
          pin = Strings.isBlank(pin)?pin:SecurityUtils.calculateMDStr(pin.toCharArray(), 
            UserConfig.PIN_MD_ALGORITHM);
        }

        user.setPIN(pin);
      }
      
      usersList.add(user);
    }
    
    userman.loadAllUser(usersList);
  }


  private final class XmlReadSupporter extends AbstractXmlReader {

    XmlReadSupporter() {
      super(XmlDataReader.this.data, XmlDataReader.this.result);
    }
  }
  
  private static String getStringFromInputStream(InputStream is) {

    BufferedReader br = null;
    StringBuilder sb = new StringBuilder();

    String line;
    try {

      br = new BufferedReader(new InputStreamReader(is));
      while ((line = br.readLine()) != null) {
        sb.append(line);
      }

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return sb.toString();

  }
}