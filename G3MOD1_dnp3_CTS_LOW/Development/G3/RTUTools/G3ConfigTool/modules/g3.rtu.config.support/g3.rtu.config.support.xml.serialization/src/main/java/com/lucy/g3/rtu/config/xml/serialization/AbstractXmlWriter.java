/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import org.apache.log4j.Logger;

import com.g3schema.ns_common.ChannelRefT;
import com.g3schema.ns_common.VirtualPointRefT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.hmi.xml.IHMIXmlWriteSupport;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.xml.VirtualPointWriteSupport;

/**
 * Abstract XML writer.
 */
abstract class AbstractXmlWriter implements VirtualPointWriteSupport, IHMIXmlWriteSupport {

  protected final Logger log = Logger.getLogger(getClass());

  protected final ValidationResult result;

  protected final IConfig data;


  AbstractXmlWriter(IConfig data, ValidationResult result) {
    this.data = data;
    this.result = result;
  }

  @Override
  public void writeChannelRef(String source, ChannelRefT xml_channel, IChannel ch) {
    if (ch != null) {
      xml_channel.moduleType.setValue(ch.getOwnerModule().getType().name());
      xml_channel.moduleID.setValue(ch.getOwnerModule().getId().name());
      xml_channel.channelID.setValue(ch.getId());
    } else {
      String msg = "The channel of \"" + source + "\" is not configured";
      log.error(msg);
      result.addError(msg);
    }
  }

  @Override
  public void writeVirtualPointRef(String source, VirtualPointRefT xml, VirtualPoint point) {
    if (point != null) {
      xml.pointGroup.setValue(point.getGroup());
      xml.pointID.setValue(point.getId());
    } else {
      String msg = "The point of \"" + source + "\" is not configured";
      log.error(msg);
      result.addError(msg);
    }
  }

  @Override
  public ValidationResult getResult() {
    return result;
  }
}
