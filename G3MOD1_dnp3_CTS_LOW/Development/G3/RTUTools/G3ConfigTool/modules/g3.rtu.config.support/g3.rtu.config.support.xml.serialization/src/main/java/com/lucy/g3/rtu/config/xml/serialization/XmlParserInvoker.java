/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;


/**
 * The object that invoke XML parser: {@linkplain XmlDataReader} or
 * {@linkplain XmlDataWriter} should implement this interface if it needs to get
 * message&progress information from XML parser.
 */
public interface XmlParserInvoker {

  /** The maximum progress value to be reported. */
  int TOTAL_PROGRESS = 100;


  /**
   * Prints the parsing message.
   */
  void printMessage(String message);

  /**
   * Sets the parsing progress which is equals or less than
   * <code>TOTAL_PROGRESS</code>.
   */
  void setProgressValue(int progress);
}
