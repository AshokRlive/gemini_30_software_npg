/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.util.Collection;

import com.g3schema.ScadaProtocolT;
import com.g3schema.ns_s101.S101T;
import com.g3schema.ns_s104.S104T;
import com.g3schema.ns_sdnp3.SDNP3T;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * XML writer for SCADA protocol.
 */
class ScadaProtoWriter extends AbstractXmlWriter {

  ScadaProtoWriter(IConfig data, ValidationResult result) {
    super(data, result);
  }

  public void write(ScadaProtocolT xml) {
    ScadaProtocolManager manager = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    Collection<IScadaProtocol<?>> slaveProtocols = manager.getAllItems();

    for (IProtocol<?> slave : slaveProtocols) {

      if (slave instanceof SDNP3) {
        writeDNP3(xml.sDNP3.append(), (SDNP3) slave);
      } else if (slave instanceof S104) {
        writeIEC104(xml.sIEC104.append(), (S104) slave);
      } else if (slave instanceof S101) {
        writeIEC101(xml.sIEC101.append(), (S101) slave);
      }
    }
  }

  private void writeDNP3(SDNP3T xml_dnp3, SDNP3 dnp3) {
    Collection<SDNP3Channel> channels = dnp3.getAllChannels();
    
    for (SDNP3Channel channel : channels) {
      channel.writeToXML(xml_dnp3.channel.append(), this);
    }
  }
  
  private void writeIEC104(S104T xml, S104 protocol) {
    Collection<S104Channel> channels = protocol.getAllChannels();

    for (S104Channel channel : channels) {
      channel.writeToXML(xml.channel.append());
    }
  }
  
  private void writeIEC101(S101T xml, S101 protocol) {
    Collection<S101Channel> channels = protocol.getAllChannels();
    
    for (S101Channel channel : channels) {
      channel.writeToXML(xml.channel.append());
    }
  }

}
