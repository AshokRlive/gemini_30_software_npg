/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.g3schema.ns_clogic.CLogicParametersT;
import com.lucy.g3.rtu.config.integrated.ConfigPlugin;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataWriter;
import com.lucy.g3.rtu.config.xml.serialization.XmlParserInvoker;
import com.lucy.g3.xml.gen.common.versions;

/**
 * The Class XmlProcessTest.
 */
//@Ignore // Ignore cause it involves GUI in the test.
public class XmlProcessTest {

  static final URL SAMPLE_CONFIG_XML = XmlProcessTest.class.getClassLoader().getResource("sampleConfig.xml");
  static final URL SAMPLE_CONFIG_XML2 = XmlProcessTest.class.getClassLoader().getResource("G3Config.xml");
  static final URL SAMPLE_CONFIG_GZ = XmlProcessTest.class.getClassLoader().getResource("sampleConfig.gz");

  private XmlParserInvoker invoker = new XmlParserInvoker() {

    @Override
    public void printMessage(String message) {

    }

    @Override
    public void setProgressValue(int progress) {

    }
  };


  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    ConfigPlugin.init();
  }

  
  @Test
  public void testReadWrite() throws URISyntaxException, Exception {
  /*IConfig data = new XmlDataReader(invoker).readFile(new File(SAMPLE_CONFIG_XML2.toURI()));
  CANModuleManager man = data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
  System.out.println("size:" + man.getSize());
  
  XmlDataWriter writer = new XmlDataWriter(data, invoker);
  writer.saveToFile("target/testOutput.xml");*/

  }
  
//TODO
  @Ignore
  // Sample config needs update
//  @Test
//  public void testReadXmlConfig() throws Exception {
//
//    ConfigData data = new XmlDataReader(inoker).readFile(new File(SAMPLE_CONFIG_XML.toURI()));
//
//    CLogicManager mgr = data.getcLogicManager();
//    assertEquals(8, mgr.getSize());
//
//    Collection<ICLogic> logics = mgr.getLogicByType(
//        CLogicType.SGL, CLogicType.DSL,
//        CLogicType.DOL, CLogicType.FAN,
//        CLogicType.BAT_CHARGER);
//
//    assertEquals(7, logics.size());
//  }

//  @Ignore
//  // Sample config needs update
//  @Test
//  public void testReadZipConfig() throws Exception {
//
//    ConfigData data = new XmlDataReader(inoker).readFile(new File(SAMPLE_CONFIG_GZ.toURI()));
//
//    CLogicManager mgr = data.getcLogicManager();
//    assertEquals(8, mgr.getSize());
//
//    Collection<ICLogic> logics = mgr.getLogicByType(
//        CLogicType.SGL, CLogicType.DSL,
//        CLogicType.DOL, CLogicType.FAN,
//        CLogicType.BAT_CHARGER);
//
//    assertEquals(7, logics.size());
//  }
//
//  @Test
//  public void testWriteXmlConfig() throws Exception {
//    ConfigData data = new ConfigDataImpl();
//    buildConfigData(data);
//
//    File file = new File("test_output.xml");
//    new XmlDataWriter(data, null).saveToFile(file.getAbsolutePath());
//    file.delete();
//  }
//
//  @Test
//  public void testWriteZipConfig() throws Exception {
//    ConfigData data = new ConfigDataImpl();
//    File file = new File("g.gz");
//    new XmlDataWriter(data, null).saveToFile(file.getPath());
//    file.delete();
//  }
//
//  @Test
//  public void testReadConfig() throws Exception {
//    ConfigData data = new ConfigDataImpl();
//    buildConfigData(data);
//
//    final File file = new File("test.xml");
//
//    new XmlDataWriter(data, null).saveToFile(file.getAbsolutePath());
//    Assert.assertTrue(file.exists());
//    ConfigData readata = new XmlDataReader(null).readFile(file);
//
//    assertEquals(data.getModulesManager().getAllModules().size(),
//        readata.getModulesManager().getAllModules().size());
//
//    assertEquals(data.getVirtualPointsManager().getAllPointsNum(),
//        readata.getVirtualPointsManager().getAllPointsNum());
//
//    file.delete();
//  }

  // Manual Test
  // @Test
  public void testReadConfig2() throws Exception {

    final File file = new File("test.xml");

    new XmlDataReader(null).readFile(file);

    file.delete();
  }

  @Test
  public void test1() throws Exception {
    com.g3schema.g3schema2 document = com.g3schema.g3schema2.createDocument();
    com.g3schema.ConfigurationT root = document.configuration.append();
    document.setSchemaLocation("g3schema.xsd");
    root.schemaVersionMajor.setValue(versions.SCHEMA_MAJOR);
    root.schemaVersionMinor.setValue(versions.SCHEMA_MINOR);

    CLogicParametersT xml = root.controlLogic.append().clogic.append().parameters.append();

    assertEquals(0, xml.getNode().getChildNodes().getLength());
    xml.actionParam.append();
    assertEquals(1, xml.getNode().getChildNodes().getLength());
    xml.actionParam.remove();
    assertEquals(0, xml.getNode().getChildNodes().getLength());
  }

  
//  private static void buildConfigData(ConfigData data) {
//    ConfiguratorImpl factory = new ConfiguratorImpl(data);
//
//    // Create module
//    factory.createCANModule(MODULE.MODULE_DSM, 7);
//    factory.createCANModule(MODULE.MODULE_SCM, 7);
//    factory.createCANModule(MODULE.MODULE_FPM, 7);
//    factory.createCANModule(MODULE.MODULE_HMI, 1);
//    factory.createCANModule(MODULE.MODULE_IOM, 7);
//    factory.createCANModule(MODULE.MODULE_PSM, 1);
//    factory.createCANModule(MODULE.MODULE_MCM, 1);
//
//    ConfiguratorOption option = new ConfiguratorOption();
//    option.addOption(ConfiguratorOption.Target.VirtualPoint, GenerateOption.GenerateFull);
//    option.addOption(ConfiguratorOption.Target.ScadaPoint, GenerateOption.DoNotGenerate);
//
//    // Create points
//    Collection<IOModule> mlist = data.getModulesManager().getAllIOModules();
//    factory.createVirtualPoints(option, mlist.toArray(new IOModule[mlist.size()]));
//  }
}
