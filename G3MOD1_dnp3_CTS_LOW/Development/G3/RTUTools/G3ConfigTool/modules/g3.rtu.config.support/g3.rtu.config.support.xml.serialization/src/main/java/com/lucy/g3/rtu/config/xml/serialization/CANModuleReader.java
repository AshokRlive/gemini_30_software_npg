/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.util.ArrayList;

import com.g3schema.ns_g3module.AnalogueInputChnlT;
import com.g3schema.ns_g3module.CyclicBatteryTestT;
import com.g3schema.ns_g3module.DSMModuleT;
import com.g3schema.ns_g3module.DigitalInputChnlT;
import com.g3schema.ns_g3module.DigitalOutputChnlT;
import com.g3schema.ns_g3module.FDMModuleT;
import com.g3schema.ns_g3module.FPIChannelT;
import com.g3schema.ns_g3module.FPMModuleT;
import com.g3schema.ns_g3module.FanSettingT;
import com.g3schema.ns_g3module.HMIGeneralConfT;
import com.g3schema.ns_g3module.HMIModuleT;
import com.g3schema.ns_g3module.IOMChannelsT;
import com.g3schema.ns_g3module.IOMModuleT;
import com.g3schema.ns_g3module.MCMChannelsT;
import com.g3schema.ns_g3module.MCMModuleT;
import com.g3schema.ns_g3module.ModulesT;
import com.g3schema.ns_g3module.PSMModuleT;
import com.g3schema.ns_g3module.SCMMK2ModuleT;
import com.g3schema.ns_g3module.SCMModuleT;
import com.g3schema.ns_g3module.SwitchOutChnlT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.Inhibit;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleDSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFDM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleHMI;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleIOM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleMCM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM_MK2;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.shared.domain.Polarity;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.MOTOR_MODE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.LED_COLOUR;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * XML reader for CAN module.
 */
class CANModuleReader extends AbstractXmlReader {

  CANModuleReader(IConfig data, ValidationResult result) {
    super(data, result);
  }

  public void read(ModulesT xml) throws Exception {

    ArrayList<ICANModule> moduleList = new ArrayList<ICANModule>(15);

    // ModulePSM 1
    int num = xml.PSM.count();
    assert num <= 1;
    for (int i = 0; i < num; i++) {
      ModulePSM psm = new ModulePSM();
      loadPSM(xml.PSM.first(), psm);
      moduleList.add(0, psm);
    }

    // ModuleSCM 0~6
    num = xml.SCM.count();
    for (int i = 0; i < num; i++) {
      ModuleSCM scm = new ModuleSCM();
      loadSCM(xml.SCM.at(i), scm);
      moduleList.add(scm);
    }

    // ModuleDSM 0~6
    num = xml.DSM.count();
    for (int i = 0; i < num; i++) {
      ModuleDSM dsm = new ModuleDSM();
      loadDSM(xml.DSM.at(i), dsm);
      moduleList.add(dsm);
    }

    // ModuleFDM 0~4
    num = xml.FDM.count();
    for (int i = 0; i < num; i++) {
      ModuleFDM fdm = new ModuleFDM();
      loadFDM(xml.FDM.at(i), fdm);
      moduleList.add(fdm);
    }

    // ModuleFPM 0~6
    num = xml.FPM.count();
    for (int i = 0; i < num; i++) {
      ModuleFPM fdm = new ModuleFPM();
      loadFPM(xml.FPM.at(i), fdm);
      moduleList.add(fdm);
    }

    // ModuleHMI 0~1
    num = xml.HMI.count();
    assert num <= 1;
    for (int i = 0; i < num; i++) {
      ModuleHMI hmi = new ModuleHMI();
      loadHMI(xml.HMI.at(i), hmi);
      moduleList.add(hmi);
    }

    // ModuleIOM
    num = xml.IOM.count();
    for (int i = 0; i < num; i++) {
      IOMModuleT xml_iom = xml.IOM.at(i);
      ModuleIOM iom = new ModuleIOM();
      loadIOM(xml_iom, iom);
      moduleList.add(iom);
    }

    // ModuleMCM
    num = xml.MCM.count();
    assert num == 1;
    for (int i = 0; i < num; i++) {
      ModuleMCM mcm = new ModuleMCM();
      loadMCM(xml.MCM.at(i), mcm);
      moduleList.add(0, mcm);
    }

    // ModuleSCM 0~6
    num = xml.SCM_MK2.count();
    for (int i = 0; i < num; i++) {
      ModuleSCM_MK2 scm = new ModuleSCM_MK2();
      loadSCMMK2(xml.SCM_MK2.at(i), scm);
      moduleList.add(scm);
    }
    
    // Add all loaded modules into buffer data
    CANModuleManager canModuleMgr =  data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
    canModuleMgr.addAll(moduleList);

    if (xml.backplaneSlotNum.exists()) {
      int slotNum = (int) xml.backplaneSlotNum.getValue();
      canModuleMgr.setBackplane(BackPlaneType.Num2Enum(slotNum));
    }

  }

  private void loadMCM(MCMModuleT xml_mcm, ModuleMCM mcm)
      throws InvalidConfException {
    MCMChannelsT xml_chs = xml_mcm.channels.first();
    // load digital input channels
    for (int j = 0; j < xml_chs.digitalInput.count(); j++) {
      loadDigitalChannels(mcm, xml_chs.digitalInput.at(j));
    }

    // load analogue input channels
    for (int j = 0; j < xml_chs.analogueInput.count(); j++) {
      loadAnalogChannels(mcm, xml_chs.analogueInput.at(j));
    }
  }

  private void loadIOM(IOMModuleT xml, ModuleIOM module)
      throws InvalidConfException {

    module.setId(convertModuleID(xml.moduleID.getValue()));

    IOMChannelsT xml_chs = xml.channels.first();
    // load digital input channels
    for (int j = 0; j < xml_chs.digitalInput.count(); j++) {
      loadDigitalChannels(module, xml_chs.digitalInput.at(j));
    }

    // load digital output channels
    for (int j = 0; j < xml_chs.digitalOutput.count(); j++) {
      loadDigitalChannels(module, xml_chs.digitalOutput.at(j));
    }

    // load analogue input channels
    for (int j = 0; j < xml_chs.analogueInput.count(); j++) {
      loadAnalogChannels(module, xml_chs.analogueInput.at(j));
    }
  }

  private void loadHMI(HMIModuleT xml_hmi, ModuleHMI hmi)
      throws InvalidConfException {

    // Load analogue channels
    for (int j = 0; j < xml_hmi.channels.first().analogueInput.count(); j++) {
      loadAnalogChannels(hmi, xml_hmi.channels.first().analogueInput.at(j));
    }

    // Load HMI general settings
    HMIGeneralConfT xml_hmiGeneral = xml_hmi.generalConfig.first();
    try {
      hmi.getSettings().setButtonClick(xml_hmiGeneral.buttonClick.getValue());

      hmi.getSettings().setRequestOLR(xml_hmiGeneral.requestOLR.getValue());
      if (xml_hmiGeneral.openClosePressTimeMs.exists()) {
        hmi.getSettings().setOpenclosePressTime((int) xml_hmiGeneral.openClosePressTimeMs.getValue());
      }
    } catch (Exception e) {
      log.error("Fail to parse HmiCh attributes cause: " + e.getMessage());
    }
  }

  private void loadFDM(FDMModuleT xml, ModuleFDM module)
      throws InvalidConfException {
    module.setId(convertModuleID(xml.moduleID.getValue()));

    // load channels
    for (int j = 0; j < xml.channels.first().digitalInput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalInput.at(j));
    }
    for (int j = 0; j < xml.channels.first().analogueInput.count(); j++) {
      loadAnalogChannels(module, xml.channels.first().analogueInput.at(j));
    }
  }

  private void loadFPM(FPMModuleT xml, ModuleFPM module)
      throws InvalidConfException {
    module.setId(convertModuleID(xml.moduleID.getValue()));

    // load channels
    int num = xml.channels.first().digitalInput.count();
    for (int j = 0; j < num; j++) {
      loadDigitalChannels(module, xml.channels.first().digitalInput.at(j));
    }

    num = xml.channels.first().analogueInput.count();
    for (int j = 0; j < num; j++) {
      loadAnalogChannels(module, xml.channels.first().analogueInput.at(j));
    }

    FPIConfig[] fpiChs = module.getAllFPIs();
    num = xml.channels.first().fpi.count();
    for (int i = 0; i < fpiChs.length && i < num; i++) {
      FPIChannelT xml_cfg = xml.channels.first().fpi.at(i);
      fpiChs[i].readFromXML(xml_cfg);
    }
  }

  private void loadPSM(PSMModuleT xml_psm, ModulePSM psm)
      throws InvalidConfException {
    FanSettingT xml_fan = xml_psm.fanSetting.first();
    psm.getSettings().setFanFitted(xml_fan.fanFitted.getValue());
    psm.getSettings().setFanSpeedSensorFitted(xml_fan.fanSpdSensorFitted.getValue());
    psm.getSettings().setFanTempHysteresis(xml_fan.fanTempHysteresis.getValue());
    psm.getSettings().setFanTempThreshold(xml_fan.fanTempThreshold.getValue());
    psm.getSettings().setFaultHysteresis(xml_fan.faultHysteresis.getValue());
    
    if(xml_fan.fanOutAsLED.exists())
    	psm.getSettings().setFanAsLED(xml_fan.fanOutAsLED.getValue());
    if(xml_fan.fanOutAsDO.exists())
    	psm.getSettings().setFanAsDO(xml_fan.fanOutAsDO.getValue());

    // Cyclic Battery Test
    if (xml_psm.cyclicBatteryTest.exists()) {
      CyclicBatteryTestT xml_cyclicBatTest = xml_psm.cyclicBatteryTest.first();
      psm.getSettings().setBatteryTestPeriod(xml_cyclicBatTest.schedulePeriodHrs.getValue() / 24);
      psm.getSettings().setBatteryTestDuration(xml_cyclicBatTest.testDurationMins.getValue());
      psm.getSettings().setBatteryTestEnabled(xml_cyclicBatTest.enabled.getValue());
    }

    // load channels
    for (int i = 0; i < xml_psm.channels.first().digitalInput.count(); i++) {
      loadDigitalChannels(psm, xml_psm.channels.first().digitalInput.at(i));
    }
    for (int i = 0; i < xml_psm.channels.first().digitalOutput.count(); i++) {
        loadDigitalChannels(psm, xml_psm.channels.first().digitalOutput.at(i));
      }
    for (int i = 0; i < xml_psm.channels.first().analogueInput.count(); i++) {
      loadAnalogChannels(psm, xml_psm.channels.first().analogueInput.at(i));
    }
  }

  private void loadSCM(SCMModuleT xml, ModuleSCM module)
      throws InvalidConfException {

    module.setId(convertModuleID(xml.moduleID.getValue()));

    LED_COLOUR ledColor = LED_COLOUR.valueOf(xml.openColour.getValue());
    module.getSettings().setOpenColour(ledColor);
    module.getSettings().setAllowForcedOperation(xml.allowForcedOperation.getValue());

    // load digital channels
    for (int j = 0; j < xml.channels.first().digitalInput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalInput.at(j));
    }
    // load analogue channels
    for (int j = 0; j < xml.channels.first().analogueInput.count(); j++) {
      loadAnalogChannels(module, xml.channels.first().analogueInput.at(j));
    }
    // load switch output channels
    for (int j = 0; j < xml.channels.first().switchOut.count(); j++) {
      loadSwitchOutputChannels(module, xml.channels.first().switchOut.at(j));
    }

    // load digital output channels
    for (int j = 0; j < xml.channels.first().digitalOutput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalOutput.at(j));
    }
  }
  
  private void loadSCMMK2(SCMMK2ModuleT xml, ModuleSCM_MK2 module)
      throws InvalidConfException {
    
    module.setId(convertModuleID(xml.moduleID.getValue()));
    
    LED_COLOUR ledColor = LED_COLOUR.valueOf(xml.openColour.getValue());
    module.getSettings().setOpenColour(ledColor);
    module.getSettings().setMotorMode(MOTOR_MODE.forValue((int) xml.motorMode.getValue()));
    module.getSettings().setAllowForcedOperation(xml.allowForcedOperation.getValue());
    
    // load digital channels
    for (int j = 0; j < xml.channels.first().digitalInput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalInput.at(j));
    }
    // load analogue channels
    for (int j = 0; j < xml.channels.first().analogueInput.count(); j++) {
      loadAnalogChannels(module, xml.channels.first().analogueInput.at(j));
    }
    // load switch output channels
    for (int j = 0; j < xml.channels.first().switchOut.count(); j++) {
      loadSwitchOutputChannels(module, xml.channels.first().switchOut.at(j));
    }
    
    // load digital output channels
    for (int j = 0; j < xml.channels.first().digitalOutput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalOutput.at(j));
    }
  }

  private void loadDSM(DSMModuleT xml, ModuleDSM module)
      throws InvalidConfException {
    module.setId(convertModuleID(xml.moduleID.getValue()));

    if (xml.polarityA.exists()) {
      module.getSettings().setPolarityA(Polarity.forValue((int) xml.polarityA.getValue()));
    } else {
      log.warn("polarityA not found for " + module);
    }

    if (xml.polarityB.exists()) {
      module.getSettings().setPolarityB(Polarity.forValue((int) xml.polarityB.getValue()));
    } else {
      log.warn("polarityB not found for " + module);
    }

    if (xml.supplyEnable24V.exists()) {
      module.getSettings().setSupplyEnable24V(xml.supplyEnable24V.getValue());
    }

    LED_COLOUR ledColor = LED_COLOUR.valueOf(xml.openColour.getValue());
    module.getSettings().setOpenColour(ledColor);
    module.getSettings().setAllowForcedOperationA(xml.allowForcedOperationA.getValue());
    module.getSettings().setAllowForcedOperationB(xml.allowForcedOperationB.getValue());

    // load digital channels
    for (int j = 0; j < xml.channels.first().digitalInput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalInput.at(j));
    }
    // load analogue channels
    for (int j = 0; j < xml.channels.first().analogueInput.count(); j++) {
      loadAnalogChannels(module, xml.channels.first().analogueInput.at(j));
    }
    // load switch output channels
    for (int j = 0; j < xml.channels.first().switchOut.count(); j++) {
      loadSwitchOutputChannels(module, xml.channels.first().switchOut.at(j));
    }
    // load digital output channels
    for (int j = 0; j < xml.channels.first().digitalOutput.count(); j++) {
      loadDigitalChannels(module, xml.channels.first().digitalOutput.at(j));
    }
  }

  private void loadDigitalChannels(IOModule module,
      DigitalInputChnlT xmlch) throws InvalidConfException {
    int channelID = (int) xmlch.channelID.getValue();
    IChannel channel = module.getChByTyeAndID(channelID, ChannelType.DIGITAL_INPUT);

    if (channel == null) {
      String message = "Cannot load Digital Channel [ID: "
          + channelID
          + "] cause it is not found in :" + module.getShortName();
      log.error(message);
      return;
    }

    try {
      setChParam(channel, ICANChannel.PARAM_DBHIGH2LOWMS, xmlch.dbHigh2LowMs.getValue());
      setChParam(channel, ICANChannel.PARAM_DBLOW2HIGHMS, xmlch.dbLow2HighMs.getValue());
      setChParam(channel, ICANChannel.PARAM_EVENTENABLED, xmlch.eventEnable.getValue());

      if (channel.hasParameter(ICANChannel.PARAM_EXTEQUITPINVERT)) {
        setChParam(channel, ICANChannel.PARAM_EXTEQUITPINVERT, xmlch.extEquipInvert.getValue());
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new InvalidConfException("Fail to set channel parameter: " + e.getMessage());
    }
  }

  private void loadDigitalChannels(IOModule module,
      DigitalOutputChnlT xmlch) throws InvalidConfException {
    int channelID = (int) xmlch.channelID.getValue();
    IChannel channel = module.getChByTyeAndID(channelID, ChannelType.DIGITAL_OUTPUT);

    if (channel == null) {
      String message = "Cannot configure Digital Channel [ID: "
          + channelID
          + "] cause it is not found in Module:" + module.getFullName();
      log.error(message);
      return;
    }

    try {
      setChParam(channel, ICANChannel.PARAM_PULSELENGTH, xmlch.pulseLength.getValue());
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new InvalidConfException("Fail to set channel parameter: " + e.getMessage());
    }
  }

  private void setChParam(IChannel ch, String param, Object value) {
    if (ch.isParameterModifiable(param)) {
      ch.setParameter(param, value);
    }
  }

  private void loadAnalogChannels(IOModule module,
      AnalogueInputChnlT xmlch) throws InvalidConfException {
    int channelID = (int) xmlch.channelID.getValue();
    IChannel channel = module.getChByTyeAndID(channelID, ChannelType.ANALOG_INPUT);

    if (channel == null) {
      String message = "Cannot configure Analogue Channel [ID: "
          + channelID
          + "] cause it is not found in " + module.getFullName();
      log.error(message);
      return;
    }

    try {
      setChParam(channel, ICANChannel.PARAM_EVENT_MS, xmlch.eventMs.getValue());
      setChParam(channel, ICANChannel.PARAM_EVENTENABLED, xmlch.eventEnable.getValue());
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new InvalidConfException("Fail to set channel parameter: " + e.getMessage());
    }
  }

  private void loadSwitchOutputChannels(IOModule module,
      SwitchOutChnlT xmlch) throws InvalidConfException {

    int channelID = (int) xmlch.channelID.getValue();
    IChannel channel = module.getChByTyeAndID(channelID, ChannelType.SWITCH_OUT);

    if (channel == null) {
      String message = "Cannot configure Switch output Channel [ID: "
          + channelID
          + "] cause it is not found in " + module.getFullName();
      log.error(message);
      return;
    }

    try {
      if (channel.hasParameter(ICANChannel.PARAM_POLARITY)&& xmlch.polarity.exists()) 
        setChParam(channel, ICANChannel.PARAM_POLARITY, Polarity.forValue((int) xmlch.polarity.getValue()));
      
      if (channel.hasParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS)&& xmlch.motorOverDriveMS.exists()) 
        setChParam(channel, ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS,  xmlch.motorOverDriveMS.getValue());
      
      if (channel.hasParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS)&& xmlch.motorReverseDriveMS.exists()) 
        setChParam(channel, ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS,  xmlch.motorReverseDriveMS.getValue());

      if (channel.hasParameter(ICANChannel.PARAM_PULSELENGTH)&& xmlch.pulseLength.exists()) 
        setChParam(channel, ICANChannel.PARAM_PULSELENGTH, xmlch.pulseLength.getValue());
      
      if (channel.hasParameter(ICANChannel.PARAM_OVERRUN_MS)&& xmlch.overrunMS.exists())
        setChParam(channel, ICANChannel.PARAM_OVERRUN_MS, xmlch.overrunMS.getValue());
      
      if(channel.hasParameter(ICANChannel.PARAM_OPTIMEOUT_SEC) && xmlch.opTimeoutS.exists())
        setChParam(channel, ICANChannel.PARAM_OPTIMEOUT_SEC, xmlch.opTimeoutS.getValue());
    } catch (Exception e) {
      log.error(e.getMessage());
      throw new InvalidConfException("Fail to set channel parameter: " + e.getMessage());
    }
    // Inhibit object
    Inhibit inhibit = (Inhibit) channel.getParameter(ICANChannel.PARAM_INHIBI);
    inhibit.setInhibitMask(xmlch.inhibit.getValue());
  }

  private static MODULE_ID convertModuleID(String idValue) {
    MODULE_ID id = MODULE_ID.valueOf(idValue);

    if (id == null) {
      assert false : "Invalid module id:" + idValue;
    }
    return id;
  }
}
