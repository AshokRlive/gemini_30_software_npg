/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.math.BigDecimal;
import java.util.Collection;

import com.g3schema.ns_vpoint.CustomLabelEntryT;
import com.g3schema.ns_vpoint.CustomLabelSetT;
import com.g3schema.ns_vpoint.CustomLabelsT;
import com.g3schema.ns_vpoint.VirtualPointsT;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabel;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSetManager;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdBinaryPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdCounterPoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdDoubleBinaryPoint;

/**
 * XML writer for virtual point.
 */
class VirtualPointWriter extends AbstractXmlWriter {

  VirtualPointWriter(IConfig data, ValidationResult result) {
    super(data, result);
  }

  void write(VirtualPointsT xml) {
    // Export analogue points
    Collection<VirtualPoint> points = getVirtualPointManager()
        .getAllPointsByType(VirtualPointType.ANALOGUE_INPUT);

    for (VirtualPoint p : points) {
      if (p instanceof StdAnaloguePoint) {
        if (!xml.analogues.exists()) {
          xml.analogues.append();
        }
        ((StdAnaloguePoint) p).writeToXML(xml.analogues.first().analogue.append(), this);
      }
    }

    // Export binary points
    points = getVirtualPointManager().getAllPointsByType(VirtualPointType.BINARY_INPUT);
    for (VirtualPoint p : points) {
      if (p instanceof StdBinaryPoint) {
        if (!xml.binaries.exists()) {
          xml.binaries.append();
        }
        ((StdBinaryPoint) p).writeToXML(xml.binaries.first().binary.append(), this);
      }
    }

    // Export double binary points
    points = getVirtualPointManager().getAllPointsByType(VirtualPointType.DOUBLE_BINARY_INPUT);
    for (VirtualPoint p : points) {
      if (p instanceof StdDoubleBinaryPoint) {
        if (!xml.doubleBinaries.exists()) {
          xml.doubleBinaries.append();
        }
        ((StdDoubleBinaryPoint) p).writeToXML(xml.doubleBinaries.first().doubleBinary.append(), this);
      }
    }

    // Export counter points
    points = getVirtualPointManager().getAllPointsByType(VirtualPointType.COUNTER);
    for (VirtualPoint p : points) {
      if (!xml.counters.exists()) {
        xml.counters.append();
      }
      if (p instanceof StdCounterPoint) {
        ((StdCounterPoint) p).writeToXML(xml.counters.first().counter.append(), this);
      }
    }

    writeLabelSets(xml.customLabels.append());
  }
  
  private VirtualPointManager getVirtualPointManager() {
    return data.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
  }
  
  private void writeLabelSets(CustomLabelsT xml) {
    ValueLabelSetManager labelMgr = getVirtualPointManager().getValueLabelManager();
    Collection<ValueLabelSet> all = labelMgr.getAllLabelSets();
    for (ValueLabelSet set : all) {
      CustomLabelSetT xmlSet = xml.labelSet.append();
      xmlSet.name.setValue(set.getId());
      Collection<ValueLabel> entries = set.getAllLabelEntries();
      for (ValueLabel entry : entries) {
        CustomLabelEntryT xmlEntry = xmlSet.labelEntry.append();
        xmlEntry.label.setValue(entry.getLabel());
        xmlEntry.value2.setValue(BigDecimal.valueOf(entry.getValue().doubleValue()));
      }
    }

  }

}
