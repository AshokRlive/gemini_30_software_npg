/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import org.netbeans.spi.wizard.ResultProgressHandle;

/**
 * Adapts {@linkplain ResultProgressHandle} to {@linkplain XmlParserInvoker}.
 */
public class ProgressHandlerAdapter implements XmlParserInvoker {

  private final ResultProgressHandle progHandler;


  public ProgressHandlerAdapter(ResultProgressHandle progHandler) {
    this.progHandler = progHandler;
  }

  @Override
  public void printMessage(String message) {
    progHandler.setBusy(message);
  }

  @Override
  public void setProgressValue(int progress) {
    progHandler.setProgress(progress, TOTAL_PROGRESS);
  }
}
