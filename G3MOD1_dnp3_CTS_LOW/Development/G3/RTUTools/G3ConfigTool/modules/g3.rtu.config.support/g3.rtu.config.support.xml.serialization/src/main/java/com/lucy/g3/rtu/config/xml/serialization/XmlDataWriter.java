/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.serialization;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import com.g3schema.ConfigVersionT;
import com.g3schema.ConfigurationT;
import com.g3schema.HeaderT;
import com.g3schema.SecurityPolicyT;
import com.g3schema.ToolVersionT;
import com.g3schema.UserAccountT;
import com.g3schema.UserSettingT;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.validation.ValidationResult;
import com.lucy.g3.common.manifest.ManifestInfo;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.device.comms.domain.CommsDeviceManager;
import com.lucy.g3.rtu.config.device.comms.xml.CommsDeviceWriter;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.device.fields.xml.FieldDeviceWriter;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.misc.MiscConfig;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.xml.PortWriter;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.integrated.CustomTemplates;
import com.lucy.g3.rtu.config.user.UserConfig;
import com.lucy.g3.rtu.config.user.UserManager;
import com.lucy.g3.security.support.SecurityUtils;
import com.lucy.g3.xml.gen.common.versions;

/**
 * Write the content of IConfig to XML.
 */
public class XmlDataWriter {

  /**
   * Charset for encoding string into raw bytes which will be written into XML.
   */
  public static final String STRING_CHARSET = "UTF-8";

  private IConfig data;

  private com.g3schema.g3schema2 doc;

  private XmlParserInvoker invoker;

  private final ValidationResult result = new ValidationResult();


  public XmlDataWriter(IConfig data, XmlParserInvoker invoker) {
    if (data == null) {
      throw new IllegalArgumentException("IConfig is null");
    }

    this.invoker = invoker;

    this.data = data;

  }

  public ValidationResult getResult() {
    return result;
  }

  /**
   * Write the configData to a file.
   */
  public void saveToFile(final String targetFile) throws Exception {
    Preconditions.checkNotNull(targetFile, "target file must not be null");

    doc = createDocument();

    ConfigurationT config = doc.configuration.first();

    // ====== General configuration ======
    exportGeneral(config);

    // ================== Doc Elements ===================
    printMsg("Export modules...");
    new CANModuleWriter(data, result).write(config.modules.append());

    printMsg("Export standard virtual points...");
    new VirtualPointWriter(data, result).write(config.virtualPoints.append());

    printMsg("Export control logic...");
    CLogicManager clogicMgr = (CLogicManager)data.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    clogicMgr.writeToXML(config.controlLogic.append(), result);

    printMsg("Export ports...");
    PortsManager portMgr = (PortsManager)data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    new PortWriter(portMgr, result).write(config.ports.append());

    printMsg("Export SCADA protocol...");
    new ScadaProtoWriter(data, result).write(config.scadaProtocol.append());

    printMsg("Export user accounts...");
    exportUserSetting(config.userSetting.append());

    printMsg("Export field devices...");
    FieldDeviceManager fieldDevMgr = (FieldDeviceManager) data.getConfigModule(FieldDeviceManager.CONFIG_MODULE_ID);
    new FieldDeviceWriter(fieldDevMgr).write(config.fieldDevices.append(), result);

    printMsg("Export comms devices...");
    CommsDeviceManager commsMgr = (CommsDeviceManager)data.getConfigModule(CommsDeviceManager.CONFIG_MODULE_ID);
    new CommsDeviceWriter(commsMgr, result).write(config.commsDevices.append());

    printMsg("Export miscellaneous...");
    MiscConfig misc =  data.getConfigModule(MiscConfig .CONFIG_MODULE_ID);
    misc.writeToXML(config.miscellaneous.append());


    printMsg("Export template...");
    CustomTemplates template = (CustomTemplates) data.getConfigModule(CustomTemplates.CONFIG_MODULE_ID);
    template.writeToXML(config.template.append());

    printMsg("Creating file...");

    if (targetFile.endsWith(G3Files.SUFFFIX_GZIP)) {
      /* Create compressed configuration file*/
      String xmlFile = getFileNameWithoutSuffix(targetFile, G3Files.SUFFFIX_GZIP) + G3Files.SUFFFIX_XML;
      doc.saveToFile(xmlFile, true);
      ZipUtil.gzip(xmlFile, targetFile);

    } else {
      /* Create XML configuration file*/
      doc.saveToFile(targetFile, true);
    }

    doc = null;
    invoker = null;

    if (result.hasErrors()) {
      XmlErrorReporter.showErrorReportDialog(result);
    }
  }

  private void exportGeneral(ConfigurationT config) throws UnsupportedEncodingException {
    GeneralConfig general = data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
    // Export site name as raw binary text cause RTU doesn't support non-euro
    // character set
    config.siteName.setValue(SecurityUtils.encodeStringToHex(general.getSiteName(), STRING_CHARSET));

    // Export configuration description
    config.configFileDescription.setValue(general.getConfigDescription());

    // Export configuration version
    if(!Strings.isBlank(general.getConfigVersion())) {
      config.configVersionMajor.setValue(general.getConfigVersionMajor());
      config.configVersionMinor.setValue(general.getConfigVersionMinor());
      config.configVersionPatch.setValue(general.getConfigVersionPatch());
    }
    
    if(!Strings.isBlank(general.getConfigReleaseType())) {
      config.configReleaseType.setValue(general.getConfigReleaseType());
    }
    
    // Export current date to configuration modification date
    DateFormat format = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.MEDIUM);
    String modificationDate = format.format(Calendar.getInstance().getTime());
    config.modificationDate.setValue(modificationDate);
    
    HeaderT xmlHeader = config.header.append();
    ConfigVersionT xmlConfig = xmlHeader.configVersion.append();
    ToolVersionT xmlTool = xmlHeader.toolVersion.append();
    xmlConfig.configVersionMajor.setValue(general.getConfigVersionMajor());
    xmlConfig.configVersionMinor.setValue(general.getConfigVersionMinor());
    xmlConfig.configVersionPatch.setValue(general.getConfigVersionPatch());
    xmlConfig.configReleaseType.setValue(general.getConfigReleaseType());
    xmlConfig.configFileDescription.setValue(general.getConfigDescription());
    xmlConfig.modificationDate.setValue(modificationDate);

    ManifestInfo ver = new ManifestInfo("");
    xmlTool.version.setValue(ver.getVersion());
    xmlTool.revision.setValue(ver.getSVNRev());
    
  }

  private static String getFileNameWithoutSuffix(String filename, String suffix) {
    if (filename.endsWith(suffix) && filename.length() > suffix.length()) {
      return filename.substring(0, filename.length() - suffix.length());
    } else {
      return filename;
    }
  }

  private void printMsg(String msg) {
    if (invoker != null) {
      invoker.printMessage(msg);
    }
  }

  private com.g3schema.g3schema2 createDocument() throws Exception {
    com.g3schema.g3schema2 document = com.g3schema.g3schema2.createDocument();
    ConfigurationT root = document.configuration.append();
    document.setSchemaLocation("g3schema.xsd");
    root.schemaVersionMajor.setValue(versions.SCHEMA_MAJOR);
    root.schemaVersionMinor.setValue(versions.SCHEMA_MINOR);

    return document;
  }

  private void exportUserSetting(UserSettingT xml_userSetting) {
    UserManager manager =  data.getConfigModule(UserManager.CONFIG_MODULE_ID);
    
    SecurityPolicyT xml_security = xml_userSetting.securityPolicy.append();
    xml_security.accountLockoutDurationSecs.setValue(manager.getAccountLockoutDurationSecs());
    xml_security.invalidLogonAttemptsThreshold.setValue(manager.getInvalidLogonAttemptsThreshold());
    
    List<UserConfig> userList = manager.getUserList();
    for (UserConfig user : userList) {  
      if (user != null) {
        UserAccountT xml_node = xml_userSetting.userAccounts.append();
        xml_node.name.setValue(user.getName());
        xml_node.mdAlgorithm.setValue(user.getAlgorithm());
        xml_node.password.setValue(user.getPasswordHash());
        xml_node.group.setValue(user.getRole().getValue());
        String pin = user.getPIN();
        if(!Strings.isBlank(pin))
          xml_node.pin.setValue(pin);
      }
    }
  }
}
