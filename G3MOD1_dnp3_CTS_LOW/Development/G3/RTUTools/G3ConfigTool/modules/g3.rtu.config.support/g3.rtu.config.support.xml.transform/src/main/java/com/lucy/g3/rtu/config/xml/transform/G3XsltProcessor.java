/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.transform;

import java.io.InputStream;
import java.io.OutputStream;

import com.lucy.g3.xml.transform.XsltProcessor;


/**
 *
 */
public class G3XsltProcessor {

  /** XSLT file for printing G3 Configuration. */
  private static final String PRINT_XSLT_FILE = "xslt/G3Config_print.xsl";
  
  public static void transfomrConfigToHtml(InputStream g3ConfigInputStream, OutputStream htmlOutputStream) throws Exception {
    InputStream xslsheet = XsltProcessor.class.getClassLoader().getResourceAsStream(PRINT_XSLT_FILE);
    XsltProcessor.transform(xslsheet, g3ConfigInputStream, htmlOutputStream);
  }
}

