<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:dnp3="DNP3Enum"
   xmlns:g3mod="ModuleProtocolEnum" 
   xmlns:mcm="MCMConfigEnum" 
   xmlns:g3="http://www.lucyswitchgear.com/g3RTUConfiguration" 
    exclude-result-prefixes="xs"
 >
 <!--<xsl:import-schema schema-location="g3schema.xsd"></xsl:import-schema>-->
 <!-- xpath-default-namespace="http://www.lucyswitchgear.com/g3RTUConfiguration"-->
<xsl:strip-space elements="*"/>

<xsl:output method="html" indent="yes"/>
 <xsl:template match="/">
  <xsl:call-template name="root"/>
 </xsl:template>
<xsl:template name="root" match="configuration">
  <html>
     <head>
    <title>Three Real Tables</title>
    <style type="text/css">
    #

	{
	font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
	width:100%;
	border-collapse:collapse;
	}
    body
	{
	background-color:white;
	}
	h1
	{
	color:black;
	text-align:center;
	}
		h3
	{
	padding-top:25px;
	padding-bottom:0px;
	color:#4863A0;
	text-align:left;
	}
	
	table, th, td
	{
	border-collapse:collapse;
	border: 1px solid black;
	}
	th
	{
	background-color:#1569C7;
	color:white;
	}
	caption
	{
	caption-side:top;
	}
	
	
     .tname {font-family:Tahoma;font-size:14pt;font-weight:bold}
     .tdesc {font-family:Tahoma;font-size:10pt}
     .tsaletxt {font-family:Tahoma;font-size:14pt;font-weight:bold;color:gray;}
     .tprice {font-family:Tahoma;font-size:18pt;font-weight:bold;color:red;text-align:center}
    </style>
   </head>
   <body> 		
	<!--<INPUT TYPE="button" value="Print" onClick="window.print()"/>-->
	<div>
	 <h1>Gemini3 RTU Configuration</h1>
	<ul>
	<li>Site Name: <xsl:value-of select="/g3:configuration/@siteName"/></li>
	<li>Description: <xsl:value-of select="/g3:configuration/@configFileDescription"/></li>
	<li>Schema Version: <xsl:value-of select="/g3:configuration/@schemaVersionMajor"/>.<xsl:value-of select="/g3:configuration/@schemaVersionMinor"/></li>
	</ul>
	</div>
	 <xsl:apply-templates select="/g3:configuration"/>
    
   </body>
  </html>
 </xsl:template>
 
 <xsl:template match="modules">
 <h3>Modules[<xsl:value-of select="count(./*)"></xsl:value-of>]</h3>
 <xsl:apply-templates />
 </xsl:template>
 
 <xsl:template match="PSM|MCM|SCM|FDM|HMI|IOM">
   <h4>Module-<xsl:value-of select="name(.)"/></h4>
   <dl>
	   	<xsl:for-each select="@*">
		<dt><xsl:value-of select="name(.)"/></dt>
		<dd><xsl:value-of select="."/> </dd>
	 	</xsl:for-each>
	</dl>	 	
 
<xsl:if test="count(channels/digitalInput) > 0">
<p/>
 <table >
 <caption>Digital Channel</caption>
		<tbody>
		<tr>		
		<th>ChannelID			
		</th>
		<th>
			<span>
				<xsl:text>extEquipInvert</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>eventEnable</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>dbHigh2LowMs</xsl:text>
			</span>
		</th>
		<th>
				<span>
				<xsl:text>dbLow2HighMs</xsl:text>
			</span>
		</th>
		</tr>
		
		<xsl:for-each select="channels/digitalInput">
		<tr >
			<td>			
					<span>
						<xsl:value-of select="@channelID"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="@extEquipInvert"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="@eventEnable"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="@dbLow2HighMs"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="@dbHigh2LowMs"/>
					</span>				
			</td>
								
			</tr>
		</xsl:for-each>
		</tbody>
	</table>
   </xsl:if>
	
	<xsl:if test="count(channels/analogueInput)>0">
	<p/>
	<table  >
	<caption>Analog Channel</caption>
		<tbody>
		<tr>		
		<th>
			<span>
				<xsl:text>channelID</xsl:text>
			</span>
		</th>		
		<th>
			<span>
				<xsl:text>eventMs</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>eventEnable</xsl:text>
			</span>
		</th>
		</tr>
	<xsl:for-each select="channels/analogueInput">
		<tr>
			<td>			
					<span>
						<xsl:value-of select="@channelID"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="@eventMs"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="@eventEnable"/>
					</span>				
			</td>		
			</tr>
		</xsl:for-each>
		</tbody>
	</table>
</xsl:if>
 
		
</xsl:template>

<xsl:template match="virtualPoints">
  <h3>Virtual Points</h3>
<xsl:if test="count(./analogue) > 0"><!--Output analogue virtual points-->
 <table  >
	<caption><xsl:value-of select="count(./analogue)"/> Analogue Virtual Points</caption>
		<tbody>
		<tr>		
		<th>
			<span>
				<xsl:text>[Group,ID]</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Description</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Filter</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Scale</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Offset</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Overflow</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Hysteresis</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Mapped Channel</xsl:text>
			</span>
		</th>
		</tr>
	<xsl:for-each select="./analogue">
		<tr>
			<td>			
					<span>
						[<xsl:value-of select="@group"/>,<xsl:value-of select="@id"/>]
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./description"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="name(./filter/*)"/>
						<xsl:for-each select="./filter/*/@*">
						;<xsl:value-of select="name(.)"/>:<xsl:value-of select="."/>
						</xsl:for-each>
					</span>
			</td>
	  		<td>			
					<span>
						<xsl:value-of select="./scale"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./offset"/>
					</span>				
			</td>	
				<td>			
					<span>
						<xsl:value-of select="./overflow/@overflow"/>
					</span>				
			</td>	
			<td>			
					<span>
						<xsl:value-of select="./overflow/@hysteresis"/>
					</span>				
			</td>		
			<td>			
					<span>
						<xsl:value-of select="substring(./channel/@moduleType,string-length('MODULE_X'))"/><xsl:value-of select="substring(./channel/@moduleID,string-length('MODULE_ID_X'))"/> -> Channel:<xsl:value-of select="./channel/@channelID"/>
					</span>				
			</td>		
			</tr>
		</xsl:for-each>
		</tbody>
	</table>
<xsl:if test="count(./binary) > 0"><!--Output binary virtual points-->	
<br/>
 <table  >
	 	<caption><xsl:value-of select="count(./binary)"/> Binary Virtual Points</caption>
		<tbody>
		<tr>		
		<th>
			<span>
				<xsl:text>[Group,ID]</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Description</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Invert</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Chatter No</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Chatter Time</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Mapped Channel</xsl:text>
			</span>
		</th>
		</tr>
	<xsl:for-each select="./binary"> 
		<tr>
			<td>			
					<span>
						[<xsl:value-of select="@group"/>,<xsl:value-of select="@id"/>]
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./description"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./invert"/>
					</span>
			</td>
	  		<td>			
					<span>
						<xsl:value-of select="./chatterNo"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./chatterTime"/>
					</span>				
			</td>	
			<td>			
					<span>
						<xsl:value-of select="substring(./channel/@moduleType,string-length('MODULE_X'))"/><xsl:value-of select="substring(./channel/@moduleID,string-length('MODULE_ID_X'))"/> -> Channel:<xsl:value-of select="./channel/@channelID"/>
					</span>				
			</td>		
			</tr>
		</xsl:for-each>
		</tbody>
	</table>
	</xsl:if>
<xsl:if test="count(./doubleBinary) > 0"><!--Output double binary virtual points-->	
<br/>
 <table  >
	 	<caption><xsl:value-of select="count(./doubleBinary)"/> Double Binary Virtual Points</caption>
		<tbody>
		<tr>		
		<th>
			<span>
				<xsl:text>[Group,ID]</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Description</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Invert</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Chatter No</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Chatter Time</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Debounce</xsl:text>
			</span>
		</th>
		<th>
			<span>
				<xsl:text>Mapped Channel</xsl:text>
			</span>
		</th>
		</tr>
	<xsl:for-each select="./doubleBinary"> 
		<tr>
			<td>			
					<span>
						[<xsl:value-of select="@group"/>,<xsl:value-of select="@id"/>]
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./description"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./invert"/>
					</span>
			</td>
	  		<td>			
					<span>
						<xsl:value-of select="./chatterNo"/>
					</span>				
			</td>
			<td>			
					<span>
						<xsl:value-of select="./chatterTime"/>
					</span>				
			</td>
			<td>			
			<span>
				<xsl:value-of select="./debounce"/>
			</span>				
			</td>	
			<td>			
					<span>
						<xsl:value-of select="substring(./channel0/@moduleType,string-length('MODULE_X'))"/><xsl:value-of select="substring(./channel0/@moduleID,string-length('MODULE_ID_X'))"/> -> Channel:<xsl:value-of select="./channel0/@channelID"/><br/>
					</span>
					<span>
						<xsl:value-of select="substring(./channel1/@moduleType,string-length('MODULE_X'))"/><xsl:value-of select="substring(./channel1/@moduleID,string-length('MODULE_ID_X'))"/> -> Channel:<xsl:value-of select="./channel1/@channelID"/>
					</span>				
			</td>		
			</tr>
		</xsl:for-each>
		</tbody>
	</table>
	</xsl:if>	
</xsl:if>
</xsl:template>
<xsl:template match="controlLogic">
<xsl:if test="count(./*) > 0"><!--Output control logic list-->	
 <h3>Control Logic</h3>
<xsl:for-each select="./*">
<li>[Group <xsl:value-of select="@group"/>]<xsl:value-of select="@name"/></li>
	<!--Output Parameters<xsl:for-each select="./@*[not(name(.)='group')][not(name(.)='name')]">
		<span><br><xsl:value-of select="name(.)"/>:<xsl:value-of select="."/></span>
	</xsl:for-each>-->
</xsl:for-each>
</xsl:if>	
</xsl:template>
<xsl:template match="network">
</xsl:template>
<xsl:template match="protocolStack">
 <h3>Protocol Stack</h3>
<xsl:if test="count(./DNP3Config) >0">
<h5>DNP3 Configuration</h5>
 <table >
  <caption>DNP Link Configuration</caption> 
	   <tr>
	 	<xsl:for-each select="./DNP3Config/dnpLinkConfig/@*">
		<th><xsl:value-of select="name(.)"/></th>
	 	</xsl:for-each>
	 	</tr>
	 	<tr>
	 	<xsl:for-each select="./DNP3Config/dnpLinkConfig/@*">
	 	<td><xsl:value-of select="."/> <span/></td>
	 	</xsl:for-each>
	 	</tr>
</table>
<br/>
 <table >
<caption>TCP/IPConfiguration</caption> 
	   <tr>
	 	<xsl:for-each select="./DNP3Config/tcpipConfig/@*">
		<th><xsl:value-of select="name(.)"/></th>
	 	</xsl:for-each>
	 	</tr>
	 	<tr>
	 	<xsl:for-each select="./DNP3Config/tcpipConfig/@*">
	 	<td><xsl:value-of select="."/> <span/></td>
	 	</xsl:for-each>
	 	</tr>
</table>
<br/>
<table >
<caption>Serial Configuration</caption> 
	   <tr>
	 	<xsl:for-each select="./DNP3Config/serialConfig/@*">
		<th><xsl:value-of select="name(.)"/></th>
	 	</xsl:for-each>
	 	</tr>
	 	<tr>
	 	<xsl:for-each select="./DNP3Config/serialConfig/@*">
	 	<td><xsl:value-of select="."/> <span/></td>
	 	</xsl:for-each>
	 	</tr>
</table>
<br/>
<table >
<caption>DNP Session Configuration</caption> 
	   <tr>
	 	<xsl:for-each select="./DNP3Config/sDNPSessionConfig/@*">
		<th><xsl:value-of select="name(.)"/></th>
	 	</xsl:for-each>
	 	</tr>
	 	<tr>
	 	<xsl:for-each select="./DNP3Config/sDNPSessionConfig/@*">
	 	<td><xsl:value-of select="."/> <span/></td>
	 	</xsl:for-each>
	 	</tr>
</table>
<br/>
<xsl:if test="count(./DNP3Config/DNPPoints/analogInput) > 0">
<table >
<caption>DNP3 Analogue Input Points</caption> 
	   <tr>
	 	<th>Protocol ID</th><th>Description</th><th>Enabled</th><th>Default Variation</th><th>Event Default Variation</th><th>Event Class</th><th>EnableClass0</th><th>Mapped Virtual Point</th>
	 	</tr>
	 	<xsl:for-each select="./DNP3Config/DNPPoints/analogInput">
	 	<tr>
	 	<td><xsl:value-of select="./@protocolID"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@description"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@enable"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@defaultVariation"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@eventDefaultVariation"></xsl:value-of></td>
		<td><xsl:value-of select="./@eventClass"></xsl:value-of></td>
		<td><xsl:value-of select="./@enableClass0"></xsl:value-of></td>
		<td>[<xsl:value-of select="./vpoint/@pointGroup"/>,<xsl:value-of select="./vpoint/@pointID"/>]</td>
		</tr>
	 	</xsl:for-each>
</table>
</xsl:if>
<xsl:if test="count(./DNP3Config/DNPPoints/binaryInput) > 0">
<table >
<caption>DNP3 Binary Input Points</caption> 
	   <tr>
	 	<th>Protocol ID</th><th>Description</th><th>Enabled</th><th>Default Variation</th><th>Event Default Variation</th><th>Event Class</th><th>EnableClass0</th><th>Mapped Virtual Point</th>
	 	</tr>
	 	<xsl:for-each select="./DNP3Config/DNPPoints/binaryInput">
	 	<tr>
	 	<td><xsl:value-of select="./@protocolID"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@description"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@enable"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@defaultVariation"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@eventDefaultVariation"></xsl:value-of></td>
		<td><xsl:value-of select="./@eventClass"></xsl:value-of></td>
		<td><xsl:value-of select="./@enableClass0"></xsl:value-of></td>
		<td>[<xsl:value-of select="./vpoint/@pointGroup"/>,<xsl:value-of select="./vpoint/@pointID"/>]</td>
		</tr>
	 	</xsl:for-each>
</table>
</xsl:if>
<xsl:if test="count(./DNP3Config/DNPPoints/doubleBinaryInput) > 0">
<table >
<caption>DNP3 Dobule Binary Input Points</caption> 
	   <tr>
	 	<th>Protocol ID</th><th>Description</th><th>Enabled</th><th>Default Variation</th><th>Event Default Variation</th><th>Event Class</th><th>EnableClass0</th><th>Mapped Virtual Point</th>
	 	</tr>
	 	<xsl:for-each select="./DNP3Config/DNPPoints/doubleBinaryInput">
	 	<tr>
	 	<td><xsl:value-of select="./@protocolID"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@description"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@enable"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@defaultVariation"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@eventDefaultVariation"></xsl:value-of></td>
		<td><xsl:value-of select="./@eventClass"></xsl:value-of></td>
		<td><xsl:value-of select="./@enableClass0"></xsl:value-of></td>
		<td>[<xsl:value-of select="./vpoint/@pointGroup"/>,<xsl:value-of select="./vpoint/@pointID"/>]</td>
		</tr>
	 	</xsl:for-each>
</table>
</xsl:if>
<xsl:if test="count(./DNP3Config/DNPPoints/binaryOutput) > 0">
<table>
<caption>DNP3 Binary Output Points</caption> 
	   <tr>
	 	<th>Protocol ID</th><th>Description</th><th>Enabled</th><th>EnableClass0</th><th>Output Control Logic Group</th>
	 	</tr>
	 	<xsl:for-each select="./DNP3Config/DNPPoints/binaryOutput">
	 	<tr>
	 	<td><xsl:value-of select="./@protocolID"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@description"></xsl:value-of></td>
	 	<td><xsl:value-of select="./@enable"></xsl:value-of></td>
		<td><xsl:value-of select="./@enableClass0"></xsl:value-of></td>
		<td><xsl:value-of select="./@controlLogicGroup"></xsl:value-of></td>
		</tr>
	 	
	 	</xsl:for-each>
	 	
</table>
</xsl:if>
</xsl:if>

</xsl:template>
<xsl:template match="logSetting">
</xsl:template>
<xsl:template match="userSetting">
</xsl:template>
<!--<xsl:template match="element(*, ModulePSMType)">
<xsl:variable name="date-value" select="data(.)"/>
<xsl:choose>
<xsl:when test="$date-value instance of xs:date">
<xsl:value-of select="format-date($date-value, ’[D] [MNn] [Y]’)"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="$date-value"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>-->
</xsl:stylesheet>


