/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.xml.transform;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.xml.transform.XsltProcessor;


/**
 * The Class G3ConfigUpgradeTest.
 */
public class G3XsltProcessorTest {
//  private final static String XSLT_FILE  = "ConfigUpdate/transform_6_to_7.xslt";
  private final static String XSLT_FILE  = "ConfigUpdate/test01.xsl";
  private final static String XML_INPUT  = "ConfigUpdate/G3Config_6.xml";
  private final static String XML_OUTPUT = "target/G3XsltProcessorTest/G3Config_7.xml";
  
  @Before
  public void setUp() throws Exception {
    new File(XML_OUTPUT).getParentFile().mkdirs();
  }

  @After
  public void tearDown() throws Exception {
  }

  //@Ignore // Failed 
  @Test
  public void testUpgradeG3Config() throws Exception {
    ClassLoader cl = XsltProcessor.class.getClassLoader();
    
    InputStream xslsheet = cl.getResourceAsStream(XSLT_FILE);
    InputStream input = cl.getResourceAsStream(XML_INPUT);
    FileOutputStream output = new FileOutputStream(XML_OUTPUT);
    
    XsltProcessor.transform(xslsheet, input, output);
  }
}
