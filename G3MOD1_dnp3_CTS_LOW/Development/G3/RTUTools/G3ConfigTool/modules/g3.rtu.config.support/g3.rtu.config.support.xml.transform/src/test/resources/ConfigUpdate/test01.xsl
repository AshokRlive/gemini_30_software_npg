<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:dnp3="DNP3Enum" xmlns:g3mod="ModuleProtocolEnum" xmlns:mcm="MCMConfigEnum"
	xmlns:g3="http://www.lucyswitchgear.com/g3RTUConfiguration"
	exclude-result-prefixes="xs">
	<!--<xsl:import-schema schema-location="g3schema.xsd"></xsl:import-schema> -->
	<!-- xpath-default-namespace="http://www.lucyswitchgear.com/g3RTUConfiguration" -->
	<xsl:strip-space elements="*" />

	<xsl:output method="xml" indent="yes" />
	<xsl:for-each select="configuration">
    <mymodules xmlns=""/>	
    </xsl:for-each>
	<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>


