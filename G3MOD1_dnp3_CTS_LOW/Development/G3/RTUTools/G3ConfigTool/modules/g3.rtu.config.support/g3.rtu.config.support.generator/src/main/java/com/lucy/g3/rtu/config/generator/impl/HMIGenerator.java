/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.clogic.CLogicRepository;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.threshold.ThresholdLogic;
import com.lucy.g3.rtu.config.generator.AbstractConfigGenerator;
import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.domain.InfoScreen;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.manager.RootScreen;
import com.lucy.g3.rtu.config.hmi.ui.IHMIGenerator;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointRepository;
import com.lucy.g3.rtu.config.virtualpoint.shared.util.VirtualPointFinder;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumIOM.IOM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumIOM.IOM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_AINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumPSM.PSM_CH_DINPUT;
import com.lucy.g3.xml.gen.ModuleProtocol.ModuleProtocolEnumSCMMK2.SCM_MK2_CH_DINPUT;
import com.lucy.g3.xml.gen.api.IChannelEnum;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.HMI_INFO_SCREEN_TYPE;

class HMIGenerator extends AbstractConfigGenerator implements IHMIGenerator{
  private static final String SNAME_MAIN = "Main Menu";
  private static final String SNAME_DATA = "Data";
  private static final String SNAME_ANALOG_DATA = "Analogue";
  private static final String SNAME_DIGITAL_DATA = "Digital";
  private static final String SNAME_CTRL = "Control";
  private static final String SNAME_INFO = "Info";

  private static final DefHMIPoint[] hmi_ai_psm = new DefHMIPoint[] {
      new DefHMIPoint(PSM_CH_AINPUT.PSM_CH_AINPUT_MS_CURRENT,
          "MS Current"),
      new DefHMIPoint(PSM_CH_AINPUT.PSM_CH_AINPUT_BT_VOLTAGE,
          "Bat Voltage"), };

  private static final DefHMIPoint[] hmi_di_psm = new DefHMIPoint[] {
      new DefHMIPoint(PSM_CH_DINPUT.PSM_CH_DINPUT_MS_OVERCURRENT,
          "MS OverCurrent"),
      new DefHMIPoint(PSM_CH_DINPUT.PSM_CH_DINPUT_AC_OFF, "AC Off"), };

  private static final DefHMIPoint[] hmi_bi_scm = new DefHMIPoint[] {
      new DefHMIPoint(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_SWITCH_OPEN, "Open"),
      new DefHMIPoint(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_SWITCH_CLOSED, "Closed")
      // Do not generate these points cause the channels are missing in the new SCM.
//      new DefHMIPoint(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_INSULTATION_LOW,
//          "Insulation Low"),
//      new DefHMIPoint(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_SWITCH_EARTHED,
//          "Earthed"),
//      new DefHMIPoint(SCM_MK2_CH_DINPUT.SCM_MK2_CH_DINPUT_ACTUATOR_DISABLED,
//          "Actuator Disabled"), 
      };

  private static final DefHMIPoint[] hmi_ai_iom = new DefHMIPoint[] {
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_CT, "A CT"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_TEMP, "Temp"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A1, "AI 1"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A2, "AI 2"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A3, "AI 3"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A4, "AI 4"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A5, "AI 5"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A6, "AI 6"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A7, "AI 7"),
      new DefHMIPoint(IOM_CH_AINPUT.IOM_CH_AINPUT_A8, "AI 8"), };

  private static final DefHMIPoint[] hmi_di_iom = new DefHMIPoint[] {
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_1, "DI 1 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_2, "DI 2 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_3, "DI 3 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_4, "DI 4 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_5, "DI 5 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_6, "DI 6 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_7, "DI 7 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_8, "DI 8 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_9, "DI 9 "),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_10, "DI 10"),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_11, "DI 11"),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_12, "DI 12"),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_13, "DI 13"),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_14, "DI 14"),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_15, "DI 15"),
      new DefHMIPoint(IOM_CH_DINPUT.IOM_CH_DINPUT_16, "DI 16"), };

  private MenuScreen analogDataMenu;
  private MenuScreen digitalDataMenu;
  private RootScreen mainMenu;
  private MenuScreen dataMenu;
  private MenuScreen ctrlMenu;
  private MenuScreen infoMenu;
  

  @Override
  public void createScreens(HMIScreenManager manager, Module m) {
    if (m == null) {
      return;
    }
    
    generateDataScreens(manager,m);
  }


  @Override
  public void generateScreensFromScratch(HMIScreenManager manager) {
    reset(manager);
    generateDataScreens(manager);
    generateCLogicScreens(manager);
    generateInfoScreens(manager);
  }


  private void reset(HMIScreenManager manager) {
    mainMenu = new RootScreen(SNAME_MAIN);
    manager.setMainMenu(mainMenu);

    analogDataMenu = null;
    digitalDataMenu = null;
    mainMenu = null;
    dataMenu = null;
    ctrlMenu = null;
  }

  
  @Override
  public void generateScreens(HMIScreenManager manager, IOperableLogic clogic) {
    if (clogic == null) {
      return;
    }

    MenuScreen parent = getControlMenu(manager);
    ControlScreen scmMenu = new ControlScreen(clogic.getDescription());
    scmMenu.setSwitchLogic(clogic);
    parent.add(scmMenu);
    manager.fireScreenAdded(parent, scmMenu);
  }

  
  private MenuScreen getAnalogDataMenu(HMIScreenManager manager) {
    if (analogDataMenu == null) {
      analogDataMenu = getSubMenu(manager,getDataMenu(manager), SNAME_ANALOG_DATA);
    }

    return analogDataMenu;
  }

  private MenuScreen getDigitalDataMenu(HMIScreenManager manager) {
    if (digitalDataMenu == null) {
      digitalDataMenu = getSubMenu(manager,getDataMenu(manager), SNAME_DIGITAL_DATA);
    }

    return digitalDataMenu;
  }

  private MenuScreen getDataMenu(HMIScreenManager manager) {
    if (dataMenu == null) {
      dataMenu = getSubMenu(manager, getMainMenu(manager), SNAME_DATA);
    }

    return dataMenu;
  }

  private MenuScreen getControlMenu(HMIScreenManager manager) {
    if (ctrlMenu == null) {
      ctrlMenu = getSubMenu(manager, getMainMenu(manager), SNAME_CTRL);
    }

    return ctrlMenu;
  }

  private MenuScreen getInfoMenu(HMIScreenManager manager) {
    if (infoMenu == null) {
      infoMenu = getSubMenu(manager, getMainMenu(manager), SNAME_INFO);
    }

    return infoMenu;
  }

  private MenuScreen getMainMenu(HMIScreenManager manager) {
    if (mainMenu == null) {
      mainMenu = manager.getMainMenu();

      // Create one
      if (mainMenu == null) {
        mainMenu = new RootScreen(SNAME_MAIN);
        manager.setMainMenu(mainMenu);
      }
    }

    return mainMenu;
  }

  private MenuScreen getSubMenu(HMIScreenManager manager,MenuScreen parent, String name) {
    MenuScreen subMenu;
    subMenu = findChildScreen(parent, name);

    // Create one
    if (subMenu == null) {
      subMenu = new MenuScreen(name);
      parent.add(subMenu);
      manager.fireScreenAdded(parent, subMenu);
    }

    return subMenu;
  }

  private void generateDataScreens(HMIScreenManager manager, Module m) {
    DataScreen dataScreen;
    MenuScreen parent;

    // Analogue
    dataScreen = createDataScreens(manager,m, true);
    if (dataScreen != null) {
      parent = getAnalogDataMenu(manager);
      parent.add(dataScreen);
      manager.fireScreenAdded(parent, dataScreen);
    }

    // Digital
    dataScreen = createDataScreens(manager,m, false);
    if (dataScreen != null) {
      parent = getDigitalDataMenu(manager);
      parent.add(dataScreen);
      manager.fireScreenAdded(parent, dataScreen);
    }
  }

  private void generateDataScreens(HMIScreenManager manager) {
    ModuleRepository repo = manager.getOwner().getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    Collection<Module> allModules = repo.getAllModules();

    for (Module m : allModules) {
      generateDataScreens(manager,m);
    }

  }

  private DataScreen createDataScreens(HMIScreenManager manager, Module m, boolean isAnalog) {
    DataScreen ret = null;

    DataEntry dataEntry;
    ArrayList<DataEntry> dataEntries = new ArrayList<DataEntry>();
    DefHMIPoint[] points = null;

    switch (m.getType()) {
    case MODULE_PSM:
      points = isAnalog ? hmi_ai_psm : hmi_di_psm;
      break;
    case MODULE_SCM:
      points = isAnalog ? null : hmi_bi_scm;
      break;
    case MODULE_IOM:
      points = isAnalog ? hmi_ai_iom : hmi_di_iom;
      break;
    default:
      points = null;
      break;
    }

    if (points != null) {
      VirtualPointRepository repo = manager.getOwner()
          .getConfigModule(VirtualPointRepository.CONFIG_MODULE_ID);
      for (DefHMIPoint point : points) {
        
        VirtualPoint vp = VirtualPointFinder.findStdVirtualPoint(m, point.ch, repo.getAllPoints());

        if (vp != null) {
          dataEntry = new DataEntry();
          dataEntry.setSource(vp);
          dataEntry.setHeader(point.name);
          dataEntries.add(dataEntry);
        }
      }

      if (!dataEntries.isEmpty()) {
        ret = new DataScreen(m.getShortName());
        ret.setDataEntries(dataEntries
            .toArray(new DataEntry[dataEntries.size()]));
      }
    }

    return ret;
  }

  private void generateCLogicScreens(HMIScreenManager manager) {
    CLogicRepository repo = manager.getOwner()
        .getConfigModule(CLogicRepository.CONFIG_MODULE_ID);
    List<IOperableLogic> swlogics = repo.getOperableCLogic();

    for (IOperableLogic sgl : swlogics) {
      if (!(sgl instanceof ThresholdLogic)) {
        generateScreens(manager, sgl);
      }
    }
  }

  private void generateInfoScreens(HMIScreenManager manager) {
    MenuScreen parent = getInfoMenu(manager);
    Screen child;

    parent.add(child = new InfoScreen("Hardware",
        HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_HARDWARE));
    manager.fireScreenAdded(parent, child);

    parent.add(child = new InfoScreen("Software",
        HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_SOFTWARE));
    manager.fireScreenAdded(parent, child);

    parent.add(child = new InfoScreen("Config",
        HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_CONFIG));
    manager.fireScreenAdded(parent, child);

    parent.add(child = new InfoScreen("Network",
        HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_NETWORK));
    manager.fireScreenAdded(parent, child);

    parent.add(child = new InfoScreen("Time",
        HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_TIME));
    manager.fireScreenAdded(parent, child);

    // Append alarms screen to root.
    parent = getMainMenu(manager);
    parent.add(child = new InfoScreen("Alarms",
        HMI_INFO_SCREEN_TYPE.HMI_INFO_SCREEN_TYPE_ALARMS));
    manager.fireScreenAdded(parent, child);
  }

  private static <T extends Screen> T findChildScreen(MenuScreen parentMenu,
      String childName) {
    Preconditions.checkNotNull(parentMenu, "parent menu is null");
    Preconditions.checkNotNull(childName, "childName is null");

    List<Screen> displayItems = parentMenu.getPreviewChildren();

    for (int i = 0; i < displayItems.size(); i++) {
      Screen screen = displayItems.get(i);
      if (screen.getTitle().equals(childName)) {
        try {
          T t = (T) screen;
          return t;
        } catch (Exception e) {
        }

      } else if (screen instanceof MenuScreen) {
        T t = findChildScreen((MenuScreen) screen, childName);
        if (t != null) {
          return t;
        }
      }
    }

    return null;
  }


  private static class DefHMIPoint {

    final IChannelEnum ch;
    final String name; // Max 10 characters


    public DefHMIPoint(IChannelEnum ch, String name) {
      super();
      this.ch = ch;
      this.name = name;
    }
  }

}
