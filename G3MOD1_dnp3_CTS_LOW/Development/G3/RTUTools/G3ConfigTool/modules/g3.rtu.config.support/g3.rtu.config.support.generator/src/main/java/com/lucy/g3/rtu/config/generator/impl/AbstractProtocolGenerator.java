/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import java.util.Collection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.generator.AbstractConfigGenerator;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.generator.GenerateOptions.GenerateOption;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocol;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointSource;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointUtility;
import com.lucy.g3.xml.gen.api.IXmlEnum.DefaultCreateOption;

abstract class AbstractProtocolGenerator extends AbstractConfigGenerator implements IIOMapGenerator {

  protected void genInputPointsForAllSessions(GenerateOptions options,
      IScadaProtocol<?> protocol,
      Collection<VirtualPoint> vpoints) {

    Collection<?> sesns = protocol.getAllSessions();
    GenerateOption option = options.getOption(GenerateOptions.Target.ScadaPoint);

    for (Object sesnObj : sesns) {
      IScadaProtocolSession sesn = (IScadaProtocolSession) sesnObj;
      genInputPoints(option, protocol, sesn.getIomap(), vpoints);
    }
  }

  protected void genOutputPointsForAllSessions(GenerateOptions options,
      IScadaProtocol<?> protocol,
      Collection<ICLogic> logics) {
    Collection<?> sesns = protocol.getAllSessions();
    GenerateOption option = options.getOption(GenerateOptions.Target.ScadaPoint);

    for (Object sesnObj : sesns) {
      IScadaProtocolSession sesn = (IScadaProtocolSession) sesnObj;
      genOutputPoints(option, protocol, sesn.getIomap(), logics);
    }
  }

  protected void genPoints(IConfig owner, IProtocolSession session) {
    IScadaProtocol<?> protocol = (IScadaProtocol<?>) session.getProtocolChannel().getProtocol();
    GenerateOptions options = GenerateOptions.genDefaultScadaPoint();
    genInputPointsForAllSessions (options, protocol, VirtualPointUtility.getAllPoints(owner));
    genOutputPointsForAllSessions(options, protocol, CLogicUtility.getAllCLogic(owner));
  }

  @Override
  public final void generate(IIoMap iomap, Object option) {
    GenerateOption genOption = (GenerateOption) option;
    IScadaProtocol<?> protocol = (IScadaProtocol<?>) iomap.getSession().getProtocolChannel().getProtocol();
    IConfig conf = protocol.getManager().getOwner();

    @SuppressWarnings("unchecked")
    IScadaIoMap<ScadaPoint> scadaIoMap = (IScadaIoMap<ScadaPoint>) iomap;

    genInputPoints(genOption, protocol, scadaIoMap, VirtualPointUtility.getAllPoints(conf));
    genOutputPoints(genOption, protocol, scadaIoMap, CLogicUtility.getAllCLogic(conf));
  }

  protected void genInputPoints(GenerateOption genOption,
      IScadaProtocol<?> protocol,
      IScadaIoMap<ScadaPoint> iomap,
      Collection<VirtualPoint> vpoints) {

    Preconditions.checkNotNull(genOption, "option must not be null");
    if (GenerateOption.shouldGenerate(genOption) == false || vpoints == null)
      return;

    ScadaPoint point = null;

    for (VirtualPoint vp : vpoints) {
      if (vp == null
          || vp.getDefaultCreateOption() != DefaultCreateOption.CreateVirtualPointAndScadaPoint)
        continue;

      ScadaPointType[] types = getScadaPointType(vp.getType());
      if (types == null)
        continue;

      for (ScadaPointType type : types) {
        long id = iomap.findAvailablePointID(type);
        point = createPoint(iomap, type, id);
        ScadaPointSource source = protocol.getManager().getSourceRepository().getSourceByRaw(vp);
        point.setSource(source);

        try {
          iomap.addPoint(point);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public void update(IIoMap iomap, Object option) {
    throw new UnsupportedOperationException("Not supported");    
  }
  
  protected void genOutputPoints(GenerateOption genOption,
      IScadaProtocol<?> protocol,
      IScadaIoMap<ScadaPoint> iomap,
      Collection<ICLogic> logics) {

    Preconditions.checkNotNull(genOption, "option must not be null");
    if (GenerateOption.shouldGenerate(genOption) == false || logics == null)
      return;

    ScadaPoint point;

    for (ICLogic logic : logics) {
      if (logic == null || !(logic instanceof IOperableLogic))
        continue;

      ScadaPointType[] types = getScadaPointType(logic.getType());

      if (types == null)
        continue;
      for (ScadaPointType type : types) {

        long id = iomap.findAvailablePointID(type);
        point = createPoint(iomap, type, id);
        ScadaPointSource source = protocol.getManager().getSourceRepository().getSourceByRaw(logic);
        point.setSource(source);

        try {
          iomap.addPoint(point);
        } catch (DuplicatedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  @Override
  public GenerateOption[] getGenerateOptions() {
    return new GenerateOption[] {
        GenerateOption.GenerateDefault,
        GenerateOption.GenerateFull,
        GenerateOption.DoNotGenerate
      };
  }

  
  abstract ScadaPoint createPoint(IScadaIoMap<?> iomap, ScadaPointType type, long id);

  abstract ScadaPointType[] getScadaPointType(ICLogicType type);

  abstract ScadaPointType[] getScadaPointType(VirtualPointType type);
}
