/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.ISDNP3Generator;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3IoMap;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;


class SDNP3Generator extends AbstractProtocolGenerator implements ISDNP3Generator, IIOMapGenerator{
  
  @Override
  ScadaPoint createPoint(IScadaIoMap<?> iomap, ScadaPointType type, long id) {
    return SDNP3PointFactory.create((SDNP3IoMap)iomap, (SDNP3PointType)type, id);
  }

  @Override
  ScadaPointType[] getScadaPointType(ICLogicType type) {
    SDNP3PointType pointType;
    if (ICLogicType.ANALOGCTRL == type.getId())
      pointType = SDNP3PointType.AnalogOutput;
    else
      pointType = SDNP3PointType.BinaryOutput;
    
    return new ScadaPointType[]{pointType}; 
  }

  @Override
  ScadaPointType[] getScadaPointType(VirtualPointType type) {
    SDNP3PointType pointType = null;
    
    switch (type) {
    case ANALOGUE_INPUT:
      pointType = SDNP3PointType.AnalogueInput;
      break;
  
    case BINARY_INPUT:
      pointType = SDNP3PointType.BinaryInput;
      break;
  
    case DOUBLE_BINARY_INPUT:
      pointType = SDNP3PointType.DoubleBinaryInput;
      break;
  
    default:
      break;
    }
    
    return pointType == null ? new ScadaPointType[0] : new ScadaPointType[]{pointType};
  }

  @Override
  public void genInputPointsForAllSessions(GenerateOptions options, SDNP3 protocol,
      Collection<VirtualPoint> vpoints) {
    super.genInputPointsForAllSessions(options, protocol, vpoints);
  }
      
  @Override
  public void genOutputPointsForAllSessions(GenerateOptions options,
      SDNP3 protocol,
      Collection<ICLogic> logics) {
    super.genOutputPointsForAllSessions(options, protocol, logics);
  }
  
  @Override
  public void genPoints(IConfig owner, IProtocolSession session) {
    super.genPoints(owner, session);
  }
}

