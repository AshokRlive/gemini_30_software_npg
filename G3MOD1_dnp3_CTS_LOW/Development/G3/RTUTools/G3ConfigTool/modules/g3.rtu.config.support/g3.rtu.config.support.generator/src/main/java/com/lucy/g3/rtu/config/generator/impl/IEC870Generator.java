/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import java.util.Collection;

import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870IoMap;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IIEC870Generator;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870PointFactory;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.IScadaIoMap;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPoint;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.ScadaPointType;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;

class IEC870Generator extends AbstractProtocolGenerator implements IIEC870Generator, IIOMapGenerator{
  
  @Override
  public void genInputPointsForAllSessions(GenerateOptions options, 
      IEC870<?> protocol,
      Collection<VirtualPoint> vpoints) {
   super.genInputPointsForAllSessions(options, protocol, vpoints); 
  }
  
  @Override
  public void genOutputPointsForAllSessions(GenerateOptions options,
      IEC870<?> protocol,
      Collection<ICLogic> logics) {
    super.genOutputPointsForAllSessions(options, protocol, logics);
  }

  @Override
  public void genPoints(IConfig owner, IProtocolSession session) {
    super.genPoints(owner, session);
  }
  
  
  @Override
  ScadaPoint createPoint(IScadaIoMap<?> iomap, ScadaPointType type, long id) {
    return IEC870PointFactory.create((IEC870IoMap)iomap, (IEC870PointType)type, id);
  }

  @Override
  ScadaPointType[] getScadaPointType(ICLogicType type) {
    IEC870PointType pointType = null;
    
    if(type.getId() == ICLogicType.SGL || type.getId() == ICLogicType.DSL)
      pointType = IEC870PointType.CDC;
      
    else if (type.getId() == ICLogicType.FPI_RESET)
      pointType = IEC870PointType.CSC;
    
    return pointType == null ? new ScadaPointType[0] : new ScadaPointType[]{pointType};
  }

  @Override
  ScadaPointType[] getScadaPointType(VirtualPointType type) {
    IEC870PointType[] types = null;
    
    switch(type){
    case ANALOGUE_INPUT:
      types = new IEC870PointType[]{IEC870PointType.MMEA,IEC870PointType.MMEB,IEC870PointType.MMEC};
      break;
    case BINARY_INPUT:
      types = new IEC870PointType[]{IEC870PointType.MSP};
      break;
    case COUNTER:
      types = new IEC870PointType[]{IEC870PointType.MIT};
      break;
    case DOUBLE_BINARY_INPUT:
      types = new IEC870PointType[]{IEC870PointType.MDP};
      break;
    default:
      break;
    }
    
    return types;
  }
}

