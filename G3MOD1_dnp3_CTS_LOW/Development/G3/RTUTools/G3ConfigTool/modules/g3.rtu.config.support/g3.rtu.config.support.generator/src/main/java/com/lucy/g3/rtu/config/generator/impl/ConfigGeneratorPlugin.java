/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.hmi.ui.IHMIGenerator;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.ISDNP3Generator;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IIEC870Generator;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.shared.factory.IIOMapGeneratorFactory;
import com.lucy.g3.rtu.config.protocol.shared.factory.IOMapGeneratorFactories;
import com.lucy.g3.rtu.config.protocol.slave.shared.factory.ScadaProtocolGenerators;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.IVirtualPointGenerator;


/**
 *
 */
public class ConfigGeneratorPlugin {
  private ConfigGeneratorPlugin() {}
  
  public static void init() {
    ConfigGeneratorRegistry reg = ConfigGeneratorRegistry.getInstance();
    
    ConfigGenerator instance = new ConfigGenerator();
    reg.registerGenerator(IVirtualPointGenerator.class, instance);
    reg.registerGenerator(ICLogicGenerator.class, instance);
    reg.registerGenerator(IIOModuleGenerator.class, instance);
    
    reg.registerGenerator(IHMIGenerator.class, new HMIGenerator());
    reg.registerGenerator(IIEC870Generator.class, new IEC870Generator());
    reg.registerGenerator(ISDNP3Generator.class, new SDNP3Generator());
    
    ScadaProtocolGenerators.registerFactory(ProtocolType.SDNP3, new SDNP3Generator());
    ScadaProtocolGenerators.registerFactory(ProtocolType.S101, new IEC870Generator());
    ScadaProtocolGenerators.registerFactory(ProtocolType.S104, new IEC870Generator());
    
    IOMapGeneratorFactories.register(ProtocolType.SDNP3, new IIOMapGeneratorFactory(){

      @Override
      public IIOMapGenerator getGenerator(IConfig config) {
        return new SDNP3Generator();
      }
      
    });
    
    IOMapGeneratorFactories.register(ProtocolType.S101, new IIOMapGeneratorFactory(){
      
      @Override
      public IIOMapGenerator getGenerator(IConfig config) {
        return new IEC870Generator();
      }
      
    });
    
    IOMapGeneratorFactories.register(ProtocolType.S104, new IIOMapGeneratorFactory(){
      
      @Override
      public IIOMapGenerator getGenerator(IConfig config) {
        return new IEC870Generator();
      }
      
    });
    
    IOMapGeneratorFactories.register(ProtocolType.MMB, new IIOMapGeneratorFactory() {
      @Override
      public IIOMapGenerator getGenerator(IConfig config) {
        return new MMBGenerator(config);
      }
    });
  }
}

