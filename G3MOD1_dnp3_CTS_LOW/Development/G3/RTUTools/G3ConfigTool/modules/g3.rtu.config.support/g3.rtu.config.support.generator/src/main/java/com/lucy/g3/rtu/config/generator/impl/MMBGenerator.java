/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.config.generator.impl;

import org.apache.commons.lang3.ArrayUtils;

import com.lucy.g3.configtool.config.template.ITemplate;
import com.lucy.g3.configtool.config.template.TemplateRepositories;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIOMapGenerator;
import com.lucy.g3.rtu.config.protocol.shared.domain.IIoMap;
import com.lucy.g3.rtu.config.protocol.shared.domain.IProtocolSession;
import com.lucy.g3.rtu.config.shared.manager.IConfig;


class MMBGenerator implements IIOMapGenerator{

  private final IConfig config;

  public MMBGenerator(IConfig config) {
    this.config = config;
  }

  @Override
  public void update(IIoMap iomap, Object option) {
    IProtocolSession target = iomap.getSession();
    ITemplate templ = (ITemplate) option;
    templ.applyTo(target, false);
  }

  @Override
  public void generate(IIoMap iomap, Object option) {
    IProtocolSession target = iomap.getSession();
    ITemplate templ = (ITemplate) option;
    templ.applyTo(target, true);
  }

  @Override
  public Object[] getGenerateOptions() {
    ITemplate[] builtinTempls = TemplateRepositories.getAvailabelTemplates(config, TemplateRepositories.KEY_MMB_BUILTIN);
    ITemplate[] customTempls = TemplateRepositories.getAvailabelTemplates(config, TemplateRepositories.KEY_MMB_CUSTOM);
    return ArrayUtils.addAll(builtinTempls, customTempls);
  }
}

