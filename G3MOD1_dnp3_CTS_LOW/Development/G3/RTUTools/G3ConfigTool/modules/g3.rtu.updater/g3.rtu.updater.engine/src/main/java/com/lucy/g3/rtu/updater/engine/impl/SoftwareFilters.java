/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.engine.impl;

import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwareFilter;


public class SoftwareFilters {
  
  public static SoftwareFilter create(IUpdateModule m, SystemAPIVersion api) {
    return new SoftwareFilter(m.getType(), m.getFeature(), api);
  }
  
  public static SoftwareFilter create(IUpdateModule m) {
    return create(m, null);
  }
}

