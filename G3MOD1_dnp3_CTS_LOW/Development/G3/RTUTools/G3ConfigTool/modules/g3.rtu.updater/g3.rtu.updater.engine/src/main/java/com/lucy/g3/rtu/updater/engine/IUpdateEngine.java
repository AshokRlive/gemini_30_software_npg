/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.engine;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import com.lucy.g3.rtu.updater.comms.CommsException;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.rtu.updater.sdp.IModuleSoftware;
import com.lucy.g3.rtu.updater.sdp.ISoftwarePackage;


public interface IUpdateEngine {
  
  void enterUpdateMode() throws CommsException, InterruptedException;
   
  void exitUpdateMode() throws CommsException, InterruptedException;
  
  /**
   * @return programmed modules.
   */
  Collection<IUpdateModule> programme(String sdpVersion, Map<IUpdateModule, IModuleSoftware> swEntries)
      throws UpdateException, IOException, CommsException, InterruptedException;
  
  /**
   * @return programmed modules.
   */
  Collection<IUpdateModule> programme(UpdateOption option, ISoftwarePackage sdp) 
      throws UpdateException, IOException, CommsException, InterruptedException;
  
  void writeConfig(File config) throws IOException, UpdateException, CommsException;
  
  public enum UpdateOption {
    BOOTLOADER_ONLY,
    ALL,
  }

}

