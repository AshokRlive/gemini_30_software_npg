/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.engine;


public class SoftwareMissingException extends UpdateException {

  public SoftwareMissingException() {
    super();
    
  }

  public SoftwareMissingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    
  }

  public SoftwareMissingException(String message, Throwable cause) {
    super(message, cause);
    
  }

  public SoftwareMissingException(String message) {
    super(message);
    
  }

  public SoftwareMissingException(Throwable cause) {
    super(cause);
    
  }

}

