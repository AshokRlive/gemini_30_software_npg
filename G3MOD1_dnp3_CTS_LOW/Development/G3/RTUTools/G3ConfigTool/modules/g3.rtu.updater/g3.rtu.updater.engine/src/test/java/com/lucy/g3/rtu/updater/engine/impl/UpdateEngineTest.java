
package com.lucy.g3.rtu.updater.engine.impl;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import com.lucy.g3.rtu.updater.comms.impl.RTUUpdateComms;
import com.lucy.g3.rtu.updater.engine.impl.UpdateEngine;


public class UpdateEngineTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }
  
  
  public static void main(String[] args) throws Exception {
    BasicConfigurator.configure();
    Logger.getRootLogger().setLevel(Level.INFO);
    
    RTUUpdateComms comms = new RTUUpdateComms();
    comms.connect("192.168.2.1", "Admin", "");
    UpdateEngine engine = new UpdateEngine(new File(""), comms, true);
    engine.getModuleList();
  }
}

