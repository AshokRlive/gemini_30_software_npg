/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.engine.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.updater.comms.CommsException;
import com.lucy.g3.rtu.updater.comms.IRTUComms;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.rtu.updater.comms.UpdateStatus;
import com.lucy.g3.rtu.updater.engine.IUpdateEngine;
import com.lucy.g3.rtu.updater.engine.SoftwareMissingException;
import com.lucy.g3.rtu.updater.engine.UpdateException;
import com.lucy.g3.rtu.updater.sdp.IModuleSoftware;
import com.lucy.g3.rtu.updater.sdp.ISoftwarePackage;
import com.lucy.g3.rtu.updater.sdp.SoftwarePackageUtils;
import com.lucy.g3.schema.support.RTUConfigSchemaResource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.validation.XmlValidate;


public class UpdateEngine implements IUpdateEngine{
  private Logger log = Logger.getLogger(UpdateEngine.class);
  
  private static final int MAX_RETRIES = 3;
  
  private final File tempDir;
  
  private final IRTUComms comms;
  
  private boolean ignoreError;
  
  public UpdateEngine(File tempDir, IRTUComms comms, boolean ignoreError) {
    this.comms = Preconditions.checkNotNull(comms, "comms must not be null");
    this.tempDir = Preconditions.checkNotNull(tempDir, "tempDir must not be null");
    this.ignoreError = ignoreError;
  }
  
  @Override
  public void exitUpdateMode() throws CommsException,InterruptedException{
    log.info("Exiting upgrade mode...");
    comms.setUpdateMode(false, true);
    
    // Give RTU enough time to initialise all modules
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {}
  }

  @Override
  public void enterUpdateMode() throws CommsException, InterruptedException {
    log.info("Entering upgrade mode...");
    comms.setUpdateMode(true, true);
    
    // Give RTU enough time to initialise all modules
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {}
  }

  @Override
  public Collection<IUpdateModule> programme(
      String sdpVersion, Map<IUpdateModule, IModuleSoftware> updateEntries)
      throws UpdateException, IOException, CommsException, InterruptedException {
    
    ArrayList<IUpdateModule> updatedList = new ArrayList<>();
        
    checkReadyForUpdate(comms);
    
    Set<IUpdateModule> modules = updateEntries.keySet();
    
    /* Programme slave modules first*/
    for (IUpdateModule module : modules) {
      if(!isMCM(module)) {
        if(programModule(comms, module, updateEntries.get(module))) {
          updatedList.add(module);
        }
      }
    }
    
    /* Programme master module*/
    for (IUpdateModule module : modules) {
      if(isMCM(module)) {
        if(programModule(comms, module, updateEntries.get(module)))
          updatedList.add(module);
      }
    }
    
    if(!Strings.isBlank(sdpVersion))
      comms.writeSDPVersion(sdpVersion);
    
    cleanup();
    
    return updatedList;
  }

  @Override
  public Collection<IUpdateModule> programme(UpdateOption option, ISoftwarePackage sdp) 
      throws UpdateException, CommsException, InterruptedException, IOException {
    
    /* Prepare software update entries*/
    IUpdateModule[] modules = getModuleList();
    logModuleList(modules);
    
    IUpdateModule mcm = findMCM(modules);
    if(mcm == null)
      throw new UpdateException("MCM is missing!");
    
    // Select API for all modules
    SystemAPIVersion api = selectSystemAPI(sdp, mcm);
    log.info("Selected System API:" + api);
    
    // Find software for each module 
    HashMap<IUpdateModule, IModuleSoftware> entries = new HashMap<>();
    for (IUpdateModule m : modules) {
      if(option == UpdateOption.BOOTLOADER_ONLY && isMCM(m)) 
        continue; // skip master if it is bootloader update 
      
      IModuleSoftware sw = sdp.getLastest(SoftwareFilters.create(m, api));
      if(sw == null) {
        String err = String.format(
            "software was not found in SDP for the module: %s, feature:%s, api:%s", 
            m.getName(), m.getFeature(), api);
        if(ignoreError) {
          log.error(err);
        } else {
          throw new SoftwareMissingException(err);
        }
      } else {
        entries.put(m, sw);
      }
    }
    
    return programme(sdp.getVersion(), entries);
  }

  @Override
  public void writeConfig(File config) throws IOException, UpdateException, CommsException {
    if(!config.isFile() && !config.exists()) {
      throw new IOException("Config file not found:" + config);
    }
    // Validate exported XML file
    log.info("Validating configuration...");
    try {
      new XmlValidate(RTUConfigSchemaResource.FILE_PATH).validate(config.getPath());
      log.info("Configuration is valid");
    } catch (Exception e) {
      throw new UpdateException(
          String.format("The provided configuration file is invalid against schema(version:%s.%s)", 
              G3ConfigAPI.CURRENT_API.SchemaVerMajor, G3ConfigAPI.CURRENT_API.SchemaVerMinor));
    }
  
    /* Compress configuration file with GZip */
    File tempFile = null; 
    String path = config.getAbsolutePath();
    if(!path.endsWith(G3Files.SUFFFIX_GZIP)) {
      log.debug("Compressing configuration...");
      String gzipConfig = path + G3Files.SUFFFIX_GZIP;
      ZipUtil.gzip(path, gzipConfig, false);
      tempFile = new File(gzipConfig);
      config = tempFile;
    }
  
    /* Write file to RTU */
    log.info("Writing configuration file to RTU...");
    comms.writeConfigFile(config); 
  
    /* Delete the generated configuration file after writing to RTU*/
    if(tempFile != null) {
      log.debug("Deleting temporary file.");
      tempFile.delete();
    }
    log.info("Finished writing config.");
  }

  IUpdateModule[] getModuleList() throws CommsException, UpdateException {
    IUpdateModule[] modules = comms.getAllModules();
    
    if(modules == null)
      throw new UpdateException("Module list not available!");
    
    return modules;
  }

  private void cleanup() {
    try {
      FileUtils.deleteDirectory(tempDir);
    } catch (IOException e) {
      log.error("Failed to delete temporary folder: " + tempDir);
    }
  }

  private SystemAPIVersion selectSystemAPI(ISoftwarePackage sdp, IUpdateModule mcm) {
    IModuleSoftware[] allMcmSw = sdp.getAvailable(SoftwareFilters.create(mcm));
    
    IModuleSoftware mcmSw = SoftwarePackageUtils.findLatest(allMcmSw);
    
    /* MCM software not found, use the current system API*/
    if(mcmSw == null) {
        return mcm.getCurrentSystemAPI(); 
    }else 
        return mcmSw.getSupportedSystemAPI();
  }

  private boolean programModule(IRTUComms comms, IUpdateModule module, IModuleSoftware software) 
      throws IOException, CommsException, UpdateException {
    
    if (software == null) {
      log.error(String.format("Module: %s is not upgraded cause no software entry is found", module));
      return false;
    }
    
    /* Transfer extracted firmware */
    log.info(String.format("Sending file \"%s\"... ", software.getName()));
    if(isMCM(module)) {
      comms.writeMCMSoftware(software.extractToDir(tempDir));
    } else {
      comms.writeSlaveSoftware(software.extractToDir(tempDir));
    }

    /* Send command to start programming */
    log.info("Programming module \"" + module+"\"...");
    int retries = 0;
    while (!isCancelled()) {
      try {
        comms.startProgramme(module.getType(),  module.getID(), module.getSerialNo());
        break;
      } catch (CommsException e) {
        /* Failed to send command, continue to retry...*/
        if (retries < MAX_RETRIES) {
          log.warn("Failed to start programme, retries:" + retries);
          retries ++;
          // Wait a while to allow module to be ready
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e1) {
            break;
          }
          continue;
        } else {
          throw e;
        }
      }
    }
    
    /* Give MCM time to extract package*/
    if(isMCM(module)) {
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
      }
    }
    
    /* Waiting programming to be finished */
    boolean success = pollUpdateResult(comms, module.getName());
    if (success) {
      log.info("Module \"" + module + "\" has been updated.");
    } else {
      String err = "Module \"" + module + "\" has NOT been updated!";
      if(ignoreError)
        log.error(err);
      else
        throw new UpdateException(err);
    }
    
    return success;
  }
  
  private int updateProgress = -1;
  
  private boolean pollUpdateResult(IRTUComms comms, String moduleName) throws CommsException {
    int retries = 0;
    UpdateStatus status;
    
    updateProgress = -1;
    
    while (!isCancelled()) {
      try {
        status = comms.getUpdateStatus();
      } catch (CommsException e) {
        /* Failed to get status, continue to retry...*/
        if (retries < MAX_RETRIES) {
          retries ++;
          log.warn("Failed to get update status, retries:" + retries);
          continue;
        } else {
          throw e;
        }
      }

      // Stop waiting if unable to retrieve state
      if (status == null) {
        log.fatal("Cannot get update status from RTU!");
        return false;
      }

      // Stop waiting if error detected
      if (status.hasError()) {
        log.error(String.format("Programming error was reported:%s",
            moduleName, status.getError()));
        return false;
      }

      // Stop waiting if upgrading is completed
      if (status.isFinshed()) {
        break;
      }

      // Update progress
      logMsg(String.format("Programming %s:  %d%%", moduleName, status.getProgress()));
      setProgress(status.getProgress());
    }
    
    return true;
  }
  
  private void setProgress(int progress) {
    if((progress != this.updateProgress) &&
        (this.updateProgress < 0 || (progress - this.updateProgress) > 20 || progress == 100)) {
      this.updateProgress = progress;
      log.info(String.format("Programming progress: %d%%", progress));
    }
  }

  private boolean isCancelled() {
    return false;
  }

  private void checkReadyForUpdate(IRTUComms comms) throws UpdateException, CommsException {
    if(comms.isUpdateMode()) {
      if(comms.isUpdatingInProgress()) {
        throw new UpdateException("Software update is already in progress!");
      }
    }
  }

  private void logMsg(String msg) {
    //log.info(msg);
  }


  private void logModuleList(IUpdateModule[] modules) {
    StringBuilder sb = new StringBuilder();
    int i = 0;
    if(modules != null) {
      for (IUpdateModule m : modules) {
        sb.append(String.format("\n[%-2d] %-12s Feature:%-6s Version:%-20s SystemAPI:%s", 
            i, m.getName(), m.getFeature(), m.getVersion(), m.getCurrentSystemAPI()));
        i ++;
      }
    } else {
      sb.append("N/A");
    }
       
    log.info("RTU Module list: " + sb.toString());
  }

  private static IUpdateModule findMCM(IUpdateModule[] modules) {
    for (int i = 0; i < modules.length; i++) {
      if(isMCM(modules[i]))
        return modules[i];
    }
    return null;
  }

  private static boolean isMCM(IUpdateModule module) {
    return module != null && module.getType() == MODULE.MODULE_MCM;
  }
  
}
