/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.app;

import org.jdesktop.application.Application;

public class Main {
  public static void main(String[] args) {
    UpdaterApp app = Application.getInstance(UpdaterApp.class);
    app.initialize(args);
    app.startup();
    
    System.exit(app.getErrorCode());
  }
}

