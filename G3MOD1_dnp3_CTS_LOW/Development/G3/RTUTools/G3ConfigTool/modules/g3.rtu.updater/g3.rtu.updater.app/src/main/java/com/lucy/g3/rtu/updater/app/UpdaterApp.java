/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.app;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

import com.lucy.g3.rtu.updater.Updater;


public class UpdaterApp  extends Application{
  private Logger log = Logger.getLogger(UpdaterApp.class);
  private final Updater updater = new Updater();
  
  @Override
  protected void initialize(String[] args) {
    updater.initialize(args);
  }
  
  @Override
  protected void startup() { 
    updater.run();
    
    exit();
  }

  @Override
  protected void shutdown() {
    int err = updater.getErrCode();
    
    if(err == 0) 
        log.info("Exit with status: " + updater.getErrMessage());
      else
        log.error("Exit with error: " + updater.getErrMessage());
  }


  public int getErrorCode() {
    return updater.getErrCode();
  }
}

