/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms;


public final class CommsException extends Exception {

  public CommsException() {
    super();
    
  }

  public CommsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    
  }

  public CommsException(String message, Throwable cause) {
    super(message, cause);
    
  }

  public CommsException(String message) {
    super(message);
    
  }

  public CommsException(Throwable cause) {
    super(cause);
    
  }

}

