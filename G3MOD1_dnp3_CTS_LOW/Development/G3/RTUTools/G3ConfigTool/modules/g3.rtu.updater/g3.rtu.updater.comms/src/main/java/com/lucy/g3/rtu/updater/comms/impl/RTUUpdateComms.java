/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.RTURestartSupport;
import com.lucy.g3.rtu.comms.service.RTURestartSupport.IWaitingRestartInvoker;
import com.lucy.g3.rtu.comms.service.filetransfer.FileWritingCommsTask;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeStatus;
import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.rtu.updater.comms.CommsException;
import com.lucy.g3.rtu.updater.comms.ConnectResult;
import com.lucy.g3.rtu.updater.comms.IRTUComms;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.rtu.updater.comms.UpdateStatus;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


public class RTUUpdateComms implements IRTUComms, IWaitingRestartInvoker {
  
  private static final int RESTART_TIMEOUT = 60;// seconds

  private final RTUCommsService rtu;
  
  private Logger log = Logger.getLogger(RTUUpdateComms.class);
  
  public RTUUpdateComms() {
    rtu = new RTUCommsService("");
    rtu.setConnectTimeout(30000);
    
    /* Always ignore certificate verification since it is not implemented for updater yet*/
    rtu.getLinkLayer().getSecurityManager().setIgnoreCertificateError(true);
  }
  
  @Override
  public void setUpdateMode(boolean enabled, boolean waiting) throws CommsException {
    try {
      if(enabled)
        rtu.getFwUpgrade().cmdEnterUpgradeMode();
      else
        rtu.getFwUpgrade().cmdExitUpgradeMode();
      
      // Wait RTU to swap mode
      CTH_RUNNINGAPP newState = RTURestartSupport.waitingRestart(this, RESTART_TIMEOUT);
  
      // Verify upgrade mode
      if (enabled && newState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
        throw new Exception("Failed to enter upgrade mode. Check the state of RTU!");
        
      }else if(!enabled && newState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
        throw new Exception("Failed to exit upgrade mode. Check the state of RTU!");
      }
    }catch(Exception e) {
      throw new CommsException(e);
    }
  }

  @Override
  public IUpdateModule[] getAllModules() throws CommsException {
    try {
      List<ModuleInfo> minfo = rtu.getRtuModules().cmdGetModueInfo();
      IUpdateModule[] mlist = new IUpdateModule[minfo.size()];
      int i = 0;
      for (ModuleInfo info: minfo) {
        mlist[i++] = new UpdateModule(info);
      }
      
      return mlist;
      
    } catch (Throwable e) {
      throw new CommsException(e);
    }
  }
  

  @Override
  public boolean isUpdatingInProgress() throws CommsException {
    /* RTU in upgrade mode.Check no upgrading is in progress */
    UpgradeStatus upgradState;
    try {
      upgradState = rtu.getFwUpgrade().cmdGetUpradeStatus();
      if (upgradState != null && upgradState.isUpgrading()) {
        return true;
      }else {
        return false;
      }
    } catch (IOException | SerializationException e) {
      throw new CommsException(e);
    }
  }

  @Override
  public boolean isUpdateMode() throws CommsException {
    try {
      return rtu.getFwUpgrade().cmdCheckUpgMode();
    } catch (IOException | SerializationException e) {
      throw new CommsException(e);
    }
  }

  @Override
  public void writeSDPVersion(String sdpVersion) throws CommsException {
    try {
      rtu.getFwUpgrade().cmdSendSDPVersion(sdpVersion);
    } catch (IOException | SerializationException e) {
      throw new CommsException(e);
    }
  }

  @Override
  public void writeMCMSoftware(File file) throws CommsException {
    writeFile(file, CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_MCM);
  }

  @Override
  public void writeSlaveSoftware(File file) throws CommsException {
    writeFile(file, CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_SLAVE);
  }

  @Override
  public void writeConfigFile(File config) throws CommsException {
    writeFile(config, CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_G3CONFIG);
    try {
      rtu.cmdActivateConfig();
    } catch (IOException | SerializationException e) {
      throw new CommsException("Failed to activiate config", e); 
    }
  }

  private void writeFile(File file, CTH_TRANSFERTYPE type) throws CommsException {
    ICommsTaskInvoker invoker = new RTUFileTransferInvoker();
    FileWritingCommsTask fileTransferTask = new FileWritingCommsTask(
        invoker, 
        rtu.getFileTransfer(),
        type, 
        file);
    
    try {
      fileTransferTask.writeFile(type, file);
    } catch (Exception e) {
      throw new CommsException(e);
    }
  }

  @Override
  public void startProgramme(MODULE type, MODULE_ID id, long serialNo) throws CommsException {
    try {
      rtu.getFwUpgrade().cmdUpgradeModule(type, id, (int) serialNo);
    } catch (IOException | SerializationException e) {
      throw new CommsException(e);
    }
  }

  @Override
  public UpdateStatus getUpdateStatus() throws CommsException{
    try {
      UpgradeStatus s = rtu.getFwUpgrade().cmdGetUpradeStatus();
      return UpdateStatus.createFrom(s);
      
    } catch (IOException | SerializationException e) {
      throw new CommsException(e);
    }
  }

  @Override
  public ConnectResult connect(String ipAddress, String user, String pass) throws CommsException {
    rtu.setHost(ipAddress);
    if(pass == null)
      pass = "";
    
    try {
      log.debug(String.format("Initialising comms: %s", ipAddress));
      CTH_RUNNINGAPP state = rtu.cmdCheckAlive();
      
      log.debug(String.format("Login to RTU with user:%s ", user));
      LoginResult result = rtu.getLogin().cmdLogin(user, pass.toCharArray());
      
      return new ConnectResult(state, result.level);
      
    } catch (Exception e) {
      throw new CommsException(e);
    }
  }

  @Override
  public void registerModules() throws CommsException {
    try {
      rtu.cmdRegisterModules();
    } catch (IOException | SerializationException e) {
      throw new CommsException(e);
    }
  }

  @Override
  public boolean isCancelled() {
    return false;
  }

  @Override
  public CTH_RUNNINGAPP checkRTUState() {
    try {
      return rtu.cmdCheckAlive();
    } catch (IOException | SerializationException e) {
      return null;
    }
  }

  @Override
  public void restartRTU(boolean waiting, String newIpAddress) throws CommsException {
    try {
      rtu.cmdRestart(RestartMode.RESTART, false);
    } catch (IOException | SerializationException e1) {
      throw new CommsException("Failed to restart RTU", e1);
    }
    
    if(waiting) {
      try {
        if(newIpAddress != null && !newIpAddress.trim().isEmpty())
          rtu.setHost(newIpAddress);
        RTURestartSupport.waitingRestart(this, RESTART_TIMEOUT);
      } catch (TimeoutException | InterruptedException e) {
        throw new CommsException("RTU restart timeout!",e);
      }
    }
  }

  @Override
  public String[] getEthIp() throws CommsException {
    try {
      NetInfo[] netinfo = rtu.getRtuInfo().cmdGetNetInfo();
      String [] ip = new String[netinfo.length];
      for (int i = 0; i < ip.length; i++) {
        ip[i] = netinfo[i].getIp();
      }
      
      return ip;
          
    } catch (IOException | SerializationException e) {
      throw new CommsException("NetInfo reading failed!",e);
    }
  }

  @Override
  public Map<String, String> getRTUInfo() throws CommsException {
    try {
      rtu.getRtuInfo().cmdGetRTUInfo();
      RTUInfo info = rtu.getRtuInfo();
      HashMap<String, String> infoMap = new HashMap<>();
      infoMap.put("Site Name", info.getSiteName());
      infoMap.put("CPU Serial", info.getCpuSerial());
      infoMap.put("Kernel", info.getKernelVersion());
      infoMap.put("Root FS", info.getRootfsVersion());
      infoMap.put("SDP Version", info.getSdpVersion());
      infoMap.put("MCM", info.getMcmVersion());
      
      return infoMap;
    } catch (IOException | SerializationException e) {
      throw new CommsException("RTUInfo reading failed!",e);
    }
  }

}

