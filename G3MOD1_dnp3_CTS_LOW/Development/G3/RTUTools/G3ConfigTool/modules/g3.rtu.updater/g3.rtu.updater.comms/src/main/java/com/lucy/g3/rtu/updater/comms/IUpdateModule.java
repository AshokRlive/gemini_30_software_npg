/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


public interface IUpdateModule {

  MODULE getType();
  
  MODULE_ID getID();

  String getName();

  FeatureVersion getFeature();

  SystemAPIVersion getCurrentSystemAPI();
  
  long getSerialNo();

  SoftwareVersion getVersion();

}

