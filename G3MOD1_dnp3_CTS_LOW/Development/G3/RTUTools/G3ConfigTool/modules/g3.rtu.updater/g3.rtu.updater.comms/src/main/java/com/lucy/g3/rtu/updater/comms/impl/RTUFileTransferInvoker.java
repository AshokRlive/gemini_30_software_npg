/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms.impl;

import java.io.File;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;

class RTUFileTransferInvoker implements ICommsTaskInvoker{
  private Logger log = Logger.getLogger("FileTransfer");
  
  public RTUFileTransferInvoker() {
  }
  
  @Override
  public boolean confirmOverrite(File overwriteFile) {
    return true;
  }

  @Override
  public boolean isCancelled() {
    return false;
  }

  private int progress = -1;
  @Override
  public void setProgress(int progress) {
    if(this.progress < 0 || (progress - this.progress) > 20 || progress == 100) {
      this.progress = progress;
      log.info(String.format("Progress: %d%%", progress));
    }
  }

  @Override
  public void setMessage(String message) {
    //log.info(message);
  }

}

