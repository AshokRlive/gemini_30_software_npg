/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms;

import java.io.File;
import java.util.Map;

import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * RTU comms API for software update.
 */
public interface IRTUComms {
  
  void setUpdateMode(boolean enabled, boolean waiting) throws CommsException, InterruptedException;

  void writeSDPVersion(String sdpVersion) throws CommsException;

  IUpdateModule[] getAllModules() throws CommsException;

  boolean isUpdatingInProgress() throws CommsException;

  boolean isUpdateMode() throws CommsException;

  void writeMCMSoftware(File file) throws CommsException;

  void writeSlaveSoftware(File file) throws CommsException;

  void startProgramme(MODULE type, MODULE_ID id, long serialNo) throws CommsException;

  UpdateStatus getUpdateStatus() throws CommsException;

  ConnectResult connect(String ipAddress, String user, String pass) throws CommsException ;

  void registerModules() throws CommsException;

  void writeConfigFile(File config) throws CommsException;

  /**
   * @param waiting
   * @param newIPAddress
   *          Use a new IP address after restart(This is because restart RTU may
   *          apply a new config that contains a new IP address).
   * @throws CommsException
   */
  void restartRTU(boolean waiting, String newIPAddress) throws CommsException;

  String[] getEthIp() throws CommsException;

  /**
   * Gets RTU information.
   * @return a map that contains entries of RTU information
   * @throws CommsException 
   */
  Map<String, String> getRTUInfo() throws CommsException;

}
