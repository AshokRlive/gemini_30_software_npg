/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms.impl;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


public class UpdateModule implements IUpdateModule {
  private final ModuleInfo info;
  public UpdateModule(ModuleInfo info) {
    super();
    if(info == null)
      throw new NullPointerException("info must not be null");
    this.info = info;
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public MODULE getType() {
    return info.getModuleType();
  }

  @Override
  public String getName() {
    return String.format("%s %s", info.getModuleType(), info.getModuleID());
  }

  @Override
  public FeatureVersion getFeature() {
    return info.getFeatureVersion();
  }

  @Override
  public SystemAPIVersion getCurrentSystemAPI() {
    return info.getSystemAPI();
  }

  @Override
  public long getSerialNo() {
    return info.getSerialNo();
  }
  
  @Override
  public SoftwareVersion getVersion() {
    return info.getVersion();
  }

  @Override
  public MODULE_ID getID() {
    return info.getModuleID();
  }
}

