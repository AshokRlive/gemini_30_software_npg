/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms;

import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;


public class ConnectResult {
  public final String state;
  public final USER_LEVEL level;
  
  public ConnectResult(CTH_RUNNINGAPP state, USER_LEVEL level) {
    if(state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      this.state = "Normal" ;
    }else if(state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      this.state = "Upgrade" ;
    } else {
      this.state = "Unknown";
    }
    
    this.level = level;
  }
}

