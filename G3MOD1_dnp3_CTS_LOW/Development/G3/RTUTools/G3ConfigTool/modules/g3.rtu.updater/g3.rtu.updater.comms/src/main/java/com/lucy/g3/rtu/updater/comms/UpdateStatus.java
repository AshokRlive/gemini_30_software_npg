/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.comms;

import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeStatus;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.UPGRADE_STATE;


public class UpdateStatus {
  public final static UpdateStatus FINISHED = new UpdateStatus(false, (byte) 100);
  public final static UpdateStatus STARTED  = new UpdateStatus(true, (byte) 0);
  public static final UpdateStatus IDLE = new UpdateStatus(false, (byte) 0);

  private final boolean inProgress;
  private final byte progress;
  private String error;
  
  private UpdateStatus(boolean inProgress, byte progress) {
    super();
    this.inProgress = inProgress;
    this.progress = progress;
  }
  
  public boolean isInProgress() {
    return inProgress;
  }
  
  public byte getProgress() {
    return progress;
  }

  public UpdateStatus increase(byte step) {
    int progress = this.progress + step;
    if(progress >= 100) {
      progress = 100;
    }
    
    boolean inProgress = progress == 100;
    
    return inProgress ? new UpdateStatus(inProgress, (byte) progress) : FINISHED;
  }

  public boolean hasError() {
    return error != null;
  }

  public String getError() {
    return error;
  }

  public boolean isFinshed() {
    return !isInProgress();
  } 
  
  public static UpdateStatus create(byte progress) {
    return new UpdateStatus(true, progress);
  }

  public static UpdateStatus createFrom(UpgradeStatus s) {
    UpdateStatus ret = new UpdateStatus(s.isUpgrading(), s.progress);
    if(s.hasError()) {
      UPGRADE_STATE interState = s.getStateEnum();
      ret.error = interState == null ? "Unknown" : interState.getDescription();
    }
    return ret;
  }

}

