/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp;

import com.lucy.g3.common.version.SoftwareVersion;


/**
 *
 */
public class SoftwarePackageUtils {

  public static IModuleSoftware findLatest(IModuleSoftware[] sws) {
    IModuleSoftware latest = null;
    
    for (IModuleSoftware sw : sws) {
      if(latest == null)
        latest = sw;
      else {
        latest = getNewer(latest, sw);
      }
    }
    
    return latest;
  }

  private static IModuleSoftware getNewer(IModuleSoftware sw0, IModuleSoftware sw1) {
    SoftwareVersion v0 = sw0.getSoftwareVersion();
    SoftwareVersion v1 = sw1.getSoftwareVersion();
    if(v0.major() >= v1.major())
      return sw0;
    else if(v0.minor() >= v1.minor())
      return sw0;
    return sw1;
  }

}

