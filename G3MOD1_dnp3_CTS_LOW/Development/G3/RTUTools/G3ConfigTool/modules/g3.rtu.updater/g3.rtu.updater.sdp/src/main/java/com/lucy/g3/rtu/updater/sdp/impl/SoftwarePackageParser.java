/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.SDPSchema.ModuleSoftwareType;
import com.SDPSchema.SupportedFeatureType;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.schema.support.SDPSchemaFile;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;
import com.lucy.g3.xml.validation.XmlValidate;


public class SoftwarePackageParser {
  public static final String SDP_XML_NAME = SoftwarePackage.SDP_XML_NAME;

  private Logger log = Logger.getLogger(SoftwarePackageParser.class);
  
  private com.SDPSchema.SDPContentType root;
  
  public SoftwarePackage parseZipFile(File zipSdpFile, boolean isBootLoader) throws Exception{
    if (zipSdpFile == null || !zipSdpFile.exists()) {
      throw new IllegalArgumentException("File \"" + zipSdpFile + "\" does not exist!");
    }
  
    ModuleSoftware[] entries;
    String version;
    String[] configFiles;
    
    byte[] sdpXmlBytes = getSDPXmlAsBytes(zipSdpFile);
    validateSDPXml(sdpXmlBytes);
    
    root = getXMLRoot(sdpXmlBytes);
    
    version = parseSDPVersion();
    entries = parseSoftwareEntries(isBootLoader);
    configFiles = parseConfigFileNames();
    
    SoftwarePackage pkg = new SoftwarePackage(zipSdpFile, version, entries, configFiles);
    for (int i = 0; i < entries.length; i++) {
      entries[i].setPackage(pkg);
    }
    
    return pkg;
  }
  
  /* Validate SDP XMl against schema*/
  private void validateSDPXml(byte[] sdpXmlBytes) {
    ByteArrayInputStream bistream = null;
    try {
      bistream = new ByteArrayInputStream(sdpXmlBytes);
      new XmlValidate(SDPSchemaFile.FILE_PATH).validate(bistream);
    } catch (Exception e) {
      log.error("Invalid format of \"" + SDP_XML_NAME + "\". " + e.getMessage());
    } finally {
      try {
        bistream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private SLAVE_IMAGE_TYPE parseImageType(ModuleSoftwareType xml) {
    if (xml.SlaveImageType.exists()) {
      SLAVE_IMAGE_TYPE imageTypeEnum  = SLAVE_IMAGE_TYPE.forValue(xml.SlaveImageType.getValue());
      if(imageTypeEnum  == null)
        imageTypeEnum = SLAVE_IMAGE_TYPE.SLAVE_IMAGE_TYPE_APPLICATION;
      return imageTypeEnum;
      
    } else {
      return null;
    }
  }
  
  private ModuleSoftware[] parseSoftwareEntries(boolean isBootLoader) {
    ArrayList<ModuleSoftware> allEntries = new ArrayList<>();
    
    int major, minor, patch;
    for (int i = 0; i < root.ModuleSoftware.count(); i++) {
      ModuleSoftwareType xml_sw = root.ModuleSoftware.at(i);
  
      // System API
      major = (int) xml_sw.APIMajor.getValue();
      minor = (int) xml_sw.APIMinor.getValue();
      SystemAPIVersion api = new SystemAPIVersion(major, minor);
  
      // Supported features
      FeatureVersion[] features = new FeatureVersion[xml_sw.SupportedFeature.count()];
      for (int j = 0; j < features.length; j++) {
        SupportedFeatureType xml_feature = xml_sw.SupportedFeature.at(j);
        major = (int) xml_feature.Major.getValue();
        minor = (int) xml_feature.Minor.getValue();
        features[j] = new FeatureVersion(major, minor);
      }
  
      // Software version
      major = (int) xml_sw.Major.getValue();
      minor = (int) xml_sw.Minor.getValue();
      patch = (int) xml_sw.Patch.getValue();
      VERSION_TYPE relType = VERSION_TYPE.forValue(xml_sw.RelType.getValue());
      SLAVE_IMAGE_TYPE imageType = parseImageType(xml_sw);
      MODULE type = MODULE.forValue(xml_sw.ModuleType.getValue());
      SoftwareVersion version = new SoftwareVersion(new ReleaseType(relType.getDescription(), relType.getValue()), major, minor, patch);
      String fileName = xml_sw.FileName.getValue();
  
      if(imageType == null) {
        allEntries.add(new ModuleSoftware(fileName, fileName, type, version, api, isBootLoader, features));
      } else {
        if(isBootLoader == isBootloader(imageType))
          allEntries.add(new ModuleSoftware(fileName, fileName, type, version, api, imageType, features));
      }
    }
  
    return allEntries.toArray(new ModuleSoftware[allEntries.size()]);
  }

  private static boolean isBootloader(SLAVE_IMAGE_TYPE imageType) {
    return imageType != SLAVE_IMAGE_TYPE.SLAVE_IMAGE_TYPE_APPLICATION;
  }

  private String[] parseConfigFileNames() {
    String[] configFileNames = new String[root.ConfigFile.count()];
    for (int i = 0; i < configFileNames.length; i++) {
      configFileNames[i] = root.ConfigFile.at(i).Location.getValue();
    }
    return configFileNames;
  }

  
  private String parseSDPVersion() {
    String version;
    
    if (root.sdpversion.exists()) {
      version = root.sdpversion.getValue();
    } else {
      version = "";
    }
    
    return version;
  }

  
  public static byte[] getSDPXmlAsBytes(File sdpFile) throws IOException {
    byte[] sdpXmlBytes = ZipUtil.getZipEntryStreamByName(SDP_XML_NAME, sdpFile);
    if (sdpXmlBytes == null || sdpXmlBytes.length <= 0) {
      throw new IllegalArgumentException("\"" + SDP_XML_NAME
          + "\" is missing in the package: \"" + sdpFile.getName() + "\"");
    }
    
    return sdpXmlBytes;
  }
  
  /**
   * Gets the XML root.
   *
   * @param sdpXmlBytes
   *          the sdp xml bytes
   * @return the XML root
   * @throws Exception
   *           the exception
   */
  public static com.SDPSchema.SDPContentType getXMLRoot(byte[] sdpXmlBytes) throws Exception {
    com.SDPSchema.SDPSchema2 doc = com.SDPSchema.SDPSchema2.loadFromBinary(sdpXmlBytes);
    com.SDPSchema.SDPContentType root = doc.SDPContent.first();
    return root;
  }

  public static SoftwarePackage parse(File zipSdpFile, boolean bootloader) throws Exception {
    return new SoftwarePackageParser().parseZipFile(zipSdpFile, bootloader);
  }
}

