/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp;

import java.io.File;
import java.io.IOException;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;


public interface IModuleSoftware {
  
  ISoftwarePackage getPackage();
  
  MODULE getSupportedType();
  
  FeatureVersion[] getSupportedFeature();

  SystemAPIVersion getSupportedSystemAPI();
  
  SoftwareVersion getSoftwareVersion();

  File extractToDir(File dir) throws IOException;

  String getName();

  SLAVE_IMAGE_TYPE getImageType();
  
  boolean isBootloader();
}

