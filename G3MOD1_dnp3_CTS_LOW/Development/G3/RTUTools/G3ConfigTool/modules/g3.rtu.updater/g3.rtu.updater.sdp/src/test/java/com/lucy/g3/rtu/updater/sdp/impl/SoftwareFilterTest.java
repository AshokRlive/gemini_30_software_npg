
package com.lucy.g3.rtu.updater.sdp.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;


public class SoftwareFilterTest {
  
  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAccept() {
    final SystemAPIVersion api = new SystemAPIVersion(0, 5);
    final FeatureVersion feature = new FeatureVersion(0, 1);
    final  MODULE type = MODULE.MODULE_SCM_MK2;
    
    SoftwareFilter filter = new SoftwareFilter(type, feature, api);
    VERSION_TYPE vType = VERSION_TYPE.forValue((short)0);
    ReleaseType releaseType = new ReleaseType(vType.getDescription(), vType.getValue());
    ModuleSoftware sw = new ModuleSoftware("SCM", type, new SoftwareVersion(releaseType, 0,1,0), api, feature);
    assertTrue(filter.isAccepted(sw));
    
    sw = new ModuleSoftware("SCM", type, new SoftwareVersion(releaseType, 0,1,0), new SystemAPIVersion(0, 6), feature);
    assertTrue(filter.isAccepted(sw));
    
    sw = new ModuleSoftware("SCM", type, new SoftwareVersion(releaseType, 0,1,0), new SystemAPIVersion(0, 4), feature);
    assertFalse(filter.isAccepted(sw));
  }

}

