/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp.impl;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.updater.sdp.IModuleSoftware;
import com.lucy.g3.rtu.updater.sdp.ISoftwarePackage;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;


public class ModuleSoftware implements IModuleSoftware {
  private ISoftwarePackage pkg;
  
  private final MODULE type;
  private  final SoftwareVersion version;
  private final SystemAPIVersion systemAPI;
  private final SLAVE_IMAGE_TYPE imageType;
  private final FeatureVersion[] feature;
  private final String relativePath;
  private final String name;
  private final boolean isBootloader;
 
  ModuleSoftware(String name, MODULE type, SoftwareVersion version, SystemAPIVersion systemAPI, 
      FeatureVersion... feature) {
    this(null, name, type, version, systemAPI, false, feature);
  }
      
  public ModuleSoftware(String relativePath, 
      String name, MODULE type, SoftwareVersion version, SystemAPIVersion systemAPI, SLAVE_IMAGE_TYPE imageType,
      FeatureVersion... feature) {
    super();
    this.relativePath = relativePath;
    this.imageType = Preconditions.checkNotNull(imageType, "imageType must not be blank");
    this.isBootloader = imageType != SLAVE_IMAGE_TYPE.SLAVE_IMAGE_TYPE_APPLICATION;
    this.name = Preconditions.checkNotBlank(name, "name must not be blank");
    this.type = Preconditions.checkNotNull(type, "type must not be null");
    this.version = Preconditions.checkNotNull(version, "version must not be null");
    this.systemAPI = Preconditions.checkNotNull(systemAPI, "systemAPI must not be null");
    Preconditions.checkNotNull(feature, "feature must not be null");
    this.feature = Arrays.copyOf(feature, feature.length);
  }
  
  public ModuleSoftware(String relativePath, 
      String name, MODULE type, SoftwareVersion version, SystemAPIVersion systemAPI, boolean isBootloader,
      FeatureVersion... feature) {
    super();
    this.relativePath = relativePath;
    this.imageType = null;
    this.isBootloader = isBootloader;
    
    this.name = Preconditions.checkNotBlank(name, "name must not be blank");
    this.type = Preconditions.checkNotNull(type, "type must not be null");
    this.version = Preconditions.checkNotNull(version, "version must not be null");
    this.systemAPI = Preconditions.checkNotNull(systemAPI, "systemAPI must not be null");
    Preconditions.checkNotNull(feature, "feature must not be null");
    this.feature = Arrays.copyOf(feature, feature.length);
  }

  void setPackage(ISoftwarePackage pkg) {
    this.pkg = pkg;
  }
  
  @Override
  public String getName() {
    return name;
  }

  
  private String getRelativePath() {
    return relativePath;
  }

  @Override
  public MODULE getSupportedType() {
    return type;
  }
  
  @Override
  public SLAVE_IMAGE_TYPE getImageType() {
    return imageType;
  }

  @Override
  public FeatureVersion[] getSupportedFeature() {
    return Arrays.copyOf(feature, feature.length);
  }

  @Override
  public SystemAPIVersion getSupportedSystemAPI() {
    return systemAPI;
  }

  @Override
  public SoftwareVersion getSoftwareVersion() {
    return version;
  }

  @Override
  public File extractToDir(File dir) throws IOException {
    Preconditions.checkNotNull(dir, "dir must not be null");
    if(pkg == null)
      throw new IOException("sdp is not specified");
    
    if(relativePath == null)
      throw new IOException("relativePath is not specified");
    
    pkg.extract(getRelativePath(), dir.getAbsolutePath());
    
    return new File(dir, getRelativePath());
  }

  @Override
  public ISoftwarePackage getPackage() {
    return pkg;
  }

  @Override
  public boolean isBootloader() {
    return isBootloader;
  }

}

