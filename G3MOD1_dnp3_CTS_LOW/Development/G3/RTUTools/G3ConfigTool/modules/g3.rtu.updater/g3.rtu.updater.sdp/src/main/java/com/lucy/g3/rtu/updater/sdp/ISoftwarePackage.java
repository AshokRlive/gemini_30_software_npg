/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp;

import java.io.File;
import java.io.IOException;


/**
 * The API of G3 software package. 
 */
public interface ISoftwarePackage {
  String SDP_EXTENSION = ".sdp";
  
  File getFile();
  
  String getVersion();
  
  /**
   * Extract a software from this package to a destination.
   * @param relativePath the relative path of the software in this package. 
   * @param destination  destination file path to be extracted to.
   */
  void extract(String relativePath, String destination) throws IOException;

  IModuleSoftware[] getAvailable(ISoftwareFilter filter);

  String printContent();

  IModuleSoftware getLastest(ISoftwareFilter filter);
  
}

