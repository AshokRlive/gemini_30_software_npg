/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp.impl;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.updater.sdp.IModuleSoftware;
import com.lucy.g3.rtu.updater.sdp.ISoftwareFilter;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;


public class SoftwareFilter implements ISoftwareFilter {
  private final MODULE type;
  private final FeatureVersion feature;
  private final SystemAPIVersion api;
  

  public SoftwareFilter(MODULE type, FeatureVersion feature, SystemAPIVersion api) {
    super();
    this.type = Preconditions.checkNotNull(type,"type must not be null");;
    this.feature = Preconditions.checkNotNull(feature,"feature must not be null");
    this.api = api;
  }


  @Override
  public boolean isAccepted(IModuleSoftware sw) {
    MODULE type = sw.getSupportedType();
    FeatureVersion[] feature = sw.getSupportedFeature();
    SystemAPIVersion api = sw.getSupportedSystemAPI();
    
    // Check type
    if (this.type != type) {
      return false;
    }
    
    // Check feature
    boolean featureFound = false;
    for (int i = 0; i < feature.length; i++) {
      if (feature[i].equals(this.feature)) {
        featureFound = true;
        break;
      }
    }
    if (featureFound == false) {
      return false;
    }

    // Check system API
    if(this.api != null) {
      if(this.api.major() != api.major() || this.api.minor() > api.minor())
        return false;
    }
    
    
    return true;
  }

}

