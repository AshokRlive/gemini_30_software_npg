/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

class UpdaterArgParser {
  private static final String DEFAULT_JAR_NAME = "G3ConfigTool.jar";
  
  public static final String OPTION_UPDATE = "update";
  public static final String OPTION_IP = "ip";
  public static final String OPTION_CONFIG = "config";
  public static final String OPTION_SDPBOOT = "sdpboot";
  public static final String OPTION_SDP = "sdp";
  public static final String OPTION_PASS0 = "pass0";
  public static final String OPTION_USER0 = "user0";
  public static final String OPTION_PASS1 = "pass1";
  public static final String OPTION_USER1 = "user1";
  public static final String OPTION_REG   = "register";
  public static final String OPTION_LOG_LEVEL   = "loglevel";
  public static final String OPTION_IGNORE_ERROR   = "ignoreError";

  
  private final Options options;
  private CommandLine line;

  private String ip;
  private File sdp;
  private File sdpboot;
  private File config;
  private String pass0;
  private String pass1;
  private String user0;
  private String user1;

  private boolean register;
  

  public UpdaterArgParser() {
    options = new Options();
    
    // ================== Mandatory Options ===================
    
    Option update = Option.builder(OPTION_UPDATE)
        .longOpt(OPTION_UPDATE)
        .desc("[mandatory] update RTU software")
        .required()
        .build();
    
    Option ip = Option.builder(OPTION_IP)
        .required()
        .hasArg().argName("IP").type(String.class)
        .desc("[mandatory] ip address of RTU to be connected to")
        .build();
    
    Option user0 = Option.builder(OPTION_USER0)
        .required()
        .hasArg().argName("USERNAME").type(String.class)
        .desc("[mandatory] user name used to login to RTU")
        .build();
    
    Option pass0 = Option.builder(OPTION_PASS0)
        .required()
        .hasArg().argName("PASSWORD").type(String.class)
        .desc("[mandatory] password used to login to RTU")
        .build();
    
    
    // ================== Optional Options===================
    
    Option sdp = Option.builder(OPTION_SDP)
        .desc("[optional ] sdp file for programming the application of all RTU modules")
        .hasArg().argName("PATH").type(File.class)
        .build();
    
    Option sdpboot = Option.builder(OPTION_SDPBOOT)
        .desc("[optional ] sdp file for programming the bootloader of all RTU modules")
        .hasArg().argName("PATH").type(File.class)
        .build();
    
    Option config = Option.builder("c")
        .longOpt(OPTION_CONFIG)
        .desc("[optional ] configuration file path to be written to the RTU.")
        .hasArg().argName("PATH").type(File.class)
        .build();
    
    Option user1 = Option.builder(OPTION_USER1)
        .hasArg().argName("USERNAME").type(String.class)
        .desc("[optional ] user name used to login to the RTU after software update")
        .build();
    
    Option pass1 = Option.builder(OPTION_PASS1)
        .hasArg().argName("PASSWORD").type(String.class)
        .desc("[optional ] password used to login to the RTU after software update")
        .build();
    
    Option reg = Option.builder("r")
        .longOpt(OPTION_REG)
        .desc("[optional ] register all modules after software update")
        .build();
    
    Option log = Option.builder("l")
        .longOpt(OPTION_LOG_LEVEL)
        .hasArg().argName("LEVEL")
        .desc("set log level to one of: DEBUG|INFO|WARN|ERROR|FATAL|OFF|ALL")
        .build();
    
    Option ignoreErr = Option.builder("i")
        .longOpt(OPTION_IGNORE_ERROR)
        .desc("Continue software update process even if there is an error detected.")
        .build();
    
    
    options.addOption(ip);
    options.addOption(update);
    options.addOption(sdp);
    options.addOption(sdpboot);
    options.addOption(config);
    options.addOption(user0);
    options.addOption(user1);
    options.addOption(pass0);
    options.addOption(pass1);
    options.addOption(reg);
    options.addOption(log);
    options.addOption(ignoreErr);
  }
  
  public void printHelp(){
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(
        800, 
        "java -jar " + DEFAULT_JAR_NAME, 
        "========== Parameters ===========", 
        options, 
        "", 
        true);
  }
  
  
  public void parse(String[] args) throws ParseException {
    parse(args,false);
  }
  
  public void parse(String[] args, boolean checkFileExist) throws ParseException {
    CommandLineParser parser = new DefaultParser();
    line = parser.parse(options, args, false);
    
    ip = (String) line.getParsedOptionValue(OPTION_IP);
    sdp = (File) line.getParsedOptionValue(OPTION_SDP);
    sdpboot = (File) line.getParsedOptionValue(OPTION_SDPBOOT);
    config = (File) line.getParsedOptionValue(OPTION_CONFIG);
    user0 = (String) line.getParsedOptionValue(OPTION_USER0);
    user1 = (String) line.getParsedOptionValue(OPTION_USER1);
    pass0 = (String) line.getParsedOptionValue(OPTION_PASS0);
    pass1 = (String) line.getParsedOptionValue(OPTION_PASS1);
    register = line.hasOption(OPTION_REG);
    
    if(checkFileExist) {
      checkFileExist(OPTION_SDP, sdp);
      checkFileExist(OPTION_SDPBOOT, sdpboot);
      checkFileExist(OPTION_CONFIG, config);
    }
  }
  
  private void checkFileExist(String opt, File file) throws ParseException {
    if(file != null && !file.exists()) {
      throw new ParseException(opt + " file not found at: " + line.getOptionValue(opt));
    }
  }

  String getOptionValue(String opt) {
    return line.getOptionValue(opt);
  }

  
  
  public String getIp() {
    return ip;
  }

  
  
  public File getSdp() {
    return sdp;
  }

  
  
  public File getSdpboot() {
    return sdpboot;
  }

  
  
  public String getPass0() {
    return pass0;
  }

  
  
  public String getPass1() {
    return pass1;
  }

  
  
  public String getUser0() {
    return user0;
  }

  
  
  public String getUser1() {
    return user1 == null ? user0 : user1;
  }

  
  
  public boolean isRegister() {
    return register;
  }

  
  public File getConfig() {
    return config;
  }

  public boolean hasLog() {
    return line.hasOption(OPTION_LOG_LEVEL);
  }
  
  public String getLogLevel() {
    return  line.getOptionValue(OPTION_LOG_LEVEL);
  }
  
  public boolean isIgnoreError() {
    return line.hasOption(OPTION_IGNORE_ERROR);
  }

  @Override
  public String toString() {
    return "Options [ip=" + ip + ", user0=" + user0 + ",pass0=" + pass0 
        +", user1=" + user1 +  ", pass1=" + pass1 + ",  register=" + register 
        + ",sdp=" + sdp + ", sdpboot=" + sdpboot + ", config=" + config +"]";
  }
}
