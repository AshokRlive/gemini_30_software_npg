/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.log4j.Logger;

import com.g3schema.g3schema2;
import com.g3schema.ns_port.EthernetPortConfT;
import com.g3schema.ns_port.PortsT;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.rtu.updater.comms.CommsException;
import com.lucy.g3.rtu.updater.comms.ConnectResult;
import com.lucy.g3.rtu.updater.comms.IRTUComms;
import com.lucy.g3.rtu.updater.engine.IUpdateEngine;
import com.lucy.g3.rtu.updater.engine.IUpdateEngine.UpdateOption;
import com.lucy.g3.rtu.updater.engine.SoftwareMissingException;
import com.lucy.g3.rtu.updater.engine.UpdateException;
import com.lucy.g3.rtu.updater.sdp.ISoftwarePackage;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwarePackage;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwarePackageParser;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;


class UpdateController {
  private Logger log = Logger.getLogger(UpdateController.class);
  
  private final IUpdateEngine    engine;
  private final IRTUComms        comms;
  private String rtuIpAddress; 
  
  public UpdateController(IUpdateEngine engine, IRTUComms comms) {
    super();
    this.engine = engine;
    this.comms = comms;
  }

  
  public void updateSoftware(ISoftwarePackage sdp, ISoftwarePackage sdpboot) 
      throws CommsException, InterruptedException, UpdateException, IOException  {
    if(sdp == null && sdpboot == null) {
      log.warn("Software update is skipped cause SDP was not specified!");
      return;
    }
    engine.enterUpdateMode();
    
    log.info("=====  Start updating RTU software ===== ");
    if(sdpboot != null) {
      log.info("Updating modules' bootloader.");

      engine.programme(UpdateOption.BOOTLOADER_ONLY, sdpboot);
    }
    
    if(sdp != null) {
      log.info("Updating modules' application.");
      
      engine.programme(UpdateOption.ALL, sdp);
    }
  
    engine.exitUpdateMode();
    log.info("===== End of updating RTU software ===== ");
  }

  public void updateConfig(File config) throws CommsException, IOException, UpdateException {
    log.info("Updating configuration...");

    /* Find new RTU IP address from config*/
    String newIp = findNewIp(config, comms.getEthIp(), rtuIpAddress);
    
    engine.writeConfig(config);
    
    log.info("Configuration updated.");
    
    if(newIp != null && !rtuIpAddress.equals(newIp)) {
      log.info("Found new RTU Address from config: " + newIp);
      rtuIpAddress = newIp;
    } else if(newIp == null){
      log.warn("Unable to find new RTU Address from config!");
    }
  }

  /**
   * Finds configured new IP address in configuration file.
   * @param config configuration file to be applied to RTU.
   * @param existingRtuIPs existing IP address of RTU eth ports. 
   * @param connectedIp current connected IP address
   * @return found new IP address 
   * @throws IOException
   */
  static String findNewIp(File config, String[] existingRtuIPs, String connectedIp) throws IOException {
    g3schema2 doc = readConfigFile(config);
    PortsT xmlPorts = doc.configuration.first().ports.first();
    
    /*Find connected port via IP address*/
    ETHERNET_PORT connectedPort = null;
    for (int id = 0; existingRtuIPs != null && id < existingRtuIPs.length; id++) {
      if (EqualsUtil.areEqual(existingRtuIPs[id], connectedIp)) {
        connectedPort = ETHERNET_PORT.forValue(id);
        break;
      }
    }
    if(connectedPort == null)
      return null;
    
    int ethsNum = xmlPorts.ethernet.count();
    for (int i = 0; i < ethsNum; i++) {
      EthernetPortConfT xmlEth = xmlPorts.ethernet.at(i);
      if(xmlEth.portName.exists() && connectedPort.name().equals(xmlEth.portName.getValue())) {
        if(xmlEth.enabled.exists() && xmlEth.enabled.getValue() == true) {
          if(xmlEth.ipv4Setting.exists()) {
            if(xmlEth.ipv4Setting.first().staticIP.exists()) {
              return xmlEth.ipv4Setting.first().staticIP.first().ipaddress.getValue();
            }
          }
        }
      }
    }
    return null;
  }


  private static g3schema2 readConfigFile(File config) throws IOException {
    g3schema2 doc = null;
    if (config.getAbsolutePath().endsWith(G3Files.SUFFFIX_GZIP)) {
     /*Read compressed config*/
      byte[] os = ZipUtil.ungzip(config.getAbsolutePath());
      try {
        doc = com.g3schema.g3schema2.loadFromString(new String(os, StandardCharsets.UTF_8));
      } catch (Exception e) {
        throw new IOException("Failed to read config file");
      }
      
    } else {
      /*Read XML config*/
      try {
        doc = com.g3schema.g3schema2.loadFromFile(config.getPath());
      } catch (Exception e) {
        throw new IOException("Failed to read config file");
      }
    }
    
    return doc;
  }
  
  
  public UpdateResult updateAll(UpdaterArgParser options) {
    UpdateResult ret = new UpdateResult();
    rtuIpAddress = options.getIp();
        
    /* Load software package*/
    ISoftwarePackage  sdp     = null; 
    ISoftwarePackage  sdpboot = null;
    try {
      sdpboot = createSDP(options.getSdpboot(), true);
      sdp = createSDP(options.getSdp(), false);
      
    } catch (Exception e) {
      log.error("Failed to load SDP cause: " + e.getMessage());
      return ret.setError(ErrorCode.INVALID_SDP);
    }
    
    /* Connect to RTU*/
    if(!connectToRTU(comms, rtuIpAddress, options.getUser0(), options.getPass0())) {
      return ret.setError(ErrorCode.COMMS);
    }
    
    /* Read RTU info*/
    try {
      log.info("Reading RTU information...");
      ret.setInfoBeforeUpdate(comms.getRTUInfo());
      ret.setModulesBeforeUpdate(comms.getAllModules());
    } catch (CommsException e1) {
      log.error("Failed to read RTU information", e1);
      return ret.setError(ErrorCode.COMMS);
    }
    
    /* Update software*/
    try {
      updateSoftware(sdp, sdpboot);
    } catch (Exception e) {
      log.error("Failed to update software cause: " + e.getMessage());
      
      if(e instanceof CommsException)
        return ret.setError(ErrorCode.COMMS);
      
      else if(e instanceof SoftwareMissingException) {
        return ret.setError(ErrorCode.MISSING_SW);
        
      } else {
        return ret.setError(ErrorCode.UPDATE);
      } 
    }
    
    boolean requireRestart = false;
    
    /* Register modules*/
    if(options.isRegister()) {
      try {
        log.info("Reigstering modules...");
        comms.registerModules();
        requireRestart = true;
        ret.addStatus("Module Registerred",true);
        log.info("Modules registerred.");
        
      } catch (CommsException e) {
        ret.addStatus("Module Registerred",false);
        log.error("Failed to register modules cause: " + e.getMessage());
        return ret.setError(ErrorCode.REGISTER_MODULE);
      }
    }
    
    /* Update config*/
    File conf = options.getConfig();
    if(conf != null) {
      try {
        updateConfig(conf);
        requireRestart = true;
        ret.addStatus("Config Updated",true);
      } catch (Exception e) {
        ret.addStatus("Config Updated",false);
        log.error("Failed to update config cause: " + e.getMessage());
        return ret.setError(ErrorCode.INVALID_CONFIG);
      }
    }
    
    /* Restart RTU*/
    if(requireRestart) {
      try {
        log.info("Restarting RTU...");
        comms.restartRTU(true, rtuIpAddress);
        
        // Wait G3 module to be initialised.
        Thread.sleep(3000);
        
      } catch (CommsException|InterruptedException e) {
        log.error("Failed to restart RTU", e);
        return ret.setError(ErrorCode.RESTART_FAILURE);
      } 
    }
    
    /* Connect to RTU*/
    log.info("Connecting RTU...");
    if(! connectToRTU(comms, rtuIpAddress, options.getUser1(), options.getPass1())) {
      return ret.setError(ErrorCode.COMMS);
    }
    
    /* Read RTU info*/
    try {
      log.info("Reading RTU information...");
      ret.setInfoAfterUpdate(comms.getRTUInfo());
      ret.setAfterUpdate(comms.getAllModules());
    } catch (CommsException e1) {
      log.error("Failed to read RTU information", e1);
      return ret.setError(ErrorCode.COMMS);
    }
    
    log.info("Update completed!");
    return ret.setError(ErrorCode.NONE);
  }

  private boolean connectToRTU(IRTUComms comms, String ip, String user, String pass) {
    try {
      log.info(String.format("Connecting to RTU. IP:%s User:%s", ip, user));
      ConnectResult result = comms.connect(ip, user, pass);
      log.info(String.format("Connected to RTU successfully. RTU State:%s User:%s Role:%s", 
          result.state, user, result.level));
      return true;
    } catch (Exception e) {
      log.error("Failed to connect to RTU cause:" + e.getMessage());
      return false;
    }
  }
  
  private ISoftwarePackage createSDP(File sdpFile, boolean bootloader) throws Exception {
    SoftwarePackage sdp = null;
    if (sdpFile != null) {
      sdp = SoftwarePackageParser.parse(sdpFile, bootloader);
    }

    if (sdp != null) {
      StringBuilder sb = new StringBuilder();
      if(bootloader)
        sb.append("Open Bootloader SDP file: ");
      else
        sb.append("Open SDP file: ");
      
      sb.append(sdpFile.getAbsolutePath());
      sb.append("\n");
      sb.append(sdp.printContent());
      log.info(sb.toString());
    }
    
    return sdp;
  }
}

