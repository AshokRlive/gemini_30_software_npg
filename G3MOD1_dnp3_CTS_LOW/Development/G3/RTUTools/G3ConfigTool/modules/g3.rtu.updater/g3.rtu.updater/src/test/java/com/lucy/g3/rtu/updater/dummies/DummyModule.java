package com.lucy.g3.rtu.updater.dummies;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.ReleaseType;
import com.lucy.g3.common.version.SoftwareVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.versions.VERSION_TYPE;


public class DummyModule implements IUpdateModule {
  private final MODULE type;
  private final String name;
  private final MODULE_ID id;
  private final long serialNo;
  private final FeatureVersion feature;
  private final SystemAPIVersion systemAPI;
  
  public DummyModule(MODULE type, MODULE_ID id, String name, int serialNo, FeatureVersion feature, SystemAPIVersion systemAPI) {
    super();
    this.type = type;
    this.name = name;
    this.id = id;
    this.serialNo = serialNo;
    this.feature = feature;
    this.systemAPI = systemAPI;
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public MODULE getType() {
    return type;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public FeatureVersion getFeature() {
    return feature;
  }

  @Override
  public SystemAPIVersion getCurrentSystemAPI() {
    return systemAPI;
  }

  @Override
  public long getSerialNo() {
    return serialNo;
  }

  @Override
  public MODULE_ID getID() {
    return id;
  }

  @Override
  public SoftwareVersion getVersion() {
    return new SoftwareVersion(new ReleaseType(VERSION_TYPE.VERSION_TYPE_TEST.getDescription(),
        VERSION_TYPE.VERSION_TYPE_TEST.getValue()), 0,0,0);
  }

}

