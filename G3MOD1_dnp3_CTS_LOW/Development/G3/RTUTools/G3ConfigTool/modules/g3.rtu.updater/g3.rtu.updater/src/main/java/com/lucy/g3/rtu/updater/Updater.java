
package com.lucy.g3.rtu.updater;

import java.io.File;

import org.apache.commons.cli.ParseException;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.jdesktop.application.Application;

import com.lucy.g3.common.manifest.ManifestInfo;
import com.lucy.g3.rtu.updater.comms.IRTUComms;
import com.lucy.g3.rtu.updater.comms.impl.RTUUpdateComms;
import com.lucy.g3.rtu.updater.engine.IUpdateEngine;
import com.lucy.g3.rtu.updater.engine.impl.UpdateEngine;

public class Updater {

  private Logger log = Logger.getLogger(Updater.class);

  private UpdaterArgParser options = new UpdaterArgParser();

  private File tempDir;
  private UpdateController controller;
  private ErrorCode err;


  public void initialize(String[] args) {
    try {
      options.parse(args, true);
    } catch (ParseException e) {
      System.out.println("Unable to parse command line options: " + e.getMessage() + "\n");
      options.printHelp();
      err = ErrorCode.ARGS;
      return;
    }

    /* Set log level */
    Level level = Level.toLevel(options.getLogLevel(), null);
    if (level != null) {
      Logger.getRootLogger().setLevel(level);
    }

    /* Log options */
    log.info(options.toString());

    /* Customize console log layout */
    Appender appd = Logger.getRootLogger().getAppender("consoleAppender");
    PatternLayout layout = new PatternLayout("%d{dd-MMM-yyyy [HH:mm:ss]} [%-5p] %c{1} - %m%n");
    appd.setLayout(layout);

    File localDir = Application.getInstance().getContext().getLocalStorage().getDirectory();
    tempDir = getTempDir(localDir);
    IRTUComms comms = new RTUUpdateComms();
    IUpdateEngine engine = new UpdateEngine(tempDir, comms, options.isIgnoreError());
    controller = new UpdateController(engine, comms);
  }

  public static void printHelp() {
    new UpdaterArgParser().printHelp();

    // Space
    System.out.println("");

    // Print error code
    System.out.println(ErrorCode.printAsString());
  }

  public void run() {
    if (err == null || err == ErrorCode.NONE) {
      ManifestInfo vinfo = new ManifestInfo("Updater");
      vinfo.readFromManifest();
      log.info("ConfigTool SVN Revision: " + vinfo.getSVNRev());

      UpdateResult result = controller.updateAll(options);
      result.printReport();
      err = result.getError();
    }
  }

  private static File getTempDir(File localDir) {
    return new File(localDir.getPath() + "/SDP" + System.currentTimeMillis());
  }

  public int getErrCode() {
    return err == null ? 0 : err.errCode();
  }
  
  public String getErrMessage() {
    return err == null ? "No Error": err.getDescription();
  }

  /**
   * Runthe application in current thread.
   * 
   * @param args
   */
//  public static int run(String[] args) {
//    
//  }
}
