/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.dummies;

import static com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE.MODULE_FPM;
import static com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE.MODULE_MCM;
import static com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE.MODULE_PSM;
import static com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE.MODULE_SCM_MK2;
import static com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID.MODULE_ID_0;
import static com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID.MODULE_ID_1;

import java.io.File;
import java.util.Map;

import org.apache.log4j.Logger;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.updater.comms.CommsException;
import com.lucy.g3.rtu.updater.comms.ConnectResult;
import com.lucy.g3.rtu.updater.comms.IRTUComms;
import com.lucy.g3.rtu.updater.comms.IUpdateModule;
import com.lucy.g3.rtu.updater.comms.UpdateStatus;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;


public class DummyRTUComms implements IRTUComms {
  private Logger log = Logger.getLogger(DummyRTUComms.class);
  private boolean updateMode;
  
  @Override
  public void setUpdateMode(boolean enabled, boolean waiting) throws CommsException, InterruptedException {
    log.info("Set Update Mode Enabled:" + enabled);
    this.updateMode = enabled;
    
    if(waiting) {
      Thread.sleep(100);
    }
  }

  @Override
  public void writeSDPVersion(String sdpVersion) throws CommsException {
    log.info("Written SDP version:" + sdpVersion);
  }

  private IUpdateModule[] modules;
  
  @Override
  public IUpdateModule[] getAllModules() throws CommsException {
    if(modules == null) {
      modules = new IUpdateModule[5];
      modules[0] = new DummyModule(MODULE_MCM,     MODULE_ID_0, "MCM ", 123, new FeatureVersion(1, 0), new SystemAPIVersion(0, 5));
      modules[1] = new DummyModule(MODULE_SCM_MK2, MODULE_ID_0, "SCM1", 234, new FeatureVersion(0, 1), new SystemAPIVersion(0, 5));
      modules[2] = new DummyModule(MODULE_SCM_MK2, MODULE_ID_1, "SCM2", 345, new FeatureVersion(0, 1), new SystemAPIVersion(0, 5));
      modules[3] = new DummyModule(MODULE_PSM,     MODULE_ID_0, "PSM ", 456, new FeatureVersion(0, 1), new SystemAPIVersion(0, 5));
      modules[4] = new DummyModule(MODULE_FPM,     MODULE_ID_0, "FPM ", 567, new FeatureVersion(1, 0), new SystemAPIVersion(0, 5));
    }
    return modules;
  }

  @Override
  public boolean isUpdatingInProgress() {
    return status.isInProgress();
  }

  @Override
  public boolean isUpdateMode() {
    return updateMode;
  }

  @Override
  public void writeMCMSoftware(File file) {
    log.info("Transferring MCM file:"+file);
  }

  @Override
  public void writeSlaveSoftware(File file) {
    log.info("Transferring Slave file:"+file);
  }

  private UpdateStatus status = UpdateStatus.IDLE;
  
  @Override
  public void startProgramme(MODULE type, MODULE_ID id, long serialNo) {
    log.info("Start programming:"+type+" id:"+id+" serial:"+serialNo);
    status = UpdateStatus.STARTED;
  }

  
  @Override
  public UpdateStatus getUpdateStatus() {
    if(status.isInProgress())
      status =  status.increase((byte) 10);
    
    return status;
  }


  @Override
  public ConnectResult connect(String ipAddress, String user, String pass) throws CommsException {
    log.info("Connected to RTU");
    return null;
  }

  @Override
  public void registerModules() {
    log.info("registerModules");
  }

  @Override
  public void writeConfigFile(File config) {
    log.info("writeConfigFile");
  }

  @Override
  public String[] getEthIp() {
    return null;
  }

  @Override
  public void restartRTU(boolean waiting, String newIPAddress) throws CommsException {
    
  }

  @Override
  public Map<String, String> getRTUInfo() {
    return null;
  }


}

