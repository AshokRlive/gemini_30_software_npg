/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater;


/**
 * Update Error Enum.
 */
enum ErrorCode {
  NONE(0, "OK"),
  INVALID_SDP(1, "Invalid SDP file"),
  REGISTER_MODULE(2, "Register Module Error"),
  UPDATE(3, "Generic Update Error"),
  COMMS(4, "Comms Error"),
  ARGS(5, "Arguments Error"),
  INVALID_CONFIG(6, "Invalid Configuration"),
  RESTART_FAILURE(7, "RTU Restart Failure"),
  MISSING_SW(8, "Missing Software");
  
  ErrorCode(int err, String description) {
    this.err = err;
    this.description = description;
  }
  
  
  public int errCode() {
    return err;
  }
  
  public String getDescription() {
    return description;
  }

  private final int err;
  private final String description;
  
  public static String printAsString() {
    StringBuilder sb = new StringBuilder();
    ErrorCode[] errs = ErrorCode.values();
    sb.append("========== Error Code =========== \n");
    for (ErrorCode e : errs) {
      sb.append(String.format(" [ %-2s] %s", e.err, e.description));
      sb.append("\n");
    }
    
    return sb.toString();
  }
}

