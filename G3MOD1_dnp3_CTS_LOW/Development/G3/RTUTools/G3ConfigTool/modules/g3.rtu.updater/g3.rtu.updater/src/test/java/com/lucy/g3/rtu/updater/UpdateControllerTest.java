/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.updater.UpdateController;
import com.lucy.g3.rtu.updater.dummies.DummyRTUComms;
import com.lucy.g3.rtu.updater.engine.impl.UpdateEngine;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwarePackage;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwarePackageParser;
import com.lucy.g3.test.support.resources.TestResources;

import org.junit.Assert;

public class UpdateControllerTest {

  private UpdateController controller;


  @Before
  public void init() {
    DummyRTUComms comms = new DummyRTUComms();
    UpdateEngine engine = new UpdateEngine(new File("target/temp"), comms, true);
    controller = new UpdateController(engine, comms);
  }

  @Test
  public void testUpdateSoftware() throws Exception {
    SoftwarePackage sdp = SoftwarePackageParser.parse(TestResources.getSampleSDPFile(), false);
    SoftwarePackage sdpboot = SoftwarePackageParser.parse(TestResources.getSampleSDPBootloaderFile(), true);

    controller.updateSoftware(sdp, sdpboot);
  }

  @Test
  public void testUpdateIpAddress() throws IOException {
    File[] configs = new File[] { TestResources.getSampleConfigFile(), TestResources.getSampleCompressedConfigFile() };

    for (File config : configs) {

      String newIp = UpdateController.findNewIp(config, new String[] { "1.2.3.4", "5.4.3.2" }, "1.2.3.4");
      Assert.assertEquals("192.168.1.11", newIp);

      newIp = UpdateController.findNewIp(config, new String[] { "1.2.3.4", "5.4.3.2" }, "5.4.3.2");
      Assert.assertEquals("192.168.2.1", newIp);

      newIp = UpdateController.findNewIp(config, new String[] { "1.2.3.4", "5.4.3.2" }, "3.3.3.3");
      Assert.assertNull("192.168.2.1", newIp);

      newIp = UpdateController.findNewIp(config, new String[] { "1.2.3.4", "5.4.3.2" }, null);
      Assert.assertNull("192.168.2.1", newIp);

      newIp = UpdateController.findNewIp(config, new String[0], "3.3.3.3");
      Assert.assertNull("192.168.2.1", newIp);
    }
  }
}
