/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.lucy.g3.rtu.updater.comms.IUpdateModule;


/**
 * The class for storing update result. 
 */
class UpdateResult {
  private Logger log = Logger.getLogger(UpdateResult.class);
  
  private IUpdateModule[] modulesBefore;
  private IUpdateModule[] modulesAfter;
  private Map<String, String> rtuInfoBefore;
  private Map<String, String> rtuInfoAfter;
  
  private final HashMap<String, Boolean> status = new HashMap<>();
  private ErrorCode err = ErrorCode.NONE;
  


  public void setModulesBeforeUpdate(IUpdateModule [] modules) {
    this.modulesBefore = modules;
  }
  
  public void setAfterUpdate(IUpdateModule [] modules) {
    this.modulesAfter = modules;
  }

  public void addStatus(String name, boolean statusValue) {
    this.status.put(name, statusValue);
  }
  
  public UpdateResult setError(ErrorCode error) {
    this.err = error;
    return this;
  }

  public ErrorCode getError() {
    return err;
  }

  public void printReport() {
    StringBuilder sb = new StringBuilder();
    sb.append("\n======== Gemini 3 RTU Software Update Result ======");
    
    if(rtuInfoBefore != null) {
      sb.append("\nRTU Before Update:");
      sb.append(printInfo(rtuInfoBefore));
      sb.append(printModuleList(modulesBefore));
    }
    
    if(rtuInfoAfter != null) {
      sb.append("\n\nRTU After Update:");
      sb.append(printInfo(rtuInfoAfter));
      sb.append(printModuleList(modulesAfter));
    }
    
    Set<String> statusKeys = status.keySet();
    for (String key: statusKeys ) {
      sb.append("\n");
      sb.append(key);
      sb.append(": ");
      sb.append(status.get(key));
    }
    
    log.info(sb.toString());
  }

  public void setInfoBeforeUpdate(Map<String, String> rtuInfo) {
    this.rtuInfoBefore = rtuInfo;
  }
  
  public void setInfoAfterUpdate(Map<String, String> rtuInfo) {
    this.rtuInfoAfter = rtuInfo;
  }
  
  private static String printInfo(Map<String, String> rtuInfo) {
    StringBuilder sb = new StringBuilder();
    Set<String> keys = rtuInfo.keySet();
    for (String key : keys) {
      sb.append(String.format("\n%-12s: %s",key, rtuInfo.get(key)));
    }
    
    return sb.toString();
  }

  private static String printModuleList(IUpdateModule[] modules) {
    // Log module list
    StringBuilder sb = new StringBuilder();
    int i = 0;
    if(modules != null) {
      for (IUpdateModule m : modules) {
        sb.append(String.format("\n[%-2d] %-12s Feature:%-6s Version:%-20s SystemAPI:%s", 
            i, m.getName(), m.getFeature(), m.getVersion(), m.getCurrentSystemAPI()));
        i ++;
      }
    } else {
      sb.append("N/A");
    }
     
    return sb.toString();
  }
}

