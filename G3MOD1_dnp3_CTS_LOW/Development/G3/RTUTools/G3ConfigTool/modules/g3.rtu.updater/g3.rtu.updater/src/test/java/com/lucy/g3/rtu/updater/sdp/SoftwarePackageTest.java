/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater.sdp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import com.lucy.g3.common.version.FeatureVersion;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.rtu.updater.sdp.IModuleSoftware;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwareFilter;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwarePackage;
import com.lucy.g3.rtu.updater.sdp.impl.SoftwarePackageParser;
import com.lucy.g3.test.support.resources.TestResources;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

public class SoftwarePackageTest {

  @Test
  public void testParseSDP() throws Exception {
    SoftwarePackage sdp = SoftwarePackageParser.parse(TestResources.getSampleSDPFile(), false);
    
    System.out.println(sdp.printContent());
    
    SoftwareFilter filter =  null; 
    IModuleSoftware[] entries = null; 
    
    entries = sdp.getAvailable(null);
    assertEquals(7, entries.length);

    filter =  new SoftwareFilter(MODULE.MODULE_SCM_MK2, new FeatureVersion(0, 1), new SystemAPIVersion(0, 5));
    entries = sdp.getAvailable(filter);
    assertEquals(1, entries.length);
    
    filter =  new SoftwareFilter(MODULE.MODULE_PSM, new FeatureVersion(0, 2), new SystemAPIVersion(0, 5));
    entries = sdp.getAvailable(filter);
    assertEquals(1, entries.length);
    assertNotNull(sdp.getLastest(filter));
    
    filter =  new SoftwareFilter(MODULE.MODULE_SCM_MK2, new FeatureVersion(0, 2), new SystemAPIVersion(0, 5));
    entries = sdp.getAvailable(filter);
    assertEquals(0, entries.length);
  }
  
  @Test
  public void testExtract() throws Exception {
    SoftwarePackage sdp = SoftwarePackageParser.parse(TestResources.getSampleSDPFile(),false);
    SoftwareFilter filter = new SoftwareFilter(MODULE.MODULE_PSM, new FeatureVersion(0, 2), new SystemAPIVersion(0, 5));
    File dir = new File("target/temp_sdp/").getAbsoluteFile();
    IModuleSoftware sw = sdp.getLastest(filter);
    sw.extractToDir(dir);
    
    File file = new File(dir, sw.getName());
    assertTrue(file.exists());
    assertTrue("File not found: "+ file, file.isFile());
  }
}


