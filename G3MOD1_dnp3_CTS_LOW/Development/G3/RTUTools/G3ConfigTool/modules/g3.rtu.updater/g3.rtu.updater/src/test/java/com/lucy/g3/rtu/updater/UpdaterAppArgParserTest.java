/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.updater;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.cli.ParseException;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.rtu.updater.UpdaterArgParser;
import com.lucy.g3.test.support.resources.TestResources;


public class UpdaterAppArgParserTest {
  private UpdaterArgParser fixture;
  
  @Before
  public void init(){
    fixture = new UpdaterArgParser();
  }
  
  @Test
  public void testPrintHelp() {
    fixture.printHelp();
  }

  @Test(expected = ParseException.class)
  public void testMissingMandatoryOptions() throws ParseException {
    fixture.parse(new String[]{"-help","update"}); // no mandatory
  }
  
  @Test
  public void testParseUpdate() throws ParseException {
    fixture.parse(new String[]{"-update",
        "-ip","10.11.11.102",
        "-user0","user0","-pass0","pass0",
        "-sdp","sdppath","-sdpboot","sdpbootPath",
        "-user1","user1","-pass1","pass1",
        "--config","configPath",
        });
    assertEquals("sdppath", fixture.getOptionValue("sdp"));
    assertEquals("10.11.11.102", fixture.getOptionValue("ip"));
    assertEquals("sdpbootPath", fixture.getOptionValue("sdpboot"));
    
    System.out.println(fixture.toString());
  }
  
  @Test
  public void testParseSDP() throws ParseException {
    fixture.parse(new String[]{ "-update", "-ip", "10.11.16.50", "-user0", "Admin", "-pass0","",
        "-sdp", "E:/work/Temp/0927/SDP.zip","-l", "INFO",  
        "-sdpboot", "E:/work/Temp/0927/SDP_Bootloader.zip", 
    });
    assertTrue("pass0:" + fixture.getPass0(), "".equals(fixture.getPass0()));
    assertNotNull("sdp is null", fixture.getSdp());
    assertNotNull("sdpboot is null",fixture.getSdpboot());
    assertEquals("INFO",fixture.getLogLevel());
  }
  
  @Test(expected = ParseException.class)
  public void testFileNotExist() throws ParseException {
    fixture.parse(new String[]{"-update",
        "-ip","10.11.11.102",
        "-user0","user0","-pass0","pass0",
        "-sdp","invalid path"
    }, true);
  }
  
  @Test
  public void testFileExist() throws ParseException {
    String sdp = TestResources.getSampleSDP();
    
    fixture.parse(new String[]{"-update",
        "-ip","10.11.11.102",
        "-user0","user0","-pass0","pass0",
        "-sdp",sdp
    }, true);
      
  }

}

