/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.security.KeyStore;
import java.security.KeyStoreException;

import javax.swing.DefaultCellEditor;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

/**
 *
 */
public class KeyStoreTable extends JTable {

  private Logger log = Logger.getLogger(KeyStoreTable.class);

  private JPopupMenu popupMenuCerts;

  private final KeyStoreActions actions;

  public KeyStoreTable() {
    this(null);
  }
  
  public KeyStoreTable(KeyStoreActions actions) {
    super(new KeyStoreTableModel());
    this.actions = actions;

    initTable();
    initPopupMenu();
  }

  @Override
  public void setModel(TableModel model) {
    if(model == null ||! (model instanceof KeyStoreTableModel)) {
      throw new IllegalArgumentException("KeyStoreTableModel expected, but actual: " + model);
    }
    
    super.setModel(model);
  }
  
  public void load(KeyStore keyStore) throws KeyStoreException {
    ((KeyStoreTableModel)getModel()).load(keyStore);
  }
  
  private void initTable() {
    JTable table = this;
    table.setShowGrid(false);
    table.setRowMargin(0);
    table.getColumnModel().setColumnMargin(0);
    table.getTableHeader().setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    // Top accommodate entry icons with spare space (16 pixels tall)
    table.setRowHeight(18);
    table.setFillsViewportHeight(true);

    // Add custom renderers for the table headers and cells
    for (int iCnt = 0; iCnt < table.getColumnCount(); iCnt++) {
      TableColumn column = table.getColumnModel().getColumn(iCnt);
      column.setHeaderRenderer(new KeyStoreTableHeadRend());
      column.setCellRenderer(new KeyStoreTableCellRend());
    }

    // Make the first column small and not resizable (it holds icons to
    // represent the different entry
    // types)
    TableColumn typeCol = table.getColumnModel().getColumn(KeyStoreTableModel.COLUMN_TYPE);
    typeCol.setResizable(false);
    typeCol.setMinWidth(20);
    typeCol.setMaxWidth(20);
    typeCol.setPreferredWidth(20);

    TableColumn aliasCol = table.getColumnModel().getColumn(KeyStoreTableModel.COLUMN_ALIAS);
    aliasCol.setMinWidth(20);
    aliasCol.setMaxWidth(10000);
    aliasCol.setPreferredWidth(200);

    // Make the table sortable
    table.setAutoCreateRowSorter(true);
    // ...and sort it by alias by default
    table.getRowSorter().toggleSortOrder(1);

    // Get usual double click edit start out of the way - we want double click
    // to show the
    // entry, even in editable columns. In-place edit can be invoked with F2.
    TableCellEditor cellEditor = table.getDefaultEditor(String.class);
    if (cellEditor instanceof DefaultCellEditor) {
      ((DefaultCellEditor) cellEditor).setClickCountToStart(1000);
    }

    // Put the table into a scroll pane
//    JScrollPane jspKeyStoreTable = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
//        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//    jspKeyStoreTable.getViewport().setBackground(table.getBackground());

    // Add mouse listeners to show pop-up menus when table entries are clicked
    // upon; maybeShowPopup for
    // both mousePressed and mouseReleased for cross-platform compatibility.
    // Also add listeners to show an
    // entry's certificate details if it is double-clicked
    
    if(actions != null) {
      table.addMouseListener(new MouseAdapter() {
  
        @Override
        public void mouseClicked(MouseEvent evt) {
          if (evt.getClickCount() > 1) {
            actions.certDetails();
          }
        }
  
        @Override
        public void mousePressed(MouseEvent evt) {
          maybeShowPopup(evt);
        }
  
        @Override
        public void mouseReleased(MouseEvent evt) {
          maybeShowPopup(evt);
        }
      });
    }

    table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  private void initPopupMenu() {
    popupMenuCerts = new JPopupMenu();
    
    if(actions != null) {
      popupMenuCerts.add(actions.getAction(KeyStoreActions.ACTION_CERTDETAILS));
      popupMenuCerts.add(actions.getAction(KeyStoreActions.ACTION_EXPORTENTRY));
      popupMenuCerts.add(actions.getAction(KeyStoreActions.ACTION_DELETEENTRY));
      popupMenuCerts.add(actions.getAction(KeyStoreActions.ACTION_RENAMECERT));
    }
  }

  /**
   * Show the appropriate pop-up menu if the originating mouse event indicates
   * that the user clicked upon a keystore entry in the UI table and the entry
   * is of type key pair or trusted certificate.
   * 
   * @param evt
   *          The mouse event
   */
  private void maybeShowPopup(MouseEvent evt) {
    if (evt.isPopupTrigger()) {
      // What row was clicked upon (if any)?
      Point point = new Point(evt.getX(), evt.getY());
      int iRow = rowAtPoint(point);

      if (iRow != -1) {
        // Make the row that was clicked upon the selected one
        setRowSelectionInterval(iRow, iRow);

        // Show one menu if the keystore entry is of type key pair...
        String currEntry = getSelectedType();
        if (KeyStoreTableModel.KEY_PAIR_ENTRY.equals(currEntry)) {
          // m_jpmKeyPair.show(evt.getComponent(), evt.getX(), evt.getY());
        }
        // ...and another if the type is trusted certificate
        else if (KeyStoreTableModel.TRUST_CERT_ENTRY.equals(currEntry)) {
          popupMenuCerts.show(evt.getComponent(), evt.getX(), evt.getY());
        }
        // ...and yet another for key-only entries
        else if (KeyStoreTableModel.KEY_ENTRY.equals(currEntry)) {
          // m_jpmKey.show(evt.getComponent(), evt.getX(), evt.getY());
        }
        // What's this?
        else {
          log.warn("Popup context menu requested for unknown entry: " + currEntry);
        }
      }
    }
  }

  public String getSelectedType() {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0)
      return null;

    selectedRow = convertRowIndexToModel(selectedRow);
    return (selectedRow >= 0) ? (String) getModel().getValueAt(selectedRow, KeyStoreTableModel.COLUMN_TYPE) : null;
  }

  public String getSelectedAlias() {
    int selectedRow = getSelectedRow();
    if (selectedRow < 0)
      return null;

    selectedRow = convertRowIndexToModel(selectedRow);
    return (selectedRow >= 0) ? (String) getModel().getValueAt(selectedRow, KeyStoreTableModel.COLUMN_ALIAS) : null;
  }

}
