/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import java.awt.Toolkit;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 */
public class Resources {
  /** Resource bundle base name */
  private static final String RB_BASENAME = Resources.class.getPackage().getName() + "/resources";

  /** Resource bundle */
  public static final ResourceBundle RB = ResourceBundle.getBundle(RB_BASENAME);

  
  public static ImageIcon getIcon(String key){
    String str = Resources.RB.getString(key);
    URL res = Resources.class.getResource(str);
    return new ImageIcon(Toolkit.getDefaultToolkit().createImage(res));
  }
}

