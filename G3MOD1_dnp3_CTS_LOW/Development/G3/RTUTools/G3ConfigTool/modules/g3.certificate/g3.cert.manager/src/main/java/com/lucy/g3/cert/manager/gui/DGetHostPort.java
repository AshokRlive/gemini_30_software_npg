/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Locale;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.security.support.SSLSupport;


/**
 * Modal dialog used for entering an IP address and a port.
 */
public class DGetHostPort
    extends AbstractDialog
{
	/** Default port */
	private static final String DEFAULT_PORT = String.valueOf(SSLSupport.DEFAULT_PORT);

	/** Host text field */
	private JTextField m_jtfHost;

	/** Port text field */
	private JTextField m_jtfPort;

	/** Stores the address entered by the user */
	private InetSocketAddress m_iAddress;
	
	private JPanel contentPane;

	private boolean resolveName = true;
	/**
	 * Creates new DGetHostPort dialog.
	 * 
	 * @param parent The parent window
	 * @param sTitle The dialog's title
	 * @param iOldHostPort The address to display initially
	 */
	public DGetHostPort(Window parent, String sTitle, InetSocketAddress iOldHostPort)
	{
		super(parent);
		setTitle(sTitle);
		setModal(true);
		initComponents(iOldHostPort);
	}

	/*
	 * Set true to resolve IP address from the given host name.
	 */
	public void setResolveName(boolean resolveName){
	  this.resolveName = resolveName;
	}
	
	public void setPortFieldEditable(boolean editable) {
	  m_jtfPort.setEditable(editable);
	}
	
	/**
	 * Get the host+port entered by the user.
	 * 
	 * @return The host+port, or null if none was entered
	 */
	public InetSocketAddress getHostPort()
	{
		return m_iAddress;
	}

	/**
	 * Initialize the dialog's GUI components.
	 * 
	 * @param iOldHostPort The host+port to display initially
	 */
	private void initComponents(InetSocketAddress iOldHostPort)
	{
	  contentPane = new JPanel(new BorderLayout());

		JLabel jlHost = new JLabel("Hostname/IP address:");
		m_jtfHost = new JTextField(15);
		jlHost.setLabelFor(m_jtfHost);

		JLabel jlPort = new JLabel("Port number:");
		m_jtfPort = new JTextField(DEFAULT_PORT, 5);
		if (iOldHostPort != null)
		{
		  InetAddress address = iOldHostPort.getAddress();
			m_jtfHost.setText(address == null ? iOldHostPort.getHostName() : address.getHostAddress());
			m_jtfPort.setText(String.valueOf(iOldHostPort.getPort()));
		}
		jlPort.setLabelFor(m_jtfPort);

		JPanel jpHostPort = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpHostPort.add(jlHost);
		jpHostPort.add(m_jtfHost);
		jpHostPort.add(jlPort);
		jpHostPort.add(m_jtfPort);
		jpHostPort.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.add(jpHostPort, BorderLayout.CENTER);
		
		pack();
	}

	/**
	 * Check that the host+port entered is valid.
	 * 
	 * @return True if the host+port is valid, false otherwise
	 */
	private boolean checkHostPort()
	{
		String sHost = m_jtfHost.getText().trim().toLowerCase(Locale.ENGLISH);
		if (sHost.isEmpty())
		{
			JOptionPane.showMessageDialog(this, "Hostname or IP address must be supplied", getTitle(),
			    JOptionPane.WARNING_MESSAGE);
			return false;
		}

		String sPort = m_jtfPort.getText().trim();
		if (sPort.isEmpty())
		{
			JOptionPane.showMessageDialog(this,"Port number must be supplied", getTitle(),
			    JOptionPane.WARNING_MESSAGE);
			return false;
		}
		int port;
		try
		{
			port = Integer.parseInt(sPort);
		}
		catch (Exception e)
		{
			MessageDialogs.error(this, null, e);
			return false;
		}

		try
		{
		  if(resolveName)
		    m_iAddress = new InetSocketAddress(sHost, port);
		  else
		    m_iAddress = InetSocketAddress.createUnresolved(sHost, port);
		}
		catch (Exception e)
		{
			MessageDialogs.error(this, null, e);
			return false;
		}

		return true;
	}

	/**
	 * OK button pressed or otherwise activated.
	 */
	@Override
	public void ok()
	{
		if (checkHostPort())
		{
			super.ok();
		}
	}

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
    
  }
}
