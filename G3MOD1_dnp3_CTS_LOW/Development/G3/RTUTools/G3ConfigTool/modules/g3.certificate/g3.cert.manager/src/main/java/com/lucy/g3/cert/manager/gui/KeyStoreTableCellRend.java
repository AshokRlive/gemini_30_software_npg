/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;


import java.awt.Component;
import java.text.DateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Custom cell renderer for the cells of the keystore table of FPortecle.
 */
class KeyStoreTableCellRend
    extends DefaultTableCellRenderer
{
  private static final ResourceBundle RB = Resources.RB;  
	/**
	 * Returns the rendered cell for the supplied value and column.
	 * 
	 * @param jtKeyStore The JTable
	 * @param value The value to assign to the cell
	 * @param bIsSelected True if cell is selected
	 * @param iRow The row of the cell to render
	 * @param iCol The column of the cell to render
	 * @param bHasFocus If true, render cell appropriately
	 * @return The rendered cell
	 */
	@Override
	public Component getTableCellRendererComponent(JTable jtKeyStore, Object value, boolean bIsSelected,
	    boolean bHasFocus, int iRow, int iCol)
	{
		JLabel cell =
		    (JLabel) super.getTableCellRendererComponent(jtKeyStore, value, bIsSelected, bHasFocus, iRow, iCol);

		// Entry column - display an icon representing the type and tool tip text
		if (iCol == 0)
		{
			ImageIcon icon = null;

			if (KeyStoreTableModel.KEY_PAIR_ENTRY.equals(value))
			{
				icon = new ImageIcon(getClass().getResource(RB.getString("KeyStoreTableCellRend.KeyPairEntry.image")));
				cell.setToolTipText(RB.getString("KeyStoreTableCellRend.KeyPairEntry.tooltip"));
			}
			else if (KeyStoreTableModel.TRUST_CERT_ENTRY.equals(value))
			{
				icon =
				    new ImageIcon(getClass().getResource(RB.getString("KeyStoreTableCellRend.TrustCertEntry.image")));
				cell.setToolTipText(RB.getString("KeyStoreTableCellRend.TrustCertEntry.tooltip"));
			}
			else
			{
				icon = new ImageIcon(getClass().getResource(RB.getString("KeyStoreTableCellRend.KeyEntry.image")));
				cell.setToolTipText(RB.getString("KeyStoreTableCellRend.KeyEntry.tooltip"));
			}

			cell.setIcon(icon);
			cell.setText("");
			cell.setVerticalAlignment(CENTER);
			cell.setHorizontalAlignment(CENTER);
		}
		// Last Modified column - format date (if date supplied)
		else if (iCol == 2)
		{
			if (value != null)
			{
				if (value instanceof Date)
				{
					// Include time zone
					cell.setText(
					    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG).format((Date) value));
				}
				else
				{
					cell.setText(value.toString());
				}
			}
		}
		// Alias column - just use alias text
		else
		{
			cell.setText(value.toString());
		}

		cell.setBorder(new EmptyBorder(0, 5, 0, 5));

		return cell;
	}
}
