/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;

import org.junit.Assert;
import org.junit.Test;

import com.lucy.g3.cert.manager.gui.Resources;

/**
 *
 */
public class ResourcesTest {
  @Test
  public void test(){
    Resources.RB.getString("FPortecle.Version");
    String iconStr = Resources.RB.getString("FileChooseFactory.KeyStoreImage");
    URL iconURL = ResourcesTest.class.getResource(iconStr);
    ImageIcon icon = new ImageIcon(Toolkit.getDefaultToolkit().createImage(iconURL));
    Assert.assertNotNull(icon);
  }
}


