/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import com.lucy.g3.cert.manager.gui.DGetPassword;

/**
 *
 */
public class GetPasswordDialogTest {

  public static void main(String[] args) {
    new DGetPassword(null, "Get Password").setVisible(true);
  }
}

