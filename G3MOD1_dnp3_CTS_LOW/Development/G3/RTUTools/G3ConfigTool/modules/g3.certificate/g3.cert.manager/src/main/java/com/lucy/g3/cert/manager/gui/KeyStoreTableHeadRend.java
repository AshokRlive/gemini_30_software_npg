/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;


import java.awt.Component;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Custom cell renderer for the headers of the keystore table of FPortecle.
 */
class KeyStoreTableHeadRend
    extends DefaultTableCellRenderer
{
  private static final ResourceBundle RB = Resources.RB;  
	/**
	 * Returns the rendered header cell for the supplied value and column.
	 * 
	 * @param jtKeyStore The JTable
	 * @param value The value to assign to the cell
	 * @param bIsSelected True if cell is selected
	 * @param iRow The row of the cell to render
	 * @param iCol The column of the cell to render
	 * @param bHasFocus If true, render cell appropriately *
	 * @return The rendered cell
	 */
	@Override
	public Component getTableCellRendererComponent(JTable jtKeyStore, Object value, boolean bIsSelected,
	    boolean bHasFocus, int iRow, int iCol)
	{
		// Get header renderer
		JLabel header = (JLabel) jtKeyStore.getColumnModel().getColumn(iCol).getHeaderRenderer();

		// The entry type header contains an icon
		if (iCol == 0)
		{
			header.setText("");
			ImageIcon icon =
			    new ImageIcon(getClass().getResource(RB.getString("KeyStoreTableHeadRend.TypeColumn.image")));
			header.setIcon(icon);
			header.setHorizontalAlignment(CENTER);
			header.setVerticalAlignment(CENTER);

			header.setToolTipText(RB.getString("KeyStoreTableHeadRend.TypeColumn.tooltip"));
		}
		// The other headers contain text
		else
		{
			header.setText((String) value);
			header.setHorizontalAlignment(LEFT);

			if (iCol == 1)
			{
				header.setToolTipText(RB.getString("KeyStoreTableHeadRend.AliasColumn.tooltip"));
			}
			else
			{
				header.setToolTipText(RB.getString("KeyStoreTableHeadRend.LastModifiedDateColumn.tooltip"));
			}
		}

		header.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED), new EmptyBorder(0, 5, 0, 5)));

		return header;
	}
}
