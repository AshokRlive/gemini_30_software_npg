/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import com.jgoodies.forms.factories.ButtonBarFactory;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.security.support.SecurityUtils;


/**
 * Modal dialog to display an X.509 object's PEM encoding.
 */
public class DViewPEM extends AbstractDialog
{
	/** Stores object to display */
	private final X509Certificate m_cert;

	/** Stores PEM encoding */
	private final String m_pem;

	private JPanel contentPane;

	/**
	 * Creates new DViewPEM dialog.
	 * 
	 * @param parent Parent window
	 * @param sTitle The dialog title
	 * @param cert Object to display encoding for
	 * @param chooser File chooser for saving the PEM encoding
	 * @throws CryptoException A problem was encountered getting the object's PEM encoding
	 */
	public DViewPEM(Window parent, String title, X509Certificate cert)
	{
		super(parent);
		m_cert = cert;
		m_pem = encodePEM(m_cert);
		
		setTitle(title);
		initComponents();
	}

  @SuppressWarnings("restriction")
  static String encodePEM(Certificate cert) {
    StringBuilder sb = new StringBuilder();
    sb.append(sun.security.provider.X509Factory.BEGIN_CERT);
    sb.append(System.getProperty("line.separator"));
    try {
      sb.append(SecurityUtils.base64Encode(cert.getEncoded()));
    } catch (CertificateEncodingException e) {
      sb.append(e.getMessage());
    }
    sb.append(sun.security.provider.X509Factory.END_CERT);
    
    return sb.toString();
  }

  /**
	 * Initialize the dialog's GUI components.
	 * 
	 * @throws CryptoException A problem was encountered getting the object's PEM encoding
	 */
	private void initComponents()
	{
	  contentPane = new JPanel(new BorderLayout());
	  contentPane.setPreferredSize(new Dimension(580, 400));
	  
		// Load text area with the PEM encoding
		JTextArea jtaPEM = new JTextArea(m_pem);
		jtaPEM.setCaretPosition(0);
		jtaPEM.setEditable(false);
		jtaPEM.setFont(new Font(Font.MONOSPACED, Font.PLAIN, jtaPEM.getFont().getSize()));

		JScrollPane jspPEM = new JScrollPane(jtaPEM, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
		    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		contentPane.add(jspPEM, BorderLayout.CENTER);
		
		pack();
	}

	/**
	 * Save button pressed or otherwise activated.
	 */
	private void savePressed()
	{
	  JFileChooser chooser = FileChooserFactory.getPEMFileChooser("Unammed.pem");
	  File file = DialogUtils.showSaveDialog(this, "Save PEM to File", chooser);
	  if(file == null)
	    return;

	  /*Overwrite?*/
    if(file.exists()) {
      boolean overwrite = MessageDialogs.showOverwriteDialog(this, file);
      if(!overwrite) {
        return;
      }
    }
    
    /*Write PEM to file*/
		try (FileWriter fw = new FileWriter(file))
		{
			fw.write(m_pem);
			MessageDialogs.finish(this, "The PEM file has be written to:\n"+file);
		}
		catch (IOException e)
		{
			MessageDialogs.error(this, null, e);
		}
	}

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    Action okAction = getAction(ACTION_KEY_OK);
    setDefaultAction(okAction);
    
    JButton btnSave = new JButton("Save PEM");
    btnSave.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        savePressed();
      }
    });
    
    JPanel buttonBar = ButtonBarFactory.buildRightAlignedBar(btnSave, new JButton(okAction));
    return buttonBar;
  }
}
