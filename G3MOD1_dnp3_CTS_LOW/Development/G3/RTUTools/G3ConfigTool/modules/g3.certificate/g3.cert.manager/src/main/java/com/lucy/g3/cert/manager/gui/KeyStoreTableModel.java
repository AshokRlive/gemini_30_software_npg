/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.Date;
import java.util.Enumeration;

import javax.swing.table.AbstractTableModel;


/**
 * The table model used to display a keystore's entries sorted by alias name.
 */
class KeyStoreTableModel
    extends AbstractTableModel
{
  
  public final static int COLUMN_TYPE = 0;
  public final static int COLUMN_ALIAS = 1;
  public final static int COLUMN_MODIFIED_DATE = 2;
  
	/** Column names */
	private static final String[] COLUMN_NAMES = { "Type","Alias","Last Modified" };

	/** Value to place in the type column for a key pair entry */
	public static final String KEY_PAIR_ENTRY = "Key Pair Entry";

	/** Value to place in the type column for a trusted certificate entry */
	public static final String TRUST_CERT_ENTRY = "Trusted Certificate Entry";

	/** Value to place in the type column for a key entry */
	public static final String KEY_ENTRY = "Key Entry";

	/** Column classes */
	private static final Class<?>[] COLUMN_CLASSES = { String.class, String.class, Date.class };

	/** Holds the table data */
	private Object[][] m_data;

	/**
	 * Construct a new KeyStoreTableModel.
	 */
	public KeyStoreTableModel()
	{
		m_data = new Object[0][getColumnCount()];
	}

	/**
	 * Load the KeyStoreTableModel with the entries from a keystore.
	 * 
	 * @param keyStore The keystore
	 * @throws KeyStoreException A problem is encountered accessing the keystore's entries
	 */
	public void load(KeyStore keyStore)
	    throws KeyStoreException
	{
		// Create one table row for each keystore entry
		m_data = new Object[keyStore.size()][getColumnCount()];

		// Iterate through the aliases, retrieving the keystore entries and populating the table model
		int iCnt = 0;
		for (Enumeration<String> en = keyStore.aliases(); en.hasMoreElements();)
		{
			String sAlias = en.nextElement();

			// Populate the type column - it is set with an integer but a custom cell renderer will cause a
			// suitable icon to be displayed
			if (keyStore.isCertificateEntry(sAlias))
			{
				m_data[iCnt][COLUMN_TYPE] = TRUST_CERT_ENTRY;
			}
			else if (keyStore.isKeyEntry(sAlias) && keyStore.getCertificateChain(sAlias) != null &&
			    keyStore.getCertificateChain(sAlias).length != 0)
			{
				m_data[iCnt][COLUMN_TYPE] = KEY_PAIR_ENTRY;
			}
			else
			{
				m_data[iCnt][COLUMN_TYPE] = KEY_ENTRY;
			}

			// Populate the alias column
			m_data[iCnt][COLUMN_ALIAS] = sAlias;

			// Populate the modified date column
			m_data[iCnt][COLUMN_MODIFIED_DATE] = keyStore.getCreationDate(sAlias);

			iCnt++;
		}

		fireTableDataChanged();
	}

	/**
	 * Get the number of columns in the table.
	 * 
	 * @return The number of columns
	 */
	@Override
	public int getColumnCount()
	{
		return COLUMN_CLASSES.length;
	}

	/**
	 * Get the number of rows in the table.
	 * 
	 * @return The number of rows
	 */
	@Override
	public int getRowCount()
	{
		return m_data.length;
	}

	/**
	 * Get the name of the column at the given position.
	 * 
	 * @param iCol The column position
	 * @return The column name
	 */
	@Override
	public String getColumnName(int iCol)
	{
		return COLUMN_NAMES[iCol];
	}

	/**
	 * Get the cell value at the given row and column position.
	 * 
	 * @param iRow The row position
	 * @param iCol The column position
	 * @return The cell value
	 */
	@Override
	public Object getValueAt(int iRow, int iCol)
	{
		return m_data[iRow][iCol];
	}

//	@Override
//	public void setValueAt(Object value, int rowIndex, int columnIndex)
//	{
//		if (isCellEditable(rowIndex, columnIndex))
//		{
//			portecle.renameEntry(m_data[rowIndex][columnIndex].toString(), value.toString(), true);
//		}
//	}

	/**
	 * Get the class at of the cells at the given column position.
	 * 
	 * @param iCol The column position
	 * @return The column cells' class
	 */
	@Override
	public Class<?> getColumnClass(int iCol)
	{
		return COLUMN_CLASSES[iCol];
	}

	/**
	 * Is the cell at the given row and column position editable?
	 * 
	 * @param iRow The row position
	 * @param iCol The column position
	 * @return True if the cell is editable, false otherwise
	 */
	@Override
	public boolean isCellEditable(int iRow, int iCol)
	{
	  return false;
	  
//		if (iCol != 1)
//		{
//			return false;
//		}
//
//		// Key-only entries are not renameable - we do a remove-store operation but the KeyStore API won't
//		// allow us to store a PrivateKey without associated certificate chain.
//		// TODO: Maybe it'd work for other Key types? Need testing material.
//		return !KEY_ENTRY.equals(m_data[iRow][0]);
	}
}
