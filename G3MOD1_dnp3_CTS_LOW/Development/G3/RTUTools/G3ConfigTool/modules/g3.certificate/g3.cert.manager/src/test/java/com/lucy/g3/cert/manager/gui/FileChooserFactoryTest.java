/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import javax.swing.Icon;
import javax.swing.JFileChooser;

import org.junit.Test;

import com.lucy.g3.cert.manager.gui.FileChooserFactory;
import com.lucy.g3.cert.manager.gui.Resources;
import com.lucy.g3.gui.common.dialogs.DialogUtils;

import org.junit.Assert;


/**
 *
 */
public class FileChooserFactoryTest {

  @Test
  public void testIcon() {
    Icon icon = Resources.getIcon("FileChooseFactory.CertificateImage");
    Assert.assertNotNull(icon);
  }

  public static void main(String[] args) {
    JFileChooser chooser = FileChooserFactory.getCertFileChooser();
    DialogUtils.showOpenDialog(null, "", chooser);
  }
}

