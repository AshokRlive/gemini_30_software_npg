/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.common.utils.StringUtils;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.security.support.SecurityUtils;

public class DViewCertificate extends AbstractDialog {

  private Logger log = Logger.getLogger(DViewCertificate.class);

  private final X509Certificate cert;
  
  private String alias;
  
  private String connectionProtocol;
  private String connectionCipherSuite;


  public DViewCertificate(Window owner, String alias, X509Certificate cert){
    this(owner, alias, cert, null, null);
  }
  
  public DViewCertificate(Window owner, String alias, X509Certificate cert,
      String connectionProtocol, String connectionCipherSuite) {
    super(owner);
    setModal(true);
    this.alias = alias;
    this.cert = cert;
    this.connectionProtocol = connectionProtocol;
    this.connectionCipherSuite= connectionCipherSuite;
    
    initComponents();
    populateDialog();
    pack();
  }

  private void populateDialog() {
    String title = getTitle();
    if(alias != null) {
      title = title +" for: \""+alias+"\"";
    }
    setTitle(title);
    
    // Has the certificate [not yet become valid/expired]
    Date currentDate = new Date();

    Date startDate = cert.getNotBefore();
    Date endDate = cert.getNotAfter();

    boolean bNotYetValid = currentDate.before(startDate);
    boolean bNoLongerValid = currentDate.after(endDate);

    // Populate the fields:

    // Version
    labelVersion.setText(Integer.toString(cert.getVersion()));
    labelVersion.setCaretPosition(0);

    // Subject
    labelSubject.setText(cert.getSubjectDN().toString());
    labelSubject.setCaretPosition(0);

    // Issuer
    labelIssuer.setText(cert.getIssuerDN().toString());
    labelIssuer.setCaretPosition(0);

    // Serial Number
    labelSerial.setText(StringUtils.toHex(cert.getSerialNumber(), 4, " ").toString());
    labelSerial.setCaretPosition(0);

    // Valid From (include timezone)
    labelValidFrom.setText(DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG).format(startDate));
    if (bNotYetValid) {
      labelValidFrom.setText(MessageFormat.format("{0} (NOT YET VALID)", labelValidFrom.getText()));
      labelValidFrom.setForeground(Color.red);
    } else {
      labelValidFrom.setForeground(labelVersion.getForeground());
    }
    labelValidFrom.setCaretPosition(0);

    // Valid Until (include time zone)
    labelValidUntil.setText(DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG).format(endDate));
    if (bNoLongerValid) {
      labelValidUntil.setText(MessageFormat.format("{0} (EXPIRED)", labelValidUntil.getText()));
      labelValidUntil.setForeground(Color.red);
    } else {
      labelValidUntil.setForeground(labelVersion.getForeground());
    }
    labelValidUntil.setCaretPosition(0);

    // Public Key (algorithm and key size)
    int iKeySize = SecurityUtils.getKeyLength(cert.getPublicKey());
    labelPublicKey.setText(cert.getPublicKey().getAlgorithm());
    if (iKeySize != SecurityUtils.UNKNOWN_KEY_SIZE) {
      labelPublicKey.setText(MessageFormat.format("{0} ({1} bits)", labelPublicKey.getText(), iKeySize));
    }
    labelPublicKey.setCaretPosition(0);

    // Signature Algorithm
    labelSignature.setText(cert.getSigAlgName());
    labelSignature.setCaretPosition(0);

    // Fingerprints
    byte[] bCert;
    try {
      bCert = cert.getEncoded();
      labelMD5.setText(SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_MD5));
      labelMD5.setCaretPosition(0);

      labelSHA1.setText(SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_SHA1));
      labelSHA1.setCaretPosition(0);
    } catch (CertificateEncodingException ex) {
      log.error("Could not get the encoded form of the certificate.");
    }

    // Enable/disable extensions button
    Set<String> critExts = cert.getCriticalExtensionOIDs();
    Set<String> nonCritExts = cert.getNonCriticalExtensionOIDs();

    if ((critExts != null && !critExts.isEmpty()) || (nonCritExts != null && !nonCritExts.isEmpty())) {
      // Extensions
      btnExtension.setEnabled(true);
    } else {
      // No extensions
      btnExtension.setEnabled(false);
    }
    

    if (connectionProtocol != null) {
      labelConnection.setVisible(true);
      tfConnection.setText(connectionProtocol);
      tfConnection.setVisible(true);
    }

    if (connectionCipherSuite != null) {
      labelCipher.setVisible(true);
      tfCipher.setText(connectionProtocol);
      tfCipher.setVisible(true);
    }
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKButtonPanel();
  }

  /**
   * PEM Encoding Encoding button pressed or otherwise activated. Show the PEM encoding for the currently selected
   * certificate.
   */
  private void btnPEMActionPerformed() {
      new DViewPEM(this, MessageFormat.format("PEM Encoding for certificate: \"{0}\"", alias), cert).setVisible(true);
  }

  private void btnExtensionActionPerformed() {
    new DViewExtensions(this,
        MessageFormat.format("Extensions for certificate: \"{0}\"", alias), true, cert).setVisible(true); 
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    label1 = new JLabel();
    labelVersion = new JTextField();
    label2 = new JLabel();
    labelSubject = new JTextField();
    label3 = new JLabel();
    labelIssuer = new JTextField();
    label4 = new JLabel();
    labelSerial = new JTextField();
    label9 = new JLabel();
    labelValidFrom = new JTextField();
    label10 = new JLabel();
    labelValidUntil = new JTextField();
    label11 = new JLabel();
    labelPublicKey = new JTextField();
    label12 = new JLabel();
    labelSignature = new JTextField();
    label13 = new JLabel();
    labelSHA1 = new JTextField();
    label14 = new JLabel();
    labelMD5 = new JTextField();
    labelConnection = new JLabel();
    tfConnection = new JTextField();
    labelCipher = new JLabel();
    tfCipher = new JTextField();
    panel1 = new JPanel();
    btnExtension = new JButton();
    btnPEM = new JButton();

    //======== this ========
    setTitle("Certificate Details");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== contentPanel ========
    {
      contentPanel.setLayout(new FormLayout(
          "right:default, $lcgap, 200dlu:grow",
          "12*(default, $lgap), default"));

      //---- label1 ----
      label1.setText("Version:");
      contentPanel.add(label1, CC.xy(1, 1));

      //---- labelVersion ----
      labelVersion.setText("NA");
      labelVersion.setEditable(false);
      contentPanel.add(labelVersion, CC.xy(3, 1));

      //---- label2 ----
      label2.setText("Subject:");
      contentPanel.add(label2, CC.xy(1, 3));

      //---- labelSubject ----
      labelSubject.setText("NA");
      labelSubject.setEditable(false);
      contentPanel.add(labelSubject, CC.xy(3, 3));

      //---- label3 ----
      label3.setText("Issuer:");
      contentPanel.add(label3, CC.xy(1, 5));

      //---- labelIssuer ----
      labelIssuer.setText("NA");
      labelIssuer.setEditable(false);
      contentPanel.add(labelIssuer, CC.xy(3, 5));

      //---- label4 ----
      label4.setText("Serial Number:");
      contentPanel.add(label4, CC.xy(1, 7));

      //---- labelSerial ----
      labelSerial.setText("NA");
      labelSerial.setEditable(false);
      contentPanel.add(labelSerial, CC.xy(3, 7));

      //---- label9 ----
      label9.setText("Valid From:");
      contentPanel.add(label9, CC.xy(1, 9));

      //---- labelValidFrom ----
      labelValidFrom.setText("NA");
      labelValidFrom.setEditable(false);
      contentPanel.add(labelValidFrom, CC.xy(3, 9));

      //---- label10 ----
      label10.setText("Valid Until:");
      contentPanel.add(label10, CC.xy(1, 11));

      //---- labelValidUntil ----
      labelValidUntil.setText("NA");
      labelValidUntil.setEditable(false);
      contentPanel.add(labelValidUntil, CC.xy(3, 11));

      //---- label11 ----
      label11.setText("Public Key:");
      contentPanel.add(label11, CC.xy(1, 13));

      //---- labelPublicKey ----
      labelPublicKey.setText("NA");
      labelPublicKey.setEditable(false);
      contentPanel.add(labelPublicKey, CC.xy(3, 13));

      //---- label12 ----
      label12.setText("Signature Algorithm:");
      contentPanel.add(label12, CC.xy(1, 15));

      //---- labelSignature ----
      labelSignature.setText("NA");
      labelSignature.setEditable(false);
      contentPanel.add(labelSignature, CC.xy(3, 15));

      //---- label13 ----
      label13.setText("SHA-1 Fingerprint:");
      contentPanel.add(label13, CC.xy(1, 17));

      //---- labelSHA1 ----
      labelSHA1.setText("NA");
      labelSHA1.setEditable(false);
      contentPanel.add(labelSHA1, CC.xy(3, 17));

      //---- label14 ----
      label14.setText("MD5 Fingerprint:");
      contentPanel.add(label14, CC.xy(1, 19));

      //---- labelMD5 ----
      labelMD5.setText("NA");
      labelMD5.setEditable(false);
      contentPanel.add(labelMD5, CC.xy(3, 19));

      //---- labelConnection ----
      labelConnection.setText("Connection Protocol:");
      labelConnection.setVisible(false);
      contentPanel.add(labelConnection, CC.xy(1, 21));

      //---- tfConnection ----
      tfConnection.setEditable(false);
      tfConnection.setText("NA");
      tfConnection.setVisible(false);
      contentPanel.add(tfConnection, CC.xy(3, 21));

      //---- labelCipher ----
      labelCipher.setText("Connection Cipher Suite:");
      labelCipher.setVisible(false);
      contentPanel.add(labelCipher, CC.xy(1, 23));

      //---- tfCipher ----
      tfCipher.setEditable(false);
      tfCipher.setText("NA");
      tfCipher.setVisible(false);
      contentPanel.add(tfCipher, CC.xy(3, 23));

      //======== panel1 ========
      {
        panel1.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- btnExtension ----
        btnExtension.setText("Extensions");
        btnExtension.setMnemonic('E');
        btnExtension.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnExtensionActionPerformed();
          }
        });
        panel1.add(btnExtension);

        //---- btnPEM ----
        btnPEM.setText("PEM Encoding");
        btnPEM.setMnemonic('P');
        btnPEM.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnPEMActionPerformed();
          }
        });
        panel1.add(btnPEM);
      }
      contentPanel.add(panel1, CC.xy(3, 25));
    }
    contentPane.add(contentPanel, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JLabel label1;
  private JTextField labelVersion;
  private JLabel label2;
  private JTextField labelSubject;
  private JLabel label3;
  private JTextField labelIssuer;
  private JLabel label4;
  private JTextField labelSerial;
  private JLabel label9;
  private JTextField labelValidFrom;
  private JLabel label10;
  private JTextField labelValidUntil;
  private JLabel label11;
  private JTextField labelPublicKey;
  private JLabel label12;
  private JTextField labelSignature;
  private JLabel label13;
  private JTextField labelSHA1;
  private JLabel label14;
  private JTextField labelMD5;
  private JLabel labelConnection;
  private JTextField tfConnection;
  private JLabel labelCipher;
  private JTextField tfCipher;
  private JPanel panel1;
  private JButton btnExtension;
  private JButton btnPEM;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  /**
   * Create, show, and wait for a new DViewCertificate dialog.
   * 
   * @param parent Parent window
   * @param url URL, URI or file to load CRL from
   */
  @SuppressWarnings("restriction")
  public static boolean showAndWait(Window parent, File certFile)
  {
    try
    {
      X509Certificate cert = new sun.security.x509.X509CertImpl(new FileInputStream(certFile));

      DViewCertificate dialog = new DViewCertificate(parent,
          certFile.getName(),
          cert);
      dialog.setVisible(true);
    }
    catch (Exception ex)
    {
      MessageDialogs.error(parent, null, ex);
      return false;
    }

    return true;
  }
}
