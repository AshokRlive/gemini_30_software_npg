/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;

//import org.bouncycastle.openssl.PasswordFinder;


/**
 * Modal dialog used for entering a masked password.
 */
public class DGetPassword
    extends AbstractDialog
{
	/** Password entry password field */
	private JPasswordField m_jpfPassword;

	/** Stores password entered */
	private char[] m_cPassword;
	
	private JPanel contentPane;

	/**
	 * Creates new DGetPassword dialog.
	 * 
	 * @param parent Parent frame
	 * @param sTitle The dialog's title
	 */
	public DGetPassword(Window parent, String sTitle)
	{
		super(parent);
		setTitle(sTitle);
		setModal(true);
		initComponents();
	}

	/**
	 * Get the password set in the dialog.
	 * 
	 * @return The password or null if none was set
	 */
	public char[] getPassword()
	{
		if (m_cPassword == null)
		{
			return null;
		}
		char[] copy = new char[m_cPassword.length];
		System.arraycopy(m_cPassword, 0, copy, 0, copy.length);
		return copy;
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
	  contentPane = new JPanel(new BorderLayout());

		JLabel jlPassword = new JLabel("Enter Password:"); 
		m_jpfPassword = new JPasswordField(15);
		jlPassword.setLabelFor(m_jpfPassword);

		JPanel jpPassword = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpPassword.add(jlPassword);
		jpPassword.add(m_jpfPassword);
		jpPassword.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.add(jpPassword, BorderLayout.CENTER);
		
		pack();
	}

	@Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
  
  @Override
  public void cancel(){
    m_cPassword = null;
    super.cancel();
  }

  @Override
  public void ok() {
    m_cPassword = m_jpfPassword.getPassword();
    super.ok();
  }
  
  public static char[] showDialog(Window parent, String title){
    DGetPassword dlg = new DGetPassword(parent, title);
    dlg.setVisible(true);
    return dlg.getPassword();
  }
}
