/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jgoodies.forms.factories.ButtonBarFactory;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.common.utils.StringUtils;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.security.support.SecurityUtils;


/**
 * Modal dialog to display a report on the contents of a supplied keystore.
 */
public class DKeyStoreReport extends AbstractDialog
{
  private static final ResourceBundle RB = Resources.RB;
  
  /** Transformer factory for XML output */
	private static final TransformerFactory TF_FACTORY = TransformerFactory.newInstance();

	static
	{
		try
		{
			// XSLTC in J2SE 5 (why oh why doesn't it grok the "normal"
			// transformer properties... :()
			TF_FACTORY.setAttribute("indent-number", "2");
		}
		catch (IllegalArgumentException e)
		{
			// Ignore.
		}
	}

	/** Transformer properties for XML output */
	private static final Properties TF_PROPS = new Properties();

	static
	{
		try (InputStream in = DKeyStoreReport.class.getResourceAsStream("keystore-report-xml.properties"))
		{
			TF_PROPS.load(in);
		}
		catch (IOException e)
		{
			throw new ExceptionInInitializerError(e);
		}
	}

	/** Stores keystore to report on */
	private final KeyStore m_keystore;
	
	private JPanel contentPane;

	/**
	 * Creates new DKeyStoreReport dialog.
	 * 
	 * @param parent Parent window
	 * @param keystore Keystore to display report on
	 * @throws GeneralSecurityException 
	 * @throws CryptoException A crypto related problem was encountered generating the keystore report
	 */
	public DKeyStoreReport(Window parent, KeyStore keystore) throws GeneralSecurityException
	{
		super(parent);
		setModal(true);
		m_keystore = keystore;
		initComponents();
		pack();
	}

	/**
	 * Initialize the dialog's GUI components.
	 * @throws GeneralSecurityException 
	 * 
	 * @throws CryptoException A crypto related problem was encountered generating the keystore report
	 */
	private void initComponents() throws GeneralSecurityException
	{
	  contentPane = new JPanel(new BorderLayout());
	  
		// Load tree with keystore report
		JTree jtrReport = new JTree(createReportNodes());
		// Top accommodate node icons with spare space (they are 16 pixels tall)
		jtrReport.setRowHeight(18);
		jtrReport.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		// Allow tool tips in tree
		ToolTipManager.sharedInstance().registerComponent(jtrReport);
		// Custom tree node renderer
		jtrReport.setCellRenderer(new ReportTreeCellRend());

		// Expand all nodes in tree
		TreeNode topNode = (TreeNode) jtrReport.getModel().getRoot();
		expandTree(jtrReport, new TreePath(topNode));

		JScrollPane jspReport = new JScrollPane(jtrReport, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
		    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		jspReport.setPreferredSize(new Dimension(450, 350));
		contentPane.add(jspReport, BorderLayout.CENTER);
		
		setTitle(RB.getString("DKeyStoreReport.Title"));
	}

	/**
	 * Expand node and all sub-nodes in a JTree.
	 * 
	 * @param tree The tree.
	 * @param parent Path to node to expand
	 */
	private void expandTree(JTree tree, TreePath parent)
	{
		// Traverse children expending nodes
		TreeNode node = (TreeNode) parent.getLastPathComponent();
		if (node.getChildCount() >= 0)
		{
			for (Enumeration<?> en = node.children(); en.hasMoreElements();)
			{
				TreeNode subNode = (TreeNode) en.nextElement();
				TreePath path = parent.pathByAddingChild(subNode);
				expandTree(tree, path);
			}
		}

		tree.expandPath(parent);
	}
	
	private static X509Certificate[] convert(Certificate[] original){
	  X509Certificate[] result = new X509Certificate[original.length];
	  for (int i = 0; i < result.length; i++) {
      result[i] = (X509Certificate) original[i];
    }
	  return result;
	}

	/**
	 * Copy the keystore report to the clipboard.
	 * 
	 * @param bXml Copy as XML?
	 * @throws GeneralSecurityException 
	 */
	private void copyPressed(boolean bXml) 
	{
		try
		{
			// Get report...
			String sKeyStoreReport = null;

			if (!bXml)
			{
				// ...plain
				sKeyStoreReport = getKeyStoreReport();
			}
			else
			{
				// ...as XML
				sKeyStoreReport = getKeyStoreReportXml();
			}

			// Copy to clipboard
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			StringSelection copy = new StringSelection(sKeyStoreReport);
			clipboard.setContents(copy, copy);
		}
		catch (ParserConfigurationException | TransformerException |GeneralSecurityException ex)
		{
			MessageDialogs.error(this, null, ex);
		}
	}

	/**
	 * Get the KeyStoreReport as XML.
	 * 
	 * @return Keystore report in XML
	 * @throws CryptoException A crypto related problem was encountered generating the keystore report
	 * @throws ParserConfigurationException There was a serious problem creating the XML report
	 * @throws TransformerException There was a serious problem creating the XML report
	 * @throws GeneralSecurityException 
	 */
	private String getKeyStoreReportXml()
	    throws  ParserConfigurationException, TransformerException, GeneralSecurityException
	{
		StringWriter xml = new StringWriter();
		Transformer tr = TF_FACTORY.newTransformer();
		tr.setOutputProperties(TF_PROPS);
		tr.transform(new DOMSource(generateDocument()), new StreamResult(xml));
		return xml.toString();
	}

	/**
	 * Get the KeyStoreReport as plain text.
	 * 
	 * @return Keystore report
	 * @throws GeneralSecurityException 
	 * @throws CryptoException A crypto related problem was encountered generating the keystore report
	 */
	private String getKeyStoreReport() throws GeneralSecurityException
	{
		try
		{
			// Buffer to hold report
			StringBuilder sbReport = new StringBuilder(2000);

			// General keystore information...

			// Keystore type
			String ksType = m_keystore.getType();
			sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.type"), ksType));
			sbReport.append("\n");

			// Keystore provider
			sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.provider"),
			    m_keystore.getProvider().getName()));
			sbReport.append("\n");

			// Keystore size (entries)
			sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.entries"), m_keystore.size()));
			sbReport.append("\n\n");

			Enumeration<String> aliases = m_keystore.aliases();

			// Get information on each keystore entry
			while (aliases.hasMoreElements())
			{
				// Alias
				String sAlias = aliases.nextElement();
				sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.alias"), sAlias));
				sbReport.append("\n");

				// Creation date

//				if (ksType.isEntryCreationDateUseful())
				{
					Date dCreation = m_keystore.getCreationDate(sAlias);

					// Include time zone
					String sCreation =
					    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.LONG).format(dCreation);
					sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.creation"), sCreation));
					sbReport.append("\n");
				}

				Certificate[] certChain = null;

				// Get entry type and certificates
				if (m_keystore.isKeyEntry(sAlias))
				{
					certChain = m_keystore.getCertificateChain(sAlias);

					if (certChain == null || certChain.length == 0)
					{
						sbReport.append(RB.getString("DKeyStoreReport.report.key"));
						sbReport.append("\n");
					}
					else
					{
						sbReport.append(RB.getString("DKeyStoreReport.report.keypair"));
						sbReport.append("\n");
					}
				}
				else
				{
					sbReport.append(RB.getString("DKeyStoreReport.report.trustcert"));
					sbReport.append("\n");

					Certificate cert = m_keystore.getCertificate(sAlias);
					if (cert != null)
					{
						certChain = new Certificate[] { cert };
					}
				}

				// Get information on each certificate in an entry
				if (certChain == null || certChain.length == 0)
				{
					// Zero certificates
					sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.certs"), 0));
					sbReport.append("\n\n");
				}
				else
				{
					X509Certificate[] x509CertChain = convert(certChain);// X509CertUtil.convertCertificates(certChain);

					// One or more certificates
					int iChainLen = x509CertChain.length;
					sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.certs"), iChainLen));
					sbReport.append("\n\n");

					for (int iCnt = 0; iCnt < iChainLen; iCnt++)
					{
						// Get information on an individual certificate
						sbReport.append(
						    MessageFormat.format(RB.getString("DKeyStoreReport.report.cert"), iCnt + 1, iChainLen));
						sbReport.append("\n");

						X509Certificate x509Cert = x509CertChain[iCnt];

						// Version
						sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.version"),
						    x509Cert.getVersion()));
						sbReport.append("\n");

						// Subject
						sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.subject"),
						    x509Cert.getSubjectDN()));
						sbReport.append("\n");

						// Issuer
						sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.issuer"),
						    x509Cert.getIssuerDN()));
						sbReport.append("\n");

						// Serial Number
						StringBuilder sSerialNumber = StringUtils.toHex(x509Cert.getSerialNumber(), 4, " ");
						sbReport.append(
						    MessageFormat.format(RB.getString("DKeyStoreReport.report.serial"), sSerialNumber));
						sbReport.append("\n");

						// Valid From
						Date dValidFrom = x509Cert.getNotBefore();
						String sValidFrom =
						    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dValidFrom);
						sbReport.append(
						    MessageFormat.format(RB.getString("DKeyStoreReport.report.validfrom"), sValidFrom));
						sbReport.append("\n");

						// Valid Until
						Date dValidTo = x509Cert.getNotAfter();
						String sValidTo =
						    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dValidTo);
						sbReport.append(
						    MessageFormat.format(RB.getString("DKeyStoreReport.report.validuntil"), sValidTo));
						sbReport.append("\n");

						// Public Key (algorithm and key size)
						int iKeySize = SecurityUtils.getKeyLength(x509Cert.getPublicKey());
						String sKeyAlg = x509Cert.getPublicKey().getAlgorithm();
						String fmtKey = (iKeySize == SecurityUtils.UNKNOWN_KEY_SIZE)
						    ? "DKeyStoreReport.report.pubkeynosize" : "DKeyStoreReport.report.pubkey";
						sbReport.append(MessageFormat.format(RB.getString(fmtKey), sKeyAlg, iKeySize));
						sbReport.append("\n");

						// Signature Algorithm
						sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.sigalg"),
						    x509Cert.getSigAlgName()));
						sbReport.append("\n");

						byte[] bCert = x509Cert.getEncoded();

						// SHA-1 fingerprint
						sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.sha1"),
						    SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_SHA1)));
						sbReport.append("\n");

						// MD5 fingerprint
						sbReport.append(MessageFormat.format(RB.getString("DKeyStoreReport.report.md5"),
						    SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_MD5)));
						sbReport.append("\n");

						if (iCnt + 1 < iChainLen)
						{
							sbReport.append("\n");
						}
					}

					if (aliases.hasMoreElements())
					{
						sbReport.append("\n");
					}
				}
			}

			// Return the report
			return sbReport.toString();
		}
		catch (GeneralSecurityException ex)
		{
			throw new GeneralSecurityException(RB.getString("DKeyStoreReport.NoGenerateReport.exception.message"), ex);
		}
	}

	/**
	 * Generate the keystore report as an XML Document.
	 * 
	 * @return The KeyStiore report as an XML Document
	 * @throws CryptoException A crypto related problem was encountered generating the keystore report
	 * @throws ParserConfigurationException There was a serious problem creating the XML report
	 * @throws GeneralSecurityException 
	 */
	private Document generateDocument()
	    throws ParserConfigurationException, GeneralSecurityException
	{
		try
		{
			// Create a new document object
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document xmlDoc = docBuilder.newDocument();

			// General keystore information
			String ksType = m_keystore.getType();//KeyStoreType.valueOfType(m_keystore.getType());
			String sProvider = m_keystore.getProvider().getName();

			Element keystoreElement = xmlDoc.createElement("keystore");
			keystoreElement.setAttribute("type", ksType);
			keystoreElement.setAttribute("provider", sProvider);
			xmlDoc.appendChild(keystoreElement);

			Enumeration<String> aliases = m_keystore.aliases();

			// Get information on each keystore entry
			while (aliases.hasMoreElements())
			{
				String sAlias = aliases.nextElement();

				String sCreation = null;
				//if (ksType.isEntryCreationDateUseful())
				{
					Date dCreation = m_keystore.getCreationDate(sAlias);
					sCreation = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dCreation);
				}

				String sEntryType = null;
				Certificate[] certChain = null;

				// Get entry type and certificates
				if (m_keystore.isKeyEntry(sAlias))
				{
					certChain = m_keystore.getCertificateChain(sAlias);

					if (certChain == null || certChain.length == 0)
					{
						sEntryType = "Key";
					}
					else
					{
						sEntryType = "KeyPair";
					}
				}
				else
				{
					sEntryType = "TrustedCertificate";
					Certificate cert = m_keystore.getCertificate(sAlias);
					if (cert != null)
					{
						certChain = new Certificate[] { cert };
					}
				}

				Element entryElement = xmlDoc.createElement("entry");
				entryElement.setAttribute("alias", sAlias);

				if (sCreation != null)
				{
					entryElement.setAttribute("creation_date", sCreation);
				}

				entryElement.setAttribute("type", sEntryType);
				keystoreElement.appendChild(entryElement);

				// Get information on each certificate in an entry
				if (certChain != null)
				{
					X509Certificate[] x509CertChain = convert(certChain);//X509CertUtil.convertCertificates(certChain);

					int iChainLen = x509CertChain.length;

					for (int iCnt = 0; iCnt < iChainLen; iCnt++)
					{
						X509Certificate x509Cert = x509CertChain[iCnt];

						Element certificateElement = xmlDoc.createElement("certificate");
						entryElement.appendChild(certificateElement);

						// Get information on an individual certificate

						// Version
						Element versionNumberElement = xmlDoc.createElement("version");
						certificateElement.appendChild(versionNumberElement);
						versionNumberElement.appendChild(xmlDoc.createTextNode("" + x509Cert.getVersion()));

						// Subject
						Element subjectElement = xmlDoc.createElement("subject");
						certificateElement.appendChild(subjectElement);
						subjectElement.appendChild(xmlDoc.createTextNode(x509Cert.getSubjectDN().toString()));

						// Issuer
						Element issuerElement = xmlDoc.createElement("issuer");
						certificateElement.appendChild(issuerElement);
						issuerElement.appendChild(xmlDoc.createTextNode(x509Cert.getIssuerDN().toString()));

						// Serial Number
						Element serialNumberElement = xmlDoc.createElement("serial_number");
						certificateElement.appendChild(serialNumberElement);
						serialNumberElement.appendChild(
						    xmlDoc.createTextNode(StringUtils.toHex(x509Cert.getSerialNumber(), 4, " ").toString()));

						// Valid From
						Date dValidFrom = x509Cert.getNotBefore();
						String sValidFrom =
						    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dValidFrom);

						Element validFromElement = xmlDoc.createElement("valid_from");
						certificateElement.appendChild(validFromElement);
						validFromElement.appendChild(xmlDoc.createTextNode(sValidFrom));

						// Valid Until
						Date dValidTo = x509Cert.getNotAfter();
						String sValidTo =
						    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dValidTo);

						Element validUntilElement = xmlDoc.createElement("valid_until");
						certificateElement.appendChild(validUntilElement);
						validUntilElement.appendChild(xmlDoc.createTextNode(sValidTo));

						// Public Key (algorithm and key size)
						int iKeySize = SecurityUtils.getKeyLength(x509Cert.getPublicKey());
						String sKeyAlg = x509Cert.getPublicKey().getAlgorithm();
						if (iKeySize != SecurityUtils.UNKNOWN_KEY_SIZE)
						{
							sKeyAlg = MessageFormat.format(RB.getString("DKeyStoreReport.KeyAlg"), sKeyAlg, iKeySize);
						}

						Element publicKeyAlgElement = xmlDoc.createElement("public_key_algorithm");
						certificateElement.appendChild(publicKeyAlgElement);
						publicKeyAlgElement.appendChild(xmlDoc.createTextNode(sKeyAlg));

						// Signature Algorithm
						Element signatureAlgElement = xmlDoc.createElement("signature_algorithm");
						certificateElement.appendChild(signatureAlgElement);
						signatureAlgElement.appendChild(xmlDoc.createTextNode(x509Cert.getSigAlgName()));

						// Fingerprints
						byte[] bCert = x509Cert.getEncoded();

						Element sha1FingerprintElement = xmlDoc.createElement("sha1_fingerprint");
						certificateElement.appendChild(sha1FingerprintElement);
						sha1FingerprintElement.appendChild(
						    xmlDoc.createTextNode(SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_SHA1)));

						Element md5FingerprintElement = xmlDoc.createElement("md5_fingerprint");
						certificateElement.appendChild(md5FingerprintElement);
						md5FingerprintElement.appendChild(
						    xmlDoc.createTextNode(SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_MD5)));
					}
				}
			}

			return xmlDoc;
		}
		catch (GeneralSecurityException ex)
		{
			throw new GeneralSecurityException(RB.getString("DKeyStoreReport.NoGenerateReport.exception.message"), ex);
		}
	}

	/**
	 * Create tree node with keystore report.
	 * 
	 * @throws CryptoException A crypto related problem was encountered creating the tree node
	 * @return The tree node
	 * @throws GeneralSecurityException 
	 */
	private DefaultMutableTreeNode createReportNodes() throws GeneralSecurityException
	{
		try
		{
			// Keystore type
			String ksType = m_keystore.getType();

			// Keystore provider
			String sProvider = m_keystore.getProvider().getName();

			// Top node
			DefaultMutableTreeNode topNode = new DefaultMutableTreeNode(
			    MessageFormat.format(RB.getString("DKeyStoreReport.TopNodeName"), ksType, sProvider));

			// One sub-node per entry
			Enumeration<String> aliases = m_keystore.aliases();

			// Get information on each keystore entry
			while (aliases.hasMoreElements())
			{
				// Entry alias
				String sAlias = aliases.nextElement();

				Certificate[] certChain = null;
				DefaultMutableTreeNode entryNode = null;

				// Entry type
				if (m_keystore.isKeyEntry(sAlias))
				{
					certChain = m_keystore.getCertificateChain(sAlias);

					if (certChain == null || certChain.length == 0)
					{
						entryNode = new DefaultMutableTreeNode(ReportTreeCellRend.Entry.getKeyInstance(sAlias));
					}
					else
					{
						entryNode = new DefaultMutableTreeNode(ReportTreeCellRend.Entry.getKeyPairInstance(sAlias));
					}
				}
				else
				{
					entryNode =
					    new DefaultMutableTreeNode(ReportTreeCellRend.Entry.getTrustedCertificateInstance(sAlias));

					Certificate cert = m_keystore.getCertificate(sAlias);
					if (cert != null)
					{
						certChain = new Certificate[] { cert };
					}
				}

				topNode.add(entryNode);

				// Creation date, if applicable
				//if (ksType.isEntryCreationDateUseful())
				{
					Date dCreation = m_keystore.getCreationDate(sAlias);
					String sCreation =
					    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dCreation);
					entryNode.add(new DefaultMutableTreeNode(sCreation));
				}

				// One or more certificates?
				if (certChain != null && certChain.length != 0)
				{
					DefaultMutableTreeNode certsNode =
					    new DefaultMutableTreeNode(RB.getString("DKeyStoreReport.Certificates"));
					entryNode.add(certsNode);

					// Get information on each certificate in entry
					X509Certificate[] x509CertChain = convert(certChain);//X509CertUtil.convertCertificates(certChain);

					int iChainLen = x509CertChain.length;

					for (int iCnt = 0; iCnt < iChainLen; iCnt++)
					{
						DefaultMutableTreeNode certNode = new DefaultMutableTreeNode(
						    MessageFormat.format(RB.getString("DKeyStoreReport.Certificate"), iCnt + 1, iChainLen));
						certsNode.add(certNode);

						X509Certificate x509Cert = x509CertChain[iCnt];

						// Version
						certNode.add(new DefaultMutableTreeNode("" + x509Cert.getVersion()));

						// Subject
						certNode.add(new DefaultMutableTreeNode(x509Cert.getSubjectDN()));

						// Issuer
						certNode.add(new DefaultMutableTreeNode(x509Cert.getIssuerDN()));

						// Serial Number
						StringBuilder sSerialNumber = StringUtils.toHex(x509Cert.getSerialNumber(), 4, " ");
						certNode.add(new DefaultMutableTreeNode(sSerialNumber));

						// Valid From
						Date dValidFrom = x509Cert.getNotBefore();
						String sValidFrom =
						    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dValidFrom);
						certNode.add(new DefaultMutableTreeNode(sValidFrom));

						// Valid Until
						Date dValidTo = x509Cert.getNotAfter();
						String sValidTo =
						    DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM).format(dValidTo);
						certNode.add(new DefaultMutableTreeNode(sValidTo));

						// Public Key (algorithm and key size)
						int iKeySize = SecurityUtils.getKeyLength(x509Cert.getPublicKey());
						String sKeyAlg = x509Cert.getPublicKey().getAlgorithm();
						if (iKeySize != SecurityUtils.UNKNOWN_KEY_SIZE)
						{
							sKeyAlg = MessageFormat.format(RB.getString("DKeyStoreReport.KeyAlg"), sKeyAlg, iKeySize);
						}
						certNode.add(new DefaultMutableTreeNode(sKeyAlg));

						// Signature Algorithm
						certNode.add(new DefaultMutableTreeNode(x509Cert.getSigAlgName()));

						byte[] bCert = x509Cert.getEncoded();

						// SHA-1 fingerprint
						certNode.add(new DefaultMutableTreeNode(SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_SHA1)));

						// MD5 fingerprint
						certNode.add(new DefaultMutableTreeNode(SecurityUtils.getMessageDigest(bCert, SecurityUtils.MD_ALGORITHM_MD5)));
					}
				}
			}

			return topNode;
		}
		catch (GeneralSecurityException ex)
		{
			throw new GeneralSecurityException(RB.getString("DKeyStoreReport.NoGenerateReport.exception.message"), ex);
		}
	}

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    JButton btnCopy = new JButton(RB.getString("DKeyStoreReport.jbCopy.text"));
    btnCopy.setMnemonic(RB.getString("DKeyStoreReport.jbCopy.mnemonic").charAt(0));
    btnCopy.setToolTipText(RB.getString("DKeyStoreReport.jbCopy.tooltip"));
    btnCopy.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent evt)
      {
        copyPressed(false);
      }
    });

    JButton btnCopyXml = new JButton(RB.getString("DKeyStoreReport.jbCopyXml.text"));
    btnCopyXml.setMnemonic(RB.getString("DKeyStoreReport.jbCopyXml.mnemonic").charAt(0));
    btnCopyXml.setToolTipText(RB.getString("DKeyStoreReport.jbCopyXml.tooltip"));
    btnCopyXml.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent evt)
      {
        copyPressed(true);
      }
    });
    
    
    Action action = getAction(ACTION_KEY_OK);
    setDefaultAction(action);
    JButton btnOk = new JButton(action);
    
    return  ButtonBarFactory.buildRightAlignedBar(btnCopy, btnCopyXml, btnOk);
  }
}
