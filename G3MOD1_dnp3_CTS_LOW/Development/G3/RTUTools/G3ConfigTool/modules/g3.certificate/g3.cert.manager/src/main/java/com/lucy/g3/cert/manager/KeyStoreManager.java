/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.log4j.Logger;


/**
 * Wrap java keystore and provides API for accessing the keystore and serialisation.
 * Key features: Import/export/remove trusted certificates. Generate KeyStore
 * report to view existing trusted certificates. Test certificates by connecting
 * to RTU.
 */
public class KeyStoreManager  {
  public final static String PROPERTY_IS_CHANGED = "changed";
  public final static String PROPERTY_KEYSTORE_FILE = "keyStoreFile";

  private KeyStore keyStore;
  private char[] password;

  private File keyStoreFile;
  
  private PropertyChangeSupport support = new PropertyChangeSupport(this); 

  /**
   * Indicator as to whether or not the keystore has been altered since its last
   * save
   */
  private boolean m_bChanged;

  private CertificateFactory cf;


  public KeyStoreManager() {
    try {
      cf = CertificateFactory.getInstance("X509");
      this.keyStore = createEmptyKeyStore();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public KeyStore getKeyStore() {
    return keyStore;
  }

  public void setKeyStore(KeyStore keyStore) {
    if (keyStore == null)
      throw new IllegalArgumentException("keyStore must not be null");

    this.keyStore = keyStore;
  }

  public void load(String keyStoreFile, char[] password) throws Exception {
    File file = new File(keyStoreFile);
    
    FileInputStream is = new FileInputStream(file);
    
    try {
      keyStore.load(is, password);
      this.password = password;
      setKeyStoreFile(file);
      setChanged(false);
    }finally{
      try{
        is.close();
      }catch(Throwable e){}
    }
  }

  public void store() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
    if(getKeyStoreFile() == null || getPassword() == null)
      throw new IllegalStateException("Both file path and password must be set before calling store()");
    
    store(getKeyStoreFile(), getPassword());
  }
  
  public void store(File targetKeyStoreFile, char[] password)
      throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
    FileOutputStream os = new FileOutputStream(targetKeyStoreFile);
    keyStore.store(os, password);


    // Update the keystore wrapper
    setPassword(password);
    setKeyStoreFile(targetKeyStoreFile);
    
    setChanged(false);
    
    os.close();
  }

  public void delete(String alias) throws KeyStoreException {
    keyStore.deleteEntry(alias);
    setChanged(true);
  }

  public void importTrustedCert(String alias, InputStream in) throws Exception {
    if (alias == null) {
      throw new IllegalArgumentException("alias must not be null");
    }

    // Read the certificate
    X509Certificate cert = null;
    try {
      cert = (X509Certificate) cf.generateCertificate(in);
    } catch (Exception cce) {
      throw new Exception("Input is not an X.509 certificate");
    }

    // if certificate is self-signed, make sure it verifies
    if (isSelfSigned(cert)) {
      cert.verify(cert.getPublicKey());
    }

    importTrustedCert(alias, cert);
  }

  public void importTrustedCert(String sAlias, X509Certificate trustCert) throws KeyStoreException {
    // Delete old entry first
    if (keyStore.containsAlias(sAlias)) {
      keyStore.deleteEntry(sAlias);
    }

    // Import the trusted certificate
    keyStore.setCertificateEntry(sAlias, trustCert);

    // Update the keystore manager
    setChanged(true);
  }

  /**
   * Returns true if the certificate is self-signed, false otherwise.
   */
  private boolean isSelfSigned(X509Certificate cert) {
    return signedBy(cert, cert);
  }

  private boolean signedBy(X509Certificate end, X509Certificate ca) {
    if (!ca.getSubjectDN().equals(end.getIssuerDN())) {
      return false;
    }
    try {
      end.verify(ca.getPublicKey());
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public int getEntriesSize() {
    try {
      return keyStore.size();
    } catch (KeyStoreException e) {
      return -1;
    }
  }

  public File getKeyStoreFile() {
    return keyStoreFile;
  }

  public char[] getPassword() {
    return password;
  }

  public void setPassword(char[] cPassword) {
    this.password = cPassword;
    setChanged(true);
  }

  public void setKeyStoreFile(File keyStoreFile) {
    Object oldValue = getKeyStoreFile();
    this.keyStoreFile = keyStoreFile;
    support.firePropertyChange(PROPERTY_KEYSTORE_FILE, oldValue, keyStoreFile);
  }

  /**
   * Register with the wrapper whether the keystore has been changed since its
   * last save.
   * 
   * @param bChanged
   *          Has the keystore been changed?
   */
  private void setChanged(boolean bChanged) {
    m_bChanged = bChanged;
    
    // Always fire event
    support.firePropertyChange(PROPERTY_IS_CHANGED, !bChanged, bChanged);
  }

  /**
   * Has the keystore been changed since its last save?
   * 
   * @return True if it has been changed, false otherwise
   */
  public boolean isChanged() {
    return m_bChanged;
  }

  public String getKeyStoreType() {
    return keyStore == null ? null : keyStore.getType();
  }

  public String getKeyStoreProvider() {
    return keyStore == null ? null : keyStore.getProvider().getName();
  }

  public boolean containsAlias(String alias) throws KeyStoreException {
    return keyStore.containsAlias(alias);
  }

  /**
   * Creates the new entry with the new name and copy the old entry across.
   * 
   * @param oldAlias
   * @param newAlias
   * @throws KeyStoreException
   */
  public void renameEntry(String oldAlias, String newAlias) throws KeyStoreException {
    // If the entry is a key pair...
    if (keyStore.isKeyEntry(oldAlias)) {
      throw new UnsupportedOperationException("Key Entry is not support");
    } else
    // ...if the entry is a trusted certificate
    {
      // Do the copy
      Certificate cert = keyStore.getCertificate(oldAlias);
      keyStore.setCertificateEntry(newAlias, cert);
    }

    // Delete the old entry
    keyStore.deleteEntry(oldAlias);

    setChanged(true);
  }

  public X509Certificate getCertificate(String alias) throws KeyStoreException {
    return (X509Certificate) keyStore.getCertificate(alias);
  }

  public static boolean createEmptyKeyStore(File ksStoreFile, char[] password) {
    KeyStore ks;
    try {
      ks = createEmptyKeyStore();
      FileOutputStream os = new FileOutputStream(ksStoreFile);
      ks.store(os, password);
      os.close();
      return true;
    } catch (NoSuchAlgorithmException | CertificateException | KeyStoreException | IOException e) {
      Logger.getLogger(KeyStoreManager.class).error("Failed to create keystore file", e);
      return false;
    }
  }
  
  public static KeyStore createEmptyKeyStore()
      throws IOException, NoSuchAlgorithmException, CertificateException, KeyStoreException {
    KeyStore keyStore = KeyStore.getInstance("JKS");
    keyStore.load(null, null);
    return keyStore;
  }

  
  //================== Delegate Methods ===================
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    support.addPropertyChangeListener(listener);
  }
  
  public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
    support.addPropertyChangeListener(property, listener);
  }
  
  public void removePropertyChangeListener(PropertyChangeListener listener) {
    support.removePropertyChangeListener(listener);
  }
  
  public void removePropertyChangeListener(String property, PropertyChangeListener listener) {
    support.removePropertyChangeListener(property, listener);
  }
  
  
}
