/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import java.awt.Window;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.cert.manager.KeyStoreManager;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.security.support.SSLSupport;

/**
 * Implementation of all actions for managing key store.
 */
public class KeyStoreActions {

  private static final String TEXT_NO_SELECTED_ENTRIES = "No selected entries!";

  private ResourceBundle RB = Resources.RB;

  public static final String ACTION_EXIT = "exit";
  public static final String ACTION_SETKEYSTOREPASSWORD = "setKeyStorePassword";
  public static final String ACTION_OPENKEYSTORE = "openKeystore";
  public static final String ACTION_NEWKEYSTORE = "newKeyStore";
  public static final String ACTION_SAVEKEYSTORE = "saveKeyStore";
  public static final String ACTION_SAVEKEYSTOREAS = "saveKeyStoreAs";
  public static final String ACTION_EXAMINECERT = "examineCert";
  public static final String ACTION_EXAMINECERTSSL = "examineCertSSL";
  public static final String ACTION_IMPORTTRUSTEDCERT = "importTrustedCert";
  public static final String ACTION_KEYSTOREREPORT = "keyStoreReport";
  public static final String ACTION_EXPORTENTRY = "exportEntry";
  public static final String ACTION_CERTDETAILS = "certDetails";
  public static final String ACTION_DELETEENTRY = "deleteEntry";
  public static final String ACTION_RENAMECERT = "renameCert";

  private final ApplicationActionMap actionMap = Application.getInstance().getContext().getActionMap(this);

  private KeyStoreManager manager;
  private ActionCallback callback;


  public KeyStoreActions(KeyStoreManager manager, ActionCallback callback) {
    this.manager = manager;
    this.callback = callback;
  }

  public Action getAction(String key) {
    Action action = actionMap.get(key);

    if (action == null)
      return null;

    Window parent = getWindow();
    if (parent != null) {
      action = new BusyCursorAction(action, parent);
    }

    return action;
  }

  @org.jdesktop.application.Action
  public void exit() {
    // Does the current keystore contain unsaved changes?
    if (needSave()) {
      // Yes - ask the user if it should be saved
      switch (wantSave()) {
      case JOptionPane.YES_OPTION:
        // Save it
        saveKeyStore();
        break;
      case JOptionPane.CANCEL_OPTION:
        return;
      }
    }

    Window win = getWindow();
    if (win != null)
      win.dispose();
  }

  private Window getWindow() {
    return callback == null ? null : callback.getParent();
  }

  /**
   * Let the user set the keystore's password.
   * 
   * @return True if the password was set, false otherwise
   */
  @org.jdesktop.application.Action
  public boolean setKeyStorePassword() {
    char[] cPassword = getNewKeyStorePassword();

    // User canceled
    if (cPassword == null) {
      return false;
    }

    // Update the keystore wrapper
    manager.setPassword(cPassword);

    // Update the frame's components and title
    updateControls();
    return true;
  }

  private void updateControls() {
    actionMap.get(ACTION_SAVEKEYSTORE).setEnabled(manager.isChanged() || manager.getKeyStoreFile() == null);
  }

  @org.jdesktop.application.Action
  public void openKeystore() {
    File ksFile = DialogUtils.showOpenDialog(getWindow(), "Open Keystore File",
        FileChooserFactory.getKeyStoreFileChooser());
    if (ksFile == null)
      return;

    // Get the user to enter the keystore's password
    DGetPassword dGetPassword = new DGetPassword(getWindow(),
        MessageFormat.format("Password for Keystore ''{0}''", ksFile.getName()));
    dGetPassword.setVisible(true);

    char[] cPassword = dGetPassword.getPassword();

    if (cPassword == null) {
      return;
    }

    try {
      this.manager.load(ksFile.getAbsolutePath(), cPassword);
      updateControls();
    } catch (Exception e1) {
      MessageDialogs.error(getWindow(), "Failed to open keystore file!", e1);
    }
  }

  /**
   * Check if the currently opened keystore requires to be saved.
   * 
   * @return True if the keystore has been changed since the last open/save,
   *         false otherwise
   */
  private boolean needSave() {
    return (manager != null && manager.isChanged());
  }

  /**
   * Ask the user if they want to save the current keystore file.
   * 
   * @return JOptionPane.YES_OPTION, JOptionPane.NO_OPTION or
   *         JOptionPane.CANCEL_OPTION; JOptionPane.CLOSED_OPTION is reported as
   *         JOptionPane.CANCEL_OPTION
   */
  private int wantSave() {

    File fKeyStoreFile = manager.getKeyStoreFile();
    String sKeyStoreName;

    if (fKeyStoreFile == null) {
      sKeyStoreName = RB.getString("FPortecle.Untitled");
    } else {
      sKeyStoreName = fKeyStoreFile.getName();
    }

    String sMessage = MessageFormat.format(RB.getString("FPortecle.WantSaveChanges.message"), sKeyStoreName);

    int iSelected =
        JOptionPane.showConfirmDialog(getWindow(), sMessage, RB.getString("FPortecle.WantSaveChanges.Title"),
            JOptionPane.YES_NO_CANCEL_OPTION);
    if (iSelected == JOptionPane.CLOSED_OPTION) {
      iSelected = JOptionPane.CANCEL_OPTION;
    }
    return iSelected;
  }

  /**
   * Create a new keystore file.
   * 
   * @return True is a new keystore file is created, false otherwise
   */
  @org.jdesktop.application.Action
  public boolean newKeyStore() {
    // Does the current keystore contain unsaved changes?
    if (needSave()) {
      // Yes - ask the user if it should be saved
      int iWantSave = wantSave();

      if (iWantSave == JOptionPane.YES_OPTION) {
        // Save it
        if (!saveKeyStore()) {
          return false;
        }
      } else if (iWantSave == JOptionPane.CANCEL_OPTION) {
        return false;
      }
    }

    try {

      // Create new keystore
      KeyStore newKeyStore = KeyStoreManager.createEmptyKeyStore();

      // Update the keystore wrapper
      manager.setKeyStore(newKeyStore);

      // Update the frame's components and title
      updateControls();

      return true;
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }
  }

  /**
   * Get a new keystore password.
   * 
   * @return The new keystore password
   */
  private char[] getNewKeyStorePassword() {
    // Display the get new password dialog
    DGetNewPassword dGetNewPassword =
        new DGetNewPassword(getWindow(), RB.getString("FPortecle.SetKeyStorePassword.Title"));
    dGetNewPassword.setVisible(true);

    // Dialog returned - retrieve the password and return it
    return dGetNewPassword.getPassword();
  }

  @org.jdesktop.application.Action
  public boolean saveKeyStore() {
    // File to save to
    File fSaveFile = manager.getKeyStoreFile();

    // Not saved before - use Save As
    if (fSaveFile == null) {
      return saveKeyStoreAs();
    }

    // Get the password to protect the keystore with
    char[] cPassword = manager.getPassword();

    // No password set for keystore - get one from the user
    if (cPassword == null) {
      cPassword = getNewKeyStorePassword();

      // User canceled - cancel save
      if (cPassword == null) {
        return false;
      }
    }

    try {
      // Do the save
      manager.store(fSaveFile, cPassword);

      // Update the frame's components and title
      updateControls();

      MessageDialogs.finish(getWindow(), "The keystore is saved!");

      return true;
    } catch (FileNotFoundException ex) {
      JOptionPane.showMessageDialog(getWindow(),
          MessageFormat.format(RB.getString("FPortecle.NoWriteFile.message"), fSaveFile),
          RB.getString("FPortecle.SaveKeyStore.Title"), JOptionPane.WARNING_MESSAGE);
      return false;
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }
  }

  /**
   * Save the currently opened keystore to disk to what may be a different file
   * from the one it was opened from (if any).
   * 
   * @return True if the keystore is saved to disk, false otherwise
   */
  @org.jdesktop.application.Action
  public boolean saveKeyStoreAs() {
    // Keystore's current password
    char[] cPassword = manager.getPassword();

    // Get a new password if this keystore exists in another file or is an
    // unsaved keystore for which no
    // password has been set yet
    if (manager.getKeyStoreFile() != null || (manager.getKeyStoreFile() == null && cPassword == null)) {
      cPassword = getNewKeyStorePassword();

      if (cPassword == null) {
        return false;
      }
    }

    // Let the user choose a save file
    JFileChooser chooser = FileChooserFactory.getKeyStoreFileChooser();
    File fSaveFile =
        DialogUtils.showSaveDialog(getWindow(), RB.getString("FPortecle.SaveKeyStoreAs.Title"), chooser);
    if (fSaveFile == null)
      return false;

    if (!fSaveFile.getName().endsWith(".jks")) {
      fSaveFile = new File(fSaveFile.getAbsolutePath() + ".jks");
    }

    try {
      if (fSaveFile.exists()) {
        if (MessageDialogs.showOverwriteDialog(getWindow(), fSaveFile) == false)
          return false;
      }

      // Save the keystore to file
      manager.store(fSaveFile, cPassword);

      // Update the frame's components and title
      updateControls();

      return true;
    } catch (FileNotFoundException ex) {
      JOptionPane.showMessageDialog(getWindow(),
          MessageFormat.format(RB.getString("FPortecle.NoWriteFile.message"), fSaveFile),
          RB.getString("FPortecle.SaveKeyStoreAs.Title"), JOptionPane.WARNING_MESSAGE);
      return false;
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }
  }

  private String getSelectedType() {
    return callback == null ? null : callback.getSelectedType();
  }

  private String getSelectedAlias() {
    return callback == null ? null : callback.getSelectedAlias();
  }

  /**
   * Let the user examine the contents of a certificate file.
   * 
   * @param fCertFile
   *          File to load the certificate from; if <code>null</code>, prompt
   *          user
   */
  @org.jdesktop.application.Action
  public void examineCert() {
    JFileChooser chooser = FileChooserFactory.getCertFileChooser();
    File certFile = DialogUtils.showOpenDialog(getWindow(), "Choose a certificate file to examine", chooser);

    if (certFile == null)
      return;

    // Show the certificates
    DViewCertificate.showAndWait(getWindow(), certFile);
  }

  @org.jdesktop.application.Action
  public void examineCertSSL() {
    executeExamineCertSSLTask(Application.getInstance(), getWindow(), null);
  }

  private static ExamineCertSSLTask task;
  
  public static void executeExamineCertSSLTask(Application app, Window parent, InetSocketAddress address) {
    if(task != null && !task.isDone()) {
      return;// The task is already running.
    }
    
    if (address == null) {
      DGetHostPort d = new DGetHostPort(parent, "Examine SSL/TLS Connection", null);
      d.setVisible(true);
      address = d.getHostPort();
    }

    if (address == null) {
      return; 
    }
    
    task = new ExamineCertSSLTask(app, parent, address);
    app.getContext().getTaskService().execute(task);
  }
  
  private static class ExamineCertSSLTask extends Task<Void, Void> {
    private Window parent;
    private InetSocketAddress address;
    private String protocol = null;
    private String cipherSuite = null;
    private X509Certificate[] certs;
    
    public ExamineCertSSLTask(Application application, Window parent, InetSocketAddress address) {
      super(application);
      this.parent = parent;
      this.address = Preconditions.checkNotNull(address, "address must not be null");
      setUserCanCancel(true);
      setInputBlocker(new DefaultInputBlocker(this, BlockingScope.WINDOW, parent, null));
    }

    @Override
    protected Void doInBackground() throws Exception {
      Thread.sleep(500);
      certs = SSLSupport.getPeerCerts(address);
      protocol = SSLSupport.getPeerProtocol();
      cipherSuite = SSLSupport.getPeerCipherSuite();
      
      if(isCancelled())
        return null;
      
      return null;
    }

    @Override
    protected void failed(Throwable cause) {
      Logger.getLogger(getClass()).error("Failed to examine SSL certificate of address:"+address, cause);
      MessageDialogs.error(parent, null, cause);
    }

    @Override
    protected void succeeded(Void result) {
      // If there are any display the view certificate dialog with them
      if (certs != null && certs.length != 0) {
          DViewCertificate dViewCertificate =
              new DViewCertificate(parent, address.getHostName(), certs[0], protocol, cipherSuite);
          dViewCertificate.setTitle(MessageFormat.format("Details for SSL/TLS connection ''{0}''",
              address.getHostName() + ":" + address.getPort()));
          
          if(isCancelled())
            return;
          
          dViewCertificate.setVisible(true);
      }
    }
  }

  /**
   * Open a certificate file.
   * 
   * @param fCertFile
   *          The certificate file
   * @return The certificates found in the file or null if there were none
   */
  @SuppressWarnings("restriction")
  private X509Certificate openCert(File fCertFile) {
    try {
      return new sun.security.x509.X509CertImpl(new FileInputStream(fCertFile));
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return null;
    }
  }

  /**
   * Let the user import a trusted certificate.
   * 
   * @return True if the import is successful, false otherwise
   */
  @org.jdesktop.application.Action
  public boolean importTrustedCert() {
    JFileChooser chooser = FileChooserFactory.getCertFileChooser();
    File fCertFile = DialogUtils.showOpenDialog(getWindow(), "Import Certificate", chooser);

    // User cancelled
    if (fCertFile == null)
      return false;

    // Load the certificate(s)
    X509Certificate trustCert = openCert(fCertFile);
    if (trustCert == null) {
      return false;
    }

    try {
      // Get the keystore
      KeyStore keyStore = manager.getKeyStore();

      // Certificate already exists in the keystore
      String sMatchAlias = X509CertUtil.matchCertificate(keyStore, trustCert);
      if (sMatchAlias != null) {
        int iSelected = JOptionPane.showConfirmDialog(getWindow(),
            MessageFormat.format(RB.getString("FPortecle.TrustCertExistsConfirm.message"), sMatchAlias),
            RB.getString("FPortecle.ImportTrustCert.Title"), JOptionPane.YES_NO_OPTION);
        if (iSelected != JOptionPane.YES_OPTION) {
          return false;
        }
      }

      // If we cannot establish trust for the certificate against the CA
      // certificates keystore or the
      // current keystore then, display the certificate to the user for
      // confirmation
      KeyStore[] compKeyStores = null;

      // Establish against CA certificates keystore and current keystore
      compKeyStores = new KeyStore[] { keyStore };

      if (X509CertUtil.establishTrust(compKeyStores, trustCert) == null) {
        // Tell the user what is happening
        JOptionPane.showMessageDialog(getWindow(), RB.getString("FPortecle.NoTrustPathCertConfirm.message"),
            RB.getString("FPortecle.ImportTrustCert.Title"), JOptionPane.INFORMATION_MESSAGE);

        // Display the certificate to the user
        DViewCertificate dViewCertificate = new DViewCertificate(getWindow(),
            MessageFormat.format(RB.getString("FPortecle.CertDetails.Title"), fCertFile.getName()),
            trustCert);
        dViewCertificate.setVisible(true);

        // Request confirmation that the certificate is to be trusted
        int iSelected = JOptionPane.showConfirmDialog(getWindow(), RB.getString("FPortecle.AcceptTrustCert.message"),
            RB.getString("FPortecle.ImportTrustCert.Title"), JOptionPane.YES_NO_OPTION);
        if (iSelected != JOptionPane.YES_OPTION) {
          return false;
        }
      }

      String sAlias = "";// X509CertUtil.getCertificateAlias(trustCert).toLowerCase();
      sAlias = getNewEntryAlias(manager, sAlias, "FPortecle.TrustCertEntryAlias.Title", false);
      if (sAlias == null) {
        return false;
      }

      manager.importTrustedCert(sAlias, trustCert);

      // Update the frame's components and title
      updateControls();

      // Display success message
      JOptionPane.showMessageDialog(getWindow(), RB.getString("FPortecle.ImportTrustCertSuccessful.message"),
          RB.getString("FPortecle.ImportTrustCert.Title"), JOptionPane.INFORMATION_MESSAGE);

      return true;
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }
  }

  /**
   * Gets a new entry alias from user, handling overwrite issues.
   * 
   * @param keyStore
   *          target keystore
   * @param sAlias
   *          suggested alias
   * @param dialogTitleKey
   *          message key for dialog titles
   * @param selectAlias
   *          whether to pre-select alias text in text field
   * @return alias for new entry, null if user cancels the operation
   */
  private String getNewEntryAlias(KeyStoreManager manager, String sAlias, String dialogTitleKey, boolean selectAlias)
      throws KeyStoreException {
    while (true) {
      // Get the alias for the new entry
      DGetAlias dGetAlias = new DGetAlias(getWindow(), RB.getString(dialogTitleKey), sAlias.toLowerCase());
      dGetAlias.setVisible(true);

      sAlias = dGetAlias.getAlias();
      if (sAlias == null) {
        return null;
      }

      // Check an entry with the selected does not already exist in the keystore
      if (!manager.containsAlias(sAlias)) {
        return sAlias;
      }

      String sMessage = MessageFormat.format(RB.getString("FPortecle.OverWriteEntry.message"), sAlias);

      int iSelected = JOptionPane.showConfirmDialog(getWindow(), sMessage, RB.getString(dialogTitleKey),
          JOptionPane.YES_NO_CANCEL_OPTION);
      switch (iSelected) {
      case JOptionPane.YES_OPTION:
        return sAlias;
      case JOptionPane.NO_OPTION:
        // keep looping
        break;
      default:
        return null;
      }
    }
  }

  /**
   * Display a report on the currently loaded keystore.
   * 
   * @return True if the keystore report was displayed successfully, false
   *         otherwise
   */
  @org.jdesktop.application.Action
  public boolean keyStoreReport() {
    try {
      DKeyStoreReport dKeyStoreReport = new DKeyStoreReport(getWindow(), manager.getKeyStore());
      dKeyStoreReport.setVisible(true);
      return true;
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }
  }

  @org.jdesktop.application.Action
  public void exportEntry() {
    MessageDialogs.error(getWindow(), "Not implemented", (String)null); // TODO TBD
  }

  /**
   * Let the user see the certificate details of the selected keystore entry.
   * 
   * @return True if the certificate details were viewed successfully, false
   *         otherwise
   */
  @org.jdesktop.application.Action
  public void certDetails() {
    showSelectedEntry();
  }

  private boolean showSelectedEntry() {
    String selectedType = getSelectedType();
    String sAlias = getSelectedAlias();
    
    if(selectedType == null || sAlias == null) {
      MessageDialogs.warning(getWindow(),TEXT_NO_SELECTED_ENTRIES);
      return false;
    }
    
    try {
      // Supply the certificates to the view certificate dialog
      DViewCertificate dViewCertificate = new DViewCertificate(getWindow(), sAlias, manager.getCertificate(sAlias));
      dViewCertificate.setVisible(true);
      return true;
    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }
  }

  /**
   * Let the user delete the selected keystore entry.
   * 
   * @return True if the deletion is successful, false otherwise
   */
  @org.jdesktop.application.Action
  public void deleteEntry() {
    deleteSelectedEntry();
  }

  private boolean deleteSelectedEntry() {

    String sAlias = getSelectedAlias();
    if (sAlias == null) {
      MessageDialogs.warning(getWindow(), TEXT_NO_SELECTED_ENTRIES);
      return false;
    }

    if (MessageDialogs.confirmRemove(getWindow(), sAlias) == false) {
      return false;
    }

    try {
      // Delete the entry
      manager.delete(sAlias);
    } catch (KeyStoreException ex) {
      MessageDialogs.error(getWindow(), "Failed to delete entry!", ex);
      return false;
    }

    // Update the frame's components and title
    updateControls();

    return true;
  }

  /**
   * Let the user rename the selected keystore entry.
   * 
   * @return True if the rename is successful, false otherwise
   */
  @org.jdesktop.application.Action
  public void renameCert() {
    renameEntry();
  }

  private boolean renameEntry() {
    String sAlias = getSelectedAlias();
    if (sAlias != null) {
      String newAlias = (String) JOptionPane.showInputDialog(getWindow(),
          "Rename Alias:", "Rename", JOptionPane.PLAIN_MESSAGE, null, null, sAlias);
      return renameEntry(sAlias, newAlias, false);
    } else {
      MessageDialogs.warning(getWindow(), TEXT_NO_SELECTED_ENTRIES);
      return false;
    }
  }

  private boolean renameEntry(String oldAlias, String newAlias, boolean silent) {
    if (newAlias == null) {
      return false;
    }

    try {

      // Check new alias differs from the present one
      if (newAlias.equalsIgnoreCase(oldAlias)) {
        return false;
      }

      // Check entry does not already exist in the keystore
      if (manager.containsAlias(newAlias)) {
        String sMessage =
            MessageFormat.format("The alias \"{0}\" already exists!\nDo you want to overwrite it?", newAlias);

        if (MessageDialogs.confirm(getWindow(), sMessage) == false) {
          return false;
        }
      }

      manager.renameEntry(oldAlias, newAlias);

    } catch (Exception ex) {
      MessageDialogs.error(getWindow(), null, ex);
      return false;
    }

    // Update the frame's components and title
    updateControls();

    return true;
  }


  public static interface ActionCallback {

    /**
     * Gets the parent window that invoke actions.
     * 
     * @return null if there is no component.
     */
    Window getParent();

    String getSelectedType();

    String getSelectedAlias();

  }

}
