/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import com.lucy.g3.cert.manager.KeyStoreManager;
import com.lucy.g3.cert.manager.gui.KeyStoreManagerFrame;

/**
 *
 */
public class KeyStoreMainFrameTest {
  public static void main(String[] args) throws Exception {
    KeyStoreManager manager = new KeyStoreManager();
    //manager.load("target/testKeyStore.jks", "Hello".toCharArray());
    
    KeyStoreManagerFrame frame = new KeyStoreManagerFrame(manager);
    frame.setVisible(true);
  }
  
}

