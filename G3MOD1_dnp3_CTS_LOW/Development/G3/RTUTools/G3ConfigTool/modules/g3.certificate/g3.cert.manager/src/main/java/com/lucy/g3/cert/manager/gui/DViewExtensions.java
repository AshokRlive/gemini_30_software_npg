/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;



import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Window;
import java.security.cert.X509Extension;
import java.util.ResourceBundle;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.common.utils.StringUtils;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;



/**
 * Displays the details of X.509 Extensions.
 */
class DViewExtensions
    extends AbstractDialog
{
  
  private static final ResourceBundle RB = Resources.RB;
  
	/** Extensions table */
	private JTable m_jtExtensions;

	/** Extension value text area */
	private JEditorPane m_jtaExtensionValue;

	/** Extensions to display */
	private final X509Extension m_extensions;
	
	private JPanel contentPane;

	/**
	 * Creates new DViewExtensions dialog.
	 * 
	 * @param parent Parent window
	 * @param sTitle The dialog title
	 * @param modal Is dialog modal?
	 * @param extensions Extensions to display
	 */
	public DViewExtensions(Window parent, String sTitle, boolean modal, X509Extension extensions)
	{
		super(parent);
		m_extensions = extensions;
		
		setTitle(sTitle);
		setModal(modal);
		initComponents();
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
	  contentPane = new JPanel(new BorderLayout());

		// Create the table using the appropriate table model
		ExtensionsTableModel extensionsTableModel = new ExtensionsTableModel();
		m_jtExtensions = new JTable(extensionsTableModel);

		m_jtExtensions.setShowGrid(false);
		m_jtExtensions.setRowMargin(0);
		m_jtExtensions.getColumnModel().setColumnMargin(0);
		m_jtExtensions.getTableHeader().setReorderingAllowed(false);
		m_jtExtensions.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		m_jtExtensions.setRowHeight(18);

		// Add custom renderers for the table cells and headers
		for (int iCnt = 0; iCnt < m_jtExtensions.getColumnCount(); iCnt++)
		{
			TableColumn column = m_jtExtensions.getColumnModel().getColumn(iCnt);
			column.setHeaderRenderer(new ExtensionsTableHeadRend());
			column.setCellRenderer(new ExtensionsTableCellRend());
		}

		// Make the first column small and not resizable (it holds an icon to represent the criticality of an
		// extension)
		TableColumn criticalCol = m_jtExtensions.getColumnModel().getColumn(0);
		criticalCol.setResizable(false);
		criticalCol.setMinWidth(20);
		criticalCol.setMaxWidth(20);
		criticalCol.setPreferredWidth(20);

		// If extension selected/deselected update extension value text area
		ListSelectionModel selectionModel = m_jtExtensions.getSelectionModel();
		selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		selectionModel.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent evt)
			{
				if (!evt.getValueIsAdjusting())
				{
					updateExtensionValue();
				}
			}
		});

		// Make the table sortable
//		TableRowSorter<ExtensionsTableModel> sorter = new TableRowSorter<>(extensionsTableModel);
//		sorter.setComparator(2, new OidComparator());
//		m_jtExtensions.setRowSorter(sorter);
//		// ...and sort it by extension name by default
//		sorter.toggleSortOrder(1);

		// Put the table into a scroll pane
		JScrollPane jspExtensionsTable = new JScrollPane(m_jtExtensions,
		    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jspExtensionsTable.getViewport().setBackground(m_jtExtensions.getBackground());

		// Put the scroll pane into a panel
		JPanel jpExtensionsTable = new JPanel(new BorderLayout(10, 10));
		jpExtensionsTable.setPreferredSize(new Dimension(520, 200));
		jpExtensionsTable.add(jspExtensionsTable, BorderLayout.CENTER);

		// Panel to hold Extension Value controls
		JPanel jpExtensionValue = new JPanel(new BorderLayout(10, 10));

		// Extension Value label
		JLabel jlExtensionValue = new JLabel("Extension Value:");

		// Put label into panel
		jpExtensionValue.add(jlExtensionValue, BorderLayout.NORTH);

		// Extension value area

		m_jtaExtensionValue = new JEditorPane("text/html", "");
		m_jtaExtensionValue.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
		m_jtaExtensionValue.setFont(m_jtExtensions.getFont());
		m_jtaExtensionValue.setEditable(false);
		m_jtaExtensionValue.setToolTipText("Displays selected X.509 extension's value");
		jlExtensionValue.setLabelFor(m_jtaExtensionValue);

		// Put the text area into a scroll pane
		JScrollPane jspExtensionValue = new JScrollPane(m_jtaExtensionValue,
		    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		// Put the scroll pane into a panel
		JPanel jpExtensionValueTextArea = new JPanel(new BorderLayout(10, 10));
		jpExtensionValueTextArea.setPreferredSize(new Dimension(520, 200));
		jpExtensionValueTextArea.add(jspExtensionValue, BorderLayout.CENTER);

		// Put text area panel into Extension Value controls panel
		jpExtensionValue.add(jpExtensionValueTextArea, BorderLayout.CENTER);

		// Put Extensions table and Extension Value text area together in extensions panel
		JPanel jpExtensions = new JPanel(new GridLayout(2, 1, 5, 5));

		jpExtensions.add(jpExtensionsTable);
		jpExtensions.add(jpExtensionValue);

		// Populate table with extensions
		extensionsTableModel.load(m_extensions);

		// Select first extension
		if (extensionsTableModel.getRowCount() > 0)
		{
			m_jtExtensions.changeSelection(0, 0, false, false);
		}

		// Put it all together
		contentPane.add(jpExtensions, BorderLayout.CENTER);
		
		pack();
	}

	/**
	 * Update the value of the Extension Value text area depending on whether or not an extension has been selected in
	 * the table.
	 */
	private void updateExtensionValue()
	{
		int iSelectedRow = m_jtExtensions.getSelectedRow();

		if (iSelectedRow == -1)
		{
			// No extension selected - clear text area
			m_jtaExtensionValue.setText("");
		}
		else
		{
			// Extension selected - get value for extension
			String sOid = m_jtExtensions.getValueAt(iSelectedRow, 2).toString();

			byte[] bValue = m_extensions.getExtensionValue(sOid);

			// Don't care about criticality
			final String HEADER = "<html><head><style type=\"text/css\">ul { list-style-type: none; margin: 0; }\n" +
			    "li ul { margin-left: 10px; }\n</style></head><body>";
			final String FOOTER = "</body></html>";

			try
			{
				m_jtaExtensionValue.setText(HEADER + convertToHexString(bValue)+ FOOTER);
			}
			// Don't like this but *anything* could go wrong in there
			catch (Exception ex)
			{
				m_jtaExtensionValue.setText("");
				MessageDialogs.error(this, null, ex);
			}
			m_jtaExtensionValue.setCaretPosition(0);
		}
	}
	
	private static String convertToHexString(Object obj)
  {
    StringBuilder strBuff = StringUtils.toHex(obj, 4, " ");
    strBuff.insert(0, "<tt>");
    strBuff.append("</tt>");
    return strBuff.toString();
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKButtonPanel();
  }
  
  /**
   * The table model used to display X.509 extensions.
   */
  private static class ExtensionsTableModel
      extends AbstractTableModel
  {
    /** Column names */
    private static final String[] COLUMN_NAMES = {"Critical","Name","Object Identifier"};

    /** Column classes */
    private static final Class<?>[] COLUMN_CLASSES = { Boolean.class, String.class, String.class };

    /** Holds the table data */
    private Object[][] m_data;

    /**
     * Construct a new ExtensionsTableModel.
     */
    public ExtensionsTableModel()
    {
      m_data = new Object[0][getColumnCount()];
    }

    /**
     * Load the ExtensionsTableModel with X.509 extensions.
     * 
     * @param extensions The X.509 extensions
     */
    public void load(X509Extension extensions)
    {
      // Get extension OIDs
      Set<String> critExts = extensions.getCriticalExtensionOIDs();
      Set<String> nonCritExts = extensions.getNonCriticalExtensionOIDs();

      // Create one table row for each extension
      m_data = new Object[critExts.size() + nonCritExts.size()][getColumnCount()];

      // Load rows
      int iCnt = 0;
      boolean critical = true;
      for (String sExtOid : critExts)
      {
        byte[] bValue = extensions.getExtensionValue(sExtOid);
        loadRow(critical, StringUtils.toHex(bValue, 4, " ").toString(),sExtOid, iCnt++);
      }
      
      critical = false;
      for (String sExtOid : nonCritExts)
      {
        byte[] bValue = extensions.getExtensionValue(sExtOid);
        loadRow(critical, StringUtils.toHex(bValue, 4, " ").toString(),sExtOid, iCnt++);
      }

      fireTableDataChanged();
    }

    /**
     * Load the ExtensionsTableModel with an X.509 extension.
     * 
     * @param extension The X.509 extension
     * @param iRow The row to add extension to
     */
    private void loadRow(boolean isCritial, String name, String oid, int iRow)
    {
      int col = 0;

      // Populate the Critical columnsExtname
      m_data[iRow][col++] = isCritial;

      // Populate the Name column
      m_data[iRow][col++] = name;

      // Populate the OID column
      m_data[iRow][col++] = oid;
    }

    /**
     * Get the number of columns in the table.
     * 
     * @return The number of columns
     */
    @Override
    public int getColumnCount()
    {
      return COLUMN_CLASSES.length;
    }

    /**
     * Get the number of rows in the table.
     * 
     * @return The number of rows
     */
    @Override
    public int getRowCount()
    {
      return m_data.length;
    }

    /**
     * Get the name of the column at the given position.
     * 
     * @param iCol The column position
     * @return The column name
     */
    @Override
    public String getColumnName(int iCol)
    {
      return COLUMN_NAMES[iCol];
    }

    /**
     * Get the cell value at the given row and column position.
     * 
     * @param iRow The row position
     * @param iCol The column position
     * @return The cell value
     */
    @Override
    public Object getValueAt(int iRow, int iCol)
    {
      return m_data[iRow][iCol];
    }

    /**
     * Get the class at of the cells at the given column position.
     * 
     * @param iCol The column position
     * @return The column cells' class
     */
    @Override
    public Class<?> getColumnClass(int iCol)
    {
      return COLUMN_CLASSES[iCol];
    }
  }
  
  /**
   * Custom cell renderer for the headers of the Extensions table of DViewExtensions.
   */
 private static class ExtensionsTableHeadRend
      extends DefaultTableCellRenderer
  {
    /**
     * Returns the rendered header cell for the supplied value and column.
     * 
     * @param jtExtensions The JTable
     * @param value The value to assign to the cell
     * @param bIsSelected True if cell is selected
     * @param iRow The row of the cell to render
     * @param iCol The column of the cell to render
     * @param bHasFocus If true, render cell appropriately
     * @return The rendered cell
     */
    @Override
    public Component getTableCellRendererComponent(JTable jtExtensions, Object value, boolean bIsSelected,
        boolean bHasFocus, int iRow, int iCol)
    {
      // Get header renderer
      JLabel header = (JLabel) jtExtensions.getColumnModel().getColumn(iCol).getHeaderRenderer();

      // The Critical header contains an icon
      if (iCol == 0)
      {
        header.setText("");
        ImageIcon icon =
            new ImageIcon(getClass().getResource(RB.getString("ExtensionsTableHeadRend.CriticalColumn.image")));
        header.setIcon(icon);
        header.setHorizontalAlignment(CENTER);
        header.setVerticalAlignment(CENTER);

        header.setToolTipText(RB.getString("ExtensionsTableHeadRend.CriticalColumn.tooltip"));
      }
      // The other headers contain text
      else
      {
        header.setText(value.toString());
        header.setHorizontalAlignment(LEFT);

        // Set tool tips
        if (iCol == 1)
        {
          header.setToolTipText(RB.getString("ExtensionsTableHeadRend.NameColumn.tooltip"));
        }
        else
        {
          header.setToolTipText(RB.getString("ExtensionsTableHeadRend.OidColumn.tooltip"));
        }
      }

      header.setBorder(new CompoundBorder(new BevelBorder(BevelBorder.RAISED), new EmptyBorder(0, 5, 0, 5)));

      return header;
    }
  }
 
 /**
  * Custom cell renderer for the cells of the Extensions table of DViewExtensions.
  */
 private static class ExtensionsTableCellRend
     extends DefaultTableCellRenderer
 {
   /**
    * Returns the rendered cell for the supplied entry type and column.
    * 
    * @param jtExtensions The JTable
    * @param value The value to assign to the cell
    * @param bIsSelected True if cell is selected
    * @param iRow The row of the cell to render
    * @param iCol The column of the cell to render
    * @param bHasFocus If true, render cell appropriately
    * @return The rendered cell
    */
   @Override
   public Component getTableCellRendererComponent(JTable jtExtensions, Object value, boolean bIsSelected,
       boolean bHasFocus, int iRow, int iCol)
   {
     JLabel cell =
         (JLabel) super.getTableCellRendererComponent(jtExtensions, value, bIsSelected, bHasFocus, iRow, iCol);

     // Critical column - display an icon representing criticality and tool tip text
     if (iCol == 0)
     {
       ImageIcon icon = null;

       if (((Boolean) value))
       {
         icon = new ImageIcon(
             getClass().getResource(RB.getString("ExtensionsTableCellRend.CriticalExtension.image")));
         cell.setToolTipText(RB.getString("ExtensionsTableCellRend.CriticalExtension.tooltip"));
       }
       else
       {
         icon = new ImageIcon(
             getClass().getResource(RB.getString("ExtensionsTableCellRend." + "NonCriticalExtension.image")));
         cell.setToolTipText(RB.getString("ExtensionsTableCellRend." + "NonCriticalExtension.tooltip"));
       }

       cell.setIcon(icon);
       cell.setText("");
       cell.setVerticalAlignment(CENTER);
       cell.setHorizontalAlignment(CENTER);
     }
     else
     {
       // Just use toString of object as text
       cell.setText(value.toString());
     }

     cell.setBorder(new EmptyBorder(0, 5, 0, 5));

     return cell;
   }
 }

}
