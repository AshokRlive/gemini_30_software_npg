/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStoreException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.cert.manager.KeyStoreManager;


/**
 *
 */
public class KeyStoreManagerTest {
  private final String KEY_STORE = new File("target/KeyStoreManagerTest.testStore").getAbsolutePath();
  private final String PASS  = "This is PASSWORD";
  private KeyStoreManager fixture;
  
  @Before
  public  void init() throws Exception {
    fixture = new KeyStoreManager();
  }

  @Test
  public void testStore() throws Exception {
    File file = new File(KEY_STORE);
    file.delete();
    fixture.store(file, PASS.toCharArray());
    Assert.assertTrue(file.exists());
  }
  
  @Test
  public void testLoad() throws Exception {
    testStore();
    
    fixture.load(KEY_STORE, PASS.toCharArray());
    
    printKeyStore(KEY_STORE, PASS);
  }
  
  @Test
  public void testAddTrustedCert() throws FileNotFoundException, Exception{
    InputStream ca = KeyStoreManagerTest.class.getResourceAsStream("/rootCA.pem");
    fixture.importTrustedCert("testAddTrustedCert", ca);
    Assert.assertEquals(1, fixture.getEntriesSize());
    
    File out = new File("target/testAddTrustedCert");
    fixture.store(out, "pass".toCharArray());
    printKeyStore(out.getAbsolutePath(), "pass");
    ca.close();
  }

  @Test 
  public void testDeleteNonExist() throws KeyStoreException {
    fixture.delete("NonexistEntry");
  }
  
  @Test 
  public void testDeleteExist() throws FileNotFoundException, Exception {
    InputStream ca = KeyStoreManagerTest.class.getResourceAsStream("/rootCA.pem");
    fixture.importTrustedCert("todelete", ca);
    Assert.assertEquals(1, fixture.getEntriesSize());
    fixture.delete("todelete");
    Assert.assertEquals(0, fixture.getEntriesSize());
    ca.close();
  }
  
  private static void printKeyStore(String file, String pass) throws Exception {
   // KeyToolAgent agent = new KeyToolAgent(file, pass);
   // agent.listEntries(System.out);
  }
}

