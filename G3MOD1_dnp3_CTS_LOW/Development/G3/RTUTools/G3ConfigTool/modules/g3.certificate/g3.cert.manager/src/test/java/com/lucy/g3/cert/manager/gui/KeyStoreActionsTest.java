/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 *
 */
public class KeyStoreActionsTest {

  @Test
  public void testGetActions() {
    KeyStoreActions actions = new KeyStoreActions( null, null);
    assertNotNull(actions.getAction(KeyStoreActions.ACTION_CERTDETAILS));
  }

}

