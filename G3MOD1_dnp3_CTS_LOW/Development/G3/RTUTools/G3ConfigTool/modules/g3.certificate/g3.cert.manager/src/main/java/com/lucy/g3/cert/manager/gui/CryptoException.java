/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

/**
 * Represents a cryptographic exception.
 */
public class CryptoException
    extends Exception {

  /**
   * Creates a new CryptoException with the specified message.
   * 
   * @param sMessage
   *          Exception message
   */
  /* default */ CryptoException(String sMessage) {
    super(sMessage);
  }

  /**
   * Creates a new CryptoException with the specified message and cause
   * throwable.
   * 
   * @param causeThrowable
   *          The throwable that caused this exception to be thrown
   * @param sMessage
   *          Exception message
   */
  public CryptoException(String sMessage, Throwable causeThrowable) {
    super(sMessage, causeThrowable);
  }

  /**
   * Creates a new CryptoException with the specified cause throwable.
   * 
   * @param causeThrowable
   *          The throwable that caused this exception to be thrown
   */
  /* default */ CryptoException(Throwable causeThrowable) {
    super(causeThrowable);
  }
}
