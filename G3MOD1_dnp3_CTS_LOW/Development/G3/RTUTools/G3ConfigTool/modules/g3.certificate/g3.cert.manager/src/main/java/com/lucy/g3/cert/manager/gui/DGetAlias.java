/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;


/**
 * Modal dialog used for entering a keystore alias.
 */
public class DGetAlias extends AbstractDialog
{
	/** Alias text field */
	private JTextField m_jtfAlias;

	/** Stores the alias entered by the user */
	private String m_sAlias;
	
	private JPanel contentPanel = new JPanel(new BorderLayout());

	/**
	 * Creates new DGetAlias dialog.
	 * 
	 * @param parent The parent window
	 * @param sTitle The dialog's title
	 * @param sOldAlias The alias to display initially
	 * @param select Whether to pre-select the initially displayed alias
	 */
	public DGetAlias(Window parent, String sTitle, String sOldAlias)
	{
		super(parent);
		setTitle(sTitle);
		setModal(true);
		initComponents(sOldAlias);
	}

	/**
	 * Get the alias entered by the user.
	 * 
	 * @return The alias, or null if none was entered
	 */
	public String getAlias()
	{
		return m_sAlias;
	}

	/**
	 * Initialize the dialog's GUI components.
	 * 
	 * @param sOldAlias The alias to display initially
	 * @param select Whether to pre-select the initially displayed alias
	 */
	private void initComponents(String sOldAlias)
	{

		JLabel jlAlias = new JLabel("Enter Alias:");
		m_jtfAlias = new JTextField(sOldAlias, 15);
		m_jtfAlias.setCaretPosition(sOldAlias.length());
		jlAlias.setLabelFor(m_jtfAlias);


		JPanel jpAlias = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jpAlias.add(jlAlias);
		jpAlias.add(m_jtfAlias);
		jpAlias.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPanel.add(jpAlias, BorderLayout.CENTER);
		
		pack();
	}

	/**
	 * Check that the alias is valid, i.e. that it is not blank.
	 * 
	 * @return True if the alias is valid, false otherwise
	 */
	private boolean checkAlias()
	{
		String sAlias = m_jtfAlias.getText().trim();

		if (!sAlias.isEmpty())
		{
			m_sAlias = sAlias;
			return true;
		}

		JOptionPane.showMessageDialog(this, "Alias must be supplied.", getTitle(),
		    JOptionPane.WARNING_MESSAGE);

		return false;
	}
	
	@Override
	public void cancel(){
	  m_sAlias = null;
	  super.cancel();
	}

	@Override
	public void ok()
	{
		if (checkAlias())
		{
			super.ok();
		}
	}

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
}
