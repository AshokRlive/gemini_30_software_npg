/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.manager.gui;

import static com.lucy.g3.cert.manager.gui.Resources.RB;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Window;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;

/**
 * Modal dialog used for entering and confirming a password and checking it against an old password which may or may not
 * have been supplied to the dialog.
 */
public class DChangePassword extends AbstractDialog
{
	/** Old password entry password field */
	private JPasswordField m_jpfOld;

	/** First password entry password field */
	private JPasswordField m_jpfFirst;

	/** Password confirmation entry password field */
	private JPasswordField m_jpfConfirm;

	/** Stores new password entered */
	private char[] m_cNewPassword;

	/** Stores old password entered/supplied */
	private char[] m_cOldPassword;
	
	private JPanel contentPanel = new JPanel(new BorderLayout());

	/**
	 * Creates new DChangePassword dialog.
	 * 
	 * @param parent Parent window
	 * @param sTitle Is dialog modal?
	 * @param cOldPassword The password to be changed
	 */
	public DChangePassword(Window parent, String sTitle, char[] cOldPassword)
	{
		super(parent);
		setTitle((sTitle == null) ? RB.getString("DChangePassword.Title") : sTitle);
		setModal(true);
		m_cOldPassword = arrayCopy(cOldPassword);
		initComponents();
		pack();
	}

	/**
	 * Get the new password set in the dialog.
	 * 
	 * @return The new password or null if none was set
	 */
	public char[] getNewPassword()
	{
		return arrayCopy(m_cNewPassword);
	}

	/**
	 * Get the old password set in the dialog.
	 * 
	 * @return The old password or null if none was set/supplied
	 */
	public char[] getOldPassword()
	{
		return arrayCopy(m_cOldPassword);
	}

	/**
	 * Copies a char array.
	 * 
	 * @param original
	 * @return a copy of the given char array
	 */
	private static final char[] arrayCopy(char[] original)
	{
		char[] copy = null;
		if (original != null)
		{
			copy = new char[original.length];
			System.arraycopy(original, 0, copy, 0, copy.length);
		}
		return copy;
	}

	/**
	 * Initialize the dialog's GUI components.
	 */
	private void initComponents()
	{
		JLabel jlFirst = new JLabel(RB.getString("DChangePassword.jlFirst.text"));
		m_jpfFirst = new JPasswordField(15);
		jlFirst.setLabelFor(m_jpfFirst);

		JLabel jlConfirm = new JLabel(RB.getString("DChangePassword.jlConfirm.text"));
		m_jpfConfirm = new JPasswordField(15);
		jlConfirm.setLabelFor(m_jpfConfirm);

		JLabel jlOld = new JLabel(RB.getString("DChangePassword.jlOld.text"));

		// Old password was supplied - just disable the old password field after filling it with junk
		if (m_cOldPassword != null)
		{
			m_jpfOld = new JPasswordField("1234567890", 15);
			m_jpfOld.setEnabled(false);
		}
		else
		{
			m_jpfOld = new JPasswordField(10);
		}
		jlOld.setLabelFor(m_jpfOld);

		JPanel jpPassword = new JPanel(new GridLayout(3, 2, 5, 5));
		jpPassword.add(jlOld);
		jpPassword.add(m_jpfOld);
		jpPassword.add(jlFirst);
		jpPassword.add(m_jpfFirst);
		jpPassword.add(jlConfirm);
		jpPassword.add(m_jpfConfirm);
		jpPassword.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPanel.add(jpPassword, BorderLayout.CENTER);
	}

	/**
	 * Check for the following:
	 * <ul>
	 * <li>That the user has supplied and confirmed a password.
	 * <li>That the passwords match.
	 * <li>That the old password was supplied or set by the user.
	 * </ul>
	 * Store the old and changed password in this object.
	 * 
	 * @return True, if the user's dialog entry matches the above criteria, false otherwise
	 */
	private boolean checkPassword()
	{
		String sOldPassword = new String(m_jpfOld.getPassword());
		String sFirstPassword = new String(m_jpfFirst.getPassword());
		String sConfirmPassword = new String(m_jpfConfirm.getPassword());

		if (sFirstPassword.equals(sConfirmPassword))
		{
			if (m_cOldPassword == null)
			{
				m_cOldPassword = sOldPassword.toCharArray();
			}
			m_cNewPassword = sFirstPassword.toCharArray();
			return true;
		}

		JOptionPane.showMessageDialog(this, RB.getString("PasswordsNoMatch.message"), getTitle(),
		    JOptionPane.WARNING_MESSAGE);

		return false;
	}

	@Override
	public void ok()
	{
		if (checkPassword())
		{
			super.ok();
		}
	}

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }
}
