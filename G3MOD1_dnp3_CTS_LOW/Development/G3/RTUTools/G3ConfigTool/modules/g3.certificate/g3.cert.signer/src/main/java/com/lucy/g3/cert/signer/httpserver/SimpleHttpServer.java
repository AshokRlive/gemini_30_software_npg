package com.lucy.g3.cert.signer.httpserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings("restriction")
public class SimpleHttpServer {
	public static final String CONFIG_HTMLROOT = "htmlroot";
	public static final String CONFIG_UPLOAD   = "upload";
	public static final String CONFIG_PORT     = "port";
	public static final String CONFIG_OPENSSL  = "openssl";
	public static final String CONFIG_ROOTCERT = "rootcert";
	public static final String CONFIG_ROOTKEY  = "rootkey";
	private static final String[] CONF_SYS_PROPERTIES = {
	    CONFIG_HTMLROOT ,
	    CONFIG_UPLOAD   ,
	    CONFIG_PORT     ,
	    CONFIG_OPENSSL  ,
	    CONFIG_ROOTCERT ,
	    CONFIG_ROOTKEY  ,
	};
	
	private static Logger log = Logger.getLogger(SimpleHttpServer.class);
  private final Properties config = new Properties();
  private boolean configLoaded;
  
  private HttpServer server;

  public SimpleHttpServer(InputStream conf) throws IOException {
      loadConfigFromStream(conf);
      loadConfigFromSystemProperties();
      checkConfig();
  }
  
	private void loadConfigFromSystemProperties() {
	  for (int i = 0; i < CONF_SYS_PROPERTIES.length; i++) {
      String prop = System.getProperty(CONF_SYS_PROPERTIES[i]);
      if(prop != null && !prop.isEmpty()) {
        config.put(CONF_SYS_PROPERTIES[i], prop);
      }
    }
  }

  private void checkConfig() throws IOException {
	    checkDirExists(config.getProperty(CONFIG_HTMLROOT));
	    checkFileExists(config.getProperty(CONFIG_ROOTCERT));
	    checkFileExists(config.getProperty(CONFIG_ROOTKEY));
  }

  public void loadConfigFromStream(InputStream conf) {
    try {
      config.load(conf);
      configLoaded = true;
      conf.close();
    } catch ( IOException e) {
      log.error("Failed to load configuration",e);
      configLoaded = false;
    }
	}
	
	public void start(){
	  if(configLoaded == false) {
	    log.error("No configuration found");
	    return;
	  }
	  
	  if(server != null) {
	    return;
	  }
	  
	  try {
	    int port = Integer.parseInt(config.getProperty(CONFIG_PORT));
	    String root = config.getProperty(CONFIG_HTMLROOT);
      
	    BasicAuthenticator auth = new BasicAuthenticatorImpl(config);
      server = HttpServer.create(new InetSocketAddress(port), 0);
      server.createContext("/", new FileStreamHandler(root)).setAuthenticator(auth);
      //server.createContext("/upload", new FileUploadHandler(upload)).setAuthenticator(auth);
      server.createContext("/result", new CSRHandler(config)).setAuthenticator(auth);
      server.setExecutor(null); // creates a default executor
      server.start();
      log.info(String.format("HTTP server started [:%s]", 
          server.getAddress().getPort()));
	  } catch (NumberFormatException | IOException e) {
	    log.error("HTTP server NOT started",e);
    }
	}
	
	public void stop(){
	  server.stop(3);
	}
	
  private static void checkDirExists(String dirPath) throws IOException {
    File dir = new File(dirPath);
    if(!dir.exists() || dir.isDirectory() == false) {
      throw new IOException("Directory not exists: " + dir.getAbsolutePath());
    }
  }

  private static void checkFileExists(String filePath) throws IOException {
    File file = new File(filePath);
    if(!file.exists() || file.isFile() == false) {
      throw new IOException("File not exists: " + file.getAbsolutePath());
    }    
  }
}
