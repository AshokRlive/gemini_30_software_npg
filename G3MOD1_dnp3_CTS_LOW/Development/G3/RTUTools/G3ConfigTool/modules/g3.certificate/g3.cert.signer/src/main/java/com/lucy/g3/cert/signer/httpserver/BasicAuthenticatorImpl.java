
package com.lucy.g3.cert.signer.httpserver;

import java.util.Properties;

import com.sun.net.httpserver.BasicAuthenticator;

@SuppressWarnings("restriction")

class BasicAuthenticatorImpl extends BasicAuthenticator {
  private final Properties config;
  public BasicAuthenticatorImpl(Properties config) {
    super("get");
    this.config = config;
  }

  @Override
  public boolean checkCredentials(String user, String pwd) {
    return user.equals(config.get("user")) && pwd.equals(config.get("pass"));
  }
}

