
package com.lucy.g3.cert.signer.httpserver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpExchange;

@SuppressWarnings("restriction")
public class FileUploadHandler extends AbstractFileUploadHandler{
  private static Logger log = Logger.getLogger(FileUploadHandler.class);
  private final String uploadDir;
  
  public FileUploadHandler (String uploadDirPath) {
    this.uploadDir = uploadDirPath;
  }
  
  protected void processFileItems(final HttpExchange t, List<FileItem> items) throws IOException {
    for (FileItem fi : items) {
      if (!fi.isFormField()) {
        processUploadedFile(fi);
      } else {
        processFormField(fi);
      }
    }
  }
 
  private void processFormField(FileItem fi) {
    log.debug(String.format("%s:%s", fi.getFieldName(),fi.getName()));
  }

  private void processUploadedFile(FileItem fi) {
    copyFileToDir(fi, uploadDir);
  }

  /**
   * Copies uploaded files to upload directory.
   * @param fileItems
   * @return
   */
  private static void copyFileToDir(FileItem fileItem, String dir) {
    String fileName;
      fileName = fileItem.getName();
      fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);

      try {
        File file = new File(dir, fileName);
        log.debug("File-Item: " + fileItem.getFieldName() + " = " + fileItem.getName());
        fileItem.write(file);
      } catch (Exception e) {
        log.error("Failed to copy file", e);
      }
  }
  
}

