package com.lucy.g3.cert.signer.httpserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

@SuppressWarnings("restriction")
abstract class AbstractFileUploadHandler implements HttpHandler {
  private Logger log = Logger.getLogger(AbstractFileUploadHandler.class);
  
	@Override
	public void handle(final HttpExchange t) throws IOException {
		for (Entry<String, List<String>> header : t.getRequestHeaders().entrySet()) {
			log.debug(header.getKey() + ": " + header.getValue().get(0));
		}
		DiskFileItemFactory disk = new DiskFileItemFactory();

		try {
			ServletFileUpload up = new ServletFileUpload(disk);

			RequestContext rc = new RequestContext() {
				@Override
				public String getCharacterEncoding() {
					return "UTF-8";
				}

				@Override
				public int getContentLength() {
					return 0; // tested to work with 0 as return
				}

				@Override
				public String getContentType() {
					return t.getRequestHeaders().getFirst("Content-type");
				}

				@Override
				public InputStream getInputStream() throws IOException {
					return t.getRequestBody();
				}

			};
			List<FileItem> items = up.parseRequest(rc);
			processFileItems(t, items);
			composeSuccessResp(t,items);

		} catch (Exception e) {
		  log.error("Failed to handle file upload request",e);
			composeFailureResp(t,e.getMessage());
		}
	}


  protected void composeSuccessResp(HttpExchange t, List<FileItem> items) throws IOException {
    t.getResponseHeaders().add("Content-type", "text/plain");
    t.sendResponseHeaders(200, 0);
    OutputStream os = t.getResponseBody();
    os.write("Successfull!".getBytes());
    os.close();
  }


  protected abstract void processFileItems(final HttpExchange t, List<FileItem> items) throws IOException;

  protected void composeFailureResp(final HttpExchange t, String message) throws IOException {
    t.getResponseHeaders().add("Content-type", "text/plain");
    t.sendResponseHeaders(200, 0);
    OutputStream os = t.getResponseBody();
    os.write(message.getBytes());
    os.write("\r\n".getBytes());
    os.close();
  }
  
  public static String getUploadDir(Properties config) {
    String upload = config.getProperty(SimpleHttpServer.CONFIG_UPLOAD);

    upload = new File(upload + "/").getAbsolutePath();
    new File(upload).mkdirs();
    return upload;
  }

}