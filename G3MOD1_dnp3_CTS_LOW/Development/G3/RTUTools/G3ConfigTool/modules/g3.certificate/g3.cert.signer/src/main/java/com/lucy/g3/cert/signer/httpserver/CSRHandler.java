package com.lucy.g3.cert.signer.httpserver;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

@SuppressWarnings("restriction")
class CSRHandler extends AbstractFileUploadHandler implements HttpHandler {

  private static Logger log = Logger.getLogger(CSRHandler.class);

  private byte[] cert = null;
  private final Properties config;


  public CSRHandler(Properties config) {
    super();
    this.config = config;
  }

  @Override
  protected void processFileItems(HttpExchange t, List<FileItem> items) throws IOException {
    final File uploaddir = new File(getUploadDir(config));
    
    for (FileItem item : items) {
        log.debug(String.format("%s: %s",item.getFieldName(),item.getString()));
        
        File csrFile = null;
        File crtFile = File.createTempFile("crt", ".txt", uploaddir);

        if("csr".equals(item.getFieldName())) {
          csrFile = File.createTempFile("csr", ".txt", uploaddir);
          try {
            item.write(csrFile);
          } catch (Exception e) {
            throw new IOException(e);
          }
          
          String certStr = generateCert(csrFile,crtFile,config);
          log.debug("Generated certificate:\""+crtFile+"\"\n"+certStr);
          cert = certStr.getBytes();
          csrFile.delete();
          crtFile.delete();
          break; 
        }
    }
  }

  @Override
  protected void composeSuccessResp(HttpExchange t, List<FileItem> items) throws IOException {
    t.getResponseHeaders().add("Content-type", "text/html");
    t.sendResponseHeaders(200, 0);
    OutputStream os = t.getResponseBody();

    os.write(
        ("<!DOCTYPE HTML><html><head>"
            + "<title>Lucy CA</title>"
            + "<script>function copy_to_clipboard(id)"
            + "{document.getElementById(id).select();"
            + "document.execCommand('copy');"
            + "}</script>"
            + "</head><body><div align='center'>"
            + "<h1 align='center'>Gemini 3 RTU Certificate Authority</h1> <p>Your signed certificate(CERT):</p>"
            + "<textarea id='csrtextarea' readonly='on' rows='25' cols='70' >").getBytes());
    os.write(cert == null ? "Failed to sign csr! \r\nCheck the validility of the CSR.".getBytes() : cert);
    os.write(("</textarea><p>"
        + "<input type='button' value='Copy Content' onclick=\"copy_to_clipboard('csrtextarea')\"></p></div></body></html>").getBytes());
    os.close();
  }

  protected static String  generateCert(File inputCsrfile, File outputCertFile, Properties config)  {
    outputCertFile.delete();
    
    String opensslCmd = config.getProperty(SimpleHttpServer.CONFIG_OPENSSL, "openssl");
    String rootcert = new File(config.getProperty(SimpleHttpServer.CONFIG_ROOTCERT, "lucyroot.crt")).getAbsolutePath();
    String rootkey = new File(config.getProperty(SimpleHttpServer.CONFIG_ROOTKEY,"lucyroot.key")).getAbsolutePath();
    String days = config.getProperty("days", "365");
    String serial = config.getProperty("serial", "1234");
    
    ProcessBuilder pb = new ProcessBuilder(opensslCmd,
        "x509",
        "-req", 
        "-CA",
        String.format("\"%s\"", rootcert),
        "-CAkey",
        String.format("\"%s\"", rootkey),
        "-in", 
        String.format("\"%s\"", inputCsrfile.getAbsolutePath()),
        "-out",
        String.format("\"%s\"", outputCertFile.getAbsolutePath()),
        "-days",
        days,
        "-set_serial",
        serial
        );
    log.info("System call commands:"+Arrays.toString(pb.command().toArray()));
    pb.redirectOutput(new File("log/sys.log"));
    pb.redirectError(new File("log/syserr.log"));
    try {
      Process p = pb.start();
      p.waitFor();
      if(p.exitValue() != 0) {
        throw new RuntimeException("System error, check details in log:syserr.log");
      }
      
      return getStringFromInputStream(new FileInputStream(outputCertFile));
      
    } catch (Exception e) {
      log.error("Failed to run openssl command", e);
      return "Failed to sign csr!";
    }
    
  }
  

  // private void handCSRForm(final HttpExchange t) throws IOException {
  // for (Entry<String, List<String>> header : t.getRequestHeaders().entrySet())
  // {
  // log.info(header.getKey() + ": " + header.getValue().get(0));
  // }
  //
  // log.info("Method:" + t.getRequestMethod());
  // final String body = Utils.getStringFromInputStream(t.getRequestBody());
  // log.info(body);
  //
  // Map<String, String> queries = queryToMap(body);
  // t.getResponseHeaders().add("Content-type", "text/plain");
  // t.sendResponseHeaders(200, 0);
  // OutputStream os = t.getResponseBody();
  // String csr = queries.get("csrcontent");
  // os.write((csr == null ? "No CSR" : csr).getBytes());
  // os.close();
  // }

  /**
   * returns the url parameters in a map
   * 
   * @param query
   * @return map
   */
  // private static Map<String, String> queryToMap(String query) {
  // Map<String, String> result = new HashMap<String, String>();
  // for (String param : query.split("&")) {
  // String pair[] = param.split("=");
  // if (pair.length > 1) {
  // result.put(pair[0], pair[1]);
  // } else {
  // result.put(pair[0], "");
  // }
  // }
  // return result;
  // }

  public static String getStringFromInputStream(InputStream is) throws IOException {

    ByteArrayOutputStream out = new ByteArrayOutputStream();
    byte[] buf = new byte[1024];
    int n;
    try {
      while ( (n = is.read(buf)) > 0) {
        out.write(buf, 0, n);
      }

    }  finally {
        is.close();
        out.close();
    }

    return out.toString();
  }

}