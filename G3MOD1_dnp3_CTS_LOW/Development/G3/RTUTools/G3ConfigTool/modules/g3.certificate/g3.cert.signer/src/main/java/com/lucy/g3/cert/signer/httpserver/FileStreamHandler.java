
package com.lucy.g3.cert.signer.httpserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

@SuppressWarnings("restriction")
class FileStreamHandler implements HttpHandler {

  private Logger log = Logger.getLogger(FileStreamHandler.class);

  private final String rootDir;


  public FileStreamHandler(String rootDir) throws IOException{
    if(rootDir == null || rootDir.trim().isEmpty())
      this.rootDir = null;
    else
      this.rootDir = new File(rootDir).getCanonicalPath();
  }

  @Override
  public void handle(HttpExchange t) throws IOException {
    String pagePath = t.getRequestURI().getPath();
    if (pagePath != null && pagePath.substring(pagePath.lastIndexOf("/") + 1).isEmpty()) {
      pagePath = "/index.html";
    }

    File file = new File(rootDir + pagePath).getCanonicalFile();
    if (!file.getPath().startsWith(rootDir)) {
      // Suspected path traversal attack: reject with 403 error.
      String response = "403 (Forbidden)\n";
      t.sendResponseHeaders(403, response.length());
      OutputStream os = t.getResponseBody();
      os.write(response.getBytes());
      os.close();
    } else if (!file.isFile()) {
      // Object does not exist or is not a file: reject with 404
      // error.
      log.error("File not found:" + file);
      String response = "404 (Not Found)\n";
      t.sendResponseHeaders(404, response.length());
      OutputStream os = t.getResponseBody();
      os.write(response.getBytes());
      os.close();
    } else {
      // Object exists and is a file: accept with response code 200.
      t.sendResponseHeaders(200, 0);
      OutputStream os = t.getResponseBody();
      FileInputStream fs = new FileInputStream(file);
      final byte[] buffer = new byte[0x10000];
      int count = 0;
      while ((count = fs.read(buffer)) >= 0) {
        os.write(buffer, 0, count);
      }
      fs.close();
      os.close();
    }
  }
}