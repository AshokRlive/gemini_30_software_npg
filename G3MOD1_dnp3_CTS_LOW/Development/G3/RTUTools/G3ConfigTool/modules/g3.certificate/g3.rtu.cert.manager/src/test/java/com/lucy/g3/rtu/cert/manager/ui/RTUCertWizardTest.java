/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.lucy.g3.rtu.cert.manager.RTUCertManager;
import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.datalink.http.HttpDataLink;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.rtu.control.IRTUCertCommands;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUCertCommands;


public class RTUCertWizardTest implements IRTUCertCommands{

  public static void main(String[] args) throws IOException {
    //testWithDummy();
    testWithRTU();
  }
  
  private static void testWithRTU() throws IOException{
    File dir = new File("target/wizard");
    dir.mkdirs();
    
    HttpDataLink https = new HttpDataLink("10.11.3.162");
    https.getSecurityManager().setIgnoreCertificateError(true);
    G3RTUClient client = new G3RTUClient(https);
    
    
    RTUCertCommands commands = new RTUCertCommands(client, new FileTransfer(client, dir));
    RTUCertManager manager = new RTUCertManager(commands, RTUCertManager.OWNER_HTTPS);
    RTUCertWizard.showWizard(manager);
    
    FileUtils.deleteDirectory(dir);
  }


  /**
   * 
   */
  private static void testWithDummy() {
    RTUCertWizardTest dummy = new RTUCertWizardTest();
        
    RTUCertManager manager = new RTUCertManager(dummy,RTUCertManager.OWNER_HTTPS);
    RTUCertWizard.showWizard(manager);
  }

  @Override
  public void cmdInstallCert(String owner) throws IOException, SerializationException {
    
    
  }

  @Override
  public void cmdGenCSR(String owner) throws IOException, SerializationException {
    
    
  }

  @Override
  public void cmdGenPrivateKey(String owner) throws IOException, SerializationException {
    
    
  }

  @Override
  public void cmdGenSelfSignedCert(String owner) throws IOException, SerializationException {
    
    
  }

  @Override
  public void readCSRFile(String owner, String destPath) throws Exception {
    
    
  }

  @Override
  public void writeCertFile(String owner, String sourcePath) throws Exception {
    
    
  }

  @Override
  public void getLog(String owner, File dest) throws IOException {
    
  }

  @Override
  public int getStatus(String owner, int maxRetries) throws IOException, InterruptedException {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void cmdCleanLog(String owner) throws IOException, SerializationException {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void readPendingFile(String owner, String fileName, String destPath) throws Exception {
    // TODO Auto-generated method stub
    
  }

}
