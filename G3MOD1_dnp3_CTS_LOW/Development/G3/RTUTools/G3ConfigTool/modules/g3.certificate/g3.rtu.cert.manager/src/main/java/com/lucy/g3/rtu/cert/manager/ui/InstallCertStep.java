/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXLabel;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.cert.manager.gui.FileChooserFactory;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
public class InstallCertStep extends WizardPage {
  public final static String OPTION_CERT_FILE_PATH = "CertFilePath";

  public InstallCertStep() {
    super("Install Certificate");
    initComponents();
    initComponentNames();
  }

  private void initComponentNames() {
    tfCertFilePath.setName(OPTION_CERT_FILE_PATH);
  }
  
  @Override
  public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
    UIUtils.enableComponents(this, false);
   
    return super.allowFinish(stepName, settings, wizard);
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    labelTips.setVisible(false);
    ValidationComponentUtils.setErrorBackground(tfCertFilePath);
    
    if(Strings.isBlank(tfCertFilePath.getText())) {
      return "The certificate file path must not be blank!";
    }
    
    File certFile = new File(tfCertFilePath.getText());
    if(!certFile.exists() || !certFile.isFile()) {
      return "The certificate file does not exist!";
    }

    
    tfCertFilePath.setBackground(Color.white);
    labelTips.setVisible(true);
    return null;
  }

  
  private void btnBrowseActionPerformed() {
    JFileChooser fc = FileChooserFactory.getCertFileChooser();
    File selectedFile = DialogUtils.showOpenDialog(this, "Select a certificate to install", fc);
    
    
    if(selectedFile != null) {
      // Update path
      tfCertFilePath.setText(selectedFile.getAbsolutePath());
    }
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    label1 = new JLabel();
    tfCertFilePath = new JTextField();
    btnBrowse = new JButton();
    labelTips = new JXLabel();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "default, $lgap, fill:default:grow, $lgap, default"));

    //---- label1 ----
    label1.setText("Certificate file to install:");
    add(label1, CC.xy(1, 1));
    add(tfCertFilePath, CC.xy(3, 1));

    //---- btnBrowse ----
    btnBrowse.setText("Browse...");
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnBrowseActionPerformed();
      }
    });
    add(btnBrowse, CC.xy(5, 1));

    //---- labelTips ----
    labelTips.setText(
        "Click \"Finish\" to start installing the selected certfifcate. This may take a few seconds to complete.Please wait patiently.");
    labelTips.setLineWrap(true);
    add(labelTips, CC.xywh(1, 3, 5, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JTextField tfCertFilePath;
  private JButton btnBrowse;
  private JXLabel labelTips;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
