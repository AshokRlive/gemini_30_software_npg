/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXLabel;

import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;

class FirstStep extends WizardPage {

  public final static String OPTION_GEN_CSR = "genCSR";
  public final static String OPTION_INSTALL_CERT = "installCert";
  public final static String OPTION_GEN_SELFSIGN = "genSelfSigned";

  public final static String ID = "FirstStep";
  
  FirstStep() {
    super(ID, "Choose options");
    initComponents();
    initComponentNames();
    updateTips();
  }


  private void updateTips() {
    labelTips.setText("");
    
    //TODO better explanation needed.
//    Enumeration<AbstractButton> btns = buttonGroup.getElements();
//    while (btns.hasMoreElements()) {
//      AbstractButton btn = btns.nextElement();
//      
//      if(btn .isSelected()) {
//        labelTips.setText(btn.getToolTipText());
//      }
//    }
  }




  private void initComponentNames() {
    radioGenCSR.setName(OPTION_GEN_CSR);
    radioInstallCert.setName(OPTION_INSTALL_CERT);
    radioGenSelfSigned.setName(OPTION_GEN_SELFSIGN);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    updateTips();
    
    if (buttonGroup.getSelection() == null)
      return "No option selected";
    
    
    return null;
  }
  
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    radioGenCSR = new JRadioButton();
    radioInstallCert = new JRadioButton();
    radioGenSelfSigned = new JRadioButton();
    labelTips = new JXLabel();
    buttonGroup = new ButtonGroup();

    //======== this ========
    setLayout(new FormLayout(
        "10dlu, $lcgap, default:grow, $lcgap, default",
        "3*(default, $lgap), default:grow, 2*($lgap, default)"));

    //---- radioGenCSR ----
    radioGenCSR.setText("Generate a CSR");
    radioGenCSR.setToolTipText("Generate a Certificate Signing Request");
    add(radioGenCSR, CC.xy(3, 1));

    //---- radioInstallCert ----
    radioInstallCert.setText("Install a signed certificate");
    radioInstallCert.setToolTipText("Install a signed certificate to RTU");
    add(radioInstallCert, CC.xy(3, 3));

    //---- radioGenSelfSigned ----
    radioGenSelfSigned.setText("Generate self signed certificate");
    radioGenSelfSigned.setToolTipText("Generate a self signed certificate");
    add(radioGenSelfSigned, CC.xy(3, 5));

    //---- labelTips ----
    labelTips.setText("Tips");
    add(labelTips, CC.xy(3, 7));

    //---- buttonGroup ----
    buttonGroup.add(radioGenCSR);
    buttonGroup.add(radioInstallCert);
    buttonGroup.add(radioGenSelfSigned);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JRadioButton radioGenCSR;
  private JRadioButton radioInstallCert;
  private JRadioButton radioGenSelfSigned;
  private JXLabel labelTips;
  private ButtonGroup buttonGroup;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
