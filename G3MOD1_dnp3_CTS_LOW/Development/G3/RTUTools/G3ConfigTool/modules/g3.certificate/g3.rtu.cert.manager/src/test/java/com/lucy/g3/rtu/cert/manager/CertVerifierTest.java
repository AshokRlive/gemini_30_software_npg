/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.cert.manager.CertVerifier;

import org.junit.Assert;

/**
 *
 */
public class CertVerifierTest {
  @Test @Ignore // Not implemented 
  public void testVerify() throws Exception{
    InputStream csr1 = getClass().getResourceAsStream("/sample/device1.key");
    InputStream cert1 = getClass().getResourceAsStream("/sample/device1.crt");
    InputStream csr2 = getClass().getResourceAsStream("/sample/device2.key");
    InputStream cert2 = getClass().getResourceAsStream("/sample/device2.crt");
    
    Assert.assertTrue(CertVerifier.verifyCert(cert1, csr1));
    Assert.assertTrue(CertVerifier.verifyCert(cert2, csr2));
    Assert.assertFalse(CertVerifier.verifyCert(cert1, csr2));
    Assert.assertFalse(CertVerifier.verifyCert(cert2, csr1));
    
    Assert.assertFalse(CertVerifier.verifyCert(csr1, cert1));
    Assert.assertFalse(CertVerifier.verifyCert(csr2, cert2));
  }
  
}

