/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;


import static com.lucy.g3.rtu.cert.manager.RTUCertArgParser.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;

import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.datalink.http.HttpDataLink;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUCertCommands;
/**
 *
 */
public class RTUCertCommandLine {

  private final String[] args;

  public RTUCertCommandLine(String[] args) {
    this.args = args;
  }
  

  public int run() {
    RTUCertArgParser parser = new RTUCertArgParser();
    
    // Parse arguments
    try {
      parser.parse(args);
    } catch (ParseException e) {
      printError(e);
      printHelp();
      return -1;
    }
    
    // Get arguments value
    String ip = parser.getOptionValue(OPTION_IP);
    String user = parser.getOptionValue(OPTION_USER);
    String pass = parser.getOptionValue(OPTION_PASS);
    
    
    // Create temp dir for file transfer.
    File dir;
    try {
      dir = Files.createTempDirectory(".cert").toFile();
    } catch (IOException e) {
      printError(e);
      return -1;
    }
    
    // Create data link
    HttpDataLink https = new HttpDataLink(ip);
    https.getSecurityManager().setIgnoreCertificateError(true);
    G3RTUClient client = new G3RTUClient(https);
    
    // Login to RTU
    // TODO...
    
    // Create certificate manager.
    RTUCertCommands commands = new RTUCertCommands(client, new FileTransfer(client, dir));
    RTUCertManager manager = new RTUCertManager(commands, RTUCertManager.OWNER_HTTPS);
    
    
    try {
      manager.cleanRTULog();
      
      if(parser.hasOption(OPTION_GEN_CSR)) {
        String csrPath = parser.getOptionValue(OPTION_GEN_CSR);
        File csrFile = new File(csrPath);
        System.out.println("Generating CSR...");
        manager.generateCSR(csrFile);
        System.out.println("CSR is generated to: " + csrPath);
        System.out.println(FileUtils.readFileToString(csrFile));
        
        
      } else if(parser.hasOption(OPTION_INSTALL)) {
        String cert = parser.getOptionValue(OPTION_INSTALL);
        System.out.println("Installing certificate...");
        manager.installCertificate(new File(cert));
        System.out.println("The certificate has been installed to RTU!");
      }
    }
    catch (Exception e){
      printError(e);
      return -1;
    }
    
    // Clean temp dir
    try {
      FileUtils.deleteDirectory(dir);
    } catch (IOException e) {
      printError(e);
      return -1;
    }
    
    return 0;
  }

  private void printError(Exception e) {
    //System.err.println(e.getMessage());
    e.printStackTrace();
  }
  
  public static int run(String[] args) {
    return new RTUCertCommandLine(args).run();
  }

  public static void printHelp(){
    new RTUCertArgParser().printHelp();
  }
}

