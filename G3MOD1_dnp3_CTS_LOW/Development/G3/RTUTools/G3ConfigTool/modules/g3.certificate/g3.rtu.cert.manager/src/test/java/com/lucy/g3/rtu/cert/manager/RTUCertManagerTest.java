/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.rtu.cert.manager.RTUCertManager;
import com.lucy.g3.rtu.comms.client.G3RTUClient;
import com.lucy.g3.rtu.comms.datalink.http.HttpDataLink;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUCertCommands;

import org.junit.Assert;


/**
 *
 */
@Ignore // Require RTU to run test
public class RTUCertManagerTest {
  private final File dir = new File("target/RTUCertManagerTest/");
  
  private final static String RTU_IP = "10.11.3.162";
  private final static String CERTIFICATE_FILE = "target/device.crt";
  
  private RTUCertManager fixture;
  
  @Before
  public void init() throws IOException{
    Assume.assumeNotNull(RTU_IP);
    Assume.assumeNotNull(CERTIFICATE_FILE);
    
    FileUtils.deleteDirectory(dir);
    dir.mkdirs();
    
    HttpDataLink https = new HttpDataLink();
    https.getSecurityManager().setIgnoreCertificateError(true);
    G3RTUClient client = new G3RTUClient(https);
    
    RTUCertCommands commands = new RTUCertCommands(client, new FileTransfer(client, dir));
    
    fixture = new RTUCertManager(commands, RTUCertManager.OWNER_HTTPS);
  }
  
  
  @Test
  public void testGenerateCSR() throws Exception {
    File csr = new File(dir, "uploaded.csr");
    fixture.generateCSR(csr);
    Assert.assertTrue(csr.exists());
    csr.delete();
  }
  
  @Test
  public void testInstall() throws Exception {
    fixture.installCertificate(new File(CERTIFICATE_FILE));
  }
}

