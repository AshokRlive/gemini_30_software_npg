/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.util.Map;

import javax.swing.JCheckBox;

import org.jdesktop.swingx.JXLabel;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
public class GenCsrStep extends WizardPage{
  public final static String OPTION_GEN_PKEY_CHECKED = "genPrivateKeyChecked";
  
  public GenCsrStep() {
    super("Generate CSR");
    initComponents();
    initComponentNames();
    cboxGenPKey.setSelected(true);
  }

  @Override
  public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
    UIUtils.enableComponents(this, false);
   
    return super.allowFinish(stepName, settings, wizard);
  }

  private void initComponentNames() {
    cboxGenPKey.setName(OPTION_GEN_PKEY_CHECKED);
  }


  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    cboxGenPKey = new JCheckBox();
    labelTips = new JXLabel();

    //======== this ========
    setLayout(new FormLayout(
        "right:default, $lcgap, default, $lcgap, default:grow, $lcgap, default",
        "default, $pgap, default, $lgap, default:grow"));

    //---- cboxGenPKey ----
    cboxGenPKey.setText("Re-generate private key");
    add(cboxGenPKey, CC.xy(3, 1));

    //---- labelTips ----
    labelTips.setText(
        "Click \"Finish\" to start generating CSR. This may take a few seconds to complete.Please wait patiently.");
    labelTips.setLineWrap(true);
    add(labelTips, CC.xywh(1, 5, 7, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JCheckBox cboxGenPKey;
  private JXLabel labelTips;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
