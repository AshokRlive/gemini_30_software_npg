/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.cert.manager.RTUCertManager;

class GenSelfSignedResult extends DeferredCertResult {

  public GenSelfSignedResult(RTUCertManager manager) {
    super(manager);
  }

  @Override
  public void start(@SuppressWarnings("rawtypes") Map settings, ResultProgressHandle progress) {
    try {
      manager.cleanRTULog();

      boolean doInstall = settings.get(GenSelfSignedStep.OPTION_INSTALL_SELF_SIGNED_CHECKED) == Boolean.TRUE;

      progress.setBusy("Generating self signed certificate...");
      File tempDir = Files.createTempDirectory("cert").toFile();
      tempDir.deleteOnExit();
      
      File[] genFiles = manager.generateSelfSignedCert(tempDir, doInstall);

      progress.finished(createSuccessSummary(
          "The self signed certificate has been generated.",
          manager.getRTULogFile(), genFiles));

    } catch (Exception e) {
      Logger.getLogger(GenSelfSignedResult.class).error("Failed with error", e);
      progress.failed(createFailureComponent("Failed to generate self signed certificate!",
          manager.getRTULogFile(), e), false);
    }

  }

}
