/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.util.Map;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.displayer.WizardDisplayerImpl;
import org.netbeans.modules.wizard.MergeMap;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.cert.manager.RTUCertManager;


public class RTUCertWizard extends WizardBranchController {

  public final static String OPTION_GEN_CSR      = FirstStep.OPTION_GEN_CSR      ;
  public final static String OPTION_INSTALL_CERT = FirstStep.OPTION_INSTALL_CERT ;
  public final static String OPTION_GEN_SELFSIGN = FirstStep.OPTION_GEN_SELFSIGN ;
  
  private static final int WITDTH = 680;
  private static final int HEIGHT = 545;
  
  private final RTUCertManager manager;
  
  private RTUCertWizard(String title, WizardPage onlyPage, RTUCertManager manager) {
    super(title, onlyPage);
    this.manager = manager;
  }

  @Override
  protected Wizard getWizardForStep(String step, Map settings) {
    if (FirstStep.ID.equals(step)) {
      
      if (settings.get(OPTION_GEN_CSR) == Boolean.TRUE) {
        return WizardPage.createWizard(
            new WizardPage[] { new GenCsrStep() }, 
            new GenCsrResult(manager));
        
      } else if (settings.get(OPTION_INSTALL_CERT) == Boolean.TRUE) {
        return WizardPage.createWizard(
            new WizardPage[] { new InstallCertStep() }, 
            new InstallCertResult(manager));
      }
      
      else if(settings.get(OPTION_GEN_SELFSIGN) == Boolean.TRUE) {
        return WizardPage.createWizard(
            new WizardPage[] { new GenSelfSignedStep() }, 
            new GenSelfSignedResult(manager));
      }
    }
    
    return null;
  }
  
  public static String showWizard(RTUCertManager manager) {
    RTUCertWizard  wiz = new RTUCertWizard("RTU Certificate Wizard", new FirstStep(), manager);
    Map result = (Map) WizardDisplayer.showWizard(wiz.createWizard(), WizardUtils.createRect(null, WITDTH, HEIGHT), null,null);
    
    if(result == null){
      return null; // User cancelled or failed
    }
    
    if (result.get(OPTION_GEN_CSR) == Boolean.TRUE) {
      return OPTION_GEN_CSR;
    } else if (result.get(OPTION_INSTALL_CERT) == Boolean.TRUE) {
      return OPTION_INSTALL_CERT;
    } else if(result.get(OPTION_GEN_SELFSIGN) == Boolean.TRUE) {
      return OPTION_GEN_SELFSIGN;
    }
    
    return null;
  }

}
