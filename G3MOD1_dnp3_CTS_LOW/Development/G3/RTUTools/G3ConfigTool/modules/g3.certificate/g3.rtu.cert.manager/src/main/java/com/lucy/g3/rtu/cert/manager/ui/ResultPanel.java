/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;
import javax.swing.text.TextAction;

import org.apache.commons.io.FileUtils;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.util.WindowUtils;

import com.jgoodies.forms.builder.ButtonBarBuilder2;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
class ResultPanel extends JPanel {
  
  ResultPanel() {
    initComponents();
    tabbedPane.setVisible(false);
  }
  
  public void addLogFile(File logFile){
    tabbedPane.add("Log", createFilePreviewer(logFile, false, true));
    tabbedPane.setVisible(true);
  }
  
  public void addCertFile(File certFile){
    tabbedPane.add(certFile.getName(), createFilePreviewer(certFile, true, true));
    tabbedPane.setVisible(true);
  }
  
  public void setMessage(String message){
    labelMessage.setText(message);
  }
  
  public void addHyperLink(Action abstractAction) {
    JXHyperlink detailLink = new JXHyperlink(abstractAction);
    panelFiles.add(detailLink);
  }
  
  private static JComponent createFilePreviewer(final File file, boolean showExport, boolean scrollable){
    // Read file
    String fileContent;
    try {
      fileContent = FileUtils.readFileToString(file);
    } catch (IOException e) {
      fileContent = e.getMessage();
    }
    
    // Create text area
    JTextArea preview = new JTextArea(fileContent);
    preview.setEditable(false);
    preview.setFont(new Font("monospaced", Font.PLAIN, 12));
    JPopupMenu popMenu = new JPopupMenu();
    popMenu.add(new TextAction("Copy") {
      @Override
      public void actionPerformed(ActionEvent e) {
        JTextComponent component = getFocusedComponent();
        if(component != null) {
          component.selectAll();
          component.copy();
        }
      }
    });
    preview.setComponentPopupMenu(popMenu);
    preview.setToolTipText("CSR Content");
    
    // Create export button
    JButton exportBtn = new JButton("Export");
    exportBtn.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        Window parent = SwingUtilities.getWindowAncestor((JButton) e.getSource());
        
        File destFile = DialogUtils.showFileChooseDialog(parent, "Export \""+file.getName()+"\"", "Export", file.getName());
        if(destFile != null) {
          
          // Confirm overwrite
          if(destFile.exists()) {
            boolean ret = MessageDialogs.showOverwriteDialog(parent, destFile);
            if(ret == false) {
              return;
            }
          }
          
          try {
            FileUtils.copyFile(file, destFile);
            MessageDialogs.finish(parent, "The file has been exported to:\n"+destFile);
          } catch (IOException e1) {
            MessageDialogs.error(parent, "Failed to export file", e1);
          }
        }
      }
    });
    
    
    JPanel container = new JPanel(new BorderLayout(0,0));
    
    if(scrollable) {
      // Create scroll pane
      JScrollPane previewPane = new JScrollPane(preview);
      previewPane.setBorder(null);
      container.add(previewPane, BorderLayout.CENTER);
    } else {
      container.add(preview, BorderLayout.CENTER);
    }
    
    if(showExport) {
      ButtonBarBuilder2 btnPanelBuilder = new ButtonBarBuilder2();
      btnPanelBuilder.addGlue();
      btnPanelBuilder.addButton(exportBtn);
      btnPanelBuilder.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      JPanel btnPanel = btnPanelBuilder.getPanel();
      btnPanel.setOpaque(false);
      container.add(btnPanel, BorderLayout.SOUTH);
    }
    
    return container;
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panelTop = new JPanel();
    labelMessage = new JXLabel();
    panelFiles = new JPanel();
    tabbedPane = new JTabbedPane();

    //======== this ========
    setBorder(new EmptyBorder(0, 10, 0, 10));
    setLayout(new BorderLayout(0, 10));

    //======== panelTop ========
    {
      panelTop.setLayout(new FormLayout(
          "default, $lcgap, default:grow",
          "default, $lgap, default"));

      //---- labelMessage ----
      labelMessage.setText("message");
      labelMessage.setLineWrap(true);
      panelTop.add(labelMessage, CC.xywh(1, 1, 3, 1));

      //======== panelFiles ========
      {
        panelFiles.setLayout(new FlowLayout());
      }
      panelTop.add(panelFiles, CC.xywh(1, 3, 3, 1));
    }
    add(panelTop, BorderLayout.NORTH);
    add(tabbedPane, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panelTop;
  private JXLabel labelMessage;
  private JPanel panelFiles;
  private JTabbedPane tabbedPane;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
  
}
