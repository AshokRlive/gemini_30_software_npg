/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import com.lucy.g3.cert.manager.gui.X509CertUtil;

/**
 *
 */
public class CertVerifier {
  public static boolean verifyCert(File certFile, File csrFile) throws Exception{
    return  verifyCert(new FileInputStream(certFile), new FileInputStream(csrFile));
  }
  
  public static boolean verifyCert(InputStream signedCertStream, InputStream singingCertStream) throws Exception{
    //return true; //TODO to be implemented
    CertificateFactory factory = CertificateFactory.getInstance(X509CertUtil.X509_CERT_TYPE);
    X509Certificate signed  = (X509Certificate) factory.generateCertificate(signedCertStream);
    X509Certificate signing = (X509Certificate)factory.generateCertificate(singingCertStream);

    return X509CertUtil.verifyCertificate(signed, signing);
  }
}

