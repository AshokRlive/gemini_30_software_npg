/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Summary;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.cert.manager.RTUCertManager;

class InstallCertResult extends DeferredCertResult {


  public InstallCertResult(RTUCertManager manager) {
    super(manager);
  }

  @Override
  public void start(Map settings, ResultProgressHandle progress) {
    try {
      manager.cleanRTULog(); 
      
      progress.setBusy("Installing certificate...");
      String target = (String) settings.get(InstallCertStep.OPTION_CERT_FILE_PATH);
      manager.installCertificate(new File(target));

      progress.finished(createSummary(settings));

    } catch (Exception e) {
      Logger.getLogger(InstallCertResult.class).error("Failed with error", e);
      progress.failed("Failed with error:" + e.getMessage(), false);
    }
  }

  private Object createSummary(Map wizardData) {
    return Summary.create("Success!\n\nThe certificate has been installed."
        + "Please reboot RTU to the apply the new certificate.", null);
  }

}
