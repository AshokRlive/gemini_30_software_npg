/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;

import javax.swing.JComponent;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Summary;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.cert.manager.RTUCertManager;
import com.lucy.g3.rtu.comms.service.rtu.control.IRTUCertCommands;

class GenCsrResult extends DeferredCertResult {

  public GenCsrResult(RTUCertManager manager) {
    super( manager);
  }

  @Override
  public void start(@SuppressWarnings("rawtypes") Map settings, ResultProgressHandle progress) {
    try {
      manager.cleanRTULog();
      
      if (settings.get(GenCsrStep.OPTION_GEN_PKEY_CHECKED) == Boolean.TRUE) {
        progress.setBusy("Generating private key...");
        manager.generatePrivateKey();
      }
      progress.setProgress(1, 2);

      progress.setBusy("Generating csr...");
      File tempDir = Files.createTempDirectory("cert").toFile();
      tempDir.deleteOnExit();
      
      File csrFile = new File(tempDir, IRTUCertCommands.FILE_NAME_CSR);
      manager.generateCSR(csrFile);

      progress.setProgress(2, 2);
      
      progress.finished(createSuccessSummary(settings, csrFile));

    } catch (Exception e) {
      Logger.getLogger(GenCsrResult.class).error("Failed with error", e);
      JComponent errComp = createFailureComponent("Failed to generate CSR!", manager.getRTULogFile(), e);
      progress.failed(errComp, false);
    }
  }
  
  private Summary createSuccessSummary(@SuppressWarnings("rawtypes") Map wizardData, File csrFile) {
    File logFile = manager.getRTULogFile();
    return createSuccessSummary(
        "The CSR has been generated. Please pass the CSR file to CA and sign it, then run the wizard again with option "
        + "\"Install a signed certificate\".",
        logFile, new File[] {csrFile});
  }

}
