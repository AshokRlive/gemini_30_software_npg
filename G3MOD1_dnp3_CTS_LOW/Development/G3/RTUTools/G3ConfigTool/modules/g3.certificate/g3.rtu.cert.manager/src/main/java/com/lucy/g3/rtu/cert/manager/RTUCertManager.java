/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;

import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.cert.manager.gui.KeyStoreActions;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.rtu.control.IRTUCertCommands;
import com.lucy.g3.security.support.SSLSupport;

public class RTUCertManager {
  public static final String OWNER_HTTPS = IRTUCertCommands.OWNER_HTTPS;
  public static final String OWNER_SDNP3 = IRTUCertCommands.OWNER_SDNP3;
  public static final String OWNER_S104  = IRTUCertCommands.OWNER_S104 ;
  
  private static final int STATUS_CHECKING_DELAY = 1000;
  
  private static final int MAX_STATUS_RETRIES = 3;

  private Logger log = Logger.getLogger(RTUCertManager.class);

  private final IRTUCertCommands commands;
  
  private String owner;
  
  private final File certLogFile = new File("log/cert.log");


  
  /**
   * Instantiates a new RTU cert manager.
   *
   * @param commands
   *          the API for communicating with RTU.
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @throws IOException
   */
  public RTUCertManager(IRTUCertCommands commands, String owner) {
    super();
    this.commands = commands;
    this.owner = Preconditions.checkNotNull(owner, "owner must not be null");
  }

  public File getRTULogFile() {
    return certLogFile;
  }

  /**
   * Generate private key.
   *
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @throws Exception
   *           the exception
   */
  public void generatePrivateKey() throws Exception {
    commands.cmdGenPrivateKey(owner);

    checkStatus();
  }

  /**
   * Checks the running status of certificate commands. 
   * @throws InterruptedException
   * @throws RTUCertException if the command sent to RTu failed to run. 
   */
  private void checkStatus() throws RTUCertException, InterruptedException{
    Thread.sleep(STATUS_CHECKING_DELAY);
    
    
    try {
      int status = commands.getStatus(owner, MAX_STATUS_RETRIES);
      
      try{
        certLogFile.delete();
        commands.getLog(owner, certLogFile);
      }catch(Exception e){}
      
      if(status != IRTUCertCommands.STATUS_SUCCESS)
        throw new RTUCertException("Certificate command failed with status: " + status);
    } catch (IOException e) {
      throw new RTUCertException("Failed to get the status of certificate command from RTU");
    }
  }

  /**
   * Generate self signed certificate.
   *
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @param dir
   *         the directory for storing the generated certificate files.
   * @return generated cert files.
   * @throws Exception
   *           the exception
   */
  public File[] generateSelfSignedCert(File dir, boolean doInstall) throws Exception {
    ArrayList<File> genFiles = new ArrayList<>();
    commands.cmdGenSelfSignedCert(owner);
    checkStatus();
    
    String [] pendingFiles = {
        IRTUCertCommands.FILE_NAME_CSR,
        IRTUCertCommands.FILE_NAME_PRIVATE_KEY,
        IRTUCertCommands.FILE_NAME_CERT,
    };
    
    dir.mkdirs();
    for (int i = 0; i < pendingFiles.length; i++) {
      File file = new File(dir, pendingFiles[i]);
      genFiles.add(file);
      try{
        commands.readPendingFile(owner, pendingFiles[i], file.getAbsolutePath());
      }catch(Exception e){
        log.error("Failed to read file from RTU: "+pendingFiles[i]);
      }
    }
    
    if(doInstall) {
      commands.cmdInstallCert(owner);
      checkStatus();
    }
    
    return genFiles.toArray(new File[genFiles.size()]);
  }
  
  
  /**
   * Generate CSR.
   *
   * @param owner
   *          the owner directory that contains the certificates. One of
   *          {@code OWNER_HTTPS},{@code OWNER_HTTPS} ,
   *          {@code OWNER_S104}.
   * @param target
   *          the target
   * @throws Exception
   *           the exception
   */
  public void generateCSR(File target) throws Exception {
    commands.cmdGenCSR(owner);

    checkStatus();

    try {
      target.getParentFile().mkdirs();
      commands.readCSRFile(owner, target.getAbsolutePath());
    }catch(Exception e){
      throw new Exception("CSR file was not generated by RTU!",e);
    }

  }


  /**
   * Install certificate.
   *
   * @param certificateFile
   *          the certificate file
   * @throws Exception
   *           the exception
   */
  public void installCertificate(File certificateFile) throws Exception {
    commands.writeCertFile(owner, certificateFile.getAbsolutePath());
    
    commands.cmdInstallCert(owner);

    checkStatus();
  }
  
  
  public void cleanRTULog() {
    try {
      commands.cmdCleanLog(owner);
    } catch (IOException | SerializationException e) {
      log.error("Failed to clean RTU cert log");
    }
  }
  
  public static void viewRTUCert(Window parent, String rtuIPAddress){
    InetSocketAddress ia = new InetSocketAddress(rtuIPAddress, SSLSupport.DEFAULT_PORT);
    KeyStoreActions.executeExamineCertSSLTask(Application.getInstance(),
        parent, ia);
  }
  
}
