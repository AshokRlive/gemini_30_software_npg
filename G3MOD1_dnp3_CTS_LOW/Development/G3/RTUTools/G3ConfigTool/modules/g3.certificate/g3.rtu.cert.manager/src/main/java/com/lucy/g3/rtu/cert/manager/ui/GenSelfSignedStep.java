/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXLabel;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
public class GenSelfSignedStep extends WizardPage{
  public final static String OPTION_INSTALL_SELF_SIGNED_CHECKED = "installSelfSignedChecked";

  public GenSelfSignedStep() {
    super("Self Signed Certificate");
    initComponents();
    initComponentNames();
    
    cboxInstall.setSelected(true);
  }

  @Override
  public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
    UIUtils.enableComponents(this, false);
   
    return super.allowFinish(stepName, settings, wizard);
  }

  private void initComponentNames() {
    cboxInstall.setName(OPTION_INSTALL_SELF_SIGNED_CHECKED);
  }


  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    cboxInstall = new JCheckBox();
    labelTips = new JXLabel();

    //======== this ========
    setLayout(new FormLayout(
        "right:default, $lcgap, default, $lcgap, default:grow",
        "default, $ugap, default:grow"));

    //---- cboxInstall ----
    cboxInstall.setText("Install the self signed certificate after it is generated");
    cboxInstall.setSelected(true);
    add(cboxInstall, CC.xy(3, 1));

    //---- labelTips ----
    labelTips.setText(
        "Click \"Finish\" to start generating self signed certificate. This may take a few seconds to complete.Please wait patiently.");
    labelTips.setLineWrap(true);
    add(labelTips, CC.xywh(1, 3, 5, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JCheckBox cboxInstall;
  private JXLabel labelTips;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
