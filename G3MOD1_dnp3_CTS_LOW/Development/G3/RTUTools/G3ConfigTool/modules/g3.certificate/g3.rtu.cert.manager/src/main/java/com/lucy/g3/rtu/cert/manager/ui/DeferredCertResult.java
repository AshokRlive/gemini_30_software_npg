/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager.ui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.JComponent;

import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.Summary;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.cert.manager.RTUCertManager;

abstract class DeferredCertResult extends DeferredWizardResult implements WizardResultProducer {
  protected RTUCertManager manager;
  private Map settings;

  public DeferredCertResult(RTUCertManager manager) {
    super();
    this.manager = manager;
  }

  @Override
  public Object finish(Map wizardData) throws WizardException {
    this.settings = wizardData;
    return this;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

  Summary createSuccessSummary(String resultMessage, File logFile, File[]certFiles){
    ResultPanel result = new ResultPanel();
    
    result.setMessage(resultMessage);

    if(certFiles != null)
    for (int i = 0; i < certFiles.length; i++) {
      result.addCertFile(certFiles[i]);
    }
    
    if(logFile != null && logFile.exists())
      result.addLogFile(logFile);
    
    return Summary.create(result, settings);
  }
  
  JComponent createFailureComponent(String message, File logFile, final Throwable cause){
    final ResultPanel result = new ResultPanel();
    if(message == null) {
      message = cause.getMessage();
    }
    
    result.setMessage(message);
    
    result.addHyperLink(new AbstractAction("See details...") {
      @Override
      public void actionPerformed(ActionEvent e) {
        MessageDialogs.error(result, null, cause);
      }
    });
    
    if(logFile.exists())
    result.addLogFile(logFile);
    
    return result;
  }
}
