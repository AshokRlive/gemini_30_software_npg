/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;


/**
 *
 */
public class RTUCertException extends Exception {

  /**
   * 
   */
  public RTUCertException() {
    super();
    
  }

  /**
   * @param message
   * @param cause
   * @param enableSuppression
   * @param writableStackTrace
   */
  public RTUCertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    
  }

  /**
   * @param message
   * @param cause
   */
  public RTUCertException(String message, Throwable cause) {
    super(message, cause);
    
  }

  /**
   * @param message
   */
  public RTUCertException(String message) {
    super(message);
    
  }

  /**
   * @param cause
   */
  public RTUCertException(Throwable cause) {
    super(cause);
    
  }

}

