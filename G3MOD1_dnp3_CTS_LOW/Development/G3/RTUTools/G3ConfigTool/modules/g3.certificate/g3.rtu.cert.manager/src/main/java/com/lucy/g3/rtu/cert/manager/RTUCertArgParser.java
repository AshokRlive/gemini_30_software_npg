/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.cert.manager;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 */
public class RTUCertArgParser {
  private static final String DEFAULT_JAR_NAME = "G3ConfigTool.jar";

  public static final String OPTION_CERT = "cert";
  public static final String OPTION_IP = "ip";
  public static final String OPTION_USER = "user";
  public static final String OPTION_PASS = "pass";
  public static final String OPTION_GEN_CSR = "csr";
  public static final String OPTION_INSTALL = "install";

  
  private final Options options;
  private CommandLine line;


  public RTUCertArgParser() {
    options = new Options();
    
    // ================== Mandatory Options ===================
    
    Option cert = Option.builder(OPTION_CERT)
        .longOpt(OPTION_CERT)
        .desc("[mandatory] update RTU certificate")
        .required()
        .build();
    
    Option ip = Option.builder(OPTION_IP)
        .required()
        .hasArg().argName("IP").type(String.class)
        .desc("[mandatory] ip address of RTU to be connected to")
        .build();
    
    Option user = Option.builder(OPTION_USER)
        //.required()
        .hasArg().argName("USERNAME").type(String.class)
        .desc("[mandatory] user name used to login to RTU")
        .build();
    
    Option pass = Option.builder(OPTION_PASS)
        //.required()
        .hasArg().argName("PASSWORD").type(String.class)
        .desc("[mandatory] password used to login to RTU")
        .build();
    
    Option csr = Option.builder(OPTION_GEN_CSR)
        .hasArg().argName("CSR FILE").type(String.class)
        .desc("[mandatory] send a request to RTU to generate CRS")
        .build();
    
    Option install = Option.builder(OPTION_INSTALL)
        .hasArg().argName("CERTIFICATE FILE").type(String.class)
        .desc("[mandatory] install the given certificate to RTU")
        .build();
    
    
    options.addOption(ip);
    options.addOption(cert);
    options.addOption(user);
    options.addOption(pass);
    options.addOption(csr);
    options.addOption(install);
  }
  
  public void printHelp(){
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(
        800, 
        "java -jar " + DEFAULT_JAR_NAME, 
        "========== Parameters ===========", 
        options, 
        "", 
        true);
  }
  
  
  public void parse(String[] args) throws ParseException {
    parse(args,false);
  }
  
  public void parse(String[] args, boolean checkFileExist) throws ParseException {
    CommandLineParser parser = new DefaultParser();
    line = parser.parse(options, args, false);
  }
  

  boolean hasOption(String opt) {
    return line.hasOption(opt);
  }
  
  String getOptionValue(String opt) {
    return line.getOptionValue(opt);
  }
}

