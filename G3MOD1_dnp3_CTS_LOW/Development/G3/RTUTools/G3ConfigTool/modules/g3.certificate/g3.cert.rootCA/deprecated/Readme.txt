root.jks - A java keystore for storing lucy root key pair.
scripts\create_root_key.bat - A script for re-generating root.jks.
scripts\sign_csr.bat - a script for signing csr using lucy CA.
src\main\resources\certs\LucyRoot.cert - lucy root public key. It will be built into configtool and imported the local keystore when running ConfigTool. 

 