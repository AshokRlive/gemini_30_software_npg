/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.rootCA;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.Test;


/**
 *
 */
public class RootCATest {

  @Test
  public void test() {
    InputStream rootCA = RootCA.getRootCAStream();
    assertNotNull(rootCA);
  }

}

