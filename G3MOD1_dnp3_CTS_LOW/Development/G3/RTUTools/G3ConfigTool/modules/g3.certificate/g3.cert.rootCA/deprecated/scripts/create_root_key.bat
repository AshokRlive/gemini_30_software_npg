@echo off
set /P AREYOUSURE=Are you sure you want to re-generate lucy root CA(Y/N)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

set ks="..\root.jks"
set pass=password
set alias=lucyroot
set certFile="..\src\main\resources\certs\LucyRoot.cer"

echo Deleting current private key...
DEL %ks%

echo Create private key and save to the keystore(jks)...
keytool -genkey -alias %alias% -keystore %ks% -storepass %pass% -keypass %pass% -keysize 2048 -keyalg RSA -validity 18250 -noprompt -dname "CN=192.168.0.1, OU=Automation, O=Lucy Electric, L=Banbury, S=Oxford, C=GB"
if %ERRORLEVEL% GEQ 1 EXIT /B 1

echo Export public certificate(pem)...
keytool -export -keystore %ks% -storepass %pass% -alias %alias% -file %certFile%

echo Lucy root CA has been re-generated!

:END



