:: A script for signing a csr with lucy CA.
@echo off
if "%1%"=="" goto missingArgs
if "%2%"=="" goto missingArgs
if "%3%"=="" goto missingArgs

echo [Step 1] Preparing root key and root CA...
set ks=%1%
set kspass=password
set ks_p12=".tmp\p12RootKeyStore.pfx"
set rootkey=".tmp\lucyroot.key"
set rootcert=".tmp\lucyroot.crt"

::Clean
rmdir /S /Q .tmp
md .tmp


::Convert the JKS into to a P12
keytool -importkeystore -srckeystore %ks% -destkeystore %ks_p12% -srcstorepass %kspass% -deststorepass %kspass% -srcstoretype jks -deststoretype pkcs12 -noprompt
if errorlevel 1 exit /b %errorlevel%

::Export the Private Key as a PEM file 
openssl pkcs12 -in %ks_p12% -passin pass:%kspass% -nocerts -nodes -out %rootkey%
if errorlevel 1 exit /B %errorlevel%

::Exporting the Certificate 
openssl pkcs12 -in %ks_p12% -passin pass:%kspass% -clcerts -nokeys -out %rootcert%
if errorlevel 1 exit /B %errorlevel%

echo.

echo [Step 2] Signing csr...
set csr=%2%
set crt=%3%
set /P serial=Enter serial:
set /P days=Enter valid days:

::Sign request with root CA and private key.
openssl x509 -req -CA %rootcert% -CAkey %rootkey% -in %csr% -out %crt% -days 123 -set_serial 321
if errorlevel 1 exit /b %errorlevel%
echo Certificate was generated: "%crt%"
echo Done
exit /B 0

:missingArgs
echo Usage: sign_csr.bat [CA key store] [Input CSR file] [Output Certificate file]
exit /B -1
