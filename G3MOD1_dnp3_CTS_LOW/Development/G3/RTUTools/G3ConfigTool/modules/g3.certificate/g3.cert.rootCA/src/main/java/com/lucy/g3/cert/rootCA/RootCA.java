/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.cert.rootCA;

import java.io.InputStream;

/**
 *
 */
public class RootCA {
  public final static String ROOT_ALIAS = "lucyroot"; 
  private final static  String ROOT_CERT_PATH = "/certs/LucyRoot.cer";

  public static InputStream getRootCAStream(){
    return RootCA.class.getResourceAsStream(ROOT_CERT_PATH);
  }
}

