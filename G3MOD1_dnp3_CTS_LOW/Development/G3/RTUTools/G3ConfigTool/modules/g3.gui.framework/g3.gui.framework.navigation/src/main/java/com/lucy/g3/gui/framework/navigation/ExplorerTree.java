/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.navigation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXTree;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.IPageViewer;
import com.lucy.g3.gui.framework.page.Navigable;
import com.lucy.g3.gui.framework.page.NavigationActionListener;
import com.lucy.g3.gui.framework.page.NavigationEvent;
import com.lucy.g3.gui.framework.page.Page;

/**
 * Adapt IExplorerTree to JXTree.
 */
public abstract class ExplorerTree extends JXTree {

  private Logger log = Logger.getLogger(ExplorerTree.class);

  private final IExplorerTreeNode rootNode;
  private final DefaultTreeModel treeModel;

  private JPopupMenu contextMenu;// Default context menu

  private final IPageViewer viewer;

  private final ConfTreeListener treelistener = new ConfTreeListener();

  private final IExplorerTree impl = new ExplorerTreeImpl();


  public ExplorerTree(IPageViewer viewer, Page rootPage) {
    this.viewer = viewer;
    this.rootNode = new ExplorerTreeNode(impl, rootPage);
    this.treeModel = new DefaultTreeModel(rootNode);

    initTree();
  }

  /**
   * Override to prevent root node to collapse .
   */
  @Override
  protected void setExpandedState(TreePath path, boolean state) {
    if (!state && path.getParentPath() == null) {
      /* Nothing to do with setting root node expanded state to false */
    } else {
      super.setExpandedState(path, state);
    }
  }

  protected abstract IPageFactory getPageFacotry();

  protected IExplorerTreeNode getRootNode() {
    return rootNode;
  }

  /**
   * Load the tree nodes from the given tree node data.(Thread-safe method).
   */
  protected void reloadTreeInEDT(final Object[] nodeData) {
    if (SwingUtilities.isEventDispatchThread()) {
      reloadTree(nodeData);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          reloadTree(nodeData);
        }
      });
    }
  }

  /**
   * Load the tree nodes from the given tree node data.
   */
  private void reloadTree(Object[] nodeData) {
    clearSelection();

    // Stop listening tree change during loading period
    removeTreeSelectionListener(treelistener);

    // Store previous pages' session
    saveSession(rootNode);

    // Remove current nodes
    rootNode.removeAllChildren();

    // Construct all nodes from data
    if (nodeData != null) {
      for (int i = 0; i < nodeData.length; i++) {

        Page page = getPageFacotry().createPage(nodeData[i]);

        setPageAsNavEventListener(page);

        rootNode.add(new ExplorerTreeNode(impl, page));
      }
    }

    // It is key to reload child node into model when root node changed
    // notify the tree root node to be updated
    treeModel.reload();

    // Listen to selection action
    addTreeSelectionListener(treelistener);

    // Note we should avoid selecting tree node after adding treeSelectListener,
    // because the treeSelectListener may stop the current updating task
    // unexpectedly.
    this.setSelectionRow(0);

  }

  public void saveSession() {
    saveSession(rootNode);
  }

  private static void saveSession(TreeNode parent) {
    if (parent == null) {
      return;
    }
  
    if (parent.getChildCount() > 0) {
      for (int index = 0; index < parent.getChildCount(); index++) {
        TreeNode child = parent.getChildAt(index);
        if (child instanceof IExplorerTreeNode) {
          Page page = ((IExplorerTreeNode) child).getPage();
  
          if (page != null && page.isInitialised()) {
            Logger log = Logger.getLogger(ExplorerTree.class);
            JComponent comp = page.getContent();
            String sessionName = page.getSessionKey();
            if (comp != null && sessionName != null) {
              try {
                Application.getInstance().getContext()
                    .getSessionStorage().save(comp, sessionName);
                log.debug("Stored the session of component \""
                    + comp.getName() + "\" to:" + sessionName);
              } catch (Exception e) {
                log.error(
                    "Fail to save the session of page: "
                        + page + ". Error: " + e.getMessage());
              }
            }
          }
  
          // Traverse children
          if (child.getChildCount() > 0) {
            saveSession(child);
          }
        }
      }
    }
  }

  public void setContextMenu(JPopupMenu contextMenu) {
    this.contextMenu = contextMenu;
  }

  private void initTree() {
    setModel(treeModel);

    setRowHeight(20);
    setBackground(Color.white);
    setOpaque(true);
    setCellRenderer(new ExplorerTreeCellRender());
    setRolloverEnabled(true);
    addHighlighter(new ColorHighlighter(
        HighlightPredicate.ROLLOVER_ROW, 
        Color.white, 
        UIThemeResources.COLOUR_LIGHT_BLUE));

    getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    setShowsRootHandles(false);

    addMouseListener(treelistener);
    addTreeSelectionListener(treelistener);

    // It is key to register tree in ToolTip manager to show render's ToolTip
    ToolTipManager.sharedInstance().registerComponent(this);

    setSelectionRow(0);
  }

  private void showPage(Page page) {
    JFrame root = WindowUtils.getMainFrame();
    try {
      if (root != null) {
        root.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      }

      if (viewer != null) {
        viewer.show(page);
      }

    } finally {
      if (root != null) {
        root.setCursor(Cursor.getDefaultCursor());
      }
    }
  }

  private void setPageAsNavEventListener(Page page) {
    if (page != null && page instanceof Navigable) {
      ((Navigable) page).addNavigationEventListener(treelistener);
    }

  }


  private class ConfTreeListener extends MouseAdapter implements TreeSelectionListener, NavigationActionListener {

    /**
     * Refresh the node of selected node on double click. This is needed because
     * sometimes a node's name is not full displayed(it looks like "Module..."),
     * user can expand the node text by double clicking it.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
      if (UIUtils.isDoubleClick(e)) {
        if (getSelectionPath() != null) {
          Object node = getSelectionPath().getLastPathComponent();
          if (node != null) {
            DefaultMutableTreeNode multableNode = (DefaultMutableTreeNode) node;
            treeModel.nodeChanged(multableNode);
          }
        }
      }
    }

    /**
     * Show context menu. Default context menu will be shown if a node doesn't
     * provide a context menu. Default context menu can be set by calling
     * setContextMenu().
     */
    @Override
    public void mousePressed(MouseEvent e) {
      JTree tree = (JTree) e.getSource();
      if (e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON3) {
        JPopupMenu menu = null;
        TreePath path = tree.getPathForLocation(e.getX(), e.getY());

        // Try to get context menu from selected node
        if (path != null) {
          Object comp = path.getLastPathComponent();
          if (comp != null && comp instanceof IExplorerTreeNode) {
            menu = ((IExplorerTreeNode) comp).getContextMenu();
          }
        }

        // Context menu not found, use default one
        if (menu == null) {
          menu = contextMenu;
        }

        if (menu != null) {
          menu.show(tree, e.getX(), e.getY());
        }
      }
    }

    /**
     * Refresh the view when tree node selection changes.
     */
    @Override
    public void valueChanged(TreeSelectionEvent e) {
      impl.refreshPageView();
    }

    /**
     * Implementation of NavigationActionListener. When an NavigationEvent
     * occurs, try to find the tree node we want to navigate to in the
     * exploreTree, if found, select that tree node. if not, do nothing.
     */
    @Override
    public void navigationPerformed(NavigationEvent evt) {
      if (evt.getSource() != null) {
        IExplorerTreeNode result = findNodeByNodeData(evt.getSource(), rootNode);
        if (result != null) {
          setSelectionPath(createNodePath(result));
        } else {
          log.warn("Cannot perform navigation. The tree node for user object \""
              + evt.getSource() + "\" in the tree is NOT found");
        }
      } else {
        log.warn("Navigation source is null");
      }
    }
  }

  private class ExplorerTreeImpl implements IExplorerTree {

    @Override
    public Page getPage(Object data) {
      return getPageFacotry().createPage(data);
    }

    @Override
    public void notifyNodeChanged(IExplorerTreeNode node) {
      treeModel.nodeChanged(node);
    }

    @Override
    public void appendChildNode(IExplorerTreeNode childNode, IExplorerTreeNode parent, boolean shouldBeVisible) {
      int position = parent.getChildCount();
      insertChildNode(childNode, parent, position, shouldBeVisible);
    }

    @Override
    public void insertChildNode(IExplorerTreeNode childNode, IExplorerTreeNode parent,
        int position, boolean shouldBeVisible) {
      if (parent == null) {
        log.error("Cannot insert ChildNode cause parent node is null");
        return;
      }

      if (childNode == null) {
        log.error("Cannot insert Child Node cause the childNode node is null");
        return;
      }

      setPageAsNavEventListener(childNode.getPage());

      // It is key to invoke this on the TreeModel, and NOT
      // DefaultMutableTreeNode
      treeModel.insertNodeInto(childNode, parent, position);

      // Make sure the user can see the lovely new node.
      if (shouldBeVisible) {
        TreeNode[] path = treeModel.getPathToRoot(childNode);
        scrollPathToVisible(new TreePath(path));
      }
    }

    @Override
    public void removeChildNode(IExplorerTreeNode parent, int position) {

      if (parent == null) {
        log.error("Cannot remove ChildNode cause parent node is null");
        return;
      }
      if (position >= parent.getChildCount()) {
        log.error("Cannot remove ChildNode cause index is out of bound:" + position + ">=" + parent.getChildCount());
        return;
      }

      DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) parent.getChildAt(position);
      if (parent != null) {
        treeModel.removeNodeFromParent(currentNode);
      }
    }

    @Override
    public void refreshPageView() {
      Object selectNode = getLastSelectedPathComponent();
      if (selectNode != null && selectNode instanceof IExplorerTreeNode) {
        showPage(((IExplorerTreeNode) selectNode).getPage());
      } else {
        showPage(null);
      }
    }
  }

  private static class ExplorerTreeCellRender extends DefaultTreeCellRenderer {

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
        boolean expanded, boolean leaf, int row, boolean hasFocus) {
      super.getTreeCellRendererComponent(
          tree, value, sel,
          expanded, leaf, row,
          hasFocus);
      setToolTipText(null);

      // Set root font as bold
      if (tree.getModel().getRoot() == value) {
        setFont(tree.getFont().deriveFont(Font.BOLD));
        // set children font as plain
      } else {
        setFont(tree.getFont().deriveFont(Font.PLAIN));
      }

      // Foreground colour
      Color fgcolor;
      if (sel) {
        fgcolor = UIManager.getColor("Tree.selectionForeground");
      } else {
        fgcolor = UIManager.getColor("Tree.foreground");
      }

      setIconTextGap(10);
      if (value instanceof IExplorerTreeNode) {
        IExplorerTreeNode treeNode = (IExplorerTreeNode) value;
        setIcon(treeNode.getNodeIcon());
        setText(treeNode.getNodeName());
        setToolTipText(treeNode.getTooltip());

        Color nodefg = treeNode.getForgroundColor();
        if (nodefg != null) {
          fgcolor = nodefg;
        }

      } else {
        setText(value.toString());
      }

      setForeground(fgcolor);
      return this;
    }

  }


  // ================== Help Methods ===================

  /**
   * Returns a TreePath containing the specified node.
   */
  private static TreePath createNodePath(TreeNode node) {
    ArrayList<TreeNode> list = new ArrayList<TreeNode>();

    // Add all nodes to list
    while (node != null) {
      list.add(node);
      node = node.getParent();
    }
    Collections.reverse(list);

    // Convert array of nodes to TreePath
    return new TreePath(list.toArray());
  }

  /**
   * Given a parent viewable node, try to find one of its children whose
   * userObject is equivalent to the specified one.
   */
  private static IExplorerTreeNode findNodeByNodeData(Object userObject, TreeNode parent) {
    IExplorerTreeNode result = null;
    if (parent.getChildCount() > 0 && userObject != null) {
      for (int index = 0; index < parent.getChildCount(); index++) {
        TreeNode child = parent.getChildAt(index);
        if (child instanceof IExplorerTreeNode) {
          IExplorerTreeNode node = (IExplorerTreeNode) child;
          Page page = node.getPage();
          // this child matches
          if (page != null) {
            if (page.getData() == userObject) {
              result = node;
            } 
          }
          // Traverse children
          if (result == null) {
            if (node.getChildCount() > 0) {
              IExplorerTreeNode childchild = findNodeByNodeData(userObject, node);
              // child's child matches
              if (childchild != null) {
                result = childchild;
              }
            }
          }
        }
      }
    }
    return result;
  }
}
