/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.navigation;

import java.awt.Color;

import javax.swing.Icon;
import javax.swing.JPopupMenu;
import javax.swing.tree.MutableTreeNode;

import com.lucy.g3.gui.framework.page.Page;

/**
 * Extended {@linkplain MutableTreeNode} used by {@linkplain IExplorerTree}.
 */
public interface IExplorerTreeNode extends MutableTreeNode {

  /**
   * Gets the node name.
   *
   * @return a non-null string which is used on {@linkplain IExplorerTree}.
   */
  String getNodeName();

  Icon getNodeIcon();

  Color getForgroundColor();

  /**
   * Gets the tool-tip content of this node.
   *
   * @return plain string or HTML string. <code>Null</code> if this node doesn't
   *         have tool-tip content.
   */
  String getTooltip();

  JPopupMenu getContextMenu();

  Page getPage();

  void removeAllChildren();

  /**
   * Add a child node to this node.
   *
   * @param explorerTreeNode
   *          child node.
   */
  void add(IExplorerTreeNode explorerTreeNode);

}
