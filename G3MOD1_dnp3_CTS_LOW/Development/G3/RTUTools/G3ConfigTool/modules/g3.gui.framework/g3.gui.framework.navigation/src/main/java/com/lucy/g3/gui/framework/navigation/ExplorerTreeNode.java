/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.navigation;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.tree.DefaultMutableTreeNode;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.itemlist.IItemListManager;

/**
 * The implementation of {@linkplain IExplorerTreeNode}.
 */
public class ExplorerTreeNode extends DefaultMutableTreeNode implements IExplorerTreeNode {
  private static final Color TREE_NODE_COLOR_ERROR = Color.RED;
  private static final Color TREE_NODE_COLOR_DISABLED = Color.GRAY;
  
  private final Page page;

  private final IExplorerTree tree;

  private RemoveAction removeAction;


  /**
   * Construct a tree node with icon and text.
   *
   * @param displayText
   *          the text of this node
   * @param imageIcon
   *          the icon of this node
   * @param userObject
   *          user object of this node
   */
  public ExplorerTreeNode(IExplorerTree tree, Page page) {
    super(page == null ? "Unknown" : page.getNodeName());
    this.tree = Preconditions.checkNotNull(tree, "tree is null");

    this.page = page;

    if (page != null) {
      page.addPropertyChangeListener(new PagePCL());

      // Load children node from children data
      ListModel<?> childrenData = page.getChildrenDataList();
      if (childrenData != null) {
        for (int i = 0, size = childrenData.getSize(); i < size; i++) {
          insertChild(childrenData.getElementAt(i), i, false);
        }

        // Observe child data change to refresh children nodes.
        childrenData.addListDataListener(new ChildrenDataListListener());
      }

      // Load children node from children pages
      Page[] childrenPages = page.getChildrenPages();
      for (int i = 0; i < childrenPages.length; i++) {
        ExplorerTreeNode childNode = new ExplorerTreeNode(tree, childrenPages[i]);
        tree.appendChildNode(childNode, this, false);
      }

    }
  }

  private void insertChild(Object childData, int position, boolean shouldBeVisible) {
    ExplorerTreeNode childNode;
    childNode = new ExplorerTreeNode(tree, tree.getPage(childData));
    tree.insertChildNode(childNode, this, position, shouldBeVisible);
  }

  private void removeChild(int position) {
    tree.removeChildNode(this, position);
  }

  @Override
  public Page getPage() {
    return page;
  }


  @Override
  public String getNodeName() {
    String nodename = page == null ? null : page.getNodeName();

    if (Strings.isBlank(nodename)) {
      nodename = "Undefined";
    }

    if (page != null) {
      /* Append the number of children */
      ListModel<?> chilist = page.getChildrenDataList();
      if (chilist != null) {
        int size = chilist.getSize();
        if (size > 1) {
          nodename += " [";
          nodename += size;
          nodename += "]";
        }

      }
    }

    return nodename;
  }

  @Override
  public Icon getNodeIcon() {
    return page == null ? null : page.getNodeIcon();
  }

  @Override
  public Color getForgroundColor() {
 // Get error colour
    if (page.hasError()) 
      return TREE_NODE_COLOR_ERROR;
    
    // Get disable colour
    Object data = page.getData();
    if (data != null) {
      try {
        Method isEnabledMethod = data.getClass().getMethod("isEnabled");
        boolean isEnabled = (boolean) isEnabledMethod.invoke(data);
        if (isEnabled == false)
          return TREE_NODE_COLOR_DISABLED;
      } catch (Throwable e) {
        // Nothing to do
      } 
    }
    
    return null;
  }

  @Override
  public String getTooltip() {
    return page != null ? page.getContextTips() : null;
  }

  @Override
  public JPopupMenu getContextMenu() {
    JPopupMenu contextMenu = new JPopupMenu();

    /* Add context actions in page*/
    if (page != null) {
      Action[] actions = page.getContextActions();
      if (actions != null) {
        for (int i = 0; i < actions.length; i++) {
          if (actions[i] != null) {
            contextMenu.add(actions[i]);
          } 
        }
      }
      
      // Add "enable/disable" button
      Action toggleEnableAction = page.getToggleEnableAction();
      if (toggleEnableAction != null) {
        contextMenu.add(toggleEnableAction);
      }
    }

    // Add "remove" action 
    Action action = getRemoveAction();
    if (action != null) {
      contextMenu.add(action);
    }
    
    return contextMenu.getComponentCount() > 0 ? contextMenu : null;
  }

  @SuppressWarnings("unchecked")
  protected Action getRemoveAction()  {
    if (removeAction == null) {
      IItemListManager<Object> manager = null;
      ExplorerTreeNode parent = (ExplorerTreeNode) getParent();
      if (parent != null) {
        if (parent.page != null) {
          Object data = parent.page.getData();
          if (data != null && data instanceof IItemListManager) {
            manager = (IItemListManager<Object>) data;
          }
        }
      }

      removeAction = new RemoveAction(manager, page);
    }
    
    return removeAction.isValid() ? removeAction : null;
  }


  private static class RemoveAction extends AbstractAction {

    private final IItemListManager<Object> manager;
    private final Page page;

    public RemoveAction(IItemListManager<Object> manager, Page page) {
      super("Remove");
      this.manager = manager;
      this.page = page;
      if (manager != null) {
        try {
          setEnabled(manager.allowToRemove(page.getData()));
        } catch (Throwable e) {
          setEnabled(false);
        }

      }
    }

    @Override
    public Object getValue(String key) {
      if (Action.NAME.equals(key)) {
        return "Remove \"" + page.getNodeName() + "\"";
      }

      return super.getValue(key);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (manager != null) {

        int option = JOptionPane.showConfirmDialog(WindowUtils.getMainFrame(),
            String.format("Are you sure you want to remove \"%s\"? ", page.getNodeName()),
            "Warning",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.WARNING_MESSAGE);

        if (option == JOptionPane.YES_OPTION) {
          manager.remove(page.getData());
        }
      }
    }

    boolean isValid() {
      return manager != null;
    }
  }

  /**
   * PCL for notifying tree this node has changed.
   */
  private class PagePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      tree.notifyNodeChanged(ExplorerTreeNode.this);
    }

  }

  private class ChildrenDataListListener implements ListDataListener {

    @Override
    public void intervalAdded(ListDataEvent e) {
      for (int i = e.getIndex0(); i <= e.getIndex1(); i++) {
        @SuppressWarnings("rawtypes")
        ListModel list = (ListModel) e.getSource();
        insertChild(list.getElementAt(i), i, true);
      }

      // Notify tree to update node name to reflect the change of children
      // number.
      tree.notifyNodeChanged(ExplorerTreeNode.this);
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      for (int i = e.getIndex0(); i <= e.getIndex1(); i++) {
        removeChild(i);
      }

      tree.refreshPageView();

      // Notify tree to update node name to reflect the change of children
      // number.
      tree.notifyNodeChanged(ExplorerTreeNode.this);
    }

    // Update modules tree node text when modules's name changed.
    @Override
    public void contentsChanged(ListDataEvent e) {
      tree.notifyNodeChanged(ExplorerTreeNode.this);
    }

  }


  @Override
  public void add(IExplorerTreeNode explorerTreeNode) {
    super.add(explorerTreeNode);
  }

}
