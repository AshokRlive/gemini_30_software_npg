/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.navigation;

import com.lucy.g3.gui.framework.page.Page;

/**
 * Interface of the tree used for exploring.
 */
public interface IExplorerTree {

  Page getPage(Object data);

  /**
   * Notifies this tree that a node has changed and tree needs repaint.
   */
  void notifyNodeChanged(IExplorerTreeNode node);

  void insertChildNode(IExplorerTreeNode childNode, IExplorerTreeNode parent,
      int position, boolean shouldBeVisible);

  void appendChildNode(IExplorerTreeNode childNode, IExplorerTreeNode parent,
      boolean shouldBeVisible);

  void removeChildNode(IExplorerTreeNode parent, int position);

  /**
   * Refresh the page view based on currently selected tree node.
   */
  void refreshPageView();

}
