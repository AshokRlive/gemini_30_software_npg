/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.common.widgets;

import java.awt.Color;


/**
 */
public final class UIThemeResources {


  // ----------------------[ Colour]--------------------------

  public static final Color COLOUR_LIGHT_BLUE = new Color(51, 153, 255);

  public static final Color COLOUR_LOGIC_BLOCK_BORDER_COLOR = new Color(0, 69, 138);

  public static final Color COLOUR_LOGIC_BLOCK_BG_COLOR = new Color(155, 181, 210);

  public static final Color COLOUR_COMMON_BACKGROUND = Color.white;

  public static final Color COLOUR_BLUE_BG_OVER = new Color(224, 232, 246);

  public static final Color COLOUR_BLUE_BG_SELECT = new Color(193, 210, 238);

  public static final Color COLOUR_BLUE_BORDER_OVER = new Color(152, 180, 226);

  public static final Color COLOUR_BLUE_BORDER_SELECT = Color.gray;

  public static final Color COLOUR_ERROR_BACKGROUND = new Color(255, 215, 215);
  
  public static final Color COLOUR_TABLE_ROLLOVER = Color.LIGHT_GRAY;

  private UIThemeResources() {
  }
}
