/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.widgets;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.plaf.SplitPaneUI;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 * A customised split pane which is more slim than the standard one.
 */
public class SlimSplit extends JSplitPane {

  public static final String PROPERTYNAME_DIVIDER_BORDER_VISIBLE = "dividerBorderVisible";

  private boolean dividerBorderVisible;


  // Instance Creation *****************************************************

  public SlimSplit() {
    this(JSplitPane.HORIZONTAL_SPLIT, false, new JButton(UIManager
        .getString("SplitPane.leftButtonText")), new JButton(
        UIManager.getString("SplitPane.rightButtonText")));
  }

  public SlimSplit(int newOrientation) {
    this(newOrientation, false);
  }

  public SlimSplit(int newOrientation, boolean newContinuousLayout) {
    this(newOrientation, newContinuousLayout, null, null);
  }

  public SlimSplit(int orientation, Component leftComponent,
      Component rightComponent) {
    this(orientation, false, leftComponent, rightComponent);
  }

  public SlimSplit(int orientation, boolean continuousLayout,
      Component leftComponent, Component rightComponent) {
    super(orientation, continuousLayout, leftComponent,
        rightComponent);
    dividerBorderVisible = false;
  }

  public static SlimSplit createStrippedSplitPane(int orientation,
      Component leftComponent, Component rightComponent) {
    SlimSplit split = new SlimSplit(orientation,
        leftComponent, rightComponent);
    split.setBorder(BorderFactory.createEmptyBorder());
    split.setOneTouchExpandable(false);
    return split;
  }

  // Accessing Properties **************************************************

  public boolean isDividerBorderVisible() {
    return dividerBorderVisible;
  }

  public void setDividerBorderVisible(boolean newVisibility) {
    boolean oldVisibility = isDividerBorderVisible();
    if (oldVisibility == newVisibility) {
      return;
    }
    dividerBorderVisible = newVisibility;
    firePropertyChange(PROPERTYNAME_DIVIDER_BORDER_VISIBLE,
        oldVisibility, newVisibility);
  }

  @Override
  public void updateUI() {
    super.updateUI();
    if (!isDividerBorderVisible()) {
      setEmptyDividerBorder();
    }
  }

  private void setEmptyDividerBorder() {
    SplitPaneUI splitPaneUI = getUI();
    if (splitPaneUI instanceof BasicSplitPaneUI) {
      BasicSplitPaneUI basicUI = (BasicSplitPaneUI) splitPaneUI;
      basicUI.getDivider().setBorder(
          BorderFactory.createEmptyBorder());
    }
  }

}
