/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowBanner;
import com.lucy.g3.help.Helper;
public class WorkbenchWindowBanner extends JPanel implements IWorkbenchWindowBanner{

  public WorkbenchWindowBanner() {
    initComponents();
    initActions();
  }
  
  private void initActions() {
    WorkbenchWindowActions actions = WorkbenchWindowActions.getInstance();
    btnExit.setAction(actions.getAction(WorkbenchWindowActions.ACTION_QUIT));
    
    // Help Button
    Helper.register(btnHelp);
    btnHelp.setText(null);
    btnHelp.setIcon(Helper.getIcon());
  }

  @Override
  public JComponent getComponent(){
    return this;
  }
  
  @Override
  public void setInfoComponent(JComponent infoComponent){
    infoPanel.removeAll();
    
    if(infoComponent != null) {
      infoComponent.setOpaque(false);
      infoPanel.add(infoComponent, BorderLayout.CENTER);
    }
  }
  
  @Override
  public void setToolbarComponents(JComponent[] toolbarItems){
    toolBar.removeAll();
    
    if(toolbarItems != null) {
      for (int i = 0; i < toolbarItems.length; i++) {
        if(toolbarItems[i] != null) {
          toolbarItems[i].setOpaque(false);
          toolbarItems[i].setFocusable(false);
          toolbarItems[i].setBorder(new EmptyBorder(5, 5, 5, 5));
          
          toolBar.add(toolbarItems[i]);
        }
      }
    }
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    JLabel bannerImage = new JLabel();
    infoPanel = new JPanel();
    JLabel logoImage = new JLabel();
    panel2 = new JPanel();
    toolBar = new JToolBar();
    toolBarDefault = new JToolBar();
    btnExit = new JButton();
    separator4 = new JToolBar.Separator();
    btnHelp = new JButton();

    //======== this ========
    setBackground(Color.white);
    setBorder(null);
    setLayout(new FormLayout(
        "default, 20px, default:grow, $lcgap, default",
        "fill:default, $nlgap, fill:pref"));

    //---- bannerImage ----
    bannerImage.setIcon(new ImageIcon(getClass().getResource("/images/title.png")));
    add(bannerImage, CC.xywh(1, 1, 1, 3));

    //======== infoPanel ========
    {
      infoPanel.setMinimumSize(new Dimension(100, 44));
      infoPanel.setOpaque(false);
      infoPanel.setLayout(new BorderLayout());
    }
    add(infoPanel, CC.xy(3, 1));

    //---- logoImage ----
    logoImage.setIcon(new ImageIcon(getClass().getResource("/images/lucy.png")));
    add(logoImage, CC.xy(5, 1));

    //======== panel2 ========
    {
      panel2.setOpaque(false);
      panel2.setLayout(new FormLayout(
          "71dlu:grow, default",
          "fill:pref"));

      //======== toolBar ========
      {
        toolBar.setRollover(true);
        toolBar.setFloatable(false);
        toolBar.setBorderPainted(false);
        toolBar.setOpaque(false);
      }
      panel2.add(toolBar, CC.xy(1, 1));

      //======== toolBarDefault ========
      {
        toolBarDefault.setRollover(true);
        toolBarDefault.setFloatable(false);
        toolBarDefault.setBorderPainted(false);
        toolBarDefault.setOpaque(false);

        //---- btnExit ----
        btnExit.setText("Exit");
        btnExit.setOpaque(false);
        btnExit.setFocusable(false);
        btnExit.setBorder(new EmptyBorder(5, 5, 5, 5));
        toolBarDefault.add(btnExit);
        toolBarDefault.add(separator4);
        separator4.setSeparatorSize(new Dimension(4, 20));
        separator4.setOrientation(toolBarDefault.getOrientation() == JToolBar.VERTICAL
            ? JSeparator.HORIZONTAL : JSeparator.VERTICAL);

        //---- btnHelp ----
        btnHelp.setOpaque(false);
        btnHelp.setFocusable(false);
        btnHelp.setBorder(new EmptyBorder(5, 5, 5, 5));
        btnHelp.setText("?");
        toolBarDefault.add(btnHelp);
      }
      panel2.add(toolBarDefault, CC.xy(2, 1));
    }
    add(panel2, CC.xywh(3, 3, 3, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel infoPanel;
  private JPanel panel2;
  private JToolBar toolBar;
  private JToolBar toolBarDefault;
  private JButton btnExit;
  private JToolBar.Separator separator4;
  private JButton btnHelp;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
