/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;
import org.jdesktop.application.TaskService;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * A task monitor window shows all background running tasks and their progress.
 */
public class TaskMonitorWindow extends JDialog {

  private final TaskMonitorPCL monitorPCL = new TaskMonitorPCL();
  private final TaskPCL taskPCL = new TaskPCL();
  private final TaskMonitor monitor;

  // ====== Data Model ======
  private final DefaultListModel<Task<?, ?>> taskListModel = new DefaultListModel<Task<?, ?>>();
  private final ArrayListModel<Task<?, ?>> taskHistory = new ArrayListModel<Task<?, ?>>();

  // ====== History Dialog ======
  private JDialog historyDialog;


  public TaskMonitorWindow(Frame parent, TaskMonitor taskMonitor) {
    super(parent, false);
    this.monitor = Preconditions.checkNotNull(taskMonitor, "Task monitor must not be null");

    @SuppressWarnings("rawtypes")
    List<Task> tasks = monitor.getTasks();
    for (Task<?, ?> t : tasks) {
      taskListModel.addElement(t);
      taskHistory.add(t);
      t.addPropertyChangeListener(taskPCL);
    }

    initComponents();
    initEventHandling();

    setForgroundTask(monitor.getForegroundTask());

  }

  private void initEventHandling() {
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        close();
      }

    });
    monitor.addPropertyChangeListener(monitorPCL);

    taskList.addListSelectionListener(new TaskListSelectionHandler());
  }

  private void setForgroundTask(Task<?,?> t) {
    if (t != null) {
      String title = t.getTitle();
      if (title == null || title.isEmpty()) {
        title = t.getClass().getSimpleName();
      }
      lblForground.setText(title);
    } else {
      lblForground.setText("");
    }
  }

  private void close() {
    monitor.removePropertyChangeListener(monitorPCL);
    dispose();

    if (historyDialog != null) {
      historyDialog.dispose();
    }
  }

  private void createUIComponents() {
    taskList =  new JList<Task<?, ?>>(taskListModel);
    taskList.setCellRenderer(new TaskListRenderer());
  }

  @SuppressWarnings("unchecked")
  private ListModel<Task<?, ?>> getTaskHisotryListModel() {
    return taskHistory;
  }
  
  private void showHistory() {
    if (historyDialog == null) {
      historyDialog = new JDialog(this, false);
      historyDialog.setTitle("History Tasks");
      historyDialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
      historyDialog.setPreferredSize(new Dimension(600, 300));
      JTable taskTable = new JTable(new HistoryTableAdapter(getTaskHisotryListModel()));

      // Set column widths
      TableColumn column;
      for (int i = 0, count = taskTable.getColumnCount(); i < count; i++) {
        column = taskTable.getColumnModel().getColumn(i);
        if (i == 0 || i == 3) {
          column.setMaxWidth(100); // third column is bigger
        }
      }

      historyDialog.setLocationRelativeTo(this);
      historyDialog.setContentPane(new JScrollPane(taskTable));
      historyDialog.pack();

    }

    historyDialog.setVisible(true);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    lblForground = new JLabel();
    scrollPane1 = new JScrollPane();
    lblDescription = new JTextArea();
    buttonBar = new JPanel();
    btnHistory = new JButton();
    btnClose = new JButton();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setName("taskMonitorWindow");
    setTitle("Task Monitor");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "default:grow",
            "fill:default, $rgap, fill:default:grow, $lgap, fill:default"));

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(" Forground Task"));
          panel1.setLayout(new FormLayout(
              "default, $rgap, default:grow",
              "fill:[12dlu,default]"));

          //---- lblForground ----
          lblForground.setText(" ");
          panel1.add(lblForground, CC.xy(3, 1));
        }
        contentPanel.add(panel1, CC.xy(1, 1));

        //======== scrollPane1 ========
        {
          scrollPane1.setViewportBorder(new TitledBorder("Background Task List"));
          scrollPane1.setBorder(null);
          scrollPane1.setViewportView(taskList);
        }
        contentPanel.add(scrollPane1, CC.xy(1, 3));

        //---- lblDescription ----
        lblDescription.setEditable(false);
        lblDescription.setOpaque(false);
        lblDescription.setLineWrap(true);
        contentPanel.add(lblDescription, CC.xy(1, 5));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "2*($lcgap, default), $glue, $button",
            "pref"));

        //---- btnHistory ----
        btnHistory.setText("History");
        btnHistory.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            showHistory();
          }
        });
        buttonBar.add(btnHistory, CC.xy(2, 1));

        //---- btnClose ----
        btnClose.setText("Close");
        btnClose.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            close();
          }
        });
        buttonBar.add(btnClose, CC.xy(6, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    setSize(555, 360);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // Set font of text area
    lblDescription.setFont(UIManager.getFont("Label.font"));
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel lblForground;
  private JScrollPane scrollPane1;
  private JList<Task<?, ?>> taskList;
  private JTextArea lblDescription;
  private JPanel buttonBar;
  private JButton btnHistory;
  private JButton btnClose;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class TaskMonitorPCL implements PropertyChangeListener {

    @SuppressWarnings("unchecked")
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      String name = evt.getPropertyName();

      if ("tasks".equals(name)) {
        List<Task<?,?>> newTasks = (List<Task<?,?>>) evt.getNewValue();
        Object selected = taskList.getSelectedValue();

        for (int i = 0, size = taskListModel.size(); i < size; i++) {
          ((Task<?,?>) taskListModel.get(i)).removePropertyChangeListener(taskPCL);
        }
        taskListModel.clear();
        for (Task<?,?> t : newTasks) {
          taskListModel.addElement(t);
          if (!taskHistory.contains(t)) {
            taskHistory.add(t);
          }
          t.addPropertyChangeListener(taskPCL);
        }

        taskList.setSelectedValue(selected, true);
      
      } else if ("foregroundTask".equals(name)) {
        setForgroundTask((Task<?,?>) evt.getNewValue());
      }
    }
  }

  private class TaskPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if ("progress".equals(evt.getPropertyName())
          || "message".equals(evt.getPropertyName())) {
        taskList.repaint();
      }
    }
  }

  private class TaskListRenderer implements ListCellRenderer<Task<?,?>> {

    private JProgressBar pbar = new JProgressBar();
    private JLabel label = new JLabel();
    private JPanel container = new JPanel(new BorderLayout());

    private Color unselectColor;
    private Color selectColor;


    public TaskListRenderer() {
      container.add(pbar, BorderLayout.CENTER);
      container.add(label, BorderLayout.NORTH);
      container.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
      unselectColor = pbar.getForeground();
      selectColor = Color.blue;

      pbar.setStringPainted(true);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Task<?, ?>> list, Task<?,?> value, int index,
        boolean isSelected, boolean cellHasFocus) {
      if (value != null) {
        String text = value.getTitle();
        if (text == null || text.isEmpty()) {
          text = value.getClass().getSimpleName();
        }

        String msg = value.getMessage();
        if (msg != null && !msg.isEmpty()) {
          text = text + " - " + msg;
        }

        TaskService service = value.getTaskService();
        if (service != null) {
          text = "[" + service.getName() + "]  " + text;
        }

        label.setText(text);
        label.setToolTipText(value.getDescription());

        pbar.setValue(value.getProgress());
        pbar.setForeground(isSelected ? selectColor : unselectColor);
      }

      return container;
    }

  }

  private class TaskListSelectionHandler implements ListSelectionListener {

    @Override
    public void valueChanged(ListSelectionEvent e) {
      if (!e.getValueIsAdjusting()) {
        Task<?,?> task = taskList.getSelectedValue();
        String description;

        if (task != null) {
          description = task.getDescription();
          if (description == null || description.isEmpty()) {
            description = "No description";
          }
        } else {
          description = "";
        }
        lblDescription.setText(description);
      }
    }
  }

  private static class HistoryTableAdapter extends AbstractTableAdapter<Task<?, ?>> {

    private static String[] COLUM_NAMES = { "Index", "Title", "Description", "Duration(ms)" };


    public HistoryTableAdapter(ListModel<Task<?, ?>> model) {
      super(model);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Task<?, ?> task = getRow(rowIndex);

      if (task == null) {
        return "n/a";
      }

      switch (columnIndex) {
      case 0:
        return rowIndex;
      case 1:
        return task.getTitle();
      case 2:
        return task.getDescription();
      case 3:
        return task.getExecutionDuration(TimeUnit.MILLISECONDS);

      default:
        break;
      }

      return "n/a";
    }

    @Override
    public int getColumnCount() {
      return COLUM_NAMES.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
      return COLUM_NAMES[columnIndex];
    }

  }

  
}
