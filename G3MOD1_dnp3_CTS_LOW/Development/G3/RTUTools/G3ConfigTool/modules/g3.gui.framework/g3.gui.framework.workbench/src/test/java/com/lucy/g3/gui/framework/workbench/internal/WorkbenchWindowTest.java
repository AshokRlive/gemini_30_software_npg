/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jdesktop.application.Application;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindow;


public class WorkbenchWindowTest extends Application {
   public static void main(String[] args) {
     Application.getInstance(WorkbenchWindowTest.class);
     
     
     WorkbenchWindow window = new WorkbenchWindow();
     
     JPanel rootPanel = new JPanel(new BorderLayout());
     rootPanel.add(window.getMenuBar(), BorderLayout.NORTH);
     rootPanel.add(window.getStatusBar().getComponent(), BorderLayout.SOUTH);
     
     JPanel contentPanel = new JPanel(new BorderLayout());    
     contentPanel.add(window.getBanner().getComponent(), BorderLayout.NORTH);
     contentPanel.add(window.getContentComponent(), BorderLayout.CENTER);
     
     rootPanel.add(contentPanel, BorderLayout.CENTER);

     
     JFrame frame = new JFrame("Test");
     frame.setContentPane(rootPanel);
     WindowUtils.showFrame(frame);
  }

  @Override
  protected void startup() {
  }
   
}

