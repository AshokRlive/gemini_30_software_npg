/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;

import com.lucy.g3.gui.framework.page.IPageViewer;



public abstract class WorkbenchWindowTab implements IWorkbenchWindowTab {
  private final String identifier;
  
  private boolean tabSelected;
  private ImageIcon tabIcon;
  private String tabTitle;
  private String tabTips;
  private IPageViewer viewer;
  
  private final JPanel tabPanel;
  
  private ResourceMap resource = Application.getInstance().getContext().getResourceMap(getClass());

  public WorkbenchWindowTab(String identifier) {
    this.identifier = identifier;
    setTabTitle(resource.getString("tab.title"));
    setTabIcon(resource.getImageIcon("tab.icon"));
    
    tabPanel = new JPanel(new BorderLayout());
    tabPanel.setOpaque(false);
  }
  
  @Override
  public String getIdentifier() {
    return identifier;
  }
  
  @Override
  public String getTabTitle() {
    return tabTitle;
  }

  @Override
  public String getTabTips() {
    return tabTips;
  }

  @Override
  public ImageIcon getTabIcon() {
    return tabIcon;
  }
  
  protected void setTabIcon(ImageIcon tabIcon) {
    this.tabIcon = tabIcon;
  }

  protected void setTabTitle(String tabTitle) {
    this.tabTitle = tabTitle;
  }
  
  protected void setTabTips(String tabTips) {
    this.tabTips = tabTips;
  }

  @Override
  public JPanel getComponent() {
    return tabPanel;
  }

  @Override
  public void setTabSelected(boolean isSelected) {
    this.tabSelected = isSelected;
    tabSelectedActionPerform(isSelected);    
  }
  
  @Override
  public boolean isTabSelected(){
    return tabSelected;
  }
  
  protected abstract void tabSelectedActionPerform(boolean selected);

  @Override
  public IPageViewer getPageViewer() {
    if(viewer == null) {
      viewer = new DefaultPageViewer();
    }
    return viewer;
  }


  @Override
  public void saveSession() {
    // Do nothing by default
  }

}

