/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.widgets;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.SwingConstants;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonUI;

import com.l2fprod.common.swing.plaf.basic.BasicButtonBarUI;
import com.lucy.g3.gui.common.widgets.UIThemeResources;

/**
 * BlueishButtonBarUI. <br>
 */
public class TransparentButtonBarUI extends BasicButtonBarUI {

  private static TransparentButtonBarUI instance;
  private static BarButtonUI barButtonUI;


  public static ComponentUI createUI(JComponent c) {
    if (instance == null) {
      instance = new TransparentButtonBarUI();
    }
    return instance;
  }

  @Override
  protected void installDefaults() {
    bar.setBorder(null);
    bar.setOpaque(false);
  }

  @Override
  public void installButtonBarUI(AbstractButton button) {
    if (barButtonUI == null) {
      barButtonUI = new BarButtonUI();
    }
    button.setUI(barButtonUI);
    button.setHorizontalTextPosition(SwingConstants.CENTER);
    button.setVerticalTextPosition(SwingConstants.BOTTOM);
    button.setOpaque(false);
  }


  /**
   * Bar button UI.
   */
  static class BarButtonUI
      extends BasicButtonUI {

    public BarButtonUI() {
      super();
    }

    @Override
    public void installUI(JComponent c) {
      super.installUI(c);

      AbstractButton button = (AbstractButton) c;
      button.setRolloverEnabled(true);
      button.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
    }

    @Override
    public void paint(Graphics g, JComponent c) {
      AbstractButton button = (AbstractButton) c;
      if (button.getModel().isRollover()
          || button.getModel().isArmed()
          || button.getModel().isSelected()) {
        Color oldColor = g.getColor();
        if (button.getModel().isSelected()) {
          g.setColor(UIThemeResources.COLOUR_BLUE_BG_SELECT);
        } else {
          g.setColor(UIThemeResources.COLOUR_BLUE_BG_OVER);
        }
        // g.fillRect(0, 0, c.getWidth() - 1, c.getHeight() - 1);
        g.fillRoundRect(0, 0, c.getWidth() - 1, c.getHeight() - 1, 8, 8);

        if (button.getModel().isSelected()) {
          g.setColor(UIThemeResources.COLOUR_BLUE_BORDER_SELECT);
        } else {
          g.setColor(UIThemeResources.COLOUR_BLUE_BORDER_OVER);
        }
        g.drawRoundRect(0, 0, c.getWidth() - 1, c.getHeight() - 1, 5, 5);

        g.setColor(oldColor);
      }

      super.paint(g, c);

    }
  }
}
