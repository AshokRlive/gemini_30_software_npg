/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;

import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;


public class TaskMonitorManager {
  public static final String ACTION_KEY_SHOW_TASKMONITOR = "showTaskMonitor";
  
  private TaskMonitorWindow taskMonitorWindow;
  
  private TaskMonitorManager(){
  }
  private static TaskMonitorManager INSTANCE;
  
  public static TaskMonitorManager getInstance() {
    if(INSTANCE == null) {
      INSTANCE = new TaskMonitorManager();
    }
    
    return INSTANCE;
  }
  
  public final javax.swing.Action getAction(String actionKey) {
    return getApp().getContext().getActionMap(this).get(actionKey);
  }
  
  /**
   * Action method. Shows the background running tasks.
   */
  @Action
  public void showTaskMonitor() {
    if (taskMonitorWindow == null || !taskMonitorWindow.isDisplayable()) {
      taskMonitorWindow = new TaskMonitorWindow(WindowUtils.getMainFrame(), getApp().getContext()
          .getTaskMonitor());
    }

    DialogUtils.showWindow(taskMonitorWindow);
  }

  private Application getApp() {
    return Application.getInstance();
  }

}

