/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.widgets;

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JSplitPane;

import com.lucy.g3.gui.framework.page.IPageViewer;
import com.lucy.g3.gui.framework.page.Page;

/**
 * Vertical split pane that wraps a view container and another component at the
 * bottom.
 */
public class SplitViewerContainer extends SlimSplit implements IPageViewer {

  private IPageViewer viewer;


  public SplitViewerContainer(IPageViewer c1, Component c2) {
    super(JSplitPane.VERTICAL_SPLIT, true, c1.getComponent(), c2);
    viewer = c1;
    setName("splitViewerWrapper");
    setBorder(BorderFactory.createEmptyBorder());
    setDividerLocation(0.7D);
    setResizeWeight(0.7F);
    setOneTouchExpandable(true);
  }

  @Override
  public void show(Page page) {
    viewer.show(page);
  }

  @Override
  public Page getShowedPage() {
    return viewer.getShowedPage();
  }

  @Override
  public void clear() {
    viewer.clear();
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

}
