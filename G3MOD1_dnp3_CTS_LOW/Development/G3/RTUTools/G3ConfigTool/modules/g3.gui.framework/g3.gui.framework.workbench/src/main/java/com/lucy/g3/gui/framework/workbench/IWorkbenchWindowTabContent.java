/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench;

import javax.swing.JComponent;

public interface IWorkbenchWindowTabContent {

  JComponent getComponent();

  void setTabs(IWorkbenchWindowTab[] tabs);
  
  IWorkbenchWindowTab[] getTabs();

  void addTab(int index, IWorkbenchWindowTab tab);

  void removeTab(int index);

  void selectTab(int index);
  
  int findTabIndex(String identifier);
}
