/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.lucy.g3.gui.framework.page.IPageViewer;


/**
 * Interface of navigation tab that is contained by a navigation pane.
 */
public interface IWorkbenchWindowTab extends ISupportPersistentSession{

  String getIdentifier();
  
  String getTabTitle();

  ImageIcon getTabIcon();

  String getTabTips();
  
  void setTabSelected(boolean isSelected);
  
  boolean isTabSelected();

  JComponent getComponent();
  
  IPageViewer getPageViewer();
}
