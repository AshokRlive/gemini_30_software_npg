
package com.lucy.g3.gui.framework.workbench;


public interface ISupportPersistentSession {
  void saveSession();
}

