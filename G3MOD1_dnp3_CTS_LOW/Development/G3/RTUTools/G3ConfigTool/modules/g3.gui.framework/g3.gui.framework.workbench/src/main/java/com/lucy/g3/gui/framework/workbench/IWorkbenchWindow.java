/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench;

import javax.swing.JComponent;
import javax.swing.JMenuBar;

/**
 *
 */
public interface IWorkbenchWindow extends ISupportPersistentSession{
  IWorkbench getWorkbench();
  
  IWorkbenchStatusBar getStatusBar();
  
  IWorkbenchWindowTabContent getContent();
  JComponent getContentComponent();
  
  IWorkbenchWindowBanner getBanner();
  JMenuBar getMenuBar();
}

