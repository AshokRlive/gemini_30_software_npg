/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import static com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindowActions.ACTION_ABOUT;
import static com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindowActions.ACTION_LOG_DIR;
import static com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindowActions.ACTION_QUIT;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import com.lucy.g3.gui.framework.workbench.IWorkbenchMenuAdvisor;
import com.lucy.g3.help.Helper;

public class WorkbenchMenuAdvisor implements IWorkbenchMenuAdvisor{
  
  @Override
  public JMenu[] createMenus(){
    return new JMenu[] {
        createDefaultFileMenuItems(new JMenu("File")),
        createDefaultHelpMenuItems(new JMenu("Help"))
    };
  }
  
  protected JMenu createDefaultFileMenuItems(JMenu fileMenu){
    fileMenu.add(getActions().getAction(ACTION_QUIT));
    return fileMenu;
  }
  
  protected JMenu createDefaultHelpMenuItems(JMenu helpMenu){
    // Help
    helpMenu.add(createHelpMenuItem());
    helpMenu.addSeparator();

    // View Log
    helpMenu.add(getActions().getAction(ACTION_LOG_DIR));
    helpMenu.addSeparator();

    // About
    helpMenu.add(getActions().getAction(ACTION_ABOUT));

    return helpMenu;
  }
  
  protected JMenuItem createHelpMenuItem(){
    JMenuItem helpBtn = new JMenuItem("Help Contents");
    helpBtn.setAccelerator(KeyStroke.getKeyStroke("F1"));
    Helper.register(helpBtn, "ROOT");
    helpBtn.setIcon(Helper.getIcon());
    return helpBtn;
  }
  
  protected WorkbenchWindowActions getActions() {
    return WorkbenchWindowActions.getInstance();
  }
}

