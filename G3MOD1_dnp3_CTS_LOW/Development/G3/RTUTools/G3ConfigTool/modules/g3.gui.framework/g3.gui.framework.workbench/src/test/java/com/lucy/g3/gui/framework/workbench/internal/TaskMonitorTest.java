/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executors;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.jdesktop.application.Application;
import org.jdesktop.application.TaskMonitor;
import org.jdesktop.application.TaskService;

import com.lucy.g3.gui.framework.workbench.internal.TaskMonitorWindow;


/**
 * The Class TaskMonitorTest.
 */
public class TaskMonitorTest {

  public static void main(String[] args) throws InterruptedException,
      InvocationTargetException {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      UIManager.put("ProgressBar.background", Color.orange);
      UIManager.put("ProgressBar.foreground", Color.blue);
      UIManager.put("ProgressBar.selectionBackground", Color.red);
      UIManager.put("ProgressBar.selectionForeground", Color.green);
    } catch (Exception e) {
    }

    Application app = Application.getInstance();
    final TaskMonitor m = app.getContext().getTaskMonitor();
    TaskService service = new TaskService("TestService",
        Executors.newSingleThreadExecutor());
    app.getContext().addTaskService(service);
    System.out.println(m.getPropertyChangeListeners().length);
    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        new TaskMonitorWindow(null, m).setVisible(true);
      }
    });
    System.out.println(m.getPropertyChangeListeners().length);
  }

}

