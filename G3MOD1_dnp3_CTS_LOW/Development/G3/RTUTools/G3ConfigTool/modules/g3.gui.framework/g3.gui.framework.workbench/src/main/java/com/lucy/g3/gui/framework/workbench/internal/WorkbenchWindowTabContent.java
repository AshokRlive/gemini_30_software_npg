/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;

import com.l2fprod.common.swing.JOutlookBar;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowTab;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowTabContent;
import com.lucy.g3.gui.framework.workbench.widgets.SlimSplit;
import com.lucy.g3.help.Helper;

public class WorkbenchWindowTabContent implements IWorkbenchWindowTabContent{

  private static final String CLIENT_PROPERTY_ID = "tab.identifier";

  private Logger log = Logger.getLogger(WorkbenchWindowTabContent.class);
  
  private JSplitPane  splitPane;
  
  private JOutlookBar tabbedPane;
  private JPanel      cardPanel;
  
  private ArrayList<IWorkbenchWindowTab> tabs = new ArrayList<>();
  private IWorkbenchWindowTab selectedTab;


  public WorkbenchWindowTabContent() {
    initComponents();
  }

  private void initComponents() {
    splitPane = new SlimSplit();
    splitPane.setBorder(null);
    splitPane.setName("WorkbenchWindowContent.splitpane");
    splitPane.setDividerLocation(182);
    
    tabbedPane = new JOutlookBar();
    tabbedPane.setBackground(Color.white);
    tabbedPane.setTabPlacement(SwingConstants.RIGHT);
    tabbedPane.setPreferredSize(new Dimension(182, 80));
    tabbedPane.setMinimumSize(new Dimension(20, 86));
    tabbedPane.setAnimated(false);
    tabbedPane.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        tabbedPaneSelectIndexChanged(tabbedPane.getSelectedIndex());
      }
    });
    Helper.register(tabbedPane, this.getClass()); // TODO FIX HELP KEY
    
    cardPanel = new JPanel(new CardLayout());
    
    splitPane.setLeftComponent(tabbedPane);
    splitPane.setRightComponent(cardPanel);
  }

  /*
   * Perform the action when outlook pane's tab selection changed. It will
   * update the content of the central panel based on the current selected
   * outlook tab.
   */
  private void tabbedPaneSelectIndexChanged(int index) {

    // Swap card
    CardLayout cl = (CardLayout) (cardPanel.getLayout());
    cl.show(cardPanel, tabs.get(index).getIdentifier());

    // previous tab unselected
    if (selectedTab != null) {
      selectedTab.setTabSelected(false);
      selectedTab = null;
    }

    // new tab selected
    int selectedIndex = tabbedPane.getSelectedIndex();
    if(selectedIndex >=0 && selectedIndex < tabs.size()) {
      selectedTab = tabs.get(selectedIndex);
      selectedTab.setTabSelected(true);
    }
  }
  
  @Override
  public JComponent getComponent() {
    return splitPane;
  }

  @Override
  public IWorkbenchWindowTab[] getTabs() {
    return tabs.toArray(new IWorkbenchWindowTab[tabs.size()]);
  }

  @Override
  public void setTabs(IWorkbenchWindowTab[] tabs) {
    this.tabs.clear();
    this.cardPanel.removeAll();
    this.tabbedPane.removeAll();
    
    if(tabs != null) {
      for (int i = 0; i < tabs.length; i++) {
        if(tabs[i] == null)
          continue;
        
        this.tabs.add(tabs[i]);
        this.cardPanel.add(tabs[i].getPageViewer().getComponent(), tabs[i].getIdentifier());
        this.tabbedPane.addTab(
            tabs[i].getTabTitle(),
            tabs[i].getTabIcon(), 
            setID(tabs[i].getComponent(), tabs[i].getIdentifier()),
            tabs[i].getTabTips());
      }
    }
    
    alignTabs();
    selectTab(0);
  }

  private JComponent setID(JComponent component, String id){
    component.putClientProperty(CLIENT_PROPERTY_ID, id);
    return component;
  }
  
  private String getID(JComponent component){
    return (String) component.getClientProperty(CLIENT_PROPERTY_ID);
  }
  
  @Override
  public void addTab(int index, IWorkbenchWindowTab tab) {
    this.tabs.add(index, tab);
    this.cardPanel.add(tab.getPageViewer().getComponent(), tab.getIdentifier());
    this.tabbedPane.insertTab(
        tab.getTabTitle(),
        tab.getTabIcon(), 
        setID(tab.getComponent(), tab.getIdentifier()),tab.getTabTips(), 
        index);
    alignTabs();
  }

  private void alignTabs() {
    tabbedPane.setAllTabsAlignment(SwingConstants.LEADING);
  }

  @Override
  public void removeTab(int index) {
    if(index >= tabs.size() || index <0) {
      log.error("Invalid index : "+index +" expected [0," + tabs.size()+")");
      return;
    }
    
    IWorkbenchWindowTab tab = tabs.get(index);
    this.cardPanel.remove(tab.getPageViewer().getComponent());
    this.tabbedPane.removeTabAt(index);
    this.tabs.remove(tab);
  }

  @Override
  public void selectTab(int index) {
    if(index >=0 && index < tabs.size()) {
      tabbedPane.setSelectedIndex(index);
      tabbedPaneSelectIndexChanged(index);
    }
  }

  @Override
  public int findTabIndex(String identifier) {
    if(identifier == null)
      return -1;
    
    int count = tabbedPane.getTabCount();
    for (int i = 0; i < count; i++) {
      Component comp = tabbedPane.getComponentAt(i);
      if(comp instanceof JComponent && identifier.equals(getID((JComponent) comp))) {
        return i;
      }
    }
    return -1;
  }


}
