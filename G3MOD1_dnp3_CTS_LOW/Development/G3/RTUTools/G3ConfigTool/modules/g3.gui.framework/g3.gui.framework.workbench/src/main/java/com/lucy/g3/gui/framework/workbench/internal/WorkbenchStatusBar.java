/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.ToolTipManager;

import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXStatusBar;
import org.jdesktop.swingx.JXStatusBar.Constraint;

import com.lucy.g3.gui.common.widgets.UIThemeResources;
import com.lucy.g3.gui.common.widgets.animation.Blinker;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.workbench.IWorkbenchStatusBar;

/**
 * Default implementation of {@linkplain IWorkbenchStatusBar}.
 */
public class WorkbenchStatusBar implements IWorkbenchStatusBar{
  private final ImageIcon iconOnline = ResourceUtils.getImg("state.icon.online", WorkbenchStatusBar.class);
  private final ImageIcon iconOffline = ResourceUtils.getImg("state.icon.offline", WorkbenchStatusBar.class);
  private final ImageIcon iconAlert = ResourceUtils.getImg("state.icon.alert", WorkbenchStatusBar.class);
  private final ImageIcon iconSecure = ResourceUtils.getImg("state.icon.secure", WorkbenchStatusBar.class);
  private final ImageIcon iconInsecure = ResourceUtils.getImg("state.icon.insecure", WorkbenchStatusBar.class);
  private final Color HYPERLINK_COLOR = UIThemeResources.COLOUR_LIGHT_BLUE;

  private final javax.swing.Action taskMonitorAction 
    = TaskMonitorManager.getInstance().getAction(TaskMonitorManager.ACTION_KEY_SHOW_TASKMONITOR);
  
  private final JXStatusBar statusBar;
  
  private JXHyperlink connectStatusLabel;
  private JLabel hostLabel;
  private JLabel messageLabel;
  private JProgressBar progressBar;
  private JPopupMenu contextMenu;

  private JLabel modeLabel;
  private Blinker modeLabelBlinker;

  private final Timer messageTimer;

  private Status status = Status.DISCONNECTED;
  private Mode mode= Mode.NONE;
  
  private ActionListener securityAciton;
  private String host;
  
  public WorkbenchStatusBar(){
    statusBar = new JXStatusBar();
    messageTimer = new Timer(5000/*ms*/, new MessageClearingHandler());
    messageTimer.setRepeats(false);
    
    initComponents();
  }
  
  private void initComponents() {
    // Create Message label
    messageLabel = new JLabel();
    statusBar.add(messageLabel, new Constraint(Constraint.ResizeBehavior.FILL));

    // Create Mode label
    modeLabel = new JLabel();
    modeLabel.setVisible(false);
    //modeLabel.setToolTipText("Running mode");
    modeLabel.setHorizontalAlignment(SwingConstants.CENTER);
    statusBar.add(modeLabel, new Constraint(100));
    modeLabelBlinker = new Blinker(modeLabel);
    modeLabelBlinker.setBlinkBg(Color.darkGray);

    // Create Host label
    hostLabel = new JLabel();
    hostLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    hostLabel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if(securityAciton != null && e.getClickCount() > 1) {
          securityAciton.actionPerformed(null);
        }
      }
      
    });
    statusBar.add(hostLabel, new Constraint(160));

    // Create Progress bar
    progressBar = new JProgressBar();
    progressBar.setEnabled(false);
    progressBar.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (UIUtils.isDoubleClick(e)) {
          taskMonitorAction.actionPerformed(new ActionEvent(e.getSource(), e.getID(), ""));
        }
        
        showTips(progressBar);
      }
    });
    statusBar.add(progressBar, new Constraint(130));

    // Create Connection state label
    connectStatusLabel = new JXHyperlink();
    connectStatusLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    connectStatusLabel.setFocusable(false);
    connectStatusLabel.setUnclickedColor(HYPERLINK_COLOR);
    connectStatusLabel.setClickedColor(HYPERLINK_COLOR);
    connectStatusLabel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (e.getSource() == connectStatusLabel) {
          int pointX = 0;
          int pointY = 0 - contextMenu.getHeight();
          contextMenu.show(connectStatusLabel, pointX, pointY);
        }        
      }
    });
    statusBar.add(connectStatusLabel, new Constraint(130));

    // Create Popup menu
    contextMenu = new JPopupMenu();

    /*
     * The TaskMonitor tracks a "foreground" task; Observes foreground task
     * property changes and indicate them on status bar.
     */
    Application.getInstance().getContext().getTaskMonitor().addPropertyChangeListener(new TaskMonitorPCL());
    
    updateGUI();
  }

  @Override
  public JComponent getComponent(){
    return statusBar;
  }
  
  @Override
  public void setStatus(Status status, Mode mode) {
    if(status == null)
      throw new IllegalArgumentException("status must not be null");
    if(mode == null)
      throw new IllegalArgumentException("mode must not be null");
    
    this.status = status;
    this.mode = mode;

    updateGUI();    
  }

  @Override
  public void setContextActions(Action[] contextActions){
    contextMenu.removeAll();
    
    for (Action action : contextActions) {
      if (action != null) {
        contextMenu.add(action);
      } else {
        contextMenu.addSeparator();
      }
    }
  }
  
  private void updateGUI() {
    // Set the content of the connection status label
    connectStatusLabel.setText(status.getName());
    connectStatusLabel.setToolTipText(status.getDescription());
    connectStatusLabel.setIcon(getStateIcon(status));

    // Show host only on CONNECT_OK state
    hostLabel.setVisible(status == Status.CONNECTED);

    // Show the tooltip of status label
    showTips(connectStatusLabel);

    String modeText = mode.getName();

    // Update mode label text & visibility
    modeLabel.setText(modeText);
    modeLabel.setVisible(modeText != null && !modeText.isEmpty());

    // stop blinking in normal mode.
    if (mode == Mode.NORMAL) {
      modeLabelBlinker.stop();
    } else {
      modeLabelBlinker.start();
    }
  }

  private ImageIcon getStateIcon(Status state) {
    if (state == null) {
      return null;
    }
    
    if (state == Status.CONNECTED) {
      return iconOnline;
    } else if (state == Status.CONNECTION_LOST) {
      return iconAlert;
    } else if (state == Status.DISCONNECTED) {
      return iconOffline;
    } else {
      return null;
    }
  }

  @Override
  public void setHost(String host) {
    this.host = host;
    hostLabel.setText(String.format("Host: %s", host));
  }
  
  @Override
  public String getHost() {
    return host;
  }

  @Override
  public void setSecurityLabel(boolean isSecure, String tips){
    hostLabel.setIcon(isSecure ? iconSecure : iconInsecure);
    hostLabel.setToolTipText(tips);
  }
  
  @Override
  public void setSecurityActionListener(ActionListener action) {
    this.securityAciton = action;
  }

  
  private void showMessage(String s) {
    messageLabel.setText((s == null) ? "" : s);
    messageTimer.restart();
  }

  /*
   * Show the tooltip of a component.
   */
  private void showTips(JComponent comp) {
    if (comp == null) {
      return;
    }

    MouseEvent phantom = new MouseEvent(
        comp,
        MouseEvent.MOUSE_MOVED,
        System.currentTimeMillis(),
        0,
        10,
        10,
        0,
        false);

    ToolTipManager.sharedInstance().mouseMoved(phantom);
  }


  private class MessageClearingHandler implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      messageLabel.setText("");
    }
  }

  private class TaskMonitorPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent e) {
      String propertyName = e.getPropertyName();

      // Debug
      // Logger log = Logger.getLogger(TaskMonitorPCL.class);
      // String src = e.getSource().toString().getClass().getSimpleName();
      // log.debug(src+" property: \""+e.getPropertyName()+
      // "\" changed from: "+e.getOldValue()
      // +" to : "+e.getNewValue());

      if ("message".equals(propertyName)) {
        String text = (String) (e.getNewValue());
        showMessage(text);
      } else if ("started".equals(propertyName)) {
        progressBar.setEnabled(true);
        progressBar.setIndeterminate(true);
        // progressBar.setStringPainted(true);

        if (e.getSource() instanceof Task) {
          Task<?, ?> task = (Task<?, ?>) (e.getSource());
          progressBar.setToolTipText(task.getDescription());
        }

      } else if ("done".equals(propertyName) || "completed".equals(propertyName)) {
        progressBar.setIndeterminate(false);
        progressBar.setEnabled(false);
        progressBar.setValue(0);
        // progressBar.setStringPainted(false);

        progressBar.setToolTipText(null);

      } else if ("progress".equals(propertyName)) {
        int value = (Integer) (e.getNewValue());
        progressBar.setEnabled(true);
        progressBar.setIndeterminate(false);
        progressBar.setValue(value);
      }
    }
  }


}

