/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.Desktop;
import java.io.File;
import java.util.Enumeration;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.lucy.g3.gui.common.dialogs.AboutBox;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;

public class WorkbenchWindowActions {
  public static final String ACTION_QUIT = "quit";
  
  public static final String ACTION_ABOUT = "about";

  public static final String ACTION_LOG_DIR = "logDir";

  private WorkbenchWindowActions(){}
  
  private static WorkbenchWindowActions INSTANCE;
  
  public static WorkbenchWindowActions getInstance(){
    if(INSTANCE == null)
      INSTANCE = new WorkbenchWindowActions();
    return INSTANCE;
  }

  @Action
  public void about() {
    new AboutBox(WindowUtils.getMainFrame()).setVisible(true);
  }
  
  @Action
  public Task<?, ?> logDir() {
    return new OpenLogTask(Application.getInstance());
  }
  
  private static class OpenLogTask extends Task<File, Void> {

    public OpenLogTask(Application application) {
      super(application);
      setTitle("View Log");
    }

    @Override
    protected File doInBackground() throws Exception {
      Enumeration<?> appds = Logger.getRootLogger().getAllAppenders();
      while (appds.hasMoreElements()) {
        Appender appd = (Appender) appds.nextElement();
        if (appd instanceof FileAppender) {
          File logFile = new File(((FileAppender) appd).getFile());
          File dir = logFile.getParentFile();
          Desktop.getDesktop().open(dir);
          return dir;
        }
      }

      return null;
    }

    @Override
    protected void failed(Throwable cause) {
      Logger.getLogger(getClass())
          .error("Cannot open log directory. " + cause.getMessage());
    }
  }
  
  public javax.swing.Action getAction(String actionName) {
    javax.swing.Action action = Application.getInstance().getContext().getActionMap(this).get(actionName);
    if(action == null) {
      throw new IllegalArgumentException("action not found: " + actionName);
    }
    
    return action;
  }
}

