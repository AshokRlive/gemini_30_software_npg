/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench.internal;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import com.lucy.g3.gui.framework.workbench.IWorkbench;
import com.lucy.g3.gui.framework.workbench.IWorkbenchStatusBar;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindow;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowAdvisor;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowBanner;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowTab;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowTabContent;


/**
 *
 */
public class WorkbenchWindow implements IWorkbenchWindow {
  private IWorkbenchWindowAdvisor advisor;
  
  private JMenuBar menuBar;
  private IWorkbenchWindowBanner banner;
  private IWorkbenchWindowTabContent content;
  private IWorkbenchStatusBar statusBar;
  
  public WorkbenchWindow() {
    this(null);
  }
  
  public WorkbenchWindow(IWorkbenchWindowAdvisor advisor) {
    this.advisor = advisor;
  }
  
  private IWorkbenchWindowAdvisor getAdvisor() {
    if(advisor == null) {
      advisor = createDefaultAdvisor();
    }
    
    return advisor;
  }
  
  private IWorkbenchWindowAdvisor createDefaultAdvisor() {
    return new WorkbenchWindowAdvisor();
  }

  @Override
  public IWorkbenchStatusBar getStatusBar() {
    if(statusBar == null) {
      statusBar = getAdvisor().createStatusBar();
    }
    
    return statusBar;
  }
  
  private JPanel container;
  
  @Override
  public JComponent getContentComponent() {
    if(getAdvisor().getShowBanner()) {
      if(container == null)
        container = new JPanel(new BorderLayout());
      container.add(getBanner().getComponent(), BorderLayout.NORTH);
      container.add(getContent().getComponent(), BorderLayout.CENTER);
      return container;
    } else {
      return getContent().getComponent();
    }    
  }

  @Override
  public IWorkbenchWindowTabContent getContent() {
    if(content == null) {
      content = new WorkbenchWindowTabContent();
      content.setTabs(getAdvisor().createTabs());
    }
    
    return content;
  }

  @Override
  public JMenuBar getMenuBar() {
    if(menuBar == null) {
      menuBar = new JMenuBar();
      JMenu[] menus = getAdvisor().getMenuAdvisor().createMenus();
      for (int i = 0; i < menus.length; i++) {
        if(menus[i] != null)
          menuBar.add(menus[i]);
      }
    }
    
    return menuBar;
  }

  @Override
  public IWorkbenchWindowBanner getBanner() {
    if(getAdvisor().getShowBanner()) {
      if(banner == null) {
        banner = new WorkbenchWindowBanner();
        banner.setInfoComponent(getAdvisor().getBannerInfoPane());
        banner.setToolbarComponents(getAdvisor().getBannerToolbarItems());
      }
      return banner;
    }else {
      return null;
    }
  }

  @Override
  public IWorkbench getWorkbench() {
    return null;//TODO 
  }

  @Override
  public void saveSession() {
    IWorkbenchWindowTab[] tabs = content.getTabs();
    for (IWorkbenchWindowTab tab : tabs) {
      tab.saveSession();
    }
  }

}

