/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.workbench;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.util.PaintUtils;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.IPageViewer;
import com.lucy.g3.gui.framework.page.Page;

/**
 * Implementation of Page viewer.
 */
public class DefaultPageViewer extends JXTitledPanel implements IPageViewer {

  private JScrollPane scrollPane;
  private final JPanel defaultView = new JPanel(new BorderLayout());
  private final JLabel msgLabel = new JLabel();
  private final JLabel iconLabel = new JLabel();
  private final PropertyChangeListener pagePCL = new PagePCL();

  private Page view;


  public DefaultPageViewer() {
    super();

    defaultView.setOpaque(false);
    defaultView.add(msgLabel, BorderLayout.CENTER);

    setTitleFont(getTitleFont().deriveFont(Font.BOLD));
    setBorder(new RoundedBorder());
    scrollPane = new JScrollPane();
    UIUtils.increaseScrollSpeed(scrollPane);
    scrollPane.setBorder(BorderFactory.createEmptyBorder());
    setLeftDecoration(iconLabel);
  }

  @Override
  public void show(Page v) {
    msgLabel.setText("");
    if (view != null) {
      view.removePropertyChangeListener(Page.PROPERTY_TITLE, pagePCL);
    }

    if (v != null) {
      try {
        if (v.isScrollable()) {
          setContentContainer(scrollPane);
          scrollPane.setViewportView(v.getContent());
        } else {
          setContentContainer(v.getContent());
          scrollPane.setViewportView(defaultView);
        }

      } catch (Exception e) {
        // Show exception on label
        msgLabel.setText("Fail to initialise view: " + v
            + " \n cause " + e.getClass().getName() + ": " + e.getMessage());
      }

      // Update icon
      iconLabel.setIcon(v.getTitleIcon());

      // Update icon
      setTitle(createPageTitle(v));

      // Synchronise title
      v.addPropertyChangeListener(Page.PROPERTY_TITLE, pagePCL);

      this.view = v;

    } else {
      setLeftDecoration(null);
      setTitle(" ");
      setContentContainer(defaultView);
    }
    revalidate();
    repaint();
  }

  private static String createPageTitle(Page p) {
    if (p == null) {
      return " ";
    }
    String title = p.getTitle();

    if (Strings.isBlank(title)) {
      title = p.getNodeName();
    }

    String description = p.getDescription();
    if (!Strings.isBlank(description)) {
      title = title + " -- " + description;
    }

    return title;
  }

  @Override
  public Page getShowedPage() {
    return view;
  }

  @Override
  public void clear() {
    show(null);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }


  private class PagePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      setTitle(createPageTitle(view));
    }
  }

  
   /**
   * Round corner Border.
   */
  private static class RoundedBorder implements Border {

    private int cornerRadius;
    private Color color;


    public RoundedBorder() {
      this(10);
    }

    public RoundedBorder(int cornerRadius) {
      this.cornerRadius = cornerRadius;
    }
    
    @SuppressWarnings("unused")
    public void setColor(Color color) {
      this.color = color;
    }

    @Override
    public Insets getBorderInsets(Component c) {
      return getBorderInsets(c, new Insets(0, 0, 0, 0));
    }

    public Insets getBorderInsets(Component c, Insets insets) {
      insets.top = insets.bottom = cornerRadius / 2;
      insets.left = insets.right = 1;
      return insets;
    }

    @Override
    public boolean isBorderOpaque() {
      return false;
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
      Graphics2D g2 = (Graphics2D) g.create();
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      Color drawColor = color;
      if (drawColor == null) {
        drawColor = UIUtils.deriveColorHSB(c.getBackground(), 0, 0, -.3f);
      }
      g2.setColor(PaintUtils.setAlpha(drawColor, 40));
      g2.drawRoundRect(x, y + 2, width - 1, height - 3, cornerRadius, cornerRadius);
      g2.setColor(PaintUtils.setAlpha(drawColor, 90));
      g2.drawRoundRect(x, y + 1, width - 1, height - 2, cornerRadius, cornerRadius);
      g2.setColor(PaintUtils.setAlpha(drawColor, 255));
      g2.drawRoundRect(x, y, width - 1, height - 1, cornerRadius, cornerRadius);

      g2.dispose();
    }
  }
}
