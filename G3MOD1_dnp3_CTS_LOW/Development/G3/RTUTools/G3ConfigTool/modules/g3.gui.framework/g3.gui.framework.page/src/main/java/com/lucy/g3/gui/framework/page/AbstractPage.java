/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ListModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

//import com.lucy.g3.help.Helper;

/**
 * <p>
 * Abstract <code>Page</code> implementation for lazily initialising the page
 * content. Initialisation code should be implemented in {@linkplain #init()}.
 * </p>
 * <p>
 * The <code>Page</code> objects extended from this class will not be
 * initialised until {@linkplain #getContent()} is called.
 * </p>
 */
public abstract class AbstractPage extends JPanel implements Page, Navigable {

  private final Logger log = Logger.getLogger(AbstractPage.class);

  private final String sessionKey;

  private final Object data;

  private boolean init = false;

  private String nodeName;

  private String title;

  private final String description;

  private ImageIcon nodeIcon;

  private final ArrayList<Page> childrenPages = new ArrayList<>();
  
  private final ToggleEnableAction toggleEnableAction;


  protected AbstractPage() {
    this(null, null);
  }
  
  public AbstractPage(Object data) {
    this(data, null);
  }

  /**
   * Constructor an abstract page.
   *
   * @param data
   *          the data that this page is constructed for, such as "Module".
   * @param description
   *          the description of this page
   */
  public AbstractPage(Object data, String description) {
    super(new BorderLayout());

    this.data = data;
    this.description = description;
    this.toggleEnableAction = createToggleEnableAction(data);

    String name = getClass().getSimpleName();
    setName(name);
    sessionKey = name + ".session.xml";

    /*
     * Clicking on panel will request focus, which help commit the changing
     * value in components (such as JFormattedTextField) contained by this page.
     */
    addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        requestFocus();
      }
    });

//    try {
//      Helper.register(this, getClass());
//    } catch (Throwable e) {
//      log.error(e);
//    }

  }

  static ToggleEnableAction createToggleEnableAction(Object data) {
    if (data != null) {
      try {
        Method setter = data.getClass().getMethod("setEnabled", boolean.class);
        Method getter = data.getClass().getMethod("isEnabled");
        return new ToggleEnableAction(setter, getter, data);
      } catch (NoSuchMethodException | SecurityException e) {
        return null;
      }
    }
    return null;
  }
  
  private static class ToggleEnableAction extends AbstractAction{
    private final Method setter;
    private final Method getter;
    private final Object data;
    
    public ToggleEnableAction(Method setter, Method getter, Object data) {
      putValue(Action.NAME, "Enable/Disable");
      this.setter = setter;
      this.getter= getter;
      this.data = data;
    }
    
    void updateActionName() {
      boolean isEnabled;
      try {
        isEnabled = (boolean) getter.invoke(data);
        putValue(Action.NAME, isEnabled ? "Disable" : "Enable");
      } catch (Throwable e1){
        // Nothing to do.
      }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      boolean isEnabled;
      try {
        isEnabled = (boolean) getter.invoke(data);
        setter.invoke(data, !isEnabled);
      } catch (Throwable e1) {
        e1.printStackTrace();
      } 
      
    }
  }

  @Override
  public String getSessionKey() {
    return sessionKey;
  }

  /**
   * Initialisation of this page.
   *
   * @throws Exception
   *           exception caught while initialising this page.
   */
  protected abstract void init() throws Exception;

  @Override
  public boolean isInitialised() {
    return init;
  }

  /**
   * Called whenever the page is rendered.
   * This can be used by the page as a notification
   * to load page-specific information in its fields.
   * <p/>
   * By default, this method does nothing.
   */
  protected void renderingPage() {
      // Empty
  }

  
  @Override
  public final JComponent getContent() {
    if (!isInitialised()) {
      try {
        init();
        init = true;
      } catch (Exception e) {
        log.error("Fail to initialse the content component", e);
        return new JLabel("Initialistion Exception: " + e.getMessage());
      }

      // Try to restore session of this page
      try {
        Application.getInstance().getContext()
            .getSessionStorage().restore(this, this.sessionKey);
      } catch (Exception e) {
        log.warn("Fail to restore the session of page \""
            + this.getClass().getSimpleName() + "\": " + e.getMessage());
      }

    }
    
    renderingPage();
    return this;
  }


  // ================== Navigation Event Support ===================

  private javax.swing.event.EventListenerList listenerList =
      new javax.swing.event.EventListenerList();


  @Override
  public final void addNavigationEventListener(NavigationActionListener listener) {
    if (listener == null) {
      return;
    }

    // Check if listener already exists
    NavigationActionListener[] listeners = listenerList.getListeners(NavigationActionListener.class);
    for (int i = 0; i < listeners.length; i++) {
      if (listeners[i] == listener) {
        return;
      }
    }

    listenerList.add(NavigationActionListener.class, listener);
  }

  @Override
  public final void removeNavigationEventListener(NavigationActionListener listener) {
    listenerList.remove(NavigationActionListener.class, listener);
  }

  /**
   * Fire an event to navigate to another page.
   *
   * @param evt
   *          navigation event
   */
  protected final void fireNavigationEvent(NavigationEvent evt) {
    Object[] listeners = listenerList.getListenerList();
    // Each listener occupies two elements - the first is the listener class
    // and the second is the listener instance
    for (int i = 0; i < listeners.length; i += 2) {
      if (listeners[i] == NavigationActionListener.class) {
        ((NavigationActionListener) listeners[i + 1]).navigationPerformed(evt);
      }
    }
  }

  @Override
  public final void setNodeName(String nodename) {
    Object oldValue = getNodeName();
    this.nodeName = nodename;
    firePropertyChange(PROPERTY_NODE_NAME, oldValue, nodeName);
  }

  @Override
  public String getNodeName() {
    // Use page title as the node name if it is not set
    if (nodeName == null) {
      return getTitle();
    }

    return nodeName;
  }

  @Override
  public ImageIcon getNodeIcon() {
    return nodeIcon;
  }

  public final void setNodeIcon(ImageIcon nodeIcon) {
    Object oldValue = getNodeIcon();
    this.nodeIcon = nodeIcon;
    firePropertyChange(PROPERTY_NODE_ICON, oldValue, nodeIcon);
  }

  @Override
  public final Object getData() {
    return data;
  }

  @Override
  public final String getTitle() {
    return title;
  }

  @Override
  public final void setTitle(String title) {
    Object oldValue = getTitle();
    this.title = title;
    firePropertyChange(PROPERTY_TITLE, oldValue, nodeIcon);
  }

  @Override
  public Icon getTitleIcon() {
    return null;
  }

  @Override
  public final String getDescription() {
    return description;
  }

  @Override
  public boolean isScrollable() {
    return false;
  }

  /**
   * Override this method if this page node contains children nodes.
   */
  @Override
  public ListModel<?> getChildrenDataList() {
    return null;
  }

  /**
   * Override this method if this page node contains actions.
   */
  @Override
  public Action[] getContextActions() {
    return null;
  }

  /**
   * Override this method to show context tool-tips for this page node.
   */
  @Override
  public String getContextTips() {
    return null;
  }

  
  @Override
  public Action getToggleEnableAction() {
    if (toggleEnableAction != null)
      toggleEnableAction.updateActionName();
    
    return toggleEnableAction;
  }

  @Override
  public final Page[] getChildrenPages() {
    return childrenPages.toArray(new Page[childrenPages.size()]);
  }

  protected final void addChildPage(Page page) {
    if (page != null) {
      childrenPages.add(page);
    }
  }
}
