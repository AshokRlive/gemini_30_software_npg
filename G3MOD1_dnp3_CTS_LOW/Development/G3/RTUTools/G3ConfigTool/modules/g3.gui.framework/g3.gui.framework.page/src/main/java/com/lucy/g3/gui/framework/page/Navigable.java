/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

/**
 * A class can implement the <code>Navigable</code> interface when it wants to
 * be informed of navigation action.
 *
 * @see {@linkplain NavigationActionListener}
 * @see {@linkplain NavigationEvent}
 */
public interface Navigable {

  /**
   * Registers a <code>NavigationActionListener</code> that can receive
   * NavigationEvent.
   *
   * @param listener
   *          the listener
   * @see {@linkplain #removeNavigationEventListener(NavigationActionListener)}
   */
  void addNavigationEventListener(NavigationActionListener listener);

  /**
   * De-registers a <code>NavigationActionListener</code>.
   *
   * @see {@linkplain #addNavigationEventListener(NavigationActionListener)}
   */
  void removeNavigationEventListener(NavigationActionListener listener);
}
