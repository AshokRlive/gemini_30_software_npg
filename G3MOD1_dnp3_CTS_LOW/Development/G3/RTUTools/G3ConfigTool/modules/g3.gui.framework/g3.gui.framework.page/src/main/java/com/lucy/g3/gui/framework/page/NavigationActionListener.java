/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import java.util.EventListener;

/**
 * Listener interface for receiving navigation events. A class that needs to
 * process an navigation event should implement this interface.
 */
public interface NavigationActionListener extends EventListener {

  void navigationPerformed(NavigationEvent evt);

}
