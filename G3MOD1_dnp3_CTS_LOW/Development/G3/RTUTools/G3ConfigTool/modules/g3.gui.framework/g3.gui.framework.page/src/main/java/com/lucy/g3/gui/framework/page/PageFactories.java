/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import java.util.Collection;
import java.util.HashMap;


/**
 * A factory for creating a page for a given object.
 */
public class PageFactories {
  public final static int KEY_GENERAL = 0;
  public final static int KEY_HMI = 1;
  public final static int KEY_CAN_MODULE = 2;
  public final static int KEY_LOGIC= 3;
  public final static int KEY_USERS= 4;
  public final static int KEY_SDNP3= 5;
  public final static int KEY_S104= 6;
  public final static int KEY_S101= 7;
  public final static int KEY_SCADA_PROTOCOL= 8;
  public final static int KEY_VPOINT= 9;
  public final static int KEY_FIELD_DEVICE= 10;
  public final static int KEY_PORTS = 11;
  public final static int KEY_MODBUS= 12;
  public final static int KEY_COMMS_DEVICE= 13;
  public final static int KEY_TEMPLATE= 14;
  public final static int KEY_LAST = 15;
  
  private PageFactories(){}
  
  private final static HashMap<Integer, IPageFactory > factories = new HashMap<>();
  
  public static void registerFactory(int key, IPageFactory factory) {
    factories.put(key, factory);
  }
  
  public static void deregisterFactory(int key) {
    factories.remove(key);
  }
  
  public static IPageFactory getFactory(int key) {
    return factories.get(key);
  }

  public static Page createPage(Object data) {
    Collection<IPageFactory> values = factories.values();
    Page page;
    for (IPageFactory f : values) {
      page = f.createPage(data);
      if(page != null)
        return page;
    }
    
    return null;
  }
}

