/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * This page is just contains a label and usually used for showing error
 * message.
 */
public final class BlankPage extends AbstractPage {

  private String message;


  public BlankPage(String title, String message) {
    this(null, title, message);
  }

  public BlankPage(String nodeName, String title, String message) {
    super();
    setNodeName(nodeName);
    setTitle(title);
    this.message = message;
  }

  @Override
  protected void init() throws Exception {
    if (message != null) {
      JLabel content = new JLabel(message);
      content.setFont(content.getFont().deriveFont(20.0f));
      content.setHorizontalAlignment(SwingConstants.CENTER);
      add(BorderLayout.CENTER, content);
    }
  }

  @Override
  public boolean hasError() {
    return false;
  }

}
