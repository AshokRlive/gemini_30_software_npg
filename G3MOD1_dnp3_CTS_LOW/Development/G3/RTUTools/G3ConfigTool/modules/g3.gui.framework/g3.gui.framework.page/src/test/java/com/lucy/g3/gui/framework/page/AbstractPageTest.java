/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import org.junit.Test;

import static org.junit.Assert.*;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.test.support.utilities.BeanTestUtil;

public class AbstractPageTest {
  
  

  @Test
  public void testBean() {
    AbstractPageImpl bean = new AbstractPageImpl();
    BeanTestUtil.testReadWriteProperties(bean, new String[] {
        AbstractPage.PROPERTY_NODE_ICON,
        AbstractPage.PROPERTY_NODE_NAME,
    });
  }

  @Test
  public void testEnableAction() throws NoSuchMethodException, SecurityException {
    TestBean bean = new TestBean();
    TestBean.class.getMethod("setEnabled", boolean.class);
    assertNotNull("action must not be null", AbstractPage.createToggleEnableAction(bean));
  }

  private static class AbstractPageImpl extends AbstractPage {

    public AbstractPageImpl() {
      super(null);
    }

    @Override
    protected void init() throws Exception {
    }

    @Override
    public boolean hasError() {
      return false;
    }
  }
  
  public static class TestBean extends Model {

    /**
     * The name of property {@value} . <li>Value type: {@link Boolean}.</li>
     */
    public static final String PROPERTY_ENABLED = "enabled";

    private boolean enabled;


    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      Object oldValue = this.enabled;
      this.enabled = enabled;
      firePropertyChange(PROPERTY_ENABLED, oldValue, enabled);
    }

  }
}
