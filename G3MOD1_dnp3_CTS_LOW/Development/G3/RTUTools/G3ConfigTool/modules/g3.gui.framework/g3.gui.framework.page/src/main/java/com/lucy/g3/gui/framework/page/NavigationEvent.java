/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import java.util.EventObject;

/**
 ** A {@code NavigationEvent} get fired when navigating action between
 * {@link Page}s is performed. For example, a user want go from one {@link Page}
 * to another, the original page should fire a {@code NavigationEvent} to inform
 * its listener to perform navigation.
 *
 * @see NavigationActionListener
 */
public class NavigationEvent extends EventObject {

  /**
   * Constructs a navigation event from source data object.
   *
   * @param data
   *          The data contained by a page.
   */
  public NavigationEvent(Object source) {
    super(source == null ? new Object() : source);
  }

}
