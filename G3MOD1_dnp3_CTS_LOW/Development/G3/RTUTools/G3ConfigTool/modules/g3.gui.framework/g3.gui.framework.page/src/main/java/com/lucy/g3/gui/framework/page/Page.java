/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.gui.framework.page;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.ListModel;

import com.jgoodies.common.bean.ObservableBean2;

/**
 * A <code>Page</code> is a GUI component that can be displayed in ConfigTool workbench.
 */
public interface Page extends ObservableBean2 {

  String PROPERTY_NODE_NAME = "nodeName";

  String PROPERTY_TITLE = "title";

  String PROPERTY_NODE_ICON = "nodeIcon";


  /**
   * Gets the tree node name of this page.
   *
   * @return the name of this node
   */
  String getNodeName();

  /**
   * Sets the tree node name of this page.
   */
  void setNodeName(String nodename);

  /**
   * Gets the tree node icon of this page.
   */
  ImageIcon getNodeIcon();

  /**
   * Gets the title of this page.
   *
   * @return null if there is no description.
   */
  String getTitle();

  /**
   * Sets the title of this page.
   */
  void setTitle(String title);

  /**
   * Gets the icon that can be displayed in title.
   *
   * @return the icon object, could be <code>null</code>.
   */
  Icon getTitleIcon();

  /**
   * Gets the description of this page.
   *
   * @return the description
   */
  String getDescription();

  /**
   * Checks if this page's component should be scrollable. A scrollable page
   * will be placed into a ScrollPane when the page is viewed.
   */
  boolean isScrollable();

  /**
   * Gets this page's content component that can be viewed by a
   *
   * @return the content component of this page, must not be <code>null</code>.
   */
  JComponent getContent();

  /**
   * Gets the name of session for saving the state of this <code>Page</code>.
   *
   * @return if <code>null</code>, this <code>Page</code>'s session will not be
   *         saved.
   */
  String getSessionKey();

  /**
   * Gets the data object of this page. The data object is the model of a
   * page(which is view in MVC). Page is usually create from a existing data
   * model and can be used to modify the content of data model. *
   *
   * @return the data object, null if it doesn't exist.
   */
  Object getData();

  /**
   * Gets the children data if they available. Children data will be used to
   * initialise children pages under this page.
   *
   * @return the children data list mode. null if it doesn't exist.
   */
  ListModel<?> getChildrenDataList();

  Page[] getChildrenPages();

  /**
   * Gets the context actions for this page. Context action will be used to
   * create Pop-up menu for this page.
   *
   * @return a list of context actions. null if there is no context actions.
   */
  Action[] getContextActions();
  
  Action getToggleEnableAction();

  /**
   * Gets the description string of this page which is used for displaying in
   * context tip box.
   */
  String getContextTips();

  /**
   * Checks if this page contains any error.
   */
  boolean hasError();

  /**
   * Checks if this page has been initialised.
   *
   * @return true if this page is already initialised, false otherwise.
   */
  boolean isInitialised();

}
