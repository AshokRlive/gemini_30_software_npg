/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.VerticalLayout;

import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.gui.framework.page.UpdatablePage;
import com.lucy.g3.rtu.commissioning.ui.layout.WrapLayout;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.PollingMode;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.IOperableLogic;
import com.lucy.g3.rtu.config.clogic.OperationMode;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.hmi.domain.ControlScreen;
import com.lucy.g3.rtu.config.hmi.domain.DataEntry;
import com.lucy.g3.rtu.config.hmi.domain.DataScreen;
import com.lucy.g3.rtu.config.hmi.domain.MenuScreen;
import com.lucy.g3.rtu.config.hmi.domain.Screen;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * The Class CommissioningMimicPage.
 */
public class CommissioningMimicPage extends AbstractPage implements UpdatablePage {
  
  private Logger log = Logger.getLogger(CommissioningMimicPage.class);
  
  private final ArrayList<IPointData> pollingDPointList = new ArrayList<IPointData>();
  private final ArrayList<IPointData> pollingAPointList = new ArrayList<IPointData>();
  private final ArrayList<IPointData> pollingCPointList = new ArrayList<IPointData>();
  
  private final PollingTask pollingTask = new PollingTask();
  
  private final ICommissioningInvoker invoker;

  private IConfig config;
  
  private JPanel mimicPanel;
  
  public CommissioningMimicPage(ICommissioningInvoker invoker) {
    super(null);
    this.invoker = Preconditions.checkNotNull(invoker, "invoker must not be null");
    setNodeName("Mimic");
    
  }
  
  private JPanel createMimicPanel(IConfig config) {
    if (config == null) {
      return null;
      
    } else {
      HMIScreenManager manager =  config.getConfigModule(HMIScreenManager.CONFIG_MODULE_ID); 
      return createMenuScreenPanel(manager.getMainMenu());
    }
  }
  
  private JPanel createMenuScreenPanel(MenuScreen menu) {
    List<Screen> children = menu.getPreviewChildren();

    JPanel panel;
    if (menu.isRoot()) {
      /* Create root panel */
      FlowLayout layout = new WrapLayout(FlowLayout.LEADING);
      layout.setAlignOnBaseline(true);
      panel = new JPanel(layout);
    } else {
      /* Create sub panel*/
      panel = new JPanel(new VerticalLayout());
      panel.setBorder(BorderFactory.createTitledBorder(
          BorderFactory.createLineBorder(Color.BLACK, 2),
          menu.getTitle()));
    }
    
    // Create children panel
    JPanel childPanel = null;
    for (Screen child : children) {
      
      if (child instanceof MenuScreen) {
        childPanel = createMenuScreenPanel((MenuScreen) child);
        
      } else if (child instanceof DataScreen) {
        childPanel = createDataScreenPanel((DataScreen) child);
        
      } else if (child instanceof ControlScreen) {
        childPanel = createControlScreenPanel((ControlScreen) child);
      }
      
      if (childPanel != null && childPanel.getComponentCount() > 0)
        panel.add(childPanel);
    }
    
    return panel.getComponentCount() > 0 ? panel : null;
  }

  private JPanel createControlScreenPanel(ControlScreen child) {
    IOperableLogic clogic = child.getCLogic();
    if (clogic == null) {
      return null;
    }

    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("right:[50dlu,default], 2*($lcgap, [30dlu,default])"));
    builder.setBorder(Borders.DLU4_BORDER);
    builder.setBackground(new Color(153,217,234));
    JLabel label = new JLabel(child.getTitle());
    if (clogic.isSwitchLogic()) {
      JButton btnOpen  = createControlButton(clogic, SWITCH_OPERATION.SWITCH_OPERATION_OPEN);
      JButton btnClose = createControlButton(clogic, SWITCH_OPERATION.SWITCH_OPERATION_CLOSE);
      
      builder.append(label, btnClose, btnOpen);
      
    } else {
      builder.append(label, createControlButton(clogic, null)); 
    }
    
    // Bind GUI enable state to the logic enable property
    final JPanel panel = builder.getPanel();
    UIUtils.enableComponents(panel, clogic.isEnabled());
    clogic.addPropertyChangeListener(ICLogic.PROPERTY_ENABLED, new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        UIUtils.enableComponents(panel, (Boolean)evt.getNewValue());        
      }
    });
    
    return panel;
  }
  
  private JPanel createDataScreenPanel(DataScreen child) {
    DataEntry[] entries = child.getDataEntries();
    if (entries.length == 0)
      return null;
    
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("right:[50dlu,default], 2*($lcgap, 50dlu)"));
    builder.setBorder(Borders.DLU4_BORDER);
    builder.setBackground(new Color(200,191,231));
    builder.setBorder(BorderFactory.createTitledBorder(child.getTitle()));
  
    VirtualPoint point = null; 
    for (DataEntry entry : entries) {
      point = entry.getSource();
      if (point == null)
        continue;
  
      String header = entry.getHeader();
      if (Strings.isBlank(header))
        header = "Unknown";
      builder.append(header + ":", createPointLabel(point), new JLabel(entry.getFooter()));
      builder.nextLine();
    }
    return builder.getPanel();
  }

  private JButton createControlButton(final IOperableLogic clogic, final SWITCH_OPERATION opCode) {
    final String actionName;
  
    OperationMode opMode = clogic.getType().getOpMode();
    switch (opMode) {
    case START_STOP:
    case GENERIC_OPERATE:
      actionName = opCode == SWITCH_OPERATION.SWITCH_OPERATION_OPEN ? "Stop" : "Start";
      break;
      
    case OPEN_CLOSE:
      actionName = opCode == SWITCH_OPERATION.SWITCH_OPERATION_OPEN ? "Open" : "Close";
      break;
      
    default:
      throw new UnsupportedOperationException("Unsupported operation mode:"+opMode);
    }
  
    JButton button = new JButton(actionName);
    //button.setForeground(opCode == SWITCH_OPERATION.SWITCH_OPERATION_OPEN ? Color.BLUE : Color.RED);
    button.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        Component owner = (Component)e.getSource();
        String logicName = clogic.getCustomName();
        if (Strings.isBlank(logicName))
          logicName = clogic.getDescription();
        
        int rc = JOptionPane.showConfirmDialog(owner, 
            String.format("<html><p>Are you sure you want to perform this action?</p>"
                + "&nbsp<p><b>%s \"%s\"</b></p>&nbsp</html>",actionName.toUpperCase(), logicName),
            "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        
        if (rc == JOptionPane.YES_OPTION) {
          invoker.getG3TaskService().execute(new LogicOperateTask(
              owner,
              clogic, opCode, LocalRemoteCode.LOCAL));
        }
      }
    });
    return button;
  }
  

  private JLabel createPointLabel(VirtualPoint point) {
    final VirtualPointData pointData = invoker.createPointData(point);
    addPollingData(pointData);
    
    ValueModel vm = new PropertyAdapter<VirtualPointData>(pointData, VirtualPointData.PROPERTY_FORMATTED_VALUE, true);
    return BasicComponentFactory.createLabel(vm);
  }

  private void addPollingData(VirtualPointData pdata) {
    VirtualPointType pointType = (VirtualPointType)pdata.getType();
    if (pointType.isAnalogue()) {
      pollingAPointList.add(pdata);
    } else if (pointType.isDigital()) {
      pollingDPointList.add(pdata);
    } else if (pointType.isCounter()) {
      pollingCPointList.add(pdata);
    } else {
      log.error("Unsupported type:" + pointType);
    }
  }

  public void setConfigData(IConfig config) {
    clearPollingData();
    
    this.config = config;
    
    populateMimicPanel(createMimicPanel(config));
  }

  private void clearPollingData() {
    pollingDPointList.clear();
    pollingAPointList.clear();
    pollingCPointList.clear();
  }


  @Override
  protected void init() throws Exception {
    initComponents();
    
    
    // Initialise Mimic Panel
    if (config != null)
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          populateMimicPanel(createMimicPanel(config));
        }
      });
  }
  
  private void populateMimicPanel(JPanel mimicPanel) {
    if (scrollpane != null) {
      scrollpane.setViewportView(mimicPanel);
      this.mimicPanel = mimicPanel;
      updateMessagePanelVisiblity();
    }
  }

  private void updateMessagePanelVisiblity() {
    // Set message panel visibility
    if (mimicPanel != null && scrollpane.isVisible()) {
      panelMessage.setVisible(false);
    } else {
      panelMessage.setVisible(true);
    }
  }

  @Override
  public void startUpdating() {
    pollingTask.start(500);
    scrollpane.setVisible(true);
    updateMessagePanelVisiblity();
  }

  @Override
  public void stopUpdating() {
    pollingTask.stop();
    scrollpane.setVisible(false);
    updateMessagePanelVisiblity();
  }

  
  private class PollingTask implements Runnable {

    private volatile Thread pollingThread;

    private int period = 500;


    public void start(int period) {
      setPeriod(period);
      if (pollingThread == null || !pollingThread.isAlive()) {
        pollingThread = new Thread(this, "Polling Points Thread");
        pollingThread.setDaemon(true);
        pollingThread.start();
      }
    }

    public void stop() {
      pollingThread = null;
    }

    public void setPeriod(int period) {
      if (period > 0) {
        this.period = period;
      }
    }

    @Override
    public void run() {
      log.debug("Start polling points value");

      Thread thisThread = Thread.currentThread();

      while (pollingThread == thisThread) {
        pollPointData();

        // Delay
        try {
          Thread.sleep(period);
        } catch (InterruptedException e) {
          // Do nothing
        }
      }

      log.debug("Stop polling points value");
    }

    private void pollPointData() {
      try {
        if (!pollingDPointList.isEmpty()) {
          invoker.cmdGetPointStatus(new ArrayList<IPointData>(pollingDPointList), PollingMode.DIGITAL);
        }
        if (!pollingAPointList.isEmpty()) {
          invoker.cmdGetPointStatus(new ArrayList<IPointData>(pollingAPointList), PollingMode.ANALOG);
        }
        if (!pollingCPointList.isEmpty()) {
          invoker.cmdGetPointStatus(new ArrayList<IPointData>(pollingCPointList), PollingMode.COUNTER);
        }
      } catch (Exception e) {
        log.error("Failed to get point status:" + e.getMessage());
      }
    }
  }
  
  private class LogicOperateTask extends Task<Void, Void> {

    private final IOperableLogic switchLogic;
    private final SWITCH_OPERATION operation;
    private final LocalRemoteCode localRemote;
    private final Component owner;

    public LogicOperateTask(Component owner, IOperableLogic switchLogic,
        SWITCH_OPERATION operation, LocalRemoteCode localRemote) {
      super(invoker.getApp());
      this.owner = owner;
      this.switchLogic = switchLogic;
      this.operation = operation;
      this.localRemote = localRemote;

      setTitle("Logic Operation");
    }

    @Override
    protected Void doInBackground() throws Exception {
      if (switchLogic.isSwitchLogic())
        invoker.cmdOperateSwitch((short) switchLogic.getGroup(), operation, localRemote);
      else
        invoker.cmdOperateLogic((short)switchLogic.getGroup(), (short)0);
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      setMessage("Failed");
      MessageDialogs.error(owner, "Failed to perform action!", cause);
    }

    @Override
    protected void succeeded(Void result) {
      setMessage("Success");
    }
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scrollpane = new JScrollPane();
    panelMessage = new JPanel();
    JLabel lblMessage = new JLabel();

    //======== this ========
    setLayout(new BorderLayout(0, 10));

    //======== scrollpane ========
    {
      scrollpane.setBorder(new EmptyBorder(5, 5, 5, 5));
    }
    add(scrollpane, BorderLayout.CENTER);

    //======== panelMessage ========
    {
      panelMessage.setBorder(Borders.DIALOG_BORDER);
      panelMessage.setLayout(new BorderLayout());

      //---- lblMessage ----
      lblMessage.setText("To view HMI mimic, please connect to RTU and read configuration first!");
      panelMessage.add(lblMessage, BorderLayout.CENTER);
    }
    add(panelMessage, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
    UIUtils.increaseScrollSpeed(scrollpane);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollpane;
  private JPanel panelMessage;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  @Override
  public boolean hasError() {
    return false;
  }

}
