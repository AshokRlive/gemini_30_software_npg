/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.prompt.PromptSupport;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.rtu.commissioning.ui.wizard.panels.IPConfigPanel;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.eth.IPConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Constraints;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * First Wizard Page.
 */
class StepInitial extends AbstractCommissioningStep {

  public static final String ID = StepInitial.class.getSimpleName();

  public static final String KEY_SITE_NAME = "siteName";
  public static final String KEY_ETH0_IP = IPConfigPanel.KEY_ADDRESS;
  public static final String KEY_ETH0_MASK = IPConfigPanel.KEY_SUBMASK;
  public static final String KEY_ETH0_GATEWAY = IPConfigPanel.KEY_GATEWAY;

  /**
   * This key is changed to private since it is replaced by 
   * {@link #generateKeyForDNP3Address(SDNP3Session, boolean)}.
   */
  private static final String KEY_DNP3_SLAVE_ADDRESS = "dnp3SlaveAddress"; 
  
  private static final long SDNP3_ADDRESS_MIN = SDNP3Constraints.INSTANCE.getMin(
      SDNP3Constraints.PREFIX_SESSION_ADDRESS, 
      SDNP3Session.Address.PROPERTY_SOURCE);
  
  private static final long SDNP3_ADDRESS_MAX = SDNP3Constraints.INSTANCE.getMax(
      SDNP3Constraints.PREFIX_SESSION_ADDRESS, 
      SDNP3Session.Address.PROPERTY_SOURCE);

  private final ArrayList<JTextField> dnp3AddressFields = new ArrayList<>();
  private final IConfig data;
  
  public StepInitial(IConfig data) {
    super(ID, "Site Information");
    
    this.data = data;
    
    initComponents();
    initComponentKeys();
    initComponentVisibility(data);
    registerePersistentComponents();

    // Set default IP
    if (data != null) {
      PortsManager manager = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
      IPConfig eth0 = manager.getEthPort(ETHERNET_PORT.ETHERNET_PORT_CONTROL).getIpConfig();
      iPConfigPanel.loadDefault(eth0);
    }
    
    // Set default site name
    PromptSupport.setPrompt("1~64 characters", tfSiteName);
    GeneralConfig general = (GeneralConfig) data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
    tfSiteName.setText(general.getSiteName());
    tfSiteName.select(0, tfSiteName.getText().length());
  }
  
  private void registerePersistentComponents() {
    super.registerPersistentComponent(tfSiteName);  
    super.registerPersistentComponent(iPConfigPanel.getTfAddr());   
    super.registerPersistentComponent(iPConfigPanel.getTfGateway());
    super.registerPersistentComponent(iPConfigPanel.getTfSubmask());
  }


  private void initComponentVisibility(IConfig data) {
    PortsManager manager = (PortsManager) data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    IEthernetPort eth0 = manager.getEthPort(ETHERNET_PORT.ETHERNET_PORT_CONTROL);
    iPConfigPanel.setVisible(eth0.isConfigEnabled());
  }


  private void initComponentKeys() {
    tfSiteName.setName(KEY_SITE_NAME);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = iPConfigPanel.validateContents();

    String siteName = tfSiteName.getText();
    if (siteName == null || siteName.matches(GeneralConfig.RTU_NAME_PATTERN) == false) {
      ValidationComponentUtils.setErrorBackground(tfSiteName);
      result = "Please input a valid site name";
    } else {
      ValidationComponentUtils.setMandatoryBackground(tfSiteName);
    }
    
    if (panelDNP3Addr.isVisible()) {
      for (JTextField tfDNP3RTUAddr:dnp3AddressFields) {
        if (!validateSDNP3Address(tfDNP3RTUAddr.getText())) {
          ValidationComponentUtils.setErrorBackground(tfDNP3RTUAddr);
          result = String.format("Please input a valid DNP3 RTU Address [%d ~ %d]",
              SDNP3_ADDRESS_MIN,
              SDNP3_ADDRESS_MAX);
        } else {
          ValidationComponentUtils.setMandatoryBackground(tfDNP3RTUAddr);
        }
      }
    }

    return result;
  }
  
  private static boolean validateSDNP3Address(String address) {
    try {
      Long addressValue = Long.parseLong(address);

      return addressValue != null
        && EqualsUtil.areEqual(addressValue.toString(), address)
        && addressValue >= SDNP3_ADDRESS_MIN
        && addressValue <= SDNP3_ADDRESS_MAX;
        
    } catch (Throwable e) {
      return false;
    }
  }

  private void createUIComponents() {
//    NumberFormatter formatter = new NumberFormatter(NumberFormat.getIntegerInstance());
//    formatter.setValueClass(Long.class);
//    ftfDNP3RTUAddr = new JFormattedTextField(formatter);
//    ftfDNP3RTUAddr.setValue(null);
    panelDNP3Addr = buildDNP3AddressPanel();
  }
  
  private JPanel buildDNP3AddressPanel() {
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("left:default, $ugap, [50dlu,default]", ""));
    Collection<SDNP3Session> sesns = CommissioningWizard.findDNP3Sessions(data);
    
    // No session
    if(sesns == null || sesns.isEmpty()) {
      JPanel panel = builder.getPanel();
      panel.setVisible(false);
      return panel;
    }
    
    // Single session
    if(sesns.size() == 1) {
      SDNP3Session sesn = sesns.iterator().next();
      JTextField tfDNP3RTUAddr = createDNP3AddField(sesn, true);
      
      builder.append("DNP3 RTU Address:", tfDNP3RTUAddr);
      builder.nextLine();
      dnp3AddressFields.add(tfDNP3RTUAddr);
      return builder.getPanel();
    }
      
    // Multi-sessions
    for (SDNP3Session sesn: sesns) {
      JTextField tfDNP3RTUAddr = createDNP3AddField(sesn, false);
      String sesnName = sesn.getSessionName();
      if(Strings.isBlank(sesnName))
        sesnName = "Unnamed";
      builder.append(String.format("Session (%s):", sesnName), tfDNP3RTUAddr);
      builder.nextLine();
      
      dnp3AddressFields.add(tfDNP3RTUAddr);
    }
    builder.setBorder(new CompoundBorder(
        new CompoundBorder(
          new TitledBorder("DNP3 RTU Addresses"),
          new EmptyBorder(10, 0, 10, 0)),
        null));
    
    return builder.getPanel();
  }

  private JTextField createDNP3AddField(SDNP3Session sesn, boolean isSingleSession) {
    JTextField tfDNP3RTUAddr = new JTextField();
    tfDNP3RTUAddr.setName(generateKeyForDNP3Address(sesn, isSingleSession));
    super.registerPersistentComponent(tfDNP3RTUAddr);
    return tfDNP3RTUAddr;
  }
  
  /**
   * Generates a key for storing dnp3 address into commissioning cache.
   * @param sesn
   * @param isSingleSession indicates if the session is a single session in dnp3.
   *  This is for keeping ConfigTool compatible with old commissioning cache file which stores 
   *  DNP3 address using old key {@value #KEY_DNP3_SLAVE_ADDRESS}
   * @return
   */
  public static String generateKeyForDNP3Address(SDNP3Session sesn, boolean isSingleSession) {
    return isSingleSession ? KEY_DNP3_SLAVE_ADDRESS : "SDNP3SlaveAddress"+sesn.getUUID();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();
    tfSiteName = new JTextField();
    iPConfigPanel = new IPConfigPanel();

    //======== this ========
    setLayout(new FormLayout(
      "right:default, $lcgap, [80dlu,default], $lcgap, default:grow",
      "2*(default, $ugap), default"));

    //---- label1 ----
    label1.setText("RTU Site Name:");
    add(label1, CC.xy(1, 1));

    //---- tfSiteName ----
    tfSiteName.setText("test");
    add(tfSiteName, CC.xywh(3, 1, 3, 1));

    //---- iPConfigPanel ----
    iPConfigPanel.setBorder(new TitledBorder("ETH-0"));
    add(iPConfigPanel, CC.xywh(1, 3, 5, 1));
    add(panelDNP3Addr, CC.xywh(1, 5, 5, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JTextField tfSiteName;
  private IPConfigPanel iPConfigPanel;
  private JPanel panelDNP3Addr;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
