
package com.lucy.g3.rtu.commissioning.ui.wizard;

import org.junit.Assert;
import org.junit.Test;

import com.lucy.g3.rtu.commissioning.ui.wizard.StepScada;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate;



public class StepScadaTest {
  private String [] classifiers = {
      "",
      "Dev",
      "Alive",
      "Dev"
  };
  
  private String [] expected = {
      "",
      "",// separator
      StepScada.formatTitle("Alive"), // title
      "Alive",
      "", // separator
      StepScada.formatTitle("Dev"), // title
      "Dev",
      "Dev",
  };

  @Test
  public void testSorting() {
    ScadaTemplate[]unsorted = create(classifiers);
    Object[] sorted = StepScada.sortTemplates(unsorted);
    
    Assert.assertEquals(expected.length, sorted.length);
    for (int i = 0; i < sorted.length; i++) {
      if(sorted[i] instanceof String)
        Assert.assertEquals(expected[i], sorted[i]);
      else
        Assert.assertEquals(expected[i], ((ScadaTemplate)sorted[i]).getClassifier());
    }
  }
  
  private ScadaTemplate[] create(String[] classifier)
  {
    ScadaTemplate[] result = new ScadaTemplate[classifier.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = new ScadaTemplate(null);
      result[i].setClassifier(classifier[i]);
    }
    return result;
  }

}

