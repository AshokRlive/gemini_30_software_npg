/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JComponent;

import org.netbeans.spi.wizard.WizardPage;


/**
 * The Class CommissioningStep.
 */
public abstract class AbstractCommissioningStep extends WizardPage{
  
  AbstractCommissioningStep(String stepId, String stepDescription) {
    super(stepId, stepDescription);
  }

  AbstractCommissioningStep(String stepDescription) {
    super(stepDescription);
  }

  
  private final ArrayList<JComponent> cachedComponents = new ArrayList<>();
  private CommissioningCache cache;
  
  /**
   * Registers a component whose state will be restored from cache later.
   */
  public final void registerPersistentComponent(JComponent ... comp) {
    if (comp != null)
      cachedComponents.addAll(Arrays.asList(comp));
  }
  
  /**
   * Restores all registered components' state from cache.
   */
  public final void restoreFromCache(CommissioningCache cache) {
    this.cache = cache;
    cache.restoreComponents(cachedComponents.toArray(new JComponent[cachedComponents.size()]));
    postRestore();
  }
  
  protected final CommissioningCache getCache(){
    return cache;
  }
  
  protected void postRestore(){}
}

