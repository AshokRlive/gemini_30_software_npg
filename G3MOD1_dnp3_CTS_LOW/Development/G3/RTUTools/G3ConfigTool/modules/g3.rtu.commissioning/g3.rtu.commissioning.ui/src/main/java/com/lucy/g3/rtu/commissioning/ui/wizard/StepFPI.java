/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.commissioning.ui.wizard.panels.FPIFaultSettingsPanel;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.template.custom.fpi.FPIChannelTemplates;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplate;

/**
 * The Class StepFPI.
 */
class StepFPI extends AbstractCommissioningStep{
  static final String KEY_IS_CREATED = "StepFPICreated";
  
  private static final boolean  FPI_ENABLED_BY_DEFAULT = false;
  
  private final SwitchGearLogic[] sgl;
  private final FPIConfig[] fpiChannels;
  private final FPIEntryPanel[] fpiEntryPanels;
  
  public StepFPI(Collection<SwitchGearLogic> switchlogic, FPIConfig[] fpiChannels, FPIChannelTemplates templates) {
    super("Configure FPI");
    this.fpiChannels = fpiChannels;
    this.sgl = switchlogic.toArray(new SwitchGearLogic[switchlogic.size()]);
    
    // Create FPI entries for SGLs
    this.fpiEntryPanels = new FPIEntryPanel[fpiChannels.length];
    for (int i = 0; i < fpiEntryPanels.length; i++) {
      if (i < sgl.length){
        fpiEntryPanels[i] = new FPIEntryPanel(i, sgl[i], fpiChannels[i], templates);
        fpiEntryPanels[i].registerePersistentComponents(this);
      }else
        break;
    }
    
    initComponents();
    putWizardData(KEY_IS_CREATED, Boolean.TRUE);
    renderingPage();
  }

  private void createUIComponents() {
    // Create form from the created FPI entries
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("default", ""));

    for (int i = 0; i < fpiEntryPanels.length; i++) {
      if(fpiEntryPanels[i] != null) {
        
        builder.append(fpiEntryPanels[i]);
        
        builder.nextLine();
        builder.appendRelatedComponentsGapRow();
        builder.nextLine();
      }
    }
    
    contentPanel = builder.getPanel();
  }
  
  /**
   * Checks if the switchgear is selected in the last step.
   */
  private boolean isSwitchgearSelected(SwitchGearLogic sgl) {
    Object sglSelection = getWizardData(StepSwitch.generateKey(sgl));
    
    if (sglSelection != null
        && !StepSwitch.TEXT_DISABLED.equals(sglSelection)) {
      SwitchTemplate sw = (SwitchTemplate) sglSelection;
      return !sw.isCircuitBreaker();
    }
    
    return false;
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    String err = null;
    
    String result = null;
    for (FPIEntryPanel p : fpiEntryPanels) {
        result = p.validateContents();
        if(result != null) {
          err = result;
        }
    }
    
    
    return err;
  }

  @Override
  protected void renderingPage() {
    refreshVisibility();
    refreshBorderTitle();
    
    for (FPIEntryPanel p : fpiEntryPanels) {
      p.renderingPanel();
    }
  }

  private void refreshVisibility() {
    int visibleEntryCount = 0;
    
    Boolean fpiVisible;
    for (int i = 0; i < sgl.length && i < fpiChannels.length; i++) {
      fpiVisible = null;
      
      if (fpiEntryPanels[i] != null) {
        fpiVisible = isSwitchgearSelected(sgl[i]);
        
        fpiEntryPanels[i].setVisible(fpiVisible);
      }
      
      putWizardData(generateKeyVisible(i), fpiVisible);
      
      if (fpiVisible == Boolean.TRUE)
        visibleEntryCount++;
      
    }
    
    // Show a message if there no visible items 
    labelInfo.setVisible(visibleEntryCount <= 0);
  }
  
  private void refreshBorderTitle() {
    TitledBorder border;
    String key;
    Object template;
    
    for (int i = 0; i < fpiEntryPanels.length; i++) {
     border = (TitledBorder) fpiEntryPanels[i].getBorder();
     key = StepSwitch.generateKey(sgl[i]);
     template = getWizardData(key);
     
     if(template != null && template instanceof SwitchTemplate)
       border.setTitle(((SwitchTemplate)template).getSwitchName());
     else
       border.setTitle("");
   } 
  }

  private static class FPIEntryPanel extends JPanel {
    private final JRadioButton internFPIEnabled;
    private final JRadioButton internFPIDisabled;
    private final FPIFaultSettingsPanel settingsPanel; 
    
    
    FPIEntryPanel(int fpiIndex, SwitchGearLogic sw, FPIConfig fpiChannel, FPIChannelTemplates templates) {
      DefaultFormBuilder builder = new DefaultFormBuilder(
          new FormLayout("default, $lcgap, default, $lcgap, default, $lcgap", ""), this);
      builder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
          "TITLE", TitledBorder.CENTER , TitledBorder.ABOVE_TOP));
      
      // Create label
      String description = sw.getDescription();
      String name = sw.getCustomName();
      String label = Strings.isBlank(name)
          ? description
          : description + " \"" + name + "\"";

      // Create radio button
      internFPIEnabled  = new JRadioButton("Internal FPI Enabled"); 
      internFPIDisabled = new JRadioButton("Internal FPI Disabled");
      internFPIEnabled.setName(generateKeyEnable(fpiIndex));
      
      ButtonGroup group = new ButtonGroup();
      group.add(internFPIDisabled);
      group.add(internFPIEnabled);
      internFPIEnabled.setSelected(FPI_ENABLED_BY_DEFAULT);
      internFPIDisabled.setSelected(!FPI_ENABLED_BY_DEFAULT);
      
      
      builder.append(label + ":", internFPIEnabled, internFPIDisabled);
      builder.nextLine();
      
      // Create settings panel
      settingsPanel = new FPIFaultSettingsPanel(fpiIndex, templates, fpiChannel);
      builder.append(settingsPanel,6);
      
      initiEventHandling();
    }

    private void initiEventHandling() {
      // Hide fault settings panel if FPI not enabled
      settingsPanel.setVisible(FPI_ENABLED_BY_DEFAULT);
      ItemListener al = new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent arg0)  {
          settingsPanel.setVisible(internFPIEnabled.isSelected());
        }
      };
      internFPIEnabled.addItemListener(al);
      internFPIDisabled.addItemListener(al);
    }
    
    public void renderingPanel() {
      settingsPanel.rendereingPanel();
    }

    public void registerePersistentComponents(AbstractCommissioningStep parent) {
      parent.registerPersistentComponent(internFPIEnabled);
      settingsPanel.registerePersistentComponents(parent);
    }
    
    public String validateContents() {
      return settingsPanel.validateContents();
    }
  }
  
  
  /**
   * Generates a key for accessing or storing SwitchTemplate in WizardData.
   */
  static String generateKeyEnable(int fpiIndex) {
    return "FPIEnabled-" + fpiIndex;
  }
  
  /**
   * Generates a key for accessing or storing SwitchTemplate in WizardData.
   */
  static String generateKeyVisible(int fpiIndex) {
    return "FPIVisible-" + fpiIndex;
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();
    labelInfo = new JTextArea();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {
      scrollPane1.setBorder(null);
      scrollPane1.setViewportView(contentPanel);
    }
    add(scrollPane1, BorderLayout.CENTER);

    //---- labelInfo ----
    labelInfo.setText("No FPI needs to be configured because there is no switchgear enabled in the previous step.");
    labelInfo.setFont(UIManager.getFont("Label.font"));
    labelInfo.setEditable(false);
    labelInfo.setLineWrap(true);
    labelInfo.setOpaque(false);
    labelInfo.setWrapStyleWord(true);
    add(labelInfo, BorderLayout.PAGE_START);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
    
    UIUtils.increaseScrollSpeed(scrollPane1);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JPanel contentPanel;
  private JTextArea labelInfo;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
