/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.beans.PropertyVetoException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.rtu.commissioning.ui.wizard.panels.FPIFaultSettingsPanel;
import com.lucy.g3.rtu.config.channel.domain.IChannel;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.fpireset.FPIReset;
import com.lucy.g3.rtu.config.clogic.pltu.PLTULogic;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettingsPanel;
import com.lucy.g3.rtu.config.clogic.tcl.TCLTrippingSettingsPanel;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.clogic.voltagemismatch.VoltageMismatchLogic;
import com.lucy.g3.rtu.config.constants.SwitchIndex;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.utils.CANModuleUtility;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IPConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelCommsConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3ChannelTCPConf;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.Failover;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.connmgr.FailoverGroup;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolUtility;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate.ScadaTemplateType;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateCommsDev;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateCommsDev.CommsDeviceSelection;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateSerial;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplateTcp;
import com.lucy.g3.rtu.config.template.custom.sdnp3.TcpScadaAddress;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchChannelTemplate;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplate;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoDoubleBinaryPoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.PLTU_INPUT;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_POINT;
import com.lucy.g3.xml.gen.common.DNP3Enum.DNPLINK_NETWORK_TYPE;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.SERIAL_PORT;

/**
 * Result producer for commissioning wizard.
 */
class CommissioningResultProducer implements WizardResultProducer {

  private Logger log = Logger.getLogger(CommissioningResultProducer.class);

  private final IConfig data;


  public CommissioningResultProducer(IConfig data) {
    this.data = data;
  }

  @SuppressWarnings("rawtypes")
  private Map wizardData;


  @Override
  public Object finish(@SuppressWarnings("rawtypes") Map wizardData) throws WizardException {
    this.wizardData = wizardData;

    applyStepInitial();

    if (wizardData.get(StepSwitch.IS_CREATED) == Boolean.TRUE)
      applyStepSwitch();

    if (wizardData.get(StepFPI.KEY_IS_CREATED) == Boolean.TRUE)
      applyStepFPI();

    if (wizardData.get(StepScada.KEY_IS_CREATED) == Boolean.TRUE)
      applyStepScada();

    if (wizardData.get(StepFieldDevice.KEY_IS_CREATED) == Boolean.TRUE)
      applyStepPowerMeter();

    if (wizardData.get(StepPLTU.KEY_IS_CREATED) == Boolean.TRUE)
      applyStepPLTU();
    
    if (wizardData.get(StepTCL.KEY_ENABLE_TCL) != null)
      applyStepTCL();

    return wizardData;
  }
  

  private void applyStepTCL() {
    boolean enable = (Boolean) wizardData.get(StepTCL.KEY_ENABLE_TCL);
    
    TCLSensingSettingsPanel panelSensing 
      = (TCLSensingSettingsPanel) wizardData.get(StepTCL.KEY_SENSING_PANEL);
    TCLTrippingSettingsPanel panelTripping 
      = (TCLTrippingSettingsPanel) wizardData.get(StepTCL.KEY_TRIPPING_PANEL);
    if(panelSensing != null) {
      panelSensing.save();
      panelSensing.getSettings().getLogic().setEnabled(enable);
    }
    
    if(panelTripping != null) {
      panelTripping.save();
      panelTripping.getSettings().getOwnerLogic().setEnabled(enable);
    }
  }


  private void applyStepPowerMeter() {
    MMB mmb = (MMB) MasterProtocolUtility.getModbusMaster(data);
    if (mmb == null) {
      return;
    }

    Collection<MMBSession> sesns = mmb.getAllSessions();
    for (MMBSession sesn : sesns) {
      Object template = wizardData.get(StepFieldDevice.generateKey(sesn));
      if (template != null && template instanceof MMBTemplate) {
        ((MMBTemplate) template).applyTo(sesn, false);
        sesn.setEnabled(true);
      } else if (StepFieldDevice.TEXT_DISABLED.equals(template)) {
        sesn.setEnabled(false);
      } else {
        log.error("ModBus template is null for: " + sesn);
      }
    }
  }

  private void applyStepScada() {
    ScadaProtocolManager manager = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    SDNP3 dnp3 = (SDNP3) manager.getProtocol(ProtocolType.SDNP3);
    if (dnp3 == null) {
      return;
    }

    // Apply template
    Collection<SDNP3Channel> channels = dnp3.getAllChannels();
    for (SDNP3Channel channel : channels) {
      String key = StepScada.generateKey(channel);
      Object template = wizardData.get(key);
      if (template != null && template instanceof ScadaTemplate) {
        applyScadaTemplate(channel, (ScadaTemplate) template);
      }
    }

  }

  private void applyScadaTemplate(SDNP3Channel channel, ScadaTemplate template) {
    if(template.getType() == ScadaTemplateType.TCPIP) {
      applyScadaTemplate(channel, template.getTcp());
      
    } else if(template.getType() == ScadaTemplateType.COMMS) {
      applyScadaTemplate(channel, template.getCommsDev());
      
    } else if(template.getType() == ScadaTemplateType.SERIAL) {
      applyScadaTemplate(channel, template.getSerial());
      
    } else {
      log.error("Unexpected state!");
    }
  }

  private void applyScadaTemplate(SDNP3Channel channel, ScadaTemplateSerial serial) {
    SDNP3ChannelConf chnlConf = channel.getChannelConfig();

    // Force network type to serial
    if(chnlConf.getNetworkType() != DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_SERIAL) {
      chnlConf.setNetworkType(DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_SERIAL);
    }
    
    SERIAL_PORT portName = serial.getPort();
    if(portName != null) {
    try {
      PortsManager portMgr = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
      chnlConf.getSerialConf().setSerialPort(portMgr.getSerialManager().getSerialPort(portName.name()));
    } catch (PropertyVetoException e) {
      log.error("Failed to apply serial port template:"+e.getMessage());
    }}
  }


  private void applyScadaTemplate(SDNP3Channel channel, ScadaTemplateCommsDev template) {
    SDNP3ChannelConf chnlConf = channel.getChannelConfig();
    
    // Force network type to comms device
    if(chnlConf.getNetworkType() != DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_COMMS_DEVICE) {
      chnlConf.setNetworkType(DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_COMMS_DEVICE);
    }
    
    // Set comms device
    SDNP3ChannelCommsConf comms = chnlConf.getCommsConf();
    CommsDeviceSelection commsDevSelection = template.getCommsDeviceSelection();
    comms.setConnectOnClassMask(commsDevSelection.getConnectionClass());
    try {
      comms.getCommsDeviceSelection().setCommsDevice(commsDevSelection.getCommsDevice());
    } catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    
    // Set comms device config
    commsDevSelection.getCommsDevice().copyConfigFrom(template.getCommsDeivceConfig());
    
    // Set master address
    try{
      long masterAddress = Long.parseLong(template.getMasterAddress());
      Collection<SDNP3Session> allsesns = channel.getAllSessions();
      for (SDNP3Session sesn : allsesns) {
          sesn.getAddress().setDestination(masterAddress);
      }
    } catch(Throwable e) {
      log.error("Failed to apply master address:"+template.getMasterAddress()+" from template", e);
    }
  }

  private void applyScadaTemplate(SDNP3Channel channel, ScadaTemplateTcp template) {
    SDNP3ChannelConf chnlConf = channel.getChannelConfig();
    
    // Set network type to TCP
    DNPLINK_NETWORK_TYPE type = chnlConf.getNetworkType();
    if(type != DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_ONLY
        && type != DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_UDP) {
      chnlConf.setNetworkType(DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_ONLY);
    }
    
    // Set SCADA IP address
    Collection<TcpScadaAddress> addresses = template.getAllItems();
    if (!addresses.isEmpty()) {
      TcpScadaAddress[] addressArray = addresses.toArray(new TcpScadaAddress[addresses.size()]);

      // Set primary ip address
      SDNP3ChannelTCPConf tcp = chnlConf.getTcpConf();
      if (tcp != null) {
        tcp.setIpAddress(addressArray[0].getIp());
        long masterAddr = addressArray[0].getMasterAddressValue();
        if (masterAddr >= 0)
          tcp.setMasterAddress(masterAddr);
      }

      // Set failover IP
      FailoverGroup failover = chnlConf.getConnectionManager().getFailoverGroup();
      Collection<Failover> failovers = failover.getAllFailOvers();
      int index = 1;
      for (Failover fo : failovers) {
        if (index >= addressArray.length) {
          break;
        }

        fo.setIpAddress(addressArray[index].getIp());
        long masterAddr = addressArray[index].getMasterAddressValue();
        if (masterAddr >= 0)
          fo.setMasterAddress(masterAddr);
        index++;
      }

    }
  
  }



  private void applyStepPLTU() {
    final PLTULogic pltu = CommissioningWizard.findPLTU(data);
    final VoltageMismatchLogic vm = CommissioningWizard.findVolMismatch(data);
    
    StepPLTU pltuStep = (StepPLTU) wizardData.get(StepPLTU.STEP_ID);

    // Enable pltu logic if it exists. 
    pltu.setEnabled(pltuStep.isPLTUEnabled());
    
    // Disable voltage mismatch
    if(vm != null) {
      // Enable VM only when PLTU is not enabled.
      vm.setEnabled(pltuStep.isVoltMismatchEnabled() && !pltuStep.isPLTUEnabled());
    }
    
    // Configure the output switch of PLTU
    if(pltuStep.isPLTUEnabled()) {
      SwitchGearLogic cb = (SwitchGearLogic) pltuStep.getSelectedCB();
      
      if(cb == null)
        throw new IllegalStateException("No circuit breaker configured for PLTU");
      
      if(pltu.getOutputSwitch() != cb) {
        pltu.setOutputSwitch(cb);
        PseudoDoubleBinaryPoint cbPosition =  CLogicUtility.findDoubleBinaryPoint(
            cb.getDoubleBinaryPoints(), 
            SGL_POINT.SGL_POINT_STATUS);
        pltu.getInput(PLTU_INPUT.PLTU_INPUT_POSITION).setValue(cbPosition);
      }
    }
  }

  /*
   * Note: switch logic that controls the same output will be applied with the same template.
   */
  private void applyStepSwitch() {
    Collection<SwitchGearLogic> sgls = CommissioningWizard.findSwitchLogic(data);
    String key;

    // Get templates from wizard data
    HashMap<SwitchModuleOutput, SwitchTemplate> templates = new HashMap<>();
    for (SwitchGearLogic sgl : sgls) {
      key = StepSwitch.generateKey(sgl);
      Object template = wizardData.get(key);
      if (template != null && template instanceof SwitchTemplate) {
        templates.put(sgl.getOutputSCM(), (SwitchTemplate)template);
      }
    }
    
    // Apply templates
    SwitchTemplate template;
    for (SwitchGearLogic sgl : sgls) {
      template = templates.get(sgl.getOutputSCM());
      if (template != null) {
        applySwitchTemplate(sgl, (SwitchTemplate) template);
        sgl.setEnabled(true);
      } else if (StepSwitch.TEXT_DISABLED.equals(template)) {
        sgl.setEnabled(false);
      }
    }
  }

  private boolean checkFPIEnabled(int fpiIndex) {
    Object fpiEnabled = wizardData.get(StepFPI.generateKeyEnable(fpiIndex));
    Object fpiVisible = wizardData.get(StepFPI.generateKeyVisible(fpiIndex));
    return fpiEnabled == Boolean.TRUE && fpiVisible == Boolean.TRUE;
  }

  private void configureFPI(FPIConfig fpi, int index) {
    fpi.setCtratio(FPIFaultSettingsPanel.getCTRatio(wizardData, index));
    fpi.setSelfResetMin((long)FPIFaultSettingsPanel.getSelfResetTime(wizardData, index));
    
    fpi.earchFault.setInstantFaultCurrent((long) FPIFaultSettingsPanel.getEarthInstantFaultCurrent(wizardData, index));
    fpi.earchFault.setMinFaultDurationMs((long) FPIFaultSettingsPanel.getEarthFaultDuration(wizardData, index));
    fpi.earchFault.setTimedFaultCurrent((long) FPIFaultSettingsPanel.getEarthTimeFaultCurrent(wizardData, index));

    long val;
    val = (long) FPIFaultSettingsPanel.getEarthInstantFaultCurrent2(wizardData, index);
    fpi.sensitiveEarthFault.setInstantFaultCurrent(val);
    val = (long) FPIFaultSettingsPanel.getEarthFaultDuration2(wizardData, index);
    fpi.sensitiveEarthFault.setMinFaultDurationMs(val);
    val = (long) FPIFaultSettingsPanel.getEarthTimeFaultCurrent2(wizardData, index);
    fpi.sensitiveEarthFault.setTimedFaultCurrent(val);

    fpi.phaseFault.setInstantFaultCurrent((long) FPIFaultSettingsPanel.getPhaseInstantFaultCurrent(wizardData, index));
    fpi.phaseFault.setMinFaultDurationMs((long) FPIFaultSettingsPanel.getPhaseFaultDuration(wizardData, index));
    fpi.phaseFault.setTimedFaultCurrent((long) FPIFaultSettingsPanel.getPhaseTimeFaultCurrent(wizardData, index));
    
    IChannel resetChannel = fpi.getResetChannel();
    FPIReset logic = findFPIResetLogic(resetChannel);
    if(logic != null) {
      logic.getSettings().setResetOnLVLost(FPIFaultSettingsPanel.isResetWhenLost(wizardData, index));
      logic.getSettings().setResetOnLVRestored(FPIFaultSettingsPanel.isResetWhenRestored(wizardData, index));
    }
  }
  
  private FPIReset findFPIResetLogic(IChannel fpiResetCh) {
    CLogicManager logicMgr = data.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    
    Collection<ICLogic> allFpiLogic = logicMgr.getLogicByType(ICLogicType.FPI_RESET);
    for (ICLogic fpiLogic: allFpiLogic) {
      if(fpiLogic instanceof FPIReset) {
        IChannel outputCh = ((FPIReset)fpiLogic).getOutputChannel();
        if(outputCh != null && outputCh == fpiResetCh)
          return (FPIReset) fpiLogic;
      }
    }

    return null;
  }

  private void applyStepFPI() {
    FPIConfig[] fpiChnls = CANModuleUtility.getAllFPIChannels(data);

    for (int index = 0; index < fpiChnls.length; index++) {
      boolean enabled = checkFPIEnabled(index);
      fpiChnls[index].setEnabled(enabled);

      if (enabled) {
        configureFPI(fpiChnls[index], index);
      }
    }
  }

  private void applyStepInitial() {
    GeneralConfig general = data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
    PortsManager ports = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    ScadaProtocolManager scada = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    
    String siteName = (String) wizardData.get(StepInitial.KEY_SITE_NAME);
    general.setSiteName(siteName);

    IPConfig ipconf = ports.getEthPort(ETHERNET_PORT.ETHERNET_PORT_CONTROL).getIpConfig();
    String ip = (String) wizardData.get(StepInitial.KEY_ETH0_IP);
    String gateway = (String) wizardData.get(StepInitial.KEY_ETH0_GATEWAY);
    String mask = (String) wizardData.get(StepInitial.KEY_ETH0_MASK);
    ipconf.setIpAddress(ip);
    ipconf.setSubmask(mask);
    ipconf.setGateway(gateway);

    // Apply DNP3 RTU Address
    Collection<SDNP3Session> sesns = CommissioningWizard.findDNP3Sessions(data);
    boolean isSingleSession = sesns.size() == 1;
    for (SDNP3Session sesn : sesns) {
      try {
        String key = StepInitial.generateKeyForDNP3Address(sesn, isSingleSession);
        Long slaveAddr = Long.valueOf((String) wizardData.get(key));
        if (slaveAddr != null && slaveAddr > 0) {
          sesn.getAddress().setSource(slaveAddr);
        }
      } catch (PropertyVetoException e) {
        log.error("Failed to set DNP3 address", e);
      }
    }
  }

  private void applySwitchTemplate(SwitchGearLogic sgl, SwitchTemplate template) {
    sgl.getSettings().copyFrom(template);

    // Copy template to switch channels
    SwitchModuleOutput output = sgl.getOutputSCM();
    if (output == null) {
      log.error("Switch Moudule is not configured for logic:" + sgl);
      return;
    }

    ISwitchModule module = output.getModule();
    SwitchIndex index = output.getIndex();
    
    // Update channel
    applySwitchTemplate(template.getOpen(), (ICANChannel)module.getOpenChannel(index));
    applySwitchTemplate(template.getClose(), (ICANChannel)module.getCloseChannel(index));
    
    // Update polarity settings
    module.getSettings().setOpenPolarity(index, template.getOpen().getPolarity());
    module.getSettings().setClosePolarity(index, template.getClose().getPolarity());
    
    module.getSettings().setAllowForcedOperation(index, template.isAllowForcedOperation());
    module.getSettings().setMotorMode(index, template.getMotorMode());
  }

  private void applySwitchTemplate(SwitchChannelTemplate template, ICANChannel switchChannel) {
    if (switchChannel.hasParameter(ICANChannel.PARAM_PULSELENGTH))
      switchChannel.setParameter(ICANChannel.PARAM_PULSELENGTH, template.getPulseLength());

    if (switchChannel.hasParameter(ICANChannel.PARAM_OVERRUN_MS))
      switchChannel.setParameter(ICANChannel.PARAM_OVERRUN_MS, template.getOverrun());

    if (switchChannel.hasParameter(ICANChannel.PARAM_OPTIMEOUT_SEC))
      switchChannel.setParameter(ICANChannel.PARAM_OPTIMEOUT_SEC, template.getOperationTimeout());

    if (switchChannel.hasParameter(ICANChannel.PARAM_POLARITY))
      switchChannel.setParameter(ICANChannel.PARAM_POLARITY, template.getPolarity());
    
    if (switchChannel.hasParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS))
      switchChannel.setParameter(ICANChannel.PARAM_MOTOR_OVER_DRIVE_MS, template.getMotoOverDrive());
    
    if (switchChannel.hasParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS))
      switchChannel.setParameter(ICANChannel.PARAM_MOTOR_REVERSE_DRIVE_MS, template.getMotoReverseDrive());
  }

  @Override
  public boolean cancel(@SuppressWarnings("rawtypes") Map settings) {
    return true;
  }

}
