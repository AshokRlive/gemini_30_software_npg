/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.binding.binder.BeanBinder;
import com.jgoodies.binding.binder.Binders;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.rtu.commissioning.ui.task.ClearCommissioningCacheTask;
import com.lucy.g3.rtu.commissioning.ui.task.CommissioningTask;
import com.lucy.g3.rtu.commissioning.ui.wizard.CommissioningCache;
import com.lucy.g3.rtu.commissioning.ui.wizard.CommissioningWizard;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.validation.impl.IConfigValidator;

/**
 * The Class CommissioningPage.
 */
public class CommissioningPage extends AbstractPage{
  
  public static final String ACTION_RUN_COMMISSION = "runCommission";
  public static final String ACTION_RUN_COMMISSION_FROM_SCRATCH = "runCommissionFromScratch";
  public static final String ACTION_RERUN_COMMISSION_FROM_CACHE = "rerunCommissionFromCache";
  public static final String ACTION_RUN_COMMISSION_WITH_NEW_CONFIG = "runCommissionWithNewConfig";
  public static final String ACTION_CLEAR_CACHE = "clearCache";
  public static final String ACTION_TEST= "testCommissioning";
  
  public static final String PROPERTY_CACHED = "cached";
  
  private final ICommissioningInvoker invoker;
  
  private Logger log = Logger.getLogger(CommissioningPage.class);
  
  //private final CommissioningCache cache = new CommissioningCache();
  
  public CommissioningPage(ICommissioningInvoker invoker) {
    super(null);
    this.invoker = invoker;
    setNodeName("Wizard");
    
  }
  
  @Action
  public void runCommission() {
    if(checkConfigLoaded() == false) {
      runCommissionWithNewConfig();
      
    } else {
      if (isCached()) {
        rerunCommissionFromCache();
      } else{
        runCommissionFromScratch();
      }
    }
  }
  
  
  private boolean checkConfigLoaded() {
    IConfig config = invoker.getConfigData();
    return config != null;
  }

  private boolean checkConfigValid() {
    IConfig data = invoker.getConfigData();
    IConfigValidator validator = data.getConfigModule(IConfigValidator.CONFIG_MODULE_ID);
    validator.validate();
    
    return validator.isValid();
  }
  
  @Action
  public void runCommissionFromScratch() {
    performCommissioning(CommissioningMode.FROM_SCRATCH);
    log.warn("Running commissioningg from scratch");
  }
  
  @Action /*(enabledProperty = PROPERTY_CACHED)*/
  public void rerunCommissionFromCache() {
    if (!isCached()) {
      log.error("No commissioning history found!");
      return;
    }
      
    performCommissioning(CommissioningMode.FROM_CACHE);
    updateGuiStates();
    log.warn("Re-Running commissioningg with cache");
  }
  
  @Action
  public void testCommissioning(ActionEvent evt){
    // Check config exists
    if (checkConfigLoaded() == false) {
      log.error("No configuration found! Please load/read the configuration first!");
      return;
    }
    
    try{
      String command = evt.getActionCommand();
      CommissioningMode mode = CommissioningMode.valueOf(command);
      
      CommissioningCache cache = null;
      // Load cache
      if(mode == CommissioningMode.FROM_CACHE_TEST) {
        cache = CommissioningCache.readFromDefaultCacheFile(); 
      }
      
      // Run wizard
      CommissioningWizard.showWizardDialog(invoker.getConfigData(),cache);
    
    }catch(Throwable e){
      log.error("Unexpected error", e);
    }
  }
  
  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Action
  public void runCommissionWithNewConfig() {
    Task<?,?> importConfigTask = invoker.createConfigImportTask();
    TaskListener listener = new TaskListener.Adapter<Void,Void>() {
      @Override
      public void succeeded(TaskEvent<Void> event) {
        runCommission();
        updateGuiStates();
      }
    };
    importConfigTask.addTaskListener(listener);
    invoker.getApp().getContext().getTaskService().execute(importConfigTask);
    log.warn("Running commissioningg with new config");
  }
  
  @Action /*(enabledProperty = PROPERTY_CACHED)*/
  public void clearCache() {
    // Delete cache from local
    CommissioningCache.DEFAULT_CACHE_FILE.delete();
    
    // Delete cache from remote RTU
    invoker.getG3TaskService().execute(
        new ClearCommissioningCacheTask(invoker.getApp(), invoker.getCommissioningCMD()));
    
    
    updateGuiStates();
    log.warn("Cleared commissioningg cache");
  }
  
  
  @Override
  protected void init() throws Exception {
    initComponents();
    initComponentsBindings();
  }
  
  private void initComponentsBindings() {
    BeanBinder binder1 = Binders.binderFor(invoker.getRTUInfo());
    binder1.bindProperty(RTUInfo.PROPERTY_CONFIG_STATUS).to(lblConfigVersion);
  }

  @Override
  protected void renderingPage() {
    updateGuiStates();
  }

  public boolean isCached() {
    return CommissioningCache.DEFAULT_CACHE_FILE.exists();
  }
  
  private void updateGuiStates() {
    firePropertyChange(PROPERTY_CACHED, null, isCached());
  }

  private String checkCommissioningConditions() {
    // Check connect state
    ConnectionState currentState = invoker.getConnectionState();
    if (currentState == ConnectionState.DISCONNECTED) {
      return "No RTU connected! Please connect to a RTU first!";
    }
    
    // Check RTU running mode
    if (currentState != ConnectionState.CONNECTED_TO_G3_APP) {
      return "The RTU is not running in normal mode!";
    }
    
    // Check API
    String err = G3ConfigAPI.checkEquals(G3ConfigAPI.CURRENT_API, invoker.getG3ConfigAPI());
    if(err != null) {
      return err = "<p>The following API is detected incompatible between ConfigTool and RTU. "
          + "You might need to upgrade the firmware of the RTU.</p><br>"+err;
    }

    // Check config exists
    if (checkConfigLoaded() == false) {
      return "No configuration found! Please load/read the configuration first!";      
    }
    
    // Check config data valid
    if (checkConfigValid() == false) {
      return "There are some errors in the configuration! Please go to \"Configuration\" and fix them first!";      
    }
    
    
    return null;
  }
  
  private void performCommissioning(CommissioningMode mode) {
    String error = checkCommissioningConditions();
    if (error != null) {
      // Show error message
      ErrorInfo errInfo = new ErrorInfo("Failure", 
          "Failed to start commissioning! Please see details.", error,
          null, null, Level.SEVERE, null);
      JXErrorPane.showDialog(this, errInfo);
      return;
    }
    
    CommissioningCache cache = null;
    switch (mode) {
    case FROM_CACHE:
      cache = CommissioningCache.readFromDefaultCacheFile();
      break;
      
    case FROM_SCRATCH:
      break;
      
    default:
      break;
    }
    
    // Run wizard
    if (CommissioningWizard.showWizardDialog(invoker.getConfigData(),cache)) {
      CommissioningTask task = new CommissioningTask(invoker.getApp(), invoker, cache);
      invoker.getApp().getContext().getTaskService().execute(task);
    }
  }
  
  private void createUIComponents() {
    ApplicationActionMap actionMap = invoker.getApp().getContext().getActionMap(this);
    
//    // ====== Create split button "New" ======
//    JSplitButton splitBtn = new JSplitButton();
//    
//    // Create default action
//    splitBtn.addSplitButtonActionListener(new SplitButtonActionAdapter(actionMap.get(ACTION_RUN_COMMISSION)));
//    
//    // Create drop-down menu 
//    JPopupMenu popup = new JPopupMenu();
//    popup.add(actionMap.get(ACTION_RUN_COMMISSION_FROM_SCRATCH));
//    popup.add(actionMap.get(ACTION_RUN_COMMISSION_WITH_NEW_CONFIG));
//    popup.add(actionMap.get(ACTION_RERUN_COMMISSION_FROM_CACHE));
//    popup.addSeparator();
//    popup.add(actionMap.get(ACTION_CLEAR_CACHE));
//    splitBtn.setPopupMenu(popup);
//    btnCommissioning = splitBtn; 
    
    btnCommissioning = new JButton(actionMap.get(ACTION_RUN_COMMISSION));
    btnScratch = new JButton(actionMap.get(ACTION_RUN_COMMISSION_FROM_SCRATCH));
    
    // Mouse listener for showing hidden test menu.
    MouseAdapter ma = new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        
        if (e.isAltDown() && e.isControlDown() && SwingUtilities.isRightMouseButton(e)) {
          ApplicationActionMap actions = invoker.getApp().getContext().getActionMap(CommissioningPage.this);
          JPopupMenu popmenu = new JPopupMenu();
          JMenuItem item = popmenu.add(actions.get(ACTION_TEST));
          item.setActionCommand(CommissioningMode.FROM_SCRATCH_TEST.name());
          item.setText("Test \"From Scratch\"");
          
          item = popmenu.add(actions.get(ACTION_TEST));
          item.setActionCommand(CommissioningMode.FROM_CACHE_TEST.name());
          item.setText("Test \"From Cache\"");
          
          popmenu.show((Component) e.getSource(), e.getX(), e.getY());
        }
      }
    };
    btnScratch.addMouseListener(ma);
    btnCommissioning.addMouseListener(ma);
   
  }
  

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    panel2 = new JPanel();
    label2 = new JLabel();
    lblConfigVersion = new JLabel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label3 = new JLabel();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new FormLayout(
        "default:grow",
        "2*(default, $ugap), default"));

    //======== panel2 ========
    {
      panel2.setBorder(new CompoundBorder(
          new TitledBorder("RTU Information"),
          Borders.DLU2_BORDER));
      panel2.setLayout(new FormLayout(
          "default, $lcgap, default",
          "fill:default"));

      //---- label2 ----
      label2.setText("Current RTU Configuration File Version:");
      panel2.add(label2, CC.xy(1, 1));
      panel2.add(lblConfigVersion, CC.xy(3, 1));
    }
    add(panel2, CC.xy(1, 1));

    //======== panel1 ========
    {
      panel1.setBorder(new CompoundBorder(
          new TitledBorder("Commissioning"),
          Borders.DLU2_BORDER));
      panel1.setLayout(new FormLayout(
          "[100dlu,default], $rgap, default:grow",
          "fill:default, $ugap, default"));

      //---- btnCommissioning ----
      btnCommissioning.setText("Start Commissioning...");
      panel1.add(btnCommissioning, CC.xy(1, 1));

      //---- label1 ----
      label1.setText("Start commissioning process with previous settings.");
      panel1.add(label1, CC.xy(3, 1));

      //---- btnScratch ----
      btnScratch.setText("Start From Scratch...");
      panel1.add(btnScratch, CC.xy(1, 3));

      //---- label3 ----
      label3.setText("Start commissioning process from scratch.");
      panel1.add(label3, CC.xy(3, 3));
    }
    add(panel1, CC.xy(1, 3));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel2;
  private JLabel label2;
  private JLabel lblConfigVersion;
  private JPanel panel1;
  private JButton btnCommissioning;
  private JLabel label1;
  private JButton btnScratch;
  private JLabel label3;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  public boolean hasError() {
    return false;
  }
}
