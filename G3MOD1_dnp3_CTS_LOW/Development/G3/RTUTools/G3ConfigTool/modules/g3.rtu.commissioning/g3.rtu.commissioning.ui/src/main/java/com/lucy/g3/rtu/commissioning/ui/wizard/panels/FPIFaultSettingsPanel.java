/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard.panels;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.LabelledFTFSupport;
import com.lucy.g3.rtu.commissioning.ui.wizard.AbstractCommissioningStep;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigEarthFault;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfigFault.FPIConfigPhaseFault;
import com.lucy.g3.rtu.config.module.canmodule.ui.widgets.CTRatioEditorCombox;
import com.lucy.g3.rtu.config.template.custom.fpi.FPIChannelTemplate;
import com.lucy.g3.rtu.config.template.custom.fpi.FPIChannelTemplates;

/**
 * The Class FPIFaultSettingsPanel.
 */
public class FPIFaultSettingsPanel extends JPanel {
  private static final String KEY_TEMPLATE = "FPIFaultSettingsPanel.Profile";   
  private static final String KEY_CT_RATIO = "FPIFaultSettingsPanel.CTRatio";   
  private static final String KEY_SELF_RESET_TIME = "FPIFaultSettingsPanel.selfResetTime";
  private static final String KEY_RESET_WHEN_RESTORED = "FPIFaultSettingsPanel.resetWhenRestored";
  private static final String KEY_RESET_WHEN_LOST = "FPIFaultSettingsPanel.resetWhenLost";
  
  private static final String TEXT_CUSTOM_PROFILE = "- Custom -";
  
  private static final String KEY_EARTH_TIMEFAULTCURRENT    = "FPIFaultSettingsPanel.earthTimeFaultCurrent   ";   
  private static final String KEY_EARTH_INSTANTFAULTCURRENT = "FPIFaultSettingsPanel.earthInstantFaultCurrent";   
  private static final String KEY_EARTH_FAULTDURATION       = "FPIFaultSettingsPanel.earthFaultDuration      ";
  private static final String KEY_EARTH_TIMEFAULTCURRENT2    = "FPIFaultSettingsPanel.earthTimeFaultCurrent2  ";   
  private static final String KEY_EARTH_INSTANTFAULTCURRENT2 = "FPIFaultSettingsPanel.earthInstantFaultCurrent2";   
  private static final String KEY_EARTH_FAULTDURATION2       = "FPIFaultSettingsPanel.earthFaultDuration2    ";         
  private static final String KEY_PHASE_FAULTDURATION       = "FPIFaultSettingsPanel.phaseFaultDuration      ";   
  private static final String KEY_PHASE_TIMEFAULTCURRENT    = "FPIFaultSettingsPanel.phaseTimeFaultCurrent   ";   
  private static final String KEY_PHASE_INSTANTFAULTCURRENT = "FPIFaultSettingsPanel.phaseInstantFaultCurrent";   
  
  private final int fpiIndex;
  private final FPIChannelTemplates templates;
  
  public FPIFaultSettingsPanel(int fpiIndex, FPIChannelTemplates templates, FPIConfig config) {
    this.fpiIndex = fpiIndex;
    this.templates = templates;
    initComponents();
    initComponentNames();
    initDefaultValues(config);
    decorateFields();
    refreshEnableStates(comboBoxTemplate.getSelectedItem());
  }
  
  private void initDefaultValues(FPIConfig config) {
    FPIConfigPhaseFault phaseFault = config.phaseFault;
    FPIConfigEarthFault earthFault = config.earchFault;
    FPIConfigEarthFault sensitiveEarchFault = config.sensitiveEarthFault;
    
    selfResetTime           .setValue(config.getSelfResetMin());
    comboBoxCTRatio.getEditor().setItem(config.getCtratio());
    
    earthTimeFaultCurrent   .setValue(earthFault.getTimedFaultCurrent());
    earthInstantFaultCurrent.setValue(earthFault.getInstantFaultCurrent());
    earthFaultDuration      .setValue(earthFault.getMinFaultDurationMs());
    earthTimeFaultCurrent2   .setValue(sensitiveEarchFault.getTimedFaultCurrent());
    earthInstantFaultCurrent2.setValue(sensitiveEarchFault.getInstantFaultCurrent());
    earthFaultDuration2      .setValue(sensitiveEarchFault.getMinFaultDurationMs());
    phaseFaultDuration      .setValue(phaseFault.getMinFaultDurationMs());
    phaseTimeFaultCurrent   .setValue(phaseFault.getTimedFaultCurrent());
    phaseInstantFaultCurrent.setValue(phaseFault.getInstantFaultCurrent());
  }

  public void registerePersistentComponents(AbstractCommissioningStep parent) {
    parent.registerPersistentComponent(comboBoxTemplate        );
    parent.registerPersistentComponent(checkBoxResetWhenLost    );
    parent.registerPersistentComponent(checkBoxResetWhenRestored);
    parent.registerPersistentComponent(comboBoxCTRatio.getTextField());
    parent.registerPersistentComponent(selfResetTime           );
    parent.registerPersistentComponent(earthTimeFaultCurrent   );
    parent.registerPersistentComponent(earthInstantFaultCurrent);
    parent.registerPersistentComponent(earthFaultDuration      );
    parent.registerPersistentComponent(earthTimeFaultCurrent2   );
    parent.registerPersistentComponent(earthInstantFaultCurrent2);
    parent.registerPersistentComponent(earthFaultDuration2      );
    parent.registerPersistentComponent(phaseFaultDuration      );
    parent.registerPersistentComponent(phaseTimeFaultCurrent   );
    parent.registerPersistentComponent(phaseInstantFaultCurrent);
  }
  
  public String validateContents() {
    if (isVisible() == false)
      return null;

    if (!checkFieldNotNull(selfResetTime)
      || !checkFieldNotNull(earthTimeFaultCurrent)
      || !checkFieldNotNull(earthInstantFaultCurrent)
      || !checkFieldNotNull(earthFaultDuration      )
      || !checkFieldNotNull(earthTimeFaultCurrent2)
      || !checkFieldNotNull(earthInstantFaultCurrent2)
      || !checkFieldNotNull(earthFaultDuration2     )
      || !checkFieldNotNull(phaseFaultDuration      )
      || !checkFieldNotNull(phaseTimeFaultCurrent   )
      || !checkFieldNotNull(phaseInstantFaultCurrent))
      return "Invalid FPI settings";
    
    if(comboBoxCTRatio.getSelectedItem() == null)
      return "Invalid CT Ratio setting";
    
    return null;
  }
  
  
  private boolean checkFieldNotNull(JFormattedTextField field) {
    if (field.getValue() == null) {
      ValidationComponentUtils.setErrorBackground(field);
      return false;
    } else {
      field.setBackground(Color.WHITE);
      return true;
    }
  }
  
  /* Customise the label of fields. */
  private void decorateFields() {
    LabelledFTFSupport.decorateWithLabel(earthFaultDuration,       0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(earthInstantFaultCurrent, 0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(earthTimeFaultCurrent,    0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(earthInstantFaultCurrent2,0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(earthTimeFaultCurrent2,   0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(earthFaultDuration2,      0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(phaseInstantFaultCurrent, 0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
    LabelledFTFSupport.decorateWithLabel(phaseTimeFaultCurrent,    0L, LabelledFTFSupport.DEFAULT_LABEL_DISABLED);
  }
  
  /* Name components for wizard style access */
  private void initComponentNames() {
    comboBoxTemplate        .setName(generateKey(KEY_TEMPLATE                  ));
    selfResetTime           .setName(generateKey(KEY_SELF_RESET_TIME          ));
    comboBoxCTRatio.getTextField().setName(generateKey(KEY_CT_RATIO));
    checkBoxResetWhenLost.setName(generateKey(KEY_RESET_WHEN_LOST));
    checkBoxResetWhenRestored.setName(generateKey(KEY_RESET_WHEN_RESTORED));
    
    earthTimeFaultCurrent   .setName(generateKey(KEY_EARTH_TIMEFAULTCURRENT   ));
    earthInstantFaultCurrent.setName(generateKey(KEY_EARTH_INSTANTFAULTCURRENT));
    earthFaultDuration      .setName(generateKey(KEY_EARTH_FAULTDURATION      ));
    earthTimeFaultCurrent2   .setName(generateKey(KEY_EARTH_TIMEFAULTCURRENT2   ));
    earthInstantFaultCurrent2.setName(generateKey(KEY_EARTH_INSTANTFAULTCURRENT2));
    earthFaultDuration2      .setName(generateKey(KEY_EARTH_FAULTDURATION2      ));
    phaseFaultDuration      .setName(generateKey(KEY_PHASE_FAULTDURATION      ));
    phaseTimeFaultCurrent   .setName(generateKey(KEY_PHASE_TIMEFAULTCURRENT   ));
    phaseInstantFaultCurrent.setName(generateKey(KEY_PHASE_INSTANTFAULTCURRENT));
    
  }

  private void createUIComponents() {
    comboBoxTemplate = new JComboBox<>(ArrayUtils.addAll(
        templates.getAllItems().toArray(), new Object[]{TEXT_CUSTOM_PROFILE}));
    comboBoxTemplate.setSelectedItem(null);
    comboBoxCTRatio = new CTRatioEditorCombox(FPIConfig.getDefaultCTRatio());
  }
  
  private void refreshEnableStates(Object selectedTemplate) {
    boolean enabled = EqualsUtil.areEqual(TEXT_CUSTOM_PROFILE, selectedTemplate);
    comboBoxCTRatio.setEnabled(enabled);
    checkBoxResetWhenRestored.setEnabled(enabled);
    checkBoxResetWhenLost.setEnabled(enabled);
    selfResetTime.setEnabled(enabled);
    earthFaultDuration.setEnabled(enabled);
    earthInstantFaultCurrent.setEnabled(enabled);
    earthTimeFaultCurrent.setEnabled(enabled);
    earthFaultDuration2.setEnabled(enabled);
    earthInstantFaultCurrent2.setEnabled(enabled);
    earthTimeFaultCurrent2.setEnabled(enabled);
    phaseFaultDuration.setEnabled(enabled);
    phaseInstantFaultCurrent.setEnabled(enabled);
    phaseTimeFaultCurrent.setEnabled(enabled);
    
  }

  private void applyTemplate(FPIChannelTemplate template) {
    selfResetTime.setValue(template.getSelfResetMin());
    comboBoxCTRatio.getEditor().setItem(template.getCtratio());
    checkBoxResetWhenRestored.setSelected(template.isResetOnLVRestored());
    checkBoxResetWhenLost.setSelected(template.isResetOnLVLost());
    
    earthFaultDuration        .setValue(template.earchFault.getMinFaultDurationMs());
    earthInstantFaultCurrent  .setValue(template.earchFault.getInstantFaultCurrent());
    earthTimeFaultCurrent     .setValue(template.earchFault.getTimedFaultCurrent());
    earthFaultDuration2        .setValue((template.sensitiveEarthFault.getMinFaultDurationMs()));
    earthInstantFaultCurrent2  .setValue((template.sensitiveEarthFault.getInstantFaultCurrent()));
    earthTimeFaultCurrent2     .setValue((template.sensitiveEarthFault.getTimedFaultCurrent()));

    phaseFaultDuration        .setValue(template.phaseFault.getMinFaultDurationMs()); 
    phaseInstantFaultCurrent  .setValue(template.phaseFault.getInstantFaultCurrent());
    phaseTimeFaultCurrent     .setValue(template.phaseFault.getTimedFaultCurrent());  
  }

  private void comboBoxTemplateItemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      Object item = e.getItem();
      if (item != null && item instanceof FPIChannelTemplate) {
        applyTemplate((FPIChannelTemplate) item);
      }
      
      refreshEnableStates(item);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label13 = new JLabel();
    checkBoxResetWhenRestored = new JCheckBox();
    checkBoxResetWhenLost = new JCheckBox();
    label14 = new JLabel();
    selfResetTime = new JFormattedTextField();
    JLabel label15 = new JLabel();
    JLabel label16 = new JLabel();
    earthTimeFaultCurrent = new JFormattedTextField();
    JLabel label12 = new JLabel();
    earthInstantFaultCurrent = new JFormattedTextField();
    phaseFaultDuration = new JFormattedTextField();
    phaseTimeFaultCurrent = new JFormattedTextField();
    JLabel label4 = new JLabel();
    JLabel label2 = new JLabel();
    JLabel label9 = new JLabel();
    JLabel label3 = new JLabel();
    JXTitledSeparator titleSeperator2 = new JXTitledSeparator();
    JLabel label6 = new JLabel();
    JLabel label7 = new JLabel();
    titleSeperator = new JXTitledSeparator();
    phaseInstantFaultCurrent = new JFormattedTextField();
    JLabel label11 = new JLabel();
    JLabel label8 = new JLabel();
    JLabel label1 = new JLabel();
    earthFaultDuration = new JFormattedTextField();
    JLabel label10 = new JLabel();
    titleSeperator3 = new JXTitledSeparator();
    JLabel label17 = new JLabel();
    earthFaultDuration2 = new JFormattedTextField();
    JLabel label18 = new JLabel();
    JLabel label21 = new JLabel();
    earthTimeFaultCurrent2 = new JFormattedTextField();
    JLabel label19 = new JLabel();
    JLabel label22 = new JLabel();
    earthInstantFaultCurrent2 = new JFormattedTextField();
    JLabel label20 = new JLabel();
    JLabel label5 = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
      "right:default, $lcgap, [80dlu,default], $lcgap, default, $lcgap, default:grow",
      "default, $rgap, 2*(default, $lgap), 2*(default, $rgap), 3*(default, $lgap), default, $ugap, 3*(default, $lgap), default, $ugap, 3*(default, $lgap), default"));

    //---- label13 ----
    label13.setText("FPI Profile:");
    add(label13, CC.xy(1, 1));

    //---- comboBoxTemplate ----
    comboBoxTemplate.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        comboBoxTemplateItemStateChanged(e);
      }
    });
    add(comboBoxTemplate, CC.xywh(3, 1, 5, 1));

    //---- checkBoxResetWhenRestored ----
    checkBoxResetWhenRestored.setText("Reset FPI when LV is restored");
    checkBoxResetWhenRestored.setVisible(false);
    add(checkBoxResetWhenRestored, CC.xywh(3, 3, 5, 1));

    //---- checkBoxResetWhenLost ----
    checkBoxResetWhenLost.setText("Reset FPI when LV is lost");
    checkBoxResetWhenLost.setVisible(false);
    add(checkBoxResetWhenLost, CC.xywh(3, 5, 5, 1));

    //---- label14 ----
    label14.setText("Self Reset Time:");
    add(label14, CC.xy(1, 7));
    add(selfResetTime, CC.xy(3, 7));

    //---- label15 ----
    label15.setText("mins");
    add(label15, CC.xy(5, 7));

    //---- label16 ----
    label16.setText("CT Ratio:");
    add(label16, CC.xy(1, 9));
    add(comboBoxCTRatio, CC.xy(3, 9));
    add(earthTimeFaultCurrent, CC.xy(3, 15));

    //---- label12 ----
    label12.setText("A");
    add(label12, CC.xy(5, 33));
    add(earthInstantFaultCurrent, CC.xy(3, 17));
    add(phaseFaultDuration, CC.xy(3, 29));
    add(phaseTimeFaultCurrent, CC.xy(3, 31));

    //---- label4 ----
    label4.setText("ms");
    add(label4, CC.xy(5, 13));

    //---- label2 ----
    label2.setText("Timed Fault Current:");
    add(label2, CC.xy(1, 15));

    //---- label9 ----
    label9.setText("A");
    add(label9, CC.xy(5, 15));

    //---- label3 ----
    label3.setText("Instant Fault Current:");
    add(label3, CC.xy(1, 17));

    //---- titleSeperator2 ----
    titleSeperator2.setTitle("Phase Fault");
    add(titleSeperator2, CC.xywh(1, 27, 7, 1));

    //---- label6 ----
    label6.setText("ms");
    add(label6, CC.xy(5, 29));

    //---- label7 ----
    label7.setText("Timed Fault Current:");
    add(label7, CC.xy(1, 31));

    //---- titleSeperator ----
    titleSeperator.setTitle("Earth Fault");
    add(titleSeperator, CC.xywh(1, 11, 7, 1));
    add(phaseInstantFaultCurrent, CC.xy(3, 33));

    //---- label11 ----
    label11.setText("A");
    add(label11, CC.xy(5, 31));

    //---- label8 ----
    label8.setText("Instant Fault Current:");
    add(label8, CC.xy(1, 33));

    //---- label1 ----
    label1.setText("Minimum Fault Duration:");
    add(label1, CC.xy(1, 13));
    add(earthFaultDuration, CC.xy(3, 13));

    //---- label10 ----
    label10.setText("A");
    add(label10, CC.xy(5, 17));

    //---- titleSeperator3 ----
    titleSeperator3.setTitle("Sensitive Earth Fault");
    add(titleSeperator3, CC.xywh(1, 19, 7, 1));

    //---- label17 ----
    label17.setText("Minimum Fault Duration:");
    add(label17, CC.xy(1, 21));
    add(earthFaultDuration2, CC.xy(3, 21));

    //---- label18 ----
    label18.setText("ms");
    add(label18, CC.xy(5, 21));

    //---- label21 ----
    label21.setText("Timed Fault Current:");
    add(label21, CC.xy(1, 23));
    add(earthTimeFaultCurrent2, CC.xy(3, 23));

    //---- label19 ----
    label19.setText("A");
    add(label19, CC.xy(5, 23));

    //---- label22 ----
    label22.setText("Instant Fault Current:");
    add(label22, CC.xy(1, 25));
    add(earthInstantFaultCurrent2, CC.xy(3, 25));

    //---- label20 ----
    label20.setText("A");
    add(label20, CC.xy(5, 25));

    //---- label5 ----
    label5.setText("Minimum Fault Duration:");
    add(label5, CC.xy(1, 29));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  public static FPIChannelTemplate getFPIProfile(Map wizardData, int fpiIndex) {
    Object profile = wizardData.get(generateKey(fpiIndex , KEY_TEMPLATE));
    if(profile != null && profile instanceof FPIChannelTemplate)
      return (FPIChannelTemplate)profile;
    else
      return null;
  }
  
  public static Object getSelfResetTime(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex, KEY_SELF_RESET_TIME));
  }
  
  public static CTRatio getCTRatio(Map wizardData, int fpiIndex) {
    return (CTRatio) wizardData.get(generateKey(fpiIndex , KEY_CT_RATIO));
  }
  
  public static Object getEarthTimeFaultCurrent(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex, KEY_EARTH_TIMEFAULTCURRENT));
  }
  
  public static Object getEarthTimeFaultCurrent2(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex, KEY_EARTH_TIMEFAULTCURRENT2));
  }
  
  public static Object getEarthInstantFaultCurrent2(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex,KEY_EARTH_INSTANTFAULTCURRENT2));
  }
  
  public static Object getEarthFaultDuration2(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex, KEY_EARTH_FAULTDURATION2));
  }


  /**
   * Generates a wizard data key for storing/reading a component's value in cache.
   */
  private String generateKey(String key) {
    return generateKey(fpiIndex,key);
  }
  
  /**
   * Generates a wizard data key for storing/reading a component's value in cache.
   */
  private static String generateKey(int index, String key) {
    return index + key;
  }
  
  public static Boolean isResetWhenLost(Map wizardData, int fpiIndex) {
    return (Boolean) wizardData.get(generateKey(fpiIndex,KEY_RESET_WHEN_LOST));
  }
  
  public static Boolean isResetWhenRestored(Map wizardData, int fpiIndex) {
    return (Boolean) wizardData.get(generateKey(fpiIndex,KEY_RESET_WHEN_RESTORED));
  }
  
  public static Object getEarthInstantFaultCurrent(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex,KEY_EARTH_INSTANTFAULTCURRENT));
  }

  public static Object getEarthFaultDuration(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex, KEY_EARTH_FAULTDURATION));
  }

  public static Object getPhaseFaultDuration(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex,KEY_PHASE_FAULTDURATION));
  }

  public static Object getPhaseTimeFaultCurrent(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex , KEY_PHASE_TIMEFAULTCURRENT));
  }

  public static Object getPhaseInstantFaultCurrent(Map wizardData, int fpiIndex) {
    return wizardData.get(generateKey(fpiIndex , KEY_PHASE_INSTANTFAULTCURRENT));
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label13;
  private JComboBox<Object> comboBoxTemplate;
  private JCheckBox checkBoxResetWhenRestored;
  private JCheckBox checkBoxResetWhenLost;
  private JLabel label14;
  private JFormattedTextField selfResetTime;
  private CTRatioEditorCombox comboBoxCTRatio;
  private JFormattedTextField earthTimeFaultCurrent;
  private JFormattedTextField earthInstantFaultCurrent;
  private JFormattedTextField phaseFaultDuration;
  private JFormattedTextField phaseTimeFaultCurrent;
  private JXTitledSeparator titleSeperator;
  private JFormattedTextField phaseInstantFaultCurrent;
  private JFormattedTextField earthFaultDuration;
  private JXTitledSeparator titleSeperator3;
  private JFormattedTextField earthFaultDuration2;
  private JFormattedTextField earthTimeFaultCurrent2;
  private JFormattedTextField earthInstantFaultCurrent2;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  public void rendereingPanel() {
    // Nothing to do.
  }
}
