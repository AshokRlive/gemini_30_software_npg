/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard.panels;

import java.awt.Color;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.config.port.eth.IPConfig;

/**
 * The panel for configuring IP in a wizard.
 */
public final class IPConfigPanel extends JPanel {

  public static final String KEY_ADDRESS = "IPConfigPanel.ADDR";
  public static final String KEY_GATEWAY = "IPConfigPanel.GATE";
  public static final String KEY_SUBMASK = "IPConfigPanel.MASK";


  public IPConfigPanel() {
    initComponents();
    initComponentNames();
  }

  public void loadDefault(IPConfig defaultIPConfig) {
    String ip = defaultIPConfig.getIpAddress();
    String gateway = defaultIPConfig.getGateway();
    String mask = defaultIPConfig.getSubmask();
    
    tfAddr.setText(ip);
    tfGateway.setText(gateway);
    tfSubmask.setText(mask);
  }

  
  /* Name components for wizard style access */
  private void initComponentNames() {
    tfAddr.setName(KEY_ADDRESS);
    tfSubmask.setName(KEY_SUBMASK);
    tfGateway.setName(KEY_GATEWAY);
  }
  
  /* Validation */
  public String validateContents() {
    String result = null;
    final String ip = tfAddr.getText();
    final String mask = tfSubmask.getText();
    final String gateway = tfGateway.getText();

    ValidationComponentUtils.setMandatoryBackground(tfAddr);
    ValidationComponentUtils.setMandatoryBackground(tfSubmask);
    tfGateway.setBackground(Color.white); // Optional

    if (!IPAddress.validateIPAddress(gateway, true)) {
      result = "Please input a valid Gateway";
      ValidationComponentUtils.setErrorBackground(tfGateway);
    }

    if (!IPAddress.validateIPAddress(mask)) {
      result = "Please input a valid Submask";
      ValidationComponentUtils.setErrorBackground(tfSubmask);
    }

    if (!IPAddress.validateIPAddress(ip)) {
      result = "Please input a valid IP Address";
      ValidationComponentUtils.setErrorBackground(tfAddr);
    }

    if (result != null) {
      return result;
    }

    // Checks gateway and IP address are in the same sub net.
    try {
      IPAddress ipAddr = IPAddress.valueOf(ip);
      IPAddress maskAddr = IPAddress.valueOf(mask);
      IPAddress gatewayAddr = IPAddress.valueOf(gateway, true);
      ipAddr = IPAddress.applySubMask(ipAddr, maskAddr);
      gatewayAddr = IPAddress.applySubMask(gatewayAddr, maskAddr);

      if (ipAddr.equals(gatewayAddr) == false) {
        result = String.format("Gateway\"%s\" and IP \"%s\" are not in the same sub net\"%s\"",
            gateway, ip, mask);
      }
    } catch (ParseException e) {
      // IP parsing failed, do not check gateway
    }

    return result;
  }

  public JTextField getTfGateway() {
    return tfGateway;
  }

  public JTextField getTfSubmask() {
    return tfSubmask;
  }

  public JTextField getTfAddr() {
    return tfAddr;
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    label5 = new JLabel();
    label1 = new JLabel();
    label4 = new JLabel();
    tfGateway = new JTextField();
    tfSubmask = new JTextField();
    tfAddr = new JTextField();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, [80dlu,default]",
        "2*(default, $lgap), default"));

    //---- label5 ----
    label5.setText("IP Address:");
    add(label5, CC.xy(1, 1, CC.LEFT, CC.DEFAULT));

    //---- label1 ----
    label1.setText("Submask:");
    add(label1, CC.xy(1, 3, CC.LEFT, CC.DEFAULT));

    //---- label4 ----
    label4.setText("Gateway:");
    add(label4, CC.xy(1, 5));
    add(tfGateway, CC.xy(3, 5));
    add(tfSubmask, CC.xy(3, 3));
    add(tfAddr, CC.xy(3, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label5;
  private JLabel label1;
  private JLabel label4;
  private JTextField tfGateway;
  private JTextField tfSubmask;
  private JTextField tfAddr;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
