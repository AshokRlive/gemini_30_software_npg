/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;

import javax.swing.JTextPane;
import javax.swing.UIManager;

import org.netbeans.spi.wizard.WizardPage;

/**
 * Last Wizard Page.
 */
class StepLast extends WizardPage {

  public StepLast() {
    super("Finish");
    initComponents();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    taMessage = new JTextPane();

    //======== this ========
    setLayout(new BorderLayout());

    //---- taMessage ----
    taMessage.setEditable(false);
    taMessage.setOpaque(false);
    taMessage.setText("The new configuration will be applied then RTU will be restarted.\n\nPress \"Finish\" to proceed or \"Cancel\" to abort commissioning.");
    taMessage.setFont(UIManager.getFont("Label.font"));
    add(taMessage, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTextPane taMessage;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
