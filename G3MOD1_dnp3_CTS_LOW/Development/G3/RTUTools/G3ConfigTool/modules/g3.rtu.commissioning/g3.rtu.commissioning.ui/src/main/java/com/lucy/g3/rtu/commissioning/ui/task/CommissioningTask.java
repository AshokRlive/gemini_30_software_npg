/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.task;

import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.rtu.commissioning.ui.ICommissioningInvoker;
import com.lucy.g3.rtu.commissioning.ui.wizard.CommissioningCache;
import com.lucy.g3.rtu.config.shared.base.exceptions.InvalidConfException;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.shared.validation.domain.IContainerValidator;
import com.lucy.g3.rtu.config.validation.impl.IConfigValidator;

/**
 * Background task for commissioning.
 */
public final class CommissioningTask extends Task<Void, Void> {

  private Logger log = Logger.getLogger(CommissioningTask.class);

  private JFrame parent = null;

  private final ICommissioningInvoker invoker;
  private final CommissioningCache cache;

  public CommissioningTask(Application app, ICommissioningInvoker invoker, CommissioningCache cache) {
    super(app);
    this.invoker = invoker;
    this.cache = cache;

    if (app instanceof SingleFrameApplication)
      parent = ((SingleFrameApplication)app).getMainFrame();

    setUserCanCancel(false);
    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, null, null));
  }

  @Override
  protected Void doInBackground() throws Exception {
    /* Validate data*/
    IConfig data = invoker.getConfigData();
    IConfigValidator validator  = data.getConfigModule(IConfigValidator.CONFIG_MODULE_ID);
    validator.validate();
    if (validator.isValid() == false) {
      throw new InvalidConfException("The configuration validating failed. Please fix the errors in the configuration first!");
    }
    
    /* Writing configuration file */
    setMessage("Writing configuration into RTU...");
    String newIP = invoker.execTaskWriteConfigAndWait(invoker.getConfigData());
    
    /* Activate the written configuration */
    invoker.cmdActivateConfig();

    /* Register modules */
    setMessage("Registering modules...");
    invoker.cmdRegisterModules();
    
    /* Restart RTU */
    setMessage("Restarting RTU to apply changes...");
    invoker.execTaskRestartRTUAndWait(newIP);

    return null;
  }

  @Override
  protected void succeeded(Void result) {
    setMessage("RTU commissioning is completed");
    JOptionPane.showMessageDialog(parent,
        "The RTU has been configured successfully!",
        "Success",
        JOptionPane.INFORMATION_MESSAGE);

    // Write local wizard cache to RTU
    cache.writeDefaultCacheFileToRTU();
    
    invoker.loginWithLastAccount();
  }

 
  @Override
  protected void failed(Throwable cause) {
    String err = "Failed to proceed commissioning process!";
    log.error(err, cause);

    ErrorInfo info = new ErrorInfo("Failure",
        err,
        cause.getMessage(), null,
        null, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
  }

}
