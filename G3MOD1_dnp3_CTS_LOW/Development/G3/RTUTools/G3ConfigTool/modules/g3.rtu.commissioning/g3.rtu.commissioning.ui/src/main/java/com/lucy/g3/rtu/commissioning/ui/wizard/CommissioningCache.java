/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JToggleButton;
import javax.swing.text.JTextComponent;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.template.custom.shared.ISupportCache;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;
import com.lucy.g3.common.utils.EqualsUtil;


/**
 * The Class for managing wizard data cache.
 */
public class CommissioningCache extends Model{
  
  private static final boolean ENABLE_XML_ENCODER = true;
  
  private static Logger log = Logger.getLogger(CommissioningCache.class);
  
  private HashMap<String,Object> cacheData;
  
  /**
   * Creates a commissioning cache using default cache file.
   */
  public static CommissioningCache readFromFile(File file) {
    CommissioningCache cache = new CommissioningCache();
    cache.cacheData = readMapFromFile(file);
    return cache; 
  }
  
  private CommissioningCache(){}
  
  
  /**
   * Restores a step from existing cache.
   * @param steps steps to be restored.
   */
  public void restoreWizards(WizardPage... steps) {
    if (cacheData != null) {
      for (int i = 0; i < steps.length; i++) {
        if (steps[i] instanceof AbstractCommissioningStep)
          ((AbstractCommissioningStep) steps[i]).restoreFromCache(this);
      }
    }
  }

  public void restoreComponents(JComponent... comps) {
    if (cacheData == null)
      return;

    for (JComponent comp : comps) {
      if(comp.getName() == null)
        continue;
      
      if (comp instanceof JTextComponent)
        restoreTextComponent((JTextComponent) comp);
      else if (comp instanceof JComboBox)
        restoreComboBox((JComboBox) comp);
      else if (comp instanceof JToggleButton)
        restoreToggleButton((JToggleButton) comp);
      else
        log.warn("Cannot restore component state. Unsupported component:" + comp);
    }
  }

  private void restoreTextComponent(JTextComponent comp) {
    Object value = cacheData.get(comp.getName());
    if (value != null && value instanceof String /*&& !Strings.isBlank((String) value)*/) {
      comp.setText((String) value);
      
      /*
       * Workaround for forcing repaint of the fields decorated with custom labels. 
       */
      if (comp instanceof JFormattedTextField) {
        try {
          ((JFormattedTextField)comp).commitEdit();
        } catch (ParseException e) {
          e.printStackTrace();
        }
        
        ((JFormattedTextField)comp).setFormatterFactory(((JFormattedTextField)comp).getFormatterFactory());
      }
      
    } else {
      log.warn("No state found in cache for the textfield: " + comp.getName());
    }
  }

  private void restoreComboBox(JComboBox comp) {
    Object value = cacheData.get(comp.getName());
    if (value != null) {
      // Search and selected the cached value
      int size = comp.getModel().getSize();
      for (int i = 0; i < size; i++) {
        Object element = comp.getModel().getElementAt(i);
        if (EqualsUtil.areEqual(value, getSerializableValue(element))) {
          comp.setSelectedIndex(i);
          return; // Success
        } 
      }
      
      // Cached value not found, insert it to comboBox
      if(comp.isEditable()) {
        comp.addItem(value);
        comp.setSelectedItem(value);
        return; // Success
      }
    }
    
    log.warn("No state found in cache for the combobox: " + comp.getName());
  }
  
  private void restoreToggleButton(JToggleButton comp) {
    Object value = cacheData.get(comp.getName());
    if (value != null) {
      try {
        comp.setSelected(Boolean.valueOf(value.toString()));
        return;
      } catch (Exception e) {
        // Nothing to do
      }
    }
    log.warn("No state found in cache for the toggle button: " + comp.getName());
  }

  private static HashMap<String,Object> convertToSerializable(@SuppressWarnings("rawtypes")Map result) {
    HashMap<String,Object> cache = new HashMap<>();
    
    @SuppressWarnings("rawtypes")Iterator it = result.keySet().iterator();
    String key;
    Object valueStr;
    while (it.hasNext()) {
      key = (String) it.next();
      valueStr = getSerializableValue(result.get(key));

      if (valueStr != null && key != null)
        cache.put(key, valueStr);
    }
    
    return cache;
  }

  private static Object getSerializableValue(Object object) {
    if (object == null)
      return null;

    try {
      // Use uuid as value
      Method getter = object.getClass().getMethod("getUUID");
      return getter.invoke(object);
    } catch (Throwable e) {
      // No uuid found, use string as value instead
      return object.toString();
    }
  }
  

//  public void readCacheFileFromRTU(boolean inBackground) {
//    File destination = getCacheFile();
//    destination.delete();
//
//    try {
//      if(inBackground)
//        fileTransfer.readFileFromRTU(CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC, cacheFile.getName(), destination,false);
//      else
//        fileTransfer.readFileFromRTUAndWait(CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC, 1, cacheFile.getName(), destination);
//
//    } catch (Exception e) {
//      log.error("Failed to read commissioning cache");
//    }
//  }
  
  @SuppressWarnings("unchecked")
  private static HashMap<String, Object> readMapFromFile(File cacheFile) {
    FileInputStream is;
    HashMap<String,Object> cache = null;
    try {
      is = new FileInputStream(cacheFile);
      if (ENABLE_XML_ENCODER) {
        XMLDecoder decoder = new XMLDecoder(is);
        cache = (HashMap<String, Object>) decoder.readObject();
        decoder.close();
      } else {
        cache = SerializationUtils.deserialize(is);
      }
      log.info("commissionging wizard cache found:" + cacheFile);
    } catch (Throwable e) {
      log.warn("commissionging wizard cache NOT found:" + cacheFile);
    }
    return cache;
  }
  
  private static void writeMapToFile(@SuppressWarnings("rawtypes") Map map, File file) {
    FileOutputStream os;
    try {
      os = new FileOutputStream(file);
  
      if (ENABLE_XML_ENCODER) {
        XMLEncoder encoder = new XMLEncoder(os);
        encoder.writeObject(convertToSerializable(map));
        encoder.close();
      } else {
        SerializationUtils.serialize(convertToSerializable(map), os);
      }
      
      log.info("commissionging wizard cache saved to:" + file);
    } catch (Throwable e) {
      log.warn("commissionging wizard cache NOT saved to:" + file);
    }
  }
  

  /** Default cache file **/
  public final static File DEFAULT_CACHE_FILE = new File(
      Application.getInstance().getContext().getLocalStorage().getDirectory().getAbsolutePath(),
      G3Protocol.DEFAULT_COMMISSIONING_CACHE_FILE_NAME);
  
  public static void writeDefaultCacheFileToRTU() {
    File cacheFile = DEFAULT_CACHE_FILE;
    
    if (cacheFile.exists()) {
      log.info("Writing commissioning cache to RTU: " + cacheFile);
      //G3RTUUtil.getFileTransferService()
      FileTransferService
      .writeFileToRTU(CommsUtil.getFileTransfer(G3RTUFactory.getDefault().getComms()), CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC, cacheFile);
    } else
      log.error("File not exists: " + cacheFile);
  }
  /**
   * Stores a wizard result to a cache file.
   * @param result wizard data map
   */
  public static void writeToDefaultCacheFile(@SuppressWarnings("rawtypes")Map result) {
    writeMapToFile(result, DEFAULT_CACHE_FILE);
  }
  
  public static CommissioningCache readFromDefaultCacheFile() {
    return readFromFile(DEFAULT_CACHE_FILE);
  }
}

