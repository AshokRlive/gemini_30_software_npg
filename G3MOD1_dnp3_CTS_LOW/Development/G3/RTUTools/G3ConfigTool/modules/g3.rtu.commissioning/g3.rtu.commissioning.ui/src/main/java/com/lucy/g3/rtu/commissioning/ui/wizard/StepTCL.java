/*
 * Created by JFormDesigner on Thu Apr 27 09:30:24 BST 2017
 */

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingLogic;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingSettingsPanel;
import com.lucy.g3.rtu.config.clogic.tcl.TCLTrippingLogic;
import com.lucy.g3.rtu.config.clogic.tcl.TCLTrippingSettingsPanel;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

class StepTCL extends AbstractCommissioningStep {
  static final String KEY_CIRCUIT_BREAKER = "StepTCL.circuitBreaker";
  static final String KEY_ENABLE_TCL = "StepTCL.enableTCL";
  static final String KEY_SENSING_PANEL = "StepTCL.panelSensing";
  static final String KEY_TRIPPING_PANEL = "StepTCL.panelTripping";
  
  private final TCLSensingLogic sensing;
  private final TCLTrippingLogic tripping;
  private final IConfig config;
  
  public StepTCL(IConfig config, TCLSensingLogic sensing, TCLTrippingLogic tripping) {
    super("Timed Connection");
    this.config = Preconditions.checkNotNull(config, "config must not be null");
    this.sensing = Preconditions.checkNotNull(sensing, "sensing must not be null");
    this.tripping = Preconditions.checkNotNull(tripping, "trippingmust not be null");
    initComponents();
    initComponentNames();
    initComponentData();
    registerePersistentComponents();

    // Init wizard data
    putWizardData(KEY_SENSING_PANEL, panelSensing);
    putWizardData(KEY_TRIPPING_PANEL, panelTripping);
    putWizardData(KEY_ENABLE_TCL, checkBoxEable.isSelected());    
    
    // Update visibility
    tabbedPane1.setVisible(checkBoxEable.isSelected());
  }

  private void initComponentNames() {
    checkBoxEable.setName(KEY_ENABLE_TCL);
  }

  private void registerePersistentComponents() {
    List<JComponent> comps = getAllComponents(this);
    super.registerPersistentComponent(comps.toArray(new JComponent[comps.size()]));  
  }

  
  private List<JComponent> getAllComponents(Container c) {
    Component[] comps = c.getComponents();
    List<JComponent> compList = new ArrayList<>();
    for (Component comp : comps) {
      if(comp instanceof JComponent && comp.getName() != null) {
        compList.add((JComponent) comp);
      }
      
      if (comp instanceof Container)
        compList.addAll(getAllComponents((Container) comp));
    }
    return compList;
  }

  private void initComponentData() {
    panelSensing.load(sensing.getSettings());
    panelTripping.load(tripping.getSettings());
  }

  private void checkBoxEableItemStateChanged(ItemEvent e) {
    tabbedPane1.setVisible(checkBoxEable.isSelected());
  }

  @Override
  protected String validateContents(Component component, Object event) {
    if(panelSensing.validateSettings() == false) 
      return "Invalid time settings!";
    
    if(panelTripping.validateSettings() == false)
      return "Invalid tripping settings!";
    
    
    Object cb = comboCB.getSelectedItem();
    if(cb == null)
      return "No circuit breaker selected for tripping logic!";
      
    
    return null;
  }
  
  @Override
  protected void renderingPage() {
    Object selection = comboCB.getSelectedItem();
    comboCB.setModel(new DefaultComboBoxModel<>(
        StepSwitch.getEnabledCircuitBreakers(getWizardDataMap(), config)));
    
    // Set initial selection
    CommissioningCache cache = getCache();
    if(selection != null) {
      comboCB.setSelectedItem(selection);
    } else if(cache != null){
      cache.restoreComponents(comboCB);
    } else if(comboCB.getItemCount() > 0){
      comboCB.setSelectedIndex(0);
    }
    
    updateVisibility();
  }

  private void updateVisibility() {
    lblTips.setVisible(comboCB.getItemCount() == 0);
  }

  @SuppressWarnings("unchecked")
  private void createUIComponents() {
    comboCB = new JComboBox<>(StepSwitch.getEnabledCircuitBreakers(getWizardDataMap(), config));
    comboCB.setRenderer(StepSwitch.createCBRenderer());
    comboCB.setName(KEY_CIRCUIT_BREAKER);
    super.registerPersistentComponent(comboCB);
    comboCB.setSelectedItem(tripping.getCircuitBreakerOutput());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    scrollPane1 = new JScrollPane();
    panelSensing = new TCLSensingSettingsPanel();
    scrollPane2 = new JScrollPane();
    panel1 = new JPanel();
    panelTripping = new TCLTrippingSettingsPanel();
    panel2 = new JPanel();
    label1 = new JLabel();
    lblTips = new JLabel();
    checkBoxEable = new JCheckBox();

    //======== this ========
    setLayout(new BorderLayout(0, 10));

    //======== tabbedPane1 ========
    {

      //======== scrollPane1 ========
      {
        scrollPane1.setViewportBorder(Borders.DLU4_BORDER);
        scrollPane1.setBorder(null);
        scrollPane1.setViewportView(panelSensing);
      }
      tabbedPane1.addTab("Sensing Logic", scrollPane1);

      //======== scrollPane2 ========
      {
        scrollPane2.setViewportBorder(Borders.DLU4_BORDER);
        scrollPane2.setBorder(null);

        //======== panel1 ========
        {
          panel1.setLayout(new BorderLayout());
          panel1.add(panelTripping, BorderLayout.CENTER);

          //======== panel2 ========
          {
            panel2.setLayout(new FormLayout(
              "default, $lcgap, [50dlu,default], $lcgap, default:grow",
              "2*(default, $lgap), default"));

            //---- label1 ----
            label1.setText("Circuit Breaker to trip:");
            panel2.add(label1, CC.xy(1, 1));
            panel2.add(comboCB, CC.xy(3, 1));

            //---- lblTips ----
            lblTips.setText("No circuit breakers configured in step 2.");
            lblTips.setForeground(Color.red);
            panel2.add(lblTips, CC.xywh(1, 3, 5, 1));
          }
          panel1.add(panel2, BorderLayout.SOUTH);
        }
        scrollPane2.setViewportView(panel1);
      }
      tabbedPane1.addTab("Tripping Logic", scrollPane2);
    }
    add(tabbedPane1, BorderLayout.CENTER);

    //---- checkBoxEable ----
    checkBoxEable.setText("Enable Timed Connection");
    checkBoxEable.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        checkBoxEableItemStateChanged(e);
      }
    });
    add(checkBoxEable, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    UIUtils.increaseScrollSpeed(scrollPane1);
    UIUtils.increaseScrollSpeed(scrollPane2);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JScrollPane scrollPane1;
  private TCLSensingSettingsPanel panelSensing;
  private JScrollPane scrollPane2;
  private JPanel panel1;
  private TCLTrippingSettingsPanel panelTripping;
  private JPanel panel2;
  private JLabel label1;
  private JComboBox<SwitchGearLogic> comboCB;
  private JLabel lblTips;
  private JCheckBox checkBoxEable;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
