/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.pltu.PLTULogic;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.clogic.voltagemismatch.VoltageMismatchLogic;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplate;

/**
 * A wizard step for configuring PLTU.
 */
class StepPLTU extends AbstractCommissioningStep {
  
  static final String KEY_IS_CREATED = "StepPLTUCreated";
  public final static String STEP_ID = "StepPLTU";
  private final PLTULogic pltu;
  private final VoltageMismatchLogic voltageMismatch;
  private final IConfig data;
  private ArrayList<SwitchGearLogic> circuitBreakers;
  
  private JCheckBox cboxPltuEnabled;
  private JComboBox<Object> comboCB; 
  private JCheckBox cboxVoltEnabled;
  private JLabel lblCB;
  private JXLabel lblTips;

  
  public StepPLTU(PLTULogic pltu,
      VoltageMismatchLogic voltageMismatch, IConfig data) {
    super("Configure PLTU");
    setLongDescription("Configure PLTU & Voltage Mismatch");
    this.voltageMismatch = voltageMismatch; 
    this.pltu = pltu;
    this.data = data;

    initComponents();
    putWizardData(KEY_IS_CREATED, Boolean.TRUE);
    putWizardData(STEP_ID, this);
  }
  
  @Override
  protected void renderingPage() {
    Object selection = comboCB.getSelectedItem();
    circuitBreakers = getEnabledCircuitBreakers();
    comboCB.setModel(new DefaultComboBoxModel<>(circuitBreakers.toArray()));
    
    // Set initial selection
    CommissioningCache cache = getCache();
    if(selection != null) {
      comboCB.setSelectedItem(selection);
    } else if(cache != null){
      cache.restoreComponents(comboCB);
    } else if(comboCB.getItemCount() > 0){
      comboCB.setSelectedIndex(0);
    }
    
    updateVisibility();
  }

  private ArrayList<SwitchGearLogic> getEnabledCircuitBreakers() {
    Collection<SwitchGearLogic> sgls = CommissioningWizard.findSwitchLogic(data);
    ArrayList<SwitchGearLogic> enabledCBs = new ArrayList<>();
    String key;
    for (SwitchGearLogic sgl : sgls) {
      key = StepSwitch.generateKey(sgl);
      Object template = getWizardData(key);
      if (template != null && template instanceof SwitchTemplate
          && ((SwitchTemplate)template).isCircuitBreaker()) {
        enabledCBs.add(sgl);
      } 
    }
    
    return enabledCBs;
  }


  @SuppressWarnings("unchecked")
  private JPanel buildContentPanel() {
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("right:[80dlu,default], $ugap, [60,default], $ugap, default:grow", ""));

    String pltuName = pltu.getCustomName();
    if (Strings.isBlank(pltuName))
      pltuName = pltu.getDescription();

    // Create PLTU enable checkbox
    cboxPltuEnabled = new JCheckBox(pltuName);
    cboxPltuEnabled.setName(generateKeyForPltuEnable(pltu));
    cboxPltuEnabled.setToolTipText("Enable/Disable PLTU");
    super.registerPersistentComponent(cboxPltuEnabled);
    builder.append(cboxPltuEnabled,3);
    builder.nextLine();
    
    // Create circuit breaker selection combobox
    circuitBreakers = getEnabledCircuitBreakers();
    comboCB = new JComboBox<>(circuitBreakers.toArray());
    comboCB.setRenderer(renderCB);
    comboCB.setName(generateKeyForCircuitBreaker(pltu));
    super.registerPersistentComponent(comboCB);
    comboCB.setSelectedItem(pltu.getOutputSwitch());
    lblCB = new JLabel("Circuit Breaker to trip:");
    lblTips = new JXLabel("(No circuit breakers configured in step 2.)");
    lblTips.setLineWrap(true);
    lblTips.setForeground(Color.gray);
    builder.append(lblCB, comboCB, lblTips);
    builder.nextLine();
    
    // Gap
    builder.appendParagraphGapRow();
    builder.nextLine();
    
    // Create voltage mismatch checkbox 
    if(voltageMismatch != null) {
      String voltName = voltageMismatch.getCustomName();
      if (Strings.isBlank(voltName))
        voltName = voltageMismatch.getDescription();
      cboxVoltEnabled = new JCheckBox(voltName);
      cboxVoltEnabled.setName(generateKeyForVoltEnable(voltageMismatch));
      cboxVoltEnabled.setSelected(true);
      cboxVoltEnabled.setToolTipText("Enable/Disable Voltage Mismatch");
      super.registerPersistentComponent(cboxVoltEnabled);
      builder.append(cboxVoltEnabled,3);
      builder.nextLine();
    }
    
    // Update visibility upon enable state 
    cboxPltuEnabled.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        updateVisibility();
      }
    });
    
    
    updateVisibility();
    
    return builder.getPanel();
  }
  
  private void updateVisibility(){
    boolean pltuEnabled = cboxPltuEnabled.isSelected();
    lblCB.setEnabled(pltuEnabled);
    comboCB.setEnabled(pltuEnabled);
    
    lblTips.setVisible(pltuEnabled
        && (circuitBreakers == null || circuitBreakers.size() == 0));
    
    if(cboxVoltEnabled != null) {
      if(pltuEnabled) {
        cboxVoltEnabled.setEnabled(false);
        //cboxVoltEnabled.setEnabled(false);
      } else {
        cboxVoltEnabled.setEnabled(true);
      }
    }
      
  }

  
  
  @Override
  protected String validateContents(Component component, Object event) {
    if(cboxPltuEnabled.isSelected()) {
      Object cb = comboCB.getSelectedItem();
      if(cb == null)
        return "No circuit breaker selected for PLTU!";
    }
    
    return null;
  }

  @Override
  protected void postRestore() {
    updateVisibility();
  }

  private void initComponents() {
    JPanel contentPanel = buildContentPanel();

    setLayout(new BorderLayout());
    add(contentPanel, BorderLayout.CENTER);
  }
  
  
  
  static String generateKeyForPltuEnable(PLTULogic pltu) {
    return generateKey("PLTUEnabled-", pltu);
  }
  
  static String generateKeyForVoltEnable(VoltageMismatchLogic volt) {
    return generateKey("VoltEnabled-", volt);
  }
  
  static String generateKeyForCircuitBreaker(PLTULogic pltu) {
    return generateKey("SelectedCircuitBreaker-", pltu);
  }
  
  /**
   * Generates a key for accessing or storing PLTU configuration in WizardData.
   */
  private static String generateKey(String key, ICLogic logic) {
    return key + logic.getUUID();
  }

  private final DefaultListRenderer renderCB =  new DefaultListRenderer(new StringValue() {
    @Override
    public String getString(Object arg0) {
      if(arg0 == null)
        return " ";
      ICLogic logic = (ICLogic) arg0;
      String name = logic.getCustomName();
      if(Strings.isBlank(name))
        name = logic.getDescription();
      return name;
    }
  });

  public boolean isPLTUEnabled() {  
    return cboxPltuEnabled.isSelected();
  }
  
  public boolean isVoltMismatchEnabled() {  
    return cboxVoltEnabled.isSelected();
  }
  
  public ICLogic getSelectedCB() {  
    return (ICLogic) comboCB.getSelectedItem();
  }
}
