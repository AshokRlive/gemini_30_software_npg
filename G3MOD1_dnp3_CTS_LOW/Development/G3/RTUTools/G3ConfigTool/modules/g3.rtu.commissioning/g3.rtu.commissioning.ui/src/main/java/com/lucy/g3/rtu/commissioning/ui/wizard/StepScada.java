/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;

import org.apache.commons.lang3.ArrayUtils;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplates;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplate.ScadaTemplateType;

/**
 * Wizard page for setting DNP3.
 */
class StepScada extends AbstractCommissioningStep {
  
  static final String KEY_IS_CREATED = "StepDNP3Created";
  
  static final private String CLIENT_PROPERTY_CHANNEL_NAME = "clientPropertyProfileName";

  private static final String TEXT_DEFAULT = "  - Default -";

  private final ScadaTemplates templates;
  private final SDNP3 dnp3;

  public StepScada(SDNP3 dnp3, ScadaTemplates templates) {
    super("Configure SCADA");
    this.dnp3 = dnp3;
    this.templates = templates;

    initComponents();
    putWizardData(KEY_IS_CREATED, Boolean.TRUE);
  }

  private void createUIComponents() {
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("default, $lcgap, default:grow", ""));

    Collection<SDNP3Channel> channels = dnp3.getAllChannels();
    for (SDNP3Channel channel: channels) {
      addSesnEntry(builder, channel);
    }

    contentPanel = builder.getPanel();
  }

  private void addSesnEntry(DefaultFormBuilder builder, SDNP3Channel channel) {
    Collection<ScadaTemplate> list = templates.getAllItems();
    ScadaTemplate[] array = list.toArray(new ScadaTemplate[list.size()]);
    JComboBox<Object> profileSelections = new JComboBox<>(ArrayUtils.addAll(new Object[]{TEXT_DEFAULT}, sortTemplates(array)));
    profileSelections.setRenderer(new TemplateRenderer());
    profileSelections.setName(generateKey(channel));
    profileSelections.putClientProperty(CLIENT_PROPERTY_CHANNEL_NAME, channel.getChannelName());
    profileSelections.setSelectedItem(TEXT_DEFAULT);
    super.registerPersistentComponent(profileSelections);
    
    builder.append(String.format("SCADA Profile(%s):", channel.getChannelName()), profileSelections);

    builder.nextLine();
    builder.appendRelatedComponentsGapRow();
    builder.nextLine();
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    if(component instanceof JComboBox) {
      JComboBox combo = (JComboBox)component;
      Object selectedItem = combo.getSelectedItem();
      if(selectedItem == null) {
        return String.format("No selection for \"%s\"!", combo.getClientProperty(CLIENT_PROPERTY_CHANNEL_NAME));
      
      } else if(selectedItem instanceof String && selectedItem != TEXT_DEFAULT) {
        /* The selection is a title */
        return String.format("Invalid selection for \"%s\"!", combo.getClientProperty(CLIENT_PROPERTY_CHANNEL_NAME));
      }
    }
    
    return null;
  }

  /**
   * Sorts the templates by classifier, and add subtitle(string) between different classifiers.
   * @return an array that contains string titles and sorted SCADA templates.
   */
  static Object[] sortTemplates(ScadaTemplate[] items) {
    // Sort by classifier
    Arrays.sort(items, new Comparator<ScadaTemplate>() {

      @Override
      public int compare(ScadaTemplate o1, ScadaTemplate o2) {
        if(o1 != null && o2 != null) {
          return o1.getClassifier().compareTo(o2.getClassifier());
        }
        return 0;
      }
    });
    
    
    // Add items with inserted titles and separator
    ArrayList<Object> result = new ArrayList<>();
    String title = "";
    for (int i = 0; i < items.length; i++) {
      if(!title.equals(items[i].getClassifier())) {
        title = items[i].getClassifier();
        result.add("");// Add separator
        result.add(formatTitle(title)); // Add title
      }
      
      result.add(items[i]);
    }
    
    return result.toArray();
  }
  
  static String formatTitle(String title) {
    return String.format("=====[ %s ]=====", title);
  }

  private static class TemplateRenderer extends DefaultListCellRenderer {

    TemplateRenderer() {
      setPreferredSize(new Dimension(100, 18));
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (value != null && value instanceof ScadaTemplate) {
        ScadaTemplate templ = (ScadaTemplate) value;
        String profileName = templ.getProfileName();
        if (Strings.isBlank(profileName)) {
          profileName = "Unknown Profile";
        }

        String typeStr = templ.getType().name();
        if(templ.getType() == ScadaTemplateType.COMMS) {
          typeStr = templ.getCommsDev().getType().getDescription();
        }
        
        profileName = String.format("  [%s] %s", typeStr, profileName);
        setText(profileName);
        setToolTipText(profileName);
      } else if(value == TEXT_DEFAULT) {
        setToolTipText("Leave SCADA as what it is in the configuration.");
      }

      return comp;
    }
  }

  
  /**
   * Generates a key for accessing or storing SDNP3Session template in
   * WizardData.
   */
  static String generateKey(SDNP3Channel channel) {
    return "SCADAProfile-" + channel.getUUID();
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    //======== this ========
    setLayout(new BorderLayout());
    add(contentPanel, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel contentPanel;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
