/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui;

import static org.junit.Assert.*;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.junit.Test;

import com.lucy.g3.rtu.commissioning.ui.CommissioningPage;


/**
 *
 */
public class CommissioningPageTest {


  @Test
  public void testGetAction() {
    CommissioningPage page = new CommissioningPage(null);
    final ApplicationActionMap actionMap = Application.getInstance().getContext().getActionMap(page);
    assertNotNull(actionMap.get(CommissioningPage.ACTION_RERUN_COMMISSION_FROM_CACHE));
    assertNotNull(actionMap.get(CommissioningPage.ACTION_TEST));

  }

}

