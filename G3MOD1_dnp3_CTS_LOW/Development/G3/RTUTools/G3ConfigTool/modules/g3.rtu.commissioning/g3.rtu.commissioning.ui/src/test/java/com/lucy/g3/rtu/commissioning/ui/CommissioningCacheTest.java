/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * The Class CommissioningCacheTest.
 */
public class CommissioningCacheTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    Cache c = new Cache();
    c.put("hello", 123L);
    try
    {
       FileOutputStream fileOut =
       new FileOutputStream("target/commmission.cache");
       ObjectOutputStream out = new ObjectOutputStream(fileOut);
       out.writeObject(c);
       out.close();
       fileOut.close();
    }catch(IOException i)
    {
        i.printStackTrace();
    }
    
    
    Cache c2 = null;
    try
    {
      FileInputStream fileIn =
          new FileInputStream("target/commmission.cache");
      ObjectInputStream in = new ObjectInputStream(fileIn);
      c2 = (Cache) in.readObject();
      in.close();
      fileIn.close();
    }catch(Exception i)
    {
      i.printStackTrace();
    }
    
    Long value = (Long) c2.get("hello");
    System.out.println("value:" +value);
  }
  
  public static class Cache implements Serializable {
    public static final String KEY_SDNP3_SLAVE_ADDRESS = "sdnp3SlaveAddress";
    
    private final HashMap<String, Object> map = new HashMap<>();
    
    public void put(String key, Object value) {
      map.put(key, value);
    }
    
    public Object get(String key) {
      return map.get(key);
    }
  }
  

}

