/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.pltu.PLTULogic;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.clogic.tcl.TCLSensingLogic;
import com.lucy.g3.rtu.config.clogic.tcl.TCLTrippingLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicFinder;
import com.lucy.g3.rtu.config.clogic.voltagemismatch.VoltageMismatchLogic;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.utils.CANModuleUtility;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Session;
import com.lucy.g3.rtu.config.protocol.master.shared.manager.MasterProtocolManager;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolType;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.integrated.CustomTemplates;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplate;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplates;
import com.lucy.g3.rtu.config.template.custom.sdnp3.ScadaTemplates;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Commissioning wizard.
 */
public final class CommissioningWizard extends WizardBranchController {
  
  private static final int WIDTH  = 750;
  private static final int HEIGHT = 500;
  
  static final String TEXT_DISABLED = "- Disabled -";

  private static final String TITLE = "Commissioning Wizard";

  private final WizardResultProducer producer;

  private final IConfig data;

  private final WizardPage[] subSteps;
  
  private final CommissioningCache cache;

  //private static Logger log = Logger.getLogger(CommissioningWizard.class);
  
  private CommissioningWizard(IConfig data, WizardPage initial, CommissioningCache cache) {
    super(TITLE, initial);
    this.data = Preconditions.checkNotNull(data, "data must not be null");
    this.producer = new CommissioningResultProducer(data);
    this.subSteps = createAllSubSteps();
    this.cache = cache;
    
    if (cache != null) {
      cache.restoreWizards(initial);
      cache.restoreWizards(subSteps);
    }
  }


  private WizardPage[] createAllSubSteps() {
    /* Create all sub steps */
    ArrayList<WizardPage> subSteps = new ArrayList<>();
    subSteps.add(createStepSwitch());
    subSteps.add(createStepFPI());
    subSteps.add(createStepScada()); 
    subSteps.add(createStepModBus());
    subSteps.add(createStepPLTU());
    subSteps.add(createStepTCL());
    subSteps.add(new StepLast());

    // Remove all null pages
    subSteps.removeAll(Collections.singleton(null));
    return subSteps.toArray(new WizardPage[subSteps.size()]);
  }
  
  private CustomTemplates getTemplate() {
    return data.getConfigModule(CustomTemplates.CONFIG_MODULE_ID);
  }

  private WizardPage createStepTCL() {
    CLogicManager man = data.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    Collection<ICLogic> clogics = man.getAllClogic();
    Collection<TCLSensingLogic> sensing = CLogicFinder.findCLogicByClass(clogics, TCLSensingLogic.class);
    Collection<TCLTrippingLogic> tripping = CLogicFinder.findCLogicByClass(clogics, TCLTrippingLogic.class);
    
    if(sensing.size() == 1 && tripping.size() == 1) {
      return new StepTCL(data,sensing.iterator().next(), tripping.iterator().next());
    } else {
      return null;
    }
  }
  
  private WizardPage createStepModBus() {
    MasterProtocolManager mgr = getMasterProtocolManager();
    MMB mmb = (MMB) mgr.getModbusMaster();
    MMBTemplates template = getTemplate().getMMBTemplates();
    if (mmb != null) {
      Collection<MMBSession> sesns = mmb.getAllSessions();
      Collection<MMBChannel> channels = mmb.getAllChannels();
      Collection<MMBTemplate> templs = template.getAllItems();
      if (!channels.isEmpty() && !sesns.isEmpty() && !templs.isEmpty()) {
        return new StepFieldDevice(channels, templs);
      }
    }
    return null;
  }


  private ScadaProtocolManager getScadaProtocolManager() {
    return data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
  }
  
  private MasterProtocolManager getMasterProtocolManager() {
    return data.getConfigModule(MasterProtocolManager.CONFIG_MODULE_ID);
  }

  private WizardPage createStepSwitch() {
    Collection<SwitchGearLogic> sgls = findSwitchLogic(data);
    if (sgls.isEmpty()) 
      return null;
    
    if (getTemplate().getSwitchTemplates().isEmpty())
      return null;
    
    return new StepSwitch(sgls, getTemplate().getSwitchTemplates());
  }
  
  private CANModuleManager getCANModulesManager() {
    return data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
  }
  
  private static CLogicManager getCLogicManager (IConfig data) {
    return data.getConfigModule(CLogicManager .CONFIG_MODULE_ID);
  }
  
  private WizardPage createStepFPI() {
    if (getCANModulesManager().getModuleNum(MODULE.MODULE_FPM) <= 0) {
      return null; // FPI step cannot be created cause No FPM module found
    }
    
    Collection<SwitchGearLogic> sgls = findSwitchLogic(data);
    if (sgls.isEmpty()) {
      return null; // No switchgear found 
    }
    
    return new StepFPI(sgls, CANModuleUtility.getAllFPIChannels(data), getTemplate().getFpiTemplates());
  }

  private WizardPage createStepScada() {
    SDNP3 dnp3 = (SDNP3) getScadaProtocolManager().getProtocol(ProtocolType.SDNP3);
    if (dnp3 == null) 
      return null; // sDNP3 not available 
    
    ScadaTemplates templates = getTemplate().getScadaTemplates();
    if (templates.isEmpty())
      return null; // sDNP3 template not available

    return new StepScada(dnp3,templates);
  }

  private WizardPage createStepPLTU() {
    PLTULogic pltu = findPLTU(data);
    return pltu == null ? null : new StepPLTU(pltu,findVolMismatch(data), data);
  }
  
  static VoltageMismatchLogic findVolMismatch(IConfig data){
    CLogicManager clogMgr = getCLogicManager(data);
    Collection<ICLogic> clogics = clogMgr.getAllClogic();
    Collection<VoltageMismatchLogic> volt = CLogicFinder.findCLogicByClass(clogics, VoltageMismatchLogic.class);
    
    if (volt == null || volt.isEmpty()) 
      return null;
    
    for (VoltageMismatchLogic p : volt) {
      if (p != null) {
        return p;
      }
    }
    
    return null;
  }
  
  //  private static Collection<SwitchGearLogic> findCircuitBreakers(ConfigData data){
//  Collection<ICLogic> clogics = data.getcLogicManager().getAllClogic();
//  Collection<SwitchGearLogic> sws = CLogicFinder.findCLogicByClass(clogics, SwitchGearLogic.class);
//  ArrayList<SwitchGearLogic> cbs = new ArrayList<>();
//  for (SwitchGearLogic sw : sws) {
//    if(sw.getSettings().isCircuitBreaker()) {
//      cbs.add(sw);
//    }
//  }
//  
//  return cbs;
//  }
  
  static PLTULogic findPLTU(IConfig data){
    CLogicManager clogMgr = getCLogicManager(data);
    Collection<ICLogic> clogics = clogMgr.getAllClogic();
    Collection<PLTULogic> pltus = CLogicFinder.findCLogicByClass(clogics, PLTULogic.class);
    
    if (pltus == null || pltus.isEmpty()) 
      return null;
    
    for (PLTULogic pltu : pltus) {
      if (pltu != null) {
        return pltu;
      }
    }
    
    return null;
  }


  static Collection<SDNP3Session> findDNP3Sessions(IConfig data){
    ArrayList<SDNP3Session> sesns = new ArrayList<>();
    ScadaProtocolManager manager = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    SDNP3 dnp3 = (SDNP3)manager.getProtocol(ProtocolType.SDNP3);
    if (dnp3 != null) {
      Collection<SDNP3Channel> channels = dnp3.getAllChannels();
      for (SDNP3Channel channel : channels) {
        // Set slave address
        sesns.addAll(channel.getAllSessions());
      }
    }
    
    // Remove disabled sessions
    ArrayList<SDNP3Session> disabledSesns = new ArrayList<>();
    for (SDNP3Session sesn : sesns) {
      if(!sesn.isEnabled() || !sesn.getProtocolChannel().isEnabled()) {
        disabledSesns.add(sesn);
      }
    }
    sesns.removeAll(disabledSesns);
    return sesns;
  }
  
  private void saveCache(@SuppressWarnings("rawtypes") Map result) {
    if (result != null) {
      CommissioningCache.writeToDefaultCacheFile(result);
    }
  }
  
  static Collection<SwitchGearLogic> findSwitchLogic(IConfig data) {
    Collection<ICLogic> clogics = getCLogicManager(data).getAllClogic();
    return CLogicFinder.findCLogicByClass(clogics, SwitchGearLogic.class);
  }

  @SuppressWarnings({ "rawtypes" })
  @Override
  protected Wizard getWizardForStep(String step, Map settings) {
    if (StepInitial.ID.equals(step)) {
      if (subSteps != null && subSteps.length > 0) {
        return WizardPage.createWizard(subSteps, producer);
      }
    }

    return null;
  }

  /**
   * Shows wizard dialog.
   *
   * @param data
   *          the configuration data to be commissioned.
   * @param  cache
   *          if not null, the wizard settings will be restored from the provided cache. 
   * @return true if wizard is finished, false if cancelled.
   */
  public static boolean showWizardDialog(IConfig data, CommissioningCache cache) {
    CommissioningWizard wizardController = new CommissioningWizard(data, new StepInitial(data), cache);
    Wizard wizard = wizardController.createWizard();
    @SuppressWarnings("rawtypes")
    Map result = (Map) WizardDisplayer.showWizard(wizard, WizardUtils.createRect(WindowUtils.getMainFrame(), WIDTH, HEIGHT));
    wizardController.saveCache(result);
    return result != null;
  }
}
