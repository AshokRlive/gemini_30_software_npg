/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui;

import java.util.List;

import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.PollingMode;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;

/**
 * The Interface ICommissioningInvoker.
 */
public interface ICommissioningInvoker {
  Task<?,?> createConfigImportTask();
  
  IConfig getConfigData();

  Application getApp();

  TaskService getG3TaskService();

  ConnectionState getConnectionState();

  void showLoginDialog();

  void loginWithLastAccount();

  void cmdGetPointStatus(List<IPointData> arrayList, PollingMode mode) throws Exception;

  void cmdOperateLogic(short group, short period) throws Exception;

  void cmdOperateSwitch(short group, SWITCH_OPERATION operation, LocalRemoteCode localRemote) throws Exception;

  void cmdRegisterModules() throws Exception;

  void cmdActivateConfig() throws Exception;
  
  String execTaskWriteConfigAndWait(IConfig configData) throws Exception;

  void execTaskRestartRTUAndWait(String newIP) throws Exception;

  VirtualPointData createPointData(VirtualPoint point);

  CommissioningAPI getCommissioningCMD();

  G3ConfigAPI getG3ConfigAPI();
  
  RTUInfo getRTUInfo();
}
