/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.lang3.ArrayUtils;
import org.jdesktop.swingx.renderer.DefaultListRenderer;
import org.jdesktop.swingx.renderer.StringValue;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.clogic.ICLogic;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.module.shared.domain.SwitchModuleOutput;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplate;
import com.lucy.g3.rtu.config.template.custom.switchgear.SwitchTemplates;


/**
 * Wizard Page for switchgear.
 */
class StepSwitch extends AbstractCommissioningStep {

  private static final String CLIENT_PROPERTY_LABEL = "clientPropertylabel";
  
  static final String IS_CREATED = "StepSwitchCreated";
  
  static final String TEXT_DISABLED = CommissioningWizard.TEXT_DISABLED;
  
  private final Collection<SwitchGearLogic> switchlogic;
  private final SwitchTemplates template;

  private final ArrayList<JComboBox<?>> allComboBoxes = new ArrayList<>();


  public StepSwitch(Collection<SwitchGearLogic> switchlogic, SwitchTemplates template) {
    super("Configure Switchgear");
    this.switchlogic = Preconditions.checkNotNull(switchlogic, "switchlogic must not be null");
    this.template = Preconditions.checkNotNull(template, "templates must not be null");

    initComponents();
    putWizardData(IS_CREATED, Boolean.TRUE);
    
  }

  private void createUIComponents() {
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("default, $ugap, default:grow", ""));

    // Add title
    builder.append("<html><b>Description</b></html>",
        new JLabel("<html><b>Type</b></html>"));
    
    ArrayList<SwitchModuleOutput> existOutputs = new ArrayList<>();
    for (SwitchGearLogic logic : switchlogic) {
      if (logic != null) {
        SwitchModuleOutput output = logic.getOutputSCM();
        if(!existOutputs.contains(output)) {
          addSwitchEntry(builder, logic);
          existOutputs.add(output);
        }
      }
    }

    contentPanel = builder.getPanel();
  }

  private void addSwitchEntry(DefaultFormBuilder builder, SwitchGearLogic sw) {
    // Create selection combobox
    
    Object[] items = ArrayUtils.addAll(new Object[] { TEXT_DISABLED }, template.getSwitchTemplates().toArray());
    items  = ArrayUtils.addAll(items,new Object[]{null});
    JComboBox<Object> comboTemplate = new JComboBox<>(items);
    
    comboTemplate.setName(generateKey(sw));
    comboTemplate.setSelectedItem(TEXT_DISABLED);
    
    // Create label
    String description = sw.getDescription();
    String name = sw.getCustomName();
    String label = Strings.isBlank(name)
        ? description
        : description + " \"" + name + "\"";

    builder.append(label, comboTemplate);

    builder.nextLine();
    builder.appendRelatedComponentsGapRow();
    builder.nextLine();

    comboTemplate.putClientProperty(CLIENT_PROPERTY_LABEL, label);
    allComboBoxes.add(comboTemplate);
    super.registerPersistentComponent(comboTemplate);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;

    for (JComboBox<?> combo : allComboBoxes) {
      if (combo.getSelectedItem() == null) {
        result = "Please select a switch for control logic: " + combo.getClientProperty(CLIENT_PROPERTY_LABEL);
        break;
      }
    }

    return result;
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {
      scrollPane1.setBorder(null);
      scrollPane1.setViewportBorder(null);
      scrollPane1.setViewportView(contentPanel);
    }
    add(scrollPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JPanel contentPanel;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Generates a key for accessing or storing SwitchTemplate in WizardData.
   */
  static String generateKey(SwitchGearLogic switchlogic) {
    return "SwitchTemplateSelection-" + switchlogic.getUUID();
  }

  
  static SwitchGearLogic[] getEnabledCircuitBreakers(Map wizardData, IConfig configData) {
    Collection<SwitchGearLogic> sgls = CommissioningWizard.findSwitchLogic(configData);
    ArrayList<SwitchGearLogic> enabledCBs = new ArrayList<>();
    String key;
    for (SwitchGearLogic sgl : sgls) {
      key = StepSwitch.generateKey(sgl);
      Object template = wizardData.get(key);
      if (template != null && template instanceof SwitchTemplate
          && ((SwitchTemplate)template).isCircuitBreaker()) {
        enabledCBs.add(sgl);
      } 
    }
    
    return enabledCBs.toArray(new SwitchGearLogic[enabledCBs.size()]);
  }
  
  static DefaultListRenderer createCBRenderer() {
    return new DefaultListRenderer(new StringValue() {
      @Override
      public String getString(Object arg0) {
        if(arg0 == null)
          return " ";
        ICLogic logic = (ICLogic) arg0;
        String name = logic.getCustomName();
        if(Strings.isBlank(name))
          name = logic.getDescription();
        return name;
      }
    });
  }

}
