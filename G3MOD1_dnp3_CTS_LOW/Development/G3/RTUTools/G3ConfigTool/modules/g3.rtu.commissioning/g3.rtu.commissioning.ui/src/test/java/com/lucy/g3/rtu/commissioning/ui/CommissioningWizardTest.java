
package com.lucy.g3.rtu.commissioning.ui;

import java.io.File;

import com.lucy.g3.rtu.commissioning.ui.wizard.CommissioningWizard;
import com.lucy.g3.rtu.config.integrated.ConfigPlugin;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataWriter;

public class CommissioningWizardTest {

  public static void main(String[] args) throws Exception {
    if(args.length != 2) {
      System.err.println("arguments required: [0]input [1]output");
      return;
    }
   ConfigPlugin.init();
   IConfig data = new XmlDataReader(null).readFile(new File(args[0]));
   CommissioningWizard.showWizardDialog(data, null);
   new XmlDataWriter(data, null).saveToFile(args[1]);
   
  }
}

