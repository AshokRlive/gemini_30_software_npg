/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.wizard;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXLabel;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.port.serial.ISerialPort;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannel;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannelConf;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBChannelTCPConf;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBSession;
import com.lucy.g3.rtu.config.protocol.shared.domain.ProtocolChannelSerialConf;
import com.lucy.g3.rtu.config.template.custom.mmb.MMBTemplate;
import com.lucy.g3.xml.gen.common.ModBusEnum.LU_MBLINK_TYPE;

/**
 * Wizard Page for power meter.
 */
class StepFieldDevice extends AbstractCommissioningStep {
  
  static final String TEXT_DISABLED = CommissioningWizard.TEXT_DISABLED;
  
  static final String KEY_IS_CREATED = "StepPowerMeterCreated";

  private Logger log = Logger.getLogger(StepFieldDevice.class);
  private final Collection<MMBChannel> channels;
  private final Collection<MMBTemplate> templates;

  private static final String CLIENT_PROPERTY_LABEL = "clientPropertylabel";

  private final ArrayList<JComboBox<?>> allComboBoxes = new ArrayList<>();


  public StepFieldDevice(Collection<MMBChannel> channels, Collection<MMBTemplate> templates) {
    super("Configure Field Device");
    this.channels = channels;
    this.templates = templates;
    
    putWizardData(KEY_IS_CREATED, Boolean.TRUE);
    initComponents();
  }

  private void createUIComponents() {
    DefaultFormBuilder builder = new DefaultFormBuilder(
        new FormLayout("3*(default, $ugap), default:grow", ""));
    
    // Add entries
    for (MMBChannel channel : channels) {
      addChannelEntry(builder, channel);
    }

    contentPanel = builder.getPanel();
  }

  private void addChannelEntry(DefaultFormBuilder builder, MMBChannel channel) {
    Collection<MMBSession> sesns = channel.getAllSessions();
    if (sesns.isEmpty())
      return;

    // Append channel information
    String channelInfo = "";
    try {
      MMBChannelConf conf = channel.getChannelConfig();
      LU_MBLINK_TYPE type = conf.getMbType();
      if (type == LU_MBLINK_TYPE.LU_MBLINK_TYPE_TCP) {
        MMBChannelTCPConf tcp = conf.getTcpConf();
        channelInfo = String.format("IP: %s  Port: %s", tcp.getIpAddress(), tcp.getIpPort());
        
      } else {
        ProtocolChannelSerialConf serial = conf.getSerialConf();
        ISerialPort port = serial.getSerialPort();
        if (port != null) {
          channelInfo = String.format("Port: %s  Baud Rate: %s  "
              + "Data Bits: %s  Stop Bits: %s  Parity: %s  Port Mode: %s", 
              port.getDisplayName(), port.getBaudRate(), port.getDataBit(), port.getStopBit(), port.getParity(),
              port.getPortMode());
        } else {
          channelInfo = String.format("Port: Not Configured");
        }
      }
    } catch (Throwable e) {
      log.error("Failed to create channel info");
    }
    
    channelInfo = String.format("Channel:%s  %s", channel.getChannelName(), channelInfo);
    JXLabel lblChnlInfo = new JXLabel(channelInfo);
    lblChnlInfo.setLineWrap(true);
    builder.append(lblChnlInfo, 7);
    builder.nextLine();
    
    // Append gap
    builder.appendUnrelatedComponentsGapRow();
    builder.nextLine();
    
    // Add title
    builder.append("<html><b>Description</b></html>",
        new JLabel("<html><b>Type</b></html>"),
        new JLabel("<html><b>Address</b></html>"), 
        new JLabel("<html><b>Model</b></html>"));
    
    // Append session information
    for (MMBSession sesn : sesns) {
      addSesnEntry(builder, sesn);
    }
    
    // Append gap
    builder.appendParagraphGapRow();
    builder.nextLine();
  }

  private void addSesnEntry(DefaultFormBuilder builder, MMBSession sesn) {
    // Create selection comboBox
    Object[] items = ArrayUtils.addAll(new Object[] { TEXT_DISABLED }, findTemplatesByClassifier(templates, sesn.getClassifier()));
    items  = ArrayUtils.addAll(items,new Object[]{null});
    JComboBox<Object> comboTemplate = new JComboBox<>(items);
    
    comboTemplate.setName(generateKey(sesn));
    comboTemplate.setSelectedItem(TEXT_DISABLED);
    comboTemplate.setRenderer(new MMBDeviceTemplateRenderer());

    String devName = sesn.getDeviceName();
    String slaveAddr   = String.valueOf(sesn.getSlaveAddress());
    String type        = "Modbus-" + sesn.getProtocolChannel().getChannelConfig().getMbType().getDescription();

    builder.append(devName, new JLabel(type), new JLabel(slaveAddr), comboTemplate);
    builder.nextLine();
    builder.appendRelatedComponentsGapRow();
    builder.nextLine();

    comboTemplate.putClientProperty(CLIENT_PROPERTY_LABEL, devName);
    allComboBoxes.add(comboTemplate);
    super.registerPersistentComponent(comboTemplate);
  }

  private static Object[] findTemplatesByClassifier(Collection<MMBTemplate> templates, String classifier) {
    ArrayList<MMBSession> foundTempls = new ArrayList<>();
    
    for (MMBSession templ : templates) {
      if (Strings.isBlank(classifier)
          || classifier.equalsIgnoreCase(templ.getClassifier())) {
        foundTempls.add(templ);
      }
    }
    return foundTempls.toArray(); 
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;

    for (JComboBox<?> combo : allComboBoxes) {
      if (combo.getSelectedItem() == null) {
        result = "Please select a model for: " + combo.getClientProperty(CLIENT_PROPERTY_LABEL);
        break;
      }
    }

    return result;
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {
      scrollPane1.setBorder(null);
      scrollPane1.setViewportView(contentPanel);
    }
    add(scrollPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JPanel contentPanel;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static class MMBDeviceTemplateRenderer extends DefaultListCellRenderer {

    MMBDeviceTemplateRenderer() {
      //setPreferredSize(new Dimension(100, 18));
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (value != null && value instanceof MMBSession) {
        MMBSession templ = (MMBSession) value;
        String display = templ.getDeviceModel();
        if (Strings.isBlank(display)) {
          display = "Unknown Model ModBus Device";
        }

        setText(display);
      }

      return comp;
    }
  }


  /**
   * Generates a key for accessing or storing MMBSession template in WizardData.
   */
  static String generateKey(MMBSession sesn) {
    return "PowerMeterSelection-" + sesn.getUUID(); 
  }

}
