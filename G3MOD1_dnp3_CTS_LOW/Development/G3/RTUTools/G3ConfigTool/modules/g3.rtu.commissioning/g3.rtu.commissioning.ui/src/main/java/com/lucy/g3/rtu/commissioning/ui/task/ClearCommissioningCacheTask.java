/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.ui.task;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;

/**
 * A task for sending command to operate configuration file in RTU device.
 */
public class ClearCommissioningCacheTask extends Task<Void, Void> {

  public static final int OPERATION_ERASE = 0;
  public static final int OPERATION_ACTIVATE = 1;
  public static final int OPERATION_RESTORE = 2;

  private Logger log = Logger.getLogger(ClearCommissioningCacheTask.class);

  private final CommissioningAPI cmd;
  
  /**
   * Constructs a task to operate configuration file in RTU.
   *
   * @param app
   *          current application.
   * @param operation
   *          one of {@code OPERATION_ERASE},{@code OPERATION_ACTIVATE},
   *          {@code OPERATION_RESTORE}
   */
  public ClearCommissioningCacheTask(Application app, CommissioningAPI cmd) {
    super(app);
    this.cmd = Preconditions.checkNotNull(cmd, "callback must not be null");
    setUserCanCancel(false);
  }

  @Override
  protected Void doInBackground() throws Exception {
    cmd.cmdEraseCommissioningCache();
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    log.error("Failed to erase commissioning cache", cause);
  }


}
