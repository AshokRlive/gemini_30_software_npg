/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.commissioning.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import javax.swing.JOptionPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.commissioning.ui.ICommissioningInvoker;
import com.lucy.g3.rtu.commissioning.ui.wizard.CommissioningCache;
import com.lucy.g3.rtu.commissioning.ui.wizard.CommissioningWizard;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.LocalRemoteCode;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.PollingMode;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.comms.service.rtu.control.CommissioningAPI;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.config.integrated.ConfigPlugin;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataWriter;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SWITCH_OPERATION;



/**
 * The Class CommissioningTest.
 */
public class CommissioningApp extends SingleFrameApplication implements ICommissioningInvoker{
  
  private IConfig data;
  private File configFile;
  
  @Override
  public IConfig getConfigData() {
    return data;
  }

  @Override
  public Application getApp() {
    return this;
  }

  @Override
  public TaskService getG3TaskService() {
    return getContext().getTaskService();
  }

  @Override
  public ConnectionState getConnectionState() {
    return ConnectionState.CONNECTED_TO_G3_APP;
  }

  @Override
  public void loginWithLastAccount() {
    doSomething("logining",2);
  }
  
  private void doSomething(String message, int timecostSecs) {
    System.out.println(message+"...");
    try {
      Thread.sleep(timecostSecs * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void cmdOperateLogic(short group, short period) throws Exception {
    
  }

  @Override
  public void cmdOperateSwitch(short group, SWITCH_OPERATION operation, LocalRemoteCode localRemote) throws Exception {
    
  }

  @Override
  public void cmdRegisterModules() throws Exception {
    doSomething("Registering module",1);    
  }

  @Override
  public void cmdActivateConfig() throws Exception {
    doSomething("Activiting config",1);        
  }

  @Override
  public String execTaskWriteConfigAndWait(IConfig configData) throws Exception {
    doSomething("Writing config",3);
    return "NewIP:1.1.1.1";
  }

  @Override
  public void execTaskRestartRTUAndWait(String newIP) throws Exception {
    doSomething("Restarting RTU",3);
    
  }

  @Override
  public VirtualPointData createPointData(VirtualPoint point) {
    return null;
  }
  
  @Override
  protected void shutdown() {
    super.shutdown();
  }

  private void saveConfig(File file) {
    try {
      new XmlDataWriter(data, null).saveToFile(file.getAbsolutePath());
    } catch (Exception e) {
      MessageDialogs.error("Cannot write file: " + file, e);
    }
  }

  @Override
  protected void initialize(String[] args) {
    ConfigPlugin.init();
  }

  @Override
  protected void startup() {
    configFile = chooseConfigFile();
    if(configFile == null)
      return;
    
    try {
      InputStream is = new FileInputStream(configFile);
      data = new XmlDataReader(null).readFromStream(is);
    } catch (Exception e) {
      MessageDialogs.error("Cannot read file: " + configFile, e);
      return;
    }    
    
    if(CommissioningWizard.showWizardDialog(data,CommissioningCache.readFromDefaultCacheFile())) {
      File output = new File(configFile.getParentFile(), "Output_" + configFile.getName());
      saveConfig(output);
      JOptionPane.showMessageDialog(null, "New config generated to:"+output);
    }
    
    exit();
  }

  private File chooseConfigFile() {
    return DialogUtils.showFileChooseDialog(null, "Choose a G3 Configuration File", G3Files.FC_FILTER_G3CFG);
  }

  @Override
  public void showLoginDialog() {
    
  }


  @Override
  public Task<?,?> createConfigImportTask() {
    return null;
  }

  @Override
  public CommissioningAPI getCommissioningCMD() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void cmdGetPointStatus(List<IPointData> arrayList, PollingMode mode) throws Exception {
    // TODO Auto-generated method stub
    
  }

  @Override
  public G3ConfigAPI getG3ConfigAPI() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public RTUInfo getRTUInfo() {
    // TODO Auto-generated method stub
    return null;
  }
  

}

