/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.app;

import org.jdesktop.application.Application;

public class Main {
  public static void main(final String[] args) {
    Application.launch(IEC61131App.class, args);
  }
}

