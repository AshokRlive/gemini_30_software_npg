/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.app;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.swing.UIManager;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jdesktop.application.SingleFrameApplication;

import com.lucy.g3.iec61131.ide.ui.AbstractManager;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;
import com.lucy.g3.iec61131.ide.ui.IEC61131Window;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager;
import com.lucy.g3.rtu.manager.G3RTU;

/**
 *
 */
public class IEC61131App extends SingleFrameApplication{
  private Logger log = Logger.getLogger(IEC61131App.class);
  
  private final IEC61131Context context = new IEC61131Context();
  
  @Override
  protected void initialize(String[] args) {
    if(args.length >= 1) 
      context.debugger.setHostAddress(args[0]);
    if(args.length >= 2) 
      context.source.readFile(new File(args[1]));
    
    // Init log4j
    try {
      Properties props = new Properties();
      props.load(getClass().getResourceAsStream("/log4j.properties"));
      PropertyConfigurator.configure(props);
    } catch (IOException e) {
      System.err.println("Failed to load log4j configuration!");
    }
    
    // Init RTU comms
    final G3RTU rtu = AbstractManager.getRTU();
    rtu.setHost(context.debugger.getHostAddress());
    context.debugger.addPropertyChangeListener(DebugManager.PROPERTY_HOST_ADDRESS, 
        new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            rtu.setHost((String) evt.getNewValue());
          }
        });
    
    
    log.info("Starting IEC61131 application...");
  }

  @Override
  protected void startup() {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Throwable e) {}
    
    IEC61131Window ui = new IEC61131Window(context);
    setMainFrame(ui);
    ui.setAllowChangeHost(true);
    ui.setVisible(true);
    ui.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(WindowEvent e) {
        exit();
      }
      
    });
    
    ui.showImportWizard();
  }
  
}
