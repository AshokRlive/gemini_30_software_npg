/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.builder;

import java.util.concurrent.TimeUnit;

import org.junit.Before;

import com.lucy.g3.iec61131.ide.ui.source.builder.BuildingManager;


/**
 *
 */
public class BuildingManagerTest {
  private BuildingManager fixture;
  
  @Before
  public void setup() {
    fixture = new BuildingManager(null);
  }
  
  // @Test // Manual test
  public void testProg() throws InterruptedException {
    fixture.programme();
    fixture.getBuildService().awaitTermination(3, TimeUnit.SECONDS);
  }

}

