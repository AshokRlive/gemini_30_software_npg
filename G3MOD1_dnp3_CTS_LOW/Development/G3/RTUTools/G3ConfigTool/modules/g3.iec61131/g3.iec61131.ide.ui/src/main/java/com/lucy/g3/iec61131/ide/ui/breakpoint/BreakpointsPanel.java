/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.breakpoint;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.jgoodies.binding.adapter.Bindings;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
public class BreakpointsPanel extends JPanel {
  private final BreakpointManager manager;
  private IBreakpointPanelCallback callback;
  
  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private BreakpointsPanel() {
    this.manager = new BreakpointManager(null);
    initComponents();
  }
  
  public BreakpointsPanel(BreakpointManager manager, IBreakpointPanelCallback callback) {
    this.manager = manager;
    this.callback = callback;
    initComponents();
  }


  private void tableBreakpointsMouseClicked(MouseEvent e) {
    if(UIUtils.isDoubleClick(e)){
      scrollToTheFirstSelectBreakpoint();
    }
  }

  private void scrollToTheFirstSelectBreakpoint() {
    List<Breakpoint> bps = manager.getSelectedBreakpoints();
    if(!bps.isEmpty()) {
      int line = bps.get(0).getLineNum() - 1;
      callback.scrollToLine(line);
    }
  }

  private void createUIComponents() {
    tableBreakpoints= new JTable(manager.getBreakpointDataModel());
    Bindings.bind(tableBreakpoints, manager.getBreakpointListModel(), manager.getBreakpointSelectModel());
    
    tableBreakpoints.getColumnModel().getColumn(0).setMaxWidth(100);
  }

  private void btnGotoActionPerformed(ActionEvent e) {
    scrollToTheFirstSelectBreakpoint();
  }

  private void btnRemoveActionPerformed(ActionEvent e) {
    List<Breakpoint> bps = manager.getSelectedBreakpoints();
    if(!bps.isEmpty()) {
      for (Breakpoint bp : bps) {
          manager.removeBreakpoint(bp);
      }
    }
  }

  private void btnRemoveAllActionPerformed(ActionEvent e) {
    Breakpoint[] all = manager.getAllBreakpoints();
    for (int i = 0; i < all.length; i++) {
        manager.removeBreakpoint(all[i]);
    }
  }

  private void btnSelectAllActionPerformed(ActionEvent e) {
    tableBreakpoints.selectAll();
  }


  private void popupMenuPopupMenuWillBecomeVisible(PopupMenuEvent e) {
    boolean hasSelection = manager.hasSelections();
    btnGoto.setEnabled(hasSelection);
    btnRemove.setEnabled(hasSelection);
    btnRemoveAll.setEnabled(!manager.isEmpty());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();
    popupMenu = new JPopupMenu();
    btnGoto = new JMenuItem();
    btnRemove = new JMenuItem();
    btnRemoveAll = new JMenuItem();
    btnSelectAll = new JMenuItem();

    //======== this ========
    setPreferredSize(new Dimension(150, 427));
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {

      //---- tableBreakpoints ----
      tableBreakpoints.setFillsViewportHeight(true);
      tableBreakpoints.setComponentPopupMenu(popupMenu);
      tableBreakpoints.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          tableBreakpointsMouseClicked(e);
        }
      });
      scrollPane1.setViewportView(tableBreakpoints);
    }
    add(scrollPane1, BorderLayout.CENTER);

    //======== popupMenu ========
    {
      popupMenu.addPopupMenuListener(new PopupMenuListener() {

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          popupMenuPopupMenuWillBecomeVisible(e);
        }
      });

      //---- btnGoto ----
      btnGoto.setText("Goto to File");
      btnGoto.setIcon(
          new ImageIcon(getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/checkin_action.gif")));
      btnGoto.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnGotoActionPerformed(e);
        }
      });
      popupMenu.add(btnGoto);
      popupMenu.addSeparator();

      //---- btnRemove ----
      btnRemove.setText("Remove");
      btnRemove.setIcon(
          new ImageIcon(getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/progress_rem.gif")));
      btnRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnRemoveActionPerformed(e);
        }
      });
      popupMenu.add(btnRemove);

      //---- btnRemoveAll ----
      btnRemoveAll.setText("Remove All");
      btnRemoveAll.setIcon(
          new ImageIcon(getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/progress_remall.gif")));
      btnRemoveAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnRemoveAllActionPerformed(e);
        }
      });
      popupMenu.add(btnRemoveAll);

      //---- btnSelectAll ----
      btnSelectAll.setText("Select All");
      btnSelectAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          btnSelectAllActionPerformed(e);
        }
      });
      popupMenu.add(btnSelectAll);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JTable tableBreakpoints;
  private JPopupMenu popupMenu;
  private JMenuItem btnGoto;
  private JMenuItem btnRemove;
  private JMenuItem btnRemoveAll;
  private JMenuItem btnSelectAll;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  public interface IBreakpointPanelCallback {

    /**
     * @param line line number from zero.
     * @param setCaretToLine
     */
    void scrollToLine(int line);
    
  }
}
