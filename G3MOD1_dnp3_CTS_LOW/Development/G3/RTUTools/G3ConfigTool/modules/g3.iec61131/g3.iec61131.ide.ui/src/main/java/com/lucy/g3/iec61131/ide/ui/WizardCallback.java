/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.lucy.g3.iec61131.debug.comms.IEC61131DebuggerComms;
import com.lucy.g3.iec61131.ide.ui.source.CommonResources;
import com.lucy.g3.iec61131.ide.ui.source.editor.FileMonitor;
import com.lucy.g3.iec61131.ide.ui.util.FileUtil;
import com.lucy.g3.iec61131.ide.ui.wizard.IImportCallback;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;


/**
 *
 */
class WizardCallback implements IImportCallback {
  private Logger log = Logger.getLogger(WizardCallback.class);
  
  private final IEC61131Context context;
  
  public WizardCallback(IEC61131Context context) {
    super();
    this.context = context;
  }

  @Override
  public String[] readSchemeNamesFromRTU() throws Exception {
    FileTransfer fileTransfer = AbstractManager.getRTU().getComms().getFileTransfer();

    String[] autolibFiles = fileTransfer.cmdGetFileList(CommonResources.AUTO_LIB_DIR);
    ArrayList<String> sourceNames = new ArrayList<>();
    
    for (int i = 0; i < autolibFiles.length; i++) {
      if(autolibFiles[i].endsWith(CommonResources.IL_SUFFIX))
        sourceNames.add(autolibFiles[i]);
    }
    return sourceNames.toArray(new String[sourceNames.size()]);
  
  }
  
  @Override
  public File readSchemeFromRTU(String schemeName, File destDir) throws Exception {
    /* Note: the file monitor should be paused when transferring file.*/
    FileMonitor fileMonitor = context.source.getFileMonitor();
    fileMonitor.pause();
    
    File fileToSave = new File(destDir, schemeName);
    FileReadingTask task = FileTransferService.createFileReadTask(
        AbstractManager.getRTU().getComms().getFileTransfer(),
        CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_AUTOLIB, schemeName, fileToSave, false);
    task.setShowConfirmOverriteDialog(true);
    task.execute();
    try{
      return task.get();
    }catch(Exception e) {
      if(task.isCancelled())
        return null;
      else
        throw e;
    }finally{
      fileMonitor.resume();
    }
  }

  @Override
  public File[] getRecentSchemeFiles() {
     List<String> list = context.recentItems.getItems();
     File[] array = new File[list.size()];
     int i = 0;
     
     for (String path : list ) {
      array[i++] = new File(path);
    }
     
     return array;
  }

  @Override
  public File getSchemeFileDir() {
    return context.source.getSourceDir();
  }

  @Override
  public void openFile(File schemefile) {
    if(schemefile != null)
      context.source.readFile(schemefile);
  }

  @Override
  public String readRunningSchemeNameFromRTU() throws Exception {
    String sourceName = null;
    IEC61131DebuggerComms comms = context.comms;
    final boolean connected= comms.isConnected();
    
    try {
      if(!connected)
        comms.connect();
      
      String libName = comms.getLibraryFileName();
      log.info("Running automation lib name:"+libName);
      
      sourceName = FileUtil.convertIEC61131LibPathToSourceName(libName, CommonResources.IL_SUFFIX);
      log.info("Running automation source name:"+sourceName);
      
    } catch (Exception e) {
      log.error("Failed to read auto lib path from:"+comms.getIp() + " port:" + comms.getPort());
    }finally {
      if(connected == false && comms != null) {
        comms.disconnect();
      }
    }
    
    return sourceName; 
  }

}

