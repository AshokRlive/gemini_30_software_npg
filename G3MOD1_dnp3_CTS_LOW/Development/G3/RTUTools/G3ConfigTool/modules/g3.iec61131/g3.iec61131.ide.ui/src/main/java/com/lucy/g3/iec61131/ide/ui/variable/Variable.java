/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.variable;

import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DBG_VAR;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVar;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVarRef;

/**
 *
 */
public class Variable{

  private final String name;
  private final DBG_VAR type;
  private final byte index;
  private final DebugVarRef ref;
  
  private int value;

  public Variable(String name, DBG_VAR type, byte index) {
    this.name = name;
    this.type = type;
    this.index = index;
    this.ref = new DebugVarRef(type, index);
  }

  public int getId() {
    return index;
  }
  
  public DebugVarRef getRef() {
    return ref; 
  }
  
  public String getName() {
    return name;
  }
  
  public int getValue() {
    return value;
  }
  
  private void setValue(int value) {
    this.value = value;
  }

  
  public DBG_VAR getType() {
    return type;
  }

  
  public byte getIndex() {
    return index;
  }

  public boolean copyFrom(DebugVar varData) {
    if(varData == null)
      return false;
    
    if(ref.equals(varData.ref)) {
      setValue(varData.value);
      return true;
    }
    
    return false;
  }
  
}
