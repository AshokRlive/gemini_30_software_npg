
package com.lucy.g3.iec61131.ide.ui.source.builder;

import com.lucy.g3.iec61131.ide.ui.compiler.IOutputHandler;


public class StringOutputHandler implements IOutputHandler {
  private StringBuilder  sb = new StringBuilder();
  
  @Override
  public void appendErr(String msg) {
    sb.append(msg);
  }

  @Override
  public void appendSuccess(String msg) {
    sb.append(msg);
  }

  @Override
  public void append(String output) {
    sb.append(output);
  }
  
  public String getOutputAsString() {
    return sb.toString();
  }
}

