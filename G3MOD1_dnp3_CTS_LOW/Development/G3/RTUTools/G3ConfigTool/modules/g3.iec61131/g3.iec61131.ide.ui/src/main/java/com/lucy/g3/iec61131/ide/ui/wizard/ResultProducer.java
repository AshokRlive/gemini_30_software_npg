/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;


/**
 *
 */
class ResultProducer implements WizardResultProducer {
  private final IImportCallback callback;
  private final int methodOption;
  public ResultProducer(IImportCallback callback, int methodOption) {
    super();
    this.callback = callback;
    this.methodOption = methodOption;
  }

  @Override
  public Object finish(Map settings) throws WizardException {
    File schemeFile = null;
    
    switch (methodOption) {
    case AutoSchemeImportWizard.OPTION_CREATE_NEW:
      schemeFile = new File((String) settings.get(StepCreateNew.KEY_SCHEME_FILE_TO_CREATE)); 
      try {
        schemeFile.createNewFile();
      } catch (IOException e) {
        throw new WizardException("Failed to create a new file cause:"+e.getMessage());
      }
      break;
      
    case AutoSchemeImportWizard.OPTION_OPEN_LOCAL:
      schemeFile = new File((String) settings.get(StepImportLocalScheme.KEY_SCHEME_FILE_TO_IMPORT)); 
      break;
      
    case AutoSchemeImportWizard.OPTION_READ_RTU:
      schemeFile = new File((String) settings.get(StepReadSchemeFromRTU.KEY_SCHEME_FILE_TO_READ)); 
      break;
      
    default:
      break;
    }
    
    if(schemeFile != null)
      callback.openFile(schemeFile);
    
    return schemeFile;
  }

  @Override
  public boolean cancel(Map settings) {
    return true;
  }

}

