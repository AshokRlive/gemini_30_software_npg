
package com.lucy.g3.iec61131.ide.ui.compiler;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.lucy.g3.iec61131.ide.ui.util.FileUtil;

public class CrossCompiler extends AbstractCompiler{
  private final String includePath;
  private final String commonSourceFile;
  
  private final String compilerPath;
  private final String prefix;
  
  private String outputFile;
  
  private Logger log = Logger.getLogger(CrossCompiler.class);
  
  public CrossCompiler(String compilerPath, String compilerPrefix, String includePath, String commonSourceFile, IOutputHandler... handler) {
    super(handler);
    this.compilerPath = compilerPath;
    this.prefix = compilerPrefix;
    this.includePath = includePath;
    this.commonSourceFile = commonSourceFile;
  }

  @Override
  public int compile(String sourceFile, String outputDir) throws IOException, InterruptedException {
    
    final String gcc = compilerPath +File.separator+ prefix+"gcc";
    final String commonObj  = outputDir + FileUtil.SEP + "common.obj";
    final String sourceObj  = outputDir + FileUtil.SEP + FileUtil.getFileNameWithoutExtension(sourceFile)+".obj";
    final String sharedLib  = outputDir + FileUtil.SEP + FileUtil.convertIEC61131SourcePathToLibName(sourceFile);
    
    new File(outputDir).mkdirs();
    
    log.info("Start cross compiling...");
    
    /* Compile common code*/
    Process p = new ProcessBuilder( 
        gcc,
        "-c",
        "-fpic",
        commonSourceFile,
        "-o",
        commonObj,
        "-I",
        includePath
        ).redirectErrorStream(true).start();
    waitAndCheckRet(p);
    log.info("common code compiled!");
    
    /* Compile source code*/
    p = new ProcessBuilder( 
        gcc,
        "-c",
        "-fpic",
        sourceFile,
        "-o",
        sourceObj,
        "-I",
        includePath
        ).redirectErrorStream(true).start();
    waitAndCheckRet(p);
    log.info("scheme source code compiled!");
    
    /* Link to shared library*/
    p = new ProcessBuilder( 
        gcc,
        "-shared",
        "-nostdlib",
        "-o",
        sharedLib,
        commonObj,
        sourceObj
        ).redirectErrorStream(true).start();
    waitAndCheckRet(p);
    log.info("shared library compiled!");
    
    /* Delete temp files*/
    new File(commonObj).delete();
    new File(sourceObj).delete();
    
    this.outputFile = sharedLib;
    return 0;
  }

  public String getOutputFile() {
    return outputFile;
  }
  
  
  private void waitAndCheckRet(Process p) throws IOException, InterruptedException {
    handleOutput(p);
    p.waitFor();
    if(p.exitValue() != 0) {
      throw new RuntimeException("Cross compiler exit with error: " + p.exitValue());
    }
  }

}

