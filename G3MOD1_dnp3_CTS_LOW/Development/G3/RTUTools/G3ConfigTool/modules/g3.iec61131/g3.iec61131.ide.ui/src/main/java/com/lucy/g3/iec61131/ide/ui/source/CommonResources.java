/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source;

import javax.swing.filechooser.FileFilter;

import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.iec61131.ide.ui.util.FileUtil;

/**
 *
 */
public class CommonResources {

  public final static FileFilter IL_FILTER = G3Files.FC_FILTER_IL_SOURCE;
  public final static String AUTO_LIB_DIR = "./lib/automation/";
  public final static String IL_SUFFIX = FileUtil.IL_SUFFIX;

  private CommonResources() {
  }
}
