/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui;

import javax.swing.Action;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 *
 */
public abstract class AbstractManager extends Model {
  private ApplicationActionMap actions =
      Application.getInstance().getContext().getActionManager().getActionMap(getClass(), this);
  
  protected final IEC61131Context context;
  
  private Application app;


  public AbstractManager(IEC61131Context context) {
    this.context = context;
  }
  
  public Application getApp() {
    if(app == null) {
      app = Application.getInstance();
    }
    
    return app;
  }
  
  public IEC61131Context getContext() {
    return context;
  }

  public Action getAction(String actionKey) {
    Action action = actions.get(actionKey);
    if(action == null)
      throw new IllegalArgumentException("Action \"" + actionKey +"\" not foudin class:"+getClass());
    
    if(context != null && context.getParent() != null)
      action =  new BusyCursorAction(action, context.getParent());
    else
      Logger.getLogger(getClass()).warn("Parent not found. Cannot create busyCursorAction!");
    
    return action;
  }
  
  public static G3RTU getRTU() {
    return G3RTUFactory.getDefault();
  }

}
