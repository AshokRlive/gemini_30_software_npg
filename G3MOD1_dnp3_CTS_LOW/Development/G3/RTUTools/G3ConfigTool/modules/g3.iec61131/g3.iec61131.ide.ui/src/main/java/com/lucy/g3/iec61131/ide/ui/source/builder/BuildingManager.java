/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.builder;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.iec61131.ide.ui.AbstractManager;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;
import com.lucy.g3.iec61131.ide.ui.source.tasks.AutoLibBuildTask;
import com.lucy.g3.iec61131.ide.ui.source.tasks.AutoLibProgTask;
import com.lucy.g3.iec61131.ide.ui.source.tasks.SDPPatchingTask;
import com.lucy.g3.iec61131.ide.ui.util.FileUtil;


/**
 * For managing build actions.
 */
public class BuildingManager extends AbstractManager{

  public final static String ACTION_KEY_BUILD = "build";
  public final static String ACTION_KEY_CLEAN = "clean";
  public final static String ACTION_KEY_CANCEL_BUILD = "cancelBuild";
  
  public final static String ACTION_KEY_PATCH_SDP = "patchSDP";
  public final static String ACTION_KEY_PROGRAMME = "programme";
  
  public final static String PROPERTY_IS_BUILDING = "building";
  public final static String PROPERTY_IS_NOT_BUILDING = "notBuilding";
  
  private TaskService buildService= new TaskService("61131BuildService", new ThreadPoolExecutor(
      1, // corePool size
      1, // maximumPool size
      1L, TimeUnit.SECONDS, // non-core threads time to live
      new LinkedBlockingQueue<Runnable>()));
  
  private List<Task<?,?>> buildTasks;
  private boolean isBuilding;
  
  public BuildingManager(IEC61131Context context) {
    super(context);
    
    buildService.addPropertyChangeListener("tasks", new PropertyChangeListener() {
      @SuppressWarnings("unchecked")
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setBuildTasks((List<Task<?,?>>) evt.getNewValue());
      }
    });
  }
  
  private void setBuildTasks(List<Task<?,?>> buildingTasks) {
    this.buildTasks = buildingTasks;
    setBuilding(buildingTasks != null && !buildingTasks.isEmpty());
  }
  
  private void setBuilding(boolean building) {
    boolean oldValue = isBuilding();
    this.isBuilding = building;
    firePropertyChange(PROPERTY_IS_BUILDING, oldValue, building);
    firePropertyChange(PROPERTY_IS_NOT_BUILDING, !oldValue, !building);
  }
  
  public boolean isBuilding() {
    return isBuilding;
  }
  
  public boolean isNotBuilding() {
    return !isBuilding;
  }
  
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_IS_NOT_BUILDING)
  public void build() {
    if(context.source.getSourceFile() == null) {
      JOptionPane.showMessageDialog(context.getParent(), "Not source file loaded!", "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
      
    BuildingParameters param = new BuildingParameters();
    param = new BuildingParameters();
    param.put(BuildingParameters.KEY_IL_COMPILER_PATH, context.prefs.getILCompilerPath());
    param.put(BuildingParameters.KEY_CROSS_COMPILER_PATH,   context.prefs.getCrossCompilerPath());
    param.put(BuildingParameters.KEY_CROSS_COMPILER_PREFIX, context.prefs.getCrossCompilerPrefix());
    param.put(BuildingParameters.KEY_TARGET_DIR_PATH, context.prefs.getBuildingDir());
    param.put(BuildingParameters.KEY_SOURCE_PATH, context.source.getSourceFile().getAbsolutePath());
    
    // Save file before build.
    context.source.save();
    
    AutoLibBuildTask task = new AutoLibBuildTask(getApp(), ConsoleOutputHandler.INSTANCE, param);
    task.setParent(context.getParent());
    buildService.execute(task);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_IS_NOT_BUILDING)
  public void clean() {
    String path = context.prefs.getBuildingDir();
    int ret = JOptionPane.showConfirmDialog(context.getParent(), 
        "Do you want to clean the building directory:\n\"" + path+"\"", 
        "Clean", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
    
    if(ret == JOptionPane.YES_OPTION) {
      try {
        FileUtils.deleteDirectory(new File(path));
      } catch (IOException e) {
        MessageDialogs.error(context.getParent(), "Failed to clean target directory!", e);
      }
    }
  }
  
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_IS_BUILDING)
  public void cancelBuild(){
    if(buildTasks != null && !buildTasks.isEmpty()) {
      context.console.appendMsg("Cancelling...");
      
      for (Task<?,?> t : buildTasks ) {
        t.cancel(true);
      }
      
    }else{
      context.console.appendMsg("No building process found.");
    }
  }
  
  @org.jdesktop.application.Action(enabledProperty = PROPERTY_IS_NOT_BUILDING)
  public void patchSDP() {
    // Prepare files
    File []files;
    try {
      files = getBuiltSchemeFiles();
    }catch (Exception e) {
      context.console.appendErr(e.getMessage());
      JOptionPane.showMessageDialog(context.getParent(),
          e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    SDPPatchingTask task = new SDPPatchingTask(getApp(), context.console, getCurrentSchemeName(), files);
    task.setParent(context.getParent());
    buildService.execute(task);
  }

  @org.jdesktop.application.Action
  public void programme() {
    File []files;
    try {
      files = getBuiltSchemeFiles();
    }catch (Exception e) {
      context.console.appendErr(e.getMessage());
      JOptionPane.showMessageDialog(context.getParent(),
          e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    buildService.execute(new AutoLibProgTask(getApp(), context.console, getRTU().getComms().getFileTransfer(), files));
  }
  
  private String getCurrentSchemeName() {
    File source = context.source.getSourceFile();
    if(source != null) {
      return FileUtil.getFileNameWithoutExtension(source.getAbsolutePath());
    }
    return null;
  }

  private File[] getBuiltSchemeFiles() throws IOException {
    File[] files = new File[2];
    
    /* Get IL source file*/
    files[0] = context.source.getSourceFile();
    if(files[0] == null || !files[0].exists()) {
      throw new IOException("No source file load!");
    }
    
    /* Get shared library file*/
    files[1] = new File(context.prefs.getBuildingDir(),
        FileUtil.convertIEC61131SourcePathToLibName(files[0].getAbsolutePath()));
    if(files[1] == null || !files[1].exists()) {
      throw new IOException("The scheme is not built!");

    }
    return files;
  }
  
  TaskService getBuildService() {
    return buildService;
  }
}

