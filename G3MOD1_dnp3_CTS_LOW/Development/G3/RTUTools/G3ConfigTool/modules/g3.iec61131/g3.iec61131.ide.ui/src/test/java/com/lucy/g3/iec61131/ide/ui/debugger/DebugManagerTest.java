/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.debugger;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager;


/**
 *
 */
public class DebugManagerTest {
  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAction() {
    DebugManager connection = new DebugManager(null);
    Assert.assertNotNull(connection.getAction(DebugManager .ACTION_KEY_START));
    Assert.assertNotNull(connection.getAction(DebugManager .ACTION_KEY_STOP));
  }

}

