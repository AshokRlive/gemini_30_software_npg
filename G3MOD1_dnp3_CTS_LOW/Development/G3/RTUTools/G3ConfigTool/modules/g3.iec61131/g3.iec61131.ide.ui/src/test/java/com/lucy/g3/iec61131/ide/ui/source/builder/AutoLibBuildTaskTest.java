/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.builder;

import java.util.concurrent.ExecutionException;

import org.jdesktop.application.Application;
import org.junit.Before;

import com.lucy.g3.iec61131.ide.ui.source.builder.BuildingParameters;
import com.lucy.g3.iec61131.ide.ui.source.tasks.AutoLibBuildTask;

/**
 *
 */
public class AutoLibBuildTaskTest {

  private BuildingParameters param;


  @Before
  public void setup() {
    param = new BuildingParameters();
    param.put(BuildingParameters.KEY_IL_COMPILER_PATH, "C:\\dev_env\\iec61131.jar");
    param.put(BuildingParameters.KEY_SOURCE_PATH, "E:\\temp\\autoscheme\\AutoChangeover.il");
    param.put(BuildingParameters.KEY_TARGET_DIR_PATH, "E:\\temp\\autoscheme\\target");
  }

  // @Test // Manual test
  public void testExecute() throws InterruptedException, ExecutionException {

    AutoLibBuildTask task = new AutoLibBuildTask(Application.getInstance(), null, param);
    task.execute();
    task.get();
  }

}
