/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui;

import java.awt.BorderLayout;
import java.io.File;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jdesktop.swingx.JXStatusBar;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.value.ConverterFactory;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.ObjectToStringConverter;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager;


/**
 * The StatusBar panel.
 */
class StatusBar extends JPanel {
  private final JLabel lblConnectStatus = new JLabel();
  private final JLabel lblHost = new JLabel();
  private final JLabel lblCurState = new JLabel();
  private final JLabel lblCurLine = new JLabel();
  private JXStatusBar statusBar;
  
  private final PresentationModel<DebugManager> pm;
  
  public StatusBar(DebugManager connection) {
    pm = new PresentationModel<>(connection);

    initComponents();
    initComponentsBinding();
  }
  
  private void initComponents() {
    statusBar = new JXStatusBar();
    statusBar.add(this.lblHost, new JXStatusBar.Constraint(120));
    statusBar.add(this.lblConnectStatus, new JXStatusBar.Constraint(140));
    statusBar.add(new JLabel(""), JXStatusBar.Constraint.ResizeBehavior.FILL);
    statusBar.add(this.lblCurState, new JXStatusBar.Constraint(140));
    statusBar.add(this.lblCurLine, new JXStatusBar.Constraint(140));
    
    setLayout(new BorderLayout());
    add(statusBar, BorderLayout.CENTER);
  }
  
  private void initComponentsBinding() {
    ValueModel vm;
    
    vm = pm.getModel(DebugManager.PROPERTY_HOST_ADDRESS);
    Bindings.bind(lblHost, vm);
    
    vm = pm.getModel(DebugManager.PROPERTY_CONNECT_STATUS);
    vm = new ConverterValueModel(vm, new ObjectToStringConverter());
    Bindings.bind(lblConnectStatus, vm);
    
    vm = pm.getModel(DebugManager.PROPERTY_RUNNING_STATE);
    vm = ConverterFactory.createStringConverter(vm, new StateToStrFormatter(StateToStrFormatter.CONTENT_STATE));
    Bindings.bind(lblCurState, vm);
    
    vm = pm.getModel(DebugManager.PROPERTY_RUNNING_STATE);
    vm = ConverterFactory.createStringConverter(vm, new StateToStrFormatter(StateToStrFormatter.CONTENT_LINE));
    Bindings.bind(lblCurLine, vm);
    
  }
  
  
  private static class FileToStrFormatter extends Format {
    private final boolean onlySimpleName;
    public FileToStrFormatter(boolean onlySimpleName) {
      this.onlySimpleName = onlySimpleName;
    }

    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
      File file = (File) obj;
      toAppendTo.append("Source: ");
      if(file != null) {
        if(onlySimpleName) {
          toAppendTo.append(file.getName());
        }else{
          toAppendTo.append(file.getAbsolutePath());
        }
      }
        
      return toAppendTo;
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
      return null;
    }
  }
  
  private static class StateToStrFormatter extends Format {
    private static final int CONTENT_LINE = 0;
    private static final int CONTENT_STATE = 1;
    private final int content;
    
    public StateToStrFormatter(int content) {
      this.content= content;
    }
    
    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
      IDebuggerProtocol.DebugState state= (IDebuggerProtocol.DebugState)obj;
      switch(content){
      case CONTENT_LINE:
        toAppendTo.append("Line:");
        if(state != null) {
          toAppendTo.append(state.curLineFromOne());
        }
        break;
        
      case CONTENT_STATE:
        toAppendTo.append("State:");
        if(state != null)
        toAppendTo.append(state.getState().name());
        break;
        
       default: 
          toAppendTo.append("-");
      }
      
      return toAppendTo;
    }
    
    @Override
    public Object parseObject(String source, ParsePosition pos) {
      return null;
    }
  }
}

