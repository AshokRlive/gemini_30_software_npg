/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.builder;

import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.iec61131.ide.ui.compiler.IOutputHandler;
import com.lucy.g3.iec61131.ide.ui.console.ConsoleFactory;


/**
 * An output handler that outputs content to console.
 */
public class ConsoleOutputHandler implements IOutputHandler {
  public IConsole console = ConsoleFactory.getConsole();
  
  public final static ConsoleOutputHandler INSTANCE = new ConsoleOutputHandler();
  

  @Override
  public void appendErr(String msg) {
    console.appendErr(msg);
  }

  @Override
  public void appendSuccess(String msg) {
    console.appendSuccess(msg);
    
  }

  @Override
  public void append(String output) {
    console.appendMsg(output);
  }

//  private void appendSeparator() {
//    console.appendMsg(System.getProperty("line.separator"));
//  }
}

