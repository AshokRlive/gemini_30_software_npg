/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.netbeans.spi.wizard.WizardPage;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.iec61131.ide.ui.source.CommonResources;
class StepCreateNew extends WizardPage {
  public final static String KEY_SCHEME_FILE_TO_CREATE = "schemeFileToCreate";
  
  public StepCreateNew() {
    super("Choose a location");
    setLongDescription("Choose a location to save the new " + AutoSchemeImportWizard.AUTO_SCHEME_STR);
    initComponents();
    tfPath.setName(KEY_SCHEME_FILE_TO_CREATE);
  }
  
  @Override
  protected String validateContents(Component component, Object event) {
    String path = tfPath.getText();
    if (path.isEmpty()) {
      return "Please specify the path for saving the new scheme file!";
    }

    File file = new File(path);
    
    if (file.isDirectory()) {
      return "The file path is invalid!";
    }
    
    if (file.exists()) {
      return "The file already exists!";
    }

    return null;
  }

  private void btnBrowseActionPerformed(ActionEvent e) {
    IImportCallback callback = (IImportCallback) getWizardData(IImportCallback.KEY_CALLBACK);
    File srcDir = callback.getSchemeFileDir();     

    File file = DialogUtils.showFileChooseDialog(getParent(),
        srcDir,
        "Create a new " + AutoSchemeImportWizard.AUTO_SCHEME_STR,
        "Select",
        "",
        CommonResources.IL_FILTER);
    if(file != null) {
      tfPath.setText(file.getAbsolutePath());
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    label1 = new JLabel();
    tfPath = new JTextField();
    btnBrowse = new JButton();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "default"));

    //---- label1 ----
    label1.setText("New Scheme Location:");
    add(label1, CC.xy(1, 1));
    add(tfPath, CC.xy(3, 1));

    //---- btnBrowse ----
    btnBrowse.setText("Browse...");
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnBrowseActionPerformed(e);
      }
    });
    add(btnBrowse, CC.xy(5, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JTextField tfPath;
  private JButton btnBrowse;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
