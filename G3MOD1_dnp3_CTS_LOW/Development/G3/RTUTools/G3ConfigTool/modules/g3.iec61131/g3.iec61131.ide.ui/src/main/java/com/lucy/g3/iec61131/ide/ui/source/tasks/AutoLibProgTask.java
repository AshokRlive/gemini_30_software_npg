/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.tasks;

import java.io.File;

import javax.swing.JOptionPane;

import org.jdesktop.application.Application;

import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.rtu.comms.service.filetransfer.FileEndInfo;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.filetransfer.FileWritingTask;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;


/**
 * The Class AutoLibProgTask.
 */
public class AutoLibProgTask extends FileWritingTask{
  private final IConsole console;
  
  public AutoLibProgTask(Application app, IConsole console, FileTransfer fileTransfer, File[] files) {
    super(app, fileTransfer, CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_AUTOLIB, files);
    this.console = console;
  }


  @Override
  protected FileEndInfo[] doInBackground() throws Exception {
    setMessage("Programming new IEC-61131 scheme...");
    return super.doInBackground();
  }


  @Override
  protected void failed(Throwable cause) {
    String msg = "Failed to programme RTU with the new IEC-61131 scheme!";
    console.appendErr(msg);
    
    MessageDialogs.error(parent, msg, cause);
  }


  @Override
  protected void succeeded(FileEndInfo[] endInfo) {
    super.succeeded(endInfo);
    String msg = "The new IEC-61131 scheme has been programmed to RTU successfully!";
    
    StringBuilder sb = new StringBuilder();
    sb.append(msg);
    sb.append(" [");
    for (int i = 0; i < endInfo.length; i++) {
      sb.append("\"");
      sb.append(endInfo[i].getFileName());
      sb.append("\" ");
    }
    sb.append(" ]");
    console.appendMsg(sb.toString());
    
    JOptionPane.showMessageDialog(parent, msg,  "Success", JOptionPane.INFORMATION_MESSAGE);
  }

  
}

