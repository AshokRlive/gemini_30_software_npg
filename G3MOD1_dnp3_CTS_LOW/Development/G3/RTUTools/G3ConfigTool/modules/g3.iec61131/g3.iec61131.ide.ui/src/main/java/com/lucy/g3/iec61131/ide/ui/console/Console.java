/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.console;

import javax.swing.JTextPane;

import org.apache.log4j.Logger;

import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.gui.common.widgets.console.SimpleConsole;


/**
 *
 */
public class Console implements IConsole {
  private Logger log = Logger.getLogger(Console.class);
  private final SimpleConsole console = new SimpleConsole(log);
  
  public Console(){
    this.console.setSuccessMark(null);
  }

  @Override
  public JTextPane getComponent() {
    return console.getComponent();
  }

  @Override
  public void appendMsg(String msg) {
    console.appendMsg(msg);
  }

  @Override
  public void appendSuccess(String msg) {
    console.appendSuccess(msg);
  }

  @Override
  public void appendBlankLine() {
    console.appendBlankLine();
  }

  @Override
  public void appendErr(String msg) {
    console.appendErr(msg);
  }

}

