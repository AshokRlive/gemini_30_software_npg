/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.prefs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.widgets.utils.KeyBindingUtil;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager.Key;
public class PreferencesEditor extends JDialog {
  
  private final PreferencesManager prefs;
  
  public PreferencesEditor(Window owner, PreferencesManager prefs) {
    super(owner);
    this.prefs = prefs;
    initComponents();
    loadPref();
  }

  private void loadPref() {
    tfCCompilerPath.setText(prefs.get(Key.KEY_CROSS_COMPILER_PATH));
    tfPrefix.setText(prefs.get(Key.KEY_CROSS_COMPILER_PREFIX));
    tfILCompilerPath.setText(prefs.get(Key.KEY_IL_COMPILER_PATH));
    tfWorkingDir.setText(prefs.get(Key.KEY_WORKING_DIR));
    try {
      ftfPollingRate.setValue(Integer.valueOf(prefs.get(Key.KEY_POLLING_RATE)));
    }catch(Throwable e){}
    
    cboxVisibleSpace.setSelected(Boolean.valueOf(prefs.get(Key.KEY_SHOW_TAB_SPACE)));
    cboxConvertTab.setSelected(Boolean.valueOf(prefs.get(Key.KEY_CONVERT_TABS_TO_SPACES)));
  }
  
  private void loadDefault() {
    tfCCompilerPath.setText(prefs.getDefault(Key.KEY_CROSS_COMPILER_PATH));
    tfPrefix.setText(prefs.getDefault(Key.KEY_CROSS_COMPILER_PREFIX));
    tfILCompilerPath.setText(prefs.getDefault(Key.KEY_IL_COMPILER_PATH));
    tfWorkingDir.setText(prefs.getDefault(Key.KEY_WORKING_DIR));
    try {
      ftfPollingRate.setValue(Integer.valueOf(prefs.getDefault(Key.KEY_POLLING_RATE)));
    }catch(Throwable e){}
  }
  
  private void savePrefs() {
    prefs.put(Key.KEY_CROSS_COMPILER_PATH, tfCCompilerPath.getText());
    prefs.put(Key.KEY_CROSS_COMPILER_PREFIX, tfPrefix.getText());
    prefs.put(Key.KEY_IL_COMPILER_PATH, tfILCompilerPath.getText());
    prefs.put(Key.KEY_WORKING_DIR,tfWorkingDir.getText());
    prefs.put(Key.KEY_POLLING_RATE,ftfPollingRate.getText());
    prefs.put(Key.KEY_SHOW_TAB_SPACE, String.valueOf(cboxVisibleSpace.isSelected()));
    prefs.put(Key.KEY_CONVERT_TABS_TO_SPACES, String.valueOf(cboxConvertTab.isSelected()));
  }
  
  private void btnOkActionPerformed(ActionEvent e) {
    savePrefs();
    dispose();
  }

  private void btnCancelActionPerformed(ActionEvent e) {
    dispose();
  }

//  private void btnSelectSourceActionPerformed(ActionEvent e) {
//    String path = chooseFilePath(this, "Choose IL source file",  JFileChooser.FILES_ONLY, 
//        new FileNameExtensionFilter("IL language(*.il)", "il"));
//    if(path != null)
//      tfILSource.setText(path);
//  }

  private void btnSelectWorkingDirActionPerformed(ActionEvent e) {
    String path = chooseFilePath(this, "Choose building directory",  JFileChooser.DIRECTORIES_ONLY );
    if(path != null)
    tfWorkingDir.setText(path);
  }

  private void btnSelectILPathActionPerformed(ActionEvent e) {
    String path = chooseFilePath(this, "Select IL compiler",  JFileChooser.FILES_ONLY,
        G3Files.FC_FILTER_IL_COMPILER);
    if(path != null)
    tfILCompilerPath.setText(path);
  }

  private void btnSelectCPathActionPerformed(ActionEvent e) {
    String path = chooseFilePath(this, "Select Cross Compiler Path",  JFileChooser.DIRECTORIES_ONLY);
    if(path != null)
      tfCCompilerPath.setText(path);
  }
  
  private String chooseFilePath(
        Component parent,
        String title,
        int mode,
        FileFilter... filter) {
      // Get previous directory
      File dir = new File(prefs.get(Key.KEY_PREFS_EDITOR_DIR));
      if (!dir.isDirectory() || !dir.canRead()) {
        dir = null;
      }

      JFileChooser fc = new JFileChooser(dir);
      fc.setDialogTitle(title);
      fc.setFileSelectionMode(mode);
      fc.setAcceptAllFileFilterUsed(true);

      if (filter != null) {
        for (int i = 0; i < filter.length; i++) {
          if (filter[i] != null) {
            fc.addChoosableFileFilter(filter[i]);
            fc.setFileFilter(filter[i]);
          }
        }
      }

      // Show dialog
      int option = fc.showOpenDialog(parent);
      
      // Save current directory
      prefs.put(Key.KEY_PREFS_EDITOR_DIR, fc.getCurrentDirectory().toString());
      
      // Dialog approved
      if (JFileChooser.APPROVE_OPTION == option) {
        return fc.getSelectedFile().getAbsolutePath();
      }else {
        return null;
      }
  }

  private void btnResetActionPerformed(ActionEvent e) {
    loadDefault();
  }

  private void btnLocateActionPerformed(ActionEvent e) {
    Object source = e.getSource();
    try {
    if(source == btnLocate1) {
        Desktop.getDesktop().open(new File(tfWorkingDir.getText()));
    }
    else if(source == btnLocate2) {
      Desktop.getDesktop().open(new File(tfILCompilerPath.getText()).getParentFile());
    }
    else if(source == btnLocate3) {
      Desktop.getDesktop().open(new File(tfCCompilerPath.getText()).getParentFile());
    }
    
    } catch (IOException e1) {
      throw new RuntimeException(e1);
    }

    
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    label2 = new JLabel();
    tfWorkingDir = new JTextField();
    btnSelectWorkingDir = new JButton();
    btnLocate1 = new JButton();
    label3 = new JLabel();
    tfILCompilerPath = new JTextField();
    btnSelectILPath = new JButton();
    btnLocate2 = new JButton();
    label4 = new JLabel();
    tfCCompilerPath = new JTextField();
    btnSelectCPath = new JButton();
    btnLocate3 = new JButton();
    label6 = new JLabel();
    tfPrefix = new JTextField();
    label1 = new JLabel();
    ftfPollingRate = new JFormattedTextField();
    label5 = new JLabel();
    cboxVisibleSpace = new JCheckBox();
    cboxConvertTab = new JCheckBox();
    buttonBar = new JPanel();
    btnReset = new JButton();
    btnOk = new JButton();
    btnCancel = new JButton();

    //======== this ========
    setTitle("IEC-61131 IDE Preferences");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.createEmptyBorder("7dlu, 7dlu, 7dlu, 7dlu"));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "right:default, $lcgap, [200dlu,default]:grow, 2*($lcgap, default)",
            "3*(default, $lgap), default, $ugap, 2*(default, $lgap), default"));

        //---- label2 ----
        label2.setText("Working Directory:");
        contentPanel.add(label2, CC.xy(1, 1));
        contentPanel.add(tfWorkingDir, CC.xy(3, 1));

        //---- btnSelectWorkingDir ----
        btnSelectWorkingDir.setText("Select...");
        btnSelectWorkingDir.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnSelectWorkingDirActionPerformed(e);
          }
        });
        contentPanel.add(btnSelectWorkingDir, CC.xy(5, 1));

        //---- btnLocate1 ----
        btnLocate1.setText("Locate");
        btnLocate1.setToolTipText("Open location");
        btnLocate1.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnLocateActionPerformed(e);
          }
        });
        contentPanel.add(btnLocate1, CC.xy(7, 1));

        //---- label3 ----
        label3.setText("IL Compiler Path:");
        contentPanel.add(label3, CC.xy(1, 3));
        contentPanel.add(tfILCompilerPath, CC.xy(3, 3));

        //---- btnSelectILPath ----
        btnSelectILPath.setText("Select...");
        btnSelectILPath.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnSelectILPathActionPerformed(e);
          }
        });
        contentPanel.add(btnSelectILPath, CC.xy(5, 3));

        //---- btnLocate2 ----
        btnLocate2.setText("Locate");
        btnLocate2.setToolTipText("Open the folder");
        btnLocate2.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnLocateActionPerformed(e);
          }
        });
        contentPanel.add(btnLocate2, CC.xy(7, 3));

        //---- label4 ----
        label4.setText("Cross Compiler Path:");
        contentPanel.add(label4, CC.xy(1, 5));
        contentPanel.add(tfCCompilerPath, CC.xy(3, 5));

        //---- btnSelectCPath ----
        btnSelectCPath.setText("Select...");
        btnSelectCPath.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnSelectCPathActionPerformed(e);
          }
        });
        contentPanel.add(btnSelectCPath, CC.xy(5, 5));

        //---- btnLocate3 ----
        btnLocate3.setText("Locate");
        btnLocate3.setToolTipText("Open location");
        btnLocate3.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnLocateActionPerformed(e);
          }
        });
        contentPanel.add(btnLocate3, CC.xy(7, 5));

        //---- label6 ----
        label6.setText("Cross Compiler Prefix:");
        contentPanel.add(label6, CC.xy(1, 7));
        contentPanel.add(tfPrefix, CC.xy(3, 7));

        //---- label1 ----
        label1.setText("Polling Rate:");
        contentPanel.add(label1, CC.xy(1, 9));

        //---- ftfPollingRate ----
        ftfPollingRate.setText("500");
        contentPanel.add(ftfPollingRate, CC.xy(3, 9));

        //---- label5 ----
        label5.setText("ms");
        contentPanel.add(label5, CC.xy(5, 9));

        //---- cboxVisibleSpace ----
        cboxVisibleSpace.setText("Visible Spaces/Tabs");
        contentPanel.add(cboxVisibleSpace, CC.xy(3, 11));

        //---- cboxConvertTab ----
        cboxConvertTab.setText("Convert Tabs to Spaces when saving");
        contentPanel.add(cboxConvertTab, CC.xy(3, 13));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.createEmptyBorder("5dlu, 0dlu, 0dlu, 0dlu"));
        buttonBar.setLayout(new FormLayout(
            "$lcgap, default, $glue, [50dlu,pref], $lcgap, $button, $rgap, $button",
            "pref"));

        //---- btnReset ----
        btnReset.setText("Reset");
        btnReset.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnResetActionPerformed(e);
          }
        });
        buttonBar.add(btnReset, CC.xy(4, 1));

        //---- btnOk ----
        btnOk.setText("OK");
        btnOk.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnOkActionPerformed(e);
          }
        });
        buttonBar.add(btnOk, CC.xy(6, 1));

        //---- btnCancel ----
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnCancelActionPerformed(e);
          }
        });
        buttonBar.add(btnCancel, CC.xy(8, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  
    // Binding key strokes
    KeyBindingUtil.registerEscapeEnterKey(this.getRootPane(), 
        btnOk.getActionListeners()[0],
        btnCancel.getActionListeners()[0]);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JLabel label2;
  private JTextField tfWorkingDir;
  private JButton btnSelectWorkingDir;
  private JButton btnLocate1;
  private JLabel label3;
  private JTextField tfILCompilerPath;
  private JButton btnSelectILPath;
  private JButton btnLocate2;
  private JLabel label4;
  private JTextField tfCCompilerPath;
  private JButton btnSelectCPath;
  private JButton btnLocate3;
  private JLabel label6;
  private JTextField tfPrefix;
  private JLabel label1;
  private JFormattedTextField ftfPollingRate;
  private JLabel label5;
  private JCheckBox cboxVisibleSpace;
  private JCheckBox cboxConvertTab;
  private JPanel buttonBar;
  private JButton btnReset;
  private JButton btnOk;
  private JButton btnCancel;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
