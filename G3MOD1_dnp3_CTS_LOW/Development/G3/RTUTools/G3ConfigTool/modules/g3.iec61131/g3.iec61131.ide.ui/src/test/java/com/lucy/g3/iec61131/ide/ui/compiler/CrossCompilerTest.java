
package com.lucy.g3.iec61131.ide.ui.compiler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.SystemUtils;
import org.junit.Assume;
import org.junit.Test;

import com.lucy.g3.iec61131.ide.ui.compiler.CrossCompiler;
import com.lucy.g3.iec61131.ide.ui.compiler.IOutputHandler;
import com.lucy.g3.iec61131.ide.ui.util.FileUtil;


public class CrossCompilerTest implements IOutputHandler{

  
  @Test
  public void testCompileSharedLib() throws IOException, InterruptedException {
    Assume.assumeTrue(SystemUtils.IS_OS_LINUX);
    
    String compilerPath = "/usr/local/arm/cross-gcc-4.5.3-armv5te-soft/x86_64-pc-linux-gnu/bin/";
    String prefix = "arm-926ejs-linux-gnueabi-";
    String includePath = "src/main/resources/automation/common/";
    String commonSourceFile = "src/main/resources/automation/common/AutomationSchemeLib.c";
    
    CrossCompiler compiler = new CrossCompiler(compilerPath,prefix, includePath, commonSourceFile, this);
    compiler.compile("src/test/resources/c_code/CustomScheme.c", "target/CrossCompilerTest/");
    
    String output = compiler.getOutputFile();
    assertNotNull(output);
    assertTrue(output.endsWith(".so"));
    assertTrue(output + " not started with lib", FilenameUtils.getName(output).startsWith("lib"));
    assertTrue(new File(output).exists());
    assertTrue(new File(output).isFile());
  }

  @Test
  public void testGetFileName() {
    assertEquals("AutoChangeover", FileUtil.getFileNameWithoutExtension("target/TestCompiler/AutoChangeover.c"));
    assertEquals("libAutoChangeover.so", FileUtil.convertIEC61131SourcePathToLibName("target/TestCompiler/AutoChangeover.c"));
    assertEquals("libAutoChangeover.so", FileUtil.convertIEC61131SourcePathToLibName("target/TestCompiler/AutoChangeover.il"));
    assertEquals("AutoChangeover.il", FileUtil.convertIEC61131LibPathToSourceName("target/TestCompiler/AutoChangeover.so",".il"));
  }
  
  @Override
  public void appendErr(String msg) {
      System.out.println(msg);
  }

  @Override
  public void appendSuccess(String msg) {
    System.out.println(msg);    
  }

  @Override
  public void append(String msg) {
    System.out.println(msg);    
  }
}

