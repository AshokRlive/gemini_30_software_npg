
package com.lucy.g3.iec61131.ide.ui.debugger;

import java.io.IOException;
import java.util.TimerTask;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugState;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVar;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVarRef;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;

class DebugPollingTask extends TimerTask {

  private Logger log = Logger.getLogger(DebugPollingTask.class);

  private final static int MAX_ERR_COUNT = 3;
  private int errCount = 0;
  private Throwable err = null;
  private IEC61131Context context;


  public DebugPollingTask(IEC61131Context context) {
    super();
    this.context = context;
  }

  @Override
  public void run() {
    /* Polling state */
    try {
      DebugState state = context.comms.getCurrentState();
      publish(state);
    } catch (IOException e) {
      log.error("Failed to read current state from RTU cause: " + e.getMessage());
      errCount++;
      err = e;
    }

    /* Polling variables */
    try {
      DebugVarRef[] vars = context.variable.getAllVarRefs();
      DebugVar[] varData = context.comms.getVariable(vars);
      publish(varData);
    } catch (IOException e) {
      log.error("Failed to read variables from RTU cause: " + e.getMessage());
      errCount++;
      err = e;
    }

    /* Handling error */
    if (errCount > MAX_ERR_COUNT) {
      context.debugger.stopDebug();
      showErrorDialog("Disconnected from the debug server cause communication failed! "
          + "\nPlease connect again and retry!.");
    }
  }

  private void publish(final DebugVar[] varData) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        context.variable.updateData(varData);
      }
     });
  }

  private void showErrorDialog(final String errorMsg) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        MessageDialogs.error(
            context.getParent(),
            errorMsg,
            err
            );
      }
    });
  }

  private void publish(final DebugState state) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        context.debugger.setRunningState(state);
      }
    });
  }
}
