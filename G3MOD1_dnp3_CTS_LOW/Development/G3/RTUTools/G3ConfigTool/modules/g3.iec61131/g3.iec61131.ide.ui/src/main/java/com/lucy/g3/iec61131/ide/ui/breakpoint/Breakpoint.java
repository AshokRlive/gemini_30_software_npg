/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.breakpoint;

/**
 *
 */
public class Breakpoint {

  private final int lineNum;
  private final String content;
  private Object tag;
  private boolean removed;

  /**
   * @param line Line number start from one.
   * @param content
   */
  public Breakpoint(int line, String content) {
    this.lineNum = line;
    this.content = content;
  }

  /**
   * @return line number start from one.
   */
  public int getLineNum() {
    return lineNum;
  }

  public String getLineContent() {
    return content;
  }

  public boolean isRemoved() {
    return removed;
  }

  void setRemoved(boolean removed) {
    this.removed = removed;
  }

  public Object getTag() {
    return tag;
  }

  public void setTag(Object tag) {
    this.tag = tag;
  }

}
