/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.editor;

import javax.swing.Action;

import org.junit.Assert;

import org.junit.Test;

import com.lucy.g3.iec61131.ide.ui.source.editor.SourceManager;


/**
 *
 */
public class SourceManagerTest {
  private SourceManager fixture;

  @Test
  public void testActionState() {
    fixture = new SourceManager(null);
    Action action = fixture.getAction(SourceManager.ACTION_KEY_SAVE);
    Assert.assertNotNull(action);
    
    fixture.setSourceChanged(true);
    Assert.assertEquals(fixture.isSourceChanged(), action.isEnabled());
    Assert.assertTrue("action enable:"+action.isEnabled(), action.isEnabled());
    
    fixture.setSourceChanged(false);
    Assert.assertEquals(fixture.isSourceChanged(), action.isEnabled());
    Assert.assertFalse(action.isEnabled());
  }

}

