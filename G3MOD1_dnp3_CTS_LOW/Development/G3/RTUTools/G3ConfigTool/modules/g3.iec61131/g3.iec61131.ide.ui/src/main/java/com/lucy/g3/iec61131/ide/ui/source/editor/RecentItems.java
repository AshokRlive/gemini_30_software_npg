
package com.lucy.g3.iec61131.ide.ui.source.editor;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

/**
 * A simple data structure to store recent items (e.g. recent file in a menu or
 * recent search text in a search dialog).
 */
public class RecentItems
{

  public interface RecentItemsObserver
  {

    void onRecentItemChange(RecentItems src);
  }


  public final static String RECENT_ITEM_STRING = "recent.item."; //$NON-NLS-1$

  private int maxItems;
  private Preferences prefNode;

  private List<String> items = new ArrayList<String>();
  private List<RecentItemsObserver> observers = new ArrayList<RecentItemsObserver>();


  public RecentItems(int maxItems, Preferences prefNode)
  {
    this.maxItems = maxItems;
    this.prefNode = prefNode;

    loadFromPreferences();
  }

  public void push(String item)
  {
    items.remove(item);
    items.add(0, item);

    if (items.size() > maxItems)
    {
      items.remove(items.size() - 1);
    }

    update();
  }

  public void remove(Object item)
  {
    items.remove(item);
    update();
  }

  public String get(int index)
  {
    return items.get(index);
  }

  public List<String> getItems()
  {
    return items;
  }

  public int size()
  {
    return items.size();
  }

  public void addObserver(RecentItemsObserver observer)
  {
    observers.add(observer);
  }

  public void removeObserver(RecentItemsObserver observer)
  {
    observers.remove(observer);
  }

  private void update()
  {
    for (RecentItemsObserver observer : observers)
    {
      observer.onRecentItemChange(this);
    }

    storeToPreferences();
  }

  private void loadFromPreferences()
  {
    // load recent files from properties
    for (int i = 0; i < maxItems; i++)
    {
      String val = prefNode.get(RECENT_ITEM_STRING + i, ""); //$NON-NLS-1$

      if (!val.equals("")) //$NON-NLS-1$
      {
        items.add(val);
      }
      else
      {
        break;
      }
    }
  }

  private void storeToPreferences()
  {
    for (int i = 0; i < maxItems; i++)
    {
      if (i < items.size())
      {
        prefNode.put(RECENT_ITEM_STRING + i, items.get(i));
      }
      else
      {
        prefNode.remove(RECENT_ITEM_STRING + i);
      }
    }
  }
}
