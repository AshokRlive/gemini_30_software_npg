/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.editor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import org.apache.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.GutterIconInfo;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugState;
import com.lucy.g3.iec61131.ide.ui.breakpoint.Breakpoint;
import com.lucy.g3.iec61131.ide.ui.breakpoint.IBreakpointObserver;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager.Status;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager.Key;
public class SourceEditorPane extends JPanel implements ISourceEditor{
  private Logger log = Logger.getLogger(SourceEditorPane.class);
  
  private final static String FILE_CHANGE_INDICATOR = "* ";
  private Icon breakPointIcon;
  
  private final Color RUNNING_HIGHTLIGHT = Color.green;
  
  private Object currentLineTag; // A tag for current highlighted line.
  
  private boolean autoScrollEnabled = true;
  
  private final SourceManager manager;
  
  private boolean fileExist;
  private boolean disconnected;
  
  private boolean convertTabsToSpaces;

  @SuppressWarnings("unused")
  private SourceEditorPane() {
    this(null);
  }
  public SourceEditorPane(SourceManager manager) {
    this.manager = manager;
    initComponents();
    initComponentProperties();
    
    if (manager != null) {
      this.manager.setEditor(this);
      
      this.disconnected = !manager.getContext().debugger.isConnected();
      this.fileExist = manager.isSourceFileExist();
      
      initEventHanding();
      initPrefsHanding();
      
    } else {
      log.warn("source manager is null");
    }
    
    updateEditableState();
  }
  
  private void initPrefsHanding() {
    PreferencesManager prefs = manager.getContext().prefs;
    prefs.addPreferenceChangeListener(new PreferenceChangeListener() {
      @Override
      public void preferenceChange(PreferenceChangeEvent evt) {
        if(evt.getKey().equals(Key.KEY_SHOW_TAB_SPACE.name())) {
          setShowTabSpace(Boolean.parseBoolean(evt.getNewValue()));
        } else if(evt.getKey().equals(Key.KEY_CONVERT_TABS_TO_SPACES.name())) {
          setConvertTabsToSpaces(Boolean.parseBoolean(evt.getNewValue()));
        }
        
      }
    });
    
    setShowTabSpace(Boolean.parseBoolean(prefs.get(Key.KEY_SHOW_TAB_SPACE)));
    setConvertTabsToSpaces(Boolean.parseBoolean(prefs.get(Key.KEY_CONVERT_TABS_TO_SPACES)));
  }
  
  private void setShowTabSpace(boolean visible) {
    xtextEditor.setWhitespaceVisible(visible);
  }
  
  private void setConvertTabsToSpaces(boolean convertTabsToSpaces) {
    this.convertTabsToSpaces = convertTabsToSpaces;
  }

  
  public boolean isAutoScrollEnabled() {
    return autoScrollEnabled;
  }
  
  public void setAutoScrollEnabled(boolean autoScrollEnabled) {
    this.autoScrollEnabled = autoScrollEnabled;
  }
  
  @Override
  public String getCaretLineContent() {
    int cursorline = xtextEditor.getCaretLineNumber();
    String content = null;
    try {
      int offsetStart = xtextEditor.getLineStartOffset(cursorline);
      int offsetEnd = xtextEditor.getLineEndOffset(cursorline);
      content = xtextEditor.getText(offsetStart, offsetEnd-offsetStart);
      return content;
    } catch (BadLocationException e1) {
      log.error(e1);
      return null;
    }
  }
  @Override
  public void write(Writer out) throws IOException {
    if(convertTabsToSpaces){
      xtextEditor.convertTabsToSpaces();
    }
    
    xtextEditor.write(out);
  }
  /**
   * 
   * @param newLine line number from zero.
   */
  private void setCurrentLine(int newLine) {
    // Remove current highlight
    if(currentLineTag != null)
      xtextEditor.removeLineHighlight(currentLineTag);
    
    if(newLine >= 0) {
      try {
        currentLineTag = xtextEditor.addLineHighlight(newLine, RUNNING_HIGHTLIGHT);
        
        if(autoScrollEnabled)
          scrollToLine(newLine,false);

      } catch (BadLocationException e) {
        log.error("Cannot hightlight line cause: " + e.getMessage());
      }
    }
  }

  /**
   * @param line line number from zero.
   * @param setCaretToLine
   */
  public void scrollToLine(int line, boolean setCaretToLine) {
    int offset;
    try {
      offset = xtextEditor.getLineStartOffset(line);
      if (offset < 0)
        return;

      if (setCaretToLine)
        xtextEditor.setCaretPosition(offset);

      xtextEditor.scrollRectToVisible(xtextEditor.modelToView(offset));
      
    } catch (Throwable e1) {
      log.error("Failed to scroll to line:"+line+", setCaretToLine:"+setCaretToLine,e1);
    }
  }
  
  private void initComponentProperties() {
    xtextEditor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
    
    xtextEditorPane.setLineNumbersEnabled(true);
    xtextEditorPane.setIconRowHeaderEnabled(true);
    xtextEditorPane.setFoldIndicatorEnabled(false);
    
    xtextEditorPane.getGutter().setBookmarkingEnabled(true);
    xtextEditorPane.getGutter().setLineNumberingStartIndex(1);
  }
  
  private void initEventHanding() {
    // Register save action
    Action save = manager.getAction(SourceManager.ACTION_KEY_SAVE);
    xtextEditor.getInputMap().put( KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK), save);
    
    // Observer breakpoint list change
    manager.getBreakpointManager().addObserver(new BreakPointListObserver());
    
    // Observe user typing.
    xtextEditor.getDocument().addDocumentListener(editorListener);
    
    // Observe current line change
    manager.getContext().debugger.addPropertyChangeListener(DebugManager.PROPERTY_RUNNING_STATE, new RunningStateChangedPCL());
    
    // Observe source content change
    manager.addPropertyChangeListener(SourceManager.PROPERTY_SOURCE_CHANGED, new SourceChangedPCL());
    
    manager.addPropertyChangeListener(SourceManager.PROPERTY_SOURCE_FILE, new SourceFilePCL());
    setTabTitle(manager.getSourceFile());
    
    manager.getContext().debugger.addPropertyChangeListener(DebugManager.PROPERTY_CONNECT_STATUS, new ConnectStatePCL());
  }
  
  private void createUIComponents() {
    xtextEditor = new RSyntaxTextArea(20, 60);
    xtextEditorPane = new RTextScrollPane(xtextEditor);
    
    menuItemToggleBP = new JMenuItem();
    if(manager != null) {
      menuItemToggleBP.setAction(manager.getAction(SourceManager.ACTION_KEY_TOGGLE_BP));
    }
  }

  
  @Override
  public boolean loadFile(File file) {
    try {
      // Remove listener to avoid changing "changed" state in file loading process.
      editorListener.deactivate();
      loadFileToEditor(xtextEditor, file);
      manager.setSourceChanged(false);
      return true;
    } catch (IOException e) {
      manager.getErrHandler().handleError("Failed to read source file", e);
      return false;
    } finally {
      editorListener.activate();
    }
  }
  
  private static void loadFileToEditor(final JTextArea editor, final File file) throws IOException {
    editor.setText("");

    if (file != null) {
      BufferedReader in = null;
      try {
        in = new BufferedReader(new FileReader(file));
        editor.read(in, "SourceFile");
        editor.setCaretPosition(0);
      } catch (IOException e) {
        throw e;
      } finally {
        if(in != null) {
          try {
            in.close();
          } catch (IOException e) {}
        }
      }
    }
  }
  
  private void setTabTitle(File file) {
    tabbedPane.setTitleAt(0, file == null ? "No Source" : file.getName());
    tabbedPane.setToolTipTextAt(0, file == null ? "No Source" : file.getAbsolutePath());
  }
  
  private void updateEditableState() {
    xtextEditor.setEditable(fileExist && disconnected);
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane = new JTabbedPane();
    xtextEditor = new RSyntaxTextArea();
    popupMenu = new JPopupMenu();

    //======== this ========
    setLayout(new BorderLayout());

    //======== tabbedPane ========
    {

      //======== xtextEditorPane ========
      {

        //---- xtextEditor ----
        xtextEditor.setPopupMenu(popupMenu);
        xtextEditorPane.setViewportView(xtextEditor);
      }
      tabbedPane.addTab("File Name", xtextEditorPane);
    }
    add(tabbedPane, BorderLayout.CENTER);

    //======== popupMenu ========
    {

      //---- menuItemToggleBP ----
      menuItemToggleBP.setText("Toggle Breakpoint");
      popupMenu.add(menuItemToggleBP);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JTabbedPane tabbedPane;
  private RTextScrollPane xtextEditorPane;
  private RSyntaxTextArea xtextEditor;
  private JPopupMenu popupMenu;
  private JMenuItem menuItemToggleBP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  private final EditorDocListener editorListener = new EditorDocListener();
  
  /**
   * Observe the editing document and update "sourceChanged" state. 
   */
  private final class EditorDocListener implements DocumentListener {
    private boolean deactivated;
    
    public void deactivate() {
      deactivated = true;
    }
    
    public void activate() {
      deactivated = false;
    }
    
    @Override
    public void removeUpdate(DocumentEvent e) {
      setSourceChanged();
    }
    @Override
    public void insertUpdate(DocumentEvent e) {
      setSourceChanged();
    }
    @Override
    public void changedUpdate(DocumentEvent e) {
      setSourceChanged();
    }
    private void setSourceChanged() {
      if(deactivated == false) {
        manager.setSourceChanged(true);
      }
    }
  };
  
  private class BreakPointListObserver implements IBreakpointObserver {

    @Override
    public void breakpointDeleted(Breakpoint bp) {
      xtextEditorPane.getGutter().removeTrackingIcon((GutterIconInfo) bp.getTag());
    }

    @Override
    public void breakpointAdded(Breakpoint bp) {
      if (breakPointIcon == null)
        breakPointIcon = menuItemToggleBP.getIcon();
      try {
        bp.setTag(xtextEditorPane.getGutter().addLineTrackingIcon(bp.getLineNum() - 1, breakPointIcon));
      } catch (BadLocationException e) {
        log.error(e);
      }
    }
  }

  /**
   * Observe running state and update current running line number.
   */
  private class RunningStateChangedPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      DebugState state = (DebugState) evt.getNewValue();
      setCurrentLine(state == null ? -1 : state.curLineFromOne() - 1);
    }
  }

  private class SourceFilePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      File file = (File) evt.getNewValue();
      
      setTabTitle(file);
      
      fileExist = file != null;
      updateEditableState();
    }
  }
  
  /** Observe source change and update tab indicator.*/
  private class SourceChangedPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      String title = tabbedPane.getTitleAt(0);
      
      if((boolean)evt.getNewValue()) {
        // Add indicator
        if (!title.startsWith(FILE_CHANGE_INDICATOR)) 
            tabbedPane.setTitleAt(0, FILE_CHANGE_INDICATOR + title);
      } else{
        // Remove indicator
        if (title.startsWith(FILE_CHANGE_INDICATOR))
          tabbedPane.setTitleAt(0, title.substring(FILE_CHANGE_INDICATOR.length(), title.length()));
      }
    }
  }
  
  private class ConnectStatePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      disconnected = evt.getNewValue() == Status.DISCONNECTED;
      updateEditableState();
    }
    
  }
  
  @Override
  public int getCaretLineNumber() {
    return xtextEditor.getCaretLineNumber();
  }
  
}
