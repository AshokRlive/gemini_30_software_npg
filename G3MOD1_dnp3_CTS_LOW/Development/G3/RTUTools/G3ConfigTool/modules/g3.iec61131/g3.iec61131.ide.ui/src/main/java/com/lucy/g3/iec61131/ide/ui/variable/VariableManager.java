/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.variable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVar;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVarRef;

/**
 *
 */
public class VariableManager {
  private Logger log = Logger.getLogger(VariableManager.class);
  
  private final MultiListSelectionAdapter<Variable> varSelectModel;
  private final ArrayListModel<Variable> varListModel;
  private final MultiSelectionInList<Variable> varSelectList;
  private final VarTableModel varDataModel;
  
  
  public VariableManager() {
    this.varListModel = new ArrayListModel<>();
    this.varSelectList = new MultiSelectionInList<>(varListModel);
    this.varSelectModel = new MultiListSelectionAdapter<>(varSelectList);
    this.varDataModel = new VarTableModel(varListModel);
  }
  
  
  public synchronized  void addVariable(Variable...newVar) {
    varListModel.addAll(Arrays.asList(newVar));
  }
  

  public synchronized  void removeVariable(List<Variable> varList) {
    varListModel.removeAll(varList);
  }

  public ListModel<Variable> getVarListModel() {
    return varListModel;
  }
  
  public List<Variable> getSelectedVars() {
    return new ArrayList<>(varSelectList.getSelection());
  }

  public ListSelectionModel getVarSelectModel() {
    return varSelectModel;
  }
  public TableModel getVarDataModel() {
    return varDataModel;
  }
  
  public Variable[] getAllVars() {
    return varListModel.toArray(new Variable[varListModel.size()]);
  }
  
  public synchronized DebugVarRef[] getAllVarRefs() {
    DebugVarRef[] refs = new DebugVarRef[varListModel.size()];
    for (int i = 0; i < refs.length; i++) {
      refs[i] = varListModel.get(i).getRef();
    }
    
    return refs;
  }
  
  private static class VarTableModel extends AbstractTableAdapter<Variable> {

    private final static int COL_ID = 0;
    private final static int COL_TYPE = 1;
    private final static int COL_NAME = 2;
    private final static int COL_VALUE = 3;
    
    private final static String COL_NAMES[] = {"Index","Type", "Name","Value"};
    
    public VarTableModel(ListModel<Variable> bplistModel) {
      super(bplistModel, COL_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Variable bp = getRow(rowIndex);
      switch(columnIndex) {
        case COL_ID:
          return bp.getId();
        case COL_NAME:
          return bp.getName();
        case COL_VALUE:
          return bp.getValue();
        case COL_TYPE:
          return bp.getType();
        default:
            return null;
      }
    }
  }

  public boolean hasSelections() {
    return varSelectList.hasSelection();
  }


  public void updateData(DebugVar[] varData) {
    for (int i = 0; i < varData.length; i++) {
      for (Variable var : varListModel) {
        if(var.copyFrom(varData[i]))
          continue;
      }
    }
    
    
    int rows = varDataModel.getRowCount();
    if(rows > 0)
      this.varDataModel.fireTableRowsUpdated(0, rows);
  }


  public void clear() {
    varListModel.clear();
  }
}
