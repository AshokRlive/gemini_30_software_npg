/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingWorker;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;
import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
class StepReadSchemeFromRTU extends WizardPage {
  public final static String KEY_SCHEME_FILE_TO_READ = "schemeFileToRead";

  private final SelectionInList<String> schemeSelectionList = new SelectionInList<>();

  private Logger log = Logger.getLogger(StepReadSchemeFromRTU.class);
  
  private String runningScheme = null;
  private ReadSchemeNamesTask task;
  
  public StepReadSchemeFromRTU() {
    super("Read scheme");
    setLongDescription("Read a selected scheme from RTU");
    initComponents();
  }

  @Override
  protected void renderingPage() {
    labelError.setText(null); // clear error
    
    schemeSelectionList.clearSelection(); // clear scheme selection
    schemeSelectionList.getList().clear(); // clear scheme names
    
    if(tfPath.getText().isEmpty()) {
      IImportCallback callback = (IImportCallback)getWizardData(IImportCallback.KEY_CALLBACK);
      tfPath.setText(callback.getSchemeFileDir().getAbsolutePath());
    }
    
    task = new ReadSchemeNamesTask();
    task.execute();
  }
  

  @Override
  public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
    return new WizardPanelNavResult(){

      @Override
      public void start(Map settings, ResultProgressHandle progress) {
        if(schemeSelectionList.isSelectionEmpty()) {
          progress.failed("No scheme selected!", true);
          return;
        }
        
        progress.setBusy("Reading scheme source file from RTU...");
        try {
          IImportCallback callback = (IImportCallback) settings.get(IImportCallback.KEY_CALLBACK);
          String schemeName = schemeSelectionList.getSelection();
          File schemeFile = callback.readSchemeFromRTU(schemeName, new File(tfPath.getText()));
          if(schemeFile != null) {
            settings.put(KEY_SCHEME_FILE_TO_READ, schemeFile.getAbsolutePath());
            progress.finished(settings);
          } else{
            progress.failed("Automation scheme reading has been cancelled!", true);
          }
        }catch(Exception e) {
          log.error("Fail to read automation scheme from RTU", e);
          progress.failed("Unexpected error occurred:" + e.getMessage(), true);
        }
      }
      
    };
  }

  @Override
  public WizardPanelNavResult allowBack(String stepName, Map settings, Wizard wizard) {
    return new WizardPanelNavResult() {
      
      @Override
      public void start(Map settings, ResultProgressHandle progress) {
        if(task != null && !task.isCancelled()){
          getRootPane().setCursor(Cursor.getDefaultCursor()); // restore cursor
          task.cancel(true);
          progress.finished(null);
        }
      }
    };
  }

  @Override
  protected String validateContents(Component component, Object event) {
    if(schemeSelectionList.isSelectionEmpty()) {
      return "No scheme selected!";
    }
    
    return null;
  }

  private void createUIComponents() {
    tableSchemeList = new JTable(new SchemeTableModel(schemeSelectionList));
    Bindings.bind(tableSchemeList, schemeSelectionList);
    tableSchemeList.getColumnModel().getColumn(COL_SCHEME_NOTE).setMaxWidth(80);
  }

  private void btnBrowseActionPerformed(ActionEvent e) {
    IImportCallback callback = (IImportCallback) getWizardData(IImportCallback.KEY_CALLBACK);
    File srcDir = callback.getSchemeFileDir();     

    JFileChooser fc = new JFileChooser(srcDir);
    fc.setDialogTitle("Choose a directory");
    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    fc.showDialog(getParent(), "Select");
    
    File file = fc.getSelectedFile();
    if(file != null) {
      tfPath.setText(file.getAbsolutePath());
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();
    tfPath = new JTextField();
    btnBrowse = new JButton();
    scrollPane1 = new JScrollPane();
    labelError = new JLabel();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "2*(default, $ugap), fill:default"));

    //---- label1 ----
    label1.setText("Save to Location:");
    add(label1, CC.xy(1, 1));
    add(tfPath, CC.xy(3, 1));

    //---- btnBrowse ----
    btnBrowse.setText("Browse");
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnBrowseActionPerformed(e);
      }
    });
    add(btnBrowse, CC.xy(5, 1));

    //======== scrollPane1 ========
    {
      scrollPane1.setBorder(new CompoundBorder(
          new TitledBorder("Existing Automation Schemes in RTU"),
          Borders.DLU2_BORDER));

      //---- tableSchemeList ----
      tableSchemeList.setFillsViewportHeight(true);
      scrollPane1.setViewportView(tableSchemeList);
    }
    add(scrollPane1, CC.xywh(1, 3, 5, 1));

    //---- labelError ----
    labelError.setForeground(Color.red);
    add(labelError, CC.xywh(1, 5, 3, 1));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel label1;
  private JTextField tfPath;
  private JButton btnBrowse;
  private JScrollPane scrollPane1;
  private JTable tableSchemeList;
  private JLabel labelError;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  private final static String[] COL_NAMES = {
    "Scheme Name", "Note"
  };
  private final static int COL_SCHEME_NAME = 0;
  private final static int COL_SCHEME_NOTE = 1;
  
  private class SchemeTableModel extends AbstractTableAdapter<String> {
    
    public SchemeTableModel(ListModel<String> listModel) {
      super(listModel,COL_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      String row = getRow(rowIndex);
      
      switch (columnIndex) {
      case COL_SCHEME_NAME:
        return row;
      case COL_SCHEME_NOTE:
        return row.equals(runningScheme)? "Running" :"Not Running";
      default:
        break;
      }
      return null;
    }
  }
  
  private class ReadSchemeNamesTask extends SwingWorker<String[], Void> {
    @Override
    public String[] doInBackground() throws Exception{
      JRootPane root = getRootPane();
      if(root != null)
        root.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      
      IImportCallback callback = (IImportCallback) getWizardData(IImportCallback.KEY_CALLBACK);
      String[] fileList;
      fileList = callback.readSchemeNamesFromRTU();
      runningScheme = callback.readRunningSchemeNameFromRTU();
      return fileList;
    }

    @Override
    public void done() {
      JRootPane root = getRootPane();
      if(root != null)
        root.setCursor(Cursor.getDefaultCursor()); // restore cursor
      
      if(!isCancelled()) {
        try {
          schemeSelectionList.getList().addAll(Arrays.asList(get()));
        } catch (InterruptedException | ExecutionException e) {
          labelError.setText(e.getCause().getMessage());
        }
      }
    }
    
    
  };
  
}
