/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.exception;

import org.apache.log4j.Logger;


/**
 *
 */
public class DefaultErrorHandler implements IErrorHandler {
  private final Logger log;
  
  public DefaultErrorHandler(Logger log) {
    super();
    this.log = log;
  }

  @Override
  public void handleError(String error, Exception cause) {
    log.error(error,cause);
  }

  @Override
  public void handleError(String error, Exception cause, boolean showDialog) {
    log.error(error,cause);
  }

}

