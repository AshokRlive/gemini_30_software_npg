
package com.lucy.g3.iec61131.ide.ui.source.tasks;

import java.awt.Window;
import java.io.File;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.Task;

import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.iec61131.sdp.patcher.IEC61131Patcher;

public class SDPPatchingTask extends Task<Void, Void> {

  private static File sdpDir = new File("./");

  private final File[] schemeFiles;
  private final String schemeName;

  private File sdpFile;

  private Window parent = null;

  private IConsole console;


  public SDPPatchingTask(Application application, IConsole console, String schemeName, File[] schemeFiles) {
    super(application);
    this.schemeName = schemeName;
    this.schemeFiles = schemeFiles;
    this.console = console;
  }

  
  public void setParent(Window parent) {
    this.parent = parent;
    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
  }

  @Override
  protected Void doInBackground() throws Exception {
    setMessage("Choosing a SDP file to apply patch...");
    Thread.sleep(500); // Time to show blocker
    
    SwingUtilities.invokeAndWait(new Runnable() {
      @Override
      public void run() {
        // Choose SDP
        sdpFile = DialogUtils.showFileChooseDialog(parent,
            sdpDir,
            "Select a SDP file",
            "Select",
            "",
            G3Files.FC_FILTER_G3SDP);
      }
    });

    // Patching
    if (sdpFile != null) {
      setMessage("Applying patch...");
      sdpDir = sdpFile.getParentFile();
      IEC61131Patcher.patchAutoLibs(sdpFile, schemeFiles);
    } else {
      cancel(true);
    }
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    setMessage("Failed!");
    console.appendErr(cause.getMessage());
    MessageDialogs.error(parent, String.format("Failed to patch SDP \"%s\"", sdpFile.getName()), cause);
  }

  @Override
  protected void succeeded(Void result) {
    setMessage("Succeeded!");
    String msg = String.format("The SDP file \"%s\" has been patched with the IEC-61131 scheme \"%s\"",
        sdpFile.getName(), schemeName);
    console.appendSuccess(msg);
    JOptionPane.showMessageDialog(parent, msg, "Success", JOptionPane.INFORMATION_MESSAGE);
  }

}
