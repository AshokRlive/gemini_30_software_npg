/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.io.File;

/**
 *
 */
public interface IImportCallback {

  String KEY_CALLBACK = "callback";


  String[] readSchemeNamesFromRTU() throws Exception;

  String readRunningSchemeNameFromRTU() throws Exception;
  
  File readSchemeFromRTU(String schemeName, File destDir) throws Exception;


  File[] getRecentSchemeFiles();

  File getSchemeFileDir();

  void openFile(File schemefile);
}
