/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Enumeration;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.netbeans.spi.wizard.ResultProgressHandle;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.iec61131.ide.ui.source.CommonResources;

class StepImportLocalScheme extends WizardPage {
  public final static String KEY_SCHEME_FILE_TO_IMPORT = "schemeFileToImport";
  
  private final static String CLIENT_PROPERTY_FILE = "clientPropertyFile";
  
  private final ButtonGroup group = new ButtonGroup();
  private boolean radioBtnInitialised;


  public StepImportLocalScheme() {
    super("Choose a location");
    setLongDescription("Choose a location to import an existing automation scheme");
    initComponents();
    group.add(radioBtnImport);
    radioBtnImport.setSelected(true);
  }

  @Override
  protected String validateContents(Component component, Object event) {
    if (group.getSelection() == null) {
      return "Please select a file to import!";
    }

    if (group.getSelection() == radioBtnImport.getModel()) {
      File file = new File(tfPath.getText());
      if (!file.exists() || !file.isFile()) {
        return "Please select a file to import!";
      }
    }

    return null;
  }

  @Override
  public WizardPanelNavResult allowFinish(String stepName, Map settings, Wizard wizard) {
    return new WizardPanelNavResult() {
      
      @Override
      public void start(Map settings, ResultProgressHandle progress) {
        if(radioBtnImport.isSelected()) {
          settings.put(KEY_SCHEME_FILE_TO_IMPORT, tfPath.getText());
          progress.finished(settings);
          
        }else {
          Enumeration<AbstractButton> radioBtns = group.getElements();
          ButtonModel sel = group.getSelection();
          JRadioButton radio;
          while(radioBtns.hasMoreElements()) {
            radio = (JRadioButton) radioBtns.nextElement();
            if(radio.getModel() == sel) {
              File file = (File) radio.getClientProperty(CLIENT_PROPERTY_FILE);
              settings.put(KEY_SCHEME_FILE_TO_IMPORT, file.getAbsolutePath());
              progress.finished(settings);
              break;
            }
          }
        }
      }
    };
  }

  @Override
  protected void renderingPage() {
    if (!radioBtnInitialised) {
      IImportCallback callback = (IImportCallback) getWizardData(IImportCallback.KEY_CALLBACK);

      File[] recentFiles = callback.getRecentSchemeFiles();

      if (recentFiles != null && recentFiles.length > 0) {
        JRadioButton btn;
        for (int i = 0; i < recentFiles.length; i++) {
          btn = new JRadioButton(String.format("<html>%s (<em>%s</em>)</html>", 
              recentFiles[i].getName(), recentFiles[i].getAbsolutePath()));
          btn.setToolTipText(recentFiles[i].getAbsolutePath());
          panelRecentFiles.add(btn);
          btn.putClientProperty(CLIENT_PROPERTY_FILE, recentFiles[i]);
          group.add(btn);
          
          if(i == 0)
            btn.setSelected(true);
        }
        labelRecentFiles.setVisible(true);

      } else {
        labelRecentFiles.setVisible(false);
      }

      radioBtnInitialised = true;
    }
  }

  private void btnBrowseActionPerformed(ActionEvent e) {
    IImportCallback callback = (IImportCallback) getWizardData(IImportCallback.KEY_CALLBACK);
    File srcDir = callback.getSchemeFileDir();     

    File file = DialogUtils.showFileChooseDialog(getParent(),
        srcDir,
        "Import an existing "+ AutoSchemeImportWizard.AUTO_SCHEME_STR,
        "Select",
        "",
        CommonResources.IL_FILTER);
    if(file != null) {
      tfPath.setText(file.getAbsolutePath());
    }
  }

  private void radioBtnImportStateChanged(ChangeEvent e) {
    // Update related components' "enable" state
    JRadioButton radioBtn = (JRadioButton) e.getSource();
    boolean enabled = radioBtn.isSelected();
    tfPath.setEnabled(enabled);
    btnBrowse.setEnabled(enabled);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    radioBtnImport = new JRadioButton();
    tfPath = new JTextField();
    btnBrowse = new JButton();
    labelRecentFiles = new JLabel();
    panelRecentFiles = new JPanel();

    //======== this ========
    setLayout(new FormLayout(
        "default, $lcgap, default:grow, $lcgap, default",
        "default, $pgap, top:default, $ugap, default"));

    //---- radioBtnImport ----
    radioBtnImport.setText("Import from location:");
    radioBtnImport.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        radioBtnImportStateChanged(e);
      }
    });
    add(radioBtnImport, CC.xy(1, 1));
    add(tfPath, CC.xy(3, 1));

    //---- btnBrowse ----
    btnBrowse.setText("Browse...");
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnBrowseActionPerformed(e);
      }
    });
    add(btnBrowse, CC.xy(5, 1));

    //---- labelRecentFiles ----
    labelRecentFiles.setText("Import a recent file:");
    add(labelRecentFiles, CC.xy(1, 3));

    //======== panelRecentFiles ========
    {
      panelRecentFiles.setLayout(new GridLayout(10, 1, 5, 5));
    }
    add(panelRecentFiles, CC.xywh(1, 5, 5, 1));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JRadioButton radioBtnImport;
  private JTextField tfPath;
  private JButton btnBrowse;
  private JLabel labelRecentFiles;
  private JPanel panelRecentFiles;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
