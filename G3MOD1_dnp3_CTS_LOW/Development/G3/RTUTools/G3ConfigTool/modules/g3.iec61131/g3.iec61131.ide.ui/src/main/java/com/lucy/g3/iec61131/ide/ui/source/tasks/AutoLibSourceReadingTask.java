/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.tasks;

import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

import com.lucy.g3.iec61131.debug.comms.IEC61131DebuggerComms;
import com.lucy.g3.iec61131.ide.ui.source.CommonResources;
import com.lucy.g3.iec61131.ide.ui.util.FileUtil;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * A task for reading 61131 source file from RTU.
 */
public class AutoLibSourceReadingTask extends FileReadingTask {

  protected Logger log = Logger.getLogger(AutoLibSourceReadingTask.class);

  
  private final IEC61131DebuggerComms comms;

  private String sourceSuffix = FileUtil.IL_SUFFIX;

  private final File targetDir;

  private final FileTransfer fileTransfer;
  
  private boolean readRunningSource;
  
  public AutoLibSourceReadingTask(Application app, FileTransfer fileTransfer, IEC61131DebuggerComms comms, File targetDir) {
    super(app, fileTransfer,
        CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_AUTOLIB, "", null, true);
    this.comms = comms;
    this.targetDir = targetDir;
    this.fileTransfer = fileTransfer;
  }
  
  public AutoLibSourceReadingTask(Application app, FileTransfer fileTransfer, String rtuIP, File targetDir) {
    this(app, fileTransfer, new IEC61131DebuggerComms(rtuIP), targetDir);
  }
    
  public boolean isReadRunningFile() {
    return readRunningSource;
  }
  
  public void setReadRunningFile(boolean readRunningFile) {
    this.readRunningSource = readRunningFile;
  }

  public String getSourceSuffix() {
    return sourceSuffix;
  }

  public void setSourceSuffix(String sourceSuffix) {
    this.sourceSuffix = sourceSuffix;
  }

  @Override
  protected File doInBackground() throws Exception {
    setMessage("Reading IEC-61131 source file path from RTU...");
    // Waiting block dialog to be shown
    Thread.sleep(500);
    
    // Get source names
    String[] allSources = getAllSourceNames();
    String runningSource = getRunningSourceName();
    
    String sourceToBeRead;
    
    if(readRunningSource) {
      sourceToBeRead = runningSource;
      if(sourceToBeRead == null) 
        throw new IOException("No running scheme found!");
    } else {
      sourceToBeRead = askUserToSelect(allSources, runningSource);
      if(sourceToBeRead == null) {
        cancel(true);
        return null;
      } 
    }
    
   
    
    // Set file to be transferred 
    setSourceFileName(sourceToBeRead);
    
    // Create local file
    File localDir = Application.getInstance().getContext().getLocalStorage().getDirectory();
    if (!localDir.exists()) {
      localDir.mkdirs();
    }
    
    File localFile = new File(targetDir == null ? localDir : targetDir, sourceToBeRead);
    localFile.deleteOnExit();
    setDestination(localFile);
    localFile.getParentFile().mkdirs(); // Create folders.

    setMessage("Transferring IL source file from RTU...");
    return super.doInBackground();
  }

  private String askUserToSelect(String[] allSources, String runningSource) throws InvocationTargetException, InterruptedException {
    SelectSourceFromList task = new SelectSourceFromList(getParent(), allSources, runningSource);
    SwingUtilities.invokeAndWait(task);
    return task.getSelected();
  }

  private String[] getAllSourceNames() throws Exception {
    String[] autolibFiles = fileTransfer.cmdGetFileList(CommonResources.AUTO_LIB_DIR);
    ArrayList<String> sourceNames = new ArrayList<>();
    
    for (int i = 0; i < autolibFiles.length; i++) {
      if(autolibFiles[i].endsWith(sourceSuffix))
        sourceNames.add(autolibFiles[i]);
    }
    return sourceNames.toArray(new String[sourceNames.size()]);
  }
  
  private String getRunningSourceName() {
    String sourceName = null;
    final boolean connected= comms.isConnected();
    
    try {
      if(!connected)
        comms.connect();
      
      String libName = comms.getLibraryFileName();
      log.info("Running automation lib name:"+libName);
      
      sourceName = FileUtil.convertIEC61131LibPathToSourceName(libName, sourceSuffix);
      log.info("Running automation source name:"+sourceName);
      
    } catch (Exception e) {
      log.error("Failed to read auto lib path from:"+comms.getIp() + " port:" + comms.getPort());
    }finally {
      if(connected == false && comms != null) {
        comms.disconnect();
      }
    }
    
    return sourceName; 
  }
  
  private static class SelectSourceFromList implements Runnable {
    private final String[] allItems;
    private final String initialSelection;
    private String selected;
    private Window parent;
    
    public SelectSourceFromList(Window parent, String[] allItems, String initialSelection) {
      super();
      this.parent = parent;
      this.allItems = allItems;
      this.initialSelection = initialSelection;
    }

    @Override
    public void run() {
      selected = (String) JOptionPane.showInputDialog(parent, 
          "Please select an IEC-61131 source file to read:", 
          "Select", 
          JOptionPane.QUESTION_MESSAGE, null, allItems, initialSelection);
    }

    public String getSelected() {
      return selected;
    }
    
  }
}
