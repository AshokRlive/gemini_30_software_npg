/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.breakpoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.iec61131.ide.ui.AbstractManager;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;

/**
 *
 */
public class BreakpointManager extends AbstractManager{
  
  private final MultiListSelectionAdapter<Breakpoint> bpSelectModel;
  private final ArrayListModel<Breakpoint> bpListModel;
  private final MultiSelectionInList<Breakpoint> bpSelectList;
  private final TableModel bpDataModel;
  
  
  private final ArrayList<IBreakpointObserver> observers;
  
  
  public BreakpointManager(IEC61131Context context) {
    super(context);
    
    this.bpListModel = new ArrayListModel<>();
    this.bpSelectList = new MultiSelectionInList<>(bpListModel);
    this.bpSelectModel = new MultiListSelectionAdapter<>(bpSelectList);
    this.bpDataModel = new BreakPointTableModel(bpListModel);
    this.observers = new ArrayList<>();
  }
  
  public void addObserver(IBreakpointObserver observer) {
    observers.add(observer);
  }
  
  public void removeObserver(IBreakpointObserver observer) {
    observers.remove(observer);
  }
  
  /**
   * 
   * @param line line number start from one.
   * @param isConnected
   * @return the breakpoint that has been toggled.
   */
  public Breakpoint toggleBreakpoint(int line, String content) {
    Breakpoint bp = findBreakpoint(line);
      if(bp != null) {
        removeBreakpoint(bp);
      } else {
        bp = addBreakpoint(line, content);
      }
    
    return bp;
  }

  /**
   * 
   * @param line line number start from one.
   * @param content
   * @return
   */
  public Breakpoint addBreakpoint(int line, String content) {
    if(context.debugger.isConnected()) {
      try {
        context.comms.setBreakpoint(line - 1);
        context.console.appendSuccess("Set remote breakpoint:"+line);
      } catch (IOException e) {
        context.errHandler.handleError("Failed to set remote breakpoint:"+line, e);
      }
    }
    
    Breakpoint bp;
    bp = new Breakpoint(line, content);
    bpListModel.add(bp);
    
    for (IBreakpointObserver ob : observers) {
      ob.breakpointAdded(bp);
    }
    return bp;
  }

  public void removeBreakpoint(Breakpoint bp) {
    if (bp != null && !bp.isRemoved()) {
      if (context.debugger.isConnected()){
        try {
          context.comms.delBreakpoint(bp.getLineNum() - 1);
          context.console.appendSuccess("Delete remote breakpoint:"+bp.getLineNum());
        } catch (IOException e) {
          context.errHandler.handleError("Failed to remove breakpoint from remote:"+bp.getLineNum(), e);
        }
      }

      bpListModel.remove(bp);
      bp.setRemoved(true);

      for (IBreakpointObserver ob : observers) {
        ob.breakpointDeleted(bp);
      }
    }
  }

  public ListModel<Breakpoint> getBreakpointListModel() {
    return bpListModel;
  }
  
  public List<Breakpoint> getSelectedBreakpoints() {
    return new ArrayList<>(bpSelectList.getSelection());
  }

  public ListSelectionModel getBreakpointSelectModel() {
    return bpSelectModel;
  }

  public void sendAllBreakpointsToServer(){
      for (Breakpoint bp : bpListModel) {
        try {
          context.comms.setBreakpoint(bp.getLineNum() - 1);
          context.console.appendSuccess("Set remote breakpoint:"+bp.getLineNum());
        } catch (IOException e) {
          context.errHandler.handleError("Failed to set remote breakpoint:"+bp.getLineNum(), e, false);
        }
      }
  }
  
  public TableModel getBreakpointDataModel() {
    return bpDataModel;
  }
  
  public Breakpoint[] getAllBreakpoints() {
    return bpListModel.toArray(new Breakpoint[bpListModel.size()]);
  }
  
  private Breakpoint findBreakpoint(int line) {
    for (Breakpoint bp : bpListModel) {
      if(bp.getLineNum() == line)
        return bp;
    }
    return null;
  }

  private static class BreakPointTableModel extends AbstractTableAdapter<Breakpoint> {

    private final static int COL_LINE_NUM = 0;
    private final static int COL_CONTENT = 1;
    
    private final static String COL_NAMES[] = {"Line Num","Line"};
    
    public BreakPointTableModel(ListModel<Breakpoint> bplistModel) {
      super(bplistModel, COL_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Breakpoint bp = getRow(rowIndex);
      switch(columnIndex) {
        case COL_LINE_NUM:
          return bp.getLineNum();
        case COL_CONTENT:
          return bp.getLineContent();
      }
      
      return null;
    }
    
  }

  public boolean hasSelections() {
    return bpSelectList.hasSelection();
  }
  
  public boolean isEmpty() {
    return bpListModel.isEmpty();
  }

  /**
   * 
   * @param curLine line number start from one.
   * @return
   */
  public boolean isBreakpoint(int curLine) {
    return findBreakpoint(curLine) != null;
  }
}
