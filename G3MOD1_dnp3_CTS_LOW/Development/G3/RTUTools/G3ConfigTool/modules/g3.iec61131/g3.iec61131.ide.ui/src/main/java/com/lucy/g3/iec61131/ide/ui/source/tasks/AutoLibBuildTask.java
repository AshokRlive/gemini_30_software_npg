/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.tasks;

import java.awt.Window;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.iec61131.ide.ui.compiler.CrossCompiler;
import com.lucy.g3.iec61131.ide.ui.compiler.ICompiler;
import com.lucy.g3.iec61131.ide.ui.compiler.IEC61131SourceCompiler;
import com.lucy.g3.iec61131.ide.ui.compiler.IOutputHandler;
import com.lucy.g3.iec61131.ide.ui.source.builder.BuildingParameters;
import com.lucy.g3.iec61131.ide.ui.source.builder.StringOutputHandler;
import com.lucy.g3.iec61131.ide.ui.util.FileUtil;

/**
 * A task for building 61131 source to library.
 */
public class AutoLibBuildTask extends Task<String, Void> {
  private Logger log = Logger.getLogger(AutoLibBuildTask.class);
  
  private final BuildingParameters param;
  private final IOutputHandler outputHandler;
  private final StringOutputHandler compilerOutput = new StringOutputHandler();
  
  private Window parent;
  
  public AutoLibBuildTask(Application application, IOutputHandler outputHandler, BuildingParameters parameters) {
    super(application);
    this.param = parameters;
    this.outputHandler = outputHandler;
    setUserCanCancel(true);
  }

  
  public void setParent(Window parent) {
    this.parent = parent;
  }

  @Override
  protected String doInBackground() throws Exception {
    outputHandler.append("Building...");
    printMap("Building with parameters:", param.getMap());
    param.checkParams();
    
    String output;
    
    output = compileIL(param);
    
    outputHandler.append(System.getProperty("line.separator"));
    
    output = compileC(output, param);

    return output;
  }

  private String compileIL(BuildingParameters param) throws IOException, InterruptedException {
    outputHandler.append("Compiling IL...");
    String source = param.getSource();
    String outputDir = param.getTargetDir();
    IEC61131SourceCompiler ilcompiler = new IEC61131SourceCompiler(param.getILCompilerPath(), outputHandler, compilerOutput);
    
    int ret = ilcompiler.compile(source, outputDir);
    if(ret != 0) {
      throw new RuntimeException("Cannot compile IL source. Error: "+ ret);
    }
    
    outputHandler.append("IL source compiled!");
    return outputDir + FileUtil.SEP+ FileUtil.getFileNameWithoutExtension(source) + ".c";
  }

  /**
   * 
   * @param source - C source code.
   * @param param
   * @return
   * @throws IOException
   * @throws InterruptedException
   * @throws Exception
   */
  private String compileC(String source, BuildingParameters param) throws IOException, InterruptedException {
    final String compilerPath = param.getCrossCompilerPath();
    final String targetDir = param.getTargetDir();
    final String commonDir = targetDir +FileUtil.SEP+"common"+FileUtil.SEP;
    final String compilerPrefix= param.getCrossCompilerPrefix();
    
    final String includePath = commonDir;
    final String commonSourceFile = commonDir + ICompiler.Resources.COMMON_C_FILENAME;
    
    outputHandler.append("Compiling shared library...");
    
    /* Extracting common files*/
    extractCommonCFiles(commonDir);
    
    CrossCompiler crosscompiler = new CrossCompiler(compilerPath, compilerPrefix, includePath, commonSourceFile, outputHandler, compilerOutput);
    int ret = crosscompiler.compile(source, param.getTargetDir());
    if(ret != 0) {
      throw new RuntimeException("Cannot compile shared library. Error: "+ ret);
    }
    
    outputHandler.append("Shared library compiled!");
    return crosscompiler.getOutputFile();
  }

  private void extractCommonCFiles(final String commonDir) throws IOException {
    ICompiler.Resources.copyTo(commonDir);
  }
  
  private void printMap(String tilte, Map<String, ?> map) { 
    log.info(tilte);  
    for (String name: map.keySet()){
      String key =name.toString();
      String value = map.get(name).toString();  
      log.info("* " + key + ": " + value);  
    } 
  }

  @Override
  protected void cancelled() {
    if(outputHandler != null)
      outputHandler.append("Building task cancelled!");
  }

  @Override
  protected void succeeded(String result) {
    if(outputHandler != null)
      outputHandler.appendSuccess("The library has been succesfully built to:\n"+result);
  }

  @Override
  protected void failed(Throwable cause) {
    String msg = cause.getMessage() == null ? "The building process failed!" : cause.getMessage();
    
    log.error("Auto scheme building task failed!",cause);
    
    if(outputHandler != null)
      outputHandler.appendErr(msg);
   

    /* Show failure dialog */
    String detail = String.format("<p>%s</p><font color=\"red\">%s</font> ", compilerOutput.getOutputAsString(), msg);
    ErrorInfo info = new ErrorInfo("Failure", "The building process failed!", detail,
        null, null, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
    
  }
  
  
}
