/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.register;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.jgoodies.binding.adapter.Bindings;

public class RegisterPanel extends JPanel {

  private final RegisterManager manager;

  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private RegisterPanel() {
    this.manager = new RegisterManager();
    initComponents();
  }
  
  public RegisterPanel(RegisterManager manager) {
    this.manager = manager;
    initComponents();
  }

  private void createUIComponents() {
    this.tableRegisters = new JTable(manager.getTableModel());
    Bindings.bind(tableRegisters, manager.getListModel(), manager.getListSelectionModel());
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {

      //---- tableRegisters ----
      tableRegisters.setFillsViewportHeight(true);
      scrollPane1.setViewportView(tableRegisters);
    }
    add(scrollPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JTable tableRegisters;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
