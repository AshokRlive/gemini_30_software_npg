
package com.lucy.g3.iec61131.ide.ui.source.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.log4j.Logger;

public class FileMonitor extends FileAlterationListenerAdaptor{

  private static final int MONITOR_INTERVAL_MS = 3000;
  
  private Logger log = Logger.getLogger(FileMonitor.class);

  private final FileAlterationMonitor monitor = new FileAlterationMonitor(MONITOR_INTERVAL_MS);
  
  private FileAlterationObserver observer;
  private File observedFile;
  
  private final SourceManager sourceManager;
  
  public FileMonitor(SourceManager sourceManager) {
    this.sourceManager =  sourceManager;
    
    this.sourceManager.addPropertyChangeListener(SourceManager.PROPERTY_SOURCE_FILE, 
        new PropertyChangeListener() {
      
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        setMonitorFile((File) evt.getNewValue());
      }
    });
  }

  public void start() {
    try {
      log.info("starting monitor...");
      monitor.start();
      log.info("monitor started.");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void stop(){
    try {
      log.info("stopping monitor...");
      monitor.stop(1);
      log.info("monitor stopped.");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void pause() {
    // Remove old observer
    if(observer != null) {
      observer.removeListener(this);
      monitor.removeObserver(observer);
      observer = null;
    }
  }
  
  public void resume() {
    if(observedFile != null) {
      observer = createObserver(observedFile);
      observer.addListener(this);
      monitor.addObserver(observer);
    }
  }
  
  private FileAlterationObserver createObserver(final File observedFile) {
    File observedDir = new File(observedFile.getAbsolutePath()).getParentFile();
    return new FileAlterationObserver(observedDir, new FileFilter() {
      
      @Override
      public boolean accept(File pathname) {
        return observedFile.equals(pathname);
      }
    });
  }

  void setMonitorFile(File file) {
    // Remove old observer
    if(observer != null) {
      observer.removeListener(this);
      monitor.removeObserver(observer);
    }
    
    this.observedFile = file;
    
    if(file != null && file.isFile()) {
      observer = createObserver(file);
      observer.addListener(this);
      monitor.addObserver(observer);
    }
  }

  @Override
  public void onFileChange(final File file) {
      log.info(file.getAbsoluteFile() + " was modified.");
      log.info("----------> length: " + file.length());
      log.info("----------> last modified: " + new Date(file.lastModified()));
      log.info("----------> readable: " + file.canRead());
      log.info("----------> writable: " + file.canWrite());
      log.info("----------> executable: " + file.canExecute());
      
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          
          @Override
          public void run() {
            if(sourceManager != null) {
              ISourceEditor editor = sourceManager.getEditor();
              if(editor != null) {
                
                int ret = JOptionPane.showConfirmDialog(sourceManager.getContext().getParent(),
                    file.getAbsolutePath()+"\n\nThe file has been modified by another program."
                        + " \nDo you want to reload it?", "Reload", JOptionPane.YES_NO_OPTION);
                
                if(ret == JOptionPane.YES_OPTION) {
                  editor.loadFile(file);
                } else {
                  sourceManager.setSourceChanged(true);
                }
              }
            }
          }
        });
      } catch (InvocationTargetException | InterruptedException e) {
        e.printStackTrace();
      }
  }

  @Override
  public void onFileDelete(final File file) {
      log.info(file.getAbsoluteFile() + " was deleted.");
      
      
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          
          @Override
          public void run() {
            if(sourceManager != null) {
                
                int ret = JOptionPane.showConfirmDialog(sourceManager.getContext().getParent(),
                    file.getAbsolutePath()+"\n\nThe file doesn't exist anymore.\nKeep this file in editor?", 
                    "Keep non existing file", JOptionPane.YES_NO_OPTION);
                
                if(ret != JOptionPane.YES_OPTION) {
                  sourceManager.closeFile();
                } else{
                  sourceManager.setSourceChanged(true);
                }
            }
          }
        });
      } catch (InvocationTargetException | InterruptedException e) {
        e.printStackTrace();
      }
  }
}