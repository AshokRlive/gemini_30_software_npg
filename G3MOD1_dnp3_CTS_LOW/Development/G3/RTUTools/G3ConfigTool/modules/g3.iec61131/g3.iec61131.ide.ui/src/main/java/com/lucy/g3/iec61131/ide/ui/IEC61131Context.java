/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui;

import java.awt.Window;
import java.util.logging.Level;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.iec61131.debug.comms.IEC61131DebuggerComms;
import com.lucy.g3.iec61131.ide.ui.breakpoint.BreakpointManager;
import com.lucy.g3.iec61131.ide.ui.console.ConsoleFactory;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager;
import com.lucy.g3.iec61131.ide.ui.exception.IErrorHandler;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager;
import com.lucy.g3.iec61131.ide.ui.register.RegisterManager;
import com.lucy.g3.iec61131.ide.ui.source.builder.BuildingManager;
import com.lucy.g3.iec61131.ide.ui.source.editor.RecentItems;
import com.lucy.g3.iec61131.ide.ui.source.editor.SourceManager;
import com.lucy.g3.iec61131.ide.ui.variable.VariableManager;

/**
 * The presentation model of debugger window.
 */
public class IEC61131Context {

  public final IEC61131DebuggerComms comms  = new IEC61131DebuggerComms();
  
  public final BreakpointManager breakpoint;
  public final RegisterManager   register;
  public final VariableManager   variable;
  public final DebugManager      debugger;
  public final IErrorHandler     errHandler;
  public final BuildingManager   builder;
  public final SourceManager     source;
  public final PreferencesManager prefs; 
  public final RecentItems       recentItems; 
  
  public final IConsole  console = ConsoleFactory.getConsole();


  private Window parent;

  public IEC61131Context() {
    this.prefs = new PreferencesManager(this);
    this.debugger = new DebugManager(this);
    this.breakpoint = new BreakpointManager(this);
    this.builder = new BuildingManager(this);
    this.register = new RegisterManager();
    this.variable = new VariableManager();
    this.errHandler = new ErrorHandler();
    this.source  = new SourceManager(this);
    this.recentItems = new RecentItems(5, prefs.getPrefs());
  }
  
  public void setParent(Window parent) {
    this.parent = parent;
  }
  
  public Window getParent() {
    return parent;
  }
  
  private class ErrorHandler implements IErrorHandler {
    @Override
    public void handleError(String error, Exception cause) {
      handleError(error, cause, true);
    }

    @Override
    public void handleError(String error, Exception cause, boolean showDialog) {
      String errorDetail = cause == null ? null : cause.getMessage();

      if (!Strings.isBlank(errorDetail))
        console.appendErr(errorDetail);

      if (showDialog) {
        ErrorInfo info = new ErrorInfo("Failure", error, errorDetail,
            null, cause, Level.SEVERE, null);
        JXErrorPane.showDialog(parent, info);
      }
    }
  }
}
