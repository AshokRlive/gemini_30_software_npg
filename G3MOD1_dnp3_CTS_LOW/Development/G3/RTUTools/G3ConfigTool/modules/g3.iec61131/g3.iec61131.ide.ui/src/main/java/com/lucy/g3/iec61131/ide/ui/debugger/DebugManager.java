/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.debugger;

import java.io.IOException;
import java.util.Timer;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DBG_STATE;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugState;
import com.lucy.g3.iec61131.ide.ui.AbstractManager;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;
import com.lucy.g3.iec61131.ide.ui.register.RegisterID;

/**
 * The presentation model of debugger window.
 */
public class DebugManager extends AbstractManager {

  public final static String PROPERTY_HOST_ADDRESS = "hostAddress";
  public final static String PROPERTY_CONNECT_STATUS = "connectStatus";
  public final static String PROPERTY_RUNNING_STATE = "runningState";
  public final static String PROPERTY_POLLING_RATE = "pollingRate";
  
  public final static String ACTION_KEY_START = "startDebug";
  public final static String ACTION_KEY_STOP = "stopDebug";
  public final static String ACTION_KEY_PAUSE = "pause";
  public final static String ACTION_KEY_RESUME = "resume";
  public final static String ACTION_KEY_STEPOVER = "stepover";

  private static final int MIN_POLLING_RATE = 500;
  
  private Logger log = Logger.getLogger(DebugManager.class);

  private Timer pollingTimer;
  private int pollingRate = MIN_POLLING_RATE;

  private String hostAddress= "0.0.0.0";
  private Status connectStatus;
  
  private DebugState runningState;

  
  public DebugManager(IEC61131Context context) {
    super(context);
  }
  
  public int getPollingRate() {
    return pollingRate;
  }
  
  public synchronized void setPollingRate(int pollingRate) {
    if(pollingRate >= MIN_POLLING_RATE) {
      int oldValue = this.pollingRate;
      this.pollingRate = pollingRate;
      firePropertyChange(PROPERTY_POLLING_RATE, oldValue, pollingRate);
      
      if(oldValue != pollingRate && isConnected()) {
        /*Restart polling task*/
        stopPolling();
        startPolling();
      }
    }
  }

  public String getHostAddress() {
    return hostAddress;
  }

  public DebugState getRunningState() {
    return runningState;
  }
  
  void setRunningState(DebugState newState) {
    DebugState oldValue = this.runningState;
    this.runningState = newState;
    
    if(runningState != null && log.isDebugEnabled()) {
      log.debug(String.format("State: CR: %s, State:%s, Line:%s ", 
        runningState.cr,runningState.curState,runningState.curLineFromOne()));
    }
    
    // Update register
    context.register.setRegister(RegisterID.CR, newState == null ? 0 :newState.cr);

    // Check if reached breakpoint
    if(newState != null
        && newState.curState == DBG_STATE.SUSPENDED.getValue()
        && context.breakpoint.isBreakpoint(newState.curLineFromOne())
        && (oldValue == null || oldValue.curLineFromOne() != newState.curLineFromOne())){
      context.console.appendMsg("Reached breakpoint:" + newState.curLineFromOne());
    }
    
    firePropertyChange(PROPERTY_RUNNING_STATE, oldValue, newState);
    updateActionState(newState);
  }

  public void setHostAddress(String hostAddress) {
      Object oldValue = this.hostAddress;
      this.hostAddress = hostAddress;
      firePropertyChange(PROPERTY_HOST_ADDRESS, oldValue, this.hostAddress);
      
    try {
      context.comms.setHost(hostAddress);
    } catch (IOException e) {
      context.errHandler.handleError("Invalid host address: " + hostAddress, e);
    }
  }

  public Status getConnectStatus() {
    return connectStatus;
  }

  private void setConnectStatus(Status status) {
    Object oldValue = this.connectStatus;
    this.connectStatus = status;
    firePropertyChange(PROPERTY_CONNECT_STATUS, oldValue, this.connectStatus);
    updateActionState(status);
    
    if(Status.CONNECTED == status) {
      startPolling();
    }else{
      stopPolling();
    }
    
    context.console.appendMsg(status.name());
  }

  private void updateActionState(DebugState state) {
    DBG_STATE debugState = state == null ? null : state.getState(); 
    
    context.debugger.getAction(DebugManager.ACTION_KEY_PAUSE)
    .setEnabled(debugState == DBG_STATE.RUNNING);
    context.debugger.getAction(DebugManager.ACTION_KEY_RESUME)
    .setEnabled(debugState == DBG_STATE.SUSPENDED);
    context.debugger.getAction(DebugManager.ACTION_KEY_STEPOVER)
    .setEnabled(debugState == DBG_STATE.SUSPENDED);
  }
  
  private void updateActionState(Status status) {
    boolean connected = status == Status.CONNECTED;
    getAction(ACTION_KEY_START).setEnabled(!connected);
    getAction(ACTION_KEY_STOP).setEnabled(connected);
  }

  private void stopPolling() {
    if(pollingTimer != null) {
      pollingTimer.cancel();
      pollingTimer.purge();
      pollingTimer = null;
    }
  }

  private void startPolling() {
    if(pollingTimer == null){
      pollingTimer = new Timer("IEC61131PollingTimer",true);
    }
    
    pollingTimer.schedule(new DebugPollingTask(context), 100, getPollingRate());
  }
  
  public boolean isConnected() {
    return Status.CONNECTED == connectStatus;
  }
  
  @org.jdesktop.application.Action
  public synchronized void startDebug() {
    if(context.source.isSourceFileExist() == false) {
      JOptionPane.showMessageDialog(context.getParent(), "No source file loaded for debugging!", 
          "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }
      
    try {
      context.console.appendMsg(String.format("Connecting to \"%s:%s\"...",context.comms.getIp(),context.comms.getPort()));
      String welcome = context.comms.connect();
      context.console.appendMsg(welcome);
      
      setConnectStatus(Status.CONNECTED);
      
      /*Send breakpoint list to server when connected*/
      context.breakpoint.sendAllBreakpointsToServer();
     
    } catch (IOException e) {
      context.errHandler.handleError("Failed to connect to the host: " + hostAddress
          +".\nPlease ensure the RTU automation is running.", e);
    }
  }

  @org.jdesktop.application.Action
  public synchronized  void stopDebug() {
      context.comms.disconnect();
      setConnectStatus(Status.DISCONNECTED);
      setRunningState(null);
      context.console.appendBlankLine();
  }
  
  @org.jdesktop.application.Action
  public void pause() {
    try {
      context.comms.pause();
      context.console.appendSuccess("PAUSED");
    } catch (IOException e) {
      context.errHandler.handleError("Failed to pause debugging", e);
    }
  }

  @org.jdesktop.application.Action
  public void resume() {
    try {
      context.comms.resume();
      context.console.appendSuccess("RESUMED");
    } catch (IOException e) {
      context.errHandler.handleError("Failed to resume debugging", e);
    }
  }

  @org.jdesktop.application.Action
  public void stepover() {
    try {
      context.comms.stepover();
      context.console.appendSuccess("STEP OVER");
    } catch (IOException e) {
      context.errHandler.handleError("Failed to step over debugging", e);
    }
  }

  public static enum Status {
    CONNECTED,
    DISCONNECTED
  }
}
