
package com.lucy.g3.iec61131.ide.ui.util;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

public class FileUtil {
  /**
   * File separator.
   */
  public final static String SEP = File.separator;
  public final static String SHARED_LIB_PREFIX = "lib";
  public final static String SHARED_LIB_SUFFIX = ".so";
  public final static String IL_SUFFIX = ".il";
  
  private FileUtil(){}
  
  public static String getFileNameWithoutExtension(String filePath) {
    return FilenameUtils.removeExtension(FilenameUtils.getName(filePath));
  }
  
  public static String convertIEC61131SourcePathToLibName(String sourcePath) {
    return String.format("%s%s%s", SHARED_LIB_PREFIX, getFileNameWithoutExtension(sourcePath), SHARED_LIB_SUFFIX); 
  }
  
  public static String convertIEC61131LibPathToSourceName(String sharedLibPath, String sourceSuffix) {
    sharedLibPath = FilenameUtils.getName(sharedLibPath);
    
    // Remove prefix
    if(sharedLibPath.startsWith(SHARED_LIB_PREFIX))
        sharedLibPath = sharedLibPath.substring(SHARED_LIB_PREFIX.length());
    
    // Remove suffix
    sharedLibPath = FilenameUtils.removeExtension(sharedLibPath);
    
    return sourceSuffix != null ? sharedLibPath + sourceSuffix : sharedLibPath;
  }
}

