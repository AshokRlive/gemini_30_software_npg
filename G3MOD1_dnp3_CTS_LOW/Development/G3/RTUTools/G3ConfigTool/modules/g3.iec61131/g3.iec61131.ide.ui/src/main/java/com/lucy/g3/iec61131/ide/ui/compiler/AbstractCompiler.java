/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.compiler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;


/**
 * The Class AbstractCompiler.
 */
public abstract class AbstractCompiler implements ICompiler {

  protected final IOutputHandler[] handlers;
  
  public AbstractCompiler(IOutputHandler... handler) {
    this.handlers = handler;
  }
  
  protected void handleOutputInThread(Process p) {
      IOThreadHandlerThread outputHandler = new IOThreadHandlerThread(p.getInputStream(), handlers);
      outputHandler.start();
  }
  
  protected void handleOutput(Process p) throws IOException, InterruptedException {
    output(p.getInputStream(), handlers);
  }
  
  private static void output(InputStream inputStream, IOutputHandler[] outputs) throws IOException, InterruptedException {
    BufferedReader br = null;
    try {
      br = new BufferedReader(new InputStreamReader(inputStream));
      String line = null;
      while ((line = br.readLine()) != null) {
        for (int i = 0; outputs !=null && i < outputs.length; i++) {
          if(outputs[i] != null) {
          outputs[i].append(line + System.getProperty("line.separator"));
          }
        }
      }
    } finally {
      br.close();
    }
  }
  
  
  /**
   * The Class IOThreadHandlerThread.
   */
  private static class IOThreadHandlerThread extends Thread {

    private InputStream inputStream;
    private final IOutputHandler[] handlers;


    IOThreadHandlerThread(InputStream inputStream, IOutputHandler[] handlers) {
      super("OutputHandler");
      
      this.setDaemon(true);
      this.inputStream = inputStream;
      this.handlers = handlers;
    }

    @Override
    public void run() {
      Scanner br = null;
      try {
        br = new Scanner(new InputStreamReader(inputStream));
        String line = null;
        while (br.hasNextLine()) {
          line = br.nextLine();
          for (int i = 0; i < handlers.length; i++) {
            if (handlers[i] != null)
              handlers[i].append(line);
          }
        }
      } finally {
        br.close();
      }
    }
  }
}

