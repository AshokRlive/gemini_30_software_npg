/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.editor;

import java.awt.Desktop;
import java.awt.Window;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.log4j.Logger;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;

import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.iec61131.ide.ui.AbstractManager;
import com.lucy.g3.iec61131.ide.ui.IEC61131Context;
import com.lucy.g3.iec61131.ide.ui.breakpoint.BreakpointManager;
import com.lucy.g3.iec61131.ide.ui.console.ConsoleFactory;
import com.lucy.g3.iec61131.ide.ui.exception.IErrorHandler;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager.Key;
import com.lucy.g3.iec61131.ide.ui.source.CommonResources;
import com.lucy.g3.iec61131.ide.ui.source.tasks.AutoLibSourceReadingTask;

/**
 *
 */
public class SourceManager extends AbstractManager {
  private Logger log = Logger.getLogger(SourceManager.class);
  
  public final static String PROPERTY_SOURCE_FILE = "sourceFile";
  public final static String PROPERTY_SOURCE_FILE_EXIST = "sourceFileExist";

  public final static String PROPERTY_SOURCE_CHANGED = "sourceChanged";
  public final static String ACTION_KEY_NEW = "createNew";
  public final static String ACTION_KEY_READ = "readFromRTU";
  public final static String ACTION_KEY_OPEN = "openFile";
  public final static String ACTION_KEY_OPEN_DIR = "openFileDir";
  public final static String ACTION_KEY_CLOSE = "closeFile";
  public final static String ACTION_KEY_SAVE = "save";
  public final static String ACTION_KEY_SAVE_AS = "saveAs";
  public final static String ACTION_KEY_TOGGLE_BP = "toggleBreakpoint";

  private final IConsole console = ConsoleFactory.getConsole();

  private File sourceFile;
  private boolean sourceChanged;

  private ISourceEditor editor;
  private final FileMonitor fileMonitor;
  
  public SourceManager(IEC61131Context ctxt) {
    super(ctxt);
    this.fileMonitor = new FileMonitor(this);
  }

  public File getSourceFile() {
    return sourceFile;
  }

  
  public FileMonitor getFileMonitor() {
    return fileMonitor;
  }

  private void setSourceFile(File sourceFile) {
    Object oldValue = this.sourceFile;
    this.sourceFile = sourceFile;
    firePropertyChange(PROPERTY_SOURCE_FILE, oldValue, sourceFile);
    firePropertyChange(PROPERTY_SOURCE_FILE_EXIST, null, isSourceFileExist());
  }
  
  private void setSourceDir(File dir) {
    context.prefs.put(Key.KEY_SOURCE_FILE_DIR, dir.getAbsolutePath());    
  }

  public File getSourceDir() {
    String dirPath = context.prefs.get(Key.KEY_SOURCE_FILE_DIR);
    File dir;
    if(dirPath == null || dirPath.isEmpty()) {
      dir = new File(PreferencesManager.USER_HOME);
    } else {
      dir = new File(dirPath);
    }
    return dir;
  }

  private static File chooseFileToSave(Window parent, File dir, String title, String defaultFileName) {
    File file = DialogUtils.showFileChooseDialog(parent,
        dir,
        title,
        "Save",
        defaultFileName,
        CommonResources.IL_FILTER);
    if (file != null && file.exists()) {
        if(MessageDialogs.showOverwriteDialog(parent, file) == false)
          return null;
    }
    
    return file;
  }
  
  @org.jdesktop.application.Action
  public void readFromRTU() {
    if(!checkSaved()) 
      return;
    
    // Select target folder to saving uploaded file
    File targetDir;
    String srcDir = context.prefs.get(Key.KEY_SOURCE_FILE_DIR);
    
    if(srcDir == null || srcDir.isEmpty()) {
      /*Source dir not found, ask user to select source dir*/
      JFileChooser fc = new JFileChooser();
      fc.setDialogTitle("Choose a directory to save");
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      fc.showDialog(getParent(), "Select");
      targetDir = fc.getSelectedFile();
      if(targetDir == null)
        return;// user cancelled
      
    }else {
      targetDir = new File(srcDir);
    }
    
    AutoLibSourceReadingTask task = new AutoLibSourceReadingTask(getApp(), 
        getRTU().getComms().getFileTransfer(), 
        context.comms,
        targetDir);
    task.setParent((JFrame) context.getParent());
    task.addTaskListener(new AutoSourceReadingListener());
    getApp().getContext().getTaskService().execute(task);
    setSourceDir(targetDir);
  }
  
  @org.jdesktop.application.Action
  public void createNew() {
    if(checkSaved() == false)
      return;
    
    File file = chooseFileToSave(getParent(), getSourceDir(), "New source file","Untitled.il");
    
    if(file != null) {
      try {
        file.createNewFile();
        readFile(file);
      } catch (IOException e1) {
        JOptionPane.showMessageDialog(getParent(),
            "Cannot create a new file cause:\n"+e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  @org.jdesktop.application.Action
  public void openFile() {
    if(checkSaved() == false)
      return;
    
    File file = DialogUtils.showFileChooseDialog(getParent(),
        getSourceDir(),
        "Open source file",
        "Select",
        "",
        CommonResources.IL_FILTER);
    if(file != null) {
      readFile(file);
    }
  }

  @org.jdesktop.application.Action
  public void openFileDir() {
    try {
      Desktop.getDesktop().open(getSourceDir());
    } catch (IOException e) {
      MessageDialogs.error("Failed to open directory", e);
    }
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_SOURCE_CHANGED)
  public void save() {
    if (editor == null) {
      log.error("Nothing to save cause editor is not found!");
      return;
    }

    if (sourceFile == null) {
      log.error("No source file created/loaded!");
      return;
    }
    
    writeEditingTextToFile(sourceFile);
  }

  @org.jdesktop.application.Action (enabledProperty = PROPERTY_SOURCE_FILE_EXIST)
  public void saveAs() {
    File file = chooseFileToSave(getParent(), getSourceDir(), "Save As...", 
        sourceFile == null ? "Untitled.il" : sourceFile.getName());
    
    if(file != null) {
      if(writeEditingTextToFile(file)) {
        setSourceFile(file);
        setSourceDir(file.getParentFile());
        getContext().recentItems.push(file.getAbsolutePath());
      }
    }

  }
  
  public void readFile(File file) {
    if(editor != null) {
      if(editor.loadFile(file) && file != null) {
        console.appendMsg("The IEC-61131 scheme file \""
      +file.getName()+"\" was loaded successfully!");
      }
    }
    
    setSourceFile(file);
    
    if(file != null) {
      setSourceDir(new File(file.getAbsolutePath()).getParentFile());
      getContext().recentItems.push(file.getAbsolutePath());
    }
  }

  private boolean writeEditingTextToFile(File file) {
    FileWriterWithEncoding fw = null;
    boolean succeeded;
    fileMonitor.pause();
    
    try {
      fw = new FileWriterWithEncoding(file, "UTF-8", false);
      editor.write(fw);
      setSourceChanged(false);
      succeeded = true;
      console.appendMsg("The source file has been saved!" );
    } catch (IOException e1) {
      succeeded = false;
      context.errHandler.handleError("Failed to save the source file!", e1);
    } finally {
      if(fw  != null) {
      try {
        fw.close();
      } catch (IOException e) {}
      }
    }
    
    fileMonitor.resume();
    
    return succeeded;
  }

  @org.jdesktop.application.Action
  public void closeFile() {
    if(!checkSaved()) 
      return;
    
    readFile(null);
  }

  private boolean checkSaved() {
    if(!isSourceChanged())
      return true; // No change to save

    int ret = JOptionPane.showConfirmDialog(context.getParent(),
        String.format("Source file \"%s\" has been modified. Save changes?", sourceFile.getName()), 
        "Save", JOptionPane.YES_NO_CANCEL_OPTION);
    if(ret == JOptionPane.YES_OPTION)
      save();
    else if(ret == JOptionPane.CANCEL_OPTION)
      return false;
    
    return true;
  }
  
  @org.jdesktop.application.Action
  public void toggleBreakpoint() {
    if(editor == null) {
      log.error("editor is null!");
      return;
    }

    int currentLine = editor.getCaretLineNumber();
    String content = editor.getCaretLineContent();
    
    if(currentLine >= 0)
      context.breakpoint.toggleBreakpoint(currentLine + 1,content);
  }
  
  /**
   * 
   * Property getter of {@link #PROPERTY_SOURCE_FILE_EXIST}
   * @return
   */
  public boolean isSourceFileExist() {
    return sourceFile != null && sourceFile.isFile() && sourceFile.exists();
  }
  
  public boolean isSourceChanged() {
    return sourceChanged;
  }
  
  public void setSourceChanged(boolean sourceChanged) {
    Object oldValue = this.sourceChanged;
    this.sourceChanged = sourceChanged;
    firePropertyChange(PROPERTY_SOURCE_CHANGED, oldValue, sourceChanged);
  }

  private Window getParent() {
    return context.getParent();
  }
  
  void setEditor(ISourceEditor editor) {
    this.editor = editor;
    
    if(editor != null) {
      editor.loadFile(sourceFile);
    }
  }

  
  public ISourceEditor getEditor() {
    return editor;
  }

  public IErrorHandler getErrHandler() {
    return context.errHandler;
  }
  
  public BreakpointManager getBreakpointManager() {
    return context.breakpoint;
  }
  
  private class AutoSourceReadingListener extends TaskListener.Adapter<File, Void> {
    @Override
    public void succeeded(TaskEvent<File> event) {
      readFile(event.getValue());
    }

    @Override
    public void failed(TaskEvent<Throwable> event) {
      MessageDialogs.error(context.getParent(), "Cannot read IEC-61131 scheme from RTU!", event.getValue());
    }
    
    
  }
}
