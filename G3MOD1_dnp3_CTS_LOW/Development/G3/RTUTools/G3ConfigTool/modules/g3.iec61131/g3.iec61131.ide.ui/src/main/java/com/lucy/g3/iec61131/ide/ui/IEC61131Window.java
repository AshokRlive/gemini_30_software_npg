/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.beans.PropertyConnector;
import com.lucy.g3.gui.common.dialogs.AboutBox;
import com.lucy.g3.gui.common.widgets.ext.swing.textfield.IPAddressField;
import com.lucy.g3.iec61131.ide.ui.breakpoint.BreakpointsPanel;
import com.lucy.g3.iec61131.ide.ui.breakpoint.BreakpointsPanel.IBreakpointPanelCallback;
import com.lucy.g3.iec61131.ide.ui.console.ConsoleFactory;
import com.lucy.g3.iec61131.ide.ui.console.ConsolePanel;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager;
import com.lucy.g3.iec61131.ide.ui.debugger.DebugManager.Status;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager;
import com.lucy.g3.iec61131.ide.ui.prefs.PreferencesManager.Key;
import com.lucy.g3.iec61131.ide.ui.register.RegisterPanel;
import com.lucy.g3.iec61131.ide.ui.source.builder.BuildingManager;
import com.lucy.g3.iec61131.ide.ui.source.editor.RecentItems;
import com.lucy.g3.iec61131.ide.ui.source.editor.RecentItems.RecentItemsObserver;
import com.lucy.g3.iec61131.ide.ui.source.editor.SourceEditorPane;
import com.lucy.g3.iec61131.ide.ui.source.editor.SourceManager;
import com.lucy.g3.iec61131.ide.ui.variable.VariablePanel;
import com.lucy.g3.iec61131.ide.ui.wizard.AutoSchemeImportWizard;

public class IEC61131Window extends JFrame implements IBreakpointPanelCallback{

  public final static String ACTION_KEY_IMPORT_WIZARD = "showImportWizard";
  
  private Logger log = Logger.getLogger(IEC61131Window.class);
  
  
  private final IEC61131Context context;
  private final String originTitle;

  private JMenu recentItemMenu;

  public IEC61131Window(IEC61131Context context) {
    super();
    this.context = context;
    this.context.setParent(this);
    
    initComponents();
    initEventHandling();
    
    this.originTitle = getTitle();
    updateFrameTitle(context.source.getSourceFile());
    
    applyPollingRate(context.prefs.get(Key.KEY_POLLING_RATE));
    context.debugger.stopDebug();
  }
  
  public IEC61131Context getContext() {
    return context;
  }
  
  @Action
  public void showImportWizard() {
    AutoSchemeImportWizard.showWizard(this, new WizardCallback(context));
  }

  private void initEventHandling() {
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(WindowEvent e) {
        getContext().source.getFileMonitor().stop();
      }

      @Override
      public void windowOpened(WindowEvent e) {
        getContext().source.getFileMonitor().start();
      }
      
    });
    
    // Observe status change
    context.debugger.addPropertyChangeListener(
        DebugManager.PROPERTY_CONNECT_STATUS,
        new PropertyChangeListener() {
      
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateGuiStates((Status) evt.getNewValue());
      }
    });
    
    context.prefs.addPreferenceChangeListener(new PreferenceChangeListener() {
      
      @Override
      public void preferenceChange(PreferenceChangeEvent evt) {
        if(Key.KEY_POLLING_RATE.name().equals(evt.getKey())) {
          applyPollingRate(evt.getNewValue());
        }
      }
    });
    
    // Update frame title
    context.source.addPropertyChangeListener(SourceManager.PROPERTY_SOURCE_FILE, 
        new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updateFrameTitle((File) evt.getNewValue());
      }
      
    });
    
    context.recentItems.addObserver(new RecentItemsObserver() {
      
      @Override
      public void onRecentItemChange(RecentItems src) {
        updateRecentMenu(recentItemMenu, src.getItems(), context.source);
      }
    });
  }
  
  private void updateFrameTitle(File newFile) {
     if(newFile == null) { 
       setTitle(originTitle);
     } else {
       setTitle(originTitle +" - "+ newFile.getAbsolutePath());
     }
  }
  
  private void applyPollingRate(String pollingRate) {
    try {
      context.debugger.setPollingRate(Integer.parseInt(pollingRate));
    }catch (Throwable e) {
      log.error("Invalid polling rate:"+pollingRate);
    }
  }
 
  private void updateGuiStates(Status connectionStatus) {
    boolean connected = connectionStatus == Status.CONNECTED;
    
    //toolBarDebug.setVisible(connected);
    //toolBarEdit.setVisible(!connected);
    tfHost.setEditable(!connected);
  }
  

  private void createUIComponents() {
    tfHost = new IPAddressField(false, false);
    ((IPAddressField)tfHost).setCommitsOnValidEdit(false);
    PropertyConnector.connect(context.debugger, DebugManager.PROPERTY_HOST_ADDRESS,
        tfHost, "value").updateProperty2();
    
    editorPane = new SourceEditorPane(context.source);
    statusbar = new StatusBar(context.debugger);

    panelBreakpoint = new BreakpointsPanel(context.breakpoint, this);
    panelRegister = new RegisterPanel(context.register);
    panelVar = new VariablePanel(context.variable);
    panelConsole = new ConsolePanel(ConsoleFactory.getConsole());
    
    AbstractManager manager = context.source;
    btnImportWiz = new JButton(getAction(ACTION_KEY_IMPORT_WIZARD));
    btnNew = new JButton(manager.getAction(SourceManager.ACTION_KEY_NEW));
    btnOpen = new JButton(manager.getAction(SourceManager.ACTION_KEY_OPEN));
    btnSave = new JButton(manager.getAction(SourceManager.ACTION_KEY_SAVE));
    btnSaveas = new JButton(manager.getAction(SourceManager.ACTION_KEY_SAVE_AS));
    btnToggleBP = new JButton(manager.getAction(SourceManager.ACTION_KEY_TOGGLE_BP));
    
    manager = context.builder;
    btnBuild = new JButton(manager.getAction(BuildingManager.ACTION_KEY_BUILD));
    btnClean = new JButton(manager.getAction(BuildingManager.ACTION_KEY_CLEAN));
    btnCancelBuild = new JButton(manager.getAction(BuildingManager.ACTION_KEY_CANCEL_BUILD));
    btnPatchSDP = new JButton(manager.getAction(BuildingManager.ACTION_KEY_PATCH_SDP));
    btnProg = new JButton(manager.getAction(BuildingManager.ACTION_KEY_PROGRAMME));
    
    btnStartDebug = new JButton(context.debugger.getAction(DebugManager.ACTION_KEY_START));
    btnStopDebug = new JButton(context.debugger.getAction(DebugManager.ACTION_KEY_STOP));
    btnPause = new JButton(context.debugger.getAction(DebugManager.ACTION_KEY_PAUSE));
    btnResume = new JButton(context.debugger.getAction(DebugManager.ACTION_KEY_RESUME));
    btnStep = new JButton(context.debugger.getAction(DebugManager.ACTION_KEY_STEPOVER));
    btnPref = new JButton(context.prefs.getAction(PreferencesManager.ACTION_KEY_EDIT));
    
    
    menuFile = createMenuFile();
    menuRun = createMenuRun();
    menuBuild = createMenuBuild();
    menuHelp = createMenuHelp();
    
  }
  private javax.swing.Action getAction(String actionKey) {
    ApplicationActionMap actions =
        Application.getInstance().getContext().getActionManager().getActionMap(getClass(), this);
    return actions.get(actionKey);
  }
  
  private JMenu createMenuFile() {
    JMenu menu = new JMenu();
    
    menu.add(getAction(ACTION_KEY_IMPORT_WIZARD));
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_NEW)).setVisible(false); // Replaced by wizard
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_OPEN)).setVisible(false);// Replaced by wizard
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_READ)).setVisible(false);// Replaced by wizard
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_SAVE));
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_SAVE_AS));
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_CLOSE));
    menu.addSeparator();
    menu.add(recentItemMenu = createRecentItemsMenu());
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_OPEN_DIR));
    menu.addSeparator();
    menu.add(context.prefs.getAction(PreferencesManager.ACTION_KEY_EDIT));
    menu.add(menuItemExit = new JMenuItem());
    return menu;
  }
  
  private JMenu createRecentItemsMenu() {
    JMenu recentItemsMenu = new JMenu("Recent Files");
    List<String> recentFiles = getContext().recentItems.getItems();
    
    updateRecentMenu(recentItemsMenu, recentFiles, getContext().source);
    return recentItemsMenu;
  }

  private void updateRecentMenu(JMenu recentItemsMenu, List<String> recentFiles, final SourceManager sourceMgr) {
    recentItemsMenu.setEnabled(!recentFiles.isEmpty());
    
    recentItemsMenu.removeAll();
    
    JMenuItem openFileMenuItem;
    for (String filePath : recentFiles) {
      final File file = new File(filePath);
      openFileMenuItem = new JMenuItem(file.getName());
      openFileMenuItem .setToolTipText(file.getAbsolutePath());
      openFileMenuItem .addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          sourceMgr.readFile(file);
        }
      });
      
      recentItemsMenu.add(openFileMenuItem);
    }
  }

  private JMenu createMenuRun() {
    JMenu menu = new JMenu();
    menu.add(context.debugger.getAction(DebugManager.ACTION_KEY_START));
    menu.add(context.debugger.getAction(DebugManager.ACTION_KEY_STOP));
    menu.addSeparator();
    
    menu.add(context.debugger.getAction(DebugManager.ACTION_KEY_PAUSE));
    menu.add(context.debugger.getAction(DebugManager.ACTION_KEY_RESUME));
    menu.add(context.debugger.getAction(DebugManager.ACTION_KEY_STEPOVER));
    menu.addSeparator();
    
    menu.add(context.source.getAction(SourceManager.ACTION_KEY_TOGGLE_BP));
    return menu;
  }
  
  private JMenu createMenuBuild() {
    JMenu menu = new JMenu();
    AbstractManager manager = context.builder;
    menu.add(manager.getAction(BuildingManager.ACTION_KEY_BUILD));
    menu.add(manager.getAction(BuildingManager.ACTION_KEY_CANCEL_BUILD));
    menu.add(manager.getAction(BuildingManager.ACTION_KEY_CLEAN));
    menu.addSeparator();
    menu.add(manager.getAction(BuildingManager.ACTION_KEY_PATCH_SDP));
    menu.add(manager.getAction(BuildingManager.ACTION_KEY_PROGRAMME));
    return menu;
  }
  
  
  @org.jdesktop.application.Action
  public void about() {
    new AboutBox(this).setVisible(true);
  }
  
  private JMenu createMenuHelp() {
    ApplicationActionMap actions = Application.getInstance().getContext().getActionMap(this);
    JMenu menu = new JMenu();
    menu.add(actions.get("about"));
    return menu;
  }


  private void close() {
    int ret = JOptionPane.showConfirmDialog(this, "Do you want to exit IEC-61131 IDE?", 
        "Exit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    if(ret == JOptionPane.YES_OPTION)
      dispose();
  }

  
  private void thisWindowClosing(WindowEvent e) {
    close();
  }

  private void thisWindowClosed(WindowEvent e) {
    context.debugger.stopDebug();
    window = null;
  }

  private void cboxAutoScrollActionPerformed(ActionEvent e) {
    editorPane.setAutoScrollEnabled(cboxAutoScroll.isSelected());
  }

  @Override
  public void scrollToLine(int line) {
    editorPane.scrollToLine(line, true);
  }
  
  private void menuItemExitActionPerformed(ActionEvent e) {
    close();
  }
  
  public void setAllowChangeHost(boolean allowChangeHost) {
    tfHost.setEnabled(allowChangeHost);
  }
  
  public boolean isAllowChangeHost() {
    return tfHost.isEnabled();
  }
  
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    toolBarMain = new JToolBar();
    toolBarEdit = new JToolBar();
    toolBarBuild = new JToolBar();
    toolBarDebug = new JToolBar();
    cboxAutoScroll = new JCheckBox();
    toolBarPref = new JToolBar();
    JLabel label1 = new JLabel();
    splitPane1 = new JSplitPane();
    splitPaneRight = new JSplitPane();
    tabbedPaneWatching = new JTabbedPane();
    tabbedPaneConsole = new JTabbedPane();
    menuBar = new JMenuBar();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setTitle("Gemini 3 IEC-61131 IDE");
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(WindowEvent e) {
        thisWindowClosed(e);
      }

      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setPreferredSize(new Dimension(1024, 768));
        contentPanel.setLayout(new BorderLayout());

        //======== toolBarMain ========
        {
          toolBarMain.setFloatable(false);

          //======== toolBarEdit ========
          {
            toolBarEdit.setFloatable(false);

            //---- btnOpen ----
            btnOpen.setHideActionText(true);
            btnOpen.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/fldr_obj.gif")));
            btnOpen.setVisible(false);
            toolBarEdit.add(btnOpen);

            //---- btnNew ----
            btnNew.setHideActionText(true);
            btnNew.setIcon(new ImageIcon(getClass().getResource(
                "/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/new_untitled_text_file.gif")));
            btnNew.setVisible(false);
            toolBarEdit.add(btnNew);

            //---- btnImportWiz ----
            btnImportWiz.setIcon(
                new ImageIcon(getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/import_wiz.gif")));
            btnImportWiz.setHideActionText(true);
            toolBarEdit.add(btnImportWiz);

            //---- btnSave ----
            btnSave.setHideActionText(true);
            btnSave.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/save_edit.gif")));
            toolBarEdit.add(btnSave);

            //---- btnSaveas ----
            btnSaveas.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/saveas_edit.gif")));
            btnSaveas.setHideActionText(true);
            toolBarEdit.add(btnSaveas);
            toolBarEdit.addSeparator();
          }
          toolBarMain.add(toolBarEdit);

          //======== toolBarBuild ========
          {
            toolBarBuild.setFloatable(false);

            //---- btnBuild ----
            btnBuild.setHideActionText(true);
            btnBuild.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/builder/resources/icons/build.png")));
            toolBarBuild.add(btnBuild);

            //---- btnCancelBuild ----
            btnCancelBuild.setHideActionText(true);
            btnCancelBuild.setIcon(new ImageIcon(getClass().getResource(
                "/com/lucy/g3/iec61131/ide/ui/source/builder/resources/icons/progress_stop.gif")));
            toolBarBuild.add(btnCancelBuild);

            //---- btnClean ----
            btnClean.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/builder/resources/icons/trash.gif")));
            btnClean.setHideActionText(true);
            toolBarBuild.add(btnClean);
            toolBarBuild.addSeparator();

            //---- btnPatchSDP ----
            btnPatchSDP.setHideActionText(true);
            btnPatchSDP.setIcon(new ImageIcon(getClass().getResource(
                "/com/lucy/g3/iec61131/ide/ui/source/builder/resources/icons/exportzip_wiz.gif")));
            toolBarBuild.add(btnPatchSDP);

            //---- btnProg ----
            btnProg.setHideActionText(true);
            btnProg.setIcon(new ImageIcon(getClass().getResource(
                "/com/lucy/g3/iec61131/ide/ui/source/builder/resources/icons/correction_move.gif")));
            toolBarBuild.add(btnProg);
            toolBarBuild.addSeparator();
          }
          toolBarMain.add(toolBarBuild);

          //======== toolBarDebug ========
          {
            toolBarDebug.setFloatable(false);

            //---- btnStartDebug ----
            btnStartDebug.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/debugger/resources/icons/debug_start.gif")));
            btnStartDebug.setHideActionText(true);
            toolBarDebug.add(btnStartDebug);

            //---- btnStopDebug ----
            btnStopDebug.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/debugger/resources/icons/debug_stop.gif")));
            btnStopDebug.setHideActionText(true);
            toolBarDebug.add(btnStopDebug);

            //---- btnResume ----
            btnResume.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/debugger/resources/icons/resume_co.gif")));
            btnResume.setHideActionText(true);
            toolBarDebug.add(btnResume);

            //---- btnPause ----
            btnPause.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/debugger/resources/icons/suspend_co.gif")));
            btnPause.setHideActionText(true);
            toolBarDebug.add(btnPause);

            //---- btnStep ----
            btnStep.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/debugger/resources/icons/stepover_co.gif")));
            btnStep.setHideActionText(true);
            toolBarDebug.add(btnStep);

            //---- btnToggleBP ----
            btnToggleBP.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/brkp_obj.gif")));
            btnToggleBP.setHideActionText(true);
            toolBarDebug.add(btnToggleBP);

            //---- cboxAutoScroll ----
            cboxAutoScroll.setToolTipText(
                "Enable/disable srcolling the view down to the highlighted line automatically.");
            cboxAutoScroll.setText("Auto Scroll");
            cboxAutoScroll.setSelected(true);
            cboxAutoScroll.addActionListener(new ActionListener() {

              @Override
              public void actionPerformed(ActionEvent e) {
                cboxAutoScrollActionPerformed(e);
              }
            });
            toolBarDebug.add(cboxAutoScroll);
            toolBarDebug.addSeparator();
          }
          toolBarMain.add(toolBarDebug);

          //======== toolBarPref ========
          {
            toolBarPref.setFloatable(false);

            //---- label1 ----
            label1.setText("Host:  ");
            toolBarPref.add(label1);

            //---- tfHost ----
            tfHost.setColumns(10);
            tfHost.setMaximumSize(new Dimension(100, 16));
            toolBarPref.add(tfHost);

            //---- btnPref ----
            btnPref.setIcon(new ImageIcon(
                getClass().getResource("/com/lucy/g3/iec61131/ide/ui/prefs/resources/icons/prop_ps.gif")));
            btnPref.setHideActionText(true);
            toolBarPref.add(btnPref);
          }
          toolBarMain.add(toolBarPref);
        }
        contentPanel.add(toolBarMain, BorderLayout.PAGE_START);

        //======== splitPane1 ========
        {
          splitPane1.setResizeWeight(0.5);
          splitPane1.setDividerLocation(650);
          splitPane1.setLeftComponent(editorPane);

          //======== splitPaneRight ========
          {
            splitPaneRight.setOrientation(JSplitPane.VERTICAL_SPLIT);
            splitPaneRight.setResizeWeight(0.8);

            //======== tabbedPaneWatching ========
            {
              tabbedPaneWatching.addTab("Breakpoints",
                  new ImageIcon(
                      getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/breakpoint_view.gif")),
                  panelBreakpoint);
              tabbedPaneWatching.addTab("Registers",
                  new ImageIcon(
                      getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/genericregister_obj.gif")),
                  panelRegister);
              tabbedPaneWatching.addTab("Variables",
                  new ImageIcon(
                      getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/variable_tab.gif")),
                  panelVar);
            }
            splitPaneRight.setTopComponent(tabbedPaneWatching);

            //======== tabbedPaneConsole ========
            {
              tabbedPaneConsole.addTab("Console",
                  new ImageIcon(
                      getClass().getResource("/com/lucy/g3/iec61131/ide/ui/console/resources/icons/monitor_obj.gif")),
                  panelConsole);
            }
            splitPaneRight.setBottomComponent(tabbedPaneConsole);
          }
          splitPane1.setRightComponent(splitPaneRight);
        }
        contentPanel.add(splitPane1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      dialogPane.add(statusbar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    //======== menuBar ========
    {

      //======== menuFile ========
      {
        menuFile.setText("File");
        menuFile.setMnemonic('F');
      }
      menuBar.add(menuFile);

      //======== menuRun ========
      {
        menuRun.setText("Debug");
        menuRun.setMnemonic('D');
      }
      menuBar.add(menuRun);

      //======== menuBuild ========
      {
        menuBuild.setText("Build");
        menuBuild.setMnemonic('B');
      }
      menuBar.add(menuBuild);

      //======== menuHelp ========
      {
        menuHelp.setText("Help");
        menuHelp.setMnemonic('H');
      }
      menuBar.add(menuHelp);
    }
    contentPane.add(menuBar, BorderLayout.NORTH);

    //---- menuItemExit ----
    menuItemExit.setText("Exit");
    menuItemExit.setIcon(
        new ImageIcon(getClass().getResource("/com/lucy/g3/iec61131/ide/ui/resources/icons/checkout_action.gif")));
    menuItemExit.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        menuItemExitActionPerformed(e);
      }
    });

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  
    /*Set window icons*/
    ArrayList<Image> images = new ArrayList<>();
    images.add(new ImageIcon(getClass().getResource(
        "/com/lucy/g3/iec61131/ide/ui/resources/icons/productIcon32.png")).getImage()); // 16*16
//    images.add(new ImageIcon(getClass().getResource(
//        "/com/lucy/g3/configtool/iec61131/ide/resources/icons/debug_32.png")).getImage()); // 32*32
    setIconImages(images);
  
    cboxAutoScroll.setSelected(editorPane.isAutoScrollEnabled());
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JToolBar toolBarMain;
  private JToolBar toolBarEdit;
  private JButton btnOpen;
  private JButton btnNew;
  private JButton btnImportWiz;
  private JButton btnSave;
  private JButton btnSaveas;
  private JToolBar toolBarBuild;
  private JButton btnBuild;
  private JButton btnCancelBuild;
  private JButton btnClean;
  private JButton btnPatchSDP;
  private JButton btnProg;
  private JToolBar toolBarDebug;
  private JButton btnStartDebug;
  private JButton btnStopDebug;
  private JButton btnResume;
  private JButton btnPause;
  private JButton btnStep;
  private JButton btnToggleBP;
  private JCheckBox cboxAutoScroll;
  private JToolBar toolBarPref;
  private JTextField tfHost;
  private JButton btnPref;
  private JSplitPane splitPane1;
  private SourceEditorPane editorPane;
  private JSplitPane splitPaneRight;
  private JTabbedPane tabbedPaneWatching;
  private BreakpointsPanel panelBreakpoint;
  private RegisterPanel panelRegister;
  private VariablePanel panelVar;
  private JTabbedPane tabbedPaneConsole;
  private ConsolePanel panelConsole;
  private JPanel statusbar;
  private JMenuBar menuBar;
  private JMenu menuFile;
  private JMenu menuRun;
  private JMenu menuBuild;
  private JMenu menuHelp;
  private JMenuItem menuItemExit;
  // JFormDesigner - End of variables declaration //GEN-END:variables


  private static IEC61131Window window;
  
  public static IEC61131Window launch(Component parent, String rtuIPAddress, File ilSourceFile, boolean autoConnect) {
    if(window == null || !window.isDisplayable()) {
      IEC61131Context context = new IEC61131Context();
      if(ilSourceFile != null)
      context.source.readFile(ilSourceFile);
      context.debugger.setHostAddress(rtuIPAddress);
      
      window = new IEC61131Window(context);
      window.setLocationRelativeTo(parent);
      window.setVisible(true);
      window.setAllowChangeHost(false);
      window.showImportWizard();
    }
    
    if(autoConnect) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          window.context.debugger.startDebug();
        }
      });
    }
    return window;
  }

}
