
package com.lucy.g3.iec61131.ide.ui.source.editor;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

public interface ISourceEditor {

  /** Writes the content of this editor to a file writer. */
  void write(Writer out) throws IOException;

  int getCaretLineNumber();

  String getCaretLineContent();

  boolean loadFile(File sourceFile);

}

