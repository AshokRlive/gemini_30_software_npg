
#include "AutomationSchemeLib.h"

void runGeneratedCode(){
    int cr = 0;
    int true = 1;
    int false = 0;

    //vars
    int Off=0;
    int Local=1;
    int Remote=2;
    int SWOpen=1;
    int SWClosed=1;

    //input vars
    int SW1Open;
    int SW1Closed;
    int SW2Open;
    int SW2Closed;
    int OffLocalRemote;
    int Fail;
    int FailDelay;
    int SwitchingDelay;
    int LimitSwitchDelay;

    //initialize constants
    SW1Open=GetConstant(-100);
    SW1Closed=GetConstant(-99);
    SW2Open=GetConstant(-98);
    SW2Closed=GetConstant(-97);
    OffLocalRemote=GetConstant(-96);
    Fail=GetConstant(-95);
    FailDelay=GetConstant(0);
    SwitchingDelay=GetConstant(1);
    LimitSwitchDelay=GetConstant(2);

    DelayMs(1000);


    SetLineNum(36);
    SetCurrentRegister(cr);
    DelayMs(100);
    goto start;//JMP start//36

start:

    SetLineNum(38);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifSW1ClosedThenCheckSW2;//JMP ifSW1ClosedThenCheckSW2//38

ifSW1ClosedThenCheckSW2:

    SetLineNum(41);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(0);//LD SW1Open//41

    SetLineNum(42);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWOpen){//EQ INT#SWOpen//42

        SetLineNum(43);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//43
    }

    SetLineNum(44);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(1);//(*Repeat*)                             LD SW1Closed//44

    SetLineNum(45);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWClosed){//EQ INT#SWClosed//45

        SetLineNum(46);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto ifSW2OpenThenCheckOLR;//JMP ifSW2OpenThenCheckOLR//46
    }

    SetLineNum(47);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto start;//JMP start//47

ifSW2OpenThenCheckOLR:

    SetLineNum(49);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(2);//LD SW2Open//49

    SetLineNum(50);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWOpen){//EQ INT#SWOpen//50

        SetLineNum(51);
        SetCurrentRegister(cr);
        DelayMs(0);
        cr=GetInput(3);//LD SW2Closed//51
    }

    SetLineNum(52);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWClosed){//EQ INT#SWClosed//52

        SetLineNum(53);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//53
    }

    SetLineNum(54);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifRemoteCheckFail;//JMP ifRemoteCheckFail//54

ifRemoteCheckFail:

    SetLineNum(56);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(4);//LD OffLocalRemote//56

    SetLineNum(57);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==Off){//EQ INT#Off//57

        SetLineNum(58);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//58
    }

    SetLineNum(59);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==Local){//EQ INT#Local//59

        SetLineNum(60);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//60
    }

    SetLineNum(61);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto checkFail;//JMP checkFail//61

checkFail:

    SetLineNum(65);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifFailThenSwitch;//JMP ifFailThenSwitch//65

ifFailThenSwitch:

    SetLineNum(67);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(5);//LD Fail//67

    SetLineNum(68);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==0){//EQ BOOL#FALSE//68

        SetLineNum(69);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//69
    }

    SetLineNum(70);
    SetCurrentRegister(cr);
    DelayMs(0);

    DelayMs(GetConstant(0));//(*No failure, go back to start*)                                  DEL FailDelay//70

    SetLineNum(71);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifRemoteThenOpenSW1;//JMP goto ifRemoteThenOpenSW1//71

ifRemoteThenOpenSW1:

    SetLineNum(75);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(4);//LD OffLocalRemote//75

    SetLineNum(76);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==Off){//EQ INT#Off//76

        SetLineNum(77);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//77
    }

    SetLineNum(78);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==Local){//EQ INT#Local//78

        SetLineNum(79);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//79
    }

    SetLineNum(80);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifSW1ClosedThenOpenSW1;//JMP ifSW1ClosedThenOpenSW1//80

ifSW1ClosedThenOpenSW1:

    SetLineNum(82);
    SetCurrentRegister(cr);
    DelayMs(0);

    DelayMs(GetConstant(1));//DEL SwitchingDelay//82

    SetLineNum(83);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(1);//LD SW1Closed//83

    SetLineNum(84);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWClosed){//EQ INT#SWClosed//84

        SetLineNum(85);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto openSW1;//JMP openSW1//85
    }

    SetLineNum(86);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifSW1ClosedThenOpenSW1;//JMP ifSW1ClosedThenOpenSW1//86

openSW1:

    SetLineNum(88);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=false;//LD FALSE//88

    SetLineNum(89);
    SetCurrentRegister(cr);
    DelayMs(0);
    Operate(0, cr);//ST SW1//89

    SetLineNum(90);
    SetCurrentRegister(cr);
    DelayMs(0);
   goto ifSW1OpenThenClose;//JMP ifSW1OpenThenClose//90


ifSW1OpenThenClose:

    SetLineNum(92);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(0);//LD SW1Open//92

    SetLineNum(93);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr!=SWOpen){//EQN INT#SWOpen//93

        SetLineNum(94);
        SetCurrentRegister(cr);
        DelayMs(0);
        //isSW1Open();//JMP isSW1Open//94
    }

    SetLineNum(95);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(1);//LD SW1Closed//95

    SetLineNum(96);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWClosed){//EQ INT#SWClosed//96

        SetLineNum(97);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//97
    }

    SetLineNum(98);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(2);//LD SW2Open//98

    SetLineNum(99);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWOpen){//EQ INT#SWOpen//99

        SetLineNum(100);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto closeSW2;//JMP closeSW2//100
    }
    SetLineNum(101);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto start;//JMP start//101

closeSW2:

    SetLineNum(103);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=true;//LD TRUE//103

    SetLineNum(104);
    SetCurrentRegister(cr);
    DelayMs(0);
    Operate(1, cr);//ST SW2//104

    SetLineNum(105);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifSW2GoBackToStart;//JMP ifSW2GoBackToStart//105

ifSW2GoBackToStart:

    SetLineNum(107);
    SetCurrentRegister(cr);
    DelayMs(0);
    cr=GetInput(3);//LD SW2Closed//107

    SetLineNum(108);
    SetCurrentRegister(cr);
    DelayMs(0);
    if(cr==SWClosed){//EQ INT#SWClosed//108

        SetLineNum(109);
        SetCurrentRegister(cr);
        DelayMs(0);
        goto start;//JMP start//109
    }

    SetLineNum(110);
    SetCurrentRegister(cr);
    DelayMs(0);
    goto ifSW2GoBackToStart;//(*SW2 is closed, go back to start*)                                  JMP ifSW2GoBackToStart//110
}

