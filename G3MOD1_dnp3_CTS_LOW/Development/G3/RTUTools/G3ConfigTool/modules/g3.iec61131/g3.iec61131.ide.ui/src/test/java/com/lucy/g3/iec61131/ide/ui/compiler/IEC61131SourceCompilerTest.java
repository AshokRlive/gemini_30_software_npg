/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.compiler;

import java.io.IOException;

import com.lucy.g3.iec61131.ide.ui.compiler.IEC61131SourceCompiler;
import com.lucy.g3.iec61131.ide.ui.compiler.IOutputHandler;



/**
 *
 */
public class IEC61131SourceCompilerTest implements IOutputHandler{

  //@org.junit.Test //Manual test.
  public void test() throws IOException, InterruptedException {
    String target = "E:\\temp\\autoscheme\\target\\2\\";
    String jarPath  = "C:\\dev_env\\iec61131.jar";
    
    IEC61131SourceCompiler compiler  = new IEC61131SourceCompiler(jarPath, this);
    int ret = compiler.compile("E:\\temp\\autoscheme\\AutoChangeover.il", target);
    System.out.println("Return:"+ret);
  }

  @Override
  public void append(String output) {
    System.out.println(output);
  }

  @Override
  public void appendErr(String msg) {
    System.out.println(msg);

  }

  @Override
  public void appendSuccess(String msg) {
    System.out.println(msg);

  }
}

