/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.register;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;


/**
 *
 */
public class RegisterManager {
  public final static int REGISTER_CR = 0;

  private final MultiListSelectionAdapter<Register> regSelectModel;
  private final ArrayListModel<Register> regListModel;
  private final MultiSelectionInList<Register> regSelectList;
  private final RegisterTableModel regTableModel;

  @SuppressWarnings("unchecked")
  public RegisterManager() {
    this.regListModel = new ArrayListModel<>();
    this.regSelectList = new MultiSelectionInList<>(regListModel);
    this.regSelectModel = new MultiListSelectionAdapter<>(regSelectList);
    this.regTableModel = new RegisterTableModel(regListModel);
  }

  public TableModel getTableModel() {
    return regTableModel;
  }
  
  public ListSelectionModel getListSelectionModel() {
    return regSelectModel;
  }
  
  @SuppressWarnings("unchecked")
  public ListModel<Register> getListModel() {
    return regListModel;
  }
  
  public void setRegister(RegisterID id, int value) {
    Register reg = new Register(id);
    int index = regListModel.indexOf(reg);
    
    if(index < 0) {
      /*Not found, add a new one*/
      regListModel.add(reg);
      index = regListModel.getSize() - 1;
    } else {
      /*Found.*/
      reg = regListModel.get(index);
    }
    
    reg.setValue(value);
    regTableModel.fireTableCellUpdated(index, RegisterTableModel.COL_VALUE);
  }
  
  private static class RegisterTableModel extends AbstractTableAdapter<Register> {

    private final static int COL_REG = 0;
    private final static int COL_VALUE = 1;
    
    private final static String COL_NAMES[] = {"Register","value"};
    
    public RegisterTableModel(ListModel<Register> bplistModel) {
      super(bplistModel, COL_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Register bp = getRow(rowIndex);
      switch(columnIndex) {
        case COL_REG:
          return bp.getID().name();
        case COL_VALUE:
          return bp.getValue();
      }
      
      return null;
    }
    
  }
}

