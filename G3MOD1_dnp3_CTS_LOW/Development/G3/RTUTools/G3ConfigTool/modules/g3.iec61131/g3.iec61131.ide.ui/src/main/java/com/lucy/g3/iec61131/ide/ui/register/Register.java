/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.register;


class Register {
  private final RegisterID id;
  private int value;
  
  
  public Register(RegisterID id) {
    super();
    if(id == null)
      throw new IllegalArgumentException("id must not be null");
    
    this.id = id;
  }
  

  public RegisterID getID() {
    return id;
  }

  
  void setValue(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Register other = (Register) obj;
    if (id != other.id)
      return false;
    return true;
  }
  
  
  
}

