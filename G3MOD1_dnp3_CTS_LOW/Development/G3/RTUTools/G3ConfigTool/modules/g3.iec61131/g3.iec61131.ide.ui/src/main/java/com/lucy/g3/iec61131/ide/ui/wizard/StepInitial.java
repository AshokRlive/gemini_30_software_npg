/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.awt.BorderLayout;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

import org.netbeans.spi.wizard.WizardPage;
class StepInitial extends WizardPage {
  public final static String ID = "ImportOptionPage"; 
  public final static String KEY_CREATE_NEW = "createNew"; 
  public final static String KEY_OPEN_LOCAL = "openLocal"; 
  public final static String KEY_READ_RTU = "readRTU"; 
  
  public StepInitial() {
    super(ID, "Choose a method");
    setLongDescription("Choose a method to import "+ AutoSchemeImportWizard.AUTO_SCHEME_STR);
    initComponents();
    initComponentsName();
  }

  /**
   * Set the name for listening to the change.
   */
  private void initComponentsName() {
    radioButton1.setName(KEY_CREATE_NEW);
    radioButton2.setName(KEY_OPEN_LOCAL);
    radioButton3.setName(KEY_READ_RTU);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    radioButton1 = new JRadioButton();
    label1 = new JLabel();
    radioButton2 = new JRadioButton();
    label2 = new JLabel();
    radioButton3 = new JRadioButton();
    label3 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== panel1 ========
    {
      panel1.setLayout(new FormLayout(
          "[15dlu,default], $lcgap, default",
          "3*(default, $lgap), default"));

      //---- radioButton1 ----
      radioButton1.setSelected(true);
      radioButton1.setOpaque(false);
      panel1.add(radioButton1, CC.xywh(1, 1, 3, 1));

      //---- label1 ----
      label1.setText("Create a new Automation Scheme");
      label1.setIcon(new ImageIcon(getClass().getResource(
          "/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/new_untitled_text_file.gif")));
      panel1.add(label1, CC.xy(3, 1));

      //---- radioButton2 ----
      radioButton2.setOpaque(false);
      panel1.add(radioButton2, CC.xywh(1, 3, 3, 1));

      //---- label2 ----
      label2.setText("Import an existing Automation Scheme from local disk");
      label2.setIcon(new ImageIcon(
          getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/source.gif")));
      panel1.add(label2, CC.xy(3, 3));

      //---- radioButton3 ----
      radioButton3.setOpaque(false);
      panel1.add(radioButton3, CC.xywh(1, 5, 3, 1));

      //---- label3 ----
      label3.setText("Read Automation Scheme from RTU");
      label3.setIcon(
          new ImageIcon(getClass().getResource("/com/lucy/g3/iec61131/ide/ui/source/editor/resources/icons/read.gif")));
      panel1.add(label3, CC.xy(3, 5));
    }
    add(panel1, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioButton1);
    buttonGroup1.add(radioButton2);
    buttonGroup1.add(radioButton3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private JRadioButton radioButton1;
  private JLabel label1;
  private JRadioButton radioButton2;
  private JLabel label2;
  private JRadioButton radioButton3;
  private JLabel label3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
