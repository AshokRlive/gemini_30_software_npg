/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.source.builder;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

/**
 * For storing the parameters for building.
 */
public class BuildingParameters {
  public final static String KEY_IL_COMPILER_PATH = "IL compiler path";
  public final static String KEY_CROSS_COMPILER_PATH = "Cross compiler path";
  public final static String KEY_CROSS_COMPILER_PREFIX = "Cross compiler prefix";
  public final static String KEY_SOURCE_PATH = "Source path";
  public final static String KEY_TARGET_DIR_PATH = "Target directory path";
  
  private final static String[] MADATORY_PARAMS = {
    KEY_IL_COMPILER_PATH,
    KEY_CROSS_COMPILER_PATH,
    KEY_CROSS_COMPILER_PREFIX,
    KEY_SOURCE_PATH,
    KEY_TARGET_DIR_PATH
  };
  
  
  private HashMap<String, Object> paramMap = new HashMap<>();
  
  public void put(String key, Object value) {
    paramMap.put(key,value);
  }
  
  public Object get(String key) {
    return paramMap.get(key);
  }

  public Map<String, Object> getMap() {
    return new HashMap<String, Object>(paramMap);
  }
  
  public String getTargetDir() {
    return (String) paramMap.get(KEY_TARGET_DIR_PATH);
  }
  
  public String getSource() {
    return (String) paramMap.get(KEY_SOURCE_PATH);
  }
  
  public String getILCompilerPath() {
    return (String) paramMap.get(KEY_IL_COMPILER_PATH);
  }
  
  public String getCrossCompilerPath() {
    return (String) paramMap.get(KEY_CROSS_COMPILER_PATH);
  }

  public String getCrossCompilerPrefix() {
    return (String) paramMap.get(KEY_CROSS_COMPILER_PREFIX);
  }
  
  public void checkParams() throws InvalidParameterException {
    for (int i = 0; i < MADATORY_PARAMS.length; i++) {
      if(paramMap.get(MADATORY_PARAMS[i]) == null) {
        throw new InvalidParameterException("Missing parameter: " + MADATORY_PARAMS[i]);
      }
    }
  }

}
