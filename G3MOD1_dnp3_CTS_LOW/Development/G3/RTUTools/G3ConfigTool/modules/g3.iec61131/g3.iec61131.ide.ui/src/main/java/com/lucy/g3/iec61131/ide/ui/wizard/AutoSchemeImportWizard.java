/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.wizard;

import java.util.Map;

import javax.swing.JFrame;

import org.jdesktop.swingx.util.WindowUtils;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardBranchController;
import org.netbeans.spi.wizard.WizardPage;

import com.lucy.g3.gui.common.wizards.WizardUtils;


/**
 * A wizard for importing IL from different places. 
 */
public class AutoSchemeImportWizard extends WizardBranchController{
  private static final int WIDTH  = 650;
  private static final int HEIGHT = 450;
  
  public final static int OPTION_CREATE_NEW = 0;
  public final static int OPTION_OPEN_LOCAL = 1;
  public final static int OPTION_READ_RTU = 2;
  
  static final String AUTO_SCHEME_STR ="Automation Scheme";
  
  private final IImportCallback callback;
  
  AutoSchemeImportWizard(IImportCallback callback) {
    super("Import " + AUTO_SCHEME_STR, new WizardPage[]{new StepInitial()});
    this.callback = callback;
  }


  @Override
  protected Wizard getWizardForStep(String step, Map settings) {
    settings.put(IImportCallback.KEY_CALLBACK, callback);
    
    if(StepInitial.ID.equals(step)) {
      Boolean createNew = (Boolean) settings.get(StepInitial.KEY_CREATE_NEW);
      Boolean openLocal = (Boolean) settings.get(StepInitial.KEY_OPEN_LOCAL);
      Boolean readRTU = (Boolean) settings.get(StepInitial.KEY_READ_RTU);
      
      if(createNew == Boolean.TRUE) {
        return WizardPage.createWizard(
            new WizardPage[]{new StepCreateNew()},
            new ResultProducer(callback, OPTION_CREATE_NEW));
        
      } else if(openLocal == Boolean.TRUE) {
        return WizardPage.createWizard(
            new WizardPage[]{new StepImportLocalScheme()}, 
            new ResultProducer(callback, OPTION_OPEN_LOCAL));
        
      } else if(readRTU == Boolean.TRUE) {
        return WizardPage.createWizard(
            new WizardPage[]{new StepReadSchemeFromRTU()}, 
            new ResultProducer(callback, OPTION_READ_RTU));
        
      } 
    }
    
    return null;
  }

  public static void showWizard(JFrame parent, IImportCallback callback) {
    Wizard wiz = new AutoSchemeImportWizard(callback).createWizard();
    WizardDisplayer.showWizard(wiz, WizardUtils.createRect(parent, WIDTH, HEIGHT));
  }
}

