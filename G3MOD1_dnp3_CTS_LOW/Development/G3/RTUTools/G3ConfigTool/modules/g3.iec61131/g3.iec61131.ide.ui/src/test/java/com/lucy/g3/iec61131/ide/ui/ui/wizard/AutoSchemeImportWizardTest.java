/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.ui.wizard;

import java.io.File;

import com.lucy.g3.iec61131.ide.ui.wizard.AutoSchemeImportWizard;
import com.lucy.g3.iec61131.ide.ui.wizard.IImportCallback;


/**
 *
 */
public class AutoSchemeImportWizardTest {

  public static void main(String[] args) {
    AutoSchemeImportWizard.showWizard(null, new DummyCallback());
  }
  
  
  private static  class DummyCallback implements IImportCallback {
    int index;
    @Override
    public String[] readSchemeNamesFromRTU() throws Exception{
      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
      }
      index++;
      if(true)
        throw new Exception("network error");
      return new String[]{index+"-AutoChangeOver.il",index+"-AutoOpen.il",index+"-CustomScheme.il"};
    }
    

    @Override
    public File[] getRecentSchemeFiles() {
      return new File[]{new File("recentA.il"),new File("recentB.il"),new File("recentC.il")};
    }

    @Override
    public File getSchemeFileDir() {
      return new File(".").getParentFile();
    }

    @Override
    public File readSchemeFromRTU(String schemeName, File destDir) throws Exception {
      try {
        Thread.sleep(3000);
        return new File(schemeName);
      } catch (InterruptedException e) {
        e.printStackTrace();
        return null;
      }
    }

    @Override
    public void openFile(File file) {
      System.out.println("open file: "+file);
      
    }


    @Override
    public String readRunningSchemeNameFromRTU() throws Exception {
      return null;
    }
    
  }
}

