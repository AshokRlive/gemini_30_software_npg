/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.compiler;

/**
 * The Interface IOutputHandler.
 */
public interface IOutputHandler {

  void appendErr(String msg);

  void appendSuccess(String msg);

  void append(String output);
}