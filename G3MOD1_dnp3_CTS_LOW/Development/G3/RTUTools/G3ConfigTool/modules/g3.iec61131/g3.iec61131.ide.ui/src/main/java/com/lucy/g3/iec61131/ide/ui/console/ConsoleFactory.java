/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.console;

import com.lucy.g3.gui.common.widgets.console.IConsole;

/**
 *
 */
public class ConsoleFactory {

  private final static Console INSTANCE = new Console();


  public static IConsole getConsole() {
    return INSTANCE;
  }

  private ConsoleFactory() {
  }
}
