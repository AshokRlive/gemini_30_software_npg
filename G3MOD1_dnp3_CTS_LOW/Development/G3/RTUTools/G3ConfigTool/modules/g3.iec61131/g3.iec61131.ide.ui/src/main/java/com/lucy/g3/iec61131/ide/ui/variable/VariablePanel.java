/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.variable;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DBG_VAR;
public class VariablePanel extends JPanel {
  private final VariableManager manager;
  /**
   * Private constructor required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private VariablePanel() {
    this.manager = new VariableManager();
    initComponents();
  }
  
  public VariablePanel(VariableManager manager) {
    this.manager = manager;
    initComponents();
  }

  private void createUIComponents() {
    this.tableRegisters = new JTable(manager.getVarDataModel());
    Bindings.bind(tableRegisters, manager.getVarListModel(), manager.getVarSelectModel());
    
    combType = new JComboBox<DBG_VAR>(DBG_VAR.values());
    combType.setSelectedItem(DBG_VAR.DBG_VAR_INPUT);
  }

  private void menuItemAddActionPerformed(ActionEvent e) {
    int ret = JOptionPane.showConfirmDialog(this, panelAdd, "Adding Variables", JOptionPane.OK_CANCEL_OPTION);
    if(ret == JOptionPane.OK_OPTION) {
      int id = (int) ftfIndex.getValue();
      DBG_VAR type = (DBG_VAR) combType.getSelectedItem();
      String name = tfName.getText();
      manager.addVariable(new Variable(name, type, (byte) id));
    }
  }

  private void menuItemRemoveActionPerformed(ActionEvent e) {
    List<Variable> selections = manager.getSelectedVars();
    manager.removeVariable(selections);
  }

  private void menuItemRemoveAllActionPerformed(ActionEvent e) {
    manager.clear();
  }


  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();
    popupMenu1 = new JPopupMenu();
    menuItemAdd = new JMenuItem();
    menuItemRemove = new JMenuItem();
    menuItemRemoveAll = new JMenuItem();
    panelAdd = new JPanel();
    label4 = new JLabel();
    label1 = new JLabel();
    label2 = new JLabel();
    ftfIndex = new JSpinner();
    label3 = new JLabel();
    tfName = new JTextField();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {

      //---- tableRegisters ----
      tableRegisters.setFillsViewportHeight(true);
      tableRegisters.setComponentPopupMenu(popupMenu1);
      scrollPane1.setViewportView(tableRegisters);
    }
    add(scrollPane1, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {

      //---- menuItemAdd ----
      menuItemAdd.setText("Add Variables");
      menuItemAdd.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemAddActionPerformed(e);
        }
      });
      popupMenu1.add(menuItemAdd);

      //---- menuItemRemove ----
      menuItemRemove.setText("Remove Variables");
      menuItemRemove.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemRemoveActionPerformed(e);
        }
      });
      popupMenu1.add(menuItemRemove);

      //---- menuItemRemoveAll ----
      menuItemRemoveAll.setText("Remove All");
      menuItemRemoveAll.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          menuItemRemoveAllActionPerformed(e);
        }
      });
      popupMenu1.add(menuItemRemoveAll);
    }

    //======== panelAdd ========
    {
      panelAdd.setLayout(new FormLayout(
          "default, $lcgap, default:grow",
          "3*(default, $lgap), default"));

      //---- label4 ----
      label4.setText("Adding variables:");
      panelAdd.add(label4, CC.xywh(1, 1, 3, 1));

      //---- label1 ----
      label1.setText("Variable Type:");
      panelAdd.add(label1, CC.xy(1, 3));
      panelAdd.add(combType, CC.xy(3, 3));

      //---- label2 ----
      label2.setText("Variable Index:");
      panelAdd.add(label2, CC.xy(1, 5));

      //---- ftfIndex ----
      ftfIndex.setModel(new SpinnerNumberModel(0, 0, 100, 1));
      panelAdd.add(ftfIndex, CC.xy(3, 5));

      //---- label3 ----
      label3.setText("Name:");
      panelAdd.add(label3, CC.xy(1, 7));
      panelAdd.add(tfName, CC.xy(3, 7));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JTable tableRegisters;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItemAdd;
  private JMenuItem menuItemRemove;
  private JMenuItem menuItemRemoveAll;
  private JPanel panelAdd;
  private JLabel label4;
  private JLabel label1;
  private JComboBox combType;
  private JLabel label2;
  private JSpinner ftfIndex;
  private JLabel label3;
  private JTextField tfName;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
