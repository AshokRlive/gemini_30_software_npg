/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.console;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.apache.log4j.Logger;

import com.lucy.g3.gui.common.widgets.console.IConsole;
import com.lucy.g3.gui.common.widgets.console.SimpleConsole;
public class ConsolePanel extends JPanel {
  private Logger log = Logger.getLogger(ConsolePanel.class);
  private final IConsole console;
  
  /**
   * Required by JFormDesigner.
   */
  @SuppressWarnings("unused")
  private ConsolePanel() {
    this.console = new SimpleConsole();
    initComponents();
  }
  
  public ConsolePanel(IConsole console) {
    this.console = console;
    initComponents();
  }

  private void createUIComponents() {
    tpConsole = (JTextPane) console.getComponent();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    scrollPane1 = new JScrollPane();

    //======== this ========
    setLayout(new BorderLayout());

    //======== scrollPane1 ========
    {
      scrollPane1.setViewportView(tpConsole);
    }
    add(scrollPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JTextPane tpConsole;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
