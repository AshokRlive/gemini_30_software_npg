/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.ide.ui.compiler;

import java.io.File;
import java.io.IOException;

/**
 * The Class ILCompiler.
 */
public class IEC61131SourceCompiler extends AbstractCompiler {

 private final String compilerPath;
 
  public IEC61131SourceCompiler(String compilerJarPath, IOutputHandler... handler) {
    super(handler);
    this.compilerPath = compilerJarPath;
  }
  
  @Override
  public int compile(String sourceFile, String outputFileDir) throws IOException, InterruptedException {
    final File targetDir = new File(outputFileDir);
    targetDir.mkdirs();

    Process p = new ProcessBuilder( 
        "java",
        "-jar",
        compilerPath,
        sourceFile,
        outputFileDir)
        .redirectErrorStream(true)
        .start();
    
    handleOutput(p);
    
    p.waitFor();
    return p.exitValue();
  }

}
