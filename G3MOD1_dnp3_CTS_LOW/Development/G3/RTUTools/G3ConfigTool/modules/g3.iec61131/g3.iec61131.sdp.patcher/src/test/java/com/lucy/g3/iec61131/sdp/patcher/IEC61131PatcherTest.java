/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.sdp.patcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

import com.lucy.g3.iec61131.sdp.patcher.IEC61131Patcher;


/**
 *
 */
public class IEC61131PatcherTest {
  @Test  public void testPatchAndCreateNew() throws IOException, InterruptedException {
    IEC61131Patcher.patchAutoLibs(new File("src/test/resources/SDP.zip"), new File("target/sdp_new.zip"), 
        new File[]{
        new File("src/test/resources/patches/libNewScheme.so.sample"),
        new File("src/test/resources/patches/NewScheme.il"),
    });
  }
  
  @Test  public void testPatchExistingSDP() throws IOException, InterruptedException {
    final File sdpFile = new File("target/SDP.zip");
    sdpFile.delete();
    
    // Copy resource to be patched.
    Files.copy(Paths.get("src/test/resources/SDP.zip"), Paths.get(sdpFile.getPath()));
    
    IEC61131Patcher.patchAutoLibs(sdpFile, 
        new File[]{
            new File("src/test/resources/patches/libNewScheme.so.sample"),
            new File("src/test/resources/patches/NewScheme.il"),
    });
  }
  
}

