/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.sdp.patcher;

import java.io.File;
import java.io.IOException;

import org.zeroturnaround.zip.FileSource;
import org.zeroturnaround.zip.ZipUtil;

/**
 *
 */
public class IEC61131Patcher {
  
  /**
   * Adds automation libraries to a SDP. If the library files already exist, they will be overwritten.
   * @param sdpFileOld
   * @param sdpFileNew
   * @param autoLibFiles
   * @throws InterruptedException
   * @throws IOException
   */
  public static void patchAutoLibs(File sdpFileOld, File... autoLibFiles) throws InterruptedException,
    IOException {
    patchAutoLibs(sdpFileOld, null, autoLibFiles);
  }
  
  /**
   * Adds automation libraries to new SDP. If the library files already exist, they will be overwritten.
   * @param sdpFileOld
   * @param sdpFileNew
   * @param autoLibFiles
   * @throws InterruptedException
   * @throws IOException
   */
  public static void patchAutoLibs(File sdpFileOld, File sdpFileNew, File... autoLibFiles) throws InterruptedException,
      IOException {
    final File mcmZip = File.createTempFile("MCM", ".zip", new File("."));
    
    try {
      // Extract MCM
      ZipUtil.unpackEntry(sdpFileOld, "MCMBoard.zip", mcmZip);

      // Update content of MCM
      FileSource[] zipsources = new FileSource[autoLibFiles.length];
      for (int i = 0; i < autoLibFiles.length; i++) {
        zipsources[i] = new FileSource("lib/automation/" + autoLibFiles[i].getName(), autoLibFiles[i]);
      }
      
      ZipUtil.addOrReplaceEntries(mcmZip, zipsources);
      
      // Replaced MCM file in SDP.
      if(sdpFileNew == null)
        ZipUtil.replaceEntry(sdpFileOld, "MCMBoard.zip", mcmZip);
      else
        ZipUtil.replaceEntry(sdpFileOld, "MCMBoard.zip", mcmZip, sdpFileNew);
      
    } catch (Throwable e1) {
      throw e1;
    } finally {
      // Delete temp file always.
      if(mcmZip != null) {
        mcmZip.delete();
      }
    }
  }
}
