/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.debug.comms;

import java.io.IOException;

import org.junit.Assert;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.iec61131.debug.comms.IEC61131DebuggerComms;
import com.lucy.g3.iec61131.debug.comms.IIEC61131DebuggerComms;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DBG_VAR;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVar;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVarRef;

/**
 * The Class IEC61131DebuggerCommsTest.
 */
public class IEC61131DebuggerCommsTest {

  public final static String HOST =null;// "10.11.11.102";

  private IIEC61131DebuggerComms fixture;

  @BeforeClass
  public static void setupBeforeClass() {
    Assume.assumeNotNull(HOST);
  }

  @Before
  public void setUp() throws Exception {

    fixture = new IEC61131DebuggerComms();
    fixture.setHost(HOST);
    fixture.connect();
  }

  @After
  public void tearDown() throws Exception {
    fixture.disconnect();
  }

  @Test
  public void testGetVariable() throws IOException {
    int len = 99;
    DebugVarRef[] varList = new DebugVarRef[len];
    DBG_VAR[] types = DBG_VAR.values();
    
    for (DBG_VAR type : types) {
      for (int i = 0; i < varList.length; i++) {
        varList[i] = new DebugVarRef(type, (byte) i);
      }

      DebugVar[] reply = fixture.getVariable(varList);

      Assert.assertEquals(reply.length, len);

      for (int i = 0; i < reply.length; i++) {
        Assert.assertNotNull(reply[i]);
        System.out.println(String.format("%s   value:%d", reply[i].ref, reply[i].value));
      }

    }
  }

}
