/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.iec61131.debug.comms;

import org.junit.Assert;

import org.junit.Test;

import com.lucy.g3.iec61131.debug.comms.DBGRequest;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DBG_CMD;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DBG_VAR;
import com.lucy.g3.iec61131.debug.comms.IDebuggerProtocol.DebugVarRef;


/**
 *
 */
public class DBGRequestTest {

  
  @Test
  public void getGetVarRequest(){
    DBGRequest request = new DBGRequest(DBG_CMD.GET_VAR);
    DebugVarRef [] varList = new DebugVarRef[2];
    varList[0] = new DebugVarRef(DBG_VAR.DBG_VAR_INPUT, (byte)0);
    varList[1] = new DebugVarRef(DBG_VAR.DBG_VAR_INPUT, (byte)1);
    
    for (int i = 0; varList != null && i < varList.length; i++) {
      request.putPayloadBytes(new byte[]{varList[i].type, varList[i].index});
    }
    
    byte[] bytes = request.toBytes();
    
    Assert.assertEquals(IDebuggerProtocol.DBG_MESSAGE_HEADER_SIZE + DebugVarRef.size()*varList.length,
        bytes.length
        );
    
  }

}

