
package com.lucy.g3.iec61131.debug.comms;

import java.io.IOException;

/**
 * The interface of 61131 comms component for debugging 61131.
 */
public interface IIEC61131DebuggerComms extends IDebuggerProtocol {

  /**
   * Connects to server. 
   * @return the reply message from server.
   * @throws IOException if there is any failure.
   */
  String connect() throws IOException;

  /**
   * Disconnects from server.
   * @throws IOException
   */
  void disconnect();

  void setHost(String ip) throws IOException;

  void setHost(String ip, int port) throws IOException;

  String getIp();

  DBGReply sendRequest(DBG_CMD cmd, int... payloadData) throws IOException;

  DBGReply sendRequest(DBG_CMD cmd, byte... payloadData) throws IOException;

  DBGReply sendRequest(DBGRequest request) throws IOException;

  void pause() throws IOException;

  void resume() throws IOException;

  void stepover() throws IOException;

  void setBreakpoint(int breakpointLine) throws IOException;

  void delBreakpoint(int breakpointLine) throws IOException;

  DebugState getCurrentState() throws IOException;
  
  DebugVar[] getVariable(DebugVarRef[] varList)  throws IOException;

  boolean isConnected();

  /**
   * Gets the file name of the running automation scheme library. 
   * @return
   * @throws IOException
   */
  String getLibraryFileName() throws IOException;

}
