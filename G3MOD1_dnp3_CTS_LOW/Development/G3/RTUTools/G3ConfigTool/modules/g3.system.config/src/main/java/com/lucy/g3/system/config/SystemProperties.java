
package com.lucy.g3.system.config;


public interface SystemProperties {
  
  /**
   * Key for accessing boolean system property:{@value}. 
   */
  String PROPERTY_SUPPRESS_SSL_WARNING = "suppressSSLWarning";
  
  String PROPERTY_PROFILE_NAME = "profileName";
  
  class getter {
    private getter(){}
    public static boolean getBoolean(String key) {
      Boolean bvalue = Boolean.valueOf(System.getProperty(key));
      return bvalue == Boolean.TRUE;
    }
    
    public static String getString(String key) {
      return System.getProperty(key);
    }
  }
}

