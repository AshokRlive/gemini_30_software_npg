/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.filetransfer;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;

import com.lucy.g3.rtu.comms.protocol.exceptions.ProtocolException.NackException;
import com.lucy.g3.rtu.comms.service.filetransfer.FileReadingCommsTask;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * Read a file from RTU device.
 */
public class FileReadingTask extends Task<File, Void> {

  protected Logger log = Logger.getLogger(FileReadingTask.class);

  private JFrame parent;


  private final FileReadingCommsTask commsTask;
  
  private boolean confirmOverrite;
  
  private boolean showConfirmOverriteDialog = true;
  
  private boolean showBlocker;

  /**
   * Construct a file reading task to read file from RTU into a destination
   * file.
   *
   * @param app
   *          the application which executes this task.
   * @param type
   *          the type of the file that is read.
   * @param sourceFileName
   *          the name of the file to be read from RTU.
   * @param destination
   *          the destination for saving the file.
   */
  public FileReadingTask(Application app, FileTransfer fileTransfer, CTH_TRANSFERTYPE type, String sourceFileName,
      File destination) {
    this(app, fileTransfer, type, sourceFileName, destination, true);
  }
  
  public FileReadingTask(Application app, FileTransfer fileTransfer, CTH_TRANSFERTYPE type, String sourceFileName,
      File destination, boolean showBlocker) {
    super(app);
    if(sourceFileName == null) 
      throw new NullPointerException("sourceFileName must not be null");
    if(fileTransfer == null) 
      throw new NullPointerException("fileTransfer must not be null");
    
    this.showBlocker = showBlocker;
    this.commsTask = new FileReadingCommsTask(new CommsInvoker(), fileTransfer, type, sourceFileName, destination);

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }

    if(showBlocker) {
      this.setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
      this.setUserCanCancel(true);
    }
  }
  
  
  public JFrame getParent() {
    return parent;
  }

  public void setParent(JFrame parent) {
    this.parent = parent;
    if(showBlocker) {
      this.setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
      this.setUserCanCancel(true);
    }
    
  }

  protected void setFileTransferType(CTH_TRANSFERTYPE type) {
    commsTask.setFileTransferType(type);
  }

  protected void setSourceFileName(String sourceFileName) {
    commsTask.setSourceFileName(sourceFileName);
  }

  public String getSourceFileName() {
    return commsTask.getSourceFileName();
  }

  protected void setDestination(File destination) {
    commsTask.setDestination(destination);
  }

  public File getDestination() {
    return commsTask.getDestination();
  }

  
  @Override
  protected File doInBackground() throws Exception {
    commsTask.run();
    return commsTask.getResult();
  }

  
  public boolean isShowConfirmOverriteDialog() {
    return showConfirmOverriteDialog;
  }

  
  public void setShowConfirmOverriteDialog(boolean showConfirmOverriteDialog) {
    this.showConfirmOverriteDialog = showConfirmOverriteDialog;
  }

  protected boolean confirmOverrite(final File file) throws InvocationTargetException, InterruptedException {
    if(showConfirmOverriteDialog) {
    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        String msg = String.format("File already exists. Do you want to overwrite?\n\"%s\"",
            file.getAbsolutePath());
        int option = JOptionPane.showConfirmDialog(parent,
            msg,
            "Overwrite",
            JOptionPane.YES_NO_CANCEL_OPTION);
        confirmOverrite = (option == JOptionPane.YES_OPTION);
      }
    });
    
    if(!confirmOverrite)
      cancel(false);
    
    return confirmOverrite;
    } else {
      return true;
    }
  }

  
  public FileTransfer getFileTransfer() {
    return commsTask.getFileTransfer();
  }
  
  @Override
  protected void succeeded(File result) {
    setProgress(100);
    setMessage("File reading succeeded");
  }

  @Override
  protected void cancelled() {
    setMessage("File reading cancelled");
  }

  @Override
  protected void failed(final Throwable cause) {
    String msg = "File reading failed";
    if (cause instanceof NackException) {
      log.error("Failed to read: "+getSourceFileName() + ". Error: "+msg + ". Nack: " + ((NackException) cause).nackCode);
    } else {
      log.error(msg, cause);
    }
    setMessage(msg);
  }

  private class CommsInvoker implements ICommsTaskInvoker {

    @Override
    public boolean isCancelled() {
      return false;
    }

    @Override
    public void setProgress(int progress) {
      FileReadingTask.this.setProgress(progress);
    }

    @Override
    public void setMessage(String message) {
      FileReadingTask.this.setMessage(message);      
    }

    @Override
    public boolean confirmOverrite(File overwriteFile) {
      try {
        return FileReadingTask.this.confirmOverrite(overwriteFile);
      } catch (InvocationTargetException | InterruptedException e) {
        return false;
      }      
    }
    
  }
}
