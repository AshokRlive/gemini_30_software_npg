/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.filetransfer;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.rtu.comms.service.filetransfer.FileEndInfo;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.shared.ICommsAPI;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;


/**
 * The Class provides methods of running file transfer tasks.
 */
public class FileTransferService {
  private FileTransferService(){}
  public static FileReadingTask createFileReadTask(FileTransfer ft, CTH_TRANSFERTYPE type, String sourceFileName,
      File destination, boolean showBlocker) {
    FileReadingTask task = new FileReadingTask(getApp(), ft, type, sourceFileName, destination, showBlocker);
    return task;
  }
  
  private static Application getApp(){
    return Application.getInstance();
  }
  
  public static FileWritingTask createFileWriteTask(FileTransfer ft,CTH_TRANSFERTYPE type, String customFilePath, File... files) {
    FileWritingTask task = new FileWritingTask(getApp(), ft, type, files);
    task.setCustomFilePath(customFilePath);
    return task;
  }

  public static FileReadingTask readFileFromRTU(FileTransfer ft,CTH_TRANSFERTYPE type, String sourceFileName,
      File destination, boolean showBlocker) {
    FileReadingTask task = createFileReadTask(ft,type, sourceFileName, destination, showBlocker);
    getApp().getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME).execute(task);
    return task;
  }
  
  public static FileWritingTask writeFileToRTU(FileTransfer ft,CTH_TRANSFERTYPE type, File... files) {
    return writeFileToRTU(ft,type, null, files);
  }
  
  public static FileWritingTask writeFileToRTU(FileTransfer ft,CTH_TRANSFERTYPE type, String customFilePath, File... files) {
    FileWritingTask task = createFileWriteTask(ft,type, customFilePath, files);
    executeTask(task);
    return task;
  }

  public static void executeTask(Task commsTask) {
    getApp().getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME).execute(commsTask);
  }
  
  public static FileEndInfo[] writeFileToRTUAndWait(FileTransfer ft,CTH_TRANSFERTYPE type,  int timeoutSecs, File... files) 
      throws InterruptedException, ExecutionException, TimeoutException {
    return writeFileToRTUAndWait(ft,type,null,timeoutSecs,files);
  }
  
  public static FileEndInfo[] writeFileToRTUAndWait(FileTransfer ft,CTH_TRANSFERTYPE type,  String customFilePath, int timeoutSecs, File... files) 
      throws InterruptedException, ExecutionException, TimeoutException {
    FileWritingTask task = writeFileToRTU(ft,type, customFilePath, files);
    return task.get(timeoutSecs, TimeUnit.SECONDS);
  }
}

