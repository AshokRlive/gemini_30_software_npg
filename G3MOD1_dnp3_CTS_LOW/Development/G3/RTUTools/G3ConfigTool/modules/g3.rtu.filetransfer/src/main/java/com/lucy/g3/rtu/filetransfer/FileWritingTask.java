/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.rtu.filetransfer;

import java.io.File;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;

import com.lucy.g3.rtu.comms.service.filetransfer.FileEndInfo;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.comms.service.filetransfer.FileWritingCommsTask;
import com.lucy.g3.rtu.comms.shared.ICommsTaskInvoker;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * This task writes files to RTU based on G3 Configuration Protocol.
 */
public class FileWritingTask extends Task<FileEndInfo[], Void> {

  private Logger log = Logger.getLogger(FileWritingTask.class);


  protected final JFrame parent;

  private final FileWritingCommsTask commsTask;


  public FileWritingTask(Application app, FileTransfer fileTransfer, CTH_TRANSFERTYPE type, File... files) {
    super(app);
    this.commsTask = new FileWritingCommsTask(new CommsInvoker(), fileTransfer, type, files);

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    this.setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
    this.setUserCanCancel(true);
  }

  public void setCustomFilePath(String customFilePath) {
    commsTask.setTargetPath(customFilePath);
  }

  @Override
  protected void finished() {
    setMessage("File transfer finished");
  }

  @Override
  protected void succeeded(FileEndInfo[] endInfo) {
    if(endInfo == null) {
      log.warn("Failed transfer completed without FileEndInfo!");
    }else {
      for (int i = 0; i < endInfo.length; i++) {
        log.info("File Transfer completed: " + endInfo[i].toString());
      }
    }
  }

  @Override
  protected void failed(Throwable cause) {
    log.error(cause.getMessage());
    setMessage(cause.getMessage());
  }

  protected FileEndInfo transferFile(CTH_TRANSFERTYPE type, File file) throws Exception {
    return commsTask.writeFile(type, file);
  }
  
  @Override
  protected FileEndInfo[] doInBackground() throws Exception {
    commsTask.run();
    return commsTask.getEndInfo();
  }
  
  private class CommsInvoker implements ICommsTaskInvoker {

    @Override
    public boolean isCancelled() {
      return false;
    }

    @Override
    public void setProgress(int progress) {
      FileWritingTask.this.setProgress(progress);
    }

    @Override
    public void setMessage(String message) {
      FileWritingTask.this.setMessage(message);      
    }

    @Override
    public boolean confirmOverrite(File overwriteFile) {
      return true;  
    }
  }
}
