/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.file.names;

import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;



/**
 * G3 file names, filters, etc.
 */
public class G3Files {
  /** The default configuration file name used by RTU. */
  public static final String DEF_G3CONFIG_FNAME = "G3Config.xml";
  
  /** File extension for GZip files. */
  public static final String SUFFFIX_GZIP = ".gz";
  
  /** File extension for XML files. */
  public static final String SUFFFIX_XML = ".xml";
  
  /**
   * File filter for G3 Configuration files.
   */
  public static final FileFilter[] FC_FILTER_G3CFG_GROUP = {
      new FileNameExtensionFilter("Compressed Gemini3 RTU Configuration(.gz)", "gz"),
      new FileNameExtensionFilter("Gemini3 RTU Configuration(.xml)", "xml"),
  };

  /**
   * File filter for G3 Configuration files.
   */
  public static final FileFilter FC_FILTER_G3CFG = new FileNameExtensionFilter(
      "Gemini3 RTU Configuration(.xml, .gz, .sdp)",
      "xml", "gz", "sdp");

  /**
   * File filter for G3 Configuration template files.
   */
  public static final FileFilter FC_FILTER_G3TEMPLATE = new FileNameExtensionFilter("Gemini3 RTU Templates (.xml)",
      "xml");

  /**
   * File filter for HTML G3 Configuration.
   */
  public static final FileFilter[] FC_FILTER_G3CFG_HTML = {
      new FileNameExtensionFilter("Gemini3 RTU Configuration(.html)", "html"),
  };
  
  /**
   * File filter for G3 certificate store.
   */
  public static final FileFilter[] FC_FILTER_G3_CERT_STORE = {
      new FileNameExtensionFilter("Gemini3 Certificate Store(.jks)", "jks"),
  };
  
  /**
   * File filter for G3 archive file.
   */
  public static final FileFilter FC_FILTER_G3_ARCHIVE = 
      new FileNameExtensionFilter("Archived RTU Files(.zip)", "zip");
  
  /**
   * File filter for IL compiler.
   */
  public static final FileFilter FC_FILTER_IL_COMPILER = 
      new FileNameExtensionFilter("IL Compiler(*.jar)", "jar");
  
  public static final FileFilter FC_FILTER_IL_SOURCE = 
      new FileNameExtensionFilter("Source File(*.il)", "il");

  /**
   * File filter for G3 Software Distribution files.
   */
  public static final FileFilter FC_FILTER_G3SDP =
    new FileNameExtensionFilter("Gemini3 RTU Software Distribution Package(.sdp, .zip)", "sdp", "zip");
  
  public static final FileFilter FC_FILTER_SLAVE_BIN =
  new FileNameExtensionFilter("Slave Firmware(.bin)", "bin");
}

