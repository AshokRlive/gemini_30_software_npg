/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.iec61131;

import java.awt.Window;

import org.jdesktop.application.Action;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.iec61131.ide.ui.IEC61131Window;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.manager.G3RTUFactory;


/**
 *
 */
public class IEC61131Subsystem extends AbstractSubsystem {
  public final static String SUBSYSTEM_ID = "subystem.id.IEC61131Subsystem";

  public static final String ACTION_LAUNCH_IEC61131 = "launchIEC61131";

  private IEC61131Window iec61131Window;
  
  /**
   * @param app
   * @param uniqueSubsystemID
   */
  public IEC61131Subsystem(IContext context) {
    super(context, SUBSYSTEM_ID);
  }

  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
//    /*Close debug window if the RTU is not in normal mode.*/
//    if(!newState.isNormalMode()) {
//      if(isWindowOpen(dbgWindow)) {
//        dbgWindow.dispose();
//        dbgWindow = null;
//      }
//    }
  }

  private static boolean isWindowOpen(Window window) {
    return window != null && window.isDisplayable();
  }
  
  
  private void setIEC61131Window(IEC61131Window iec61131Window) {
    this.iec61131Window = iec61131Window;
  }

  @Action
  public void launchIEC61131() {
    if(isWindowOpen(iec61131Window)) {
      /*Window already open, bring it to the front.*/
      iec61131Window.setVisible(true);
      return;
    }
    
    String rtuIP = G3RTUFactory.getComms().getHost();
//    if(isConnected()) {
//        /*Read file then launch IEC61131 */
//        AutoLibSourceReadingTask task = new AutoLibSourceReadingTask(getApp(),
//            G3CommsService.getFileTransferService().getFileTransfer(), 
//            rtuIP, null);
//        task.setReadRunningFile(true);
//        task.addTaskListener(new AutoLibSourceReadingTaskHandler(rtuIP));
//        CommsTaskService.getInstance(getApp()).execute(task);
//        
//      } else {
        /* Launch IEC61131 directly*/
        IEC61131Window window = IEC61131Window.launch(
            IEC61131Subsystem.this.getMainFrame(),
            rtuIP, 
            null, false);
        setIEC61131Window(window);
//      }
  }

//  private class AutoLibSourceReadingTaskHandler extends TaskListener.Adapter<File,Void>{
//    final private String rtuIP;
//    
//    private File sourceFile;
//
//    private String error;
//    
//    public AutoLibSourceReadingTaskHandler(String rtuIP) {
//      super();
//      this.rtuIP = rtuIP;
//    }
//
//    @Override
//    public void succeeded(TaskEvent<File> event) {
//      sourceFile = event.getValue();
//    }
//
//    @Override
//    public void failed(TaskEvent<Throwable> event) {
//      error = "Cannot read the source of the current automation scheme from RTU! ";
//      log.error(error, event.getValue());
//    }
//
//    @Override
//    public void finished(TaskEvent<Void> event) {
//      IEC61131Window window = IEC61131Window.launch(
//          IEC61131Subsystem.this.getMainFrame(),
//          rtuIP, 
//          sourceFile, false);
//      setIEC61131Window(window);
//      
//      if(error != null)
//        window.getContext().console.appendErr(error);
//    }
//  }
  
}
