
=== G3ConfigTool Project Maven Structure ===

com.lucy.g3.configtool.root     		     POM<-- Properties, version, plugin management,etc.
|
+---com.lucy.g3.configtool.parent            POM<-- Module aggregation, dependency management,etc
+---com.lucy.g3.configtool.app               JAR<-- Main application.
+---com.lucy.g3.sdp.model                	 JAR<-- SDP model.
.
.
.
.
+---com.lucy.g3.configtool.deps              POM<-- Project external dependencies.
    |
    +---../common/g3.common.build            
    +---../G3Updater 
    +---../IEC61131Tool/g3.iec61131.ide.ui
    +---../G3CommissioningTool/g3.commissioning.ui 