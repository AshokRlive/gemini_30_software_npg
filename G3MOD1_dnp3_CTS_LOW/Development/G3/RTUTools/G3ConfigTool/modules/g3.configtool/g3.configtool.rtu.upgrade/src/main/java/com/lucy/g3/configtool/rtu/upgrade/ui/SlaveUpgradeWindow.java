/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.JXLabel;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.upgrade.task.SlaveUpgradeTask;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeWindow;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.manager.G3RTU;

/**
 * A window for upgrading slave module. This updater will not check any
 * compatibility before updating.
 */
public class SlaveUpgradeWindow extends AbstractRealtimeWindow {

  private static String ACTION_KEY_START = "start";
  private static String ACTION_KEY_CLOSE = "close";
  private static String ACTION_KEY_SELECT_FW = "selectFirmware";

  private final  G3RTU rtu;

  private JComponent content;

  private Task<?, ?> listUpdateTask;

  private final HashMap<String, ModuleInfo> slavesMap = new HashMap<String, ModuleInfo>();
  private final HashMap<ModuleInfo, File> updateMap = new HashMap<ModuleInfo, File>();
  private final HashMap<ModuleInfo, JTextField> filePathCompMap = new HashMap<ModuleInfo, JTextField>();

  private final TaskService taskservice;


  public SlaveUpgradeWindow(Application app, G3RTU rtu) {
    super(app, null);
    this.rtu = Preconditions.checkNotNull(rtu, "rtu must not be null");
    this.taskservice = app.getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
    initComponents();

    // Show loading message
    JLabel msgLabel = new JLabel("Loading...");
    msgLabel.setHorizontalAlignment(JLabel.CENTER);
    setContent(msgLabel);

    listUpdateTask = new SlaveListUpdateTask(app);
    taskservice.execute(listUpdateTask);
  }

  @Action
  public void start() {
    // Check module selection
    if (slavesMap.isEmpty()) {
      JOptionPane.showMessageDialog(this, "No slave module found!",
          "Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    Task<?, ?> upgradeTask = new SlaveUpgradeTask(application,
        rtu, new HashMap<ModuleInfo, File>(updateMap));
    taskservice.execute(upgradeTask);

    // close dialog after starting task.
    close();
  }

  /* Overridden as an action. */
  @Override
  @Action
  public void close() {
    super.close();
  }

  @Action
  public void selectFirmware(ActionEvent e) {
    File fwFile = DialogUtils.showFileChooseDialog(SlaveUpgradeWindow.this,
        "Select a firmware file",
        "Select",G3Files.FC_FILTER_SLAVE_BIN
        );

    if (fwFile != null) {
      String fwFilePath = fwFile.getPath();

      String moduleKey = e.getActionCommand();
      ModuleInfo m = slavesMap.get(moduleKey);

      if (m != null) {
        JTextField filePathComp = filePathCompMap.get(m);
        filePathComp.setText(fwFilePath);

        updateMap.put(m, fwFile);
      }
    }
  }

  /*
   * Update the content view of this dialog.
   */
  private void setContent(JComponent newContent) {
    if (!(newContent instanceof JScrollPane)) {
      newContent = new JScrollPane(newContent);
      ((JScrollPane) newContent).setBorder(BorderFactory.createEmptyBorder());
    }

    // Remove old content
    if (this.content != null) {
      dialogPane.remove(this.content);
    }

    this.content = newContent;
    dialogPane.add(newContent, BorderLayout.CENTER);

    // Refresh
    SlaveUpgradeWindow.this.validate();
    SlaveUpgradeWindow.this.repaint();
  }

  private void createUIComponents() {
    ApplicationActionMap actions = application.getContext().getActionMap(this);
    btnCancel = new JButton(actions.get(ACTION_KEY_CLOSE));
    btnOk = new JButton(actions.get(ACTION_KEY_START));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    buttonBar = new JPanel();
    label2 = new JXLabel();

    // ======== this ========
    setTitle("Slave Upgrade");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setPreferredSize(new Dimension(522, 300));
      dialogPane.setLayout(new BorderLayout(0, 10));

      // ======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$glue, $button, $rgap, $button",
            "pref"));
        buttonBar.add(btnOk, CC.xy(2, 1));
        buttonBar.add(btnCancel, CC.xy(4, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);

      // ---- label2 ----
      label2.setText("* Please note the version compatibility is not checked.");
      label2.setLineWrap(true);
      label2.setForeground(Color.darkGray);
      dialogPane.add(label2, BorderLayout.NORTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel buttonBar;
  private JButton btnOk;
  private JButton btnCancel;
  private JXLabel label2;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private JPanel createContentPanel(Collection<ModuleInfo> slaves) {
    slavesMap.clear();
    updateMap.clear();
    filePathCompMap.clear();

    FormLayout layout = new FormLayout("default, $lcgap, default:grow, $lcgap, default");

    DefaultFormBuilder builder = new DefaultFormBuilder(layout);
    builder.setDefaultDialogBorder();

    if (slaves != null) {
      for (ModuleInfo m : slaves) {
        if (m == null) {
          continue;
        }
        
        String moduleName = ModuleResource.INSTANCE.getModuleShortName(m.getModuleType(), m.getModuleID());
        slavesMap.put(moduleName, m);
        appendRow(builder, m);
      }
    }

    return builder.getPanel();
  }

  private void appendRow(DefaultFormBuilder builder, ModuleInfo m) {
    String name = ModuleResource.INSTANCE.getModuleShortName(m.getModuleType(), m.getModuleID());
    JLabel label = new JLabel(name + ": ");

    JTextField filePath = new JTextField();
    filePath.setEditable(false);
    filePathCompMap.put(m, filePath);

    javax.swing.Action browseAction = getActionMap().get(ACTION_KEY_SELECT_FW);
    JButton browse = new JButton(new BusyCursorAction(browseAction, this));
    browse.setActionCommand(name);// Module name used as a key
    browse.setFocusPainted(false);

    builder.append(label, filePath, browse);
  }


  /**
   * Gets slave list from RTU and update local slave list model.
   */
  private class SlaveListUpdateTask extends Task<Collection<ModuleInfo>, Void> {

    public SlaveListUpdateTask(Application app) {
      super(app);
    }

    @Override
    protected Collection<ModuleInfo> doInBackground() throws Exception {
      Collection<ModuleInfo> upgmodules = rtu.getComms().getFwUpgrade().cmdGetUpgradeableModules();
      ArrayList<ModuleInfo> slaves = new ArrayList<ModuleInfo>();

      for (ModuleInfo m : upgmodules) {
        if (!m.isMCM()) {
          slaves.add(m);
        }
      }

      return slaves;
    }

    @Override
    protected void failed(Throwable cause) {
      JTextArea errLabel = new JTextArea("Fail to retrieve slave module list.\r\n Cause: "
          + cause.getMessage());
      errLabel.setEditable(false);
      setContent(errLabel);
    }

    @Override
    protected void succeeded(Collection<ModuleInfo> result) {
      super.succeeded(result);
      setContent(createContentPanel(result));
    }

  }


  protected void stopTesting() {
    if (listUpdateTask != null) {
      listUpdateTask.cancel(true);
    }
  }

  @Override
  public void notifyConnectionStateChanged(ConnectionState oldState,
      ConnectionState newState) {
    if (ConnectionState.isUpgradeMode(newState) == false) {
      close();
    }
  }
}
