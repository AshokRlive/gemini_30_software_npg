/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.VerticalLayout;

import com.lucy.g3.gui.common.widgets.ext.jdesktop.BusyIndicatorInputBlocker;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.ModuleRef;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeAPI;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * The task for doing factory reset to RTU. The RTU will be restarted after
 * reset. Show error
 */
public class EraseFirmwareTask extends Task<ArrayList<ModuleRef>, Void> {

  private final JFrame parent;


  public EraseFirmwareTask(Application app, JFrame parent) {
    super(app);
    this.parent = parent;

    setInputBlocker(new BusyIndicatorInputBlocker(this, parent));
    setUserCanCancel(true);
  }

  @Override
  protected void succeeded(ArrayList<ModuleRef> result) {
    String message;
    if (result == null || result.isEmpty()) {
      message = "No module has been erased!";
    } else {
      StringBuilder sb = new StringBuilder();
      sb.append("<html><p>The following modules have been erased:</p><ul>");
      for (ModuleRef m : result) {
        if (m != null) {
          sb.append("<li>");
          sb.append(m);
          sb.append("</li>");
        }
      }
      sb.append("</ul></html>");
      message = sb.toString();
    }
    JOptionPane.showMessageDialog(parent, message, "Done", JOptionPane.INFORMATION_MESSAGE);
  }

  private List<ModuleRef> selectModule(ModuleInfo[] mInfo) {
    ModuleListPanel selectionPanel = new ModuleListPanel(mInfo);
    int option = JOptionPane.showConfirmDialog
        (
            parent,
            selectionPanel,
            "Erase Firmware",
            JOptionPane.OK_CANCEL_OPTION
        );

    if (option == JOptionPane.OK_OPTION) {
      return selectionPanel.getSelectedModule();
    } else {
      return null;
    }
  }

  @Override
  protected ArrayList<ModuleRef> doInBackground() throws Exception {
    UpgradeAPI upgcmd = CommsUtil.getUpgradeAPI(G3RTUFactory.getComms());
    
    final ArrayList<ModuleRef> selection = new ArrayList<ModuleRef>();
    Collection<ModuleInfo> mlist = upgcmd.cmdGetUpgradeableModules();
    final ModuleInfo[] mInfo = mlist.toArray(new ModuleInfo[mlist.size()]);

    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        List<ModuleRef> result = selectModule(mInfo);
        if (result != null) {
          selection.addAll(result);
        }
        else {
          cancel(true);// User cancelled
        }
      }
    });

    if (!selection.isEmpty()) {
      return upgcmd.cmdEraseModules(selection);
    } else {
      return null;
    }
  }

  @Override
  protected void failed(final Throwable cause) {
    cause.printStackTrace();
    JOptionPane.showMessageDialog(parent, cause.getMessage(),
        "Firmware Erase Failure", JOptionPane.ERROR_MESSAGE);
  }


  private static class ModuleListPanel extends JPanel {

    private HashMap<ModuleInfo, JCheckBox> moduleSelectMap = new HashMap<ModuleInfo, JCheckBox>();


    public ModuleListPanel(ModuleInfo[] mInfo) {
      super(new VerticalLayout());

      JLabel title = new JLabel("Select the module to be erased:");
      add(title);
      JCheckBox chbox;
      for (ModuleInfo m : mInfo) {
        if (m != null && m.isPresent()) {
          chbox = new JCheckBox(ModuleResource.INSTANCE.getModuleShortName(m.getModuleType(), m.getModuleID()));
          add(chbox);
          moduleSelectMap.put(m, chbox);

          // MCM erasing unsupported
          if (m.getModuleType() == MODULE.MODULE_MCM) {
            chbox.setEnabled(false);
          }

        }
      }

      if (moduleSelectMap.isEmpty()) {
        title.setText("No present module!");
      }
    }

    public List<ModuleRef> getSelectedModule() {
      ArrayList<ModuleRef> selected = new ArrayList<ModuleRef>();
      Iterator<ModuleInfo> it = moduleSelectMap.keySet().iterator();

      ModuleInfo m;
      while (it.hasNext()) {
        m = it.next();
        if (moduleSelectMap.get(m).isSelected()) {
          selected.add(new ModuleRef(m.getModuleType(), m.getModuleID(), m.getSerialNo()));
        }
      }
      return selected;
    }

  }
}
