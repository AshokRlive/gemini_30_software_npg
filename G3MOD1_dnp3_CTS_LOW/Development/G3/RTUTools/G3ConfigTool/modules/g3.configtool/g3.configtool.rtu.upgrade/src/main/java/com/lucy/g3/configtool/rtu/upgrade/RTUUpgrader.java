/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;

import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.configtool.rtu.upgrade.task.EraseFirmwareTask;
import com.lucy.g3.configtool.rtu.upgrade.task.FactoryResetTask;
import com.lucy.g3.configtool.rtu.upgrade.ui.SlaveUpgradeWindow;
import com.lucy.g3.configtool.subsys.permissions.PermissionCheckChecks;
import com.lucy.g3.configtool.subsys.permissions.UserActivities;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;


public class RTUUpgrader extends AbstractSubsystem  {
  public final static String SUBSYSTEM_ID = "subystem.id.RTUUpgrader";
  
  public static final String ACTION_KEY_SLAVE_UPGRADE  = "slaveUpgrade";
  public static final String ACTION_KEY_ERASE_FIRMWARE = "eraseFirmware";
  public static final String ACTION_KEY_FACTORY_RESET  = "factoryResetRtu";
  public static final String ACTION_KEY_ENABLE_UPGMODE       = "enableUpgradeMode";
  public static final String ACTION_KEY_DISABLE_UPGMODE      = "disableUpgradeMode";

  private Logger log = Logger.getLogger(RTUUpgrader.class);
  
  public RTUUpgrader(com.lucy.g3.common.context.IContext context) {
    super(context, SUBSYSTEM_ID);
  }

  private void setUpgradeMode(boolean enabled) {
    if (checkIsConnected() == false) {
      return;
    }

    if (PermissionCheckChecks.checkPermitted(UserActivities.UPGRADE) == false) {
      return;
    }

    int rtn = JOptionPane.showConfirmDialog(getMainFrame(),
        "Require to restart RTU. Do you want to continue?",
        "Warning",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.WARNING_MESSAGE);

    if (rtn == JOptionPane.YES_OPTION) {
      RestartMode mode = enabled ? RestartMode.ENTER_UPGRADE : RestartMode.EXIT_UPGRADE;
      EventBroker.getInstance().send(EventTopics.EVT_TOPIC_RESTART_RTU, mode);
    }
  }
  
  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void enableUpgradeMode() {
    setUpgradeMode(true);
  }

  
  @Action(enabledProperty = PROPERTY_UPGRADE_MODE)
  public void disableUpgradeMode() {
    setUpgradeMode(false);
  }

  
  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void factoryResetRtu() {
    if (checkIsConnected() == false) {
      return;
    }

    int option = JOptionPane.showConfirmDialog(
        getMainFrame(),
        "Factory reset will clean configuration and registration database.\nContinue?",
        "Factory Reset",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.WARNING_MESSAGE);

    if (option == JOptionPane.YES_OPTION) {
      FactoryResetTask task = new FactoryResetTask(getApp(), getMainFrame());
      getCommsTaskService().execute(task);
    }
  }

  
  @Action(enabledProperty = PROPERTY_UPGRADE_MODE)
  public void eraseFirmware() {
    if (isUpgradeMode()) {
      getCommsTaskService().execute(new EraseFirmwareTask(getApp(), getMainFrame()));
    } else {
      log.error("Cannot erase firmware, upgrade mode needed!");
    }
  }
  
  @Action(enabledProperty = PROPERTY_UPGRADE_MODE)
  public void slaveUpgrade() {
    if (checkIsConnected() == true) {
      SlaveUpgradeWindow testWindow = new SlaveUpgradeWindow(getApp(), getRTU());
      showWindow(testWindow);
    }
  }


  private G3RTU getRTU() {
    return G3RTUFactory.getDefault();
  }

  
  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    
  }

}

