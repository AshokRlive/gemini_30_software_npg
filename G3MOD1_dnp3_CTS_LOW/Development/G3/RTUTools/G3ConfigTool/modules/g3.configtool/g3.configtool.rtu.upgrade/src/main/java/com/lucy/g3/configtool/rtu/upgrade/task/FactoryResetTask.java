/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade.task;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.Task;

import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.FactoryResetAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * The task for doing factory reset to RTU. The RTU will be restarted after
 * reset. Show error
 */
public class FactoryResetTask extends Task<Void, Void> {

  private final JFrame parent;


  public FactoryResetTask(Application app, JFrame parent) {
    super(app);
    this.parent = parent;
    

    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
    setUserCanCancel(true);
  }

  @Override
  protected void succeeded(Void result) {
    EventBroker.getInstance().send(EventTopics.EVT_TOPIC_RESTART_RTU, RestartMode.RESTART);
  }

  @Override
  protected Void doInBackground() throws Exception {
    FactoryResetAPI resetCmd = CommsUtil.getFactoryResetAPI(G3RTUFactory.getComms());
    
    resetCmd.cmdFactoryReset();
    resetCmd.cmdRestart(RestartMode.RESTART, true);

    // time period which allows RTU to be reset
    setMessage("Waiting restart...");
    Thread.sleep(2000);

    return null;
  }

  @Override
  protected void failed(final Throwable cause) {
    JOptionPane.showMessageDialog(parent, cause.getMessage(),
        "Restart Fail", JOptionPane.ERROR_MESSAGE);
  }

}
