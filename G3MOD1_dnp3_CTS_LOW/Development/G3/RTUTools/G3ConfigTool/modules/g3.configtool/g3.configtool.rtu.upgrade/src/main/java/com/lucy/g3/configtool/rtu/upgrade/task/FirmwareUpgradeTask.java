/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade.task;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.error.ErrorInfo;
import org.zeroturnaround.zip.ZipUtil;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.gui.common.widgets.ext.swing.action.FileLinkAction;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.RTURestartSupport;
import com.lucy.g3.rtu.comms.service.RTURestartSupport.IWaitingRestartInvoker;
import com.lucy.g3.rtu.comms.service.filetransfer.FileEndInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.FirmwareUpgrade;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeStatus;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.rtu.filetransfer.FileWritingTask;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * Firmware Upgrading task which gets G3 software from {@code SoftwareEntry} map
 * and upgrade G3 modules one after another. A dialog will be showed to report
 * result after this task is finished.
 */
public class FirmwareUpgradeTask extends FileWritingTask implements IWaitingRestartInvoker{

  private Logger log = Logger.getLogger("FirmwareUpgradeTask");

  private static final int TIMEOUT = 180;// seconds
  private static final int MAX_RETRIES = 3;

  private final HashMap<ModuleInfo, SoftwareEntry> appSoftwareMap;
  private final HashMap<ModuleInfo, SoftwareEntry> bootSoftwareMap;

  private final File sdpFile;

  private final Application app;

  /* The modules to be upgraded */
  private final ArrayList<ModuleInfo> preUpgradeModules = new ArrayList<ModuleInfo>();

  /* The modules that have been upgraded successfully */
  private final ArrayList<ModuleInfo> upgradedModules = new ArrayList<ModuleInfo>();

  /* The directory where the SDP will be extracted to*/
  private final File firmwareDir;

  private final G3RTU rtu;

  private final String sdpVersion;

  private FirmwareUpgrade upgradecmd;


  public FirmwareUpgradeTask(Application app,
      G3RTU rtu,
      HashMap<ModuleInfo, SoftwareEntry> appSoftwareMap,
      HashMap<ModuleInfo, SoftwareEntry> bootSoftwareMap,
      File sdpFile,
      String sdpVersion) {

    super(app, rtu.getComms().getFileTransfer(), null);
    this.rtu = Preconditions.checkNotNull(rtu, "rtu must not be null");
    this.upgradecmd = rtu.getComms().getFwUpgrade();
    this.sdpFile = Preconditions.checkNotNull(sdpFile, "SDP must not be null");
    this.app = Preconditions.checkNotNull(app, "app is null");
    this.sdpVersion = sdpVersion;
    Preconditions.checkNotNull(appSoftwareMap, "Software map must not be null");

    this.appSoftwareMap = new HashMap<ModuleInfo, SoftwareEntry>(appSoftwareMap);
    this.bootSoftwareMap = new HashMap<ModuleInfo, SoftwareEntry>(bootSoftwareMap);

    // Get modules to be upgraded
    preUpgradeModules.addAll(appSoftwareMap.keySet());
    preUpgradeModules.removeAll(Collections.singleton(null));// Remove all null
    // module

    // Create a random writable directory for extracting firmware file from SDP
    firmwareDir = new File(UIUtils.getLocalStorageDir().getPath() + "/SDP" + System.currentTimeMillis());
    firmwareDir.mkdir();

    setUserCanCancel(false);
  }

  /**
   * Display a message and save it to log.
   */
  private void logMsg(String message) {
    setMessage(message);
    log.info(message);
  }

  @Override
  protected FileEndInfo[] doInBackground() throws Exception {
    pauseInactivityListener();
    
    ArrayList<FileEndInfo> endInfos = new ArrayList<FileEndInfo>();

    log.info("Upgrading task started");
    
    enterUpgradeMode();

    Thread.sleep(3000);// Give RTU enough time to initialise modules

    checkModuleList(preUpgradeModules);

    /* Store SDP version into RTU. */
    writeSDPVersion();
    
    writeUpgradeScripts();
    
    /* Programming bootloader for all slave modules. */
    programBooterloader(endInfos);

    /* Programming application for all modules. */
    programApplicaiton(endInfos);

    exitUpgradeMode();

    return endInfos.toArray(new FileEndInfo[endInfos.size()]);
  }

  private int originalInactivityMins = -1;
  
  private void pauseInactivityListener() {
    ConnectionPreference pref = PreferenceManager.INSTANCE.getPreference(ConnectionPreference.ID);
    originalInactivityMins = pref.getInactivityMins();
    pref.setInactivityMins(0); // disable inactivity action
    log.info("Paused inactivity listener");
  }
  
  private void resumeInactivityListener() {
    ConnectionPreference pref = PreferenceManager.INSTANCE.getPreference(ConnectionPreference.ID);
    if(originalInactivityMins > 0) {
      pref.setInactivityMins(originalInactivityMins);
    }
    
    log.info("Resumed inactivity listener");
  }

  private void writeUpgradeScripts() throws Exception {
    File script = extractUpgradeScripts(sdpFile, firmwareDir.getPath());
    transferFile(CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC, script);
    
    if(script.exists()) {
      FileTransferService
      .writeFileToRTUAndWait(CommsUtil.getFileTransfer(rtu.getComms()), 
          CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC, "../application/"+script.getName(), 60, script);
    }
  }
  
  static File extractUpgradeScripts(final File sdpFile, final String destDir) {
    /* Extract upgrade script from SDP*/
    File mcmFile = new File(destDir, "MCMBoard_temp.zip");
    File scriptFile = new File(destDir, "update_mcm.sh");
    ZipUtil.unpackEntry(sdpFile, "MCMBoard.zip", mcmFile);
    ZipUtil.unpackEntry(mcmFile, scriptFile.getName(), scriptFile);
    mcmFile.delete();
    return scriptFile;
  }

  private void programApplicaiton(ArrayList<FileEndInfo> endInfos) throws Exception {
    
    FileEndInfo result;
    
    logMsg("Start programming modules, num:" + preUpgradeModules.size());
    
    for (ModuleInfo module : preUpgradeModules) {
      result = programModule(module,this.appSoftwareMap);
      
      if(result != null) {
        endInfos.add(result);
      }
    }
  }

  private void programBooterloader(ArrayList<FileEndInfo> results) throws Exception, IOException,
      SerializationException, InterruptedException {
    if(bootSoftwareMap == null) {
      log.warn("Bootloaders not found. Skipped programming bootloaders.");
      return;
    }
      
    FileEndInfo result;
    boolean bootAppProgrammed = false;
    logMsg("Start programming the bootloader of slave modules, num:" + preUpgradeModules.size());
    
    for (ModuleInfo module : preUpgradeModules) {
      if(module.isMCM()) 
       continue;
      
      result = programModule(module,this.bootSoftwareMap);
      
      if(result != null) {
        results.add(result);
        bootAppProgrammed = true;
      }
    }
    
    if(bootAppProgrammed) {
      /* Restart to write bootloader region*/
      upgradecmd.warmRestart();
      
      // Wait RTU to restart
      CTH_RUNNINGAPP newState = RTURestartSupport.waitingRestart(this, TIMEOUT);
      // Verify upgrade mode
      if (newState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
        throw new Exception("Fail to enter upgrade mode. Check the state of RTU!");
      }
    }
  }

  private FileEndInfo programModule(ModuleInfo module, HashMap<ModuleInfo, SoftwareEntry> softwareMap) throws Exception{
    UpgradeStatus upgradState;
    FileEndInfo result = null;
    
    /* Extract slave firmware from SDP */
    setMessage("Extracting firmware for " + module);
    SoftwareEntry softwareEntry = softwareMap.get(module);
    log.info(String.format("Start upgrading module:%s to version: %s", module, softwareEntry));
    if (softwareEntry == null) {
      log.fatal(String.format("Module: %s is not upgraded cause no software entry is found", module));
      return null;
    }
    //ZipUtil.unzip(sdpFile.getPath(), softwareEntry.getEntryName(), firmwareDir.getPath());
    ZipUtil.unpackEntry(sdpFile, softwareEntry.getEntryName(), new File(firmwareDir.getPath(), softwareEntry.getEntryName()));

    /* Transfer extracted firmware */
    CTH_TRANSFERTYPE fileType = module.isMCM() 
        ? CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_MCM
        : CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_SLAVE;
    result = transferFile(fileType, new File(firmwareDir, softwareEntry.getEntryName()));
    logMsg("Firmware \"" + softwareEntry.getEntryName() + "\" has been written.");

    /* Send command to start programming */
    logMsg("Programming board: " + module);
    upgradecmd.cmdUpgradeModule(module.getModuleType(),
        module.getModuleID(), (int) module.getSerialNo());

    /* Waiting upgrade procedure to be finished */
    boolean success = false;
    int retries = MAX_RETRIES;
    while (!isCancelled()) {
      try {
        upgradState = upgradecmd.cmdGetUpradeStatus();
      } catch (Exception e) {
        /* Failed to get status, continue to retry...*/
        if (retries > 0) {
          log.error("Failed to get upgrade status, retries:" + retries);
          retries--;
          continue;
        } else {
          throw e;
        }
      }

      // Stop waiting if unable to retrieve state
      if (upgradState == null) {
        log.fatal("Cannot get upgrade state from RTU!");
        success = false;
        break;
      }

      // Stop waiting if error found
      if (upgradState.hasError()) {
        success = false;
        log.error("Fail to programme board:" + module
            + ". Reported error state:" + upgradState.getStateEnum());
        break;
      }

      // Stop waiting if upgrading is completed
      if (upgradState.isFinshed()) {
        success = true;
        break;
      }

      // Update progress
      setMessage(String.format("Programming %s:  %d%%", 
          ModuleResource.INSTANCE.getModuleShortName(module.getModuleType(), module.getModuleID()), 
          upgradState.progress));
      setProgress(upgradState.progress);
      Thread.sleep(1000);
    }

    if (success) {
      upgradedModules.add(module);
      logMsg("Module:" + module + " has been upgraded");
      Thread.sleep(1000); // Displaying above message for a little while
    }
    
    return result;
  }

  private void writeSDPVersion() throws IOException, SerializationException {
    upgradecmd.cmdSendSDPVersion(sdpVersion);
  }

  /**
   * Checks if all modules are alive and ready for update.
   */
  private void checkModuleList(ArrayList<ModuleInfo> expectedModules) throws IOException, SerializationException {
    ArrayList<ModuleInfo> upgModules = new ArrayList<ModuleInfo>(upgradecmd.cmdGetUpgradeableModules());

    /* Check if all modules to be upgraded are in Online state */
    int index;
    ArrayList<ModuleInfo> missingModules = new ArrayList<ModuleInfo>();
    for (ModuleInfo module : expectedModules) {
      index = upgModules.indexOf(module);
      if (index < 0) {
        missingModules.add(module);
      } else {
        ModuleInfo minfo = upgModules.get(index);
        if (minfo == null || minfo.isPresent() == false) {
          missingModules.add(minfo);
        }
      }
    }

    if (missingModules.isEmpty() == false) {
      log.error(String.format("Missing modules for upgrade: %s",
          Arrays.toString(missingModules.toArray())));
//      if (showMissingModuleDialog(missingModules) == true) {
//        // User cancelled
//        exitUpgradeMode();
//        cancel(true);
//        return null;
//      } else {
//        // Remove missing module from upgrade list
//        preUpgradeModules.removeAll(missingModules);
//      }
    }
  }

  private void enterUpgradeMode() throws Exception {
    UpgradeStatus upgradState;
    
    logMsg("Entering upgrade mode...");
    
    if (upgradecmd.cmdCheckUpgMode()) {
      /* RTU in upgrade mode.Check no upgrading is in progress */
      upgradState = upgradecmd.cmdGetUpradeStatus();
      if (upgradState != null && upgradState.isUpgrading()) {
        throw new IllegalStateException("Illegal state.RTU upgrading is in progress");
      }
      
    } else {
      /* RTU is not in upgrade mode. Put it into upgrade mode... */
      upgradecmd.cmdEnterUpgradeMode();

      // Wait RTU to swap mode
      CTH_RUNNINGAPP newState = RTURestartSupport.waitingRestart(this, TIMEOUT);

      // Verify upgrade mode
      if (newState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
        throw new Exception("Fail to enter upgrade mode. Check the state of RTU!");
      }
    }
  }

  private void exitUpgradeMode() throws Exception {
    // Exit upgrade mode
    logMsg("Exiting upgrade mode...");
    upgradecmd.cmdExitUpgradeMode();

    // Wait RTU to swap mode
    CTH_RUNNINGAPP conState = RTURestartSupport.waitingRestart(this, TIMEOUT);

    // Check normal mode
    if (conState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      log.error("Fail to exit upgrade mode. Check the state of RTU!");
    }
  }

  /*
   * For some modules are missing, shows user a dialog to continue or cancel the
   * whole upgrade task.
   */
  private boolean showMissingModuleDialog(ArrayList<ModuleInfo> missingModules)
      throws InterruptedException, InvocationTargetException {
    RunMissingModuleDialog run = new RunMissingModuleDialog(parent, missingModules);
    SwingUtilities.invokeAndWait(run);
    return run.isUserCancelled();
  }

  @Override
  protected void finished() {
    super.finished();
    
    resumeInactivityListener();

    // Clear temporary file
    if (firmwareDir != null && firmwareDir.exists()) {
      for (File c : firmwareDir.listFiles()) {
        c.delete();
      }

      if (firmwareDir.delete() == false) {
        log.warn("Fail to delete upgrade files at:" + firmwareDir);
      }
    }
  }

  @Override
  protected void failed(Throwable cause) {
    super.failed(cause);
    setUserCanCancel(true);
    
    // Exit upgrade mode
    try {
      upgradecmd.cmdExitUpgradeMode();
    } catch (Exception e) {
      log.error("Failed to exit upgrade mode",e);
    }
    
    log.error("Firmware Upgrade failed", cause);

    ErrorInfo error = new ErrorInfo("Fail",
        "Failed to upgrade RTU firmware!",
        cause.getMessage(),
        null, cause, Level.SEVERE, null);
    JFrame parent = null;
    if (app != null && app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }
    JXErrorPane.showDialog(parent, error);
  }

  @Override
  protected void succeeded(FileEndInfo[] endInfo) {
    String msg = "Firmware upgrade completed!";
    setMessage(msg);
    log.info(msg);

    // Create result message
    StringBuilder sb = new StringBuilder();
    sb.append("<html><p>Firmware upgrade is completed.</p><br>");
    if (upgradedModules.isEmpty()) {
      sb.append("No module has been upgraded!");
    } else {
      sb.append("The following modules have been upgraded:<ul>");
      for (ModuleInfo info : upgradedModules) {
        sb.append("<li>");
        sb.append(info);
        sb.append("</li>");
      }
      sb.append("</ul>");
    }
    sb.append("</html>");

    JPanel msgPanel = new JPanel(new BorderLayout());
    msgPanel.add(BorderLayout.CENTER, new JLabel(sb.toString()));

    String log = getLogFile();
    if(log != null) {
    JPanel logLinkPanel = new JPanel(new FlowLayout());
    logLinkPanel.add(new JLabel("For more detail view the log:"));
    logLinkPanel.add(new JXHyperlink(new FileLinkAction(new File(log))));

    msgPanel.add(BorderLayout.SOUTH, logLinkPanel);

    // Show result dialog
    JOptionPane.showMessageDialog(parent, msgPanel, "Done", JOptionPane.INFORMATION_MESSAGE);
    }
  }
  
  private String getLogFile() {
    Enumeration appders = this.log.getAllAppenders();
    Appender appd = null;
    while(appders.hasMoreElements()) {
      appd = (Appender) appders.nextElement();
      if(appd != null && appd instanceof FileAppender)
        return ((FileAppender)appd).getFile();
    }
    
    return null;
  }

  @Override
  protected void cancelled() {
    super.cancelled();
    String msg = "Firmware upgrade is cancelled!";
    setMessage(msg);
    log.info(msg);
    JOptionPane.showMessageDialog(parent, msg, "Upgrade", JOptionPane.INFORMATION_MESSAGE);
  }


  private static class RunMissingModuleDialog implements Runnable {

    private final ArrayList<ModuleInfo> missingModules;
    private boolean userCancelled;
    private final Component parent;


    public RunMissingModuleDialog(Component parent, ArrayList<ModuleInfo> missingModules) {
      this.missingModules = missingModules;
      this.parent = parent;
    }

    public boolean isUserCancelled() {
      return userCancelled;
    }

    @Override
    public void run() {
      // Compose message
      StringBuilder sb = new StringBuilder();
      sb.append("<html><p>The following modules were not found and will not be upgraded." 
          + " <br>Do you want to continue upgrade?</p>");

      for (ModuleInfo m : missingModules) {
        sb.append("<li>");
        sb.append(ModuleResource.INSTANCE.getModuleShortName(m.getModuleType(), m.getModuleID()));
        sb.append("</li>");
      }
      sb.append("</html>");

      int rtn = JOptionPane.showConfirmDialog(parent,
          sb.toString(),
          "Warning",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.WARNING_MESSAGE);

      if (rtn != JOptionPane.YES_OPTION) {
        userCancelled = true;
      }
    }
  }


  @Override
  public CTH_RUNNINGAPP checkRTUState() {
    try {
      return rtu.getComms().cmdCheckAlive();
    } catch (IOException | SerializationException e) {
      return null;
    }
  }

}
