/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.version.SystemAPIVersion;
import com.lucy.g3.configtool.rtu.sysalarm.SystemAlarmsManager;
import com.lucy.g3.configtool.rtu.upgrade.task.FirmwareUpgradeTask;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.sdp.importer.SdpImporter;
import com.lucy.g3.sdp.model.SDP;
import com.lucy.g3.sdp.model.SDPFinder;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmMCMEnum.SYSALC_MCM;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmSystemEnum.SYSALC_SYSTEM;
import com.lucy.g3.xml.gen.api.IXmlEnum;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.SLAVE_IMAGE_TYPE;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SUBSYSTEM;

/**
 * This class is for loading software distribution package and selecting
 * compatible software version for a list of G3 modules, and also it provide
 * actions and presentation model for upgrade GUI component.
 */
public class SDPUpgrader extends AbstractSubsystem {
  
  public final static String SUBSYSTEM_ID = "subystem.id.SDPUpgrader";
  

  // /**The key value for saving SDP directory in preference.*/
  // final static private String SDP_DIRECTORY_KEY = "sdp.dir";

  // ====== Action keys ======
  public static final String ACTION_KEY_UPGRADE = "upgrade";
  public static final String ACTION_KEY_CLOSE = "close";
  public static final String ACTION_KEY_LOAD = "load";
  public static final String ACTION_KEY_REFRESH = "refresh";
  public static final String ACTION_KEY_CLEAR = "clear";

  // ====== Property Names ======
  public static final String PROPERTY_UPGRADEABLE = "upgradeable";
  public static final String PROPERTY_SDP_FILE_NAME = "sdpFileName";
  public static final String PROPERTY_SDP_FILE_VERSION = "sdpFileVersion";
  public static final String PROPERTY_AUTO_SELECT = "autoSelect";
  
  private static IXmlEnum [][] CHECKED_ALARMS = {
    /*Subsystem, Alarm*/
    {SYS_ALARM_SUBSYSTEM.SYS_ALARM_SUBSYSTEM_MCM, SYSALC_MCM.SYSALC_MCM_MODULE_SERIAL}, 
    {SYS_ALARM_SUBSYSTEM.SYS_ALARM_SUBSYSTEM_SYSTEM, SYSALC_SYSTEM.SYSALC_SYSTEM_ID_SWITCH},
    
  };

  private Logger log = Logger.getLogger(SDPUpgrader.class);

  private SDP sdp;

  /** Main module */
  private ModuleInfo moduleMCM;

  private SoftwareEntry currentMCMVerion;

  /** Slave modules */
  private final SelectionInList<ModuleInfo> slaveModuleSelection;

  /** Slave software selection list */
  private final SelectionInList<SoftwareEntry> slaveSoftwareList;

  /** MCM software selection list */
  private final SelectionInList<SoftwareEntry> mcmSoftwareSelection;

  /** Store the selected software entry for each module */
  private final HashMap<ModuleInfo, SoftwareEntry> upgradeMap;

  /** Current selected system API */
  private SystemAPIVersion selectedAPI;

  private String sdpFileName;
  private String sdpFileVersion;
  private boolean autoSelect = true;

  private final PropertyChangeListener slaveSWSelectionPCL = new SlaveSWSelectionPCL();

  private final  G3RTU rtu;


  public SDPUpgrader(IContext context, G3RTU rtu) {
    super(context,SUBSYSTEM_ID);
    this.rtu = Preconditions.checkNotNull(rtu, "rtu msut not be null");

    slaveModuleSelection = new SelectionInList<ModuleInfo>();
    mcmSoftwareSelection = new SelectionInList<SoftwareEntry>();
    upgradeMap = new HashMap<ModuleInfo, SoftwareEntry>();
    slaveSoftwareList = new SelectionInList<SoftwareEntry>();

    mcmSoftwareSelection.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        new MCMSWSelectionPCL());

    slaveModuleSelection.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        new SlaveModuleSelectionPCL());

    slaveSoftwareList.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        slaveSWSelectionPCL);

    updateModuleList();

  }

  public SelectionInList<ModuleInfo> getSlaveModuleSelection() {
    return slaveModuleSelection;
  }

  public SelectionInList<SoftwareEntry> getSlaveSWListModel() {
    return slaveSoftwareList;
  }

  public SelectionInList<SoftwareEntry> getMcmSoftwareSelection() {
    return mcmSoftwareSelection;
  }

  /**
   * Get the selected software version entry for specific module
   *
   * @param module
   * @return the software entry selected for the module. Null if there is no
   *         software ever selected.
   */
  public SoftwareEntry getSelectedSoftware(ModuleInfo module) {
    return upgradeMap.get(module);
  }

  public String getSdpFileName() {
    return sdpFileName;
  }

  public boolean isAutoSelect() {
    return autoSelect;
  }

  public void setAutoSelect(boolean autoSelect) {
    Object oldValue = isAutoSelect();
    this.autoSelect = autoSelect;
    doAutoSelect();
    firePropertyChange(PROPERTY_AUTO_SELECT, oldValue, autoSelect);
  }

  /**
   * Getter method of {@link #PROPERTY_UPGRADEABLE}.
   * @return
   */
  public boolean isUpgradeable() {
    ConnectionState state = getConnectionState();
    return checkStateUpgradable(state);
  }

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_UPGRADEABLE)
  public void refresh() {
    updateModuleList();
  }

  @org.jdesktop.application.Action
  public void load() {
    File sdpFile = DialogUtils.showFileChooseDialog(getMainFrame(),
        "Choose Software Distribution Package",
        "Select", G3Files.FC_FILTER_G3SDP);

    /* Load selected SDP file */
    if (sdpFile != null) {
      try {
        loadSDPFile(sdpFile);
        refresh();

      } catch (Exception e) {
        String err = "<html>Failed to open the SDP file:<p>\"" + sdpFile+"\"</p></html>";
        log.error(err, e);
        MessageDialogs.error(err, e);
      }
    }
  }
  

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_UPGRADEABLE)
  public void upgrade() {
    if(checkReadyForUpgrade() == false)
      return;

    // Build confirm dialog
    JTable checkListTable = new JTable(new UpgradeCheckListTableModel(upgradeMap));
    checkListTable.setFillsViewportHeight(true);
    checkListTable.setPreferredScrollableViewportSize(new Dimension(400, 150));
    checkListTable.getColumnModel().getColumn(1).setPreferredWidth(180);
    checkListTable.getColumnModel().getColumn(2).setPreferredWidth(180);

    JPanel message = new JPanel(new BorderLayout(0, 10));
    message.add(BorderLayout.NORTH,
        new JLabel("<html><b>The RTU will be set to OFF mode to do the following software upgrade.</b></html>"));
    message.add(BorderLayout.SOUTH,
        new JLabel("<html><b>Do you want to continue?</b></html>"));
    message.add(BorderLayout.CENTER, new JScrollPane(checkListTable));

    // Show confirm dialog
    int rtn = JOptionPane.showConfirmDialog(getMainFrame(), message, "Confirm Upgrade",
        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
    if (rtn != JOptionPane.YES_OPTION) {
      return;
    }

    // Execute task
    getCommsTaskService().execute(new FirmwareUpgradeTask(getApp(),
        rtu, upgradeMap, getSoftwareForBootloader(upgradeMap),
        new File(sdp.getSDPFilePath()), sdp.getRevision()));
    log.info("Upgrading task scheduled");
  }

  private boolean checkStateUpgradable(ConnectionState state) {
    String error = null;
    if (!ConnectionState.isConnected(state)) {
      error = "No connection";
    }
  
    if (sdp == null) {
      error = "No software package";
    }
  
    if (error != null) {
      log.debug("No upgradable: " + error);
      return false;
    } else {
      return true;
    }
  }

  private boolean checkReadyForUpgrade() {
    if (checkStateUpgradable(getConnectionState()) == false) {
      showError("Cannot upgrade. Invalid state: " + getConnectionState());
      return false;
    }

    // Check MCM version selected
    if (getMcmSoftwareSelection().hasSelection() == false) {
      showError("Please select the version of MCM software!");
      return false;
    }
    
    // Check MCM version selected
    if (upgradeMap.isEmpty()) {
      showError("No software has been selected for upgrade!");
      return false;
    }
    
    if(checkAlarmRaised()) {
      showError("Cannot upgrade. Please check the raised alarms!");
      return false;
    }
    
    return true;
  }

  private boolean checkAlarmRaised() {
    SystemAlarmsManager alarmMgr = getContext().getComponent(SystemAlarmsManager.SUBSYSTEM_ID);
    
    for(int i = 0; i < CHECKED_ALARMS.length; i ++) {
      if(alarmMgr.checkAlarmRaised((SYS_ALARM_SUBSYSTEM) CHECKED_ALARMS[i][0], CHECKED_ALARMS[i][1]))
        return true;
    }
    
    return false;
  }

  private HashMap<ModuleInfo, SoftwareEntry> getSoftwareForBootloader(HashMap<ModuleInfo, SoftwareEntry> upgradeMap) {
    HashMap<ModuleInfo, SoftwareEntry> bootMap = new HashMap<>();
    
    Set<ModuleInfo> modules = upgradeMap.keySet();
    for (ModuleInfo m: modules) {
      SoftwareEntry[] sw = SDPFinder.INSTANCE.findBootSoftwareEntries(sdp, m.getSystemAPI(), 
          m.getFeatureVersion(), m.getModuleType());
      if(sw != null && sw.length > 0 && sw[0] != null) {
        bootMap.put(m, sw[0]);
      }else {
        log.warn("Bootloader application is not found for: "+ m);
      }
    }
    
    return bootMap;
  }

  /** @see <a href="http://10.11.253.18/work_packages/2771">refs #2771</a>. */
  private boolean checkOffLocalRemoteState() {
    boolean success = true;

    if (getConnectionState().isNormalMode()) {
      try {
        OLR_STATE olrState = rtu.getComms().getFwUpgrade().cmdGetOLR();
        if (olrState != OLR_STATE.OLR_STATE_OFF) {
          showError("Invalid RTU state: " + olrState + ".\nPlease set RTU to OFF state before upgrading software!");
          success = false;
        }
      } catch (IOException | SerializationException e) {
        showError("Failed get Off/Local/Remote State:" + e.getMessage());
        success = false;
      }
    }

    return success;
  }

  private void showError(String error) {
    JOptionPane.showMessageDialog(getMainFrame(), error,
        "Error", JOptionPane.ERROR_MESSAGE);
  }

  @org.jdesktop.application.Action
  public void close() {
    try {
      loadSDPFile(null);

    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  @org.jdesktop.application.Action
  public void clear() {
    slaveSoftwareList.clearSelection();
  }

  /**
   * Load SDP package into {@code SDPModel}
   *
   * @param file
   * @throws Exception
   */
  public void loadSDPFile(File file) throws Exception {
    upgradeMap.clear();

    if (file == null) {
      setSdpFileName("");
      sdp = null;

    } else {
      sdp = SdpImporter.createFromFile(file);
      if(sdp == null) {
        log.error("SDP not loaded");
        return;
      } else {
        setSdpFileName(file.getAbsolutePath());
        log.info("SDP has been loaded.");
      }
    }

    updateMCMSoftwareList();
    setSdpFileVersion(sdp == null ? "" : sdp.getRevision());
    
    firePropertyChange(PROPERTY_UPGRADEABLE, null, isUpgradeable());
  }

  private void setSdpFileName(String sdpFileName) {
    Object oldValue = getSdpFileName();
    this.sdpFileName = sdpFileName;
    firePropertyChange(PROPERTY_SDP_FILE_NAME, oldValue, sdpFileName);
  }

  // private void setUpgradeable(boolean upgradeable) {
  // Object oldValue = isUpgradeable();
  // this.upgradeable = upgradeable;
  //
  // if(upgradeable == false){
  // slaveModuleSelection.clearSelection();
  // }
  // firePropertyChange(PROPERTY_UPGRADEABLE, oldValue, upgradeable);
  // }

  /**
   * MCM software list depends on the current MCM Info.
   */
  private void updateMCMSoftwareList() {
    mcmSoftwareSelection.getList().clear();

    if (moduleMCM != null) {
      // Add current MCM version
      currentMCMVerion = new SoftwareEntry("Do Not Upgrade MCM",
          MODULE.MODULE_MCM,
          moduleMCM.getVersion(), moduleMCM.getSystemAPI(), 
          SLAVE_IMAGE_TYPE.SLAVE_IMAGE_TYPE_APPLICATION, 
          moduleMCM.getFeatureVersion());
      // Add current running version
      mcmSoftwareSelection.getList().add(currentMCMVerion);

      // Add versions in SDP
      if (sdp != null) {
        SoftwareEntry[] entries = SDPFinder.INSTANCE.findSoftwareEntries(sdp, moduleMCM.getFeatureVersion(),
            moduleMCM.getModuleType());
        mcmSoftwareSelection.getList().addAll(Arrays.asList(entries));
        log.debug("MCM firmware entries count in SDP: " + entries.length);
      }
    }

    if (mcmSoftwareSelection.getSize() > 1) {
      // Select first MCM in SDP
      mcmSoftwareSelection.setSelectionIndex(1);
    } else if (mcmSoftwareSelection.getSize() > 0) {
      // No MCM in SDP, select current one
      mcmSoftwareSelection.setSelectionIndex(0);
    }
  }

  /**
   * Update slave software list which should match with the current selected
   * slave module.
   */
  private void updateSlaveSoftwareList() {
    // It is essential to stop observing before clearing the current list
    // cause it is not expected to clear the current selected software from
    // upgrade map.
    slaveSoftwareList.removePropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        slaveSWSelectionPCL);
    slaveSoftwareList.getList().clear();

    if (selectedAPI == null) {
      return;
    }

    ModuleInfo selectSlave = slaveModuleSelection.getSelection();

    // Add "None" Item
    if (selectSlave != null) {
      slaveSoftwareList.getList().add(null);
    }

    if (selectSlave != null && sdp != null) {
      // Find compatible software entries in SDP
      SoftwareEntry[] entries = SDPFinder.INSTANCE.findSoftwareEntries(sdp, selectedAPI,
          selectSlave.getFeatureVersion(), selectSlave.getModuleType());
      if (entries == null || entries.length == 0) {
        log.warn("No compatible software entry in SDP for: " + selectSlave);
      } else {
        slaveSoftwareList.getList().addAll(Arrays.asList(entries));
      }

      // Retrieve already selected entry
      SoftwareEntry selectEntry = upgradeMap.get(selectSlave);
      if (selectEntry != null) {
        slaveSoftwareList.setSelection(selectEntry);
      }
      else {
        slaveSoftwareList.setSelectionIndex(0);// Select "None" item
      }

    } else if (selectSlave != null) {
      slaveSoftwareList.setSelectionIndex(0); // Select "None" item
    }

    // Observe again
    slaveSoftwareList.addPropertyChangeListener(
        SelectionInList.PROPERTY_SELECTION,
        slaveSWSelectionPCL);
  }

  /**
   * Auto select the latest software for each slave module.
   */
  private void doAutoSelect() {
    int size = slaveModuleSelection.getSize();

    if (isAutoSelect() && size > 0 && selectedAPI != null) {
      for (int i = 0; i < size; i++) {
        ModuleInfo slave = slaveModuleSelection.getElementAt(i);
        // Find compatible software entries in SDP
        if (slave != null && sdp != null) {
          SoftwareEntry[] entries = SDPFinder.INSTANCE.findSoftwareEntries(sdp, selectedAPI,
              slave.getFeatureVersion(),
              slave.getModuleType());
          // Select the latest version for this slave module
          if (entries.length > 0) {
            if (entries[0] == null) {
              log.error(slave + "'s software is null. size: " + entries.length);
            }
            upgradeMap.put(slave, entries[0]);
          } else {
            upgradeMap.remove(slave);
          }
        }
      }
    }

    if (size > 0) {
      slaveModuleSelection.fireContentsChanged(0, size - 1);
      updateSlaveSoftwareList();
    }
  }

  /**
   * Update modules list depends on current connection state.
   */
  private void updateModuleList() {
    if (isDisconnected()) {
      // Clear module list
      moduleMCM = null;
      slaveModuleSelection.getList().clear();
      upgradeMap.clear();

      updateMCMSoftwareList();
    } else if (isConnected()) {
      getCommsTaskService().execute(new ModuleListUpdateTask(getApp()));
    }
  }


  
  public String getSdpFileVersion() {
    return sdpFileVersion;
  }

  
  private void setSdpFileVersion(String sdpFileVersion) {
    Object oldValue = this.sdpFileVersion;
    this.sdpFileVersion = sdpFileVersion;
    firePropertyChange(PROPERTY_SDP_FILE_VERSION, oldValue, sdpFileVersion);
  }


  private class MCMSWSelectionPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      SoftwareEntry selectedMCMSoftware = mcmSoftwareSelection.getSelection();
      if (selectedMCMSoftware == null) {
        log.info("No MCM software has been selected!");
      } else {
        log.info("MCM software has been selected:" + selectedMCMSoftware);
      }

      if (selectedMCMSoftware != null) {
        if (currentMCMVerion != selectedMCMSoftware && selectedMCMSoftware != null) {
          upgradeMap.put(moduleMCM, selectedMCMSoftware);
        } else {
          upgradeMap.remove(moduleMCM);
        }
        selectedSystemAPI(selectedMCMSoftware.getSystemAPI());

      } else {
        upgradeMap.remove(moduleMCM);
        selectedSystemAPI(null);
      }
    }

    private void selectedSystemAPI(SystemAPIVersion selectedAPI) {
      if (SDPUpgrader.this.selectedAPI != selectedAPI) {
        SDPUpgrader.this.selectedAPI = selectedAPI;

        log.info("Selelcted System API: " + selectedAPI);

        // Clear the mapped software of all slaves
        SoftwareEntry mcmEntry = upgradeMap.get(moduleMCM);
        upgradeMap.clear();
        if(mcmEntry != null)
        upgradeMap.put(moduleMCM, mcmEntry);

        doAutoSelect();

        // Notify module list that current upgrade map has changed
        int size = slaveModuleSelection.getSize();
        if (size > 0) {
          slaveModuleSelection.fireContentsChanged(0, size - 1);
        }
      }
    }
  }

  private class SlaveModuleSelectionPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateSlaveSoftwareList();
    }
  }

  private class SlaveSWSelectionPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      ModuleInfo slave = slaveModuleSelection.getSelection();
      SoftwareEntry softwareEntry = slaveSoftwareList.getSelection();

      if (slave != null && softwareEntry != null) {
        upgradeMap.put(slave, softwareEntry);
        log.info("Select software \"" + softwareEntry + "\" for module \"" + slave + "\"");

      } else if (slave != null && softwareEntry == null) {
        upgradeMap.remove(slave);
        log.info("Unselect software for module \"" + slave + "\"");
      }

      assert upgradeMap.size() <= slaveModuleSelection.getSize() + 1;

      slaveModuleSelection.fireSelectedContentsChanged();
    }
  }

  private class ModuleListUpdateTask extends Task<Collection<ModuleInfo>, Void> {

    public ModuleListUpdateTask(Application app) {
      super(app);
    }

    @Override
    protected Collection<ModuleInfo> doInBackground() throws Exception {
      return rtu.getComms().getFwUpgrade().cmdGetUpgradeableModules();
    }

    @Override
    protected void failed(Throwable cause) {
      log.error("Fail to refresh module list: " + cause.getMessage());
    }

    @Override
    protected void succeeded(Collection<ModuleInfo> result) {
      moduleMCM = null;
      slaveModuleSelection.getList().clear();

      if (result != null) {
        for (ModuleInfo module : result) {
          if (module == null) {
            continue;
          }

          if (module.getModuleType() == MODULE.MODULE_MCM) {
            moduleMCM = module;
          } else {
            slaveModuleSelection.getList().add(module);
          }
        }
      }

      if (moduleMCM == null) {
        log.fatal("MCM module not found in RTU");
      }

      updateMCMSoftwareList();
      log.debug("Module list has been updated");
    }
  }

  private static class UpgradeCheckListTableModel extends DefaultTableModel {

    static String[] COLUMNS = { "Module", "Current Version", "New Version" };

    private HashMap<ModuleInfo, SoftwareEntry> upgradeMap;
    private Object[] modules;


    public UpgradeCheckListTableModel(HashMap<ModuleInfo, SoftwareEntry> upgradeMap) {
      super(COLUMNS, upgradeMap.size());
      modules = upgradeMap.keySet().toArray();
      this.upgradeMap = upgradeMap;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      return false;
    }

    @Override
    public Object getValueAt(int row, int column) {
      String text = "";
      SoftwareEntry entry = upgradeMap.get(modules[row]);
      ModuleInfo info = (ModuleInfo) modules[row];

      switch (column) {
      case 0:
        if (info != null) {
          text = ModuleResource.INSTANCE.getModuleShortName(info.getModuleType(), info.getModuleID());
        }
        break;

      case 1:
        if (info != null) {
          text = info.getVersion().toString() + " [API: " + info.getSystemAPI() + "]";
        }
        break;

      case 2:
        if (entry != null) {
          text = entry.getVersion().toString() + " [API: " + entry.getSystemAPI().toString() + "]";
        }
        break;

      default:
        break;
      }
      return text;
    }
  }


  @Override
  protected void notifyStateChanged(ConnectionState oldState,
      ConnectionState newState) {

    firePropertyChange(PROPERTY_UPGRADEABLE,
        checkStateUpgradable(oldState),
        checkStateUpgradable(newState));

    updateModuleList();
  }

  public RTUInfo getRtuInfo() {
    return rtu.getComms().getRtuInfo();
  }
}
