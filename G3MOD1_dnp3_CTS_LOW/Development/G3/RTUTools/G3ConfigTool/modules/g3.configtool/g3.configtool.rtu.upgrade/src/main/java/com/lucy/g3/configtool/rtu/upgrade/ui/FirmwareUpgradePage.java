/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;

import javax.swing.Action;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXLabel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.binder.BeanBinder;
import com.jgoodies.binding.binder.Binders;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.upgrade.SDPUpgrader;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.ext.swing.splitbutton.JSplitButton;
import com.lucy.g3.gui.common.widgets.ext.swing.splitbutton.SplitButtonActionAdapter;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.sdp.model.impl.SoftwareEntry;

/**
 * A panel for doing RTU software upgrade.
 */
public class FirmwareUpgradePage extends AbstractPage {

  private final SDPUpgrader upgrader;


  public FirmwareUpgradePage(com.lucy.g3.common.context.IContext context, G3RTU rtu) {
    super(null);
    setNodeName("Firmware Upgrade");
    upgrader = context.getComponent(SDPUpgrader.SUBSYSTEM_ID);
  }

  private void createUIComponents() {
    Action loadAction = upgrader.getAction(SDPUpgrader.ACTION_KEY_LOAD);
    String loadActionName = (String) loadAction.getValue(Action.NAME);
    Icon loadActionIcon = (Icon) loadAction.getValue(Action.SMALL_ICON);

    loadAction = new BusyCursorAction(loadAction, this);
    Action closeAction = new BusyCursorAction(upgrader.getAction(SDPUpgrader.ACTION_KEY_CLOSE), this);
    btnLoad = new JSplitButton(loadActionName, loadActionIcon);
    ((JSplitButton) btnLoad).addSplitButtonActionListener(new SplitButtonActionAdapter(loadAction));

    // Create popup menu for split button "New"
    JPopupMenu popup = new JPopupMenu();
    popup.add(loadAction);
    popup.add(closeAction);
    ((JSplitButton) btnLoad).setPopupMenu(popup);

    Action upgAction = new BusyCursorAction(upgrader.getAction(SDPUpgrader.ACTION_KEY_UPGRADE), this);
    btnUpgrade = new JButton(upgAction);

    menuItemRefresh = new JMenuItem(upgrader.getAction(SDPUpgrader.ACTION_KEY_REFRESH));
    menuItemClear = new JMenuItem(upgrader.getAction(SDPUpgrader.ACTION_KEY_CLEAR));

    slaveModuleList = BasicComponentFactory.createList(upgrader.getSlaveModuleSelection(), new SlaveModuleListRenderer());
    slaveSwList = BasicComponentFactory.createList(upgrader.getSlaveSWListModel(), new SoftwareListRenderer());
    comboBoxMCM = BasicComponentFactory.createComboBox(upgrader.getMcmSoftwareSelection());
    comboBoxMCM.setRenderer(new MCMModuleListRenderer());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    p1 = new JPanel();
    panel1 = new JPanel();
    lblFile = new JLabel();
    panel2 = new JPanel();
    lblSDPPath = new JLabel();
    label5 = new JLabel();
    lblSDPVersion = new JLabel();
    label4 = new JLabel();
    lblRTUVersion = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    checkBoxAuto = new JCheckBox();
    label1 = new JLabel();
    lblSelectFirmware = new JXLabel();
    scrollPane0 = new JScrollPane();
    scrollPane1 = new JScrollPane();
    panelUpgrade = new JPanel();
    btnCurrentVer = new JButton();
    JPopupMenu popup1 = new JPopupMenu();
    JPopupMenu popup2 = new JPopupMenu();

    //======== this ========
    setBorder(Borders.DIALOG_BORDER);
    setLayout(new BorderLayout(10, 10));

    //======== p1 ========
    {
      p1.setLayout(new FormLayout(
        "default:grow, $ugap, default:grow",
        "fill:default, $rgap, default, $lgap, fill:default:grow"));
      ((FormLayout)p1.getLayout()).setColumnGroups(new int[][] {{1, 3}});

      //======== panel1 ========
      {
        panel1.setLayout(new FormLayout(
          "default, $lcgap, default:grow",
          "default, $lgap, fill:[12dlu,default], $pgap, default, $lgap, default, $pgap, default, $lgap, default"));

        //---- lblFile ----
        lblFile.setFont(lblFile.getFont().deriveFont(lblFile.getFont().getStyle() | Font.BOLD));
        lblFile.setText("1. Select Software Distribution Package File(.zip)");
        panel1.add(lblFile, CC.xywh(1, 1, 3, 1));
        panel1.add(btnLoad, CC.xy(1, 3));

        //======== panel2 ========
        {
          panel2.setLayout(new FormLayout(
            "default, $lcgap, [50dlu,default], $ugap, default, $lcgap, default:grow",
            "fill:[12dlu,default], $lgap, fill:default"));
          panel2.add(lblSDPPath, CC.xywh(1, 1, 7, 1));

          //---- label5 ----
          label5.setText("Loaded SDP Version:");
          panel2.add(label5, CC.xy(1, 3));

          //---- lblSDPVersion ----
          lblSDPVersion.setFont(lblSDPVersion.getFont().deriveFont(lblSDPVersion.getFont().getStyle() | Font.ITALIC));
          panel2.add(lblSDPVersion, CC.xy(3, 3));

          //---- label4 ----
          label4.setText("Current RTU Software Version:");
          panel2.add(label4, CC.xy(5, 3));

          //---- lblRTUVersion ----
          lblRTUVersion.setFont(lblRTUVersion.getFont().deriveFont(lblRTUVersion.getFont().getStyle() | Font.ITALIC));
          panel2.add(lblRTUVersion, CC.xy(7, 3));
        }
        panel1.add(panel2, CC.xy(3, 3));

        //---- label2 ----
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
        label2.setText("2. Select software version for MCM");
        panel1.add(label2, CC.xywh(1, 5, 3, 1));
        panel1.add(comboBoxMCM, CC.xywh(1, 7, 3, 1));

        //---- label3 ----
        label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
        label3.setText("3. Select software version for slave modules");
        panel1.add(label3, CC.xywh(1, 9, 3, 1));

        //---- checkBoxAuto ----
        checkBoxAuto.setText("Auto select the latest version");
        checkBoxAuto.setHorizontalAlignment(SwingConstants.LEFT);
        checkBoxAuto.setSelected(true);
        panel1.add(checkBoxAuto, CC.xywh(1, 11, 3, 1));
      }
      p1.add(panel1, CC.xywh(1, 1, 3, 1));

      //---- label1 ----
      label1.setText("- Slave Module List -");
      label1.setHorizontalAlignment(SwingConstants.CENTER);
      p1.add(label1, CC.xy(1, 3));

      //---- lblSelectFirmware ----
      lblSelectFirmware.setText("- Available firmware to install -");
      lblSelectFirmware.setTextAlignment(JXLabel.TextAlignment.CENTER);
      lblSelectFirmware.setHorizontalAlignment(SwingConstants.CENTER);
      p1.add(lblSelectFirmware, CC.xy(3, 3));

      //======== scrollPane0 ========
      {

        //---- slaveModuleList ----
        slaveModuleList.setComponentPopupMenu(popup1);
        scrollPane0.setViewportView(slaveModuleList);
      }
      p1.add(scrollPane0, CC.xy(1, 5));

      //======== scrollPane1 ========
      {

        //---- slaveSwList ----
        slaveSwList.setFixedCellHeight(30);
        slaveSwList.setComponentPopupMenu(popup2);
        scrollPane1.setViewportView(slaveSwList);
      }
      p1.add(scrollPane1, CC.xy(3, 5));
    }
    add(p1, BorderLayout.CENTER);

    //======== panelUpgrade ========
    {
      panelUpgrade.setLayout(new FormLayout(
        "default:grow, $lcgap, [70dlu,default], $lcgap, default:grow",
        "fill:[22dlu,default]"));

      //---- btnCurrentVer ----
      btnCurrentVer.setText("Current Version");
      btnCurrentVer.setToolTipText("View current firmware version");
      btnCurrentVer.setVisible(false);
      panelUpgrade.add(btnCurrentVer, CC.xy(1, 1));

      //---- btnUpgrade ----
      btnUpgrade.setText("Start Upgrade");
      btnUpgrade.setFont(btnUpgrade.getFont().deriveFont(btnUpgrade.getFont().getStyle() | Font.BOLD, btnUpgrade.getFont().getSize() + 1f));
      panelUpgrade.add(btnUpgrade, CC.xy(3, 1));
    }
    add(panelUpgrade, BorderLayout.SOUTH);

    //======== popup1 ========
    {

      //---- menuItemRefresh ----
      menuItemRefresh.setText("Refresh");
      popup1.add(menuItemRefresh);
    }

    //======== popup2 ========
    {

      //---- menuItemClear ----
      menuItemClear.setText("Clear");
      popup2.add(menuItemClear);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p1;
  private JPanel panel1;
  private JLabel lblFile;
  private JButton btnLoad;
  private JPanel panel2;
  private JLabel lblSDPPath;
  private JLabel label5;
  private JLabel lblSDPVersion;
  private JLabel label4;
  private JLabel lblRTUVersion;
  private JLabel label2;
  private JComboBox<SoftwareEntry> comboBoxMCM;
  private JLabel label3;
  private JCheckBox checkBoxAuto;
  private JLabel label1;
  private JXLabel lblSelectFirmware;
  private JScrollPane scrollPane0;
  private JList slaveModuleList;
  private JScrollPane scrollPane1;
  private JList slaveSwList;
  private JPanel panelUpgrade;
  private JButton btnCurrentVer;
  private JButton btnUpgrade;
  private JMenuItem menuItemRefresh;
  private JMenuItem menuItemClear;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  protected void init() {
    initComponents();
    initComponentsBinding();
  }

  private void initComponentsBinding() {
    PresentationModel<SDPUpgrader> adapter = new PresentationModel<SDPUpgrader>(upgrader);
    ValueModel vm = adapter.getModel(SDPUpgrader.PROPERTY_AUTO_SELECT);
    Bindings.bind(checkBoxAuto, vm);

    vm = adapter.getModel(SDPUpgrader.PROPERTY_SDP_FILE_NAME);
    Bindings.bind(lblSDPPath, vm);
    
    vm = adapter.getModel(SDPUpgrader.PROPERTY_SDP_FILE_VERSION);
    Bindings.bind(lblSDPVersion, vm);
    
    BeanBinder binder1 = Binders.binderFor(upgrader.getRtuInfo());
    binder1.bindProperty(RTUInfo.PROPERTY_SDP_VERSION).to(lblRTUVersion);

    vm = adapter.getModel(SDPUpgrader.PROPERTY_UPGRADEABLE);
    PropertyConnector.connectAndUpdate(vm, slaveModuleList, "enabled");
    PropertyConnector.connectAndUpdate(vm, slaveSwList, "enabled");
    PropertyConnector.connectAndUpdate(vm, comboBoxMCM, "enabled");
    PropertyConnector.connectAndUpdate(vm, checkBoxAuto, "enabled");

  }


  private class MCMModuleListRenderer extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      if (value != null && value instanceof SoftwareEntry) {
        SoftwareEntry sw = (SoftwareEntry) value;
        setText(String.format("%s [%s]", sw.getEntryName(), sw.getVersion()));
      }
      return comp;
    }
  }
  
  private class SlaveModuleListRenderer extends DefaultListCellRenderer {

    private final StringBuilder builder = new StringBuilder();


    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      
      if (value != null && value instanceof ModuleInfo) {
        ModuleInfo info = (ModuleInfo) value;
        String moduleName = ModuleResource.INSTANCE.getModuleShortName(info.getModuleType(), info.getModuleID());
        
        // Set displayed text
        builder.setLength(0);// Clear
        builder.append(moduleName);
        builder.append("  >>  ");
        SoftwareEntry selectedEntry = upgrader.getSelectedSoftware(info);
        if (selectedEntry != null) {
          builder.append(String.format("%s [%s]", selectedEntry.getVersion(), selectedEntry.getEntryName()));
        } else {
          builder.append("None");
        }
        setText(builder.toString());

        // Set tooltips text
        builder.setLength(0);// Clear
        builder.append("<html>");
        builder.append("Name: ");
        builder.append(moduleName);
        builder.append("<br>Feature:");
        builder.append(info.getFeatureVersion());
        builder.append("<br><b>- Before Upgrade -</b>");
        builder.append("<ul>");
        builder.append("<li>System API: ");
        builder.append(info.getSystemAPI());
        builder.append("</li>");
        builder.append("<li>Version:");
        builder.append(info.getVersion());
        builder.append("</li>");
        builder.append("</ul>");
        builder.append("<b>- After Upgrade -</b>");
        builder.append("<ul>");
        builder.append("<li>Version:");
        builder.append(selectedEntry == null ? "N/A" : selectedEntry.getVersion());
        builder.append("</li>");
        builder.append("<li>System API: ");
        builder.append(selectedEntry == null ? "N/A" : selectedEntry.getSystemAPI());
        builder.append("</li>");
        builder.append("</ul>");
        builder.append("</html>");
        
        setToolTipText(builder.toString());
        builder.setLength(0);// Clear

        // Set displayed icon
        setIcon(ModuleResource.INSTANCE.getModuleIconThumbnail(info.getModuleType()));
      }

      return comp;
    }
  }

  private class SoftwareListRenderer extends JCheckBox implements ListCellRenderer {

    public SoftwareListRenderer() {
      setOpaque(false);
      // setSelectedIcon(selectImage);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      // super.getListCellRendererComponent(list, value, index, isSelected,
      // cellHasFocus);
      if (value != null && value instanceof SoftwareEntry) {
        setText(((SoftwareEntry) value).getVersion().toString());
      } else if (value == null) {
        setText("None");
      } else {
        setText("");
      }

      setSelected(isSelected);
      return this;
    }

  }

  @Override
  public boolean hasError() {
    return false;
  }

  // private class ToggleSelectionModel extends DefaultListSelectionModel{
  //
  // boolean gestureStarted = false;
  //
  // @Override
  // public void setSelectionInterval(int index0, int index1) {
  // if(!gestureStarted){
  // if (isSelectedIndex(index0)) {
  // super.removeSelectionInterval(index0, index1);
  // } else {
  // super.addSelectionInterval(index0, index1);
  // }
  // }
  // gestureStarted = true;
  // }
  //
  // @Override
  // public void setValueIsAdjusting(boolean isAdjusting) {
  // if (isAdjusting == false) {
  // gestureStarted = false;
  // }
  // }
  //
  // }

}
