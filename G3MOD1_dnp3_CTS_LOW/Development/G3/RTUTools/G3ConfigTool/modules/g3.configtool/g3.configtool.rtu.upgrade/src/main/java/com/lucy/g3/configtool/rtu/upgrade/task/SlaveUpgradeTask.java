/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.upgrade.task;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.comms.protocol.exceptions.UpgradeStateErrorException;
import com.lucy.g3.rtu.comms.service.filetransfer.FileEndInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.rtu.upgrade.UpgradeStatus;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.rtu.filetransfer.FileWritingTask;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * Task for upgrading a slave module firmware without checking version
 * compatibility.
 */
public class SlaveUpgradeTask extends FileWritingTask {

  private Logger log = Logger.getLogger(SlaveUpgradeTask.class);

  private final G3RTU g3RTU;
  private JFrame parent;

  private FileEndInfo endInfo;

  private Map<ModuleInfo, File> updateMap;


  public SlaveUpgradeTask(Application app,
      G3RTU g3RTU,
      Map<ModuleInfo, File> updateMap) {

    super(app, g3RTU.getComms().getFileTransfer(), CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_SLAVE);
    this.g3RTU = Preconditions.checkNotNull(g3RTU, "upgcmd is null");
    this.updateMap = Preconditions.checkNotNull(updateMap, "updateMap is null");

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    }

    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, null, null));
    setUserCanCancel(false);
  }

  @Override
  protected FileEndInfo[] doInBackground() throws Exception {
    // Check RTU is in upgrading state
    UpgradeStatus state = g3RTU.getComms().getFwUpgrade().cmdGetUpradeStatus();
    if (state != null && state.isUpgrading()) {
      throw new IllegalStateException("Cannot enter into MCM upgrading mode. MCM is now doing upgrade.");
    }

    ModuleInfo module;
    File firmwareFile;

    Iterator<ModuleInfo> allModules = updateMap.keySet().iterator();
    while (allModules.hasNext() && !isCancelled()) {
      // Get a module for upgrade
      module = allModules.next();

      if (module == null) {
        continue;
      }

      // Get firmware file
      firmwareFile = updateMap.get(module);
      if (firmwareFile == null
          || firmwareFile.exists() == false
          || firmwareFile.isFile() == false) {
        log.error(module + "is not upgraded cause firmware file not found at: "
            + firmwareFile);
        continue;
      }

      // Transfer firmware file
      endInfo = transferFile(CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_SLAVE, firmwareFile);

      // Send command to upgrade the selected module
      g3RTU.getComms().getFwUpgrade().cmdUpgradeModule(module.getModuleType(), module.getModuleID(), (int) module.getSerialNo());

      // Checks upgrading progress and wait it to be finished.
      while (!isCancelled()) {
        state = g3RTU.getComms().getFwUpgrade().cmdGetUpradeStatus();

        // Stop if not able to retrieve state
        if (state == null) {
          throw new UpgradeStateErrorException("Cannot get upgrade state!");
        }

        // Stop waiting if an error occurs
        if (state.hasError()) {
          throw new UpgradeStateErrorException("Fail to programm board. State:" + state.getStateEnum());
        }

        // Stop waiting if upgrading is completed
        if (state.isFinshed()) {
          break;
        }

        String moduleName = ModuleResource.INSTANCE.getModuleShortName(module.getModuleType(), module.getModuleID());

        setMessage(String.format("Programming %s:  %d%%",
            moduleName,
            state.progress));
        setProgress(state.progress);
        Thread.sleep(1000);
      }

      log.info(module + " has been upgraded with: " + firmwareFile);
    }

    return new FileEndInfo[] { endInfo };
  }

  @Override
  protected void failed(final Throwable cause) {
    super.failed(cause);
    ErrorInfo error = new ErrorInfo("Upgrade Fail",
        "Fail to upgrade slave module",
        cause.getMessage(), null, cause, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, error);
  }

  @Override
  protected void succeeded(FileEndInfo[] endInfo) {
    String msg = "Slave firmware upgrade is finished.";
    setMessage(msg);
    log.info(msg);
    JOptionPane.showMessageDialog(parent, msg,
        "Upgrade Done", JOptionPane.INFORMATION_MESSAGE);
  }

  @Override
  protected void cancelled() {
    super.cancelled();
    setMessage("Upgrade cancelled");
    log.info("Upgrade cancelled");
    JOptionPane.showMessageDialog(parent, "Upgrade is cancelled!",
        "Upgrade", JOptionPane.INFORMATION_MESSAGE);
  }
}
