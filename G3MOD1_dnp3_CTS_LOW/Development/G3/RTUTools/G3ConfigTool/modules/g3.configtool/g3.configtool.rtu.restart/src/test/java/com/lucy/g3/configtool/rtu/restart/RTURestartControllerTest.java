/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.restart;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.configtool.rtu.restart.RTURestartController;
import com.lucy.g3.test.support.CommsTestSupport;

/**
 * The Class RTURestartController Test.
 */
public class RTURestartControllerTest extends Model {

  private RTURestartController fixture;


  @Before
  public void setUp() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    fixture = new RTURestartController(null);
  }

  @After
  public void tearDown() throws Exception {
  }


  @Test
  public void testGetAction() {
    String[] actionKeys = {
        RTURestartController.ACTION_KEY_REBOOT_RTU,
        RTURestartController.ACTION_KEY_RESTART_RTU,
        RTURestartController.ACTION_KEY_SHUTDOWN_RTU,
        RTURestartController.ACTION_KEY_SHOW_RESTART_OPTION,
    };

    for (String key : actionKeys) {
      assertNotNull("action for the key:\"" + key + "\" is null.", fixture.getAction(key));
    }
  }

}
