/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.tasks;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.jdesktop.application.Application;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;
import org.jdesktop.application.TaskListener.Adapter;

import com.lucy.g3.configtool.rtu.restart.RTURestartTask;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.ProtocolException;
import com.lucy.g3.rtu.comms.service.rtu.control.RestartAPI;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_ACKNACKCODE;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * The Class RTURestartTaskTest.
 */
public class RTURestartTaskTest implements RestartAPI {

  public void test() throws InterruptedException, ExecutionException {
    Application app = Application.getInstance();
    RTURestartTask task = new RTURestartTask(app, null, this, RestartMode.RESTART, true);
    task.addTaskListener(listener);
    task.execute();
    Thread.sleep(5000);
  }

  public static void main(String[] args) throws InterruptedException, ExecutionException {
    new RTURestartTaskTest().test();
  }

  @Override
  public CTH_RUNNINGAPP cmdCheckAlive() throws IOException, ProtocolException {
    System.out.println("CheckAlive");
    return CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP;
  }

  @Override
  public void cmdRestart(RestartMode mode, boolean force) throws IOException, ProtocolException {
    System.out.println("Send command. mode:" + mode + " force:" + force);
    if (error) {
      error = false;
      throw new ProtocolException.NackException(CTH_ACKNACKCODE.CTH_NACK_RESTARTREFUSAL);
    }
  }


  private boolean error = true;

  private Adapter<CTH_RUNNINGAPP, Void> listener = new TaskListener.Adapter<CTH_RUNNINGAPP, Void>() {

    @Override
    public void succeeded(TaskEvent<CTH_RUNNINGAPP> event) {
      System.out.println("task succeeded");
    }

    @Override
    public void failed(TaskEvent<Throwable> event) {
      System.out.println("task failed");
    }

    @Override
    public void cancelled(TaskEvent<Void> event) {
      System.out.println("task cancelled");
    }

    @Override
    public void interrupted(TaskEvent<InterruptedException> event) {
      System.out.println("task interrupted");
    }

    @Override
    public void finished(TaskEvent<Void> event) {
      System.out.println("task finished");
    }

  };
}
