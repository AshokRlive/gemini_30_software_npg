/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.restart;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.jdesktop.application.Action;

import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventConstants;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.manager.G3RTUFactory;


/**
 *
 */
public class RTURestartController extends AbstractSubsystem {
  public final static String SUBSYSTEM_ID = "subystem.id.RTURestartController";
  
  public static final String ACTION_KEY_RESTART_RTU          = "restartRtu";
  public static final String ACTION_KEY_SHUTDOWN_RTU         = "shutdownRtu";
  public static final String ACTION_KEY_REBOOT_RTU           = "rebootRtu";
  public static final String ACTION_KEY_SHOW_RESTART_OPTION  = "showRestartOption";

  public RTURestartController(com.lucy.g3.common.context.IContext context) {
    super(context, SUBSYSTEM_ID);
    subscribeEvents();
  }

  private void subscribeEvents() {
    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_RESTART_RTU, new IEventHandler() {
      
      
      @Override
      public void handleEvent(Event event) {
        G3Protocol.RestartMode mode = (RestartMode) event.getProperty(EventTopics.EVT_PROPERTY_RESTART_MODE);
        if(mode == null) {
          Object data = event.getProperty(EventConstants.EVENT_DATA);
          if(data instanceof G3Protocol.RestartMode)
            mode = (RestartMode) data;
        }
        
        String  newIP = (String) event.getProperty(EventTopics.EVT_PROPERTY_IP_ADDRESS);
        restartRTU(mode, newIP);
      }
    });
  }
  
  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void restartRtu() {
    if (checkIsConnected() == false) {
      return;
    }
    if (confirmRestart(RestartMode.RESTART)) {
      restartRTU(RestartMode.RESTART);
    }
  }

  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void rebootRtu() {
    if (checkIsConnected() == false) {
      return;
    }

    if (confirmRestart(RestartMode.REBOOT)) {
      restartRTU(RestartMode.REBOOT);
    }
  }

  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void shutdownRtu() {
    if (checkIsConnected() == false) {
      return;
    }

    if (confirmRestart(RestartMode.SHUT_DOWN)) {
      restartRTU(RestartMode.SHUT_DOWN);
    }
  }

  @Action(enabledProperty = PROPERTY_CONNECTED)
  
  public void showRestartOption() {
    JComboBox<RestartMode> combModes = new JComboBox<>(new RestartMode[] {
        RestartMode.RESTART,
        RestartMode.REBOOT,
    });
    int option = JOptionPane.showConfirmDialog(
        getMainFrame(),
        new Object[] { combModes },
        "Choose Restart Option",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE);

    if (option == JOptionPane.YES_OPTION) {
      RestartMode mode = (RestartMode) combModes.getSelectedItem();
      if (confirmRestart(mode)) {
        restartRTU(mode);
      }
    }
  }

  private boolean confirmRestart(RestartMode mode) {
    String title = mode == RestartMode.SHUT_DOWN ? "Shut Down" : "Restart";
    String operation = mode == RestartMode.SHUT_DOWN ? "shut down" : "restart";

    int option = JOptionPane.showConfirmDialog(
        getMainFrame(),
        "Do you really want to " + operation + " RTU?",
        title,
        JOptionPane.YES_NO_OPTION,
        JOptionPane.WARNING_MESSAGE);
    return option == JOptionPane.YES_OPTION;
  }

  
  /**
   * Restarts RTU straight forward.
   */
  public void restartRTU(RestartMode mode) {
    restartRTU(mode, null);
  }

  /**
   * Restarts RTU straight forward and then connects ConfigTool to the new IP
   * address.
   */
  public void restartRTU(RestartMode mode, String newIP) {
    getCommsTaskService().execute(new RTURestartTask(getApp(), getContext(), G3RTUFactory.getComms(), mode, newIP));
  }

  
  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    
  }

}

