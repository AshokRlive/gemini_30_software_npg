/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.restart;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.protocol.exceptions.ProtocolException.NackException;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.comms.service.RTURestartSupport;
import com.lucy.g3.rtu.comms.service.RTURestartSupport.IWaitingRestartInvoker;
import com.lucy.g3.rtu.comms.service.rtu.control.RestartAPI;
import com.lucy.g3.rtu.comms.shared.LoginAccount;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_ACKNACKCODE;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * <p>
 * Task for rebooting the connected RTU.
 * </p>
 */
public class RTURestartTask extends Task<CTH_RUNNINGAPP, Void> implements IWaitingRestartInvoker {

  private Logger log = Logger.getLogger(RTURestartTask.class);

  private static int RESTART_TIMEOUT_SECS = 60;// seconds

  private static int REBOOT_TIMEOUT_SECS = 120;// seconds

  private final JFrame parent;

  private final RestartMode mode;

  private final boolean showSuccessDialog;

  private final ConnectionManager connectMgr;

  private final LoginAccount loginAccount;

  private final RestartAPI restartCmd;

  private String newIP;

  private boolean force = false;
  private boolean retry = false;


  public RTURestartTask(Application app, IContext context, RestartAPI restartCmd, RestartMode mode) {
    this(app, context,restartCmd, mode, null);
  }

  public RTURestartTask(Application app, IContext context,RestartAPI restartCmd, RestartMode mode, String newIP) {
    this(app, context,restartCmd, mode, true);
    this.newIP = newIP;
  }

  public RTURestartTask(Application app, IContext context,RestartAPI restartCmd, RestartMode mode, boolean showSuccessDialog) {
    super(app);
    this.connectMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
    this.restartCmd = Preconditions.checkNotNull(restartCmd, "restartCmd is null");
    this.showSuccessDialog = showSuccessDialog;
    this.mode = mode == null ? RestartMode.RESTART : mode;

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    // Keep the login account for setting up connection after restart.
    loginAccount = connectMgr == null ? null : connectMgr.getLoginAccount();

    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
    setUserCanCancel(true);
  }

  public void setForce(boolean force) {
    this.force = force;
  }

  @Override
  protected void succeeded(CTH_RUNNINGAPP result) {
    String msg = "";
    switch (mode) {
    case REBOOT:
      msg = "The RTU has been cold restarted.";
      break;
    case RESTART:
      msg = "The RTU has been warm restarted.";
      break;
    case SHUT_DOWN:
      msg = "The RTU has been shut down.";
      break;
    case ENTER_UPGRADE:
      msg = "The RTU is now in Upgrade Mode.";
      break;
    case EXIT_UPGRADE:
      msg = "The RTU is now in Normal Mode";
      break;
    default:
      msg = "The RTU has been restarted";
    }

    log.info(msg);

    if (showSuccessDialog) {
      JOptionPane.showMessageDialog(parent, msg,
          "Done", JOptionPane.INFORMATION_MESSAGE);
    }

    // Start setting up new connection
    if (result != null) {
      if (connectMgr != null) {
        connectMgr.setupConnection(loginAccount, false);
      }
    }
  }

  @Override
  protected void failed(final Throwable cause) {
    log.error("Failed to restart/shutdown RTU", cause);
    JOptionPane.showMessageDialog(parent,
        "Operation failed: " + mode.getName() + "\nReason:" + cause.getMessage(),
        "Failure",
        JOptionPane.ERROR_MESSAGE);
  }

  @Override
  public CTH_RUNNINGAPP doInBackground() throws Exception {
    // Logging
    setMessage(mode.getDescription());
    log.info(mode.getDescription());

    // Disconnect ConfigTool from RTU
    if (connectMgr != null) {
      connectMgr.disconnect();
    }

    // Send restart command
    sendRequest();

    // Update host IP
    redirectToNewIP();

    CTH_RUNNINGAPP newState;

    if (mode.isRestart()) {
      // Waiting restart complete
      setMessage("Waiting for RTU restart...");

      int timeoutSecs = (mode == RestartMode.REBOOT)
          ? REBOOT_TIMEOUT_SECS
          : RESTART_TIMEOUT_SECS;
      newState = RTURestartSupport.waitingRestart(this, timeoutSecs);
      checkNewState(newState);

    } else {
      // Shut down
      newState = null;//CTH_RUNNINGAPP.DISCONNECTED;
    }

    return newState;
  }

  private void checkNewState(CTH_RUNNINGAPP newState) throws Exception {
    if (mode == RestartMode.ENTER_UPGRADE
        && newState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      /*Fail to set upgrade mode*/
      throw new Exception("Fail to set upgrade mode ON. Check "
          + "if the updater is running in RTU!");

    } else if (mode == RestartMode.EXIT_UPGRADE
        &&  newState != CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      /* Fail to exit upgrade mode*/
      throw new Exception("Fail to set upgrade mode OFF. "
          + "Check if the main application is running in RTU!");
    }
  }

  private void sendRequest() throws Exception {
    try {
      restartCmd.cmdRestart(mode, force);
    } catch (NackException nack) {
      if (nack.nackCode == CTH_ACKNACKCODE.CTH_NACK_RESTARTREFUSAL) {
        askUserRetry(nack);

        if (retry) {
          restartCmd.cmdRestart(mode, true);
        } else {
          throw nack;
        }

      } else {
        throw nack;
      }
    }
  }

  private void askUserRetry(NackException nack) throws InvocationTargetException, InterruptedException {
    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        String[] buttons = { "Force " + mode.getName(), "No", "Cancel" };

        int rc = JOptionPane.showOptionDialog(parent,
            "Request rejected. There might be a switch operation in progress.\n"
                + "Do you want to force RTU to " + mode.getName() + "?",
            "Request Rejected",
            JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.WARNING_MESSAGE,
            null, buttons, buttons[1]);

        if (rc == 0) {
          retry = true;
        } else if (rc == 2) {
          cancel(true);
        }
      }
    });
  }

  /* Reset the server address in case of IP change */
  private void redirectToNewIP() {
    if (!Strings.isBlank(newIP)) {
      ConnectionPreference pref = PreferenceManager.INSTANCE.getPreference(ConnectionPreference.ID);
      pref.setServer(newIP);
      log.info("Set new server: " + newIP);
    }

  }

  @Override
  public CTH_RUNNINGAPP checkRTUState() {
    try {
      return restartCmd.cmdCheckAlive();
    } catch (IOException | SerializationException e) {
      return null;
    }
  }
}
