/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.points;

import java.util.Date;

import org.apache.log4j.Logger;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.configtool.subsys.realtime.formatter.PointValueFormatter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData.PointStatus;
import com.lucy.g3.rtu.config.constants.VirtualPointType;

/**
 * Adapt a list of virtual points to table model which presents the real time
 * information in columns.
 */
public final class RTVirtualPointsTableModel extends AbstractTableAdapter<IPointData>
    implements RTPointsTableModel, ISupportHighlight {

  // @formatter:off
  public static final int COLUMN_GROUPID      = 0;
  public static final int COLUMN_DESCRIPTION  = 1;
  public static final int COLUMN_TYPE         = 2;
  public static final int COLUMN_VALUE        = 3;
  public static final int COLUMN_LABEL        = 4;
  public static final int COLUMN_SCALE        = 5;
  public static final int COLUMN_ONLINE       = 6;
  public static final int COLUMN_FILTER       = 7;
  public static final int COLUMN_OVERFLOW     = 8;
  public static final int COLUMN_CHATTER      = 9;
  public static final int COLUMN_LAST_UPDATE  = 10;
  // @formatter:on
  private static final String[] COLUMN_NAMES = {
      "[Group,ID]", "Description", "Type", "Value", "Label", "Scale", "Online", "Filtered", "Out of Range", "Chatter",
      "Last Update"
  };
  
  private Logger log = Logger.getLogger(RTVirtualPointsTableModel.class);
  
  private final PointValueFormatter format = PointValueFormatter.getFormatter();

  private boolean showUnit = true;


  public RTVirtualPointsTableModel(ArrayListModel<IPointData> listModel) {
    super(listModel, COLUMN_NAMES);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    IPointData pointData = getRow(rowIndex);
    if (pointData == null) {
      log.fatal("virtual point must not be null");
      return null;
    }

    switch (columnIndex) {
    case COLUMN_GROUPID:
      return pointData.getGroupIDString();

    case COLUMN_TYPE:
      return ((VirtualPointType)pointData.getType()).getName();

    case COLUMN_VALUE:
      return format.format(pointData, pointData.getValue(), showUnit);
      
    case COLUMN_LABEL:
      return pointData.getLabel() == null ? PointValueFormatter.NULL_VALUE : pointData.getLabel();

    case COLUMN_SCALE:
      return pointData.getScale();

    case COLUMN_CHATTER:
      return pointData.getStatus(PointStatus.CHATTER);

    case COLUMN_FILTER:
      return pointData.getStatus(PointStatus.FILTER);

    case COLUMN_OVERFLOW:
      return pointData.getStatus(PointStatus.OVERFLOW);

    case COLUMN_ONLINE:
      return pointData.getStatus(PointStatus.ONLINE);

    case COLUMN_DESCRIPTION:
      return pointData.getDescription();

    case COLUMN_LAST_UPDATE:
      return pointData.getTimestamp();

    default:
      return "";
    }
  }

  @Override
  public IPointData getPointData(int row) {
    if (row < getRowCount() && row >= 0) {
      return getRow(row);
    } else {
      return null;
    }
  }

  @Override
  public int[] getRegFilterColumns() {
    return new int[] { COLUMN_DESCRIPTION };
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
    // case COLUMN_ONLINE :
    case COLUMN_FILTER:
    case COLUMN_OVERFLOW:
    case COLUMN_CHATTER:
      return Boolean.class;
    case COLUMN_LAST_UPDATE:
      return Date.class;
    default:
      return super.getColumnClass(columnIndex);
    }
  }

  @Override
  public boolean shouldHightlight(int rowIndex, int columnIndex) {
    IPointData point = getRow(rowIndex);
    if (point == null) {
      return false;
    }

    switch (columnIndex) {
    case COLUMN_VALUE:
      return point.isValueChanged();
    case COLUMN_CHATTER:
      return point.isStatusChanged(PointStatus.CHATTER);
    case COLUMN_FILTER:
      return point.isStatusChanged(PointStatus.FILTER);
    case COLUMN_ONLINE:
      return point.isStatusChanged(PointStatus.ONLINE);
    case COLUMN_OVERFLOW:
      return point.isStatusChanged(PointStatus.OVERFLOW);
    default:
      return false;
    }
  }

  @Override
  public void clearChangeHistory() {
    for (int size = getRowCount(), i = 0; i < size; i++) {
      IPointData vp = getRow(i);
      if (vp != null) {
        vp.resetValueChanged();
      }
    }
  }

}
