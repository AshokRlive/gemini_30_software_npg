/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.modules;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.JXTitledSeparator;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.binder.Binders;
import com.jgoodies.binding.binder.PresentationModelBinder;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.sysalarm.ModuleAlarmsTableModel;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.BooleanToIconConverter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.BooleanToStringConverter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.ObjectToStringConverter;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleDetail;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfoAPI;
import com.lucy.g3.rtu.config.module.res.ModuleResource;

/**
 * Dialog for displaying the detail information of a module, including
 * status,alarms, etc.
 */
public class ModuleInfoDialog extends JDialog {

  private static Logger log = Logger.getLogger(ModuleInfoDialog.class);

  private static final String TEXT_YES = "Yes";
  private static final String TEXT_NO = "No";
  private static final String TEXT_NA = "n/a";

  private ImageIcon iconOk;
  private ImageIcon iconError;

  private final ArrayListModel<ModuleAlarm> alarmList = new ArrayListModel<ModuleAlarm>();

  @SuppressWarnings("unchecked")
  private final ModuleAlarmsTableModel alarmTableModel = new ModuleAlarmsTableModel(alarmList);

  private Task<?, ?> refreshTask;

  private final PropertyChangeListener outSyncHandler = new OutofSyncHandler();
  private final PropertyChangeListener onlineHandler = new OfflineHandler();

  private final ModuleInfo mInfo;

  private final PresentationModel<ModuleInfo> pmMState;
  private final PresentationModel<ModuleDetail> pmMDetail;

  private CANStatDialog canstatDlg = null;

  private final ModuleInfoAPI moduleCmd;


  public ModuleInfoDialog(Frame owner, ModuleInfoAPI moduleCmd, ModuleInfo mInfo,
      ImageIcon iconOk,
      ImageIcon iconError) {
    super(owner);
    this.moduleCmd = Preconditions.checkNotNull(moduleCmd, "moduleCmd is null");
    this.iconOk = iconOk;
    this.iconError = iconError;
    this.mInfo = mInfo;

    pmMState = new PresentationModel<ModuleInfo>(mInfo);
    pmMDetail = new PresentationModel<ModuleDetail>(mInfo.getDetail());

    initComponents();
    initComponentsBinding();

    // Module m = ModuleFactory.instance.createModule(mInfo.getModuleType(),
    // mInfo.getModuleID());
    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());

    if (mInfo != null) {
      // Set title and icon
      setTitle(ModuleResource.INSTANCE.getModuleShortName(mInfo.getModuleType(), mInfo.getModuleID()));
      setIconImage(ModuleResource.INSTANCE.getModuleIconThumbnail(mInfo.getModuleType()).getImage());

      if (mInfo.isOutOfSynch()) {
        setDialogOutOfSynch();// Set outOfSync state
      } else {
        refreshActionPerformed();// Refresh all
      }

      setDialogOnline(mInfo.isPresent());
    }
    this.setModal(false);

  }

  private void initComponentsBinding() {
    // Construct converters
    BindingConverter<?, ?> obj2Str = new ObjectToStringConverter(TEXT_NA);
    BindingConverter<?, ?> bool2Text = new BooleanToStringConverter(TEXT_NA, TEXT_YES, TEXT_NO);

    // ====== Binding Module Info ======
    PresentationModelBinder binder = Binders.binderFor(pmMState);
    // Binding label text
    binder.bindBeanProperty(ModuleInfo.PROPERTY_MODULE_TYPE).converted(obj2Str).to(labelMType);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_MODULE_ID).converted(obj2Str).to(labelMID);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_BOOTAPI).converted(obj2Str).to(labelBLAPI);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_BOOTVERSION).converted(obj2Str).to(labelBLVer);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_FEATUREVERSION).converted(obj2Str).to(labelFeature);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_VERSION).converted(obj2Str).to(labelFwVer);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_SERIALNO).converted(obj2Str).to(labelSerial);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_SUPPLIER_ID).converted(obj2Str).to(lblSupplier);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_SYSTEMAPI).converted(obj2Str).to(labelSystemAPI);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_INTERNALSTATE).converted(obj2Str).to(labelInternalState);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_FSM_STATE).converted(obj2Str).to(labelLifecycle);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_OK).converted(bool2Text).to(labelOKStatus);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_DETECTED).converted(bool2Text).to(labelDetected);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_CONFIGURED).converted(bool2Text).to(labelConfigured);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_REGISTERED).converted(bool2Text).to(labelRegistered);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_PRESENT).converted(bool2Text).to(labelPresent);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_DISABLED).converted(bool2Text).to(labelDisabled);
    binder.bindBeanProperty(ModuleInfo.PROPERTY_STATE_TIMESYNC).converted(bool2Text).to(labelTimeSync);

    /*
     * Binding label icon!Note ConverterValueModel is a ComponentValueModel
     * which has Visibility and Enable property and by default they are true.
     */
    BindingConverter<?, ?> bool2Icon = new BooleanToIconConverter(iconOk, iconError);
    Bindings.bind(labelOKStatus, "icon", new ConverterValueModel(pmMState.getModel(ModuleInfo.PROPERTY_STATE_OK),
        bool2Icon));
    Bindings.bind(labelDetected, "icon", new ConverterValueModel(
        pmMState.getModel(ModuleInfo.PROPERTY_STATE_DETECTED), bool2Icon));
    Bindings.bind(labelConfigured, "icon",
        new ConverterValueModel(pmMState.getModel(ModuleInfo.PROPERTY_STATE_CONFIGURED), bool2Icon));
    Bindings.bind(labelRegistered, "icon",
        new ConverterValueModel(pmMState.getModel(ModuleInfo.PROPERTY_STATE_REGISTERED), bool2Icon));
    Bindings.bind(labelPresent, "icon", new ConverterValueModel(pmMState.getModel(ModuleInfo.PROPERTY_STATE_PRESENT),
        bool2Icon));

    // Handle OutOfSync event
    pmMState.getModel(ModuleInfo.PROPERTY_OUT_OF_SYNCH).addValueChangeListener(outSyncHandler);
    pmMState.getModel(ModuleInfo.PROPERTY_STATE_PRESENT).addValueChangeListener(onlineHandler);

    // ====== Binding Module Details ======
    binder = Binders.binderFor(pmMDetail);
    binder.bindBeanProperty(ModuleDetail.PROPERTY_ARCH).converted(obj2Str).to(labelArch);
    binder.bindBeanProperty(ModuleDetail.PROPERTY_SVNREVISION_BL).converted(obj2Str).to(labelBLSVN);
    binder.bindBeanProperty(ModuleDetail.PROPERTY_SVNREVISION_APP).converted(obj2Str).to(labelAppSVN);

    // TODO Hide temporarily cause it is not implemented by Slave
    // binder.bindBeanProperty(ModuleDetail.PROPERTY_RUNNINGTIMESECS)
    // .converted(sec2Str).to(labelRunningTime);
  }

  private void setDialogOutOfSynch() {
    btnRefresh.setEnabled(false);
    btnCopy.setEnabled(false);
    btnCanStat.setEnabled(false);
    btnAck.setEnabled(false);

    if (canstatDlg != null) {
      canstatDlg.dispose();
      canstatDlg = null;
    }

    alarmList.clear();
    String title = getTitle() + " [Out of Date]";
    setTitle(title);

    release();
    log.info(mInfo + " info dialog is set out of synch!");
  }

  private void setDialogOnline(boolean online) {
    btnCanStat.setEnabled(online);

    if (!online && canstatDlg != null) {
      canstatDlg.dispose();
      canstatDlg = null;
    }

    if (!online) {
      alarmList.clear();
      log.info(mInfo + " info dialog is offfline!");
    }

    Component[] children = contentPanel.getComponents();
    for (int i = 0; i < children.length; i++) {
      /* Gray out all labels except "present" if module go offline */
      if (children[i] instanceof JLabel
          && children[i] != labelPresent
          && children[i] != label13) {
        children[i].setEnabled(online);
      }
    }

  }

  private TaskService getService(Application app) {
    return app.getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
  }

  private void refreshActionPerformed() {
    if (refreshTask == null || refreshTask.isDone()) {
      // Clear alarm list before updating.
      alarmList.clear();

      // Run background task to update
      Application app = Application.getInstance();
      refreshTask = new ModuleInfoRefreshTask(app, moduleCmd, alarmList, mInfo, false);
      getService(app).execute(refreshTask);
    }
  }

  private void copyAlarmsActionPerformed() {
    if (alarmList.isEmpty() == false) {
      alarmTableModel.copytoClipboard();
      log.info("Alarm list has been copied to clipboard.");
    } else {
      log.warn("Nothing is copied. Alarm list is empty.");
    }
  }

  private void btnCanStatActionPerformed(ActionEvent e) {
    // Create dialog if it is not created.
    if (canstatDlg == null || canstatDlg.isDisplayable() == false) {
      canstatDlg = new CANStatDialog(this, moduleCmd, mInfo.getModuleType(), mInfo.getModuleID());
      canstatDlg.setModal(false);
    }

    canstatDlg.setVisible(true);
  }

  private void btnAckActionPerformed(ActionEvent e) {
    if (refreshTask == null || refreshTask.isDone()) {
      // Clear alarm list before updating.
      alarmList.clear();

      Application app = Application.getInstance();
      refreshTask = new ModuleInfoRefreshTask(app, moduleCmd, alarmList, mInfo, true);
      getService(app).execute(refreshTask);
    }
  }

  /**
   * Close this dialog and clear event listener.
   */
  private void close() {
    if (refreshTask != null && !refreshTask.isDone()) {
      refreshTask.cancel(true);
      refreshTask = null;
      log.info("Alarm Update Task is cancelled");
    }

    release();
    dispose();
  }

  private void release() {
    pmMState.release();
    pmMState.setBean(null);

    pmMDetail.release();
    pmMDetail.setBean(null);
  }

  private void createUIComponents() {
    alarmTable = new JXTable(alarmTableModel) {

      @Override
      public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {

        // Set tool-tip for table cell
        Component comp = super.prepareRenderer(renderer, row, column);
        if (comp instanceof JComponent) {
          JComponent jc = (JComponent) comp;
          jc.setToolTipText(alarmTableModel.getTooltip(row, column));
        }

        // Set background colour
        comp.setBackground(alarmTableModel.getAlarmEntryBG(row));
        comp.setForeground(alarmTableModel.getAlarmEntryFG(row));
        return comp;
      }
    };

    ((JXTable)alarmTable).setColumnControlVisible(true);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    buttonBar = new JPanel();
    btnHelp = new JButton();
    okButton = new JButton();
    contentPanel = new JPanel();
    JXTitledSeparator xTitledSeparator1 = new JXTitledSeparator();
    JLabel label1 = new JLabel();
    labelMType = new JLabel();
    JLabel label3 = new JLabel();
    labelMID = new JLabel();
    JLabel label6 = new JLabel();
    labelFeature = new JLabel();
    JLabel label2 = new JLabel();
    labelSerial = new JLabel();
    JLabel label18 = new JLabel();
    labelArch = new JLabel();
    label22 = new JLabel();
    lblSupplier = new JLabel();
    JXTitledSeparator xTitledSeparator2 = new JXTitledSeparator();
    JLabel label20 = new JLabel();
    labelFwVer = new JLabel();
    JLabel label5 = new JLabel();
    labelBLVer = new JLabel();
    JLabel label4 = new JLabel();
    labelSystemAPI = new JLabel();
    JLabel label7 = new JLabel();
    labelBLAPI = new JLabel();
    label21 = new JLabel();
    labelAppSVN = new JLabel();
    label19 = new JLabel();
    labelBLSVN = new JLabel();
    JXTitledSeparator xTitledSeparator3 = new JXTitledSeparator();
    JLabel label8 = new JLabel();
    labelOKStatus = new JLabel();
    label13 = new JLabel();
    labelPresent = new JLabel();
    JLabel label12 = new JLabel();
    labelRegistered = new JLabel();
    JLabel label11 = new JLabel();
    labelDetected = new JLabel();
    JLabel label10 = new JLabel();
    labelConfigured = new JLabel();
    JLabel label15 = new JLabel();
    labelLifecycle = new JLabel();
    JLabel label14 = new JLabel();
    labelDisabled = new JLabel();
    JLabel label9 = new JLabel();
    labelInternalState = new JLabel();
    JLabel label16 = new JLabel();
    labelTimeSync = new JLabel();
    label17 = new JLabel();
    labelRunningTime = new JLabel();
    JScrollPane scrollPane1 = new JScrollPane();
    panel1 = new JPanel();
    btnRefresh = new JButton();
    btnCopy = new JButton();
    btnAck = new JButton();
    btnCanStat = new JButton();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        close();
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      //======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$lcgap, default, $glue, $button",
            "pref"));

        //---- btnHelp ----
        btnHelp.setText("Help");
        buttonBar.add(btnHelp, CC.xy(2, 1));

        //---- okButton ----
        okButton.setText("Close");
        okButton.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            close();
          }
        });
        buttonBar.add(okButton, CC.xy(4, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.PAGE_END);

      //======== contentPanel ========
      {
        contentPanel.setLayout(new FormLayout(
            "default, $lcgap, [50dlu,default], $lcgap, 50dlu, $ugap, left:default, $lcgap, default:grow, $lcgap, default",
            "default, 3*($lgap, fill:default), $ugap, 3*(fill:default, $lgap), fill:default, $ugap, 5*(fill:default, $lgap), fill:default, $ugap, fill:100dlu:grow"));

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Hardware");
        contentPanel.add(xTitledSeparator1, CC.xywh(1, 1, 11, 1));

        //---- label1 ----
        label1.setText("Module Type:");
        contentPanel.add(label1, CC.xy(1, 3));
        contentPanel.add(labelMType, CC.xy(3, 3));

        //---- label3 ----
        label3.setText("Module ID:");
        contentPanel.add(label3, CC.xy(7, 3));
        contentPanel.add(labelMID, CC.xywh(9, 3, 3, 1));

        //---- label6 ----
        label6.setText("Feature Revision:");
        contentPanel.add(label6, CC.xy(1, 5));
        contentPanel.add(labelFeature, CC.xywh(3, 5, 3, 1));

        //---- label2 ----
        label2.setText("Serial Number: ");
        contentPanel.add(label2, CC.xy(7, 5));
        contentPanel.add(labelSerial, CC.xywh(9, 5, 3, 1));

        //---- label18 ----
        label18.setText("Architecture:");
        contentPanel.add(label18, CC.xy(1, 7));
        contentPanel.add(labelArch, CC.xywh(3, 7, 3, 1));

        //---- label22 ----
        label22.setText("Supplier ID:");
        contentPanel.add(label22, CC.xy(7, 7));
        contentPanel.add(lblSupplier, CC.xywh(9, 7, 3, 1));

        //---- xTitledSeparator2 ----
        xTitledSeparator2.setTitle("Firmware");
        contentPanel.add(xTitledSeparator2, CC.xywh(1, 9, 11, 1));

        //---- label20 ----
        label20.setText("Firmware Version:");
        contentPanel.add(label20, CC.xy(1, 11));
        contentPanel.add(labelFwVer, CC.xywh(3, 11, 3, 1));

        //---- label5 ----
        label5.setText("Bootloader Version:");
        contentPanel.add(label5, CC.xy(7, 11));
        contentPanel.add(labelBLVer, CC.xywh(9, 11, 3, 1));

        //---- label4 ----
        label4.setText("System API Version:");
        contentPanel.add(label4, CC.xy(1, 13));
        contentPanel.add(labelSystemAPI, CC.xywh(3, 13, 3, 1));

        //---- label7 ----
        label7.setText("Bootloader API Version:");
        contentPanel.add(label7, CC.xy(7, 13));
        contentPanel.add(labelBLAPI, CC.xywh(9, 13, 3, 1));

        //---- label21 ----
        label21.setText("Firmware SVN:");
        contentPanel.add(label21, CC.xy(1, 15));
        contentPanel.add(labelAppSVN, CC.xywh(3, 15, 3, 1));

        //---- label19 ----
        label19.setText("Bootloader SVN:");
        contentPanel.add(label19, CC.xy(7, 15));
        contentPanel.add(labelBLSVN, CC.xywh(9, 15, 3, 1));

        //---- xTitledSeparator3 ----
        xTitledSeparator3.setTitle("Status");
        contentPanel.add(xTitledSeparator3, CC.xywh(1, 17, 11, 1));

        //---- label8 ----
        label8.setText("Active:");
        contentPanel.add(label8, CC.xy(1, 19));

        //---- labelOKStatus ----
        labelOKStatus.setToolTipText("This flag indicates  if this module is running OK.");
        contentPanel.add(labelOKStatus, CC.xy(3, 19));

        //---- label13 ----
        label13.setText("Present:");
        contentPanel.add(label13, CC.xy(7, 19));

        //---- labelPresent ----
        labelPresent.setToolTipText("This flag indicates if this module is now present on bus.");
        contentPanel.add(labelPresent, CC.xywh(9, 19, 3, 1));

        //---- label12 ----
        label12.setText("Registered:");
        contentPanel.add(label12, CC.xy(1, 21));

        //---- labelRegistered ----
        labelRegistered.setToolTipText("This flag indicates if this module has been registered.");
        contentPanel.add(labelRegistered, CC.xy(3, 21));

        //---- label11 ----
        label11.setText("Detected:");
        contentPanel.add(label11, CC.xy(7, 21));

        //---- labelDetected ----
        labelDetected.setToolTipText("This flag indicates if the module has ever been detected on bus.");
        contentPanel.add(labelDetected, CC.xywh(9, 21, 3, 1));

        //---- label10 ----
        label10.setText("Configured:");
        contentPanel.add(label10, CC.xy(1, 23));

        //---- labelConfigured ----
        labelConfigured.setToolTipText("This flag indicates if this module has been configured in current configuration file.");
        contentPanel.add(labelConfigured, CC.xywh(3, 23, 3, 1));

        //---- label15 ----
        label15.setText("Life-cycle State:");
        contentPanel.add(label15, CC.xy(7, 23));

        //---- labelLifecycle ----
        labelLifecycle.setToolTipText("The state of this module's machine state managed by MCM.");
        contentPanel.add(labelLifecycle, CC.xywh(9, 23, 3, 1));

        //---- label14 ----
        label14.setText("Disabled:");
        contentPanel.add(label14, CC.xy(1, 25));

        //---- labelDisabled ----
        labelDisabled.setToolTipText("This flag indicates if this module has been disabled. A module that is not registered, configured or has incompatible API could be disabled by force.");
        contentPanel.add(labelDisabled, CC.xywh(3, 25, 3, 1));

        //---- label9 ----
        label9.setText("Internal State:");
        contentPanel.add(label9, CC.xy(7, 25));

        //---- labelInternalState ----
        labelInternalState.setToolTipText("The internal state reported from this module.");
        contentPanel.add(labelInternalState, CC.xywh(9, 25, 3, 1));

        //---- label16 ----
        label16.setText("Time Synchronized:");
        contentPanel.add(label16, CC.xy(1, 27));
        contentPanel.add(labelTimeSync, CC.xywh(3, 27, 3, 1));

        //---- label17 ----
        label17.setText("Running Time:");
        label17.setVisible(false);
        contentPanel.add(label17, CC.xy(7, 27));

        //---- labelRunningTime ----
        labelRunningTime.setVisible(false);
        contentPanel.add(labelRunningTime, CC.xywh(9, 27, 3, 1));

        //======== scrollPane1 ========
        {
          scrollPane1.setBorder(new TitledBorder("Alarms"));

          //---- alarmTable ----
          alarmTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
          scrollPane1.setViewportView(alarmTable);
        }
        contentPanel.add(scrollPane1, CC.xywh(1, 29, 9, 1));

        //======== panel1 ========
        {
          panel1.setBorder(new EmptyBorder(10, 5, 5, 5));
          panel1.setLayout(new FormLayout(
              "default",
              "2*(default, $lgap), default, $ugap, default"));

          //---- btnRefresh ----
          btnRefresh.setText("Refresh");
          btnRefresh.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              refreshActionPerformed();
            }
          });
          panel1.add(btnRefresh, CC.xy(1, 1));

          //---- btnCopy ----
          btnCopy.setText("Copy Alarms");
          btnCopy.setToolTipText("Copy alarms to clipboard");
          btnCopy.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              copyAlarmsActionPerformed();
            }
          });
          panel1.add(btnCopy, CC.xy(1, 3));

          //---- btnAck ----
          btnAck.setText("Ack  Alarms");
          btnAck.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              btnAckActionPerformed(e);
            }
          });
          panel1.add(btnAck, CC.xy(1, 5));

          //---- btnCanStat ----
          btnCanStat.setText("CAN Stat");
          btnCanStat.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
              btnCanStatActionPerformed(e);
            }
          });
          panel1.add(btnCanStat, CC.xy(1, 7));
        }
        contentPanel.add(panel1, CC.xy(11, 29));
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton okButton;
  private JPanel contentPanel;
  private JLabel labelMType;
  private JLabel labelMID;
  private JLabel labelFeature;
  private JLabel labelSerial;
  private JLabel labelArch;
  private JLabel label22;
  private JLabel lblSupplier;
  private JLabel labelFwVer;
  private JLabel labelBLVer;
  private JLabel labelSystemAPI;
  private JLabel labelBLAPI;
  private JLabel label21;
  private JLabel labelAppSVN;
  private JLabel label19;
  private JLabel labelBLSVN;
  private JLabel labelOKStatus;
  private JLabel label13;
  private JLabel labelPresent;
  private JLabel labelRegistered;
  private JLabel labelDetected;
  private JLabel labelConfigured;
  private JLabel labelLifecycle;
  private JLabel labelDisabled;
  private JLabel labelInternalState;
  private JLabel labelTimeSync;
  private JLabel label17;
  private JLabel labelRunningTime;
  private JTable alarmTable;
  private JPanel panel1;
  private JButton btnRefresh;
  private JButton btnCopy;
  private JButton btnAck;
  private JButton btnCanStat;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class OutofSyncHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      Object newValue = evt.getNewValue();
      if (newValue != null && (Boolean) evt.getNewValue() == true) {
        setDialogOutOfSynch();
      }
    }
  }

  private class OfflineHandler implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      Object newValue = evt.getNewValue();
      if (newValue != null) {
        setDialogOnline((Boolean) evt.getNewValue());
      }
    }
  }

  /**
   * Update the module info, module alarms and module details.
   */
  private static class ModuleInfoRefreshTask extends Task<Void, ModuleAlarm[]> {

    private final ModuleInfo mInfo;
    private final List<ModuleAlarm> alarmlist;
    private final ModuleInfoAPI moduleCmd;
    private final boolean ackAlarms;


    public ModuleInfoRefreshTask(Application application,
        ModuleInfoAPI moduleCmd,
        List<ModuleAlarm> alarmlist,
        ModuleInfo mInfo,
        boolean ackAlarms) {
      super(application);
      this.moduleCmd = moduleCmd;
      this.mInfo = mInfo;
      this.alarmlist = alarmlist;
      this.ackAlarms = ackAlarms;
      setTitle("Module Info Update");
      setDescription("Getting module info:" + mInfo);
    }

    @Override
    protected Void doInBackground() throws Exception {
      if (mInfo == null) {
        return null;
      }

      // Getting module info
      moduleCmd.cmdGetModueInfo();

      // Getting module detail
      moduleCmd.cmdGetModueDetail(mInfo.getDetail());

      if (ackAlarms) {
        moduleCmd.cmdAckModueAlarm(mInfo.getModuleType(), mInfo.getModuleID());
      }

      // Getting all alarms
      try {
        ModuleAlarm[] alarms = moduleCmd.cmdGetModueAlarm(
            mInfo.getModuleType(), mInfo.getModuleID());

        if (!isCancelled()) {
          publish(alarms);
          log.info("Alarm information has been read from: " + mInfo);
        }
      } catch (Exception e) {
        log.warn("Fail to get alarms from:" + mInfo);
      }

      return null;
    }

    @Override
    protected void process(List<ModuleAlarm[]> values) {
      assert SwingUtilities.isEventDispatchThread();

      for (int i = 0; i < values.size(); i++) {
        alarmlist.addAll(0, Arrays.asList(values.get(i)));
      }
    }

    @Override
    protected void failed(Throwable cause) {
      log.error("Fail to update module information cause: " + cause.getMessage());
    }
  }

  // Test
  /*
   * public static void main(String[] args) { ModuleInfo info = new
   * ModuleInfo(MODULE.MODULE_PSM, MODULE_ID.MODULE_ID_0);
   * System.out.println("listeners : "
   * +info.getPropertyChangeListeners().length); ModuleInfoDialog m = new
   * ModuleInfoDialog(null, null, info, null, null); m.setModal(true);
   * m.setVisible(true);
   * System.out.println("listeners : "+info.getPropertyChangeListeners
   * ().length); // m = new ModuleInfoDialog(null, info, null, null); //
   * m.setModal(true); m.setVisible(true); info.setOutOfSynch(true);
   * System.out.println
   * ("listeners : "+info.getPropertyChangeListeners().length); // m = new
   * ModuleInfoDialog(null, info, null, null); m.setModal(true);
   * m.setVisible(true);
   * System.out.println("listeners : "+info.getPropertyChangeListeners
   * ().length); }
   */
}
