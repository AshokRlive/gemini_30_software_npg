/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.modules;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.EnabledHighlighter;
import org.jdesktop.swingx.decorator.HighlighterFactory;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.RealtimePreference;
import com.lucy.g3.configtool.subsys.realtime.ui.modules.RTModulesTableModel.ModuleInfoColumn;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.gui.framework.page.UpdatablePage;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfoAPI;
import com.lucy.g3.rtu.comms.service.realtime.modules.RTUModules;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;

public class ModuleInfoPage extends AbstractPage implements UpdatablePage {

  private Logger log = Logger.getLogger(ModuleInfoPage.class);

  public static final ImageIcon onIcon = ResourceUtils.getImg("statusOn.icon", ModuleInfoPage.class);
  public static final ImageIcon offIcon = ResourceUtils.getImg("statusOff.icon", ModuleInfoPage.class);
  public static final ImageIcon exclamationIcon = ResourceUtils.getImg("statusExclamation.icon", ModuleInfoPage.class);

  private static final String ACTION_KEY_VIEW = "viewSelection";

  private static final String PROPERTY_VIEW_ENABLED = "viewEnabled";

  private static final String MODULE_NODE_NAME = "Modules";

  private final SelectionInList<ModuleInfo> moduleInfoList;

  // Module info polling task
  private Task<?, ?> updateTask;

  // Module info polling period
  private static int period = 500; // ms

  private final TaskService taskService;
  private final Application app;
  private final RTUModules rtuModules;

  private final Action viewSelectionAction;// Action for viewing the selected


  // module info.

  public ModuleInfoPage(RTUModules rtuModules) {
    super(null);
    app = Application.getInstance();
    this.rtuModules = Preconditions.checkNotNull(rtuModules, "rtuPoints must not be null");
    taskService = app.getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);

    moduleInfoList = new SelectionInList<ModuleInfo>(rtuModules.getModulesListModel());

    Action viewAction = app.getContext().getActionMap(this).get(ACTION_KEY_VIEW);
    viewSelectionAction = new BusyCursorAction(viewAction, this);

    /* Handle selection event */
    moduleInfoList.addPropertyChangeListener(SelectionInList.PROPERTY_SELECTION,
        new PropertyChangeListener() {

          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            firePropertyChange(PROPERTY_VIEW_ENABLED, null, isViewEnabled());
          }
        });

    /* Sync period value with option bean */
    try {
      RealtimePreference pref =  PreferenceManager.INSTANCE.getPreference(RealtimePreference.ID);
      period = pref.getUpdatePeriod();
      pref.addPropertyChangeListener(RealtimePreference.PROPERTY_UPDATE_PERIOD_MS, new OptionsPeriodPCL());
    } catch (Exception e) {
      log.error("Fail to get user option");
    }

    // Set the node name of this page.
    setNodeName(MODULE_NODE_NAME);
  }

  @Override
  protected void init() {
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder()); // Change default border

    JXTable mInfoTable = buildModuleTable();
    JScrollPane pane = new JScrollPane(mInfoTable);
    // pane.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.gray));
    pane.setBorder(BorderFactory.createEmptyBorder());
    add(pane, BorderLayout.CENTER);
  }

  private JXTable buildModuleTable() {
    JXTable mInfoTable = new JXTable(new RTModulesTableModel(moduleInfoList));
    
    UIUtils.setDefaultConfig(mInfoTable, true, true, true);
    
    // Set cell renderer
    mInfoTable.getColumn(ModuleInfoColumn.STATUS.getIndex()).setCellRenderer(new StatusCellRender());
    mInfoTable.getColumn(ModuleInfoColumn.MODULE.getIndex()).setCellRenderer(new ModuleNameCellRender());

    // Set column widths
    mInfoTable.getColumnExt(ModuleInfoColumn.STATUS.getIndex()).setPreferredWidth(300);

    // Hide columns
    mInfoTable.getColumnExt(ModuleInfoColumn.BOOTLOADER_API.getIndex()).setVisible(false);
    mInfoTable.getColumnExt(ModuleInfoColumn.BOOTLOADER_VERSION.getIndex()).setVisible(false);
    mInfoTable.getColumnExt(ModuleInfoColumn.API_VERSION.getIndex()).setVisible(false);

    // Set propertGies
    mInfoTable.setName("ModuleInfoTable");
    mInfoTable.setFillsViewportHeight(true);
    mInfoTable.setShowGrid(false, false);
    mInfoTable.setRowHeight(30);
    mInfoTable.setRowSelectionAllowed(true);

    // Set cell highlighter
    mInfoTable.setHighlighters(HighlighterFactory.createSimpleStriping(), new ModulePresentHighlighter());

    // Set pop-up menu
    JPopupMenu popMenu = new JPopupMenu();
    popMenu.add(viewSelectionAction);
    mInfoTable.setComponentPopupMenu(popMenu);

    mInfoTable.addMouseListener(new DoubleClickHandler());

    // Binding
    Bindings.bind(mInfoTable, moduleInfoList);

    // Change column packing action
    mInfoTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    mInfoTable.setHorizontalScrollEnabled(true);

    // Set keyboard action
    mInfoTable.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), ACTION_KEY_VIEW);
    mInfoTable.getActionMap().put(ACTION_KEY_VIEW, viewSelectionAction);


    return mInfoTable;
  }

  @Override
  public void startUpdating() {
    if (updateTask == null || updateTask.isDone()) {
      updateTask = new ModuleInfoUpdateTask(app, rtuModules);
      taskService.execute(updateTask);
      log.info("Module info start updating");
    } else {
      log.error("Cannot start new ModuleInfo Updating task cause it is already running");
    }
  }

  @Override
  public void stopUpdating() {
    if (updateTask != null && !updateTask.isDone()) {
      if (updateTask.cancel(true) == true) {
        log.info("Module info stops updating");
      } else {
        log.warn("ModuleInfo Updating task cannot be cancelld");
      }

      updateTask = null;
    }
  }

  /**
   * View the detail of the currently selected module.
   */
  @org.jdesktop.application.Action
  public void viewSelection() {
    ModuleInfo selection = moduleInfoList.getSelection();
    if (selection != null) {
      JDialog infoDialog = new ModuleInfoDialog(WindowUtils.getMainFrame(),
          rtuModules,
          selection,
          onIcon, offIcon);
      infoDialog.pack();
      infoDialog.setVisible(true);
    }
  }

  /**
   * Bean getter method. Checks if the "view" button is enabled.
   */
  public boolean isViewEnabled() {
    return moduleInfoList.hasSelection();
  }


  private static class OptionsPeriodPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      period = (Integer) evt.getNewValue();
    }
  }

  private class DoubleClickHandler extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      if (UIUtils.isDoubleClick(e)) {
        viewSelectionAction.actionPerformed(new ActionEvent(e.getSource(), e.getID(), ""));
      }
    }
  }

  /**
   * Cell highlighter for indicating if a module is present or not. Disabled if
   * the module is not present, enabled otherwise.
   */
  private class ModulePresentHighlighter extends EnabledHighlighter {

    public ModulePresentHighlighter() {
      super(false);
    }

    @Override
    protected Component doHighlight(Component renderer, ComponentAdapter adapter) {
      int rowInModel = adapter.convertRowIndexToModel(adapter.row);
      renderer.setEnabled(moduleInfoList.getList().get(rowInModel).isPresent());
      return renderer;
    }

  }

  private class ModuleNameCellRender extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
        Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      int rowInModel = table.convertRowIndexToModel(row);

      ModuleInfo minfo = moduleInfoList.getList().get(rowInModel);
      MODULE moduleType = minfo.getModuleType();
      ModuleResource res = ModuleResource.INSTANCE;
      setIcon(res.getModuleIconThumbnail(moduleType));
      setText(res.getModuleShortName(moduleType, minfo.getModuleID()));
      setToolTipText(res.getModuleTypeShortName(moduleType));

      if (rowInModel % 2 == 0 && !isSelected) {
        this.setBackground(UIManager.getColor("Table.alternateRowColor"));
      }
      return c;
    }
  }

  private class StatusCellRender extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
        Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      int rowInModel = table.convertRowIndexToModel(row);

      ModuleInfo moduleInfo = moduleInfoList.getList().get(rowInModel);

      boolean active = moduleInfo.isOk();

      // MODULE_BOARD_STATUS internalState = moduleInfo.getInternalState();

      ArrayList<SYS_ALARM_SEVERITY> errorFlags = moduleInfo.getErrorFlags();

      boolean critical = false;
      boolean error = false;
      boolean warning = false;
      boolean info = false;

      for (int i = 0; i < errorFlags.size(); i++) {
        SYS_ALARM_SEVERITY sys_ALARM_SEVERITY = errorFlags.get(i);
        if (sys_ALARM_SEVERITY.getValue() == 0) {
          critical = true;
        } else if (sys_ALARM_SEVERITY.getValue() == 1) {
          error = true;
        } else if (sys_ALARM_SEVERITY.getValue() == 2) {
          warning = true;
        } else if (sys_ALARM_SEVERITY.getValue() == 3) {
          info = true;
        }
      }

      if (active && (critical || error || warning || info)) {
        this.setIcon(exclamationIcon);
      } else if (active) {
        this.setIcon(onIcon);
      } else {
        this.setIcon(offIcon);
      }

      setText(moduleInfo.getAllStatusText());

      if (rowInModel % 2 == 0 && !isSelected) {
        this.setBackground(UIManager.getColor("Table.alternateRowColor"));
      }
      return c;
    }
  }

  private static class ModuleInfoUpdateTask extends Task<Void, Void> {

    private Logger log = Logger.getLogger(ModuleInfoUpdateTask.class);
    private final ModuleInfoAPI moduleCmd;


    public ModuleInfoUpdateTask(Application application, ModuleInfoAPI moduleCmd) {
      super(application);
      this.moduleCmd = moduleCmd;
    }

    @Override
    protected Void doInBackground() {
      message("startMessage");
      while (!isCancelled()) {
        try {
          moduleCmd.cmdGetModueInfo();
        } catch (Exception e) {
          log.error("Fail to read Module Info: " + e.getMessage());
        }

        // Delay
        try {
          Thread.sleep(period);
        } catch (InterruptedException e) {
        }
      }
      return null;
    }

    @Override
    protected void cancelled() {
      super.cancelled();
      message("cancelMessage");
    }

    @Override
    protected void failed(Throwable cause) {
      super.failed(cause);
      message("errorMessage");
    }

  }

  @Override
  public boolean hasError() {
    return false;
  }

}
