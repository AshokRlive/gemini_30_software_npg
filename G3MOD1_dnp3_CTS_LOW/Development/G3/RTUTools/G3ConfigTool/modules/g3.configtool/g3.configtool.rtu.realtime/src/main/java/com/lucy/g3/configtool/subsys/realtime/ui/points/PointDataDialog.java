/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.points;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.BasicComponentFactory;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;

/**
 * A dialog for showing the detail of a point data.
 */
public class PointDataDialog extends JDialog {

  private final PresentationModel<IPointData> pm;


  public PointDataDialog(Frame owner, IPointData pointData) {
    super(owner, false);
    pm = new PresentationModel<IPointData>(pointData);
    initComponents();
  }

  public void close() {
    pm.release();
    dispose();
  }

  private void createUIComponents() {
    /* Build content panel */
    FormLayout layout = new FormLayout("left:default, $lcgap, default:grow", "");
    DefaultFormBuilder builder = new DefaultFormBuilder(layout);

    // Append point ID
    ValueModel vm = pm.getModel(IPointData.PROPERTY_DESCRIPTIOIN);
    builder.append("Virtual Point:", BasicComponentFactory.createLabel(vm), true);

    // Append value
    vm = pm.getModel(IPointData.PROPERTY_VALUE);
    vm = new ConverterValueModel(
        vm,
        new NumberToStringConverter());
    builder.append("Value:", BasicComponentFactory.createLabel(vm), true);

    contentPanel = builder.getPanel();
  }

  private void thisWindowClosing(WindowEvent e) {
    close();
  }

  private void okButtonActionPerformed(ActionEvent e) {
    close();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    buttonBar = new JPanel();
    okButton = new JButton();

    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setTitle("Point Status");
    setMinimumSize(new Dimension(300, 200));
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== dialogPane ========
    {
      dialogPane.setBorder(Borders.DIALOG_BORDER);
      dialogPane.setLayout(new BorderLayout());

      // ======== buttonBar ========
      {
        buttonBar.setBorder(Borders.BUTTON_BAR_GAP_BORDER);
        buttonBar.setLayout(new FormLayout(
            "$glue, $button",
            "pref"));

        // ---- okButton ----
        okButton.setText("OK");
        okButton.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(okButton, CC.xy(2, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
      dialogPane.add(contentPanel, BorderLayout.CENTER);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel buttonBar;
  private JButton okButton;
  private JPanel contentPanel;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static final class NumberToStringConverter
      implements BindingConverter<Number, String> {

    /**
     * @return an Integer that is the Float source value multiplied with this
     *         converter's multiplier
     */
    @Override
    public String targetValue(Number sourceValue) {
      if (sourceValue != null) {
        return sourceValue.toString();
      } else {
        return "";
      }
    }

    /**
     * Converts a {@code Float} using the multiplier.
     */
    @Override
    public Number sourceValue(String sourceValue) {
      return null;
    }

  }
}
