/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.modules;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.service.realtime.modules.CANStatistics;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfoAPI;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 *
 */
public class CANStatDialog extends JDialog {

  private Logger log = Logger.getLogger(CANStatDialog.class);

  private Task<?, ?> refreshTask;

  private final ModuleInfoAPI moduleCmd;

  private final MODULE type;
  private final MODULE_ID id;


  public CANStatDialog(Dialog owner, ModuleInfoAPI moduleCmd, MODULE type, MODULE_ID id) {
    super(owner);
    this.moduleCmd = moduleCmd;
    this.type = Preconditions.checkNotNull(type, "type is null");
    this.id = Preconditions.checkNotNull(id, "id is null");

    // Set dialog title
    String title = "CAN Statistics";
    title += " - ";
    title += ModuleResource.INSTANCE.getModuleShortName(type, id);
    setTitle(title);

    initComponents();
    btnRereshActionPerformed();
  }

  private void updateContentPane(CANStatistics[] canstat) {
    // Build content panel
    DefaultFormBuilder builder = new DefaultFormBuilder(new FormLayout(
        "right:[50dlu,default],$lcgap,[default,100dlu]:grow", ""));

    for (int i = 0; i < canstat.length; i++) {
      builder.appendSeparator("can" + i);
      builder.nextLine();

      String[] fieldNames = canstat[i].getFieldNames();
      boolean[] fieldsValid = canstat[i].getFieldsValid();
      long[] fieldsValue = canstat[i].getFieldsValue();

      for (int j = 0; j < fieldNames.length; j++) {
        JLabel valueLabel = new JLabel(String.valueOf(fieldsValue[j]));
        if (fieldsValid[j] == false) {
          valueLabel.setText("");
        }
        builder.append(fieldNames[j] + " :", valueLabel, true);
      }
    }

    contentPane.setViewportView(builder.getPanel());
  }

  private void btnCloseActionPerformed() {
    dispose();
  }

  private void btnRereshActionPerformed() {
    if (refreshTask == null || refreshTask.isDone()) {
      Application app = Application.getInstance();
      refreshTask = new CanStatRefreshTask(app);
      getService(app).execute(refreshTask);
    }
  }

  private TaskService getService(Application app) {
    return app.getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    buttonBar = new JPanel();
    btnReresh = new JButton();
    btnClose = new JButton();
    contentPane = new JScrollPane();

    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    Container contentPane2 = getContentPane();
    contentPane2.setLayout(new BorderLayout());

    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setLayout(new BorderLayout());

      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new FormLayout(
            "pref:grow, $lcgap, [80px,pref], $lcgap, pref",
            "fill:pref"));
        ((FormLayout) buttonBar.getLayout()).setColumnGroups(new int[][] { { 3, 5 } });

        // ---- btnReresh ----
        btnReresh.setText("Refresh");
        btnReresh.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnRereshActionPerformed();
          }
        });
        buttonBar.add(btnReresh, CC.xy(3, 1));

        // ---- btnClose ----
        btnClose.setText("Close");
        btnClose.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnCloseActionPerformed();
          }
        });
        buttonBar.add(btnClose, CC.xy(5, 1));
      }
      dialogPane.add(buttonBar, BorderLayout.PAGE_END);

      // ======== contentPane ========
      {
        contentPane.setBorder(BorderFactory.createEmptyBorder());
      }
      dialogPane.add(contentPane, BorderLayout.CENTER);
    }
    contentPane2.add(dialogPane, BorderLayout.CENTER);
    setSize(390, 425);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    UIUtils.increaseScrollSpeed(contentPane);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel buttonBar;
  private JButton btnReresh;
  private JButton btnClose;
  private JScrollPane contentPane;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  private class CanStatRefreshTask extends Task<CANStatistics[], Void> {

    public CanStatRefreshTask(Application application) {
      super(application);
    }

    @Override
    protected CANStatistics[] doInBackground() throws Exception {
      return moduleCmd.cmdGetCANStat(type, id);
    }

    @Override
    protected void failed(Throwable cause) {
      log.error("Fail to refresh CANStatistics:" + cause.getMessage());
    }

    @Override
    protected void succeeded(CANStatistics[] result) {
      log.info("Received CAN Statistics Num: "
          + result == null ? 0 : result.length);
      updateContentPane(result);
    }

  }

  // public static void main(String[] args) {
  // Logger.getRootLogger().setLevel(Level.DEBUG);
  // new CANStatDialog((Dialog)null, null, null).setVisible(true);
  // }
}
