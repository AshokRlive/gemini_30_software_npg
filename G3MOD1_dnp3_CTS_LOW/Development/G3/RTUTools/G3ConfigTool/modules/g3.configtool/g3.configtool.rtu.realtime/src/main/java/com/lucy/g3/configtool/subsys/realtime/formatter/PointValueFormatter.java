/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.formatter;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.RealtimePreference;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointValueFormatter;

/**
 * The formatter for virtual point data value.
 */
public final class PointValueFormatter implements IPointValueFormatter {

  public static final String NULL_VALUE = "";
  public static final String NAN_VALUE = "NaN";
  public static final String INFINIT_VALUE = "Infinite";
  public static final String INVALID_VALUE = "?";

  private final DecimalFormat format = new DecimalFormat();


  private PointValueFormatter() {
    RealtimePreference prefs = PreferenceManager.INSTANCE.getPreference(RealtimePreference.ID);

    format.setMaximumFractionDigits(prefs.getRtDigits());
    format.setRoundingMode(prefs.getRtRoundMode());

    prefs.addPropertyChangeListener(new PropertyChangeListener() {

      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(RealtimePreference.PROPERTY_RT_FRACTION_DIGITS)) {
          format.setMaximumFractionDigits((Integer) evt.getNewValue());

          // Rounding mode changed
        } else if (evt.getPropertyName().equals(RealtimePreference.PROPERTY_RT_FRACTION_ROUND_MODE)) {
          format.setRoundingMode((RoundingMode) evt.getNewValue());
        }
      }
    });
  }

  @Override
  public String format(IPointData source, final Number value, boolean showUnit) {
    String valueText;

    Boolean isValid = source.isValid();

    if (value != null && isValid == Boolean.TRUE) {
      valueText = format.format(value);

      if (Float.isNaN(value.floatValue())) {
        valueText = NAN_VALUE;
      }

      if (Float.isInfinite(value.floatValue())) {
        valueText = INFINIT_VALUE;

        // Add value's unit to the end if it exists
      } else if (showUnit) {
        String unit = source.getUnit();
        if (unit != null && !unit.isEmpty()) {
          valueText = String.format("%-8s %s", valueText, unit);
        }
      }
    } else if (value != null && isValid == Boolean.FALSE) {
      valueText = INVALID_VALUE;
    } else {
      valueText = NULL_VALUE;
    }

    /* Fix bug value is displayed as "-0" */
    if ("-0".equals(valueText)) {
      valueText = "0";
    }

    return valueText;

  }


  private static PointValueFormatter INSTANCE;


  public static PointValueFormatter getFormatter() {
    if (INSTANCE == null)
      INSTANCE = new PointValueFormatter();

    return INSTANCE;
  }

}
