/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.overview;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import org.apache.log4j.Logger;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;

import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.binder.BeanBinder;
import com.jgoodies.binding.binder.Binders;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.controller.RTUController;
import com.lucy.g3.configtool.rtu.restart.RTURestartController;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.DateTimeConverter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.bindconverter.SecondsToStrConverter;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.gui.framework.page.UpdatablePage;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;

/**
 * The page for showing RTU information.
 */
public class RTMonitorRootPage extends AbstractPage implements UpdatablePage {

  private Logger log = Logger.getLogger(RTMonitorRootPage.class);

  private final RTUController controller;
  private final RTURestartController  restarter;

  private final G3RTU g3RTU;

  // RTU info polling task
  private Task<?, ?> updateTask;

  // RTU info polling period
  private int period = 1000 * 60; // 1 minute

  private JXTaskPaneContainer content;


  public RTMonitorRootPage(com.lucy.g3.common.context.IContext context, G3RTU rtu) {
    super(null, "Overview");
    this.restarter = context.getComponent(RTURestartController.SUBSYSTEM_ID);
    this.controller = context.getComponent(RTUController.SUBSYSTEM_ID);
    this.g3RTU = rtu;

    PropertyConnector.connect(g3RTU.getComms().getRtuInfo(), RTUInfo.PROPERTY_SITENAME, this, PROPERTY_NODE_NAME).updateProperty2();
    PropertyConnector.connect(g3RTU.getComms().getRtuInfo(), RTUInfo.PROPERTY_SITENAME, this, PROPERTY_TITLE).updateProperty2();
    PropertyConnector.connect(g3RTU, G3RTU.PROPERTY_ICON, this, PROPERTY_NODE_ICON).updateProperty2();
  }

  protected String getRtuName() {
    return g3RTU.getComms().getRtuInfo().getSiteName();
  }

  @Override
  public Icon getTitleIcon() {
    return null;
  }

  @Override
  public boolean isScrollable() {
    return true;
  }

  @Override
  public String getSessionKey() {
    return null;
  }

  /*
   * Binding all components to beans. Note those binding are not released.
   */
  private void initComponentsBinding() {
    /* Binding RTUInfo */
    BeanBinder binder1 = Binders.binderFor(g3RTU.getComms().getRtuInfo());
    binder1.bindProperty(RTUInfo.PROPERTY_SITENAME).to(lblSiteName);
    binder1.bindProperty(RTUInfo.PROPERTY_MCMVERSION).to(lblMCMRevision);
    binder1.bindProperty(RTUInfo.PROPERTY_KERNELVERSION).to(lblKernel);
    binder1.bindProperty(RTUInfo.PROPERTY_ROOTFSVERSION).to(lblRootFs);
    binder1.bindProperty(RTUInfo.PROPERTY_NETWORK_IP0).to(lblIP0);
    binder1.bindProperty(RTUInfo.PROPERTY_NETWORK_IP1).to(lblIP1);
    binder1.bindProperty(RTUInfo.PROPERTY_CONFIG_STATUS).to(lblConfigVersion);
    binder1.bindProperty(RTUInfo.PROPERTY_SDP_VERSION).to(lblRTUVersion);

    binder1.bindProperty(RTUInfo.PROPERTY_CONFIG_API)
        .converted(new ModuleProtocolConverter())
        .to(lblModProtocol);
 
    binder1.bindProperty(RTUInfo.PROPERTY_CONFIG_API)
        .converted(new G3ProtocolConverter())
        .to(lblG3Protocol);

    binder1.bindProperty(RTUInfo.PROPERTY_CONFIG_API)
        .converted(new G3SchemaConverter())
        .to(lbllG3Schema);

    // ====== Bind RTUStatus ======
    BeanBinder binder2 = Binders.binderFor(g3RTU.getComms().getRtuStatus());
    BindingConverter<?, ?> secsConverter = new SecondsToStrConverter(RTUInfo.TEXT_NA);
    BindingConverter<?, ?> timeConverter = new DateTimeConverter(RTUTimeDecoder.getRTUTimeFormatter(),
        RTUInfo.TEXT_NA);
    BindingConverter<?, ?> serviceConverter = new ServiceModeConverter();
    binder2.bindProperty(RTUStatus.PROPERTY_RTU_TIME)
        .converted(timeConverter)
        .to(lblDateTime);

    binder2.bindProperty(RTUStatus.PROPERTY_SYS_UPTIME_SEC)
        .converted(secsConverter)
        .to(lblSysuptime);

    binder2.bindProperty(RTUStatus.PROPERTY_APP_UPTIME_SEC)
        .converted(secsConverter)
        .to(lblAppuptime);

    binder2.bindProperty(RTUStatus.PROPERTY_SERVICE_MODE)
        .converted(serviceConverter)
        .to(lblServiceMode);

    binder2.bindProperty(RTUStatus.PROPERTY_OUTSERVICE_REASON)
        .to(lblOutServiceReason); // TODO change PROPERTY_OUTSERVICE_REASON to
    // enum type and convert with G3EnumConverter

    /* Hyper link actions */
    xHyperlinkRestart.setAction(restarter.getAction(RTURestartController.ACTION_KEY_SHOW_RESTART_OPTION));
    xHyperlinkDateTime.setAction(controller.getAction(RTUController.ACTION_KEY_SET_TIME));
    xHyperlinkNet.setAction(controller.getAction(RTUController.ACTION_KEY_SHOW_NETINFO));
    xHyperlinkSystemDetail.setAction(controller.getAction(RTUController.ACTION_KEY_ABOUT_RTU));
    xHyperlinkSystemDetail.setText("Detail...");
    menuItemRefresh.setAction(controller.getAction(RTUController.ACTION_KEY_REFRESH_INFO));

    /* Binding label and action of service state */
    updateServiceStateLabel(g3RTU.getComms().getRtuStatus().getServiceMode());
    g3RTU.getComms().getRtuStatus().addPropertyChangeListener(RTUStatus.PROPERTY_SERVICE_MODE, new ServiceStatePCL());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    panelSiteInfo = new JPanel();
    JLabel label0 = new JLabel();
    lblSiteName = new JLabel();
    JLabel label2 = new JLabel();
    lblServiceMode = new JLabel();
    xHyperlinkService = new JXHyperlink();
    lblReason = new JLabel();
    lblOutServiceReason = new JLabel();
    JLabel label1 = new JLabel();
    lblDateTime = new JLabel();
    xHyperlinkDateTime = new JXHyperlink();
    JLabel label12 = new JLabel();
    lblIP0 = new JLabel();
    xHyperlinkNet = new JXHyperlink();
    lblIP1 = new JLabel();
    xHyperlinkRestart = new JXHyperlink();
    JLabel label7 = new JLabel();
    lblSysuptime = new JLabel();
    JLabel label9 = new JLabel();
    lblAppuptime = new JLabel();
    panelSoftware = new JPanel();
    label13 = new JLabel();
    lblRTUVersion = new JLabel();
    xHyperlinkSystemDetail = new JXHyperlink();
    label10 = compFactory.createLabel("");
    lblConfigVersion = compFactory.createLabel("");
    JLabel label6 = new JLabel();
    lblKernel = new JLabel();
    JLabel label11 = new JLabel();
    lblRootFs = new JLabel();
    JLabel label8 = new JLabel();
    lblMCMRevision = new JLabel();
    popupMenu = new JPopupMenu();
    menuItemRefresh = new JMenuItem();
    xTaskPaneRTUInfo = new JXTaskPane();
    xTaskPaneSwInfo = new JXTaskPane();
    lblG3Protocol = new JLabel();
    JLabel label5 = new JLabel();
    JLabel label4 = new JLabel();
    JLabel label3 = new JLabel();
    lblModProtocol = new JLabel();
    lbllG3Schema = new JLabel();

    //======== panelSiteInfo ========
    {
      panelSiteInfo.setOpaque(false);
      panelSiteInfo.setLayout(new FormLayout(
        "right:default, $lcgap, [65dlu,default], $ugap, default",
        "default, $ugap, default, $lgap, 2*(default, $ugap), default, $lgap, fill:default, $ugap, default, 7dlu, default, $rgap, default"));

      //---- label0 ----
      label0.setText("Site Name:");
      panelSiteInfo.add(label0, CC.xy(1, 1));

      //---- lblSiteName ----
      lblSiteName.setText("Banbury RTU-01");
      panelSiteInfo.add(lblSiteName, CC.xy(3, 1));

      //---- label2 ----
      label2.setText("Service Mode:");
      panelSiteInfo.add(label2, CC.xy(1, 3));

      //---- lblServiceMode ----
      lblServiceMode.setText("In service");
      panelSiteInfo.add(lblServiceMode, CC.xy(3, 3));

      //---- xHyperlinkService ----
      xHyperlinkService.setText("start/stop");
      panelSiteInfo.add(xHyperlinkService, CC.xy(5, 3));

      //---- lblReason ----
      lblReason.setText("Reason:");
      panelSiteInfo.add(lblReason, CC.xy(1, 5));

      //---- lblOutServiceReason ----
      lblOutServiceReason.setText("Reason");
      panelSiteInfo.add(lblOutServiceReason, CC.xywh(3, 5, 3, 1));

      //---- label1 ----
      label1.setText("Date & Time:");
      panelSiteInfo.add(label1, CC.xy(1, 7));

      //---- lblDateTime ----
      lblDateTime.setText("28/06/2012 18:38");
      panelSiteInfo.add(lblDateTime, CC.xy(3, 7));

      //---- xHyperlinkDateTime ----
      xHyperlinkDateTime.setText("Change Date Time...");
      panelSiteInfo.add(xHyperlinkDateTime, CC.xy(5, 7));

      //---- label12 ----
      label12.setText("Network:");
      panelSiteInfo.add(label12, CC.xy(1, 9));

      //---- lblIP0 ----
      lblIP0.setText("x.x.x.x");
      panelSiteInfo.add(lblIP0, CC.xy(3, 9));

      //---- xHyperlinkNet ----
      xHyperlinkNet.setText("Network...");
      panelSiteInfo.add(xHyperlinkNet, CC.xy(5, 9));

      //---- lblIP1 ----
      lblIP1.setText("x.x.x.x");
      panelSiteInfo.add(lblIP1, CC.xy(3, 11));

      //---- xHyperlinkRestart ----
      xHyperlinkRestart.setText("Restart RTU...");
      xHyperlinkRestart.setHorizontalTextPosition(SwingConstants.LEFT);
      panelSiteInfo.add(xHyperlinkRestart, CC.xy(5, 13));

      //---- label7 ----
      label7.setText("System Uptime:");
      panelSiteInfo.add(label7, CC.xy(1, 15));

      //---- lblSysuptime ----
      lblSysuptime.setText("XXX");
      panelSiteInfo.add(lblSysuptime, CC.xywh(3, 15, 3, 1));

      //---- label9 ----
      label9.setText("App Uptime:");
      panelSiteInfo.add(label9, CC.xy(1, 17));

      //---- lblAppuptime ----
      lblAppuptime.setText("XXX");
      panelSiteInfo.add(lblAppuptime, CC.xywh(3, 17, 3, 1));
    }

    //======== panelSoftware ========
    {
      panelSoftware.setOpaque(false);
      panelSoftware.setLayout(new FormLayout(
        "right:default, $lcgap, default:grow, $lcgap, default",
        "default, $lgap, default, $pgap, 2*(default, $lgap), default"));

      //---- label13 ----
      label13.setText("Software Package Version:");
      panelSoftware.add(label13, CC.xy(1, 1));

      //---- lblRTUVersion ----
      lblRTUVersion.setText("xxx");
      panelSoftware.add(lblRTUVersion, CC.xy(3, 1));

      //---- xHyperlinkSystemDetail ----
      xHyperlinkSystemDetail.setText("Detail...");
      panelSoftware.add(xHyperlinkSystemDetail, CC.xy(5, 1));

      //---- label10 ----
      label10.setText("Configuration File:");
      panelSoftware.add(label10, CC.xy(1, 3));

      //---- lblConfigVersion ----
      lblConfigVersion.setText("xxx");
      panelSoftware.add(lblConfigVersion, CC.xywh(3, 3, 3, 1));

      //---- label6 ----
      label6.setText("OS  Kernel:");
      panelSoftware.add(label6, CC.xy(1, 5));

      //---- lblKernel ----
      lblKernel.setText("XXX");
      lblKernel.setPreferredSize(new Dimension(600, 14));
      lblKernel.setMinimumSize(new Dimension(0, 14));
      lblKernel.setMaximumSize(new Dimension(800, 14));
      panelSoftware.add(lblKernel, CC.xy(3, 5));

      //---- label11 ----
      label11.setText("Root-FS:");
      panelSoftware.add(label11, CC.xy(1, 7));

      //---- lblRootFs ----
      lblRootFs.setText("XXX");
      panelSoftware.add(lblRootFs, CC.xy(3, 7));

      //---- label8 ----
      label8.setText("MCM Application:");
      panelSoftware.add(label8, CC.xy(1, 9));

      //---- lblMCMRevision ----
      lblMCMRevision.setText("XXX");
      panelSoftware.add(lblMCMRevision, CC.xy(3, 9));
    }

    //======== popupMenu ========
    {

      //---- menuItemRefresh ----
      menuItemRefresh.setText("Refresh");
      popupMenu.add(menuItemRefresh);
    }

    //---- xTaskPaneRTUInfo ----
    xTaskPaneRTUInfo.setTitle("Site Information");

    //---- xTaskPaneSwInfo ----
    xTaskPaneSwInfo.setTitle("Software Information");

    //---- lblG3Protocol ----
    lblG3Protocol.setText("XXX");

    //---- label5 ----
    label5.setText("Config Protocol:");

    //---- label4 ----
    label4.setText("Config Schema:");

    //---- label3 ----
    label3.setText("Module Protocol:");

    //---- lblModProtocol ----
    lblModProtocol.setText("XXX");

    //---- lbllG3Schema ----
    lbllG3Schema.setText("XXX");
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ----[ Custom code ]----
    content = new JXTaskPaneContainer();

    xTaskPaneRTUInfo.add(panelSiteInfo);
    xTaskPaneSwInfo.add(panelSoftware);

    xTaskPaneRTUInfo.setName("rtuInfoGroup");
    xTaskPaneSwInfo.setName("swInfoGroup");

    xTaskPaneSwInfo.setComponentPopupMenu(popupMenu);
    xTaskPaneRTUInfo.setComponentPopupMenu(popupMenu);

    content.add(xTaskPaneRTUInfo);
    content.add(xTaskPaneSwInfo);

    content.setComponentPopupMenu(popupMenu);
    
    //Application.getInstance().getContext().getResourceMap(content.getClass()).injectComponents(content);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelSiteInfo;
  private JLabel lblSiteName;
  private JLabel lblServiceMode;
  private JXHyperlink xHyperlinkService;
  private JLabel lblReason;
  private JLabel lblOutServiceReason;
  private JLabel lblDateTime;
  private JXHyperlink xHyperlinkDateTime;
  private JLabel lblIP0;
  private JXHyperlink xHyperlinkNet;
  private JLabel lblIP1;
  private JXHyperlink xHyperlinkRestart;
  private JLabel lblSysuptime;
  private JLabel lblAppuptime;
  private JPanel panelSoftware;
  private JLabel label13;
  private JLabel lblRTUVersion;
  private JXHyperlink xHyperlinkSystemDetail;
  private JLabel label10;
  private JLabel lblConfigVersion;
  private JLabel lblKernel;
  private JLabel lblRootFs;
  private JLabel lblMCMRevision;
  private JPopupMenu popupMenu;
  private JMenuItem menuItemRefresh;
  private JXTaskPane xTaskPaneRTUInfo;
  private JXTaskPane xTaskPaneSwInfo;
  private JLabel lblG3Protocol;
  private JLabel lblModProtocol;
  private JLabel lbllG3Schema;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private void updateServiceStateLabel(SERVICEMODE mode) {
    // Set visibility of the "Out of service reason label"
    boolean outservice = (mode == SERVICEMODE.SERVICEMODE_OUTSERVICE);
    lblOutServiceReason.setVisible(outservice);
    lblReason.setVisible(outservice);

    // TODO Replace this with Binder.bind(Action)
    // Set service controlling actions
    if (outservice) {
      xHyperlinkService.setAction(controller.getAction(RTUController.ACTION_KEY_ENABLE_SERVICE));
    } else {
      xHyperlinkService.setAction(controller.getAction(RTUController.ACTION_KEY_DISABLE_SERVICE));
    }
  }


  private class ServiceStatePCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      updateServiceStateLabel((SERVICEMODE) evt.getNewValue());
    }
  }

  private static class ServiceModeConverter implements BindingConverter<SERVICEMODE, String> {

    @Override
    public SERVICEMODE sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(SERVICEMODE subjectValue) {
      return subjectValue == null ? RTUInfo.TEXT_NA : subjectValue.getDescription();
    }
  }

  private static class ModuleProtocolConverter implements BindingConverter<G3ConfigAPI, String> {

    @Override
    public G3ConfigAPI sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(G3ConfigAPI subjectValue) {
      return subjectValue == null ? RTUInfo.TEXT_NA : subjectValue.getModuleProtocollVersion();

    }
  }

  private static class G3SchemaConverter implements BindingConverter<G3ConfigAPI, String> {

    @Override
    public G3ConfigAPI sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(G3ConfigAPI subjectValue) {
      return subjectValue == null ? RTUInfo.TEXT_NA : subjectValue.getG3SchemaVersion();
    }
  }

  private static class G3ProtocolConverter implements BindingConverter<G3ConfigAPI, String> {

    @Override
    public G3ConfigAPI sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(G3ConfigAPI subjectValue) {
      return subjectValue == null ? RTUInfo.TEXT_NA : subjectValue.getG3ProtocolVersion();
    }
  }


  @Override
  public void startUpdating() {
    if (updateTask == null || updateTask.isDone()) {
      updateTask = new SystemInfoUpdateTask();
      controller.getCommsTaskService().execute(updateTask);
      log.info("System info start updating");
    } else {
      log.error("Cannot start new SystemInfo Updating task cause it is already running");
    }
  }

  @Override
  public void stopUpdating() {
    if (updateTask != null && !updateTask.isDone()) {
      if (updateTask.cancel(true) == true) {
        log.info("SystemInfo stops updating");
      } else {
        log.warn("SystemInfo Updating task cannot be cancelld");
      }

      updateTask = null;
    }
  }


  private class SystemInfoUpdateTask extends Task<Void, Void> {

    public SystemInfoUpdateTask() {
      super(controller.getApp());
    }

    @Override
    protected Void doInBackground() {
      while (!isCancelled()) {
        try {
          g3RTU.getComms().getRtuStatus().cmdGetDateTime();
        } catch (Exception e) {
          log.error("Fail to read RTU datetime: " + e.getMessage());
        }

        // Delay
        try {
          Thread.sleep(period);
        } catch (InterruptedException e) {
        }
      }
      return null;
    }
  }


  /**
   * Overload to show RTU status in tool-tip box of the tree node.
   */
  @Override
  public String getContextTips() {
    CTH_RUNNINGAPP state = g3RTU.getConnectionState();
    SERVICEMODE service = g3RTU.getServiceMode();
    String reason = g3RTU.getComms().getRtuStatus().getOutServiceReason();
    
    String tips = "";
    if (state != CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP && state != CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      tips = "<font color=\"red\">The RTU is offline!</font>";
      
    } else if(state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      tips = "<font color=\"orange\">The RTU is out of service in upgrade mode.</font>";
      
    } else if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      if(service == SERVICEMODE.SERVICEMODE_INSERVICE) {
        tips = "<font color=\"green\">The RTU is in service.</font>";
        
      } else if(service == SERVICEMODE.SERVICEMODE_OUTSERVICE) {
        tips = String.format("<font color=\"orange\"> The RTU is out of service cause: %s!</font>", reason);
      }
    }
    
    tips = String.format("<html>%s</html>",tips);
    
    return tips;
  }

  @Override
  protected void init() throws Exception {
    initComponents();
    initComponentsBinding();

    add(BorderLayout.CENTER, content);
  }

  @Override
  public boolean hasError() {
    return false;
  }

}
