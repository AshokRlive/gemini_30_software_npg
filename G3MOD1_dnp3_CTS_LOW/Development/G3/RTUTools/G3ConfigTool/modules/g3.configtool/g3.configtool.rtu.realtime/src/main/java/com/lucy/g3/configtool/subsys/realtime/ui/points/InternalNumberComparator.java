/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.points;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InternalNumberComparator implements Comparator<String> {

  Pattern splitter = Pattern.compile("\\[\\d+\\]");
  Pattern splitter2 = Pattern.compile("\\d+");


  @Override
  public int compare(String o1, String o2) {
    try {
      Matcher m1 = splitter.matcher(o1);
      if (m1.find()) {
        o1 = m1.group(0);
      }
      m1 = splitter2.matcher(o1);
      if (m1.find()) {
        o1 = m1.group(0);
      }
      m1 = splitter.matcher(o2);
      int n1 = Integer.parseInt(o1);
      if (m1.find()) {
        o2 = m1.group(0);
      }
      m1 = splitter2.matcher(o2);
      if (m1.find()) {
        o2 = m1.group(0);
      }
      int n2 = Integer.parseInt(o2);
      return n1 - n2;
    } catch (Exception e) {
      return 0;
    }

  }
}
