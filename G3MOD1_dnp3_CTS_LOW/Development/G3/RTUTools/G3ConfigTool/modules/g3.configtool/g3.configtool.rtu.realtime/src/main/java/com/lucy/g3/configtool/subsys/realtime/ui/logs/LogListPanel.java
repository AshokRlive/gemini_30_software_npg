/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.logs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;

import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.eventlog.manager.EventLogManager;
import com.lucy.g3.configtool.rtu.eventlog.ui.EventLogPanel;
import com.lucy.g3.configtool.rtu.sysalarm.SystemAlarmsManager;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.ext.swing.button.ButtonHoverEffect;
import com.lucy.g3.help.Helper;

/**
 * This panel is for displaying the received logs from RTU.
 */
public class LogListPanel extends JPanel {

  private EventLogManager eventLog; 
  private SystemAlarmsManager sysalarm; 
  
  public LogListPanel(com.lucy.g3.common.context.IContext context,ConnectionState initialState) {
    eventLog = context.getComponent(EventLogManager.SUBSYSTEM_ID);
    sysalarm = context.getComponent(SystemAlarmsManager.SUBSYSTEM_ID);

    initComponents();
    initEventHandling();

    Helper.register(this, getClass());
  }

  private void initEventHandling() {
    // Update tab title 
    sysalarm.addPropertyChangeListener(
        SystemAlarmsManager.PROPERTY_ALARMS_COUNT,
        new EntrySizePCL(tabbedPane1, tabbedPane1.indexOfComponent(tabAlarms)));

    eventLog.addPropertyChangeListener(
        EventLogManager.PROPERTY_EVENT_COUNT,
        new EntrySizePCL(tabbedPane1, tabbedPane1.indexOfComponent(tabEvents)));    
  }

  private void createUIComponents() {
    
    tabEvents = new EventLogPanel(eventLog.getApp(), eventLog);

    alarmsListTable = new JXTable(sysalarm.getAlarmsTableModel());
    alarmsListTable.setSortOrderCycle(SortOrder.ASCENDING, SortOrder.DESCENDING, SortOrder.UNSORTED);

    ApplicationActionMap actions;

    actions = sysalarm.getActionMap();
    btnAckAlarms = creatTabButton(actions.get(SystemAlarmsManager.ACTION_KEY_CLEAR));
    btnCopyAlarms = creatTabButton(actions.get(SystemAlarmsManager.ACTION_KEY_COPY));
    btnSaveAlarms = creatTabButton(actions.get(SystemAlarmsManager.ACTION_KEY_SAVE));
    checkBoxupdateAlarms = new JCheckBox(actions.get(SystemAlarmsManager.ACTION_KEY_ENABLE_UPDATE));
    checkBoxupdateAlarms.setSelected(sysalarm.isUpdateEnabled());
    
    JPopupMenu popup = new JPopupMenu();
    popup.add(actions.get(SystemAlarmsManager.ACTION_KEY_REFRESH));
    alarmsListTable.setComponentPopupMenu(popup);
  }

  private JButton creatTabButton(Action action) {
    JButton btn = new JButton(action);
    btn.setPreferredSize(new Dimension(25, 25));
    btn.setHideActionText(true);
    ButtonHoverEffect.install(btn);
    return btn;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    tabbedPane1 = new JTabbedPane();
    tabAlarms = new JPanel();
    panel6 = new JPanel();
    scrollPane3 = new JScrollPane();

    // ======== this ========
    setLayout(new BorderLayout());

    // ======== tabbedPane1 ========
    {
      tabbedPane1.setTabPlacement(SwingConstants.BOTTOM);
      tabbedPane1.setFont(tabbedPane1.getFont().deriveFont(tabbedPane1.getFont().getStyle() | Font.BOLD));
      tabbedPane1.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
      tabbedPane1.addTab("Events ", null, tabEvents, "RTU System events");

      // ======== tabAlarms ========
      {
        tabAlarms.setLayout(new BorderLayout());

        // ======== panel6 ========
        {
          panel6.setLayout(new FormLayout(
              "default, $lcgap, default:grow, 4*($lcgap, default)",
              "fill:default"));
          panel6.add(checkBoxupdateAlarms, CC.xy(1, 1));
          panel6.add(btnCopyAlarms, CC.xy(7, 1));
          panel6.add(btnSaveAlarms, CC.xy(9, 1));
          panel6.add(btnAckAlarms, CC.xy(11, 1));
        }
        tabAlarms.add(panel6, BorderLayout.NORTH);

        // ======== scrollPane3 ========
        {
          scrollPane3.setBorder(BorderFactory.createEmptyBorder());

          // ---- alarmsListTable ----
          alarmsListTable.setForeground(Color.darkGray);
          alarmsListTable.setBorder(null);
          alarmsListTable.setEditable(false);
          alarmsListTable.setShowHorizontalLines(false);
          alarmsListTable.setShowVerticalLines(false);
          alarmsListTable.setColumnMargin(0);
          alarmsListTable.setRolloverEnabled(false);
          alarmsListTable.setName("alarmListTable");
          scrollPane3.setViewportView(alarmsListTable);
        }
        tabAlarms.add(scrollPane3, BorderLayout.CENTER);
      }
      tabbedPane1.addTab("Alarms", null, tabAlarms, "RTU System alarms");

    }
    add(tabbedPane1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JPanel tabEvents;
  private JPanel tabAlarms;
  private JPanel panel6;
  private JCheckBox checkBoxupdateAlarms;
  private JButton btnCopyAlarms;
  private JButton btnSaveAlarms;
  private JButton btnAckAlarms;
  private JScrollPane scrollPane3;
  private JXTable alarmsListTable;


  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Updates tab title text when entry size changes.
   */
  private static class EntrySizePCL implements PropertyChangeListener {

    private final String orginTitle;

    private final int tabIndex;

    private final JTabbedPane pane;


    public EntrySizePCL(JTabbedPane pane, int tabIndex) {
      this.tabIndex = tabIndex;
      this.pane = pane;
      this.orginTitle = pane.getTitleAt(tabIndex);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      int size = (Integer) evt.getNewValue();
      String title = (size <= 0) ? orginTitle
          : String.format("%s (%d)", orginTitle, size);

      pane.setTitleAt(tabIndex, title);
    }
  }

}
