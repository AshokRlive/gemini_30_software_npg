/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.points;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.*;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.ComponentAdapter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import org.jdesktop.swingx.renderer.StringValues;
import org.jdesktop.swingx.table.TableColumnExt;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.beans.PropertyConnector;
import com.jgoodies.binding.value.ConverterFactory.BooleanNegator;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.collect.ArrayListModel;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.RealtimePreference;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiListSelectionAdapter;
import com.lucy.g3.gui.common.widgets.ext.jgoodies.list.MultiSelectionInList;
import com.lucy.g3.gui.common.widgets.ext.swing.button.ButtonHoverEffect;
import com.lucy.g3.gui.common.widgets.ext.swing.renderer.BooleanCellRenderer;
import com.lucy.g3.gui.common.widgets.ext.swing.table.HighlightPredicateAdapter;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.gui.common.widgets.utils.ComboBoxUtil;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.gui.framework.page.AbstractPage;
import com.lucy.g3.gui.framework.page.UpdatablePage;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.ProtocolPointData;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints.RTUPointsListChangeObserver;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;
import com.lucy.g3.rtu.config.constants.VirtualPointType;

/**
 * <p>
 * A viewable <code>Page</code> which displays the status of all virtual points
 * or protocol points. The status of points will be updated by polling RTU.
 * </p>
 */
// TODO refactor this too big class
public class PointStatusPage extends AbstractPage
    implements UpdatablePage, RTUPointsListChangeObserver {

  public static final String ACTION_KEY_CLEAR_SEARCH = "clearSearch";

  /** Show raw virtual point in table. */
  public static final int VIEW_TYPE_VIRTUAL = 0;

  /** Show protocol points in table. */
  public static final int VIEW_TYPE_PROTOCOL = 1;

  /** Filter item "All" */
  private static final String FILTER_ITEM_ALL = "All";

  private Logger log = Logger.getLogger(PointStatusPage.class);

  // Point data list model
  private final ArrayListModel<IPointData> pointDataList = new ArrayListModel<IPointData>();

  // Point data selection model
  private final MultiListSelectionAdapter<IPointData> pointDataSelectionModel =
      new MultiListSelectionAdapter<IPointData>(
          new MultiSelectionInList<IPointData>(pointDataList)
      );

  /* Flags for indicating if any point has been selected */
  private boolean hasAnalogSelection;
  private boolean hasDigitalSelection;
  private boolean hasCounterSelection;

  private Task<?, ?> updateTask;// Current running periodic task
  private int period = 500; // Polling rate(ms) of the periodic task

  // Listener to the change of search box
  private final DocumentListener searchBoxDocumentListener;

  /*
   * Timer for triggering a task that sends point select command. Timer gives us
   * a delay to send command because it not expected to send command immediately
   * after user changes point filters.
   */
  private final Timer pollingPointsSelectTimer;

  private ConnectionState curState;

  private final Application app;
  private final TaskService taskService;
  private final RTUPoints rtuPoints;
  private final int type; // Page view type

  private final IContext context;

  /**
   * Constructor.
   * @param context 
   *
   * @param application
   *          current running application
   * @param type
   *          the type of this view: <code>VIEW_TYPE_VIRTUAL</code> or
   *          <code>VIEW_TYPE_PROTOCOL</code>
   */
  public PointStatusPage(IContext context, RTUPoints rtuPoints, int type) {
    super(null);
    this.context = context;
    app = Application.getInstance();
    this.rtuPoints = Preconditions.checkNotNull(rtuPoints, "rtuPoints must not be null");
    taskService = app.getContext().getTaskService(IContext.COMMS_TASK_SERVICE_NAME);
    this.type = type;

    pollingPointsSelectTimer = new Timer(1000, new SelectPollingPointAction());
    pollingPointsSelectTimer.setRepeats(false);
    searchBoxDocumentListener = new SearchBoxDocumentListener();

    if (type == VIEW_TYPE_VIRTUAL) {
      setNodeName("Virtual Points");
    } else if (type == VIEW_TYPE_PROTOCOL) {
      setNodeName("Protocol Points");
    } else {
      throw new IllegalArgumentException("Invalid type: " + type);
    }
    // Hack code to diff contents types for help
    Class<?> key = null;
    if (type == 0) {
      key = VirtualPointData.class;
    } else {
      key = ProtocolPointData.class;
    }
    Helper.register(this, key);

    
    subscribeEvent();
  }

  // TODO to support in future
  // /**
  // * Pop-up a dialog to show the detail of the selected point.
  // */
  // private void showPointDetail(){
  // PointDataIF selPointData = pointDataSelection.getSelection();
  //
  // if(selPointData != null){
  // new PointDataDialog(UIUtilities.getMainFrame(),
  // selPointData).setVisible(true);
  // }else{
  // log.warn("Cannot show detail dialog. No selected point!");
  // }
  // }

  @org.jdesktop.application.Action
  public void clearSearch() {
    searchBox.setText("");
  }

  /**
   * Clear the highlighted fields.
   */
  private void clearHighlight() {
    if (pointsTable != null) {
      TableModel model = pointsTable.getModel();
      if (model instanceof RTPointsTableModel) {
        ((RTPointsTableModel) model).clearChangeHistory();
        pointsTable.repaint();
      }
    }
  }

  @Override
  public void startUpdating() {
    pollingPointsSelectTimer.restart();

    if (updateTask == null || updateTask.isDone()) {
      updateTask = new PointStatusUpdateTask(app);
      taskService.execute(updateTask);
      log.info("Points start updating");
    } else {
      log.warn("Cannot start Points Updating task cause it is already running");
    }
  }

  @Override
  public void stopUpdating() {
    pollingPointsSelectTimer.stop();

    if (isUpdating()) {
      if (updateTask.cancel(true) == true) {
        log.info("Point info stops updating");
      } else {
        log.warn("Point info updating task cannot be cancelld");
      }
      updateTask = null;
    }
  }

  @Override
  public void realtimeVirtualPointListUpdated(
      ArrayList<VirtualPointData> newVirtualPoints) {
    if (type == VIEW_TYPE_VIRTUAL) {
      pointDataList.clear();

      if (newVirtualPoints != null) {
        pointDataList.addAll(newVirtualPoints);
      }

      updateFilters();
    }
  }

  @Override
  public void realtimeProtocolPointListUpdated(
      ArrayList<ProtocolPointData> newProtocolPoints) {
    if (type == VIEW_TYPE_PROTOCOL) {
      pointDataList.clear();

      if (newProtocolPoints != null) {
        pointDataList.addAll(newProtocolPoints);
      }

      updateFilters();
    }
  }

  @Override
  protected void init() {
    initComponents();
    
    // Initial monitored points
    if (type == VIEW_TYPE_PROTOCOL) {
      pointDataList.addAll(rtuPoints.getAllProtocolPointData());
    } else if (type == VIEW_TYPE_VIRTUAL) {
      pointDataList.addAll(rtuPoints.getAllVritualPointData());
    }
    updateFilters();
    rtuPoints.addObserver(this);

    // Get polling period from user option.
    try {
      RealtimePreference pref = PreferenceManager.INSTANCE.getPreference(RealtimePreference.ID);

      // Set polling period
      period = pref.getUpdatePeriod();

      // Observe user preference
      pref.addPropertyChangeListener(RealtimePreference.PROPERTY_UPDATE_PERIOD_MS, 
          new PropertyChangeListener() {
        
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            period = (Integer) evt.getNewValue();
        }
      });
      
    } catch (Exception e) {
      log.error("Fail to get user option", e);
    }
  }

  /**
   * Update table's model and filters choices with the latest data
   */
  private void updateFilters() {
    // Update filter components
    updateGroupFilterItems();
    updateModuleFilterItems();
    filterTable();
    // Pack all columns
    Action packAction = pointsTable.getActionMap().get(JXTable.PACKALL_ACTION_COMMAND);
    if (packAction != null) {
      packAction.actionPerformed(new ActionEvent(pointsTable, 0, JXTable.PACKALL_ACTION_COMMAND));
    }
  }

  private void comboFilterActionPerformed(ActionEvent e) {
    JComboBox<?> combo = (JComboBox<?>) e.getSource();
    Object select = combo.getSelectedItem();
    if (select != null) {
      combo.setToolTipText(String.valueOf(select));
    } else {
      combo.setToolTipText(null);
    }
    filterTable();
  }

  /**
   * Update point table's filter with current filter options and send point
   * select command to RTU
   */
  private void filterTable() {
    ArrayList<RowFilter<TableModel, Integer>> filters = new ArrayList<RowFilter<TableModel, Integer>>();

    // Point type filter
    PointTypeFilterChoices selectType = (PointTypeFilterChoices) comboFilterByType.getSelectedItem();
    filters.add(new PointTypeFilter(selectType));

    // Group filter
    Object selFilter = comboFilterByGroup.getSelectedItem();
    if (selFilter != FILTER_ITEM_ALL) {
      filters.add(new PointGroupFilter(selFilter));
    }

    // Module filter
    selFilter = comboFilterByModule.getSelectedItem();
    if (selFilter != FILTER_ITEM_ALL) {
      filters.add(new ModuleFilter(selFilter));
    }

    // Search box filter
    String term = searchBox.getText().trim();
    if (!term.isEmpty()) {
      try {
        term = "(?i)" + term;
        int[] descriptionCol;
        TableModel m = pointsTable.getModel();
        if (m instanceof RTPointsTableModel) {
          descriptionCol = ((RTPointsTableModel) m).getRegFilterColumns();
          RowFilter<TableModel, Integer> regxFilter = RowFilter.regexFilter(term, descriptionCol);
          filters.add(regxFilter);
        }
      } catch (java.util.regex.PatternSyntaxException e) {
        log.warn("Regx expression error: " + e.getMessage());
      }
    }

    // Apply new filter
    // pointDataSelection.clearSelection();
    pointsTable.setRowFilter(RowFilter.andFilter(filters));

    // Re-select polling points cause the point list is filtered.
    if (isUpdating()) {
      pollingPointsSelectTimer.restart();
    }
  }

  private boolean isUpdating() {
    return updateTask != null && !updateTask.isDone();
  }

  private void updateGroupFilterItems() {
    ArrayList<String> groups = new ArrayList<String>();

    for (IPointData vp : pointDataList) {
      String groupName = vp.getGroupName();
      if (groupName != null && !groups.contains(groupName)) {
        groups.add(groupName);
      }
    }

    //Object[] array = groups.toArray();
    Collections.sort(groups, new InternalNumberComparator());

    groups.add(0, FILTER_ITEM_ALL);

    // Arrays.sort(array, new InternalNumberComparator());

    // Collections.sort(groups, new Comparator<String>() {
    //
    // @Override
    // public int compare(String o1, String o2) {
    // System.out.println(o1+ ""+ o2);
    // int a = 0;
    // int b = 0;
    // if(o1.contains("["))
    // a = getNumberFromPoint(o1);
    // if(o2.contains("["))
    // b = getNumberFromPoint(o2);
    //
    // if(a<b)
    // return a;
    // else
    // return b;
    // }
    //
    // });

    comboFilterByGroup.setModel(new DefaultComboBoxModel<>(groups.toArray()));
    comboFilterByGroup.setSelectedIndex(0);
  }

  private void updateModuleFilterItems() {
    ArrayList<String> modules = new ArrayList<String>();
    modules.add(FILTER_ITEM_ALL);

    for (IPointData vp : pointDataList) {
      String owner = vp.getMappingModuleName();
      if (owner != null && !modules.contains(owner)) {
        modules.add(owner);
      }
    }

    comboFilterByModule.setModel(new DefaultComboBoxModel<>(modules.toArray()));
    comboFilterByModule.setSelectedIndex(0);
  }

  private JXTable createPointsTable() {
    RTPointsTableModel tm;
    if (type == VIEW_TYPE_PROTOCOL) {
      tm = new RTProtocolPointsTableModel(pointDataList);
    } else {
      tm = new RTVirtualPointsTableModel(pointDataList);
    }

    JXTable pointable = new JXTable(tm);
    UIUtils.setDefaultConfig(pointable , true, true, true);
    
    // Binding table to selection model.
    Bindings.bind(pointable, pointDataList, pointDataSelectionModel);

    // Set column width and visibility
    TableColumnExt column;
    if (type == VIEW_TYPE_VIRTUAL) {
      // NOTE: column width must be set before hiding any column
      column = pointable.getColumnExt(RTVirtualPointsTableModel.COLUMN_VALUE);
      column.setMinWidth(120);
      column.setCellRenderer(new FixWidthFontRenderer());
      
      column = pointable.getColumnExt(RTVirtualPointsTableModel.COLUMN_ONLINE);
      column.setCellRenderer(BooleanCellRenderer.createOnline());

      column = pointable.getColumnExt(RTVirtualPointsTableModel.COLUMN_TYPE);
      column.setVisible(false);
    } else if (type == VIEW_TYPE_PROTOCOL) {
      column = pointable.getColumnExt(RTProtocolPointsTableModel.COLUMN_VALUE);
      column.setMinWidth(120);
      column.setCellRenderer(new FixWidthFontRenderer());

      column = pointable.getColumnExt(RTProtocolPointsTableModel.COLUMN_MAP_POINT);
      column.setVisible(false);
    }

    // Set cell renderer
    pointable.setDefaultRenderer(Boolean.class, new BooleanCellRenderer());
    pointable.setDefaultRenderer(Date.class, new PointsTableRenderer());
    pointable.setDefaultRenderer(Long.class, new LongRenderer());

    // Set cell highlighter
    if (tm instanceof ISupportHighlight) {
      HighlightPredicate predicator = new HighlightPredicateAdapter(tm);
      Highlighter hlighter0 = new ColorHighlighter(predicator, Color.yellow, Color.black);

      // Set enable/disable for online/offline point.
      // Highlighter hlighter1 = new EnabledHighlighter(new
      // DisableEntryPredicator());
      Highlighter hlighter1 = new ColorHighlighter(new DisabledCellPredicator(), Color.white, Color.gray);

      pointable.setHighlighters(hlighter0, hlighter1);
    }

    // Grid colour
    pointable.setGridColor(Color.lightGray);


    // Disable the action "Enable Horizontal Scroll" in controller pop up menu
    // pointsTable.getActionMap().put(JXTable.HORIZONTALSCROLL_ACTION_COMMAND,
    // null);

    return pointable;
  }

  private void createUIComponents() {
    pointsTable = createPointsTable();

    comboFilterByType = new JComboBox<Object>(PointTypeFilterChoices.values());

    btnClearSearch = new JButton(app.getContext().getActionMap(this).get(ACTION_KEY_CLEAR_SEARCH));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    tableScroll = new JScrollPane();
    controlPanel = new JPanel();
    label1 = new JLabel();
    label3 = new JLabel();
    comboFilterByGroup = new JComboBox<>();
    label2 = new JLabel();
    comboFilterByModule = new JComboBox<>();
    label4 = new JLabel();
    searchBox = new JTextField();
    btnClearHighlight = new JButton();
    panelWarning = new JPanel();
    labelWarning = new JLabel();
    labelReadConf = new JXHyperlink();

    //======== this ========
    setBorder(BorderFactory.createEmptyBorder());
    setLayout(new BorderLayout());

    //======== tableScroll ========
    {
      tableScroll.setBorder(BorderFactory.createEmptyBorder());

      //---- pointsTable ----
      pointsTable.setFillsViewportHeight(true);
      pointsTable.setBorder(BorderFactory.createEmptyBorder());
      pointsTable.setDoubleBuffered(true);
      pointsTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
      pointsTable.setEditable(false);
      pointsTable.setColumnControlVisible(true);
      pointsTable.setName("pointStatusTable");
      pointsTable.setHorizontalScrollEnabled(true);
      tableScroll.setViewportView(pointsTable);
    }
    add(tableScroll, BorderLayout.CENTER);

    //======== controlPanel ========
    {
      controlPanel.setBorder(Borders.DLU4_BORDER);
      controlPanel.setRequestFocusEnabled(false);
      controlPanel.setMinimumSize(new Dimension(167, 20));
      controlPanel.setLayout(new FormLayout(
        "[50dlu,pref,120dlu], [default,20dlu]",
        "default, $lgap, default, $ugap, default, $lgap, default, $ugap, default, $lgap, default, $ugap, default, $lgap, fill:default, $ugap, default:grow, $lgap, fill:default, $lgap, 20dlu"));

      //---- label1 ----
      label1.setText("Point Type:");
      controlPanel.add(label1, CC.xy(1, 1));

      //---- comboFilterByType ----
      comboFilterByType.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          comboFilterActionPerformed(e);
        }
      });
      controlPanel.add(comboFilterByType, CC.xywh(1, 3, 2, 1));

      //---- label3 ----
      label3.setText("Point Group:");
      controlPanel.add(label3, CC.xy(1, 5));

      //---- comboFilterByGroup ----
      comboFilterByGroup.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          comboFilterActionPerformed(e);
        }
      });
      controlPanel.add(comboFilterByGroup, CC.xywh(1, 7, 2, 1));

      //---- label2 ----
      label2.setText("Module:");
      controlPanel.add(label2, CC.xy(1, 9));

      //---- comboFilterByModule ----
      comboFilterByModule.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          comboFilterActionPerformed(e);
        }
      });
      controlPanel.add(comboFilterByModule, CC.xywh(1, 11, 2, 1));

      //---- label4 ----
      label4.setText("Search:");
      controlPanel.add(label4, CC.xy(1, 13));
      controlPanel.add(searchBox, CC.xy(1, 15));

      //---- btnClearSearch ----
      btnClearSearch.setHideActionText(true);
      btnClearSearch.setContentAreaFilled(false);
      btnClearSearch.setFocusPainted(false);
      controlPanel.add(btnClearSearch, CC.xy(2, 15));

      //---- btnClearHighlight ----
      btnClearHighlight.setText("Clear Highlight");
      btnClearHighlight.setToolTipText("Clear all hilighlight fileds");
      btnClearHighlight.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          clearHighlight();
        }
      });
      controlPanel.add(btnClearHighlight, CC.xy(1, 19));
    }
    add(controlPanel, BorderLayout.EAST);

    //======== panelWarning ========
    {
      panelWarning.setLayout(new FormLayout(
        "2*(default, $lcgap), default:grow",
        "fill:default:grow"));

      //---- labelWarning ----
      labelWarning.setText("  *  RTU configuration required!");
      labelWarning.setHorizontalAlignment(SwingConstants.LEFT);
      labelWarning.setForeground(Color.gray);
      panelWarning.add(labelWarning, CC.xy(1, 1));

      //---- labelReadConf ----
      labelReadConf.setText("Click here to read configuration from RTU.");
      labelReadConf.setClickedColor(Color.gray);
      labelReadConf.setForeground(Color.gray);
      panelWarning.add(labelReadConf, CC.xy(3, 1));
    }
    add(panelWarning, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ----[ Custom code ]----

    // Search box event handling
    searchBox.getDocument().addDocumentListener(searchBoxDocumentListener);

    PromptSupport.setPrompt("Point's ID/description", searchBox);

    // Click blank area of control panel to clear selection
    controlPanel.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        pointsTable.clearSelection();
      }
    });

    // Binding visibility of panelWarning
    PropertyAdapter<?> vm = new PropertyAdapter<RTUPoints>(
        rtuPoints, RTUPoints.PROPERTY_POINTS_LOADED, true);
    ValueModel converter = new ConverterValueModel(vm, new BooleanNegator());

    /*
     * DO NOT USE binding API cause it will automatically binding the label to a
     * ComponentValueModel,which will change visible to true.
     */
    // Bindings.bind(labelWarning, "visible", converter);
    PropertyConnector.connectAndUpdate(converter, panelWarning, "visible");

    // Add hover effect
    ButtonHoverEffect.install(btnClearSearch);
    
    ComboBoxUtil.makeWider(comboFilterByGroup);
    ComboBoxUtil.makeWider(comboFilterByModule);
    ComboBoxUtil.makeWider(comboFilterByType);
    
    labelReadConf.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        ConfigurationManager confmgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
        confmgr.readConfig();
      }
    });

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane tableScroll;
  private JXTable pointsTable;
  private JPanel controlPanel;
  private JLabel label1;
  private JComboBox<Object> comboFilterByType;
  private JLabel label3;
  private JComboBox<Object> comboFilterByGroup;
  private JLabel label2;
  private JComboBox<Object> comboFilterByModule;
  private JLabel label4;
  private JTextField searchBox;
  private JButton btnClearSearch;
  private JButton btnClearHighlight;
  private JPanel panelWarning;
  private JLabel labelWarning;
  private JXHyperlink labelReadConf;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Try to find out the virtual point at row of a points AbstractTableAdapter.
   *
   * @param tableModel
   *          supposed to be a AbstractTableAdapter which adapts a list of
   *          points to table model.
   * @param index
   *          the position of the point in the table.
   * @return the found virtual point, null if not found.
   */
  private static IPointData getVPointOfTableModelByIndex(TableModel tableModel, int index) {
    IPointData point = null;
    if (tableModel != null && tableModel instanceof RTPointsTableModel) {
      point = ((RTPointsTableModel) tableModel).getPointData(index);
    }
    return point;
  }


  /**
   * Get the list of filtered points in point table and send point selection
   * command to RTU to enable those filtered point being monitored.
   */
  private class SelectPollingPointAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      if (pointsTable == null) {
        return;
      }

      if (!isUpdating()) {
        return;
      }

      if (!ConnectionState.isNormalMode(curState)) {
        return;
      }

      ArrayList<IPointData> analogPointsInView = new ArrayList<IPointData>();
      ArrayList<IPointData> digiPointsInView = new ArrayList<IPointData>();
      ArrayList<IPointData> counterPointsInView = new ArrayList<IPointData>();

      /* Select points to be monitored */
      TableModel tableModel = pointsTable.getModel();
      if (tableModel instanceof RTPointsTableModel) {
        int viewRowCount = pointsTable.getRowSorter().getViewRowCount();
        RTPointsTableModel model = (RTPointsTableModel) tableModel;
        for (int i = 0; i < viewRowCount; i++) {
          int pointIndex = pointsTable.convertRowIndexToModel(i);
          IPointData p = model.getPointData(pointIndex);
          if (p != null) {
            VirtualPointType t = (VirtualPointType) p.getType();
            if (t.isAnalogue()) {
              analogPointsInView.add(p);
            } else if (t.isDigital()) {
              digiPointsInView.add(p);
            } else if (t.isCounter()) {
              counterPointsInView.add(p);
            } else {
              log.error("Unsupported point type: " + t);
            }
          }
        }
      }

      // Reset selection flags
      hasAnalogSelection = false;
      hasDigitalSelection = false;
      hasCounterSelection = false;

      // Run task to select polling points
      Task<?, ?> selectPointTask = new PointSelectTask(app,
          analogPointsInView,
          digiPointsInView,
          counterPointsInView);
      taskService.execute(selectPointTask);
    }
  }

  private class PointStatusUpdateTask extends Task<Void, Void> {

    public PointStatusUpdateTask(Application application) {
      super(application);
    }

    @Override
    protected Void doInBackground() {
      message("startMessage");
      while (!isCancelled()) {

        if (!ConnectionState.isNormalMode(curState)) {
          continue;
        }

        long start = System.currentTimeMillis();
        try {
          if (hasAnalogSelection) {
            rtuPoints.cmdPollPointStatus(G3Protocol.PollingMode.ANALOG, true);
          }

          if (hasDigitalSelection) {
            rtuPoints.cmdPollPointStatus(G3Protocol.PollingMode.DIGITAL, true);
          }

          if (hasCounterSelection) {
            rtuPoints.cmdPollPointStatus(G3Protocol.PollingMode.COUNTER, true);
          }

        } catch (Exception e) {
          log.error("Fail to update point status: " + e.getMessage());
        }

        /*
         * Repaint is needed cause point's change doesn't fire event, we need to
         * manually update UI to reflect the data changes. Note method repaint()
         * is thread safe.
         */
        if (pointsTable != null) {
          pointsTable.repaint();
        }

        long end = System.currentTimeMillis();
        long timecost = end - start;
        if (timecost > 1000) {
          log.warn("Points Polling - RTU is responding too slowly(" + timecost + " ms).");
        }

        // Delay
        try {
          if (timecost > 0 && timecost < period) {
            Thread.sleep(period - timecost);
          }
        } catch (InterruptedException e) {
        }
      }
      return null;
    }

    @Override
    protected void cancelled() {
      super.cancelled();
      message("cancelMessage");
    }

    @Override
    protected void failed(Throwable cause) {
      super.failed(cause);
      message("errorMessage");
    }
  }

  private class SearchBoxDocumentListener implements DocumentListener {

    @Override
    public void removeUpdate(DocumentEvent e) {
      filterTable();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
      filterTable();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
      filterTable();
    }
  }

  /**
   * This task sends a point select command to RTU for selecting a list of point
   * to be polled.
   */
  private class PointSelectTask extends Task<Void, Void> {

    private ArrayList<IPointData> analogPoints;
    private ArrayList<IPointData> counterPoints;
    private ArrayList<IPointData> digitalPoints;


    public PointSelectTask(Application application,
        ArrayList<IPointData> analogPoints,
        ArrayList<IPointData> digitalPoints,
        ArrayList<IPointData> counterPoints) {
      super(application);
      this.analogPoints = analogPoints;
      this.digitalPoints = digitalPoints;
      this.counterPoints = counterPoints;
    }

    @Override
    protected void failed(Throwable cause) {
      String msg = "Fail to select points for mointor: " + cause.getMessage();
      log.error(msg, cause);
      setMessage(msg);
    }

    @Override
    protected Void doInBackground() throws Exception {
      if (!ConnectionState.isNormalMode(curState)) {
        return null;
      }

      rtuPoints.cmdClearSelectedPoints(G3Protocol.PollingMode.ANALOG);
      rtuPoints.cmdClearSelectedPoints(G3Protocol.PollingMode.DIGITAL);
      rtuPoints.cmdClearSelectedPoints(G3Protocol.PollingMode.COUNTER);

      if (analogPoints != null && analogPoints.size() > 0) {
        IPointData[] points = analogPoints.toArray(new IPointData[analogPoints.size()]);
        rtuPoints.cmdSelectPoints(points, G3Protocol.PollingMode.ANALOG);

        log.info("Selected analogue polling points number: " + analogPoints.size());
      }
      hasAnalogSelection = analogPoints.size() > 0;

      if (digitalPoints != null && digitalPoints.size() > 0) {
        IPointData[] points = digitalPoints.toArray(new IPointData[digitalPoints.size()]);
        rtuPoints.cmdSelectPoints(points, G3Protocol.PollingMode.DIGITAL);
        log.info("Selected digital polling points number: " + digitalPoints.size());
      }
      hasDigitalSelection = digitalPoints.size() > 0;

      if (counterPoints != null && counterPoints.size() > 0) {
        IPointData[] points = counterPoints.toArray(new IPointData[counterPoints.size()]);
        rtuPoints.cmdSelectPoints(points, G3Protocol.PollingMode.COUNTER);
        log.info("Selected digital polling points number: " + counterPoints.size());
      }
      hasCounterSelection = counterPoints.size() > 0;

      // Debug
      if (log.isDebugEnabled()) {
        StringBuilder sb = new StringBuilder();
        sb.append("Selected analogue points: ");
        for (IPointData p : analogPoints) {
          sb.append("\n");
          sb.append(p.getGroupIDString());
        }
        log.debug(sb.toString());

        sb.setLength(0);
        sb.append("Selected digital points: ");
        for (IPointData p : digitalPoints) {
          sb.append("\n");
          sb.append(p.getGroupIDString());
        }
        log.debug(sb.toString());

        sb.setLength(0);
        sb.append("Selected counter points: ");
        for (IPointData p : counterPoints) {
          sb.append("\n");
          sb.append(p.getGroupIDString());
        }
        log.debug(sb.toString());
      }

      return null;
    }
  }

  static class FixWidthFontRenderer extends DefaultTableRenderer {

    private final Font font = new Font("Courier New", Font.PLAIN, 12);

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      comp.setFont(font);
      return comp;
    }


  }

  private static class PointsTableRenderer extends DefaultTableRenderer {

    public PointsTableRenderer() {
      super(new StringValue() {
        
        @Override
        public String getString(Object value) {
          if (value != null && value instanceof Date) {
            return RTUTimeDecoder.formatPoint((Date) value);
          }
          return null;
        }
      });
    }
  }

  private static class LongRenderer extends DefaultTableRenderer {

    public LongRenderer() {
      super(StringValues.TO_STRING, SwingConstants.CENTER);
    }

  }

  private static class PointTypeFilter extends RowFilter<TableModel, Integer> {

    private final PointTypeFilterChoices type;


    public PointTypeFilter(PointTypeFilterChoices type) {
      this.type = type;
    }

    @Override
    public boolean include(javax.swing.RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
      int index = entry.getIdentifier().intValue();
      IPointData point = getVPointOfTableModelByIndex(entry.getModel(), index);

      // Filter by type
      boolean includedByType = true;
      if (point != null && type != null) {
        switch (type) {
        case ALL:
          includedByType = true;
          break;

        case ANALOG:
          includedByType = (point.getType() == VirtualPointType.ANALOGUE_INPUT);
          break;

        case BINARY:
          includedByType = point.getType() == VirtualPointType.BINARY_INPUT;
          break;

        case DOUBLE_BINARY:
          includedByType = point.getType() == VirtualPointType.DOUBLE_BINARY_INPUT;
          break;

        case DIGITAL:
          includedByType = point.getType() == VirtualPointType.BINARY_INPUT
              || point.getType() == VirtualPointType.DOUBLE_BINARY_INPUT;
          break;

        case COUNTER:
          includedByType = point.getType() == VirtualPointType.COUNTER;
          break;

        default:
          includedByType = true;
        }
      }

      return includedByType;
    }
  }

  private static class PointGroupFilter extends RowFilter<TableModel, Integer> {

    private final Object pointGroup;


    public PointGroupFilter(Object pointGroup) {
      this.pointGroup = pointGroup;
    }

    @Override
    public boolean include(javax.swing.RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
      int index = entry.getIdentifier().intValue();
      IPointData point = getVPointOfTableModelByIndex(entry.getModel(), index);

      return pointGroup == null // No filter
          || pointGroup.equals(FILTER_ITEM_ALL) // Filter set to "All"
          || (point != null && point.getGroupName() != null
          && pointGroup.equals(point.getGroupName()));
    }
  }

  private static class ModuleFilter extends RowFilter<TableModel, Integer> {

    private final Object moduleFilterItem;


    public ModuleFilter(Object moduleFilterItem) {
      this.moduleFilterItem = moduleFilterItem;
    }

    @Override
    public boolean include(javax.swing.RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
      int index = entry.getIdentifier().intValue();
      IPointData pointData = getVPointOfTableModelByIndex(entry.getModel(), index);

      return moduleFilterItem == null
          || moduleFilterItem.equals(FILTER_ITEM_ALL)
          || (pointData != null && pointData.getMappingModuleName() != null
          && moduleFilterItem.equals(pointData.getMappingModuleName()));
    }
  }

  private class DisabledCellPredicator implements HighlightPredicate {

    @Override
    public boolean isHighlighted(Component renderer,
        ComponentAdapter adapter) {
      if (adapter.row >= 0) {
        int rowInModel = adapter.convertRowIndexToModel(adapter.row);
        return pointDataList.get(rowInModel).isOnline() != Boolean.TRUE;
      }
      return false;
    }

  }

  static enum PointTypeFilterChoices {
    ALL("All"),
    ANALOG("Analogue"),
    BINARY("Single Binary"),
    DOUBLE_BINARY("Double Binary"),
    DIGITAL("Single & Double Binary"),
    COUNTER("Counter");

    PointTypeFilterChoices(String description) {
      this.description = description;
    }


    private String description;


    @Override
    public String toString() {
      return description;
    }
  }


  private void subscribeEvent() {
    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, 
        new IEventHandler() {
      
      @Override
      public void handleEvent(Event event) {
        ConnectionState newState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
        curState = newState;
        
        /*
         * It is essential to re-send a command to select polling points in RTU
         * device once the application is going online, otherwise the points data
         * may not be updated.
         */
        if (ConnectionState.isNormalMode(newState)) {
          pollingPointsSelectTimer.restart();
        }
      }
    });
  }

  @Override
  public boolean hasError() {
    return false;
  }
}
