/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.points;

import javax.swing.table.TableModel;

import com.lucy.g3.gui.common.widgets.ext.swing.table.ISupportHighlight;
import com.lucy.g3.rtu.comms.service.realtime.points.IPointData;

/**
 * Extended table model interface for real time point table.
 */
public interface RTPointsTableModel extends TableModel, ISupportHighlight {

  /**
   * Try to get the raw virtual point data object at a specific row.
   *
   * @param row
   * @return
   */
  IPointData getPointData(int row);

  /**
   * Gets the index of columns that can be filtered by regular express filter.
   *
   * @return the column index array. <code>null</code> if no column is supposed
   *         to be filtered by regluar express.
   */
  int[] getRegFilterColumns();


  /**
   * Clear change history.
   */
  void clearChangeHistory();

}
