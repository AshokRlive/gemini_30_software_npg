/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.ui.modules;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.config.module.res.ModuleResource;

/**
 * The model of a module table for displaying real time module information.
 */
public class RTModulesTableModel extends AbstractTableAdapter<ModuleInfo> {

  private static final ModuleInfoColumn[] columnEnums = ModuleInfoColumn.values();

  private final Object TEXT_NA = "N/A";

  private final ModuleResource nameResource = ModuleResource.INSTANCE;


  public RTModulesTableModel(ListModel<ModuleInfo> listModel) {
    super(listModel);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    if (rowIndex >= getRowCount() || rowIndex < 0) {
      return "";
    }

    ModuleInfo minfo = getRow(rowIndex);
    ModuleInfoColumn columnEnum = columnEnums[columnIndex];

    Object value = null;
    switch (columnEnum) {
    case MODULE:
      value = nameResource.getModuleFullName(minfo.getModuleType(), minfo.getModuleID());
      break;

    case SERIAL:
      value = minfo.getSerialNo();
      break;
      
    case SUPPLIER:
      value = minfo.getSupplierID();
      break;

    case FEATURE_REVISION:
      value = minfo.getFeatureVersion();
      break;

    case FIRMWARE_VERSION:
      value = minfo.getVersion();
      break;

    case API_VERSION:
      value = minfo.getSystemAPI();
      break;

    case BOOTLOADER_VERSION:
      value = minfo.getBootVersion();
      break;

    case BOOTLOADER_API:
      value = minfo.getBootAPI();
      break;

    case INTERNAL_STATE:
      value = minfo.getInternalState();
      break;

    case ERRORS:
      value = minfo.getErrorText();
      break;

    case STATUS:
      value = minfo.getAllStatusText();
      break;

    default:
      value = "Unknown column";
      break;
    }

    if (value == null) {
      value = TEXT_NA;
    }

    return value;
  }

  @Override
  public int getColumnCount() {
    return columnEnums.length;
  }

  @Override
  public String getColumnName(int columnIndex) {
    return columnEnums[columnIndex].getName();
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return false;
  }


  /**
   * Column Enums.
   */
  public enum ModuleInfoColumn {
    MODULE("Module"),
    SERIAL("Serial"),
    SUPPLIER("Supplier ID"),
    FEATURE_REVISION("Feature"),
    FIRMWARE_VERSION("Firmware"),
    API_VERSION("System API"),
    BOOTLOADER_VERSION("Bootloader"),
    BOOTLOADER_API("Bootloader API"),
    INTERNAL_STATE("Internal State"),
    ERRORS("Active Alarms"),
    STATUS("Status");

    ModuleInfoColumn(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public int getIndex() {
      return ordinal();
    }


    private final String name;
  }
}
