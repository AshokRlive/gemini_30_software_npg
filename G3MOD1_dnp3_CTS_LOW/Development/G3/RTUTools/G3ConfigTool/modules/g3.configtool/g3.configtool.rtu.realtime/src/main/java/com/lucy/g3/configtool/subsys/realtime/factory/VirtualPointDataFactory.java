/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.realtime.factory;

import java.util.ArrayList;
import java.util.List;

import com.lucy.g3.configtool.subsys.realtime.formatter.PointValueFormatter;
import com.lucy.g3.rtu.comms.service.realtime.points.VirtualPointData;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.AnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.IPseudoPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPoint;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.label.ValueLabelSet;

/**
 *
 *
 */
public class VirtualPointDataFactory {

  private final static PointValueFormatter VALUE_FORMATTER = PointValueFormatter.getFormatter();
  
  public static List<VirtualPointData> createPointData(VirtualPoint vpoint) {
    VirtualPointType type = vpoint.getType();
    int group = vpoint.getGroup();
    int id = vpoint.getId();
    String refernceText = vpoint.getName();
    String description = refernceText.substring(refernceText.indexOf("]") + 1);
    String unit, scale;
    String groupName, moduleName;
    ArrayList<VirtualPointData> dataList = new ArrayList<>();
    
    // Get unit & scaling factor
    if (vpoint instanceof AnaloguePoint) {
      double scalingFactor = ((AnaloguePoint) vpoint).getScalingFactor();
      unit = ((AnaloguePoint) vpoint).getUnit();
      unit = unit != null ? unit : "";
      scale = scalingFactor != 0 ? Double.toString(scalingFactor) : "";
    } else {
      unit = null;
      scale = null;
    }

    // Get group name
    if (vpoint instanceof IPseudoPoint) {
      groupName = ((IPseudoPoint) vpoint).getSource().getName();
    } else {
      groupName = "[" + group + "]Virtual Points";
    }

    // Get module name
    moduleName = vpoint.getSourceModule() != null ? vpoint.getSourceModule().getShortName() : null;

    
    switch (type) {
    case ANALOGUE_INPUT:
      dataList.add(new VirtualPointData.AIPointData(VALUE_FORMATTER,
          type, groupName, moduleName, group, id, description, unit, scale));
      break;
    
    case BINARY_INPUT:
    case DOUBLE_BINARY_INPUT:
      dataList.add(new VirtualPointData.DIPointData(VALUE_FORMATTER,
          type, groupName, moduleName, group, id, description, unit, scale));
      break;
    
    case COUNTER:
      dataList.add(new VirtualPointData.CounterPointData(VALUE_FORMATTER,
          type, groupName, moduleName, group, id, description, unit, scale, false));
      dataList.add(new VirtualPointData.CounterPointData(VALUE_FORMATTER,
          type, groupName, moduleName, group, id, description+"(Frozen)", unit, scale, true));
      break;
      
    default:
      throw new RuntimeException("Unsupported type:"+type);
    }
    
    // Set label set if exists
    ValueLabelSet labelSet = vpoint.getLabelSetRef().getLabelSet();
    if (labelSet != null) {
      for (VirtualPointData d : dataList) {
        d.setLabelSet(labelSet.convertToMap());
      }
    }

    return dataList;
  }
}
