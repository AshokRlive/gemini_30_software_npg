/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.integrated;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.rtu.connection.IConnectionManagerCallbacks;
import com.lucy.g3.configtool.rtu.controller.RTUController;
import com.lucy.g3.configtool.rtu.diagnostic.RTUDiagnositics;
import com.lucy.g3.configtool.rtu.eventlog.manager.EventLogManager;
import com.lucy.g3.configtool.rtu.restart.RTURestartController;
import com.lucy.g3.configtool.rtu.sysalarm.SystemAlarmsManager;
import com.lucy.g3.configtool.rtu.syslog.manager.SystemLogManager;
import com.lucy.g3.configtool.rtu.test.RTUTester;
import com.lucy.g3.configtool.rtu.upgrade.RTUUpgrader;
import com.lucy.g3.configtool.rtu.upgrade.SDPUpgrader;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.rtu.manager.G3RTUManager;


/**
 *
 */
public class SubsystemPlugin{
  private SubsystemPlugin () {}
  

  public static void init(com.lucy.g3.common.context.IContext ctxt) {

    G3RTU g3RTU = G3RTUFactory.getDefault();
    
    // Create all subsystems
    new ConfigurationManager(ctxt);
    new ConnectionManager(ctxt, ConnectionState.DISCONNECTED, new ConnectionManagerCallback(ctxt));
    new EventLogManager(ctxt, CommsUtil.getRTUEventsAPI(G3RTUFactory.getComms()));
    new RTUController(ctxt, g3RTU);
    new RTUDiagnositics(ctxt);
    new RTURestartController(ctxt);
    new RTUTester(ctxt, g3RTU);
    new SystemAlarmsManager(ctxt);
    new SystemLogManager(ctxt);
    new RTUUpgrader(ctxt);
    new SDPUpgrader(ctxt, g3RTU);
    //new com.lucy.g3.configtool.iec61131.IEC61131Subsystem(ctxt);
    new G3RTUManager(ctxt);
  }
  
  private static class ConnectionManagerCallback implements IConnectionManagerCallbacks {
    private final IContext context;
    
    
    public ConnectionManagerCallback(IContext context) {
      super();
      this.context = context;
    }

    @Override
    public void readConfig() {
      ((ConfigurationManager)context.getComponent(ConfigurationManager.SUBSYSTEM_ID))
      .readConfig();
    }

    @Override
    public void readRTUInfo() {
      ((RTUController)context.getComponent(RTUController.SUBSYSTEM_ID))
      .refreshRtuInfo();
    }
  
  }
}

