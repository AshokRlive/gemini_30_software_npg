/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.subsys.integrated;

import org.junit.Test;

import com.lucy.g3.configtool.subsys.integrated.SubsystemPlugin;


/**
 *
 */
public class SubsystemPluginTest {

  @Test
  public void test() {
    SubsystemPlugin.init(null);
  }

}

