/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.ntp.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.ButtonBarFactory;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.configtool.rtu.controller.ntp.NTPSettings;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.rtu.manager.G3RTU;

/**
 * The Dialog for configuring NTP.
 * @deprecated temporary solution for configuring NTP.
*/
@Deprecated
public class NTPSettingsDialog extends com.lucy.g3.gui.common.dialogs.AbstractDialog {

  
  private final NTPSettings ntp;
  
  public NTPSettingsDialog(Frame owner) {
    this(owner, null);
  }
  
  public NTPSettingsDialog(Frame owner, G3RTU rtu) {
    super(owner);
    this.ntp = new NTPSettings(rtu);
    initComponents();
    initEventHandling();
    
    // Read current script from RTU.
    ntp.readFromRTU();
  }

  private void initEventHandling() {
    // Refresh GUI when NTP script changes 
    ntp.addPropertyChangeListener(NTPSettings.PROPERTY_NTP_SCRIPT,
        new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            taNTPEditor.setText((String)evt.getNewValue());
          }
        });
  }

  private void btnLoadActionPerformed(ActionEvent e) {
    if (Strings.isBlank(taNTPEditor.getText()) == false) {
      int ret = JOptionPane.showConfirmDialog(this, 
          "The current editing script will be discarded.\n"
          + "Do you really want to continue?", 
          "Warning", 
          JOptionPane.YES_NO_CANCEL_OPTION);
      
      if (ret != JOptionPane.YES_OPTION) {
        return;
      }
      
      File file = DialogUtils.showFileChooseDialog(this, "Choose NTP script file", null);
      if (file != null) {
        ntp.loadFromFile(file);
      }
    }
  }

  private void btnApplyActionPerformed(ActionEvent e) {
    ntp.setNtpScript(taNTPEditor.getText());
    ntp.writeToRTU();
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    contentPanel = new JPanel();
    scrollPane1 = new JScrollPane();
    taNTPEditor = new JTextArea();
    label1 = new JLabel();
    btnLoad = new JButton();
    btnApply = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(400, 300));
    setTitle("NTP Settings");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== contentPanel ========
    {
      contentPanel.setLayout(new BorderLayout(0, 10));

      //======== scrollPane1 ========
      {

        //---- taNTPEditor ----
        taNTPEditor.setText("Loading script from RTU....");
        scrollPane1.setViewportView(taNTPEditor);
      }
      contentPanel.add(scrollPane1, BorderLayout.CENTER);

      //---- label1 ----
      label1.setText("- NTP Configuration Script -");
      contentPanel.add(label1, BorderLayout.NORTH);
    }
    contentPane.add(contentPanel, BorderLayout.CENTER);

    //---- btnLoad ----
    btnLoad.setText("Load from file...");
    btnLoad.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnLoadActionPerformed(e);
      }
    });

    //---- btnApply ----
    btnApply.setText("Apply");
    btnApply.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnApplyActionPerformed(e);
      }
    });

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return ButtonBarFactory.buildRightAlignedBar(
        btnLoad,
        btnApply,
        new JButton(getAction(ACTION_KEY_CLOSE)));
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel contentPanel;
  private JScrollPane scrollPane1;
  private JTextArea taNTPEditor;
  private JLabel label1;
  private JButton btnLoad;
  private JButton btnApply;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
