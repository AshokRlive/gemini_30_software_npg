/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.ntp;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.bean.Bean;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.service.filetransfer.FileEndInfo;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.rtu.filetransfer.FileWritingTask;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;


/**
 * The Class for controlling the NTP configuration.
 * 
 * @deprecated temporary solution for configuring NTP.
 */
@Deprecated
public class NTPSettings extends Bean{

  public static final String PROPERTY_NTP_SCRIPT = "ntpScript";
  
  private static final int MAX_NTP_LINE_NUM = 500;
  
  private static final String  RTU_CONF_FOLDER_PATH = "../application/config/";
  private static final String  NTP_CONF_FILE_NAME = "ntp.conf";
  private static final String  NTP_CONF_FILE_PATH = RTU_CONF_FOLDER_PATH + NTP_CONF_FILE_NAME;
  
  private Logger log = Logger.getLogger(NTPSettings.class);
  
  private final G3RTU rtu;
  private final File localNTPFile = new File(UIUtils.getLocalStorageDir(), NTP_CONF_FILE_NAME);
  
  private Component parent;
  
  private String ntpScript;
  
  public NTPSettings() {
    this(null);
  }
  
  public NTPSettings(G3RTU rtu) {
    this.rtu = rtu == null ? G3RTUFactory.getDefault() : rtu;
  }
  
  public File getLocalNTPFile() {
    return localNTPFile;
  }

  public String getNtpScript() {
    return ntpScript;
  }

  public void setNtpScript(String ntpScript) {
    Object oldValue = this.ntpScript;
    this.ntpScript = ntpScript;
    firePropertyChange(PROPERTY_NTP_SCRIPT, oldValue, ntpScript);
  }

  public void setParentComponent(Component parent) {
    this.parent = parent;
  }
  
  public void loadFromFile(File scriptFile) {
    setNtpScript(readFile(scriptFile));
  }

  G3RTU getRTU() {
    return rtu;
  }
  
  /**
   * Writes local NTP config to RTU.
   */
  public FileWritingTask writeToRTU() {
    writeFile(localNTPFile, getNtpScript());
    return writeToRTU(localNTPFile);
  }
  
  /**
   * Writes a new NTP config to RTU.
   */
  public FileWritingTask writeToRTU(File newNTPConf) {
    Preconditions.checkNotNull(newNTPConf, "newNTPConf must not be null");

    FileWritingTask task = FileTransferService.createFileWriteTask(rtu.getComms().getFileTransfer(), CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC,
        NTP_CONF_FILE_PATH, newNTPConf);
    task.addTaskListener(new TaskListener.Adapter<FileEndInfo[], Void>() {

      @Override
      public void succeeded(TaskEvent<FileEndInfo[]> event) {
        JOptionPane.showMessageDialog(parent, 
            "The NTP script has been written to RTU successfully!",
            "Succeeded", JOptionPane.INFORMATION_MESSAGE);
      }

      @Override
      public void failed(TaskEvent<Throwable> event) {
        MessageDialogs.error(parent, 
            "Failed to write NTP script to RTU!",
            event.getValue());
      }
    });
    
    FileTransferService.executeTask(task);
    return task;
  }

  /**
   * Reads the current NTP config from RTU and save to local.
   */
  public FileReadingTask readFromRTU() {
    return readFromRTU(localNTPFile);
  }

  /**
   * Reads the current NTP config from RTU and save to a file.
   */
  public FileReadingTask readFromRTU(File dest) {
    Preconditions.checkNotNull(dest, "dest must not be null");

    dest.mkdirs();
    dest.delete();// Delete current file
    setNtpScript(null);

    FileReadingTask task = FileTransferService.createFileReadTask(rtu.getComms().getFileTransfer(),CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC,
        NTP_CONF_FILE_PATH, dest,true);
    
    task.addTaskListener(new TaskListener.Adapter<File, Void>() {

      @Override
      public void succeeded(TaskEvent<File> event) {
        setNtpScript(readFile(event.getValue()));
        log.info("The NTP script has been read from RTU successfully."
            + "\nPlease restart the RTU to apply changes!");
      }

      @Override
      public void failed(TaskEvent<Throwable> event) {
        MessageDialogs.error(parent, 
            "Failed to read NTP script from RTU!",
            event.getValue());
      }
    });
    
    FileTransferService.executeTask(task);
    
    return task;
  }

  private void writeFile(File file, String content) {
    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(file));
      if (content != null)
        out.write(content);
      out.close();
      log.info("NTP file written successfully");
    } catch (IOException e) {
      log.error("Failed to write NTP file.", e);
    }
  }

  private String readFile(File file) {
    StringBuilder sb = new StringBuilder();
    if (file != null) {
      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(file));
        String line;
        int lineNum = 0;
        while ((line = br.readLine()) != null && lineNum < MAX_NTP_LINE_NUM) {
          sb.append(line);
          lineNum++;
        }
  
      } catch (IOException e) {
        log.error("Failed to read NTP file", e);
  
      } finally {
        if (br != null) {
          try {
            br.close();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
      log.info("NTP Script file has been read.");
    }
    
    return sb.toString();
  }
}

