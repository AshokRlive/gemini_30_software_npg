/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.datetime.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DateEditor;
import javax.swing.SpinnerDateModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.JXLabel;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;

/**
 * Dialog for changing RTU date time.
 */
public class RTUDateTimeSettingDialog {

  final String lblTimeFormat = "Current time: %s\r\n\r\n New date and time : ";

  private final RTUStatus rtuStatus;
  private final SpinnerDateModel dateModel;

  private TaskService ts;
  private Application app;
  private JXLabel lblTime;
  private JLabel lblTips;
  private JSpinner spinner;
  private JButton curTimeBtn;
  private JPanel contentPanel;
  private JButton btnHelp;


  public RTUDateTimeSettingDialog(Application app, RTUStatus rtuStatus) {
    this(app, null, rtuStatus);
  }

  public RTUDateTimeSettingDialog(Application app, TaskService ts, RTUStatus rtuStatus) {
    this.app = Preconditions.checkNotNull(app, "app is null");
    this.rtuStatus = Preconditions.checkNotNull(rtuStatus, "rtuStatus is null");
    this.ts = ts == null ? app.getContext().getTaskService() : ts;
    this.dateModel = createDateModel();
    
    createUIComponents();
  }

  private SpinnerDateModel createDateModel() {
    SpinnerDateModel model = new SpinnerDateModel(Calendar.getInstance().getTime(), 
        null, null, Calendar.DAY_OF_MONTH);
    
    // Workaround to set SpinnerModel to use UTC 
    Field f;
    try {
      f = model.getClass().getDeclaredField("value");
      f.setAccessible(true);
      Calendar value = (Calendar) f.get(model);
      value.setTimeZone(TimeZone.getTimeZone("UTC"));
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    
    return model;
  }

  private void createUIComponents() {
    spinner = new JSpinner(dateModel);
    
    // Set editor
    DateEditor editor = new JSpinner.DateEditor(spinner, "dd-MMM-yyyy HH:mm:ss z");
    editor.getFormat().setTimeZone(TimeZone.getTimeZone("UTC"));
    spinner.setEditor(editor);
    
    // Set current value
    Date date = rtuStatus.getRtuTime();
    if (date == null) {
      date = Calendar.getInstance().getTime();
    }
    dateModel.setValue(date);

    SpinnerWheelSupport.installMouseWheelSupport(spinner);
    spinner.setPreferredSize(new Dimension(120, 90));
    spinner.setFont(spinner.getFont().deriveFont(16f));

    lblTips = new JLabel("*Click a field and scroll up/down to adjust");
    lblTips.setForeground(Color.GRAY);

    String curDate = rtuStatus.getRTUTimeStr();
    lblTime = new JXLabel(String.format(lblTimeFormat, curDate));
    lblTime.setLineWrap(true);

    curTimeBtn = new JButton("Current Time");
    curTimeBtn.setToolTipText("Set the time to your current system time.");
    curTimeBtn.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        dateModel.setValue(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
      }
    });
    btnHelp = Helper.createHelpButton(RTUDateTimeSettingDialog.class);

    // Build content panel
    contentPanel = new JPanel(new BorderLayout(5, 5));

    contentPanel.add(lblTime, BorderLayout.NORTH);
    contentPanel.add(spinner, BorderLayout.CENTER);
    contentPanel.add(lblTips, BorderLayout.SOUTH);
    contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
  }

  public void showDialog(Window parent) {
    // Get the latest RTU date time.
    ts.execute(new TimeReadTask());

    // Show input dialog
    Object[] options = { btnHelp, curTimeBtn, "OK", "Cancel" };
    int rtn = JOptionPane.showOptionDialog(parent, contentPanel, "Change Date and Time",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.PLAIN_MESSAGE, null, options, options[2]);

    // Apply changes
    if (rtn == 2) {
      Date newDate = dateModel.getDate();
      ts.execute(new TimeWriteTask(app, newDate, rtuStatus));
    }
  }


  private class TimeReadTask extends Task<Void, Void> {

    private Logger log = Logger.getLogger(TimeReadTask.class);


    public TimeReadTask() {
      super(app);
      setTitle("Read RTU time");
      setDescription("Read date and time from remote RTU");
    }

    @Override
    protected Void doInBackground() throws Exception {
      rtuStatus.cmdGetDateTime();
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      String msg = "Fail to read RTU system time";
      setMessage(msg);
      log.error(msg + ", cause:" + cause.getMessage());
    }

    @Override
    protected void succeeded(Void result) {
      String curDate = rtuStatus.getRTUTimeStr();
      String msg = "Read RTU current date time:" + curDate;
      log.info(msg);
      setMessage(msg);

      // Update date&time label's content
      lblTime.setText(String.format(lblTimeFormat, curDate));
    }
  }

  private static class TimeWriteTask extends Task<Void, Void> {

    private Logger log = Logger.getLogger(TimeWriteTask.class);
    private final Date newDate;
    private final RTUStatus rtustatus;


    public TimeWriteTask(Application application, Date newDate, RTUStatus rtustatus) {
      super(application);
      this.newDate = newDate;
      this.rtustatus = rtustatus;
      setTitle("Change RTU time");
      setDescription("Set date and time:" + newDate);
    }

    @Override
    protected Void doInBackground() throws Exception {
      rtustatus.cmdSetDateTime(newDate);
      rtustatus.cmdGetDateTime();
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      String msg = "Failed to set RTU date and time. ";
      log.error(msg,cause);
      setMessage(msg + cause.getMessage());
      MessageDialogs.error(msg, cause);
    }

    @Override
    protected void succeeded(Void result) {
      String msg = "Set the RTU date and time to: " + RTUTimeDecoder.formatRTUTime(newDate);
      log.info(msg);
      setMessage(msg);
    }
  }

  // public static void main(String[] args) {
  // Logger.getLogger(TimeWriteTask.class).setLevel(Level.DEBUG);
  // Logger.getLogger(TimeReadTask.class).setLevel(Level.DEBUG);
  // Application app = Application.getInstance();
  // RTUStatus rtu = new RTUStatus(G3HttpClient.createDefault("10.11.11.102"),
  // new Session());
  // new RTUDateTimeSettingDialog(app, rtu).showDialog(null);
  // }
}
