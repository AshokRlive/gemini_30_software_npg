/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.controller.ntp;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.controller.ntp.NTPSettings;
import com.lucy.g3.configtool.rtu.controller.ntp.ui.NTPSettingsDialog;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.test.support.CommsTestSupport;


/**
 * The Class NTPSettingsTest.
 */
public class NTPSettingsTest {
  private static NTPSettings fixture;

  @BeforeClass
  public static void setUp() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    fixture = new NTPSettings();
    fixture.getRTU().setHost(CommsTestSupport.HOST);
    Logger.getRootLogger().setLevel(Level.INFO);
  }

  @Test
  public void testRead() throws InterruptedException, ExecutionException {
    fixture.readFromRTU().get();
  }
  
  @Test
  public void testWrite() throws IOException, InterruptedException, ExecutionException {
    File file = new File(new File("target"), "..//application//hat2.sh");
    fixture.writeToRTU(file).get();
  }
  
  
  public static void main(String[] args) throws Exception {
    Logger.getRootLogger().setLevel(Level.INFO);

    G3RTU rtu = new G3RTU("10.11.11.102");
    new NTPSettingsDialog(null, rtu).setVisible(true);
    Application.getInstance().exit();
  }
}

