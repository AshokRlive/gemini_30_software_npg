/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;
import java.text.MessageFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXLabel;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.cert.manager.gui.KeyStoreActions;
import com.lucy.g3.gui.common.widgets.ext.swing.action.BusyCursorAction;
import com.lucy.g3.security.support.SSLSupport;
public class SecurityOptionsDialog extends JDialog {
  public final static int OPTION_ADD_TO_EXCEPTION = 0;
  public final static int OPTION_IMPORT_CERTIFICATE = 1;
  public final static int OPTION_CANCELLED = 2;
  
  private boolean hasBeenConfirmed;
  
  private final String host;
  
  public SecurityOptionsDialog(Window owner, String host) {
    super(owner);
    this.host = host;
    initComponents();
    setMessage(host);
    setModal(true);
    
    //BusyCursorAction.apply(viewCertButton, this);
  }

  private void setMessage(String host) {
    labelMessage.setText(MessageFormat.format("Unable to verfiy the certificate of RTU:{0}\n", host));
  }

  public boolean hasBeenConfirmed() {
    return hasBeenConfirmed;
  }

  private void okButtonActionPerformed(ActionEvent e) {
    hasBeenConfirmed = true;
    closeDialog();
  }
  
  private void closeDialog() {
    dispose();
  }

  private void cancelButtonActionPerformed(ActionEvent e) {
    closeDialog();
  }

  private void viewCertButtonActionPerformed() {
    InetSocketAddress ia = new InetSocketAddress(host, SSLSupport.DEFAULT_PORT);
    KeyStoreActions.executeExamineCertSSLTask(Application.getInstance(), this, ia);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    labelMessage = new JXLabel();
    panel1 = new JPanel();
    radioButtonImport = new JRadioButton();
    radioButtonAddExcp = new JRadioButton();
    buttonBar = new JPanel();
    viewCertButton = new JButton();
    okButton = new JButton();
    cancelButton = new JButton();

    //======== this ========
    setTitle("Untrusted RTU");
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //---- labelMessage ----
        labelMessage.setIcon(UIManager.getIcon("OptionPane.warningIcon"));
        labelMessage.setText("message");
        labelMessage.setLineWrap(true);
        labelMessage.setIconTextGap(20);
        contentPanel.add(labelMessage, BorderLayout.NORTH);

        //======== panel1 ========
        {
          panel1.setLayout(new FormLayout(
            "50dlu, $lcgap, left:default",
            "2*(default, $lgap), 20dlu"));

          //---- radioButtonImport ----
          radioButtonImport.setText("Import CA certificate");
          radioButtonImport.setSelected(true);
          panel1.add(radioButtonImport, CC.xy(3, 1));

          //---- radioButtonAddExcp ----
          radioButtonAddExcp.setText("Add this RTU to the exception list and proceed with insecure connection (Unsafe)");
          panel1.add(radioButtonAddExcp, CC.xy(3, 3));
        }
        contentPanel.add(panel1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 0, 85, 80};
        ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 0.0};

        //---- viewCertButton ----
        viewCertButton.setText("View RTU Certificate...");
        viewCertButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            viewCertButtonActionPerformed();
          }
        });
        buttonBar.add(viewCertButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- okButton ----
        okButton.setText("Continue");
        okButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(okButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- cancelButton ----
        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            cancelButtonActionPerformed(e);
          }
        });
        buttonBar.add(cancelButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioButtonImport);
    buttonGroup1.add(radioButtonAddExcp);

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JXLabel labelMessage;
  private JPanel panel1;
  private JRadioButton radioButtonImport;
  private JRadioButton radioButtonAddExcp;
  private JPanel buttonBar;
  private JButton viewCertButton;
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  public static int confirmContinue(Window owner, String host) {
    SecurityOptionsDialog dlg = new SecurityOptionsDialog(owner, host);
    dlg.pack();
    dlg.setVisible(true);
    return dlg.getOption();
  }

  public int getOption() {
    if(hasBeenConfirmed() == false)
      return OPTION_CANCELLED;
    
    if(radioButtonImport.isSelected()) {
      return OPTION_IMPORT_CERTIFICATE;
    } else if(radioButtonAddExcp.isSelected()) {
      return OPTION_ADD_TO_EXCEPTION;
    }
    
    return OPTION_CANCELLED;
  }

}
