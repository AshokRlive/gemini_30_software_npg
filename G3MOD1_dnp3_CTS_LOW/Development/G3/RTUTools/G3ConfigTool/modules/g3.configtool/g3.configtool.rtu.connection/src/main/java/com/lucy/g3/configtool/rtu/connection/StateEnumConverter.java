
package com.lucy.g3.configtool.rtu.connection;

import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

class StateEnumConverter {

  public static CTH_RUNNINGAPP convertToEnum(ConnectionState state) {
    if (state == ConnectionState.CONNECTED_TO_G3_APP) {
      return CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP;
    } else if (state == ConnectionState.CONNECTED_TO_G3_UPDATER) {
      return CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER;
    } else {
      return null;
    } 
  }

  public static ConnectionState convertFrom(CTH_RUNNINGAPP runningApp) {
    if (runningApp == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      return ConnectionState.CONNECTED_TO_G3_APP;
    } else if (runningApp == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      return ConnectionState.CONNECTED_TO_G3_UPDATER;
    } else if (runningApp == null) {
      return ConnectionState.CONNECTION_LOST;
    } else {
      return ConnectionState.CONNECTED_TO_G3_UNKNOWN;
    }
  }
  
  public static String convertToText(CTH_RUNNINGAPP runningApp) {
    return ConnectionState.convertToText(convertFrom(runningApp));
  }
}

