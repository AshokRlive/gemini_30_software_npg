/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXLoginPane.Status;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.configtool.rtu.connection.ui.LoginWindow;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.RTULoginExtAPI;
import com.lucy.g3.rtu.comms.shared.LoginAccount;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * A task for setting up a connection to RTU. It will check the running mode of
 * RTU and the compatibility between RTU and ConfigTool. If not compatible, an
 * error dialog will be shown to user.
 */
final class ConnectionSetupTask extends Task<ConnectionSetupResult, Void> {

  private Logger log = Logger.getLogger(ConnectionSetupTask.class);

  private final JFrame parent;

  private LoginAccount account;
  private LoginResult loginResult;



  /**
   * Constructor a ConnectionSetupTask.
   */
  public ConnectionSetupTask(Application app, LoginAccount loginAccount) {
    super(app);
    this.account = loginAccount;

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    this.setUserCanCancel(true);
    this.setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));
  }

  @Override
  protected ConnectionSetupResult doInBackground() throws Exception {
    final RTULoginExtAPI logincmd = CommsUtil.getLoginCommand(G3RTUFactory.getComms());

    
    message("startMessage");
    Thread.sleep(300);// Delay for showing popping up blocker dialog.

    // Check RTU alive
    CTH_RUNNINGAPP runningMode = logincmd.cmdCheckAlive();

    // Get RTU API
    final G3ConfigAPI targetAPI = logincmd.cmdGetConfigAPI();

    // Get Config Exists
    boolean configExist = logincmd.cmdCheckConfigExist();

    SwingUtilities.invokeAndWait(new Runnable() {

      @Override
      public void run() {
        /* refs #3131 Do not check API to allow user to connect always cause UKPN engineers always use
         * the latest ConfigTool to connect to RTU to do software upgrade
         */
        
        // Check ConfigTool API against RTU API.
//      if (isCancelled() == false) {
//      if(!checkCanConnect(targetAPI))
//         cancel(true);
//    }

        // Login
        if (isCancelled() == false) {
          remoteLogin(logincmd);
        }
      }
    });

    if (isCancelled() == false) {
      final ConnectionState state = StateEnumConverter.convertFrom(runningMode);
      
      /* Read commissioning cache*/
      if(ConnectionState.isNormalMode(state))
        readCommissioningCacheFile(30);
      
      // refs #3131 Check if need upgrade 
      boolean needUpgrade = checkNeedUpgrade(targetAPI);

      return new ConnectionSetupResult(account, loginResult, state, configExist, needUpgrade);
    } else {
      return null;
    }
  }


  public static boolean checkNeedUpgrade(final G3ConfigAPI remote) {
    boolean needUpgrade = false;
    final G3ConfigAPI local = G3ConfigAPI.CURRENT_API;
    
    if (local.G3ProtocolMajor > remote.G3ProtocolMajor || 
        (local.G3ProtocolMajor == remote.G3ProtocolMajor && local.G3ProtocolMinor > remote.G3ProtocolMinor)) {
      needUpgrade  = true;
    }

    else if (local.SchemaVerMajor> remote.SchemaVerMajor || 
        (local.SchemaVerMajor == remote.SchemaVerMajor && local.SchemaVerMinor> remote.SchemaVerMinor)) {
      needUpgrade  = true;
    }

    else  if (local.ModuleProtocolMajor> remote.ModuleProtocolMajor || 
        (local.ModuleProtocolMajor == remote.ModuleProtocolMajor 
        && local.ModuleProtocolMinor> remote.ModuleProtocolMinor)) {
      needUpgrade  = true;
    }

    return needUpgrade;
  }
  
 /**
  * Check API compatibility, if they are incompatible, show a dialog to allow
  * user to cancel this task.
  */
 private boolean checkCompatbility(G3ConfigAPI target) {
     String err = G3ConfigAPI.checkCompatibility(G3ConfigAPI.CURRENT_API, target);
     if(err != null) {
       // Show error message
       ErrorInfo errInfo = new ErrorInfo("Error", 
           "You are using an incompatible version of Configuration Tool. Please download the correct version from the RTU.", err,
           null, null, Level.SEVERE, null);
       JXErrorPane.showDialog(parent, errInfo);
       return false;
     } 
     
     return true;
 }

  /**
   * Send user name & password to RTU and check the result. If RTU rejects,
   * shows user a login dialog to type new user name and password until RTU
   * accept the given name&password or user clicks cancel.
   */
  private void remoteLogin(RTULoginExtAPI logincmd) {
    while (true) {
      /* Show login dialog if necessary */
      if (account == null) {
        LoginWindow loginWindow = new LoginWindow();
        Status status = loginWindow.showDialog(false);

        if (status == Status.CANCELLED) {
          cancel(true);
          break;// Cancel setup process
        } else if (status == Status.SUCCEEDED) {
          account = loginWindow.getLoginAccount();
        }
      }

      /* Login to RTU */
      try {
        loginResult = logincmd.cmdLogin(account.getName(), account.getPassword().toCharArray());
        break;
      } catch (Exception e) {
        // Login failed, clear user account
        account = null;
      }
    }
  }

  private void readCommissioningCacheFile(int timeoutSecs) {
    setMessage("Reading commissioning cache...");
    try {
      String filename =G3Protocol.DEFAULT_COMMISSIONING_CACHE_FILE_NAME;
      FileReadingTask task = FileTransferService
          .createFileReadTask(
              CommsUtil.getFileTransfer(G3RTUFactory.getComms()),
              CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC,  
              filename, 
              new File(UIUtils.getLocalStorageDir(),filename), false);
      task.setShowConfirmOverriteDialog(false);
      getTaskService().execute(task);
      task.get(timeoutSecs, TimeUnit.SECONDS);
      
    } catch (InterruptedException | ExecutionException | TimeoutException e) {
      log.warn("No commissioning cache file found.");
    }

  }
  
  @Override
  protected void failed(final Throwable cause) {
    message("failMessage");
    log.error("connection failed", cause);

  }

  @Override
  protected void cancelled() {
    message("cancelMessage");
  }

  @Override
  protected void succeeded(ConnectionSetupResult result) {
    message("successMessage");
  }

}
