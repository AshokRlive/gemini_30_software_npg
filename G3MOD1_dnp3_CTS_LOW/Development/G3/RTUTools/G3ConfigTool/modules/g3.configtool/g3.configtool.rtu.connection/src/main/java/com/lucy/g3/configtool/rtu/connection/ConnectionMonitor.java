/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.RTUCommsService;
import com.lucy.g3.rtu.comms.service.RTULoginExtAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * ConnectionManager contains a thread which monitors the communication status
 * by sending heart beat message to the connected RTU.
 * <p>
 * If an error is detected, change the application state and save error
 * information into {@code errorInfo}.
 * </p>
 */
class ConnectionMonitor implements Runnable {

  private Logger log = Logger.getLogger(ConnectionMonitor.class);

  private volatile Thread monitorThread;

  private int delay = 3000; // heart beat period (ms)

  private IConnectionMonitorHandler monitorHandler;

  private int errCounter;



  public IConnectionMonitorHandler getMonitorHandler() {
    return monitorHandler;
  }

  public void setMonitorHandler(IConnectionMonitorHandler monitorHandler) {
    this.monitorHandler = monitorHandler;
  }

  public int getDelay() {
    return delay;
  }

  /**
   * Sets the delay of sending heart-beat.
   *
   * @param delay
   *          milliseconds delay.
   */
  public void setDelay(int delay) {
    this.delay = delay;
  }

  public void start() {
    if (monitorThread == null || !monitorThread.isAlive()) {
      monitorThread = new Thread(this, "Connection Monitor");
      monitorThread.setDaemon(true);
      monitorThread.start();
      log.info("Connection monitor started");
    }
  }

  public void stop() {
    if (monitorThread != null) {
      monitorThread = null;
      log.info("Connection monitor terminated");
    }
  }

  /* Check if this monitor is running */
  private boolean isRunning() {
    return (monitorThread == Thread.currentThread());
  }

  @Override
  public void run() {
    RTUCommsService comms = G3RTUFactory.getComms();
    //final RTULoginExtAPI logincmd = CommsUtil.getLoginCommand();
    
    log.debug("Enter connection monitor thread");

    Thread thisThread = Thread.currentThread();

    while (monitorThread == thisThread) {

      final ConnectionState curState = monitorHandler == null ? null : monitorHandler.getCurrentState();
      try {
        final CTH_RUNNINGAPP runningMode = comms.cmdCheckAlive();
        
        /* Workaround to poll Off/Local/Remote state*/
        if(CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP == runningMode) {
          try {
            comms.getRtuControl().cmdGetOLR();
            comms.getRtuStatus().cmdGetServiceStatus();
          } catch (Throwable e) {
            // Do nothing
          }
        }
        
        log.debug("Connection OK: " + runningMode);

        ConnectionState newState = StateEnumConverter.convertFrom(runningMode);
        /*
         * Current connection state doesn't match with RTU reply state.
         */
        if (ConnectionState.isConnected(curState)
            && curState != newState
            && isRunning()) {
          log.error("Wrong connection state: " + curState + ". Actual RTU return state: " + runningMode);

          if (monitorHandler != null) {
            monitorHandler.handleConnectionStateChanged(curState, newState);
          }
        }

        /* State changed from LOST to CONNECTED */
        if (curState == ConnectionState.CONNECTION_LOST
            && isRunning()) {
          log.warn("Connection recovered!");

          if (monitorHandler != null) {
            monitorHandler.handleConnectionRecovered(newState);
          }
        }

        errCounter = 0;

      } catch (final Exception e) {
        // Only logging limited errors.
        if (errCounter++ < 1) {
          log.error(e.getMessage());
        }

        /* State changed from CONNECTED to LOST */
        if (ConnectionState.isConnected(curState)) {
          if (isRunning()) {
            if (monitorHandler != null) {
              monitorHandler.handleConnectionLost(createError(e));
            }

            log.warn("Connection Lost!");
          }
        }
      }

      // Delay
      if (isRunning()) {
        try {
          Thread.sleep(delay);
        } catch (InterruptedException e) {
          // Nothing to do
        }
      }
    }

    log.debug("Exit connection mointior thread");
  }

  private ErrorInfo createError(final Exception e) {
    return new ErrorInfo(
        "Connection Error", // title
        "Connection is lost!", // basic message
        e.getMessage(), // detail message
        "ConnectError", e,
        java.util.logging.Level.SEVERE, null);
  }

}
