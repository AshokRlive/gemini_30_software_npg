/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.LocalStorage;
import org.jdesktop.swingx.JXLoginPane;
import org.jdesktop.swingx.JXLoginPane.JXLoginDialog;
import org.jdesktop.swingx.JXLoginPane.SaveMode;
import org.jdesktop.swingx.JXLoginPane.Status;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.auth.PasswordStore;
import org.jdesktop.swingx.auth.UserNameStore;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreference;
import com.lucy.g3.configtool.rtu.connection.ui.G3RTULoginService.ILoginServiceCallback;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.shared.LoginAccount;

/**
 * <p>
 * Login window allows user to login to RTU by giving a user name and password.
 * </p>
 * The features include:
 * <li>Automatically load/store server list from/into user option object.
 * <li>User name/password can be remembered and stored into standard Java
 * preference object.
 * <li>"offline" button can be shown optionally.
 */
public class LoginWindow {

  private static Logger log = Logger.getLogger(LoginWindow.class);

  private JButton btnOffline;

  private JXLoginDialog dialog;

  private boolean offlineMode = false;

  private final ConnectionPreference connPref;
  private final G3RTULoginService loginService;


  public LoginWindow() {
    connPref = (ConnectionPreference) PreferenceManager.INSTANCE.getPreference(ConnectionPreference.ID);
    SecurityPreference secPref = (SecurityPreference) PreferenceManager.INSTANCE.getPreference(SecurityPreference.ID);
    loginService = new G3RTULoginService(new LoginServiceCallback(), connPref, secPref);
    btnOffline = new JButton(new OfflineAction());
    initComponents();
  }
  
  public void setServerFieldEnabled(boolean enabled) {
    String serverLabel = (String)UIManager.get("JXLoginPane.serverString");
    List<Component> all = getAllComponents(loginPane);
    for (Component comp : all) {
      if(comp instanceof JLabel && ((JLabel)comp).getText().equals(serverLabel)){
        JLabel labelComp = (JLabel) comp;
        labelComp.getLabelFor().setEnabled(enabled);
      }
    }
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }

  public void close() {
    if (dialog != null) {
      dialog.dispose();
    }
  }

  public Status showDialog(boolean showOfflineButton) {
    loginPane.setOpaque(false);
    JFrame parent = WindowUtils.getMainFrame();
    dialog = new JXLoginDialog(parent, loginPane);
    dialog.setLocationRelativeTo(parent);
    dialog.setAlwaysOnTop(true);
    dialog.setPreferredSize(new Dimension(300,350));

    if (showOfflineButton) {
      // [Patch] Add button "Offline Mode" to login panel.
      try {
        BorderLayout bl = (BorderLayout) dialog.getContentPane().getLayout();
        JXPanel controlPanel = (JXPanel) bl.getLayoutComponent(BorderLayout.SOUTH);
        JXPanel btnPanel = (JXPanel) controlPanel.getComponent(1);
        GridLayout layout = new GridLayout(1, 3);
        layout.setHgap(5);
        btnPanel.setLayout(layout);
        btnPanel.add(btnOffline, 0);
      } catch (Exception e) {
        log.error("Offline Mode is unavailable!" + e.getMessage());
      }
    }

    // Change library Default 'server' selection to be called 'Gemini 3 RTU'.
    UIManager.put("JXLoginPane.serverString", "Gemini 3 RTU");
    dialog.setVisible(true);
    return offlineMode ? null : dialog.getStatus();
  }

  public LoginAccount getLoginAccount() {
    return loginService.getLoginAccount();
  }

  /**
   * <p>
   * Prepare a list of server address for login purpose.
   * </p>
   * <em>Note that the amount of server items should be at least two, otherwise
   * the server comboBox component would not be added to login pane. This is a
   * limitation of JXLogin component in the library.</em>
   */
  private List<String> prepareServerList() {
    ArrayList<String> list;

    // Prepare server
    if(connPref != null)
      list = connPref.getServerList();
    else
      list = new ArrayList<>(); 

    /*
     * Note: In JXLoginPane, if the number of server items is less than 2, the
     * server component is initialised as JTextField rather than JCombobox we
     * expect, so we hack server list to make it at least two items are
     * contained.
     */
    if (list.size() == 0) {
      list.add("");
      list.add("192.168.0.1");
    } else if (list.size() == 1) {
      list.add("");
    }

    return list;
  }

  private void createUIComponents() {
    // Do not store password
    PasswordStore pwdStore = null;// new DefaultPasswordStore();

    // Read user name from storage.
    XMLUserNameStore userStore = new XMLUserNameStore();
    userStore.loadUserNames();

    // Change library Default 'server' selection to be called 'Gemini 3 RTU'.
    UIManager.put("JXLoginPane.serverString", "RTU Address");
    loginPane = new JXLoginPane(loginService, pwdStore, userStore, prepareServerList());
//    loginPane.setScrollableHeightHint(ScrollableSizeHint.PREFERRED_STRETCH);
//    loginPane.setScrollableWidthHint(ScrollableSizeHint.PREFERRED_STRETCH);

    /*
     * Bug fix: we shouldn't enable any save mode, because the auto-complete of
     * user name is not case sensitive.
     */
    loginPane.setSaveMode(SaveMode.NONE);

    /* Initialise input user name */
    String defaultUserName = G3RTULoginService.getLastLoginUserName();
    if (defaultUserName == null) {
      String[] storeNames = userStore.getUserNames();
      if (storeNames != null && storeNames.length > 0) {
        defaultUserName = storeNames[0];
      }
    }
    loginPane.setUserName(defaultUserName);

    /* Initialise server component */
    String defaultServer = connPref == null ? "" : connPref.getServer();
    initServerCombo(loginPane, defaultServer);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();


    //---- loginPane ----
    loginPane.setBanner(new ImageIcon(getClass().getResource("/images/title.png")).getImage());
    loginPane.setBackground(Color.white);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    // ====== Custom Code ======

    // Change banner background
    Component banner = loginPane.getComponent(0);
    banner.setBackground(Color.white);
  }

  /*
   * [Patch]Since the server list comboBox component is not editable by default,
   * we need to find and set it to editable, and also set the default selected
   * item.
   */
  private void initServerCombo(Component pane, String defHost) {
    if (pane.getClass() == JComboBox.class) {
      JComboBox<?> comb = ((JComboBox<?>) pane);
      comb.setEditable(true);
      comb.setVisible(true);
      comb.setSelectedItem(defHost);
    }

    if (pane instanceof Container) {
      Component[] comps = ((Container) pane).getComponents();
      for (int i = 0; i < comps.length; i++) {
        if (comps[i] instanceof Container) {
          initServerCombo(comps[i], defHost);
        }
      }
    }
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JXLoginPane loginPane;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  /**
   * Implementation of {@code UserNameStore}. Store login user names to a file
   * in application local storage directory.
   * <p>
   * Note this class must be public cause it is a JavaBean.
   * </p>
   */
  public static class XMLUserNameStore extends UserNameStore {

    private final String storageFileName = "login.xml";

    private final ArrayList<String> namesList = new ArrayList<String>();


    private LocalStorage getLocalStorage() {
      LocalStorage localstore = Application.getInstance().getContext().getLocalStorage();
      return localstore;
    }

    @Override
    public String[] getUserNames() {
      return namesList.toArray(new String[namesList.size()]);
    }

    @Override
    public void setUserNames(String[] names) {
      this.namesList.clear();
      if (names != null) {
        this.namesList.addAll(Arrays.asList(names));
      }
    }

    @Override
    public void loadUserNames() {
      try {
        XMLUserNameStore obj = (XMLUserNameStore) getLocalStorage().load(storageFileName);
        if (obj != null) {
          setUserNames(obj.getUserNames());
        }
      } catch (Exception e) {
        log.info("Login user names not found!");
      }

    }

    @Override
    public void saveUserNames() {
      try {
        getLocalStorage().save(this, storageFileName);
      } catch (IOException e) {
        log.warn(e);
      }
    }

    @Override
    public boolean containsUserName(String name) {
      return name != null && namesList.contains(name);
    }

    @Override
    public void addUserName(String userName) {
      if (!Strings.isBlank(userName) && !containsUserName(userName)) {
        namesList.add(userName);
      }
    }

    @Override
    public void removeUserName(String userName) {
      namesList.add(userName);
    }

  }

  // /**
  // * Use java Preference for storing password.
  // *
  // *
  // */
  // public static class DefaultPasswordStore extends PasswordStore{
  // //Preference for saving user names and passwords
  // final private Preferences prefs =
  // Preferences.userNodeForPackage(LoginWindow.class);
  //
  // @Override
  // public boolean set(String username, String server, char[] password) {
  // if(username == null || password == null)
  // return false;
  //
  // prefs.put(username, String.valueOf(password));
  // return true;
  // }
  //
  // @Override
  // public char[] get(String username, String server) {
  // char[] pass = null;
  //
  // if(username != null){
  // String md5 = prefs.get(username,null);
  // if(md5 != null){
  // pass = md5.toCharArray();
  // }
  // }
  //
  // return pass;
  // }
  //
  // @Override
  // public void removeUserPassword(String username) {
  // if(username != null)
  // prefs.remove(username);
  // }
  // }
  private class LoginServiceCallback implements ILoginServiceCallback {

    @Override
    public void setErrorMessage(String errorMessage) {
      loginPane.setErrorMessage(errorMessage);
    }

    @Override
    public Window getOwner() {
      return dialog;
    }
  }

  private class OfflineAction extends AbstractAction {

    public OfflineAction() {
      putValue(NAME, "Offline");
      putValue(SHORT_DESCRIPTION, "Launch configuration tool in Offline Mode");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      offlineMode = true;
      if (dialog != null) {
        dialog.dispose();
      }
    }
  }

}
