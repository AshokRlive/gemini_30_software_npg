/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.rtu.comms.shared.LoginAccount;
import com.lucy.g3.rtu.comms.shared.LoginResult;

/**
 * LoginSession object stores all session information which includes the current
 * logged in user, the login result and connection state.
 */
public class ConnectionSetupResult {

  private final LoginAccount account;
  private final LoginResult result;
  private final ConnectionState state;
  private final boolean hasConfig;
  private final boolean needUpgrade;


  public ConnectionSetupResult(LoginAccount account, LoginResult result,
      ConnectionState connectionState, boolean hasConfig, boolean needUpgrade) {
    super();
    this.account = account;
    this.hasConfig = hasConfig;
    this.needUpgrade = needUpgrade;
    this.state = connectionState;
    this.result = Preconditions.checkNotNull(result, "loginResult is null");
  }

  public LoginAccount getAccount() {
    return account;
  }

  
  public boolean isNeedUpgrade() {
    return needUpgrade;
  }

  public LoginResult getResult() {
    return result;
  }

  public ConnectionState getState() {
    return state;
  }

  public boolean isHasConfig() {
    return hasConfig;
  }

}
