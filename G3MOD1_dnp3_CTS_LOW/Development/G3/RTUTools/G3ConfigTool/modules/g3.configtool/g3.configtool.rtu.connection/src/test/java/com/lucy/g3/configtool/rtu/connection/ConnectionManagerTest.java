/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection;

import static org.junit.Assert.assertNotNull;

import java.io.File;

import org.apache.log4j.chainsaw.Main;
import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.rtu.connection.IConnectionManagerCallbacks;
import com.lucy.g3.configtool.rtu.connection.ui.LoginWindow;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.test.support.utilities.TestUtil;

/**
 * The Class ConnectionManagerTest.
 */
public class ConnectionManagerTest implements IConnectionManagerCallbacks {

  private ConnectionManager fixture;

  private static String[] ACTION_KEYS = {
      ConnectionManager.ACTION_KEY_CONNECT,
      ConnectionManager.ACTION_KEY_DISCONNECT, };

  private static String[] READ_ONLY_PROPERTIES = {
      ConnectionManager.PROPERTY_CONNECTION_STATE,
      ConnectionManager.PROPERTY_CONNECT_ENABLED,
      ConnectionManager.PROPERTY_DISCONNECT_ENABLED,
      ConnectionManager.PROPERTY_LOGIN_ACCOUNT,
      ConnectionManager.PROPERTY_LOGIN_INFO, };


  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    Application app = Application.getInstance();
    fixture = new ConnectionManager(null, ConnectionState.DISCONNECTED, this);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAction() {
    for (int i = 0; i < ACTION_KEYS.length; i++) {
      assertNotNull(fixture.getAction(ACTION_KEYS[i]));
    }
  }

  @Test
  public void testReadOnlyProperties() {
    TestUtil.testReadOnlyProperties(fixture, READ_ONLY_PROPERTIES);
  }

  @Override
  public void readConfig() {
  }

  @Override
  public void readRTUInfo() {
  }

  
  public static void main(String[] args) {
    LoginWindow login  = new LoginWindow();
    login.setServerFieldEnabled(false);
    login.showDialog(true);
  }
}
