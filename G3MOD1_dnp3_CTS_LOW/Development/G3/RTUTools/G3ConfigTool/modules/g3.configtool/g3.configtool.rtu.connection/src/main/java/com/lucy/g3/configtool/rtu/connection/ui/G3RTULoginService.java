package com.lucy.g3.configtool.rtu.connection.ui;

import java.awt.Window;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.auth.LoginService;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreferenceApplier;
import com.lucy.g3.configtool.preferences.ui.PreferenceDialog;
import com.lucy.g3.rtu.comms.datalink.ISecurityManager;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.RTULoginExtAPI;
import com.lucy.g3.rtu.comms.shared.LoginAccount;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.system.config.SystemProperties;

class G3RTULoginService extends LoginService {
  private Logger log = Logger.getLogger(G3RTULoginService.class);
  
  private final ConnectionPreference connPref;
  private final SecurityPreference secPref;
  
  private String inputUserName; // Name for login
  private String inputPassword; // Pass for login
  
  private static String lastInputUserName;
  
  private final ILoginServiceCallback callback;
  
   public G3RTULoginService(ILoginServiceCallback callback, ConnectionPreference connPref, SecurityPreference secPref) {
      this.connPref = connPref;
      this.secPref = secPref;
      setSynchronous(false); // authentication procedure will be running in another thread.
      this.callback = Preconditions.checkNotNull(callback, "callback must not be null");
    }

    @Override
    public boolean authenticate(String name, char[] password, String server) {
      setServer(server);
      RTULoginExtAPI logincmd = CommsUtil.getLoginCommand(G3RTUFactory.getComms());
      
      connPref.setServer(server);
      inputUserName = name;
      inputPassword = new String(password);
      lastInputUserName = inputUserName;
      ISecurityManager security = logincmd.getLinkLayer().getSecurityManager();
      
      if(security != null) {
        security.setIgnoreCertificateError(secPref.exceptions().isException(server)); 
      }

      // Ping
      try{
        log.info("Login to host: " + getServer());
        logincmd.cmdCheckAlive();
      } catch(javax.net.ssl.SSLException e1) {
        log.warn("Certificate invalid!", e1);
        callback.setErrorMessage("Certificate invalid!");
        
        if(security != null) {
          if(handleSSLException(server, security) == false)
            return false;
        } 
        
      } catch (Exception|Error e) {
        log.error("Login failed", e);
        callback.setErrorMessage(extractErrorMsg(e));
      }
      
      // Authentication
      try {
        LoginResult result = logincmd.cmdLogin(name, password);
        if (result == null) {
          log.error("Login failed. User: " + name);
          return false;
        } else {
          log.info("Login succeeded.  User Level:  " + result.level);
          return true;
        }

      } catch(javax.net.ssl.SSLException e1) {
        log.error("Login failed", e1);
        callback.setErrorMessage("Certificate invalid!");
        return false;
        
      } catch (Throwable e) {
        log.error("Login failed", e);
        callback.setErrorMessage(extractErrorMsg(e));
        return false;
      } 
    }
    
    public LoginAccount getLoginAccount() {
      return new LoginAccount(inputUserName, inputPassword);
    }
    

    /**
     * Gives user options to handle certificate exception
     * @param server
     * @param security
     * @return true user choose to continue.
     */
    private boolean handleSSLException(String server, ISecurityManager security) {
      int opt;
      if(SystemProperties.getter.getBoolean(SystemProperties.PROPERTY_SUPPRESS_SSL_WARNING) == true) {
        // Add host to exception list and hide security dialog
        opt = SecurityOptionsDialog.OPTION_ADD_TO_EXCEPTION;
        
      } else {
        // Show security dialog
        opt = SecurityOptionsDialog.confirmContinue(callback.getOwner(), server);
      }
      
      if(opt == SecurityOptionsDialog.OPTION_ADD_TO_EXCEPTION) {
          secPref.exceptions().addException(server);
          security.setIgnoreCertificateError(true);
          return true;
          
      } else if(opt == SecurityOptionsDialog.OPTION_IMPORT_CERTIFICATE) {
        PreferenceDialog dlg = new PreferenceDialog(callback.getOwner(), PreferenceManager.INSTANCE);
        dlg.selectTab(secPref.getId()); // Select security tab.
        dlg.setVisible(true);
        if(dlg.hasBeenCanceled()) {
          return false;
        }
        
        SecurityPreferenceApplier.apply(security, secPref);
        return true;
        
      } else {
        // Unknown option
        return false;
      }
    }
    
    private String extractErrorMsg(Throwable e) {
      String msg = e.getMessage();
      
      // Trim long message
      if (msg != null && !msg.isEmpty()) {
        if (msg.length() > 150) {
          msg = msg.substring(0, 150);
        }
      }
      
      if (Strings.isBlank(msg))
        msg = e.getClass().getSimpleName();
      
      return msg;
    }
    
    public static String getLastLoginUserName(){
      return lastInputUserName;
    }
    
    static public interface ILoginServiceCallback {
      void setErrorMessage(String errorMessage);
      Window getOwner();
    }
    
}