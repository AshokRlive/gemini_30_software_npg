/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXLoginPane.Status;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.rtu.connection.ui.LoginWindow;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.shared.LoginAccount;
import com.lucy.g3.rtu.comms.shared.LoginResult;

/**
 * A subsystem for managing the connection to RTU.
 */
public final class ConnectionManager extends AbstractSubsystem {
  public final static String SUBSYSTEM_ID = "subystem.id.ConnectionManager";
  
  public static final String ACTION_KEY_CONNECT = "connect";

  public static final String ACTION_KEY_DISCONNECT = "disconnect";
  
  public static final String ACTION_KEY_SWITCH_USER = "switchUser";

  public static final String PROPERTY_CONNECT_ENABLED = "connectEnabled";

  public static final String PROPERTY_DISCONNECT_ENABLED = "disconnectEnabled";
  
  public static final String PROPERTY_LOGIN_ACCOUNT = "loginAccount";
  
  public static final String PROPERTY_LOGIN_INFO = "loginInfo";


  private Logger log = Logger.getLogger(ConnectionManager.class);

  private LoginAccount lastLoginAccount;// The last login account
  private LoginAccount loginAccount;// The current login account
  private LoginResult loginInfo;    // The current login state
  
  /* A component for monitoring the connection state */
  private ConnectionMonitor monitor;
  
  private final IConnectionManagerCallbacks callbacks;
  
  private ArrayList<TaskListener<ConnectionSetupResult, Void>> connectHandlersRegistry = new ArrayList<>();

  // Constructor
  public ConnectionManager(IContext context,ConnectionState initialState, IConnectionManagerCallbacks callbacks) {
    super(context, SUBSYSTEM_ID);
    this.callbacks = Preconditions.checkNotNull(callbacks , "callbacks must not be null");
  }
  
  public void registerConnectionHandler(TaskListener<ConnectionSetupResult, Void> handler){
    if(handler != null)
      connectHandlersRegistry.add(handler);
  }
  
  public void deregisterConnectionHandler(TaskListener<ConnectionSetupResult, Void> handler){
    connectHandlersRegistry.remove(handler);
  }
  
  /**
   * Shows user a login dialog for setting up connection to RTU.
   */
  @Action(enabledProperty = PROPERTY_CONNECT_ENABLED)
  public void connect() {
    LoginWindow login = new LoginWindow();
    Status loginResult = login.showDialog(false);

    /* Login success, now set up a connection */
    if (loginResult == Status.SUCCEEDED) {
      setupConnection(login.getLoginAccount(), true);
    }
  }

  /**
   * Disconnects from RTU and clear the login information.
   * <p>
   * <strong>Note: </strong> the real-time information and loaded RTU
   * configuration might be cleared as well.
   * </p>
   */
  @Action(enabledProperty = PROPERTY_DISCONNECT_ENABLED)
  public void disconnect() {
    changeConnectionState(ConnectionState.DISCONNECTED);
  }
  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE) 
  public void switchUser() {
    LoginWindow login = new LoginWindow();
    login.setServerFieldEnabled(false);
    Status loginResult = login.showDialog(false);

    /* Login success, now set up a connection */
    if (loginResult == Status.SUCCEEDED) {
      setupConnection(login.getLoginAccount(), false, null);
    }
  }

  private void changeConnectionState(ConnectionState newState) {
    Preconditions.checkNotNull(newState, "Cannot set newState to null");
    
    /* User account must be cleared when disconnect*/
    if(newState == ConnectionState.DISCONNECTED
        || newState == ConnectionState.CONNECTION_LOST) {
      setLoginAccount(null);
      setLoginInfo(null);
    }
    
    /* Notify the world to change state*/
    ConnectionState oldState = getConnectionState();
    publishEvent(oldState, newState);

    /* Start / Stop monitor */
    if (ConnectionState.isConnected(newState)) {
      getMonitor().start();
    } else if (ConnectionState.isDisconnected(newState)) {
      getMonitor().stop();
    }
  }
  
  /**
   * Setup a connection to RTU.
   * <P>
   * Thread safe method.
   * </P>
   *
   * @param user
   *          user account for authentication, if null the last user account
   *          will be used.
   * @param readConfig
   *          read configuration after the connection is set up.
   */
  public void setupConnection(LoginAccount user, boolean readConfig, TaskListener<ConnectionSetupResult, Void>... handlers) {
    if (user == null) {
      user = lastLoginAccount; // Use last login account
    }

    if (user != null) {
      // It is important to stop connection monitor before setup a connection
      getMonitor().stop();

      ConnectionSetupTask task = new ConnectionSetupTask(getApp(), user);
      
      // Add default handler
      task.addTaskListener(new ConnectionSetupHandler(readConfig));
      
      // Add registered handlers
      for (TaskListener<ConnectionSetupResult, Void> hdl : connectHandlersRegistry) {
        task.addTaskListener(hdl);
      }
      
      // Add extra handler
      if(handlers != null) {
        for (int i = 0; i < handlers.length; i++) {
          task.addTaskListener(handlers[i]);
        }
      }
      
      // Run task
      getCommsTaskService().execute(task);

    } else {
      connect();
    }
  }


  /**
   * Bean getter method for property {@link #PROPERTY_CONNECT_ENABLED}.
   */
  public boolean isConnectEnabled() {
    return isConnectEnabled(getConnectionState());
  }

  private static boolean isConnectEnabled(ConnectionState state) {
    return ConnectionState.DISCONNECTED == state;
  }

  /**
   * Bean getter method for property {@link #PROPERTY_DISCONNECT_ENABLED}.
   */
  public boolean isDisconnectEnabled() {
    return isDisconnectEnabled(getConnectionState());
  }

  private static boolean isDisconnectEnabled(ConnectionState state) {
    return ConnectionState.DISCONNECTED != state;
  }

  private ConnectionMonitor getMonitor() {
    if (monitor == null) {
      monitor = new ConnectionMonitor();
      monitor.setMonitorHandler(new ConnectionMonitorHandler());
    }
    return monitor;
  }


  /**
   * Handle the result of {@linkplain ConnectionSetupTask} to update all
   * subsystem connection state , store the connection information (such as user
   * account).
   */
  private class ConnectionSetupHandler extends TaskListener.Adapter<ConnectionSetupResult, Void> {

    private final boolean readConfig;


    public ConnectionSetupHandler(boolean readConfig) {
      this.readConfig = readConfig;
    }

    
    @Override
    public void succeeded(TaskEvent<ConnectionSetupResult> event) {
      ConnectionSetupResult result = event.getValue();
      log.info("Connection setup succeeded: " + result.getState());

      assert result.getState() != ConnectionState.DISCONNECTED;

      // Update all subsystems state
      lastLoginAccount = result.getAccount();
      setLoginAccount(result.getAccount());
      setLoginInfo(result.getResult());
      changeConnectionState(result.getState());
      
      /* Read RTU information */
      if (ConnectionState.isConnected(result.getState())) {
        callbacks.readRTUInfo();
      }

      
      if(readConfig) {
        /* Read configuration */
        if (ConnectionState.isNormalMode(result.getState()) && result.isNeedUpgrade() == false) {
          if (result.isHasConfig()) {
            callbacks.readConfig();
          } else {
            JOptionPane.showMessageDialog(WindowUtils.getMainFrame(),
                "No RTU configuration present!", "Alert", JOptionPane.WARNING_MESSAGE);
          }
        }
      }
    }

    @Override
    public void failed(TaskEvent<Throwable> event) {
      Throwable cause = event.getValue();
      log.info("Connection setup failed! Cause: " + cause.getMessage());

      disconnect();

      // Create an error
      String reason = cause == null ? "Unknown reason" : cause.getMessage();
      StringBuilder content = new StringBuilder();
      content.append("<html><p><b>Cannot connect to RTU device!</b></p><br>");
      if (reason != null && !reason.isEmpty()) {
        content.append("Reason: ");
        content.append(reason);
      }
      content.append("</html>");
      ErrorInfo error = new ErrorInfo("Connection Failed", content.toString(), null,
          null, null, java.util.logging.Level.SEVERE, null);

      // Show error dialog
      JXErrorPane.showDialog(WindowUtils.getMainFrame(), error);
    }

    @Override
    public void cancelled(TaskEvent<Void> event) {
      disconnect();
    }
  }
  
  /**
   * Handle the events from <code>ConnectionMonitor</code> and change the state
   * of this <code>ConnectionManager</code>.
   */
  private class ConnectionMonitorHandler implements IConnectionMonitorHandler {

    @Override
    public ConnectionState getCurrentState() {
      return ConnectionManager.this.getConnectionState();
    }

    @Override
    public void handleConnectionLost(final ErrorInfo reason) {
      changeConnectionState(ConnectionState.CONNECTION_LOST);
    }

    @Override
    public void handleConnectionRecovered(ConnectionState newState) {
      // Set up connection without reading configuration
      setupConnection(null, false);
    }

    @Override
    public void handleConnectionStateChanged(ConnectionState old,
        ConnectionState newState) {

      // Setup connection and read configuration if necessary
      setupConnection(null, ConnectionState.isNormalMode(newState));
    }
  }


  private void publishEvent(ConnectionState oldState, ConnectionState newState) {
    
    HashMap<String, Object> properties = new HashMap<>();
    properties.put(EventTopics.EVT_PROPERTY_OLD_STATE,oldState);
    properties.put(EventTopics.EVT_PROPERTY_NEW_STATE,newState);
    EventBroker.getInstance().send(EventTopics.EVT_TOPIC_CONN_STATE_CHG, properties);
    
    properties = new HashMap<>();
    properties.put(EventTopics.EVT_PROPERTY_LOGIN_ACCOUNT,loginAccount);
    properties.put(EventTopics.EVT_PROPERTY_LOGIN_RESULT,loginInfo);
    EventBroker.getInstance().send(EventTopics.EVT_TOPIC_LOGIN_ACCOUNT_CHG, properties);
    
    firePropertyChange(PROPERTY_CONNECT_ENABLED, !isConnectEnabled(newState),  isConnectEnabled(newState));
    firePropertyChange(PROPERTY_DISCONNECT_ENABLED, !isDisconnectEnabled(newState), isDisconnectEnabled(newState));
  }


  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    
  }
  
  public LoginAccount getLoginAccount() {
    return loginAccount;
  }

  private void setLoginAccount(LoginAccount newValue) {
    Object oldValue = getLoginAccount();
    this.loginAccount = newValue;
    firePropertyChange(PROPERTY_LOGIN_ACCOUNT, oldValue, newValue);
  }

  public LoginResult getLoginInfo() {
    return loginInfo;
  }

  private void setLoginInfo(LoginResult newValue) {
    Object oldValue = getLoginInfo();
    this.loginInfo = newValue;
    firePropertyChange(PROPERTY_LOGIN_INFO, oldValue, newValue);
  }
  
}
