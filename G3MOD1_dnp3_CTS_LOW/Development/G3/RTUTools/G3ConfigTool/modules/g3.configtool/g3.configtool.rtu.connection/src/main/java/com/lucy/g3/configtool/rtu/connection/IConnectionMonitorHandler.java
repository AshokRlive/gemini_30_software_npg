/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.connection;

import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.configtool.subsystem.ConnectionState;

/**
 * The Interface IConnectionMonitorHandler.
 */
interface IConnectionMonitorHandler {

  ConnectionState getCurrentState();

  void handleConnectionLost(ErrorInfo reason);

  void handleConnectionRecovered(ConnectionState newState);

  void handleConnectionStateChanged(ConnectionState old, ConnectionState newState);
}
