/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Assert;

/**
 *
 */
public class PreferenceManagerTest {

  private static final File DIR = new File("target/DummyPrefDir/");

  private PreferenceManager fixture;


  @BeforeClass
  public static void initBeforeClass() {
    DIR.mkdirs();
  }

  @AfterClass
  public static void cleanAfterClass() {
    try {
      FileUtils.deleteDirectory(DIR);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Before
  public void init() {
    fixture = new PreferenceManager();
  }

  @Test
  public void testInit() {
    fixture.init(DIR, new String[]{DummyPref.ID}, new Class<?>[]{DummyPref.class});
    assertNotNull(fixture.getPreference(DummyPref.ID));
  }

  @Test
  public void testSaveRead() throws Exception {
    File file = new File(DIR, "testSave");
    DummyPref dummy = new DummyPref();
    dummy.property = "PropertyValue" + new Random().nextInt();
    
    PreferenceManager.saveToFile(dummy, file);

    IPreference pref = PreferenceManager.readFromFile(file);
    Assert.assertEquals(pref.getClass(), DummyPref.class);
    Assert.assertEquals(dummy.property, ((DummyPref)pref).property);
    file.delete();
  }

  @Test
  public void testSavePref() {
    fixture.init(DIR, new String[]{DummyPref.ID}, new Class<?>[]{DummyPref.class});

    
    DummyPref obj = (DummyPref) fixture.getPreference(DummyPref.ID);
    obj.property = "NewProperty"+new Random().nextInt();
    fixture.saveAll();

    PreferenceManager manager = new PreferenceManager();
    manager.init(DIR, new String[]{DummyPref.ID}, new Class<?>[]{DummyPref.class});
    DummyPref newObj = manager.getPreference(DummyPref.ID);
    Assert.assertEquals(obj.property, newObj.property);
  }


  public static class DummyPref implements IPreference {

    private static final String ID = "DummyPref";
    private String property;


    @Override
    public String getId() {
      return ID;
    }

    @Override
    public IPreferenceView createView(Object owner) {
      return null;
    }
  }
}
