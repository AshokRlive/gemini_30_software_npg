/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import com.jgoodies.binding.beans.Model;
import com.lucy.g3.configtool.preferences.IPreference;
import com.lucy.g3.configtool.preferences.IPreferenceView;


public abstract class AbstractPreference extends Model implements IPreference {
  private static final long serialVersionUID = 1L;
  private final String id;
  
  public AbstractPreference(String id) {
    this.id = id;
  }
  
  @Override
  public String getId() {
    return id;
  }
  
}

