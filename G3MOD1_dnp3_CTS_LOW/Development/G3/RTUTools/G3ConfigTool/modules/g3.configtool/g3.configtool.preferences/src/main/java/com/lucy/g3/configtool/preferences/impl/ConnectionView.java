/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.*;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.UIUtilities;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
class ConnectionView extends AbstractView<ConnectionPreference> {
  
  public ConnectionView(Object owner, ConnectionPreference bean) {
    super(owner, bean);
    initComponents();
    initComponentsCustomization();
    initComponentsBinding();
  }

  private void createUIComponents() {
    // Create connect time field
    ValueModel vm = model.getBufferedModel(ConnectionPreference.PROPERTY_CONNECT_TIMEOUT);
    vm = convertMsToSec(vm);
    SpinnerNumberModel spinnerModel = SpinnerAdapterFactory.createNumberAdapter(vm, 
        ConnectionPreference.DEFAULT_CONNECT_TIMEOUT_MS/1000, 
        ConnectionPreference.MIN_CONNECT_TIMEOUT_MS/1000,
        ConnectionPreference.MAX_CONNECT_TIMEOUT_MS/1000,
        1);
    ftfConnectTimeout =  new JSpinner(spinnerModel);
    
    // Create inactivity field
    vm = model.getBufferedModel(ConnectionPreference.PROPERTY_INACTIVITY_MINS);
    spinnerModel = SpinnerAdapterFactory.createNumberAdapter(vm, 0, 0, Integer.MAX_VALUE, 1);
    spinnerInactivity = new JSpinner(spinnerModel);
  }
  
  private ValueModel convertMsToSec(ValueModel vm) {
    return new ConverterValueModel(vm, new BindingConverter<Integer, Integer>() {
      @Override
      public Integer targetValue(Integer sourceValue) {
        return sourceValue == null ? null : sourceValue / 1000;
      }

      @Override
      public Integer sourceValue(Integer targetValue) {
        return targetValue == null ? null : targetValue * 1000;
      }
    });
  }

  
  private void initComponentsCustomization() {
    lblConnectTimeout.setText(lblConnectTimeout.getText() + createHint(
        ConnectionPreference.MIN_CONNECT_TIMEOUT_MS / 1000,
        ConnectionPreference.MAX_CONNECT_TIMEOUT_MS / 1000));
  }
  
  private void initComponentsBinding(){
    /* Binding server list comboBox */
    ArrayList<String> list = model.getBean() != null ? model.getBean().getServerList() : new ArrayList<String>();
    @SuppressWarnings("unchecked")
    ComboBoxModel<String> serverAddModel =
        new ComboBoxAdapter<String>(list, model.getBufferedModel(ConnectionPreference.PROPERTY_SERVER));
    comboIpAdd.setModel(serverAddModel);
  }

  private String createHint(Comparable<?> min, Comparable<?> max) {
    return String.format(" [%d ~ %d]",  min, max);
  }
  
  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    panelConnection = new JPanel();
    panelTop = new JPanel();
    JLabel label1 = new JLabel();
    comboInterface = new JComboBox<>();
    JLabel lblIpAdd = new JLabel();
    comboIpAdd = new JComboBox<>();
    JLabel label3 = new JLabel();
    lblConnectTimeout = new JLabel();
    label2 = new JLabel();
    label4 = new JLabel();

    //======== this ========
    setLayout(new BorderLayout());

    //======== panelConnection ========
    {
      panelConnection.setLayout(new BorderLayout());

      //======== panelTop ========
      {
        panelTop.setLayout(new FormLayout(
          "[50dlu,default], $lcgap, [80dlu,default], $lcgap, default",
          "3*(default, $ugap), default"));

        //---- label1 ----
        label1.setText("Interface:");
        panelTop.add(label1, CC.xy(1, 1));

        //---- comboInterface ----
        comboInterface.setModel(new DefaultComboBoxModel<>(new String[] {
          "TCP/IP"
        }));
        comboInterface.setEnabled(false);
        panelTop.add(comboInterface, CC.xy(3, 1));

        //---- lblIpAdd ----
        lblIpAdd.setText("Host:");
        panelTop.add(lblIpAdd, CC.xy(1, 3));

        //---- comboIpAdd ----
        comboIpAdd.setEditable(true);
        comboIpAdd.setEnabled(false);
        panelTop.add(comboIpAdd, CC.xy(3, 3));

        //---- label3 ----
        label3.setText("Connection Timeout:");
        panelTop.add(label3, CC.xy(1, 5));
        panelTop.add(ftfConnectTimeout, CC.xy(3, 5));

        //---- lblConnectTimeout ----
        lblConnectTimeout.setText("seconds");
        panelTop.add(lblConnectTimeout, CC.xy(5, 5));

        //---- label2 ----
        label2.setText("Log out after ");
        panelTop.add(label2, CC.xy(1, 7));
        panelTop.add(spinnerInactivity, CC.xy(3, 7));

        //---- label4 ----
        label4.setText("minutes of inactivity ");
        panelTop.add(label4, CC.xy(5, 7));
      }
      panelConnection.add(panelTop, BorderLayout.NORTH);
    }
    add(panelConnection, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panelConnection;
  private JPanel panelTop;
  private JComboBox<String> comboInterface;
  private JComboBox<String> comboIpAdd;
  private JSpinner ftfConnectTimeout;
  private JLabel lblConnectTimeout;
  private JLabel label2;
  private JSpinner spinnerInactivity;
  private JLabel label4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

  @Override
  public int getIndex() {
    return 1;
  }
}
