/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.apache.log4j.Logger;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.l2fprod.common.swing.JButtonBar;
import com.lucy.g3.configtool.preferences.IPreference;
import com.lucy.g3.configtool.preferences.IPreferenceView;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.help.Helper;

/**
 * The Class PreferenceDialog.
 * Note: all tabs are sorted by its ID.
 */
public class PreferenceDialog extends AbstractDialog {
  
  private static final int WIDTH = 550;
  private static final int HEIGHT = 400;

  public static final String ACTION_KEY_APPLY = "apply";
  
  public static final String PROPERTY_APPLY_ENABLED = "applyEnabled";
  
  private Logger log = Logger.getLogger(PreferenceDialog.class);
  

  private boolean applyEnabled;

  /** The current displayed content component, changed when select tab.*/
  private Component currentComponent;

  private final Collection<IPreferenceView> allPrefsViews;

  private final PreferenceManager manager;
  private final ButtonGroup buttonGroup = new ButtonGroup();
  
  private static String previousSelectedId = null;
   
  /**
   * Construct a preferences dialog to edit options and save it to a file.
   * @param owner owner frame
   * @param prefs options bean
   * @param prefsFile the file options will be saved to. Default file will be used if it is null.
   */
  public PreferenceDialog(Window owner, PreferenceManager manager) {
    super(owner);
    this.manager = manager;
    this.allPrefsViews =  getAllViews(this, manager);

    initComponents();
    loadPreferenceViews();
    
    pack();
    setLocationRelativeTo(owner);
    setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    setDialogResult(RESULT_CANCELLED);
  }

  private static ArrayList<IPreferenceView> getAllViews(Object owner, PreferenceManager manager) {
    Collection<IPreference> prefs = manager.getAllPreferences();
    ArrayList<IPreferenceView> views = new ArrayList<>();
    for (IPreference pref : prefs) {
      IPreferenceView view = pref.createView(owner);
      if(view != null) {
        views.add(view);
      }
    }
    
    Collections.sort(views, new Comparator<IPreferenceView>() {

      @Override
      public int compare(IPreferenceView o1, IPreferenceView o2) {
        return o1.getIndex() - o2.getIndex();
      }
    });
    return views;
  }

  public boolean isApplyEnabled() {
    return applyEnabled;
  }
  
  private void setApplyEnabled(boolean applyEnabled) {
    Object oldValue = this.applyEnabled;
    this.applyEnabled = applyEnabled;
    firePropertyChange("applyEnabled", oldValue, applyEnabled);
  }

  

  // -----------------------[Actions]--------------------------

  @org.jdesktop.application.Action(enabledProperty = PROPERTY_APPLY_ENABLED)
  public void apply() {
    setDialogResult(RESULT_AFFIRMED);
    triggerCommit();
    setApplyEnabled(false);
    saveToFile();
  }
  
  @org.jdesktop.application.Action
  @Override
  public void ok() {
    setDialogResult(RESULT_AFFIRMED);
    triggerCommit();
    release();
    
    closeDialog();
    saveToFile();
  }

  private void saveToFile() {
    try {
      manager.saveAll();
    } catch (Exception e) {
      log.error("Failed to save options to file",e);
    }
  }
  
  @org.jdesktop.application.Action
  @Override
  public void cancel() {
    release();
    closeDialog();
  }

  private void closeDialog() {
    ButtonModel btnModel = buttonGroup.getSelection();
    if(btnModel != null)
      previousSelectedId = btnModel.getActionCommand(); // Id was store as command.
    
    this.dispose();
  }
   
  private void triggerCommit() {
    for (IPreferenceView v : allPrefsViews) {
      v.commit();
    }
  }
  
  private void release() {
    for (IPreferenceView v : allPrefsViews) {
      v.release();
    }
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return contentPane;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createHelpButtonPanel(Helper.createHelpButton(getClass()), 
        new JButton(getAction(ACTION_KEY_APPLY)),
        new JButton(getAction(ACTION_KEY_OK)),
        new JButton(getAction(ACTION_KEY_CANCEL)));
  }
  
  private void loadPreferenceViews() {
    // Handling buffering change event.
    PropertyChangeListener pcl = new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if(PresentationModel.PROPERTY_BUFFERING.equals(evt.getPropertyName())
            && (Boolean)evt.getNewValue() == true)
          setApplyEnabled(true);
      }
    };

    for (IPreferenceView v : allPrefsViews) {
        v.getModel().addPropertyChangeListener(PresentationModel.PROPERTY_BUFFERING,pcl);
        final String id = v.getModel().getBean().getId();
        
        JComponent comp = v.getComponent();
        if(comp != null) {
          Preconditions.checkNotNull(v.getButtonName(), "title must not be null:"+v.getClass().getSimpleName());
          Preconditions.checkNotNull(v.getButtonIcon(), "icon must not be null:"+v.getClass().getSimpleName());
          Preconditions.checkNotNull(v.getViewTitle(), "title must not be null:"+v.getClass().getSimpleName());
          
          // Add to button bar.
          addTabButton(v.getButtonName(), v.getButtonIcon(), 
              wrapContent(v.getViewTitle(), comp), buttonBar, buttonGroup, id);
        }
    }
    
    if(buttonGroup.getSelection() == null) {
      selectTab(null); // Select first tab 
    }
  }
  

  /**
   * Wrap the given content component into a panel which has a title on top.
   */
  private JComponent wrapContent(String title, JComponent view) {
    JPanel contentPanel = new JPanel(new BorderLayout());
    // ---- TOP Title----
    JPanel top = new JPanel(new FormLayout("default:grow", "default, $lgap, default"));
    JLabel lblTitle = new JLabel(title);
    lblTitle.setFont(top.getFont().deriveFont(Font.BOLD, 12));
    top.setBackground(contentPanel.getBackground().brighter());
    top.add(lblTitle, CC.xy(1, 1));
    top.add(new JSeparator(), CC.xy(1, 3));
    contentPanel.add("North", top);
    top.setOpaque(false);
    top.setBorder(Borders.DLU4_BORDER);
  
    // ---- ContentPanel ----
    if (view != null) {
      if(!(view instanceof JScrollPane)) {
        JScrollPane sp = new JScrollPane(view);
        UIUtils.increaseScrollSpeed(sp);
        view = sp;
        sp.setBorder(null);
        sp.setPreferredSize(new Dimension(WIDTH, HEIGHT));
      }
      contentPanel.add("Center", view);
    }
    return contentPanel;
  }

  private void addTabButton(String title, ImageIcon icon, final Component component, JButtonBar bar,
      ButtonGroup group, String id) {
    
    Action action = new AbstractAction(title, icon) {
  
      @Override
      public void actionPerformed(ActionEvent e) {
        PreferenceDialog.this.show(component);
      }
    };
    
    // Store id as command
    action.putValue(Action.ACTION_COMMAND_KEY, id);
  
    JToggleButton button = new JToggleButton(action);
    button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bar.add(button);
    group.add(button);
    
    
    // Set selection state
    if(id.equals(previousSelectedId)) {
      button.setSelected(true);
      PreferenceDialog.this.show(component);
    }
  }
  
  public void selectTab(String preferenceId){
    Enumeration<AbstractButton> allBtns = buttonGroup.getElements();
    while(allBtns.hasMoreElements()) {
      AbstractButton btn = allBtns.nextElement();
      String id = btn.getActionCommand();
      if(preferenceId == null || preferenceId.equals(id)) {
        btn.setSelected(true);
        btn.getAction().actionPerformed(null);
        break;
      }
    }
  }
  
  private void show(Component component) {
    if (currentComponent != null) {
      contentPane.remove(currentComponent);
    }
    contentPane.add("Center", currentComponent = component);
    contentPane.revalidate();
    repaint();
  }
  
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    contentPane = new JPanel();
    buttonBar = new JButtonBar();

    //======== this ========
    setTitle("Preferences");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    Container contentPane2 = getContentPane();
    contentPane2.setLayout(new BorderLayout());

    //======== contentPane ========
    {
      contentPane.setLayout(new BorderLayout(5, 5));

      //---- buttonBar ----
      buttonBar.setMaximumSize(new Dimension(30, 53));
      buttonBar.setBorder(new CompoundBorder(
          new LineBorder(Color.gray, 1, true),
          new EmptyBorder(5, 10, 5, 10)));
      buttonBar.setPreferredSize(new Dimension(100, 53));
      contentPane.add(buttonBar, BorderLayout.WEST);
    }
    contentPane2.add(contentPane, BorderLayout.CENTER);

    setSize(585, 420);
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents

    /* Set layout*/
    buttonBar.setOrientation(SwingConstants.VERTICAL);
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel contentPane;
  private JButtonBar buttonBar;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
