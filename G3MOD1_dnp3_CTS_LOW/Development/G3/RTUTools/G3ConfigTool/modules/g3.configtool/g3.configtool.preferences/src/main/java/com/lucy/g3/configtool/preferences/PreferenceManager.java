/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.log4j.Logger;


/**
 * The Class PreferenceManager.
 */
public class PreferenceManager {
  private static final String PREF_EXTENSION = "pref";

  private static Logger log = Logger.getLogger(PreferenceManager.class);
  
  private final HashMap<String, IPreference> prefsMap = new HashMap<>();  
  
  private File prefDir;
  
  public static final PreferenceManager INSTANCE = new PreferenceManager();

  public void init(File prefDir, String[] prefIDs, Class<?>[] prefClasses) {
    this.prefDir = prefDir;
    this.prefDir.mkdirs();

    if(prefIDs != null && prefClasses != null) {
      for (int i = 0; i < prefIDs.length; i++) {
        initPreference(prefIDs[i], prefClasses[i]);
      }
    }
    
  }


  public Collection<IPreference> getAllPreferences(){
    return prefsMap.values();
  }
  
  @SuppressWarnings("unchecked")
  public <T extends IPreference> T getPreference(String id){
    return (T) prefsMap.get(id);
  }
  
  /**
   * Lazily initialise the preference object.
   * @param id
   * @param c
   * @return null if the key was not provided to init(). 
   */
  private void initPreference(String id, Class<?> c){
    IPreference pref = prefsMap.get(id);
    
    if(pref == null) {
      // Read from file
      try {
        File prefFile = idToFile(id);
        pref = readFromFile(prefFile);
        if(c.isInstance(pref)) {
          prefsMap.put(id, pref);
          log.info("Loaded preference file:"+prefFile);
        }
      } catch (Exception e) {
        log.info("Preferece not found: " +id);
      }
    }
    
    
    if(pref == null || !c.isInstance(pref)) {
      // Create default
      try {
          pref = (IPreference)c.newInstance();
          prefsMap.put(id, pref);
          log.info("Default preference was created: " +id);
      } catch (Exception e) {
        log.error("Failed to create preference instance: " + c);
      } 
    }
  }
  
  
  public void saveAll(){
    Collection<IPreference> values = prefsMap.values();
    for (IPreference pref : values) {
      try {
        save(pref);
      } catch(Exception e){
        log.error("Failed to save preferece: " + pref, e);
      }
    }
    
    log.info("All preferences saved to:"+prefDir);
  }
  
  public void save(IPreference pref) throws Exception{
    saveToFile(pref, idToFile(pref.getId()));
  }
  
  private File idToFile(String id) {
    return new File(prefDir, id+"."+PREF_EXTENSION);
  }

  /**
   * Serialises preference object  into a file.
   */
  static void saveToFile(IPreference obj, File outputFile) throws Exception {
    FileOutputStream fo = null;

    try {
      fo = new FileOutputStream(outputFile);
      ObjectOutputStream so = new ObjectOutputStream(fo);
      so.writeObject(obj);
      so.flush();
      so.close();
    } catch (Exception e) {
      throw e;
    } finally {
      if (fo != null) {
        try {
          fo.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

//  static IPreference[] readAll(File prefDir) {
//    
//    String[] prefFiles = prefDir.list(new SuffixFileFilter("."+PREF_EXTENSION));
//    ArrayList<IPreference> prefs = new ArrayList<>();
//    
//    for (String pref : prefFiles) {
//      try {
//        IPreference prefObj = readFromFile(new File(prefDir, pref));
//        prefs.add(prefObj);
//        
//      } catch(Exception e){
//        log.error("Failed to save preferece: " + pref, e);
//      }
//    }
//    
//    return prefs.toArray(new IPreference[prefs.size()]);
//  }
  
  /**
   * Restores {@code Options} instance from a file.
   *
   * @return option bean. Null if it is not found.
   */
  @SuppressWarnings("unchecked")
  static <PreferenceT extends IPreference> PreferenceT readFromFile(File preferenceFile) throws Exception {
    FileInputStream fi = null;

    try {
      fi = new FileInputStream(preferenceFile);
      ObjectInputStream si = new ObjectInputStream(fi);
      return (PreferenceT) si.readObject();
    } catch (Exception e) {
      throw e;
    } finally {
      if (fi != null) {
        try {
          fi.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}

