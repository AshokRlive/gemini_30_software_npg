/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.ui;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;

import com.lucy.g3.configtool.preferences.PreferenceManager;


/**
 *
 */
public class PreferenceDialogTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
  }

  public static void main(String[] args) throws Exception {
    Logger log = Logger.getLogger(PreferenceDialogTest.class);
    log.setLevel(Level.DEBUG);
    PreferenceManager prefs = PreferenceManager.INSTANCE;
    File dir = new File("target/PreferenceDialogTest/");
    dir.mkdirs();
    prefs.init(dir, null, null);
    new PreferenceDialog(null, prefs).setVisible(true);
  }
}

