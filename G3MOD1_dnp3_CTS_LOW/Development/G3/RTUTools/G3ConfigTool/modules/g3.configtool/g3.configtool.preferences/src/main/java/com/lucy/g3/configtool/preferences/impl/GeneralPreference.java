/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.util.Locale;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.lucy.g3.configtool.preferences.IPreferenceView;

/**
 *
 */
public class GeneralPreference extends AbstractPreference {

  private static final long serialVersionUID = -2920100199461662797L;

  public final static String ID = "GeneralPreference";

  /** Rounding mode for float value. */
  public static final String PROPERTY_ERRORREPORT_ENABLED = "errorReportEnabled";
  public static final String PROPERTY_LOGLEVEL = "loglevel";
  public static final String PROPERTY_LOCALE = "locale";

  private Locale locale = Locale.getDefault();

  private Level loglevel = Logger.getRootLogger().getLevel();

  private boolean errorReportEnabled;


  public GeneralPreference() {
    super(ID);
  }

  public Level getLoglevel() {
    return loglevel;
  }

  public void setLoglevel(Level loglevel) {
    if (loglevel == null) {
      return;
    }

    Object oldValue = getLoglevel();
    this.loglevel = loglevel;
    firePropertyChange(PROPERTY_LOGLEVEL, oldValue, loglevel);
  }

  public boolean isErrorReportEnabled() {
    return errorReportEnabled;
  }

  public void setErrorReportEnabled(boolean errorReportEnabled) {
    Object oldValue = this.errorReportEnabled;
    this.errorReportEnabled = errorReportEnabled;
    firePropertyChange(PROPERTY_ERRORREPORT_ENABLED, oldValue, errorReportEnabled);
  }

  public Locale getLocale() {
    return locale;
  }

  public void setLocale(Locale locale) {
    if (locale == null) {
      return;
    }

    Object oldValue = getLocale();
    this.locale = locale;
    firePropertyChange(PROPERTY_LOCALE, oldValue, locale);
  }

  @Override
  public IPreferenceView createView(Object owner) {
    return new GeneralView(owner, this);
  }
}
