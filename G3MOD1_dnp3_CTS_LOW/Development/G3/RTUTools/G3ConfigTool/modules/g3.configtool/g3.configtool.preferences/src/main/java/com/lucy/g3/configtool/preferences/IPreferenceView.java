/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.jgoodies.binding.PresentationModel;

/**
 *
 */
public interface IPreferenceView {
  JComponent getComponent();

  PresentationModel<? extends IPreference> getModel();

  String getButtonName();

  ImageIcon getButtonIcon();

  String getViewTitle();
  
  /**
   * Gets the index of the view, which determines this view's position in preference dialog. 
   */
  int getIndex();

  void release();

  void commit();
}

