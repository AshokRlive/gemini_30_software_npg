/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences;
import java.io.Serializable;

/**
 *
 */
public interface IPreference extends Serializable {
  String getId();
  
  /**
   * Creates a view for viewing/editing this preference. 
   * @param owner the owner component of the view, could be component, window, or null.
   * @return a view of this preference.
   */
  IPreferenceView createView(Object owner);
}

