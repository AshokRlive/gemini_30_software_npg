/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import com.lucy.g3.configtool.preferences.IPreferenceView;

/**
 *
 */
public class AdvancedPreference extends AbstractPreference {

  private static final long serialVersionUID = 7272346469188125597L;
  
  public final static String ID = "AdvancedPreference";


  public AdvancedPreference() {
    super(ID);
  }


  @Override
  public IPreferenceView createView(Object owner) {
    return new AdvancedView(owner, this);
  }


}
