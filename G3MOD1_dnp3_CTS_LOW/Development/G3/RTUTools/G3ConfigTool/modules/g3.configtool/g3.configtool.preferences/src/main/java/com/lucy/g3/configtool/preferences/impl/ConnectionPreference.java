/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.util.ArrayList;

import com.lucy.g3.configtool.preferences.IPreferenceView;

/**
 *
 */
public class ConnectionPreference extends AbstractPreference {

  private static final long serialVersionUID = 5788839607597085245L;

  public final static String ID = "ConnectionPreference";

  public static final int DEFAULT_CONNECT_TIMEOUT_MS = 12000;
  public static final int MIN_CONNECT_TIMEOUT_MS = 5000;      
  public static final int MAX_CONNECT_TIMEOUT_MS = 1000000;   
  
  public static final String PROPERTY_CONNECT_TIMEOUT = "connectTimeout";
  public static final String PROPERTY_SERVER = "server";
  public static final String PROPERTY_SERVER_LIST = "serverList";
  
  public static final String PROPERTY_INACTIVITY_MINS = "inactivityMins";

  private int connectTimeout = DEFAULT_CONNECT_TIMEOUT_MS; // 10 seconds

  private String server = "0.0.0.0";

  private ArrayList<String> serverList = new ArrayList<String>();

  private int inactivityMins = 10000;
  
  public ConnectionPreference() {
    super(ID);
  }

  /**
   * Add a server to server list if it doesn't exist.
   *
   * @param server
   *          the server address to be added.
   */
  public void addServer(String server) {
    if (server != null && !serverList.contains(server)) {
      serverList.add(server);
    }
  }

  public ArrayList<String> getServerList() {
    return new ArrayList<String>(serverList);
  }


  public String getServer() {
    return server;
  }

  public void setServer(String server) {
    if (server != null && !server.equalsIgnoreCase(getServer())) {
      Object oldValue = getServer();
      this.server = server;
      addServer(server);
      firePropertyChange(PROPERTY_SERVER, oldValue, server);
    }
  }

  public void setServers(String[] servers) {
    if (servers != null) {
      for (int i = 0; i < servers.length; i++) {
        setServer(servers[i]);
      }
    }
  }

  public int getConnectTimeout() {
    if (connectTimeout < MIN_CONNECT_TIMEOUT_MS)
      connectTimeout = DEFAULT_CONNECT_TIMEOUT_MS;

    return connectTimeout;
  }

  public void setConnectTimeout(int connectTimeout) {
    if (connectTimeout < MIN_CONNECT_TIMEOUT_MS)
      return;

    Object oldValue = this.connectTimeout;
    this.connectTimeout = connectTimeout;
    firePropertyChange(PROPERTY_CONNECT_TIMEOUT, oldValue, connectTimeout);
  }

  @Override
  public IPreferenceView createView(Object owner) {
    return new ConnectionView(owner, this);
  }

  
  public int getInactivityMins() {
    return inactivityMins;
  }

  public void setInactivityMins(int inactivityMins) {
    Object oldValue = this.inactivityMins;
    this.inactivityMins = inactivityMins;
    firePropertyChange(PROPERTY_INACTIVITY_MINS, oldValue, inactivityMins);
  }
  

}
