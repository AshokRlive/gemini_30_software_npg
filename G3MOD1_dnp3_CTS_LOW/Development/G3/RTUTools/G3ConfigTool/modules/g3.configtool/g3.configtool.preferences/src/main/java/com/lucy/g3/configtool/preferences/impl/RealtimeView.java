/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.awt.BorderLayout;
import java.math.RoundingMode;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.jgoodies.binding.PresentationModel;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.adapter.SpinnerAdapterFactory;
import com.jgoodies.binding.value.BufferedValueModel;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.ComponentsFactory;
import com.lucy.g3.gui.common.widgets.ext.swing.spinner.SpinnerWheelSupport;
import com.lucy.g3.xml.gen.common.IEC870Enum.LU_IOA_FORMAT;

class RealtimeView extends AbstractView<RealtimePreference> {


  public RealtimeView(Object owner, RealtimePreference bean) {
    super(owner, bean);
    initComponents();
    initComponentsCustomization();
    initComponentsBinding();
  }

  private void createUIComponents() {
    // Create refresh rate field
    BufferedValueModel vm = model.getBufferedModel(RealtimePreference.PROPERTY_UPDATE_PERIOD_MS);
    ftfRefreshRate = ComponentsFactory.createNumberField(
        vm,
        RealtimePreference.MIN_PERIOD_MS,
        RealtimePreference.MAX_PERIOD_MS);
  }

  private void initComponentsCustomization() {
    SpinnerWheelSupport.installMouseWheelSupport(spinnerFDigits);

    /* Set hint label text */
    lblRefreshRate.setText(
        lblRefreshRate.getText() + createHint(RealtimePreference.MIN_PERIOD_MS, RealtimePreference.MAX_PERIOD_MS));
    labelFDigitsBound.setText(labelFDigitsBound.getText() + createHint(
        RealtimePreference.MIN_RT_FRACTION_DIGITS,
        RealtimePreference.MAX_RT_FRACTION_DIGITS));
  }

  private String createHint(Comparable<?> min, Comparable<?> max) {
    return String.format(" [%d ~ %d]", min, max);
  }

  private void initComponentsBinding() {
    /* Binding fraction digits spinner */
    BufferedValueModel vm = model.getBufferedModel(RealtimePreference.PROPERTY_RT_FRACTION_DIGITS);
    SpinnerNumberModel fdigitsModel = SpinnerAdapterFactory.createNumberAdapter(vm,
        RealtimePreference.DEFAULT_RT_FRACTION_DIGITS, RealtimePreference.MIN_RT_FRACTION_DIGITS,
        RealtimePreference.MAX_RT_FRACTION_DIGITS, 1);
    spinnerFDigits.setModel(fdigitsModel);

    /* Binding rounding mode comboBox */
    @SuppressWarnings("unchecked")
    ComboBoxModel<RoundingMode> roundingModel = new ComboBoxAdapter<RoundingMode>(RoundingMode.values(),
        model.getBufferedModel(RealtimePreference.PROPERTY_RT_FRACTION_ROUND_MODE));
    comboRound.setModel(roundingModel);

    /* Binding IOA format */
    @SuppressWarnings("unchecked")
    ComboBoxModel<LU_IOA_FORMAT> ioaModel = new ComboBoxAdapter<LU_IOA_FORMAT>(LU_IOA_FORMAT.values(),
        model.getBufferedModel(RealtimePreference.PROPERTY_IOA_FORMAT));
    comboIoa.setModel(ioaModel);
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    panelRealtime = new JPanel();
    JLabel labelRate = new JLabel();
    lblRefreshRate = new JLabel();
    JLabel labelFDigits = new JLabel();
    spinnerFDigits = new JSpinner();
    labelFDigitsBound = new JLabel();
    JLabel labelRound = new JLabel();
    comboRound = new JComboBox<>();
    JLabel label2 = new JLabel();
    comboIoa = new JComboBox<>();

    //======== this ========
    setLayout(new BorderLayout());

    //======== panelRealtime ========
    {
      panelRealtime.setLayout(new FormLayout(
          "default, $lcgap, [80dlu,default], $lcgap, default",
          "3*(default, $ugap), default"));

      //---- labelRate ----
      labelRate.setText("Refresh Rate:");
      panelRealtime.add(labelRate, CC.xy(1, 1));
      panelRealtime.add(ftfRefreshRate, CC.xy(3, 1));

      //---- lblRefreshRate ----
      lblRefreshRate.setText("ms");
      panelRealtime.add(lblRefreshRate, CC.xy(5, 1));

      //---- labelFDigits ----
      labelFDigits.setText("Maximum Fraction Digits: ");
      panelRealtime.add(labelFDigits, CC.xy(1, 3));
      panelRealtime.add(spinnerFDigits, CC.xy(3, 3));
      panelRealtime.add(labelFDigitsBound, CC.xy(5, 3));

      //---- labelRound ----
      labelRound.setText("Rounding Mode:");
      panelRealtime.add(labelRound, CC.xy(1, 5));
      panelRealtime.add(comboRound, CC.xy(3, 5));

      //---- label2 ----
      label2.setText("IOA Format:");
      panelRealtime.add(label2, CC.xy(1, 7));
      panelRealtime.add(comboIoa, CC.xy(3, 7));
    }
    add(panelRealtime, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelRealtime;
  private JFormattedTextField ftfRefreshRate;
  private JLabel lblRefreshRate;
  private JSpinner spinnerFDigits;
  private JLabel labelFDigitsBound;
  private JComboBox<RoundingMode> comboRound;
  private JComboBox<LU_IOA_FORMAT> comboIoa;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  public int getIndex() {
    return 2;
  }
}
