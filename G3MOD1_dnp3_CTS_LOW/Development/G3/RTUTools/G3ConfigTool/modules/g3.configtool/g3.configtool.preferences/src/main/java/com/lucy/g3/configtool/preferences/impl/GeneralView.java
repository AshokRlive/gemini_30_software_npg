/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Locale;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

import org.apache.log4j.Level;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

class GeneralView extends AbstractView<GeneralPreference>{

  public GeneralView(Object owner, GeneralPreference bean) {
    super(owner, bean);
    initComponents();
    initComponentsCustomization();
    initComponentsBinding();
  }
  
  private void initComponentsCustomization() {
    comboLang.setRenderer(new LanuageComboRender());
  }
  
  private void initComponentsBinding() {
    /* Binding Languages ComboBox */
    Locale[] locales = new Locale[] {/* Locale.CHINA, */Locale.UK };
    @SuppressWarnings("unchecked")
    ComboBoxModel<Locale> langModel = new ComboBoxAdapter<Locale>(locales,
        model.getBufferedModel(GeneralPreference.PROPERTY_LOCALE));
    comboLang.setModel(langModel);
  
    /* Binding Log level comboBox */
    Level[] levels = new Level[] { Level.FATAL, Level.ERROR, Level.WARN, Level.INFO, Level.DEBUG };
    @SuppressWarnings("unchecked")
    ComboBoxModel<Level> levelsModel = new ComboBoxAdapter<Level>(levels,
        model.getBufferedModel(GeneralPreference.PROPERTY_LOGLEVEL));
    comboLevels.setModel(levelsModel);
    
    Bindings.bind(checkBoxErrorReport, model.getBufferedModel(GeneralPreference.PROPERTY_ERRORREPORT_ENABLED));
  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panelGeneral = new JPanel();
    labelLang = new JLabel();
    comboLang = new JComboBox<>();
    labelLoglevel = new JLabel();
    comboLevels = new JComboBox<>();
    checkBoxErrorReport = new JCheckBox();

    //======== this ========
    setLayout(new BorderLayout());

    //======== panelGeneral ========
    {
      panelGeneral.setLayout(new FormLayout(
          "[50dlu,default], $lcgap, [80dlu,default]",
          "2*(default, $ugap), 2*(default, $lgap), default"));

      //---- labelLang ----
      labelLang.setText("Language:");
      labelLang.setVisible(false);
      panelGeneral.add(labelLang, CC.xy(1, 1));

      //---- comboLang ----
      comboLang.setVisible(false);
      panelGeneral.add(comboLang, CC.xy(3, 1));

      //---- labelLoglevel ----
      labelLoglevel.setText("Log Level:");
      panelGeneral.add(labelLoglevel, CC.xy(1, 3));
      panelGeneral.add(comboLevels, CC.xy(3, 3));

      //---- checkBoxErrorReport ----
      checkBoxErrorReport.setText("Enable error report");
      checkBoxErrorReport.setToolTipText("If enabled, a  dialog will pop up when an unexpected error is detected.");
      panelGeneral.add(checkBoxErrorReport, CC.xy(3, 5));
    }
    add(panelGeneral, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelGeneral;
  private JLabel labelLang;
  private JComboBox<Locale> comboLang;
  private JLabel labelLoglevel;
  private JComboBox<Level> comboLevels;
  private JCheckBox checkBoxErrorReport;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static class LanuageComboRender extends DefaultListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
        boolean cellHasFocus) {
      super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

      Locale locale = (Locale) value;
      if (value != null) {
        setText(locale.getDisplayLanguage());
      } else {
        setText("");
      }
      return this;
    }
  }

  @Override
  public int getIndex() {
    return 0;
  }
}
