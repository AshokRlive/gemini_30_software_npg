/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.awt.Component;
import java.awt.Window;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.jdesktop.application.Application;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.swingx.util.WindowUtils;

import com.jgoodies.binding.PresentationModel;
import com.lucy.g3.configtool.preferences.IPreference;
import com.lucy.g3.configtool.preferences.IPreferenceView;


/**
 *
 */
public abstract class AbstractView<PreferenceT extends IPreference> extends JPanel implements IPreferenceView{
  
  private ResourceMap resourceMap = Application.getInstance().getContext().getResourceMap(getClass());
  
  protected final PresentationModel<PreferenceT> model; 
  private Object owner;
  
  public AbstractView(Object owner, PreferenceT bean){
    this.owner = owner;
    this.model = new PresentationModel<>(bean);
  }
  
  protected Window getWindow(){
    if(owner == null) 
      return null;
    if(owner instanceof Window)
      return (Window) owner;
    if(owner instanceof Component)
      return WindowUtils.findWindow((Component)owner);
    return null;
  }
  
  public PreferenceT getPreference(){
    return this.model.getBean();
  }
  
  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public PresentationModel<PreferenceT> getModel() {
    return model;
  }

  @Override
  public ImageIcon getButtonIcon() {
    return resourceMap.getImageIcon("buttonIcon");
  }
  
  @Override
  public String getButtonName() {
    return resourceMap.getString("buttonName");
  }

  @Override
  public String getViewTitle() {
    return resourceMap.getString("viewTitle");
  }

  @Override
  public void release() {
    model.release();
    model.setBean(null);
  }

  @Override
  public void commit() {
    model.triggerCommit();
  }

  
  
}

