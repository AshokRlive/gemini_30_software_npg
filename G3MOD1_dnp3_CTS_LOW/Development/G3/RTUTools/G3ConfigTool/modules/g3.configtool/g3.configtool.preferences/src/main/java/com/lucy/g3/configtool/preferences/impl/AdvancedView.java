/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.preferences.impl;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.commons.io.FileUtils;
import org.jdesktop.swingx.JXLabel;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
class AdvancedView extends AbstractView<AdvancedPreference>{
  
  public AdvancedView(Object owner, AdvancedPreference bean) {
    super(owner, bean);
    initComponents();
    initComponentsCustomization();
  }

  private void initComponentsCustomization() {
    /* Set cache path*/
    lblCachePath.setText(UIUtils.getLocalStorageDir().getAbsolutePath());
  }
  
  private void btnCleanCacheActionPerformed(ActionEvent e) {
    int ret = JOptionPane.showConfirmDialog(this, "The content of the cache directory will be cleaned."
        + "\nAre you sure?", 
        "Cleaning Cache", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
    
    if(ret == JOptionPane.YES_OPTION) {
      try {
        FileUtils.cleanDirectory(new File(lblCachePath.getText()));
        JOptionPane.showMessageDialog(this, "The cache has been cleaned successfully!", 
            "Success", JOptionPane.INFORMATION_MESSAGE);
      } catch (IOException e1) {
        MessageDialogs.error(this, "Failed to clean cache directory", e1);
      }
    }
  }
  

  private void btnLocateCacheActionPerformed(ActionEvent e) {
    try {
      Desktop.getDesktop().open(new File(lblCachePath.getText()));
    } catch (IOException e1) {
      MessageDialogs.error(this, "Failed to locate cache directory", e1);
    }
  }


  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panelAdvanced = new JPanel();
    lblCache = new JLabel();
    lblCachePath = new JXLabel();
    panel1 = new JPanel();
    btnLocateCache = new JButton();
    btnCleanCache = new JButton();

    //======== this ========
    setLayout(new BorderLayout());

    //======== panelAdvanced ========
    {
      panelAdvanced.setLayout(new FormLayout(
          "default, $lcgap, [default,120dlu]:grow, $lcgap, default",
          "2*(default, $lgap), default"));

      //---- lblCache ----
      lblCache.setText("Configuration Tool Cache:");
      panelAdvanced.add(lblCache, CC.xy(1, 3));

      //---- lblCachePath ----
      lblCachePath.setText("cache folder path");
      lblCachePath.setLineWrap(true);
      panelAdvanced.add(lblCachePath, CC.xy(3, 3));

      //======== panel1 ========
      {
        panel1.setLayout(new FormLayout(
            "default",
            "default, $ugap, default"));

        //---- btnLocateCache ----
        btnLocateCache.setText("Locate");
        btnLocateCache.setToolTipText("Open Cache Location");
        btnLocateCache.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnLocateCacheActionPerformed(e);
          }
        });
        panel1.add(btnLocateCache, CC.xy(1, 1));

        //---- btnCleanCache ----
        btnCleanCache.setText("Clean Cache");
        btnCleanCache.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnCleanCacheActionPerformed(e);
          }
        });
        panel1.add(btnCleanCache, CC.xy(1, 3));
      }
      panelAdvanced.add(panel1, CC.xy(5, 3));
    }
    add(panelAdvanced, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panelAdvanced;
  private JLabel lblCache;
  private JXLabel lblCachePath;
  private JPanel panel1;
  private JButton btnLocateCache;
  private JButton btnCleanCache;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
  @Override

  public JComponent getComponent() {
    return this;
  }

  @Override
  public int getIndex() {
    return 4;
  }
}
