/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.eventlog.task;

import java.util.logging.Level;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.service.rtu.events.RTUEventsAPI;

public class EventLogEraseTask extends Task<Void, Void> {

  private Logger log = Logger.getLogger(EventLogEraseTask.class);
  private RTUEventsAPI commsAPI;
  

  public EventLogEraseTask(Application app, RTUEventsAPI commsAPI) {
    super(app);
    this.commsAPI = commsAPI;
    setUserCanCancel(false);
    setTitle("Event Log Erase");
    setDescription("Send command to RTU to erase all event log");
  }

  @Override
  protected void succeeded(Void result) {
    setMessage("Event logs have been erased");
    JOptionPane.showMessageDialog(WindowUtils.getMainFrame(),
        "The event logs have been erased from RTU", "Success",
        JOptionPane.INFORMATION_MESSAGE);
  }

  @Override
  protected void failed(Throwable cause) {
    log.error("Fail to erase event logs", cause);

    ErrorInfo info = new ErrorInfo("Fail", "Fail to erase event logs.",
        cause.getMessage(), null, null, Level.SEVERE, null);
    JXErrorPane.showDialog(WindowUtils.getMainFrame(), info);
  }

  @Override
  protected Void doInBackground() throws Exception {
    if (commsAPI != null) {
      commsAPI.cmdEraseEvents();
    } else {
      log.error("No command message sent out. logcmd is null");
    }
    return null;
  }
}