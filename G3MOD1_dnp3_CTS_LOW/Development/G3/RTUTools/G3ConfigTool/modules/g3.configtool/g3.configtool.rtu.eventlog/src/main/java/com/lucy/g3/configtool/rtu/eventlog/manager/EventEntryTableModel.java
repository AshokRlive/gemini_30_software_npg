/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.eventlog.manager;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.rtu.comms.service.rtu.events.EventEntry;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;

class EventEntryTableModel extends AbstractTableAdapter<EventEntry> implements
    Transferable {

  private final DataFlavor[] DATA_FLAVORS = { DataFlavor.stringFlavor };

  public static final int COLUMN_INDEX_INDEX = 0;

  public static final int COLUMN_INDEX_DATE = 1;

  public static final int COLUMN_INDEX_EVENT_CLASS = 2;

  public static final int COLUMN_INDEX_EVENT = 3;

  public static final int COLUMN_INDEX_POINTID = 4;

  public static final int COLUMN_INDEX_POINT_VALUE = 5;

  public static final int COLUMN_INDEX_STATUS = 6;

  private static final String[] COLUMN_NAMES = { "Index", "Time",
      "Event Class", "Event", "Point ID", "Value", "Status" };

  // Point value formatter
  private DecimalFormat valueFormat = new DecimalFormat("0.######");

  static final String NAN_VALUE = "NaN";


  public EventEntryTableModel(ListModel listModel) {
    super(listModel, COLUMN_NAMES);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    if (rowIndex >= getRowCount()) {
      return null;
    }

    EventEntry entry = getRow(rowIndex);
    Number value;
    switch (columnIndex) {
    case COLUMN_INDEX_INDEX:
      return entry.getIndex();

    case COLUMN_INDEX_DATE:
      return RTUTimeDecoder.formatEventLog(entry.getTimestamp());

    case COLUMN_INDEX_EVENT_CLASS:
      return entry.getEventClass();

    case COLUMN_INDEX_EVENT:
      return entry.getEventType();

    case COLUMN_INDEX_POINTID:
      return entry.getPointID();

    case COLUMN_INDEX_POINT_VALUE:
      value = entry.getPointValue();
      if (value == null) {
        return null;
      }

      if (Float.isNaN(value.floatValue())) {
        return NAN_VALUE;
      }

      return valueFormat.format(value);

    case COLUMN_INDEX_STATUS:
      return entry.getStatus();

    default:
      throw new IllegalStateException("Unknown column");
    }
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    switch (columnIndex) {
    case COLUMN_INDEX_INDEX:
      return Integer.class;
    default:
      return super.getColumnClass(columnIndex);
    }
  }

  @Override
  public DataFlavor[] getTransferDataFlavors() {
    return DATA_FLAVORS;
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    for (int i = 0; i < DATA_FLAVORS.length; i++) {
      if (flavor == DATA_FLAVORS[i]) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Object getTransferData(DataFlavor flavor)
      throws UnsupportedFlavorException, IOException {
    return getContent();
  }

  public String getContent() {
    StringBuffer buf = new StringBuffer();
    int size = getRowCount();
    for (int i = 0; i < size; i++) {
      EventEntry entry = getRow(i);
      buf.append(entry.getFormattedEventText());
      buf.append("\r\n");
    }
    return buf.toString();
  }
}