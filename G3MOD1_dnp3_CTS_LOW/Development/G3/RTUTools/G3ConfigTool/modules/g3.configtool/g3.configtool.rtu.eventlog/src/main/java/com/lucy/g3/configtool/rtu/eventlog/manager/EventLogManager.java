/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.eventlog.manager;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;

import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.eventlog.task.EventLogEraseTask;
import com.lucy.g3.configtool.rtu.eventlog.task.EventUpdateTask;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.service.rtu.events.EventEntry;
import com.lucy.g3.rtu.comms.service.rtu.events.RTUEventsAPI;

/**
 * A presentation model for managing a list of event entries received from RTU,
 * providing data model and actions for GUI, updating the data model by
 * communicating with RTU.
 */
public class EventLogManager extends AbstractSubsystem implements ClipboardOwner {
  public final static String SUBSYSTEM_ID = "subystem.id.EventLogManager";
  
  public static final String ACTION_KEY_CLEAR = "clearEvents";
  public static final String ACTION_KEY_SAVE = "saveEvents";
  public static final String ACTION_KEY_COPY = "copyEvents";
  public static final String ACTION_KEY_ENABLE_UPDATE = "enableUpdate";

  // Time stamp formatter
  /** Read-only boolean property, indicating if the event log list is empty. */
  public static final String PROPERTY_NOT_EMPTY = "notEmpty";

  /** Boolean property. Indicate if the update action is selected. */
  public static final String PROPERTY_UPDATE_ENABLED = "updateEnabled";

  /** Read-only integer property. The size of alarms in this manager. */
  public static final String PROPERTY_EVENT_COUNT = "eventCount";

  private Logger log = Logger.getLogger(EventLogManager.class);

  private ArrayListModel<EventEntry> entryList;

  private int eventCount;

  private boolean updateEnabled;

  private final EventEntryTableModel entryTableModel;

  private EventUpdateTask updateTask;

  private RTUEventsAPI commsAPI;
  public EventLogManager(IContext context,RTUEventsAPI commsAPI) {
    super(context, SUBSYSTEM_ID);
    this.commsAPI = commsAPI;
    entryList = new ArrayListModel<EventEntry>(500);
    entryTableModel = new EventEntryTableModel(entryList);

    entryList.addListDataListener(new EventEntryListChangeListener());
  }

  @SuppressWarnings("unchecked")
  public ListModel<EventEntry> getEventListModel() {
    return entryList;
  }

  public int getEventCount() {
    return eventCount;
  }

  private void setEventCount(int eventCount) {
    Object oldValue = getEventCount();
    this.eventCount = eventCount;
    firePropertyChange(PROPERTY_EVENT_COUNT, oldValue, eventCount);
  }

  public boolean isUpdateEnabled() {
    return updateEnabled;
  }

  public void setUpdateEnabled(boolean updateEnabled) {
    Object oldValue = isUpdateEnabled();

    this.updateEnabled = updateEnabled;

    firePropertyChange(PROPERTY_UPDATE_ENABLED, oldValue, updateEnabled);

    if (updateEnabled) {
      mayStartUpdateTask();
    } else {
      mayStopUpdateTask();
    }
  }

  private synchronized void mayStartUpdateTask() {
    if (updateEnabled == true && updateTask == null && isNormalMode()) {
      updateTask = new EventUpdateTask(getApp(), commsAPI, entryList);
      getCommsTaskService().execute(updateTask);
      log.info("Start updating event log");
    }
  }

  private synchronized void mayStopUpdateTask() {
    // Stop event updating if user disable it or not running in normal mode.
    if (updateEnabled == false
        || isNormalMode() == false) {
      if (updateTask != null) {
        updateTask.cancel(true);
        updateTask = null;
        log.info("Stop updating event log");
      }
    }
  }

  public TableModel getEventTableModel() {
    return entryTableModel;
  }

  /**
   * Property Getter method for PROPERTY_NOT_EMPTY.
   */
  public boolean isNotEmpty() {
    return entryList.getSize() > 0;
  }

  @Action
  public void clearEvents() {
    if (isNormalMode()) {

      int choice = JOptionPane.showOptionDialog(getMainFrame(),
          "Are you sure that you want to erase the event log on the RTU permanently?",
          "Clear All Events",
          JOptionPane.YES_NO_CANCEL_OPTION,
          JOptionPane.QUESTION_MESSAGE,
          null,
          new String[] { "Save and Erase", "Erase", "Cancel" }, null);

      if (choice == 0) {
        DialogUtils.showSavingDialog(WindowUtils.getMainFrame(), "Event Log", getContent());
      }

      if (choice == 0 || choice == 1) {
        getCommsTaskService().execute(new EventLogEraseTask(getApp(),commsAPI));// Send
        // erase
        // command
        clearLocalEventList();
      }

      if (choice == 0 || choice == 1) {
        clearLocalEventList();
      }

    } else {
      clearLocalEventList();
    }
  }

  private synchronized void clearLocalEventList() {
    if (updateTask == null) {
      entryList.clear();
    } else {
      try {
        updateTask.clearLocalEventList();
      } catch (Exception e) {
        log.error(e);
      }
    }
  }

  @Action(enabledProperty = PROPERTY_NOT_EMPTY)
  public void saveEvents() {
    if (entryList.isEmpty()) {
      JOptionPane.showMessageDialog(getMainFrame(), "No Events");
      return;
    }

    DialogUtils.showSavingDialog(WindowUtils.getMainFrame(), "Event Log", getContent());
  }

  @Action(enabledProperty = PROPERTY_NOT_EMPTY)
  public void copyEvents() {
    if (entryList.isEmpty()) {
      JOptionPane.showMessageDialog(getMainFrame(), "No System Events",
          "Warn", JOptionPane.WARNING_MESSAGE);
      return;
    }

    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(entryTableModel, this);
    JOptionPane.showMessageDialog(getMainFrame(), "Event logs have been copied to Clipboard.",
        "Copy", JOptionPane.DEFAULT_OPTION);
  }

  @Action(selectedProperty = PROPERTY_UPDATE_ENABLED)
  public void enableUpdate() {
    /*
     * Nothing to do cause this action is already binded to the property
     * PROPERTY_UPDATE_ENABLED and it will update the value of that property
     * automatically.
     */
  }

  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {
    // Do nothing
  }

  private String getContent() {
    StringBuffer buf = new StringBuffer();
    int size = entryList.getSize();
    for (int i = 0; i < size; i++) {
      EventEntry entry = entryList.get(i);
      buf.append(entry.getFormattedEventText());
      buf.append("\r\n");
    }
    return buf.toString();
  }


  private class EventEntryListChangeListener implements ListDataListener {

    @Override
    public void intervalAdded(ListDataEvent e) {
      fireEmptyPropertyEvent();
      setEventCount(entryList.getSize());
    }

    @Override
    public void intervalRemoved(ListDataEvent e) {
      fireEmptyPropertyEvent();
      setEventCount(entryList.getSize());
    }

    @Override
    public void contentsChanged(ListDataEvent e) {
    }

    private void fireEmptyPropertyEvent() {
      firePropertyChange(PROPERTY_NOT_EMPTY, null, isNotEmpty());
    }
  }


  @Override
  protected synchronized void notifyStateChanged(ConnectionState oldState,
      ConnectionState newState) {

    if (newState == ConnectionState.DISCONNECTED) {
      if (updateTask != null) {
        // Set flag for reseting event so we can read from the beginning.
        updateTask.setNeedResetEvent(true);
      }

      // Clear events
      clearLocalEventList();
    }

    if (isConnected()) {
      mayStartUpdateTask();
    } else {
      mayStopUpdateTask();
    }
  }
}
