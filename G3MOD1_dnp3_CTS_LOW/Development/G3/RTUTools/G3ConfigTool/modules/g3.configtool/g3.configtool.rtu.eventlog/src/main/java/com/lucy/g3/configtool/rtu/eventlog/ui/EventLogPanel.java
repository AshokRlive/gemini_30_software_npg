/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.eventlog.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.eventlog.manager.EventLogManager;
import com.lucy.g3.gui.common.widgets.ext.swing.button.ButtonHoverEffect;
import com.lucy.g3.help.Helper;

public class EventLogPanel extends JPanel {

  private final EventLogManager eventLogMgr;


  public EventLogPanel(Application app,
      EventLogManager eventLogMgr) {
    this.eventLogMgr = Preconditions.checkNotNull(eventLogMgr, "eventLogMgr is null");

    initComponents();
    customiseComponents();
    filterEventTable();

    Helper.register(this, getClass());
  }

  private void customiseComponents() {
    // Configure EventLogTable columns
    eventListTable.getColumn(0).setPreferredWidth(30);
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    eventListTable.getColumn(0).setCellRenderer(centerRenderer);
  }

  private void filterEventTable() {
    // TODO to be done
  }

  private JButton creatTabButton(Action action) {
    JButton btn = new JButton(action);
    btn.setPreferredSize(new Dimension(25, 25));
    btn.setHideActionText(true);
    ButtonHoverEffect.install(btn);
    return btn;
  }


  private static class AutoScrollListener implements TableModelListener {

    private final JTable table;


    public AutoScrollListener(JTable table) {
      this.table = table;

    }

    @Override
    public void tableChanged(TableModelEvent e) {
      if (table.getRowCount() > 0) {
        showCell(table, 0, 0);
      }
    }

    private void showCell(JTable table, int row, int column) {
      Rectangle rect = table.getCellRect(row, column, true);
      table.scrollRectToVisible(rect);
      // table.clearSelection();
      // table.setRowSelectionInterval(row, row);
      // table.getModel().fireTableDataChanged(); // notify the model
    }
  }


  // private static class EventFilter extends RowFilter<TableModel,Integer>{
  // final private EVENT_CLASS eventClass;
  // final private SUBSYSTEM_ID subSystem;
  // public EventFilter(EVENT_CLASS logLevel, SUBSYSTEM_ID subSystem){
  // this.logLevel = logLevel;
  // this.subSystem = subSystem;
  // }
  // @Override
  // public boolean include(javax.swing.RowFilter.Entry<? extends TableModel, ?
  // extends Integer> entry) {
  // boolean includedBySubsystem = true;
  // boolean includedByLevel = true;
  // int index = entry.getIdentifier().intValue();
  //
  // LOG_LEVEL curLevel = (LOG_LEVEL) entry.getModel().getValueAt(index,
  // SystemLogManageModel.COLUMN_INDEX_LEVEL);
  // SUBSYSTEM_ID curSubsys = (SUBSYSTEM_ID) entry.getModel().getValueAt(index,
  // SystemLogManageModel.COLUMN_INDEX_SUB_SYSTEM);
  //
  // if(subSystem != null){
  // includedBySubsystem = (subSystem == curSubsys);
  // }
  //
  // if(logLevel != null){
  // includedByLevel = curLevel.getValue() <= logLevel.getValue() ;
  // }
  //
  // return includedByLevel && includedBySubsystem;
  // }
  // }

  private void createUIComponents() {

    eventListTable = new JXTable(eventLogMgr.getEventTableModel());
    eventListTable.setSortOrderCycle(SortOrder.ASCENDING, SortOrder.DESCENDING, SortOrder.UNSORTED);
    eventListTable.getModel().addTableModelListener(new AutoScrollListener(eventListTable));

    ApplicationActionMap actions = eventLogMgr.getActionMap();
    btnClearEvent = creatTabButton(actions.get(EventLogManager.ACTION_KEY_CLEAR));
    btnCopyEvent = creatTabButton(actions.get(EventLogManager.ACTION_KEY_COPY));
    btnSaveEvent = creatTabButton(actions.get(EventLogManager.ACTION_KEY_SAVE));
    checkBoxUpdateEvent = new JCheckBox(actions.get(EventLogManager.ACTION_KEY_ENABLE_UPDATE));
    checkBoxUpdateEvent.setSelected(eventLogMgr.isUpdateEnabled());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    JPanel topPanel = new JPanel();
    JLabel lblEventClass = new JLabel();
    comboBoxEventClass = new JComboBox();
    JLabel lblEventType = new JLabel();
    comboBoxEventType = new JComboBox();
    centerPanel = new JScrollPane();

    // ======== this ========
    setLayout(new BorderLayout());

    // ======== topPanel ========
    {
      topPanel.setLayout(new FormLayout(
          "default, $lcgap, default:grow, 2*($lcgap, default), $ugap, 2*(default, $lcgap), 20dlu, 3*($lcgap, default)",
          "fill:default"));
      topPanel.add(checkBoxUpdateEvent, CC.xy(1, 1));

      // ---- lblEventClass ----
      lblEventClass.setText("Event Class");
      lblEventClass.setVisible(false);
      topPanel.add(lblEventClass, CC.xy(5, 1));

      // ---- comboBoxEventClass ----
      comboBoxEventClass.setVisible(false);
      comboBoxEventClass.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          filterEventTable();
        }
      });
      topPanel.add(comboBoxEventClass, CC.xy(7, 1));

      // ---- lblEventType ----
      lblEventType.setText("Event Type");
      lblEventType.setVisible(false);
      topPanel.add(lblEventType, CC.xy(9, 1));

      // ---- comboBoxEventType ----
      comboBoxEventType.setFocusable(false);
      comboBoxEventType.setVisible(false);
      comboBoxEventType.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          filterEventTable();
        }
      });
      topPanel.add(comboBoxEventType, CC.xy(11, 1));
      topPanel.add(btnCopyEvent, CC.xy(15, 1));
      topPanel.add(btnSaveEvent, CC.xy(17, 1));
      topPanel.add(btnClearEvent, CC.xy(19, 1));
    }
    add(topPanel, BorderLayout.NORTH);

    // ======== centerPanel ========
    {
      centerPanel.setBorder(BorderFactory.createEmptyBorder());

      // ---- eventListTable ----
      eventListTable.setForeground(Color.darkGray);
      eventListTable.setBorder(null);
      eventListTable.setEditable(false);
      eventListTable.setShowHorizontalLines(false);
      eventListTable.setShowVerticalLines(false);
      eventListTable.setColumnMargin(0);
      eventListTable.setRolloverEnabled(false);
      eventListTable.setName("eventListTable");
      centerPanel.setViewportView(eventListTable);
    }
    add(centerPanel, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox checkBoxUpdateEvent;
  private JComboBox comboBoxEventClass;
  private JComboBox comboBoxEventType;
  private JButton btnCopyEvent;
  private JButton btnSaveEvent;
  private JButton btnClearEvent;
  private JScrollPane centerPanel;
  private JXTable eventListTable;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
