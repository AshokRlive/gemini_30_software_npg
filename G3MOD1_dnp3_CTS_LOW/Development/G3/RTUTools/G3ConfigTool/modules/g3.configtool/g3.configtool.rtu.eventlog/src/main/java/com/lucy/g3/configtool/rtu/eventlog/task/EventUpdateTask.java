/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.eventlog.task;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.rtu.comms.service.rtu.events.EventEntry;
import com.lucy.g3.rtu.comms.service.rtu.events.RTUEventsAPI;

public class EventUpdateTask extends Task<Void, EventEntry> {

  private static final int EVENT_LIST_MAX_SIZE = 20000;

  private Logger log = Logger.getLogger(EventUpdateTask.class);

  private int eventIndex = 0;

  private boolean needResetEvent = true;

  private int period = 1000; // ms


  private List<EventEntry> entryList;
  private RTUEventsAPI commsAPI;

  public EventUpdateTask(Application application, RTUEventsAPI commsAPI, List<EventEntry> entryList) {
    super(application);
    this.commsAPI = commsAPI;
    this.entryList = Preconditions.checkNotNull(entryList, "entryList is null");

    setTitle("Event Log Update");
    setDescription("Continously reading event log from RTU and updating GUI data model");
  }

  public void clearLocalEventList() throws InvocationTargetException, InterruptedException {
    if (SwingUtilities.isEventDispatchThread()) {
      entryList.clear();
      eventIndex = 0;
    } else {
      SwingUtilities.invokeAndWait(new Runnable() {

        @Override
        public void run() {
          entryList.clear();
          eventIndex = 0;
        }
      });
    }
  }

  @Override
  protected Void doInBackground() {
    while (!isCancelled()) {
      // Reset event pointer's position
      if (needResetEvent) {
        try {
          if (commsAPI != null) {
            commsAPI.cmdResetEvent();
          }

          needResetEvent = false;
          clearLocalEventList();

        } catch (Exception e) {
          log.error("Fail to reset event pointer:" + e.getMessage());
        }
      }

      try {
        Collection<EventEntry> events = commsAPI.cmdGetLatestEvents();
        if (events != null && events.size() > 0)
        {
          updateEventList(new ArrayList<EventEntry>(events));
          // publish(events.toArray(new EventEntry[events.size()]));
        }

      } catch (Throwable e) {
        log.error("Fail to read the latest events:" + e.getMessage());
      }

      // Delay
      try {
        Thread.sleep(period);
      } catch (InterruptedException e) {
      }

    }
    return null;
  }

  public void setNeedResetEvent(boolean needResetEvent) {
    this.needResetEvent = needResetEvent;
  }

  private void updateEventList(List<EventEntry> events) {
    // Event list is full
    int size = entryList.size();
    if (size >= EVENT_LIST_MAX_SIZE && size > events.size()) {
      int trimSize = size - EVENT_LIST_MAX_SIZE + events.size();
      for (int i = 0; i < trimSize && entryList.size() > 0; i++) {
        entryList.remove(entryList.size() - 1);
      }
    }

    // Set the index of events
    for (int i = events.size() - 1; i >= 0; i--) {
      events.get(i).setIndex(eventIndex++);
    }

    entryList.addAll(0, events);
  }

}
