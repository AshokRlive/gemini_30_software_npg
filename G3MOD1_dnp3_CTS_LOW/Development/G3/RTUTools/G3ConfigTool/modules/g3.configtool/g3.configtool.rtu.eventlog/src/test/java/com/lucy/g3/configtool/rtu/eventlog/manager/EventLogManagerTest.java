/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.eventlog.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;

import javax.swing.JCheckBox;

import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiTask;
import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.eventlog.manager.EventLogManager;

public class EventLogManagerTest {

  private EventLogManager fixture;
  
  @Before
  public void before() {
    fixture = new EventLogManager(null,null);
  }
  
  @After
  public void after() {
  }


  @Test
  public void testAction_EnableUpdate() {
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {
        JCheckBox comp = new JCheckBox(fixture
            .getAction(EventLogManager.ACTION_KEY_ENABLE_UPDATE));
        assertEquals(comp.isSelected(), fixture.isUpdateEnabled());

        fixture.setUpdateEnabled(false);
        assertFalse(comp.isSelected());

        fixture.setUpdateEnabled(true);
        assertTrue(comp.isSelected());

        comp.setSelected(false);
        assertFalse(fixture.isUpdateEnabled());

        comp.setSelected(true);
        assertTrue(fixture.isUpdateEnabled());

      }
    });
  }


  @Test
  public void testValueFormatter() {
    DecimalFormat valueFormat = new DecimalFormat("0.######");

    System.out.println(valueFormat.format(new Integer(2)));
    System.out.println(valueFormat.format(new Float(2.123)));
  }
}
