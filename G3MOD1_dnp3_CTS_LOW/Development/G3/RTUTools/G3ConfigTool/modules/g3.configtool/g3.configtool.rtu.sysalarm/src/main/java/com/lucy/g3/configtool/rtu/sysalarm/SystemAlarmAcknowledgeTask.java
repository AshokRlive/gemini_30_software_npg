/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.sysalarm;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfoAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;

class SystemAlarmAcknowledgeTask extends Task<Void, Void> {

  private Logger log = Logger.getLogger(SystemAlarmAcknowledgeTask.class);


  public SystemAlarmAcknowledgeTask(Application app) {
    super(app);
  }

  @Override
  protected Void doInBackground() throws Exception {
    final ModuleInfoAPI moduleCMD = CommsUtil.getModuleInfoAPI(G3RTUFactory.getComms());
    Collection<ModuleInfo> mInfoList = moduleCMD.cmdGetModueInfo();
    for (ModuleInfo mInfo : mInfoList) {
      moduleCMD.cmdAckModueAlarm(mInfo.getModuleType(), mInfo.getModuleID());
    }
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    log.error("Failed to acknowledge alarms", cause);
  }

  @Override
  protected void finished() {
    log.info("Succeeded to acknowledge alarms");
  }
}
