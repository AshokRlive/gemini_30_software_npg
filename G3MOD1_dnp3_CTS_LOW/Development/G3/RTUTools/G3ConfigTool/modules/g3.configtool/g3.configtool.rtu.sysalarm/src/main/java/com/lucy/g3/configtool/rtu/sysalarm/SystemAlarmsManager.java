/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.sysalarm;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.util.Collection;
import java.util.concurrent.Callable;

import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.SwingXUtilities;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.xml.gen.api.IXmlEnum;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SUBSYSTEM;

/**
 * A presentation model for managing a list of alarms received from RTU,
 * providing data model and actions for GUI, updating the data model by
 * communicating with RTU.
 */
public class SystemAlarmsManager extends AbstractSubsystem implements ClipboardOwner {
  public final static String SUBSYSTEM_ID = "subystem.id.SystemAlarmsManager";

  public static final String ACTION_KEY_CLEAR = "clearAlarms";
  public static final String ACTION_KEY_SAVE = "saveAlarms";
  public static final String ACTION_KEY_COPY = "copyAlarms";
  public static final String ACTION_KEY_ENABLE_UPDATE = "enableUpdate";
  public static final String ACTION_KEY_REFRESH = "refresh";

  /** Read-only boolean property. Indicate if the event log list is empty. */
  public static final String PROPERTY_NOT_EMPTY = "notEmpty";

  /** Boolean property. Indicate if the update action is selected. */
  public static final String PROPERTY_UPDATE_ENABLED = "updateEnabled";

  /** Read-only integer property. The size of alarms in this manager. */
  public static final String PROPERTY_ALARMS_COUNT = "alarmsCount";

  /** Read-only property:{@value} . type: {@linkplain SYS_ALARM_SEVERITY} */
  public static final String PROPERTY_ALARM_STATUS = "alarmStatus";

  private Logger log = Logger.getLogger(SystemAlarmsManager.class);

  private final ModuleAlarmsTableModel alarmsTableModel;
  private final ArrayListModel<ModuleAlarm> alarmlist;

  /** The task for updating the alarm list. */
  private Task<?,?> updateTask;
  private Task<?,?> ackTask;
  private Task<?,?> refreshTask;

  private boolean updateEnabled = true;

  private int alarmsCount;
  private SYS_ALARM_SEVERITY alarmStatus;


  public SystemAlarmsManager(IContext context) {
    super(context, SUBSYSTEM_ID);

    this.alarmlist = new ArrayListModel<ModuleAlarm>(100);

    @SuppressWarnings("unchecked")
    ListModel<ModuleAlarm> lm = alarmlist;
    this.alarmsTableModel = new ModuleAlarmsTableModel(lm);
  }

  public SYS_ALARM_SEVERITY getAlarmStatus() {
    return alarmStatus;
  }

  private void setAlarmStatus(SYS_ALARM_SEVERITY alarmStatus) {
    Object oldValue = this.alarmStatus;
    this.alarmStatus = alarmStatus;
    firePropertyChange(PROPERTY_ALARM_STATUS, oldValue, alarmStatus);
  }


  public boolean isUpdateEnabled() {
    return updateEnabled;
  }

  /**
   * Setter method for the property: {@linkplain #PROPERTY_UPDATE_ENABLED}.
   */
  public void setUpdateEnabled(boolean updateEnabled) {
    Object oldValue = isUpdateEnabled();

    this.updateEnabled = updateEnabled;

    firePropertyChange(PROPERTY_UPDATE_ENABLED, oldValue, updateEnabled);
    if (updateEnabled) {
      mayStartUpdateTask();
    } else {
      mayStopUpdateTask();
    }
    
    updateAlarmCount();
  }

  private void mayStartUpdateTask() {
    if (updateEnabled == true && updateTask == null && isNormalMode()) {
      updateTask = new SystemAlarmUpdateTask(getApp(), this, true);
      getCommsTaskService().execute(updateTask);
      log.info("Start updating alarm log");
    }
  }

  private void mayStopUpdateTask() {
    // if user disable it or not running in normal mode
    if (updateEnabled == false
        || isNormalMode() == false) {
      if (updateTask != null) {
        updateTask.cancel(true);
        updateTask = null;
        log.info("Stop updating system alarms");
      }
    }
  }

  public TableModel getAlarmsTableModel() {
    return alarmsTableModel;
  }

  /**
   * Getter method for the property: PROPERTY_NOT_EMPTY.
   */
  public boolean isNotEmpty() {
    return !alarmlist.isEmpty();
  }

  public int getAlarmsCount() {
    return alarmsCount;
  }

  private void clearLocalAlarms() {
    assert SwingUtilities.isEventDispatchThread();

    alarmlist.clear();
    updateAlarmStatus();
    updateAlarmCount();
  }

  private void setAlamrsCount(int newValue) {
    Object oldValue = getAlarmsCount();
    this.alarmsCount = newValue;
    firePropertyChange(PROPERTY_ALARMS_COUNT, oldValue, newValue);
  }

  /**
   * Acknowledges all alarms if connected, otherwise clear local alarms.
   */
  @Action(enabledProperty = PROPERTY_NOT_EMPTY)
  public void clearAlarms() {
    if(isConnected()) {
      if (ackTask == null || ackTask.isDone()) {
        ackTask = new SystemAlarmAcknowledgeTask(getApp());
        getCommsTaskService().execute(ackTask);
      } else {
        log.warn("Alarm acknowledge task is already running.");
      }
    } 
  }

  @Action(enabledProperty = PROPERTY_NOT_EMPTY)
  public void saveAlarms() {
    if (alarmlist.isEmpty()) {
      return;
    }

    DialogUtils.showSavingDialog(WindowUtils.getMainFrame(), "System Alarms", alarmsTableModel.getContent());
  }

  @Action(enabledProperty = PROPERTY_NOT_EMPTY)
  public void copyAlarms() {
    if (alarmlist.isEmpty()) {
      return;
    }

    alarmsTableModel.copytoClipboard();
    JOptionPane.showMessageDialog(getMainFrame(),
        "System Alarms have been copied to Clipboard.",
        "Copy", JOptionPane.DEFAULT_OPTION);
  }

  @Action(selectedProperty = PROPERTY_UPDATE_ENABLED)
  public void enableUpdate() {
    /*
     * Nothing to do cause this action is already bound to the property
     * PROPERTY_UPDATE_ENABLED and it will update the value of that property
     * automatically.
     */
  }
  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void refresh() {

    if(isConnected()) {
      if (refreshTask == null || refreshTask.isDone()) {
        clearLocalAlarms();
        refreshTask = new SystemAlarmUpdateTask(getApp(), this, false);
        getCommsTaskService().execute(refreshTask);
      } else {
        log.warn("Alarm acknowledge task is already running.");
      }
    } 
  }  

  public boolean checkAlarmRaised(SYS_ALARM_SUBSYSTEM subsystem, IXmlEnum alarmEnum) {
    for (ModuleAlarm alarm : alarmlist) {
      if(alarm.getSubSystemEnum() == subsystem && alarm.getAlarmCodeEnum() == alarmEnum) {
        return true;
      }
    }
    
    return false;
  }
  
  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {
    // Do nothing
  }

  void updateAlarmStatus() {
    setAlarmStatus(getSeverity(alarmlist));
  }

  void updateAlarmCount() {
    firePropertyChange(PROPERTY_NOT_EMPTY, null, isNotEmpty());
    setAlamrsCount(alarmlist.getSize());
  }

  ArrayListModel<ModuleAlarm> getAlarmListModel() {
    return alarmlist;
  }
  
  static SYS_ALARM_SEVERITY getSeverity(Collection<ModuleAlarm> alarms) {
    SYS_ALARM_SEVERITY severity = null;
    SYS_ALARM_SEVERITY temp;

    for (ModuleAlarm alarm : alarms) {
      temp = alarm.getSeverity();
      if (temp != null && alarm.isActive()) {
        if (severity == null) {
          severity = temp;
        } else if (temp.getValue() < severity.getValue()) {
          severity = temp;
        }
      }
    }
    return severity;
  }


  @Override
  protected void notifyStateChanged(ConnectionState oldState,
      ConnectionState newState) {

    if (isConnected()) {
      mayStartUpdateTask();
    } else {
      mayStopUpdateTask();
      
      if (SwingUtilities.isEventDispatchThread()) {
        clearLocalAlarms();
      } else {
        // Clear all alarms when disconnected
        SwingXUtilities.invokeLater(new Callable<Void>() {

          @Override
          public Void call() throws Exception {
            clearLocalAlarms();
            return null;
          }
        });
      }
    }
  }
}
