/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.sysalarm;

import static org.junit.Assert.assertEquals;

import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import com.lucy.g3.configtool.rtu.sysalarm.SystemAlarmsManager;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.xml.gen.SysAlarm.SysAlarmMCMEnum.SYSALC_MCM;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_STATE;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SUBSYSTEM;

public class SystemAlarmsManagerTest {
  private SystemAlarmsManager fixture;

  @Before
  public void setUp() throws Exception {
    fixture = new SystemAlarmsManager(null);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCheckAlarmRaised() {
    assertEquals(0, fixture.getAlarmsCount());
    
    SYS_ALARM_SUBSYSTEM subsystem = SYS_ALARM_SUBSYSTEM.SYS_ALARM_SUBSYSTEM_MCM;
    SYSALC_MCM alarm = SYSALC_MCM.SYSALC_MCM_MODULE_SERIAL;
    
    assertFalse(fixture.checkAlarmRaised(subsystem, alarm));
    
    ModuleAlarm alarmObj = new ModuleAlarm(MODULE.MODULE_DSM, MODULE_ID.MODULE_ID_0, SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_ERROR,
        (short)SYS_ALARM_STATE.SYS_ALARM_STATE_ACTIVE.getValue(), (short)SYS_ALARM_SUBSYSTEM.SYS_ALARM_SUBSYSTEM_MCM.getValue(), 
        SYSALC_MCM.SYSALC_MCM_MODULE_SERIAL.getValue(), 0);
    fixture.getAlarmListModel().add(alarmObj);
    
    assertTrue(fixture.checkAlarmRaised(subsystem, alarm));
  }

}
