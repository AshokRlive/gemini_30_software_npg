/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.sysalarm;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import javax.swing.ListModel;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.rtu.config.module.res.ModuleResource;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;

public class ModuleAlarmsTableModel extends AbstractTableAdapter<ModuleAlarm> implements ClipboardOwner {

  // Column Names
  static final String[] COLUMN_NAMES = { "Module", "Severity", "SubSystem", "Alarm Code", "State", "Param" };

  // Column Index
  public static final int COLUMN_MODULE = 0;
  public static final int COLUMN_SEVERTIY = 1;
  public static final int COLUMN_SUBSYSTEM = 2;
  public static final int COLUMN_ALARM = 3;
  public static final int COLUMN_STATE = 4;
  public static final int COLUMN_PARAMTER = 5;

  // Cell colour constants
  private static final Color COLOR_CRITICAL = new Color(255, 100, 100);
  private static final Color COLOR_ERROR = Color.pink;
  private static final Color COLOR_WARN = new Color(255, 255, 128);
  private static final Color COLOR_INFO = Color.white;


  public ModuleAlarmsTableModel(ListModel<ModuleAlarm> listModel) {
    super(listModel, COLUMN_NAMES);
  }

  @Override
  public int getColumnCount() {
    return COLUMN_NAMES.length;
  }

  @Override
  public String getColumnName(int column) {
    return COLUMN_NAMES[column];
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    ModuleAlarm alarm = getRow(rowIndex);

    if (alarm != null) {
      switch (columnIndex) {
      case COLUMN_MODULE:
        return ModuleResource.INSTANCE.getModuleShortName(alarm.getType(), alarm.getId());
      case COLUMN_SEVERTIY:
        return alarm.getSeverity();
      case COLUMN_SUBSYSTEM:
        return alarm.getSubSystem();
      case COLUMN_ALARM:
        return alarm.getAlarmMessage();
      case COLUMN_STATE:
        return alarm.getState();
      case COLUMN_PARAMTER:
        return alarm.getParameter();
      default:
        break;
      }
    }
    return null;
  }

  public String getTooltip(int rowIndex, int columnIndex) {
    ModuleAlarm alarm = getRow(rowIndex);

    if (alarm != null) {
      switch (columnIndex) {
      case COLUMN_MODULE:
        return ModuleResource.INSTANCE.getModuleShortName(alarm.getType(), alarm.getId());
      case COLUMN_SEVERTIY:
        return String.valueOf(alarm.getSeverity());
      case COLUMN_SUBSYSTEM:
        return alarm.getSubSystem();
      case COLUMN_ALARM:
        return String.format("%s [Alarm Code: %s]",
            alarm.getAlarmMessage(), alarm.getAlarmCode());
      case COLUMN_STATE:
        return alarm.getState();
      case COLUMN_PARAMTER:
        return String.valueOf(alarm.getParameter());
      default:
        break;
      }
    }
    return null;
  }

  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {
    // do nothing
  }

  public void copytoClipboard() {
    StringSelection stringSelection = new StringSelection(getContent());
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(stringSelection, this);
  }

  public String getContent() {
    StringBuffer buf = new StringBuffer();

    // Add column header
    buf.append(String.format(ModuleAlarm.ALARM_PRINT_FORMAT,
        "[Severity]", "[Sub System]", "[Alarm]", "[State]\r\n"));
    for (int i = 0; i < getRowCount(); i++) {
      buf.append(getRow(i).getPrintText());
      buf.append("\r\n");
    }

    return buf.toString();
  }

  public Color getAlarmEntryBG(int rowIndex) {
    ModuleAlarm alarm = getRow(rowIndex);
    boolean active = alarm.isActive();
    
    /*Inactive background*/
    if (active == false) {
      return Color.white;
    }

    SYS_ALARM_SEVERITY severity = alarm.getSeverity();
    switch (severity) {
    case SYS_ALARM_SEVERITY_CRITICAL:
      return COLOR_CRITICAL;
    case SYS_ALARM_SEVERITY_ERROR:
      return COLOR_ERROR;
    case SYS_ALARM_SEVERITY_INFO:
      return COLOR_INFO;
    case SYS_ALARM_SEVERITY_WARNING:
      return COLOR_WARN;
    default:
      return Color.white;
    }
  }

  public Color getAlarmEntryFG(int rowIndex) {
    ModuleAlarm alarm = getRow(rowIndex);
    boolean active = alarm.isActive();
    
    /*Inactive foreground*/
    if (active == false) {
      return Color.gray;
    }

    SYS_ALARM_SEVERITY severity = alarm.getSeverity();
    if (severity == SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_CRITICAL) {
      return Color.white;
    } else {
      return Color.black;
    }
  }

}
