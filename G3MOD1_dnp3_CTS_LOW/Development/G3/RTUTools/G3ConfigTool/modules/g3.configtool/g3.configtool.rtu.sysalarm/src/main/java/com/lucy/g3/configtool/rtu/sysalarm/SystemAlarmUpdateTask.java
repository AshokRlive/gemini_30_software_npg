/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/
package com.lucy.g3.configtool.rtu.sysalarm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.SwingXUtilities;

import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleAlarm;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfo;
import com.lucy.g3.rtu.comms.service.realtime.modules.ModuleInfoAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;

class SystemAlarmUpdateTask extends Task<Void, Void> {

    private Logger log = Logger.getLogger(SystemAlarmUpdateTask.class);

    private final ArrayList<ModuleAlarm> alarmsCache = new ArrayList<ModuleAlarm>();

    /** The updating period. */
    private final int period = 1000 * 5; // 5 seconds
    
    private final boolean isPeriodic;
    
    private final SystemAlarmsManager manager;

    private ArrayListModel<ModuleAlarm> alarmlist;

    public SystemAlarmUpdateTask(Application app, SystemAlarmsManager manager, boolean isPeriodic) {
      super(app);
      this.manager = manager;
      this.isPeriodic = isPeriodic;
      this.alarmlist = manager.getAlarmListModel();
      
      setTitle("System Alarms Update");
      setDescription("Getting all modules' alarms");
    }

    @Override
    protected Void doInBackground() throws Exception {
      final ModuleInfoAPI moduleCMD = CommsUtil.getModuleInfoAPI(G3RTUFactory.getComms());

      do {
        // Getting module list
        Collection<ModuleInfo> mInfoList = moduleCMD.cmdGetModueInfo();

        // Read alarms from all module
        alarmsCache.clear();
        for (ModuleInfo mInfo : mInfoList) {
          if (!isCancelled()) {
            try {
              final ModuleAlarm[] alarms = moduleCMD.cmdGetModueAlarm(mInfo.getModuleType(), mInfo.getModuleID());
              if (alarms != null && !isCancelled()) {
                alarmsCache.addAll(Arrays.asList(alarms));
                
                // Update GUI
                if (!isCancelled()) {
                  SwingXUtilities.invokeLater(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        updateAlarmList(alarmlist, alarms);
                      return null;
                    }
                  });
                }
                log.debug(String.format("Alarm information has been read from: %s", mInfo));
              }
            } catch (Exception e) {
              log.warn("Fail to get alarms from " + mInfo + ":" + e.getMessage());
            }
          }
        }

        /* Clean alarm list*/
        SwingXUtilities.invokeLater(new Callable<Void>() {
          @Override
          public Void call() throws Exception {
            if (!isCancelled()) {
              cleanAlarmList();
            }
            manager.updateAlarmStatus();
            return null;
          }
        });

        // Updating delay
        if (isPeriodic) {
          try {
            Thread.sleep(period);
          } catch (InterruptedException e) {
          }
        }
        
      } while (!isCancelled() && isPeriodic);

      return null;
    }

    /* Remove local alarm that are not updated */
    private void cleanAlarmList() throws Exception {
      assert SwingUtilities.isEventDispatchThread();
      
      final ArrayList<ModuleAlarm> outOfDateAlarms = new ArrayList<ModuleAlarm>(alarmlist);
      outOfDateAlarms.removeAll(alarmsCache);
      if (!outOfDateAlarms.isEmpty()) {
        alarmlist.removeAll(outOfDateAlarms);
      }
      
      manager.updateAlarmCount();
    }

    private void updateAlarmList(ArrayListModel<ModuleAlarm> localList, ModuleAlarm[] incomingAlarms) {
      assert SwingUtilities.isEventDispatchThread();
      
      if(incomingAlarms == null)
        return;
      
      for (int j = 0; j < incomingAlarms.length; j++) {
        int index = localList.indexOf(incomingAlarms[j]);

        if (index >= 0) {
          /* Update existing alarm content */
          localList.set(index, incomingAlarms[j]);
          localList.fireContentsChanged(index);
        } else {
          /* Add new alarm */
          localList.add(0, incomingAlarms[j]);
          manager.updateAlarmCount();
        }
      }
    }

    @Override
    protected void failed(Throwable cause) {
      log.error("Fail to update system alarms cause: " + cause.getMessage());
    }
  }