/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;
import org.zeroturnaround.zip.ZipUtil;

import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.config.xml.serialization.XmlParserInvoker;
import com.lucy.g3.rtu.config.xml.validation.XmlValidator;
import com.lucy.g3.schema.support.RTUConfigSchemaResource;
import com.lucy.g3.sdp.importer.SdpImporter;
//import com.lucy.g3.sdp.model.ZipSDPModel;
import com.lucy.g3.sdp.model.SDP;

/**
 * A task is for importing configuration from a selected XML file in file
 * chooser dialogue.
 */
public class ConfigFileImportTask extends Task<IConfig, Void> implements XmlParserInvoker {

  private Logger log = Logger.getLogger(ConfigFileImportTask.class);

  private final JFrame parent;
  
  private final Listener listener ;
  
  private InputStream[] streams;
  
  public ConfigFileImportTask(Application app, JFrame parent) {
    super(app);
    this.parent = parent;
    this.listener = new Listener(parent);
    addTaskListener(listener);
    
    setUserCanCancel(true);
    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, null, null));
  }

  @Override
  protected void succeeded(IConfig result) {
    super.succeeded(result);
    setMessage("Configuration imported");
  }

  @Override
  protected void finished() {
    if(streams != null) {
      for (int i = 0; i < streams.length; i++) {
        try {
          streams[i].close();
        } catch (IOException e) {
          log.error("Failed to close stream at index:"+i);
        }
      }
    }
  }

  @Override
  protected void failed(Throwable cause) {
    String msg = "Failed to import configuration file";
    setMessage(msg);
    log.error(msg, cause);

    // Show error dialog
    ErrorInfo info = new ErrorInfo("Failure", msg, cause.getMessage(),
        null, cause, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
  }

  @Override
  protected IConfig doInBackground() throws Exception {
    listener.setIncompatibleConfigLoaded(false);
    
    /*
     * Appropriate delay to give the block window displayed before FileChoose is
     * showed, otherwise the fileChooser dialog maybe overlapped by the
     * blockDialog.
     */
    Thread.sleep(500);

    /*
     * Show file chooser to choose a configuration file to be imported
     */
    File userSelectedFile = DialogUtils.showFileChooseDialog(parent,
        "Open Configuration File", "Open", G3Files.FC_FILTER_G3CFG);

    /*
     * Cancel if no file has been chosen
     */
    if (userSelectedFile == null) {
      cancel(true);
      return null;
    }

    /*
     * Parse selected file
     */
    InputStream inputStream = getInputStream(userSelectedFile);
    if(inputStream == null) {
      cancel(true);
      return null;
    }
    
    
    /* Validating selected file*/
    streams = cloneAndClose(inputStream);
    
    try {
      setMessage("Validating against schema...");
      /*Note: we must clone the stream since validator will close the stream during validation.*/
      new XmlValidator(RTUConfigSchemaResource.FILE_PATH).validate(streams[0]);
      
    } catch (Exception e) {
      log.error("G3 configuration validation failed", e);
      if(XmlValidator.showConfirmDialog(parent, e) == false) {
        cancel(true);
      } else{
        listener.setIncompatibleConfigLoaded(true);
       }
    } 

    if(isCancelled()) {
      return null;
    }
    
    /*
     * Parse the configuration file
     */
    IConfig data = null;
    if (!isCancelled()) {
      data = new XmlDataReader(this).readFromStream(streams[1]);
      GeneralConfigUtility.setFilePath(data, userSelectedFile.getAbsolutePath());
    }
    
  
    return data;
  }
  
  private InputStream[] cloneAndClose(InputStream input) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    // Fake code simulating the copy
    // You can generally do better with nio if you need...
    // And please, unlike me, do something about the Exceptions :D
    byte[] buffer = new byte[1024];
    int len;
    while ((len = input.read(buffer)) > -1) {
      baos.write(buffer, 0, len);
    }
    baos.flush();
    
    input.close();
    
    // Open new InputStreams using the recorded bytes
    // Can be repeated as many times as you wish
    return new InputStream[]{
        new ByteArrayInputStream(baos.toByteArray()),
        new ByteArrayInputStream(baos.toByteArray())};
  }

  private InputStream getInputStream(File userSelectedFile) throws IOException,
      Exception, InterruptedException, InvocationTargetException, MalformedURLException {
    if(userSelectedFile == null)
      return null;
    
    InputStream configInputStream = null;
    
    if(userSelectedFile.getPath().endsWith(SDP.SDP_EXTENSION)) {
      /* Selected file is SDP*/
      String configFilePath = chooseConfigFromSDP(userSelectedFile);
      if(configFilePath != null) {
        configInputStream = new ByteArrayInputStream(
            ZipUtil.unpackEntry(userSelectedFile, configFilePath));
      }
      
    } else if(com.lucy.g3.common.utils.ZipUtil.isGZipFile(userSelectedFile.getName())) {
      /* Selected file is a zipped config file*/
      configInputStream = new ByteArrayInputStream(
          com.lucy.g3.common.utils.ZipUtil.ungzip(userSelectedFile.getPath()));
      
    } else {
      /* Selected file is a xml config file*/
      configInputStream = userSelectedFile.toURI().toURL().openStream();
    }
    
    return configInputStream;
  }


  private String chooseConfigFromSDP(final File sdp) throws IOException, Exception, InterruptedException, InvocationTargetException {
    
    final String[] fileNames = SdpImporter.getConfigFileNames(sdp);
    
    if(fileNames == null || fileNames.length == 0) {
      SwingUtilities.invokeAndWait(new Runnable() {
        
        @Override
        public void run() {
          JOptionPane.showMessageDialog(parent, "No configuration file found in:\n"+sdp, "Error", JOptionPane.ERROR_MESSAGE);
          cancel(true);
        }
      });
      
      return null;
      
    } else {
      ConfigChooseRunner runner = new ConfigChooseRunner(parent, fileNames);
      SwingUtilities.invokeAndWait(runner);
      return runner.selected;
    }
    
  }

  
  private static class  ConfigChooseRunner implements Runnable {
    private String selected;
    private final String[] fileNames;
    private final JFrame parent;
    public ConfigChooseRunner(JFrame parent, String[] fileNames) {
      this.fileNames = fileNames;
      this.parent = parent;
    }
    
    @Override
    public void run() {
      selected = (String) JOptionPane.showInputDialog(parent,
          "Select a configuration file to load:", 
          "Select Configuration", 
          JOptionPane.PLAIN_MESSAGE, 
          null,  
          fileNames,
          fileNames!= null && fileNames.length > 0 ? fileNames[0] : null);
    }
  
  }

  @Override
  public void printMessage(String message) {
    setMessage(message);
    Logger.getLogger(getClass()).info(message);
  }

  @Override
  public void setProgressValue(int progress) {
    if (progress >= 0 && progress <= 100) {
      setProgress(progress);
    }
  }
  
  
  private static class Listener extends TaskListener.Adapter<IConfig, Void>{
    private boolean incompatibleConfigLoaded;
    
    final private JFrame parent;
    
    public Listener(JFrame parent) {
      super();
      this.parent = parent;
    }
    
    public void setIncompatibleConfigLoaded(boolean incompatibleConfigLoaded) {
      this.incompatibleConfigLoaded = incompatibleConfigLoaded;
    }

    @Override
    public void succeeded(TaskEvent<IConfig> event) {
      if(incompatibleConfigLoaded) {
          JOptionPane.showMessageDialog(
              parent,
              "An incompatible configuration has been loaded and converted successfully.\n "
                  + "Please review the configuration before writing it to the RTU.",
                  "Warning", JOptionPane.WARNING_MESSAGE);
          }
    }
    
  }

}
