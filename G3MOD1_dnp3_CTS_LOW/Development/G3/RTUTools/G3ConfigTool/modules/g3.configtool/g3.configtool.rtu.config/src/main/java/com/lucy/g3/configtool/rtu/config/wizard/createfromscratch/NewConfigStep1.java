/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.prompt.PromptSupport;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.widgets.ext.swing.formatter.RegexFormatter;
import com.lucy.g3.rtu.config.general.GeneralConfig;

/**
 * Wizard page for configuring site name and config version.
 */
class NewConfigStep1 extends WizardPage {

  public static final String KEY_NAME_SITE_NAME = "siteName";
  public static final String KEY_NAME_CONFIG_VERSION = "configVersion";
  public static final String KEY_NAME_CONFIG_DESP = "configDescription";
  public static final String KEY_NAME_CREATE_FILE = "doCreateFile";
  public static final String KEY_NAME_FILE_LOCATION = "configFileLocation";


  public NewConfigStep1() {

    initComponents();
    initComponentNames();
    initComponentsValue();
    updateEnablement();
    tfSiteName.selectAll();
  }

  public static String getDescription() {
    return "Basic";
  }

  private void initComponentNames() {
    tfDescription.setName(KEY_NAME_CONFIG_DESP);
    ftfVersion.setName(KEY_NAME_CONFIG_VERSION);
    tfFileLocation.setName(KEY_NAME_FILE_LOCATION);
    tfSiteName.setName(KEY_NAME_SITE_NAME);
    checkBoxCreateFile.setName(KEY_NAME_CREATE_FILE);
  }

  private void initComponentsValue() {
    ftfVersion.setValue("1.0.0");
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;

    if (tfSiteName.getText().matches(GeneralConfig.RTU_NAME_PATTERN) == false) {
      result = "Please type a valid site name";
      ValidationComponentUtils.setErrorBackground(tfSiteName);
    } else {
      ValidationComponentUtils.setMandatoryBackground(tfSiteName);
    }

    String parent = new File(tfFileLocation.getText()).getParent();
    // validate location if checkbox selected
    if (checkBoxCreateFile.isSelected()
        && (parent == null || !new File(parent).exists())) {
      result = "";// "Invalid file location";
      ValidationComponentUtils.setErrorBackground(tfFileLocation);
    } else {
      ValidationComponentUtils.setMandatoryBackground(tfFileLocation);
    }

    return result;
  }

  private void updateEnablement() {
    boolean createFile = checkBoxCreateFile.isSelected();
    tfFileLocation.setEnabled(createFile);
    btnBrowse.setEnabled(createFile);
    labelFileLocation.setEnabled(createFile);
  }

  private void checkBoxCreateFileStateChanged(ChangeEvent e) {
    updateEnablement();
  }

  private void btnBrowseActionPerformed() {
    File file = DialogUtils.showFileChooseDialog(this,
        "Export Configuration File",
        "Save",
        G3Files.FC_FILTER_G3CFG_GROUP);

    if (file != null) {
      tfFileLocation.setText(file.getPath());
    }
  }

  private void createUIComponents() {
    ftfVersion = new JFormattedTextField(new RegexFormatter(GeneralConfig.CONFIG_VERSION_PATTERN));
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    label1 = new JLabel();
    tfSiteName = new JTextField();
    label4 = new JLabel();
    label2 = new JLabel();
    tfDescription = new JTextField();
    checkBoxCreateFile = new JCheckBox();
    labelFileLocation = new JLabel();
    tfFileLocation = new JTextField();
    btnBrowse = new JButton();

    // ======== this ========
    setLayout(new FormLayout(
        "right:default, $lcgap, [50dlu,default]:grow, $lcgap, 15dlu",
        "default, $ugap, default, $lgap, default, $rgap, 3dlu:grow, default, $rgap, default, $lgap, default:grow, $lgap, default"));

    // ---- label1 ----
    label1.setText("RTU Site Name:");
    add(label1, CC.xy(1, 1));

    // ---- tfSiteName ----
    tfSiteName.setText("Gemini3 RTU");
    add(tfSiteName, CC.xy(3, 1));

    // ---- label4 ----
    label4.setText("Version:");
    add(label4, CC.xy(1, 3));
    add(ftfVersion, CC.xy(3, 3));

    // ---- label2 ----
    label2.setText(" Description:");
    add(label2, CC.xy(1, 5));
    add(tfDescription, CC.xy(3, 5));

    // ---- checkBoxCreateFile ----
    checkBoxCreateFile.setText("Save configuration to XML file");
    checkBoxCreateFile.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxCreateFileStateChanged(e);
      }
    });
    add(checkBoxCreateFile, CC.xywh(1, 8, 3, 1));

    // ---- labelFileLocation ----
    labelFileLocation.setText("File Location:");
    add(labelFileLocation, CC.xy(1, 10));
    add(tfFileLocation, CC.xy(3, 10));

    // ---- btnBrowse ----
    btnBrowse.setText("...");
    btnBrowse.setName("chooseBtn");
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnBrowseActionPerformed();
      }
    });
    add(btnBrowse, CC.xy(5, 10));
    // //GEN-END:initComponents

    // ================== Custom Code ===================
    PromptSupport.setPrompt("Config version number. e.g. \"1.0.0\"", ftfVersion);
    PromptSupport.setPrompt("Config description (optional)", tfDescription);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JLabel label1;
  private JTextField tfSiteName;
  private JLabel label4;
  private JFormattedTextField ftfVersion;
  private JLabel label2;
  private JTextField tfDescription;
  private JCheckBox checkBoxCreateFile;
  private JLabel labelFileLocation;
  private JTextField tfFileLocation;
  private JButton btnBrowse;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
