/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromfile;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.configtool.rtu.config.wizard.createfromscratch.NewConfigFromScratchWizard;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;

/**
 * First step of wizard. 
 */
public class LoadConfigStep1 extends WizardPage {

  static final String KEY_NAME_FILE_PATH = "filepath";


  public LoadConfigStep1() {
    initComponents();
    initComponentNames();
  }

  /* Required by wizard library */
  public static String getDescription() {
    return "Choose file";
  }

  private void btnBrowseActionPerformed(ActionEvent e) {
    File file = DialogUtils.showFileChooseDialog(this,
        "Choose Configuration File", "Open",
        G3Files.FC_FILTER_G3CFG);

    if (file != null) {
      String configFilePath = file.getPath();
      tfFilePath.setText(configFilePath);
    }
  }

  private String getFilePath() {
    return tfFilePath.getText();
  }


  @Override
  public WizardPanelNavResult allowFinish(String stepName, @SuppressWarnings("rawtypes") Map settings,
      Wizard wizard) {
    return new ConfigFileParser(new File(getFilePath()));
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;

    String filepath = getFilePath();
    if (filepath == null || filepath.isEmpty()) {
      result = "";// "Please choose a configuration file";

    } else {
      File file = new File(filepath);
      if (file.exists() == false) {
        result = "";// "The template file:\"" + file.getName() +
        // "\" doesn't exist!";
      }
    }

    if (result == null) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE_OR_FINISH);
      ValidationComponentUtils.setMandatoryBackground(tfFilePath);
    } else {
      ValidationComponentUtils.setErrorBackground(tfFilePath);
    }

    return result;
  }

  /**
   * Give a name to the component so its value can be accessed when producing
   * wizard result.
   *
   * @see NewConfigFromScratchWizard.NewConfigResult
   */
  private void initComponentNames() {
    tfFilePath.setName(KEY_NAME_FILE_PATH);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    JLabel label1 = new JLabel();
    tfFilePath = new JTextField();
    btnBrowse = new JButton();

    // ======== this ========
    setPreferredSize(new Dimension(350, 270));
    setLayout(new FormLayout(
        "default, $lcgap, [default,100dlu]:grow, $lcgap, 15dlu",
        "default"));

    // ---- label1 ----
    label1.setText("Configuration File:");
    add(label1, CC.xy(1, 1));
    add(tfFilePath, CC.xy(3, 1));

    // ---- btnBrowse ----
    btnBrowse.setText("Browse...");
    btnBrowse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        btnBrowseActionPerformed(e);
      }
    });
    add(btnBrowse, CC.xy(5, 1));
    // //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JTextField tfFilePath;
  private JButton btnBrowse;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
