/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.impl.ConnectionPreference;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.client.G3ConfigAPI;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.PortsUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.validation.impl.IConfigValidator;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataWriter;
import com.lucy.g3.rtu.config.xml.serialization.XmlParserInvoker;
import com.lucy.g3.rtu.config.xml.validation.XmlValidator;
import com.lucy.g3.rtu.filetransfer.FileTransferService;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.schema.support.RTUConfigSchemaResource;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * A task for exporting configData to a file and and writing it to RTU device.
 */
public class ConfigFileWriteTask extends Task<IConfig, Void> implements XmlParserInvoker {

  private Logger log = Logger.getLogger(ConfigFileWriteTask.class);

  private final CTH_TRANSFERTYPE type = G3Protocol.CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_G3CONFIG;

  private final JFrame parent;

  private final IConfig data;

  private final boolean compressed = true;

  private final boolean requireActivate;
  
  public ConfigFileWriteTask(Application app, IConfig data, 
      boolean requireActivate) {
    super(app);
    this.requireActivate = requireActivate;

    if (data == null) {
      throw new IllegalArgumentException("IConfig is null");
    }

    this.data = data;
    setUserCanCancel(true);

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, null, null));
  }

  @Override
  protected void succeeded(IConfig result) {
    log.info("Configuration has been written to RTU successfully.");

    if (requireActivate) {
      activateConfig();
    }
  }

  private void activateConfig() {
    int rtn = JOptionPane.showOptionDialog(parent,
        "The configuration has been written.\nDo you want to activate the new configuration now?\n\n",
        "Done", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
        null, new String[] { "Activate Now", "Activate Later" }, "Activate Now");
 
    if (rtn == 0) {
      String newIP = findNewIP();
      log.info("ConfigTool detected new IP to connect to:" + newIP);
 
      // Activate new written configuration
      ConfigFileOperationTask task = new ConfigFileOperationTask(getApplication(),
          ConfigFileOperationTask.OPERATION_ACTIVATE);
      task.setNewIpAfterRestart(newIP);
      getTaskService().execute(task);
    }
  }
  
  public String findNewIP () {
    return findNewIP(G3RTUFactory.getDefault()
        .getComms().getRtuInfo().getNetInfoArray(), data);
  }

  
  @Override
  protected void failed(Throwable cause) {
    log.error("Fail to write configuration", cause);
    ConfigFileExportTask.showConfigExportFailDlg(parent, cause);
  }

  @Override
  public IConfig doInBackground() throws Exception {
    File configFile = new File(UIUtils.getLocalStorageDir(), G3Files.DEF_G3CONFIG_FNAME);

    /*
     * A delay to allow the block window displayed before any other dialog is
     * shown.
     */
    Thread.sleep(500);
    
    
    /* Checks API before writing*/
    G3ConfigAPI targetAPI = G3RTUFactory.getComms().cmdGetConfigAPI();
    if(checkAPI(targetAPI) == false) {
      cancel(true);
      return null;
    }



    /* Validate configData */
    IConfigValidator validator = data.getConfigModule(IConfigValidator.CONFIG_MODULE_ID); 
    validator.validateAndShowErrors(parent);
    if (validator.isValid() == false) {
      cancel(true);
      return null;
    }

    // Output configuration file
    setMessage("Generating configuration file...");
    new XmlDataWriter(data, this).saveToFile(configFile.getAbsolutePath());

    // Validate exported XML file
    setMessage("Validating configuration...");
    new XmlValidator(RTUConfigSchemaResource.FILE_PATH).validate(configFile.getPath());

    // Read exported configuration file
    // TODO: improve this copying mechanism using serialisation deep clone.
    IConfig exportedConfigData = new XmlDataReader(null).readFile(configFile);

    /* Compress configuration file with GZip */
    if (compressed) {
      String orgConfig = configFile.getAbsolutePath();
      String gzipConfig = orgConfig + G3Files.SUFFFIX_GZIP;
      ZipUtil.gzip(orgConfig, gzipConfig);
      configFile = new File(gzipConfig);
    }

    /* Transfer file to destination */
    FileTransferService.writeFileToRTUAndWait(CommsUtil.getFileTransfer(G3RTUFactory.getComms()),type, 180, configFile);

    // Delete the generated configuration file after writing to RTU
    configFile.delete();

    return exportedConfigData;
  }
  
  
  private boolean checkAPI(final G3ConfigAPI target) {
    G3ConfigAPI source = G3ConfigAPI.CURRENT_API;
    String err = G3ConfigAPI.checkConfigSchemaAPI(source, target);
    
    if (err != null) {
      // Show error dialog
      final String errorMsg = err;
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          
          @Override
          public void run() {
            ErrorInfo errInfo = new ErrorInfo("Error", 
                "You are using an incompatible version of Configuration Tool. "
                + "Please update the firmware of the RTU or download the compatible version from the RTU.", 
                errorMsg,
                null, null, Level.SEVERE, null);
            JXErrorPane.showDialog(parent, errInfo);
          }
        });
      } catch (InvocationTargetException | InterruptedException e) {
        e.printStackTrace();
      }
    }
  
    return err == null;
  }
  


  @Override
  public void printMessage(String message) {
    setMessage(message);
  }

  @Override
  public void setProgressValue(int progress) {
    if (progress >= 0 && progress <= 100) {
      setProgress(progress);
    }
  }
  
  public static String findNewIP(NetInfo[] netInfo, IConfig newConfig) {
    ConnectionPreference pref = PreferenceManager.INSTANCE
        .getPreference(ConnectionPreference.ID);
    String currentIP = pref.getServer();
    PortsManager portMgr =  newConfig.getConfigModule(PortsManager.CONFIG_MODULE_ID);
  
    String[] rtuIp = new String[netInfo.length];
    for (int i = 0; i < rtuIp.length; i++) {
      rtuIp[i] = netInfo[i].getIp();
    }
    return PortsUtility.findNewIP(rtuIp, portMgr.getEthManager(), currentIP);
  }
}
