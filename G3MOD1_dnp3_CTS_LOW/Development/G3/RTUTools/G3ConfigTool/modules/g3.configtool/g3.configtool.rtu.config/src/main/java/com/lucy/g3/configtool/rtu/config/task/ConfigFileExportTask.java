/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXLabel;
import org.jdesktop.swingx.error.ErrorInfo;

import com.jgoodies.common.base.Preconditions;
import com.jgoodies.common.base.Strings;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.validation.impl.IConfigValidator;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataWriter;
import com.lucy.g3.rtu.config.xml.serialization.XmlParserInvoker;
import com.lucy.g3.rtu.config.xml.validation.XmlValidator;
import com.lucy.g3.schema.support.RTUConfigSchemaResource;

/**
 * A task is for exporting {@linkplain configData} into a XML file.
 */
public class ConfigFileExportTask extends Task<Void, Void> implements XmlParserInvoker {

  private Logger log = Logger.getLogger(ConfigFileExportTask.class);

  protected final JFrame parent;

  private IConfig data;

  private File target;

  private XmlDataWriter writer;


  public ConfigFileExportTask(Application app, JFrame parent, IConfig data, File target) {
    super(app);
    this.data = Preconditions.checkNotNull(data, "Configdata must not be null");
    this.target = target;
    this.parent = parent;

    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, null, null));
    setUserCanCancel(true);
  }

  protected final File getTarget() {
    return target;
  }

  protected final void setTarget(File output) {
    this.target = output;
  }

  @Override
  protected Void doInBackground() throws Exception {
    /*
     * Appropriate delay to give the block window displayed before FileChoose is
     * showed, otherwise the fileChooser dialog maybe overlapped by the
     * blockDialog.
     */
    Thread.sleep(500);

    /* Validate configData */
    IConfigValidator validator = data.getConfigModule(IConfigValidator.CONFIG_MODULE_ID);
    
    validator.validateAndShowErrors(parent);
    if (validator.isValid() == false) {
      cancel(true);
      return null;
    }

    /*
     * Set the output file path
     */
    if (target == null) {
      setTarget(chooseDestination(parent));
    }

    /*
     * Cancel if no output file was chosen
     */
    if (target == null) {
      cancel(true);
      return null;
    }

    /*
     * Show dialog to confirm overwriting
     */
    if (MessageDialogs.showOverwriteDialog(parent, getTarget()) == false) {
      cancel(true);
      return null;
    }

    /*
     * Output configuration file.
     */
    final String filepath = target.getAbsolutePath();
    setProgress(20);
    message("createMessage");
    
    writer = new XmlDataWriter(data, this);
    writer.saveToFile(filepath);
    setProgress(60);

    /*
     * Validate the generated configuration file
     */
    message("validateMessage");
    try {
      new XmlValidator(RTUConfigSchemaResource.FILE_PATH).validate(filepath);
      setProgress(90);
    } catch (Exception e) {
      setProgress(90);
      log.error("G3 configuration validation failed", e);
      
      ErrorInfo info = new ErrorInfo("Invalid Configuration File",
          "The exported configuration is invalid against schema!",
          e.getMessage(), null,
          e, Level.SEVERE, null);
      JXErrorPane.showDialog(parent, info);
    }
    setProgress(100);
    message("finishedMessage");

    /*
     * Store the file path
     */
    GeneralConfigUtility.setFilePath(data, target.getAbsolutePath());
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    log.error("Fail to export configuration", cause);
    showConfigExportFailDlg(parent, cause);
  }

  @Override
  protected void succeeded(Void res) {
    showSuccessDlg(parent, "The configuration is saved to:", GeneralConfigUtility.getFilePath(data));
  }

  /**
   * Show success message dialog with a "Open Folder" button.
   */
  protected static void showSuccessDlg(JFrame parent, String message, String path) {
    if (path != null && new File(path).exists()) {
      // Prepare dialog components
      JPanel container = new JPanel(new BorderLayout(0, 10));
      container.add(new JXLabel(message), BorderLayout.NORTH);
      JXLabel lblMsg = new JXLabel(path);
      lblMsg.setLineWrap(true);
      lblMsg.setVerticalAlignment(SwingConstants.TOP);
      container.add(lblMsg, BorderLayout.CENTER);
      container.setPreferredSize(new Dimension(320, 80));

      // Show success dialog
      int rtn = JOptionPane.showOptionDialog(parent,
          container,
          "Save",
          JOptionPane.YES_NO_OPTION,
          JOptionPane.INFORMATION_MESSAGE, null,
          new String[] { "Open folder", "OK" }, "OK");

      // Open destination folder
      if (rtn == JOptionPane.YES_OPTION) {
        File file = new File(path).getParentFile();
        if (Desktop.isDesktopSupported()) {
          try {
            Desktop.getDesktop().open(file);
          } catch (IOException e) {
            Logger.getLogger(ConfigFileExportTask.class)
                .error("Cannot open the saved file: " + e.getMessage());
          }
        }
      }
    }
  }

  public static void showConfigExportFailDlg(Window parent, Throwable cause) {
    ErrorInfo info;
    String causeMsg = cause.getMessage();

    if (!Strings.isBlank(causeMsg)) {
      /* Compose cause message */
      StringBuilder sb = new StringBuilder();
      sb.append("<strong>Cause:</strong><br>");
      sb.append(causeMsg);
    } else {
      /* No cause message */
      causeMsg = null;
    }

    /* Compose and show error info */
    info = new ErrorInfo("Failure",
        "Failed to export configuration!",
        causeMsg,
        null,
        cause, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
  }

  @Override
  public void printMessage(String message) {
    setMessage(message);
  }

  @Override
  public void setProgressValue(int progress) {
    if (progress >= 0 && progress <= 100) {
      setProgress(progress);
    }

  }

  private File chooseDestination(Component parent) {
    File file = DialogUtils.showFileChooseDialog(parent,
        "Save Configuration File",
        "Save As",
        G3Files.FC_FILTER_G3CFG_GROUP);

    return file;
  }
}