/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.util.HashMap;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.control.ConfigurationAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * A task for sending command to operate configuration file in RTU device.
 */
public class ConfigFileOperationTask extends Task<Void, Void> {

  public static final int OPERATION_ERASE = 0;
  public static final int OPERATION_ACTIVATE = 1;
  public static final int OPERATION_RESTORE = 2;

  private Logger log = Logger.getLogger(ConfigFileOperationTask.class);

  private final JFrame parent;

  private final String opName;
  private final int opCode;

  private String newIpAfterRestart;

  
  /**
   * Constructs a task to operate configuration file in RTU.
   *
   * @param app
   *          current application.
   * @param operation
   *          one of {@code OPERATION_ERASE},{@code OPERATION_ACTIVATE},
   *          {@code OPERATION_RESTORE}
   */
  public ConfigFileOperationTask(Application app,  int operation) {
    super(app);
    this.opCode = operation;
    setUserCanCancel(false);

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    switch (operation) {
    case OPERATION_ERASE:
      opName = "erase";
      break;
    case OPERATION_ACTIVATE:
      opName = "activate";
      break;
    case OPERATION_RESTORE:
      opName = "restore";
      break;
    default:
      throw new IllegalArgumentException("Invalid operation code:" + operation);
    }
  }

  @Override
  protected Void doInBackground() throws Exception {
    ConfigurationAPI cmd = CommsUtil.getConfigurationAPI(G3RTUFactory.getComms());
    switch (opCode) {
    case OPERATION_ERASE:
      cmd.cmdEraseConfig();
      break;
    case OPERATION_ACTIVATE:
      cmd.cmdActivateConfig();
      break;
    case OPERATION_RESTORE:
      cmd.cmdRestoreConfig();
      break;
    default:
      throw new IllegalArgumentException("Invalid operation code:" + opCode);
    }

    setMessage(String.format("Configuration file is %sd!", opName));
    return null;
  }

  @Override
  protected void failed(Throwable cause) {
    String error = String.format("Configuration file is not %sd!", opName);
    log.error(error, cause);
    ErrorInfo info = new ErrorInfo("Fail",
        error,
        cause.getMessage(), null,
        null, Level.SEVERE, null);
    JXErrorPane.showDialog(parent, info);
  }

  @Override
  protected void succeeded(Void result) {
    int rtn = JOptionPane.showOptionDialog(parent,
        String.format("The configuration file has been %sd.\nPlease restart RTU to apply!\n\n", opName),
        "Success", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
        null, new String[] { "Restart Now", "Restart Later" }, "Restart Now");

    if (rtn == 0) {
      HashMap<String, Object> properties = new HashMap<>();
      properties.put(EventTopics.EVT_PROPERTY_RESTART_MODE, G3Protocol.RestartMode.RESTART);
      properties.put(EventTopics.EVT_PROPERTY_IP_ADDRESS, newIpAfterRestart);
      EventBroker.getInstance().send(EventTopics.EVT_TOPIC_RESTART_RTU, properties);
//      callback.restartRTU(RestartMode.RESTART, newIpAfterRestart);
    }
  }

  public void setNewIpAfterRestart(String newIpAfterRestart) {
    this.newIpAfterRestart = newIpAfterRestart;
  }

}
