/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromfile;

import java.util.Map;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * <p>
 * Wizard producer for loading IConfig from a selected configuration file.
 * </p>
 * {@linkplain #createWizard()} or {@linkplain #showWizardDialog()} can be used
 * to create a wizard or wizard dialog.
 */
public class LoadConfigFromFileWizard implements WizardResultProducer {

  private static final Class<?>[] WIZARD_CLASS_CREATE_FROM_TEMPLATE = new Class[] {
      LoadConfigStep1.class };


  @Override
  public Object finish(@SuppressWarnings("rawtypes") Map wizardData) throws WizardException {
    return wizardData.get(ConfigFileParser.KEY_CONFIG_DATA);
  }

  @Override
  public boolean cancel(@SuppressWarnings("rawtypes") Map settings) {
    return true;
  }

  public static Wizard createWizard() {
    return WizardPage.createWizard("New Configuration From Template",
        WIZARD_CLASS_CREATE_FROM_TEMPLATE,
        new LoadConfigFromFileWizard());
  }

  public static IConfig showWizardDialog() {
    return (IConfig) WizardDisplayer.showWizard(createWizard(), Helper.createHelpAction(LoadConfigFromFileWizard.class));
  }
}
