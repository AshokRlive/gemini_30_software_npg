/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config;

import java.awt.Cursor;
import java.awt.Frame;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskEvent;
import org.jdesktop.application.TaskListener;

import com.jgoodies.common.base.Strings;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.configtool.rtu.config.task.ClearCommissioningCacheTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileExportTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileImportTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileOperationTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFilePrintTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileReadTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileWriteTask;
import com.lucy.g3.configtool.rtu.config.wizard.createfromscratch.NewConfigFromScratchWizard;
import com.lucy.g3.configtool.subsys.permissions.PermissionCheckChecks;
import com.lucy.g3.configtool.subsys.permissions.UserActivities;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.eth.IPConfig;
import com.lucy.g3.rtu.config.shared.manager.ConfigManager;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.validation.impl.IConfigValidator;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * This class manages the configuration data object and provides global actions
 * for GUI layer to load, save... the configuration data.
 */
public class ConfigurationManager extends AbstractSubsystem {
  public final static String  EVENT_TOPIC_CONFIG_PATH_CHANGED  = "event.topics.configuration.filepath.change";

  
  public final static String SUBSYSTEM_ID = "subystem.id.ConfigurationManager";
  
  /** The name of read-only property {@linkplain #configData}. */
  public static final String PROPERTY_CONFIG_DATA = "configData";

  /**
   * The name of read-only property {@linkplain #configDataExist}, which
   * indicates if any G3 configuration has been set in ConfigurationManager.
   */
  public static final String PROPERTY_CONFIG_DATA_EXIST = "configDataExist";

  /** The name of read-only property {@linkplain #rtuConfig}. */
  public static final String PROPERTY_RTU_CONFIG = "rtuConfig";
  
  public static final String PROPERTY_CONFIG_FROM = "configFrom";

  /**
   * The name of read-only property {@linkplain #rtuConfigLoaded}, which
   * indicates if the RTU configuration has been loaded into
   * ConfigurationManager.
   */
  public static final String PROPERTY_RTU_CONFIG_LOADED = "rtuConfigLoaded";
  
  private String configFrom;

  // ====== Action Keys ======

  public static final String ACTION_CREATE_FROM_SCRATCH = "createConfig";
  public static final String ACTION_OPEN_CONFIG = "openConfig";
  public static final String ACTION_SAVEAS_CONFIG = "saveAsConfig";
  public static final String ACTION_SAVE_CONFIG = "saveConfig";
  public static final String ACTION_CLOSE_CONFIG = "closeConfig";
  public static final String ACTION_WRITE_CONFIG = "writeConfig";
  public static final String ACTION_READ_CONFIG = "readConfig";
  public static final String ACTION_ERASE_CONFIG = "eraseConfig";
  public static final String ACTION_ACTIVATE_CONFIG = "activateConfig";
  public static final String ACTION_RESTORE_CONFIG = "restoreConfig";
  public static final String ACTION_VALIDATE_CONFIG = "validate";
  public static final String ACTION_CLEAN_CONFIG = "clean";
  public static final String ACTION_PRINT_COFIG = "print";

  private static final int OPERATION_ERASE = ConfigFileOperationTask.OPERATION_ERASE;
  private static final int OPERATION_ACTIVATE = ConfigFileOperationTask.OPERATION_ACTIVATE;
  private static final int OPERATION_RESTORE = ConfigFileOperationTask.OPERATION_RESTORE;


  private Logger log = Logger.getLogger(ConfigurationManager.class);

  /** Indicates if the value of <code>configData</code> is not null. */
  private boolean configDataExist = false;

  /** Indicates if the value of <code>rtuConfig</code> is not null. */
  private boolean rtuConfigLoaded = false;

  private final ConfigManager manager = ConfigManager.getInstance();
  
  public ConfigurationManager(IContext context) {
    super(context, SUBSYSTEM_ID);
  }

  
  public String getConfigFrom() {
    return configFrom;
  }
  
  private void setConfigFrom(String configFrom) {
    Object oldValue = this.configFrom;
    this.configFrom = configFrom;
    firePropertyChange(PROPERTY_CONFIG_FROM, oldValue, configFrom);
  }

  private void setConfigData(IConfig data) {
    setConfigData(null, data);
  }
  
  private void setConfigData(String configFrom, IConfig data) {
    assert SwingUtilities.isEventDispatchThread();

    JFrame parent = getMainFrame();
    try {
      // Change cursor to WAIT as updating configData will block GUI for a
      // while
      if (parent != null) {
        parent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      }

      IConfig oldValue = getConfigData();

      manager.setLocalConfig(data);

      setConfigDataExist(data != null);
      firePropertyChange(PROPERTY_CONFIG_DATA, oldValue, data);

      setConfigFrom(configFrom);
      
      EventBroker.getInstance().send(EVENT_TOPIC_CONFIG_PATH_CHANGED, 
          GeneralConfigUtility.getFilePath(getConfigData()));
      
      log.info("ConfigData updated" );

    } finally {
      // Change cursor back
      if (parent != null) {
        parent.setCursor(Cursor.getDefaultCursor());
      }
    }
  }

  public boolean isConfigDataExist() {
    return configDataExist;
  }

  private void setConfigDataExist(boolean configDataExist) {
    Object oldValue = isConfigDataExist();
    this.configDataExist = configDataExist;
    firePropertyChange(PROPERTY_CONFIG_DATA_EXIST, oldValue, configDataExist);
  }

  public boolean isRtuConfigLoaded() {
    return rtuConfigLoaded;
  }

  // TODO review!
  public void setRtuConfigLoaded(boolean rtuConfigLoaded) {
    Object oldValue = isRtuConfigLoaded();
    this.rtuConfigLoaded = rtuConfigLoaded;
    firePropertyChange(PROPERTY_RTU_CONFIG_LOADED, oldValue, rtuConfigLoaded);
  }

  public IConfig getConfigData() {
    return manager.getLocalConfig();
  }

  public IConfig getRTUConfig() {
    return manager.getRemoteConfig();
  }

  private void setRTUConfig(IConfig rtuConfig) {
    log.info("Local RTU Configuration updated: " + rtuConfig);

    Object oldValue = getRTUConfig();
    manager.setRemoteConfig(rtuConfig);
    firePropertyChange(PROPERTY_RTU_CONFIG, oldValue, rtuConfig);

    setRtuConfigLoaded(rtuConfig != null);
  }

  // ================== Actions ===================

  @Action
  public void createConfig() {
    if (showAbortWarnDialog("Create Configuration", "create a new configuration") == true) {
      IConfig data = NewConfigFromScratchWizard.showWizardDialog(CommsUtil.getRTUEth0Info(G3RTUFactory.getComms()));
      if (data != null) {
        setConfigData(data);
      }
    }
  }

  public Task<?,?> createConfigImportTask() {
    ConfigFileImportTask task = new ConfigFileImportTask(getApp(), getMainFrame());
    task.addTaskListener(new ConfigDataImportHandler());
    return task;
  }
  
  @Action
  public void openConfig() {
    if (showAbortWarnDialog("Open Configuration", "open a configuration file")) {
      getDefaultTaskService().execute(createConfigImportTask());
    }
  }

  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public void saveConfig() {
    if (isConfigDataExist()) {
      IConfig data = getConfigData();
      String targetPath = GeneralConfigUtility.getFilePath(data);
      getDefaultTaskService().execute(
          new ConfigFileExportTask(getApp(),
              getMainFrame(),
              data,
              Strings.isBlank(targetPath) ? null : new File(targetPath)));
    }
  }

  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public void saveAsConfig() {
    if (isConfigDataExist()) {
      getDefaultTaskService().execute(
          new ConfigFileExportTask(getApp(),
              getMainFrame(), getConfigData(), null));
    }
  }

  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public void closeConfig() {
    if (!showAbortWarnDialog("Close Configuration", "close the configuration")) {
      return;
    }

    setConfigData(null);
  }

  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void readConfig() {
    if (checkIsConnected() == false) {
      return;
    }

    if (showAbortWarnDialog("Read Configuration", "read the configuration from the RTU") == false) {
      return;
    }

    ConfigFileReadTask task = new ConfigFileReadTask(getApp(), CommsUtil.getFileTransfer(G3RTUFactory.getComms()));
    task.addTaskListener(new ConfigDataReadHandler());
    getCommsTaskService().execute(task);
  }

  /* Write the current config to the RTU */
  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public void writeConfig() {
    String warn = null;

    if (checkIsConnected()) {
      if (PermissionCheckChecks.checkPermitted(UserActivities.WRITE_CONFIG)) {
        if (isConfigDataExist()) {
          if (confirmEth0Change()) {
            writeConfig(getConfigData(), true, false);
          } else {
            warn = "Unable to write RTU - Ethernet change not accepted.";
          }
        } else {
          warn = "Unable to write RTU - No configuration data exists.";
        }
      } else {
        warn = "Unable to write RTU - User not authorised.";
      }
    } else {
      warn = "Unable to write RTU - RTU not connected.";
    }

    if (warn != null) {
      log.warn(warn);
    }else{
      /*
       * New config is written, commissioning cache must be cleared now
       * since it might be inconsistent with the new config
       */
      getCommsTaskService().execute(new ClearCommissioningCacheTask(getApp(), CommsUtil.getCommissionAPI(G3RTUFactory.getComms())));
    }
  }
  
  /**
   * Writes a configuration data to RTU.
   *
   * @param data
   *          the data to be written
   * @param requireActivate
   *          the require activate after writing
   * @param blocked
   *          block invoker thread
   * @return the task for writing config
   */
  public ConfigFileWriteTask writeConfig(IConfig data, boolean requireActivate, boolean blocked) {
    ConfigFileWriteTask task = new ConfigFileWriteTask(getApp(), data, requireActivate);
    task.addTaskListener(new ConfigDataWriteHandler());
    getCommsTaskService().execute(task);
    
    // Waiting task to be completed... 
    if (blocked) {
      try {
        task.get();
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
    }
    
    return task;
  }

  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void eraseConfig() {
    operateConfig("Do you really want to erase the configuration of RTU?", OPERATION_ERASE);
  }

  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void activateConfig() {
    operateConfig("The new written configuration file will be activated.\n"
        + "Do you want to continue?", OPERATION_ACTIVATE);
  }

  private void operateConfig(String msg, int operationCode) {
    if (checkIsConnected() == false) {
      return;
    }

    int rtn = JOptionPane.showConfirmDialog(getMainFrame(),
        msg,
        "Confirm", JOptionPane.YES_NO_CANCEL_OPTION,
        JOptionPane.WARNING_MESSAGE);

    if (rtn == JOptionPane.YES_OPTION) {
      getCommsTaskService().execute(new ConfigFileOperationTask(getApp(),  operationCode));
    }
  }

  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void restoreConfig() {
    operateConfig("Do you really want to restore the last backup configuration?", OPERATION_RESTORE);
  }

  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public void validate() {
      IConfigValidator validator = getConfigData().getConfigModule(IConfigValidator.CONFIG_MODULE_ID);
      validator.executeValidation(getApp());
  }

  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public void clean() {
      IConfigValidator validator = getConfigData().getConfigModule(IConfigValidator.CONFIG_MODULE_ID);
      validator.executeCleaning(getApp());
  }

  @Action(enabledProperty = PROPERTY_CONFIG_DATA_EXIST)
  public Task<?, ?> print() {
    if (isConfigDataExist()) {
      getCommsTaskService().execute(new ConfigFilePrintTask(getApp(), getMainFrame(), getConfigData()));
    }
    return null;
  }


  /* Confirm IP change for Eth0 */
  private boolean confirmEth0Change() {
    final ETHERNET_PORT portEnum = ETHERNET_PORT.ETHERNET_PORT_CONTROL;
    PortsManager portsMgr = getConfigData().getConfigModule(PortsManager.CONFIG_MODULE_ID);
    IEthernetPort newEth0 = portsMgr.getEthPort(portEnum);
    NetInfo eth0Info = CommsUtil.getRTUEth0Info(G3RTUFactory.getComms());
    if (newEth0 == null || eth0Info == null) {
      log.warn("Fail to get Ethernet info");
      return true;
    }

    if (!newEth0.isConfigEnabled()) {
      return true; // Configuration not enabled
    }

    String newIp = newEth0.getIpConfig().getIpAddress();
    String oldIp = eth0Info.getIp();

    if (EqualsUtil.areEqual(oldIp, newIp)) {
      return true; // No change
    }

    // Ask use to confirm change or revert
    String och = "Old IP - " + oldIp;
    String nch = "New IP - " + newIp;
    String dch = "Discard write";

    Frame par = WindowUtils.getMainFrame();
    String tle = "Select the IP address";
    int typ = JOptionPane.YES_NO_CANCEL_OPTION;
    int ico = JOptionPane.QUESTION_MESSAGE;
    String[] opt = { och, nch, dch };

    String msg = "        The RTU has an ethernet 0 IP address of \n"
        + "            " + oldIp + "                         \n"
        + "        but the new configuration has           \n"
        + "            " + newIp + "                         \n\n"
        + "        Which address do you want to use?       \n\n";

    int choice = JOptionPane.showOptionDialog(par, msg, tle, typ, ico, null, opt, 2);
    if (choice == 0) {
      // Keep old IP
      IPConfig eth0 = newEth0.getIpConfig();
      eth0.setIpAddress(eth0Info.getIp());
      eth0.setSubmask(eth0Info.getMask());
      eth0.setGateway(eth0Info.getGateway());
      return true;

    } else if (choice == 1) {
      // Apply new IP
      return true;

    } else {
      // Discard
      return false;
    }
  }

  private boolean showAbortWarnDialog(String title, String action) {
    if (getConfigData() != null) {
      int option = JOptionPane.showConfirmDialog(getMainFrame(), 
         String.format("You are trying to %s.\nAny changes that have been made locally will be lost.\nAre you sure?",
             action), 
              title,JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
      if (option != JOptionPane.YES_OPTION) {
        return false;
      }
    }
    return true;
  }


  private class ConfigDataImportHandler extends TaskListener.Adapter<IConfig, Void> {

    @Override
    public void succeeded(TaskEvent<IConfig> event) {
      String from = null;
      IConfig config = event.getValue();
      if(config != null) {
        from = ((GeneralConfig)config.getConfigModule(GeneralConfig.CONFIG_MODULE_ID)).getFilePath();
      }
      setConfigData(from, event.getValue());
    }
  }

  private class ConfigDataReadHandler extends TaskListener.Adapter<IConfig[], Void> {

    @Override
    public void succeeded(TaskEvent<IConfig[]> event) {
      IConfig[] data = event.getValue();
      if (data != null) {
        setConfigData("RTU", data[0]);
        setRTUConfig(data[1]);
      } else {
        setConfigData(null);
        setRTUConfig(null);
      }
    }

	@Override
	public void failed(TaskEvent<Throwable> event) {
        setConfigData(null);
        setRTUConfig(null);
	}
    
    
  }

  private class ConfigDataWriteHandler extends TaskListener.Adapter<IConfig, Void> {

    @Override
    public void succeeded(TaskEvent<IConfig> event) {
      setRTUConfig(event.getValue());
    }
  }


  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
  }

}
