/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.rtu.config.module.canmodule.domain.BackPlaneType;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.ui.widgets.CTRatioEditorCombox;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * WizardPage. Define modules.
 */
class NewConfigStep2 extends WizardPage {
  private static final ArrayList<ModuleCfg> ALL_CONF = new ArrayList<>(); 
  private static final ModuleCfg CONF_MCM = new ModuleCfg(ALL_CONF,MODULE.MODULE_MCM, "checkedMCM", null, true, false);
  private static final ModuleCfg CONF_PSM = new ModuleCfg(ALL_CONF,MODULE.MODULE_PSM, "checkedPSM", null, true, true);
  private static final ModuleCfg CONF_SCM = new ModuleCfg(ALL_CONF,MODULE.MODULE_SCM_MK2, "checkedSCM", "SCMQuantity", false, true);
  private static final ModuleCfg CONF_DSM = new ModuleCfg(ALL_CONF,MODULE.MODULE_DSM, "checkedDSM", "DSMQuantity", false, true);
  private static final ModuleCfg CONF_FDM = new ModuleCfg(ALL_CONF,MODULE.MODULE_FDM, "checkedFDM", "FDMQuantity", false, true);
  private static final ModuleCfg CONF_FPM = new ModuleCfg(ALL_CONF,MODULE.MODULE_FPM, "checkedFPM", "FPMQuantity", false, true);
  private static final ModuleCfg CONF_IOM = new ModuleCfg(ALL_CONF,MODULE.MODULE_IOM, "checkedIOM", "IOMQuantity", false, true);
  private static final ModuleCfg CONF_HMI = new ModuleCfg(ALL_CONF,MODULE.MODULE_HMI, "checkedHMI", null, true, true);

  public static final String KEY_FAN_FITTED = "fanfitted"; // bool
  public static final String KEY_SELECT_BACKPLANE = "selectBackPlane"; // Enum
  public static final String KEY_CREATE_HMI_MENU = "checkedHmiMenu"; // bool
  public static final String KEY_FPM_CTRATIO = "fpmCTRatio"; // CTRatio

  private final ModuleCompGroup[] groups;


  public NewConfigStep2() {
    initComponents();

    this.groups = new ModuleCompGroup[] {
        new ModuleCompGroup(this.checkBoxMCM, null, CONF_MCM),
        new ModuleCompGroup(this.checkBoxPSM, null, CONF_PSM),
        new ModuleCompGroup(this.checkBoxSCM, this.combSCMNum, CONF_SCM),
        new ModuleCompGroup(this.checkBoxDSM, this.combDSMNum, CONF_DSM),
        new ModuleCompGroup(this.checkBoxFDM, this.combFDMNum, CONF_FDM),
        new ModuleCompGroup(this.checkBoxFPM, this.combFPMNum, CONF_FPM),
        new ModuleCompGroup(this.checkBoxIOM, this.combIOMNum, CONF_IOM),
        new ModuleCompGroup(this.checkBoxHMI, null, CONF_HMI),
    };

    initComponentStates();
    updateVisibility();
  }

  public static String getDescription() {
    return "G3 Modules";
  }

  private void initComponentStates() {
    for (ModuleCompGroup g : this.groups) {

      // Set component name for accessing their value from wizardDatat
      g.checked.setName(g.conf.selectedKey);
      g.checked.setSelected(g.conf.selected);
      g.checked.setEnabled(g.conf.enabled);

      if (g.quantity != null) {
        // Set component name for accessing their value from wizardDatat
        g.quantity.setName(g.conf.quantityKey);

        // Set data model
        DefaultComboBoxModel<Integer> model = new DefaultComboBoxModel<Integer>();
        for (int i = 1; i <= g.maxQuantity; i++) {
          model.addElement(i);
        }
        g.quantity.setModel(model);
      }

    }

    this.combBackplane.setName(KEY_SELECT_BACKPLANE);

    this.checkBoxCreateHMIMenu.setName(KEY_CREATE_HMI_MENU);
    this.checkBoxCreateHMIMenu.setSelected(true);
    this.checkBoxCreateHMIMenu.setVisible(CONF_HMI.selected);

    this.checkBoxFanFitted.setName(KEY_FAN_FITTED);

    this.combFPMCTRatio.setName(KEY_FPM_CTRATIO);
    this.combFPMCTRatio.getEditor().getEditorComponent().setName(KEY_FPM_CTRATIO);
  }

  private int totalModuleNum() {
    int num = 0;

    for (ModuleCompGroup g : this.groups) {

      if (g.checked.isSelected() && !"HMI".equals(g.conf.type.getDescription())) {
        if (g.quantity != null) {
          num += Integer.valueOf(g.quantity.getSelectedItem().toString());
        } else {
          num++;
        }
      }
    }

    return num;
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;
    BackPlaneType backplaneType = (BackPlaneType) this.combBackplane.getSelectedItem();
    int slotNum = backplaneType.getSlotNum();
    int selectedModuleNum = totalModuleNum();

    if (selectedModuleNum > slotNum) {
      result = "";// "The quantity of selected modules exceeds backplane's slot number";
    }

    if (result == null) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    }

    return result;
  }

  private void createUIComponents() {
    this.combBackplane = new JComboBox<BackPlaneType>(BackPlaneType.values());
    this.combFPMCTRatio = new CTRatioEditorCombox(FPIConfig.getDefaultCTRatio());
  }

  private void checkBoxChanged(ChangeEvent e) {
    Object source = e.getSource();

    for (ModuleCompGroup g : this.groups) {
      // When selected a module, enable quantity component if it exists
      if (source == g.checked) {
        if (g.quantity != null) {
          g.quantity.setEnabled(g.checked.isSelected());
        }
        break;
      }
    }

    updateVisibility();
  }

  private void updateVisibility() {
    this.checkBoxFanFitted.setVisible(this.checkBoxPSM.isSelected());
    this.combFPMCTRatio.setVisible(this.checkBoxFPM.isSelected());
    this.labelRatio.setVisible(this.checkBoxFPM.isSelected());
    this.checkBoxCreateHMIMenu.setVisible(this.checkBoxHMI.isSelected());
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    panel1 = new JPanel();
    label2 = new JLabel();
    label1 = new JLabel();
    checkBoxMCM = new JCheckBox();
    checkBoxPSM = new JCheckBox();
    checkBoxFanFitted = new JCheckBox();
    checkBoxHMI = new JCheckBox();
    checkBoxCreateHMIMenu = new JCheckBox();
    checkBoxSCM = new JCheckBox();
    label3 = new JLabel();
    combSCMNum = new JComboBox<>();
    checkBoxDSM = new JCheckBox();
    label6 = new JLabel();
    combDSMNum = new JComboBox<>();
    checkBoxIOM = new JCheckBox();
    label5 = new JLabel();
    combIOMNum = new JComboBox<>();
    checkBoxFPM = new JCheckBox();
    label7 = new JLabel();
    combFPMNum = new JComboBox<>();
    labelRatio = new JLabel();
    checkBoxFDM = new JCheckBox();
    label8 = new JLabel();
    combFDMNum = new JComboBox<>();

    //======== this ========
    setLayout(new FormLayout(
        "default, $ugap, right:pref, 2*($lcgap, default), $lcgap, default:grow",
        "default, $ugap, 17dlu, 8*($lgap, default)"));

    //======== panel1 ========
    {
      panel1.setLayout(new FormLayout(
          "default, $lcgap, pref:grow",
          "default"));

      //---- label2 ----
      label2.setText("Backplane Type:");
      panel1.add(label2, CC.xy(1, 1));
      panel1.add(combBackplane, CC.xy(3, 1));
    }
    add(panel1, CC.xywh(1, 1, 5, 1));

    //---- label1 ----
    label1.setText(" G3 Modules:");
    add(label1, CC.xy(1, 3));

    //---- checkBoxMCM ----
    checkBoxMCM.setText("[MCM] Main Control Module");
    add(checkBoxMCM, CC.xywh(1, 5, 5, 1));

    //---- checkBoxPSM ----
    checkBoxPSM.setText("[PSM] Power Supply Module");
    checkBoxPSM.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });
    add(checkBoxPSM, CC.xy(1, 7));

    //---- checkBoxFanFitted ----
    checkBoxFanFitted.setText("Fan Fitted");
    add(checkBoxFanFitted, CC.xywh(3, 7, 7, 1));

    //---- checkBoxHMI ----
    checkBoxHMI.setText("[HMI] Human Machine Interface");
    checkBoxHMI.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });
    add(checkBoxHMI, CC.xy(1, 9));

    //---- checkBoxCreateHMIMenu ----
    checkBoxCreateHMIMenu.setText("Default HMI menu");
    add(checkBoxCreateHMIMenu, CC.xywh(3, 9, 7, 1));

    //---- checkBoxSCM ----
    checkBoxSCM.setText("[SCM] Switch Control Module");
    checkBoxSCM.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });
    add(checkBoxSCM, CC.xy(1, 11));

    //---- label3 ----
    label3.setText("Quantity:");
    add(label3, CC.xy(3, 11));

    //---- combSCMNum ----
    combSCMNum.setEnabled(false);
    add(combSCMNum, CC.xy(5, 11));

    //---- checkBoxDSM ----
    checkBoxDSM.setText("[DSM] Dual Switch Module");
    checkBoxDSM.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });
    add(checkBoxDSM, CC.xy(1, 13));

    //---- label6 ----
    label6.setText("Quantity:");
    add(label6, CC.xy(3, 13));

    //---- combDSMNum ----
    combDSMNum.setEnabled(false);
    add(combDSMNum, CC.xy(5, 13));

    //---- checkBoxIOM ----
    checkBoxIOM.setText("[IOM] IO Module");
    checkBoxIOM.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });
    add(checkBoxIOM, CC.xy(1, 15));

    //---- label5 ----
    label5.setText("Quantity:");
    add(label5, CC.xy(3, 15));

    //---- combIOMNum ----
    combIOMNum.setEnabled(false);
    add(combIOMNum, CC.xy(5, 15));

    //---- checkBoxFPM ----
    checkBoxFPM.setText("[FPM] Fault Passage Module");
    checkBoxFPM.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });
    add(checkBoxFPM, CC.xy(1, 17));

    //---- label7 ----
    label7.setText("Quantity:");
    add(label7, CC.xy(3, 17));

    //---- combFPMNum ----
    combFPMNum.setEnabled(false);
    add(combFPMNum, CC.xy(5, 17));

    //---- labelRatio ----
    labelRatio.setText("CT Ratio:");
    labelRatio.setHorizontalAlignment(SwingConstants.RIGHT);
    add(labelRatio, CC.xy(1, 19));
    add(combFPMCTRatio, CC.xywh(3, 19, 3, 1));

    //---- checkBoxFDM ----
    checkBoxFDM.setText("[FDM] Fault Detect Module");
    checkBoxFDM.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        checkBoxChanged(e);
      }
    });

    //---- label8 ----
    label8.setText("Quantity:");

    //---- combFDMNum ----
    combFDMNum.setEnabled(false);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel label2;
  private JComboBox<BackPlaneType> combBackplane;
  private JLabel label1;
  private JCheckBox checkBoxMCM;
  private JCheckBox checkBoxPSM;
  private JCheckBox checkBoxFanFitted;
  private JCheckBox checkBoxHMI;
  private JCheckBox checkBoxCreateHMIMenu;
  private JCheckBox checkBoxSCM;
  private JLabel label3;
  private JComboBox<Integer> combSCMNum;
  private JCheckBox checkBoxDSM;
  private JLabel label6;
  private JComboBox<Integer> combDSMNum;
  private JCheckBox checkBoxIOM;
  private JLabel label5;
  private JComboBox<Integer> combIOMNum;
  private JCheckBox checkBoxFPM;
  private JLabel label7;
  private JComboBox<Integer> combFPMNum;
  private JLabel labelRatio;
  private JComboBox<CTRatio> combFPMCTRatio;
  private JCheckBox checkBoxFDM;
  private JLabel label8;
  private JComboBox<Integer> combFDMNum;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  private static class ModuleCfg {

    protected final MODULE type;

    /** The key for retrieving module selection value from wizardData. */
    protected final String selectedKey;

    /** The key for retrieving quantity value from wizardData. */
    protected final String quantityKey;

    /** Initial selection state. */
    protected final boolean selected;

    /** Component enable state. */
    protected final boolean enabled;


    public ModuleCfg(ArrayList<ModuleCfg> all, MODULE type, String selectedKey, String quantityKey,
        boolean initialSelection, boolean enabled) {
      super();
      all.add(this);
      this.type = type;
      this.selectedKey = selectedKey;
      this.quantityKey = quantityKey;
      this.selected = initialSelection;
      this.enabled = enabled;
    }
  }

  private static class ModuleCompGroup {

    protected final JComboBox<Integer> quantity;
    protected final int maxQuantity;
    protected final JCheckBox checked;
    protected final ModuleCfg conf;


    public ModuleCompGroup(JCheckBox checked,
        JComboBox<Integer> quantity, ModuleCfg conf) {
      this.quantity = quantity;
      this.checked = checked;
      this.conf = conf;
      this.maxQuantity = ModuleEnums.getMaxModuleNumber(conf.type);
    }
  }


  // ================== Help Methods ===================

  public static boolean fanFitted(Map<?,?> wData) {
    return (Boolean) wData.get(KEY_FAN_FITTED);
  }

  public static boolean createHMIMenu(Map<?,?> wData) {
    return (Boolean) wData.get(KEY_CREATE_HMI_MENU);
  }

  public static BackPlaneType backplaneType(Map<?,?> wData) {
    return (BackPlaneType) wData.get(KEY_SELECT_BACKPLANE);
  }

  /** Get the quantity of a module to be created. */
  public static int getQuantity(Map<?,?> wData, MODULE type) {
    for (ModuleCfg conf : ALL_CONF) {
      if (conf.type == type) {

        if ((Boolean) wData.get(conf.selectedKey)) {
          Object num = wData.get(conf.quantityKey);
          return num == null ? 1 : (Integer) num;
        }

        break;
      }
    }
    return 0;
  }

}
