/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * Wizard page. Generate control logic.
 */
class NewConfigStep4 extends WizardPage {

  public static final String KEY_GENERATE_CLOGIC = "genControlLogic";
  public static final String KEY_GEN_DEFAULT_CLOGIC = "genDefControlLogic";

  // Keys for custom logic options
  public static final String KEY_CUSTOM_FAN_TEST = "customFanTest";
  public static final String KEY_CUSTOM_OLR = "customOLR";
  public static final String KEY_CUSTOM_DUMMY_CONTROL = "customDummyControl";
  public static final String KEY_CUSTOM_SWITCH_CONTROL = "customSwitchControl";
  public static final String KEY_CUSTOM_DIGITAL_OUTPUT = "customDigitalOutput";


  public NewConfigStep4() {
    initComponents();
    initComponentNames();
    udpateVisibility();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;
    if (result == null) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    }
    return result;
  }

  /* Set the name of components so their value will retrievable in wizardDatat */
  private void initComponentNames() {
    radioButtonGenCLogicYes.setName(KEY_GENERATE_CLOGIC);
    radioButtonGenDefault.setName(KEY_GEN_DEFAULT_CLOGIC);
    checkBoxCustomOLRLogic.setName(KEY_CUSTOM_OLR);
    checkBoxCustomDummySwitchLogic.setName(KEY_CUSTOM_DUMMY_CONTROL);
    checkBoxCustomFanLogic.setName(KEY_CUSTOM_FAN_TEST);
    checkBoxCustomSCMLogic.setName(KEY_CUSTOM_SWITCH_CONTROL);
    checkBoxCustomDigiIOLogic.setName(KEY_CUSTOM_DIGITAL_OUTPUT);
  }

  public static String getDescription() {
    return "Control Logic";
  }

  @Override
  protected void renderingPage() {
    @SuppressWarnings("rawtypes")
    Map wdata = getWizardDataMap();

    // Hide Fan Logic checkbox if there is no PSM checked or Fan not fitted
    if (NewConfigStep2.getQuantity(wdata, MODULE.MODULE_PSM) > 0
        && NewConfigStep2.fanFitted(wdata)) {
      checkBoxCustomFanLogic.setVisible(true);
    } else {
      checkBoxCustomFanLogic.setVisible(false);
      checkBoxCustomFanLogic.setSelected(false);
    }

    // Hide Switch Logic checkbox if there is no SCM defined
    if (NewConfigStep2.getQuantity(wdata, MODULE.MODULE_SCM) > 0
        || NewConfigStep2.getQuantity(wdata, MODULE.MODULE_DSM) > 0
        || NewConfigStep2.getQuantity(wdata, MODULE.MODULE_SCM_MK2) > 0) {
      checkBoxCustomSCMLogic.setVisible(true);
    } else {
      checkBoxCustomSCMLogic.setVisible(false);
      checkBoxCustomSCMLogic.setSelected(false);
    }

    // Hide digital IO Logic checkbox if there is no IOM defined
    if (NewConfigStep2.getQuantity(wdata, MODULE.MODULE_IOM) > 0) {
      checkBoxCustomDigiIOLogic.setVisible(true);
    } else {
      checkBoxCustomDigiIOLogic.setVisible(false);
      checkBoxCustomDigiIOLogic.setSelected(false);
    }

    super.renderingPage();
  }

  private void udpateVisibility() {
    panelLogicKinds.setVisible(radioButtonGenCLogicYes.isSelected());
    panelCustomLogic.setVisible(radioButtonGenCustom.isSelected());
  }

  private void radioButtonGenCLogicYesStateChanged(ChangeEvent e) {
    udpateVisibility();
  }

  private void radioButtonGenCustomStateChanged(ChangeEvent e) {
    udpateVisibility();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    scrollPane1 = new JScrollPane();
    panel2 = new JPanel();
    label1 = new JLabel();
    radioButtonGenCLogicYes = new JRadioButton();
    radioButtonGenCLogicNo = new JRadioButton();
    panelLogicKinds = new JPanel();
    label2 = new JLabel();
    radioButtonGenDefault = new JRadioButton();
    radioButtonGenCustom = new JRadioButton();
    panelCustomLogic = new JPanel();
    checkBoxCustomOLRLogic = new JCheckBox();
    checkBoxCustomDummySwitchLogic = new JCheckBox();
    checkBoxCustomFanLogic = new JCheckBox();
    checkBoxCustomSCMLogic = new JCheckBox();
    checkBoxCustomDigiIOLogic = new JCheckBox();

    // ======== this ========
    setLayout(new BorderLayout());

    // ======== scrollPane1 ========
    {
      scrollPane1.setBorder(BorderFactory.createEmptyBorder());

      // ======== panel2 ========
      {
        panel2.setLayout(new FormLayout(
            "5dlu, $lcgap, default:grow",
            "2*(default, $lgap), default, $pgap, default"));

        // ---- label1 ----
        label1.setText("Do you want to generate Control Logic?");
        panel2.add(label1, CC.xywh(1, 1, 3, 1));

        // ---- radioButtonGenCLogicYes ----
        radioButtonGenCLogicYes.setText("Yes");
        radioButtonGenCLogicYes.setSelected(true);
        radioButtonGenCLogicYes.addChangeListener(new ChangeListener() {

          @Override
          public void stateChanged(ChangeEvent e) {
            radioButtonGenCLogicYesStateChanged(e);
          }
        });
        panel2.add(radioButtonGenCLogicYes, CC.xy(3, 3));

        // ---- radioButtonGenCLogicNo ----
        radioButtonGenCLogicNo.setText("No");
        panel2.add(radioButtonGenCLogicNo, CC.xy(3, 5));

        // ======== panelLogicKinds ========
        {
          panelLogicKinds.setLayout(new FormLayout(
              "5dlu, $lcgap, default:grow",
              "3*(default, $lgap), default"));

          // ---- label2 ----
          label2.setText("What kinds of Control Logic do you want to generate?");
          panelLogicKinds.add(label2, CC.xywh(1, 1, 3, 1));

          // ---- radioButtonGenDefault ----
          radioButtonGenDefault.setText("Default (Recommended)");
          radioButtonGenDefault.setSelected(true);
          panelLogicKinds.add(radioButtonGenDefault, CC.xy(3, 3));

          // ---- radioButtonGenCustom ----
          radioButtonGenCustom.setText("Custom");
          radioButtonGenCustom.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
              radioButtonGenCustomStateChanged(e);
            }
          });
          panelLogicKinds.add(radioButtonGenCustom, CC.xy(3, 5));

          // ======== panelCustomLogic ========
          {
            panelCustomLogic.setLayout(new FormLayout(
                "10dlu, default:grow",
                "4*(default, $rgap), default"));

            // ---- checkBoxCustomOLRLogic ----
            checkBoxCustomOLRLogic.setText("Off-Local-Remote Control Logic");
            checkBoxCustomOLRLogic.setEnabled(false);
            checkBoxCustomOLRLogic.setSelected(true);
            panelCustomLogic.add(checkBoxCustomOLRLogic, CC.xy(2, 1));

            // ---- checkBoxCustomDummySwitchLogic ----
            checkBoxCustomDummySwitchLogic.setText("Dummy Switch Control Logic");
            panelCustomLogic.add(checkBoxCustomDummySwitchLogic, CC.xy(2, 3));

            // ---- checkBoxCustomFanLogic ----
            checkBoxCustomFanLogic.setText("Fan Test");
            panelCustomLogic.add(checkBoxCustomFanLogic, CC.xy(2, 5));

            // ---- checkBoxCustomSCMLogic ----
            checkBoxCustomSCMLogic.setText("Switch Control Logic");
            panelCustomLogic.add(checkBoxCustomSCMLogic, CC.xy(2, 7));

            // ---- checkBoxCustomDigiIOLogic ----
            checkBoxCustomDigiIOLogic.setText("Digital IO Control Logic");
            panelCustomLogic.add(checkBoxCustomDigiIOLogic, CC.xy(2, 9));
          }
          panelLogicKinds.add(panelCustomLogic, CC.xy(3, 7));
        }
        panel2.add(panelLogicKinds, CC.xywh(1, 7, 3, 1));
      }
      scrollPane1.setViewportView(panel2);
    }
    add(scrollPane1, BorderLayout.CENTER);

    // ---- buttonGroup2 ----
    ButtonGroup buttonGroup2 = new ButtonGroup();
    buttonGroup2.add(radioButtonGenCLogicYes);
    buttonGroup2.add(radioButtonGenCLogicNo);

    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioButtonGenDefault);
    buttonGroup1.add(radioButtonGenCustom);
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    UIUtils.increaseScrollSpeed(scrollPane1);
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JPanel panel2;
  private JLabel label1;
  private JRadioButton radioButtonGenCLogicYes;
  private JRadioButton radioButtonGenCLogicNo;
  private JPanel panelLogicKinds;
  private JLabel label2;
  private JRadioButton radioButtonGenDefault;
  private JRadioButton radioButtonGenCustom;
  private JPanel panelCustomLogic;
  private JCheckBox checkBoxCustomOLRLogic;
  private JCheckBox checkBoxCustomDummySwitchLogic;
  private JCheckBox checkBoxCustomFanLogic;
  private JCheckBox checkBoxCustomSCMLogic;
  private JCheckBox checkBoxCustomDigiIOLogic;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
