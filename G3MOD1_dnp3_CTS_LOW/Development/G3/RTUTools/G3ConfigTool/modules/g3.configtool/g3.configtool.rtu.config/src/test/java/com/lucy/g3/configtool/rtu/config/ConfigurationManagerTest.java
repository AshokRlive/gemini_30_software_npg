/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.swing.JLabel;

import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiTask;
import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.value.ConverterFactory.BooleanNegator;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.jgoodies.binding.value.ConverterValueModel;
import com.jgoodies.binding.value.ValueModel;

/**
 * The Class ConfigurationManagerTest.
 */
public class ConfigurationManagerTest {

  private ConfigurationManager cfgMgr;


  @Before
  public void setUp() throws Exception {
    Application app = Application.getInstance();
    cfgMgr = new ConfigurationManager(null);
  }

  @After
  public void tearDown() throws Exception {
    cfgMgr = null;
  }

  @Test
  public void testBindingPropertyRTUConfigLoaded() {
    GuiActionRunner.execute(new GuiTask() {

      @Override
      protected void executeInEDT() throws Throwable {

        assertEquals("There should be no listener initially",
            0, cfgMgr.getPropertyChangeListeners().length);

        // Create value model
        final PropertyAdapter<?> vm = new PropertyAdapter<ConfigurationManager>(
            cfgMgr, ConfigurationManager.PROPERTY_RTU_CONFIG_LOADED, true);
        assertEquals("There is 1 listener after creating a value model",
            1, cfgMgr.getPropertyChangeListeners().length);

        // Create converter for the value model
        final ValueModel converter = new ConverterValueModel(vm, new BooleanNegator());

        // Binding GUI component
        final JLabel label = new JLabel("THIS IS A TEST LABEL");
        // JFrame frame = new JFrame();
        // frame.setSize(100, 100);
        // frame.setVisible(true);
        // frame.setContentPane(label);
        Bindings.bind(label, "visible", converter);
        assertTrue(label.isVisible() == (Boolean) converter.getValue());
        assertTrue(label.isVisible() == !(Boolean) vm.getValue());
        assertTrue(label.isVisible() == !cfgMgr.isRtuConfigLoaded());

        // Updater property value
        cfgMgr.setRtuConfigLoaded(true);
        assertTrue(label.isVisible() == (Boolean) converter.getValue());
        assertTrue(label.isVisible() == !(Boolean) vm.getValue());
        assertTrue(label.isVisible() == !cfgMgr.isRtuConfigLoaded());
        assertFalse(label.isVisible());

        // Updater property value
        cfgMgr.setRtuConfigLoaded(false);
        assertTrue(label.isVisible() == (Boolean) converter.getValue());
        assertTrue(label.isVisible() == !(Boolean) vm.getValue());
        assertTrue(label.isVisible() == !cfgMgr.isRtuConfigLoaded());
        assertTrue(label.isVisible());

        vm.release();
        assertEquals("There should be no listener after release",
            0, cfgMgr.getPropertyChangeListeners().length);

      }
    });

  }




  // @Test
  // public void testBindingPropertyRTUConfigLoaded2() {
  // Application app = Application.getInstance();
  // OptionsManager.getInstance().initalise(app);
  // PointStatusPage page = new PointStatusPage(app, cfgMgr,
  // PointStatusPage.VIEW_TYPE_VIRTUAL);
  // cfgMgr.setRtuConfigLoaded(true);
  // JFrame frame = new JFrame();
  // frame.setSize(200, 200);
  // frame.setContentPane(page.getContent());
  // frame.setVisible(true);
  //
  // cfgMgr.setRtuConfigLoaded(true);
  // cfgMgr.setRtuConfigLoaded(false);
  // cfgMgr.setRtuConfigLoaded(true);
  // cfgMgr.setRtuConfigLoaded(false);
  // }

}
