/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.io.File;
import java.net.URL;
import java.util.Calendar;

import org.junit.Assert;

import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.config.task.ConfigFileReadTask;
import com.lucy.g3.configtool.rtu.config.task.ConfigFileWriteTask;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.test.support.CommsTestSupport;


/**
 * Testing configuration file reading/writing task.
 */
public class ConfigFileTaskTest  {

  private G3RTU g3RTU;


  @Before
  public void setUp() throws Exception {
    g3RTU = G3RTUFactory.createDefault(CommsTestSupport.HOST);
  }

  @After
  public void tearDown() throws Exception {
  }

  private static String mark = null;


  @Ignore // This is a manual test
  @Test
  public void testWriteConfigFile() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    final URL config = ConfigFileTaskTest.class.getClassLoader().getResource("sampleConfig.xml");
    Assert.assertNotNull(config);
    File file = new File(config.toURI());
    IConfig data = new XmlDataReader(null).readFile(file);
    mark = Calendar.getInstance().getTime().toString();
    GeneralConfig general = data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
    general.setConfigDescription(mark);
    ConfigFileWriteTask task = new ConfigFileWriteTask(Application.getInstance(), data, true);
    task.execute();
    task.get();// wait
    general = data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
    System.out.println("Configuration has been written. Description:" + general.getConfigDescription());
  }

  @Test
  public void testReadConfigFile() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    for (int i = 0; i < 3; i++) {
      System.out.println("\nTest Index: " + i);

      ConfigFileReadTask task = new ConfigFileReadTask(Application.getInstance(), g3RTU.getComms().getFileTransfer());
      task.execute();
      IConfig[] data = task.get();// wait
      GeneralConfig general = data[0].getConfigModule(GeneralConfig.CONFIG_MODULE_ID);
      System.err.println("Configuration has been read. Mark:" + general.getConfigDescription());

      // Check if the read file is the same as the written file.
      if (mark != null) {
       Assert.assertTrue(mark.equals(general.getConfigDescription()));
      }
    }
  }


 
}

