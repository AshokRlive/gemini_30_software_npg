/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.Component;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * Wizard page. Generate virtual points.
 */
class NewConfigStep3 extends WizardPage {

  public static final String KEY_NAME_GEN_VPOINT_DEF = "genDefVPoint";
  public static final String KEY_NAME_GEN_VPOINT_FULL = "genFullVPoint";
  public static final String KEY_NAME_GEN_VPOINT_NONE = "genNoneVPoint";


  public NewConfigStep3() {
    initComponents();
    initComponentNames();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;
    if (result == null) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE);
    }
    return result;
  }

  public static String getDescription() {
    return "Virtual Points";
  }

  /**
   * Give a name to the component so its value can be accessed when producing
   * wizard result.
   *
   * @see NewConfigFromScratchWizard.NewConfigResult
   */
  private void initComponentNames() {
    this.radioButtonDefault.setName(KEY_NAME_GEN_VPOINT_DEF);
    this.radioButtonFull.setName(KEY_NAME_GEN_VPOINT_FULL);
    this.radioButtonNone.setName(KEY_NAME_GEN_VPOINT_NONE);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    this.label1 = new JLabel();
    this.radioButtonDefault = new JRadioButton();
    this.radioButtonFull = new JRadioButton();
    this.radioButtonNone = new JRadioButton();

    // ======== this ========
    setLayout(new FormLayout(
        "5dlu, $lcgap, default, $lcgap, [80dlu,default]:grow",
        "default, $rgap, 2*(default, $lgap), default"));

    // ---- label1 ----
    this.label1.setText("How many virtual points do you want to generate?");
    add(this.label1, CC.xywh(1, 1, 5, 1));

    // ---- radioButtonDefault ----
    this.radioButtonDefault.setText("Default (Recommended)");
    this.radioButtonDefault.setSelected(true);
    add(this.radioButtonDefault, CC.xy(3, 3));

    // ---- radioButtonFull ----
    this.radioButtonFull.setText("Full (For testing purpose only)");
    add(this.radioButtonFull, CC.xy(3, 5));

    // ---- radioButtonNone ----
    this.radioButtonNone.setText("None");
    add(this.radioButtonNone, CC.xy(3, 7));

    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(this.radioButtonDefault);
    buttonGroup1.add(this.radioButtonFull);
    buttonGroup1.add(this.radioButtonNone);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel label1;
  private JRadioButton radioButtonDefault;
  private JRadioButton radioButtonFull;
  private JRadioButton radioButtonNone;


  // JFormDesigner - End of variables declaration //GEN-END:variables
}
