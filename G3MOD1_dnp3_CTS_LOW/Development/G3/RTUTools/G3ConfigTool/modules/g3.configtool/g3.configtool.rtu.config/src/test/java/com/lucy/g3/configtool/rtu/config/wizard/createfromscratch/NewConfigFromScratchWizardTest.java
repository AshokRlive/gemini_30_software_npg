/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import javax.swing.JDialog;

import org.fest.swing.annotation.GUITest;
import org.fest.swing.core.matcher.JButtonMatcher;
import org.fest.swing.edt.GuiActionRunner;
import org.fest.swing.edt.GuiTask;
import org.fest.swing.finder.WindowFinder;
import org.fest.swing.fixture.DialogFixture;
import org.fest.swing.junit.testcase.FestSwingJUnitTestCase;
import org.fest.swing.junit.v4_5.runner.GUITestRunner;
import org.fest.swing.launcher.ApplicationLauncher;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.lucy.g3.configtool.rtu.config.wizard.createfromscratch.NewConfigStep1;


//These two annotation are required for taking screen shot when fails.
/**
 * The Class NewConfigFromScratchWizardTest.
 */
@GUITest
@RunWith(GUITestRunner.class)
@Ignore
// Disabled temporarily since this test case always fails in Linux.
public class NewConfigFromScratchWizardTest extends FestSwingJUnitTestCase {

  private DialogFixture window;


  @Override
  protected void onSetUp() {
    // Create a thread is necessary, otherwise the modal wizard dialog will
    // block the test running thread.
    new Thread(new Runnable() {

      @Override
      public void run() {
        GuiActionRunner.execute(new GuiTask() {

          @Override
          protected void executeInEDT() throws Throwable {
            ApplicationLauncher.application(
                NewConfigFromScratchWizardTest.class).start();
          }
        });
      }
    }).start();
  }

  @After
  public void teardown() {
    window.cleanUp();
  }

  @Ignore
  // Disabled temporarily since this test case always fails in Linux.
  @Test
  public void test() {
    window = WindowFinder.findDialog(JDialog.class).withTimeout(4000).using(robot());

    window.button(JButtonMatcher.withText("Finish")).requireDisabled();
    window.button(JButtonMatcher.withText("Next >")).requireEnabled();

    // Test "createFile" selected
    window.checkBox(NewConfigStep1.KEY_NAME_CREATE_FILE).check();
    window.textBox(NewConfigStep1.KEY_NAME_FILE_LOCATION).requireEmpty();
    window.button(JButtonMatcher.withText("Next >")).requireDisabled();
    window.button(JButtonMatcher.withText("< Prev")).requireDisabled();
    window.button(JButtonMatcher.withText("Finish")).requireDisabled();

    // Test "createFile" unselected
    window.checkBox(NewConfigStep1.KEY_NAME_CREATE_FILE).uncheck();
    window.button(JButtonMatcher.withText("Next >")).requireEnabled();
  }

  // public static void main(String[] args) {
  // NewConfigFromScratchWizard.showWizardDialog();
  // }

  // private void createFieldDevice(ConfigData data,Map wData)throws Exception{
  // /*Add example TCP device*/
  // {
  // MMBChannel tcpch = mmb.addChannel("channel-tcp");
  // MMBDevice mbmDev = tcpch.addSession("session 0").getDevice();
  // mbmDev.setCustomName("Simulator");
  // mgr.add(mbmDev);
  //
  // // Configure TCP channel
  // MMBChannelConfig conf = tcpch.getChannelConfig();
  // conf.setChnlName("channel-tcp");
  // conf.setMbType(MBLINK_TYPE.MBLINK_TYPE_TCP);
  // conf.setIpAddress("10.11.1.182");
  // conf.setIpPort(505);
  // conf.setMaxQueueSize(1000);
  //
  // // Configure session
  // MMBSession sesn = mbmDev.getSession();
  // sesn.setDefRespTimeout(1000);
  //
  // // Add example Coil channel for test
  // MMBDeviceChannel sampleCh = new MMBDeviceChannel(ChannelType.DIGITAL_INPUT,
  // 0, mbmDev, "Coils 1");
  // mbmDev.getIomap().addChannel(sampleCh);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_ADDRESS, 1L);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_TYPE,
  // REG_TYPE.COIL_STATUS);
  //
  //
  //
  // // Add example Holding Reg channel for test
  // sampleCh = new MMBDeviceChannel(ChannelType.ANALOG_INPUT, 0, mbmDev,
  // "Holding Register 50");
  // mbmDev.getIomap().addChannel(sampleCh);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_ADDRESS, 50L);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_DATA_TYPE,
  // REG_DATA_TYPE.FLOAT32);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_TYPE,
  // REG_TYPE.HOLDING_REGISTERS);
  //
  // }
  //
  // /*Add example Serial device*/
  // {
  // MMBChannel serialch = mmb.addChannel("channel-serial");
  // MMBDevice mbmDev = serialch.addSession("session 1").getDevice();
  // mbmDev.setCustomName("PowerMeter");
  // mgr.add(mbmDev);
  //
  // // Configure TCP channel
  // MMBChannelConfig conf = serialch.getChannelConfig();
  // conf.setChnlName("channel-serial");
  // conf.setMaxQueueSize(1000);
  // conf.setMbType(MBLINK_TYPE.MBLINK_TYPE_RTU);
  // PortsManager portMgr = data.getPortsManager();
  // ISerialPort port =
  // portMgr.getSerialPort(SERIAL_PORT.SERIAL_PORT_RS485.name());
  // conf.setSerialPort(port);
  // port.setBaudRate(LIN232_BAUD_RATE.LIN232_BAUD_19200);
  // port.setDataBit(LIN232_DATA_BITS.LIN232_DATA_BITS_8);
  // port.setStopBit(LIN232_STOP_BITS.LIN232_STOP_BITS_1);
  // port.setEnabled(true);
  // port.setParity(LIN232_PARITY.LIN232_PARITY_NONE);
  //
  //
  // // Configure session
  // MMBSession sesn = mbmDev.getSession();
  // sesn.setDefRespTimeout(1000);
  //
  // // Add example Holding Reg channel for test
  // MMBDeviceChannel sampleCh = new MMBDeviceChannel(ChannelType.ANALOG_INPUT,
  // 0, mbmDev, "Holding Register 9000");
  // mbmDev.getIomap().addChannel(sampleCh);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_ADDRESS, 9000L);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_DATA_TYPE,
  // REG_DATA_TYPE.FLOAT32);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_TYPE,
  // REG_TYPE.HOLDING_REGISTERS);
  //
  // // Add example Reg channel for test
  // sampleCh = new MMBDeviceChannel(ChannelType.DIGITAL_INPUT, 0, mbmDev,
  // "Holding Register 9000");
  // mbmDev.getIomap().addChannel(sampleCh);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_ADDRESS, 9000L);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_REG_TYPE,
  // REG_TYPE.HOLDING_REGISTERS);
  // sampleCh.setParameter(MMBDeviceChannel.PARAM_BIT_MASK, BIT_MASK.BIT1);
  //
  // }
  // }
}
