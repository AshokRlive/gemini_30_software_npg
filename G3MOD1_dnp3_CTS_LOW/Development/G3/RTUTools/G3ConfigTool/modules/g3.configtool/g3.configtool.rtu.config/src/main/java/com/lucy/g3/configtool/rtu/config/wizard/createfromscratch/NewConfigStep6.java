/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * Wizard page. Choose master protocol stack.
 */
class NewConfigStep6 extends WizardPage {

  public static final String KEY_SELECT_MODBUS = "selectModbus";


  public NewConfigStep6() {
    setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE_OR_FINISH);
    initComponents();
    initComponentNames();
  }

  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;
    if (result == null) {
      setForwardNavigationMode(WizardController.MODE_CAN_CONTINUE_OR_FINISH);
    }
    return result;
  }

  /**
   * Give a name to the component so its value can be accessed when producing
   * wizard result.
   *
   * @see NewConfigFromScratchWizard.NewConfigResult
   */
  private void initComponentNames() {
    radioButtonModBus.setName(KEY_SELECT_MODBUS);
  }

  public static String getDescription() {
    return "Field Device Protocol";
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    JLabel label2 = new JLabel();
    radioButtonModBus = new JCheckBox();

    //======== this ========
    setLayout(new FormLayout(
        "10dlu, default:grow",
        "2*(default, $lgap), default"));

    //---- label2 ----
    label2.setText("Select Field Device Protocol:");
    add(label2, CC.xywh(1, 1, 2, 1));

    //---- radioButtonModBus ----
    radioButtonModBus.setText("ModBus");
    radioButtonModBus.setSelected(true);
    add(radioButtonModBus, CC.xy(2, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox radioButtonModBus;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
