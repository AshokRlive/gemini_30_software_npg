/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;

import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
import com.lucy.g3.gui.common.dialogs.InfoDialog;
import com.lucy.g3.gui.common.dialogs.MessageDialogs;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.transform.G3XsltProcessor;

/**
 * A task for saving configuration file and then transform it into HTML which is
 * printable. The generated HTML will be opened in a browser or dialog.
 */
public class ConfigFilePrintTask extends ConfigFileExportTask {

  private Logger log = Logger.getLogger(ConfigFilePrintTask.class);

  private File outputHTMLFile;


  public ConfigFilePrintTask(Application app, JFrame parent, IConfig data) {
    super(app, parent, data, null);
  }

  @Override
  protected Void doInBackground() throws Exception {
    /* Choose destination */
    outputHTMLFile = DialogUtils.showFileChooseDialog(parent,
        "Export Configuration",
        "Export",
        G3Files.FC_FILTER_G3CFG_HTML);

    if (outputHTMLFile == null) {
      cancel(true);
      return null;
    } else {
      if (MessageDialogs.showOverwriteDialog(parent, outputHTMLFile)) {
        setTarget(new File(outputHTMLFile.getAbsolutePath() + ".xml"));
      }
    }

    super.doInBackground();

    if (isCancelled()) {
      return null;
    }

    File configFile = getTarget();

    // Transform exported XML into HTML
    FileOutputStream outHTMLStream = new FileOutputStream(outputHTMLFile);
    InputStream inputXMLStream;
    if (configFile.getAbsolutePath().endsWith(G3Files.SUFFFIX_GZIP)) {
      byte[] bytes = ZipUtil.ungzip(configFile.getAbsolutePath());
      inputXMLStream = new ByteArrayInputStream(bytes);
    } else {
      inputXMLStream = new FileInputStream(configFile);
    }
    G3XsltProcessor.transfomrConfigToHtml(inputXMLStream, outHTMLStream);

    return null;
  }

  /**
   * Open the output HTML file.
   */
  @Override
  protected void succeeded(Void res) {
    try {
      // Open output file in system default browser
      if (Desktop.isDesktopSupported()) {
        Desktop.getDesktop().open(outputHTMLFile);
        log.debug("Opening output HTML in default browser");
        showSuccessDlg(parent, "Configuration is saved to HTML file:", outputHTMLFile.getAbsolutePath());
      

      // Show output file in a dialog
      } else {
        JEditorPane outputPane = new JEditorPane();
        outputPane.setContentType("text/html");
        outputPane.setEditable(false);
        outputPane.setPage(outputHTMLFile.toURI().toURL());
        InfoDialog.show(parent, "Gemini3 Configuration Print", outputPane);
        log.debug("Opening output HTML in dialog");
      }

    } catch (Exception e2) {
      JOptionPane.showMessageDialog(parent, e2.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  @Override
  protected void finished() {
    super.finished();

    // Delete temporary file
    File target = getTarget();
    if (target != null && target.exists()) {
      target.delete();
    }
  }


}
