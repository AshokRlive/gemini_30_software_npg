/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.util.Collection;
import java.util.Map;

import org.netbeans.spi.wizard.DeferredWizardResult;
import org.netbeans.spi.wizard.ResultProgressHandle;

import com.lucy.g3.rtu.config.clogic.CLogicFactories;
import com.lucy.g3.rtu.config.clogic.CLogicManager;
import com.lucy.g3.rtu.config.clogic.ICLogicType;
import com.lucy.g3.rtu.config.clogic.ICLogicGenerator;
import com.lucy.g3.rtu.config.clogic.offlocalremote.OffLocalRemote;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.device.fields.domain.FieldDeviceFactory;
import com.lucy.g3.rtu.config.device.fields.manager.FieldDeviceManager;
import com.lucy.g3.rtu.config.general.GeneralConfig;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.hmi.manager.HMIScreenManager;
import com.lucy.g3.rtu.config.hmi.ui.IHMIGenerator;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.canmodule.domain.FPIConfig.CTRatio;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANModule;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleFPM;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModulePSM;
import com.lucy.g3.rtu.config.module.iomodule.domain.IOModule;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.rtu.config.module.iomodule.manager.util.IOModuleFinder;
import com.lucy.g3.rtu.config.module.res.ModuleEnums;
import com.lucy.g3.rtu.config.module.shared.ModuleRepository;
import com.lucy.g3.rtu.config.module.shared.domain.ISwitchModule;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.util.ModuleUtility;
import com.lucy.g3.rtu.config.port.PortsManager;
import com.lucy.g3.rtu.config.port.eth.IEthernetPort;
import com.lucy.g3.rtu.config.port.eth.IPConfig;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.ISDNP3Generator;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.SDNP3Channel;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101;
import com.lucy.g3.rtu.config.protocol.iec101.slave.domain.S101Channel;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104;
import com.lucy.g3.rtu.config.protocol.iec104.slave.domain.S104Channel;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IIEC870Generator;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMB;
import com.lucy.g3.rtu.config.protocol.slave.shared.manager.ScadaProtocolManager;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointUtility;
import com.lucy.g3.rtu.config.virtualpoint.standard.ui.page.IVirtualPointGenerator;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataWriter;
import com.lucy.g3.xml.gen.common.DNP3Enum.DNPLINK_NETWORK_TYPE;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * Generate a ConfigData instance based on user's preference in wizard dialog.
 */
@SuppressWarnings("rawtypes")
class NewConfigResult extends DeferredWizardResult {

  private ConfigGeneratorRegistry generators = ConfigGeneratorRegistry.getInstance();


  public NewConfigResult(Map wizardData, boolean abortable) {
    super(abortable);
  }

  private void createGenerl(IConfig data, Map wData) throws Exception {
    GeneralConfig general = data.getConfigModule(GeneralConfig.CONFIG_MODULE_ID);

    // Set site name
    String siteName = (String) wData.get(NewConfigStep1.KEY_NAME_SITE_NAME);
    general.setSiteName(siteName.trim());

    // Set version
    String configVersion = (String) wData.get(NewConfigStep1.KEY_NAME_CONFIG_VERSION);
    general.setConfigVersion(configVersion);

    // Set description
    String configDescritpion = (String) wData.get(NewConfigStep1.KEY_NAME_CONFIG_DESP);
    general.setConfigDescription(configDescritpion);

    // Set Backplane type
    CANModuleManager canman = data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);
    canman.setBackplane(NewConfigStep2.backplaneType(wData));
  }

  private void createModules(IConfig data, Map wData) throws Exception {
    IIOModuleGenerator gen = generators.getGenerator(data, IIOModuleGenerator.class);

    CANModuleManager manager = data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID);

    // Create all modules
    MODULE[] types = ModuleEnums.getSupportedTypes();
    for (int i = 0; i < types.length; i++) {
      gen.createCANModule(manager, types[i], NewConfigStep2.getQuantity(wData, types[i]));
    }

    // Set fan fitted
    Module psm = manager.getModule(MODULE.MODULE_PSM, MODULE_ID.MODULE_ID_0);
    if (psm != null && psm instanceof ModulePSM) {
      Boolean fanfitted = (Boolean) wData.get(NewConfigStep2.KEY_FAN_FITTED);
      ((ModulePSM) psm).getSettings().setFanFitted(fanfitted);
    }

    // Set FPM CTRatio
    Collection<ICANModule> fpms = manager.getModulesByType(MODULE.MODULE_FPM);
    if (fpms != null && !fpms.isEmpty()) {
      for (Module fpm : fpms) {
        CTRatio ratio = (CTRatio) wData.get(NewConfigStep2.KEY_FPM_CTRATIO);
        ((ModuleFPM) fpm).getSettings().getFpi(0).setCtratio(ratio);
        ((ModuleFPM) fpm).getSettings().getFpi(1).setCtratio(ratio);
      }
    }

  }

  private void createStandardVirtualPoints(IConfig data, Map wData) throws Exception {

    if ((Boolean) wData.get(NewConfigStep3.KEY_NAME_GEN_VPOINT_NONE) == Boolean.TRUE) {
      return;
    }

    /* Prepare option */
    GenerateOptions option;
    if (wData.get(NewConfigStep3.KEY_NAME_GEN_VPOINT_FULL) == Boolean.TRUE) {
      option = GenerateOptions.genFullVPoint();
    } else if (wData.get(NewConfigStep3.KEY_NAME_GEN_VPOINT_NONE) == Boolean.TRUE) {
      option = GenerateOptions.genNoPoint();
    } else {
      option = GenerateOptions.genDefaultVPoint();
    }

    /* Add points for each module's channels */
    ModuleRepository mrepo = data.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    Collection<IOModule> mlist = IOModuleFinder.findIOModules(mrepo.getAllModules());
    IOModule[] allmodules = mlist.toArray(new IOModule[mlist.size()]);

    IIOModuleGenerator gen = generators.getGenerator(data, IIOModuleGenerator.class);
    gen.createVirtualPoints(option, allmodules);
  }

  private void createControlLogic(IConfig data, Map wdata) throws Exception {
    // Create mandatory clogic
    CLogicManager clogic = data.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    clogic.add(new OffLocalRemote());

    Boolean generateCLogic = (Boolean) wdata.get(NewConfigStep4.KEY_GENERATE_CLOGIC);
    Boolean generateDefCLogic = (Boolean) wdata.get(NewConfigStep4.KEY_GEN_DEFAULT_CLOGIC);

    if (generateCLogic != Boolean.FALSE) {

      if (generateDefCLogic != Boolean.FALSE) {
        // Generate default control logic
        createDefaultLogic(data, wdata);
      } else {
        // Generate custom selected control logic
        createCustomLogic(data, wdata);
      }
    }
  }

  private void createDefaultLogic(IConfig data, Map wdata) throws Exception {
    ModuleRepository repo = data.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    Collection<Module> mlist = repo.getAllModules();
    Module[] allmodules = mlist.toArray(new Module[mlist.size()]);
    IIOModuleGenerator gen = generators.getGenerator(data, IIOModuleGenerator.class);
    gen.createCLogic(allmodules);
  }

  private void createCustomLogic(IConfig data, Map wdata) throws Exception {
    ModuleRepository repo = data.getConfigModule(ModuleRepository.CONFIG_MODULE_ID);
    CLogicManager manager = data.getConfigModule(CLogicManager.CONFIG_MODULE_ID);
    ModulePSM psm = (ModulePSM) repo.getModule(MODULE.MODULE_PSM, MODULE_ID.MODULE_ID_0);
    ICLogicGenerator gen = generators.getGenerator(data, ICLogicGenerator.class);
    IIOModuleGenerator mgen = generators.getGenerator(data, IIOModuleGenerator.class);

    /* Fan Test Logic */
    Boolean checked = (Boolean) wdata.get(NewConfigStep4.KEY_CUSTOM_FAN_TEST);
    if (checked != Boolean.FALSE && psm != null) {
      gen.createFanTestLogic(psm.getFanTestChannel());
    }

    /* DummySwitchLogic */
    checked = (Boolean) wdata.get(NewConfigStep4.KEY_CUSTOM_DUMMY_CONTROL);
    if (checked != Boolean.FALSE) {
      manager.add(CLogicFactories.getFactory(ICLogicType.DSL).createLogic());
    }

    /* SwitchGearLogic for SCM */
    checked = (Boolean) wdata.get(NewConfigStep4.KEY_CUSTOM_SWITCH_CONTROL);
    if (checked != Boolean.FALSE) {
      Collection<Module> scms = repo.getModulesByType(MODULE.MODULE_SCM, MODULE.MODULE_DSM, MODULE.MODULE_SCM_MK2);
      for (Module scm : scms) {
        mgen.createSwitchLogic((ISwitchModule) scm);
      }
    }

    /* Digital Output Logic */
    checked = (Boolean) wdata.get(NewConfigStep4.KEY_CUSTOM_DIGITAL_OUTPUT);
    if (checked != Boolean.FALSE) {
      Collection<Module> ioms = ModuleUtility.getModulesByType(data, MODULE.MODULE_IOM);
      mgen.createCLogic(ioms.toArray(new Module[ioms.size()]));
    }
  }

  private void createProtocol(IConfig data, Map WData) throws Exception {
    if (WData.get(NewConfigStep5.KEY_SELECT_SDNP3) == Boolean.TRUE) {
      createSNP3(data, WData);
    }

    if (WData.get(NewConfigStep5.KEY_SELECT_S104) == Boolean.TRUE) {
      createS104(data, WData);
    }

    if (WData.get(NewConfigStep5.KEY_SELECT_S101) == Boolean.TRUE) {
      createS101(data, WData);
    }

    /* Create ModBus protocol */
    if (WData.get(NewConfigStep6.KEY_SELECT_MODBUS) != Boolean.FALSE) {
      FieldDeviceManager manager = data.getConfigModule(FieldDeviceManager.CONFIG_MODULE_ID);
      createMMB(manager);
    }
  }

  private void createMMB(FieldDeviceManager mgr) {
    MMB mmb = new MMB(FieldDeviceFactory.INSTANCE);
    mgr.getMasterProtoManager().add(mmb);
  }

  private void createS104(IConfig data, Map WData) {
    S104 s104 = new S104();
    S104Channel ch = s104.addChannel("Channel-TCP");
    ch.addSession("SCADA-A");
    ScadaProtocolManager manager = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    manager.add(s104);

    /* Prepare option */
    GenerateOptions option;
    Boolean genNoneDNP3Point = (Boolean) WData.get(NewConfigStep5.KEY_GEN_PROTOCOL_POINTS_NONE);
    option = (genNoneDNP3Point == Boolean.TRUE)
        ? GenerateOptions.genNoPoint()
        : GenerateOptions.genDefaultVPoint();

    IIEC870Generator gen = generators.getGenerator(data, IIEC870Generator.class);
    gen.genInputPointsForAllSessions(option, s104, VirtualPointUtility.getAllPoints(data));
    gen.genOutputPointsForAllSessions(option, s104, CLogicUtility.getAllCLogic(data));
  }

  private void createS101(IConfig data, Map WData) {
    S101 s101 = new S101();
    S101Channel ch = s101.addChannel("Channel-Serial");
    ch.addSession("SCADA-A");
    ScadaProtocolManager manager = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    manager.add(s101);

    /* Prepare option */
    GenerateOptions option;
    Boolean genNoneDNP3Point = (Boolean) WData.get(NewConfigStep5.KEY_GEN_PROTOCOL_POINTS_NONE);
    option = (genNoneDNP3Point == Boolean.TRUE)
        ? GenerateOptions.genNoPoint()
        : GenerateOptions.genDefaultVPoint();

    IIEC870Generator gen = generators.getGenerator(data, IIEC870Generator.class);
    gen.genInputPointsForAllSessions(option, s101, VirtualPointUtility.getAllPoints(data));
    gen.genOutputPointsForAllSessions(option, s101, CLogicUtility.getAllCLogic(data));
  }

  private void createSNP3(IConfig data, Map WData) {
    SDNP3 sdnp3 = new SDNP3();
    SDNP3Channel ch = sdnp3.addChannel("Channel-TCP");
    ch.getChannelConfig().setNetworkType(DNPLINK_NETWORK_TYPE.DNPLINK_NETWORK_TCP_ONLY);
    ch.addSession("SCADA-A");
    ScadaProtocolManager manager = data.getConfigModule(ScadaProtocolManager.CONFIG_MODULE_ID);
    manager.add(sdnp3);

    /* Prepare option */
    GenerateOptions option;
    Boolean genNoneDNP3Point = (Boolean) WData.get(NewConfigStep5.KEY_GEN_PROTOCOL_POINTS_NONE);
    option = (genNoneDNP3Point == Boolean.TRUE)
        ? GenerateOptions.genNoPoint()
        : GenerateOptions.genDefaultVPoint();

    ISDNP3Generator gen = generators.getGenerator(data, ISDNP3Generator.class);
    gen.genInputPointsForAllSessions(option, sdnp3, VirtualPointUtility.getAllPoints(data));
    gen.genOutputPointsForAllSessions(option, sdnp3, CLogicUtility.getAllCLogic(data));
  }

  private void createHMIMenu(IConfig data, Map wData) throws Exception {
    // Generate HMI menu
    if (NewConfigStep2.createHMIMenu(wData)) {
      IHMIGenerator gen = generators.getGenerator(data, IHMIGenerator.class);
      HMIScreenManager manager = data.getConfigModule(HMIScreenManager.CONFIG_MODULE_ID);
      gen.generateScreensFromScratch(manager);
    }
  }

  /* Create ethernet 0 in config */
  private void createEthernet0(IConfig data, Map map) throws Exception {

    PortsManager pm = data.getConfigModule(PortsManager.CONFIG_MODULE_ID);
    IEthernetPort ep = pm.getEthPort(ETHERNET_PORT.ETHERNET_PORT_CONTROL);

    boolean enabled = (Boolean) map.get(NewConfigStep1a.KEY_STATE);
    if (enabled) {
      ep.setConfigEnabled(true);
      ep.setEnabled(true);

      IPConfig ip = ep.getIpConfig();
      ip.setIpAddress((String) map.get(NewConfigStep1a.KEY_ADDRESS));
      ip.setSubmask((String) map.get(NewConfigStep1a.KEY_SUBMASK));
      ip.setGateway((String) map.get(NewConfigStep1a.KEY_GATEWAY));
    } else {
      ep.setConfigEnabled(false);
    }
  }

  @Override
  public void start(Map wizardData, ResultProgressHandle progress) {
    final int totalSteps = 10;
    try {
      progress.setBusy("Generating Configuration...");
      final IConfig data = ConfigFactory.getInstance().create();

      createGenerl(data, wizardData);
      progress.setProgress(1, totalSteps);

      createModules(data, wizardData);
      progress.setProgress(2, totalSteps);

      createLabelSet(data, wizardData);

      createStandardVirtualPoints(data, wizardData);
      progress.setProgress(3, totalSteps);

      createControlLogic(data, wizardData);
      progress.setProgress(4, totalSteps);

      createProtocol(data, wizardData);
      progress.setProgress(5, totalSteps);

      createHMIMenu(data, wizardData);
      progress.setProgress(7, totalSteps);

      createEthernet0(data, wizardData);
      progress.setProgress(8, totalSteps);

      progress.setProgress(9, totalSteps);
      Boolean genFile = (Boolean) wizardData.get(NewConfigStep1.KEY_NAME_CREATE_FILE);
      String location = (String) wizardData.get(NewConfigStep1.KEY_NAME_FILE_LOCATION);
      if (genFile == Boolean.TRUE) {
        new XmlDataWriter(data, null).saveToFile(location);
        GeneralConfigUtility.setFilePath(data, location);
      }

      progress.setProgress(10, totalSteps);

      progress.finished(data); // progress.finished(createSummary (wizardData));

    } catch (Exception e) {
      e.printStackTrace();
      progress.failed(e.getMessage(), true);
      return;
    }
  }

  private void createLabelSet(IConfig data, Map wizardData) {
    IVirtualPointGenerator gen = generators.getGenerator(data, IVirtualPointGenerator.class);
    VirtualPointManager manager = data.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
    gen.createDefaultLabelSets(manager.getValueLabelManager());
  }

  @Override
  public String toString() {
    return "ScratchWizard.ConfigDataProducer";
  }

  // private Object createSummary (Map wizardData) {
  // //We will just return the wizard data here. In real life we would
  // //a compute a result here
  // Summary summary;
  //
  // Object[][] data = new Object[wizardData.size()][2];
  // int ix = 0;
  // for (Iterator i=wizardData.keySet().iterator(); i.hasNext();) {
  // Object key = i.next();
  // Object val = wizardData.get(key);
  // data[ix][0] = key;
  // data[ix][1] = val;
  // ix++;
  // }
  // TableModel mdl = new DefaultTableModel (data,
  // new String[] { "Key", "Value"});
  // summary = Summary.create(new JScrollPane(new JTable(mdl)), wizardData);
  // return summary == null ? (Object) wizardData : summary;
  // }

}
