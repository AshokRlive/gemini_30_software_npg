/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.task;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.DefaultInputBlocker;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.error.ErrorInfo;

import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.rtu.comms.protocol.G3Protocol;
import com.lucy.g3.rtu.comms.protocol.exceptions.ProtocolException.NackException;
import com.lucy.g3.rtu.comms.service.filetransfer.FileTransfer;
import com.lucy.g3.rtu.config.general.GeneralConfigUtility;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.xml.serialization.XmlDataReader;
import com.lucy.g3.rtu.config.xml.serialization.XmlParserInvoker;
import com.lucy.g3.rtu.config.xml.validation.XmlValidator;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.schema.support.RTUConfigSchemaResource;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_ACKNACKCODE;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * A task for reading configuration file from RTU device and saving it to
 * {@code IConfig} instance.
 */
public class ConfigFileReadTask extends Task<IConfig[], Void> implements XmlParserInvoker {

  private Logger log = Logger.getLogger(ConfigFileReadTask.class);

  private final String configFileName;

  private final boolean compressed = true;

  private final CTH_TRANSFERTYPE type = CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_G3CONFIG;

  private final JFrame parent;

  private File configFile;

  private final FileReadingTask fileReadTask;


  public ConfigFileReadTask(Application app, FileTransfer fileTransfer) {
    super(app);

    if (app instanceof SingleFrameApplication) {
      parent = ((SingleFrameApplication) app).getMainFrame();
    } else {
      parent = null;
    }

    if (compressed) {
      configFileName = G3Files.DEF_G3CONFIG_FNAME + G3Files.SUFFFIX_GZIP;
    } else {
      configFileName = G3Files.DEF_G3CONFIG_FNAME;
    }

    setUserCanCancel(true);

    setInputBlocker(new DefaultInputBlocker(this, BlockingScope.APPLICATION, parent, null));

    fileReadTask = new FileReadingTask(app, fileTransfer, type, configFileName, null);
    /*
     * It is important to set the sub task blocker to null, if not, an exception
     * will be thrown from calling the sub task.
     */
    fileReadTask.setInputBlocker(null);
  }

  @Override
  protected IConfig[] doInBackground() throws Exception {
    log.info("Start reading config file");
    IConfig data = null;

    TaskService ts = getTaskService();
    if (ts != null) {
      ts.execute(fileReadTask);
    } else {
      fileReadTask.execute();
    }

    configFile = fileReadTask.get();

    if (configFile == null || isCancelled()) {
      return null;
    }

    if (!configFile.exists()) {
      log.error("Received config file is not found: " + configFile);
      throw new IOException("File transferring failed");
    }

    setMessage("Parsing configuration file...");

    // Extract file with GZIP
    if (compressed) {
      String outputFile = configFile.getPath() + ".xml";
      log.info("Unzipping config file to: " + outputFile);
      ZipUtil.ungzip(configFile.getPath(), outputFile);
      configFile.delete();// delete gzip file
      configFile = new File(outputFile);
    }

    // Validate configuration file
    try {
      setMessage("Validating config file against schema...");
      new XmlValidator(RTUConfigSchemaResource.FILE_PATH).validate(configFile.getPath());
      log.info("XML configuration validation passed");
    } catch (IOException e0) {
      log.error("XML configuration validation failed. ", e0);
      throw e0;
    } catch (final Exception e) {
      /* Invalid configuration file. Show detail dialog */
      if (!isCancelled()) {
        log.error("G3 configuration validation failed", e);
        if(XmlValidator.showConfirmDialog(parent, e) == false) {
          cancel(true);
          return null;
        }
      } else {
        return null;// User cancelled
      }
    }

    // Parse IConfig from XML
    setMessage("Parsing configuration file...");
    data = new XmlDataReader(this).readFile(configFile);
    GeneralConfigUtility.setFilePath(data, null);// This data is not read from a local file.
    setMessage("Configuration file has been parsed.");

    // TODO needs a deep clone method to do copy
    // Copy configData instance
    IConfig dataCopy = new XmlDataReader(this).readFile(configFile);
    GeneralConfigUtility.setFilePath(dataCopy, null);

    // Delete configFile after imported
    if (!configFile.delete()) {
      log.error("Temp configFile not deleted: " + configFile);
    }

    if (isCancelled()) {
      return null;
    } else {
      setMessage("Configuration has been read from RTU");
      return new IConfig[] { data, dataCopy };
    }
  }

  @Override
  protected void cancelled() {
    String msg = "Configuration reading is cancelled";
    setMessage(msg);
    log.info(msg);
    fileReadTask.cancel(true);
  }

  @Override
  protected void failed(Throwable cause) {
    if (cause instanceof ExecutionException) {
      cause = ((ExecutionException) cause).getCause();
    }

    /* Show no configuration message */
    if (cause != null && cause instanceof NackException
        && ((NackException) cause).nackCode == CTH_ACKNACKCODE.CTH_NACK_NOCONFIG) {

      JOptionPane.showMessageDialog(parent, "No configuration file!",
          "No configuration",
          JOptionPane.INFORMATION_MESSAGE);
      
    } else {
      /* Show failure message */
      String error = "Fail to read configuration file!";
      log.error(error, cause);
      // Show error dialog
      ErrorInfo info = new ErrorInfo("Error",
          error, cause.getMessage(), null, cause, Level.SEVERE, null);
      JXErrorPane.showDialog(parent, info);
    }
  }

  @Override
  public void printMessage(String message) {
    setMessage(message);
  }

  @Override
  public void setProgressValue(int progress) {
    if (progress >= 0 && progress <= 100) {
      setProgress(progress);
    }
  }
}
