/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.awt.Component;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.prompt.PromptSupport;
import org.netbeans.spi.wizard.WizardPage;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.validation.view.ValidationComponentUtils;
import com.lucy.g3.network.support.IPAddress;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.xml.gen.common.MCMConfigEnum.ETHERNET_PORT;

/**
 * The Wizard Page for configuring Eth0 Port.
 */
class NewConfigStep1a extends WizardPage {

  private Logger log = Logger.getLogger(NewConfigStep1a.class);

  public static final String KEY_STATE = "Step1a.STATE";
  public static final String KEY_ADDRESS = "Step1a.ADDR";
  public static final String KEY_GATEWAY = "Step1a.GATE";
  public static final String KEY_SUBMASK = "Step1a.MASK";

  private final NetInfo eth0Info;
  
  /* Constructor */
  public NewConfigStep1a(NetInfo eth0Info) {
    this.eth0Info = eth0Info;
    initComponents();
    initComponentNames();
    updatEnabledState();

    initComponentValues();
  }

  /* Name components for wizard style access */
  private void initComponentNames() {
    state.setName(KEY_STATE);
    tfAddr.setName(KEY_ADDRESS);
    tfSubmask.setName(KEY_SUBMASK);
    tfGateway.setName(KEY_GATEWAY);
  }

  /* Get the IP config from current config */
  private void initComponentValues() {
    if (eth0Info != null) {
      tfAddr.setText(eth0Info.getIp());
      tfSubmask.setText(eth0Info.getMask());
      tfGateway.setText(eth0Info.getGateway());
    }
  }

  /* Validation */
  @Override
  protected String validateContents(Component component, Object event) {
    String result = null;
    ValidationComponentUtils.setMandatoryBackground(tfAddr);
    ValidationComponentUtils.setMandatoryBackground(tfSubmask);
    ValidationComponentUtils.setMandatoryBackground(tfGateway);

    if (state.isSelected()) {
      if (!IPAddress.validateIPAddress(tfAddr.getText())) {
        result = "IP Address is invalid";
        ValidationComponentUtils.setErrorBackground(tfAddr);

      } else if (!IPAddress.validateIPAddress(tfSubmask.getText())) {
        result = "Submask is invalid";
        ValidationComponentUtils.setErrorBackground(tfSubmask);

      } else if (!IPAddress.validateIPAddress(tfGateway.getText(), true)) {
        result = "Gateway is invalid";
        ValidationComponentUtils.setErrorBackground(tfGateway);
      }
    }

    // Debug
    Map<?, ?> data = super.getWizardDataMap();
    log.debug("Configured:" + data.get(KEY_STATE));
    log.debug("IP:" + data.get(KEY_ADDRESS));
    log.debug("Gateway:" + data.get(KEY_GATEWAY));
    log.debug("Submask:" + data.get(KEY_SUBMASK));

    return result;
  }

  /* Get the page description */
  public static String getDescription() {
    return "IP Configuration";
  }

  /* Set UI component enable by checkbox */
  private void updatEnabledState() {
    panelIP.setVisible(state.isSelected());
  }

  /* Component initialisation - generated code, do not edit */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    state = new JCheckBox();
    panelIP = new JPanel();
    label5 = new JLabel();
    tfAddr = new JTextField();
    label1 = new JLabel();
    tfSubmask = new JTextField();
    label4 = new JLabel();
    tfGateway = new JTextField();

    // ======== this ========
    setLayout(new FormLayout(
        "default:grow",
        "default, $ugap, default"));

    // ---- state ----
    state.setText("Configure Port");
    state.addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(ChangeEvent e) {
        updatEnabledState();
      }
    });
    add(state, CC.xy(1, 1));

    // ======== panelIP ========
    {
      panelIP.setLayout(new FormLayout(
          "default, $lcgap, left:default, $lcgap, [80dlu,default]",
          "default, $lgap, default, $rgap, default"));

      // ---- label5 ----
      label5.setText("IP Address:");
      panelIP.add(label5, CC.xy(3, 1, CC.LEFT, CC.DEFAULT));
      panelIP.add(tfAddr, CC.xy(5, 1));

      // ---- label1 ----
      label1.setText("Submask:");
      panelIP.add(label1, CC.xy(3, 3, CC.LEFT, CC.DEFAULT));
      panelIP.add(tfSubmask, CC.xy(5, 3));

      // ---- label4 ----
      label4.setText("Gateway:");
      panelIP.add(label4, CC.xy(3, 5));
      panelIP.add(tfGateway, CC.xy(5, 5));
    }
    add(panelIP, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents

    PromptSupport.setPrompt("Enter IP address", tfAddr);
    PromptSupport.setPrompt("Enter subnet mask", tfSubmask);
    state.setText("Configure Port \"" + ETHERNET_PORT.ETHERNET_PORT_CONTROL.getDescription() + "\"");
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JCheckBox state;
  private JPanel panelIP;
  private JLabel label5;
  private JTextField tfAddr;
  private JLabel label1;
  private JTextField tfSubmask;
  private JLabel label4;
  private JTextField tfGateway;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
