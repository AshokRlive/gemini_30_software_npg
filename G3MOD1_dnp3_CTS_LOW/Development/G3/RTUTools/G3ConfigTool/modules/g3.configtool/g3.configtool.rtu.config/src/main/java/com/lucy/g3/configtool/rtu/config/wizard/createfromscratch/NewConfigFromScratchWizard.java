/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.config.wizard.createfromscratch;

import java.util.Map;

import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardException;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPage.WizardResultProducer;

import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.gui.common.wizards.WizardUtils;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.service.rtu.info.NetInfo;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * <p>
 * Wizard producer for creating a ConfigData instance based on the user selected
 * options in wizard panels.
 * </p>
 * {@linkplain #createWizard()} or {@linkplain #showWizardDialog()} can be used
 * to create a wizard or wizard dialog.
 */
public class NewConfigFromScratchWizard implements WizardResultProducer {

  @Override
  public Object finish(@SuppressWarnings("rawtypes") Map wizardData) throws WizardException {
    return new NewConfigResult(wizardData, true);
  }

  @Override
  public boolean cancel(@SuppressWarnings("rawtypes") Map settings) {
    return true;
  }

  public static Wizard createWizard(NetInfo eth0Info) {
    return WizardPage.createWizard(
        "New Configuration",
        getPages(eth0Info),
        new NewConfigFromScratchWizard()
        );
  }

  public static WizardPage[] getPages(NetInfo eth0Info) {
    return new WizardPage[] {
        new NewConfigStep1(),
        new NewConfigStep1a(eth0Info),
        new NewConfigStep2(),
        new NewConfigStep3(),
        new NewConfigStep4(),
        new NewConfigStep5(),
        new NewConfigStep6()
    };
  }

  public static IConfig showWizardDialog(NetInfo eth0Info) {
    return (IConfig) WizardDisplayer.showWizard(createWizard(eth0Info), 
        WizardUtils.createRect(WindowUtils.getMainFrame(), 600, 500), Helper.createHelpAction(NewConfigFromScratchWizard.class), null);
  }
}
