/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;

import com.jgoodies.common.base.Preconditions;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.widgets.ext.swing.table.ToolTipHeader;
import com.lucy.g3.gui.common.widgets.utils.ResourceUtils;
import com.lucy.g3.gui.common.widgets.utils.UIUtils;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.log.LogEntry;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogsAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * A dialog for setting the log levels of RTU system.
 */
public class LogLevelDialog extends AbstractDialog {

  private Logger log = Logger.getLogger(LogLevelDialog.class);

  private final LogSettingTableModel logLevelTableModel = new LogSettingTableModel();

  private JXTable logLevelTable;

  private JComboBox comboLoglevels;

  private final LogLevelDialogInvoker invoker;


  public LogLevelDialog(LogLevelDialogInvoker invoker) {
    super(invoker.getDialogParent());
    this.invoker = Preconditions.checkNotNull(invoker, "invoker is null");

    setName("LogLevelSettingDialog");
    setTitle(ResourceUtils.getString(getClass(), "dialog.title"));
    setPreferredSize(new Dimension(400, 300));
    pack();

    refresh();// Read all log levels
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    JPanel contentPanel = new JPanel(new BorderLayout(0, 5));

    // ====== Build Log Level Table ======
    logLevelTable = new JXTable(logLevelTableModel);
    logLevelTable.setName("LogLevelTable");

    // Header
    ToolTipHeader header = new ToolTipHeader(logLevelTable.getColumnModel());
    header.setToolTipStrings(LogSettingTableModel.columnToolTips);
    header.setToolTipText("Default ToolTip TEXT");
    header.setFont(header.getFont().deriveFont(Font.BOLD));
    // Header font & alignment
    ((DefaultTableCellRenderer) header.getDefaultRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
    logLevelTable.setTableHeader(header);

    // Set table cell editor
    JComboBox editorComb = new JComboBox(LogEntry.getAllLogLevels(getState()));
    logLevelTable.setDefaultEditor(String.class, new DefaultCellEditor(editorComb));

    // Highlighter // Set column 0 background color
    ColorHighlighter highlighter = new ColorHighlighter(new HighlightPredicate.ColumnHighlightPredicate(0),
        new Color(180, 180, 180, 80), null);
    logLevelTable.setHighlighters(highlighter);

    JScrollPane sc = new JScrollPane(logLevelTable);
    UIUtils.increaseScrollSpeed(sc);
    logLevelTable.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
    contentPanel.add(BorderLayout.CENTER, sc);

    // Set pop menu
    JPopupMenu popMenu = new JPopupMenu();
    popMenu.add(getAction("refresh"));
    logLevelTable.setComponentPopupMenu(popMenu);

    // ====== Build Log Level setting components ======
    JPanel settingPanel = new JPanel(new FlowLayout());
    comboLoglevels = new JComboBox(getAllLogLevels());
    comboLoglevels.setFocusable(false);
    comboLoglevels.setSelectedItem(null);
    comboLoglevels.setPreferredSize(new Dimension(150, 20));
    comboLoglevels.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        setAllLevels();
      }
    });
    settingPanel.add(new JLabel("All Levels: "));
    settingPanel.add(comboLoglevels);
    contentPanel.add(BorderLayout.NORTH, settingPanel);

    return contentPanel;
  }

  @Override
  protected JPanel createButtonPanel() {
    return createOKCancelButtonPanel();
  }

  /**
   * Read log levels from RTU and refresh table.
   */
  @org.jdesktop.application.Action
  public void refresh() {
    getTS().execute(new GetLogLevelTask(invoker.getApp(),
        logLevelTableModel, CommsUtil.getRTULogsAPI(G3RTUFactory.getComms())));
  }

  @Override
  public void ok() {
    applyAllLevels();
    super.ok();
  }

  private void setAllLevels() {
    if (comboLoglevels == null) {
      return;
    }

    Object selection = comboLoglevels.getSelectedItem();

    if (selection != null) {
      log.info("Set log levels of all subsystem to:" + selection);

      int rows = logLevelTableModel.getRowCount();
      for (int i = 0; i < rows; i++) {
        logLevelTableModel.setValueAt(selection.toString(),
            i, LogSettingTableModel.LOG_LEVEL_COLUMN);
      }
    }
  }

  private void applyAllLevels() {
    getTS().execute(new SetLogLevelTask(invoker.getApp(),
        logLevelTableModel, CommsUtil.getRTULogsAPI(G3RTUFactory.getComms())));
  }

  private TaskService getTS() {
    return invoker.getLogSettingTaskService();
  }

  private CTH_RUNNINGAPP getState() {
    return invoker.getCurrentState();
  }

  private Object[] getAllLogLevels() {
    if (getState() == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      return com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL.values();
    } else {
      // Hide debug&info level that may slow down the application of RTU.
      com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.values();
      levels = ArrayUtils.removeElements(levels, 
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.LOG_LEVEL_DEBUG,
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.LOG_LEVEL_INFO);
      return levels;
      }
  }


  private static class GetLogLevelTask extends Task<String[][], Void> {

    private final LogSettingTableModel model;
    private final RTULogsAPI cmd;


    public GetLogLevelTask(Application application, LogSettingTableModel m, RTULogsAPI cmd) {
      super(application);
      this.model = m;
      this.cmd = cmd;
      setTitle("Read Log Level");
      setDescription("Read RTU subsystem log levels");
    }

    @Override
    protected void succeeded(String[][] result) {
      model.setDataVector(result, LogSettingTableModel.COLUMNS);
    }

    @Override
    protected void failed(Throwable cause) {
      message("Fail to read log levels");
      model.setDataVector(new Short[0][2], LogSettingTableModel.COLUMNS);
    }

    @Override
    protected String[][] doInBackground() throws Exception {
      return cmd.cmdGetLogLevel();
    }
  }

  private static class SetLogLevelTask extends Task<Void, Void> {

    private Logger log = Logger.getLogger(SetLogLevelTask.class);
    private LogSettingTableModel model;

    private String subSystem;
    private String level;

    private final RTULogsAPI cmd;


    public SetLogLevelTask(Application application, LogSettingTableModel model, RTULogsAPI cmd) {
      super(application);
      this.model = model;
      this.cmd = cmd;

      setTitle("Set Log Level");
      setDescription("Set RTU subsystem log levels");
    }

    @Override
    protected Void doInBackground() throws Exception {
      int rows = model.getRowCount();

      // TODO Set only the changed value.
      for (int i = 0; i < rows; i++) {
        subSystem = (String) model.getValueAt(i, LogSettingTableModel.SYSTEM_COLUMN);
        level = (String) model.getValueAt(i, LogSettingTableModel.LOG_LEVEL_COLUMN);

        if (subSystem != null && level != null) {
          cmd.cmdSetLogLevel(subSystem, level);
        }
      }
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      String msg = "Fail to set the log level of sub system \"" + subSystem
          + "\"  to \"" + level + "\"";
      setMessage(msg);
      log.error(msg);
    }

    @Override
    protected void succeeded(Void result) {
      setMessage("Set the log level of sub system successfully!");
    }

  }

  private static class LogSettingTableModel extends DefaultTableModel {

    // Load column names from properties resource
    private static final String[] COLUMNS = new String[] {
        ResourceUtils.getString(LogLevelDialog.class, "column0.text"),
        ResourceUtils.getString(LogLevelDialog.class, "column1.text") };

    // Load column tips from properties resource
    private static final String[] columnToolTips = {
        ResourceUtils.getString(LogLevelDialog.class, "column0.tooltip"),
        ResourceUtils.getString(LogLevelDialog.class, "column1.tooltip")
    };

    static final int SYSTEM_COLUMN = 0;

    static final int LOG_LEVEL_COLUMN = 1;


    public LogSettingTableModel() {
      super(COLUMNS, 0);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
      return column == LOG_LEVEL_COLUMN;

    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
      return String.class;
    }
  }

  public interface LogLevelDialogInvoker {

    Application getApp();

    CTH_RUNNINGAPP getCurrentState();


    TaskService getLogSettingTaskService();

    JFrame getDialogParent();
  }
}
