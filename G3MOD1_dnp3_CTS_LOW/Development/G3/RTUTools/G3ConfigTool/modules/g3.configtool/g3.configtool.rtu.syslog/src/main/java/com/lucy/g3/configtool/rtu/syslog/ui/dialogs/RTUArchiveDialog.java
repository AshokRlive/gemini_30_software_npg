/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.ui.dialogs;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.jgoodies.common.base.Strings;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.l2fprod.common.swing.BannerPanel;
import com.lucy.g3.file.names.G3Files;
import com.lucy.g3.gui.common.dialogs.AbstractDialog;
import com.lucy.g3.gui.common.dialogs.DialogUtils;
public class RTUArchiveDialog extends AbstractDialog {

  private static final String TITLE = "Archiving RTU Files";
  private static final String DEFAULT_ARCHIVE_FILE_NAME = "RTUArchive.zip";
  
  public RTUArchiveDialog(Frame owner) {
    super(owner);
    setTitle(TITLE);
    initComponents();
    initEventHandling();
    validateAndUpdateUI();
  }

  public RTUArchiveDialog(Dialog owner) {
    super(owner);
    setTitle(TITLE);
    initComponents();
    initEventHandling();
    validateAndUpdateUI();
  }
  
  private void initEventHandling() {
    tfArchiveFile.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void removeUpdate(DocumentEvent e) {
        validateAndUpdateUI();
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        validateAndUpdateUI();
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        validateAndUpdateUI();
      }
    });
  }

  @Override
  public void ok() {
    String error = validateContent();
    
    if(error == null)
      super.ok();
    else
      JOptionPane.showMessageDialog(this, error, "Error", JOptionPane.ERROR_MESSAGE);
  }

  private String validateContent() {
    String path = tfArchiveFile.getText();
    
    if(Strings.isBlank(path)) {
      return "Please select the file path!" ;
    }else {
      File dest = new File(path).getParentFile();
      if(dest == null || dest.exists() == false)
        return "Invalid destatination path!" ;
    }
    
    if(!cboxSyslog.isSelected()       
       && !cboxConfig       .isSelected()
       && !cboxModuleReg    .isSelected()
       && !cboxCommissioning.isSelected())
      return "No file has been selected for archiving";
    
    return null;
  }

  public File getArchiveDestination() {
    return new File(tfArchiveFile.getText());
  }
  
  public boolean isConfigIncluded() {
    return cboxConfig.isSelected();
  }
  
  public boolean isCommissionIncluded(){
    return cboxCommissioning.isSelected();
  }
  
  public boolean isSyslogIncluded() {
    return cboxSyslog.isSelected();
  }
  
  public boolean isModuleRegIncluded() {
    return cboxModuleReg.isSelected();
  }

  private void btnBrowseActionPerformed(ActionEvent e) {
    File destination = DialogUtils.showFileChooseDialog(this,
        "Select Destination",
        "Select", DEFAULT_ARCHIVE_FILE_NAME,
        G3Files.FC_FILTER_G3_ARCHIVE
        );
    if(destination != null && destination.getParentFile().exists()) {
      tfArchiveFile.setText(destination.getAbsolutePath());
    }
  }

  private void cboxActionPerformed(ActionEvent e) {
    validateAndUpdateUI();
  }

  private void validateAndUpdateUI() {
    String error = validateContent();
    setOkEnabled(error == null);
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    content = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    tfArchiveFile = new JTextField();
    btnBrowse = new JButton();
    label2 = new JLabel();
    cboxSyslog = new JCheckBox();
    cboxConfig = new JCheckBox();
    cboxModuleReg = new JCheckBox();
    cboxCommissioning = new JCheckBox();

    //======== content ========
    {
      content.setLayout(new FormLayout(
          "8dlu, default:grow",
          "default, $pgap, 4*(default, $lgap), default"));

      //======== panel1 ========
      {
        panel1.setLayout(new FormLayout(
            "default, $lcgap, [100dlu,default]:grow, $lcgap, default",
            "default"));

        //---- label1 ----
        label1.setText("Save archive to file:");
        panel1.add(label1, CC.xy(1, 1));
        panel1.add(tfArchiveFile, CC.xy(3, 1));

        //---- btnBrowse ----
        btnBrowse.setText("Browse...");
        btnBrowse.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            btnBrowseActionPerformed(e);
          }
        });
        panel1.add(btnBrowse, CC.xy(5, 1));
      }
      content.add(panel1, CC.xywh(1, 1, 2, 1));

      //---- label2 ----
      label2.setText("Files to be archived:");
      content.add(label2, CC.xywh(1, 3, 2, 1));

      //---- cboxSyslog ----
      cboxSyslog.setText("Include System Log");
      cboxSyslog.setSelected(true);
      cboxSyslog.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          cboxActionPerformed(e);
        }
      });
      content.add(cboxSyslog, CC.xy(2, 5));

      //---- cboxConfig ----
      cboxConfig.setText("Include Configuration File");
      cboxConfig.setSelected(true);
      cboxConfig.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          cboxActionPerformed(e);
        }
      });
      content.add(cboxConfig, CC.xy(2, 7));

      //---- cboxModuleReg ----
      cboxModuleReg.setText("Include Module Registration");
      cboxModuleReg.setSelected(true);
      cboxModuleReg.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          cboxActionPerformed(e);
        }
      });
      content.add(cboxModuleReg, CC.xy(2, 9));

      //---- cboxCommissioning ----
      cboxCommissioning.setText("Include Commissioning Information");
      cboxCommissioning.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          cboxActionPerformed(e);
        }
      });
      content.add(cboxCommissioning, CC.xy(2, 11));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }

  @Override
  protected BannerPanel createBannerPanel() {
    return null;
  }

  @Override
  protected JComponent createContentPanel() {
    return content;
  }

  @Override
  protected JPanel createButtonPanel() {
    return super.createOKCancelButtonPanel();
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel content;
  private JPanel panel1;
  private JLabel label1;
  private JTextField tfArchiveFile;
  private JButton btnBrowse;
  private JLabel label2;
  private JCheckBox cboxSyslog;
  private JCheckBox cboxConfig;
  private JCheckBox cboxModuleReg;
  private JCheckBox cboxCommissioning;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
