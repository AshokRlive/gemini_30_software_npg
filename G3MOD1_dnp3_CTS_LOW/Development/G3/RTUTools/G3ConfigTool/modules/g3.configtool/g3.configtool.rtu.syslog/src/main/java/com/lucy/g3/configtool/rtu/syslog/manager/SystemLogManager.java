/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskService;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.syslog.task.RTUArchiveTask;
import com.lucy.g3.configtool.rtu.syslog.ui.LogLevelDialog;
import com.lucy.g3.configtool.rtu.syslog.ui.ProtocolTrafficViewer;
import com.lucy.g3.configtool.rtu.syslog.ui.ProtocolTrafficViewerModel;
import com.lucy.g3.configtool.rtu.syslog.ui.SystemLogViewer;
import com.lucy.g3.configtool.rtu.syslog.ui.SystemLogViewerModel;
import com.lucy.g3.configtool.rtu.syslog.ui.LogLevelDialog.LogLevelDialogInvoker;
import com.lucy.g3.configtool.rtu.syslog.ui.dialogs.RTUArchiveDialog;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeSubsytem;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeWindow;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.log.LogEntry;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogsAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.rtu.manager.G3RTUManager;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * A subsystem for controlling the update of system logs and publish the received log to its observers.
 * Also, it provides actions to set RTU log level.
 */
public class SystemLogManager extends AbstractRealtimeSubsytem implements LogLevelDialogInvoker {
  public final static String SUBSYSTEM_ID = "subystem.id.SystemLogManager";
  

  private static final String WINDOW_ID_SYS_LOG_VIEWER = "SystemLogViewerWindow";
  private static final String WINDOW_ID_PROTOCOL_TRAFFIC_VIEWER = "ProtocolStackTrafficViewerWindow";

  
  // ====== Action Keys ======
  public static final String ACTION_VIEW_SYSLOGS = "viewSysLogs";
  public static final String ACTION_VIEW_PROTOCOL_TRAFFIC = "viewProtocolTrafficLogs";
  public static final String ACTION_KEY_ARCHIVE = "archive";
  
  public static final String ACTION_KEY_SET_LOGLEVEL = "setLogLevel";
  

  // ====== Properties ======
  /** Read-only boolean property, indicating if the event log list is empty. */
  public static final String PROPERTY_UPDATE_PAUSED = "updatePaused";


  private Logger log = Logger.getLogger(SystemLogManager.class);

  private boolean updatePaused;

  private Task<?, ?> updateTask;
  
  private final HashMap<String, ISystemLogObserver> observers = new HashMap<>();


  public SystemLogManager(IContext context) {
    super(context, SUBSYSTEM_ID);

  }

  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void viewSysLogs() {
    final String windowID = WINDOW_ID_SYS_LOG_VIEWER;

    if (checkIsConnected() == true) {
      AbstractRealtimeWindow logViewer = getWindow(windowID);

      if (logViewer == null) {
        SystemLogViewerModel model = new SystemLogViewerModel(this);
        logViewer = new SystemLogViewer(getApp(), windowID, model);
        
        addLogObserver(windowID, model);
        registerRealtimeWindow(windowID, logViewer);
      }

      showWindow(logViewer);
    }
  }

  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void viewProtocolTrafficLogs() {
    final String windowID = WINDOW_ID_PROTOCOL_TRAFFIC_VIEWER;
    
    if (checkIsConnected() == true) {
      AbstractRealtimeWindow logViewer = getWindow(windowID);
      
      if (logViewer == null) {
        ProtocolTrafficViewerModel model = new ProtocolTrafficViewerModel(this);
        logViewer = new ProtocolTrafficViewer(getApp(), windowID, model);
        
        addLogObserver(windowID,model);
        registerRealtimeWindow(windowID, logViewer);
      }
      
      showWindow(logViewer);
    }
  }
  
  @Override
  protected void realtimeWindowClosed(AbstractRealtimeWindow closedWindow) {
    removeObserver(closedWindow.getId());
    degisterRealtimeWindow(closedWindow.getId());
  }


  public void addLogObserver(String windowID, ISystemLogObserver observer) {
    observers.put(windowID, observer);
    
    startStopUpdateTask();
  }
  
  public void removeObserver(String windowID) {
    observers.remove(windowID);
    
    startStopUpdateTask();
  }
  
  public List<String> getAllLogLevels() {
    return getAllLogLevels(getCurrentState());
  }
  
  public List<String> getAllSubsystems() {
    return getAllSubsystems(getCurrentState());
  }
  
  public boolean isUpdatePaused() {
    return updatePaused;
  }

  public void setUpdatePaused(boolean updatePaused) {
    Object oldValue = isUpdatePaused();
    this.updatePaused = updatePaused;
    firePropertyChange(PROPERTY_UPDATE_PAUSED, oldValue, updatePaused);

    startStopUpdateTask();
  }


  /**
   * Starts or stops update task depending on if there is any observers.
   */
  private void startStopUpdateTask() {
    // Start
    if (updateTask == null && isConnected() && isUpdatePaused() == false && observers.isEmpty() == false) {
      updateTask = new SysLogUpdateTask(getApp());
      getCommsTaskService().execute(updateTask);
      log.info("Started updating system log");
    }
    
    // Stop task
    else if(observers.isEmpty() || isUpdatePaused() || !isConnected()) {
        if (updateTask != null) {
          updateTask.cancel(true);
          updateTask = null;
          log.info("Stopped updating system log");
        }
    }
  }


  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void setLogLevel() {
    LogLevelDialog dlg = new LogLevelDialog(this);
    Application app = getApp();
    if (app instanceof SingleFrameApplication) {
      SingleFrameApplication sapp = (SingleFrameApplication) app;
      sapp.show(dlg);
    } else {
      dlg.setVisible(true);
    }
  }

  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public void archive() {
    RTUArchiveDialog dialog = new RTUArchiveDialog(WindowUtils.getMainFrame());
    dialog.pack();
    dialog.setVisible(true);
    
    if(dialog.hasBeenAffirmed()) {
      File destination = dialog.getArchiveDestination();
      if(destination != null && destination.getParentFile().exists()) {
        RTUArchiveTask task = new RTUArchiveTask(getApp(), destination);
        task.setIncludeConfig(dialog.isConfigIncluded());
        task.setIncludeLog(dialog.isSyslogIncluded());
        task.setIncludeRegDB(dialog.isModuleRegIncluded());
        task.setIncludeCommission(dialog.isCommissionIncluded());
        getCommsTaskService().execute(task);
      } 
    }
  }

 


  private class SysLogUpdateTask extends Task<Void, LogEntry> {
    
    private int period = 1000; // ms


    public SysLogUpdateTask(Application application) {
      super(application);
      setTitle("System Log Update");
      setDescription("Read system logs from remote RTU");
    }

    @Override
    protected Void doInBackground() throws Exception {
      RTULogsAPI logcmd = CommsUtil.getRTULogsAPI(G3RTUFactory.getComms());
      
      while (!isCancelled()) {
        try {
          ArrayList<LogEntry> logs = logcmd.cmdGetLatestLogs();
          if (logs != null && logs.size() > 0) {
            publish(logs.toArray(new LogEntry[logs.size()]));
          }
        } catch (Throwable e) {
          log.error("Fail to read the latest system logs:" + e.getMessage());
        }

        // Delay
        try {
          Thread.sleep(period);
        } catch (InterruptedException e) {
        }
      }

      return null;
    }

    @Override
    protected void process(List<LogEntry> logs) {
      log.info("Received logs: " + logs.size());
      Collection<ISystemLogObserver> obs = observers.values();
      for (ISystemLogObserver ob : obs) {
        ob.publishSyslogs(new ArrayList<>(logs));
      }
    }
  }

  

  @Override
  protected void notifyStateChanged(ConnectionState oldState,
      ConnectionState newState) {

//    if (isConnected()) {
//      mayStartUpdateTask();
//    } else {
//      mayStopUpdateTask();
//    }
    startStopUpdateTask();

    if (newState != oldState) {
      CTH_RUNNINGAPP state = getCurrentState();
      List<String> newLogLevels = getAllLogLevels(state);
      List<String> newSubsystems = getAllSubsystems(state);
      
      Collection<ISystemLogObserver> obs = observers.values();
      for (ISystemLogObserver ob : obs) {
        ob.publishLogLevels(newLogLevels);
        ob.publishSubsystems(newSubsystems);
      }
    }
  }

  // ================== LogLevelDialogInvoker ===================


  @Override
  public CTH_RUNNINGAPP getCurrentState() {
    G3RTUManager manager = getContext().getComponent(G3RTUManager.SUBSYSTEM_ID);
    CTH_RUNNINGAPP state = manager.getDefaultRTU().getConnectionState();
    return state;
  }


  @Override
  public TaskService getLogSettingTaskService() {
    return getCommsTaskService();
  }

  @Override
  public JFrame getDialogParent() {
    return getMainFrame();
  }

  /**
   * Gets list of description of all available G3 sub systems.
   *
   * @param state
   * @return
   */
  static final List<String> getAllSubsystems(CTH_RUNNINGAPP state) {
    String[] subsytemItems = new String[0];
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID[] subsystems =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.SUBSYSTEM_ID.values();
      subsytemItems = new String[subsystems.length];
      for (int i = 0; i < subsystems.length; i++) {
        subsytemItems[i] = subsystems[i].getDescription();
      }
    } else if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID[] subsystems =
          com.lucy.g3.xml.gen.common.LogLevelDef.SUBSYSTEM_ID.values();
      subsytemItems = new String[subsystems.length];
      for (int i = 0; i < subsystems.length; i++) {
        subsytemItems[i] = subsystems[i].getDescription();
      }
    }
    return Arrays.asList(subsytemItems);
  }

  /**
   * Gets list of description of all available G3 log levels.
   *
   * @param state
   * @return
   */
  static final List<String> getAllLogLevels(CTH_RUNNINGAPP state) {
    String[] levelItems = new String[0];
    if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_UPDATER) {
      com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef_Updater.LOG_LEVEL.values();
      levelItems = new String[levels.length];
      for (int i = 0; i < levels.length; i++) {
        levelItems[i] = levels[i].getDescription();
      }
    } else if (state == CTH_RUNNINGAPP.CTH_RUNNINGAPP_MCMAPP) {
      com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL[] levels =
          com.lucy.g3.xml.gen.common.LogLevelDef.LOG_LEVEL.values();
      levelItems = new String[levels.length];
      for (int i = 0; i < levels.length; i++) {
        levelItems[i] = levels[i].getDescription();
      }
    }
    return Arrays.asList(levelItems);
  }
}
