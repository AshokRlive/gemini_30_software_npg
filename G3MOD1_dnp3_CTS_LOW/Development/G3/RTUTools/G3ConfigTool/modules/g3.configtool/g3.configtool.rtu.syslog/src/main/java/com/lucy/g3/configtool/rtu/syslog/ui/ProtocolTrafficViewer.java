/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SortOrder;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.Task;
import org.jdesktop.swingx.JXTable;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.syslog.manager.SystemLogManager;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeWindow;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.ext.swing.button.ButtonHoverEffect;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.rtu.log.RTULogsAPI;
import com.lucy.g3.rtu.manager.G3RTUFactory;
public class ProtocolTrafficViewer extends AbstractRealtimeWindow {
  
  private final ProtocolTrafficViewerModel pm; 
  private final PropertyAdapter<SystemLogManager> pauseProperty;

  public ProtocolTrafficViewer(Application app, String windowID, ProtocolTrafficViewerModel model) {
    super(app, windowID);
    this.pm = model;
    this.pauseProperty = new PropertyAdapter<>(pm.getManager(), SystemLogManager.PROPERTY_UPDATE_PAUSED,true);
    
    initComponents();
    initComponentsActions();
    
    // Set ProtocolStack Subystem to debug level in order to view traffic 
    app.getContext().getTaskService().execute(
        new SetLogProtocolStatckLogLevelTask(application, pm.getRTUProtocolSubystemID(), pm.getRTUDebugLogLevel()));
  }

  private void initComponentsActions() {

    ApplicationActionMap actions;
    
    
    actions = pm.getActionMap();
    btnClear.setAction(actions.get(ProtocolTrafficViewerModel.ACTION_KEY_CLEAR));
    btnCopy.setAction(actions.get(ProtocolTrafficViewerModel.ACTION_KEY_COPY));
    
    menuItemClear.setAction(actions.get(ProtocolTrafficViewerModel.ACTION_KEY_CLEAR));
    menuItemCopy.setAction(actions.get(ProtocolTrafficViewerModel.ACTION_KEY_COPY));

    ButtonHoverEffect.install(btnCopy);
    ButtonHoverEffect.install(btnClear);

    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());
    
    
    Bindings.bind(cboxPause, pauseProperty);
  
  }

  private void okButtonActionPerformed(ActionEvent e) {
    close();
  }

  private void thisWindowClosing(WindowEvent e) {
    close();
  }

  @Override
  public void close() {
    super.close();
    pauseProperty.release();
    
    // Turn off debug level
    application.getContext().getTaskService().execute(
        new SetLogProtocolStatckLogLevelTask(application,  pm.getRTUProtocolSubystemID(), pm.getRTUDefaultLogLevel()));
  }


  public JPanel getPanelToolbar() {
    return panelToolbar;
  }

  @Override
  public void notifyConnectionStateChanged(ConnectionState oldState, ConnectionState newState) {
    // Nothing to do.
  }

  private void createUIComponents() {
    logListTable = new JXTable(pm.getLogTableModel());
    logListTable.setSortOrderCycle(SortOrder.ASCENDING, SortOrder.DESCENDING, SortOrder.UNSORTED);
    
    // Customise column width
    logListTable.getColumn(ProtocolTrafficViewerModel.COLUMN_INDEX_DATE).setMinWidth(180);
    logListTable.getColumn(ProtocolTrafficViewerModel.COLUMN_INDEX_DATE).setMaxWidth(220);
  }
  
  private static class SetLogProtocolStatckLogLevelTask extends Task<Void, Void> {

    private Logger log = Logger.getLogger(SetLogProtocolStatckLogLevelTask.class);


    private final String level;
    private final String subsystem;

    public SetLogProtocolStatckLogLevelTask(Application application, String subsystem, String level) {
      super(application);
      this.level = level;
      this.subsystem = subsystem;

      setTitle("Set Log Level");
      setDescription("Set RTU subsystem log levels");
    }

    @Override
    protected Void doInBackground() throws Exception {
      RTULogsAPI logcmd = CommsUtil.getRTULogsAPI(G3RTUFactory.getComms());
      logcmd.cmdSetLogLevel(subsystem, level);
      return null;
    }

    @Override
    protected void failed(final Throwable cause) {
      String msg = "Fail to set the log level of sub system \"" + subsystem
          + "\"  to \"" +  level + "\"";
      setMessage(msg);
      log.error(msg);
    }

    @Override
    protected void succeeded(Void result) {
      setMessage("Set the log level of sub system successfully!");
      log.error("Set the log level of:"+subsystem+" to:"+level);
    }

  }

  private void initComponents() {
    // @formatter:off
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    panelToolbar = new JPanel();
    cboxPause = new JCheckBox();
    btnCopy = new JButton();
    btnClear = new JButton();
    scrollPane1 = new JScrollPane();
    buttonBar = new JPanel();
    btnHelp = new JButton();
    btnClose = new JButton();
    popupMenu = new JPopupMenu();
    menuItemCopy = new JMenuItem();
    menuItemClear = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setTitle("Protocol Traffic Viewer");
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setPreferredSize(new Dimension(800, 600));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //======== panel1 ========
        {
          panel1.setLayout(new FormLayout(
              "default:grow",
              "default, $rgap, fill:default:grow"));

          //======== panelToolbar ========
          {
            panelToolbar.setLayout(new FormLayout(
                "default, $lcgap, default, $ugap, default:grow, 2*($lcgap, 20dlu)",
                "default"));

            //---- cboxPause ----
            cboxPause.setText("Pause");
            panelToolbar.add(cboxPause, CC.xy(3, 1));

            //---- btnCopy ----
            btnCopy.setContentAreaFilled(false);
            btnCopy.setPreferredSize(new Dimension(25, 25));
            btnCopy.setHideActionText(true);
            btnCopy.setText("Copy");
            panelToolbar.add(btnCopy, CC.xy(7, 1));

            //---- btnClear ----
            btnClear.setContentAreaFilled(false);
            btnClear.setPreferredSize(new Dimension(25, 25));
            btnClear.setHideActionText(true);
            btnClear.setText("Clear");
            panelToolbar.add(btnClear, CC.xy(9, 1));
          }
          panel1.add(panelToolbar, CC.xy(1, 1));

          //======== scrollPane1 ========
          {
            scrollPane1.setBorder(BorderFactory.createEmptyBorder());
            scrollPane1.setMinimumSize(new Dimension(300, 25));

            //---- logListTable ----
            logListTable.setEditable(false);
            logListTable.setRolloverEnabled(false);
            logListTable.setName("logListTable");
            logListTable.setColumnMargin(0);
            logListTable.setForeground(Color.darkGray);
            logListTable.setShowVerticalLines(false);
            logListTable.setShowHorizontalLines(false);
            logListTable.setBorder(null);
            logListTable.setComponentPopupMenu(popupMenu);
            logListTable.setSortsOnUpdates(false);
            scrollPane1.setViewportView(logListTable);
          }
          panel1.add(scrollPane1, CC.xy(1, 3));
        }
        contentPanel.add(panel1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 0, 0, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0 };

        //---- btnHelp ----
        btnHelp.setText("Help");
        buttonBar.add(btnHelp, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

        //---- btnClose ----
        btnClose.setText("Close");
        btnClose.setPreferredSize(new Dimension(61, 25));
        btnClose.setMinimumSize(new Dimension(61, 25));
        btnClose.setMaximumSize(new Dimension(61, 25));
        btnClose.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(btnClose, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    //======== popupMenu ========
    {

      //---- menuItemCopy ----
      menuItemCopy.setText("Copy");
      popupMenu.add(menuItemCopy);
      popupMenu.addSeparator();

      //---- menuItemClear ----
      menuItemClear.setText("Clear");
      popupMenu.add(menuItemClear);
    }

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
    // @formatter:on
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JPanel panelToolbar;
  private JCheckBox cboxPause;
  private JButton btnCopy;
  private JButton btnClear;
  private JScrollPane scrollPane1;
  private JXTable logListTable;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton btnClose;
  private JPopupMenu popupMenu;
  private JMenuItem menuItemCopy;
  private JMenuItem menuItemClear;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
