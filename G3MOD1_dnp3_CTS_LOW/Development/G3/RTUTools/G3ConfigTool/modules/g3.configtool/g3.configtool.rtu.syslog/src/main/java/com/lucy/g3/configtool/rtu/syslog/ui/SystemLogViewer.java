/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.RowFilter;
import javax.swing.SortOrder;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableModel;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.JXTable;

import com.jgoodies.binding.adapter.Bindings;
import com.jgoodies.binding.adapter.ComboBoxAdapter;
import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.configtool.rtu.syslog.manager.SystemLogManager;
import com.lucy.g3.configtool.subsystem.AbstractRealtimeWindow;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.ext.swing.button.ButtonHoverEffect;
import com.lucy.g3.help.Helper;
import com.lucy.g3.rtu.comms.service.rtu.log.LogEntry;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_RUNNINGAPP;

/**
 * The frame window for viewing RTU system log.
 */
public class SystemLogViewer extends AbstractRealtimeWindow{

  private static final String ITEM_ALL_SUBSYTEM = "All";

  private final SystemLogViewerModel pm; 
  private final PropertyAdapter<SystemLogManager> pauseProperty;

  public SystemLogViewer(Application app, String windowID, SystemLogViewerModel model) {
    super(app, windowID);
    this.pm = model;
    this.pauseProperty = new PropertyAdapter<>(pm.getManager(), SystemLogManager.PROPERTY_UPDATE_PAUSED,true);
    initComponents();
    initComponentsActions();
  }
  
  private void filterLogTable() {
    String logLevel = (String) comboBoxLogLevel.getSelectedItem();
    String subSystem = (String) comboBoxSubsys.getSelectedItem();
    logListTable.setRowFilter(new SystemLogFilter(logLevel, subSystem));
  }

  private void createUIComponents() {
    logListTable = new JXTable(pm.getLogTableModel());
    logListTable.setSortOrderCycle(SortOrder.ASCENDING, SortOrder.DESCENDING, SortOrder.UNSORTED);

    // Customise column width
    logListTable.getColumn(SystemLogViewerModel.COLUMN_INDEX_LEVEL).setMaxWidth(80);
    logListTable.getColumn(SystemLogViewerModel.COLUMN_INDEX_SUB_SYSTEM).setMaxWidth(200);

    ComboBoxModel<String> comboModel = new ComboBoxAdapter<String>(pm.getFilterLogLevel());
    comboBoxLogLevel = new JComboBox<String>(comboModel);

    comboModel = new ComboBoxAdapter<String>(pm.getFilterSubsys());
    comboBoxSubsys = new JComboBox(comboModel);
  }

  private void okButtonActionPerformed(ActionEvent e) {
    close();
  }

  private void thisWindowClosing(WindowEvent e) {
    close();
  }

  @Override
  public void close() {
    super.close();
    pauseProperty.release();
  }

  private void initComponentsActions() {
    ApplicationActionMap actions;
    
    actions = pm.getManager().getActionMap();
    btnConfigure.setAction(actions.get(SystemLogManager.ACTION_KEY_SET_LOGLEVEL));
    btnSave.setAction(actions.get(SystemLogManager.ACTION_KEY_ARCHIVE));
    
    actions = pm.getActionMap();
    btnClear.setAction(actions.get(SystemLogViewerModel.ACTION_KEY_CLEAR));
    btnCopy.setAction(actions.get(SystemLogViewerModel.ACTION_KEY_COPY));
    
    menuItemClear.setAction(actions.get(SystemLogViewerModel.ACTION_KEY_CLEAR));
    menuItemCopy.setAction(actions.get(SystemLogViewerModel.ACTION_KEY_COPY));

    ButtonHoverEffect.install(btnCopy);
    ButtonHoverEffect.install(btnClear);
    ButtonHoverEffect.install(btnSave);
    ButtonHoverEffect.install(btnConfigure);

    Helper.register(btnHelp, getClass());
    btnHelp.setIcon(Helper.getIcon());
    
    
    Bindings.bind(cboxPause, pauseProperty);
  }

  public JComboBox getComboBoxLogLevel() {
    return comboBoxLogLevel;
  }

  public JComboBox getComboBoxSubsys() {
    return comboBoxSubsys;
  }

  public JXTable getLogListTable() {
    return logListTable;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    createUIComponents();

    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    panelToolbar = new JPanel();
    JLabel lblLogLevel = new JLabel();
    JLabel lblSubsys = new JLabel();
    cboxPause = new JCheckBox();
    btnConfigure = new JButton();
    btnCopy = new JButton();
    btnSave = new JButton();
    btnClear = new JButton();
    scrollPane1 = new JScrollPane();
    buttonBar = new JPanel();
    btnHelp = new JButton();
    btnClose = new JButton();
    popupMenu = new JPopupMenu();
    menuItemCopy = new JMenuItem();
    menuItemClear = new JMenuItem();

    //======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setTitle("RTU System Logs Viewer");
    setName("RTUSystemLogViewerwindow");
    addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setLayout(new BorderLayout());

      //======== contentPanel ========
      {
        contentPanel.setLayout(new BorderLayout());

        //======== panel1 ========
        {
          panel1.setLayout(new FormLayout(
              "default:grow",
              "default, $rgap, fill:default:grow"));

          //======== panelToolbar ========
          {
            panelToolbar.setLayout(new FormLayout(
                "default, $lcgap, [50dlu,default], $ugap, default, $lcgap, [50dlu,default], $lcgap, default, $lcgap, default:grow, 4*($lcgap, 20dlu)",
                "default"));

            //---- lblLogLevel ----
            lblLogLevel.setText("Filter by Log Level:");
            panelToolbar.add(lblLogLevel, CC.xy(1, 1));

            //---- comboBoxLogLevel ----
            comboBoxLogLevel.setToolTipText("Filter log entries by log level");
            comboBoxLogLevel.setFocusable(false);
            comboBoxLogLevel.addActionListener(new ActionListener() {

              @Override
              public void actionPerformed(ActionEvent e) {
                filterLogTable();
              }
            });
            panelToolbar.add(comboBoxLogLevel, CC.xy(3, 1));

            //---- lblSubsys ----
            lblSubsys.setText("Filter by Sub System:");
            panelToolbar.add(lblSubsys, CC.xy(5, 1));

            //---- comboBoxSubsys ----
            comboBoxSubsys.setToolTipText("Filter log entries by sub system");
            comboBoxSubsys.setFocusable(false);
            comboBoxSubsys.addActionListener(new ActionListener() {

              @Override
              public void actionPerformed(ActionEvent e) {
                filterLogTable();
              }
            });
            panelToolbar.add(comboBoxSubsys, CC.xy(7, 1));

            //---- cboxPause ----
            cboxPause.setText("Pause");
            panelToolbar.add(cboxPause, CC.xy(9, 1));

            //---- btnConfigure ----
            btnConfigure.setContentAreaFilled(false);
            btnConfigure.setPreferredSize(new Dimension(25, 25));
            btnConfigure.setHideActionText(true);
            btnConfigure.setText("Setting");
            panelToolbar.add(btnConfigure, CC.xy(13, 1));

            //---- btnCopy ----
            btnCopy.setContentAreaFilled(false);
            btnCopy.setPreferredSize(new Dimension(25, 25));
            btnCopy.setHideActionText(true);
            btnCopy.setText("Copy");
            panelToolbar.add(btnCopy, CC.xy(15, 1));

            //---- btnSave ----
            btnSave.setContentAreaFilled(false);
            btnSave.setPreferredSize(new Dimension(25, 25));
            btnSave.setHideActionText(true);
            btnSave.setText("Save");
            panelToolbar.add(btnSave, CC.xy(17, 1));

            //---- btnClear ----
            btnClear.setContentAreaFilled(false);
            btnClear.setPreferredSize(new Dimension(25, 25));
            btnClear.setHideActionText(true);
            btnClear.setText("Clear");
            panelToolbar.add(btnClear, CC.xy(19, 1));
          }
          panel1.add(panelToolbar, CC.xy(1, 1));

          //======== scrollPane1 ========
          {
            scrollPane1.setBorder(BorderFactory.createEmptyBorder());

            //---- logListTable ----
            logListTable.setEditable(false);
            logListTable.setRolloverEnabled(false);
            logListTable.setName("logListTable");
            logListTable.setColumnMargin(0);
            logListTable.setForeground(Color.darkGray);
            logListTable.setShowVerticalLines(false);
            logListTable.setShowHorizontalLines(false);
            logListTable.setBorder(null);
            logListTable.setComponentPopupMenu(popupMenu);
            scrollPane1.setViewportView(logListTable);
          }
          panel1.add(scrollPane1, CC.xy(1, 3));
        }
        contentPanel.add(panel1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);

      //======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 0, 0, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0 };

        //---- btnHelp ----
        btnHelp.setText("Help");
        buttonBar.add(btnHelp, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

        //---- btnClose ----
        btnClose.setText("Close");
        btnClose.setPreferredSize(new Dimension(61, 25));
        btnClose.setMinimumSize(new Dimension(61, 25));
        btnClose.setMaximumSize(new Dimension(61, 25));
        btnClose.addActionListener(new ActionListener() {

          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(btnClose, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);

    //======== popupMenu ========
    {

      //---- menuItemCopy ----
      menuItemCopy.setText("Copy");
      popupMenu.add(menuItemCopy);
      popupMenu.addSeparator();

      //---- menuItemClear ----
      menuItemClear.setText("Clear");
      popupMenu.add(menuItemClear);
    }

    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents

  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JPanel panelToolbar;
  private JComboBox comboBoxLogLevel;
  private JComboBox comboBoxSubsys;
  private JCheckBox cboxPause;
  private JButton btnConfigure;
  private JButton btnCopy;
  private JButton btnSave;
  private JButton btnClear;
  private JScrollPane scrollPane1;
  private JXTable logListTable;
  private JPanel buttonBar;
  private JButton btnHelp;
  private JButton btnClose;
  private JPopupMenu popupMenu;
  private JMenuItem menuItemCopy;
  private JMenuItem menuItemClear;
  // JFormDesigner - End of variables declaration //GEN-END:variables

  @Override
  public void notifyConnectionStateChanged(ConnectionState oldState,
      ConnectionState newState) {
    
  }


  private class SystemLogFilter extends RowFilter<TableModel, Integer> {

    private final String filterLogLevel;
    private final String filterSubsystem;


    public SystemLogFilter(String logLevel, String subSystem) {
      this.filterLogLevel = logLevel;
      this.filterSubsystem = subSystem;
    }

    @Override
    public boolean include(javax.swing.RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
      boolean includedBySubsystem = true;
      boolean includedByLevel = true;
      int index = entry.getIdentifier().intValue();

      String curLevel = (String) entry.getModel().getValueAt(index, SystemLogViewerModel.COLUMN_INDEX_LEVEL);
      String curSubsys = (String) entry.getModel().getValueAt(index, SystemLogViewerModel.COLUMN_INDEX_SUB_SYSTEM);

      if (filterSubsystem != null && !filterSubsystem.equals(ITEM_ALL_SUBSYTEM)) {
        includedBySubsystem = (filterSubsystem.equals(curSubsys));
      }

      CTH_RUNNINGAPP state = pm.getManager().getCurrentState();
      if (filterLogLevel != null) {
        Byte curlevelValue = LogEntry.logLevelDescription2Value(curLevel, state);
        Byte filterlevelValue = LogEntry.logLevelDescription2Value(filterLogLevel, state);
        if (filterlevelValue != null && curlevelValue != null) {
          includedByLevel = curlevelValue <= filterlevelValue;
        }
      }

      return includedByLevel && includedBySubsystem;
    }
  }

}
