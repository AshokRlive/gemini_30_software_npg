/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.ui;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Action;
import org.jdesktop.application.ApplicationActionMap;

import com.jgoodies.binding.adapter.AbstractTableAdapter;
import com.jgoodies.binding.beans.Model;
import com.jgoodies.binding.list.SelectionInList;
import com.jgoodies.common.collect.ArrayListModel;
import com.lucy.g3.configtool.rtu.syslog.manager.ISystemLogObserver;
import com.lucy.g3.configtool.rtu.syslog.manager.SystemLogManager;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.service.rtu.log.LogEntry;
import com.lucy.g3.rtu.comms.shared.RTUTimeDecoder;


/**
 *
 */
public class SystemLogViewerModel extends Model implements ClipboardOwner, ISystemLogObserver{
  private final static int LOG_LIST_BUFFER_SIZE = 2000;
  
  public static final String ACTION_KEY_CLEAR = "clearLogs";
  public static final String ACTION_KEY_COPY = "copyLogs";
  
  public static final String PROPERTY_NOT_EMPTY = "notEmpty";
  
  // ====== Table Model Constants ======
  public static final int COLUMN_INDEX_DATE = 0;
  public static final int COLUMN_INDEX_SUB_SYSTEM = 1;
  public static final int COLUMN_INDEX_LEVEL = 2;
  public static final int COLUMN_INDEX_CONTENT = 3;
  private static final String[] COLUMN_NAMES = { "Time", "Sub-System", "Log Level", "Log" };


  private Logger log = Logger.getLogger(SystemLogViewerModel.class);
  
  private static final String ITEM_ALL_SUBSYTEM = "All";
  
  private final ArrayListModel<LogEntry> logList;
  private boolean notEmpty;
  
  private final LogEntryTableAdapter logListTableModel;
  
  private final SelectionInList<String> filterLogLevel = new SelectionInList<>(); 
  private final SelectionInList<String> filterSubsys = new SelectionInList<>(); 
  private final SystemLogManager manager;
  
  @SuppressWarnings("unchecked")
  public SystemLogViewerModel(SystemLogManager manager) {
    this.manager = manager;
    this.logList = new ArrayListModel<LogEntry>(LOG_LIST_BUFFER_SIZE);
    this.logListTableModel = new LogEntryTableAdapter(logList);
    
    setLogLevels(manager.getAllLogLevels());
    setSubsystems(manager.getAllSubsystems());
  }

  
  public SystemLogManager getManager() {
    return manager;
  }

  public SelectionInList<String> getFilterLogLevel() {
    return filterLogLevel;
  }

  public SelectionInList<String> getFilterSubsys() {
    return filterSubsys;
  }
  
  public ConnectionState getCurrentState() {
    return manager.getConnectionState();
  }
  
  private void setLogLevels(List<String> logLevels) {
    String oldLevelSelection = filterLogLevel.getSelection();
    filterLogLevel.getList().clear();
    filterLogLevel.getList().addAll(logLevels);
    filterLogLevel.setSelection(oldLevelSelection);
  }
  
  private void setSubsystems(List<String> subsystems) {
    String oldSubsysSelection = filterSubsys.getSelection();
    filterSubsys.getList().clear();
    filterSubsys.getList().addAll(subsystems);
    filterSubsys.getList().add(0, ITEM_ALL_SUBSYTEM);
    filterSubsys.setSelection(oldSubsysSelection);
  }
  
  @SuppressWarnings("unchecked")
  public ListModel<LogEntry> getLogListModel() {
    return logList;
  }

  public TableModel getLogTableModel() {
    return logListTableModel;
  }
  
  /* Property Getter method for PROPERTY_NOT_EMPTY */
  public boolean isNotEmpty() {
    return notEmpty;
  }
  
  private void setNotEmpty(boolean newValue) {
    Object oldValue = isNotEmpty();
    this.notEmpty = newValue;
    firePropertyChange(PROPERTY_NOT_EMPTY, oldValue, newValue);
  }
  @Action
  public void clearLogs() {
    logList.clear();
    firePropertyChange(PROPERTY_NOT_EMPTY, true, false);
  }
  
  @Action(enabledProperty = PROPERTY_NOT_EMPTY)
  public void copyLogs() {
    if (logList.isEmpty()) {
      log.warn("No System Logs");
      return;
    }

    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(logListTableModel, this);
    JOptionPane.showMessageDialog(WindowUtils.getMainFrame(), "System logs have been copied to Clipboard.");
  }
  
  @Override
  public void lostOwnership(Clipboard clipboard, Transferable contents) {
    // Do nothing
  }
  
  @Override
  public void publishSyslogs(List<LogEntry> newLogs) {
    int size = logList.size();
    if (size >= LOG_LIST_BUFFER_SIZE && size > newLogs.size()) {
      int trimSize = size - LOG_LIST_BUFFER_SIZE + newLogs.size();
      for (int i = 0; i < trimSize && logList.size() > 0; i++) {
        logList.remove(logList.size() - 1);
      }
    }
    logList.addAll(0, newLogs);
    
    setNotEmpty(!logList.isEmpty());
  }


  @Override
  public void publishLogLevels(List<String> newLogLevels) {
    setLogLevels(newLogLevels);
  }


  @Override
  public void publishSubsystems(List<String> newSubsystems) {
    setSubsystems(newSubsystems);
  }


  /**
   * Adapt a list of logs to table model.<br>
   */
  private static class LogEntryTableAdapter extends AbstractTableAdapter<LogEntry>
      implements Transferable {

    private final DataFlavor[] DATA_FLAVORS = { DataFlavor.stringFlavor };


    public LogEntryTableAdapter(ListModel<LogEntry> listModel) {
      super(listModel, COLUMN_NAMES);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      LogEntry entry = getRow(rowIndex);
      switch (columnIndex) {
      case COLUMN_INDEX_DATE:
        return RTUTimeDecoder.formatEventLog(entry.getTimestamp());

      case COLUMN_INDEX_SUB_SYSTEM:
        return String.valueOf(entry.getSubSystemID());

      case COLUMN_INDEX_LEVEL:
        return String.valueOf(entry.getLogLevelID());

      case COLUMN_INDEX_CONTENT:
        return entry.getLogContent();

      default:
        throw new IllegalStateException("Unknown column");
      }
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
      return DATA_FLAVORS;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
      for (int i = 0; i < DATA_FLAVORS.length; i++) {
        if (flavor == DATA_FLAVORS[i]) {
          return true;
        }
      }
      return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
      return getContent();
    }

    public String getContent() {
      StringBuffer buf = new StringBuffer();
      int size = getRowCount();
      for (int i = 0; i < size; i++) {
        LogEntry entry = getRow(i);
        buf.append(entry.getFormattedLogText());
        buf.append("\r\n");
      }
      return buf.toString();
    }
  }


  public ApplicationActionMap getActionMap() {
    return manager.getApp().getContext().getActionMap(this);
  }

}

