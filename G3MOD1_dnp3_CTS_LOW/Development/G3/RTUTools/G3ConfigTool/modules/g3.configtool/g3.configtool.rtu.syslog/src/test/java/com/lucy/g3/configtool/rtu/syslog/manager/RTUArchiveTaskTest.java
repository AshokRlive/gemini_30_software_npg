/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.manager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.syslog.task.RTUArchiveTask;
import com.lucy.g3.rtu.comms.protocol.exceptions.SerializationException;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.test.support.CommsTestSupport;


/**
 * The Class LogFileReadingTaskTest.
 */
public class RTUArchiveTaskTest {
  private static G3RTU rtu;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Assume.assumeNotNull(CommsTestSupport.HOST);
    rtu = G3RTUFactory.createDefault(CommsTestSupport.HOST);
  }

  @Test
  public void testGetLogFileName() throws InterruptedException, ExecutionException, IOException, SerializationException {
    Logger.getRootLogger().setLevel(Level.DEBUG);
    String log = rtu.getComms().getFileTransfer().cmdGetLogFileName();
    System.out.println(log);
  }
  
  @Test
  public void testReadArchiveFile() throws InterruptedException, ExecutionException {
    Application app = Application.getInstance();
    Logger.getRootLogger().setLevel(Level.INFO);
    File parent = new File("target/ArchiveTest");
    parent.mkdirs();
    File file = new File(parent, "log.zip");
    RTUArchiveTask task = new RTUArchiveTask(app, file, true);
    app.getContext().getTaskService().execute(task);
    task.get();
  }

}

