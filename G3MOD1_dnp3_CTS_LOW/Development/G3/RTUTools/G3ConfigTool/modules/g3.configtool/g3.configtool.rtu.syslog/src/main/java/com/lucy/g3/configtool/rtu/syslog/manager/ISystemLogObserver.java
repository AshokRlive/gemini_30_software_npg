/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.manager;

import java.util.List;

import com.lucy.g3.rtu.comms.service.rtu.log.LogEntry;

/**
 *
 */
public interface ISystemLogObserver {

  void publishSyslogs(List<LogEntry> newLogs);

  void publishLogLevels(List<String> newLogLevels);

  void publishSubsystems(List<String> newSubsystems);

}
