/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.rtu.syslog.task;

import java.awt.FlowLayout;
import java.io.File;
import java.net.ProtocolException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Logger;
import org.jdesktop.application.Application;
import org.jdesktop.swingx.JXHyperlink;

import com.jgoodies.common.base.Preconditions;
import com.lucy.g3.common.utils.ZipUtil;
import com.lucy.g3.gui.common.widgets.ext.swing.action.FileLinkAction;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * Read a file from RTU device.
 */
public class RTUArchiveTask extends FileReadingTask {

  protected Logger log = Logger.getLogger(RTUArchiveTask.class);

  private final File archiveFile;
  private final ArrayList<FileTransferEntry> transferList = new ArrayList<>();

  private final boolean silent;
  
  private boolean includeLog = true;
  private boolean includeConfig = true;
  private boolean includeRegDB  = true;
  private boolean includeCommissioning  = false;

  public RTUArchiveTask(Application app, File archiveDestination) {
    this(app, archiveDestination, false);
  }
  
  public RTUArchiveTask(Application app, File archiveDestination, boolean silent) {
    super(app, CommsUtil.getFileTransfer(G3RTUFactory.getComms()),
        CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_LOG, "", null, !silent);
    this.archiveFile = Preconditions.checkNotNull(archiveDestination, "destination must not be null");
    this.silent = silent;
  }

  @Override
  protected File doInBackground() throws Exception {
    // Check destination specified
    if (archiveFile == null) {
      cancel(true);
      return null;
    }

    // Waiting block dialog to be shown
    Thread.sleep(500);

    // Confirm overwrite
    if (!silent && archiveFile.exists() && !confirmOverrite(archiveFile)) {
      cancel(true);
      return null;
    }

    if(includeLog) {
      /* Get log file list */
      String fileList = getFileTransfer().cmdGetLogFileName();
      if (fileList == null || fileList.isEmpty()) {
        throw new ProtocolException("Failed to get log file list");
      } else {
        log.info("Log file list:" + fileList);
      }
      
      /* Add log file entries*/
      final String[] fileNames = fileList.split("\\n");
      if (fileNames.length == 0) {
        log.warn("No log file found in RTU!");
      }else{
        for (int i = 0; i < fileNames.length; i++) {
          transferList.add(new FileTransferEntry(
              new File(archiveFile.getParent(), fileNames[i]),
              fileNames[i],
              CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_LOG));
        }
      }
    }

    /* Add config file entry*/
    if(includeConfig) {
      transferList.add(new FileTransferEntry(
          new File(archiveFile.getParent(), "G3Config.xml.gz"), 
          "../application/G3Config.xml.gz",
          CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC));
    }    
    
    /* Add reg db file entry*/
    if(includeRegDB) {
      transferList.add(new FileTransferEntry(new File(archiveFile.getParent(), "registrationDB.xml"),
          "../application/registrationDB.xml",
          CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC));
    }
    
    /* Add commissioning cache file entry*/
    if(includeCommissioning){
      transferList.add(new FileTransferEntry(new File(archiveFile.getParent(), "commissioning.cache"),
          "../upload/commissioning.cache",
          CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_GENERIC));
    }
    
    /* Transfer all files in the transfer list*/
    final ArrayList<File> succeededFiles = new ArrayList<>();
    for (FileTransferEntry entry : transferList) {
      setSourceFileName(entry.sourceFileName);
      setDestination(entry.destFile);
      setFileTransferType(entry.type);
      
      try{
        super.doInBackground();
        succeededFiles.add(entry.destFile);
      }catch(Exception e) {
        log.error("failed to transfer file:"+entry.sourceFileName+" cause:"+e.getMessage());
      }
    }

    /* Archive transfered files */
    if(!succeededFiles.isEmpty())
      ZipUtil.zip(archiveFile.getAbsolutePath(), succeededFiles);
    else
      throw new Exception("No file found in RTU!");
    
    return archiveFile;
  }

  @Override
  public void succeeded(File result) {
    super.succeeded(result);
    log.info("Succeeded to fetch archive files.");
    
    if (!silent && result != null) {
      JPanel msg = new JPanel(new FlowLayout());
      msg.add(new JLabel("The archive file has been saved to:"));
      msg.add(new JXHyperlink(new FileLinkAction(result)));
      
      JOptionPane.showMessageDialog(getParent(),
          msg,
          "Success",
          JOptionPane.INFORMATION_MESSAGE);
    }
  }

  @Override
  public void failed(Throwable cause) {
    super.failed(cause);
    log.error("Failed to fetch files for archiving", cause);
    
    if (!silent)
    JOptionPane.showMessageDialog(getParent(),
        "Failed to fetch RTU files. \nError: " + cause.getMessage(),
        "Failure",
        JOptionPane.ERROR_MESSAGE);
  }

  @Override
  protected void finished() {

      /* Delete temporary files */
      for (FileTransferEntry entry : transferList) {
        entry.destFile.delete();
      }

  }

  private static class FileTransferEntry {
    final File destFile;
    final String sourceFileName;
    final CTH_TRANSFERTYPE type;
    public FileTransferEntry(File destFile, String sourceFileName, CTH_TRANSFERTYPE type) {
      super();
      this.destFile = destFile;
      this.sourceFileName = sourceFileName;
      this.type = type;
    }
  }
  
  
  public void setIncludeLog(boolean includeLog) {
    this.includeLog = includeLog;
  }

  public void setIncludeConfig(boolean includeConfig) {
    this.includeConfig = includeConfig;
  }

  public void setIncludeRegDB(boolean includeRegDB) {
    this.includeRegDB = includeRegDB;
  }

  public void setIncludeCommission(boolean commissionIncluded) {
    this.includeCommissioning = commissionIncluded;
  }

}
