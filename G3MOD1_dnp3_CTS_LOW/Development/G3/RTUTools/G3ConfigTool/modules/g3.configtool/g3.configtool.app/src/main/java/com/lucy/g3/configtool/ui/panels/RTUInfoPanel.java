/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui.panels;

import java.awt.Color;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;

import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.binding.binder.BeanBinderImpl;
import com.jgoodies.binding.value.BindingConverter;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.rtu.sysalarm.SystemAlarmsManager;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.animation.Blinker;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.realtime.status.RTUStatus;
import com.lucy.g3.rtu.comms.service.rtu.info.RTUInfo;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.xml.gen.common.ControlLogicDef.OLR_STATE;
import com.lucy.g3.xml.gen.common.SysAlarmCodeEnum.SYS_ALARM_SEVERITY;

/**
 * Panel for displaying RTU information and status.
 */
public class RTUInfoPanel extends JPanel {

  private Blinker alarmBlinker;


  /**
   * Private non-parameter constructor required by JFormDesigner. Do not delete!
   */
  @SuppressWarnings("unused")
  private RTUInfoPanel() {
    initComponents();
  }

  public RTUInfoPanel(IContext context) {
    SystemAlarmsManager manager = context.getComponent(SystemAlarmsManager.SUBSYSTEM_ID);
    
    initComponents();
    initComponentsBinding(manager);
    initEventHandling(manager);
    subscribeEvents();
    updateVisibility(null);
  }

  private void subscribeEvents() {
    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, 
        new IEventHandler() {
      
      @Override
      public void handleEvent(Event event) {
        ConnectionState newState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
        setState(newState);
      }
    });
  }
  
  private void initEventHandling(SystemAlarmsManager manager) {
    // Blink alarm label if it is not normal status
    alarmBlinker = new Blinker(lblAlarmStatus);
    alarmBlinker.setBlinkBg(null);
    manager.addPropertyChangeListener(
        SystemAlarmsManager.PROPERTY_ALARM_STATUS,
        new AlarmStatusPCL());
  }

  private void setState(ConnectionState newState) {
    updateVisibility(newState);
  }

  private void updateVisibility(ConnectionState state) {
    labelCommLost.setVisible(state == ConnectionState.CONNECTION_LOST);
    panelInfo.setVisible(state == ConnectionState.CONNECTED_TO_G3_APP);
  }

  private void initComponentsBinding(SystemAlarmsManager manager) {
    G3RTU rtu = G3RTUFactory.getDefault();
    BeanBinderImpl<RTUStatus> statusBinder = new BeanBinderImpl<>(rtu.getComms().getRtuStatus());
    BeanBinderImpl<RTUInfo> infoBinder = new BeanBinderImpl<>(rtu.getComms().getRtuInfo());
    BeanBinderImpl<SystemAlarmsManager> alarmBinder = new BeanBinderImpl<>(manager);

    // Bind door status
    statusBinder
        .bindProperty(RTUStatus.PROPERTY_DOOR_OPEN)
        .converted(new DoorConverter())
        .to(lblDoorStatus);
    
    // Bind OLR status
    statusBinder
      .bindProperty(RTUStatus.PROPERTY_OLR_STATUS)
      .converted(new OLRConverter())
      .to(lblOLRStatus);
    
    // Bind auto status
    statusBinder
        .bindProperty(RTUStatus.PROPERTY_AUTO_STATUS)
        .to(lblAutoStatus);

    // Bind IP address
    infoBinder
        .bindProperty(RTUInfo.PROPERTY_NETWORK_IP0)
        .to(lblRtuIpAddress);

    // Bind site name
    infoBinder
        .bindProperty(RTUInfo.PROPERTY_SITENAME)
        .to(lblSiteName);

    // Bind alarm status
    alarmBinder
        .bindProperty(SystemAlarmsManager.PROPERTY_ALARM_STATUS)
        .converted(new AlarmStatusConverter())
        .to(lblAlarmStatus);
  }


  private class AlarmStatusPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (evt.getNewValue() == evt.getOldValue()) {
        return;
      }

      SYS_ALARM_SEVERITY alarmSeverity = (SYS_ALARM_SEVERITY) evt.getNewValue();

      if (isNormal(alarmSeverity)) {
        alarmBlinker.stop();

      } else if (alarmSeverity != null) {
        int blinkPeriod;
        Color blinkColor;
        switch (alarmSeverity) {
        case SYS_ALARM_SEVERITY_CRITICAL:
          blinkPeriod = 300;
          blinkColor = Color.red;
          break;
        case SYS_ALARM_SEVERITY_ERROR:
          blinkColor = Color.pink;
          blinkPeriod = 700;
          break;
        case SYS_ALARM_SEVERITY_WARNING:
          blinkColor = Color.orange;
          blinkPeriod = 1000;
          break;
        default:
          blinkPeriod = 1000;
          blinkColor = Color.red;
          break;
        }

        alarmBlinker.setBlinkingPeriod(blinkPeriod);
        alarmBlinker.setBlinkFg(blinkColor);

        alarmBlinker.start();
      }
    }
  }

  private static class DoorConverter implements BindingConverter<Boolean, String> {

    @Override
    public Boolean sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(Boolean open) {
      if (open != null) {
        return open ? "Open" : "Closed";
      } else {
        return "";
      }
    }
  }
  
  private static class OLRConverter implements BindingConverter<OLR_STATE, String> {
    
    @Override
    public OLR_STATE sourceValue(String arg0) {
      return null;
    }
    
    @Override
    public String targetValue(OLR_STATE olr) {
      if (olr != null) {
        String text = olr.getDescription();
        // Set first letter upper case 
        if (text.length() > 1)
          text = text.substring(0,1).toUpperCase() + text.substring(1).toLowerCase();
        return text;
      } else {
        return "";
      }
    }
  }

  private static class AlarmStatusConverter implements BindingConverter<SYS_ALARM_SEVERITY, String> {

    @Override
    public SYS_ALARM_SEVERITY sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(SYS_ALARM_SEVERITY alarmSeverity) {
      String alarmStatus;
      if (isNormal(alarmSeverity)) {
        alarmStatus = "Normal";
      } else {
        alarmStatus = capitalizeFirstChar(alarmSeverity.getDescription());
      }

      return alarmStatus;
    }
  }


  private static String capitalizeFirstChar(String str) {
    return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
  }

  private static boolean isNormal(SYS_ALARM_SEVERITY severity) {
    return severity == null || severity == SYS_ALARM_SEVERITY.SYS_ALARM_SEVERITY_INFO;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    panelInfo = new JPanel();
    panel1 = new JPanel();
    JLabel lblSiteName2 = new JLabel();
    lblSiteName = new JLabel();
    JLabel label1 = new JLabel();
    lblRtuIpAddress = new JLabel();
    JLabel label2 = new JLabel();
    lblDoorStatus = new JLabel();
    JLabel label3 = new JLabel();
    lblAlarmStatus = new JLabel();
    JLabel label4 = new JLabel();
    lblOLRStatus = new JLabel();
    label5 = new JLabel();
    lblAutoStatus = new JLabel();
    labelCommLost = new JLabel();

    //======== this ========
    setOpaque(false);
    setLayout(new FormLayout(
      "477dlu:grow",
      "fill:default, $lgap, fill:default"));

    //======== panelInfo ========
    {
      panelInfo.setOpaque(false);
      panelInfo.setLayout(new FormLayout(
        "default, $lcgap, [80dlu,default], $ugap, 2*(default, $lcgap), 7dlu, 2*($lcgap, default), $lcgap, default:grow",
        "2*(fill:default, $lgap), fill:default"));

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setLayout(new FormLayout(
          "default, $lcgap, default:grow",
          "fill:default"));

        //---- lblSiteName2 ----
        lblSiteName2.setForeground(Color.darkGray);
        lblSiteName2.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblSiteName2.setText("Site Name:");
        panel1.add(lblSiteName2, CC.xy(1, 1));

        //---- lblSiteName ----
        lblSiteName.setForeground(Color.darkGray);
        lblSiteName.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblSiteName.setText("Gemini3 RTU Site Name Gemini3 RTU Site Name Gemini3 RTU Site Name");
        panel1.add(lblSiteName, CC.xy(3, 1));
      }
      panelInfo.add(panel1, CC.xywh(1, 1, 15, 1));

      //---- label1 ----
      label1.setText("RTU IP Address:");
      label1.setForeground(Color.darkGray);
      panelInfo.add(label1, CC.xy(1, 3));

      //---- lblRtuIpAddress ----
      lblRtuIpAddress.setForeground(Color.darkGray);
      lblRtuIpAddress.setText("10.11.11.102");
      lblRtuIpAddress.setFont(lblRtuIpAddress.getFont().deriveFont(lblRtuIpAddress.getFont().getStyle() | Font.BOLD));
      panelInfo.add(lblRtuIpAddress, CC.xy(3, 3));

      //---- label2 ----
      label2.setText("Door Status:");
      label2.setForeground(Color.darkGray);
      panelInfo.add(label2, CC.xy(5, 3));

      //---- lblDoorStatus ----
      lblDoorStatus.setForeground(Color.darkGray);
      lblDoorStatus.setText("Door Status");
      lblDoorStatus.setFont(lblDoorStatus.getFont().deriveFont(lblDoorStatus.getFont().getStyle() | Font.BOLD));
      panelInfo.add(lblDoorStatus, CC.xy(7, 3));

      //---- label3 ----
      label3.setText("Alarm Status:");
      label3.setForeground(Color.darkGray);
      panelInfo.add(label3, CC.xy(1, 5));

      //---- lblAlarmStatus ----
      lblAlarmStatus.setText("Alarm Status");
      lblAlarmStatus.setForeground(Color.darkGray);
      lblAlarmStatus.setFont(lblAlarmStatus.getFont().deriveFont(lblAlarmStatus.getFont().getStyle() | Font.BOLD));
      panelInfo.add(lblAlarmStatus, CC.xy(3, 5));

      //---- label4 ----
      label4.setText("OLR Status:");
      label4.setForeground(Color.darkGray);
      panelInfo.add(label4, CC.xy(5, 5));

      //---- lblOLRStatus ----
      lblOLRStatus.setForeground(Color.darkGray);
      lblOLRStatus.setText("ORL");
      lblOLRStatus.setFont(lblOLRStatus.getFont().deriveFont(lblOLRStatus.getFont().getStyle() | Font.BOLD));
      panelInfo.add(lblOLRStatus, CC.xy(7, 5));

      //---- label5 ----
      label5.setText("Automation Status:");
      panelInfo.add(label5, CC.xy(11, 5));

      //---- lblAutoStatus ----
      lblAutoStatus.setFont(lblAutoStatus.getFont().deriveFont(lblAutoStatus.getFont().getStyle() | Font.BOLD));
      lblAutoStatus.setForeground(Color.darkGray);
      panelInfo.add(lblAutoStatus, CC.xy(13, 5));
    }
    add(panelInfo, CC.xy(1, 1));

    //---- labelCommLost ----
    labelCommLost.setText("Communication lost!");
    labelCommLost.setForeground(Color.red);
    labelCommLost.setFont(labelCommLost.getFont().deriveFont(labelCommLost.getFont().getStyle() | Font.BOLD));
    add(labelCommLost, CC.xy(1, 3));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
    
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panelInfo;
  private JPanel panel1;
  private JLabel lblSiteName;
  private JLabel lblRtuIpAddress;
  private JLabel lblDoorStatus;
  private JLabel lblAlarmStatus;
  private JLabel lblOLRStatus;
  private JLabel label5;
  private JLabel lblAutoStatus;
  private JLabel labelCommLost;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
