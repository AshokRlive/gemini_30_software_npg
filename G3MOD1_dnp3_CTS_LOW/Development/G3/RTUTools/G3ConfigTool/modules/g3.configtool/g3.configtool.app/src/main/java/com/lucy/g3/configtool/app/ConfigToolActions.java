/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import javax.swing.JOptionPane;

import org.jdesktop.application.Action;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.preferences.PreferenceManager;
import com.lucy.g3.configtool.preferences.security.SecurityPreference;
import com.lucy.g3.configtool.preferences.security.SecurityPreferenceApplier;
import com.lucy.g3.configtool.preferences.ui.PreferenceDialog;
import com.lucy.g3.configtool.rtu.restart.RTURestartController;
import com.lucy.g3.configtool.subsystem.AbstractSubsystem;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.dialogs.AboutBox;
import com.lucy.g3.rtu.cert.manager.RTUCertManager;
import com.lucy.g3.rtu.cert.manager.ui.RTUCertWizard;
import com.lucy.g3.rtu.comms.protocol.G3Protocol.RestartMode;
import com.lucy.g3.rtu.comms.service.rtu.control.RTUCertCommands;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;

public class ConfigToolActions extends AbstractSubsystem{
  public final static String SUBSYSTEM_ID = "MainAppActions";
  
  public static final String ACTION_ABOUT = "about"; // Default action defined by Application
  public static final String ACTION_EXIT = "quit"; // Default action defined by Application
  public static final  String ACTION_PREFERENCE = "preferences";
  
  public static final String ACTION_SHOW_CERT_WIZARD = "showCertWizard"; 
  public static final String ACTION_VIEW_RTU_CERT = "viewRTUCert";
  
  private RTUCertManager certManager;
  
  ConfigToolActions(IContext context) {
    super(context, SUBSYSTEM_ID);
  }
  
  @Action
  public void preferences() {
    PreferenceManager prefs = PreferenceManager.INSTANCE;
    
    PreferenceDialog prefdialog = new PreferenceDialog(getMainFrame(), prefs);
    prefdialog.setVisible(true);
    
    SecurityPreference pref = prefs.getPreference(SecurityPreference.ID);
    SecurityPreferenceApplier.apply(getRTU().getComms().getLinkLayer().getSecurityManager(), pref);
  }
  
  @Action
  public void about() {
    new AboutBox(getMainFrame(), ConfigToolVersion.getInstance().getManifest()).setVisible(true);
  }
  
  @Action(enabledProperty = PROPERTY_NORMAL_MODE)
  public void showCertWizard() {
    if(certManager == null) {
      G3RTU rtu = getRTU();
      RTUCertCommands commands = new RTUCertCommands(rtu.getComms().getClient(), rtu.getComms().getFileTransfer());
      certManager = new RTUCertManager(commands, RTUCertManager.OWNER_HTTPS);
    }
    
    String option = RTUCertWizard.showWizard(certManager);
    
    // Show reboot message
    if(RTUCertWizard.OPTION_INSTALL_CERT.equals(option) 
        || RTUCertWizard.OPTION_GEN_SELFSIGN.equals(option)) {
      int ret = JOptionPane.showConfirmDialog(getMainFrame(), 
          "You must reboot RTU to apply the changes.\nDo you want to continue?",
          "Reboot RTU", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
      if(ret == JOptionPane.YES_OPTION) {
        RTURestartController controller = getContext().getComponent(RTURestartController.SUBSYSTEM_ID);
        controller.restartRTU(RestartMode.REBOOT);
      }
    }
  }
  
  @Action(enabledProperty = PROPERTY_CONNECTED)
  public  void viewRTUCert(){
    RTUCertManager.viewRTUCert(getMainFrame(), getRTU().getHost());
  }
  
  private G3RTU getRTU() {
    return G3RTUFactory.getDefault();
  }

  @Override
  protected void notifyStateChanged(ConnectionState oldState, ConnectionState newState) {
    //Nothing to do
  }
}

