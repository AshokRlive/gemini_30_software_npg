/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.jgoodies.binding.beans.PropertyAdapter;
import com.jgoodies.binding.binder.ValueModelBindingBuilderImpl;
import com.jgoodies.binding.value.BindingConverter;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.configtool.ui.panels.RTUInfoPanel;
import com.lucy.g3.rtu.comms.shared.LoginAccount;
import com.lucy.g3.rtu.comms.shared.LoginResult;

class BannerFactory {
  private final IContext context;
  
  
  public BannerFactory(IContext context) {
    super();
    this.context = context;
  }

  public JComponent createInfoPane(){
    return new RTUInfoPanel(context);
  }
  
  public JComponent[] createToolBarItems(){
    ArrayList<JComponent> toobarlItems = new ArrayList<>();

    ConnectionManager conMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);

    // Create value mode for binding
    PropertyAdapter<ConnectionManager> vm_account = new PropertyAdapter<ConnectionManager>(conMgr,
        ConnectionManager.PROPERTY_LOGIN_ACCOUNT, true);

    PropertyAdapter<ConnectionManager> vm_login = new PropertyAdapter<ConnectionManager>(conMgr,
        ConnectionManager.PROPERTY_LOGIN_INFO, true);

    JLabel label;
    ValueModelBindingBuilderImpl binder;
    
    /* Add user label*/
    label = new JLabel("User");
    label.setForeground(Color.darkGray);
    binder = new ValueModelBindingBuilderImpl(vm_account);
    binder.converted(new LoginAccountConverter()).to(label);
    toobarlItems.add(label);
    
    /* Add user level label*/
    label = new JLabel("User Level");
    label.setForeground(Color.darkGray);
    binder = new ValueModelBindingBuilderImpl(vm_login);
    binder.converted(new LoginResultConverter()).to(label);
    toobarlItems.add(label);

    
    /* Add spacer*/
    JPanel hSpacer = new JPanel(null);
    toobarlItems.add(hSpacer);
    
    /* Add Connect button*/
    JButton[] btns = createConnectButtons();
    for (int i = 0; i < btns.length; i++) {
      toobarlItems.add(btns[i]);
    }
  
    return toobarlItems.toArray(new JComponent[toobarlItems.size()]);
  }
  
  private JButton[] createConnectButtons(){
    final JButton btnConnect = new JButton("Connect");
    
    ConnectionManager conMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
    final Action connectAction = conMgr.getAction(ConnectionManager.ACTION_KEY_CONNECT);
    final Action disconnectAction = conMgr.getAction(ConnectionManager.ACTION_KEY_DISCONNECT);
    final Action swUserAction = conMgr.getAction(ConnectionManager.ACTION_KEY_SWITCH_USER);
    btnConnect.setAction(connectAction);
    
    // Update button action when connection state changes.
    EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, 
        new IEventHandler() {
      @Override
      public void handleEvent(Event event) {
        ConnectionState newState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
        /* Update button actions */
        if (newState != ConnectionState.DISCONNECTED) {
          btnConnect.setAction(disconnectAction);

        } else if (newState == ConnectionState.DISCONNECTED) {
          btnConnect.setAction(connectAction);
        }
      }
    });
    
    return new JButton[]{
        btnConnect,
        new JButton(swUserAction),
    };
  }
  
  /**
   * A binding converter for converting login session to user name.
   */
  private static class LoginAccountConverter implements BindingConverter<LoginAccount, String> {

    @Override
    public LoginAccount sourceValue(String arg0) {
      return null;
    }

    @Override
    public String targetValue(LoginAccount subjectValue) {
      if (subjectValue != null) {
        return String.format("User: %s  ", subjectValue.getName());
      } else {
        return "";
      }
    }
  }
  
  /**
   * A binding converter for converting login session to user level.
   */
  private static class LoginResultConverter implements BindingConverter<LoginResult, String> {

    @Override
    public LoginResult sourceValue(String arg0) {
      throw new UnsupportedOperationException("Not supported!");
    }

    @Override
    public String targetValue(LoginResult subjectValue) {
      if (subjectValue != null) {
        return String.format("  Role: %s  ", subjectValue.getUserLevelText());
      } else {
        return "";
      }
    }
  }
}

