/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.jdesktop.application.Application;
import org.jdesktop.application.TaskService;

import com.lucy.g3.common.context.IContext;


/**
 * Task service for running communication task.
 */
final class CommsTaskService extends TaskService {

  /**
   * The name of the task scheduler that runs communicating tasks.
   */
  private static final String COMMS_TASK_SERVICE_NAME = IContext.COMMS_TASK_SERVICE_NAME;

  CommsTaskService() {
    super(COMMS_TASK_SERVICE_NAME, new ThreadPoolExecutor(
        3, // corePool size
        10, // maximumPool size
        1L, TimeUnit.SECONDS, // non-core threads time to live
        new SynchronousQueue<Runnable>(),
        new ThreadFactory() {

          private int id = 0;


          @Override
          public Thread newThread(Runnable r) {
            return new Thread(r, "Communication Thread-" + (id++));
          }
        }));
  }
}
