/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.integrity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jgoodies.common.bean.ObservableBean2;
import com.lucy.g3.common.utils.EqualsUtil;
import com.lucy.g3.rtu.config.channel.domain.ChannelType;
import com.lucy.g3.rtu.config.clogic.switchgear.SwitchGearLogic;
import com.lucy.g3.rtu.config.clogic.utils.CLogicUtility;
import com.lucy.g3.rtu.config.module.canmodule.domain.ICANChannel;
import com.lucy.g3.rtu.config.module.canmodule.domain.ModuleSCM;
import com.lucy.g3.rtu.config.module.shared.domain.Module;
import com.lucy.g3.rtu.config.module.shared.stub.ModuleStub;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3Point;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointFactory;
import com.lucy.g3.rtu.config.protocol.dnp3.slave.domain.point.SDNP3PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.IEC870PointType;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870InputPoint;
import com.lucy.g3.rtu.config.protocol.iec870.shared.domain.point.IEC870PointFactory;
import com.lucy.g3.rtu.config.protocol.modbus.master.domain.MMBIoChannel;
import com.lucy.g3.rtu.config.protocol.slave.shared.domain.source.ScadaPointInput;
import com.lucy.g3.rtu.config.shared.base.exceptions.DuplicatedException;
import com.lucy.g3.rtu.config.shared.model.IElement;
import com.lucy.g3.rtu.config.virtualpoint.pseudo.domain.PseudoAnaloguePoint;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.StdAnaloguePoint;
import com.lucy.g3.xml.gen.common.ControlLogicDef.SGL_POINT;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE_ID;

/**
 * The Class NameUpdateTest.
 */
public class NameUpdateTest {

  private final static String NAME = IElement.PROPERTY_NAME;
  private static Logger log = Logger.getLogger("NameUpdateTest");

  private ModuleSCM module;
  private Module device;
  private ICANChannel canChannel;
  private MMBIoChannel deviceChannel;
  private StdAnaloguePoint vpoint;
  private SwitchGearLogic clogic;
  private SDNP3Point dnp3Point;
  private IEC870InputPoint s104Point;
  private PseudoAnaloguePoint clogicPoint;

  private NameObserver nameObserver;


  @Before
  public void setUp() throws Exception {
    nameObserver = new NameObserver();

    clogic = new SwitchGearLogic();
    clogicPoint = CLogicUtility.findAnalogPoint(clogic.getAllPoints(), SGL_POINT.SGL_POINT_PEAKMOTORCURRENT);
    vpoint = com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory.createAnalogPoint();
    module = new ModuleSCM();
    device = new ModuleStub(MODULE_ID.MODULE_ID_0, MODULE.MODULE_MBDEVICE);
    canChannel = module.getChByTyeAndID(0, ChannelType.ANALOG_INPUT);
    deviceChannel = new MMBIoChannel(ChannelType.ANALOG_INPUT, 0, device, "ABC");
    vpoint.setChannel(canChannel);
    dnp3Point = SDNP3PointFactory.create(null, SDNP3PointType.AnalogueInput, 0);
    s104Point = IEC870PointFactory.createInputPoint(null, IEC870PointType.MIT, 0);
    ScadaPointInput scadaSource = new ScadaPointInput(vpoint);

    dnp3Point.mapTo(scadaSource);
    s104Point.mapTo(scadaSource);

    log.setLevel(Level.DEBUG);
    // LoggingUtil.logPropertyChanges(vpoint,log);
    // LoggingUtil.logPropertyChanges(scadaSource,log);
    // LoggingUtil.logPropertyChanges(dnp3Point,log);
    // LoggingUtil.logPropertyChanges(s104Point,log);
    // LoggingUtil.logPropertyChanges(module,log);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testChannel() {
    nameObserver.observe(canChannel);
    changeModule();
    nameObserver.assertChanged();
  }

  @Test
  public void testVirtualPoint() {
    nameObserver.observe(vpoint);
    changeModule();
    nameObserver.assertChanged();
  }

  @Test
  public void testCLogicPoint() {
    nameObserver.observe(clogicPoint);
    clogic.setGroup(clogic.getGroup() + 1);
    nameObserver.assertChanged();
  }

  @Test
  public void testDNP3Point() {
    nameObserver.observe(dnp3Point);
    changeVirtualPoint();
    nameObserver.assertChanged();
  }

  @Test
  public void testDNP3Point2() {
    nameObserver.observe(dnp3Point);
    dnp3Point.setCustomDescription("NewDescription");
    nameObserver.assertChanged();
  }

  @Test
  public void testDNP3Point3() throws DuplicatedException {
    nameObserver.observe(dnp3Point);
    dnp3Point.setPointID(dnp3Point.getPointID() + 1);
    nameObserver.assertChanged();
  }

  @Test
  public void testS104Point() {
    nameObserver.observe(s104Point);
    changeVirtualPoint();
    nameObserver.assertChanged();
  }

  @Test
  public void testFieldDeviceChannel() {
    nameObserver.observe(vpoint);
    vpoint.setChannel(deviceChannel);
    assertTrue(vpoint.getChannel() == deviceChannel);
    nameObserver.assertChanged();
  }

  @Test
  public void testVirtualPointDescriptionSyncWithChannel() {
    vpoint.setCustomDescriptionEnabled(false);
    vpoint.setDescription("This is default description");

    vpoint.setChannel(deviceChannel);
    assertEquals(deviceChannel.getDescription(), vpoint.getDescription());

    String newDesc = "New Description";
    deviceChannel.setDescription(newDesc);
    assertEquals(newDesc, vpoint.getDescription());
  }

  private void changeModule() {
    module.setId(MODULE_ID.MODULE_ID_11);
  }

  private void changeVirtualPoint() {
    vpoint.setDescription("VPointNewDescription");
    ;
  }

  public void printNameChange() {
    // vpoint.setDescription("This is Point A");
    print();
    System.out.println("\n");

    // make changes
    module.setId(MODULE_ID.MODULE_ID_3);
    clogic.setGroup(12);
    vpoint.setId(123);

    print();

  }

  private void print() {
    System.out.println("Channel Name:" + canChannel.getName());
    System.out.println("Channel Description:" + canChannel.getDescription());

    System.out.println("Virtual Point Name:" + vpoint.getName());
    System.out.println("Virtual Point Description:" + vpoint.getDescription());

    System.out.println("CLogic Point Name:" + clogicPoint.getName());
    System.out.println("CLogic Point Description:" + clogicPoint.getDescription());

    System.out.println("DNP3 Point Name:" + dnp3Point.getName());
    System.out.println("DNP3 Point Description:" + dnp3Point.getDescription());
  }


  private static class NameObserver implements PropertyChangeListener {

    private boolean changed = false;
    private ObservableBean2 bean;
    private String oldName;
    private String newName;


    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      if (NAME.equals(evt.getPropertyName())
          && !EqualsUtil.areEqual(evt.getOldValue(), evt.getNewValue())) {
        changed = true;
        newName = (String) evt.getNewValue();
        log.info("Name updated to: " + newName);
      }
    }

    public void observe(ObservableBean2 bean) {
      this.bean = bean;
      bean.addPropertyChangeListener(NAME, this);

      try {
        Method getter = bean.getClass().getMethod("getName");
        oldName = (String) getter.invoke(bean);
      } catch (Throwable e) {
        e.printStackTrace();
      }
    }

    public void assertChanged() {
      log.info(String.format("\"%-20s\"->Name changed detected. %s ==> %s\n",
          bean.getClass().getSimpleName(), oldName, newName));
      assertTrue("Expected name change event in:\"" + bean + "\"", changed);
    }
  }

}
