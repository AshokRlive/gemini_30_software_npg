/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.swingx.search.SearchFactory;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.rtu.config.ConfigurationManager;
import com.lucy.g3.gui.framework.workbench.WorkbenchWindowTab;
import com.lucy.g3.rtu.config.shared.manager.IConfig;

/**
 * This implementation of <code>TabContent</code> manages G3 configuration in a
 * dynamic tree. The selection of a tree node will show the content of the
 * selected node in a <code>PageViewer</code> .
 */
public class ConfigurationTab extends WorkbenchWindowTab  {
  public final static String ID = "ConfTabContent";

  private final static String ACTION_EXPAND_ALL = "expandAll";
  private final static String ACTION_COLLAPSE_ALL = "collapseAll";
  private final static String ACTION_FIND = "find";
  

  /* Configuration explorer tree.*/
  private final ConfigTree confTree; 
  private final IContext context;


  public ConfigurationTab(IContext context) {
    super(ID);
    this.context = context;
    ConfigurationManager configMgr = context.getComponent(ConfigurationManager.SUBSYSTEM_ID);
    this.confTree = new ConfigTree(new ConfigurationRoot(context), getPageViewer(), new ConfigPageFactory());
    this.confTree.setContextMenu(createConfTreeContextMenu(configMgr));

    JScrollPane pane = new JScrollPane(confTree);
    pane.setViewportBorder(BorderFactory.createEmptyBorder());
    pane.setBorder(BorderFactory.createMatteBorder(8, 15, 8, 0, Color.white));
    getComponent().add(pane, BorderLayout.CENTER);
    
    // Observe the change of configuration data
    configMgr.addPropertyChangeListener(
        ConfigurationManager.PROPERTY_CONFIG_DATA,
        new ConfigDataPCL());

  }

  @org.jdesktop.application.Action
  public void find(){
    /* Deep search Workaround: do deep a search the tree must be expanded first.*/
    expandAll();  
    
    /* Run later is required to solve the bug that find window disappears after open.*/ 
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        SearchFactory.getInstance().showFindDialog(confTree, confTree.getSearchable());
      }
    });
  }
  
  @org.jdesktop.application.Action
  public void expandAll(){
    confTree.expandAll();
  }
  
  @org.jdesktop.application.Action
  public void collapseAll(){
    confTree.collapseAll();
  }
  
  /**
   * Save the session for all tree nodes.
   */
  @Override
  public void saveSession() {
    getPageViewer().clear();
    confTree.saveSession();
  }

  private JPopupMenu createConfTreeContextMenu(ConfigurationManager configMgr) {
    JPopupMenu contextMenu = new JPopupMenu();

    ApplicationActionMap actionMap = configMgr.getActionMap();

    // Add menu items
    JMenu submenu = new JMenu("New");
    submenu.add(actionMap.get(ConfigurationManager.ACTION_CREATE_FROM_SCRATCH));
    // hide create from template as it keeps breaking when the schema changes
    // newMenu.add(actionMap.get(ConfigurationManager.ACTION_CREATE_FROM_TEMPL));
    submenu.add(actionMap.get(ConfigurationManager.ACTION_OPEN_CONFIG));
    contextMenu.add(submenu);
    contextMenu.add(actionMap.get(ConfigurationManager.ACTION_CLOSE_CONFIG));
    contextMenu.addSeparator();
    contextMenu.add(actionMap.get(ConfigurationManager.ACTION_SAVE_CONFIG));
    contextMenu.add(actionMap.get(ConfigurationManager.ACTION_SAVEAS_CONFIG));
    contextMenu.addSeparator();
    submenu = new JMenu("RTU Configuration");
    submenu.add(actionMap.get(ConfigurationManager.ACTION_READ_CONFIG));
    submenu.add(actionMap.get(ConfigurationManager.ACTION_WRITE_CONFIG));
    submenu.add(actionMap.get(ConfigurationManager.ACTION_ACTIVATE_CONFIG));
    submenu.add(actionMap.get(ConfigurationManager.ACTION_RESTORE_CONFIG));
    submenu.add(actionMap.get(ConfigurationManager.ACTION_ERASE_CONFIG));
    contextMenu.add(submenu);
    contextMenu.addSeparator();
    contextMenu.add(actionMap.get(ConfigurationManager.ACTION_VALIDATE_CONFIG));
    contextMenu.add(actionMap.get(ConfigurationManager.ACTION_CLEAN_CONFIG));
    contextMenu.add(actionMap.get(ConfigurationManager.ACTION_PRINT_COFIG));
    contextMenu.addSeparator();
    actionMap = Application.getInstance().getContext().getActionMap(this);
    contextMenu.add(actionMap.get(ACTION_EXPAND_ALL));
    contextMenu.add(actionMap.get(ACTION_COLLAPSE_ALL));
    contextMenu.add(actionMap.get(ACTION_FIND));
    
    return contextMenu;
  }

  @Override
  public void tabSelectedActionPerform(boolean isSelected) {
    // do nothing when this tab is selected
  }


  private class ConfigDataPCL implements PropertyChangeListener {

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      confTree.setConfigData((IConfig) evt.getNewValue());
    }

  }

}
