/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.tasks;

import java.io.File;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import org.jdesktop.application.Application;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.lucy.g3.configtool.rtu.controller.task.RTUInfoReadTask;
import com.lucy.g3.rtu.config.constants.VirtualPointType;
import com.lucy.g3.rtu.config.integrated.ConfigPlugin;
import com.lucy.g3.rtu.config.shared.manager.ConfigFactory;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.rtu.config.validation.impl.IConfigValidator;
import com.lucy.g3.rtu.config.virtualpoint.shared.domain.VirtualPointManager;
import com.lucy.g3.rtu.config.virtualpoint.standard.domain.impl.VirtualPointFactory;
import com.lucy.g3.rtu.filetransfer.FileReadingTask;
import com.lucy.g3.rtu.filetransfer.FileWritingTask;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.CTH_TRANSFERTYPE;

/**
 * The Class TasksTest.
 */
@Ignore
// TODO GUI test lib needed
public class TasksTest {

  private Application app;
  private G3RTU g3RTU;


  @Before
  public void setUp() throws Exception {
    ConfigPlugin.init();
    app = Application.getInstance();
    g3RTU = new G3RTU("10.11.11.102");
    
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConfigDataCleaningTask() throws InterruptedException, ExecutionException {
    IConfig data = ConfigFactory.getInstance().create();
    VirtualPointManager man = data.getConfigModule(VirtualPointManager.CONFIG_MODULE_ID);
    man.addPoint(VirtualPointFactory.createStanardPoint(VirtualPointType.ANALOGUE_INPUT));

    IConfigValidator validator = data.getConfigModule(IConfigValidator.CONFIG_MODULE_ID);
    validator.executeCleaning(app).get();
  }

  @Test
  public void testRTUInfoReadTask() throws InterruptedException, ExecutionException {
    RTUInfoReadTask task = new RTUInfoReadTask(app, null, g3RTU.getComms());
    task.execute();
    try {
      task.get();
    } catch (CancellationException e) {
      // Nothing
    }
  }


  // ================== Task Test ===================
  @Test
  public void testWriteMCMFirmware() throws Exception {
    FileWritingTask task =
        new FileWritingTask(Application.getInstance(), g3RTU.getComms().getFileTransfer(), CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_MCM,
            new File("MCMAppSample.zip"));
    task.execute();
    task.get();// wait
  }

  @Test
  public void testReadLog() throws Exception {
    FileReadingTask task =
        new FileReadingTask(Application.getInstance(), g3RTU.getComms().getFileTransfer(), CTH_TRANSFERTYPE.CTH_TRANSFERTYPE_LOG,
            "MCMUpdater.log", new File("MCMUpdater.log"));
    task.execute();
    task.get();// wait
  }

}
