/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.ui;

import javax.swing.tree.TreeNode;

import com.lucy.g3.common.context.IContext;
import com.lucy.g3.configtool.subsys.realtime.ui.modules.ModuleInfoPage;
import com.lucy.g3.configtool.subsys.realtime.ui.overview.RTMonitorRootPage;
import com.lucy.g3.configtool.subsys.realtime.ui.points.PointStatusPage;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.framework.navigation.ExplorerTree;
import com.lucy.g3.gui.framework.navigation.IExplorerTreeNode;
import com.lucy.g3.gui.framework.page.BlankPage;
import com.lucy.g3.gui.framework.page.IPageFactory;
import com.lucy.g3.gui.framework.page.IPageViewer;
import com.lucy.g3.gui.framework.page.Page;
import com.lucy.g3.rtu.comms.service.CommsUtil;
import com.lucy.g3.rtu.comms.service.realtime.modules.RTUModules;
import com.lucy.g3.rtu.comms.service.realtime.points.RTUPoints;
import com.lucy.g3.rtu.manager.G3RTU;
import com.lucy.g3.rtu.manager.G3RTUFactory;

/**
 * Tree of real-time monitor.
 */
public class RealtimeMonitorTree extends ExplorerTree
    implements IPageFactory, IConnectionStateHandler {
  private IContext context;
  
  public RealtimeMonitorTree(IContext context, IPageViewer viewer) {
    super(viewer, new RTMonitorRootPage(context, G3RTUFactory.getDefault()));
    this.context = context;
    setRowHeight(25);

    G3RTU rtu = G3RTUFactory.getDefault();
    Object[] nodeData = new Object[] {
        rtu.getComms().getRtuModules(),
        rtu.getComms().getRtuPoints(),
        PROTO_POINT_NODE_DATA
    };
    reloadTreeInEDT(nodeData);
  }

  @Override
  protected IPageFactory getPageFacotry() {
    return this;
  }

  @Override
  public Page createPage(Object data) {
    if (data == null) {
      return null;
    }

    Page page;

    if (data instanceof G3RTU) {
      page = new RTMonitorRootPage(context, (G3RTU) data);

    } else if (data instanceof RTUModules) {
      page = new ModuleInfoPage((RTUModules) data);

    } else if (data instanceof RTUPoints) {
      page = new PointStatusPage(context, (RTUPoints) data, PointStatusPage.VIEW_TYPE_VIRTUAL);

    /* Patch: for creating page for protocol point manager. */
    } else if (data.equals(PROTO_POINT_NODE_DATA)) {
      G3RTU rtu = G3RTUFactory.getDefault();
      page = new PointStatusPage(context, rtu.getComms().getRtuPoints(), PointStatusPage.VIEW_TYPE_PROTOCOL);

    } else {
      page = new BlankPage(data.getClass().getSimpleName(), "Not Implemented");
    }

    return page;
  }

  @Override
  public void handleStateChange(ConnectionState oldState, ConnectionState newState) {
    notifyConnectionStateChanged(getRootNode(), oldState, newState);
  }

  /**
   * Notifies the page of this node and pages of children nodes that connections
   * state has changed.
   */
  private void notifyConnectionStateChanged(TreeNode parent, ConnectionState oldState,
      ConnectionState newState) {
    if (parent == null) {
      return;
    }

    if (parent instanceof IExplorerTreeNode) {
      Page page = ((IExplorerTreeNode) parent).getPage();
      if (page instanceof IConnectionStateHandler) {
        ((IConnectionStateHandler) page).handleStateChange(oldState, newState);
      }
    }

    if (parent.getChildCount() > 0) {
      for (int index = 0; index < parent.getChildCount(); index++) {
        TreeNode child = parent.getChildAt(index);
        notifyConnectionStateChanged(child, oldState, newState);
      }
    }
  }


  /*
   * Patch: Since there is no node data defined for protocol point, this is a
   * fake one used for creating page for protocol points monitor.
   */
  private static final Object PROTO_POINT_NODE_DATA = "protocolPoint";

}
