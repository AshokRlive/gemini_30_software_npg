/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.awt.Window;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.lucy.g3.configtool.subsys.permissions.AbstractPermissionChecker;
import com.lucy.g3.configtool.subsys.permissions.IPermissionChecker;
import com.lucy.g3.configtool.subsys.permissions.IPermissionCheckerCallback;
import com.lucy.g3.configtool.subsys.permissions.IPermissonCheckReporter;
import com.lucy.g3.configtool.subsys.permissions.UserActivities;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.gui.common.widgets.utils.WindowUtils;
import com.lucy.g3.rtu.comms.shared.LoginResult;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.USER_LEVEL;

/**
 * Implementation of {@linkplain IPermissionChecker} and {@linkplain IPermissonCheckReporter}.
 */
class PermissionCheckerImpl extends AbstractPermissionChecker implements IPermissonCheckReporter{
  public PermissionCheckerImpl(IPermissionCheckerCallback cb) {
    super(cb);
  }

  @Override
  public void showActionNotPermittedMsg(String title, String error, UserActivities activity, USER_LEVEL userlevel) {
    showMsgDlgInEDT(error, title, JOptionPane.ERROR_MESSAGE);
  }

  @Override
  public void showErrorMsg(String title, String error) {
    showMsgDlgInEDT(error, title, JOptionPane.ERROR_MESSAGE);
  }

  private void showMsgDlgInEDT(
      final String msg,
      final String title,
      final int msgType) {
    final Window parent = WindowUtils.getMainFrame();
    if (SwingUtilities.isEventDispatchThread()) {
      JOptionPane.showMessageDialog(parent, msg, title, msgType);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          JOptionPane.showMessageDialog(parent, msg, title, msgType);
        }
      });
    }
  }
}
