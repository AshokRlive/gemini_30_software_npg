/**************************************************************anager*****************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool;

import com.lucy.g3.rtu.config.generator.ConfigGeneratorRegistry;
import com.lucy.g3.rtu.config.generator.GenerateOptions;
import com.lucy.g3.rtu.config.generator.GenerateOptions.GenerateOption;
import com.lucy.g3.rtu.config.module.canmodule.domain.CANModuleManager;
import com.lucy.g3.rtu.config.module.iomodule.manager.IIOModuleGenerator;
import com.lucy.g3.rtu.config.shared.manager.IConfig;
import com.lucy.g3.test.support.utilities.TestUtil;
import com.lucy.g3.xml.gen.common.ModuleProtocolEnum.MODULE;

/**
 * The Class ExtTestUtil.
 */
public final class ExtTestUtil extends TestUtil {

  private ExtTestUtil() {
  }

  public static void buildIConfig(IConfig data) {
    ConfigGeneratorRegistry gens = ConfigGeneratorRegistry.getInstance();
    // Create module
    IIOModuleGenerator factory = gens.getGenerator(data,IIOModuleGenerator.class);
    CANModuleManager manager = data.getConfigModule(CANModuleManager.CONFIG_MODULE_ID); 
    factory.createCANModule(manager, MODULE.MODULE_DSM, 7);
    factory.createCANModule(manager, MODULE.MODULE_SCM, 7);
    factory.createCANModule(manager, MODULE.MODULE_FPM, 7);
    factory.createCANModule(manager, MODULE.MODULE_HMI, 1);
    factory.createCANModule(manager, MODULE.MODULE_IOM, 7);
    factory.createCANModule(manager, MODULE.MODULE_PSM, 1);
    factory.createCANModule(manager, MODULE.MODULE_MCM, 1);

    GenerateOptions option = new GenerateOptions();
    option.addOption(GenerateOptions.Target.VirtualPoint, GenerateOption.GenerateFull);
    option.addOption(GenerateOptions.Target.ScadaPoint, GenerateOption.DoNotGenerate);

    // Create points
//    Collection<IOModule> mlist = IOModuleFinder.findIOModules(manager.getAllModules());
//    IVirtualPointGenerator factory2 = gens.getGenerator(IVirtualPointGenerator.class);
    //factory2.createVirtualPoints(option, mlist.toArray(new IOModule[mlist.size()]));
  }

//  public static void createSampleConfigFile(String filepath) {
//    IConfig config = new IConfigImpl();
//    ConfiguratorImpl factory = new ConfiguratorImpl(config);
//
//    CANModuleManager mmgr = config.getCANModulesManager();
//    mmgr.addModule(MODULE.MODULE_PSM, MODULE_ID.MODULE_ID_0);
//    mmgr.addModule(MODULE.MODULE_MCM, MODULE_ID.MODULE_ID_0);
//
//    Collection<IChannel> allchs = config.getAllChannels(ChannelType.ANALOG_INPUT, ChannelType.DIGITAL_INPUT);
//
//    factory.createVirtualPoints(ConfiguratorOption.genDefaultVPoint(), allchs);
//    try {
//      new XmlDataWriter(config, null).saveToFile("sample01.xml");
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
}
