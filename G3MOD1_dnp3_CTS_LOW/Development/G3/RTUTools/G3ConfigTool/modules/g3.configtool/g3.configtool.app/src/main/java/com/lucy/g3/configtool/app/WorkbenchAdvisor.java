/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.InetSocketAddress;

import javax.swing.JComponent;

import org.jdesktop.application.Application;
import org.jdesktop.swingx.util.WindowUtils;

import com.lucy.g3.cert.manager.gui.KeyStoreActions;
import com.lucy.g3.common.context.IContext;
import com.lucy.g3.common.event.Event;
import com.lucy.g3.common.event.EventBroker;
import com.lucy.g3.common.event.EventTopics;
import com.lucy.g3.common.event.IEventHandler;
import com.lucy.g3.configtool.rtu.connection.ConnectionManager;
import com.lucy.g3.configtool.subsystem.ConnectionState;
import com.lucy.g3.configtool.ui.CommissioningTab;
import com.lucy.g3.configtool.ui.ConfigurationTab;
import com.lucy.g3.configtool.ui.RealtimeMonitorTab;
import com.lucy.g3.configtool.ui.UtilitiesTab;
import com.lucy.g3.gui.framework.workbench.IWorkbenchMenuAdvisor;
import com.lucy.g3.gui.framework.workbench.IWorkbenchStatusBar;
import com.lucy.g3.gui.framework.workbench.IWorkbenchWindowTab;
import com.lucy.g3.gui.framework.workbench.IWorkbenchStatusBar.Mode;
import com.lucy.g3.gui.framework.workbench.IWorkbenchStatusBar.Status;
import com.lucy.g3.gui.framework.workbench.internal.WorkbenchWindowAdvisor;
import com.lucy.g3.rtu.cert.manager.RTUCertManager;
import com.lucy.g3.rtu.comms.datalink.DataLink;
import com.lucy.g3.rtu.comms.datalink.ISecurityManager;
import com.lucy.g3.rtu.manager.G3RTUFactory;
import com.lucy.g3.security.support.SSLSupport;
import com.lucy.g3.xml.gen.common.G3ConfigProtocol.SERVICEMODE;


class WorkbenchAdvisor extends WorkbenchWindowAdvisor {
  private final MenuAdvisor menuAdvisor;
  private final IContext context;
  private final BannerFactory bannerFactory;


  public WorkbenchAdvisor(IContext context) {
    super();
    this.context = context;
    this.menuAdvisor = new MenuAdvisor(context);
    this.bannerFactory = new BannerFactory(context);
  }
  
  @Override
  public IWorkbenchMenuAdvisor getMenuAdvisor() {
    return menuAdvisor;
  }

  @Override
  public JComponent getBannerInfoPane() {
    return bannerFactory.createInfoPane();
  }

  @Override
  public JComponent[] getBannerToolbarItems() {
    return bannerFactory.createToolBarItems();
  }

  @Override
  public IWorkbenchWindowTab[] createTabs() {
    IWorkbenchWindowTab[] tabContents = new IWorkbenchWindowTab[4];
    
    tabContents[0] = new RealtimeMonitorTab(context);
    tabContents[1] = new ConfigurationTab(context);
    tabContents[2] = new UtilitiesTab(context);
    tabContents[3] = new CommissioningTab(context, ((UtilitiesTab)tabContents[2]).getFirwmarePage());

    return tabContents;
  }

  @Override
  public IWorkbenchStatusBar createStatusBar() {
    final IWorkbenchStatusBar statusBar = super.createStatusBar();
    ConnectionManager conMgr = context.getComponent(ConnectionManager.SUBSYSTEM_ID);
    javax.swing.Action[] contextActions = new javax.swing.Action[] {
        conMgr.getAction(ConnectionManager.ACTION_KEY_CONNECT),
        conMgr.getAction(ConnectionManager.ACTION_KEY_DISCONNECT),
    };
    statusBar.setContextActions(contextActions);
    
    // Register an action for security indicator.
    statusBar.setSecurityActionListener(
        new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        RTUCertManager.viewRTUCert(WindowUtils.findWindow(statusBar.getComponent()),
            statusBar.getHost());        
      }
    });

    
    new StatusBarUpdater(statusBar);
    
    return statusBar;
  }


  private static class StatusBarUpdater  {
    private final  IWorkbenchStatusBar statusBar;
    private final  DataLink  link;
    
    private ConnectionState connState = null;
    private SERVICEMODE serviceMode = null;
    private boolean debugMode;
    
    public StatusBarUpdater(IWorkbenchStatusBar statusBar) {
      super();
      this.statusBar = statusBar;
      this.link = G3RTUFactory.getDefault().getComms().getLinkLayer();
      
      // Update host name
      updateHost(link.getHost());
      this.link.addPropertyChangeListener(new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          if (DataLink.PROPERTY_HOST.equals(evt.getPropertyName())) {
            updateHost((String)evt.getNewValue());
          }          
        }
      });
      
      // Update security indicator
      ISecurityManager securitySupport = link.getSecurityManager();
      if(securitySupport != null) {
        updateSecureIndicator(securitySupport.isIgnoreCertificateError() == false);
        securitySupport.addPropertyChangeListener(ISecurityManager.PROPERTY_IGNORECERTIFICATEERROR, 
            new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent evt) {
            updateSecureIndicator((Boolean)evt.getNewValue() == false);
          }
        });
      }
      
      
      // Update connection state
      EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_CONN_STATE_CHG, 
          new IEventHandler() {
        @Override
        public void handleEvent(Event event) {
          connState = (ConnectionState) event.getProperty(EventTopics.EVT_PROPERTY_NEW_STATE);
          updateStatus();
        }
      });
      
      // Update service mode
      EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_SERVICE_MODE_CHG, 
          new IEventHandler() {
        @Override
        public void handleEvent(Event event) {
          serviceMode = (SERVICEMODE) event.getProperty(EventTopics.EVT_PROPERTY_SERVICE_MODE);
          updateStatus();
        }
      });
      
      // Update debug mode
      EventBroker.getInstance().subscribe(EventTopics.EVT_TOPIC_DEBUG_MODE_CHG, 
          new IEventHandler() {
        @Override
        public void handleEvent(Event event) {
          debugMode = (boolean) event.getEventData();
          updateStatus();
        }
      });
      
    }

    private void updateStatus() {
      IWorkbenchStatusBar.Status status = Status.NONE;
      IWorkbenchStatusBar.Mode   mode = Mode.NONE;
      
      if(ConnectionState.isConnected(connState)) {
        status = Status.CONNECTED;
      } else if(ConnectionState.isDisconnected(connState)) {
        status = Status.DISCONNECTED;
      } else if(connState == ConnectionState.CONNECTION_LOST) {
        status = Status.CONNECTION_LOST;
      }
      
      if(ConnectionState.isNormalMode(connState)) {
        mode = Mode.NORMAL;
        if(serviceMode == SERVICEMODE.SERVICEMODE_OUTSERVICE)
          mode = Mode.OUTSERVICE;
        
      }  else if (ConnectionState.isUpgradeMode(connState)) {
        mode = Mode.UPGRADE;
      }
      
      if(debugMode) {
        mode = Mode.DEBUG;
      } 
      
      
      statusBar.setStatus(status, mode);
    }
    
    private void updateHost(String hostName) {
      statusBar.setHost(hostName);
    }
    
    private void updateSecureIndicator(boolean isSecure) {
      String tips = isSecure ? "Secure Connection" : "Insecure Connection";
      statusBar.setSecurityLabel(isSecure, tips);
    }
    
  }
  
}

