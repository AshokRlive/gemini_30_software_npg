/*******************************************************************************
 * Lucy Electric: http://www.lucyelectric.com/ - Automation Dept.
 * ****************************************************************************
 *    COPYRIGHT
 *        This Document is the property of Lucy Electric.
 *        It must not be reproduced, in whole or in part, or otherwise
 *        disclosed without prior consent in writing from Lucy Electric.
 ******************************************************************************/

package com.lucy.g3.configtool.app;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.lucy.g3.rtu.cert.manager.RTUCertArgParser;

class ConfigToolArgParser {
  public static final String OPTION_UPDATE = "update";
  public static final String OPTION_VERSION = "version";
  public static final String OPTION_LOG_LEVEL = "loglevel";
  public static final String OPTION_HELP = "help";
  public static final String OPTION_CERT = RTUCertArgParser.OPTION_CERT;

  private static final String DEFAULT_JAR_NAME = "G3ConfigTool.jar";
  
  private final Options options;
  private CommandLine line;
  

  public ConfigToolArgParser() {
    options = new Options();
    
    // ====== Mandatory ======
    
    
    
    // ====== Optional ======
    Option help = Option.builder("h")
        .longOpt(OPTION_HELP)
        .desc("print this message.")
        .numberOfArgs(1).optionalArg(true).argName("OPTION").type(String.class)
        .build();
    
    Option update = Option.builder(OPTION_UPDATE)
        .longOpt(OPTION_UPDATE)
        .desc("update RTU software")
        .build();
    
    Option cert = Option.builder(OPTION_CERT)
        .longOpt(OPTION_CERT)
        .desc("update RTU certificate")
        .build();
    
    Option version = Option.builder("v")
        .longOpt(OPTION_VERSION)
        .desc("print version information")
        .build();
    
    Option log = Option.builder("l")
        .longOpt(OPTION_LOG_LEVEL)
        .numberOfArgs(1).argName("LEVEL")
        .desc("set log level to one of: DEBUG|INFO|WARN|ERROR|FATAL|OFF|ALL")
        .build();
    
    options.addOption(help);
    options.addOption(update);
    options.addOption(cert);
    options.addOption(version);
    options.addOption(log);
  }
  
  public void printHelp(){
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(
        "java -jar " + DEFAULT_JAR_NAME, 
        options, 
        true);
  }
  
  public CommandLine parse(String[] args) throws ParseException {
    CommandLineParser parser = new DefaultParser();
    line = parser.parse(options, args, true);
    return line;
  }

  public boolean hasOption(String optionName) {
    return  line.hasOption(optionName);
  }

  public String getOptionValue(String optionName) {
    return  line.getOptionValue(optionName);
  }
}
